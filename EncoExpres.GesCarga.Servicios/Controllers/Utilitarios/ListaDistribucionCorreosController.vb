﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Utilitarios
Imports EncoExpres.GesCarga.Negocio.Utilitarios

<Authorize>
Public Class ListaDistribucionCorreosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ListaDistribucionCorreos) As Respuesta(Of IEnumerable(Of ListaDistribucionCorreos))
        Return New LogicaListaDistribucionCorreos(CapaPersistenciaListaDistribucionCorreos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ListaDistribucionCorreos) As Respuesta(Of ListaDistribucionCorreos)
        Return New LogicaListaDistribucionCorreos(CapaPersistenciaListaDistribucionCorreos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ListaDistribucionCorreos) As Respuesta(Of Long)
        Return New LogicaListaDistribucionCorreos(CapaPersistenciaListaDistribucionCorreos).Guardar(entidad)
    End Function


End Class