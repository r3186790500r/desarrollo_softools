﻿
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Fachada.Paqueteria

Namespace Paqueteria
    Public Class LogicaRecepcionGuias
        Inherits LogicaBase(Of RemesaPaqueteria)



        ReadOnly _persistencia As IPersistenciaBase(Of RemesaPaqueteria)

        Sub New(persistencia As IPersistenciaBase(Of RemesaPaqueteria))
            _persistencia = persistencia
        End Sub
        Public Overrides Function Consultar(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Guardar(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function

        Public Function RecibirGuias(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente

            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRecepcionGuiasOficina).RecibirGuias(entidad)


            If valor.Equals(0) Then

                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)


            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Remesa.Numero, .MensajeOperacion = IIf(entidad.Remesa.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}
        End Function
    End Class
End Namespace
