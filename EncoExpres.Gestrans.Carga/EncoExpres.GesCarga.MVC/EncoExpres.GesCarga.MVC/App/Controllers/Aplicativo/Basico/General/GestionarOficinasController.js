﻿SofttoolsApp.controller("GestionarOficinasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'CiudadesFactory', 'OficinasFactory', 'ValorCatalogosFactory',
    'ZonasFactory', 'blockUIConfig', 'TercerosFactory', 'PlanUnicoCuentasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, CiudadesFactory, OficinasFactory, ValorCatalogosFactory,
        ZonasFactory, blockUIConfig, TercerosFactory, PlanUnicoCuentasFactory) {

        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'GESTIONAR OFICINAS';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Oficinas' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_OFICINAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.ListaTipoOficinas = [];
        $scope.ListadoZonas = [];
        $scope.ListaRegiones = [];
        $scope.ListadoCuentasPUC = [];
        $scope.HabilitarZonas = true;
        $scope.ListadoEstados = [
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');

        $scope.ListadoCiudades = [];
        $scope.MaxLength = 3;
        // Se crea la propiedad modelo en el ambito
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            AplicaEnturnamiento: false,
            PorcentajeSeguroPaqueteria: ''
        };


        $scope.SeccionComisiones = false;
        $scope.Comisiones = {
            Porcentaje: '',
            Tercero: '',
            TerceroAgencista: '',
            OficinaPrincipal: '',
            PorcentajeAgencista: ''
        };

        $scope.ListadoTercerosPerfilProovedor = [];


        $scope.ListadoTercerosPerfilProovedor = TercerosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            CadenaPerfiles: PERFIL_PROVEEDOR,

            Sync: true
        }).Datos;

        $scope.ListadoOficinas = [];
        $scope.ListadoOficinas.push({ Nombre: '(Seleccione...)', Codigo: -1 });
        $scope.ListadoOficinasFactura = [];
        $scope.ListadoOficinasFactura.push({ Nombre: 'SELECCIONE', Codigo: -1 });
        $scope.LongitudCiudad = 0;// verifica la cantidad de datos ingresados  de la ciudad
        $scope.IngresoValidaCiudad = true;
        $scope.CodigoCiudad = 0;
        $scope.Modelo.CuentaPUC = '';
        $scope.Comisiones.OficinaPrincipal = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
        $scope.Modelo.OficinaFactura = $linq.Enumerable().From($scope.ListadoOficinasFactura).First('$.Codigo == -1');
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete-----------//

        /*Cargar el combo de cuentas PUC*/
        var LPUC = PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true });
        if (LPUC != undefined) {

            if (LPUC.ProcesoExitoso === true) {
                $scope.ListadoCuentasPUC = LPUC.Datos
                //$scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)

            }
            else {
                $scope.ListadoCuentasPUC = [];
                $scope.Modelo.CuentaPUC = '';
            }

        }


        //--Ciudades
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        }
        //----------AutoComplete-----------//
        //-----------Init
        $scope.InitLoad = function () {
            //--Oficinas - Oficinas Factura
            var resOfic = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true });
            if (resOfic.ProcesoExitoso) {
                $scope.ListadoOficinasFactura = $scope.ListadoOficinasFactura.concat(resOfic.Datos);
                for (var i = 0; i < resOfic.Datos.length; i++) {
                    if (resOfic.Datos[i].CodigoTipoOficina == 9901) {
                        $scope.ListadoOficinas.push(resOfic.Datos[i]);
                    }
                }
            }
            //--Regiones
            OficinasFactory.ConsultarRegionesPaises({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaRegiones = response.data.Datos;

                            if ($scope.CodigoRegion !== undefined && $scope.CodigoRegion !== null && $scope.CodigoRegion !== '') {
                                $scope.Modelo.Region = $linq.Enumerable().From($scope.ListaRegiones).First('$.Codigo ==' + $scope.CodigoRegion);
                            } else {
                                $scope.Modelo.Region = $linq.Enumerable().From($scope.ListaRegiones).First('$.Codigo == 0');
                            }

                        }
                        else {
                            $scope.ListaRegiones = [];
                        }
                    }
                }, function (response) {
                });
            //--Tipo Oficina
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_OFICINA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaTipoOficinas = response.data.Datos;

                            if ($scope.CodigoTipo > 0 && $scope.CodigoTipo !== undefined && $scope.CodigoTipo !== null && $scope.CodigoTipo !== '') {
                                $scope.Modelo.TipoOficina = $linq.Enumerable().From($scope.ListaTipoOficinas).First('$.Codigo ==' + $scope.CodigoTipo);
                            } else {
                                $scope.Modelo.TipoOficina = $scope.ListaTipoOficinas[-1]
                            }
                        }
                        else {
                            $scope.ListaTipoOficinas = []
                        }
                    }
                }, function (response) {
                });
            //---Obtener Parametros
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.Codigo = $routeParams.Codigo;
                $scope.Titulo = 'CONSULTAR OFICINA';
                $scope.Deshabilitar = true;
                ObtenerOficina();
            }
            else {
                $scope.Modelo.Codigo = 0;
            }
        };
        //-----------Init
        //-----------------------------Informacion Requerida para funcionar---------------------------------//
        //------------------------------------------------Funciones Generales------------------------------------------------//
        $scope.ValidarTipoOficina = function () {

            $scope.Comisiones = {
                Porcentaje: '',
                Tercero: '',
                TerceroAgencista: '',
                OficinaPrincipal: $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1'),
                PorcentajeAgencista: ''
            }
            if ($scope.Modelo.TipoOficina.Codigo != null && $scope.Modelo.TipoOficina.Codigo != '' && $scope.Modelo.TipoOficina.Codigo != undefined) {
                switch ($scope.Modelo.TipoOficina.Codigo) {
                    case 9901:
                        $scope.SeccionComisiones = true;
                        break;
                    case 9902:
                        $scope.SeccionComisiones = false;
                        break;
                    case 9903:
                        $scope.SeccionComisiones = true;
                        break;
                    case 9904:
                        $scope.SeccionComisiones = true;
                        break;
                }

            }
        }
        $scope.ValidarZona = function (Ciudad) {
            $scope.codigociudad = Ciudad
            ZonasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Ciudad: { Codigo: $scope.codigociudad.Codigo } }).
                then(function (response) {
                    $scope.ListadoZonas = []
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoZonas = response.data.Datos;
                            if ($scope.CodigoZonas !== undefined && $scope.CodigoZonas !== '' && $scope.CodigoZonas !== null && $scope.CodigoZonas !== 0) {
                                $scope.Modelo.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==' + $scope.CodigoZonas);
                                $scope.HabilitarZonas = false;
                            } else {
                                $scope.Modelo.Zona = $scope.ListadoZonas[-1];
                                $scope.HabilitarZonas = false;
                            }
                        } else {
                            $scope.ListadoZonas.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                            $scope.Modelo.Zona = $scope.ListadoZonas[0];
                            $scope.HabilitarZonas = false;
                        }
                    }
                }, function (response) {
                });
        }
        //--Obtener
        function ObtenerOficina() {
            blockUI.start('Cargando Oficina Código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Oficina Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            OficinasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        if (response.data.Datos.OficinaPrincipal != undefined && response.data.Datos.OficinaPrincipal != null && response.data.Datos.OficinaPrincipal != '') {
                            if (response.data.Datos.OficinaPrincipal.Codigo != -1 && response.data.Datos.OficinaPrincipal.Nombre != null) {
                                $scope.ListadoOficinas.push(response.data.Datos.OficinaPrincipal);
                            }
                        }

                        if (response.data.Datos.Porcentaje != undefined && response.data.Datos.Porcentaje != null && response.data.Datos.Porcentaje != '') {
                            $scope.Comisiones = {};

                            $scope.Comisiones.Porcentaje = response.data.Datos.Porcentaje;

                            if (response.data.Datos.PorcentajeAgencista != undefined && response.data.Datos.PorcentajeAgencista != null && response.data.Datos.PorcentajeAgencista != '') {
                                $scope.Comisiones.PorcentajeAgencista = response.data.Datos.PorcentajeAgencista;
                            }

                            if (response.data.Datos.Tercero != undefined && response.data.Datos.Tercero != null && response.data.Datos.Tercero != '') {
                                if (response.data.Datos.Tercero.Codigo != undefined && response.data.Datos.Tercero.Codigo != null && response.data.Datos.Tercero.Codigo != '') {
                                    $scope.Comisiones.Tercero = $scope.CargarTercero(response.data.Datos.Tercero.Codigo);
                                }
                            }

                            if (response.data.Datos.TerceroAgencista != undefined && response.data.Datos.TerceroAgencista != null && response.data.Datos.TerceroAgencista != '') {
                                if (response.data.Datos.TerceroAgencista.Codigo != undefined && response.data.Datos.TerceroAgencista.Codigo != null && response.data.Datos.TerceroAgencista.Codigo != '') {
                                    $scope.Comisiones.TerceroAgencista = $scope.CargarTercero(response.data.Datos.TerceroAgencista.Codigo);
                                }
                            }

                            if (response.data.Datos.OficinaPrincipal != undefined && response.data.Datos.OficinaPrincipal != null && response.data.Datos.OficinaPrincipal != '') {
                                if (response.data.Datos.OficinaPrincipal.Codigo != undefined && response.data.Datos.OficinaPrincipal.Codigo != null && response.data.Datos.OficinaPrincipal.Codigo != '') {

                                    $scope.Comisiones.OficinaPrincipal = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + response.data.Datos.OficinaPrincipal.Codigo);
                                }
                            }
                        } else {
                            $scope.SeccionComisiones = false;
                        }
                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.Ciudad.Codigo);
                        $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                        $scope.Modelo.Nombre = response.data.Datos.Nombre;

                        $scope.Modelo.Direccion = response.data.Datos.Direccion;
                        $scope.Modelo.Telefono = response.data.Datos.Telefono;
                        $scope.ValidarZona(response.data.Datos.Ciudad);
                        $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo==' + response.data.Datos.CuentaPUCICA)
                        $scope.CodigoZonas = response.data.Datos.CodigoZona;
                        if ($scope.ListadoZonas.length > 0 && $scope.CodigoZonas > 0) {
                            $scope.Modelo.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==' + response.data.Datos.CodigoZona);
                        }
                        $scope.CodigoTipo = response.data.Datos.TipoOficina.Codigo;
                        if ($scope.ListaTipoOficinas.length > 0 && $scope.CodigoTipo > 0) {
                            $scope.Modelo.TipoOficina = $linq.Enumerable().From($scope.ListaTipoOficinas).First('$.Codigo ==' + $scope.CodigoTipo);
                        }
                        if ($scope.Modelo.TipoOficina.Codigo != null && $scope.Modelo.TipoOficina.Codigo != '' && $scope.Modelo.TipoOficina.Codigo != undefined) {
                            switch ($scope.Modelo.TipoOficina.Codigo) {
                                case 9901:
                                    $scope.SeccionComisiones = true;
                                    break;
                                case 9902:
                                    $scope.SeccionComisiones = false;
                                    break;
                                case 9903:
                                    $scope.SeccionComisiones = true;
                                    break;
                                case 9904:
                                    $scope.SeccionComisiones = true;
                                    break;
                            }

                        }
                        $scope.CodigoRegion = response.data.Datos.Region.Codigo;
                        if ($scope.ListaRegiones.length > 0) {
                            $scope.Modelo.Region = $linq.Enumerable().From($scope.ListaRegiones).First('$.Codigo ==' + $scope.CodigoRegion);
                        }
                        $scope.Modelo.Email = response.data.Datos.Email;
                        $scope.Modelo.ResolucionFacturacion = response.data.Datos.ResolucionFacturacion;
                        $scope.Modelo.AplicaEnturnamiento = response.data.Datos.AplicaEnturnamiento == 1 ? true : false;
                        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        if ($scope.Modelo.OficinaFactura.Codigo == 0) {
                            $scope.Modelo.OficinaFactura = $linq.Enumerable().From($scope.ListadoOficinasFactura).First('$.Codigo == -1');
                        }
                        else {
                            $scope.Modelo.OficinaFactura = $linq.Enumerable().From($scope.ListadoOficinasFactura).First('$.Codigo == ' + $scope.Modelo.OficinaFactura.Codigo);
                        }
                    }
                    else {
                        ShowError('No se logro consultar la oficina No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarOficinas';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarOficinas';
                });
            blockUI.stop();
        };
        //--Obtener
        //--Guardar
        $scope.ConfirmacionGuardarOficina = function () {
            if (DatosRequeridosOficina()) {
                showModal('modalConfirmacionGuardarOficina');
            }
        };
        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarOficina');
            $scope.Modelo.Estado = $scope.ModeloEstado.Codigo;
            $scope.Modelo.CodigoZona = $scope.Modelo.Zona.Codigo;
            $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
            $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
            $scope.Modelo.AplicaEnturnamiento = $scope.Modelo.AplicaEnturnamiento == true ? 1 : 0;
            $scope.Modelo.Porcentaje = $scope.Comisiones.Porcentaje;
            $scope.Modelo.CuentaPUCICA =  $scope.Modelo.CuentaPUC.Codigo 
            if ($scope.Comisiones.Tercero == undefined || $scope.Comisiones.Tercero == null) {

            } else {
                $scope.Modelo.Tercero = { Codigo: $scope.Comisiones.Tercero.Codigo };
            }

            if ($scope.Comisiones.TerceroAgencista == undefined || $scope.Comisiones.TerceroAgencista == null) {

            } else {
                $scope.Modelo.TerceroAgencista = { Codigo: $scope.Comisiones.TerceroAgencista.Codigo };
            }
            if ($scope.Comisiones.OficinaPrincipal == undefined || $scope.Comisiones.OficinaPrincipal == null) {
                //$scope.Modelo.OficinaPrincipal = { Codigo: $scope.Comisiones.OficinaPrincipal.Codigo };
            } else {
                $scope.Modelo.OficinaPrincipal = { Codigo: $scope.Comisiones.OficinaPrincipal.Codigo };
            }
            // $scope.Modelo.TerceroAgencista = { Codigo: $scope.Comisiones.TerceroAgencista.Codigo };
            //$scope.Modelo.OficinaPrincipal = { Codigo: $scope.Comisiones.OficinaPrincipal.Codigo };
            $scope.Modelo.PorcentajeAgencista = $scope.Comisiones.PorcentajeAgencista;
            $scope.Modelo.Porcentaje = parseFloat($scope.Modelo.Porcentaje);
            OficinasFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó la oficina: ' + $scope.Modelo.Nombre);
                            }
                            else {
                                ShowSuccess('Se modificó la oficina: ' + $scope.Modelo.Nombre);
                            }
                            location.href = '#!ConsultarOficinas/' + response.data.Datos;
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };
        function DatosRequeridosOficina() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Email == undefined) {
                $scope.Modelo.Email = "";
            }

            if ($scope.Modelo.ResolucionFacturacion == undefined) {
                $scope.Modelo.ResolucionFacturacion = "";
            }

            if ($scope.Modelo.Nombre === undefined || $scope.Modelo.Nombre === '' && $scope.Modelo.Nombre === null) {
                $scope.MensajesError.push('Debe ingresar el nombre de la oficina');
                continuar = false;
            }

            if ($scope.Modelo.Ciudad == undefined || $scope.Modelo.Ciudad == "") {
                $scope.MensajesError.push('Debe ingresar el nombre de la ciudad');
                continuar = false;
            }

            if ($scope.Modelo.Direccion == undefined || $scope.Modelo.Direccion == "") {
                $scope.MensajesError.push('Debe ingresar la dirección de la oficina');
                continuar = false;
            }

            if ($scope.Modelo.Telefono == undefined || $scope.Modelo.Telefono1 == "") {
                $scope.MensajesError.push('Debe ingresar el teléfono de la oficina');
                continuar = false;
            } else {
                if ($scope.Modelo.Telefono.length !== undefined) {
                    if ($scope.Modelo.Telefono.length < 7) {
                        $scope.MensajesError.push('El teléfono debe tener al menos 7 digitos');
                        continuar = false;
                    }
                }
            }

            if ($scope.ListadoZonas[0].Codigo != 0) {
                if ($scope.ListadoZonas.length > 0) {
                    if ($scope.Modelo.Zona == undefined || $scope.Modelo.Zona == null || $scope.Modelo.Zona == "" || $scope.Modelo.Zona.Codigo == 0) {
                        $scope.MensajesError.push('Debe ingresar la zona');
                        continuar = false;
                    }
                }
            }
            if ($scope.Modelo.TipoOficina == undefined || $scope.Modelo.TipoOficina == null || $scope.Modelo.TipoOficina == "") {
                $scope.MensajesError.push('Debe ingresar el tipo de oficina');
                continuar = false;
            } else {
                switch ($scope.Modelo.TipoOficina.Codigo) {
                    case 9901:
                        //if ($scope.Comisiones.Porcentaje == undefined || $scope.Comisiones.Porcentaje == null || $scope.Comisiones.Porcentaje == '') {
                        //    $scope.MensajesError.push('Debe ingresar un porcentaje de comisión');
                        //    continuar = false;
                        //} else {
                        //    //$scope.Comisiones.Porcentaje = parseFloat($scope.Comisiones.Porcentaje);
                        //}
                        break;
                    case 9902:
                        break;
                    case 9903:
                        if ($scope.Comisiones.Porcentaje == undefined || $scope.Comisiones.Porcentaje == null || $scope.Comisiones.Porcentaje == '') {
                            $scope.MensajesError.push('Debe ingresar un porcentaje de comisión');
                            continuar = false;
                        } else {
                            // $scope.Comisiones.Porcentaje = parseFloat($scope.Comisiones.Porcentaje);
                        }
                        if ($scope.Comisiones.Tercero == undefined || $scope.Comisiones.Tercero == null || $scope.Comisiones.Tercero == '') {
                            $scope.MensajesError.push('Debe ingresar un tercero');
                            continuar = false;
                        }
                        if ($scope.Comisiones.TerceroAgencista == undefined || $scope.Comisiones.TerceroAgencista == null || $scope.Comisiones.TerceroAgencista == '') {
                            $scope.MensajesError.push('Debe ingresar un tercero agencista');
                            continuar = false;
                        }
                        if ($scope.Comisiones.OficinaPrincipal == undefined || $scope.Comisiones.OficinaPrincipal == null || $scope.Comisiones.OficinaPrincipal == '' || $scope.Comisiones.OficinaPrincipal.Codigo == -1) {
                            $scope.MensajesError.push('Debe seleccionar una oficina principal');
                            continuar = false;
                        }
                        if ($scope.Comisiones.PorcentajeAgencista == undefined || $scope.Comisiones.PorcentajeAgencista == null || $scope.Comisiones.PorcentajeAgencista == '') {
                            $scope.MensajesError.push('Debe ingresar un porcentaje agencista');
                            continuar = false;
                        } else {
                            // $scope.Comisiones.PorcentajeAgencista = parseFloat($scope.Comisiones.PorcentajeAgencista);
                        }

                        break;
                    case 9904:
                        if ($scope.Comisiones.Porcentaje == undefined || $scope.Comisiones.Porcentaje == null || $scope.Comisiones.Porcentaje == '') {
                            $scope.MensajesError.push('Debe ingresar un porcentaje de comisión');
                            continuar = false;
                        } else {
                            //  $scope.Comisiones.Porcentaje = parseFloat($scope.Comisiones.Porcentaje);
                        }
                        if ($scope.Comisiones.Tercero == undefined || $scope.Comisiones.Tercero == null || $scope.Comisiones.Tercero == '') {
                            $scope.MensajesError.push('Debe ingresar un tercero');
                            continuar = false;
                        }
                        break;
                }
            }


            return continuar;
        }
        //--Guardar
        //--Volver Master
        $scope.VolverMaster = function () {
            if ($routeParams.Codigo > 0) {
                document.location.href = '#!ConsultarOficinas/' + $routeParams.Codigo;
            } else {
                document.location.href = '#!ConsultarOficinas';
            }
        };
        //--Volver Master
        //--Mascaras
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function () {
            $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno)
        };
        $scope.MaskTelefono = function () {
            $scope.Modelo.Telefono = MascaraTelefono($scope.Modelo.Telefono);
        };
        $scope.MaskMinus = function () {
            try { $scope.Modelo.Email = $scope.Modelo.Email.toLowerCase() } catch (e) { }
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope, 2);
        };
        $scope.MaskDecimales = function () {
            MascaraDecimalesGeneral($scope);
        };
        $scope.MaskPorcentaje = function (value) {
            var numeroarray = [];
            for (var i = 0; i < value.length; i++) {
                patron = /[0123456789.]/
                if (i == 0 && value[i] == '-') {
                    numeroarray.push(value[i])
                }
                else if (patron.test(value[i])) {
                    if (patron.test(value[i]) == ' ') {
                        option[i] = '';
                    } else {
                        numeroarray.push(value[i])
                    }
                }
            }

            var val = numeroarray.join('');
            $scope.Modelo.PorcentajeSeguroPaqueteria = val;
            var nval = [];
            if (val[1] != '.') {
                if (val.length == 3) {
                    if (val[0] != 1 && val[0] != "1" && val[2] != ".") {
                        $scope.MaxLength = 3;
                        nval.push(val[0]);
                        nval.push(val[1]);
                        $scope.Modelo.PorcentajeSeguroPaqueteria = nval.join('');
                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.PorcentajeSeguroPaqueteria = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.PorcentajeSeguroPaqueteria = nval.join('');
                        }
                    } else if (val[2] == ".") {
                        $scope.MaxLength = 5;
                    }
                    else if ((val[0] == 1 || val[0] == "1") && val[2] != ".") {
                        $scope.MaxLength = 3;

                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.PorcentajeSeguroPaqueteria = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.PorcentajeSeguroPaqueteria = nval.join('');
                        }
                    }

                }
            } else {
                $scope.MaxLength = 4;
            }
        }
        //--Mascaras
    }]);