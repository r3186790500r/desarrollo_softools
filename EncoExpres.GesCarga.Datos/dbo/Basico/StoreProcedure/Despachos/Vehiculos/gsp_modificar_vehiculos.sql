﻿PRINT 'gsp_modificar_vehiculos'
go
DROP PROCEDURE gsp_modificar_vehiculos
GO
CREATE PROCEDURE gsp_modificar_vehiculos
(
@par_EMPR_Codigo SMALLINT,
@Codigo NUMERIC ,
@par_Codigo_Alterno	varchar(20) = null,
@par_SEMI_Codigo	numeric = null,
@par_TERC_Codigo_Propietario	numeric = null,
@par_TERC_Codigo_Tenedor	numeric = null,
@par_TERC_Codigo_Conductor	numeric = null,
@par_TERC_Codigo_Afiliador	numeric = null,
@par_COVE_Codigo	numeric = null,
@par_MAVE_Codigo	numeric = null,
@par_LIVE_Codigo	numeric = null,
@par_Modelo	smallint = null,
@par_Modelo_Repotenciado	smallint = null,
@par_CATA_TIVE_Codigo	numeric = null,
@par_Cilindraje	numeric = null,
@par_CATA_TICA_Codigo	numeric = null,
@par_Numero_Ejes	smallint = null,
@par_Numero_Motor	varchar(50) = null,
@par_Numero_Serie	varchar(50) = null,
@par_Peso_Bruto	numeric = null,
@par_Kilometraje	numeric = null,
@par_Fecha_Actuliza_Kilometraje	datetime = null,
@par_ESSE_Numero	numeric = null,
@par_Fecha_Ultimo_Cargue	datetime = null,
@par_Estado	smallint = null,
@par_CATA_ESVE_Codigo	numeric = null,
@par_USUA_Codigo_Crea	smallint = null,
@par_CATA_TIDV_Codigo	numeric = null,
@par_Tarjeta_Propiedad	varchar(50) = null,
@par_Capacidad	numeric = null,
@par_Capacidad_Galones	numeric = null,
@par_Peso_Tara	numeric = null,
@par_TERC_Codigo_Proveedor_GPS	numeric = null,
@par_Usuario_GPS	varchar(50) = null,
@par_Clave_GPS	varchar(50) = null,
@par_TelefonoGPS	numeric = null,
@par_CATA_CAIV_Codigo	numeric = null,
@par_Justificacion_Bloqueo	varchar (150) = null,
@par_Foto	image = null,
@par_CATA_CAVE_Codigo	numeric = null
)
AS
BEGIN


update Vehiculos set
           Codigo_Alterno = ISNULL(@par_Codigo_Alterno,'')
           ,SEMI_Codigo = ISNULL(@par_SEMI_Codigo,0)
           ,TERC_Codigo_Propietario = ISNULL(@par_TERC_Codigo_Propietario,0)
           ,TERC_Codigo_Tenedor =ISNULL(@par_TERC_Codigo_Tenedor,0)
           ,TERC_Codigo_Conductor = ISNULL(@par_TERC_Codigo_Conductor,0)
           ,TERC_Codigo_Afiliador = ISNULL(@par_TERC_Codigo_Afiliador,0)
           ,COVE_Codigo = ISNULL(@par_COVE_Codigo,0)
           ,MAVE_Codigo = ISNULL(@par_MAVE_Codigo,0)
           ,LIVE_Codigo = ISNULL(@par_LIVE_Codigo,0)
           ,Modelo = ISNULL(@par_Modelo,0)
           ,Modelo_Repotenciado = @par_Modelo_Repotenciado
           ,CATA_TIVE_Codigo = ISNULL(@par_CATA_TIVE_Codigo,2200)
           ,Cilindraje = ISNULL(@par_Cilindraje,0)
           ,CATA_TICA_Codigo = ISNULL(@par_CATA_TICA_Codigo,2300)
           ,Numero_Ejes = ISNULL(@par_Numero_Ejes,0)
           ,Numero_Motor = ISNULL(@par_Numero_Motor,'')
           ,Numero_Serie = ISNULL(@par_Numero_Serie,0)
           ,Peso_Bruto = ISNULL(@par_Peso_Bruto,0)
           ,Kilometraje = ISNULL(@par_Kilometraje,0)
           ,Fecha_Actuliza_Kilometraje = ISNULL(@par_Fecha_Actuliza_Kilometraje,'01/01/1900')
           ,ESSE_Numero = ISNULL(@par_ESSE_Numero,0)
           ,Fecha_Ultimo_Cargue =   ISNULL(@par_Fecha_Ultimo_Cargue,'01/01/1900')
           ,Estado = ISNULL(@par_Estado,0)
          
           ,USUA_Codigo_Modifica = ISNULL(@par_USUA_Codigo_Crea,0)
           ,Fecha_Modifica = GETDATE()
           ,CATA_TIDV_Codigo = ISNULL(@par_CATA_TIDV_Codigo,2100)
           ,Tarjeta_Propiedad = Isnull(@par_Tarjeta_Propiedad,'')
           ,Capacidad_Kilos = Isnull(@par_Capacidad,0)
		   ,Capacidad_Galones = @par_Capacidad_Galones
           ,Peso_Tara = @par_Peso_Tara
           ,TERC_Codigo_Proveedor_GPS = ISNULL(@par_TERC_Codigo_Proveedor_GPS,0)
           ,Usuario_GPS = @par_Usuario_GPS
           ,Clave_GPS = @par_Clave_GPS
           ,CATA_CAIV_Codigo = isnull(@par_CATA_CAIV_Codigo,2400)
           ,Justificacion_Bloqueo = @par_Justificacion_Bloqueo
           ,Telefono_GPS = @par_TelefonoGPS
           ,CATA_CAVE_Codigo = @par_CATA_CAVE_Codigo
		   where EMPR_Codigo = @par_EMPR_Codigo
		   AND Codigo = @Codigo
select @Codigo as Codigo
END
GO