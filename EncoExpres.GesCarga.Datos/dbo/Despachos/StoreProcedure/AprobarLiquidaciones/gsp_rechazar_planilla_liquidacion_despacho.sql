﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 01/10/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_rechazar_planilla_liquidacion_despacho'
GO
DROP PROCEDURE gsp_rechazar_planilla_liquidacion_despacho
GO   
CREATE PROCEDURE gsp_rechazar_planilla_liquidacion_despacho 
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_Numero NUMERIC,  
 @par_Causa_Rechazo VARCHAR(150)  
)  
AS  
BEGIN  
 UPDATE  
  Encabezado_Liquidacion_Planilla_Despachos  
 SET  
  Observaciones = Observaciones +' CAUSA RECHAZO: '+ @par_Causa_Rechazo,
   Estado = 0  
 WHERE  
  EMPR_Codigo = @par_EMPR_Codigo  
  AND Numero = @par_Numero   
  
 SELECT @par_Numero AS Numero  
END  
GO