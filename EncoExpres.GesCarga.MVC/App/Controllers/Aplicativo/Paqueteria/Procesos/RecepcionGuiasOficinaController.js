﻿EncoExpresApp.controller("RecepcionRemesaOficinaCtrl", ['$scope', '$timeout', '$linq', 'blockUI', '$routeParams', 'RemesaGuiasFactory', 'OficinasFactory', 'CiudadesFactory', 'TercerosFactory', 'PlanillaRecoleccionesFactory', 'PlanillaDespachosFactory', 'blockUIConfig','RecepcionGuiasFactory',
    function ($scope, $timeout, $linq, blockUI, $routeParams, RemesaGuiasFactory, OficinasFactory, CiudadesFactory, TercerosFactory, PlanillaRecoleccionesFactory, PlanillaDespachosFactory, blockUIConfig, RecepcionGuiasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Procesos' }, { Nombre: 'Recepción Remesas Oficina' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.registrorepetido = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoGuias = [];
        $scope.ListaResultadoFiltroConsulta = [];
        $scope.ListadoRecorridosTotal = [];
        $scope.ListadoAuxiliar = [];
        $scope.CodigoCiudad = 0;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_RECEPCION_GUIAS_OFICINA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.seleccionarCheck = false;
        $scope.ListadoOficinasOrigen = [
            { Codigo: 0, Nombre: '(Todas)' }
        ];
        $scope.ListadoOficinasActual = [
            { Codigo: 0, Nombre: '(Todas)' }
        ];

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ModeloCiudadCargue = { Nombre: '', Codigo: 0 }
        //$scope.ModeloCliente = { NombreCompleto: '', Codigo: 0 }
        $scope.ModeloPlanillaRecogida = 0
        $scope.ModeloPlanillaDespacho = 0
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasActual.push(item);
                        $scope.ListadoOficinasOrigen.push(item);
                    })
                    $scope.ModeloOficinaOrigen = $scope.ListadoOficinasOrigen[0];
                    //$scope.ModeloOficinaActual = $scope.ListadoOficinasActual[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ListaCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListaCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCiudades)
                }
            }
            return $scope.ListaCiudades
        }
        //$scope.ModeloCiudadDescargue = { Nombre: '', Codigo: 0 }

        $scope.ListaCliente = [];

        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCliente)
                }
            }
            return $scope.ListaCliente
        }

        $scope.MarcarRemesas = function (chk) {
            if (chk) {
                for (var i = 0; i < $scope.ListadoRecorridosTotal.length; i++) {
                    $scope.ListadoRecorridosTotal[i].Seleccionado = true
                }
            } else {
                for (var i = 0; i < $scope.ListadoRecorridosTotal.length; i++) {
                    $scope.ListadoRecorridosTotal[i].Seleccionado = false
                }
            }
        }

        $scope.PrimerPagina = function () {
            /*Verificar si la página actual ya está guardada si no lo esta procede a realizar la busqueda*/
            if ($scope.paginaActual > 1) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    $scope.paginaActual = 1;
                    $scope.ListadoGuias = [];

                    var i = 0;
                    for (i = 0; i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina) {
                            $scope.ListadoGuias.push($scope.ListaResultadoFiltroConsulta[i])
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = 1;
                        $scope.ListadoGuias = [];

                        var i = 0;
                        for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                }

            }


        };

        $scope.Siguiente = function () {

            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.ListadoGuias = [];
                $scope.PaginaAuxiliar = $scope.paginaActual;
                $scope.paginaActual += 1;

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoGuias.push($scope.ListaResultadoFiltroConsulta[i]);
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                }
            }

        }

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    $scope.ListadoGuias = [];
                    $scope.paginaActual -= 1;

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoGuias.push($scope.ListaResultadoFiltroConsulta[i]);
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.ListadoGuias = [];
                        $scope.paginaActual -= 1;

                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                }

            }
        };

        $scope.UltimaPagina = function () {

            if ($scope.paginaActual < $scope.totalPaginas) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListadoGuias = [];

                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoGuias.push($scope.ListaResultadoFiltroConsulta[i])
                            }
                        }
                    }
                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListadoGuias = [];

                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                }

            }
        }

        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionRecepcionGuias');


            $scope.listadoGuiasSeleccionadas = [];
            $scope.detalleguia = ''
            if ($scope.CodigoCiudad == 0) {
                $scope.CodigoCiudad = $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo;

            }
            $scope.ListadoRecorridosTotal.forEach(function (itemRT) {
                if (itemRT.Seleccionado == true) {

                    $scope.detalleguia += itemRT.Remesa.Numero + ',';

                    var guias = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Numero: itemRT.Remesa.Numero,
                        EstadoRemesaPaqueteria: { Codigo: CODIGO_CATALOGO_RECIBIDA_OFICINA_DESTINO },
                        CodigoOficinaActual: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                        CodigoCiudad: $scope.CodigoCiudad,
                    }
                    $scope.listadoGuiasSeleccionadas.push(guias)
                }
            })



            $scope.objEnviar = {

                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Remesa: {
                    Numero: PERMISO_ACTIVO,
                    Ruta: { Codigo: 0 },
                    ProductoTransportado: { Codigo: 0 },
                    FormaPago: { Codigo: 0 },
                    Cliente: { Codigo: 0 },
                    Remitente: { Codigo: 0 },
                    Planilla: { TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO }

                },
                
                RemesaPaqueteria: $scope.listadoGuiasSeleccionadas,
                CadenaGuias: $scope.detalleguia,

            };



            RecepcionGuiasFactory.RecibirGuias($scope.objEnviar).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Numero == 0) {
                                ShowSuccess('La cantidad de remesa recibidas fueron : ' + response.data.Datos);
                            }
                            else {
                                ShowSuccess('La cantidad de remesa recibidas fueron  : ' + response.data.Datos);
                            }
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };

        $scope.modalConfirmacionRecepcionGuias = function () {
            showModal('modalConfirmacionRecepcionGuias');
        };


        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                if (DatosRequeridos()) {
                    blockUIConfig.autoBlock = true;
                    blockUIConfig.delay = 0;
                    blockUI.start("Buscando registros ...");
                    $timeout(function () { blockUI.message("Buscando registros ..."); Find(); }, 100);
                    //Find();
                }
            }
        };

        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.NumeroInicial = $routeParams.Codigo;
                $scope.Modelo.NumeroFinal = $routeParams.Codigo;
                Find();
            }
        }

        function DatosRequeridosFiltrar() {
            $scope.MensajesErrorfil = [];
            bolDatosRequeridosFiltrar = true;
            if ($scope.ModeloFiltroNumeroInicial == '' || $scope.ModeloFiltroNumeroInicial == undefined || $scope.ModeloFiltroNumeroInicial == null) {
                $scope.ModeloFiltroNumeroInicial = 0;
            }
            if ($scope.ModeloFiltroNumeroFinal == '' || $scope.ModeloFiltroNumeroFinal == undefined || $scope.ModeloFiltroNumeroFinal == null) {
                $scope.ModeloFiltroNumeroFinal = $scope.ModeloFiltroNumeroInicial;
            }
            if ($scope.ModeloFiltroNumeroInicial > $scope.ModeloFiltroNumeroFinal) {
                $scope.MensajesErrorfil.push('El número de planilla inicial no puede ser superior al número de planilla final')
                bolDatosRequeridosFiltrar = false;
            }

            if (($scope.modelofiltrofechaInicial !== null && $scope.modelofiltrofechaInicial !== '' && $scope.modelofiltrofechaInicial !== undefined) && ($scope.modelofiltrofechaFinal !== null && $scope.modelofiltrofechaFinal !== undefined && $scope.modelofiltrofechaFinal !== '')) {
                if ($scope.modelofiltrofechaInicial > $scope.modelofiltrofechaFinal) {
                    $scope.MensajesErrorfil.push('La fecha inicial debe ser menor que la fecha final');
                    bolDatosRequeridosFiltrar = false;
                }
            }
            if (($scope.modelofiltrofechaInicial !== null && $scope.modelofiltrofechaInicial !== '' && $scope.modelofiltrofechaInicial !== undefined) && ($scope.modelofiltrofechaFinal == null || $scope.modelofiltrofechaFinal == undefined || $scope.modelofiltrofechaFinal == '')) {
                $scope.modelofiltrofechaFinal = $scope.modelofiltrofechaInicial;
            }
            if (($scope.modelofiltrofechaInicial == null || $scope.modelofiltrofechaInicial == '' || $scope.modelofiltrofechaInicial == undefined) && ($scope.modelofiltrofechaFinal !== null && $scope.modelofiltrofechaFinal !== '' && $scope.modelofiltrofechaFinal !== undefined)) {
                $scope.MensajesErrorfil.push('Debe seleccionar la fecha inicial');
                bolDatosRequeridosFiltrar = false;
            }

        }

        function Find() {
            //blockUI.start('Buscando registros ...');
            //blockUI.message('Espere por favor ...');

           
            //$timeout(function () {
            //    blockUI.message('Espere por favor ...');
            //}, 100);


            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //NumeroInicial: $scope.ModeloGuia,
                //NumeroFinal: $scope.ModeloGuia,
                //Numeracion: $scope.ModeloNumeracion,

                Remesa: {
                    //Cliente: { Codigo: $scope.ModeloCliente.Codigo },
                    NumeroplanillaDespacho: $scope.ModeloPlanillaDespacho,
                    Numeroplanillarecoleccion: $scope.ModeloPlanillaRecogida
                },
                //OficinaActual: { Codigo: $scope.ModeloOficinaActual.Codigo },
                OficinaOrigen: { Codigo: $scope.ModeloOficinaOrigen.Codigo },
                actualizaestado: PERMISO_ACTIVO,
                //CodigoCiudad: $scope.ModeloCiudadDescargue.Codigo,
                CodigoCiudadOrigen: $scope.ModeloCiudadCargue.Codigo,
                //Estado: ESTADO_ACTIVO,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                NumeroPlanilla: -1,
                Anulado: -1,
                CodigoOficinaActualUsuario: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            }

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            if (DatosRequeridos()) {
                if ($scope.Buscando) {
                    if ($scope.MensajesError.length == 0) {
                       // blockUI.delay = 1000;

                        //RemesaGuiasFactory.Consultar(filtro).
                        //    then(function (response) {
                        //        if (response.data.ProcesoExitoso === true) {
                        //            if (response.data.Datos.length > 0)
                        //                $scope.ListadoGuias = [];

                        //            $scope.ListadoAuxiliar = [];
                        //            $scope.ListadoAuxiliar = response.data.Datos;
                        //            $scope.paginaActual = 1;


                        //            //Llena la lista $scope.ListadoRecorridosTotal para acumular los recorridos consultados con la factura y los nuevos a traer
                        //            if ($scope.ListadoGuias.length > 0) {
                        //                //Datos consultados
                        //                $scope.ListadoRecorridosTotal = [];



                        //                //Datos nuevos


                        //                for (var i = 0; i < $scope.ListadoAuxiliar.length; i++) {
                        //                    var existe = true;

                        //                    for (var j = 0; j < $scope.ListadoRecorridosTotal.length; j++) {
                        //                        if ($scope.ListadoAuxiliar[i].Remesa.Numero == $scope.ListadoRecorridosTotal[j].Remesa.Numero) {
                        //                            existe = false;
                        //                            $scope.registrorepetido += 1;
                        //                            break;
                        //                        }
                        //                    }

                        //                    if (existe == true) {
                        //                        $scope.ListadoRecorridosTotal.push($scope.ListadoAuxiliar[i]);
                        //                    }

                        //                }

                        //            } else {
                        //                $scope.ListadoRecorridosTotal = $scope.ListadoAuxiliar;


                        //            }

                        //            $scope.registrorepetido = 0;
                        //            /*se ordena el listado del Grid*/
                        //            if ($scope.ListadoRecorridosTotal.length > 0) {
                        //                $scope.ListadoRecorridosTotal = $linq.Enumerable().From($scope.ListadoRecorridosTotal).OrderBy(function (x) {
                        //                    return x.Numero
                        //                }).ToArray();
                        //            }

                        //            /*----------------------------*/

                        //            //Se muestran la cantidad de recorridos por pagina
                        //            //En este punto se muestra siempre desde la primera pagina
                        //            $scope.ListadoRecorridosTotal.forEach(function (item) {
                        //                item.Seleccionado = $scope.seleccionarCheck;
                        //            });

                        //            var i = 0;
                        //            for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                        //                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                        //                    $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                        //                }
                        //            }

                        //            //Se operan los totales de registros y de paginas
                        //            if ($scope.ListadoRecorridosTotal.length > 0) {
                        //                $scope.totalRegistros = $scope.ListadoRecorridosTotal.length;
                        //                $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                        //                $scope.Buscando = false;
                        //                $scope.ResultadoSinRegistros = '';

                        //            }
                        //            else {
                        //                $scope.totalRegistros = 0;
                        //                $scope.totalPaginas = 0;
                        //                $scope.paginaActual = 1;
                        //                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                        //                $scope.Buscando = false;
                        //            }


                        //        }
                        //    }, function (response) {
                        //        $scope.Buscando = false;
                        //    });
                        filtro.Sync = true;
                        var ResponseGuias = RemesaGuiasFactory.Consultar(filtro);
                        console.log('llegaron', ResponseGuias) 

                            //then(function (response) {
                        if (ResponseGuias.ProcesoExitoso === true) {
                            if (ResponseGuias.Datos.length > 0)
                                $scope.ListadoGuias = [];

                            $scope.ListadoAuxiliar = [];
                            $scope.ListadoAuxiliar = ResponseGuias.Datos;
                       
                            $scope.paginaActual = 1;


                            //Llena la lista $scope.ListadoRecorridosTotal para acumular los recorridos consultados con la factura y los nuevos a traer
                            if ($scope.ListadoGuias.length > 0) {
                                //Datos consultados
                                $scope.ListadoRecorridosTotal = []; 

                                //Datos nuevos
                                 
                                for (var i = 0; i < $scope.ListadoAuxiliar.length; i++) {
                                    var existe = true;

                                    for (var j = 0; j < $scope.ListadoRecorridosTotal.length; j++) {
                                        if ($scope.ListadoAuxiliar[i].Remesa.Numero == $scope.ListadoRecorridosTotal[j].Remesa.Numero) {
                                            existe = false;
                                            $scope.registrorepetido += 1;
                                            break;
                                        }
                                    }

                                    if (existe == true) {
                                        $scope.ListadoRecorridosTotal.push($scope.ListadoAuxiliar[i]);
                                    }

                                }

                            } else {
                                $scope.ListadoRecorridosTotal = $scope.ListadoAuxiliar;


                            }

                            $scope.registrorepetido = 0;
                            /*se ordena el listado del Grid*/
                            if ($scope.ListadoRecorridosTotal.length > 0) {
                                $scope.ListadoRecorridosTotal = $linq.Enumerable().From($scope.ListadoRecorridosTotal).OrderBy(function (x) {
                                    return x.Numero
                                }).ToArray();
                            }

                            /*----------------------------*/

                            //Se muestran la cantidad de recorridos por pagina
                            //En este punto se muestra siempre desde la primera pagina
                            $scope.ListadoRecorridosTotal.forEach(function (item) {
                                item.Seleccionado = $scope.seleccionarCheck;
                            });

                            var i = 0;
                            for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                    $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                                }
                            }

                            //Se operan los totales de registros y de paginas
                            if ($scope.ListadoRecorridosTotal.length > 0) {
                                $scope.totalRegistros = $scope.ListadoRecorridosTotal.length;
                                $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';

                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }


                            //  }
                            //}, function (response) {
                            //    $scope.Buscando = false;
                        }  //});
                    }
                    else {
                        $scope.Buscando = false;
                    }
                    
                }
            }
            blockUI.stop();
        }

        function DatosRequeridos() {
            var continuar = true;
            $scope.MensajesError = [];
            if ($scope.ModeloPlanillaDespacho == undefined || $scope.ModeloPlanillaDespacho == null || $scope.ModeloPlanillaDespacho == 0 || isNaN($scope.ModeloPlanillaDespacho || $scope.ModeloPlanillaDespacho == '')) {
                if ($scope.ModeloPlanillaRecogida == undefined || $scope.ModeloPlanillaRecogida == null || $scope.ModeloPlanillaRecogida == '' || $scope.ModeloPlanillaRecogida == 0 || isNaN($scope.ModeloPlanillaRecogida)) {
                    $scope.MensajesError.push('Debe ingresar un número de Planilla Despacho o Planilla Recolección');
                    continuar = false;
                }
            }
            return continuar;
        }

        $scope.LimpiarFiltro = function () {

            $scope.ModeloFiltroNumeroInicial = '';
            $scope.ModeloFiltroNumeroFinal = '';
            $scope.modelofiltrofechaInicial = '';
            $scope.modelofiltrofechaFinal = '';
            $scope.ListadoGuias = [];
            $scope.ModeloPlanillaRecogida = $linq.Enumerable().From($scope.ListaPlanillaRecogida).First('$.Numero == 0 ');
            $scope.ModeloPlanillaDespacho = $linq.Enumerable().From($scope.ListaPlanillaDespacho).First('$.Numero == 0 ');
            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==0');
            //$scope.ModeloCliente = $linq.Enumerable().From($scope.ListaCliente).First('$.Codigo ==0');

            $scope.ListaResultadoFiltroConsulta = [];

            //Se muestran la cantidad de recorridos por pagina
            //En este punto se muestra siempre desde la primera pagina
            var i = 0;
            for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                    $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                }
            }

            //Se operan los totales de registros y de paginas
            if ($scope.ListadoRecorridosTotal.length > 0) {
                $scope.totalRegistros = $scope.ListadoRecorridosTotal.length;
                $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                $scope.Buscando = false;
                $scope.ResultadoSinRegistros = '';

            }
            else {
                $scope.totalRegistros = 0;
                $scope.totalPaginas = 0;
                $scope.paginaActual = 1;
                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                $scope.Buscando = false;
            }



        }

        $scope.Filtrar = function () {
            DatosRequeridosFiltrar();

            if (bolDatosRequeridosFiltrar == true) {

                if ($scope.ModalRecorridos > 0) {
                    $scope.ValorRecorridosAuxiliar = $scope.ModalRecorridos;
                } else {
                    $scope.ValorRecorridosAuxiliar = 0;
                }

                if ($scope.ListadoRecorridosTotal.length > 0) {
                    $scope.ListadoGuias = [];

                    if ($scope.ModeloFiltroNumeroInicial > 0 && $scope.ModeloFiltroNumeroFinal == 0) {
                        $scope.ModeloFiltroNumeroFinal = $scope.ModeloFiltroNumeroInicial;
                    }
                    if ($scope.ModeloFiltroNumeroInicial == 0 && $scope.ModeloFiltroNumeroFinal > 0) {
                        $scope.ModeloFiltroNumeroInicial = $scope.ModeloFiltroNumeroFinal;
                    }
                    //FOR SACAR NUMERO DOCUMENTO Y FECHA
                    $scope.ListadoRecorridosTotal.forEach(function (item) {
                        item.NumeroDocumentoAuxiliar = item.Remesa.NumeroDocumento;
                        item.FechaAuxiliarAuxiliar = item.Remesa.Fecha;

                    })

                    var FiltroConsulta = '$.CodigoEmpresa == ' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;


                    if ($scope.ModeloFiltroNumeroInicial > 0 && $scope.ModeloFiltroNumeroFinal > 0) {
                        FiltroConsulta += ' && $.NumeroDocumentoAuxiliar >= ' + $scope.ModeloFiltroNumeroInicial + ' && $.NumeroDocumentoAuxiliar <=' + $scope.ModeloFiltroNumeroFinal;
                    }

                    if (($scope.modelofiltrofechaInicial !== null && $scope.modelofiltrofechaInicial !== '' && $scope.modelofiltrofechaInicial !== undefined) && ($scope.modelofiltrofechaFinal !== null || $scope.modelofiltrofechaFinal !== undefined || $scope.modelofiltrofechaFinal !== '')) {
                        var FechaInicialAuxiliar = Formatear_Fecha($scope.modelofiltrofechaInicial.toDateString(), FORMATO_FECHA_s);
                        var FechaFinalAuxiliar = Formatear_Fecha($scope.modelofiltrofechaFinal.toDateString(), FORMATO_FECHA_s);

                        FiltroConsulta += ' && $.FechaAuxiliarAuxiliar >="' + FechaInicialAuxiliar + '" && $.FechaAuxiliarAuxiliar <= "' + FechaFinalAuxiliar + '"';

                    } 
                    //Ejecutar consulta
                    $scope.ListaResultadoFiltroConsulta = [];
                    $scope.ListaResultadoFiltroConsulta = $linq.Enumerable().From($scope.ListadoRecorridosTotal).Where(FiltroConsulta).ToArray();

                    //La consulta se hace si el resultado del filtro trajo datos

                    //Se muestran la cantidad de recorridos por pagina
                    //En este punto se muestra siempre desde la primera pagina
                    if ($scope.ListaResultadoFiltroConsulta.length > 0) {
                        $scope.ListadoGuias = [];
                        $scope.paginaActual = 1;
                        var i = 0;
                        for (i = 0; i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                            if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                $scope.ListadoGuias.push($scope.ListaResultadoFiltroConsulta[i])
                            }
                        }

                        //Se operan los totales de registros y de paginas
                        if ($scope.ListaResultadoFiltroConsulta.length > 0) {
                            $scope.totalRegistros = $scope.ListaResultadoFiltroConsulta.length;
                            $scope.totalPaginas = Math.ceil($scope.ListaResultadoFiltroConsulta.length / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';

                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }

                    }
                    //Si no hay datos consulta todos los datos de la lista total
                    else {

                        if (($scope.modelofiltrofechaInicial == null || $scope.modelofiltrofechaInicial == '' || $scope.modelofiltrofechaInicial == undefined) || ($scope.modelofiltrofechaFinal == null || $scope.modelofiltrofechaFinal == undefined || $scope.modelofiltrofechaFinal == '')) {
                            if ($scope.ModeloFiltroNumeroInicial == 0 && $scope.ModeloFiltroNumeroFinal == 0) {
                                if ($scope.ListadoRecorridosTotal.length > 0) {

                                    //Se muestran la cantidad de recorridos por pagina
                                    //En este punto se muestra siempre desde la primera pagina
                                    var i = 0;
                                    for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                                        if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                            $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                                        }
                                    }

                                    //Se operan los totales de registros y de paginas
                                    if ($scope.ListadoRecorridosTotal.length > 0) {
                                        $scope.totalRegistros = $scope.ListadoRecorridosTotal.length;
                                        $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';

                                    }
                                    else {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }


                                }
                            }
                        }

                    }

                }

            }

        }

        $scope.MarcarRecorridos = function (RecorridosSeleccionados) {

            $scope.RecorridosSeleccionados = RecorridosSeleccionados;
            if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                if ($scope.RecorridosSeleccionados == true) {
                    $scope.ListaResultadoFiltroConsulta.forEach(function (itemTotal) {
                        itemTotal.Seleccionado = true;
                    });
                    $scope.ListadoGuias.forEach(function (item) {
                        item.Seleccionado = true;
                    });
                } else {
                    $scope.ListaResultadoFiltroConsulta.forEach(function (itemTotal) {
                        itemTotal.Seleccionado = false;
                    });
                    $scope.ListadoGuias.forEach(function (item) {
                        item.Seleccionado = false;
                    });
                }

            }
            else {
                if ($scope.RecorridosSeleccionados == true) {
                    $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                        itemTotal.Seleccionado = true;
                    });
                    $scope.ListadoGuias.forEach(function (item) {
                        item.Seleccionado = true;
                    });
                } else {
                    $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                        itemTotal.Seleccionado = false;
                    });
                    $scope.ListadoGuias.forEach(function (item) {
                        item.Seleccionado = false;
                    });
                }
            }


        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
    }]);