﻿EncoExpresApp.controller("ConsultarVehiculosCtrl", ['$scope', '$timeout', 'VehiculosFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'TercerosFactory', function ($scope, $timeout, VehiculosFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, TercerosFactory) {

    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Vehiculos' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

    $scope.cantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.ModalErrorCompleto = ''
    $scope.ModalError = ''
    $scope.MostrarMensajeError = false
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_VEHICULOS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }
    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.DeshabilitarCrear = $scope.Permisos.DeshabilitarCrear
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        Conductor: {},
        Tenedor: {}
    }

    $scope.ModalPropietarios = {
        Vehiculo: '',
        Propietario: '',
        PorcentajeParticipacion: ''
    }
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };
    /*Cargar el combo de estado*/
    $scope.ListadoEstados = [
        { Codigo: -1, Nombre: "(NO APLICA)" },
        { Codigo: 0, Nombre: "INACTIVO" },
        { Codigo: 1, Nombre: "ACTIVO" },
    ]
    $scope.Modelo.Estado = $scope.ListadoEstados[0]

    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
            document.location.href = '#!GestionarVehiculos';
        }
    };

    //Modal consultar auditoria tercero
    $scope.modalAuditoriavehiculo = function (CodigoVehiculo, PlacaVehiculo) {
        $scope.vehiculoModal = CodigoVehiculo;
        $scope.PlacaVehiculo = PlacaVehiculo;
        $scope.BuscarDetallleAuditoria()
        showModal('modalAuditoriavehiculo');
    }

    // Limpiar Detalle Auditoria 
    $scope.LimpiarDetalleauditoria = function () {
        $scope.ListadoAuditoria = []
    }


    $scope.AutocompletePropietarios = function (value) {
        var ResponsePropietarios = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROPIETARIO, ValorAutocomplete: value, Sync: true }).Datos;
        //ResponsePropietarios.forEach(item => {
        //    $scope.ListadoPropietarios.push(item);
        //});

        return ResponsePropietarios;
    }
    //tipo vehiculo
    /*Cargar el combo de tipo ruta*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoTipoRuta = [];
                if (response.data.Datos.length > 0) {
                    $scope.ListadoTipoVehiculos = response.data.Datos;
                    $scope.TipoVehiculo = $scope.ListadoTipoVehiculos[0]
                } else {
                    $scope.ListadoTipoVehiculos = []
                }
            }
        }, function (response) {
        });
    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        $scope.Modelo.Codigo = 0
        if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
            Find()
        }
    };

    $scope.BuscarDetallleAuditoria = function () {
        if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
            FindAuditoria()
        }
    };


    function FindAuditoria() {
        blockUI.start('Buscando registros...');

        $timeout(function () {
            blockUI.message('Espere por favor...');
        });

        $scope.Buscando = true;
        $scope.ListadoAuditoria = [];
        $scope.Modelo.Pagina = $scope.paginaActual
        $scope.Modelo.RegistrosPagina = $scope.cantidadRegistrosPorPagina
        if ($scope.MensajesError.length === 0) {
            //blockUI.delay = 1000;
            $scope.ListadoAuditoria = [];
            VehiculosFactory.ConsultarAuditoria({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.vehiculoModal, FechaInicial: $scope.ModeloFechaInicial, FechaFinal: $scope.ModeloFechaFinal }).
                then(function (response) {
                    console.log(response);
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoAuditoria = response.data.Datos;
                            $scope.Buscando = true;
                            $scope.ResultadoSinRegistros = '';
                        } else {
                            $scope.ListadoAuditoria = [];
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;

                        }
                    }
                }, function (response) {
                });
        }
        blockUI.stop();
    }

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoVehiculos = [];
        try {
            $scope.Modelo.TipoVehiculo = {
                Codigo: $scope.TipoVehiculo.Codigo
            };
        } catch (e) {
        }

        if ($scope.Buscando) {
            if ($scope.MensajesError.length === 0) {
                $scope.Modelo.Pagina = $scope.paginaActual
                $scope.Modelo.RegistrosPagina = $scope.cantidadRegistrosPorPaginas
                blockUI.delay = 1000;
                VehiculosFactory.Consultar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoVehiculos = response.data.Datos

                                $scope.ListadoVehiculos.forEach(function (item) {
                                    $scope.codigo = item.Codigo;
                                });
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                                $scope.ListadoVehiculos.Asc = true
                                OrderBy('Placa', undefined, $scope.ListadoVehiculos);
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                            var listado = [];
                            response.data.Datos.forEach(function (item) {
                                listado.push(item);
                            });
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        blockUI.stop();
    }
    /*------------------------------------------------------------------------------------Eliminar Vehiculos-----------------------------------------------------------------------*/
    $scope.EliminarVehiculos = function (codigo, Nombre) {
        $scope.Codigo = codigo
        $scope.Nombre = Nombre
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        showModal('modalEliminarVehiculos');
    };

    $scope.Eliminar = function () {
        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Codigo,
        };

        VehiculosFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se elimino la ruta ' + $scope.Nombre);
                    closeModal('modalEliminarVehiculos');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                var result = ''
                result = InternalServerError(response.statusText)
                showModal('modalMensajeEliminarVehiculos');
                $scope.ModalError = 'No se puede eliminar la ruta ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                $scope.ModalErrorCompleto = response.statusText

            });

    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


    $scope.AbrirModalPropietarios = function (item) {
        showModal('modalPropietariosVehiculo');
        $scope.ModalPropietarios.Vehiculo = item
        $scope.ModalPropietarios.ListadoPropietariosVehiculo = []
        var ResponsePropietarios = VehiculosFactory.ConsultarPropietarios({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.ModalPropietarios.Vehiculo.Codigo, Sync: true });
        ResponsePropietarios.Datos.forEach(item => {
            $scope.ModalPropietarios.ListadoPropietariosVehiculo.push({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Vehiculo: { Codigo: item.Vehiculo.Codigo },
                Propietario: $scope.CargarTercero(item.Propietario.Codigo),
                PorcentajeParticipacion: parseFloat(item.PorcentajeParticipacion)
            });
        });
        //$scope.ModalPropietarios.ListadoPropietariosVehiculo = ResponsePropietarios.Datos

    }

    $scope.AbrirModalConsultaPropietarios = function (item) {
        showModal('modalConsultaPropietariosVehiculo');
        $scope.ModalPropietarios.Vehiculo = item
        $scope.ModalPropietarios.ListadoPropietariosVehiculo = []
        var ResponsePropietarios = VehiculosFactory.ConsultarPropietarios({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.ModalPropietarios.Vehiculo.Codigo, Sync: true });
        ResponsePropietarios.Datos.forEach(item => {
            $scope.ModalPropietarios.ListadoPropietariosVehiculo.push({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Vehiculo: { Codigo: item.Vehiculo.Codigo },
                Propietario: $scope.CargarTercero(item.Propietario.Codigo),
                PorcentajeParticipacion: parseFloat(item.PorcentajeParticipacion)
            });
        });
        //$scope.ModalPropietarios.ListadoPropietariosVehiculo = ResponsePropietarios.Datos

    }

    $scope.AgregarPropietario = function () {

        if ($scope.ModalPropietarios.ListadoPropietariosVehiculo != null && $scope.ModalPropietarios.ListadoPropietariosVehiculo != undefined) {
            var TotalPorcentajes = 0;
            $scope.ModalPropietarios.ListadoPropietariosVehiculo.forEach(item => {
                TotalPorcentajes += parseFloat(item.PorcentajeParticipacion)
            });
            if ($scope.ModalPropietarios.Propietario != undefined && $scope.ModalPropietarios.Propietario != null) {
                $scope.ModalPropietarios.ListadoPropietariosVehiculo.forEach(item => {

                    if ($scope.ModalPropietarios.Propietario.Codigo == item.Propietario.Codigo) {
                        ShowError('Este propietario ya existe para este Vehículo');

                    }

                });

                    $scope.ModalPropietarios.ListadoPropietariosVehiculo.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Vehiculo: { Codigo: $scope.ModalPropietarios.Vehiculo.Codigo },
                    Propietario: $scope.CargarTercero($scope.ModalPropietarios.Propietario.Codigo),
                    PorcentajeParticipacion: parseFloat($scope.ModalPropietarios.PorcentajeParticipacion)
                    });

                    $scope.ModalPropietarios.Propietario = '';
                    $scope.ModalPropietarios.PorcentajeParticipacion = '';
                
            } else {
                ShowError('Debe ingresar un Propietario');
            }
        } else {
            $scope.ModalPropietarios.ListadoPropietariosVehiculo = [];
            if ($scope.ModalPropietarios.Propietario != undefined && $scope.ModalPropietarios.Propietario != null) {
                    $scope.ModalPropietarios.ListadoPropietariosVehiculo.push({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Vehiculo: { Codigo: $scope.ModalPropietarios.Vehiculo.Codigo },
                        Propietario: $scope.CargarTercero($scope.ModalPropietarios.Propietario.Codigo),
                        PorcentajeParticipacion: parseFloat($scope.ModalPropietarios.PorcentajeParticipacion)
                    });
                    $scope.ModalPropietarios.Propietario = '';
                    $scope.ModalPropietarios.PorcentajeParticipacion = '';
                
            } else {
                ShowError('Debe ingresar un Propietario');
            }
        }
    }

    $scope.EliminarPropietario = function (index, item) {
        $scope.ModalPropietarios.ListadoPropietariosVehiculo.splice(index, 1)
    }

    $scope.GuardarPropietarios = function () {
        var TotalPorcentajes = 0;
        var ListadoPropietarios = [];
        var propietario = false

        console.log('propietario', $scope.ModalPropietarios.Vehiculo.Tenedor)

        $scope.ModalPropietarios.ListadoPropietariosVehiculo.forEach(item => {
            TotalPorcentajes += parseFloat(item.PorcentajeParticipacion)
            ListadoPropietarios.push({
                CodigoEmpresa: item.CodigoEmpresa,
                Vehiculo: { Codigo: item.Vehiculo.Codigo },
                Propietario: { Codigo: item.Propietario.Codigo },
                PorcentajeParticipacion: parseFloat(item.PorcentajeParticipacion)
            });
        });

        for (i = 0; i < ListadoPropietarios.length; i++) {
            console.log('listPropietarios', ListadoPropietarios)
            console.log('listados', $scope.ModalPropietarios.Vehiculo.Tenedor.Codigo)

            if (ListadoPropietarios[i].Propietario.Codigo ==
                $scope.ModalPropietarios.Vehiculo.Tenedor.Codigo) {
                propietario = true
                console.log('recodrido', i)
            } 
        }

        if (propietario) {
            continuar = false
        } else {
            continuar = true
            ShowError('Debe ingresar el Propietario titular ' + $scope.ModalPropietarios.Vehiculo.Tenedor.Nombre);
        }


        if (TotalPorcentajes != 100) {
            continuar = true
            ShowError('La suma de los porcentajes de participación debe ser igual al 100%');
        } else {

            if (continuar) {

                console.log('validacion')

            } else {

                var Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModalPropietarios.Vehiculo.Codigo,
                    Propietarios: ListadoPropietarios
                }


                var Novedad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModalPropietarios.Vehiculo.Codigo,
                    Estado: { Codigo: 1 },
                    Novedad: { Codigo: 22321 },
                    JustificacionNovedad: 'Cambio Conductor y/o propietario',
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }

                VehiculosFactory.GuardarNovedad(Novedad).
                    then(function (response) {
                        console.log('guardadoNovedad', response)
                    });

                VehiculosFactory.GuardarPropietarios(Entidad).
                    then(function (response) {

                        if (response.data.ProcesoExitoso == true) {
                            ShowSuccess('Se guardaron los propietarios de manera exitosa');
                            closeModal('modalPropietariosVehiculo');
                            $scope.ModalPropietarios.Propietario = '';
                            $scope.ModalPropietarios.PorcentajeParticipacion = '';
                        } else {
                            ShowError(response.data.MensajeOperacion)
                        }
                    });

            }

        }

    }

    /* Obtener parametros*/
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Find();
    }

    $scope.CerrarModal = function () {
        closeModal('modalEliminarVehiculos');
        closeModal('modalMensajeEliminarVehiculos');
    }

    $scope.MaskPlaca = function () {
        try { $scope.Modelo.Placa = MascaraPlaca($scope.Modelo.Placa) } finally { }
    };
    $scope.MaskNumero = function (option) {
        MascaraNumeroGeneral($scope)
    };
    $scope.MaskMayus = function (option) {
        MascaraMayusGeneral($scope)
    };

    $scope.MaskPorcentaje = function (value, item) {
        var numeroarray = [];
        for (var i = 0; i < value.length; i++) {
            patron = /[0123456789.]/
            if (i == 0 && value[i] == '-') {
                numeroarray.push(value[i])
            }
            else if (patron.test(value[i])) {
                if (patron.test(value[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(value[i])
                }
            }
        }

        var val = numeroarray.join('');
        item = val;
        var nval = [];
        if (val[1] != '.') {
            if (val.length == 3) {
                if (val[0] != 1 && val[0] != "1" && val[2] != ".") {
                    var MaxLength = 3;
                    nval.push(val[0]);
                    nval.push(val[1]);
                    item = nval.join('');
                    if (val[1] != 0 || val[1] != "0") {
                        nval = [];
                        nval.push(val[0]);
                        nval.push(val[1]);
                        item = nval.join('');
                    } else if (val[2] != 0 || val[2] != "0") {
                        nval = [];
                        nval.push(val[0]);
                        nval.push(val[1]);
                        item = nval.join('');
                    }
                } else if (val[2] == ".") {
                    MaxLength = 5;
                }
                else if ((val[0] == 1 || val[0] == "1") && val[2] != ".") {
                    MaxLength = 3;

                    if (val[1] != 0 || val[1] != "0") {
                        nval = [];
                        nval.push(val[0]);
                        nval.push(val[1]);
                        item = nval.join('');
                    } else if (val[2] != 0 || val[2] != "0") {
                        nval = [];
                        nval.push(val[0]);
                        nval.push(val[1]);
                        item = nval.join('');
                    }
                }

            } else if (val.length > 3) {
                if (val[2] == ".") {
                    if (val.length == 4) {
                        nval.push(val[0]);
                        nval.push(val[1]);
                        nval.push(val[2]);
                        if (val[3] != ".") {
                            nval.push(val[3]);
                        }
                        item = nval.join('');
                    } else if (val.length >= 5) {
                        nval.push(val[0]);
                        nval.push(val[1]);
                        nval.push(val[2]);
                        if (val[3] != ".") {
                            nval.push(val[3]);
                            if (val[4] != ".") {
                                nval.push(val[4]);
                            }
                        }

                        item = nval.join('');
                    }
                } else {
                    nval.push(val[0]);
                    nval.push(val[1]);
                    nval.push(val[2]);
                    item = nval.join('');
                }

            }
        } else {
            nval.push(val[0]);
            nval.push(val[1]);
            if (val[2] != ".") {
                nval.push(val[2]);
                if (val[3] != ".") {
                    nval.push(val[3]);
                }
            }


            item = nval.join('');
            MaxLength = 4;
        }
        return item;
    }

}]);