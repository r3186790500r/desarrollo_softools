﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Tesoreria
    <JsonObject>
    Public NotInheritable Class CuentasPorCobrar
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Numero = Read(lector, "Numero")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Fecha = Read(lector, "Fecha")
            Tercero = New Terceros With {.Codigo = Read(lector, "TERC_Codigo"), .NombreCompleto = Read(lector, "NombreTercero")}
            DocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOOR_Codigo"), .Nombre = Read(lector, "CATA_DOOR_Nombre")}
            CodigoDocumentoOrigen = Read(lector, "Codigo_Documento_Origen")
            NumeroDocumentoOrigen = Read(lector, "Documento_Origen")
            FechaVenceDocumento = Read(lector, "Fecha_Vence_Documento_Origen")
            ValorTotal = Read(lector, "Valor_Total")
            Saldo = Read(lector, "Saldo")
            Anulado = Read(lector, "Anulado")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
            Observaciones = Read(lector, "Observaciones")
            EstadoCuenta = Read(lector, "Estado_Cuenta")
            DiasPlazo = Read(lector, "Dias_Plazo")
            CodigoConcepto = Read(lector, "Codigo_Concepto")
            NombreConcepto = Read(lector, "Nombre_Concepto")
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "OFIC_Nombre")}
            CreadaManual = Read(lector, "Cuenta_Manual")
            CausaAnulacion = Read(lector, "Causa_Anulacion")
        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Tercero As Terceros
        <JsonProperty>
        Public Property CodigoDocumentoOrigen As Integer
        <JsonProperty>
        Public Property NumeroDocumentoOrigen As Integer
        <JsonProperty>
        Public Property DocumentoOrigen As ValorCatalogos
        <JsonProperty>
        Public Property FechaDocumento As DateTime
        <JsonProperty>
        Public Property FechaVenceDocumento As DateTime
        <JsonProperty>
        Public Property ValorTotal As Double
        <JsonProperty>
        Public Property ValorPagar As Double
        <JsonProperty>
        Public Property Abono As Double
        <JsonProperty>
        Public Property Saldo As Double
        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleDocumentoCuentaComprobantes)
        <JsonProperty>
        Public Property DetalleObservaciones As IEnumerable(Of DetalleObservacionesCuentas)
        <JsonProperty>
        Public Property CuentaPuc As PlanUnicoCuentas
        <JsonProperty>
        Public Property FechaCancelacionPago As Date
        <JsonProperty>
        Public Property Aprobado As Integer
        <JsonProperty>
        Public Property UsuarioAprobo As Usuarios
        <JsonProperty>
        Public Property FechaAprobo As Date
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Numeracion As String
        <JsonProperty>
        Public Property AplicaPSL As Integer
        <JsonProperty>
        Public Property EstadoCuenta As Integer
        <JsonProperty>
        Public Property CreadaManual As Integer
        <JsonProperty>
        Public Property DiasPlazo As Integer
        <JsonProperty>
        Public Property CodigoConcepto As Integer
        <JsonProperty>
        Public Property NombreConcepto As String
        <JsonProperty>
        Public Property ELGCNumeroDocumento As Integer
        <JsonProperty>
        Public Property ELPDNumeroDocumento As Integer
    End Class
End Namespace