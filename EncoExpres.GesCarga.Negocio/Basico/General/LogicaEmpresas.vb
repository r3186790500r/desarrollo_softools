﻿Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Fachada.Basico.General

Namespace Basico.General
    Public NotInheritable Class LogicaEmpresas
        Inherits LogicaBase(Of Empresas)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Empresas)

        Sub New(persistencia As IPersistenciaBase(Of Empresas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Empresas) As Respuesta(Of IEnumerable(Of Empresas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Empresas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Empresas))(consulta)
        End Function

        Public Function ConsultarMaster(filtro As Empresas) As Respuesta(Of IEnumerable(Of Empresas))
            Dim consulta = CType(_persistencia, PersistenciaEmpresas).ConsultarMaster(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Empresas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Empresas))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Empresas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Empresas) As Respuesta(Of Empresas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Empresas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Empresas)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As Empresas) As Respuesta(Of Empresas)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Empresas) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Empresas) With {.ProcesoExitoso = True}

        End Function



        Public Function CambioClave(filtro As Empresas) As Respuesta(Of IEnumerable(Of Empresas))
            Dim consulta = CType(_persistencia, PersistenciaEmpresas).CambioClave(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Empresas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Empresas))(consulta)
        End Function
    End Class

End Namespace