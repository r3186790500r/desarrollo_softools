﻿Print 'gsp_consultar_cerrar_orden_servicio'
GO
DROP PROCEDURE gsp_consultar_cerrar_orden_servicio
GO
CREATE PROCEDURE gsp_consultar_cerrar_orden_servicio
(
	@par_EMPR_Codigo smallint,
	@par_ENOS_Numero numeric = NULL,
	@par_ENOS_TIDO_Codigo numeric = NULL,
	@par_TERC_Codigo_Cliente numeric = NULL,
	@par_Fecha_Inicial datetime = NULL,
	@par_Fecha_Final datetime = NULL,
	@par_ENOS_Estado smallint = NULL,
	@par_ENOS_Anulado smallint = NULL,
	@par_CATA_ESSO smallint = NULL,
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL  
)
AS                                 
BEGIN                        
	SET NOCOUNT ON;                              
	DECLARE @CantidadRegistros INT                        
	SELECT                        
	@CantidadRegistros = (SELECT COUNT(1)

	FROM Encabezado_Solicitud_Orden_Servicios ESOS

	LEFT JOIN Terceros as CLIE ON
	ESOS.EMPR_Codigo = CLIE.EMPR_Codigo
	AND esos.TERC_Codigo_Cliente = CLIE.Codigo

	LEFT JOIN Valor_Catalogos as CATA_ESSO ON
	ESOS.EMPR_Codigo = CATA_ESSO.EMPR_Codigo
	AND ESOS.CATA_ESSO_Codigo = CATA_ESSO.Codigo

	WHERE ESOS.EMPR_Codigo = @par_EMPR_Codigo
	AND ESOS.Numero_Documento = ISNULL(@par_ENOS_Numero, ESOS.Numero_Documento)
	AND ESOS.TIDO_Codigo = ISNULL(@par_ENOS_TIDO_Codigo, ESOS.TIDO_Codigo)
	AND ESOS.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ESOS.TERC_Codigo_Cliente)
	AND ESOS.Fecha >= ISNULL(@par_Fecha_Inicial, ESOS.Fecha)
	AND ESOS.Fecha <= ISNULL(@par_Fecha_Final, ESOS.Fecha)
	AND ESOS.Estado = ISNULL(@par_ENOS_Estado, ESOS.Estado)
	AND ESOS.CATA_ESSO_Codigo = ISNULL(@par_CATA_ESSO, ESOS.CATA_ESSO_Codigo)
	AND ESOS.Anulado = ISNULL(@par_ENOS_Anulado, ESOS.Anulado)
);                        
WITH Pagina
AS
(
	SELECT
	ESOS.EMPR_Codigo,
	ESOS.Numero AS ESOS_Numero,
	ESOS.Numero_Documento AS ESOS_NumeroDocumento,
	ESOS.Fecha AS ESOS_Fecha,
	ISNULL(CLIE.Codigo, 0) AS CLIE_Codigo,
	IIF(CLIE.Nombre is NULL OR CLIE.Nombre = '', CLIE.Razon_Social, CONCAT(CLIE.Nombre,' ', CLIE.Apellido1, CLIE.Apellido2)) AS CLIE_Nombre,
	ISNULL(ESOS.Estado, 1) AS ESOS_Estado,
	ISNULL(ESOS.Anulado, 0 ) AS ESOS_Anuado,

	ISNULL(CATA_ESSO.Codigo, 0)  AS CATA_ESSO_Codigo,
	ISNULL(CATA_ESSO.Campo1, '') AS CATA_ESSO_Campo1,
	ISNULL(CATA_CCOS.Codigo, 0) AS CATA_CCOS_Codigo,
	ISNULL(CATA_CCOS.Campo1, '') AS CATA_CCOS_Campo1,
	ISNULL(ESOS.Observaciones_Cierre, '') AS ESOS_Observaciones_Cierre,

	ROW_NUMBER() OVER (ORDER BY ESOS.Numero) AS RowNumber
	        
	FROM Encabezado_Solicitud_Orden_Servicios ESOS

	LEFT JOIN Terceros as CLIE ON
	ESOS.EMPR_Codigo = CLIE.EMPR_Codigo
	AND esos.TERC_Codigo_Cliente = CLIE.Codigo

	LEFT JOIN Valor_Catalogos as CATA_ESSO ON
	ESOS.EMPR_Codigo = CATA_ESSO.EMPR_Codigo
	AND ESOS.CATA_ESSO_Codigo = CATA_ESSO.Codigo

	LEFT JOIN Valor_Catalogos as CATA_CCOS ON
	ESOS.EMPR_Codigo = CATA_CCOS.EMPR_Codigo
	AND ESOS.CATA_CCOS_Codigo = CATA_CCOS.Codigo

	WHERE ESOS.EMPR_Codigo = @par_EMPR_Codigo
	AND ESOS.Numero_Documento = ISNULL(@par_ENOS_Numero, ESOS.Numero_Documento)
	AND ESOS.TIDO_Codigo = ISNULL(@par_ENOS_TIDO_Codigo, ESOS.TIDO_Codigo)
	AND ESOS.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ESOS.TERC_Codigo_Cliente)
	AND ESOS.Fecha >= ISNULL(@par_Fecha_Inicial, ESOS.Fecha)
	AND ESOS.Fecha <= ISNULL(@par_Fecha_Final, ESOS.Fecha)
	AND ESOS.Estado = ISNULL(@par_ENOS_Estado, ESOS.Estado)
	AND ESOS.CATA_ESSO_Codigo = ISNULL(@par_CATA_ESSO, ESOS.CATA_ESSO_Codigo)
	AND ESOS.Anulado = ISNULL(@par_ENOS_Anulado, ESOS.Anulado)
)                        
	SELECT
	EMPR_Codigo,
	ESOS_Numero,
	ESOS_NumeroDocumento,
	ESOS_Fecha,
	CLIE_Codigo,
	CLIE_Nombre,
	ESOS_Estado,
	ESOS_Anuado,

	CATA_ESSO_Codigo,
	CATA_ESSO_Campo1,
	CATA_CCOS_Codigo,
	CATA_CCOS_Campo1,
	ESOS_Observaciones_Cierre,
	@CantidadRegistros AS TotalRegistros,
	@par_NumeroPagina AS PaginaObtener,
	@par_RegistrosPagina AS RegistrosPagina

	FROM Pagina                        
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                        
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
END         
GO