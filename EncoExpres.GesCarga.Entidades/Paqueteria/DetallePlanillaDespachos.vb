﻿Imports Newtonsoft.Json

Namespace Despachos.Paqueteria
    <JsonObject>
    Public Class DetallePlanillaDespachos
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePlanillaDespachos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroPlanilla = Read(lector, "ENPD_Numero")
            NumeroRemesa = Read(lector, "ENRE_Numero")
            NumeroDocumentoRemesa = Read(lector, "Numero_Documento_Remesa")
            NumeroFacturaRemesa = Read(lector, "Numero_Factura_Remesa")
            NumeroLiquidaPlanilla = Read(lector, "Numero_Liquida_Planilla")

        End Sub

        <JsonProperty>
        Public Property NumeroPlanilla As Integer
        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property NumeroDocumentoRemesa As Integer
        <JsonProperty>
        Public Property NumeroFacturaRemesa As Integer
        <JsonProperty>
        Public Property NumeroLiquidaPlanilla As Integer

    End Class
End Namespace
