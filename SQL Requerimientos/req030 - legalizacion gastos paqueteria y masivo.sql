USE [ENCOEXPRES]
GO

/****** Object:  StoredProcedure [dbo].[gsp_modificar_encabezado_Legalizacion_gastos]    Script Date: 16/02/2022 9:37:06 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_detalle_legalizacion_gastos]
(            
	@par_EMPR_Codigo  smallint            
	,@par_ELGC_Numero  numeric(18,0)            
	,@par_ENPD_Numero  numeric(18,0)            
	,@par_ENMC_Numero  numeric(18,0) = NULL            
	,@par_CLGC_Codigo  numeric(18,0)            
	,@par_Valor  money            
	,@par_TERC_Codigo_Proveedor  numeric(18,0)            
	,@par_Identificacion_Proveedor  varchar(20)            
	,@par_Nombre_Proveedor  varchar(50)            
	,@par_Numero_Factura  varchar(50)  = NULL            
	,@par_Fecha_Factura  date  = NULL            
	,@par_Observaciones  varchar(500) = NULL            
	,@par_Cantidad_Galones  numeric(18,4) = NULL            
	,@par_Tiene_Chip  smallint            
	,@par_Valor_Galones  numeric(18,4) = NULL            
	,@par_Nombre_Estacion  varchar(80)  = NULL            
	,@par_TERC_Codigo_Conductor  numeric(18,0) = NULL  ,  
	@par_ENDC_Codigo numeric(18,0) = NULL    
)            
AS            
BEGIN            
	IF @par_TERC_Codigo_Conductor > 0      
	BEGIN      
		UPDATE Detalle_Conductores_Planilla_Despachos set ELGC_Numero = @par_ELGC_Numero where EMPR_Codigo = @par_EMPR_Codigo and ENPD_Numero = @par_ENPD_Numero  AND TERC_Codigo_Conductor = @par_TERC_Codigo_Conductor      
	END     
	
	UPDATE Encabezado_Planilla_Despachos set ELGC_Numero = @par_ELGC_Numero where EMPR_Codigo = @par_EMPR_Codigo and Numero = @par_ENPD_Numero 
	
	INSERT INTO Detalle_Legalizacion_Gastos_Conductor            
	(EMPR_Codigo            
	,ELGC_Numero            
	,ENPD_Numero            
	,ENMC_Numero            
	,CLGC_Codigo            
	,Valor            
	,TERC_Codigo_Proveedor            
	,Identificacion_Proveedor            
	,Nombre_Proveedor            
	,Numero_Factura            
	,Fecha_Factura            
	,Observaciones            
	,Cantidad_Galones            
	,Tiene_Chip          
	,Valor_Galones          
	,Nombre_Estacion      
	,TERC_Codigo_Conductor,  
	ENDC_Codigo  
	)            
	VALUES            
	(@par_EMPR_Codigo            
	,@par_ELGC_Numero            
	,@par_ENPD_Numero            
	,@par_ENMC_Numero            
	,@par_CLGC_Codigo            
	,@par_Valor            
	,@par_TERC_Codigo_Proveedor            
	,ISNULL(@par_Identificacion_Proveedor,'')            
	,ISNULL(@par_Nombre_Proveedor,'')            
	,ISNULL(@par_Numero_Factura,0)            
	,ISNULL(@par_Fecha_Factura,'')            
	,@par_Observaciones            
	,@par_Cantidad_Galones            
	,@par_Tiene_Chip          
	,@par_Valor_Galones          
	,@par_Nombre_Estacion      
	,@par_TERC_Codigo_Conductor  
	,@par_ENDC_Codigo)            
	SELECT @@IDENTITY as ID            
END
GO


