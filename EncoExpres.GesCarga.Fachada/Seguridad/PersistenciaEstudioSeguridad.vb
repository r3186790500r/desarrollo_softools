﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Repositorio.SeguridadUsuarios

Namespace SeguridadUsuarios
    Public NotInheritable Class PersistenciaEstudioSeguridad
        Implements IPersistenciaBase(Of EstudioSeguridad)
        Public Function Consultar(filtro As EstudioSeguridad) As IEnumerable(Of EstudioSeguridad) Implements IPersistenciaBase(Of EstudioSeguridad).Consultar
            Return New RepositorioEstudioSeguridad().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EstudioSeguridad) As Long Implements IPersistenciaBase(Of EstudioSeguridad).Insertar
            Return New RepositorioEstudioSeguridad().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EstudioSeguridad) As Long Implements IPersistenciaBase(Of EstudioSeguridad).Modificar
            Return New RepositorioEstudioSeguridad().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EstudioSeguridad) As EstudioSeguridad Implements IPersistenciaBase(Of EstudioSeguridad).Obtener
            Return New RepositorioEstudioSeguridad().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EstudioSeguridad) As Boolean
            Return New RepositorioEstudioSeguridad().Anular(entidad)
        End Function
        Public Function Autorizar(entidad As EstudioSeguridad) As Boolean
            Return New RepositorioEstudioSeguridad().Autorizar(entidad)
        End Function
        Public Function Rechazar(entidad As EstudioSeguridad) As Boolean
            Return New RepositorioEstudioSeguridad().Rechazar(entidad)
        End Function

    End Class
End Namespace
