﻿PRINT 'gsp_modificar_cajas'
GO
DROP PROCEDURE gsp_modificar_cajas
GO
CREATE PROCEDURE gsp_modificar_cajas
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo SMALLINT,
@par_Codigo_Alterno VARCHAR(20), 
@par_Nombre VARCHAR(50), 
@par_PLUC_Codigo SMALLINT,
@par_OFIC_Codigo SMALLINT,
@par_Estado SMALLINT,
@par_USUA_Codigo_Modifica SMALLINT
)
AS
BEGIN
	UPDATE  Cajas
	SET
	Codigo_Alterno = @par_Codigo_Alterno,
	Nombre = @par_Nombre,
	PLUC_Codigo = @par_PLUC_Codigo,
	OFIC_Codigo = @par_OFIC_Codigo,
	Estado = @par_Estado,
	USUA_Modifica = @par_USUA_Codigo_Modifica,
	Fecha_Modifica = GETDATE()
	WHERE

	EMPR_Codigo = @par_EMPR_Codigo
	AND Codigo = @par_Codigo

	SELECT @par_Codigo AS Codigo
END
GO