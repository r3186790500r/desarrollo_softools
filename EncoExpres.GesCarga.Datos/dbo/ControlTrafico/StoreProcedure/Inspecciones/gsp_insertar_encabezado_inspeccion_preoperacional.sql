﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 09/01/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

PRINT 'gsp_insertar_encabezado_inspeccion_preoperacional'
GO
DROP PROCEDURE gsp_insertar_encabezado_inspeccion_preoperacional
GO
CREATE PROCEDURE gsp_insertar_encabezado_inspeccion_preoperacional 
(        
@par_EMPR_Codigo SMALLINT,        
@par_TIDO_Codigo NUMERIC,            
@par_EFIN_Codigo NUMERIC,        
@par_Fecha_Inspeccion DATETIME = null,        
@par_Tiempo_Vigencia SMALLINT,        
@par_Codigo_Alterno VARCHAR(20) = NULL,        
@par_VEHI_Codigo NUMERIC,        
@par_SEMI_Codigo NUMERIC  = NULL,        
@par_TERC_Codigo_Cliente NUMERIC  = NULL,       
@par_TERC_Codigo_Conductor NUMERIC  = NULL,        
@par_TERC_Codigo_Transportador NUMERIC  = NULL,        
@par_Fecha_Hora_Inicio_Inspeccion DATETIME = NULL,        
@par_Fecha_Hora_Fin_Inspeccion DATETIME = NULL,        
@par_SICD_Codigo_Cargue NUMERIC = NULL,       
@par_SICD_Codigo_Descargue NUMERIC = NULL,        
@par_Observaciones VARCHAR(150) = NULL,        
@par_TERC_Codigo_Responsable NUMERIC  = NULL,        
@par_Firma IMAGE  = NULL,            
@par_Estado SMALLINT,        
@par_USUA_Codigo_Crea SMALLINT      
)        
AS        
BEGIN        
        
DECLARE @Numero NUMERIC         
EXEC gsp_generar_consecutivo  @par_EMPR_Codigo, @par_TIDO_Codigo, 0, @Numero OUTPUT  
  
INSERT INTO Encabezado_Inspecciones      
(        
 EMPR_Codigo,        
 Numero,        
 TIDO_Codigo,   
 EFIN_Codigo,       
 Fecha_Inspeccion,        
 Tiempo_Vigencia,        
 Codigo_Alterno,        
 VEHI_Codigo,        
 SEMI_Codigo,        
 TERC_Codigo_Cliente,        
 TERC_Codigo_Conductor,        
 TERC_Codigo_Transportador,        
 Fecha_Hora_Inicio_Inspeccion,        
 Fecha_Hora_Fin_Inspeccion,        
 SICD_Codigo_Cargue,        
 SICD_Codigo_Descargue,        
 Observaciones,        
 TERC_Codigo_Responsable,     
 TERC_Codigo_Autoriza,     
 Firma,            
 Estado,        
 Anulado,        
 Fecha_Crea,        
 USUA_Codigo_Crea,  
 ENMC_Numero,  
 ESOS_Numero,  
 ENOC_Numero,  
 ENPD_Numero  
)        
VALUES         
(        
 @par_EMPR_Codigo,        
 @Numero,        
 @par_TIDO_Codigo,         
 @par_EFIN_Codigo,        
 ISNULL(@par_Fecha_Inspeccion, GETDATE()),       
 @par_Tiempo_Vigencia,        
 ISNULL(@par_Codigo_Alterno,''),        
 @par_VEHI_Codigo,        
 ISNULL(@par_SEMI_Codigo, 0),       
 ISNULL(@par_TERC_Codigo_Cliente,    0),    
 @par_TERC_Codigo_Conductor,        
 ISNULL(@par_TERC_Codigo_Transportador,    0),    
 @par_Fecha_Hora_Inicio_Inspeccion,        
 @par_Fecha_Hora_Fin_Inspeccion,        
 ISNULL(@par_SICD_Codigo_Cargue, 0),        
 ISNULL(@par_SICD_Codigo_Descargue, 0),        
 ISNULL(@par_Observaciones,''),        
 ISNULL(@par_TERC_Codigo_Responsable, 0),   
 0,      
 @par_Firma,             
 @par_Estado,        
 0,        
 GETDATE(),        
 @par_USUA_Codigo_Crea,  
 0,  
 0,  
 0,  
 0  
 )        
        
SELECT @Numero AS Numero      
       
END       
GO 