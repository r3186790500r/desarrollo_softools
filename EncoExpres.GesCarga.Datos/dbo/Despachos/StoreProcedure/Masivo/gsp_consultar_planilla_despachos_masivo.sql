﻿PRINT 'gsp_consultar_planilla_despachos_masivo'
GO
DROP PROCEDURE gsp_consultar_planilla_despachos_masivo
GO
CREATE PROCEDURE gsp_consultar_planilla_despachos_masivo
(                       
	@par_EMPR_Codigo SMALLINT,                              
	@par_Numero NUMERIC= NULL,                               
	@par_Numero_Manifiesto NUMERIC= NULL,                               
	@par_Fecha_Inicial DATE= NULL,                             
	@par_Fecha_Final DATE= NULL,                              
	@par_Estado SMALLINT = NULL,                            
	@par_Anulado SMALLINT = NULL ,                            
	@par_Codigo_Conductor NUMERIC = NULL,                             
	@par_Ruta_Codigo INT = NULL,                             
	@par_Codigo_Placa INT = NULL,                    
	@par_Conductor VARCHAR(50)  = NULL,                             
	@par_Ruta VARCHAR(50) = NULL,                             
	@par_Placa VARCHAR(50)  = NULL,                         
	@par_NumeroPagina INT = NULL,                              
	@par_RegistrosPagina INT = NULL,                            
	@par_TIDO_Codigo  INT = NULL,
	@par_OFIC_Codigo INT = NULL,
	@par_Usuario_Consulta INT                    
)                              
AS                              
BEGIN                              
	SET NOCOUNT ON;                              
	DECLARE                              
	@CantidadRegistros INT                              
	SELECT @CantidadRegistros = (                              
		SELECT DISTINCT                               
		COUNT(1)                               
		FROM Encabezado_Planilla_Despachos  ENPD                       
                            
		left join Vehiculos VEHI on                    
		ENPD.EMPR_Codigo = VEHI.EMPR_Codigo                             
		AND ENPD.VEHI_Codigo = VEHI.Codigo                            
                        
		left join Terceros TECO  on                    
		ENPD.EMPR_Codigo = TECO.EMPR_Codigo                            
		AND ENPD.TERC_Codigo_Conductor = TECO.Codigo                             
                    
		left join Rutas RUTA   on                    
		ENPD.EMPR_Codigo = RUTA.EMPR_Codigo                            
		AND ENPD.RUTA_Codigo = RUTA.Codigo                      
                     
                       
		left join Encabezado_Manifiesto_Carga ENMC   on                    
		ENPD.EMPR_Codigo = ENMC.EMPR_Codigo                            
		AND ENPD.ENMC_Numero = ENMC.Numero           
                           
		WHERE                              
                          
		ENPD.EMPR_Codigo = @par_EMPR_Codigo                             
		AND ENPD.Numero_Documento = ISNULL(@par_Numero, ENPD.Numero_Documento)                            
		AND (ENMC.Numero_Documento = ISNULL(@par_Numero_Manifiesto, ENMC.Numero_Documento) OR @par_Numero_Manifiesto IS NULL)                            
                         
		AND ENPD.FECHA >= ISNULL(@par_Fecha_Inicial ,ENPD.FECHA)                            
		AND ENPD.FECHA <=ISNULL(@par_Fecha_Final,ENPD.FECHA )                            
                            
		AND ENPD.RUTA_Codigo = ISNULL( @par_Ruta_Codigo,ENPD.RUTA_Codigo)                            
		AND ENPD.TERC_Codigo_Conductor = ISNULL(@par_Codigo_Conductor,ENPD.TERC_Codigo_Conductor)                            
		AND VEHI.Codigo = ISNULL(@par_Codigo_Placa,VEHI.Codigo)                            
		AND RUTA.Nombre Like'%'+ ISNULL( @par_Ruta,RUTA.Nombre ) +'%'              
		AND ((TECO.Nombre + ' ' + TECO.Apellido1 + ' ' + TECO.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%' OR TECO.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%') OR (@par_Conductor IS NULL))                  
		AND VEHI.Placa = ISNULL(@par_Placa,VEHI.Placa)                 
		AND ENPD.Estado=ISNULL(@par_Estado,ENPD.Estado)                            
		AND ENPD.Anulado = ISNULL (@par_Anulado,ENPD.Anulado )                            
		AND ENPD.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENPD.OFIC_Codigo)
		AND ENPD.TIDO_Codigo = @par_TIDO_Codigo
		AND ENPD.OFIC_Codigo IN (
			SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE USOF.USUA_Codigo = @par_Usuario_Consulta
		)                              
	);                                     
	WITH Pagina AS                              
	(                              
		SELECT                             
		ENPD.EMPR_Codigo,                           
		ENPD.Numero ,                            
		ENPD.Numero_Documento ,                            
		ENPD.Fecha,                            
		ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')                              
		+' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS NombreConductor,      
		VEHI.Placa as PlacaVehiculo,                             
		VEHI.Codigo as VEHI_Codigo,      
		RUTA.Nombre AS NombreRuta,                            
		RUTA.Codigo AS RUTA_Codigo,      
		ENPD.Estado,                            
		ENPD.Anulado,                 
		ENMC.Numero_Documento AS NumeroManifiesto,             
		ENMC.Numero AS ENMC_Numero,               
		ENPD.Valor_Flete_Transportador,            
		ENPD.Valor_Anticipo,            
		Obtener=0,                            
		NVDE.Cantidad AS Novedades,    
		ENPD.ECPD_Numero AS ECPD_Numero,    
		ROW_NUMBER() OVER ( ORDER BY   ENPD.NUMERO DESC) AS RowNumber        
        
		FROM Encabezado_Planilla_Despachos  ENPD                       
                            
		left join Vehiculos VEHI on                    
		ENPD.EMPR_Codigo = VEHI.EMPR_Codigo                             
		AND ENPD.VEHI_Codigo = VEHI.Codigo                            
                        
		left join Terceros TECO  on                    
		ENPD.EMPR_Codigo = TECO.EMPR_Codigo                            
		AND ENPD.TERC_Codigo_Conductor = TECO.Codigo                             
                    
		left join Rutas RUTA   on                    
		ENPD.EMPR_Codigo = RUTA.EMPR_Codigo                            
		AND ENPD.RUTA_Codigo = RUTA.Codigo                   
                     
		left join Encabezado_Manifiesto_Carga ENMC   on                    
		ENPD.EMPR_Codigo = ENMC.EMPR_Codigo                            
		AND ENPD.ENMC_Numero = ENMC.Numero        
           
		LEFT JOIN (SELECT ENPD_Numero,COUNT(*) AS Cantidad FROM Detalle_Novedades_Despacho WHERE EMPR_Codigo = @par_EMPR_Codigo GROUP BY ENPD_Numero) AS NVDE        
		ON NVDE.ENPD_Numero = ENPD.Numero        
                                     
		WHERE                              
                          
		ENPD.EMPR_Codigo = @par_EMPR_Codigo                             
		AND ENPD.Numero_Documento = ISNULL(@par_Numero, ENPD.Numero_Documento)                            
		AND (ENMC.Numero_Documento = ISNULL(@par_Numero_Manifiesto, ENMC.Numero_Documento) OR @par_Numero_Manifiesto IS NULL)                            
                         
		AND ENPD.FECHA >= ISNULL(@par_Fecha_Inicial ,ENPD.FECHA)                            
		AND ENPD.FECHA <=ISNULL(@par_Fecha_Final,ENPD.FECHA )                            
                            
		AND ENPD.RUTA_Codigo = ISNULL( @par_Ruta_Codigo,ENPD.RUTA_Codigo)                            
		AND ENPD.TERC_Codigo_Conductor = ISNULL(@par_Codigo_Conductor,ENPD.TERC_Codigo_Conductor)                            
		AND VEHI.Codigo = ISNULL(@par_Codigo_Placa,VEHI.Codigo)                            
		AND RUTA.Nombre Like'%'+ ISNULL( @par_Ruta,RUTA.Nombre ) +'%'              
		AND ((TECO.Nombre + ' ' + TECO.Apellido1 + ' ' + TECO.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%' OR TECO.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%') OR (@par_Conductor IS NULL))                  
		AND VEHI.Placa = ISNULL(@par_Placa,VEHI.Placa)                            
		AND ENPD.Estado=ISNULL(@par_Estado,ENPD.Estado)                            
		AND ENPD.Anulado = ISNULL (@par_Anulado,ENPD.Anulado )                            
		AND ENPD.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENPD.OFIC_Codigo)   
		AND ENPD.TIDO_Codigo = @par_TIDO_Codigo
		AND ENPD.OFIC_Codigo IN (
			SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE USOF.USUA_Codigo = @par_Usuario_Consulta
		)    
	)                             
	SELECT DISTINCT                              
	EMPR_Codigo,                            
	Numero ,                      
	Numero_Documento,                            
	Fecha,                            
	PlacaVehiculo,      
	VEHI_Codigo,                    
	NombreConductor,                          
	NombreRuta,      
	RUTA_Codigo,   
	Estado,                            
	NumeroManifiesto,            
	ENMC_Numero,              
	Valor_Flete_Transportador,            
	Valor_Anticipo,            
	Anulado,                            
	Novedades,        
	0 as Obtener,    
	ECPD_Numero,                         
                  
	@CantidadRegistros AS TotalRegistros,                              
	@par_NumeroPagina AS PaginaObtener,                              
	@par_RegistrosPagina AS RegistrosPagina                              
	FROM                              
	Pagina                          
	WHERE                              
	RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                              
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                              
	order by Numero_Documento  DESC                            
END
GO