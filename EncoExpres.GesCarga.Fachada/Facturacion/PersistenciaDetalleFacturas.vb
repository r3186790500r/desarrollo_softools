﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Repositorio.Facturacion

Namespace Facturacion
    Public NotInheritable Class PersistenciaDetalleFacturas
        Implements IPersistenciaBase(Of DetalleFacturas)

        Public Function Consultar(filtro As DetalleFacturas) As IEnumerable(Of DetalleFacturas) Implements IPersistenciaBase(Of DetalleFacturas).Consultar
            Return New RepositorioDetalleFacturas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleFacturas) As Long Implements IPersistenciaBase(Of DetalleFacturas).Insertar
            Return New RepositorioDetalleFacturas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleFacturas) As Long Implements IPersistenciaBase(Of DetalleFacturas).Modificar
            Return New RepositorioDetalleFacturas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleFacturas) As DetalleFacturas Implements IPersistenciaBase(Of DetalleFacturas).Obtener
            Return New RepositorioDetalleFacturas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleFacturas) As Boolean
            Return New RepositorioDetalleFacturas().Anular(entidad)
        End Function

    End Class

End Namespace

