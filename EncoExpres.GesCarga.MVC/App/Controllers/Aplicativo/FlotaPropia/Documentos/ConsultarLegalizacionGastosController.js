﻿EncoExpresApp.controller("ConsultarLegalizacionGastosCtrl", ['$scope', '$timeout', 'LegalizacionGastosFactory', 'TercerosFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'OficinasFactory', 'EmpresasFactory',
    function ($scope, $timeout, LegalizacionGastosFactory, TercerosFactory, $linq, blockUI, blockUIConfig, $routeParams, OficinasFactory, EmpresasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Flota Propia' }, { Nombre: 'Documentos' }, { Nombre: 'Legalización Gastos' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.MostrarMensajeError = false;
        $scope.NumeroAnular;
        $scope.NumeroDocumentoAnular;
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoOficinas = [];
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LEGALIZACION_GASTOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarLegalizacionGastos';
            }
        };
        $scope.ModeloOficina = {}
        $scope.ModeloEstado = {}
        //-------------------------------------------------AUTOCOMPLETE-------------------------------------------------//
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores);
                }
            }
            return $scope.ListadoConductores;
        }
        //-------------------------------------------------AUTOCOMPLETE-------------------------------------------------//
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };


        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: CERO },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ]

        $scope.ModeloEstado = $scope.ListadoEstados[CERO]

        $scope.ListadoOficinas = []

        //-- cargar Prefijo Empresa --//
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.pref = response.data.Datos[0].Prefijo;
                //console.log("Empresa Prefijo: ", $scope.pref);
            }
        });

        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.ModeloOficina = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.ModeloNumeros > 0) {
                return continuar
            } else {

                if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '')
                    && ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '')
                    && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)

                ) {
                    $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                    continuar = false

                } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                    || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

                ) {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                        && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                        if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                            $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                            continuar = false
                        } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                            $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO+' dias');
                            continuar = false
                        }
                    }

                    else {
                        if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                            $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                        } else {
                            $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                        }
                    }
                }
                return continuar
            }
        }


        function Find() {
            if (DatosRequeridos()) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: $scope.ModeloNumero,
                    FechaInicial: $scope.ModeloFechaInicial,
                    FechaFinal: $scope.ModeloFechaFinal,
                    Conductor: { Nombre: $scope.ModeloConductor == undefined ? '' : $scope.ModeloConductor.NombreCompleto  },
                    Estado: $scope.ModeloEstado.Codigo,
                    Pagina: $scope.paginaActual,
                    Oficina: $scope.ModeloOficina,
                    Numero: $scope.ModeloNumeros,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
                    Aprobado: -1
                };

                blockUI.start('Buscando registros ...');
                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);
                $scope.Buscando = true;
                $scope.MensajesError = [];
                $scope.ListadoLegalizacionGastos = [];
                if ($scope.Buscando) {
                    if ($scope.MensajesError.length === 0) {
                        blockUI.delay = 1000;
                        $scope.Pagina = $scope.paginaActual
                        $scope.RegistrosPagina = 10
                        LegalizacionGastosFactory.Consultar(filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    $scope.ModeloNumeros = 0
                                    if (response.data.Datos.length > 0) {
                                        $scope.ListadoLegalizacionGastos = response.data.Datos
                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                    }
                                    else {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                    var listado = [];
                                    response.data.Datos.forEach(function (item) {
                                        listado.push(item);
                                    });
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }
                blockUI.stop();
            }
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskValoresGrid = function (valor) {
            return MascaraValores(valor)
        };
        $scope.Anular = function (Numero, NumeroDocumento, Anulado) {
            $scope.MensajesErrorAnula = [];
            $scope.ModeloCausaAnula = '';
            $scope.NumeroAnular = Numero;
            $scope.NumeroDocumentoAnular = NumeroDocumento;
            if (Anulado === 0) {
                showModal('modalAnular')
            } else {
                ShowError('La liquidación ' + NumeroDocumento + ' ya se encuentra anulada')
            }
        }

        $scope.ConfirmaAnular = function () {
            if ($scope.ModeloCausaAnula !== undefined && $scope.ModeloCausaAnula !== null && $scope.ModeloCausaAnula !== '') {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.NumeroAnular,
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnulacion: $scope.ModeloCausaAnula,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_LEGALIZACION_GASTOS
                }
                LegalizacionGastosFactory.Anular(filtros).
                    then(function (response) {
                        closeModal('modalAnular')
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.ProcesoExitoso === true) {
                                ShowSuccess('La legalización de gastos se anulo correctamente');
                                Find();
                            }
                            else {
                                ShowError('Se presentó un error al anular la liquidación')
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.MensajesErrorAnula.push('Debe ingresar la causa de anulación');
            }
        }
        //-----------------------------------------------Reportes-----------------------------------------------//
        $scope.DesplegarInforme = function ( Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroReporte = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_LEGALIZACION_GASTOS;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL +'&usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroReporte != undefined && $scope.NumeroReporte != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroReporte;
            }
        };

        $scope.GenerarMovimientoContable = function (NumeroDocumento) {
            EncabezadoComprobantesContablesFactory.Guardar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroDocumento: NumeroDocumento, TipoDocumentoOrigen: { Codigo: CODIGO_TIPO_DOCUMENTO_LEGALIZACION_GASTOS } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('El movimiento contable se regeneró correctamente');
                    } else {
                        ShowError('No se generó el movimiento contable, por favor verifique la parametrización ');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        //-----------------------------------------------Reportes-----------------------------------------------//
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.ModeloNumero = $routeParams.Codigo;
                Find();
            } else {
                $scope.ModeloFechaFinal = new Date();
            }
        } else {
            $scope.ModeloFechaFinal = new Date();
        }
    }]);