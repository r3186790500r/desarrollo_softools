﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Repositorio.Basico.Almacen

Namespace Basico.Almacen
    Public NotInheritable Class PersistenciaEncabezadoUbicacionAlmacenes
        Implements IPersistenciaBase(Of EncabezadoUbicacionAlmacenes)

        Public Function Consultar(filtro As EncabezadoUbicacionAlmacenes) As IEnumerable(Of EncabezadoUbicacionAlmacenes) Implements IPersistenciaBase(Of EncabezadoUbicacionAlmacenes).Consultar
            Return New RepositorioEncabezadoUbicacionAlmacenes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoUbicacionAlmacenes) As Long Implements IPersistenciaBase(Of EncabezadoUbicacionAlmacenes).Insertar
            Return New RepositorioEncabezadoUbicacionAlmacenes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoUbicacionAlmacenes) As Long Implements IPersistenciaBase(Of EncabezadoUbicacionAlmacenes).Modificar
            Return New RepositorioEncabezadoUbicacionAlmacenes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoUbicacionAlmacenes) As EncabezadoUbicacionAlmacenes Implements IPersistenciaBase(Of EncabezadoUbicacionAlmacenes).Obtener
            Return New RepositorioEncabezadoUbicacionAlmacenes().Obtener(filtro)
        End Function
        Public Function Eliminar(entidad As EncabezadoUbicacionAlmacenes) As Boolean
            Return New RepositorioEncabezadoUbicacionAlmacenes().Eliminar(entidad)
        End Function
    End Class

End Namespace

