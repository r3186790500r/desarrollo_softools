﻿EncoExpresApp.controller("ConsultarEstudioSeguridadCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', '$routeParams', 'EstudioSeguridadFactory',
    function ($scope, $timeout, $linq, blockUI, ValorCatalogosFactory, $routeParams, EstudioSeguridadFactory) {
        $scope.MapaSitio = [{ Nombre: 'Seguridad' }, { Nombre: 'Documentos' }, { Nombre: 'Estudio Seguridad' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.Numero = 0;
        $scope.Aprobacion = -1;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ESTUDIO_SEGURIDAD); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];

        $scope.Estado = $scope.ListadoEstados[0]

        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_SOLICITUD } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSolicitud = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoSolicitud.push({ Nombre: '(TODOS)', Codigo: -1 })
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoSolicitud.push(item)
                        });

                        $scope.EstadoSolicitud = $scope.ListadoSolicitud[0]
                    }
                    else {
                        $scope.ListadoSolicitud = []
                    }
                }
            }, function (response) {
            });

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        /*Metodo buscar*/
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                    $scope.Aprobacion = -1;
                    Find()
                }
            }
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarEstudioSeguridad';
            }
        };

        /* Obtener parametros*/
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            $scope.EstadoSolicitud = { Codigo: -1 }
            if ($routeParams.Numero > 0) {
                $scope.NumeroDocumento = $routeParams.Numero;
                $scope.Aprobacion = $routeParams.Aprobacion 
                Find();
            } else {
                $scope.FechaFin = new Date();
                $scope.FechaInicio = new Date();
            }
    
        } else {
            $scope.FechaFin = new Date();
            $scope.FechaInicio = new Date();
        }

        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.ListadoEstudioSeguridad = [];

            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.NumeroDocumento,
                FechaInicio: $scope.FechaInicio,
                FechaFin: $scope.FechaFin,
                Oficinas: $scope.Sesion.UsuarioAutenticado.Oficinas,
                Placa: $scope.ModeloPlaca,
                NombrePropietario: $scope.NombrePropietario,
                NombreConductor: $scope.NombreConductor,
                NombreTenedor: $scope.NombreTenedor,
                UsuarioSolicito: $scope.UsuarioSolicito,
                UsuarioEstudio: $scope.UsuarioEstudio,
                EstadoAutorizacion: $scope.EstadoSolicitud.Codigo,
                Estado: $scope.Estado.Codigo,
                Numeracion: $scope.Numeracion,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina
            }

            blockUI.delay = 1000;
            EstudioSeguridadFactory.Consultar(Filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoEstudioSeguridad = response.data.Datos

                            for (var i = 0; i < $scope.ListadoEstudioSeguridad.length; i++) {
                                if (new Date($scope.ListadoEstudioSeguridad[i].FechaEstudio) < MIN_DATE) {
                                    $scope.ListadoEstudioSeguridad[i].FechaEstudio = ''
                                }
                            }
                            for (var k = 0; k < $scope.ListadoEstudioSeguridad.length; k++) {
                                if ($scope.ListadoEstudioSeguridad[k].NumeroAutorizacion === 0) {
                                    $scope.ListadoEstudioSeguridad[k].NumeroAutorizacion = ''
                                }
                            }

                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = true;
                            $scope.ResultadoSinRegistros = '';

                            if ($scope.Aprobacion === '1') {
                                ShowSuccess('Se autorizó el estudio de seguridad No. ' + $scope.NumeroDocumento);
                            } else if ($scope.Aprobacion === '0') {
                                ShowSuccess('Se rechazó el estudio de seguridad No. ' + $scope.NumeroDocumento);
                            }
                        }
                        else {
                            $scope.ListadoEstudioSeguridad = "";
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    $scope.Buscando = false;
                });
            blockUI.stop();
        };


        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroDocumento === null || $scope.NumeroDocumento === undefined || $scope.NumeroDocumento === '' || $scope.NumeroDocumento === 0 || isNaN($scope.NumeroDocumento) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de número o fecha');
                continuar = false

            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------------Eliminar-----------------------------------------------------------------------*/
        $scope.EliminarEstudioSeguridad = function (codigo, numero) {
            $scope.CodigoEstudioSeguridad = codigo
            $scope.NumeroDocumentoEstudioSeguridad = numero
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarEstudioSeguridad');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoEstudioSeguridad,
            };

            EstudioSeguridadFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se anulo el estudio de seguridad No.' + $scope.NumeroDocumentoEstudioSeguridad);
                        closeModal('modalEliminarEstudioSeguridad');
                        $scope.CodigoAlterno = '';
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarEstudioSeguridad');
                    $scope.ModalError = 'No se puede anular el estudio de seguridad No.' + $scope.NumeroDocumentoEstudioSeguridad + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.CerrarModal = function () {
            closeModal('modalEliminarEstudioSeguridad');
            closeModal('modalMensajeEliminarEstudioSeguridad');
        }
        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskPlaca = function () {
            try { $scope.ModeloPlaca = MascaraPlaca($scope.ModeloPlaca) } finally { }
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    }]);