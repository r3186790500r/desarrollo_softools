﻿PRINT 'gsp_consultar_documentos_pendientes_reportar_ministerio'
GO

DROP PROCEDURE gsp_consultar_documentos_pendientes_reportar_ministerio
GO

CREATE PROCEDURE gsp_consultar_documentos_pendientes_reportar_ministerio
(
	@par_EMPR_Codigo SMALLINT,
	@par_USUA_Codigo INT
)
AS
BEGIN
	--LIMPIA LA TABLA TEMPORAL
	DELETE T_Documentos_Pendientes_Reportar_Ministerio WHERE EMPR_Codigo = @par_EMPR_Codigo AND USUA_Codigo = @par_USUA_Codigo

	--INSERTAR REMESAS
	INSERT INTO T_Documentos_Pendientes_Reportar_Ministerio
	(
		EMPR_Codigo,
		USUA_Codigo,
		ENRE_Numero,
		ENMA_Numero,
		ENCU_Numero
	)
	SELECT
		@par_EMPR_Codigo,
		@par_USUA_Codigo,
		ENRE.Numero,
		0,
		0
	FROM
		Encabezado_Remesas ENRE,
		Detalle_Manifiesto_Carga DMCA,
		Rutas RUTA,
		Ciudades CIOR,
		Ciudades CIDE
	WHERE
		ENRE.EMPR_Codigo = DMCA.EMPR_Codigo
		AND ENRE.Numero = DMCA.ENRE_Numero
		AND ENRE.EMPR_Codigo = RUTA.EMPR_Codigo
		AND ENRE.RUTA_Codigo = RUTA.Codigo
		AND ENRE.EMPR_Codigo = CIOR.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo
		AND ENRE.EMPR_Codigo = CIDE.EMPR_Codigo		
		AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo

		AND ENRE.EMPR_Codigo = @par_EMPR_Codigo
		AND CIOR.Codigo_Alterno <>  CIDE.Codigo_Alterno
		AND ENRE.CATA_TIRE_Codigo = 8811 --Remesa Nacional
		AND ENRE.Estado = 1
		AND ENRE.Anulado = 0
		AND DMCA.Numero_Remesa_Electronico = 0
------------------------------------------------------------------------------------

	--INSERTAR MANIFIESTOS
	INSERT INTO T_Documentos_Pendientes_Reportar_Ministerio
	(
		EMPR_Codigo,
		USUA_Codigo,
		ENRE_Numero,
		ENMA_Numero,
		ENCU_Numero
	)
	SELECT
		@par_EMPR_Codigo,
		@par_USUA_Codigo,
		0,
		ENMC.Numero,
		0
	FROM
		Encabezado_Manifiesto_Carga ENMC,
		Rutas RUTA,
		Ciudades CIOR,
		Ciudades CIDE
	WHERE 
		ENMC.EMPR_Codigo = RUTA.EMPR_Codigo
		AND ENMC.RUTA_Codigo = RUTA.Codigo
		AND ENMC.EMPR_Codigo = CIOR.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo
		AND ENMC.EMPR_Codigo = CIDE.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo

		AND ENMC.EMPR_Codigo = @par_EMPR_Codigo		
		AND CIOR.Codigo_Alterno <>  CIDE.Codigo_Alterno
		AND ENMC.Estado = 1
		AND ENMC.Anulado = 0
		AND ENMC.Numero_Manifiesto_Electronico = 0
------------------------------------------------------------------------------------

	--INSERTAR CUMPLIDOS
	INSERT INTO T_Documentos_Pendientes_Reportar_Ministerio
	(
		EMPR_Codigo,
		USUA_Codigo,
		ENRE_Numero,
		ENMA_Numero,
		ENCU_Numero
	)
	SELECT
		@par_EMPR_Codigo,
		@par_USUA_Codigo,
		0,
		0,
		ECPD.Numero
	FROM
		Encabezado_Cumplido_Planilla_Despachos ECPD,
		Encabezado_Manifiesto_Carga ENMC,
		Rutas RUTA,
		Ciudades CIOR,
		Ciudades CIDE
	WHERE 
		ECPD.EMPR_Codigo = ENMC.EMPR_Codigo
		AND ECPD.ENMC_Numero = ENMC.Numero
		AND ENMC.EMPR_Codigo = RUTA.EMPR_Codigo
		AND ENMC.RUTA_Codigo = RUTA.Codigo
		AND ENMC.EMPR_Codigo = CIOR.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo
		AND ENMC.EMPR_Codigo = CIDE.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo

		AND ECPD.EMPR_Codigo = @par_EMPR_Codigo
		AND CIOR.Codigo_Alterno <>  CIDE.Codigo_Alterno
		AND ECPD.Anulado = 0
		AND ENMC.Anulado = 0
		AND ECPD.Estado = 1
		AND ENMC.Estado = 1
		AND ENMC.Numero_Cumplido_Electronico = 0
------------------------------------------------------------------------------------

	--INSERTAR REMESAS ANULADAS
	INSERT INTO T_Documentos_Pendientes_Reportar_Ministerio
	(
		EMPR_Codigo,
		USUA_Codigo,
		ENRE_Numero,
		ENMA_Numero,
		ENCU_Numero
	)
	SELECT
		@par_EMPR_Codigo,
		@par_USUA_Codigo,
		ENRE.Numero,
		0,
		0
	FROM
		Encabezado_Remesas ENRE,
		Detalle_Manifiesto_Carga DMCA,
		Rutas RUTA,
		Ciudades CIOR,
		Ciudades CIDE
	WHERE
		ENRE.EMPR_Codigo = DMCA.EMPR_Codigo
		AND ENRE.Numero = DMCA.ENRE_Numero
		AND ENRE.EMPR_Codigo = RUTA.EMPR_Codigo
		AND ENRE.RUTA_Codigo = RUTA.Codigo
		AND ENRE.EMPR_Codigo = CIOR.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo
		AND ENRE.EMPR_Codigo = CIDE.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo

		AND ENRE.EMPR_Codigo = @par_EMPR_Codigo
		AND CIOR.Codigo_Alterno <>  CIDE.Codigo_Alterno
		AND DMCA.Numero_Remesa_Electronico <> 0
		--AND DMCA.Confirmacion_Anulacion_Documento_Ministerio = 0 Por confirmar
		AND ENRE.Anulado = 1
		AND ENRE.Estado = 1
------------------------------------------------------------------------------------

	--INSERTAR MANIFIESTOS ANULADOS
	INSERT INTO T_Documentos_Pendientes_Reportar_Ministerio
	(
		EMPR_Codigo,
		USUA_Codigo,
		ENRE_Numero,
		ENMA_Numero,
		ENCU_Numero
	)
	SELECT
		@par_EMPR_Codigo,
		@par_USUA_Codigo,
		0,
		ENMC.Numero,
		0
	FROM
		Encabezado_Manifiesto_Carga ENMC,
		Rutas RUTA,
		Ciudades CIOR,
		Ciudades CIDE
	WHERE
		ENMC.EMPR_Codigo = RUTA.EMPR_Codigo
		AND ENMC.RUTA_Codigo = RUTA.Codigo
		AND ENMC.EMPR_Codigo = CIOR.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo
		AND ENMC.EMPR_Codigo = CIDE.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo

		AND ENMC.EMPR_Codigo = @par_EMPR_Codigo		
		AND CIOR.Codigo_Alterno <>  CIDE.Codigo_Alterno
		AND ENMC.Anulado = 1
		AND ENMC.Numero_Manifiesto_Electronico  <> 0
		AND ENMC.Numero_Anulacion_Cumplido_Electronico = 0
------------------------------------------------------------------------------------

	--INSERTA REMESAS CUMPLIDAS EN GESTRANS PERO PENDIENTES POR CUMPLIR EN EL MINISTERIO SIEMPRE Y CUANDO TENGAN UNA RUTA CON CIDUAD DE ORIGEN Y CIUDAD DESTINO DISTINTA	
	INSERT INTO T_Documentos_Pendientes_Reportar_Ministerio
	(
		EMPR_Codigo,
		USUA_Codigo,
		ENRE_Numero,
		ENMA_Numero,
		ENCU_Numero
	)
	SELECT
		@par_EMPR_Codigo,
		@par_USUA_Codigo,
		ENRE.Numero,
		0,
		0 
	FROM
		Encabezado_Remesas ENRE,
		Detalle_Manifiesto_Carga DMCA,
		Encabezado_Manifiesto_Carga ENMC,
		Rutas RUTA,
		Ciudades CIOR,
		Ciudades CIDE,
		Encabezado_Cumplido_Planilla_Despachos ECPD, 
		Terceros REMI
	WHERE
		ENRE.EMPR_Codigo = DMCA.EMPR_Codigo
		AND ENRE.Numero = DMCA.ENRE_Numero
		AND DMCA.EMPR_Codigo = ENMC.EMPR_Codigo
		AND DMCA.ENMC_Numero = ENMC.Numero
		AND ENRE.EMPR_Codigo = RUTA.EMPR_Codigo
		AND ENRE.RUTA_Codigo = RUTA.Codigo
		AND ENRE.EMPR_Codigo = CIOR.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo
		AND ENRE.EMPR_Codigo = CIDE.EMPR_Codigo
		AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo
		AND ENRE.EMPR_Codigo = ECPD.EMPR_Codigo
		AND ENRE.ECPD_Numero = ECPD.Numero
		AND ENRE.EMPR_Codigo = REMI.EMPR_Codigo
		AND ENRE.TERC_Codigo_Remitente = REMI.Codigo

		AND ENRE.EMPR_Codigo = @par_EMPR_Codigo		
		AND CIOR.Codigo_Alterno <>  CIDE.Codigo_Alterno		

		AND DMCA.Numero_Remesa_Electronico <> 0 --Remesa reportada
		AND ENMC.Numero_Cumplido_Electronico = 0 --Cumplido no reportado
		AND ENRE.Anulado = 0
		AND ECPD.Anulado = 0
		AND ENRE.CATA_TIRE_Codigo = 8811 --Remesa Nacional
		AND ENRE.ENMC_Numero <> 0
END
GO