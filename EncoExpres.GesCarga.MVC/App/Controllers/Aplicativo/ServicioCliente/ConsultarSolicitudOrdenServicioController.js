﻿EncoExpresApp.controller("ConsultarSolicitudOrdenServicioCtrl", ['$scope', '$timeout', 'EncabezadoSolicitudOrdenServiciosFactory', '$linq', 'blockUI', '$routeParams', 'TercerosFactory', 'EmpresasFactory', 'ValorCatalogosFactory',
    function ($scope, $timeout, EncabezadoSolicitudOrdenServiciosFactory, $linq, blockUI, $routeParams, TercerosFactory, EmpresasFactory, ValorCatalogosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Documentos' }, { Nombre: 'Solicitud Servicio' }];
        $scope.Titulo = "CONSULTAR SOLICITUD SERVICIO";
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.TipoSolicitud = 0;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.ListadoOrdenServicios = [];
        $scope.MostrarMensajeError = false
        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];

        $scope.ModalEstadoSolicitus = { Codigo: 0 };
        $scope.ListadoEstadoSolicitud = []
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.Cliente = '';
        try {
            try {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SOLICITUD_ORDEN_SERVICIO);
                if ($scope.Sesion.UsuarioAutenticado.Cliente.Codigo > 0 && $scope.Sesion.UsuarioAutenticado.Cliente.Codigo != undefined && $scope.Sesion.UsuarioAutenticado.Cliente.Codigo != '') {
                    $scope.Cliente = $scope.CargarTercero($scope.Sesion.UsuarioAutenticado.Cliente.Codigo)
                    $scope.DeshabilitarCliente = true;
                    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

                } else {
                    if ($scope.Sesion.UsuarioAutenticado.Codigo > 0) {
                        ShowError('El usuario ingresado no tiene clientes asociados')
                        $scope.DeshabilitarConsulta = true
                        $scope.DeshabilitarEliminarAnular = true
                        $scope.DeshabilitarImprimir = true
                        $scope.DeshabilitarActualizar = true

                    }
                }

            } catch (e) {

                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SOLICITUD_SERVICIO);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
            }
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/



        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*----------------------------------------------------------------- FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_SOLICITUD_ORDEN_SERVICIO;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref + '&TipoDocumento=' + CODIGO_TIPO_DOCUMENTO_SOLICITUD_ORDEN_SERVICIO);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref + '&TipoDocumento=' + CODIGO_TIPO_DOCUMENTO_SOLICITUD_ORDEN_SERVICIO);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_TRAMITE_SOLICITUD } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEstadoSolicitud = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoEstadoSolicitud.push({ Codigo: 0, Nombre: '(TODAS)' });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoEstadoSolicitud.push(response.data.Datos[i]);

                        }
                        $scope.ModalEstadoSolicitus = $linq.Enumerable().From($scope.ListadoEstadoSolicitud).First('$.Codigo == ' + CERO);
                    }
                    else {
                        $scope.ListadoEstadoSolicitud = []
                    }
                }
            }, function (response) {
            });

        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento != undefined && $scope.NumeroDocumento != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }

        }
        //-- Carga Informacion necesaria para iniciar
        var ClientesCargados = false;
        var TimeOutCarga;
        //-- Consulta Clientes --//
        //-- Consulta Clientes --//
        $scope.ListadoCliente = []
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente)
                }
            }
            return $scope.ListadoCliente
        }
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarSolicitudOrdenservicio/' + CERO + "/" + $scope.TipoSolicitud;
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    $scope.Codigo = 0
                    Find()
                }
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoOrdenServicios = [];
            if ($scope.Buscando) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Numero,
                    NumeroDocumento: $scope.ModeloNumero,
                    FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaInicial,
                    FechaFinal: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaFinal,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_SOLICITUD_ORDEN_SERVICIO,
                    Estado: $scope.Estado.Codigo,
                    Cliente: $scope.Cliente,
                    Pagina: $scope.paginaActual,
                    EstadoOrden: { Codigo: $scope.ModalEstadoSolicitus.Codigo },
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                }
                blockUI.delay = 1000;
                EncabezadoSolicitudOrdenServiciosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoOrdenServicios = response.data.Datos
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.stuatusText);
                    });
            }
            blockUI.stop();
        }

        $scope.AnularOrdenServicio = function (Numero, NumeroDocumento) {
            $scope.Numero = Numero

            $scope.ModalErrorCompleto = ''
            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnularOrdenServicio');
        };

        $scope.Anular = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.ModeloCausaAnula,
                Numero: $scope.Numero,

            };
            if (DatosRequeridosAnular()) {
                EncabezadoSolicitudOrdenServiciosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anuló la orden de servicio ' + $scope.NumeroDocumento);
                            closeModal('modalAnularOrdenServicio');
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            }


        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/

        try {
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && parseInt($routeParams.Numero) !== 0) {
                $scope.ModeloNumero = $routeParams.Numero;
                //$scope.Cliente = { NombreCompleto: '', Codigo: 0 }
                ValEnviaFind();
            }
            if ($routeParams.Tipo !== undefined && $routeParams.Tipo !== null && $routeParams.Tipo !== '') {//habilita o desabilita Cliente
                $scope.TipoSolicitud = parseInt($routeParams.Tipo);
                if ($scope.TipoSolicitud == 1) {
                    $scope.DeshabilitarCliente = false;
                    $scope.Cliente = '';
                }
            }
        }
        catch (e) {
            ShowError(e);
        }

        function ValEnviaFind() {
            if (ClientesCargados == true) {
                clearTimeout(TimeOutCarga);
                Find();
            }
            else {
                clearTimeout(TimeOutCarga);
                TimeOutCarga = setTimeout(ValEnviaFind, 100);
            }
        }

        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '')
                && ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '')
                && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                    if ($scope.ModeloFechaInicial < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.ModeloFechaInicial - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }
                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaInicial
                    }
                }
            }
            return continuar
        }

        $scope.MaskMayus = function () {
            try { $scope.Cliente = $scope.Cliente.toUpperCase() } catch (e) { }
        };

        $scope.AsignarListadoClientes = function (value) {
            return RetornarListaAutocomplete(value, $scope.ListadoClientes, 'NombreCompleto', 'Codigo');
        }
    }]);