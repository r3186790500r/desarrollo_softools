﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Negocio.Comercial.Documentos

''' <summary>
''' Controlador <see cref="TipoLineaNegocioTransportesController"/>
''' </summary>
<Authorize>
Public Class TipoLineaNegocioTransportesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TipoLineaNegocioTransportes) As Respuesta(Of IEnumerable(Of TipoLineaNegocioTransportes))
        Return New LogicaTipoLineaNegocioTransportes(CapaPersistenciaTipoLineaNegocioTransportes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TipoLineaNegocioTransportes) As Respuesta(Of TipoLineaNegocioTransportes)
        Return New LogicaTipoLineaNegocioTransportes(CapaPersistenciaTipoLineaNegocioTransportes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TipoLineaNegocioTransportes) As Respuesta(Of Long)
        Return New LogicaTipoLineaNegocioTransportes(CapaPersistenciaTipoLineaNegocioTransportes).Guardar(entidad)
    End Function

End Class
