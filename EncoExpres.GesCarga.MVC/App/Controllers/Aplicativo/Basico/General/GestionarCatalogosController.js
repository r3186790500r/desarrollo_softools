﻿EncoExpresApp.controller("GestionarCatalogosCtrl", ['$scope', '$routeParams', 'blockUI', '$timeout', 'CatalogosFactory', 'ConfiguracionCatalogosFactory', 'ValorCatalogosFactory', '$linq',
    function ($scope, $routeParams, blockUI, $timeout, CatalogosFactory, ConfiguracionCatalogosFactory, ValorCatalogosFactory, $linq) {
        $scope.MensajesError = [];
        $scope.MensajesErrorDetalle = [];
        $scope.Deshabilitar = false;

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoConfiguracionCatalogos = [];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CATALOGOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Catálogos' }];
        $scope.Modelo = {

        };
        $scope.Entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        };
        $scope.ModeloFecha = new Date();
        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            $scope.CodigoAlteno = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.CodigoAlteno = 0;

        }
        /* Obtener parametros del enrutador*/
        if ($routeParams.CodigoCatalogo !== undefined) {
            $scope.Modelo.CodigoCatalogo = $routeParams.CodigoCatalogo;
        }
        else {
            $scope.Modelo.CodigoCatalogo = 0;
        }
        /* Obtener parametros del enrutador*/
        if ($routeParams.Consecutivo !== undefined) {
            $scope.Modelo.Consecutivo = $routeParams.Consecutivo;
        }
        else {
            $scope.Modelo.Consecutivo = 0;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($routeParams.CodigoCatalogo > 0) {
                location.href = '#!ConsultarCatalogos/' + $scope.ValorCatalogo.Catalogo.Codigo;
            } else {
                document.location.href = '#!ConsultarCatalogos';
            }
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            /*Cargar el combo de Catalogos*/
            ValorCatalogosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: parseInt($scope.Modelo.Codigo) }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ValorCatalogo = response.data.Datos;

                        if ($scope.ValorCatalogo.Codigo !== '') {
                            $scope.Codigo = $scope.ValorCatalogo.Codigo;
                            $scope.DivCodigo = true
                            $scope.Codigo = $scope.ValorCatalogo.Codigo;
                        } else {
                            $scope.DivCodigo = false
                        }
                        if ($scope.ValorCatalogo.Nombre !== '') {
                            $scope.Nombre = $scope.ValorCatalogo.Nombre;
                            $scope.DivNombre = true
                        } else {
                            $scope.DivNombre = false
                        }
                        if ($scope.ValorCatalogo.CampoAuxiliar2 !== '') {
                            $scope.Campo2 = $scope.ValorCatalogo.CampoAuxiliar2;
                            $scope.DivCampo2 = true
                        } else {
                            $scope.DivCampo2 = false
                        }
                        if ($scope.ValorCatalogo.CampoAuxiliar3 !== '') {
                            $scope.Campo3 = $scope.ValorCatalogo.CampoAuxiliar3;
                            $scope.DivCampo3 = true
                        } else {
                            $scope.DivCampo3 = false
                        }
                        if ($scope.ValorCatalogo.CampoAuxiliar4 !== '') {
                            $scope.Campo4 = $scope.ValorCatalogo.CampoAuxiliar4;
                            $scope.DivCampo4 = true
                        } else {
                            $scope.DivCampo4 = false
                        }
                        if ($scope.ValorCatalogo.CampoAuxiliar5 !== '') {
                            $scope.Campo5 = $scope.ValorCatalogo.CampoAuxiliar5;
                            $scope.DivCampo5 = true
                        } else {
                            $scope.DivCampo5 = false
                        }

                        $scope.Titulo = 'EDITAR CATÁLOGO ' + $scope.ValorCatalogo.Catalogo.Nombre.toUpperCase();
                        $scope.Deshabilitar = true;
                        $scope.ConfiguracionCatalogos($scope.ValorCatalogo.Catalogo);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            $scope.ConfiguracionCatalogos = function (Catalogo) {
                /*Cargar el combo de Catalogos*/
                ConfiguracionCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoCatalogo: Catalogo.Codigo }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoConfiguracionCatalogos = response.data.Datos;
                            $scope.ListadoConfiguracionCatalogos.forEach(function (item) {
                                if (item.Secuencia == 1) {
                                    $scope.Modelo.Codigo = item.Nombre
                                }
                                if (item.Secuencia == 2) {
                                    $scope.Modelo.Nombre = item.Nombre
                                }
                                if (item.Secuencia == 3) {
                                    $scope.Modelo.Campo2 = item.Nombre
                                }
                                if (item.Secuencia == 4) {
                                    $scope.Modelo.Campo3 = item.Nombre
                                }
                                if (item.Secuencia == 5) {
                                    $scope.Modelo.Campo4 = item.Nombre
                                }
                                if (item.Secuencia == 6) {
                                    $scope.Modelo.Campo5 = item.Nombre
                                }
                            });
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }

        } else if ($scope.Modelo.CodigoCatalogo > 0) {
            /*Cargar el combo de Catalogos*/
            ConfiguracionCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoCatalogo: $scope.Modelo.CodigoCatalogo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoConfiguracionCatalogos = [];
                        $scope.ListadoConfiguracionCatalogos = response.data.Datos;
                        $scope.ListadoConfiguracionCatalogos.forEach(function (item) {
                            if (item.Secuencia == 1) {
                                $scope.Modelo.Codigo = item.Nombre
                            }
                            if (item.Secuencia == 2) {
                                $scope.Modelo.Nombre = item.Nombre
                            }
                            if (item.Secuencia == 3) {
                                $scope.Modelo.Campo2 = item.Nombre
                            }
                            if (item.Secuencia == 4) {
                                $scope.Modelo.Campo3 = item.Nombre
                            }
                            if (item.Secuencia == 5) {
                                $scope.Modelo.Campo4 = item.Nombre
                            }
                            if (item.Secuencia == 6) {
                                $scope.Modelo.Campo5 = item.Nombre
                            }
                        });
                    }


                }, function (response) {
                    ShowError(response.statusText);
                });
            /*Cargar el combo de Catalogos*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: $scope.Modelo.CodigoCatalogo } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ValorCatalogo = response.data.Datos[0];

                        if ($scope.ValorCatalogo.Codigo !== '') {
                            $scope.Codigo = "";
                            $scope.DivCodigo = true
                        } else {
                            $scope.DivCodigo = false
                        }
                        if ($scope.ValorCatalogo.Nombre !== '') {
                            $scope.Nombre = "";
                            $scope.DivNombre = true
                        } else {
                            $scope.DivNombre = false
                        }
                        if ($scope.ValorCatalogo.CampoAuxiliar2 !== '') {
                            $scope.Campo2 = "";
                            $scope.DivCampo2 = true
                        } else {
                            $scope.DivCampo2 = false
                        }
                        if ($scope.ValorCatalogo.CampoAuxiliar3 !== '') {
                            $scope.Campo3 = "";
                            $scope.DivCampo3 = true
                        } else {
                            $scope.DivCampo3 = false
                        }
                        if ($scope.ValorCatalogo.CampoAuxiliar4 !== '') {
                            $scope.Campo4 = "";
                            $scope.DivCampo4 = true
                        } else {
                            $scope.DivCampo4 = false
                        }
                        if ($scope.ValorCatalogo.CampoAuxiliar5 !== '') {
                            $scope.Campo5 = "";
                            $scope.DivCampo5 = true
                        } else {
                            $scope.DivCampo5 = false
                        }

                        $scope.Titulo = 'NUEVO CATÁLOGO ' + $scope.ValorCatalogo.Catalogo.Nombre.toUpperCase();
                        $scope.Codigo = $scope.Modelo.Consecutivo
                    }
                });
        }

        CatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.CodigoCatalogo }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEstados = [];
                    if (response.data.Datos.length > 0) {
                        $scope.Catalogo = response.data.Datos
                    }
                    else {
                        $scope.ListadoEstados = []
                    }
                }
            }, function (response) {
            });

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardarCatalogo = function () {
            showModal('modalConfirmacionGuardarCatalogo');
            $scope.Entidad.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                $scope.Entidad.Codigo = $scope.Codigo;
            $scope.Entidad.CodigoAlteno = $scope.CodigoAlteno
            $scope.Entidad.Nombre = $scope.Nombre;
            $scope.Entidad.Catalogo = { Codigo: $scope.ValorCatalogo.Catalogo.Codigo }
            $scope.Entidad.CampoAuxiliar2 = $scope.Campo2;
            $scope.Entidad.CampoAuxiliar3 = $scope.Campo3;
            $scope.Entidad.CampoAuxiliar4 = $scope.Campo4;
            $scope.Entidad.CampoAuxiliar5 = $scope.Campo5;
        };
        // Metodo para guardar el catalogo/modificar
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarCatalogo');
            if (DatosRequeridos()) {
                ValorCatalogosFactory.Guardar($scope.Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.CodigoAlteno == 0) {
                                    ShowSuccess('Se guardó el ítem ' + $scope.Nombre + ' del catálogo ' + $scope.Catalogo[0].Nombre);
                                } else {
                                    ShowSuccess('Se modificó el ítem ' + $scope.Nombre + ' del catálogo ' + $scope.Catalogo[0].Nombre);
                                }
                                location.href = '#!ConsultarCatalogos/' + $scope.ValorCatalogo.Catalogo.Codigo;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            return continuar;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


    }]);