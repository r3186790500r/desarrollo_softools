﻿EncoExpresApp.controller("RecibirGuiasOficinaDestinoCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'blockUIConfig', 'OficinasFactory', 'PlanillaPaqueteriaFactory',
    'RemesaGuiasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, blockUIConfig, OficinasFactory, PlanillaPaqueteriaFactory,
        RemesaGuiasFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'RECIBIR GUÍAS OFICINA DESTINO';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Recibir Guías Oficina Destino' }];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.RECIBIR_GUIAS_OFICINA_DESTINO);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        }
        catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.paginaActual = 1;
        $scope.ListadoGuias = [];
        $scope.ListadoGuiasFiltradas = [];
        $scope.PagRemesas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.Filtro = {
            NumeroInicial: '',
            NumeroFinal: '',
            FechaInicial: '',
            FechaFinal: ''
        };
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicial = new Date();
            $scope.FechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Autocomplete

        //----------Autocomplete
        //----------Init
        $scope.InitLoad = function () {

        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //--Filtro Remesas
        $scope.Filtrar = function () {
            var Filtro = $scope.Filtro;
            var nFiltrosAplica = 0;
            if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                nFiltrosAplica++;
            }
            if (Filtro.NumeroFinal !== undefined && Filtro.NumeroFinal > 0) {
                nFiltrosAplica++;
            }
            if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                nFiltrosAplica++;
            }
            if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                nFiltrosAplica++;
            }
            $scope.ListadoGuiasFiltradas = [];
            for (var i = 0; i < $scope.ListadoGuias.length; i++) {
                var cont = 0;
                var item = $scope.ListadoGuias[i];
                if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                    if (item.Remesa.NumeroDocumento >= Filtro.NumeroInicial) {
                        cont++;
                    }
                }
                if (Filtro.NumeroFinal !== undefined && Filtro.NumeroFinal > 0) {
                    if (item.Remesa.NumeroDocumento <= Filtro.NumeroFinal) {
                        cont++;
                    }
                }
                if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                    if (new Date(item.Remesa.Fecha) >= Filtro.FechaInicial) {
                        cont++;
                    }
                }
                if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                    if (new Date(item.Remesa.Fecha) <= Filtro.FechaFinal) {
                        cont++;
                    }
                }
                if (cont == nFiltrosAplica) {
                    $scope.ListadoGuiasFiltradas.push(item);
                }
            }
            $scope.PagRemesas.array = $scope.ListadoGuiasFiltradas;
            ResetPaginacion($scope.PagRemesas);
        };
        $scope.LimpiarFiltro = function () {
            $scope.ListadoGuiasFiltradas = $scope.ListadoGuias;
            $scope.PagRemesas.array = $scope.ListadoGuiasFiltradas;
            ResetPaginacion($scope.PagRemesas);
            $scope.Filtro = {
                NumeroInicial: '',
                NumeroFinal: '',
                FechaInicial: '',
                FechaFinal: ''
            };
        };
        //--Filtro Remesas
        //--Marcar Remeas
        $scope.MarcarRemesas = function (chk) {
            for (var i = 0; i < $scope.ListadoGuiasFiltradas.length; i++) {
                $scope.ListadoGuiasFiltradas[i].Seleccionado = chk;
            }
        };
        //--Marcar Remeas
        //--Busca Planilla y Guias
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                if ($scope.Modelo.Planilla.NumeroDocumento != undefined && $scope.Modelo.Planilla.NumeroDocumento != null && $scope.Modelo.Planilla.NumeroDocumento != '' &&
                    !isNaN($scope.Modelo.Planilla.NumeroDocumento)) {
                    blockUIConfig.autoBlock = true;
                    blockUIConfig.delay = 0;
                    blockUI.start("Consultando información...");
                    $timeout(function () { blockUI.message("Consultando información"); Find(); }, 100);
                }
            }
        };
        function Find() {
            var ResponsePlanilla = PlanillaPaqueteriaFactory.ConsultarPlanillasPendientesRecepcionar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Planilla: {
                    NumeroDocumento: $scope.Modelo.Planilla.NumeroDocumento
                },
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                Sync: true
            });
            if (ResponsePlanilla.ProcesoExitoso == true) {
                if (ResponsePlanilla.Datos.length > 0) {
                    $scope.Modelo = ResponsePlanilla.Datos[0];
                    $scope.Modelo.Planilla.FechaSalida = new Date($scope.Modelo.Planilla.FechaHoraSalida);
                    $scope.Modelo.Planilla.Fecha = new Date($scope.Modelo.Planilla.Fecha);
                    $scope.Modelo.Planilla.HoraSalida = $scope.Modelo.Planilla.FechaSalida.getHours().toString();
                    if ($scope.Modelo.Planilla.FechaSalida.getMinutes().toString().length == 1) {
                        $scope.Modelo.Planilla.HoraSalida += ':0' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString();
                    } else {
                        $scope.Modelo.Planilla.HoraSalida += ':' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString();
                    }
                    $scope.Modelo.Planilla.Cantidad = MascaraValores($scope.Modelo.Planilla.Cantidad);
                    $scope.Modelo.Planilla.Peso = MascaraValores($scope.Modelo.Planilla.Peso);
                    var filtro = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                        Planilla: {
                            Numero: $scope.Modelo.Planilla.Numero,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA
                        },
                        OficinaRecibe: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        Sync: true
                    };
                    $scope.PagRemesas.Buscando = true;
                    $scope.MensajesError = [];
                    $scope.ListadoGuias = [];
                    if ($scope.PagRemesas.Buscando) {
                        var responseRemesas = RemesaGuiasFactory.ConsultarRemesasPendientesRecibirOficina(filtro);
                        if (responseRemesas.ProcesoExitoso == true) {
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                            if (responseRemesas.Datos.length > 0) {
                                $scope.ListadoGuias = responseRemesas.Datos;
                                $scope.ListadoGuiasFiltradas = $scope.ListadoGuias;
                                $scope.PagRemesas.array = $scope.ListadoGuiasFiltradas;
                                $scope.PagRemesas.ResultadoSinRegistros = '';
                                ResetPaginacion($scope.PagRemesas);
                            }
                            else {
                                $scope.PagRemesas.totalRegistros = 0;
                                $scope.PagRemesas.totalPaginas = 0;
                                $scope.PagRemesas.paginaActual = 1;
                                $scope.PagRemesas.array = [];
                                ResetPaginacion($scope.PagRemesas);
                                $scope.PagRemesas.ResultadoSinRegistros = 'No hay datos para mostrar';
                            }
                        }
                    }
                    $scope.PagRemesas.Buscando = false;
                }
                else {
                    ShowError('La planilla ingresada no es válida');
                    $scope.Modelo.Planilla.NumeroDocumento = '';
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                }
            }
            else {
                ShowError('La planilla ingresada no es válida');
                $scope.Modelo.Planilla.NumeroDocumento = '';
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
            }
        }
        //--Busca Planilla y Guias
        //-------------Guardar
        //--Confirma Guardar
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionRecepcionGuias');
        };
        //--Confirma Guardar
        //--ValidarGuardar
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var countReme = 0;
            for (var i = 0; i < $scope.ListadoGuiasFiltradas.length; i++) {
                if ($scope.ListadoGuiasFiltradas[i].Seleccionado) {
                    countReme += 1;
                }
            }
            if (countReme <= 0) {
                $scope.MensajesError.push('Debe selecionar al menos una guía');
                continuar = false;
            }
            return continuar;
        }
        //--ValidarGuardar
        $scope.ValidarRecibirGuias = function () {
            closeModal('modalConfirmacionRecepcionGuias');
            if (DatosRequeridos()) {
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Recibiendo Guías...");
                $timeout(function () { blockUI.message("Recibiendo Guías..."); Guardar(); }, 100);
            }
        };
        function Guardar() {
            var RemePaque = [];
            for (var i = 0; i < $scope.ListadoGuiasFiltradas.length; i++) {
                if ($scope.ListadoGuiasFiltradas[i].Seleccionado) {
                    RemePaque.push({ Numero: $scope.ListadoGuiasFiltradas[i].Remesa.Numero });
                }
            }
            var objRecibirGuia = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroPlanilla: $scope.Modelo.Planilla.Numero,
                EstadoRemesaPaqueteria: { Codigo: CODIGO_CATALOGO_RECIBIDA_OFICINA_DESTINO },
                CodigoOficinaActual: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                RemesaPaqueteria: RemePaque,
                CodigoCiudad: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Sync: true
            };
            var response = RemesaGuiasFactory.RecibirRemesaPaqueteriaOficinaDestino(objRecibirGuia);
            if (response.ProcesoExitoso == true) {
                $scope.ListadoGuias = [];
                $scope.ListadoGuiasFiltradas = [];
                $scope.Modelo = {
                    Planilla: {
                        NumeroDocumento: '',
                        Fecha: '',
                        Vehiculo: '',
                        Semirremolque: '',
                        Conductor: '',
                        Tenedor: '',
                        FechaSalida: '',
                        HoraSalida: '',
                        Ruta: '',
                        Cantidad: '',
                        Peso: ''
                    },
                    Observaciones: ''
                };
                ShowSuccess('La cantidad de remesas recibidas fueron : ' + response.Datos + ' de ' + RemePaque.length);
            }
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
        }
        //-------------Guardar
        //--------------------------PAGINACION-----------------------------//
        function ResetPaginacion(obj) {
            obj.totalRegistros = obj.array.length;
            obj.totalPaginas = Math.ceil(obj.totalRegistros / 10);
            $scope.PrimerPagina(obj);
        }
        $scope.PrimerPagina = function (obj) {
            if (obj.totalRegistros > 10) {
                obj.PagArray = [];
                obj.paginaActual = 1;
                for (var i = 0; i < 10; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            } else {
                obj.PagArray = obj.array;
            }
        };
        $scope.Anterior = function (obj) {
            if (obj.paginaActual > 1) {
                obj.paginaActual -= 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                }
            }
            else {
                $scope.PrimerPagina(obj);
            }
        };
        $scope.Siguiente = function (obj) {
            if (obj.paginaActual < obj.totalPaginas) {
                obj.paginaActual += 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                } else {
                    $scope.UltimaPagina(obj);
                }
            } else if (obj.paginaActual == obj.totalPaginas) {
                $scope.UltimaPagina(obj);
            }
        };
        $scope.UltimaPagina = function (obj) {
            if (obj.totalRegistros > 10 && obj.totalPaginas > 1) {
                obj.paginaActual = obj.totalPaginas;
                var a = obj.paginaActual * 10;
                obj.PagArray = [];
                for (var i = a - 10; i < obj.array.length; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            }
        }
        //--------------------------PAGINACION-----------------------------//
        //--Mascaras
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };
        //----------------------------Funciones Generales---------------------------------//
    }]);