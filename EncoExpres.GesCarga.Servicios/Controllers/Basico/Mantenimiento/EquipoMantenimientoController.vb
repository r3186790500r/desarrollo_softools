﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Negocio.Mantenimiento

<Authorize>
Public Class EquipoMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EquipoMantenimiento) As Respuesta(Of IEnumerable(Of EquipoMantenimiento))
        Return New LogicaEquipoMantenimiento(CapaPersistenciaEquipoMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EquipoMantenimiento) As Respuesta(Of EquipoMantenimiento)
        Return New LogicaEquipoMantenimiento(CapaPersistenciaEquipoMantenimiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EquipoMantenimiento) As Respuesta(Of Long)
        Return New LogicaEquipoMantenimiento(CapaPersistenciaEquipoMantenimiento).Guardar(entidad)
    End Function
End Class