﻿Imports Newtonsoft.Json

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="EstudioSeguridadDocumentos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EstudioSeguridadDocumentos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EstudioSeguridadDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EstudioSeguridadDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            ENESNumero = Read(lector, "ENES_Numero")
            EOESCodigo = Read(lector, "EOES_Codigo")
            TDESCodigo = Read(lector, "TDES_Codigo")
            Referencia = Read(lector, "Referencia")
            Emisor = Read(lector, "Emisor")
            FechaEmision = Read(lector, "Fecha_Emision")
            FechaVence = Read(lector, "Fecha_Vence")
            Documento = Read(lector, "Documento")
            NombreDocumento = Read(lector, "Nombre_Documento")
            ExtensionDocumento = Read(lector, "Extension_Documento")
            CodigoConfiguracion = Read(lector, "CDGC_Codigo")

        End Sub

        <JsonProperty>
        Public Property ENESNumero As Integer
        <JsonProperty>
        Public Property EOESCodigo As Integer
        <JsonProperty>
        Public Property TDESCodigo As Integer
        <JsonProperty>
        Public Property Referencia As String
        <JsonProperty>
        Public Property Emisor As String
        <JsonProperty>
        Public Property FechaEmision As Date
        <JsonProperty>
        Public Property FechaVence As Date
        <JsonProperty>
        Public Property Documento As Byte()
        <JsonProperty>
        Public Property NombreDocumento As String
        <JsonProperty>
        Public Property ExtensionDocumento As String
        <JsonProperty>
        Public Property CodigoConfiguracion As Integer
    End Class
End Namespace
