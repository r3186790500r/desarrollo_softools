﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaTipoDocumentos
        Implements IPersistenciaBase(Of TipoDocumentos)

        Public Function Consultar(filtro As TipoDocumentos) As IEnumerable(Of TipoDocumentos) Implements IPersistenciaBase(Of TipoDocumentos).Consultar
            Return New RepositorioTipoDocumentos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TipoDocumentos) As Long Implements IPersistenciaBase(Of TipoDocumentos).Insertar
            Return New RepositorioTipoDocumentos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TipoDocumentos) As Long Implements IPersistenciaBase(Of TipoDocumentos).Modificar
            Return New RepositorioTipoDocumentos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TipoDocumentos) As TipoDocumentos Implements IPersistenciaBase(Of TipoDocumentos).Obtener
            Return New RepositorioTipoDocumentos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As TipoDocumentos) As Boolean
            Return New RepositorioTipoDocumentos().Anular(entidad)
        End Function
    End Class

End Namespace

