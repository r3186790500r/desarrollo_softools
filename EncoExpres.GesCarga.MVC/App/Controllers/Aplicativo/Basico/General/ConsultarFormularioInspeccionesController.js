﻿EncoExpresApp.controller("ConsultarFormularioInspeccionesCtrl", ['$scope', '$timeout', '$linq', 'blockUI', '$routeParams', 'FormularioInspeccionesFactory', 'ValorCatalogosFactory',
    function ($scope, $timeout, $linq, blockUI, $routeParams, FormularioInspeccionesFactory, ValorCatalogosFactory ) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Formatos Inspección' }];
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_FORMATOS_INSPECCIONES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        /*Cargar el combo de documento origen*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_TIPO_DOCUMENTO_DESPACHO_INSPECCIONES } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoDocumentoOrigen = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoTipoDocumentoOrigen.push(item);
                    });
                    $scope.TipoDocumentoOrigen = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarFormularioInspecciones';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoFormularioInspecciones = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CodigoAlterno: $scope.CodigoAlterno,
                        Nombre: $scope.Nombre,
                        Codigo: $scope.Codigo,
                        TipoDocumentoOrigen: $scope.TipoDocumentoOrigen,
                        Estado: $scope.ModalEstado.Codigo,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }
                    blockUI.delay = 1000;
                    FormularioInspeccionesFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoFormularioInspecciones = response.data.Datos

                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        /*------------------------------------------------------------------------------------Eliminar formulario de inspección-----------------------------------------------------------------------*/
        $scope.EliminarFormularioInspecciones = function (codigo, Nombre, CodigoAlterno) {
            $scope.CodigoFormularioInspecciones = codigo
            $scope.NombreFormularioInspecciones = Nombre
            $scope.CodigoAlterno = CodigoAlterno
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalFormularioInspecciones');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoFormularioInspecciones,
            };

            FormularioInspeccionesFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó el formulario de inspección ' + $scope.CodigoAlterno + ' - ' + $scope.NombreFormularioInspecciones);
                        closeModal('modalFormularioInspecciones');
                        $scope.CodigoAlterno = '';
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                        $scope.CodigoAlterno = '';
                        Find();
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeFormularioInspecciones');
                    $scope.ModalError = 'No se puede eliminar el formulario de inspección ' + $scope.NombreFormularioInspecciones + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            $scope.TipoDocumentoOrigen = { Codigo: 13000 }
            Find();
        }

        $scope.CerrarModal = function () {
            closeModal('modalFormularioInspecciones');
            closeModal('modalMensajeFormularioInspecciones');
        }

        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };

    }]);