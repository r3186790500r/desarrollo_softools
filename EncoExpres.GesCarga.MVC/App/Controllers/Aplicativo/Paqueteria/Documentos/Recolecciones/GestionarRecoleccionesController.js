﻿EncoExpresApp.controller("GestionarRecoleccionesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'RutasFactory', 'CiudadesFactory', 'OficinasFactory', 'ZonasFactory', 'RecoleccionesFactory', 'UnidadEmpaqueFactory', 'TercerosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, RutasFactory, CiudadesFactory, OficinasFactory, ZonasFactory, RecoleccionesFactory, UnidadEmpaqueFactory, TercerosFactory) {

        $scope.Titulo = 'GESTIONAR RECOLECCIONES';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Recolecciones' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            try {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_RECOLECCIONES_GESPHONE);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

            } catch (e) {

                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_RECOLECCIONES);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
            }
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }


        $scope.DeshabilitarNumeroDocumento = false;
        $scope.DeshabilitarCiudad = false;
        $scope.Numero = 0;
        $scope.ModeloFecha = new Date();
        $scope.Latitud = '';
        $scope.Longitud = '';

        /* Obtener parametros*/
        if ($routeParams.Numero !== undefined) {
            $scope.Numero = $routeParams.Numero;
            if ($scope.Numero > 0) {
                Obtener()
            }
        }


        /*Cargar el combo de oficinas*/
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Pagina: 1, Codigo: -1, RegistrosPagina: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = []
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.forEach(function (item) {
                            if (item.Codigo !== 0) {
                                $scope.ListadoOficinas.push(item);
                            }
                        });
                        if ($scope.CodigoOficinas !== undefined && $scope.CodigoOficinas !== '' && $scope.CodigoOficinas !== null) {
                            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas[0]).First('$.Codigo ==' + $scope.CodigoOficinas);
                        }
                    }
                    else {
                        $scope.ListadoOficinas = '';
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de zonas*/
        ZonasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Ciudad: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoZonas = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoZonas = response.data.Datos;
                        if ($scope.CodigoZonas !== undefined && $scope.CodigoZonas !== '' && $scope.CodigoZonas !== null) {
                            $scope.ModeloZonas = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==' + $scope.CodigoZonas);
                        }
                    } else {
                        $scope.ListadoZonas.push({ Nombre: '(NO APLICA)', Codigo: 0 })
                        $scope.ModeloZonas = $scope.ListadoZonas[0]
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de ciudades*/
        CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListaCiudades = response.data.Datos;
                        if ($scope.CodigoCiudad !== undefined && $scope.CodigoCiudad !== '' && $scope.CodigoCiudad !== 0 && $scope.CodigoCiudad !== null) {
                            if ($scope.CodigoCiudad > 0) {
                                $scope.ModeloCiudad = $linq.Enumerable().From($scope.ListaCiudades).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo);
                            } else {
                                $scope.ModeloCiudad = $linq.Enumerable().From($scope.ListaCiudades).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.ModeloCiudad = $linq.Enumerable().From($scope.ListaCiudades).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo);
                        }
                    }
                    else {
                        $scope.ListaCiudades = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de unidad empaque*/
        UnidadEmpaqueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoUnidadEmpaque = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoUnidadEmpaque = response.data.Datos;
                        if ($scope.CodigoUnidadEmpaque !== undefined && $scope.CodigoUnidadEmpaque !== '' && $scope.CodigoUnidadEmpaque !== null) {
                            $scope.ModeloUnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo ==' + $scope.CodigoUnidadEmpaque);
                        }
                    }
                    else {
                        $scope.ListadoUnidadEmpaque = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de cliente*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, CadenaPerfiles: PERFIL_CLIENTE }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                        $scope.ListaClientes = response.data.Datos;
                        if ($scope.CodigoCliente !== undefined && $scope.CodigoCliente !== '' && $scope.CodigoCliente !== 0 && $scope.CodigoCliente !== null) {
                            if ($scope.CodigoCliente > 0) {
                                $scope.ModeloCliente = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo ==' + $scope.CodigoCliente);
                            } else {
                                $scope.ModeloCliente = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.ModeloCliente = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo ==0');
                        }
                    }
                    else {
                        $scope.ListaClientes = [];
                        $scope.ModeloCliente = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.AutocompleteRemitentes = function (value) {
            var ListadoRemitentes = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, CadenaPerfiles: PERFIL_REMITENTE, Sync: true });
            if (ListadoRemitentes.ProcesoExitoso == true) {
                if (ListadoRemitentes.Datos.length > 0) {
                    $scope.ListaRemitentes = ListadoRemitentes.Datos;
                    return $scope.ListaRemitentes
                }
            }
        }

        //var ResponseListaRemitentes = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, CadenaPerfiles: PERFIL_REMITENTE, Sync: true });
        //if (ResponseListaRemitentes.ProcesoExitoso == true) {
        //    if (ResponseListaRemitentes.Datos.length > 0) {                
        //        $scope.ListaRemitentes = ResponseListaRemitentes.Datos;
        //        $scope.ListaRemitentes.push({ NombreCompleto: '', Codigo: 0 });
        //        $scope.ModeloRemitente = $linq.Enumerable().From($scope.ListaRemitentes).First('$.Codigo ==0');
        //    }
        //}

        var ResponseListaRemitentes = [];
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, CadenaPerfiles: PERFIL_REMITENTE }).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaRemitentes = response.data.Datos;
                        $scope.ListaRemitentes.push({ NombreCompleto: '', Codigo: 0 });
                        $scope.ModeloRemitente = $linq.Enumerable().From($scope.ListaRemitentes).First('$.Codigo ==0');
                    }
                }
            })

        /*Funciones validar codigo cliente y cargar direccion y telefonos*/
        $scope.VerificarCliente = function (cliente) {
            $scope.ValidarCliente = cliente.Codigo;
            if ($scope.ValidarCliente > 0) {
                TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, CadenaPerfiles: PERFIL_CLIENTE, Codigo: $scope.ValidarCliente }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {

                                item = response.data.Datos[0];

                                $scope.ModeloDireccion = item.Direccion
                                $scope.ModeloTelefonos = item.Telefonos
                            } else {
                                ShowError('El cliente no se encuentra registrado, por favor diligenciar los campos.')
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        /*Funciones validar codigo remitente*/
        $scope.VerificarRemitente = function (remitente) {
            $scope.ValidarRemitente = remitente.Codigo;
            if ($scope.ValidarCliente > 0) {
                TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, CadenaPerfiles: PERFIL_REMITENTE, Codigo: $scope.ValidarRemitente }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {

                                item = response.data.Datos[0];

                                $scope.ModeloDireccion = item.Direccion
                                $scope.ModeloTelefonos = item.Telefonos
                            } else {
                                ShowError('El remitente no se encuentra registrado, por favor diligenciar los campos.')
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando Recoleccion número ' + $scope.Numero);

            $timeout(function () {
                blockUI.message('Cargando Recoleccion número ' + $scope.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
            };

            blockUI.delay = 1000;
            RecoleccionesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.ModeloNumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.ModeloFecha = new Date(response.data.Datos.Fecha);
                        $scope.ModeloContacto = response.data.Datos.NombreContacto;
                        $scope.ModeloIdentificacionContacto = response.data.Datos.IdentificacionContacto;
                        $scope.ModeloBarrio = response.data.Datos.Barrio;
                        $scope.ModeloDireccion = response.data.Datos.Direccion;
                        $scope.ModeloTelefonos = response.data.Datos.Telefonos;
                        $scope.ModeloMercancia = response.data.Datos.Mercancia;
                        $scope.ModeloCantidad = response.data.Datos.Cantidad;
                        $scope.ModeloPeso = response.data.Datos.Peso;
                        $scope.ModeloObservaciones = response.data.Datos.Observaciones;
                        $scope.Latitud = response.data.Datos.Latitud;
                        $scope.Longitud = response.data.Datos.Longitud;
                        if (response.data.Datos.Estado == 1) {
                            $scope.Deshabilitar = false
                        } else {
                            $scope.Deshabilitar = true
                        }
                        if (new Date(response.data.Datos.FechaRecoleccion) > MIN_DATE) {
                            $scope.ModeloFechaRecoleccion = new Date(response.data.Datos.FechaRecoleccion);
                        }
                        $scope.CodigoCliente = response.data.Datos.Cliente.Codigo;
                        if ($scope.ListaClientes !== null && $scope.ListaClientes !== undefined) {
                            if ($scope.ListaClientes.length > 0) {
                                $scope.ModeloCliente = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo == ' + $scope.CodigoCliente);
                            }
                        };

                        if (response.data.Datos.Remitente != null && response.data.Datos.Remitente != undefined) {
                            if (response.data.Datos.Remitente.Codigo > 0) {
                                $scope.ModeloRemitente = $scope.CargarTercero(response.data.Datos.Remitente.Codigo);
                            }
                        }

                        $scope.CodigoZonas = response.data.Datos.Zonas.Codigo
                        if ($scope.ListadoZonas.length > 0 && $scope.CodigoZonas > 0) {
                            $scope.ModeloZonas = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo == ' + response.data.Datos.Zonas.Codigo);
                        }

                        $scope.CodigoCiudad = response.data.Datos.Ciudad.Codigo;
                        if ($scope.ListaCiudades !== null && $scope.ListaCiudades !== undefined) {
                            if ($scope.ListaCiudades.length > 0) {
                                $scope.ModeloCiudad = $linq.Enumerable().From($scope.ListaCiudades).First('$.Codigo == ' + $scope.CodigoCiudad);
                            }
                        };

                        $scope.CodigoUnidadEmpaque = response.data.Datos.CodigoUnidadEmpaque
                        if ($scope.ListadoUnidadEmpaque.length > 0 && $scope.CodigoUnidadEmpaque > 0) {
                            $scope.ModeloUnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo == ' + response.data.Datos.CodigoUnidadEmpaque);
                        }
                        $scope.MaskValores()
                        $scope.MaskValores();
                        $scope.MaskValoresInt();
                    }
                    else {
                        ShowError('No se logro consultar la Recoleccion número ' + $scope.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarRecolecciones';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarRecolecciones';
                });

            blockUI.stop();
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');

            //$scope.ModeloFecha = RetornarFechaEspecificaSinTimeZone($scope.ModeloFecha);

            $scope.DatosGuardar = {

                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                NumeroDocumento: $scope.ModeloNumeroDocumento,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_RECOLECCION,
                Fecha: $scope.ModeloFecha,
                Cliente: $scope.ModeloCliente,
                Remitente: $scope.ModeloRemitente,
                NombreContacto: $scope.ModeloContacto,
                IdentificacionContacto: $scope.ModeloIdentificacionContacto,
                Oficinas: $scope.Sesion.UsuarioAutenticado.Oficinas,
                Zonas: $scope.ModeloZonas,
                Ciudad: $scope.ModeloCiudad,
                Barrio: $scope.ModeloBarrio,
                Direccion: $scope.ModeloDireccion,
                Latitud: $scope.Latitud,
                Longitud: $scope.Longitud,
                Telefonos: $scope.ModeloTelefonos,
                Mercancia: $scope.ModeloMercancia,
                CodigoUnidadEmpaque: $scope.ModeloUnidadEmpaque.Codigo,
                Cantidad: $scope.ModeloCantidad,
                Peso: $scope.ModeloPeso,
                Observaciones: $scope.ModeloObservaciones,
                Estado: ESTADO_ACTIVO,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                FechaRecoleccion: $scope.ModeloFechaRecoleccion,
                Version1: ESTADO_ACTIVO
            };

            RecoleccionesFactory.Guardar($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Numero === 0) {
                                ShowSuccess('Se guardó la recolección ' + response.data.Datos);
                            }
                            else {
                                ShowSuccess('Se modificó la recolección ' + response.data.Datos);
                            }
                            location.href = '#!ConsultarRecolecciones/' + response.data.Datos;
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.ModeloFecha === undefined || $scope.ModeloFecha === '' || $scope.ModeloFecha === null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }
            if ($scope.ModeloFechaRecoleccion === undefined || $scope.ModeloFechaRecoleccion === '' || $scope.ModeloFechaRecoleccion === null) {
                $scope.MensajesError.push('Debe ingresar la fecha de recolección');
                continuar = false;
            } else {
                if ($scope.ModeloFechaRecoleccion < new Date()) {
                    $scope.MensajesError.push('La fecha de recoleccion no puede ser menor a la actual');
                    continuar = false;
                }
            }
            if (($scope.ModeloCliente === undefined || $scope.ModeloCliente.Codigo === 0 || $scope.ModeloCliente === null) && ($scope.ModeloRemitente === undefined || $scope.ModeloRemitente === '' || $scope.ModeloRemitente === null || $scope.ModeloRemitente.Codigo === 0) && ($scope.ModeloContacto === undefined || $scope.ModeloContacto === '' || $scope.ModeloContacto === null)) {
                $scope.MensajesError.push('Debe ingresar el nombre del cliente o remitente');
                continuar = false;
            }

            if (($scope.ModeloContacto === undefined || $scope.ModeloContacto === '' || $scope.ModeloContacto === null)) {
                $scope.MensajesError.push('Debe ingresar el nombre del contacto o remitente');
                continuar = false;
            }

            if (($scope.ModeloIdentificacionContacto === undefined || $scope.ModeloIdentificacionContacto === '' || $scope.ModeloIdentificacionContacto === null)) {
                $scope.MensajesError.push('Debe ingresar el número de identificación del contacto');
                continuar = false;
            }

            if ($scope.ModeloCiudad === undefined || $scope.ModeloCiudad.Codigo === 0 || $scope.ModeloCiudad === null) {
                $scope.MensajesError.push('Debe ingresar la ciudad');
                continuar = false;
            }
            //if ($scope.ModeloBarrio === undefined || $scope.ModeloBarrio === '' || $scope.ModeloBarrio === null) {
            //    $scope.MensajesError.push('Debe ingresar el barrio');
            //    continuar = false;
            //}
            if ($scope.ModeloDireccion === undefined || $scope.ModeloDireccion === '' || $scope.ModeloDireccion === null) {
                $scope.MensajesError.push('Debe ingresar la dirección');
                continuar = false;
            }
            if ($scope.ModeloTelefonos === undefined || $scope.ModeloTelefonos === '' || $scope.ModeloTelefonos === null) {
                $scope.MensajesError.push('Debe ingresar los teléfonos');
                continuar = false;
            }
            if ($scope.ModeloMercancia === undefined || $scope.ModeloMercancia === '' || $scope.ModeloMercancia === null) {
                $scope.MensajesError.push('Debe ingresar la mercancía');
                continuar = false;
            }
            if ($scope.ModeloUnidadEmpaque === undefined || $scope.ModeloUnidadEmpaque.Codigo === 0 || $scope.ModeloUnidadEmpaque === null) {
                $scope.MensajesError.push('Debe seleccionar la unidad de empaque');
                continuar = false;
            }
            if ($scope.ModeloCantidad === undefined || $scope.ModeloCantidad === 0 || $scope.ModeloCantidad === null) {
                $scope.MensajesError.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if ($scope.ListadoZonas.length > 0) {
                if ($scope.ModeloZonas === undefined || $scope.ModeloZonas === 0 || $scope.ModeloZonas === null) {
                    $scope.MensajesError.push('Debe ingresar la zona');
                    continuar = false;
                }
            } else {
                $scope.ModeloZonas = [{ Nombre: '', Codigo: 0 }]
            }

            return continuar;
        }

        //----------------------------------------------------------------------------------------------------------------------------------------
        if ($scope.Numero > 0) {
            $scope.Titulo = 'CONSULTAR RECOLECCIÓN'
            Obtener()
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarRecolecciones/' + $scope.ModeloNumeroDocumento;
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresInt = function (Value) {
            return MascaraValoresInt(Value)
        };
        $scope.MaskintValores = function (option) {
            MascaraValoresIntGeneral($scope)
        };
        $scope.MaskMayus = function (option) {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskDireccion = function (option) {
            try { $scope.ModeloDireccion = MascaraDireccion($scope.ModeloDireccion) } catch (e) { }
        };
        $scope.MaskTelefono = function (option) {
            try { $scope.ModeloTelefonos = MascaraTelefono($scope.ModeloTelefonos) } catch (e) { }
        };

    }]);