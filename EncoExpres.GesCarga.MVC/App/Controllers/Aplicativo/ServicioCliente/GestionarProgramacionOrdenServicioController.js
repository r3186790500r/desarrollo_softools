﻿EncoExpresApp.controller("GestionarProgramacionOrdenServicioCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'EncabezadoSolicitudOrdenServiciosFactory', 'VehiculosFactory', 'blockUIConfig', 'EncabezadoProgramacionOrdenServiciosFactory', 'TipoTarifaTransportesFactory', 'TarifarioVentasFactory', 'ProductoTransportadosFactory', 'TarifaTransportesFactory', 'RutasFactory', 'CiudadesFactory', 'SitiosTerceroClienteFactory', 'blockUIConfig', 'SeguridadUsuariosFactory', 'EmpresasFactory', 'AutorizacionesFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'SitiosCargueDescargueFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, EncabezadoSolicitudOrdenServiciosFactory, VehiculosFactory, blockUIConfig, EncabezadoProgramacionOrdenServiciosFactory, TipoTarifaTransportesFactory, TarifarioVentasFactory, ProductoTransportadosFactory, TarifaTransportesFactory, RutasFactory, CiudadesFactory, SitiosTerceroClienteFactory, blockUIConfig, SeguridadUsuariosFactory, EmpresasFactory, AutorizacionesFactory, ValorCatalogosFactory, TercerosFactory, SitiosCargueDescargueFactory) {

        $scope.MapaSitio = [{ Nombre: 'Servicio al Cliente' }, { Nombre: 'Procesos' }, { Nombre: 'Programar Orden Servicio' }];
        $scope.Titulo = "PROGRAMAR ORDEN SERVICIO";
        $scope.Master = '#!ConsultarProgramarOrdenServicio'
        $scope.MasterOrdenServicio = '#!ConsultarProgramarOrdenServicio'
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ValorDeclaradoExcedePoliza = false;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PROGRAMAR_ORDEN_SERVICIO); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar



        $scope.Modal = {}


        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            Numero: 0,
            Fecha: new Date(),
            OrdenServicio: {}
        }

        //-- Varibles Carga Listas
        //Variables documentos
        $scope.Numero = 0;
        $scope.CodigoDocumento = 0;
        $scope.TipoImagen = '';[]
        $scope.DetalleDocumentos = [];


        //----------------------------
        $scope.ListadoEstados = [
            { Nombre: "BORRADOR", Codigo: 0 },
            { Nombre: "DEFINITIVO", Codigo: 1 }
        ]

        $scope.Estado = $scope.ListadoEstados[0];

        $scope.ModificoOrdenServicio = 0;
        $scope.MostrarCamposModificacion = 0;

        $scope.ModificarEstado = function (item) {
            item.Editado = 1;
        }

        //--Informacion Requerida
        /*Cargar el combo de TarifaTransportes*/
        //TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        //    then(function (response) {
        //        if (response.data.ProcesoExitoso === true) {
        //            $scope.ListadoTarifaTransportes = [];
        //            if (response.data.Datos.length > 0) {
        //                $scope.ListadoTarifaTransportes = response.data.Datos;
        //            }
        //            else {
        //                $scope.ListadoTarifaTransportes = []
        //            }
        //        }
        //    }, function (response) {
        //    });
        //--Tarifa Transporte
        $scope.ListadoTarifaTransportes = [];
        var resTaTP = TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true });
        if (resTaTP.ProcesoExitoso) {
            $scope.ListadoTarifaTransportes = [];
            if (resTaTP.Datos.length > 0) {
                $scope.ListadoTarifaTransportes = resTaTP.Datos;
            }
            else {
                $scope.ListadoTarifaTransportes = []
            }
        }
        var restttc = TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true });
        if (restttc.ProcesoExitoso) {
            TipoTarifaCarga = true;
            $scope.ListadoTipoTarifaTransportes = [];
            if (restttc.Datos.length > 0) {
                $scope.ListadoTipoTarifaTransportes = restttc.Datos;
            }
            else {
                $scope.ListadoTipoTarifaTransportes = []
            }
        }
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoRutas = response.data.Datos
                }
            }, function (response) {
            });
        //-- Productos Transportados --//
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ProductoTranportadoCarga = true;
                    if (response.data.Datos.length > 0) {
                        $scope.ListaProductoTransportado = response.data.Datos;
                    }
                    else {
                        $scope.ListaProductoTransportado = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //--Informacion Requerida

        //Orden e servicio:
        //--Rutas
        $scope.ListadoAutocompleteRutas = [];
        var ResponseRutas = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
        var ResponseTarifarioVentas = [];
        var ResponseCliente = {};

        $scope.ValidaCupoSede = false
        $scope.ValidaCupoGeneral = false


        $scope.ListadoCupoSedes = []
        $scope.ListadoTipoDespacho = [];

        //--Tipo Despacho
        var ResponseTiposDespacho = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DESPACHO }, Sync: true }).Datos;
        ResponseTiposDespacho.forEach(item => {
            if (item.Codigo != 18201/*Viaje*/) {
                $scope.ListadoTipoDespacho.push(item);
            }
        })


        $scope.CambioTipoDespachos = function () {
            if ($scope.Modelo.OrdenServicio.TipoDespacho.Codigo == 18202) {//Asigna Tarifa predeterminada cuando es cupo/tonelada
                $scope.Modelo.OrdenServicio.Tarifa = { Codigo: 301 };
                try {
                    if ($scope.ListadoTarifaTarifario.length > 0) {
                        $scope.Modelo.OrdenServicio.Tarifa = $linq.Enumerable().From($scope.ListadoTarifaTarifario).First('$.Codigo == ' + $scope.Modelo.OrdenServicio.Tarifa.Codigo);
                    }
                    $scope.CargarListadoTarifas();
                } catch (e) {

                }
                $scope.TipoDespachoCantidadViajes = false
            }
            else {
                $scope.Modelo.Tarifa = undefined;
                $scope.CargarListadoTarifas();
                $scope.TipoDespachoCantidadViajes = true
            }
        };

        //Fin Tipo Despacho

        $scope.ListadoCupoSedes = ResponseCliente.TerceroClienteCupoSedes
        function CargarTarifarioOrdenServicio() {
            if ($scope.Modelo.OrdenServicio.Cliente != undefined) {

                ResponseTarifarioVentas = TarifarioVentasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoCliente: $scope.Modelo.OrdenServicio.Cliente.Codigo, TipoConsulta: 1, Sync: true }).Datos;
                ResponseCliente = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.OrdenServicio.Cliente.Codigo, Sync: true }).Datos;
                $scope.ValoresCargueDescargue = ResponseCliente.SitiosTerceroCliente
                $scope.SaldoCliente = ResponseCliente.Saldo
                if (ResponseCliente.TipoValidacionCupo.Codigo == 19203) {
                    $scope.ValidaCupoSede = true
                }
                if (ResponseCliente.TipoValidacionCupo.Codigo == 19202) {
                    $scope.ValidaCupoGeneral = true
                }
            }
        }
        function CargarRutasTarifario() {


            if ($scope.Modelo.OrdenServicio.Cliente != undefined) {

                if (ResponseTarifarioVentas.Tarifas.length > 0) {
                    ResponseTarifarioVentas.Tarifas.forEach(tarifa => {
                        if (tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.OrdenServicio.LineaNegocioCarga.Codigo
                            && tarifa.TipoLineaNegocioTransportes.Codigo == $scope.Modelo.OrdenServicio.TipoLineaNegocioCarga.Codigo
                        ) {
                            ResponseRutas.forEach(ruta => {
                                if (ruta.Codigo == tarifa.Ruta.Codigo) {
                                    if ($scope.ListadoAutocompleteRutas.length > 0) {
                                        var cont = 0
                                        $scope.ListadoAutocompleteRutas.forEach(rutalistado => {

                                            if (rutalistado.Codigo == tarifa.Ruta.Codigo) {
                                                cont++;
                                            }

                                        });
                                        if (cont == 0) {
                                            $scope.ListadoAutocompleteRutas.push(ruta)
                                        }
                                    } else {
                                        $scope.ListadoAutocompleteRutas.push(ruta)
                                    }
                                }
                            });

                        }
                    });
                }
            }
        }
        $scope.AutocompleteRutas = function () {

            return $scope.ListadoAutocompleteRutas;
        }

     

        $scope.CargarTarifasViajeMaster = function (Ruta) {
            $scope.ListadoTarifaTarifario = [];
            if (Ruta !== undefined && Ruta !== "" && Ruta !== null) {
                ResponseTarifarioVentas.Tarifas.forEach(function (item) {
                    //Asignar lineas de negocio apartir del tarifario
                    if ($scope.ListadoTarifaTarifario.length == 0) {
                        $scope.ListadoTarifaTransportes.forEach(function (itemTarifa) {
                            if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo && $scope.Modelo.OrdenServicio.TipoLineaNegocioCarga.Codigo == item.TipoTarifaTransportes.TarifaTransporte.TipoLineaNegocioTransporte.Codigo) {
                                if ($scope.ListadoTarifaTarifario.length == 0) {
                                    $scope.ListadoTarifaTarifario.push(itemTarifa)
                                } else {
                                    var contarifa = 0;
                                    $scope.ListadoTarifaTarifario.forEach(tarifa => {
                                        if (tarifa.Codigo == itemTarifa.Codigo) {
                                            contarifa++;
                                        }
                                    });

                                    if (contarifa == 0) {
                                        $scope.ListadoTarifaTarifario.push(itemTarifa)
                                    }
                                }
                            }
                        })
                    }
                    else {
                        var con = 0
                        $scope.ListadoTarifaTarifario.forEach(function (itemTarifa) {
                            if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                con++
                            }
                        })
                        if (con == 0) {
                            var con1 = 0
                            $scope.ListadoTarifaTransportes.forEach(function (itemTarifa) {
                                if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo) {
                                    $scope.ListadoTarifaTarifario.forEach(function (itemDetalle) {
                                        if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemDetalle.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                            con1++
                                        }
                                    })
                                    if (con1 == 0) {
                                        if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                            if ($scope.ListadoTarifaTarifario.length == 0) {
                                                $scope.ListadoTarifaTarifario.push(itemTarifa)
                                            } else {
                                                var contarifa = 0;
                                                $scope.ListadoTarifaTarifario.forEach(tarifa => {
                                                    if (tarifa.Codigo == itemTarifa.Codigo) {
                                                        contarifa++;
                                                    }
                                                });

                                                if (contarifa == 0) {
                                                    $scope.ListadoTarifaTarifario.push(itemTarifa)
                                                }
                                            }
                                        }
                                    }
                                }
                            })
                        }
                    }
                    try {
                        if ($scope.Modelo.Ruta.Codigo > 0) {
                            $scope.Modelo.Tarifa = $linq.Enumerable().From($scope.ListadoTarifaTarifario).First('$.Codigo == ' + $scope.Modelo.Tarifa.Codigo);
                        }
                    } catch (e) {

                    }
                });


                $scope.Modal.ModificarCiudadDestino = $scope.CargarCiudad(Ruta.CiudadDestino.Codigo)

                if ($scope.ListadoTarifaTarifario.length > 0) {
                    $scope.DeshabilitarTarifas = false
                }
                else {
                    $scope.DeshabilitarTarifas = true
                }

                if ($scope.Modelo.OrdenServicio.SitioCargue != undefined) {
                    if ($scope.Modelo.OrdenServicio.Ruta.CiudadOrigen.Codigo != $scope.Modelo.OrdenServicio.SitioCargue.Ciudad.Codigo) {
                        $scope.Modelo.OrdenServicio.SitioCargue = '';
                    }
                }

                if ($scope.Modelo.OrdenServicio.SitioDescargue != undefined) {
                    if ($scope.Modelo.OrdenServicio.Ruta.CiudadDestino.Codigo != $scope.Modelo.OrdenServicio.SitioDescargue.Ciudad.Codigo) {
                        $scope.Modelo.OrdenServicio.SitioDescargue = '';
                    }
                }
            }
        }

        //-- Consultar tarifas del cliente --//
        $scope.ValidarSaldoCliente = function () {
            var continuar = true


            var valorFlete = 0
            var valorCargue = 0
            var valorDescargue = 0
            if ($scope.Modelo.OrdenServicio.TipoDespacho.Codigo == 18202) {
                if ($scope.Modelo.OrdenServicio.Ruta !== undefined) {
                    if (ResponseTarifarioVentas.Tarifas.length > 0) {
                        var encontrotarifa = false
                        for (var i = 0; i < ResponseTarifarioVentas.Tarifas.length; i++) {
                            var tarifa = ResponseTarifarioVentas.Tarifas[i]
                            if (
                                tarifa.TipoLineaNegocioTransportes.Codigo == $scope.Modelo.OrdenServicio.TipoLineaNegocioCarga.Codigo &&
                                tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.OrdenServicio.LineaNegocioCarga.Codigo &&
                                tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 301 && //Peso Tonelada
                                tarifa.Ruta.Codigo == $scope.Modelo.OrdenServicio.Ruta.Codigo
                            ) {
                                encontrotarifa = true
                                valorFlete = tarifa.ValorFlete
                                $scope.Modelo.OrdenServicio.ValorFleteCliente = tarifa.ValorFlete
                                $scope.Modelo.OrdenServicio.ValorFleteTrnasportador = tarifa.ValorFleteTransportador
                                break;
                            }
                        }
                        if (encontrotarifa) {
                            if ($scope.ValoresCargueDescargue.length > 0) {
                                for (var i = 0; i < $scope.ValoresCargueDescargue.length; i++) {
                                    if ($scope.ValoresCargueDescargue[i].SitioCliente.Codigo == $scope.Modelo.OrdenServicio.SitioCargue.SitioCliente.Codigo) {
                                        valorCargue = $scope.ValoresCargueDescargue[i].ValorCargueCliente
                                    }
                                    if ($scope.ValoresCargueDescargue[i].SitioCliente.Codigo == $scope.Modelo.OrdenServicio.SitioDescargue.SitioCliente.Codigo) {
                                        valorDescargue = $scope.ValoresCargueDescargue[i].ValorDescargueCliente
                                    }
                                }
                            }
                            valorFlete = valorFlete + valorCargue + valorDescargue
                            ValorTotal = valorFlete * (parseInt(MascaraNumero($scope.Modelo.OrdenServicio.ValorTipoDespacho)) / 1000)
                            if ($scope.ValidaCupoGeneral) {
                                if (ValorTotal > $scope.SaldoCliente) {
                                    ShowError('El valor del cupo que esta intentado ingresar excede el saldo del cliente /n Saldo disponible $' + MascaraValores($scope.SaldoCliente) + ' Valor del despacho $' + MascaraValores(ValorTotal))
                                    $scope.Modelo.OrdenServicio.ValorTipoDespacho = 0
                                    $scope.Modelo.OrdenServicio.SaldoTipoDespacho = 0
                                    //$('#SaldoOrden').focus()
                                    continuar = false
                                }
                            }
                            if ($scope.ValidaCupoSede) {
                                if ($scope.ListadoCupoSedes.length > 0) {
                                    for (var i = 0; i < $scope.ListadoCupoSedes.length; i++) {
                                        if ($scope.ListadoCupoSedes[i].Codigo == $scope.Modelo.SedeFacturacion.Codigo) {
                                            if (ValorTotal > $scope.ListadoCupoSedes[i].Saldo) {
                                                ShowError('El valor del cupo que esta intentado ingresar excede el saldo de la sede del cliente /n Saldo disponible $' + MascaraValores($scope.ListadoCupoSedes[i].Saldo) + ' Valor del despacho $' + MascaraValores(ValorTotal))
                                                $scope.Modelo.OrdenServicio.ValorTipoDespacho = 0
                                                $scope.Modelo.OrdenServicio.SaldoTipoDespacho = 0
                                                //$('#SaldoOrden').focus()
                                                continuar = false
                                            }
                                        }
                                        break;
                                    }
                                } else {
                                    ShowError('No se encontro una parametrización valida del saldo de la sede seleccionada del cliente')
                                }
                            }
                        } else {
                            ShowError('No se encontraron tarifas con las condiciones especificadas para validar el saldo del cliente')
                            continuar = false
                        }
                    }
                }
            }

            return continuar
        }

        $scope.ListadoSitiosCargueAlterno2 = [];
        $scope.AutocompleteSitiosClienteCargue = function (value) {
            $scope.ListadoSitiosCargueAlterno2 = [];
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: $scope.Modelo.OrdenServicio.Cliente.Codigo },
                        CiudadCargue: { Codigo: $scope.Modelo.OrdenServicio.Ruta.CiudadOrigen.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    }
                    $scope.ListadoSitiosCargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosCargueAlterno2)
                }
            }
            return $scope.ListadoSitiosCargueAlterno2
        }

        $scope.ListadoSitiosDescargue = [];
        $scope.AutocompleteSitiosClienteDescargueMaster = function (value) {
            $scope.ListadoSitiosDescargue = [];
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: $scope.Modelo.OrdenServicio.Cliente.Codigo },
                        CiudadCargue: { Codigo: $scope.Modelo.OrdenServicio.Ruta.CiudadDestino.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    }
                    $scope.ListadoSitiosDescargue = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosDescargue)
                }
            }
            return $scope.ListadoSitiosDescargue
        }

        //Fin Orden de Servicio


        $scope.ListadoTipoVehiculo = [];
        //--Tipo Vehiculo
        var resTive = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO },
            Estado: 1,
            Sync: true
        });
        if (resTive.ProcesoExitoso) {
            $scope.ListadoTipoVehiculo = [];
            if (resTive.Datos.length > 0) {
                for (var i = 0; i < resTive.Datos; i++) {
                    if (resTive.Datos[i].Codigo !== 2200) {
                        $scope.ListadoTipoVehiculo.push(resTive.Datos[i])
                    }
                }
                $scope.ListadoTipoVehiculo = resTive.Datos;
                if ($scope.CodigoTipoVehiculo > 0) {
                    $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == ' + $scope.CodigoTipoVehiculo);
                }
            }
            else {
                $scope.ListadoTipoVehiculo = [];
            }
        }

        /* Obtener parametros*/
        if (parseInt($routeParams.Numero) > CERO) {
            $scope.Modelo.Numero = $routeParams.Numero;
            Obtener();
            //$scope.Titulo = 'CONSULTAR SOLICITUD ORDEN SERVICIO';

            //ValCargaFactorys();
        } else {
            $scope.DeshabilitarLineaNegocio = false
        }

        $scope.AsignarObservaciones = function (obs) {
            $scope.ListadoVehiculos.forEach(item => {
                item.Data.forEach(item2 => {
                    if (item2.Observaciones == undefined || item2.Observaciones == null || item2.Observaciones == '') {
                        item2.Observaciones = obs;
                    }
                })
            });
        }


        $scope.CargarListadoTarifas = function () {
            //Cantidad Viajes
            if ($scope.Modelo.OrdenServicio.TipoDespacho.Codigo == 18203) {
                $scope.ListadoTarifas = []
                $scope.TipoDespachoCantidadViajes = true
                $scope.TipoDespachoCupoTonelada = false
                TarifarioVentasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoCliente: $scope.Modelo.OrdenServicio.Cliente.Codigo, TipoConsulta: 1 }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.TarifarioVentas = response.data.Datos;
                            console.log(response.data.Datos);
                            if ($scope.TarifarioVentas.Tarifas.length > 0) {
                                for (var i = 0; i < $scope.TarifarioVentas.Tarifas.length; i++) {
                                    var tarifa = $scope.TarifarioVentas.Tarifas[i]
                                    if ($scope.DiferentesSitiosDescargue) {
                                        if (tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.OrdenServicio.LineaNegocioCarga.Codigo
                                            && tarifa.TipoLineaNegocioTransportes.Codigo == $scope.Modelo.OrdenServicio.TipoLineaNegocioCarga.Codigo
                                            //&& tarifa.Ruta.Codigo == $scope.Modelo.OrdenServicio.Ruta.Codigo
                                            //&& tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == $scope.Modelo.OrdenServicio.Tarifa.Codigo
                                        ) {
                                            $scope.ListadoTarifas.push(tarifa)

                                            if ($scope.ListadoRutasTarifario.length == 0) {
                                                $scope.ListadoRutasTarifario.push($linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + tarifa.Ruta.Codigo))
                                            } else {
                                                var cont = 0
                                                for (var K = 0; K < $scope.ListadoRutasTarifario.length; K++) {
                                                    if (tarifa.Ruta.Codigo == $scope.ListadoRutasTarifario[K].Codigo) {
                                                        cont++
                                                    }
                                                }
                                                if (cont == 0) {
                                                    $scope.ListadoRutasTarifario.push($linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + tarifa.Ruta.Codigo))
                                                }
                                            }
                                            //if (tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 6) {
                                            //    for (var j = 0; j < $scope.ListaProductoTransportado.length; j++) {
                                            //        if (tarifa.TipoTarifaTransportes.Codigo == $scope.ListaProductoTransportado[j].Codigo) {
                                            //            tarifa.TipoTarifaTransportes.Nombre = $scope.ListaProductoTransportado[j].Nombre
                                            //            tarifa.TipoTarifaTransportes.ValorFlete = tarifa.ValorFlete
                                            //            tarifa.TipoTarifaTransportes.ValorFleteTransportador = tarifa.ValorFleteTransportador
                                            //            break;
                                            //        }
                                            //    }
                                            //} else {

                                            //    for (var j = 0; j < $scope.ListadoTipoTarifaTransportes.length; j++) {
                                            //        if (tarifa.TipoTarifaTransportes.Codigo == $scope.ListadoTipoTarifaTransportes[j].Codigo) {
                                            //            tarifa.TipoTarifaTransportes.Nombre = $scope.ListadoTipoTarifaTransportes[j].Nombre
                                            //            tarifa.TipoTarifaTransportes.ValorFlete = tarifa.ValorFlete
                                            //            tarifa.TipoTarifaTransportes.ValorFleteTransportador = tarifa.ValorFleteTransportador
                                            //            break;
                                            //        }
                                            //    }
                                            //}
                                            //$scope.ListadoTarifas.push(tarifa.TipoTarifaTransportes)
                                        }
                                    } else {
                                        if (tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.OrdenServicio.LineaNegocioCarga.Codigo
                                            && tarifa.TipoLineaNegocioTransportes.Codigo == $scope.Modelo.OrdenServicio.TipoLineaNegocioCarga.Codigo
                                            && tarifa.Ruta.Codigo == $scope.Modelo.OrdenServicio.Ruta.Codigo
                                            && tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == $scope.Modelo.OrdenServicio.Tarifa.Codigo
                                        ) {
                                            if (tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 6) {
                                                for (var j = 0; j < $scope.ListaProductoTransportado.length; j++) {
                                                    if (tarifa.TipoTarifaTransportes.Codigo == $scope.ListaProductoTransportado[j].Codigo) {
                                                        tarifa.TipoTarifaTransportes.Nombre = $scope.ListaProductoTransportado[j].Nombre;
                                                        tarifa.TipoTarifaTransportes.ValorFlete = tarifa.ValorFlete;
                                                        tarifa.TipoTarifaTransportes.ValorFleteTransportador = tarifa.ValorFleteTransportador;
                                                        tarifa.TipoTarifaTransportes.TarifaTransporte.ValorMaximo = tarifa.TipoTarifaTransportes.TarifaTransporte.ValorMaximo;
                                                        break;
                                                    }
                                                }
                                            } else {

                                                for (var j = 0; j < $scope.ListadoTipoTarifaTransportes.length; j++) {
                                                    if (tarifa.TipoTarifaTransportes.Codigo == $scope.ListadoTipoTarifaTransportes[j].Codigo) {
                                                        tarifa.TipoTarifaTransportes.Nombre = $scope.ListadoTipoTarifaTransportes[j].Nombre
                                                        tarifa.TipoTarifaTransportes.ValorFlete = tarifa.ValorFlete
                                                        tarifa.TipoTarifaTransportes.ValorFleteTransportador = tarifa.ValorFleteTransportador
                                                        tarifa.TipoTarifaTransportes.TarifaTransporte.ValorMaximo = tarifa.TipoTarifaTransportes.TarifaTransporte.ValorMaximo;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (tarifa.TipoTarifaTransportes.Codigo > 0) {
                                                $scope.ListadoTarifas.push(tarifa.TipoTarifaTransportes)
                                                $scope.DeshabilitarActualizar = false
                                            }
                                        }
                                    }

                                }
                                if ($scope.ListadoTarifas.length == 0) {
                                    $scope.DeshabilitarActualizar = true
                                    ShowError('No se encontraron tarifas vigentes o disponibles, por favor revise la parametrización del tarifario del cliente')
                                }
                            } else {
                                $scope.DeshabilitarActualizar = true
                                ShowError('No se encontraron tarifarios vigentes o disponibles, por favor revise la parametrizacion')
                            }
                        }
                        else {
                            $scope.DeshabilitarActualizar = true
                            ShowError('No se encontraron tarifarios vigentes o disponibles, por favor revise la parametrizacion')
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            //Cupo Tonelada
            else if ($scope.Modelo.OrdenServicio.TipoDespacho.Codigo == 18202) {
                $scope.TipoDespachoCantidadViajes = false
                $scope.TipoDespachoCupoTonelada = true
                if ($scope.DiferentesSitiosDescargue) {
                    $scope.ListadoTarifas = []
                    TarifarioVentasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoCliente: $scope.Modelo.OrdenServicio.Cliente.Codigo }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.TarifarioVentas = response.data.Datos;
                                if ($scope.TarifarioVentas.Tarifas.length > 0) {
                                    for (var i = 0; i < $scope.TarifarioVentas.Tarifas.length; i++) {
                                        var tarifa = $scope.TarifarioVentas.Tarifas[i]
                                        if (tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.OrdenServicio.LineaNegocioCarga.Codigo
                                            && tarifa.TipoLineaNegocioTransportes.Codigo == $scope.Modelo.OrdenServicio.TipoLineaNegocioCarga.Codigo
                                            && tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 301
                                        ) {
                                            if ($scope.ListadoRutasTarifario.length == 0) {
                                                $scope.ListadoRutasTarifario.push($linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + tarifa.Ruta.Codigo))
                                            } else {
                                                var cont = 0
                                                for (var K = 0; K < $scope.ListadoRutasTarifario.length; K++) {
                                                    if (tarifa.Ruta.Codigo == $scope.ListadoRutasTarifario[K].Codigo) {
                                                        cont++
                                                    }
                                                }
                                                if (cont == 0) {
                                                    $scope.ListadoRutasTarifario.push($linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + tarifa.Ruta.Codigo))
                                                }
                                            }
                                            $scope.ListadoTarifas.push(tarifa)
                                            $scope.DeshabilitarActualizar = false;
                                        }
                                    }
                                    if ($scope.ListadoTarifas.length == 0) {
                                        $scope.DeshabilitarActualizar = true
                                        ShowError('No se encontraron tarifas vigentes o disponibles, por favor revise la parametrización del tarifario del cliente')
                                    }
                                } else {
                                    $scope.DeshabilitarActualizar = true
                                    ShowError('No se encontraron tarifarios vigentes o disponibles, por favor revise la parametrizacion')
                                }
                            }
                            else {

                                $scope.DeshabilitarActualizar = true
                                ShowError('No se encontraron tarifarios vigentes o disponibles, por favor revise la parametrizacion')
                            }
                        });
                }
            }
            $scope.CargarTarifasViaje($scope.Modelo.OrdenServicio.Ruta.Codigo);
        }

        $scope.CargarTarifasViaje = function (CodigoRuta) {
            $scope.ListadoTarifasRuta = []
            if (CodigoRuta > 0) {
                if ($scope.TipoDespachoCupoTonelada) {
                    for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                        if ($scope.ListadoTarifas[i].Ruta.Codigo == CodigoRuta && $scope.ListadoTarifas[i].TipoTarifaTransportes.TarifaTransporte.Codigo == 301) {
                            $scope.Modal.FleteCliente = MascaraValores($scope.ListadoTarifas[i].ValorFlete);
                            $scope.Modal.FleteTransportador = MascaraValores($scope.ListadoTarifas[i].ValorFleteTransportador)
                            $scope.Modal.Tarifa = $scope.ListadoTarifas[i].TipoTarifaTransportes.TarifaTransporte;
                            break;
                        }
                    }
                } else {
                    for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                        if ($scope.ListadoTarifas[i].Ruta.Codigo == CodigoRuta) {
                            $scope.ListadoTarifasRuta.push($linq.Enumerable().From($scope.ListadoTarifaTransportes).First('$.Codigo ==' + $scope.ListadoTarifas[i].TipoTarifaTransportes.TarifaTransporte.Codigo))
                        }
                    }
                    $scope.Modal.Tarifa = undefined
                    $scope.Modal.TipoTarifa = undefined
                    $scope.Modal.FleteCliente = 0
                    $scope.Modal.FleteTransportador = 0
                }

            }
        }
        $scope.CargarTipoTarifasViaje = function (CodigoRuta, CodigoTarifa) {
            $scope.ListadoTipoTarifasRuta = []
            if (CodigoRuta > 0 && CodigoTarifa > 0) {
                for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                    if ($scope.ListadoTarifas[i].Ruta.Codigo == CodigoRuta && $scope.ListadoTarifas[i].TipoTarifaTransportes.TarifaTransporte.Codigo == CodigoTarifa) {
                        if (CodigoTarifa == 5 || CodigoTarifa == 6 || CodigoTarifa == 300 || CodigoTarifa == 302) {
                            $scope.ListadoTipoTarifasRuta.push($linq.Enumerable().From($scope.ListaProductoTransportado).First('$.Codigo ==' + $scope.ListadoTarifas[i].TipoTarifaTransportes.Codigo))
                        } else {
                            $scope.ListadoTipoTarifasRuta.push($linq.Enumerable().From($scope.ListadoTipoTarifaTransportes).First('$.Codigo ==' + $scope.ListadoTarifas[i].TipoTarifaTransportes.Codigo))
                        }
                    }
                }
                $scope.Modal.TipoTarifa = undefined
                $scope.Modal.FleteCliente = 0
                $scope.Modal.FleteTransportador = 0
            }
            if ($scope.Modelo.OrdenServicio.TipoDespacho.Codigo == 18203) {
                $scope.TipoDespachoCantidadViajes = true
            } else {
                $scope.TipoDespachoCantidadViajes = false;
            }

        }

        $scope.CambiarTipoTarifa = function (item) {
            $scope.Modal.FleteCliente = $scope.MaskValoresGrid(item.ValorFlete);
            if ($scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta == false) {
                $scope.Modal.FleteTransportador = 0;
            }
            else {
                $scope.Modal.FleteTransportador = $scope.MaskValoresGrid(item.ValorFleteTransportador);
            }
        };

        $scope.AsignarValoresFlete = function (CodigoRuta, CodigoTarifa, CodigoTipoTarifa) {
            $scope.Modal.FleteCliente = 0
            $scope.Modal.FleteTransportador = 0
            if (CodigoRuta > 0 && CodigoTarifa > 0 && CodigoTipoTarifa) {
                for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                    if ($scope.ListadoTarifas[i].Ruta.Codigo == CodigoRuta && $scope.ListadoTarifas[i].TipoTarifaTransportes.TarifaTransporte.Codigo == CodigoTarifa && $scope.ListadoTarifas[i].TipoTarifaTransportes.Codigo == CodigoTipoTarifa) {
                        $scope.Modal.FleteCliente = MascaraValores($scope.ListadoTarifas[i].ValorFlete);
                        $scope.Modal.FleteTransportador = MascaraValores($scope.ListadoTarifas[i].ValorFleteTransportador);
                    }
                }
            }
        }
        
        $scope.ObtenerOrdenSerivicio = function (Numero, Codigo) {
            if ((Numero !== undefined && Numero > 0 && Numero !== '' && Numero !== null) || (Codigo !== undefined && Codigo > 0 && Codigo !== '' && Codigo !== null)) {
                $scope.ListadoRutasTarifario = []
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: Numero,
                    Numero: Codigo,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                    Estado: ESTADO_DEFINITIVO,
                    Sync: true
                };
                var response = EncabezadoSolicitudOrdenServiciosFactory.ObtenerOrdenServicioProgramacion(filtros);
                if (response.ProcesoExitoso) {
                    if (response.Datos.Numero > 0) {
                        if (response.Datos.NumeroProgramacion > 0 && !($scope.Modelo.Numero > 0)) {
                            ShowError("La orden de servicio ingresada ya se encuentra programada")
                            $scope.Modelo.OrdenServicio.NumeroDocumento = ''
                        }
                        else {
                            $scope.Modelo.OrdenServicio = response.Datos;
                            if ($scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta == false) {
                                $scope.Modelo.OrdenServicio.ValorFleteTrnasportador = 0;
                            }

                            if ($scope.Modelo.OrdenServicio.TipoDespacho.Codigo == 18203 || $scope.Modelo.OrdenServicio.TipoDespacho.Codigo == 18202) {
                                $scope.Modelo.OrdenServicio.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == ' + $scope.Modelo.OrdenServicio.TipoVehiculo.Codigo);
                                var ResponseRuta = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.OrdenServicio.Ruta.Codigo, Sync: true }).Datos;
                                $scope.Modelo.OrdenServicio.TipoDespacho = $linq.Enumerable().From($scope.ListadoTipoDespacho).First('$.Codigo == ' + $scope.Modelo.OrdenServicio.TipoDespacho.Codigo);
                                $scope.Modelo.OrdenServicio.Ruta.CiudadOrigen = ResponseRuta.CiudadOrigen
                                $scope.Modelo.OrdenServicio.Ruta.CiudadDestino = ResponseRuta.CiudadDestino

                                $scope.Modelo.OrdenServicio.SitioCargue.Ciudad = SitiosCargueDescargueFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.OrdenServicio.SitioCargue.SitioCliente.Codigo, Sync: true }).Datos.Ciudad;
                                $scope.Modelo.OrdenServicio.SitioDescargue.Ciudad = SitiosCargueDescargueFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.OrdenServicio.SitioDescargue.SitioCliente.Codigo, Sync: true }).Datos.Ciudad;
                                CargarTarifarioOrdenServicio();
                                CargarRutasTarifario();
                                $scope.CargarTarifasViajeMaster($scope.Modelo.OrdenServicio.Ruta);
                                $scope.Modelo.OrdenServicio.Tarifa = ($scope.Modelo.OrdenServicio.Tarifa == undefined || $scope.Modelo.OrdenServicio.Tarifa == null || $scope.Modelo.OrdenServicio.Tarifa == '') || $scope.Modelo.OrdenServicio.Tarifa.Codigo == 0 ? $linq.Enumerable().From($scope.ListadoTarifaTarifario).First('$.Codigo ==' + $scope.ListadoTarifaTarifario[0].Codigo) : $linq.Enumerable().From($scope.ListadoTarifaTarifario).First('$.Codigo ==' + $scope.Modelo.OrdenServicio.Tarifa.Codigo)
                                $scope.Modelo.OrdenServicio.Producto = $scope.CargarProducto(response.Datos.Producto.Codigo);
                                if ($scope.Modelo.OrdenServicio.DiferentesSitiosDescargue > 0) {
                                    $scope.DiferentesSitiosDescargue = true
                                } else {
                                    $scope.DiferentesSitiosDescargue = false
                                }
                                //Cantidad Viajes
                                if ($scope.Modelo.OrdenServicio.TipoDespacho.Codigo == 18203) {
                                    $scope.ListadoTarifas = []
                                    $scope.TipoDespachoCantidadViajes = true
                                    $scope.TipoDespachoCupoTonelada = false
                                    var responseTarifario = TarifarioVentasFactory.Obtener({
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        CodigoCliente: $scope.Modelo.OrdenServicio.Cliente.Codigo,
                                        TipoConsulta: 1,
                                        Sync: true
                                    });
                                    if (responseTarifario.ProcesoExitoso) {
                                        $scope.TarifarioVentas = responseTarifario.Datos;
                                        if ($scope.TarifarioVentas.Tarifas.length > 0) {
                                            for (var i = 0; i < $scope.TarifarioVentas.Tarifas.length; i++) {
                                                var tarifa = $scope.TarifarioVentas.Tarifas[i]
                                                if ($scope.DiferentesSitiosDescargue) {
                                                    if (tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.OrdenServicio.LineaNegocioCarga.Codigo
                                                        && tarifa.TipoLineaNegocioTransportes.Codigo == $scope.Modelo.OrdenServicio.TipoLineaNegocioCarga.Codigo
                                                        //&& tarifa.Ruta.Codigo == $scope.Modelo.OrdenServicio.Ruta.Codigo
                                                        //&& tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == $scope.Modelo.OrdenServicio.Tarifa.Codigo
                                                    ) {
                                                        $scope.ListadoTarifas.push(tarifa)

                                                        if ($scope.ListadoRutasTarifario.length == 0) {
                                                            $scope.ListadoRutasTarifario.push($linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + tarifa.Ruta.Codigo))
                                                        } else {
                                                            var cont = 0
                                                            for (var K = 0; K < $scope.ListadoRutasTarifario.length; K++) {
                                                                if (tarifa.Ruta.Codigo == $scope.ListadoRutasTarifario[K].Codigo) {
                                                                    cont++
                                                                }
                                                            }
                                                            if (cont == 0) {
                                                                $scope.ListadoRutasTarifario.push($linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + tarifa.Ruta.Codigo))
                                                            }
                                                        }
                                                        //if (tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 6) {
                                                        //    for (var j = 0; j < $scope.ListaProductoTransportado.length; j++) {
                                                        //        if (tarifa.TipoTarifaTransportes.Codigo == $scope.ListaProductoTransportado[j].Codigo) {
                                                        //            tarifa.TipoTarifaTransportes.Nombre = $scope.ListaProductoTransportado[j].Nombre
                                                        //            tarifa.TipoTarifaTransportes.ValorFlete = tarifa.ValorFlete
                                                        //            tarifa.TipoTarifaTransportes.ValorFleteTransportador = tarifa.ValorFleteTransportador
                                                        //            break;
                                                        //        }
                                                        //    }
                                                        //} else {

                                                        //    for (var j = 0; j < $scope.ListadoTipoTarifaTransportes.length; j++) {
                                                        //        if (tarifa.TipoTarifaTransportes.Codigo == $scope.ListadoTipoTarifaTransportes[j].Codigo) {
                                                        //            tarifa.TipoTarifaTransportes.Nombre = $scope.ListadoTipoTarifaTransportes[j].Nombre
                                                        //            tarifa.TipoTarifaTransportes.ValorFlete = tarifa.ValorFlete
                                                        //            tarifa.TipoTarifaTransportes.ValorFleteTransportador = tarifa.ValorFleteTransportador
                                                        //            break;
                                                        //        }
                                                        //    }
                                                        //}
                                                        //$scope.ListadoTarifas.push(tarifa.TipoTarifaTransportes)
                                                    }
                                                } else {
                                                    if (tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.OrdenServicio.LineaNegocioCarga.Codigo
                                                        && tarifa.TipoLineaNegocioTransportes.Codigo == $scope.Modelo.OrdenServicio.TipoLineaNegocioCarga.Codigo
                                                        && tarifa.Ruta.Codigo == $scope.Modelo.OrdenServicio.Ruta.Codigo
                                                        && tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == $scope.Modelo.OrdenServicio.Tarifa.Codigo
                                                    ) {
                                                        if (tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 6) {
                                                            for (var j = 0; j < $scope.ListaProductoTransportado.length; j++) {
                                                                if (tarifa.TipoTarifaTransportes.Codigo == $scope.ListaProductoTransportado[j].Codigo) {
                                                                    tarifa.TipoTarifaTransportes.Nombre = $scope.ListaProductoTransportado[j].Nombre;
                                                                    tarifa.TipoTarifaTransportes.ValorFlete = tarifa.ValorFlete;
                                                                    tarifa.TipoTarifaTransportes.ValorFleteTransportador = tarifa.ValorFleteTransportador;
                                                                    tarifa.TipoTarifaTransportes.TarifaTransporte.ValorMaximo = tarifa.TipoTarifaTransportes.TarifaTransporte.ValorMaximo;
                                                                    break;
                                                                }
                                                            }
                                                        } else {

                                                            for (var j = 0; j < $scope.ListadoTipoTarifaTransportes.length; j++) {
                                                                if (tarifa.TipoTarifaTransportes.Codigo == $scope.ListadoTipoTarifaTransportes[j].Codigo) {
                                                                    tarifa.TipoTarifaTransportes.Nombre = $scope.ListadoTipoTarifaTransportes[j].Nombre
                                                                    tarifa.TipoTarifaTransportes.ValorFlete = tarifa.ValorFlete
                                                                    tarifa.TipoTarifaTransportes.ValorFleteTransportador = tarifa.ValorFleteTransportador
                                                                    tarifa.TipoTarifaTransportes.TarifaTransporte.ValorMaximo = tarifa.TipoTarifaTransportes.TarifaTransporte.ValorMaximo;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        $scope.ListadoTarifas.push(tarifa.TipoTarifaTransportes)
                                                    }
                                                }

                                            }
                                            if ($scope.ListadoTarifas.length == 0) {
                                                ShowError('No se encontraron tarifas vigentes o disponibles, por favor revise la parametrización del tarifario del cliente')
                                            }
                                        } else {
                                            ShowError('No se encontraron tarifarios vigentes o disponibles, por favor revise la parametrizacion')
                                        }
                                    }
                                    else {
                                        ShowError('No se encontraron tarifarios vigentes o disponibles, por favor revise la parametrizacion')
                                    }
                                }
                                //Cupo Tonelada
                                else if ($scope.Modelo.OrdenServicio.TipoDespacho.Codigo == 18202) {
                                    $scope.TipoDespachoCantidadViajes = false
                                    $scope.TipoDespachoCupoTonelada = true
                                    if ($scope.DiferentesSitiosDescargue) {
                                        $scope.ListadoTarifas = []
                                        TarifarioVentasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoCliente: $scope.Modelo.OrdenServicio.Cliente.Codigo }).
                                            then(function (response) {
                                                if (response.data.ProcesoExitoso === true) {
                                                    $scope.TarifarioVentas = response.data.Datos;
                                                    if ($scope.TarifarioVentas.Tarifas.length > 0) {
                                                        for (var i = 0; i < $scope.TarifarioVentas.Tarifas.length; i++) {
                                                            var tarifa = $scope.TarifarioVentas.Tarifas[i]
                                                            if (tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.OrdenServicio.LineaNegocioCarga.Codigo
                                                                && tarifa.TipoLineaNegocioTransportes.Codigo == $scope.Modelo.OrdenServicio.TipoLineaNegocioCarga.Codigo
                                                                && tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 301
                                                            ) {
                                                                if ($scope.ListadoRutasTarifario.length == 0) {
                                                                    $scope.ListadoRutasTarifario.push($linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + tarifa.Ruta.Codigo))
                                                                } else {
                                                                    var cont = 0
                                                                    for (var K = 0; K < $scope.ListadoRutasTarifario.length; K++) {
                                                                        if (tarifa.Ruta.Codigo == $scope.ListadoRutasTarifario[K].Codigo) {
                                                                            cont++
                                                                        }
                                                                    }
                                                                    if (cont == 0) {
                                                                        $scope.ListadoRutasTarifario.push($linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + tarifa.Ruta.Codigo))
                                                                    }
                                                                }
                                                                $scope.ListadoTarifas.push(tarifa)
                                                            }
                                                        }
                                                        if ($scope.ListadoTarifas.length == 0) {
                                                            ShowError('No se encontraron tarifas vigentes o disponibles, por favor revise la parametrización del tarifario del cliente')
                                                        }
                                                    } else {
                                                        ShowError('No se encontraron tarifarios vigentes o disponibles, por favor revise la parametrizacion')
                                                    }
                                                }
                                                else {
                                                    ShowError('No se encontraron tarifarios vigentes o disponibles, por favor revise la parametrizacion')
                                                }
                                            }, function (response) {
                                                ShowError(response.statusText);
                                            });
                                    }
                                    else {
                                        var responTarifas = TarifarioVentasFactory.Obtener({
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            CodigoCliente: $scope.Modelo.OrdenServicio.Cliente.Codigo,
                                            TipoConsulta: 1,
                                            Sync: true
                                        });
                                        if (responTarifas.ProcesoExitoso) {
                                            if (responTarifas.Datos.Tarifas.length > 0) {
                                                for (var i = 0; i < responTarifas.Datos.Tarifas.length; i++) {
                                                    var tarifa = responTarifas.Datos.Tarifas[i];
                                                    if (tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.OrdenServicio.LineaNegocioCarga.Codigo
                                                        && tarifa.TipoLineaNegocioTransportes.Codigo == $scope.Modelo.OrdenServicio.TipoLineaNegocioCarga.Codigo
                                                        && tarifa.Ruta.Codigo == $scope.Modelo.OrdenServicio.Ruta.Codigo
                                                        && tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 301

                                                    ) {
                                                        $scope.Modal.TipoTarifa = tarifa.TipoTarifaTransportes;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                ShowError('El tipo de despacho de la orden de servicio no corresponde a Cantidad Viajes o Cupo Tonelada')
                                $scope.Modelo.OrdenServicio = {}
                            }
                            if ($scope.Modelo.Numero > 0) {
                                $scope.CalcularValores()
                            }
                        }
                    }
                    else {
                        ShowError('No se encontro una orden de servicio con el número ingresado')
                    }
                }
                else {
                    ShowError('No se encontro una orden de servicio con el número ingresado');
                }
            }
        }

        $scope.ListadoVehiculos = []
        $scope.AdicionarDetalle = function () {
            if (ValidacionCamposOrdenServicio()) {
                try {
                    if ($scope.Modelo.OrdenServicio.Cliente.Codigo > 0) {
                        showModal('AdicionarVehiculos')
                        var TipoTarifa = $scope.Modal.TipoTarifa;
                        $scope.Modal = {}
                        $scope.Modal.Fecha = new Date()
                        var fecha = new Date(new Date().setSeconds(3600));
                        $scope.Modal.FechaCargue = FormatoFechaISO8601(fecha)
                        $scope.Modal.CantidadXVehiculo = 0
                        $scope.Modal.FleteCliente = MascaraValores($scope.Modelo.OrdenServicio.ValorFleteCliente)
                        $scope.Modal.FleteTransportador = MascaraValores($scope.Modelo.OrdenServicio.ValorFleteTrnasportador)
                        $scope.Modal.ValorCargueCliente = MascaraValores($scope.Modelo.OrdenServicio.ValorCargue)
                        $scope.Modal.ValorDescargueCliente = MascaraValores($scope.Modelo.OrdenServicio.ValorDescargue)
                        $scope.Modal.ValorCargueTransportador = MascaraValores($scope.Modelo.OrdenServicio.ValorCargueTransportador)
                        $scope.Modal.ValorDescargueTransportador = MascaraValores($scope.Modelo.OrdenServicio.ValorDescargueTransportador)
                        $scope.Modal.Producto = $scope.Modelo.OrdenServicio.Producto
                        $scope.Modal.PermisoInvias = $scope.Modelo.OrdenServicio.PermisoInvias;
                        $scope.Modal.ValorDeclarado = MascaraValores($scope.Modelo.OrdenServicio.ValorDeclarado);
                        $scope.Modal.Observaciones = $scope.Modelo.Observaciones;
                        $scope.Modal.TipoTarifa = TipoTarifa;
                    } else {
                        ShowError('Por favor ingrese el número de la orden de servicio')
                    }
                } catch (e) {
                    ShowError('Por favor ingrese el número de la orden de servicio')
                }
            }

        }

        function ValidacionCamposOrdenServicio() {
            var cont = true

            if ($scope.Modelo.OrdenServicio.Ruta == undefined) {
                ShowError('Debe ingresar una ruta');
                cont = false;
            } else if ($scope.Modelo.OrdenServicio.SitioCargue == undefined || $scope.Modelo.OrdenServicio.SitioCargue == '') {
                ShowError('Debe ingresar un sitio de cargue');
                cont = false;
            } else if ($scope.Modelo.OrdenServicio.SitioDescargue == undefined || $scope.Modelo.OrdenServicio.SitioDescargue == '') {
                ShowError('Debe ingresar un sitio de descargue');
                cont = false;
            } else if ($scope.Modelo.OrdenServicio.TipoVehiculo == undefined || $scope.Modelo.OrdenServicio.TipoVehiculo == '') {
                ShowError('Debe ingresar un tipo vehículo');
                cont = false;
            }

            return cont
        }
        $scope.CambiarFecha = function () {
            var fecha = new Date($scope.Modal.Fecha.setSeconds(3600));
            $scope.Modal.FechaCargue = FormatoFechaISO8601(fecha)
        }
        $scope.AgregarVehiculos = function () {
            $scope.ListadoVehiculosTmp = []
            if ($scope.Modal.Fecha == undefined || $scope.Modal.CantidadVehiculos == 0 || $scope.Modal.CantidadVehiculos == undefined || $scope.Modal.CantidadXVehiculo == 0 || $scope.Modal.CantidadXVehiculo == '' || $scope.Modal.CantidadXVehiculo == undefined) {
                ShowError('Por favor ingrese la fecha, la cantidad de vehículos y el peso de cargue')
            } else {
                if ($scope.Modal.FechaCargue == undefined || $scope.Modal.FechaDescargue == undefined) {
                    ShowError('Por favor ingrese correctamente la fecha y hora de cargue y descargue')
                } else {
                    //if ($scope.Modal.FechaCargue <= new Date()) {
                    //    ShowError('La fecha de cargue no puede ser menor a la fecha actual')

                    //} else {
                    if ($scope.Modal.FechaCargue > $scope.Modal.FechaDescargue) {
                        ShowError('La fecha de cargue no puede ser mayor a la fecha de descargue')

                    } else {
                        var continuar = true
                        if ($scope.DiferentesSitiosDescargue) {
                            if ($scope.Modal.Ruta == undefined || $scope.Modal.Ruta == '' || $scope.Modal.Ruta == null) {
                                ShowError('Debe ingresar la Ruta')
                                continuar = false
                            } else {
                                if ($scope.TipoDespachoCantidadViajes) {
                                    if ($scope.Modal.Tarifa == undefined || $scope.Modal.Tarifa == '' || $scope.Modal.Tarifa == null) {
                                        ShowError('Debe ingresar la Tarifa')
                                        continuar = false
                                    } else {
                                        if ($scope.Modal.TipoTarifa == undefined || $scope.Modal.TipoTarifa == '' || $scope.Modal.TipoTarifa == null) {
                                            ShowError('Debe ingresar el Tipo Tarifa')
                                            continuar = false
                                        }
                                    }
                                }
                            }
                        }
                        if ($scope.Modal.Producto.NaturalezaProducto == 9403 || $scope.Modal.Producto.NaturalezaProducto == 9404) {//Estrapesada o estradimnensionada 
                            if (($scope.Modal.PermisoInvias == undefined || $scope.Modal.PermisoInvias == null || $scope.Modal.PermisoInvias == "")) {
                                ShowError('Debe ingresar permiso INVIAS')
                                continuar = false;
                            }
                        }
                        if (continuar) {
                            if ($scope.TipoDespachoCupoTonelada) {
                                $scope.cantidadProgramada = 0
                                for (var i = 0; i < $scope.ListadoVehiculos.length; i++) {
                                    for (var j = 0; j < $scope.ListadoVehiculos[i].Data.length; j++) {
                                        $scope.ListadoVehiculosTmp.push($scope.ListadoVehiculos[i].Data[j])
                                        $scope.cantidadProgramada += parseFloat(MascaraDecimales($scope.ListadoVehiculos[i].Data[j].CantidadDespacho).replace(',', ''))
                                    }
                                }
                                if (($scope.cantidadProgramada + (parseFloat($scope.Modal.CantidadXVehiculo.replace(',', '')) * $scope.Modal.CantidadVehiculos)) > $scope.Modelo.OrdenServicio.ValorTipoDespacho) {
                                    ShowError('El número de vehículos con la cantidad ingresada sumado a lo ya programado supera el cupo de la solicitud')

                                } else {
                                    var fecha = new Date($scope.Modal.Fecha)
                                    fecha.setMinutes(0)
                                    fecha.setHours(0)
                                    fecha.setSeconds(0)
                                    fecha.setMilliseconds(0)
                                    for (var i = 0; i < $scope.Modal.CantidadVehiculos; i++) {
                                        $scope.ListadoVehiculosTmp.push({
                                            Fecha: fecha,
                                            CantidadDespacho: MascaraValores($scope.Modal.CantidadXVehiculo),
                                            Cantidad: $scope.DiferentesSitiosDescargue ? $scope.Modal.Cantidad : MascaraValores($scope.Modal.CantidadXVehiculo),
                                            FechaCargue: new Date($scope.Modal.FechaCargue),
                                            FechaDescargue: new Date($scope.Modal.FechaDescargue),
                                            FleteCliente: $scope.Modal.FleteCliente,
                                            ValorCargueCliente: $scope.Modal.ValorCargueCliente,
                                            ValorDescargueCliente: $scope.Modal.ValorDescargueCliente,
                                            FleteTransportador: $scope.Modal.FleteTransportador,
                                            ValorCargueTransportador: $scope.Modal.ValorCargueTransportador,
                                            ValorDescargueTransportador: $scope.Modal.ValorDescargueTransportador,
                                            Ruta: $scope.Modal.Ruta,
                                            Tarifa: $scope.Modal.Tarifa,
                                            Producto: $scope.Modal.Producto,
                                            PermisoInvias: $scope.Modal.PermisoInvias,
                                            Observaciones: $scope.Modal.Observaciones,
                                            ValorDeclarado: $scope.Modal.ValorDeclarado,
                                            ValorDeclaradoAutorizado: $scope.Modal.ValorDeclaradoAutorizado == undefined ? 1 : $scope.Modal.ValorDeclaradoAutorizado,
                                            idtemp: Math.random()
                                        })
                                    }
                                    $scope.ListadoVehiculos = AgruparListados($scope.ListadoVehiculosTmp, 'Fecha')
                                    closeModal('AdicionarVehiculos')
                                    $scope.CalcularValores()
                                }
                            }
                            if ($scope.TipoDespachoCantidadViajes) {
                                $scope.cantidadProgramada = 0
                                for (var i = 0; i < $scope.ListadoVehiculos.length; i++) {
                                    for (var j = 0; j < $scope.ListadoVehiculos[i].Data.length; j++) {
                                        $scope.ListadoVehiculosTmp.push($scope.ListadoVehiculos[i].Data[j])
                                        $scope.cantidadProgramada++
                                    }
                                }
                                if (($scope.cantidadProgramada + (MascaraNumero($scope.Modal.CantidadVehiculos))) > $scope.Modelo.OrdenServicio.ValorTipoDespacho) {
                                    ShowError('El número de vehículos ingresados sumado a lo ya programado supera el cupo de la solicitud')

                                } else {
                                    var fecha = new Date($scope.Modal.Fecha)
                                    fecha.setMinutes(0)
                                    fecha.setHours(0)
                                    fecha.setSeconds(0)
                                    fecha.setMilliseconds(0)
                                    var ValorCargueClie = 0
                                    var ValorCargueTrans = 0
                                    var ValorDescargueClie = 0
                                    var ValorDescargueTrans = 0
                                    var ValorFleteClie = 0
                                    var ValorFleteTrans = 0
                                    for (var i = 0; i < $scope.Modal.CantidadVehiculos; i++) {
                                        //ValorCargueClie = MascaraValores(Math.round(parseFloat(MascaraNumero($scope.Modal.ValorCargueCliente)) * (parseInt($scope.Modal.CantidadXVehiculo) / 1000)))

                                        ValorCargueClie = parseInt(MascaraNumero($scope.Modal.ValorCargueCliente)) * (parseInt(MascaraNumero($scope.Modal.CantidadXVehiculo)) / 1000)
                                        ValorCargueTrans = parseInt(MascaraNumero($scope.Modal.ValorCargueTransportador)) * (parseInt(MascaraNumero($scope.Modal.CantidadXVehiculo)) / 1000)
                                        ValorDescargueClie = parseInt(MascaraNumero($scope.Modal.ValorDescargueCliente)) * (parseInt(MascaraNumero($scope.Modal.CantidadXVehiculo)) / 1000)
                                        ValorDescargueTrans = parseInt(MascaraNumero($scope.Modal.ValorDescargueTransportador)) * (parseInt(MascaraNumero($scope.Modal.CantidadXVehiculo)) / 1000)
                                        //ValorFleteClie = parseInt(MascaraNumero($scope.Modal.FleteCliente)) + ValorCargueClie + ValorDescargueClie
                                        //ValorFleteTrans = parseInt(MascaraNumero($scope.Modal.FleteTransportador)) + ValorCargueTrans + ValorDescargueTrans
                                        ValorFleteClie = parseFloat(MascaraDecimales($scope.Modal.FleteCliente)) + ValorCargueClie + ValorDescargueClie
                                        ValorFleteTrans = parseFloat(MascaraDecimales($scope.Modal.FleteTransportador)) + ValorCargueTrans + ValorDescargueTrans
                                        $scope.ListadoVehiculosTmp.push({
                                            Fecha: fecha,
                                            CantidadDespacho: MascaraValores($scope.Modal.CantidadXVehiculo),
                                            Cantidad: $scope.DiferentesSitiosDescargue ? $scope.Modal.Cantidad : MascaraValores($scope.Modal.CantidadXVehiculo),
                                            FechaCargue: new Date($scope.Modal.FechaCargue),
                                            FechaDescargue: new Date($scope.Modal.FechaDescargue),
                                            FleteCliente: MascaraValores(ValorFleteClie),
                                            ValorCargueCliente: MascaraValores(Math.round(ValorCargueClie)),
                                            ValorDescargueCliente: MascaraValores(Math.round(ValorDescargueClie)),
                                            FleteTransportador: MascaraValores(ValorFleteTrans),
                                            ValorCargueTransportador: MascaraValores(Math.round(ValorCargueTrans)),
                                            ValorDescargueTransportador: MascaraValores(Math.round(ValorDescargueTrans)),
                                            TipoTarifa: $scope.Modal.TipoTarifa,
                                            Producto: $scope.Modal.Producto,
                                            PermisoInvias: $scope.Modal.PermisoInvias,
                                            Ruta: $scope.Modal.Ruta,
                                            Tarifa: $scope.Modal.Tarifa,
                                            TipoTarifa: $scope.Modal.TipoTarifa,
                                            CiudadDescargue: $scope.Modal.CiudadDescargue,
                                            SitioDescargue: $scope.Modal.SitioDescargue,
                                            Observaciones: $scope.Modal.Observaciones,
                                            ValorDeclarado: $scope.Modal.ValorDeclarado,
                                            ValorDeclaradoAutorizado: $scope.Modal.ValorDeclaradoAutorizado == undefined ? 1 : $scope.Modal.ValorDeclaradoAutorizado,
                                            idtemp: Math.random()
                                        })
                                    }
                                    $scope.ListadoVehiculos = AgruparListados($scope.ListadoVehiculosTmp, 'Fecha')
                                    closeModal('AdicionarVehiculos')
                                    $scope.CalcularValores()
                                }
                            }
                        }
                    }
                    //}
                }
            }
        }
        $scope.CalcularValores = function (item) {
            $scope.cantidadProgramada = 0
            for (var i = 0; i < $scope.ListadoVehiculos.length; i++) {
                $scope.ListadoVehiculos[i].Cantidad = 0
                $scope.ListadoVehiculos[i].CantidadPendiente = 0
                for (var j = 0; j < $scope.ListadoVehiculos[i].Data.length; j++) {
                    if ($scope.TipoDespachoCupoTonelada) {
                        $scope.ListadoVehiculos[i].Data[j].FechaCargue = new Date($scope.ListadoVehiculos[i].Data[j].FechaCargue)
                        $scope.ListadoVehiculos[i].Data[j].FechaDescargue = new Date($scope.ListadoVehiculos[i].Data[j].FechaDescargue)
                        $scope.ListadoVehiculos[i].Cantidad += MascaraNumero($scope.ListadoVehiculos[i].Data[j].CantidadDespacho)
                        $scope.cantidadProgramada += MascaraNumero($scope.ListadoVehiculos[i].Data[j].CantidadDespacho)
                        try {
                            if ($scope.ListadoVehiculos[i].Data[j].OrdenServicio.IDRegistroDetalle > 0) {
                                $scope.ListadoVehiculos[i].CantidadPendiente += MascaraNumero($scope.ListadoVehiculos[i].Data[j].CantidadDespacho)
                            }
                        } catch (e) {

                        }
                        $scope.ListadoVehiculos[i].Data[j].CantidadDespacho = MascaraValores($scope.ListadoVehiculos[i].Data[j].CantidadDespacho)
                        if ($scope.cantidadProgramada > $scope.Modelo.OrdenServicio.ValorTipoDespacho && $scope.Modelo.OrdenServicio.ValorTipoDespacho > 0) {
                            item.CantidadDespacho = 0
                            ShowError('La cantidad ingresada sumado a lo progrmado supera el cupo de la solicitud')
                            $scope.CalcularValores()
                        }
                    }
                    if ($scope.TipoDespachoCantidadViajes) {
                        $scope.ListadoVehiculos[i].Data[j].FechaCargue = new Date($scope.ListadoVehiculos[i].Data[j].FechaCargue)
                        $scope.ListadoVehiculos[i].Data[j].FechaDescargue = new Date($scope.ListadoVehiculos[i].Data[j].FechaDescargue)
                        $scope.ListadoVehiculos[i].Cantidad++
                        $scope.cantidadProgramada++
                        try {
                            if ($scope.ListadoVehiculos[i].Data[j].OrdenServicio.IDRegistroDetalle > 0) {
                                $scope.ListadoVehiculos[i].CantidadPendiente++
                            }
                        } catch (e) {

                        }
                        $scope.ListadoVehiculos[i].Data[j].CantidadDespacho = MascaraValores($scope.ListadoVehiculos[i].Data[j].CantidadDespacho)
                        if ($scope.cantidadProgramada > $scope.Modelo.OrdenServicio.ValorTipoDespacho && $scope.Modelo.OrdenServicio.ValorTipoDespacho > 0) {
                            item.CantidadDespacho = 0
                            ShowError('La cantidad ingresada sumado a lo progrmado supera el cupo de la solicitud')
                            $scope.CalcularValores()
                        }
                    }
                }
                if ($scope.TipoDespachoCupoTonelada) {
                    $scope.ListadoVehiculos[i].CantidadPendiente =  ($scope.ListadoVehiculos[i].Cantidad) -  ($scope.ListadoVehiculos[i].CantidadPendiente)
                }
                if ($scope.TipoDespachoCantidadViajes) {
                    $scope.ListadoVehiculos[i].CantidadPendiente = ($scope.ListadoVehiculos[i].Cantidad) -  ($scope.ListadoVehiculos[i].CantidadPendiente)

                   
                }
            }
        }
        $scope.ShowList = function (item) {
            if (item.show == true) {
                item.show = false
            } else {
                item.show = true
            }
        }
        //Funciones Autocomplete
        $scope.ListadoPlacas = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoPlacas = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoPlacas)
                }
            }
            return $scope.ListadoPlacas
        }

        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }

        $scope.ValidarFechas = function (item) {
            if (item.FechaCargue !== undefined && item.FechaDescargue !== undefined) {
                if (item.FechaCargue > item.FechaDescargue) {
                    ShowError('La fecha de cargue no puede ser mayor a la fecha de descargue')
                    item.FechaDescargue = undefined
                }
            }
        }

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                detalle = []
                for (var i = 0; i < $scope.ListadoVehiculos.length; i++) {
                    if ($scope.ListadoVehiculos[i].Data !== undefined) {
                        for (var j = 0; j < $scope.ListadoVehiculos[i].Data.length; j++) {
                            detalle.push($scope.ListadoVehiculos[i].Data[j])
                        }
                    }
                }
                $scope.Modelo.Detalle = detalle
                $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                var arrDetalle = [];
                for (var i = 0; i < detalle.length; i++) {
                    var Detalle = {
                        CodigoEmpresa: detalle[i].CodigoEmpresa,
                        Numero: detalle[i].Numero,
                        Codigo: detalle[i].Codigo,
                        Fecha: detalle[i].Fecha,
                        CantidadDespacho: detalle[i].CantidadDespacho,
                        FleteCliente: detalle[i].FleteCliente,
                        FleteTransportador: detalle[i].FleteTransportador,
                        ValorDeclarado: detalle[i].ValorDeclarado,
                        ValorDeclaradoAutorizado: detalle[i].ValorDeclaradoAutorizado == undefined ? 1 : detalle[i].ValorDeclaradoAutorizado,
                        ValorCargueCliente: detalle[i].ValorCargueCliente,
                        ValorDescargueCliente: detalle[i].ValorDescargueCliente,
                        ValorCargueTransportador: detalle[i].ValorCargueTransportador,
                        ValorDescargueTransportador: detalle[i].ValorDescargueTransportador,
                        FechaCargue: detalle[i].FechaCargue,
                        FechaDescargue: detalle[i].FechaDescargue,
                        Observaciones: detalle[i].Observaciones,
                        Vehiculo: {
                            Codigo: (detalle[i].Vehiculo == null || detalle[i].Vehiculo == undefined) ? 0 : detalle[i].Vehiculo.Codigo
                        },
                        TipoTarifa: {
                            Codigo: (detalle[i].TipoTarifa == null || detalle[i].TipoTarifa == undefined) ? 0 : detalle[i].TipoTarifa.Codigo
                        },
                        Producto: {
                            Codigo: (detalle[i].Producto == null || detalle[i].Producto == undefined) ? 0 : detalle[i].Producto.Codigo
                        },
                        NumeroContenedor: detalle[i].NumeroContenedor,
                        Conductor: {
                            Codigo: (detalle[i].Conductor == null || detalle[i].Conductor == undefined) ? 0 : detalle[i].Conductor.Codigo
                        },
                        Ruta: {
                            Codigo: (detalle[i].Ruta == null || detalle[i].Ruta == undefined) ? 0 : detalle[i].Conductor.Codigo
                        },
                        Tarifa: {
                            Codigo: (detalle[i].Tarifa == null || detalle[i].Tarifa == undefined) ? 0 : detalle[i].Tarifa.Codigo
                        },
                        CiudadDescargue: {
                            Codigo: (detalle[i].CiudadDescargue == null || detalle[i].CiudadDescargue == undefined) ? 0 : detalle[i].CiudadDescargue.Codigo
                        },
                        SitioDescargue: {
                            Codigo: (detalle[i].SitioDescargue == null || detalle[i].SitioDescargue == undefined) ? 0 : detalle[i].SitioDescargue.Codigo
                        },
                        PermisoInvias: detalle[i].PermisoInvias,
                        Cantidad: detalle[i].Cantidad,
                        Modifica: detalle[i].Modifica,
                        UsuarioCrea: {
                            Codigo: $scope.Modelo.UsuarioCrea.Codigo
                        },
                        EditaValoresManejoObservaciones: detalle[i].Editado,
                        Editado: detalle[i].Modifica
                    }
                    arrDetalle.push(Detalle);
                }


                var programacion = {
                    Numero: $scope.Modelo.Numero,
                    codigoEmpresa: $scope.Modelo.CodigoEmpresa,
                    IDdetalle: $scope.Modelo.IDdetalle,
                    Codigo: $scope.Modelo.Codigo,
                    OrdenServicio: {
                        Numero: $scope.Modelo.OrdenServicio.Numero,
                        Cliente: {
                            Codigo: $scope.Modelo.OrdenServicio.Cliente.Codigo
                        },
                        TipoDespacho: {
                            Codigo: $scope.Modelo.OrdenServicio.TipoDespacho.Codigo
                        },
                        Ruta: {
                            Codigo: $scope.Modelo.OrdenServicio.Ruta.Codigo
                        },
                        Tarifa: {
                            Codigo: $scope.Modelo.OrdenServicio.Tarifa.Codigo
                        },
                        TipoVehiculo: {
                            Codigo: $scope.Modelo.OrdenServicio.TipoVehiculo.Codigo
                        },
                        ValorFleteTrnasportador: $scope.Modelo.OrdenServicio.ValorFleteTrnasportador,
                        ValorFleteCliente: $scope.Modelo.OrdenServicio.ValorFleteCliente,
                        CiudadCargue: {
                            Codigo: $scope.Modelo.OrdenServicio.SitioCargue.Ciudad.Codigo
                        },
                        CiudadDescargue: {
                            Codigo: $scope.Modelo.OrdenServicio.SitioDescargue.Ciudad.Codigo
                        },
                        SitioCargue: {
                            SitioCliente: {
                                Codigo: $scope.Modelo.OrdenServicio.SitioCargue.SitioCliente.Codigo
                            }
                        },
                        SitioDescargue: {
                            SitioCliente: {
                                Codigo: $scope.Modelo.OrdenServicio.SitioDescargue.SitioCliente.Codigo
                            }
                        },
                        ValorTipoDespacho: $scope.Modelo.OrdenServicio.ValorTipoDespacho,
                        SaldoTipoDespacho: $scope.Modelo.OrdenServicio.SaldoTipoDespacho
                    },
                    Fecha: $scope.Modelo.Fecha,
                    Observaciones: $scope.Modelo.Observaciones,
                    UsuarioCrea: {
                        Codigo: $scope.Modelo.UsuarioCrea.Codigo
                    },
                    Detalle: arrDetalle
                };


                //EncabezadoProgramacionOrdenServiciosFactory.Guardar($scope.Modelo).
                EncabezadoProgramacionOrdenServiciosFactory.Guardar(programacion).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Numero == 0) {
                                    ShowSuccess('Se guardó la programación ' + response.data.Datos);
                                    location.href = $scope.MasterOrdenServicio + '/' + response.data.Datos;
                                } else {
                                    ShowSuccess('Se modificó la programación ' + response.data.Datos);
                                    location.href = $scope.MasterOrdenServicio + '/' + response.data.Datos;
                                }
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            if (response.data.Datos == -1) {
                                ShowError('Ya se genero una programación con la orden de servicio ingresada');
                            } else {
                                ShowError(response.statusText);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true
            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == null || $scope.Modelo.Fecha == "") {
                $scope.MensajesError.push('Debe ingresar la fecha')
                continuar = false
            }
            try {
                if ($scope.Modelo.OrdenServicio.Cliente.Codigo > 0) {
                } else {
                    $scope.MensajesError.push('Por favor ingrese el número de la orden de servicio')
                    continuar = false
                }
            } catch (e) {
                $scope.MensajesError.push('Por favor ingrese el número de la orden de servicio')
                continuar = false
            }
            if ($scope.ListadoVehiculos.length == 0) {
                $scope.MensajesError.push('Debe ingresar al menos un detalle')
                continuar = false
            }
            var contProd = 0
            var contObs = 0
            for (var i = 0; i < $scope.ListadoVehiculos.length; i++) {
                if ($scope.ListadoVehiculos[i].Data !== undefined) {
                    for (var j = 0; j < $scope.ListadoVehiculos[i].Data.length; j++) {
                        if (($scope.ListadoVehiculos[i].Data[j].Producto == undefined || $scope.ListadoVehiculos[i].Data[j].Producto == '' || $scope.ListadoVehiculos[i].Data[j].Producto == null) && $scope.TipoDespachoCantidadViajes) {
                            contProd++
                        }
                        if ($scope.ListadoVehiculos[i].Data[j].Observaciones == undefined || $scope.ListadoVehiculos[i].Data[j].Observaciones == '' || $scope.ListadoVehiculos[i].Data[j].Observaciones == null) {
                            if ($scope.ListadoVehiculos[i].Data[j].OrdenServicio != undefined) {
                                if ($scope.ListadoVehiculos[i].Data[j].OrdenServicio.IDRegistroDetalle != undefined) {
                                    if ($scope.ListadoVehiculos[i].Data[j].OrdenServicio.IDRegistroDetalle > 0 && ($scope.ListadoVehiculos[i].Data[j].OrdenServicio.NumeroOrdenCargue > 0 || $scope.ListadoVehiculos[i].Data[j].OrdenServicio.NumeroRemesa > 0 || $scope.ListadoVehiculos[i].Data[j].OrdenServicio.NumeroPlanilla > 0)) {

                                    } else {
                                        if ($scope.ListadoVehiculos[i].Data[j].Editado > 0) {
                                            contObs++
                                        }
                                    }
                                } else {
                                    if ($scope.ListadoVehiculos[i].Data[j].Editado > 0) {
                                        contObs++
                                    }
                                }
                            } else {
                                if ($scope.ListadoVehiculos[i].Data[j].Editado > 0) {
                                    contObs++
                                }
                            }

                        }
                    }
                }
            }
            if (contProd > 0) {
                $scope.MensajesError.push('Debe ingresar el producto de todos los servicios programados')
                continuar = false
            }
            if (contObs > 0) {
                $scope.MensajesError.push('Debe ingresar las observaciones de todos los servicios modificados')
                continuar = false
            }
            //if ($scope.ModificoOrdenServicio == 1 && ($scope.Modelo.Observaciones == '' || $scope.Modelo.Observaciones == undefined || $scope.Modelo.Observaciones == null)) {
            //    $scope.MensajesError.push('Debe ingresar las observaciones')
            //    continuar = false
            //}
            return continuar
        }

        $scope.ValidarFleteClienteMaximo = function () {
            if ($scope.Modal.TipoTarifa != undefined) {
                if (RevertirMV($scope.Modal.FleteCliente) > $scope.Modal.TipoTarifa.TarifaTransporte.ValorMaximo) {
                    ShowError('El valor flete cliente no puede superar el valor máximo de ' + MascaraValores($scope.Modal.TipoTarifa.TarifaTransporte.ValorMaximo));
                    if (RevertirMV($scope.Modelo.OrdenServicio.ValorFleteCliente) > 0) {
                        $scope.Modal.FleteCliente = MascaraValores($scope.Modelo.OrdenServicio.ValorFleteCliente);
                    }
                    else {
                        $scope.Modal.FleteCliente = MascaraValores($scope.Modal.TipoTarifa.ValorFlete);
                    }
                }
            }
        };

        $scope.ValidarFleteTransportadorMaximo = function () {
            if ($scope.Modal.TipoTarifa != undefined) {
                if (RevertirMV($scope.Modal.FleteTransportador) > $scope.Modal.TipoTarifa.TarifaTransporte.ValorMaximo) {
                    ShowError('El valor flete transportador no puede superar el valor máximo de ' + MascaraValores($scope.Modal.TipoTarifa.TarifaTransporte.ValorMaximo));
                    if (RevertirMV($scope.Modelo.OrdenServicio.ValorFleteTrnasportador) > 0) {
                        $scope.Modal.FleteTransportador = MascaraValores($scope.Modelo.OrdenServicio.ValorFleteTrnasportador);
                    }
                    else {
                        $scope.Modal.FleteTransportador = MascaraValores($scope.Modal.TipoTarifa.ValorFleteTransportador);
                    }
                }
            }
        };

        $scope.ValidarFleteClienteMaximoGrid = function (detalle) {
            if ($scope.Modal.TipoTarifa != undefined) {
                if (RevertirMV(detalle.FleteCliente) > $scope.Modal.TipoTarifa.TarifaTransporte.ValorMaximo) {
                    ShowError('El valor flete cliente no puede superar el valor máximo de ' + MascaraValores($scope.Modal.TipoTarifa.TarifaTransporte.ValorMaximo));
                    if (RevertirMV($scope.Modelo.OrdenServicio.ValorFleteCliente) > 0) {
                        detalle.FleteCliente = MascaraValores($scope.Modelo.OrdenServicio.ValorFleteCliente);
                    }
                    else {
                        detalle.FleteCliente = MascaraValores($scope.Modal.TipoTarifa.ValorFlete);
                    }
                    if ($scope.TipoDespachoCantidadViajes) {
                        var ValorCargueClie = parseInt(MascaraNumero($scope.Modal.ValorCargueCliente)) * (parseInt(MascaraNumero($scope.Modal.CantidadXVehiculo)) / 1000);
                        var ValorDescargueClie = parseInt(MascaraNumero($scope.Modal.ValorDescargueCliente)) * (parseInt(MascaraNumero($scope.Modal.CantidadXVehiculo)) / 1000);
                        detalle.FleteCliente = MascaraValores(parseFloat(MascaraDecimales(detalle.FleteCliente)) + ValorCargueClie + ValorDescargueClie);
                    }
                }
            }
        };

        $scope.ValidarFleteTransportadorMaximoGrid = function (detalle) {
            if ($scope.Modal.TipoTarifa != undefined) {
                if (RevertirMV(detalle.FleteTransportador) > $scope.Modal.TipoTarifa.TarifaTransporte.ValorMaximo) {
                    ShowError('El valor flete transportador no puede superar el valor máximo de ' + MascaraValores($scope.Modal.TipoTarifa.TarifaTransporte.ValorMaximo));
                    if (RevertirMV($scope.Modelo.OrdenServicio.ValorFleteTrnasportador) > 0) {
                        detalle.FleteTransportador = MascaraValores($scope.Modelo.OrdenServicio.ValorFleteTrnasportador);
                    }
                    else {
                        detalle.FleteTransportador = MascaraValores($scope.Modal.TipoTarifa.ValorFleteTransportador);
                    }
                    if ($scope.TipoDespachoCantidadViajes) {
                        var ValorCargueTrans = parseInt(MascaraNumero($scope.Modal.ValorCargueTransportador)) * (parseInt(MascaraNumero($scope.Modal.CantidadXVehiculo)) / 1000);
                        var ValorDescargueTrans = parseInt(MascaraNumero($scope.Modal.ValorDescargueTransportador)) * (parseInt(MascaraNumero($scope.Modal.CantidadXVehiculo)) / 1000);
                        detalle.FleteTransportador = MascaraValores(parseFloat(MascaraDecimales(detalle.FleteTransportador)) + ValorCargueTrans + ValorDescargueTrans);
                    }
                }
            }
        };

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        console.clear()
        function Obtener() {
            blockUI.start('Cargando orden servicio');

            $timeout(function () {
                blockUI.message('Cargando orden servicio');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
            };
            blockUI.delay = 1000;
            EncabezadoProgramacionOrdenServiciosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = response.data.Datos;
                        for (var i = 0; i < $scope.Modelo.Detalle.length; i++) {
                            if ($scope.Modelo.Detalle[i].Producto.Codigo > 0) {
                                $scope.Modelo.Detalle[i].Producto = $scope.CargarProducto($scope.Modelo.Detalle[i].Producto.Codigo);
                            }
                            try {
                                if ($scope.Modelo.Detalle[i].Vehiculo.Codigo > 0) {
                                    $scope.Modelo.Detalle[i].Vehiculo = $scope.CargarVehiculos($scope.Modelo.Detalle[i].Vehiculo.Codigo)
                                }
                            } catch (e) {
                            }
                            try {
                            } catch (e) {
                                if ($scope.Modelo.Detalle[i].Conductor.Codigo > 0) {
                                    $scope.Modelo.Detalle[i].Conductor = $scope.CargarTercero($scope.Modelo.Detalle[i].Conductor.Codigo)
                                }
                            }
                            try {
                            } catch (e) {
                                $scope.Modelo.Detalle[i].FechaCargue = new Date($scope.Modelo.Detalle[i].FechaCargue)
                                $scope.Modelo.Detalle[i].FechaDescargue = new Date($scope.Modelo.Detalle[i].FechaDescargue)
                            }
                        }
                        if ($scope.Sesion.UsuarioAutenticado.ManejoValorDeclarado) {
                            var ResponseAutorizaciones = AutorizacionesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoAutorizacion: { Codigo: 18407 /*Autorizar Mayor Valor Declarado*/ }, Sync: true }).Datos;
                        }
                        for (var i = 0; i < $scope.Modelo.Detalle.length; i++) {
                            $scope.Modelo.Detalle[i].Fecha = new Date($scope.Modelo.Detalle[i].Fecha)
                            $scope.Modelo.Detalle[i].FleteCliente = MascaraValores($scope.Modelo.Detalle[i].FleteCliente)
                            $scope.Modelo.Detalle[i].FleteTransportador = MascaraValores($scope.Modelo.Detalle[i].FleteTransportador)
                            $scope.Modelo.Detalle[i].Cantidad = MascaraValores($scope.Modelo.Detalle[i].Cantidad)
                            $scope.Modelo.Detalle[i].ValorCargueCliente = MascaraValores($scope.Modelo.Detalle[i].ValorCargueCliente)
                            $scope.Modelo.Detalle[i].ValorDescargueCliente = MascaraValores($scope.Modelo.Detalle[i].ValorDescargueCliente)
                            $scope.Modelo.Detalle[i].ValorCargueTransportador = MascaraValores($scope.Modelo.Detalle[i].ValorCargueTransportador)
                            $scope.Modelo.Detalle[i].ValorDescargueTransportador = MascaraValores($scope.Modelo.Detalle[i].ValorDescargueTransportador)
                            $scope.Modelo.Detalle[i].ValorDeclarado = MascaraValores($scope.Modelo.Detalle[i].ValorDeclarado)

                            if ($scope.Modelo.Detalle[i].ValorDeclaradoAutorizado == 0) {
                                $scope.Modelo.Detalle[i].DeshabilitarValorDeclarado = true;
                            } else {
                                if ($scope.Sesion.UsuarioAutenticado.ManejoValorDeclarado) {
                                    ResponseAutorizaciones.forEach(itemAutorizacion => {
                                        if (itemAutorizacion.NumeroDocumento == $scope.Modelo.Detalle[i].Codigo) {
                                            $scope.Modelo.Detalle[i].DeshabilitarValorDeclarado = true;
                                        }
                                    });
                                } else {
                                    $scope.Modelo.Detalle[i].DeshabilitarValorDeclarado = true;
                                }
                            }

                        }
                        $scope.ListadoVehiculos = AgruparListados($scope.Modelo.Detalle, 'Fecha')

                        for (var i = 0; i < $scope.ListadoVehiculos.length; i++) {
                            if ($scope.ListadoVehiculos[i].Data !== undefined) {
                                for (var j = 0; j < $scope.ListadoVehiculos[i].Data.length; j++) {
                                    $scope.ListadoVehiculos[i].Data[j].FechaCargue = new Date($scope.ListadoVehiculos[i].Data[j].FechaCargue)
                                    $scope.ListadoVehiculos[i].Data[j].FechaDescargue = new Date($scope.ListadoVehiculos[i].Data[j].FechaDescargue)
                                }
                            }
                        }
                        $scope.Modelo.Fecha = RetornarFechaEspecifica(new Date(response.data.Datos.Fecha));
                        $scope.Modelo.FechaModificacion = new Date(response.data.Datos.FechaModifica);
                        $scope.Modelo.UsuarioModifica = response.data.Datos.UsuarioModifica.Nombre;
                        if ($scope.Modelo.UsuarioModifica != undefined) {
                            $scope.MostrarCamposModificacion = 1;
                        }
                        $scope.ObtenerOrdenSerivicio(null, $scope.Modelo.OrdenServicio.Numero)

                    }
                    else {
                        ShowError('No se logro consultar la orden servicio ' + response.data.MensajeOperacion);
                        document.location.href = $scope.Master;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = $scope.Master;
                });

            blockUI.stop();
        };
        $scope.EliminarDetalle = function (item, index, item2) {
            $scope.temItem = item
            $scope.itemEliminar = item2;
            $scope.temindex = index
            ShowWarningConfirm('Esta seguro eliminar este registro?', EliminarDetalle)

        }
        function EliminarDetalle() {
            $scope.temItem.Data.splice($scope.temindex, 1)
            ShowSuccess('El registro se elimino correctamente')
            $scope.CalcularValores()
            if ($scope.itemEliminar.Codigo > 0) {
                EncabezadoProgramacionOrdenServiciosFactory.EliminarDetalle({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.itemEliminar.Codigo, NumeroProgramacion: $scope.Modelo.Numero, Sync: true });
            }

        }

        $scope.ListadoCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }
        $scope.AutocompleteSitiosClienteDescargue = function (value, ciudad) {
            $scope.ListadoSitiosDescargue = []
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: $scope.Modelo.OrdenServicio.Cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    }
                    $scope.ListadoSitiosDescargue = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosDescargue)
                }
            }
            return $scope.ListadoSitiosDescargue
        }



        //Valor Declarado:
        $scope.ValidarValorDeclarado = function (index, item) {
            if ($scope.Sesion.UsuarioAutenticado.ManejoValorDeclarado) {
                $scope.indexDataListadoVehiculos = index;
                $scope.itemListadoVehiculos = item;
                $scope.ValorPoliza = EmpresasFactory.Obtener({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos.ValorPoliza;
                if (MascaraNumero($scope.itemListadoVehiculos.Data[$scope.indexDataListadoVehiculos].ValorDeclarado) > $scope.ValorPoliza) {
                    ShowConfirm('El valor declarado excede la cobertura de la póliza. No se podrá despachar este pedido hasta no estar autorizado. ¿Desea solicitar autorización?', $scope.SolicitarAutorizacion, $scope.RechazarAutorizacion)
                    $scope.ValorDeclaradoExcedePoliza = true;

                } else {
                    $scope.itemListadoVehiculos.Data[$scope.indexDataListadoVehiculos].ValorDeclaradoAutorizado = 1;
                }
            }
        }

        $scope.ValidarValorDeclaradoModal = function () {
            if ($scope.Sesion.UsuarioAutenticado.ManejoValorDeclarado) {
                $scope.ValorPoliza = EmpresasFactory.Obtener({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos.ValorPoliza;
                if (MascaraNumero($scope.Modal.ValorDeclarado) > $scope.ValorPoliza) {
                    ShowConfirm('El valor declarado excede la cobertura de la póliza. No se podrá despachar este pedido hasta no estar autorizado.¿Desea solicitar autorización?.', $scope.SolicitarAutorizacionModal, $scope.RechazarAutorizacionModal);
                    $scope.ValorDeclaradoExcedePoliza = true;
                } else {
                    $scope.Modal.ValorDeclaradoAutorizado = 1;
                }
            }
        }

        $scope.SolicitarAutorizacionModal = function () {
            $scope.Modal.ValorDeclaradoAutorizado = 0;
        }
        $scope.RechazarAutorizacionModal = function () {
            $scope.Modal.ValorDeclaradoAutorizado = 1;
            $scope.Modal.ValorDeclarado = MascaraValores($scope.Modelo.OrdenServicio.ValorDeclarado);
        }

        $scope.SolicitarAutorizacion = function () {
            $scope.itemListadoVehiculos.Data[$scope.indexDataListadoVehiculos].ValorDeclaradoAutorizado = 0;
        }
        $scope.RechazarAutorizacion = function () {
            $scope.itemListadoVehiculos.Data[$scope.indexDataListadoVehiculos].ValorDeclaradoAutorizado = 1;
            $scope.itemListadoVehiculos.Data[$scope.indexDataListadoVehiculos].ValorDeclarado = MascaraValores($scope.Modelo.OrdenServicio.ValorDeclarado);
        }

        //Fin Valor Declarado

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            document.location.href = $scope.Master + '/' + $scope.Modelo.Numero;
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (option) {
            return MascaraNumero(option)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };

    }]);
