﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Despachos

    ''' <summary>
    ''' Clase <see cref="EncabezadoSeguimientoVehiculos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoSeguimientoVehiculos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoSeguimientoVehiculos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoSeguimientoVehiculos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            NumeroRemesa = Read(lector, "NumeroRemesa")
            NumeroDocumento = Read(lector, "Numero_Documento")
            FechaDocumento = Read(lector, "FechaDocumento")
            CantidadCliente = Read(lector, "Cantidad_Cliente")
            PesoCliente = Read(lector, "Peso_Cliente")
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "NombreCliente")}
            CiudadDestino = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Remitente"), .Nombre = Read(lector, "NombreCiudad")}
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "NombreProducto")}
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
        End Sub
        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property FechaDocumento As DateTime
        <JsonProperty>
        Public Property CantidadCliente As Integer
        <JsonProperty>
        Public Property PesoCliente As Integer
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property CiudadDestino As Ciudades
        <JsonProperty>
        Public Property Producto As ProductoTransportados

    End Class
End Namespace
