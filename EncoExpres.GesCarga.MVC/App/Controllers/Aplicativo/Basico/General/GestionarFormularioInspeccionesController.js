﻿EncoExpresApp.controller("GestionarFormularioInspeccionesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'FormularioInspeccionesFactory', 'ValorCatalogosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, FormularioInspeccionesFactory, ValorCatalogosFactory) {

        $scope.Titulo = 'GESTIONAR FORMATOS INSPECCIÓN';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Formatos Inspección' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_FORMATOS_INSPECCIONES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoTipoDocumentoOrigen = []
        $scope.ListaTipoControl = [];
        $scope.MensajesErrorModal = [];
        $scope.ListadoSecciones = [];
        $scope.ListadoItemSeccion = [];
        $scope.DeshabilitarEliminar = false
        $scope.HabilitarTamaño = false
        $scope.ModeloDetalle = {

        };
        $scope.ModalTipoInspeccion = '';
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }



        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVO" },
            { Codigo: 1, Nombre: "ACTIVO" },
        ]
        $scope.ListTitem = function (item) {
            if (item.items) {
                item.items = false
            } else {
                item.items = true
            }
        }
        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            $scope.Titulo = 'CONSULTAR FORMATOS INSPECCIÓN';
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }

        //Combo TiposInspección:
        var ResponseTiposInspeccion = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPOS_INSPECCION }, Sync: true }).Datos;
        $scope.ListadoTiposInspeccion = ResponseTiposInspeccion;
        $scope.ModalTipoInspeccion = $scope.ListadoTiposInspeccion[0];


        /*Cargar el combo de Tipo documento origen*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_TIPO_DOCUMENTO_DESPACHO_INSPECCIONES } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoDocumentoOrigen.push({ Nombre: 'Seleccione tipo documento', Codigo: 0 });
                        response.data.Datos.forEach(function (item) {
                            if (item.Codigo != 13000) {
                                $scope.ListadoTipoDocumentoOrigen.push(item);
                            }
                        })
                        if ($scope.CodigoDocumentoOrigen !== undefined && $scope.CodigoDocumentoOrigen !== '' && $scope.CodigoDocumentoOrigen !== null) {
                            $scope.Modelo.TipoDocumentoOrigen = $linq.Enumerable().From($scope.ListadoTipoDocumentoOrigen).First('$.Codigo ==' + $scope.CodigoDocumentoOrigen);
                        } else {
                            $scope.Modelo.TipoDocumentoOrigen = $linq.Enumerable().From($scope.ListadoTipoDocumentoOrigen).First('$.Codigo == 0');
                        }
                    } else {
                        $scope.ListadoTipoDocumentoOrigen = []
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONTROL } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaConsulta = []
                    $scope.ListaConsulta = response.data.Datos
                    for (i = 0; i <= $scope.ListaConsulta.length - 1; i++) {
                        var Decimal = $scope.ListaConsulta[i].Codigo / 100
                        if (Decimal - Math.floor(Decimal) > CERO) {
                            $scope.ListaTipoControl.push($scope.ListaConsulta[i])
                        };
                    };
                    if ($scope.ListadoSecciones.length > CERO) {
                        $scope.ListadoSecciones.forEach(function (item) {
                            item.ListadoItemSeccion.forEach(function (itemSeccion) {
                                itemSeccion.TipoControl = $linq.Enumerable().From($scope.ListaTipoControl).First('$.Codigo ==' + itemSeccion.TipoControl.Codigo);
                            })
                        })
                    }
                };
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.Agregar = function () {
            $scope.ModeloDetalle.Nombre = ''
            for (var i = 0; i < $scope.ListadoSecciones.length; i++) {
                $scope.ListadoSecciones[i].Orden = i + 1
            }
            $scope.ModeloDetalle.EstadoDocumento = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
            showModal('modalAdicionarSeccion');
        }

        $scope.AgregarSeccion = function () {
            $scope.MensajesErrorModal = [];
            var continuar = true

            if ($scope.ModeloDetalle.Nombre == '' && $scope.ModeloDetalle.Nombre == undefined) {
                $scope.MensajesErrorModal.push('Debe ingresar el nombre de la sección')
                continuar = false
            }
            if (continuar == true) {
                if ($scope.ListadoSecciones.length >= 50) {
                    ShowError('No se pueden agregar mas secciones, el número máximo de registros permitidos es 50')
                } else {
                    $scope.ListadoSecciones.push(
                        {
                            ListadoItemSeccion: [{ EstadoDocumento: $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1'), TipoControl: $scope.ListaTipoControl[0] }],
                            EstadoDocumento: $scope.ModeloDetalle.EstadoDocumento,
                            Nombre: $scope.ModeloDetalle.Nombre,
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            items: true,
                            editable: false,
                        })

                    for (var i = 0; i < $scope.ListadoSecciones.length; i++) {
                        for (var j = 0; j < $scope.ListadoSecciones[i].ListadoItemSeccion.length; j++) {
                            $scope.ListadoSecciones[i].ListadoItemSeccion[j].Orden = j + 1
                        }
                    }

                    closeModal('modalAdicionarSeccion');
                }
            }
        }

        $scope.AgregarItem = function (Nombre, Orden) {
            var MaximoRegistros = false
            try {
                if (Nombre == '') {
                    ShowError('Debe completar toda la información de la seccón anterior')
                } else {
                    $scope.ListadoSecciones.forEach(function (item) {
                        if (item.Orden == Orden) {
                            item.ListadoItemSeccion.push({
                                NombreSeccion: item.Nombre,
                                Nombre: '',
                                Tamano: '',
                                EstadoDocumento: $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1'),
                                TipoControl: $scope.ListaTipoControl[0],
                            })
                        }
                    });

                    for (var i = 0; i < $scope.ListadoSecciones.length; i++) {
                        for (var j = 0; j < $scope.ListadoSecciones[i].ListadoItemSeccion.length; j++) {
                            $scope.ListadoSecciones[i].ListadoItemSeccion[j].Orden = j + 1
                        }
                    }
                }
            } catch (e) {
            }

        }
        $scope.ValidarDatosSeccion = function (Item) {
            $scope.ListadoSecciones.forEach(function (Item1) {
                if (Item.$$hashKey == Item1.$$hashKey) {
                    if (Item.Nombre == '' || Item.Nombre == undefined) {
                        ShowError('Debe Ingresar el nombre de la sección')
                    }
                    else {
                        Item.editable = false
                        Item1.EstadoDocumento = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + Item.EstadoDocumento.Codigo);
                    }

                }
            })
        }

        $scope.ValidarCampos = function (Item) {
            $scope.ValidarDatosSeccion(Item)

        }
        $scope.RestaurarCampos = function () {
            $scope.ListadoSecciones.forEach(function (Item1) {
                if ($scope.TemSeccion.$$hashKey == Item1.$$hashKey) {
                    Item1 = JSON.parse(JSON.stringify($scope.TemSeccion))
                    Item1.EstadoDocumento = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.TemSeccion.EstadoDocumento.Codigo);
                }
            })
        }
        $scope.GuardarCampos = function (item) {
            $scope.TemSeccion = JSON.parse(JSON.stringify(item))
            $timeout(function () {
                $scope.RestaurarCampos()
            }, 200);
        }
        $scope.EliminarSeccion = function (index) {
            $scope.ListadoSecciones.splice(index, 1)
        }

        $scope.EliminarItem = function (index, itemSeccion) {
            $scope.ListadoSecciones.forEach(function (item) {
                if (itemSeccion.Orden == item.Orden) {
                    item.ListadoItemSeccion.splice(index, 1)
                }
            })
        }

        $scope.SubirSeccion = function (index) {
            var ItemTemp = JSON.parse(JSON.stringify($scope.ListadoSecciones[index - 1]))
            $scope.ListadoSecciones[index - 1] = JSON.parse(JSON.stringify($scope.ListadoSecciones[index]))
            $scope.ListadoSecciones[index] = JSON.parse(JSON.stringify(ItemTemp))
        }

        $scope.BajarSeccion = function (index) {
            var ItemTemp = JSON.parse(JSON.stringify($scope.ListadoSecciones[index + 1]))
            $scope.ListadoSecciones[index + 1] = JSON.parse(JSON.stringify($scope.ListadoSecciones[index]))
            $scope.ListadoSecciones[index] = JSON.parse(JSON.stringify(ItemTemp))
        }

        $scope.SubirItem = function (index, Orden,Seccion) {
            $scope.ListadoSecciones.forEach(function (item) {
                
                for (var i = 0; i < item.ListadoItemSeccion.length; i++) {
                    if (Seccion.Codigo == item.ListadoItemSeccion[i].Seccion) {
                        if (item.ListadoItemSeccion[i].Orden == Orden) {
                            var ItemActual = item.ListadoItemSeccion[i];
                            var itemArribaActual = item.ListadoItemSeccion[i - 1];
                            item.ListadoItemSeccion[i - 1] = ItemActual;
                            item.ListadoItemSeccion[i] = itemArribaActual;

                            item.ListadoItemSeccion[i - 1].Orden = MascaraValores(MascaraNumero(item.ListadoItemSeccion[i - 1].Orden) - 1);
                            item.ListadoItemSeccion[i].Orden = MascaraValores(MascaraNumero(item.ListadoItemSeccion[i].Orden) + 1);
                        }
                    }
                }
                

            })
        }

        $scope.BajarItem = function (index, Orden, Seccion) {
            
            
            $scope.ListadoSecciones.forEach(function (item) {
               
                for (var i = 0; i < item.ListadoItemSeccion.length; i++) {
                    if (Seccion.Codigo == item.ListadoItemSeccion[i].Seccion) {
                        if (item.ListadoItemSeccion[i].Orden == Orden) {
                            var ItemActual = item.ListadoItemSeccion[i];
                            var itemArribaActual = item.ListadoItemSeccion[i + 1];
                            item.ListadoItemSeccion[i + 1] = ItemActual;
                            item.ListadoItemSeccion[i] = itemArribaActual;

                            item.ListadoItemSeccion[i + 1].Orden = MascaraValores(MascaraNumero(item.ListadoItemSeccion[i + 1].Orden) + 1);
                            item.ListadoItemSeccion[i].Orden = MascaraValores(MascaraNumero(item.ListadoItemSeccion[i].Orden) - 1);
                        }
                    }
                }
                

            })
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando formularo inspección código No. ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando formularo inspección  Código No.' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
                ConsularDetalle: 1,
            };

            blockUI.delay = 1000;
            FormularioInspeccionesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo.Codigo = response.data.Datos.Codigo;
                        $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                        $scope.Modelo.Nombre = response.data.Datos.Nombre;
                        $scope.Modelo.Version = response.data.Datos.Version;

                        $scope.CodigoDocumentoOrigen = response.data.Datos.TipoDocumentoOrigen.Codigo
                        if ($scope.ListadoTipoDocumentoOrigen.length > 0 && $scope.CodigoDocumentoOrigen > 0) {
                            $scope.Modelo.TipoDocumentoOrigen = $linq.Enumerable().From($scope.ListadoTipoDocumentoOrigen).First('$.Codigo == ' + response.data.Datos.TipoDocumentoOrigen.Codigo);
                        }
                        $scope.ModalTipoInspeccion = $linq.Enumerable().From($scope.ListadoTiposInspeccion).First('$.Codigo == ' + response.data.Datos.TipoInspeccion.Codigo);
                        //if (response.data.Datos.Preoperacional == 1) {
                        //    $scope.Modelo.Preoperacional = true
                        //}
                        //else {
                        //    $scope.Modelo.Preoperacional = false
                        //}

                        if (response.data.Datos.Firma == 1) {
                            $scope.Modelo.Firma = true
                        }
                        else {
                            $scope.Modelo.Firma = false
                        }

                        //$scope.DeshabilitarEliminar = true
                        response.data.Datos.SeccionFormulario.forEach(function (item) {
                            item.items = true,
                                item.editable = false,
                                item.ListadoItemSeccion = [];
                            item.EstadoDocumento = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + item.EstadoDocumento.Codigo)

                            response.data.Datos.itemFormulario.forEach(function (itemSeccion) {
                                if (itemSeccion.Seccion == item.Codigo) {
                                    itemSeccion.EstadoDocumento = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + itemSeccion.EstadoDocumento.Codigo)
                                    if ($scope.ListaTipoControl.length > CERO) {
                                        itemSeccion.TipoControl = $linq.Enumerable().From($scope.ListaTipoControl).First('$.Codigo ==' + itemSeccion.TipoControl.Codigo);
                                    }
                                    itemSeccion.ExigeCumplimiento = itemSeccion.ExigeCumplimiento == 1 ? true : false;
                                    item.ListadoItemSeccion.push(itemSeccion)
                                }
                            })

                            $scope.ListadoSecciones.push(item)
                        })

                        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)
                        Ventana.scrollTop = 0
                    }
                    else {
                        ShowError('No se logro consultar el formularo inspección  código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarFormularioInspecciones';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarFormularioInspecciones';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                //if ($scope.Modelo.Preoperacional == true) {
                //    $scope.Modelo.Preoperacional = 1
                //}
                //else {
                //    $scope.Modelo.Preoperacional = 0
                //}

                if ($scope.Modelo.Firma == true) {
                    $scope.Modelo.Firma = 1
                }
                else {
                    $scope.Modelo.Firma = 0
                }

                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
                $scope.Modelo.TipoInspeccion = { Codigo: $scope.ModalTipoInspeccion.Codigo }
                $scope.ListadoSecciones.forEach(item => {
                    item.ListadoItemSeccion.forEach(detalleItem => {
                        detalleItem.ExigeCumplimiento = detalleItem.ExigeCumplimiento == true ? 1: 0
                    });
                });
                $scope.Modelo.SeccionFormulario = $scope.ListadoSecciones

                FormularioInspeccionesFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el formulario de inspección "' + $scope.Modelo.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se Modificó el formulario de inspección "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = '#!ConsultarFormularioInspecciones/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        $scope.HabilitarTamañoControl = function (TipoControl) {
            $scope.HabilitarTamaño = false
            if (TipoControl.Codigo == CODIGO_TIPO_CONTROL_TEXTOS || TipoControl.Codigo == CODIGO_TIPO_CONTROL_NUMEROS) {
                $scope.HabilitarTamaño = true
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.CodigoAlterno == undefined || $scope.Modelo.CodigoAlterno == null) {
                $scope.Modelo.CodigoAlterno = ''
            }
            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '' || $scope.Modelo.Nombre == null) {
                $scope.MensajesError.push('Debe ingresar el nombre ');
                continuar = false;
            }
            if ($scope.Modelo.Version == undefined || $scope.Modelo.Version == null || $scope.Modelo.Version == '') {
                $scope.MensajesError.push('Debe ingresar la versión ');
                continuar = false;
            }
            if ($scope.Modelo.TipoDocumentoOrigen == undefined || $scope.Modelo.TipoDocumentoOrigen == null || $scope.Modelo.TipoDocumentoOrigen == '' || $scope.Modelo.TipoDocumentoOrigen.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar el tipo documento origen ');
                continuar = false;
            }
            if ($scope.ListadoSecciones.length == 0) {
                $scope.MensajesError.push('Debe ingresar mínimo una sección');
                continuar = false;
            }
            else {
                var cadenaerror = [];
                $scope.ListadoSecciones.forEach(function (item) {
                    if (item.ListadoItemSeccion.length == 0) {
                        cadenaerror.push('Debe ingresar mínimo un item para la sección ' + item.Nombre)
                    }
                    else {
                        item.ListadoItemSeccion.forEach(function (itemSeccion) {
                            if (itemSeccion.Nombre == '' || itemSeccion.Nombre == undefined) {
                                cadenaerror.push('Debe ingresar el nombre del item  ' + itemSeccion.Nombre)
                            }
                            if (itemSeccion.TipoControl.Codigo == CODIGO_TIPO_CONTROL_TEXTO || itemSeccion.TipoControl.Codigo == CODIGO_TIPO_CONTROL_NUMERO) {
                                if (itemSeccion.Tamano == 0 || itemSeccion.Tamano == undefined || itemSeccion.Tamano == null || itemSeccion.Tamano == "") {
                                    cadenaerror.push('Debe ingresar el tamaño del item  ' + itemSeccion.Nombre)
                                }
                                else {
                                    if (itemSeccion.Tamano > 200) {
                                        cadenaerror.push('El tamaño del control ' + item.Nombre + ' debe ser inferior a 200')
                                    }
                                }
                            }
                        })
                    }
                })
                if (cadenaerror.length > 0) {
                    cadenaerror.forEach(function (item) {
                        $scope.MensajesError.push(item)
                    })
                    continuar = false;
                }
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarFormularioInspecciones/' + $scope.Modelo.Codigo;
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
            try { $scope.Modelo.Version = $scope.Modelo.Version.toUpperCase() } catch (e) { }
            try { $scope.ModeloDetalle.Nombre = $scope.ModeloDetalle.Nombre.toUpperCase() } catch (e) { }
        };

        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
    }]);