﻿PRINT 'gsp_insertar_recolecciones'
GO
DROP PROCEDURE gsp_insertar_recolecciones
GO
CREATE PROCEDURE gsp_insertar_recolecciones    
(    
  @par_EMPR_Codigo SMALLINT, 
  @par_TIDO_Codigo NUMERIC = NULL,       
  @par_Fecha DATE = NULL,  
  @par_TERC_Codigo_Cliente NUMERIC = NULL,              
  @par_Contacto VARCHAR(50) = NULL, 
  @par_CIUD_Codigo NUMERIC = NULL,   
  @par_ZOCI_Codigo NUMERIC = NULL, 
  @par_Barrio VARCHAR(50) = NULL,      
  @par_Direccion VARCHAR(150) = NULL,      
  @par_Telefonos VARCHAR(100) = NULL,  
  @par_Mercancia VARCHAR(250) = NULL,      
  @par_UEPT_Codigo NUMERIC = NULL, 
  @par_Cantidad NUMERIC = NULL,      
  @par_Peso NUMERIC = NULL,      
  @par_Observaciones VARCHAR(500) = NULL,      
  @par_OFIC_Codigo NUMERIC = NULL,    
  @par_Estado SMALLINT,    
  @par_USUA_Codigo_Crea SMALLINT       
)    
AS    
BEGIN    

  DECLARE @numNumeroDocumento NUMERIC = 0    
  DECLARE @numNumero NUMERIC = 0    
    
  EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, @par_OFIC_Codigo, @numNumeroDocumento OUTPUT    

 INSERT INTO    
  Encabezado_Recolecciones    
  (    
  EMPR_Codigo,
  TIDO_Codigo,
  Numero_Documento,
  Fecha,
  TERC_Codigo_Cliente,
  Nombre_Contacto,
  CIUD_Codigo,
  ZOCI_Codigo,
  Barrio,
  Direccion,
  Telefonos,
  Mercancia,
  UEPT_Codigo,
  Cantidad,
  Peso,
  Observaciones,
  Estado,
  OFIC_Codigo ,
  Fecha_Crea,  
  USUA_Codigo_Crea,
  Anulado,
  ENPR_Numero 
  )    
 VALUES    
 (    
  @par_EMPR_Codigo,  
  @par_TIDO_Codigo, 
  @numNumeroDocumento,
  @par_Fecha, 
  ISNULL(@par_TERC_Codigo_Cliente, ''),     
  ISNULL(@par_Contacto, ''),  
  @par_CIUD_Codigo,
  @par_ZOCI_Codigo,
  ISNULL(@par_Barrio, ''),
  ISNULL(@par_Direccion, ''),
  @par_Telefonos,  
  ISNULL(@par_Mercancia, ''),      
  @par_UEPT_Codigo, 
  ISNULL(@par_Cantidad, 0),      
  ISNULL(@par_Peso, 0),      
  ISNULL(@par_Observaciones, ''),      
  @par_Estado,    
  @par_OFIC_Codigo,  
  GETDATE(),  
  @par_USUA_Codigo_Crea,
  0,
  0    
 )    

  SET @numNumero = @@IDENTITY    
    
  SELECT Numero, Numero_Documento FROM Encabezado_Recolecciones WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @numNumero   
    
END    
GO