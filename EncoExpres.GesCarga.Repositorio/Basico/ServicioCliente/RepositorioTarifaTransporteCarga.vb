﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente

Namespace Basico.ServicioCliente

    ''' <summary>
    ''' Clase <see cref="RepositorioTarifaTransporteCarga"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTarifaTransporteCarga
        Inherits RepositorioBase(Of TarifaTransporteCarga)

        Public Overrides Function Consultar(filtro As TarifaTransporteCarga) As IEnumerable(Of TarifaTransporteCarga)
            Dim lista As New List(Of TarifaTransporteCarga)
            Dim item As TarifaTransporteCarga

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[GSP_consultar_empresas]")

                While resultado.Read
                    item = New TarifaTransporteCarga(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As TarifaTransporteCarga) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Modificar(entidad As TarifaTransporteCarga) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As TarifaTransporteCarga) As TarifaTransporteCarga
            Dim item As New TarifaTransporteCarga

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_empresas]")

                While resultado.Read
                    item = New TarifaTransporteCarga(resultado)
                End While

            End Using

            Return item
        End Function
    End Class

End Namespace