﻿Imports System.Runtime.Caching
Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.Despachos

<Authorize>
Public Class CombinacionContratoVinculacionController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As CombinacionContratoVinculacion) As Respuesta(Of IEnumerable(Of CombinacionContratoVinculacion))
        Return New LogicaCombinacionContratoVinculacion(CapaPersistenciaCombinacionContratoVinculacion).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As CombinacionContratoVinculacion) As Respuesta(Of CombinacionContratoVinculacion)
        Return New LogicaCombinacionContratoVinculacion(CapaPersistenciaCombinacionContratoVinculacion).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As CombinacionContratoVinculacion) As Respuesta(Of Long)
        Return New LogicaCombinacionContratoVinculacion(CapaPersistenciaCombinacionContratoVinculacion).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As CombinacionContratoVinculacion) As Respuesta(Of Boolean)
        Return New LogicaCombinacionContratoVinculacion(CapaPersistenciaCombinacionContratoVinculacion).Anular(entidad)
    End Function
End Class
