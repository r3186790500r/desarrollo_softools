﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Repositorio.Tesoreria

Namespace Tesoreria
    Public NotInheritable Class PersistenciaEncabezadoDocumentoCuentas
        Implements IPersistenciaBase(Of EncabezadoDocumentoCuentas)

        Public Function Consultar(filtro As EncabezadoDocumentoCuentas) As IEnumerable(Of EncabezadoDocumentoCuentas) Implements IPersistenciaBase(Of EncabezadoDocumentoCuentas).Consultar
            Return New RepositorioEncabezadoDocumentoCuentas().Consultar(filtro)
        End Function
        Public Function ConsultarCuentasconEgresos(filtro As EncabezadoDocumentoCuentas) As IEnumerable(Of EncabezadoDocumentoCuentas)
            Return New RepositorioEncabezadoDocumentoCuentas().ConsultarCuentasconEgresos(filtro)
        End Function

        Public Function ConsultarDetalleCruceDocumentos(filtro As EncabezadoDocumentoCuentas) As IEnumerable(Of DetalleCruceDocumentoCuenta)
            Return New RepositorioEncabezadoDocumentoCuentas().ConsultarDetalleCruceDocumentos(filtro)
        End Function
        Public Function Insertar(entidad As EncabezadoDocumentoCuentas) As Long Implements IPersistenciaBase(Of EncabezadoDocumentoCuentas).Insertar
            Return New RepositorioEncabezadoDocumentoCuentas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoDocumentoCuentas) As Long Implements IPersistenciaBase(Of EncabezadoDocumentoCuentas).Modificar
            Return New RepositorioEncabezadoDocumentoCuentas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoDocumentoCuentas) As EncabezadoDocumentoCuentas Implements IPersistenciaBase(Of EncabezadoDocumentoCuentas).Obtener
            Return New RepositorioEncabezadoDocumentoCuentas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EncabezadoDocumentoCuentas) As Boolean
            Return New RepositorioEncabezadoDocumentoCuentas().Anular(entidad)
        End Function
    End Class

End Namespace

