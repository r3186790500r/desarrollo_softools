﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento

    Public NotInheritable Class PersistenciaDetalleTareaMantenimiento
        Implements IPersistenciaBase(Of DetalleTareaMantenimiento)
        Public Function Consultar(filtro As DetalleTareaMantenimiento) As IEnumerable(Of DetalleTareaMantenimiento) Implements IPersistenciaBase(Of DetalleTareaMantenimiento).Consultar
            Return New RepositorioDetalleTareaMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleTareaMantenimiento) As Long Implements IPersistenciaBase(Of DetalleTareaMantenimiento).Insertar
            Return New RepositorioDetalleTareaMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleTareaMantenimiento) As Long Implements IPersistenciaBase(Of DetalleTareaMantenimiento).Modificar
            Return New RepositorioDetalleTareaMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleTareaMantenimiento) As DetalleTareaMantenimiento Implements IPersistenciaBase(Of DetalleTareaMantenimiento).Obtener
            Return New RepositorioDetalleTareaMantenimiento().Obtener(filtro)
        End Function

        Public Function Anular(entidad As DetalleTareaMantenimiento) As Boolean
            Return New RepositorioDetalleTareaMantenimiento().Anular(entidad)
        End Function

    End Class
End Namespace
