﻿PRINT 'gsp_modificar_encabezado_parametrizacion_contables'
GO
DROP PROCEDURE gsp_modificar_encabezado_parametrizacion_contables
GO
CREATE PROCEDURE gsp_modificar_encabezado_parametrizacion_contables
(
	@par_EMPR_Codigo SMALLINT,
	@par_Codigo SMALLINT,
	@par_Codigo_Alterno VARCHAR (20),
	@par_Nombre VARCHAR (50),
	@par_CATA_TIDG_Codigo NUMERIC,
	@par_Fuente VARCHAR (10),
	@par_Fuente_Anulacion VARCHAR(10), 
	@par_CATA_TGDC_Codigo NUMERIC,
	@par_Observaciones VARCHAR (250),
	@par_Provision SMALLINT,
	@par_Estado SMALLINT,
	@par_USUA_Codigo_Modifica SMALLINT
)
AS
BEGIN

DELETE Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPC_Codigo = @par_Codigo

UPDATE Encabezado_Parametrizacion_Contables
SET
	Codigo_Alterno = @par_Codigo_Alterno,
	Nombre = @par_Nombre,
	CATA_TIDG_Codigo = @par_CATA_TIDG_Codigo,
	Fuente = @par_Fuente,
	Fuente_Anulacion = @par_Fuente_Anulacion,
	CATA_TGDC_Codigo = @par_CATA_TGDC_Codigo,
	Observaciones = @par_Observaciones,
	Provision = @par_Provision,
	Estado = @par_Estado,
	USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,
	Fecha_Modifica = GETDATE()
WHERE
	EMPR_Codigo = @par_EMPR_Codigo
	AND Codigo = @par_Codigo

SELECT @par_Codigo AS Codigo

END
GO