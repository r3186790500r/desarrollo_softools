﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports System.Transactions

Namespace ControlViajes

    ''' <summary>
    ''' Clase <see cref="RepositorioAutorizaciones"/>
    ''' </summary>
    Public NotInheritable Class RepositorioAutorizaciones
        Inherits RepositorioBase(Of Autorizaciones)

        Public Overrides Function Consultar(filtro As Autorizaciones) As IEnumerable(Of Autorizaciones)
            Dim lista As New List(Of Autorizaciones)
            Dim item As Autorizaciones
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()


                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                Else
                    If Not IsNothing(filtro.NombreUsuarioSolicita) Then
                        conexion.AgregarParametroSQL("@par_USUA_Nombre_Solicita", filtro.NombreUsuarioSolicita)
                    End If
                    If Not IsNothing(filtro.NombreUsuarioAutoriza) Then
                        conexion.AgregarParametroSQL("@par_USUA_Nombre_Autoriza", filtro.NombreUsuarioAutoriza)
                    End If

                    If Not IsNothing(filtro.TipoAutorizacion) Then
                        If filtro.TipoAutorizacion.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_CATA_TSAO_Codigo", filtro.TipoAutorizacion.Codigo)
                        End If
                    End If
                    If Not IsNothing(filtro.EstadoAutorizacion) Then
                        conexion.AgregarParametroSQL("@par_CATA_ESAO_Codigo", filtro.EstadoAutorizacion.Codigo)
                    End If

                    If filtro.FechaSolicita > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Solicita", filtro.FechaSolicita, SqlDbType.Date)
                    End If

                    If filtro.FechaFinAutoriza > DateTime.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Autoriza_Fin", filtro.FechaFinAutoriza, SqlDbType.DateTime)
                    End If
                    If filtro.FechaInicioAutoriza > DateTime.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Autoriza_Inicio", filtro.FechaInicioAutoriza, SqlDbType.DateTime)
                    End If
                End If

                If Not IsNothing(filtro.Usuario) Then
                    If filtro.Usuario.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_USUA_Codigo", filtro.Usuario.Codigo)
                    End If
                End If
                If filtro.Pagina > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                End If
                If filtro.RegistrosPagina > 0 Then
                    conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                End If
                If filtro.Notificacion > 0 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_solicitud_autorizaciones_operacion_Notificación]")
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_solicitud_autorizaciones_operacion]")
                End If

                While resultado.Read
                    item = New Autorizaciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As Autorizaciones) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As Autorizaciones) As Long
            Dim inserto As Boolean = True
            Dim insertoSeccion As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()


                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_ESAO_Codigo", entidad.EstadoAutorizacion.Codigo)
                    conexion.AgregarParametroSQL("@par_Observacion_Autoriza", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Valor", entidad.Valor, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Autoriza", entidad.UsuarioGestion.Codigo)

                    Dim resultado As IDataReader
                    If Not IsNothing(entidad.TipoAutorizacion) Then
                        If entidad.TipoAutorizacion.Codigo = CODIGO_CATALOGO_TIPO_AUTORIZACION_VALOR_DECLARADO Then
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_solicitud_autorizaciones_operacion_valor_declarado")
                        Else
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_solicitud_autorizaciones_operacion")
                        End If
                    Else
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_solicitud_autorizaciones_operacion")

                    End If



                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If


                    conexion.CloseConnection()
                    'Conceptos'
                    If Not IsNothing(entidad.DetalleConceptos) Then
                        For Each detalle In entidad.DetalleConceptos
                            detalle.Autorizacion = New Autorizaciones With {.Codigo = entidad.Numero}
                            inserto = IIf(InsertarDetalleAutorizacionValorDeclarado(detalle, conexion) > 0, True, False)
                            If inserto = False Then
                                transaccion.Dispose()
                            End If
                        Next
                    End If


                End Using

                If entidad.Numero > 0 Then
                    transaccion.Complete()
                End If
            End Using

            'Documentos
            If Not IsNothing(entidad.Documento) Then

                entidad.Documento.NumeroDocumento = entidad.Numero
                inserto = IIf(InsertarDocumentoAutorizacionValorDeclarado(entidad.Documento) > 0, True, False)

            End If
            Return IIf(inserto = True, 1, 0)
        End Function

        Public Function InsertarDetalleAutorizacionValorDeclarado(entidad As ConceptoAutorizacionValorDeclarado, conexion As DataBaseFactory) As Integer
            Dim inserto As Integer = 0
            Dim resultado As IDataReader
            conexion.CreateConnection()
            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENAU_Codigo", entidad.Autorizacion.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_CAVD_Codigo", entidad.Codigo)
            conexion.AgregarParametroSQL("@par_Valor", entidad.Valor, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Facturar", entidad.Facturar)

            resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_autorizacion_valor_declarado")


            While resultado.Read
                inserto = resultado.Item("Numero").ToString()
            End While
            resultado.Close()
            conexion.CloseConnection()

            Return inserto
        End Function

        Public Function InsertarDocumentoAutorizacionValorDeclarado(entidad As Documentos) As Integer
            Dim inserto As Integer = 0
            Dim resultado As IDataReader

            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                conexionDocumentos.CleanParameters()

                conexionDocumentos.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexionDocumentos.AgregarParametroSQL("@par_ENAU_Codigo", entidad.NumeroDocumento)
                conexionDocumentos.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexionDocumentos.AgregarParametroSQL("@par_Tipo", entidad.Tipo)
                conexionDocumentos.AgregarParametroSQL("@par_Extension", entidad.Extension)
                conexionDocumentos.AgregarParametroSQL("@par_Archivo", entidad.Archivo, SqlDbType.VarBinary)
                resultado = conexionDocumentos.ExecuteReaderStoreProcedure("gsp_insertar_documento_autorizacion_valor_declarado")

                While resultado.Read
                    inserto = resultado.Item("Numero").ToString()
                End While
                resultado.Close()
                conexionDocumentos.CloseConnection()

            End Using


            Return inserto
        End Function
        Public Overrides Function Obtener(filtro As Autorizaciones) As Autorizaciones
            Dim item As New Autorizaciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_solicitud_autorizaciones_operacion]")


                While resultado.Read
                    item = New Autorizaciones(resultado)
                End While
                conexion.CloseConnection()
            End Using

            Return item
        End Function
        Public Function InsertarAutorizacion(entidad As Autorizaciones, conexion As Conexion.DataBaseFactory) As Long
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            'conexion.AgregarParametroSQL("@par_ENES_Numero", If(Not IsNothing(entidad.EstudioSeguridad), entidad.EstudioSeguridad.Codigo, DBNull.Value))
            If Not IsNothing(entidad.EstudioSeguridad) Then
                conexion.AgregarParametroSQL("@par_ENES_Numero", entidad.EstudioSeguridad.Numero)
            End If
            If Not IsNothing(entidad.PlanillaDespacho) Then
                conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.PlanillaDespacho.Codigo)
            End If
            If entidad.NumeroAutorizacion > 0 Then
                conexion.AgregarParametroSQL("@par_Numero_Autorización", entidad.NumeroAutorizacion)
            End If
            If entidad.NumeroComprobante > 0 Then
                conexion.AgregarParametroSQL("@par_ENDC_Numero", entidad.NumeroComprobante)
            End If
            If Not IsNothing(entidad.TipoAutorizacion) Then
                conexion.AgregarParametroSQL("@par_CATA_TSAU_Codigo", entidad.TipoAutorizacion.Codigo)
            End If
            If Not IsNothing(entidad.EstadoAutorizacion) Then
                conexion.AgregarParametroSQL("@par_CATA_ESAU_Codigo", entidad.EstadoAutorizacion.Codigo)
            End If
            If Not IsNothing(entidad.CuentaPorPagar) Then

                If Not IsNothing(entidad.CuentaPorPagar.Vehiculo) Then
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.CuentaPorPagar.Vehiculo.Codigo)
                End If
                If Not IsNothing(entidad.CuentaPorPagar.TipoDocumento) Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo_Cuenta", entidad.CuentaPorPagar.TipoDocumento)
                End If
                If Not IsNothing(entidad.CuentaPorPagar.Tercero) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.CuentaPorPagar.Tercero.Codigo)
                End If
                If Not IsNothing(entidad.CuentaPorPagar.DocumentoOrigen) Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo_Documento_Origen", entidad.CuentaPorPagar.DocumentoOrigen.Codigo)
                End If
                If entidad.CuentaPorPagar.ValorTotal > 0 Then
                    conexion.AgregarParametroSQL("@par_Valor", entidad.CuentaPorPagar.ValorTotal, SqlDbType.Money)
                End If
                If entidad.CuentaPorPagar.FechaCancelacionPago > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Cancelacion_Pago", entidad.CuentaPorPagar.FechaCancelacionPago, SqlDbType.Date)
                End If
                If Not IsNothing(entidad.CuentaPorPagar.Oficina) Then
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.CuentaPorPagar.Oficina.Codigo)
                End If
                If entidad.CuentaPorPagar.CodigoDocumentoOrigen > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo_Documento_Origen", entidad.CuentaPorPagar.CodigoDocumentoOrigen)
                End If
                If entidad.CuentaPorPagar.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Origen", entidad.CuentaPorPagar.Numero)
                End If
                If entidad.CuentaPorPagar.Observaciones <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Observaciones_Cuenta", entidad.CuentaPorPagar.Observaciones)
                End If
            Else
                If Not IsNothing(entidad.Vehiculo) Then
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                End If
                If entidad.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Origen", entidad.NumeroDocumento)
                End If
            End If
            If entidad.Observaciones <> String.Empty Then
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            End If
            If entidad.ID_Enturne > 0 Then
                conexion.AgregarParametroSQL("@par_ENDE_ID", entidad.ID_Enturne)
            End If
            If entidad.ValorFlete > 0 Then
                conexion.AgregarParametroSQL("@par_valor_Flete_Autorizacion", entidad.ValorFlete)
            End If
            If entidad.ValorDeclaradoAutorizacion > 0 Then
                conexion.AgregarParametroSQL("@par_Valor_Declarado_Autorizacion", entidad.ValorDeclaradoAutorizacion)
            End If
            If entidad.ValorFleteReal > 0 Then
                conexion.AgregarParametroSQL("@par_Valor_Flete", entidad.ValorFleteReal)
            End If
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Solicita", entidad.UsuarioSolicita.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_encabezado_autorizaciones]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            If entidad.Codigo.Equals(Cero) Then
                Return Cero
            End If

            Return entidad.Codigo
        End Function

        Public Function Anular(entidad As Autorizaciones) As Boolean
            Throw New NotImplementedException()
        End Function

    End Class

End Namespace