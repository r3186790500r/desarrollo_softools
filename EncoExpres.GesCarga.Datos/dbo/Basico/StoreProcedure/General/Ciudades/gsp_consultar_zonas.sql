﻿PRINT 'gsp_consultar_zonas'
GO
DROP PROCEDURE gsp_consultar_zonas
GO
CREATE PROCEDURE gsp_consultar_zonas      
(      
 @par_EMPR_Codigo SMALLINT,      
 @par_Codigo SMALLINT = NULL,  
 @par_CIUD_Codigo SMALLINT = NULL,
 @par_Estado SMALLINT = NULL      
)      
AS      
BEGIN      
     SELECT      
     ZOCI.EMPR_Codigo,      
     ZOCI.Codigo,      
     ZOCI.Nombre,     
     CIUD.Nombre AS Nombre_Ciudad,     
     ZOCI.CIUD_Codigo,       
     ZOCI.Estado    
 FROM      
     Zona_Ciudades ZOCI LEFT JOIN Ciudades CIUD ON    
     ZOCI.EMPR_Codigo = CIUD.EMPR_Codigo    
     AND ZOCI.CIUD_Codigo = CIUD.Codigo    
     WHERE      
     ZOCI.EMPR_Codigo = @par_EMPR_Codigo       
     AND ZOCI.Codigo = ISNULL(@par_Codigo, ZOCI.Codigo)    
     AND ZOCI.CIUD_Codigo = ISNULL(@par_CIUD_Codigo, ZOCI.CIUD_Codigo)   
	 AND ZOCI.Estado = ISNULL(@par_Estado, ZOCI.Estado)     
  
  ORDER BY Nombre      
END   
GO  