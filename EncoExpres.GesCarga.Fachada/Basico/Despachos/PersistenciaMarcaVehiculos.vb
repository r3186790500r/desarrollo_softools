﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaMarcaVehiculos
        Implements IPersistenciaBase(Of MarcaVehiculos)

        Public Function Consultar(filtro As MarcaVehiculos) As IEnumerable(Of MarcaVehiculos) Implements IPersistenciaBase(Of MarcaVehiculos).Consultar
            Return New RepositorioMarcaVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As MarcaVehiculos) As Long Implements IPersistenciaBase(Of MarcaVehiculos).Insertar
            Return New RepositorioMarcaVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As MarcaVehiculos) As Long Implements IPersistenciaBase(Of MarcaVehiculos).Modificar
            Return New RepositorioMarcaVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As MarcaVehiculos) As MarcaVehiculos Implements IPersistenciaBase(Of MarcaVehiculos).Obtener
            Return New RepositorioMarcaVehiculos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As MarcaVehiculos) As Boolean
            Return New RepositorioMarcaVehiculos().Anular(entidad)
        End Function
    End Class

End Namespace

