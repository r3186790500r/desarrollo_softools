﻿
Print 'CREATE PROCEDURE gsp_insertar_lista_factura_empresa'
GO
DROP PROCEDURE gsp_insertar_lista_factura_empresa
GO

CREATE PROCEDURE gsp_insertar_lista_factura_empresa   
(@par_EMPR_Codigo SMALLINT,        
@par_Proveedor VARCHAR(50) = NULL,       
@par_Usuario VARCHAR(50) = NULL,       
@par_Clave VARCHAR(50) = NULL,        
@par_Operador_Virtual VARCHAR(50) = NULL,      
@par_Prefijo_Factura VARCHAR(50) = NULL,      
@par_Clave_Tecnica_Factura VARCHAR(50) = NULL,      
@par_Clave_Externa_Factura VARCHAR(50) = NULL,      
@par_Prefijo_Nota_Credito VARCHAR(50) = NULL,      
@par_Clave_Externa_Nota_Credito VARCHAR(50) = NULL
)        
AS        
BEGIN        
        
INSERT INTO [dbo].Empresa_Factura_Electronica        
           (EMPR_Codigo      
           ,Proveedor        
           ,Usuario      
           ,Clave      
           ,Operador_Virtual      
           ,Prefijo_Factura
		   ,Clave_Tecnica_Factura 
		   ,Clave_Externa_Factura
		   ,Prefijo_Nota_Credito
		   ,Clave_Externa_Nota_Credito
         )        
     VALUES        
           (@par_EMPR_Codigo       
            ,@par_Proveedor  
            ,@par_Usuario  
            ,@par_Clave  
            ,@par_Operador_Virtual  
            ,@par_Prefijo_Factura
			,@par_Clave_Tecnica_Factura
			,@par_Clave_Externa_Factura
			,@par_Prefijo_Nota_Credito
			,@par_Clave_Externa_Nota_Credito)  
      
END
GO       
