﻿Print 'gsp_generar_consulta_SIPLAFT'
GO
DROP PROCEDURE gsp_generar_consulta_SIPLAFT
GO
CREATE PROCEDURE gsp_generar_consulta_SIPLAFT (

	@par_EMPR_Codigo	smallint,
	@par_Fecha_Inicial	date,
	@par_Fecha_Final	date
)
AS
BEGIN

--1. Consulta los terceros que aplican reporte al SIPLAFT
	DECLARE @tblTerceros As TABLE (EMPR_Codigo SMALLINT, TERC_Codigo NUMERIC, Iden_Tercero VARCHAR(20), CATA_TIID_Codigo INT, Valor MONEY)

	INSERT INTO @tblTerceros
	EXEC gsp_consultar_terceros_SIPLAFT 

		@par_EMPR_Codigo	= @par_EMPR_Codigo,
		@par_Fecha_Inicial	= @par_Fecha_Inicial,
		@par_Fecha_Final	= @par_Fecha_Final

	--2. Consulta para generar el archivo SIPLAFT
	SELECT 
	 ENRE.EMPR_Codigo, 
	 DENSE_RANK() OVER (order by ENRE.Numero) As ID, 
	 ENRE.Numero_Documento As NumeroDocumentoRemesa, 
	 ENRE.Numero As NumeroRemesa, 
	 ISNULL(ENFA.Numero_Documento,0) As NumeroDocumentoFactura, 
	 ENRE.ENFA_Numero As NumeroFactura, 
	 ENRE.Fecha As FechaComprobante, 
	 ENRE.ENMC_Numero As NumeroManifiesto,      
	 CIOR.Nombre As  CIUD_Origen,
	 CIDE.Nombre As CIUD_Destino, 
	 VEHI.Placa, 
	 CASE PRVE.CATA_TIID_Codigo WHEN CONVERT(VARCHAR,101) THEN '13' WHEN CONVERT(VARCHAR,102) THEN '31' WHEN CONVERT(VARCHAR,103) THEN '22' ELSE '13' END As TipoIdenPropVehi, 
	 PRVE.Numero_Identificacion As IdenPropVehi, 
	 ISNULL(PRVE.Razon_Social,'') + ISNULL(PRVE.Nombre,'') + ' ' + ISNULL(PRVE.Apellido1,'') + ' ' + ISNULL(PRVE.Apellido2,'') As  NombPropVehi,      
	 CASE TENE.CATA_TIID_Codigo WHEN CONVERT(VARCHAR,101) THEN '13' WHEN CONVERT(VARCHAR,102) THEN '31' WHEN CONVERT(VARCHAR,103) THEN '22' ELSE '13' END As TipoIdenTeneVehi, 
	 TENE.Numero_Identificacion As  IdenTeneVehi, 
	 ISNULL(TENE.Razon_Social,'') + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') As NombTeneVehi, 
	 CASE COND.CATA_TIID_Codigo WHEN CONVERT(VARCHAR,101) THEN '13' WHEN CONVERT(VARCHAR,102) THEN '31' WHEN CONVERT(VARCHAR,103) THEN '22' ELSE '13' END As TipoIdenCondVehi, 
	 COND.Numero_Identificacion As IdenCondVehi, 
	 ISNULL(COND.Razon_Social,'') + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As NombCondVehi,      
	 ISNULL(SEMI.Placa,'') As  PlacaSemirremolque, 

	 CASE ISNULL(PRSE.CATA_TIID_Codigo,0) WHEN CONVERT(VARCHAR,101) THEN '13' WHEN CONVERT(VARCHAR,102) THEN '31' WHEN CONVERT(VARCHAR,103) THEN '22' ELSE '13' END As TipoIdenPropSemi, 
	 ISNULL(PRSE.Numero_Identificacion, '') As IdenPropSemi, 
	 ISNULL(PRSE.Razon_Social,'') + ISNULL(PRSE.Nombre,'') + ' ' + ISNULL(PRSE.Apellido1,'') + ' ' + ISNULL(PRSE.Apellido2,'') As NombPropSemi, 
	 PROD.Nombre As  NombreProducto, 
	 CASE ISNULL(CLIE.CATA_TIID_Codigo,0) WHEN CONVERT(VARCHAR,101) THEN '13' WHEN CONVERT(VARCHAR,102) THEN '31' WHEN CONVERT(VARCHAR,103) THEN '22' ELSE '13' END As TipoIdenRemi,      
	 ISNULL(CLIE.Numero_Identificacion,'') As IdenRemi, 
	 ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'') As NombreRemi, 
	 CASE ISNULL(DEST.CATA_TIID_Codigo,0) WHEN CONVERT(VARCHAR,101) THEN '13' WHEN CONVERT(VARCHAR,102) THEN '31' WHEN CONVERT(VARCHAR,103) THEN '22' ELSE '13' END As TipoIdenDesti, 
	 DEST.Numero_Identificacion As IdenDesti, 
	 ISNULL(DEST.Razon_Social,'') + ISNULL(DEST.Nombre,'') + ' ' + ISNULL(DEST.Apellido1,'') + ' ' + ISNULL(DEST.Apellido2,'') As NombreDest, 
	 ENRE.Valor_Flete_Cliente As ValorTotalViaje,
	 0 As ValorPagadoEfectivo, 
	 ENRE.Observaciones

	FROM Encabezado_Remesas ENRE

	INNER JOIN
	Ciudades CIOR ON
	ENRE.EMPR_Codigo = CIOR.EMPR_Codigo
	AND ENRE.CIUD_Codigo_Remitente = CIOR.Codigo

	INNER JOIN
	Ciudades CIDE ON
	ENRE.EMPR_Codigo = CIDE.EMPR_Codigo
	AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo

	INNER JOIN
	Producto_Transportados PROD ON
	ENRE.EMPR_Codigo = PROD.EMPR_Codigo
	AND ENRE.PRTR_Codigo = PROD.Codigo

	INNER JOIN
	Terceros CLIE ON
	ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo

	INNER JOIN
	Terceros DEST ON
	ENRE.EMPR_Codigo = DEST.EMPR_Codigo
	AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo

	INNER JOIN
	Vehiculos VEHI ON
	ENRE.EMPR_Codigo = VEHI.EMPR_Codigo
	AND ENRE.VEHI_Codigo = VEHI.Codigo

	INNER JOIN
	Terceros PRVE ON
	VEHI.EMPR_Codigo = PRVE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Propietario = PRVE.Codigo

	INNER JOIN
	Terceros TENE ON
	VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	INNER JOIN
	Terceros COND ON
	VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo

	LEFT JOIN
	Encabezado_Facturas ENFA ON
	ENRE.EMPR_Codigo = ENFA.EMPR_Codigo
	AND ENRE.ENFA_Numero = ENFA.Numero

	LEFT JOIN
	Semirremolques SEMI ON
	ENRE.EMPR_Codigo = SEMI.EMPR_Codigo
	AND ENRE.SEMI_Codigo = SEMI.Codigo

	LEFT JOIN
	Terceros PRSE ON
	SEMI.EMPR_Codigo = PRSE.EMPR_Codigo
	AND SEMI.TERC_Codigo_Propietario = PRSE.Codigo


	WHERE 
	ENRE.EMPR_Codigo = @par_EMPR_Codigo
	AND ENRE.TIDO_Codigo IN (100, 110)
	AND ENRE.Estado = 1
	AND ENRE.Anulado = 0
	AND ENRE.Fecha >= CONVERT(DATE,@par_Fecha_Inicial,101)    
	AND ENRE.Fecha <= CONVERT(DATE,@par_Fecha_Final,101)    


	AND (
		ENRE.TERC_Codigo_Cliente IN (SELECT TERC_Codigo FROM @tblTerceros)
		OR ENRE.TERC_Codigo_Cliente IN (SELECT TERC_Codigo FROM @tblTerceros)
		OR ENRE.TERC_Codigo_Destinatario IN (SELECT TERC_Codigo FROM @tblTerceros)
		OR VEHI.TERC_Codigo_Tenedor IN (SELECT TERC_Codigo FROM @tblTerceros)
	)

END
GO