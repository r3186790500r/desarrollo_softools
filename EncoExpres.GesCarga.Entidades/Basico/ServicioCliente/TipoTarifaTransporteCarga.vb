﻿Imports Newtonsoft.Json

Namespace Basico.ServicioCliente

    ''' <summary>
    ''' Clase <see cref="TipoTarifaTransporteCarga"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TipoTarifaTransporteCarga
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="TipoTarifaTransporteCarga"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoTarifaTransporteCarga"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            TarifaTransporteCarga = New TarifaTransporteCarga With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "NombreTarifa")}
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")

        End Sub
        <JsonProperty>
        Public Property TarifaTransporteCarga As TarifaTransporteCarga
        <JsonProperty>
        Public Property Nombre As String


    End Class

End Namespace
