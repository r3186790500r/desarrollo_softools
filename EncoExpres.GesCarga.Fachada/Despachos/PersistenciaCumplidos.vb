﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Repositorio.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos

Namespace Despachos
    Public NotInheritable Class PersistenciaCumplidos
        Implements IPersistenciaBase(Of Cumplido)

        Public Function Consultar(filtro As Cumplido) As IEnumerable(Of Cumplido) Implements IPersistenciaBase(Of Cumplido).Consultar
            Return New RepositorioCumplidos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Cumplido) As Long Implements IPersistenciaBase(Of Cumplido).Insertar
            Return New RepositorioCumplidos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Cumplido) As Long Implements IPersistenciaBase(Of Cumplido).Modificar
            Return New RepositorioCumplidos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Cumplido) As Cumplido Implements IPersistenciaBase(Of Cumplido).Obtener
            Return New RepositorioCumplidos().Obtener(filtro)
        End Function

        Public Function Anular(entidad As Cumplido) As Cumplido
            Return New RepositorioCumplidos().Anular(entidad)
        End Function

        Public Function ConsultarTiempos(filtro As Cumplido) As Cumplido
            Return New RepositorioCumplidos().ConsultarTiempos(filtro)
        End Function

    End Class

End Namespace

