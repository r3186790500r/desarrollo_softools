﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Listados.aspx.vb" Inherits="EncoExpres.GesCarga.ASP.Listados" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="1205000">
            </asp:ScriptManager>
            <rsweb:ReportViewer ID ="rpwDocumento" runat="server" width="769px">
            </rsweb:ReportViewer>
            <asp:Label ID="lblMensajeError" runat="server" ForeColor="Red"></asp:Label>
        </div>

    </form>
</body>
</html>
