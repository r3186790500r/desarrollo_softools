﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="PuertosPaises"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class PuertosPaises
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PuertosPaises"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PuertosPaises"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            Obtener = Read(lector, "Obtener")

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Pais = New Paises With {.Codigo = Read(lector, "PAIS_Codigo"), .Nombre = Read(lector, "Nombre_Pais")}
            NombreCiudad = Read(lector, "Nombre_Ciudad")
            Nombre = Read(lector, "Nombre")
            Contacto = Read(lector, "Contacto")
            Direccion = Read(lector, "Direccion")
            Telefono = Read(lector, "Telefono")
            Observaciones = Read(lector, "Observaciones")
            Estado = Read(lector, "Estado")
            If Obtener = 0 Then 'Consultar
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
        End Sub

        <JsonProperty>
        Public Property Pais As Paises
        <JsonProperty>
        Public Property NombreCiudad As String
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Contacto As String
        <JsonProperty>
        Public Property Direccion As String
        <JsonProperty>
        Public Property Telefono As String
        <JsonProperty>
        Public Property Observaciones As String

    End Class
End Namespace
