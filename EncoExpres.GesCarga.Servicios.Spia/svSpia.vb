﻿Imports Spia.co.com.spia
Imports System.Threading
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.IO

Public Class svSpia

#Region "Declaración de Variables"
    Dim objSpiaEvents As co.com.spia.ObtenerUbicacion
    Dim thrDespacho As Thread
    Dim intCodiEmpr As Byte
    Dim strUsuaWebServ As String
    Dim strClavWebServ As String
    Dim intDiasConsDesp As Integer
    Dim intDiasConsNove As Integer
    Dim TERCCodigoSpia As Integer
    Dim strUsrEmp As String
    Dim strPssEmpr As String
    Dim strCadenaDeConexionSQL As String
    Dim sqlConexion As SqlConnection
    Dim sqlComando As SqlCommand
    Dim strRutaArchLog As String
    Dim stwrLog As StreamWriter
    Dim intNumeMani As Integer
    Dim strPlacVehi As String
    Dim intCodiRuta As Integer
    Dim XML As String
    Dim intEstaRepo As Integer
    Dim strMensSatr As String
    Dim strNombLog As String
    Dim intTiemSuspServ As Integer

#End Region

#Region "Declaración de Constantes"
    Const NO_ANULADO = 0
    Const REPORTADO_CON_EXITO = 2
    Const REPORTADO_A_Spia = 2
    Const REPORTADO_SIN_EXITO = 1
    Const PENDIENTE_POR_REPORTAR = 0
    Const REPORTADO_CON_EXITO_Spia = 1
#End Region

#Region "Propiedades"

    Public Property CodigoEmpresa As Integer
        Get
            Return Me.intCodiEmpr
        End Get
        Set(ByVal value As Integer)
            Me.intCodiEmpr = value
        End Set
    End Property

    Public Property UsuarioWebServices As String
        Get
            Return Me.strUsuaWebServ
        End Get
        Set(ByVal value As String)
            Me.strUsuaWebServ = value
        End Set
    End Property

    Public Property ClaveWebServices As String
        Get
            Return Me.strClavWebServ
        End Get
        Set(ByVal value As String)
            Me.strClavWebServ = value
        End Set
    End Property

    Public Property CadenaConexionSQL As String
        Get
            Return Me.strCadenaDeConexionSQL
        End Get
        Set(ByVal value As String)
            Me.strCadenaDeConexionSQL = value
        End Set
    End Property

    Public Property DiasConsultarDespachos As Integer
        Get
            Return Me.intDiasConsDesp
        End Get
        Set(ByVal value As Integer)
            Me.intDiasConsDesp = value
        End Set
    End Property

    Public Property DiasConsultarNovedades As Integer
        Get
            Return Me.intDiasConsNove
        End Get
        Set(ByVal value As Integer)
            Me.intDiasConsNove = value
        End Set
    End Property

    Public Property ConexionSQL As SqlConnection
        Get
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ComandoSQL As SqlCommand
        Get
            Return Me.sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComando = value
        End Set
    End Property

    Public Property RutaArchivoLog As String
        Get
            Return Me.strRutaArchLog
        End Get
        Set(ByVal value As String)
            Me.strRutaArchLog = value
        End Set
    End Property

    Public Property ArchivoLog As StreamWriter
        Get
            Return Me.stwrLog
        End Get
        Set(ByVal value As StreamWriter)
            Me.stwrLog = value
        End Set
    End Property

    Public Property NombreLog() As String
        Get
            Return strNombLog
        End Get
        Set(ByVal value As String)
            strNombLog = value
        End Set
    End Property

    Public Property TiempoSuspencionServicio() As Integer
        Get
            Return intTiemSuspServ
        End Get
        Set(ByVal value As Integer)
            intTiemSuspServ = value
        End Set
    End Property

    Public Property Manifiesto As Integer
        Get
            Return Me.intNumeMani
        End Get
        Set(ByVal value As Integer)
            Me.intNumeMani = value
        End Set
    End Property

    Public Property PlacaVehiculo As String
        Get
            Return Me.strPlacVehi
        End Get
        Set(ByVal value As String)
            Me.strPlacVehi = value
        End Set
    End Property

    Public Property CodigoRuta As Integer
        Get
            Return Me.intCodiRuta
        End Get
        Set(ByVal value As Integer)
            Me.intCodiRuta = value
        End Set
    End Property

    Public Property EstadoReporte As Integer
        Get
            Return Me.intEstaRepo
        End Get
        Set(ByVal value As Integer)
            Me.intEstaRepo = value
        End Set
    End Property

    Public Property MensajeSpia As String
        Get
            Return Me.strMensSatr
        End Get
        Set(ByVal value As String)
            Me.strMensSatr = value
        End Set
    End Property

#End Region

    Private Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                ConexionSQL.Open()
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try

    End Function

#Region "Funcionamiento del Servicio"

#Region "Proceso Despacho"

    Sub New()
        InitializeComponent()
        'Me.thrDespacho = New Thread(AddressOf Iniciar_Proceso_Despacho)
        'Me.thrDespacho.Start()
    End Sub

    Private Sub Iniciar_Proceso_Despacho()
        Try
            Me.objSpiaEvents = New co.com.spia.ObtenerUbicacion
            While (True)
                Dim ArchivoLog As FileStream
                If Cargar_Valores_AppConfig() Then
                    If Not My.Computer.FileSystem.DirectoryExists(Me.RutaArchivoLog) Then
                        My.Computer.FileSystem.CreateDirectory(Me.RutaArchivoLog)
                        ArchivoLog = File.Create(Me.RutaArchivoLog & Me.NombreLog)
                        ArchivoLog.Close()
                    Else
                        If Not My.Computer.FileSystem.FileExists(Me.RutaArchivoLog & Me.NombreLog) Then
                            ArchivoLog = File.Create(Me.RutaArchivoLog & Me.NombreLog)
                            ArchivoLog.Close()
                        End If
                    End If
                    Consultar_Seguimientos_Spia()
                End If
                Thread.Sleep(Val(Me.TiempoSuspencionServicio))
            End While
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaArchivoLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: " & ex.Message)
            Me.ArchivoLog.Close()
        End Try
    End Sub

    Function Cargar_Valores_AppConfig() As Boolean
        Try
            Me.CodigoEmpresa = AppSettings.Get("Empresa")
            Me.UsuarioWebServices = AppSettings.Get("UsuarioSpia")
            Me.ClaveWebServices = AppSettings.Get("ClaveSpia")
            Me.DiasConsultarDespachos = AppSettings.Get("DiasCrearDespacho")
            Me.DiasConsultarNovedades = AppSettings.Get("DiasConsultaNovedades")
            Me.CadenaConexionSQL = AppSettings.Get("ConexionSQLGestrans")
            Me.RutaArchivoLog = AppSettings.Get("RutaArchivoLog")
            Me.TiempoSuspencionServicio = AppSettings.Get("TiempoSuspencionProcesoDespacho")
            Me.NombreLog = AppSettings.Get("NombreLog")
            Me.TERCCodigoSpia = AppSettings.Get("TERC_Codigo_Spia")
            Me.strUsrEmp = AppSettings.Get("Usr_Emp")
            Me.strPssEmpr = AppSettings.Get("Pss_Empr")
            Cargar_Valores_AppConfig = True
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaArchivoLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & " Resultado: " & ex.Message)
            Me.ArchivoLog.Close()
            Cargar_Valores_AppConfig = False
        End Try
    End Function

    Private Sub Actualizar_Datos_Spia(ByVal Manifiesto As Integer)
        Try
            Me.ComandoSQL = New SqlCommand("sp_actualizar_datos_Spia", Me.ConexionSQL)
            Me.ComandoSQL.CommandType = CommandType.StoredProcedure

            Me.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.CodigoEmpresa
            Me.ComandoSQL.Parameters.Add("@par_Numero", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_Numero").Value = Me.Manifiesto
            Me.ComandoSQL.Parameters.Add("@par_ReporteSatelital", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_ReporteSatelital").Value = Me.EstadoReporte
            Me.ComandoSQL.Parameters.Add("@par_MensajeReporteSatelital", SqlDbType.VarChar, 100) : Me.ComandoSQL.Parameters("@par_MensajeReporteSatelital").Value = Me.MensajeSpia

            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
                If Me.ComandoSQL.ExecuteNonQuery = 1 Then
                    If EstadoReporte = 0 Then
                        Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaArchivoLog & Me.NombreLog, True)
                        Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: El manifiesto: " & Me.Manifiesto & " presenta el siguiente error: " & Me.MensajeSpia)
                        Me.ArchivoLog.Close()
                    ElseIf EstadoReporte = 1 Then
                        Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaArchivoLog & Me.NombreLog, True)
                        Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: Manifiesto " & Me.Manifiesto & " reportado correctamente con la siguiente respuesta: " & Me.MensajeSpia)
                        Me.ArchivoLog.Close()
                    End If
                End If
                Me.ConexionSQL.Close()
            End If
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaArchivoLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & " Resultado: " & ex.Message)
            Me.ArchivoLog.Close()
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Sub


    Private Sub Consultar_Seguimientos_Spia()
        Try

            Dim strSQL
            Dim dsDataSet As New DataSet
            Dim dsRetornoSpia As New DataSet
            Dim strRetornoSpia As String = ""
            Dim objUbicacion As New co.com.spia.ubicacion
            Dim strPlaca As String = String.Empty
            Dim strUsuarioGPS As String = String.Empty
            Dim strPasswordGPS As String = String.Empty
            Dim intPlanilla As Integer = 0


            strSQL = "EXEC gsp_consultar_vehiculos_seguimiento_reporte_gps "
            strSQL += Me.CodigoEmpresa.ToString()
            strSQL += "," & Me.TERCCodigoSpia.ToString()

            Me.ConexionSQL = New SqlConnection(Me.CadenaConexionSQL)
            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            dsDataSet = Me.Retorna_Dataset(strSQL)
            If dsDataSet.Tables(0).Rows.Count > 0 Then
                For Each Registro As DataRow In dsDataSet.Tables(0).Rows
                    strPlaca = Registro.Item(0).ToString
                    strUsuarioGPS = Registro.Item(1).ToString
                    strPasswordGPS = Registro.Item(2).ToString
                    intPlanilla = Registro.Item(3).ToString

                    objUbicacion = Me.objSpiaEvents.CallObtenerUbicacion(strUsrEmp, strPssEmpr, strPlaca, strPasswordGPS)
                    If Not IsNothing(objUbicacion) Then
                        InsertarSeguimientoGPS(objUbicacion, intPlanilla)
                    End If

                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub InsertarSeguimientoGPS(objSpiaEvent As ubicacion, Planilla As Integer)
        Try

            Dim FechaReporte As Date
            Dim Evento As String = String.Empty
            Dim Placa As String = String.Empty
            Dim Velocidad As String = String.Empty
            Dim Ubicacion As String = String.Empty
            Dim Latitud As String = String.Empty
            Dim Longitud As String = String.Empty
            Dim dblLatitud As Double = 0
            Dim dblLongitud As Double = 0
            'Estructura de retorno Spia
            '(0) placa
            '(1) Fecha Sistema
            '(2) Fecha GPS
            '(3) Evento / Prioridad
            '(4) Velocidad y Sentido
            '(5) Edad Posición
            '(6) Ubicación
            '(7) Longitud
            '(8) Latitud

            If objSpiaEvent.longitud <> "" And objSpiaEvent.latitud <> "" Then
                Placa = objSpiaEvent.placa
                FechaReporte = objSpiaEvent.fechagps
                Evento = objSpiaEvent.estado
                Velocidad = objSpiaEvent.velocidad
                Ubicacion = objSpiaEvent.direccion
                Longitud = Math.Round(CDbl(Val(objSpiaEvent.longitud)), 10).ToString
                Latitud = Math.Round(CDbl(Val(objSpiaEvent.latitud)), 10).ToString

                If Me.ConexionSQL.State = ConnectionState.Open Then
                    Me.ConexionSQL.Close()
                Else
                    Me.ConexionSQL.Open()
                    Me.ComandoSQL = New SqlCommand("gsp_insertar_seguimientos_empresa_gps", Me.ConexionSQL)
                    Me.ComandoSQL.CommandType = CommandType.StoredProcedure

                    Me.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.CodigoEmpresa
                    Me.ComandoSQL.Parameters.Add("@par_Fecha_Reporte", SqlDbType.DateTime) : Me.ComandoSQL.Parameters("@par_Fecha_Reporte").Value = Convert.ToDateTime(FechaReporte)
                    Me.ComandoSQL.Parameters.Add("@par_VEHI_Placa", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_VEHI_Placa").Value = Placa
                    Me.ComandoSQL.Parameters.Add("@par_Nombre_Evento", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_Nombre_Evento").Value = Evento
                    Me.ComandoSQL.Parameters.Add("@par_Latitud", SqlDbType.Money) : Me.ComandoSQL.Parameters("@par_Latitud").Value = Latitud
                    Me.ComandoSQL.Parameters.Add("@par_Longitud", SqlDbType.Money) : Me.ComandoSQL.Parameters("@par_Longitud").Value = Longitud
                    Me.ComandoSQL.Parameters.Add("@par_Velocidad", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_Velocidad").Value = Velocidad
                    Me.ComandoSQL.Parameters.Add("@par_Mensaje_Evento", SqlDbType.VarChar, 50) : Me.ComandoSQL.Parameters("@par_Mensaje_Evento").Value = Ubicacion
                    Me.ComandoSQL.Parameters.Add("@par_ENPD_Numero", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_ENPD_Numero").Value = Planilla

                    If Me.ComandoSQL.ExecuteNonQuery = 1 Then
                        Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaArchivoLog & Me.NombreLog, True)
                        Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: Ubicacion del vehiculo: " & Placa & " , Latitud: " & Latitud & " , Longitud: " & Longitud & " , Evento: " & Evento)
                        Me.ArchivoLog.Close()
                    Else
                        Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaArchivoLog & Me.NombreLog, True)
                        Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: La ubicacion del vehiculo: " & Placa & " , No se pudo guardar ")
                        Me.ArchivoLog.Close()
                    End If
                    Me.ConexionSQL.Close()
                End If
            End If
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaArchivoLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & " Resultado: " & ex.Message)
            Me.ArchivoLog.Close()
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Sub


#End Region

#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)

        Me.thrDespacho = New Thread(AddressOf Iniciar_Proceso_Despacho)
        Me.thrDespacho.Start()
    End Sub

    Protected Overrides Sub OnStop()
        Me.thrDespacho.Abort()
    End Sub

End Class
