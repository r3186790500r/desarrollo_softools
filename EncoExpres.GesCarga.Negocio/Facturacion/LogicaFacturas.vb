﻿Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Fachada.Facturacion

Namespace Facturacion
    Public NotInheritable Class LogicaFacturas
        Inherits LogicaBase(Of Facturas)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Facturas)

        Sub New(persistencia As IPersistenciaBase(Of Facturas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Facturas) As Respuesta(Of IEnumerable(Of Facturas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Facturas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Facturas))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Facturas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Numero, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Facturas) As Respuesta(Of Facturas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Facturas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Facturas)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As Facturas) As Respuesta(Of Facturas)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Facturas) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Facturas) With {.ProcesoExitoso = True}

        End Function

        Public Function Anular(entidad As Facturas) As Respuesta(Of Boolean)
            Dim anulo As Boolean = False
            Dim mensajeAnulacionFallida = Recursos.FalloOperacionAnularRegistro
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            anulo = CType(_persistencia, PersistenciaFacturas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeAnulacionFallida, entidad.Numero, Recursos.OperacionAnulo)}
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function ObtenerDatosFacturaElectronicaSaphety(filtro As Facturas) As Respuesta(Of Facturas)
            Dim consulta As Facturas
            consulta = New PersistenciaFacturas().ObtenerDatosFacturaElectronicaSaphety(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Facturas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Facturas)(consulta)
        End Function

        Public Function GuardarFacturaElectronica(entidad As Facturas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long
            valor = New PersistenciaFacturas().GuardarFacturaElectronica(entidad)

            If valor.Equals(0) Or Len(Trim(entidad.MensajeSQL)) > 0 Then
                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If

            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function ObtenerDocumentoReporteSaphety(filtro As Facturas) As Respuesta(Of Facturas)
            Dim consulta As Facturas
            consulta = New PersistenciaFacturas().ObtenerDocumentoReporteSaphety(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Facturas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Facturas)(consulta)
        End Function

    End Class
End Namespace