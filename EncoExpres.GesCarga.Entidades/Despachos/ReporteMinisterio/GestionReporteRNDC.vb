﻿Imports System.Xml
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Procesos.General

Namespace Despachos.Procesos
    Public Class GestionReporteRNDC
#Region "Propiedades"

        Public Tercero As TerceroRNDC
        Public Vehiculo As VehiculoRNDC
        Public Manifiesto As ManifiestoRNDC
        Public AceptacionElectronica As AceptacionElectronicaRNDC

        Public Codigo As Long

        Dim strSQL As String
        Dim solicitud As Solicitud
        Dim objGeneral As General
        Dim acceso As Acceso
        Dim wsManifiestoElectronico As ManifiestoElectronico.IBPMServicesservice
        Dim strRespuesta As String
        'Dim objInterfaz As InterfazMinisterioTransporte

        Public Property EnlaceMinisterio As String
            Get
                Return Me.wsManifiestoElectronico.Url
            End Get
            Set(value As String)
                Me.wsManifiestoElectronico.Url = value
            End Set
        End Property

#End Region

#Region "Constantes"
        Public Const NOMBRE_TAG As String = "variables"
        Public Const NOMBRE_TAG_CONSULTA As String = "documento"
        Public Const TIPO_IDENTIFICACION_NIT As String = "N"
        Public Const TIPO_IDENTIFICACION_CEDULA As String = "C"
        Public Const CODIGO_SEDE_DEFECTO As String = "0"
        Public Const APELLIDO_X_DEFECTO As String = "APELLIDO"
        Public Const DIRECCION_X_DEFECTO As String = "(NO APLICA)"
        Public Const TELEFONO_X_DEFECTO As String = "1234567"
        Public Const CELULAR_X_DEFECTO As String = "0123456789"
        Public Const DIGITOS_TELEFONO As Int16 = 7
        Public Const DIGITOS_CELULAR As Int16 = 10
        Public Const ERROR_TERCERO_DUPLICADO As String = "TER015" ' error Duplicado Tercero en el RNDC
        Public Const ERROR_TERCERO_VARIABLE_DESCONOCIDA As String = "Campo no encontrado: MANIFIESTOTRASBORDOID 2" ' error Duplicado Tercero en el RNDC
        Public Const CAMPO_CEROS_DERECHA As Integer = 5
#End Region

#Region "Constructor"
        Public Sub New(ByVal acceso As Acceso, ByVal solicitud As Solicitud, ByVal tercero As TerceroRNDC, ByVal Empresa As Empresas)
            Me.solicitud = New Solicitud
            Me.acceso = New Acceso
            objGeneral = New General
            Me.Tercero = tercero
            AjustarValoresParaMinisterioTercero()

            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
            Me.solicitud = solicitud
            Me.acceso = acceso
            Me.EnlaceMinisterio = Empresa.ListadoRNDC.EnlaceWSMinisterio
            'Me.objInterfaz = New InterfazMinisterioTransporte()
        End Sub
        Public Sub New(ByVal acceso As Acceso, ByVal solicitud As Solicitud, ByVal Vehiculo As VehiculoRNDC, ByVal Empresa As Empresas)
            Me.solicitud = New Solicitud
            Me.acceso = New Acceso
            objGeneral = New General
            Me.Vehiculo = Vehiculo
            AjustarValoresParaMinisterioVehiuclo()

            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
            Me.solicitud = solicitud
            Me.acceso = acceso
            Me.EnlaceMinisterio = Empresa.ListadoRNDC.EnlaceWSMinisterio
            'Me.objInterfaz = New InterfazMinisterioTransporte()
        End Sub
        Public Sub New(ByVal acceso As Acceso, ByVal solicitud As Solicitud, ByVal Manifiesto As ManifiestoRNDC, ByVal Empresa As Empresas)
            Me.solicitud = New Solicitud
            Me.acceso = New Acceso
            objGeneral = New General
            Me.Manifiesto = Manifiesto
            AjustarValoresParaMinisterioVehiuclo()

            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
            Me.solicitud = solicitud
            Me.acceso = acceso
            Me.EnlaceMinisterio = Empresa.ListadoRNDC.EnlaceWSMinisterio
            'Me.objInterfaz = New InterfazMinisterioTransporte()
        End Sub

        Public Sub New(ByVal acceso As Acceso, ByVal solicitud As Solicitud, ByVal Manifiesto As AceptacionElectronicaRNDC, ByVal Empresa As Empresas)
            Me.solicitud = New Solicitud
            Me.acceso = New Acceso
            objGeneral = New General
            Me.AceptacionElectronica = Manifiesto
            'AjustarValoresParaMinisterioVehiuclo()

            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
            Me.solicitud = solicitud
            Me.acceso = acceso
            Me.EnlaceMinisterio = Empresa.ListadoRNDC.EnlaceWSMinisterio
            'Me.objInterfaz = New InterfazMinisterioTransporte()
        End Sub
        Sub New()
            solicitud = New Solicitud
            acceso = New Acceso
            objGeneral = New General
            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
            'Me.objInterfaz = New InterfazMinisterioTransporte()
        End Sub
#End Region

#Region "Funciones Publicas"

        Public Sub AjustarValoresParaMinisterioTercero()
            'validacion telefono
            If Tercero.NUMTELEFONOCONTACTO = String.Empty Or Len(Tercero.NUMTELEFONOCONTACTO) < DIGITOS_TELEFONO Then
                Tercero.NUMTELEFONOCONTACTO = TELEFONO_X_DEFECTO
            Else
                Tercero.NUMTELEFONOCONTACTO = Replace(Tercero.NUMTELEFONOCONTACTO, " ", "")
                Tercero.NUMTELEFONOCONTACTO = Mid(Tercero.NUMTELEFONOCONTACTO, General.UNO, DIGITOS_TELEFONO)
            End If
            'validacion celular
            If Tercero.NUMCELULARPERSONA = String.Empty Or Len(Tercero.NUMCELULARPERSONA) < DIGITOS_CELULAR Then
                Tercero.NUMCELULARPERSONA = TELEFONO_X_DEFECTO
            Else
                Tercero.NUMCELULARPERSONA = Replace(Tercero.NUMCELULARPERSONA, " ", "")
                Tercero.NUMCELULARPERSONA = Mid(Tercero.NUMCELULARPERSONA, General.UNO, DIGITOS_CELULAR)
            End If
            'validacion direccion
            If Tercero.NOMENCLATURADIRECCION = String.Empty Then
                Tercero.NOMENCLATURADIRECCION = DIRECCION_X_DEFECTO
            End If
            'validacion Ciudad(Municipio)
            Tercero.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(Tercero.CODMUNICIPIORNDC, 5, General.CAMPO_CEROS_IZQUIERDA)
            'validacion Ciudad(Municipio)
            Tercero.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(Tercero.CODMUNICIPIORNDC, 8, General.CAMPO_CEROS_DERECHA)
            'validacion Ciudad(sede)
            Tercero.CODSEDETERCERO = objGeneral.FormCampMiniTran(Mid(Tercero.CODSEDETERCERO, 1, 6), 5, General.CAMPO_CEROS_IZQUIERDA)
        End Sub
        Public Sub AjustarValoresParaMinisterioVehiuclo()

        End Sub

        Public Sub AdicionarNodoXml(ByRef XmlDocumento As XmlDocument, ByRef xmlElementoRoot As XmlElement, Elemento As String, ElementoNodo As String)
            xmlElementoRoot.LastChild.AppendChild(XmlDocumento.CreateElement(Elemento))
            xmlElementoRoot.LastChild.LastChild.AppendChild(XmlDocumento.CreateTextNode(ElementoNodo))
        End Sub

        Public Sub AdicionarValorXml(ByRef XmlDocumento As XmlDocument, ByRef xmlElementoRoot As XmlElement, Elemento As String, ElementoNodo As String)
            'xmlElementoRoot.LastChild.AppendChild(XmlDocumento.CreateElement(Elemento))
            xmlElementoRoot.LastChild.AppendChild(XmlDocumento.CreateTextNode(ElementoNodo))
        End Sub

        Public Sub Crear_XML_Tercero(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByVal lstVariables As List(Of String), ByVal Tipos_Rol As Tipos_Roles)
            Try
                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Tercero.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODTIPOIDTERCERO", Tercero.CODTIPOIDTERCERO)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NOMIDTERCERO", Tercero.NOMIDTERCERO)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDTERCERO", Tercero.NUMIDTERCERO + Tercero.DIGITOCHEQUEO)
                        If Tercero.PRIMERAPELLIDOIDTERCERO <> String.Empty And Tercero.CODTIPOIDTERCERO <> "N" Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "PRIMERAPELLIDOIDTERCERO", Tercero.PRIMERAPELLIDOIDTERCERO)
                        End If
                        If Tercero.SEGUNDOAPELLIDOIDTERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "SEGUNDOAPELLIDOIDTERCERO", Tercero.SEGUNDOAPELLIDOIDTERCERO)
                        End If
                        Tercero.NUMTELEFONOCONTACTO = Replace(Tercero.NUMTELEFONOCONTACTO, "-", "") ' El teléfono debe ir solo sin espacios ni nada
                        Tercero.NUMTELEFONOCONTACTO = Replace(Tercero.NUMTELEFONOCONTACTO, " ", "")
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMTELEFONOCONTACTO", Tercero.NUMTELEFONOCONTACTO)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NOMENCLATURADIRECCION", Tercero.NOMENCLATURADIRECCION)
                        If Len(Tercero.CODMUNICIPIORNDC) > 0 Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODMUNICIPIORNDC", Tercero.CODMUNICIPIORNDC)
                        End If
                        If Tercero.CODSEDETERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODSEDETERCERO", Tercero.CODSEDETERCERO)
                        End If
                        If Tercero.NOMSEDETERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NOMSEDETERCERO", Tercero.NOMSEDETERCERO)
                        End If
                        If Tercero.NUMLICENCIACONDUCCION <> String.Empty Then
                            If Len(Tercero.NUMLICENCIACONDUCCION) > 15 Then
                                Tercero.NUMLICENCIACONDUCCION = Mid(Tercero.NUMLICENCIACONDUCCION, Len(Tercero.NUMLICENCIACONDUCCION) - 15, 15)
                            End If
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMLICENCIACONDUCCION", Tercero.NUMLICENCIACONDUCCION)
                            If Tercero.CODCATEGORIALICENCIACONDUCCION <> String.Empty Then
                                AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODCATEGORIALICENCIACONDUCCION", Tercero.CODCATEGORIALICENCIACONDUCCION)
                            End If
                            If Tercero.FECHAVENCIMIENTOLICENCIA <> String.Empty Then
                                AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHAVENCIMIENTOLICENCIA", Format(Date.Parse(Tercero.FECHAVENCIMIENTOLICENCIA), "dd/MM/yyyy"))
                            End If
                        End If
                    Case Tipos_Procesos.FORTMATO_CONSULTA
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariables
                            If strAxuliarCampos = String.Empty Then
                                strAxuliarCampos = Items
                            Else
                                strAxuliarCampos &= "," & Items
                            End If
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, NOMBRE_TAG_CONSULTA, "NUMIDTERCERO" & strAxuliarCampos)
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Tercero.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODTIPOIDTERCERO", "'" & Tercero.CODTIPOIDTERCERO & "'")
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDTERCERO", "'" & Tercero.NUMIDTERCERO & "'")
                        If Tipos_Rol <> Tipos_Roles.CONDUCTOR Then
                            If Tercero.CODSEDETERCERO <> String.Empty Then
                                AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODSEDETERCERO", Tercero.CODSEDETERCERO)
                            End If
                        End If

                End Select
            Catch ex As Exception
            End Try
        End Sub
        Public Sub Crear_XML_Vehiculo(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByVal lstVariables As List(Of String))
            Try
                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Vehiculo.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMPLACA", Vehiculo.NUMPLACA)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODCONFIGURACIONUNIDADCARGA", Vehiculo.CODCONFIGURACIONUNIDADCARGA)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODMARCAVEHICULOCARGA", Vehiculo.CODMARCAVEHICULOCARGA)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODLINEAVEHICULOCARGA", Vehiculo.CODLINEAVEHICULOCARGA)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "ANOFABRICACIONVEHICULOCARGA", Vehiculo.ANOFABRICACIONVEHICULOCARGA)

                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODTIPOIDPROPIETARIO", Vehiculo.CODTIPOIDPROPIETARIO)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDPROPIETARIO", Vehiculo.NUMIDPROPIETARIO)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODTIPOIDTENEDOR", Vehiculo.CODTIPOIDTENEDOR)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDTENEDOR", Vehiculo.NUMIDTENEDOR)

                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODTIPOCOMBUSTIBLE", 1) 'Opcional
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "PESOVEHICULOVACIO", Vehiculo.PESOVEHICULOVACIO)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "UNIDADMEDIDACAPACIDAD", Vehiculo.UNIDADMEDIDACAPACIDAD)
                        If Vehiculo.CODCOLORVEHICULOCARGA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODCOLORVEHICULOCARGA", Vehiculo.CODCOLORVEHICULOCARGA) 'Opcional
                        End If
                        'If Vehiculo.CODTIPOCARROCERIA <> String.Empty Then
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODTIPOCARROCERIA", 1) 'Opcional
                        'End If
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITASEGURADORASOAT", Vehiculo.NUMNITASEGURADORASOAT) ''
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHAVENCIMIENTOSOAT", Format(Date.Parse(Vehiculo.FECHAVENCIMIENTOSOAT), "dd/MM/yyyy")) ''
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMSEGUROSOAT", Vehiculo.NUMSEGUROSOAT) ''
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMCHASIS", Vehiculo.NUMCHASIS)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CAPACIDADUNIDADCARGA", Vehiculo.CAPACIDADUNIDADCARGA)

                    Case Tipos_Procesos.FORTMATO_CONSULTA
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariables
                            If strAxuliarCampos = String.Empty Then
                                strAxuliarCampos = Items
                            Else
                                strAxuliarCampos &= "," & Items
                            End If
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        'AdicionarNodoXml(XmlDocumento, xmlElementoRoot, NOMBRE_TAG_CONSULTA, NOMBRE_TAG_CONSULTA)
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Vehiculo.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMPLACA", "'" & Vehiculo.NUMPLACA & "'")


                End Select
            Catch ex As Exception
            End Try
        End Sub
        Public Sub Crear_XML_Manifiesto(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByVal lstVariables As List(Of String))
            Try
                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Manifiesto.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMMANIFIESTOCARGA", Manifiesto.NUMMANIFIESTOCARGA)
                        'AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CONSECUTIVOINFORMACIONVIAJE", Manifiesto.CONSECUTIVOINFORMACIONVIAJE)
                        If Len(Manifiesto.MANNROMANIFIESTOTRANSBORDO) > 0 Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "MANNROMANIFIESTOTRANSBORDO", Manifiesto.MANNROMANIFIESTOTRANSBORDO)
                        End If
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODOPERACIONTRANSPORTE", Manifiesto.CODOPERACIONTRANSPORTE)
                        If Manifiesto.CODOPERACIONTRANSPORTE = "D" Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "VIAJESDIA", Manifiesto.VIAJESDIA.ToString)
                        End If
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHAEXPEDICIONMANIFIESTO", Format(Date.Parse(Manifiesto.FECHAEXPEDICIONMANIFIESTO), "dd/MM/yyyy"))
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODMUNICIPIOORIGENMANIFIESTO", Manifiesto.CODMUNICIPIOORIGENMANIFIESTO)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODMUNICIPIODESTINOMANIFIESTO", Manifiesto.CODMUNICIPIODESTINOMANIFIESTO)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODIDTITULARMANIFIESTO", Manifiesto.CODIDTITULARMANIFIESTO) ''TENEDOR
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDTITULARMANIFIESTO", Manifiesto.NUMIDTITULARMANIFIESTO) ''TENEDOR
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMPLACA", Manifiesto.NUMPLACA)
                        If Manifiesto.NUMPLACAREMOLQUE <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMPLACAREMOLQUE", Manifiesto.NUMPLACAREMOLQUE)
                        End If
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODIDCONDUCTOR", Manifiesto.CODIDCONDUCTOR)
                        'AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDCONDUCTOR", "1012449647")
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDCONDUCTOR", Manifiesto.NUMIDCONDUCTOR)
                        If Manifiesto.NUMIDCONDUCTOR2 <> "0" Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODIDCONDUCTOR2", Manifiesto.CODIDCONDUCTOR2)
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDCONDUCTOR2", Manifiesto.NUMIDCONDUCTOR2)
                        End If
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "VALORFLETEPACTADOVIAJE", Integer.Parse(Double.Parse(Manifiesto.VALORFLETEPACTADOVIAJE)))
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "RETENCIONICAMANIFIESTOCARGA", Replace(If(Not IsNothing(Manifiesto.RETENCIONICAMANIFIESTOCARGA), Manifiesto.RETENCIONICAMANIFIESTOCARGA, 0), ",", "."))
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "VALORANTICIPOMANIFIESTO", Integer.Parse(Double.Parse(Manifiesto.VALORANTICIPOMANIFIESTO)))
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "RETENCIONFUENTEMANIFIESTO", Integer.Parse(If(Not IsNothing(Manifiesto.RETENCIONFUENTEMANIFIESTO), Double.Parse(Manifiesto.RETENCIONFUENTEMANIFIESTO), 0)))
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODMUNICIPIOPAGOSALDO", Manifiesto.CODMUNICIPIOPAGOSALDO)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODRESPONSABLEPAGOCARGUE", Manifiesto.CODRESPONSABLEPAGOCARGUE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODRESPONSABLEPAGODESCARGUE", Manifiesto.CODRESPONSABLEPAGODESCARGUE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHAPAGOSALDOMANIFIESTO", Format(Date.Parse(Manifiesto.FECHAPAGOSALDOMANIFIESTO), "dd/MM/yyyy"))
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "OBSERVACIONES", "Observaciones")
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "ACEPTACIONELECTRONICA", Manifiesto.ACEPTACIONELECTRONICA)

                        NuevoElemento = XmlDocumento.CreateElement("REMESASMAN")
                        Dim xmlAtributo As XmlAttribute = XmlDocumento.CreateAttribute("procesoid")
                        xmlAtributo.Value = "43"
                        NuevoElemento.Attributes.Append(xmlAtributo)
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        Dim cont As Short = 0
                        Dim txtNodo As XmlText
                        While cont <> Manifiesto.CONSECUTIVOREMESA.Length
                            NuevoElemento = XmlDocumento.CreateElement("REMESA")
                            xmlElementoRoot.LastChild.LastChild.AppendChild(NuevoElemento)
                            NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
                            txtNodo = XmlDocumento.CreateTextNode(Manifiesto.CONSECUTIVOREMESA(cont))
                            xmlElementoRoot.LastChild.LastChild.LastChild.AppendChild(NuevoElemento)
                            xmlElementoRoot.LastChild.LastChild.LastChild.LastChild.AppendChild(txtNodo)
                            cont = cont + 1
                        End While

                    Case Tipos_Procesos.FORTMATO_CONSULTA
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariables
                            If strAxuliarCampos = String.Empty Then
                                strAxuliarCampos = Items
                            Else
                                strAxuliarCampos &= "," & Items
                            End If
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        'AdicionarNodoXml(XmlDocumento, xmlElementoRoot, NOMBRE_TAG_CONSULTA, NOMBRE_TAG_CONSULTA)
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Manifiesto.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMPLACA", "'" & Manifiesto.NUMPLACA & "'")


                End Select
            Catch ex As Exception
            End Try
        End Sub

        Public Sub Crear_XML_Consulta_Aceptacion_Electronica(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByVal lstVariables As List(Of String))
            Try
                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement



                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO



                    Case Tipos_Procesos.FORTMATO_CONSULTA
                        'Dim strAxuliarCampos As String = String.Empty
                        'For Each Items As String In lstVariables
                        '    If strAxuliarCampos = String.Empty Then
                        '        strAxuliarCampos = Items
                        '    Else
                        '        strAxuliarCampos &= "," & Items
                        '    End If
                        'Next
                        'If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos

                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
                        Dim NuevoElementoVariables As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                        xmlElementoRoot.AppendChild(NuevoElementoVariables)
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        'AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "INGRESOID,FECHAING,NUMNITEMPRESATRANSPORTE,INGRESOIDMANIFIESTO,TIPO,CODIDCONDUCTOR,NUMIDCONDUCTOR,CODIDTITULARMANIFIESTO,NUMIDTITULARMANIFIESTO,OBSERVACION", "")

                        AdicionarValorXml(XmlDocumento, xmlElementoRoot, NOMBRE_TAG, "INGRESOID,FECHAING,NUMNITEMPRESATRANSPORTE,INGRESOIDMANIFIESTO,TIPO,CODIDCONDUCTOR,NUMIDCONDUCTOR,CODIDTITULARMANIFIESTO,NUMIDTITULARMANIFIESTO,OBSERVACION")

                        Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
                        xmlElementoRoot.AppendChild(NuevoElemento)
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", AceptacionElectronica.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "INGRESOIDMANIFIESTO", "'" & AceptacionElectronica.INGRESOIDMANIFIESTO & "'")

                End Select
            Catch ex As Exception
            End Try
        End Sub

        Public Sub Crear_XML_Cimplido_Manifiesto(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByVal lstVariables As List(Of String))
            Try
                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Manifiesto.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMMANIFIESTOCARGA", Manifiesto.NUMMANIFIESTOCARGA)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "TIPOCUMPLIDOMANIFIESTO", Manifiesto.TIPOCUMPLIDOMANIFIESTO)
                        If Manifiesto.TIPOCUMPLIDOMANIFIESTO = "S" Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "MOTIVOSUSPENSIONMANIFIESTO", Manifiesto.MOTIVOSUSPENSIONMANIFIESTO)
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CONSECUENCIASUSPENSION", Manifiesto.CONSECUENCIASUSPENSIONMANIFIESTO)
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "VALORDESCUENTOFLETE", Manifiesto.VALORDESCUENTOFLETE)
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "MOTIVOVALORDESCUENTOMANIFIESTO", Manifiesto.MOTIVOVALORDESCUENTOMANIFIESTO)
                        End If
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHAENTREGADOCUMENTOS", Format(Date.Parse(Manifiesto.FECHAENTREGADOCUMENTOS), "dd/MM/yyyy"))
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "VALORADICIONALHORASCARGUE", Manifiesto.VALORADICIONALHORASCARGUE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "VALORADICIONALHORASDESCARGUE", Manifiesto.VALORADICIONALHORASDESCARGUE)
                    Case Tipos_Procesos.FORTMATO_CONSULTA
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariables
                            If strAxuliarCampos = String.Empty Then
                                strAxuliarCampos = Items
                            Else
                                strAxuliarCampos &= "," & Items
                            End If
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        'AdicionarNodoXml(XmlDocumento, xmlElementoRoot, NOMBRE_TAG_CONSULTA, NOMBRE_TAG_CONSULTA)
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Manifiesto.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMPLACA", "'" & Manifiesto.NUMPLACA & "'")
                End Select
            Catch ex As Exception
            End Try
        End Sub
        Public Sub Crear_XML_Anulacion_Cumplido_Manifiesto(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByVal lstVariables As List(Of String))
            Try
                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Manifiesto.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMMANIFIESTOCARGA", Manifiesto.NUMMANIFIESTOCARGA)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODMOTIVOANULACIONCUMPLIDO", "D")

                    Case Tipos_Procesos.FORTMATO_CONSULTA
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariables
                            If strAxuliarCampos = String.Empty Then
                                strAxuliarCampos = Items
                            Else
                                strAxuliarCampos &= "," & Items
                            End If
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        'AdicionarNodoXml(XmlDocumento, xmlElementoRoot, NOMBRE_TAG_CONSULTA, NOMBRE_TAG_CONSULTA)
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Manifiesto.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMPLACA", "'" & Manifiesto.NUMPLACA & "'")


                End Select
            Catch ex As Exception
            End Try
        End Sub
        Public Sub Crear_XML_Anulacion_Manifiesto(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByVal lstVariables As List(Of String))
            Try
                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Manifiesto.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMMANIFIESTOCARGA", Manifiesto.NUMMANIFIESTOCARGA)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "MOTIVOANULACIONMANIFIESTO", "D")
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "OBSERVACIONES", "DATOS ERRONEOS")

                    Case Tipos_Procesos.FORTMATO_CONSULTA
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariables
                            If strAxuliarCampos = String.Empty Then
                                strAxuliarCampos = Items
                            Else
                                strAxuliarCampos &= "," & Items
                            End If
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        'AdicionarNodoXml(XmlDocumento, xmlElementoRoot, NOMBRE_TAG_CONSULTA, NOMBRE_TAG_CONSULTA)
                        'xmlElementoRoot = XmlDocumento.DocumentElement
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Manifiesto.NUMNITEMPRESATRANSPORTE)
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMPLACA", "'" & Manifiesto.NUMPLACA & "'")

                End Select
            Catch ex As Exception
            End Try
        End Sub
        Public Sub Crear_XML_Consulta_Tercero(ByRef XmlDocumento As XmlDocument)
            Try
                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                xmlElementoRoot = XmlDocumento.DocumentElement

                Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("TERIDENTIFICACION")
                xmlElementoRoot.LastChild.AppendChild(txtNodo)

                xmlElementoRoot = XmlDocumento.DocumentElement
                NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
                xmlElementoRoot.AppendChild(NuevoElemento)

                NuevoElemento = XmlDocumento.CreateElement("NUMIDTERCERO")
                txtNodo = XmlDocumento.CreateTextNode(Tercero.NUMIDTERCERO)
                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
            Catch ex As Exception

            End Try
        End Sub
        'Public Function Reportar_Tercero(ByRef TextoXmlTercero As String, ByVal TipoFormato As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String), ByVal Tipos_Rol As Tipos_Roles) As Double
        Public Sub Reportar_Tercero(ByVal TipoFormato As Tipos_Procesos, ByVal Tipos_Rol As Tipos_Roles, ByVal ResultadoReporte As ResultReporteMinisterio)
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                Me.acceso.Crear_XML_Acceso(xmlDoc)
                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_TERCERO)
                Me.Crear_XML_Tercero(xmlDoc, TipoFormato, ResultadoReporte.LstVariables, Tipos_Rol)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                If ResultadoReporte.StrError.IndexOf(ERROR_TERCERO_DUPLICADO) <> -1 Or ResultadoReporte.StrError = ERROR_TERCERO_VARIABLE_DESCONOCIDA Then
                    ResultadoReporte.NumConfirmacion = 1
                    ResultadoReporte.StrError = String.Empty
                End If

            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = 0
                ResultadoReporte.StrError = ex.Message
            End Try
        End Sub
        Public Sub Reportar_Vehiuclo(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio)
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                Me.acceso.Crear_XML_Acceso(xmlDoc)
                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_VEHICULO)
                Me.Crear_XML_Vehiculo(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                If ResultadoReporte.StrError.IndexOf(ERROR_TERCERO_DUPLICADO) <> -1 Then
                    ResultadoReporte.NumConfirmacion = 1
                    ResultadoReporte.StrError = String.Empty
                End If

            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = 0
                ResultadoReporte.StrError = ex.Message
            End Try
        End Sub
        Public Sub Reportar_Semirremolque(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio)
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                Me.acceso.Crear_XML_Acceso(xmlDoc)
                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_VEHICULO)
                Me.Crear_XML_Vehiculo(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                If ResultadoReporte.StrError.IndexOf(ERROR_TERCERO_DUPLICADO) <> -1 Then
                    ResultadoReporte.NumConfirmacion = 1
                    ResultadoReporte.StrError = String.Empty
                End If

            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = 0
                ResultadoReporte.StrError = ex.Message
            End Try
        End Sub

        Public Sub Reportar_Cumplido_Manifiesto(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio)
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                Me.acceso.Crear_XML_Acceso(xmlDoc)
                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_MANIFIESTO)
                Me.Crear_XML_Cimplido_Manifiesto(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                'If ResultadoReporte.StrError.IndexOf(ERROR_TERCERO_DUPLICADO) <> -1 Then
                '    ResultadoReporte.NumConfirmacion = 1
                '    ResultadoReporte.StrError = String.Empty
                'End If

            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = 0
                ResultadoReporte.StrError = ex.Message
            End Try
        End Sub
        Public Sub Reportar_Cumplido_Anulado_Manifiesto(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio)
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                Me.acceso.Crear_XML_Acceso(xmlDoc)
                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_CUMPLIDO_MANIFIESTO)
                Me.Crear_XML_Anulacion_Cumplido_Manifiesto(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                If True Then

                End If
                'If ResultadoReporte.StrError.IndexOf(ERROR_TERCERO_DUPLICADO) <> -1 Then
                '    ResultadoReporte.NumConfirmacion = 1
                '    ResultadoReporte.StrError = String.Empty
                'End If

            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = 0
                ResultadoReporte.StrError = ex.Message
            End Try
        End Sub
        Public Sub Reportar_Anulado_Manifiesto(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio)
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                Me.acceso.Crear_XML_Acceso(xmlDoc)
                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_MANIFIESTO)
                Me.Crear_XML_Anulacion_Manifiesto(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                If True Then

                End If
                'If ResultadoReporte.StrError.IndexOf(ERROR_TERCERO_DUPLICADO) <> -1 Then
                '    ResultadoReporte.NumConfirmacion = 1
                '    ResultadoReporte.StrError = String.Empty
                'End If

            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = 0
                ResultadoReporte.StrError = ex.Message
            End Try
        End Sub
        Public Sub Reportar_Manifiesto(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio)
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                Me.acceso.Crear_XML_Acceso(xmlDoc)
                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_MANIFIESTO)
                Me.Crear_XML_Manifiesto(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables, ResultadoReporte.seguridadqr, ResultadoReporte.observacionesqr)
                'If ResultadoReporte.StrError.IndexOf(ERROR_TERCERO_DUPLICADO) <> -1 Then
                '    ResultadoReporte.NumConfirmacion = 1
                '    ResultadoReporte.StrError = String.Empty
                'End If  

            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = 0
                ResultadoReporte.StrError = ex.Message
            End Try
        End Sub

        Public Sub Consultar_Aceptacion_Electronica_Manifiesto(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultAceptacionMinisterio)
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                Me.acceso.Crear_XML_Acceso(xmlDoc)
                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_CONSULTA_ACEPTACION_ELECTRONICA)
                Me.Crear_XML_Consulta_Aceptacion_Electronica(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)

                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                ResultadoReporte.NumConfirmacion = ConsumirAceptacionElectronicaRNDC(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables, ResultadoReporte.fechaAceptacion)

            Catch ex As Exception

            End Try
        End Sub

        Public Function ConsumirServicioRNDC(ByVal strMensaje As String, ByVal TipoFormato As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String), Optional ByRef seguridadqr As String = "", Optional ByRef observacionesqr As String = "") As Double
            Try
                ConsumirServicioRNDC = General.CERO
                Dim xmlRespuesta As New XmlDocument
                xmlRespuesta.LoadXml(strMensaje)
                Dim lstxmlObjetos As New List(Of XmlElement)
                For Each Item As XmlNode In xmlRespuesta.DocumentElement
                    lstxmlObjetos.Add(Item)
                Next
                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
                            ConsumirServicioRNDC = General.CERO
                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
                        End If
                        If xmlRespuesta.GetElementsByTagName("ingresoid").Count > 0 Then
                            ConsumirServicioRNDC = Val(xmlRespuesta.GetElementsByTagName("ingresoid")(0).InnerText)
                            strErrores = String.Empty
                        End If
                        If xmlRespuesta.GetElementsByTagName("seguridadqr").Count > 0 Then
                            seguridadqr = xmlRespuesta.GetElementsByTagName("seguridadqr")(0).InnerText
                        End If
                        If xmlRespuesta.GetElementsByTagName("observacionesqr").Count > 0 Then
                            observacionesqr = xmlRespuesta.GetElementsByTagName("observacionesqr")(0).InnerText
                        End If
                    Case Tipos_Procesos.FORTMATO_CONSULTA
                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
                            ConsumirServicioRNDC = General.CERO
                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
                        End If
                        If xmlRespuesta.GetElementsByTagName("numidtercero").Count > 0 Then
                            ConsumirServicioRNDC = Val(xmlRespuesta.GetElementsByTagName("numidtercero")(0).InnerText)
                            strErrores = String.Empty
                            For i As Integer = 0 To lstVariables.Count - 1
                                If xmlRespuesta.GetElementsByTagName(lstVariables(i).ToLower).Count > 0 Then
                                    lstVariables(i) = xmlRespuesta.GetElementsByTagName(lstVariables(i).ToLower)(0).InnerText
                                Else
                                    lstVariables(i) = String.Empty
                                End If
                            Next
                        End If
                End Select
            Catch ex As Exception
                strErrores = ex.Message
            End Try
        End Function

        Public Function ConsumirAceptacionElectronicaRNDC(ByVal strMensaje As String, ByVal TipoFormato As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String), Optional ByRef fechaAceptacion As String = "") As Double
            Try
                ConsumirAceptacionElectronicaRNDC = General.CERO
                Dim xmlRespuesta As New XmlDocument
                xmlRespuesta.LoadXml(strMensaje)
                Dim lstxmlObjetos As New List(Of XmlElement)
                For Each Item As XmlNode In xmlRespuesta.DocumentElement
                    lstxmlObjetos.Add(Item)
                Next
                Select Case TipoFormato
                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
                            ConsumirAceptacionElectronicaRNDC = General.CERO
                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
                        End If
                        If xmlRespuesta.GetElementsByTagName("ingresoid").Count > 0 Then
                            ConsumirAceptacionElectronicaRNDC = Val(xmlRespuesta.GetElementsByTagName("ingresoid")(0).InnerText)
                            strErrores = String.Empty
                        End If
                        If xmlRespuesta.GetElementsByTagName("fechaing").Count > 0 Then
                            fechaAceptacion = xmlRespuesta.GetElementsByTagName("fechaing")(0).InnerText
                        End If
                End Select
            Catch ex As Exception
                strErrores = ex.Message
            End Try
        End Function
        Public Function FormCampMiniTran(ByVal strValor As String, ByVal intLongitud As Integer, ByVal intTipoCampo As Integer, Optional ByVal intPresicion As Integer = 0) As String
            Dim intCon%, strFormato$, intPos%, strValorEntero$, strValorDecimal$, intLon%, strSignoNegativo$, intLongSignoNegativo%
            FormCampMiniTran = String.Empty

            strValor = strValor.Replace(CARACTER_SALTO_LINEA, "")
            strValor = strValor.Replace(CARACTER_ENTER, "")

            strFormato = ""
            strSignoNegativo = ""
            strValor = Trim$(strValor)

            If intTipoCampo = CAMPO_CEROS_DERECHA Then
                strValor = Trim$(strValor)
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = strValor & "0"
                Next
                FormCampMiniTran = strValor

            ElseIf intTipoCampo = CAMPO_CEROS_IZQUIERDA Then
                strValor = Trim$(strValor)
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = "0" & strValor
                Next
                FormCampMiniTran = strValor

            ElseIf intTipoCampo = CAMPO_NUMERICO Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    FormCampMiniTran = strFormato

                Else
                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    For intCon = 1 To intLongitud - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    FormCampMiniTran = strSignoNegativo & Format$(Val(strValor), strFormato)

                End If
            ElseIf intTipoCampo = CAMPO_IDENTIFICACION_DIGITO_CHEQUEO Then
                strValor = Replace(strValor, ".", "")
                strValor = Replace(strValor, "-", "")
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = "0" & strValor
                Next
                FormCampMiniTran = strValor

            ElseIf intTipoCampo = CAMPO_ALFANUMERICO Then

                If Len(strValor) > intLongitud Then
                    strValor = Mid$(strValor, 1, intLongitud)
                End If
                FormCampMiniTran = strValor & Space$(intLongitud - Len(strValor))


            ElseIf intTipoCampo = CAMPO_DECIMAL Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    FormCampMiniTran = strFormato

                Else

                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    ' Buscar signo decimal
                    intPos = InStr(strValor, ".")
                    If intPos > 0 Then
                        strValorEntero = Mid$(strValor, 1, intPos - 1)
                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                    Else
                        strValorEntero = strValor
                        strValorDecimal = ""
                    End If
                    strValorEntero = Trim$(strValorEntero)
                    strValorDecimal = Trim$(strValorDecimal)

                    ' Formatear Entero
                    For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    If strValorEntero = "" Then
                        strValorEntero = strFormato
                    Else
                        strValorEntero = Format$(Val(strValorEntero), strFormato)
                    End If

                    ' Formatear Decimal
                    strFormato = ""
                    intLon = Len(strValorDecimal)
                    If intLon < intPresicion Then
                        ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                        For intCon = intLon To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    ElseIf intLon = 0 Then
                        ' Si la longitud es 0 se llena el valor decimal con ceros
                        For intCon = 0 To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    End If

                    FormCampMiniTran = strSignoNegativo & strValorEntero & "." & strValorDecimal

                End If

            ElseIf intTipoCampo = CAMPO_FECHA Then
                FormCampMiniTran = Format$(CDate(strValor), "yyyy/MM/dd")
                FormCampMiniTran = Replace(FormCampMiniTran, "/", "")
            End If

            Return FormCampMiniTran

        End Function
#End Region

    End Class
End Namespace
