﻿Imports System.Data.OleDb
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioSitiosCargueDescargue"/>
    ''' </summary>
    Public NotInheritable Class RepositorioSitiosCargueDescargue
        Inherits RepositorioBase(Of SitiosCargueDescargue)

        Public Overrides Function Consultar(filtro As SitiosCargueDescargue) As IEnumerable(Of SitiosCargueDescargue)
            Dim lista As New List(Of SitiosCargueDescargue)
            Dim item As SitiosCargueDescargue

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.ValorAutocomplete) Or filtro.Sync = True Then
                    conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If

                    If Not IsNothing(filtro.CodigoTipoSitio) Then
                        If filtro.CodigoTipoSitio > 0 Then
                            conexion.AgregarParametroSQL("@par_TipoSitio", filtro.CodigoTipoSitio)
                        End If
                    End If
                    If Not IsNothing(filtro.Ciudad) Then
                        If filtro.Ciudad.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Ciudad", filtro.Ciudad.Codigo)
                        End If
                    End If
                    If filtro.Estado >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_sitios_cargue_descargue_Autocomplete")
                        While resultado.Read
                            item = New SitiosCargueDescargue(resultado)
                            lista.Add(item)
                        End While

                    Else
                        If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If

                    If Not IsNothing(filtro.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If

                    If filtro.CodigoTipoSitio > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Tipo_Sitio", filtro.CodigoTipoSitio)
                    End If

                    If Not IsNothing(filtro.Pais) Then
                        conexion.AgregarParametroSQL("@par_Codigo_Pais", filtro.Pais.Codigo)
                    End If

                    If Not IsNothing(filtro.Ciudad) Then
                        If filtro.Ciudad.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Codigo_Ciudad", filtro.Ciudad.Codigo)
                        End If
                    End If

                    If filtro.Estado >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If

                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If

                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_sitios_cargue_descargue]")
                    While resultado.Read
                        item = New SitiosCargueDescargue(resultado)
                        lista.Add(item)
                    End While
                End If
            End Using
            Return lista
        End Function

        Public Function ConsultarSitiosRutas(filtro As Rutas) As IEnumerable(Of SitiosCargueDescargueRutas)
            Dim lista As New List(Of SitiosCargueDescargueRutas)
            Dim item As SitiosCargueDescargueRutas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_RUTA", filtro.Codigo)
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_sitios_cargue_descargue_ruta")
                While resultado.Read
                    item = New SitiosCargueDescargueRutas(resultado)
                    lista.Add(item)
                End While


            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As SitiosCargueDescargue) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Tipo_Sitio", entidad.CodigoTipoSitio)
                conexion.AgregarParametroSQL("@par_Codigo_Pais", entidad.Pais.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Ciudad", entidad.Ciudad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Zona", entidad.Zona.Codigo)
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Contacto", entidad.Contacto)
                conexion.AgregarParametroSQL("@par_Valor_Cargue", entidad.ValorCargue, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Valor_Descargue", entidad.ValorDescargue, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                If Not IsNothing(entidad.Longitud) Then
                    conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Float)
                End If
                If Not IsNothing(entidad.Latitud) Then
                    conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Float)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_sitios_cargue_descargue")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Function GuardarSitiosRuta(entidad As IEnumerable(Of SitiosCargueDescargueRutas)) As Long
            Dim Numero As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad(0).CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Ruta", entidad(0).Ruta.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_sitios_cargue_descargue_ruta")

                resultado.Close()



                For Each sitio In entidad

                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", sitio.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Ruta", sitio.Ruta.Codigo)
                    conexion.AgregarParametroSQL("@par_SICD_Codigo", sitio.Sitio.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", sitio.TipoVehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Cargue", sitio.ValorCargue, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Descargue", sitio.ValorDescargue, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Aplica_Anticipo", sitio.AplicaAnticipo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", sitio.UsuarioCrea.Codigo)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_sitios_cargue_descargue_ruta")

                    While resultado.Read
                        Numero = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()

                Next



            End Using

            Return Numero
        End Function

        Public Overrides Function Modificar(entidad As SitiosCargueDescargue) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Tipo_Sitio", entidad.CodigoTipoSitio)
                conexion.AgregarParametroSQL("@par_Codigo_Pais", entidad.Pais.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Ciudad", entidad.Ciudad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Zona", entidad.Zona.Codigo)
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Contacto", entidad.Contacto)
                conexion.AgregarParametroSQL("@par_Valor_Cargue", entidad.ValorCargue, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Valor_Descargue", entidad.ValorDescargue, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                If Not IsNothing(entidad.Longitud) Then
                    conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Float)
                End If
                If Not IsNothing(entidad.Latitud) Then
                    conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Float)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_sitios_cargue_descargue")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As SitiosCargueDescargue) As SitiosCargueDescargue
            Dim item As New SitiosCargueDescargue

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_sitios_cargue_descargue]")

                While resultado.Read
                    item = New SitiosCargueDescargue(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As SitiosCargueDescargue) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_sitios_cargue_descargue]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
        Public Function GenerarPlanitilla(filtro As SitiosCargueDescargue) As SitiosCargueDescargue

            Dim fullPath As String
            fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
            System.IO.File.Copy(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Sitios_Cargue_Descargue.xlsx", fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Sitios_Cargue_Descargue_Generado.xlsx", True)
            Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Sitios_Cargue_Descargue_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"
            Dim Ciudades As IEnumerable(Of Ciudades)
            Dim Ciudad As New Ciudades
            Dim ObjCiudad As New RepositorioCiudades
            Ciudad.CodigoEmpresa = filtro.CodigoEmpresa
            Ciudad.Estado = 1
            Ciudades = ObjCiudad.Consultar(Ciudad)

            Using cnn As New OleDbConnection(cn)
                cnn.Open()
                Ciudades = Ciudades.OrderBy(Function(a) a.Nombre)
                For i = 1 To Ciudades.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        cmd.CommandText = "INSERT INTO [Ciudad$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Ciudades(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Ciudades(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    End Using
                Next
                cnn.Close()
            End Using
            Dim RTA As New SitiosCargueDescargue
            Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Sitios_Cargue_Descargue_Generado.xlsx")
            RTA.Planitlla = Archivo
            Return RTA
        End Function

    End Class

End Namespace