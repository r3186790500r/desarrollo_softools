﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports System.Transactions
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace ControlViajes

    ''' <summary>
    ''' Clase <see cref="RepositorioInspeccionPreoperacional"/>
    ''' </summary>
    Public NotInheritable Class RepositorioInspeccionPreoperacional
        Inherits RepositorioBase(Of InspeccionPreoperacional)

        Public Overrides Function Consultar(filtro As InspeccionPreoperacional) As IEnumerable(Of InspeccionPreoperacional)
            Dim lista As New List(Of InspeccionPreoperacional)
            Dim item As InspeccionPreoperacional

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If Not IsNothing(filtro.Vehiculo) Then
                    conexion.AgregarParametroSQL("@par_Vehiculo", filtro.Vehiculo.Placa)
                End If

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If (filtro.FechaInicial > Date.MinValue) And (filtro.FechaFinal >= filtro.FechaInicial) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If

                If Not IsNothing(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If

                If Not IsNothing(filtro.FormatoInspeccion) Then
                    conexion.AgregarParametroSQL("@par_Formato_Inspeccion", filtro.FormatoInspeccion.Nombre)
                End If

                If Not IsNothing(filtro.Conductor) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Conductor", filtro.Conductor.NombreCompleto)
                End If

                If Not IsNothing(filtro.Transportador) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Transportador", filtro.Transportador.NombreCompleto)
                End If

                If Not IsNothing(filtro.Cliente) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Cliente", filtro.Cliente.NombreCompleto)
                End If

                If filtro.Estado > -1 Then
                    If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                        conexion.AgregarParametroSQL("@par_Anulado", 0)

                    ElseIf filtro.Estado = 2 Then
                        conexion.AgregarParametroSQL("@par_Anulado", 1)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_inspeccion_preoperacional]")

                While resultado.Read
                    item = New InspeccionPreoperacional(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As InspeccionPreoperacional) As Long
            Dim inserto As Boolean = True
            Dim insertoSeccion As Boolean = True
            Dim trasladoFotos As Boolean = False
            Dim limpioTemporal As Boolean = False
            Dim resultado As IDataReader
            'Dim DetalleFotos As New DocumentoInspeccionPreoperacional
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                        conexion.AgregarParametroSQL("@par_EFIN_Codigo", entidad.FormatoInspeccion.Codigo)
                        If entidad.FechaInspeccion > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Inspeccion", entidad.FechaInspeccion, SqlDbType.DateTime)
                        End If
                        conexion.AgregarParametroSQL("@par_Tiempo_Vigencia", entidad.TiempoVigencia)
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                        conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                        If Not IsNothing(entidad.Semirremolque) Then
                            conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
                        End If
                        If Not IsNothing(entidad.Cliente) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                        End If
                        If Not IsNothing(entidad.Conductor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                        End If
                        If Not IsNothing(entidad.Transportador) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Transportador", entidad.Transportador.Codigo)
                        End If
                        If entidad.FechaInicioHora > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Hora_Inicio_Inspeccion", entidad.FechaInicioHora, SqlDbType.DateTime)
                        End If
                        If entidad.FechaFinHora > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Hora_Fin_Inspeccion", entidad.FechaFinHora, SqlDbType.DateTime)
                        End If
                        If Not IsNothing(entidad.SitioCargue) Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.Codigo)
                        End If
                        If Not IsNothing(entidad.SitioDescargue) Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                        If Not IsNothing(entidad.Responsable) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", entidad.Responsable.Codigo)
                        End If
                        If Not IsNothing(entidad.Autoriza) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Autoriza", entidad.Autoriza.Codigo)
                        End If
                        If entidad.FechaAutoriza > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Autoriza", entidad.FechaAutoriza, SqlDbType.DateTime)
                        End If
                        If Not IsNothing(entidad.Firma) Then
                            conexion.AgregarParametroSQL("@par_Firma", entidad.Firma, SqlDbType.Image)
                        End If
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                        If Not IsNothing(entidad.OrdenCargue) Then
                            conexion.AgregarParametroSQL("@par_ENOC_Numero", entidad.OrdenCargue.Numero)
                        End If
                        If Not IsNothing(entidad.Planilla) Then
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Planilla.Numero)
                            conexion.AgregarParametroSQL("@par_ENMC_Numero", entidad.Planilla.Manifiesto.Numero)
                        End If
                        If entidad.Cumple > 0 Then
                            conexion.AgregarParametroSQL("@par_Cumple", entidad.Cumple)
                        End If

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_inspeccion_preoperacional")

                        While resultado.Read
                            entidad.Numero = resultado.Item("Numero").ToString()
                        End While

                        resultado.Close()

                        If entidad.Numero.Equals(Cero) Then
                            Return Cero
                        End If

                        If Not IsNothing(entidad.Detalle) Then
                            For Each detalle In entidad.Detalle
                                detalle.NumeroEncabezado = entidad.Numero
                                If Not InsertarSeccion(detalle, conexion) Then
                                    insertoSeccion = False
                                    inserto = False
                                    Exit For
                                End If
                            Next
                        End If

                        'If Not IsNothing(entidad.DetalleFotos) Then
                        '    If entidad.DetalleFotos.Count() > 0 Then

                        '        DetalleFotos.CodigoEmpresa = entidad.CodigoEmpresa
                        '        DetalleFotos.Numero = entidad.Numero
                        '        DetalleFotos.NumeroInspeccion = entidad.Numero
                        '        DetalleFotos.UsuarioCrea = entidad.UsuarioModifica

                        '        trasladoFotos = New RepositorioDocumentoInspeccionPreoperacional().TrasladarFotos(DetalleFotos, conexionDocumentos)

                        '        If trasladoFotos Then
                        '            limpioTemporal = New RepositorioDocumentoInspeccionPreoperacional().LimpiarTemporalUsuario(DetalleFotos)
                        '        End If
                        '    End If
                        'End If

                        If entidad.Numero > 0 Then
                            resultado.Close()
                        End If
                        If entidad.Estado = 1 Then
                            If entidad.Cumple <= 0 And (entidad.OrdenCargue.Numero > 0 Or entidad.Planilla.Numero > 0) Then
                                Dim Autorizacion = New Autorizaciones
                                Dim strCausas As String = ""
                                Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                                Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = CODIGO_CATALOGO_TIPO_AUTORIZACION_NO_CUMPLIMIENTO_INSPECCION}
                                Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                                Autorizacion.NumeroDocumento = entidad.Numero
                                For Each item In entidad.Detalle
                                    If item.Cumple <= 0 Then
                                        strCausas = strCausas + item.Nombre + ", "
                                    End If
                                Next
                                If strCausas <> "" Then
                                    strCausas = Left(strCausas, Len(strCausas) - 1)
                                    strCausas = Left(strCausas, Len(strCausas) - 1)
                                End If
                                Autorizacion.Observaciones = "Inspección: " + entidad.Numero.ToString() + " ,no cumple: " + strCausas
                                Dim repAutorizacion = New RepositorioAutorizaciones()

                                Dim detalleCorreo As InspeccionPreoperacional
                                detalleCorreo = New InspeccionPreoperacional With {.CodigoEmpresa = entidad.CodigoEmpresa, .Numero = entidad.Numero, .UsuarioCrea = entidad.UsuarioCrea, .Codigo = entidad.Numero, .Oficina = New Oficinas With {.Codigo = entidad.Oficina.Codigo}}
                                EnviarCorreo(detalleCorreo, conexion)

                                inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                            End If
                        End If


                    End Using

                    If inserto Then
                        transaccion.Complete()
                    Else
                        transaccion.Dispose()
                        entidad.Numero = Cero
                    End If

                End Using
            End Using
            Return entidad.Numero
        End Function

        Public Function EnviarCorreo(entidad As InspeccionPreoperacional, ByVal conexion As DataBaseFactory) As Boolean

            Dim inserto As Boolean = True

            entidad.TipoDocumento = 240
            Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
            Correos.GenerarCorreo(entidad, conexion)

            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As InspeccionPreoperacional) As Long
            Dim trasladoFotos As Boolean = False
            Dim limpioTemporal As Boolean = False
            Dim inserto As Boolean = True
            Dim insertoSeccion As Boolean = True
            'Dim DetalleFotos As New DocumentoInspeccionPreoperacional
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                        conexion.AgregarParametroSQL("@par_EFIN_Codigo", entidad.FormatoInspeccion.Codigo)
                        conexion.AgregarParametroSQL("@par_Fecha_Inspeccion", entidad.FechaInspeccion, SqlDbType.DateTime)
                        conexion.AgregarParametroSQL("@par_Tiempo_Vigencia", entidad.TiempoVigencia)
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                        conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                        conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
                        If Not IsNothing(entidad.Cliente) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                        If Not IsNothing(entidad.Transportador) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Transportador", entidad.Transportador.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Inicio_Inspeccion", entidad.FechaInicioHora, SqlDbType.DateTime)
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Fin_Inspeccion", entidad.FechaFinHora, SqlDbType.DateTime)
                        conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.Codigo)
                        conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.Codigo)
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                        If Not IsNothing(entidad.Responsable) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", entidad.Responsable.Codigo)
                        End If

                        If Not IsNothing(entidad.Autoriza) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Autoriza", entidad.Autoriza.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Fecha_Autoriza", entidad.FechaAutoriza, SqlDbType.DateTime)
                        If Not IsNothing(entidad.Firma) Then
                            conexion.AgregarParametroSQL("@par_Firma", entidad.Firma, SqlDbType.Image)
                        End If
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_inspeccion_preoperacional")

                        While resultado.Read
                            entidad.Codigo = resultado.Item("Numero").ToString()
                        End While
                        resultado.Close()

                        If entidad.Codigo.Equals(Cero) Then
                            inserto = False
                            Return Cero
                        Else
                            inserto = True
                        End If

                        For Each detalle In entidad.Detalle
                            detalle.NumeroEncabezado = entidad.Numero
                            If Not InsertarSeccion(detalle, conexion) Then
                                insertoSeccion = False
                                inserto = False
                                Exit For
                            End If
                        Next

                        'If entidad.DetalleFotos.Count() > 0 Then

                        '    DetalleFotos.CodigoEmpresa = entidad.CodigoEmpresa
                        '    DetalleFotos.Numero = entidad.Numero
                        '    DetalleFotos.UsuarioCrea = entidad.UsuarioModifica
                        '    DetalleFotos.NumeroInspeccion = entidad.Numero

                        '    trasladoFotos = New RepositorioDocumentoInspeccionPreoperacional().TrasladarFotos(DetalleFotos, conexionDocumentos)

                        '    If trasladoFotos Then
                        '        limpioTemporal = LimpiarTemporalUsuario(DetalleFotos, conexionDocumentos)
                        '    End If
                        'End If

                        If inserto = False Then
                            inserto = False
                            Return Cero
                        End If

                    End Using

                    If inserto Then
                        transaccion.Complete()
                    Else
                        transaccion.Dispose()
                        entidad.Codigo = Cero
                    End If

                End Using
            End Using
            Return entidad.Codigo
        End Function
        Public Function InsertarSeccion(entidad As DetalleInspeccionPreoperacional, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_ENIN_Numero", entidad.NumeroEncabezado)
            contextoConexion.AgregarParametroSQL("@par_DSFI_Codigo", entidad.SeccionFormulario)
            contextoConexion.AgregarParametroSQL("@par_DIFI_Codigo", entidad.ItemFormulario)
            contextoConexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
            contextoConexion.AgregarParametroSQL("@par_Orden_Seccion", entidad.OrdenSeccion)
            contextoConexion.AgregarParametroSQL("@par_Relevante", entidad.Relevante)
            contextoConexion.AgregarParametroSQL("@par_Cumple", entidad.Cumple)
            contextoConexion.AgregarParametroSQL("@par_No_Aplica", entidad.NoAplica)
            contextoConexion.AgregarParametroSQL("@par_Orden_Item", entidad.OrdenItem)
            contextoConexion.AgregarParametroSQL("@par_Tamaño", entidad.Tamano)
            contextoConexion.AgregarParametroSQL("@par_CATA_TCON_Codigo", entidad.TipoControl)
            contextoConexion.AgregarParametroSQL("@par_Valor", entidad.Valor)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_inspeccion_preoperacional]")

            While resultado.Read
                inserto = IIf(Convert.ToInt64(resultado.Item("Codigo")) > 0, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function
        Public Overrides Function Obtener(filtro As InspeccionPreoperacional) As InspeccionPreoperacional
            Dim item As New InspeccionPreoperacional
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                    If filtro.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    End If

                    If Not IsNothing(filtro.FormatoInspeccion) Then
                        If filtro.FormatoInspeccion.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Codigo_Formato_Inspeccion", filtro.FormatoInspeccion.Codigo)
                        End If
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_inspeccion_preoperacional]")

                    While resultado.Read
                        item = New InspeccionPreoperacional(resultado)
                    End While
                    If filtro.ConsularDetalle > 0 Then
                        Dim lista As New List(Of DetalleInspeccionPreoperacional)
                        Dim Seccion As DetalleInspeccionPreoperacional
                        resultado.Close()
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                        If filtro.Numero > 0 Then
                            conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                        End If

                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_inspeccion_preoperacional]")
                        While resultado.Read
                            Seccion = New DetalleInspeccionPreoperacional(resultado)
                            lista.Add(Seccion)
                        End While
                        item.Detalle = lista

                    End If

                End Using
            End Using
            Return item
        End Function

        Public Function ObtenerDocumentoActivo(filtro As InspeccionPreoperacional) As InspeccionPreoperacional
            Dim item As New InspeccionPreoperacional
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                    If filtro.Vehiculo.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Vehiculo", filtro.Vehiculo.Codigo)
                    End If



                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_documentos_despacho_vehiculo")

                    While resultado.Read
                        item = New InspeccionPreoperacional(resultado)
                    End While


                End Using
            End Using
            Return item
        End Function

        Public Function ObtenerUltimaInspeccion(filtro As InspeccionPreoperacional) As InspeccionPreoperacional
            Dim item As New InspeccionPreoperacional
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                    If filtro.Vehiculo.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Vehiculo", filtro.Vehiculo.Codigo)
                    End If



                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_ultima_inspeccion_vehiculo")

                    While resultado.Read
                        item = New InspeccionPreoperacional(resultado)
                    End While


                End Using
            End Using
            Return item
        End Function
        Public Function Anular(entidad As InspeccionPreoperacional) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_anular_encabezado_inspeccion_preoperacional]")

                While resultado.Read
                    anulo = IIf(Convert.ToInt64(resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
        Public Function LimpiarTemporalUsuario(entidad As DocumentoInspeccionPreoperacional, ByRef contextoConexion As DataBaseFactory) As Boolean
            Dim inserto As Boolean = False
            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Numero", entidad.NumeroInspeccion)
            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_limpiar_t_documento_inspecciones]")

            While resultado.Read
                inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
            End While

            Return inserto
        End Function

    End Class

End Namespace