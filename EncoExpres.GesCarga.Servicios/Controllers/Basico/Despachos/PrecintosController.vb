﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="PrecintosController"/>
''' </summary>
<Authorize>
Public Class PrecintosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Precintos) As Respuesta(Of IEnumerable(Of Precintos))
        Return New LogicaPrecintos(CapaPersistenciaPrecintos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Precintos) As Respuesta(Of Precintos)
        Return New LogicaPrecintos(CapaPersistenciaPrecintos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Precintos) As Respuesta(Of Long)
        Return New LogicaPrecintos(CapaPersistenciaPrecintos).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("GuardarPrecintos")>
    Public Function GuardarPrecintos(ByVal entidad As Precintos) As Respuesta(Of Precintos)
        Return New LogicaPrecintos(CapaPersistenciaPrecintos).GuardarPrecintos(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Precintos) As Respuesta(Of Boolean)
        Return New LogicaPrecintos(CapaPersistenciaPrecintos).Anular(entidad)
    End Function
    <ActionName("ConsultarUnicoPrecinto")>
    Public Function ConsultarUnicoPrecinto(ByVal filtro As DetallePrecintos) As Respuesta(Of IEnumerable(Of DetallePrecintos))
        Return New LogicaPrecintos(CapaPersistenciaPrecintos).ConsultarUnicoPrecinto(filtro)
    End Function

    <HttpPost>
    <ActionName("LiberarPrecinto")>
    Public Function LiberarPrecinto(ByVal entidad As Precintos) As Respuesta(Of Boolean)
        'Return New LogicaPrecintos(CapaPersistenciaPrecintos).LiberarPrecinto(entidad)
        Dim retorno As Respuesta(Of Boolean) = New LogicaPrecintos(CapaPersistenciaPrecintos).LiberarPrecinto(entidad)
        Return retorno
    End Function
    <HttpPost>
    <ActionName("ValidarPrecintoPlanillaPaqueteria")>
    Public Function ValidarPrecintoPlanillaPaqueteria(ByVal entidad As Precintos) As Respuesta(Of Precintos)
        Return New LogicaPrecintos(CapaPersistenciaPrecintos).ValidarPrecintoPlanillaPaqueteria(entidad)
    End Function
End Class
