﻿EncoExpresApp.controller("CargarTercerosCtrl", ['$scope', '$timeout', '$linq', 'TercerosFactory', 'blockUI', 'ValorCatalogosFactory', 'TercerosFactory', 'CiudadesFactory', 'TarifarioVentasFactory', 'TarifarioComprasFactory', 'BancosFactory', 'blockUIConfig',
    function ($scope, $timeout, $linq, TercerosFactory, blockUI, ValorCatalogosFactory, TercerosFactory, CiudadesFactory, TarifarioVentasFactory, TarifarioComprasFactory, BancosFactory, blockUIConfig) {
        console.clear()



        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.continuar = false
        $scope.VerErrores = false;
        $scope.DeshabilitarCliente = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.ListadoRutasPuntosGestion = []
        $scope.Modelo = {
            Fecha: new Date(),
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,

        }
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Procesos' }, { Nombre: 'Cargue Terceros' }];
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/


        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ];
        /*Cargar los check de perfiles*/

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_PERFIL_TERCEROS }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.perfilTerceros = response.data.Datos;

                        $scope.perfilTerceros.splice(0, 1)
                    }
                    else {
                        $scope.perfilTerceros = []
                    }
                }
            }, function (response) {
            });
        $scope.verificacionPerfiles = function (item) {
            item.ContPerfiltes = 0
            item.PerfCliente = 0
            item.PerfConductor = 0
            item.PerfEmpleado = 0
            item.PerfProveedor = 0
            item.PerfTenedor = 0
            item.perfilTerceros.forEach(function (perfil) {
                if (perfil.Estado == true) {
                    item.ContPerfiltes++
                    if (perfil.Codigo == 1401) {
                        item.PerfCliente = 1
                    }
                    if (perfil.Codigo == 1403) {
                        item.PerfConductor = 1
                    }
                    if (perfil.Codigo == 1405) {
                        item.PerfEmpleado = 1
                    }
                    if (perfil.Codigo == 1408) {
                        item.PerfPropietario = 1
                    }
                    if (perfil.Codigo == 1409) {
                        item.PerfProveedor = 1
                    }
                    if (perfil.Codigo == 1412) {
                        item.PerfTenedor = 1
                    }
                }
            }
            )
            if (item.PerfCliente == 0) {
                item.Cliente = {}
            }
            if (item.PerfConductor == 0) {
                item.Conductor = {}
            }
            if (item.PerfEmpleado == 0) {
                item.Empleado = {}
            }
            if (item.PerfProveedor == 0 && item.PerfTenedor == 0) {
                item.Proveedor = {}
            }
        };
        /*Cargar el combo de Sexos*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SEXO_TERCERO_PERSONA_NATURAL }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSexos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoSexos = response.data.Datos;
                    }
                    else {
                        $scope.ListadoSexos = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de tipo identificaciones*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoIdentificacion = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoIdentificacion = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoIdentificacion = []
                    }
                }
            }, function (response) {
            });
        $scope.ListadoCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        };
        /*Cargar el combo de Bancos*/
        BancosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoBancos = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoBancos.push(item);
                    });

                }
            }, function (response) {
            });
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CUENTA_BANCARIA }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoBancos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoBancos = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoBancos = []
                    }
                }
            }, function (response) {
            });
        /*Cargar Autocomplete de Grupos empresariales*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_COMERCIAL, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoRepresentanteComerciales = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoRepresentanteComerciales = response.data.Datos;
                    }
                    else {
                        $scope.ListadoGrupoEmpresariales = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        TarifarioVentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
            then(function (response) {
                $scope.ListadoTarifarios = [];
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTarifarios = response.data.Datos

                }
            }, function (response) {
                ShowError(response.statusText);
            });
        TarifarioComprasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
            then(function (response) {
                $scope.ListadoTarifariosCompra = [];
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTarifariosCompra = response.data.Datos

                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de forma pago */
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoFormaPago = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoFormaPago = response.data.Datos;
                    }
                    else {
                        $scope.ListadoFormaPago = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de forma cOBRO */
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_COBRO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoFormaCobro = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoFormaCobro = response.data.Datos;
                    }
                    else {
                        $scope.ListadoFormaCobro = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de tipo anticipo*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 187 }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoAnticipo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoAnticipo = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoAnticipo = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Categoria Licencia*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CATEGORIAS_LICENCIAS_DE_CONDUCCION }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCategorias = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCategorias = response.data.Datos;

                    }
                    else {
                        $scope.ListadoCategorias = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de departamento empleado*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DEPARTAMENTO_EMPLEADOS }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDepartamentosEmpleado = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoDepartamentosEmpleado = response.data.Datos;

                    }
                    else {
                        $scope.ListadoDepartamentosEmpleado = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de cargos*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CARGOS }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCargos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCargos = response.data.Datos;

                    }
                    else {
                        $scope.ListadoCargos = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Tipo Sangres*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DE_SANGRE }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoSangres = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoSangres = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoSangres = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Tipo Contrato empleados*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONTRATO_EMPLEADO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoContratoEmpleados = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoContratoEmpleados = response.data.Datos;

                    }
                    else {
                        $scope.ListadoTipoContrato = []
                    }
                }
            }, function (response) {
            });
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.DataActual = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.DataActual = []
                for (var i = a - 10; i < $scope.DataArchivo.length; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }

        //Paginacion MOdal de errores

        $scope.PrimerPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10) {
                $scope.DataErrorActual = []
                $scope.paginaActualError = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.AnteriorModalError = function () {
            if ($scope.paginaActualError > 1) {
                $scope.paginaActualError -= 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteModalError = function () {
            if ($scope.paginaActualError < $scope.totalPaginasErrores) {
                $scope.paginaActualError += 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    $scope.UltimaPaginaModalError()

                }
            } else if ($scope.paginaActualError == $scope.totalPaginasErrores) {
                $scope.UltimaPaginaModalError()
            }
        }
        $scope.UltimaPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10 && $scope.totalPaginasErrores > 1) {
                $scope.paginaActualError = $scope.totalPaginasErrores
                var a = $scope.paginaActualError * 10
                $scope.DataErrorActual = []
                for (var i = a - 10; i < $scope.ListaErrores.length; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10) {
                $scope.ProgramacionesNoGuardadasActual = []
                $scope.paginaActualNoGuardadas = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }
        $scope.AnteriorNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas -= 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas < $scope.totalPaginasNoGuardadas) {
                $scope.paginaActualNoGuardadas += 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardados()

                }
            } else if ($scope.paginaActualNoGuardadas == $scope.totalPaginasNoGuardadas) {
                $scope.UltimaPaginaNoGuardados()
            }
        }
        $scope.UltimaPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10 && $scope.totalPaginasNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas = $scope.totalPaginasNoGuardadas
                var a = $scope.paginaActualNoGuardadas * 10
                $scope.ProgramacionesNoGuardadasActual = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10) {
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                $scope.paginaActualNoGuardadasRemplazo = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }
        $scope.AnteriorNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo -= 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo < $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.paginaActualNoGuardadasRemplazo += 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardadosRemplazo()

                }
            } else if ($scope.paginaActualNoGuardadasRemplazo == $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.UltimaPaginaNoGuardadosRemplazo()
            }
        }
        $scope.UltimaPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10 && $scope.totalPaginasNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo = $scope.totalPaginasNoGuardadasRemplazo
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }


        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardaras = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardadasRemplazo = function (item) {
            if ($scope.Option.name == 'Omitido') {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Omitido'
                }
            } else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Remplazado'
                }
            }

        }
        $scope.LimpiarData = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            $scope.DataVerificada = []
            $scope.DataActual = []
            $scope.DataErrorActual = []
            $scope.DataArchivo = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            $scope.VerErrores = false
        }

        /*-----------------------------------------------------------------------funciones de lectura y apertura de archivos----------------------------------------------------------------------------*/

        $scope.ListaProgramaciones = []
        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined || item == '') {
                return true
            } else {
                return false
            }
        }
        function AsignarValoresTabla() {
            $scope.NumeroCargue = 0

            if ($scope.DataArchivo.Valores.length > 0) {
                var DataTemporal = []
                for (var i = 0; i < $scope.DataArchivo.Valores.length; i++) {
                    var item = $scope.DataArchivo.Valores[i]

                    if (!Eval(item.Numero_Identificacion)) {
                        DataTemporal.push(item)
                    }
                }
                $scope.DataArchivo = DataTemporal
                $scope.totalRegistros = ($scope.DataArchivo.length)
                $scope.paginaActual = 1
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                blockUI.start();
                blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');
                $timeout(function () {
                    try {
                        var data = []
                        for (var i = 0; i < $scope.DataArchivo.length; i++) {
                            var DataItem = $scope.DataArchivo[i]
                            var item = {}
                            //DataItem.Cliente = {}
                            //DataItem.Conductor = {}
                            //DataItem.Empleado = {}
                            //DataItem.Proveedor = {}
                            try {
                                item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                                item.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                item.perfilTerceros = angular.copy($scope.perfilTerceros)
                                item.ContPerfiltes = 0
                                for (var j = 0; j < item.perfilTerceros.length; j++) {
                                    if (DataItem.Aseguradora == 1 && item.perfilTerceros[j].Codigo == 1402) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                    }
                                    if (DataItem.Clientes == 1 && item.perfilTerceros[j].Codigo == 1401) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                        item.Cliente = {}
                                        if (!Eval(DataItem.Representante_Comercial)) { try { item.Cliente.RepresentanteComercial = $scope.CargarTerceroIdentificacion(DataItem.Representante_Comercial) } catch (e) { } }
                                        if (!Eval(DataItem.ENTV_Numero)) { try { item.Cliente.Tarifario = $linq.Enumerable().From($scope.ListadoTarifarios).First('$.Codigo == ' + DataItem.ENTV_Numero); } catch (e) { } }
                                        if (!Eval(DataItem.CATA_FOPA_Codigo)) { try { item.Cliente.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo == ' + DataItem.CATA_FOPA_Codigo); } catch (e) { } }
                                    }
                                    if (DataItem.Comercial == 1 && item.perfilTerceros[j].Codigo == 1413) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                    }
                                    if (DataItem.Conductor == 1 && item.perfilTerceros[j].Codigo == 1403) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                        item.Conductor = {}
                                        if (!Eval(DataItem.Numero_Licencia)) { try { item.Conductor.NumeroLicencia = $scope.MaskNumeroGrid(DataItem.Numero_Licencia); } catch (e) { } }
                                        if (!Eval(DataItem.Fecha_Vencimiento)) { try { item.Conductor.FechaVencimiento = new Date(DataItem.Fecha_Vencimiento) } catch (e) { } }
                                        if (!Eval(DataItem.CATA_CALI_Codigo)) { try { item.Conductor.CategoriaLicencia = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo == ' + DataItem.CATA_CALI_Codigo); } catch (e) { } }
                                        if (!Eval(DataItem.CATA_TISA_Codigo)) { try { item.Conductor.TipoSangre = $linq.Enumerable().From($scope.ListadoTipoSangres).First('$.Codigo == "' + DataItem.CATA_TISA_Codigo + '"'); } catch (e) { } }
                                    }
                                    if (DataItem.Destinatario == 1 && item.perfilTerceros[j].Codigo == 1404) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                    }
                                    if (DataItem.Empleado == 1 && item.perfilTerceros[j].Codigo == 1405) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                        item.Empleado = {}
                                        if (!Eval(DataItem.Fecha_Vinculacion)) { try { item.Empleado.FechaVinculacion = new Date(DataItem.Fecha_Vinculacion); } catch (e) { } }
                                        if (!Eval(DataItem.CATA_TICO_Codigo)) { try { item.Empleado.TipoContrato = $linq.Enumerable().From($scope.ListadoTipoContratoEmpleados).First('$.Codigo == ' + DataItem.CATA_TICO_Codigo); } catch (e) { } }
                                        if (!Eval(DataItem.CATA_DEEM_Codigo)) { try { item.Empleado.Departamento = $linq.Enumerable().From($scope.ListadoDepartamentosEmpleado).First('$.Codigo == ' + DataItem.CATA_DEEM_Codigo); } catch (e) { } }
                                        if (!Eval(DataItem.CATA_CAEM_Codigo)) { try { item.Empleado.Cargo = $linq.Enumerable().From($scope.ListadoCargos).First('$.Codigo == ' + DataItem.CATA_CAEM_Codigo); } catch (e) { } }
                                    }
                                    if (DataItem.Afiliadora == 1 && item.perfilTerceros[j].Codigo == 1406) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                    }
                                    if (DataItem.Transportadora == 1 && item.perfilTerceros[j].Codigo == 1407) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                    }
                                    if (DataItem.Propietario == 1 && item.perfilTerceros[j].Codigo == 1408) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                    }
                                    if (DataItem.Proveedor == 1 && item.perfilTerceros[j].Codigo == 1409) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                        item.Proveedor = {}
                                        if (!Eval(DataItem.ENTC_Numero)) { try { item.Proveedor.Tarifario = $linq.Enumerable().From($scope.ListadoTarifariosCompra).First('$.Codigo == ' + DataItem.ENTC_Numero); } catch (e) { } }
                                        if (!Eval(DataItem.CATA_FOCO_Codigo)) { try { item.Proveedor.FormaCobro = $linq.Enumerable().From($scope.ListadoFormaCobro).First('$.Codigo == ' + DataItem.CATA_FOCO_Codigo); } catch (e) { } }
                                        if (!Eval(DataItem.CATA_TIAN_Codigo)) { try { item.TipoAnticipo = $linq.Enumerable().From($scope.ListadoTipoAnticipo).First('$.Codigo == ' + DataItem.CATA_TIAN_Codigo); } catch (e) { } }
                                    }
                                    if (DataItem.GPS == 1 && item.perfilTerceros[j].Codigo == 1414) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                    }
                                    if (DataItem.Remitente == 1 && item.perfilTerceros[j].Codigo == 1410) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                    }
                                    if (DataItem.Seguridad_Social == 1 && item.perfilTerceros[j].Codigo == 1411) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                    }
                                    if (DataItem.Tenedor == 1 && item.perfilTerceros[j].Codigo == 1412) {
                                        item.perfilTerceros[j].Estado = true
                                        item.ContPerfiltes++
                                        item.Proveedor = {}
                                        if (!Eval(DataItem.ENTC_Numero)) { try { item.Proveedor.Tarifario = $linq.Enumerable().From($scope.ListadoTarifariosCompra).First('$.Codigo == ' + DataItem.ENTC_Numero); } catch (e) { } }
                                        if (!Eval(DataItem.CATA_FOCO_Codigo)) { try { item.Proveedor.FormaCobro = $linq.Enumerable().From($scope.ListadoFormaCobro).First('$.Codigo == ' + DataItem.CATA_FOCO_Codigo); } catch (e) { } }
                                        if (!Eval(DataItem.CATA_TIAN_Codigo)) { try { item.TipoAnticipo = $linq.Enumerable().From($scope.ListadoTipoAnticipo).First('$.Codigo == ' + DataItem.CATA_TIAN_Codigo); } catch (e) { } }
                                    }

                                }
                                $scope.verificacionPerfiles(item)
                            } catch (e) {
                            }
                            $scope.perfilTerceros.Asc = true
                            OrderBy('Nombre', undefined, $scope.perfilTerceros);
                            //Representante_Comercial	ENTV_Numero	CATA_FOPA_Codigo	ENTC_Numero	CATA_FOCO_Codigo	CATA_TIAN_Codigo	Numero_Licencia	Fecha_Vencimiento	CATA_CALI_Codigo	CATA_TISA_Codigo	Fecha_Vinculacion	CATA_TICO_Codigo	CATA_DEEM_Codigo	CATA_CAEM_Codigo	Estado	Justificacion_Bloqueo

                            if (!Eval(DataItem.CATA_TIID_Codigo)) { try { item.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + DataItem.CATA_TIID_Codigo); } catch (e) { } }
                            if (!Eval(DataItem.Numero_Identificacion)) { try { item.NumeroIdentificacion = $scope.MaskNumeroGrid(DataItem.Numero_Identificacion); } catch (e) { } }
                            if (DataItem.CATA_TIID_Codigo == 102) {
                                item.Nombre = ''
                                item.PrimeroApellido = ''
                                item.SegundoApellido = ''
                                item.Sexo = ''
                                if (!Eval(DataItem.Razon_Social)) { try { item.RazonSocial = DataItem.Razon_Social; } catch (e) { } }
                                if (!Eval(DataItem.Representante_Legal)) { try { item.RepresentanteLegal = DataItem.Representante_Legal; } catch (e) { } }
                            } else {
                                item.RazonSocial = ''
                                item.RepresentanteLegal = ''
                                if (!Eval(DataItem.Nombre)) { try { item.Nombre = DataItem.Nombre; } catch (e) { } }
                                if (!Eval(DataItem.Apellido1)) { try { item.PrimeroApellido = DataItem.Apellido1; } catch (e) { } }
                                if (!Eval(DataItem.Apellido2)) { try { item.SegundoApellido = DataItem.Apellido2; } catch (e) { } }
                                if (!Eval(DataItem.CATA_SEXO_Codigo)) { try { item.Sexo = $linq.Enumerable().From($scope.ListadoSexos).First('$.Codigo == ' + DataItem.CATA_SEXO_Codigo); } catch (e) { item.Sexo = $scope.ListadoSexos[0] } }
                            }

                            if (!Eval(DataItem.CIUD_Codigo)) { try { item.Ciudad = $scope.CargarCiudad(DataItem.CIUD_Codigo) } catch (e) { } }
                            if (!Eval(DataItem.Barrio)) { try { item.Barrio = DataItem.Barrio; } catch (e) { } }
                            if (!Eval(DataItem.Direccion)) { try { item.Direccion = DataItem.Direccion; } catch (e) { } }
                            if (!Eval(DataItem.Telefono)) { try { item.Telefonos = $scope.MaskNumeroGrid(DataItem.Telefono); } catch (e) { } }
                            if (!Eval(DataItem.Celular)) { try { item.Celular = $scope.MaskNumeroGrid(DataItem.Celular); } catch (e) { } }
                            if (!Eval(DataItem.Email)) { try { item.Correo = DataItem.Email; } catch (e) { } }
                            if (!Eval(DataItem.BANC_Codigo)) { try { item.Banco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo == ' + DataItem.BANC_Codigo); } catch (e) { } }
                            if (!Eval(DataItem.CATA_TICB_Codigo)) { try { item.TipoBanco = $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo == ' + DataItem.CATA_TICB_Codigo); } catch (e) { } }
                            if (!Eval(DataItem.Numero_Cuenta)) { try { item.CuentaBancaria = $scope.MaskNumeroGrid(DataItem.Numero_Cuenta); } catch (e) { } }
                            if (!Eval(DataItem.Titular)) { try { item.TitularCuentaBancaria = DataItem.Titular; } catch (e) { } }
                            if (!Eval(DataItem.Estado)) { try { item.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + DataItem.Estado); } catch (e) { } }
                            if (!Eval(DataItem.Justificacion_Bloqueo)) { try { item.JustificacionBloqueo = DataItem.Justificacion_Bloqueo; } catch (e) { } }
                            data.push(item)
                        }
                        $scope.DataArchivo = data
                        if ($scope.totalRegistros > 10) {
                            for (var i = 0; i < 10; i++) {
                                $scope.DataActual.push($scope.DataArchivo[i])
                            }
                        } else {
                            $scope.DataActual = $scope.DataArchivo
                        }
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    } catch (e) {
                        ShowError('Error en la lectura del archivo por favor intente nuevamente')
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    }


                }, 1000)
            }

        }
        var conterroresprevalidar = 0

        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            if ($scope.DataArchivo.Valores != undefined) {
                $scope.DataArchivo = { Valores: $scope.DataArchivo.Valores }
                AsignarValoresTabla()
            }
            else {
                ShowError('El arhivo cargado no corresponde al formato de carge masivo del tarifario')
            }
            blockUI.stop()

        }

        $scope.handleFile = function (e) {
            var files = e.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = e.target.result;
                $scope.arr = fixdata(data);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(btoa($scope.arr), { type: 'base64' });
                        process_wb($scope.wb);
                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);
            });
        }

        function restrow(item) {
            item.st = {}
            item.ms = {}
        }

        $scope.ValidarDocumento = function () {
            $scope.ListaErrores = []
            $scope.MensajesError = []
            $scope.DataErrorActual = [];
            /*
                        blockUI.start();
                        $timeout(function() {
                            blockUI.message('Validando Archivo');
                        }, 1000);*/
            var contadorgeneral = 0
            var contadorRegistros = 0
            $scope.DataVerificada = []
            $scope.DataVerificadatmp = []
            for (var i = 0; i < $scope.DataArchivo.length; i++) {
                var item = $scope.DataArchivo[i]
                item.ID = i
                //resetea todos los estilos de la fila que se va a evaluar
                restrow(item);
                var errores = 0
                //Condicion para saber si aplican las validaciones (Si el registro no tiene una ruta seleccionada no validara el resto de campos)
                if (item.Omitido !== true) {
                    contadorRegistros++
                    if (item.TipoIdentificacion == undefined || item.TipoIdentificacion == '') {
                        item.st.TipoIdentificacion = stError
                        item.ms.TipoIdentificacion = 'Ingrese el tipo de identificación'
                        errores++
                    }
                    if (item.NumeroIdentificacion == undefined || item.NumeroIdentificacion == '') {
                        item.st.NumeroIdentificacion = stError
                        item.ms.NumeroIdentificacion = 'Ingrese el número de identificación'
                        errores++
                    }
                    if (item.TipoIdentificacion != undefined) {
                        if (item.TipoIdentificacion.Codigo == 100) {
                            item.st.TipoIdentificacion = stError
                            item.ms.TipoIdentificacion = 'Ingrese el tipo de identificación'
                            errores++
                        } else {
                            if (item.TipoIdentificacion.Codigo == 102) {
                                item.TipoNaturaleza = { Codigo: 502 }
                            } else {
                                item.TipoNaturaleza = { Codigo: 501 }
                            }
                            if (item.TipoIdentificacion.Codigo == 102 && (item.RazonSocial == undefined || item.RazonSocial == '')) {
                                item.st.RazonSocial = stError
                                item.ms.RazonSocial = 'Ingrese la razon social'
                                errores++
                            }
                            if (item.TipoIdentificacion.Codigo == 102 && (item.RepresentanteLegal == undefined || item.RepresentanteLegal == '')) {
                                item.st.RepresentanteLegal = stError
                                item.ms.RepresentanteLegal = 'Ingrese el representante legal'
                                errores++
                            }
                            if (item.TipoIdentificacion.Codigo != 102 && (item.Nombre == undefined || item.Nombre == '')) {
                                item.st.Nombre = stError
                                item.ms.Nombre = 'Ingrese el Nombre'
                                errores++
                            }
                            if (item.TipoIdentificacion.Codigo != 102 && (item.PrimeroApellido == undefined || item.PrimeroApellido == '')) {
                                item.st.PrimeroApellido = stError
                                item.ms.PrimeroApellido = 'Ingrese el primer apellido'
                                errores++
                            }
                            if (item.TipoIdentificacion.Codigo != 102 && (item.Sexo == undefined || item.Sexo == '')) {
                                item.st.Sexo = stError
                                item.ms.Sexo = 'Ingrese el sexo'
                                errores++
                            } else {
                                if (item.Sexo.Codigo == 600) {
                                    item.st.Sexo = stError
                                    item.ms.Sexo = 'Ingrese el sexo'
                                    errores++
                                }
                            }
                        }
                    }
                    if (item.Ciudad == undefined || item.Ciudad == '') {
                        item.st.Ciudad = stError
                        item.ms.Ciudad = 'Ingrese la ciudad'
                        errores++
                    }
                    if (item.Direccion == undefined || item.Direccion == '') {
                        item.st.Direccion = stError
                        item.ms.Direccion = 'Ingrese la Direccion'
                        errores++
                    }
                    if (item.Telefonos == undefined || item.Telefonos == '') {
                        item.st.Telefonos = stError
                        item.ms.Telefonos = 'Ingrese el telefono'
                        errores++
                    }
                    if (item.Celular == undefined || item.Celular == '') {
                        item.st.Celular = stError
                        item.ms.Celular = 'Ingrese el Celular'
                        errores++
                    }
                    if (item.Correo == undefined || item.Correo == '') {
                        item.st.Correo = stError
                        item.ms.Correo = 'Ingrese el Correo'
                        errores++
                    }
                    var contperfiles = 0
                    var VarlidaCliente = false
                    var VarlidaConductor = false
                    var VarlidaEmpleado = false
                    var VarlidaProveedorTenedor = false
                    item.CadenaPerfiles = ''
                    for (var j = 0; j < item.perfilTerceros.length; j++) {
                        if (item.perfilTerceros[j].Estado == true) {
                            contperfiles++
                            if (item.perfilTerceros[j].Codigo == 1401) {
                                VarlidaCliente = true
                            }
                            if (item.perfilTerceros[j].Codigo == 1403) {
                                VarlidaConductor = true
                            }
                            if (item.perfilTerceros[j].Codigo == 1405) {
                                VarlidaEmpleado = true
                            }
                            if (item.perfilTerceros[j].Codigo == 1409 || item.perfilTerceros[j].Codigo == 1412) {
                                VarlidaProveedorTenedor = true
                            }
                            item.CadenaPerfiles = item.CadenaPerfiles + item.perfilTerceros[j].Codigo.toString() + ','
                        }

                    }
                    if (contperfiles == 0) {
                        item.st.Perfiles = stError
                        item.st.General = stError
                        item.ms.General = 'Seleccione al menos un perfil'
                        errores++
                    } else {
                        if (VarlidaCliente) {
                            if ((item.Cliente.Tarifario == undefined || item.Cliente.Tarifario == '') && $scope.ListadoTarifarios.length > 0) {
                                item.st.TarifarioVenta = stError
                                item.ms.TarifarioVenta = 'Ingrese el tarifario del cliente'
                                errores++
                            }
                        }
                        if (VarlidaProveedorTenedor) {
                            if ((item.Proveedor.Tarifario == undefined || item.Proveedor.Tarifario == '') && $scope.ListadoTarifariosCompra.length > 0) {
                                item.st.TarifarioCompra = stError
                                item.ms.TarifarioCompra = 'Ingrese el tarifario del tenedor/proveedor'
                                errores++
                            }
                        }
                        if (VarlidaConductor) {
                            if (item.Conductor.NumeroLicencia == undefined || item.Conductor.NumeroLicencia == '') {
                                item.st.NumeroLicencia = stError
                                item.ms.NumeroLicencia = 'Ingrese el número de licencia'
                                errores++
                            }
                            if (item.Conductor.FechaVencimiento == undefined || item.Conductor.FechaVencimiento == '') {
                                item.st.FechaVencimiento = stError
                                item.ms.FechaVencimiento = 'Ingrese la fecha de vencimiento de la licencia'
                                errores++
                            }
                            if (item.Conductor.CategoriaLicencia == undefined || item.Conductor.CategoriaLicencia == '') {
                                item.st.CategoriaLicencia = stError
                                item.ms.CategoriaLicencia = 'Ingrese la categoria de la licencia'
                                errores++
                            } else {
                                if (item.Conductor.CategoriaLicencia.Codigo == 1800) {
                                    item.st.CategoriaLicencia = stError
                                    item.ms.CategoriaLicencia = 'Ingrese la categoria de la licencia'
                                    errores++
                                }
                            }
                            if (item.Conductor.TipoSangre == undefined || item.Conductor.TipoSangre == '') {
                                item.st.TipoSangre = stError
                                item.ms.TipoSangre = 'Ingrese el tipo de sangre'
                                errores++
                            } else {
                                if (item.Conductor.TipoSangre.Codigo == 1900) {
                                    item.st.TipoSangre = stError
                                    item.ms.TipoSangre = 'Ingrese el tipo de sangre'
                                }
                            }
                        }
                        if (VarlidaEmpleado) {
                            if (item.Empleado.FechaVinculacion == undefined || item.Empleado.FechaVinculacion == '') {
                                item.st.FechaVinculacion = stError
                                item.ms.FechaVinculacion = 'Ingrese la fecha de vinculación'
                                errores++
                            }
                            if (item.Empleado.TipoContrato == undefined || item.Empleado.TipoContrato == '') {
                                item.st.TipoContrato = stError
                                item.ms.TipoContrato = 'Ingrese el tipo de contrato'
                                errores++
                            } else {
                                if (item.Empleado.TipoContrato.Codigo == 1200) {
                                    item.st.TipoContrato = stError
                                    item.ms.TipoContrato = 'Ingrese el tipo de contrato'
                                    errores++
                                }
                            }
                            if (item.Empleado.Departamento == undefined || item.Empleado.Departamento == '') {
                                item.st.Departamento = stError
                                item.ms.Departamento = 'Ingrese el departamento'
                                errores++
                            } else {
                                if (item.Empleado.Departamento.Codigo == 8700) {
                                    item.st.Departamento = stError
                                    item.ms.Departamento = 'Ingrese el departamento'
                                    errores++
                                }
                            }
                            if (item.Empleado.Cargo == undefined || item.Empleado.Cargo == '') {
                                item.st.Cargo = stError
                                item.ms.Cargo = 'Ingrese el cargo'
                                errores++
                            } else {
                                if (item.Empleado.Cargo.Codigo == 2000) {
                                    item.st.Cargo = stError
                                    item.ms.Cargo = 'Ingrese el cargo'
                                    errores++
                                }
                            }
                        }
                    }

                    if (errores > 0) {
                        item.st.Row = stwarning
                        $scope.ListaErrores.push(item)
                        contadorgeneral++
                    }
                    else {
                        item.stRow = ''
                        $scope.DataVerificada.push(item)
                    }
                }
                else {
                    restrow(item);
                }
            }
            //blockUI.stop();

            if (contadorgeneral > 0) {
                ShowError('Se encontraron inconsistencias en los datos ingresados, por favor corrija los datos marcados o seleccione omitirlos.');
                $scope.paginaActualError = 1
                $scope.DataErrorActual = []
                $scope.totalRegistrosErrores = $scope.ListaErrores.length
                $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 10);
                if ($scope.totalRegistrosErrores > 10) {
                    for (var i = 0; i < 10; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    for (var i = 0; i < $scope.ListaErrores.length; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                if (contadorRegistros == 0) {
                    ShowError('No se encontraron registros para validar, por favor revise que la casilla de omitir no se encuentre marcada en al menos un registro')
                } else {
                    ShowWarningConfirm('Los datos se verificaron correctamente.¿Desea continuar con el cargue de la información?', $scope.Guardar)
                }
            }


        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var modelo = $scope.Modelo
            if ($scope.Tarifarios.Codigo == 0) {

                if (ValidarCampo(modelo.Nombre) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el nombre ');
                    continuar = false;
                }
                if (ValidarFecha(modelo.FechaInicio) == FECHA_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar la fecha de inicio ');
                    continuar = false;
                }
                if (ValidarFecha(modelo.FechaFin) == FECHA_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar la fecha fin ');
                    continuar = false;
                } else if (ValidarFecha(modelo.FechaFin, false, true) == FECHA_ES_MENOR) {
                    $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha actual');
                    continuar = false;
                }
                if (ValidarFecha(modelo.FechaInicio) == FECHA_VALIDA && ValidarFecha(modelo.FechaFin, false, true) == FECHA_VALIDA) {
                    if (modelo.FechaInicio > modelo.FechaFin) {
                        $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha de inicio');
                        continuar = false;
                    }
                }
            } else {
                modelo.Nombre = $scope.Tarifarios.Nombre
                modelo.Codigo = $scope.Tarifarios.Codigo
                modelo.FechaInicio = $scope.Tarifarios.FechaInicio
                modelo.FechaInicio = $scope.Tarifarios.FechaInicio
            }
            return continuar;
        }


        $scope.Guardar = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        $scope.GuardarErrados = function () {
            //closeModal('ModalErrores')
            data = []
            for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                if (!$scope.ProgramacionesNoGuardadas[i].Omitido && $scope.ProgramacionesNoGuardadas[i].Option != 'Omitido') {
                    if ($scope.ProgramacionesNoGuardadas[i].Option == 'Remplazado') {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 1
                    } else {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 0
                    }
                    data.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
            $scope.DataVerificada = data
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        var contGuard = 0
        var contGuardGeneral = 0
        var contGeneral = 0
        var Guardaros = 0
        function GuardarCompleto(inicio, opt) {
            contGuard++
            if (contGuard > $scope.DataVerificada.length) {

                ShowSuccess('Se guardaron ' + contGuardGeneral + ' de ' + contGeneral + ' terceros')
                closeModal('modalConfirmacionRemplazarProgramacion');
                closeModal('ModalErrores');
                $scope.LimpiarData()

            } else {

                blockUI.start();
                $timeout(function () {
                    blockUI.message('Guardando Terceros');
                }, 1000);
                delete $scope.DataVerificada[inicio].st
                delete $scope.DataVerificada[inicio].ms
                delete $scope.DataVerificada[inicio].perfilTerceros
                delete $scope.DataVerificada[inicio].ContPerfiltes
                delete $scope.DataVerificada[inicio].PerfCliente
                delete $scope.DataVerificada[inicio].PerfConductor
                delete $scope.DataVerificada[inicio].PerfEmpleado
                delete $scope.DataVerificada[inicio].PerfPropietario
                delete $scope.DataVerificada[inicio].PerfProveedor
                delete $scope.DataVerificada[inicio].PerfTenedor
                delete $scope.DataVerificada[inicio].ID
                delete $scope.DataVerificada[inicio].stRow
                try {
                    $scope.DataVerificada[inicio].Cliente.RepresentanteComercial = { Codigo: $scope.DataVerificada[inicio].Cliente.RepresentanteComercial.Codigo }
                } catch (e) {
                }
                console.log($scope.DataVerificada[inicio])
                TercerosFactory.Guardar($scope.DataVerificada[inicio]).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        Guardaros++
                        contGuardGeneral++
                        GuardarCompleto(contGuard)
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $scope.ProgramacionesNoGuardadas.push($scope.DataVerificada[inicio])
                    GuardarCompleto(contGuard)
                    blockUI.stop();
                })
            }
        }

        $scope.ModalConfirmacionNuevoProceso = function () {
            showModal('modalConfirmacionCancelarProceso');
        }

        $scope.CerrarModalNuevoProceso = function (e) {
            var fileVal = document.getElementById("fileModal");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
            closeModal('modalConfirmacionCancelarProceso');


        }

        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (o) {
            return MascaraNumero(o)
        }
        $scope.MaskValoresGrid = function (o) {
            return MascaraValores(o)
        }
        $scope.MaskMayusGrid = function (o) {
            return MascaraMayus(o)
        }
        $scope.MaskplacaGrid = function (o) {
            return MascaraPlaca(o)
        }
        $scope.MaskplacaSemiremolqueGrid = function (o) {
            return MascaraPlacaSemirremolque(o)
        }

        $scope.GenerarPlanilla = function () {
            var entidad = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
            blockUI.start();
            $timeout(function () {
                blockUI.message('Generando Plantilla..');
            }, 1000);
            TercerosFactory.GenerarPlanitilla(entidad).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var pdfAsDataUri = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data.Datos.Planitlla;
                    var link = document.createElement('a');
                    link.href = pdfAsDataUri;
                    link.download = "Plantilla_Cargue_Masivo_Terceros.xlsx";
                    link.click();
                    //window.open(pdfAsDataUri);
                    blockUI.stop();
                    ShowSuccess('La plantilla se generó correctamente')
                }
            }, function (response) {
                blockUI.stop();
                ShowError(response.statusText)
            })

        }

        $scope.DataActual = []
        //$scope.DataActual.push({})
    }]);

