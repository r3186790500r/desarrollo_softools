﻿EncoExpresApp.factory('UsuariosFactory', ['$http', function ($http) {

    var urlService = GetUrl('Usuarios');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.ConsultarUsuarioGrupoSeguridades = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarUsuarioGrupoSeguridades';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.GuardarUsuarioGrupoSeguridades = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GuardarUsuarioGrupoSeguridades';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.ConsultarUsuarioOficinas = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarUsuarioOficinas';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.GuardarUsuarioOficinas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GuardarUsuarioOficinas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.RegistrarActividad = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/RegistrarActividad';
        request.data = entidad;
        return $http(request);
    };

    return factory;
}]);