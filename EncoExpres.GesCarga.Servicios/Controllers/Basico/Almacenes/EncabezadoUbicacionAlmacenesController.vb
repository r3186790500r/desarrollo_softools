﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Negocio.Basico.Almacen

''' <summary>
''' Controlador <see cref="EncabezadoUbicacionAlmacenesController"/>
''' </summary>
<Authorize>
Public Class EncabezadoUbicacionAlmacenesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoUbicacionAlmacenes) As Respuesta(Of IEnumerable(Of EncabezadoUbicacionAlmacenes))
        Return New LogicaEncabezadoUbicacionAlmacen(CapaPersistenciaEncabezadoUbicacionAlmacenes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoUbicacionAlmacenes) As Respuesta(Of EncabezadoUbicacionAlmacenes)
        Return New LogicaEncabezadoUbicacionAlmacen(CapaPersistenciaEncabezadoUbicacionAlmacenes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoUbicacionAlmacenes) As Respuesta(Of Long)
        Return New LogicaEncabezadoUbicacionAlmacen(CapaPersistenciaEncabezadoUbicacionAlmacenes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Eliminar(ByVal entidad As EncabezadoUbicacionAlmacenes) As Respuesta(Of Boolean)
        Return New LogicaEncabezadoUbicacionAlmacen(CapaPersistenciaEncabezadoUbicacionAlmacenes).Eliminar(entidad)
    End Function

End Class
