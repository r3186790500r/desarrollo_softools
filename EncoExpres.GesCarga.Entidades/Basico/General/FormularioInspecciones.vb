﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" FormularioInspecciones"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class FormularioInspecciones
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="FormularioInspecciones"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="FormularioInspecciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            Preoperacional = Read(lector, "Preoperacional")
            Firma = Read(lector, "Firma")
            Version = Read(lector, "Version")
            TipoDocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "TIDO_Codigo_Origen"), .Nombre = Read(lector, "Nombre_Tipo_Documento_Origen")}
            Obtener = Read(lector, "Obtener")

            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
            TipoInspeccion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIIN_Codigo")}
        End Sub
        <JsonProperty>
        Public Property TipoInspeccion As ValorCatalogos
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Version As String
        <JsonProperty>
        Public Property ConsularDetalle As Short
        <JsonProperty>
        Public Property Preoperacional As Short
        <JsonProperty>
        Public Property Firma As Short
        <JsonProperty>
        Public Property TipoDocumentoOrigen As ValorCatalogos
        <JsonProperty>
        Public Property itemFormulario As IEnumerable(Of ItemFormularioInspecciones)
        <JsonProperty>
        Public Property SeccionFormulario As IEnumerable(Of SeccionFormularioInspecciones)
    End Class
End Namespace
