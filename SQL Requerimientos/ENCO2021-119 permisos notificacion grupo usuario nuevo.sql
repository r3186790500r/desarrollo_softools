USE [ENCOEXPRES]
GO

/****** Object:  StoredProcedure [dbo].[gsp_insertar_grupo_usuarios]    Script Date: 14/02/2022 11:48:13 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER   PROCEDURE [dbo].[gsp_insertar_grupo_usuarios]  
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo_Grupo VARCHAR(50),  
@par_Nombre VARCHAR (20),  
@par_USUA_Codigo_Crea SMALLINT  
)  
AS  
BEGIN  
INSERT INTO Grupo_Usuarios  
(  
EMPR_Codigo,  
Codigo_Grupo,  
Nombre,  
USUA_Codigo_Crea,  
Fecha_Crea  
)  
VALUES  
(  
@par_EMPR_Codigo,  
@par_Codigo_Grupo,  
@par_Nombre,  
@par_USUA_Codigo_Crea,  
GETDATE()  
)

DECLARE @id AS NUMERIC 
SET @id = @@IDENTITY


INSERT INTO 
 Permiso_Grupo_Usuarios  
 SELECT EMPR_Codigo  
 ,Codigo  
 ,@id  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0
 ,0
 FROM Menu_Aplicaciones   
 WHERE EMPR_Codigo = @par_EMPR_Codigo  

INSERT INTO Permiso_Grupo_Bandeja_Notificaciones  
SELECT EMPR_Codigo  
 ,@id  
 ,Codigo  
 ,0  
 ,0  
 FROM Valor_Catalogos   
 WHERE EMPR_Codigo = @par_EMPR_Codigo
 AND CATA_Codigo = 185 --notificaciones

SELECT @@ROWCOUNT AS Codigo  
  
END  
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_eliminar_grupo_usuarios]  
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo SMALLINT  
)  
AS  
BEGIN  
  
  
DELETE Permiso_Grupo_Usuarios 
WHERE EMPR_Codigo = @par_EMPR_Codigo AND 
GRUS_Codigo =  @par_Codigo

DELETE Permiso_Grupo_Bandeja_Notificaciones 
WHERE EMPR_Codigo = @par_EMPR_Codigo AND 
GRUS_Codigo =  @par_Codigo

DELETE Grupo_Usuarios  
WHERE  
EMPR_Codigo = @par_EMPR_Codigo  
AND Codigo = @par_Codigo  
  
SELECT @@ROWCOUNT AS Codigo  
  
END  
GO


