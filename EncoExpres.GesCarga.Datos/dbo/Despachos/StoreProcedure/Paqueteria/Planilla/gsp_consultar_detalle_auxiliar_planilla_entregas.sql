﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_consultar_detalle_auxiliar_planilla_entregas'
DROP PROCEDURE gsp_consultar_detalle_auxiliar_planilla_entregas 
GO

CREATE PROCEDURE gsp_consultar_detalle_auxiliar_planilla_entregas (  
     @par_EMPR_Codigo SMALLINT,        
     @par_Numero NUMERIC = null,  
	 @par_NumeroPagina  NUMERIC = null,
     @par_RegistrosPagina NUMERIC = null 
 )  
 AS  
 BEGIN  
  SET NOCOUNT ON;  
   DECLARE  
    @CantidadRegistros INT  
   SELECT @CantidadRegistros = (  
     SELECT DISTINCT   
      COUNT(1)   
      FROM Detalle_Auxiliares_Planilla_Entregas DAPE
	  ,Encabezado_Planilla_Entregas ENPE
	  ,Terceros FUNC

      WHERE  
	  DAPE.EMPR_Codigo = ENPE.EMPR_Codigo
	  AND DAPE.ENPE_Numero = ENPE.Numero

	  AND DAPE.EMPR_Codigo = FUNC.EMPR_Codigo
	  AND DAPE.TERC_Codigo_Funcionario = FUNC.Codigo


	  AND ENPE.Numero = @par_Numero
       );         
     WITH Pagina AS  
     (  
      SELECT 
	  DAPE.EMPR_Codigo,
	  DAPE.ENPE_Numero ENPR_Numero,
	  FUNC.Codigo TERC_Codigo_Funcionario,
      ISNULL(FUNC.Razon_Social,'')+' '+ISNULL(FUNC.Nombre,'')  
	+' '+ISNULL(FUNC.Apellido1,'')+' '+ISNULL(FUNC.Apellido2,'') AS Nombre_Funcionario,
	  DAPE.Numero_Horas_Trabajadas,
	  DAPE.Valor,
      DAPE.Observaciones,
	  ROW_NUMBER() OVER ( ORDER BY   ENPE.NUMERO) AS RowNumber  
	  FROM Detalle_Auxiliares_Planilla_Entregas DAPE
	  ,Encabezado_Planilla_Entregas ENPE
	  ,Terceros FUNC

      WHERE  
	  DAPE.EMPR_Codigo = ENPE.EMPR_Codigo
	  AND DAPE.ENPE_Numero = ENPE.Numero
	  AND DAPE.EMPR_Codigo = FUNC.EMPR_Codigo
	  AND DAPE.TERC_Codigo_Funcionario = FUNC.Codigo
	AND ENPE.Numero = @par_Numero
	      ) 
   SELECT DISTINCT  
	EMPR_Codigo,
	ENPR_Numero,
	TERC_Codigo_Funcionario,
	Nombre_Funcionario,
	Numero_Horas_Trabajadas,
	Valor,
	Observaciones,
	@CantidadRegistros AS TotalRegistros,  
    @par_NumeroPagina AS PaginaObtener,  
    @par_RegistrosPagina AS RegistrosPagina  
    FROM  
    Pagina  
    WHERE  
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    order by  TERC_Codigo_Funcionario
   END   
GO