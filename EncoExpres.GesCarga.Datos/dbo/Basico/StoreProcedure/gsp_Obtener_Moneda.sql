﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_Obtener_Moneda'
GO
DROP PROCEDURE gsp_Obtener_Moneda
GO
CREATE PROCEDURE gsp_Obtener_Moneda(  
@par_EMPR_Codigo NUMERIC,  
@par_Codigo NUMERIC=NULL,
@par_Local smallint=null)  
AS BEGIN   
	SELECT 
	EMPR_Codigo,
	Codigo,
	Nombre,
	Nombre_Corto,	
	Simbolo,	
	Local,
	Estado,
	 0 AS Valor_Moneda_Local 
	FROM Monedas 
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	and Codigo= @par_Codigo  
	and Local = @par_Local

END  
GO