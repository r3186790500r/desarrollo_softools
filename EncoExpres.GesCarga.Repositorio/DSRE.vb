﻿Imports Newtonsoft.Json

Public Class DSRE
    Public Function DSR(Of T)(objeto As Object) As T
        Return JsonConvert.DeserializeObject(Of T)(objeto.ToString())
    End Function
End Class
