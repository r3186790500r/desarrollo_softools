﻿EncoExpresApp.controller("ConsultarAsignacionPerfilesCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'GrupoUsuariosFactory', 'PermisoGrupoUsuariosFactory', 'ValorCatalogosFactory',
    function ($scope, $timeout, $linq, blockUI, GrupoUsuariosFactory, PermisoGrupoUsuariosFactory, ValorCatalogosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Utilitarios' }, { Nombre: 'Notificaciones' }, { Nombre: 'Asignación Perfiles' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.Seleccionado = false;
        $scope.TipoAplicacion = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;


        $scope.Auxtree = [];
        $scope.tree = [];
        $scope.FiltroOpcion = '';

        $scope.TodosConsultar = false;
        $scope.TodosAutorizar = false;
        $scope.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;

        $scope.ListadoAplicativos = [
            { Nombre: '(Seleccione...)', Codigo: -1 },
            { Nombre: 'APLICATIVO', Codigo: 220 },
            { Nombre: 'PORTAL', Codigo: 210 },
            { Nombre: 'GESPHONE', Codigo: 150 },
        ];
        $scope.TipoAplicativo = $linq.Enumerable().From($scope.ListadoAplicativos).First('$.Codigo == -1');
        $scope.changeTree = function () {

        }

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        };


        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ASIGNACION_PERFILES); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN E IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarGrupoPerfiles';
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------//

        $scope.ListadoGrupos = [];

        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
        }
        blockUI.delay = 1000;
        GrupoUsuariosFactory.Consultar(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoGrupos = response.data.Datos;

                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                        $scope.ResultadoSinRegistros = '';
                    }
                    else {
                        $scope.totalRegistros = 0;
                        $scope.totalPaginas = 0;
                        $scope.paginaActual = 1;
                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                    }
                }
                blockUI.stop();
            }, function (response) {
                ShowError(response.statusText);
                blockUI.stop();
            });



        Find();


        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.ListadoNotificaciones = [];

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_PROCESO_NOTIFICACIONES }
            }
            blockUI.delay = 1000;
            ValorCatalogosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNotificaciones = response.data.Datos;

                            //$scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            //$scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            //$scope.totalRegistros = 0;
                            //$scope.totalPaginas = 0;
                            //$scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                        }
                    }
                    blockUI.stop();
                }, function (response) {
                    ShowError(response.statusText);
                    blockUI.stop();
                });
        }

        $scope.MarcarNotificacionesConsultar = function (value) {
            $scope.ListadoGrupos.forEach(item => {
                item.Consultar = value;
                if (value == false) {
                    item.Autorizar = false;
                }
            });
        }

        $scope.MarcarNotificacionesAutorizar = function (value) {
            $scope.ListadoGrupos.forEach(item => {
                item.Autorizar = value;
                if (value == true) {
                    item.Consultar = true;
                }
            });
        }

        $scope.ValidarConsultarIndividual = function (item) {
            if (item.Consultar == false) {
                item.Autorizar = false;
            }
        }
        $scope.ValidarAutorizarIndividual = function (item) {
            if (item.Autorizar == true) {
                item.Consultar = true;
            }
        }

        //----------------------------PERMISOS----------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.CerrarModal = function () {
            closeModal('modalEliminarGrupo');
            closeModal('modalMensajeEliminarGrupo');
        }

        $scope.MaskNumero = function () {
            $scope.Modelo.CodigoGrupo = MascaraNumero($scope.Modelo.CodigoGrupo)
        };

        /*Funcion que solicita los permisos */
        $scope.AsignarGruposUsuarios = function (item) {
            $scope.itemModal = item;
            $scope.ListadoGrupos.forEach(item => {
                item.Consultar = false;
                item.Autorizar = false;
            });

            var filtrosNotificacion = {
                CodigoEmpresa: $scope.CodigoEmpresa,
                Notificacion: { Codigo: item.Codigo },
                Sync: true
            }

            var ResponseAsignacionPerfiles = PermisoGrupoUsuariosFactory.ConsultarAsigancionPerfilesNotificaciones(filtrosNotificacion).Datos;
            if (ResponseAsignacionPerfiles != undefined) {
                ResponseAsignacionPerfiles.forEach(item => {
                    $scope.ListadoGrupos.forEach(grupo => {
                        if (grupo.Codigo == item.Grupo.Codigo) {
                            grupo.Consultar = item.Consultar == 1 ? true : false;
                            grupo.Autorizar = item.Autorizar == 1 ? true : false;
                        }
                    });
                });
            }
            showModal('modalGruposUsuarios');
            //$scope.ListadoPermisos = [];
        };

        $scope.changedCB = function (e, data) {
            $scope.Nodo = { Codigo: data.node.id, Nombre: data.node.text, Padre: data.node.parent };
            if ($scope.Nodo.Padre != "#") {
                blockUI.start('Cargando ...');

                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);




                showModal('modalModificarPermisos');
                $scope.CargarPermisos();
                blockUI.stop();
            }



        };

        $scope.GuardarAsignaciones = function () {

            var ListadoAsignaciones = [];

            $scope.ListadoGrupos.forEach(item => {
                ListadoAsignaciones.push({
                    Grupo: { Codigo: item.Codigo },
                    Consultar: item.Consultar == true || item.Consultar > 0 ? 1 : 0,
                    Autorizar: item.Autorizar == true || item.Autorizar > 0 ? 1 : 0
                });
            });

            var Entidad = {
                CodigoEmpresa: $scope.CodigoEmpresa,
                Notificacion: { Codigo: $scope.itemModal.Codigo },
                ListaAsignaciones: ListadoAsignaciones
            }


            PermisoGrupoUsuariosFactory.GuardarAsigancionPerfilesNotificaciones(Entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('Se guardaron los permisos');
                            closeModal('modalGruposUsuarios');

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            //$scope.ListadoPermisos.forEach(function (item) {
            //    if (item.Habilitado == true) {
            //        item.Habilitar = 1;
            //    } else {
            //        item.Habilitar = 0;
            //    }
            //    PermisoGrupoUsuariosFactory.Guardar(item).
            //        then(function (response) {
            //            if (response.data.ProcesoExitoso == true) {
            //                if (response.data.Datos > 0) {
            //                }
            //                else {
            //                    ShowError(response.data.MensajeOperacion);
            //                }
            //            }
            //            else {
            //                ShowError(response.data.MensajeOperacion);
            //            }
            //        }, function (response) {
            //            ShowError(response.statusText);
            //        });
            //})
        };
    }]);
