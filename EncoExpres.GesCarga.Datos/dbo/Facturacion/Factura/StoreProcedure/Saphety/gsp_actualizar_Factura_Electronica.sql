﻿
Print 'CREATE PROCEDURE gsp_actualizar_Factura_Electronica'
GO
DROP PROCEDURE gsp_actualizar_Factura_Electronica
GO

CREATE PROCEDURE gsp_actualizar_Factura_Electronica (
@par_EMPR_Codigo	   smallint,
@par_ENFA_Numero	   NUMERIC,
@par_ID_NUM_DOC_PRO	   VARCHAR(MAX) = ''
)
AS
BEGIN
	UPDATE Factura_Electronica SET 
	ID_Numero_Documento_Proveedor = @par_ID_NUM_DOC_PRO
	
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND ENFA_Numero = @par_ENFA_Numero
	
	UPDATE Encabezado_Facturas SET 
	Factura_Electronica = 1
	
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND Numero = @par_ENFA_Numero
	
	SELECT ENFA_Numero AS ENFA_Numero
	FROM Factura_Electronica
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND ENFA_Numero=@par_ENFA_Numero
END
GO