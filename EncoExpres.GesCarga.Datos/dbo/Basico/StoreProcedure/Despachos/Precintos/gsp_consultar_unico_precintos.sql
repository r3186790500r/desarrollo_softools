﻿
DROP PROCEDURE gsp_Obtener_detalle_Precinto    
GO
CREATE PROCEDURE gsp_Obtener_detalle_Precinto       
(@par_Empr_Codigo Numeric       
,@par_Numero numeric )      
AS BEGIN      
    
SELECT        
DAPO.ID IDPrecinto,      
DAPO.EMPR_Codigo,      
DAPO.Numero_Precinto,      
isnull(ENMC.Numero_Documento ,0) as NumeroMafiesto,      
isnull(ENPD.Numero_Documento ,0) as NumeroPlanillaDespacho,     
ISNULL(ENRE.Numero_Documento,0) AS NumeroRemesa,   
OFIC.Nombre NombreOficina,      
OFOR.Nombre NombreOficinaOrigen,      
DAPO.Estado,      
DAPO.Anulado      
FROM Detalle_Asignacion_Precintos_Oficina DAPO      
      
LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON       
DAPO.EMPR_Codigo = ENMC.EMPR_Codigo      
AND DAPO.ENMD_Numero = ENMC.Numero      
      
LEFT JOIN Encabezado_Planilla_Despachos ENPD ON       
DAPO.EMPR_Codigo = ENPD.EMPR_Codigo       
AND DAPO.ENPD_Numero = ENPD.Numero       
      
LEFT JOIN Oficinas OFIC on       
DAPO.EMPR_Codigo = OFIC.EMPR_Codigo      
AND DAPO.OFIC_Codigo = OFIC.Codigo      
      
LEFT JOIN Oficinas OFOR on       
DAPO.EMPR_Codigo = OFOR.EMPR_Codigo      
AND DAPO.OFIC_Codigo_Origen = OFOR.Codigo      
      
 LEFT JOIN Encabezado_Remesas ENRE ON   
DAPO.EMPR_Codigo = ENRE.EMPR_Codigo   
AND DAPO.ENRE_Numero = ENRE.Numero    
AND ENRE.TIDO_Codigo = 100  
  
WHERE DAPO.EMPR_Codigo = @par_Empr_Codigo      
AND DAPO.EAPO_Numero = @par_Numero      
AND (ENPD.TIDO_Codigo = 150 or ENPD.TIDO_Codigo  is null)  
AND DAPO.ENPD_Numero = 0    
AND DAPO.ENRE_Numero = 0    
AND DAPO.ENMD_Numero = 0    
END       
GO

PRINT 'gsp_consultar_unico_precintos'
DROP PROCEDURE gsp_consultar_unico_precintos   
GO

CREATE PROCEDURE gsp_consultar_unico_precintos   
(              
@par_empr_codigo NUMERIC,           
@par_oficina_Origen NUMERIC = null,              
@par_Oficina_Destino NUMERIC  = null,       
@par_Tipo_Precinto NUMERIC  = null,             
@par_Numero_Inicial NUMERIC = null  ,    
@par_Numero_Final NUMERIC = null  ,    
@par_Estado numeric = null,    
@par_Estado_Precinto numeric = null,    
@par_ENPD_Numero NUMERIC = null  ,    
@par_ENRE_Numero NUMERIC = null  ,    
@par_ENMC_Numero NUMERIC = null  ,    
@par_numeropagina numeric,              
@par_registrospagina numeric                        
)                
as                
begin                
declare                
--estados    
--0 disponible    
--1 ocupado    
--2 anulado    
  @cantidadregistros int                
  select @cantidadregistros = (                
    select distinct                 
     count(1)                 
        
from Detalle_Asignacion_Precintos_Oficina DAPO        
        
LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON         
DAPO.EMPR_Codigo = ENMC.EMPR_Codigo        
AND DAPO.ENMD_Numero = ENMC.Numero       
    
LEFT JOIN Encabezado_Planilla_Despachos  ENPD ON         
DAPO.EMPR_Codigo = ENPD.EMPR_Codigo         
AND DAPO.ENPD_Numero = ENPD.Numero         
        
LEFT JOIN Oficinas OFIC on         
DAPO.EMPR_Codigo = OFIC.EMPR_Codigo        
AND DAPO.OFIC_Codigo = OFIC.Codigo        
        
LEFT JOIN Oficinas OFOR on         
DAPO.EMPR_Codigo = OFOR.EMPR_Codigo        
AND DAPO.OFIC_Codigo_Origen = OFOR.Codigo        
        
LEFT JOIN Encabezado_Remesas ENRE ON     
DAPO.EMPR_Codigo = ENRE.EMPR_Codigo     
AND DAPO.ENRE_Numero = ENRE.Numero      
AND ENRE.TIDO_Codigo = 100    
    
WHERE DAPO.EMPR_Codigo = @par_Empr_Codigo    
AND DAPO.Numero_Precinto >= ISNULL(@par_Numero_Inicial, DAPO.Numero_Precinto)  
AND DAPO.Numero_Precinto <= ISNULL(@par_Numero_Final, DAPO.Numero_Precinto)      
AND DAPO.OFIC_Codigo = ISNULL(@par_Oficina_Destino,DAPO.OFIC_Codigo)    
AND DAPO.OFIC_Codigo_Origen = ISNULL(@par_oficina_Origen,DAPO.OFIC_Codigo_Origen)   
AND (ENPD.Numero_Documento = @par_ENPD_Numero or @par_ENPD_Numero is null)   
AND (ENRE.Numero_Documento = @par_ENRE_Numero or @par_ENRE_Numero is null)   
AND (ENMC.Numero_Documento = @par_ENMC_Numero or @par_ENMC_Numero is null)        
AND (case   
WHEN DAPO.Estado = 1 AND DAPO.Anulado = 0  AND DAPO.ENRE_Numero = 0 THEN 0    
WHEN DAPO.Estado = 1 AND DAPO.Anulado = 0  AND DAPO.ENRE_Numero > 0 THEN 1    
WHEN DAPO.Anulado = 1  THEN 2 END      
= @par_Estado_Precinto OR  @par_Estado_Precinto IS NULL    
)     
AND (ENPD.TIDO_Codigo = 150 or ENPD.TIDO_Codigo  is null)             
     );                
                            
    with pagina as                
    (                
                
select               
DAPO.ID IDPrecinto,        
DAPO.EMPR_Codigo,        
DAPO.Numero_Precinto,        
ISNULL(ENMC.Numero_Documento ,0) as NumeroMafiesto,        
ISNULL(ENPD.Numero_Documento ,0) as NumeroPlanillaDespacho,        
ISNULL (ENRE.Numero_Documento,0) AS NumeroRemesa,    
OFIC.Nombre NombreOficina,        
OFOR.Nombre NombreOficinaOrigen,
CASE WHEN DAPO.ENRE_Numero <> 0 OR DAPO.ENMD_Numero <> 0 OR DAPO.ENPD_Numero <> 0 THEN 1
WHEN DAPO.Anulado = 1 THEN 2
ELSE 0 END AS Estado,        
DAPO.Anulado , 
DAPO.ENRE_Numero,
ROW_NUMBER() over(order by DAPO.Numero_Precinto) as rownumber                
from Detalle_Asignacion_Precintos_Oficina DAPO        
        
LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON         
DAPO.EMPR_Codigo = ENMC.EMPR_Codigo        
AND DAPO.ENMD_Numero = ENMC.Numero        
    
LEFT JOIN Encabezado_Remesas ENRE ON     
DAPO.EMPR_Codigo = ENRE.EMPR_Codigo     
AND DAPO.ENRE_Numero = ENRE.Numero      
AND ENRE.TIDO_Codigo = 100    
    
LEFT JOIN Encabezado_Planilla_Despachos  ENPD ON         
DAPO.EMPR_Codigo = ENPD.EMPR_Codigo         
AND DAPO.ENPD_Numero = ENPD.Numero         
        
LEFT JOIN Oficinas OFIC on         
DAPO.EMPR_Codigo = OFIC.EMPR_Codigo        
AND DAPO.OFIC_Codigo = OFIC.Codigo        
        
LEFT JOIN Oficinas OFOR on         
DAPO.EMPR_Codigo = OFOR.EMPR_Codigo        
AND DAPO.OFIC_Codigo_Origen = OFOR.Codigo        
        
WHERE DAPO.EMPR_Codigo = @par_Empr_Codigo        
AND DAPO.Numero_Precinto >= ISNULL(@par_Numero_Inicial, DAPO.Numero_Precinto)  
AND DAPO.Numero_Precinto <= ISNULL(@par_Numero_Final, DAPO.Numero_Precinto)     
AND DAPO.OFIC_Codigo = ISNULL(@par_Oficina_Destino,DAPO.OFIC_Codigo)    
AND DAPO.OFIC_Codigo_Origen = ISNULL(@par_oficina_Origen,DAPO.OFIC_Codigo_Origen)    
AND (ENPD.Numero_Documento = @par_ENPD_Numero or @par_ENPD_Numero is null)   
AND (ENRE.Numero_Documento = @par_ENRE_Numero or @par_ENRE_Numero is null)   
AND (ENMC.Numero_Documento = @par_ENMC_Numero or @par_ENMC_Numero is null)     
AND (case       
WHEN DAPO.Estado = 1 AND DAPO.Anulado = 0  AND DAPO.ENRE_Numero = 0 THEN 0    
WHEN DAPO.Estado = 1 AND DAPO.Anulado = 0  AND DAPO.ENRE_Numero > 0 THEN 1    
WHEN DAPO.Anulado = 1  THEN 2 END       
= @par_Estado_Precinto OR  @par_Estado_Precinto IS NULL    
)       
AND (ENPD.TIDO_Codigo = 150 or ENPD.TIDO_Codigo  is null)             
  )                
  SELECT DISTINCT                
  0 as obtener,                
  EMPR_Codigo ,              
  IDPrecinto,        
  EMPR_Codigo,        
  Numero_Precinto,        
  NumeroMafiesto,        
  NumeroPlanillaDespacho,        
  NombreOficina,        
  NombreOficinaOrigen,        
  NumeroRemesa,
  Estado,      
  ENRE_Numero,
 Anulado,              
 @cantidadregistros as TotalRegistros,                
 @par_numeropagina as PaginaObtener,                
 @par_registrospagina as RegistrosPagina                
 FROM                
 pagina                
 WHERE                
 rownumber > (isnull(@par_numeropagina, 1) -1) * isnull(@par_registrospagina, @cantidadregistros)                
 and rownumber <= isnull(@par_numeropagina, 1) * isnull(@par_registrospagina, @cantidadregistros)                
 order by Numero_Precinto                
                 
END                
GO