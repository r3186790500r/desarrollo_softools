﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Repositorio.Basico.Almacen

Namespace Basico.Almacen
    Public NotInheritable Class PersistenciaDetalleUbicacionAlmacenes
        Implements IPersistenciaBase(Of DetalleUbicacionAlmacenes)

        Public Function Consultar(filtro As DetalleUbicacionAlmacenes) As IEnumerable(Of DetalleUbicacionAlmacenes) Implements IPersistenciaBase(Of DetalleUbicacionAlmacenes).Consultar
            Return New RepositorioDetalleUbicacionAlmacenes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleUbicacionAlmacenes) As Long Implements IPersistenciaBase(Of DetalleUbicacionAlmacenes).Insertar
            Return New RepositorioDetalleUbicacionAlmacenes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleUbicacionAlmacenes) As Long Implements IPersistenciaBase(Of DetalleUbicacionAlmacenes).Modificar
            Return New RepositorioDetalleUbicacionAlmacenes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleUbicacionAlmacenes) As DetalleUbicacionAlmacenes Implements IPersistenciaBase(Of DetalleUbicacionAlmacenes).Obtener
            Return New RepositorioDetalleUbicacionAlmacenes().Obtener(filtro)
        End Function
    End Class

End Namespace

