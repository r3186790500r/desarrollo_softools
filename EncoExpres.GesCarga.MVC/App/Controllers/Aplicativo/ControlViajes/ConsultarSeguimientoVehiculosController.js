﻿EncoExpresApp.controller("ConsultarSeguimientoVehiculosCtrl", ['$scope', '$timeout', 'RutasFactory', '$linq', 'DetalleSeguimientoVehiculosFactory', 'blockUIConfig',
    'ValorCatalogosFactory', 'TercerosFactory', 'EventoCorreosFactory', 'DetalleDistribucionRemesasFactory', 'ProductoTransportadosFactory', 'RemesasFactory',
    'VehiculosFactory', 'ReporteMinisterioFactory', 'OficinasFactory', 'CiudadesFactory', 'PuestoControlesFactory', 'PlanillaDespachosFactory', 'blockUI', 'EmpresasFactory',
    'SitiosTerceroClienteFactory', 'OrdenCargueFactory',
    function ($scope, $timeout, RutasFactory, $linq, DetalleSeguimientoVehiculosFactory, blockUIConfig,
        ValorCatalogosFactory, TercerosFactory, EventoCorreosFactory, DetalleDistribucionRemesasFactory, ProductoTransportadosFactory, RemesasFactory,
        VehiculosFactory, ReporteMinisterioFactory, OficinasFactory, CiudadesFactory, PuestoControlesFactory, PlanillaDespachosFactory, blockUI, EmpresasFactory,
        SitiosTerceroClienteFactory, OrdenCargueFactory) {

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        // Se crea la propiedad modelo en el ambito
        $scope.pref = '';
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.VerOpcionPDF = false;
        $('#TabPlanillas').show()
        $('#TabPlanillas2').show()
        $scope.Min_Date = MIN_DATE
        $scope.ContNivel = 0; 
        //Obtiene la URL para llamar el proyecto ASP
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DeshabilitarDetalle = false;
        $scope.DeshabilitarPuestoControl = true;
        $scope.DeshabilitarFlotaPropia = false;
        var filtros = {};
        $scope.ListadoOficinas = [];
        $scope.ListadoNivelRiesgo = [];
        $scope.cantidadRegistrosPorPaginas = 20;
        $scope.paginaActual = 1;
        $scope.paginaActualOrden = 1;
        $scope.paginaActualRemesa = 1;
        $scope.paginaActualFlotaPropia = 1;
        $scope.paginaActualRNDC = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.ListaPuntosControl = [];
        $scope.ListaSitioReporteModal = [];
        $scope.CodigoSitioReporteSeguimiento = 0;
        $scope.ListaNovedadSeguimiento = [];
        $scope.CodigoNovedadSeguimiento = 0;
        $scope.ListadoSeguimientosPlanilla = [];
        $scope.ListadoSeguimientosOrden = [];
        $scope.MensajesErrorSeguimiento = [];
        $scope.ListaCantidadSeguimientos = [];
        $scope.ListaTipoOrigenSeguimiento = [];
        $scope.ConsultaProgramacion = {};
        $scope.MensajesErrorAnular = [];
        $scope.CorreosTransportador = [];
        $scope.CorreosClientes = [];
        $scope.TipoConsulta = 1;
        $scope.ValorBanderaAnularRemesa = 0;
        $scope.ListaPuntosGestionModal = [];
        $scope.ListaProductoTransportados = [];
        $scope.ListadoSeguimientosRemesasPlanillaDespachos = [];
        $scope.Correo = {};
        $scope.Modelo = {
            Orden: 0
        };
        $scope.Modelo.FechaSeguimiento = new Date();

        $scope.Remesas = {
            RegistrosPagina: 20,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
        };
        $scope.Distribucion = {};
        $scope.Despachos = {
            Estado: false
        };
        $scope.Ordenes = {
            Estado: false
        };
        $scope.FlotaPropia = {
            Estado: false
        };
        $scope.RNDC = {};

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            try {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SEGUIMIENTO_VEHICULAR);
                $scope.MapaSitio = [{ Nombre: 'Control Trafico' }, { Nombre: 'Procesos' }, { Nombre: 'Seguimiento Vehicular en Tránsito' }];
                $('#ConsultaOrdenesCargue').hide(); $('#ConsultaPlanillasDespachos').show(); $('#ConsultaRemesasPlanillasDespachos').hide(); $('#ConsultaFlotaPropia').hide(); $('#ConsultaRNDC').hide();
            } catch (e) {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SEGUIMIENTO_VEHICULAR);
                $scope.MapaSitio = [{ Nombre: 'Portal' }, { Nombre: 'Control Trafico' }, { Nombre: 'Procesos' }, { Nombre: 'Seguimiento Vehicular en Tránsito' }];
                $('#ConsultaOrdenesCargue').hide(); $('#ConsultaPlanillasDespachos').show(); $('#ConsultaRemesasPlanillasDespachos').hide(); $('#ConsultaFlotaPropia').hide(); $('#ConsultaRNDC').hide();
            }
        } catch (e) {
            ShowError('No Tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.ClienteOrdenCargue = "";
        $scope.MensajesErrorCargueDescargue = [];
        $scope.OrdenCargueObj = {};
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR PLANILLA--------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR ORDEN--------------------------------------------------------------------------------- */
        $scope.PrimerPaginaOrden = function () {
            $scope.paginaActualOrden = 1;
            Find();
        };

        $scope.SiguienteOrden = function () {
            if ($scope.paginaActualOrden < $scope.totalPaginasOrden) {
                $scope.paginaActualOrden = $scope.paginaActualOrden + 1;
                Find();
            }
        };

        $scope.AnteriorOrden = function () {
            if ($scope.paginaActualOrden > 1) {
                $scope.paginaActualOrden = $scope.paginaActualOrden - 1;
                Find();
            }
        };

        $scope.UltimaPaginaOrden = function () {
            $scope.paginaActualOrden = $scope.totalPaginasOrden;
            Find();
        };

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN REMESAS--------------------------------------------------------------------------------- */
        $scope.PrimerPaginaRemesa = function () {
            $scope.paginaActualRemesa = 1;
            FindRemesa();
        };

        $scope.SiguienteRemesa = function () {
            if ($scope.paginaActualRemesa < $scope.totalPaginasRemesa) {
                $scope.paginaActualRemesa = $scope.paginaActualRemesa + 1;
                FindRemesa();
            }
        };

        $scope.AnteriorRemesa = function () {
            if ($scope.paginaActualRemesa > 1) {
                $scope.paginaActualRemesa = $scope.paginaActualRemesa - 1;
                FindRemesa();
            }
        };

        $scope.UltimaPaginaRemesa = function () {
            $scope.paginaActualRemesa = $scope.totalPaginasRemesa;
            FindRemesa();
        };
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN FLOTA PROPIA--------------------------------------------------------------------------------- */
        $scope.PrimerPaginaFlotaPropia = function () {
            $scope.paginaActualFlotaPropia = 1;
            FindFlotaPropia();
        };

        $scope.SiguienteFlotaPropia = function () {
            if ($scope.paginaActualFlotaPropia < $scope.totalPaginasFlotaPropia) {
                $scope.paginaActualFlotaPropia = $scope.paginaActualFlotaPropia + 1;
                FindFlotaPropia();
            }
        };

        $scope.AnteriorFlotaPropia = function () {
            if ($scope.paginaActualFlotaPropia > 1) {
                $scope.paginaActualFlotaPropia = $scope.paginaActualFlotaPropia - 1;
                FindFlotaPropia();
            }
        };

        $scope.UltimaPaginaFlotaPropia = function () {
            $scope.paginaActualFlotaPropia = $scope.totalPaginasFlotaPropia;
            FindFlotaPropia();
        };
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN RNDC--------------------------------------------------------------------------------- */
        $scope.PrimerPaginaRNDC = function () {
            $scope.paginaActualRNDC = 1;
            FindRNDC();
        };

        $scope.SiguienteRNDC = function () {
            if ($scope.paginaActualRNDC < $scope.totalPaginasRNDC) {
                $scope.paginaActualRNDC = $scope.paginaActualRNDC + 1;
                FindRNDC();
            }
        };

        $scope.AnteriorRNDC = function () {
            if ($scope.paginaActualRNDC > 1) {
                $scope.paginaActualRNDC = $scope.paginaActualRNDC - 1;
                FindRNDC();
            }
        };

        $scope.UltimaPaginaRNDC = function () {
            $scope.paginaActualRNDC = $scope.totalPaginasRNDC;
            FindRNDC();
        };

        /*-----------------------------------------------------------------COMBOS Y AUTOCOMPLETE--------------------------------------------------------------------------------- */
        ///*Verifica oficinas */
        $scope.AsignarOficina = function (ofic) {
            if (ofic.Codigo === -1) {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo === -1) {
                        item.Estado = true;
                    } else {
                        item.Estado = false;
                    }
                });
            } else {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo === -1) {
                        item.Estado = false;
                    }
                });
            }
        };


    
        $scope.AsignarNivelRiesgo = function (nivel) { 
            if (nivel.Estado ) {
                  $scope.ContNivel++ 
            } else {
               $scope.ContNivel-- 
            }
        };


        /*Verifica rAutocomplete Puesto Control */
        $scope.VerificarAutocompletePuestoControl = function (puestocontrol) {
            if (puestocontrol.Codigo === undefined) {
                $scope.Modelo.PuestoControl = '';
            }
        };

        /*$scope.VerificarAutocomplete = function () {
            alert($scope.Despachos.Conductor.Codigo)
        }*/

        $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1, Estado : false });
        $scope.ListadoOficinas.push({ Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre, Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo, Estado : true });
        /*Cargar el combo  cantidad seguimientos */
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CANTIDAD_LISTA_SEGUIMIENTOS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaCantidadSeguimientos = response.data.Datos;
                    $scope.Despachos.Mostrar = $scope.ListaCantidadSeguimientos[1];
                    $scope.Ordenes.Mostrar = $scope.ListaCantidadSeguimientos[1];
                    $scope.Remesas.Mostrar = $scope.ListaCantidadSeguimientos[1];
                    $scope.FlotaPropia.Mostrar = $scope.ListaCantidadSeguimientos[1];
                    Find();
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.CargarListadoCompletoOficinas = function () {
            if ($scope.ListadoOficinas.length <= 2) {
                OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            response.data.Datos.forEach(function (item) {
                                item.Estado = false
                                if (item.Codigo != $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo) {
                                    $scope.ListadoOficinas.push(item);
                                }
                            });

                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }


        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DUEÑO_VEHICULO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoDueno = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoDueno = response.data.Datos;
                        $scope.Despachos.TipoDueno = $scope.ListadoTipoDueno[0]
                        $scope.Ordenes.TipoDueno = $scope.ListadoTipoDueno[0]
                    }
                    else {
                        $scope.ListadoTipoDueno = []
                    }
                }
            }, function (response) {
            });
        //----Catalogo Niveles de riesgo
    
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NIVELES_RIESGO } }).
            then(function (response) {
                console.log('llegaron',response)
                $scope.ListadoNivelRiesgo.push({ Nombre: '(TODAS)', Codigo: -1 }); 
                if (response.data.ProcesoExitoso === true) { 
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoNivelRiesgo = response.data.Datos; 
                    }
                    else {
                        $scope.ListadoTipoDueno = []
                    }
                }
            }, function (response) {
            });

        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de Conductor*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }
        // Vehiculo
        $scope.ListaPlaca = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 2) == 0 || value.length == 1) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaPlaca = ValidarListadoAutocomplete(Response.Datos, $scope.ListaPlaca);
                }
            }
            return $scope.ListaPlaca;
        };
        $scope.AutocompleteProductos = function (value) {
            $scope.ListaProductoTransportado = [];
            if (value.length > 0) {
                /*Cargar Autocomplete de propietario*/
                blockUIConfig.autoBlock = false;
                var Response = ProductoTransportadosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    ValorAutocomplete: value,
                    Sync: true
                })
                $scope.ListaProductoTransportado = ValidarListadoAutocomplete(Response.Datos, $scope.ListaProductoTransportado)
            }
            return $scope.ListaProductoTransportado
        }
        //-- Productos Transportados --//
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ProductoTranportadoCarga = true;
                    if (response.data.Datos.length > 0) {
                        $scope.ListaProductoTransportado = response.data.Datos;
                    }
                    else {
                        $scope.ListaProductoTransportado = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //Cargar datos vehiculo
        $scope.CargarDatosVehiculo = function (vehiculo) {
            if (vehiculo !== '' && vehiculo !== null && vehiculo !== undefined) {
                $scope.CodigoVehiculo = vehiculo.Codigo;
                VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.CodigoVehiculo }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ModalFlotaPropia = {};
                            $scope.ModalFlotaPropia = response.data.Datos;
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        $scope.ListadoCliente = []
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente)
                }
            }
            return $scope.ListadoCliente
        }
        $scope.ListadoCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }
        /*Cargar el combo Tipo Origen */
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ORIGEN_SEGUIMIENTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaTipoOrigenSeguimiento = [];
                    $scope.ListaTipoOrigenSeguimiento = response.data.Datos;
                    $scope.Despachos.OrigenReporte = $scope.ListaTipoOrigenSeguimiento[0];
                    $scope.Ordenes.OrigenReporte = $scope.ListaTipoOrigenSeguimiento[0];
                    $scope.Remesas.TipoOrigenSeguimiento = $scope.ListaTipoOrigenSeguimiento[0];
                    $scope.FlotaPropia.OrigenReporte = $scope.ListaTipoOrigenSeguimiento[0];
                    //Find();
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo sitio reporte estado*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SITIO_REPORTE_SEGUIMIENTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaSitioReporte = [];
                    $scope.ListaSitioReporteModal = [];
                    $scope.ListaSitioReporte = response.data.Datos;
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo !== 8201) {
                            $scope.ListaSitioReporteModal.push(item);
                        }
                    });
                    $scope.Despachos.SitioReporte = $scope.ListaSitioReporte[0];
                    $scope.Ordenes.SitioReporte = $scope.ListaSitioReporte[0];
                    $scope.Remesas.SitioReporteSeguimiento = $scope.ListaSitioReporte[0];
                    $scope.FlotaPropia.SitioReporte = $scope.ListaSitioReporte[0];
                    //Find();
                    if ($scope.CodigoSitioReporteSeguimiento !== undefined && $scope.CodigoSitioReporteSeguimiento !== '' && $scope.CodigoSitioReporteSeguimiento !== null && $scope.CodigoSitioReporteSeguimiento !== 0) {
                        $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo ==' + $scope.CodigoSitioReporteSeguimiento);
                    } else {
                        $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == 8200');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });



        //Carga autocomplete de rutas
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaRutas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaRutas = response.data.Datos;
                    }
                    else {
                        $scope.ListaRutas = [];
                    }
                }
            }, function (response) {
            });
        //-- Puestos Control --//
        PuestoControlesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaPuestosControl = response.data.Datos;
                    }
                    else {
                        $scope.ListaPuestosControl = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIEMPOS_LOGISTICOS_REMESA }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTiemposLogisticos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTiemposLogisticos = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTiemposLogisticos = []
                    }
                }
            }, function (response) {
            });

        $scope.ListadoSitiosCargueAlterno2 = [];
        $scope.AutocompleteSitiosClienteCargue = function (value, cliente, ciudad) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    $scope.ListadoSitiosCargueAlterno2 = [];
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo;
                    }
                    $scope.ListadoSitiosCargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosCargueAlterno2);
                }
            }
            return $scope.ListadoSitiosCargueAlterno2;
        };
        //-------------------------------------------------FUNCIONES BUSCAR FLOTA PROPIA--------------------------------------------------------
        $scope.BuscarFlotaPropia = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                FindFlotaPropia();
            }
        };

        function FindFlotaPropia() {
            $scope.MensajesError = [];
            $scope.ListadoSeguimientosFlotaPropia = [];

            var CodigosOficinas = '';
            $scope.ListadoOficinas.forEach(function (item) {
                if (item.Estado === true) {
                    CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                }
            });

            if (CodigosOficinas === '-1,') {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo !== -1) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });
            }

            var anuladoflotapropia = 0;
            if ($scope.FlotaPropia.Estado === true) {
                anuladoflotapropia = 1;
            } else {
                anuladoflotapropia = 0;
            }

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Pagina: $scope.paginaActualFlotaPropia,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                TipoConsulta: 7,
                Vehiculo: { Placa: $scope.FlotaPropia.Vehiculo },
                Conductor: { NombreCompleto: $scope.FlotaPropia.Conductor },
                Ruta: $scope.FlotaPropia.Ruta,
                PuestoControl: $scope.FlotaPropia.PuestoControl,
                SitioReporteSeguimiento: $scope.FlotaPropia.SitioReporte,
                TipoOrigenSeguimiento: $scope.FlotaPropia.OrigenReporte,
                Mostrar: $scope.FlotaPropia.Mostrar,
                Anulado: anuladoflotapropia,
                CodigosOficinas: CodigosOficinas,
                FlotaPropia: 1
            };

            BloqueoPantalla.start('Cargando Registros...');
            DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                then(function (response) {
                    console.log(response);
                    BloqueoPantalla.stop();
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Observaciones !== '' && item.Observaciones !== undefined && item.Observaciones !== null) {
                                    var str = item.Observaciones;
                                    item.Observaciones = str.substring(0, 10);
                                    item.ObservacionCompleta = str.substring(0, 250);
                                    item.OcultarVerMas = true;
                                } else {
                                    item.OcultarVerMas = false;
                                }
                            });
                            $scope.ListadoSeguimientosFlotaPropia = response.data.Datos;
                            for (var i = 0; i < $scope.ListadoSeguimientosFlotaPropia.length; i++) {
                                var item = $scope.ListadoSeguimientosFlotaPropia[i]
                                if (new Date(item.FechaUltimoReporte) < MIN_DATE) {
                                    item.FechaUltimoReporte = ''
                                }
                            }
                            $scope.totalRegistrosFlotaPropia = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasFlotaPropia = Math.ceil($scope.totalRegistrosFlotaPropia / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoFlotaPropia = false;
                            $scope.ResultadoSinRegistrosFlotaPropia = '';
                        }
                        else {
                            $scope.totalRegistrosFlotaPropia = 0;
                            $scope.totalPaginasFlotaPropia = 0;
                            $scope.paginaActualFlotaPropia = 1;
                            $scope.ResultadoSinRegistrosFlotaPropia = 'No hay datos para mostrar';
                            $scope.BuscandoFlotaPropia = false;
                        }
                    }
                }, function (response) {
                    BloqueoPantalla.stop();
                    ShowError(response.statusText);
                });
        }

        //-------------------------------------------------FUNCIONES BUSCAR REMESAS RNDC--------------------------------------------------------
        $scope.BuscarRNDC = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                FindRNDC();
            }
        };

        function FindRNDC() {
            $scope.MensajesError = [];
            $scope.ListadoRemesasRNDC = [];

            var CodigosOficinas = '';
            $scope.ListadoOficinas.forEach(function (item) {
                if (item.Estado === true) {
                    CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                }
            });

            if (CodigosOficinas === '-1,') {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo !== -1) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });
            }

            $scope.RNDC.CodigosOficinas = CodigosOficinas;
            $scope.RNDC.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.RNDC.Pagina = $scope.paginaActualRNDC;
            $scope.RNDC.RegistrosPagina = $scope.cantidadRegistrosPorPaginas;
            BloqueoPantalla.start('Cargando Registros...');

            ReporteMinisterioFactory.ConsultarRemesasSinManifiesto($scope.RNDC).
                then(function (response) {
                    BloqueoPantalla.stop();
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoRemesasRNDC = [];
                            response.data.Datos.forEach(function (item) {
                                item.Remesa.Mensaje = item.Mensaje;
                                $scope.ListadoRemesasRNDC.push(item.Remesa);
                            });

                            $scope.totalRegistrosRNDC = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasRNDC = Math.ceil($scope.totalRegistrosRNDC / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoRNDC = false;
                            $scope.ResultadoSinRegistrosRNDC = '';
                        }
                        else {
                            $scope.totalRegistrosRNDC = 0;
                            $scope.totalPaginasRNDC = 0;
                            $scope.paginaActualRNDC = 1;
                            $scope.ResultadoSinRegistrosRNDC = 'No hay datos para mostrar';
                            $scope.BuscandoRNDC = false;
                        }
                    }
                }, function (response) {
                    BloqueoPantalla.stop();
                    ShowError(response.statusText);
                });
        }

        //-------------------------------------------------FUNCIONES BUSCAR REMESAS--------------------------------------------------------
        $scope.BuscarRemesa = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                FindRemesa();
            }
        };

        function FindRemesa() {
            $scope.MensajesError = [];
            $scope.ListadoSeguimientosRemesa = [];

            if ($scope.RemesasEstado === true) {
                $scope.Remesas.Anulado = 1;
            } else {
                $scope.Remesas.Anulado = 0;
            }

            var CodigosOficinas = '';
            $scope.ListadoOficinas.forEach(function (item) {
                if (item.Estado === true) {
                    CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                }
            });

            if (CodigosOficinas === '-1,') {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo !== -1) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });
            }

              var CodigosNivelRiesgo = '';
            $scope.ListadoNivelRiesgo.forEach(function (item) {
                if (item.Estado === true) {
                    CodigosNivelRiesgo = CodigosNivelRiesgo + item.Codigo + ',';
                }
            });

            if (CodigosNivelRiesgo === '-1,') {
                $scope.ListadoNivelRiesgo.forEach(function (item) {
                    if (item.Codigo !== -1) {
                        CodigosNivelRiesgo = CodigosNivelRiesgo + item.Codigo + ',';
                    }
                });
            }

            $scope.Remesas.CodigosNivelRiesgo = CodigosNivelRiesgo;
            $scope.Remesas.CodigosOficinas = CodigosOficinas;

            $scope.Remesas.Pagina = $scope.paginaActualRemesa;
            $scope.Remesas.Consulta = 1;
            BloqueoPantalla.start('Cargando Registros...');

            console.log('buscando', $scope.Remesas)

            DetalleSeguimientoVehiculosFactory.ConsultarMasterRemesa($scope.Remesas).
                then(function (response) {
                    BloqueoPantalla.stop();
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoSeguimientosRemesa = response.data.Datos;
                            for (var i = 0; i < $scope.ListadoSeguimientosRemesa.length; i++) {
                                if (new Date($scope.ListadoSeguimientosRemesa[i].FechaDevolucion) <= new Date('1900-01-01T00:00:00-05:00') || new Date($scope.ListadoSeguimientosRemesa[i].FechaDevolucion) <= new Date(FORMATO_FECHA_MINIMA)) {
                                    $scope.ListadoSeguimientosRemesa[i].FechaDevolucion = '';
                                }
                            }
                            $scope.totalRegistrosRemesa = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasRemesa = Math.ceil($scope.totalRegistrosRemesa / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoRemesa = false;
                            $scope.ResultadoSinRegistrosRemesa = '';
                        }
                        else {
                            $scope.totalRegistrosRemesa = 0;
                            $scope.totalPaginasRemesa = 0;
                            $scope.paginaActualRemesa = 1;
                            $scope.ResultadoSinRegistrosRemesa = 'No hay datos para mostrar';
                            $scope.BuscandoRemesa = false;
                        }
                    }
                }, function (response) {
                    BloqueoPantalla.stop();
                    ShowError(response.statusText);
                });
        }

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        //--Empresa
        EmpresasFactory.Consultar({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.pref = response.data.Datos[0].Prefijo;
            }
        });

        $scope.CargarNovedadesSitioReporte = function (sitioReporte) {
            $scope.ListaNovedadSeguimiento = [];
            $scope.Modelo.NovedadSeguimiento = '';
            if (sitioReporte.Codigo !== undefined && sitioReporte.Codigo !== 8200) {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    SitioReporteSeguimiento: sitioReporte
                };
                DetalleSeguimientoVehiculosFactory.ConsultarNovedadesSitiosReporte(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaNovedadSeguimiento = response.data.Datos;
                                $scope.DeshabilitarNovedadesSeguimiento = false;
                            } else {
                                $scope.DeshabilitarNovedadesSeguimiento = true;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                $scope.DeshabilitarNovedadesSeguimiento = true;
            }
        };

        $scope.CargarPuestoControlRutas = function (ruta) {
            $scope.ListaPuntosControl = [];
            $scope.Modelo.PuestoControl = '';
            if (ruta !== undefined && ruta !== null && ruta !== '') {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Ruta: ruta
                };
                DetalleSeguimientoVehiculosFactory.ConsultarPuestoControlRutas(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaPuntosControl = response.data.Datos;
                                if ($scope.CodigoPuestoControl !== undefined && $scope.CodigoPuestoControl !== '' && $scope.CodigoPuestoControl !== null && $scope.CodigoPuestoControl !== 0) {
                                    if (!$scope.DeshabilitarDetalle) {
                                        $scope.Modelo.PuestoControl = $linq.Enumerable().From($scope.ListaPuntosControl).First('$.PuestoControl.Codigo ==' + $scope.CodigoPuestoControl);
                                    } else {
                                        $scope.Modelo.PuestoControl = '';
                                    }
                                } else {
                                    $scope.Modelo.PuestoControl = '';
                                }
                                $scope.DeshabilitarPuestoControl = false;
                            } else {
                                if (!$scope.DeshabilitarDetalle) {
                                    $scope.DeshabilitarPuestoControl = true;
                                    ShowError('La ruta no tiene puntos de control asociados');
                                }
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                $scope.DeshabilitarPuestoControl = true;
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                Find();
            }
        };

        function Find() {

            $scope.ListadoSeguimientosPlanilla = [];
            $scope.ListadoSeguimientosOrden = [];
            $scope.MensajesError = [];
            if ($scope.MensajesError.length == 0) {
                filtros = {};

                var anuladodespachos = 0;
                if ($scope.Despachos.Estado == true) {
                    anuladodespachos = 1;
                } else {
                    anuladodespachos = 0;
                }
                var anuladoorden = 0;
                if ($scope.Ordenes.Estado == true) {
                    anuladoorden = 1;
                } else {
                    anuladoorden = 0;
                }

                var CodigosOficinas = '';
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Estado === true) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });

                if (CodigosOficinas === '-1,') {
                    $scope.ListadoOficinas.forEach(function (item) {
                        if (item.Codigo !== -1) {
                            CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                        }
                    });
                }


                var CodigosNivelRiesgo = '';
                $scope.ListadoNivelRiesgo.forEach(function (item) {
                    if (item.Estado === true) {
                        CodigosNivelRiesgo = CodigosNivelRiesgo + item.Codigo + ',';
                    }
                });

                if (CodigosNivelRiesgo === '-1,') {
                    $scope.ListadoNivelRiesgo.forEach(function (item) {
                        if (item.Codigo !== -1) {
                            CodigosNivelRiesgo = CodigosNivelRiesgo + item.Codigo + ',';
                        }
                    });
                }

                

                if ($scope.TipoConsulta == 0) {
                       filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Pagina: $scope.paginaActualOrden,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        TipoConsulta: $scope.TipoConsulta,
                        Vehiculo: { Placa: $scope.Ordenes.Vehiculo },
                        Conductor: $scope.Ordenes.Conductor,
                        Cliente: $scope.Ordenes.Cliente,
                        TipoDueno: $scope.Ordenes.TipoDueno,
                        Producto: $scope.Ordenes.Producto,
                        Origen: $scope.Ordenes.Origen,
                        Destino: $scope.Ordenes.Destino,
                        Ruta: $scope.Ordenes.Ruta,
                        PuestoControl: $scope.Ordenes.PuestoControl,
                        NumeroDocumentoOrden: $scope.Ordenes.NumeroPlanilla,
                        NumeroManifiesto: $scope.Ordenes.NumeroManifiesto,
                        SitioReporteSeguimiento: $scope.Ordenes.SitioReporte,
                        TipoOrigenSeguimiento: $scope.Ordenes.OrigenReporte,
                        Mostrar: $scope.Ordenes.Mostrar,
                        Anulado: anuladoorden,
                        CodigosOficinas: CodigosOficinas,
                        CodigosNivelRiesgo : CodigosNivelRiesgo

                    };
                }
                else if ($scope.TipoConsulta == 1) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        TipoConsulta: $scope.TipoConsulta,
                        Vehiculo: { Placa: $scope.Despachos.Vehiculo },
                        Conductor: $scope.Despachos.Conductor,
                        Cliente: $scope.Despachos.Cliente,
                        TipoDueno: $scope.Despachos.TipoDueno,
                        Producto: $scope.Despachos.Producto,
                        Origen: $scope.Despachos.Origen,
                        Destino: $scope.Despachos.Destino,
                        Ruta: $scope.Despachos.Ruta,
                        PuestoControl: $scope.Despachos.PuestoControl,
                        NumeroDocumentoPlanilla: $scope.Despachos.NumeroPlanilla,
                        NumeroManifiesto: $scope.Despachos.NumeroManifiesto,
                        SitioReporteSeguimiento: $scope.Despachos.SitioReporte,
                        TipoOrigenSeguimiento: $scope.Despachos.OrigenReporte,
                        Mostrar: $scope.Despachos.Mostrar,
                        Anulado: anuladodespachos,
                        CodigosOficinas: CodigosOficinas,
                        CodigosNivelRiesgo: CodigosNivelRiesgo,
                        Listado: 1
                    };
                }
            
                BloqueoPantalla.start('Cargando Registros...');
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {
                        BloqueoPantalla.stop();

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoSeguimientosPlanilla = [];
                                $scope.ListadoSeguimientosOrden = [];

                                if ($scope.TipoConsulta == 0) {
                                    var concidencias = 0
                                    if (filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                        concidencias++
                                    }
                                    if (filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                        concidencias++
                                    }
                                    if (filtros.PuestoControl !== undefined) {
                                        if (filtros.PuestoControl.Codigo !== undefined) {
                                            concidencias++
                                        }
                                    }
                                    if (filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                        concidencias++
                                    }
                                    response.data.Datos.forEach(function (item) {
                                        var con = 0
                                        if (filtros.SitioReporteSeguimiento.Codigo == item.SitioReporteSeguimiento.Codigo && filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                            con++
                                        }
                                        if (filtros.TipoOrigenSeguimiento.Codigo == item.TipoOrigenSeguimiento.Codigo && filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                            con++
                                        }
                                        if (filtros.PuestoControl !== undefined) {
                                            if (filtros.PuestoControl.Codigo == item.PuestoControl.Codigo && filtros.PuestoControl.Codigo !== undefined) {
                                                con++
                                            }
                                        }
                                        if (filtros.Vehiculo.Placa == item.Vehiculo.Placa && filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                            con++
                                        }
                                        if (con == concidencias) {
                                            $scope.ListadoSeguimientosOrden.push(item)
                                        }
                                        if (item.Observaciones !== '') {
                                            var str = item.Observaciones;
                                            item.Observaciones = str.substring(0, 10);
                                            item.ObservacionCompleta = str.substring(0, 250);
                                            item.OcultarVerMas = true;
                                        } else {
                                            item.OcultarVerMas = false;
                                        }

                                    })
                                    if ($scope.ListadoSeguimientosOrden.length == 0) {
                                        $scope.totalRegistrosOrden = 0;
                                        $scope.totalPaginasOrden = 0;
                                        $scope.paginaActualOrden = 1;
                                        $scope.ResultadoSinRegistrosOrden = 'No hay datos para mostrar';
                                        $scope.BuscandoOrden = false;

                                    } else {
                                        $scope.totalRegistrosOrden = $scope.ListadoSeguimientosOrden[0].TotalRegistros;
                                        $scope.totalPaginasOrden = Math.ceil($scope.totalRegistrosOrden / $scope.cantidadRegistrosPorPaginas);
                                        $scope.BuscandoOrden = false;
                                        $scope.ResultadoSinRegistrosOrden = '';
                                    }

                                } else if ($scope.TipoConsulta == 1) {
                                    var concidencias = 0
                                    if (filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                        concidencias++
                                    }
                                    if (filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                        concidencias++
                                    }
                                    if (filtros.PuestoControl !== undefined) {
                                        if (filtros.PuestoControl.Codigo !== undefined) {
                                            concidencias++
                                        }
                                    }
                                    if (filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                        concidencias++
                                    }
                                    response.data.Datos.forEach(function (item) {
                                        var con = 0
                                        if (filtros.SitioReporteSeguimiento.Codigo == item.SitioReporteSeguimiento.Codigo && filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                            con++
                                        }
                                        if (filtros.TipoOrigenSeguimiento.Codigo == item.TipoOrigenSeguimiento.Codigo && filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                            con++
                                        }
                                        if (filtros.PuestoControl !== undefined) {
                                            if (filtros.PuestoControl.Codigo == item.PuestoControl.Codigo && filtros.PuestoControl.Codigo !== undefined) {
                                                con++
                                            }
                                        }
                                        if (filtros.Vehiculo.Placa == item.Vehiculo.Placa && filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                            con++
                                        }
                                        if (con == concidencias) {
                                            if (new Date(item.FechaUltimoReporte) < MIN_DATE) {
                                                item.FechaUltimoReporte = ''
                                            }
                                            $scope.ListadoSeguimientosPlanilla.push(item)
                                        }
                                        if (item.Observaciones !== '') {
                                            var str = item.Observaciones;
                                            item.Observaciones = str.substring(0, 10);
                                            item.ObservacionCompleta = str.substring(0, 250);
                                            item.OcultarVerMas = true;
                                        } else {
                                            item.OcultarVerMas = false;
                                        }

                                    })
                                    if ($scope.ListadoSeguimientosPlanilla.length == 0) {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;

                                    } else {
                                        $scope.totalRegistros = $scope.ListadoSeguimientosPlanilla[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                    }
                                }
                            } else {
                                if ($scope.TipoConsulta == 1) {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                } else {
                                    $scope.totalRegistrosOrden = 0;
                                    $scope.totalPaginasOrden = 0;
                                    $scope.paginaActualOrden = 1;
                                    $scope.ResultadoSinRegistrosOrden = 'No hay datos para mostrar';
                                    $scope.BuscandoOrden = false;
                                }
                            }
                        }
                    }, function (response) {
                        BloqueoPantalla.stop();
                        ShowError(response.statusText);
                    });
            }
        }

        /*---------------------------------------------------------------------Funcion Anular Seguimiento----------------------------------------------------------------------------*/
        /*Validacion boton anular */
        $scope.ConfirmacionAnularSeguimiento = function (item) {
            if (item.NumeroDocumentoRemesa !== null && item.NumeroDocumentoRemesa !== undefined && item.NumeroDocumentoRemesa !== '' && item.NumeroDocumentoRemesa !== 0) {
                $scope.ValorBanderaAnularRemesa = item.NumeroPlanilla;
            }
            if ($scope.ValidarPermisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
                $scope.CodigoSeguimiento = item.Codigo;
                if (item.Anulado !== 1) {
                    showModal('modalConfirmacionAnularSeguimiento');
                }
                else {
                    closeModal('modalConfirmacionAnularSeguimiento');
                }
            }
        };

        /*Funcion que solicita los datos de la anulación */
        $scope.SolicitarDatosAnulacion = function () {
            $scope.FechaAnulacion = new Date();
            closeModal('modalConfirmacionAnularSeguimiento');
            showModal('modalDatosAnularSeguimiento');
        };

        $scope.Anular = function () {
            if (DatosRequeridosAnular()) {
                var entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.CodigoSeguimiento,
                    NumeroDocumentoRemesa: $scope.ValorBanderaAnularRemesa,
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnulacion: $scope.CausaAnulacion
                };

                DetalleSeguimientoVehiculosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anuló el seguimiento');
                            closeModal('modalDatosAnularSeguimiento');
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridosAnular() {
            $scope.MensajesErrorAnular = [];
            var continuar = true;
            if ($scope.CausaAnulacion == undefined || $scope.CausaAnulacion == '' || $scope.CausaAnulacion == null) {
                $scope.MensajesErrorAnular.push('Debe ingresar la causa anulación');
                continuar = false;
            }
            return continuar
        }

        /*---------------------------------------------------------------------Funcion Anular Seguimiento flota propia----------------------------------------------------------------------------*/
        /*Validacion boton anular */
        $scope.ConfirmacionAnularSeguimientoFlotaPropia = function (item) {
            if ($scope.ValidarPermisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
                $scope.CodigoSeguimiento = item.Codigo;
                if (item.Anulado !== 1) {
                    showModal('modalConfirmacionAnularSeguimientoflotapropia');
                } else {
                    closeModal('modalConfirmacionAnularSeguimientoflotapropia');
                }
            }
        };

        /*Funcion que solicita los datos de la anulación */
        $scope.SolicitarDatosAnulacionFlotaPropia = function () {
            $scope.FechaAnulacion = new Date();
            closeModal('modalConfirmacionAnularSeguimientoflotapropia');
            showModal('modalDatosAnularSeguimientoflotapropia');
        };

        $scope.AnularFlotaPropia = function () {
            if (DatosRequeridosAnular()) {
                var entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.CodigoSeguimiento,
                    FlotaPropia: 1,
                    NumeroDocumentoRemesa: $scope.ValorBanderaAnularRemesa,
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnulacion: $scope.CausaAnulacion
                };

                DetalleSeguimientoVehiculosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anuló el seguimiento');
                            closeModal('modalDatosAnularSeguimientoflotapropia');
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        //--------------funciones de mascaras html-angular------------------------------------------------------------------------------------------------------------------
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskPlaca = function () {

            MascaraPlacaGeneral($scope)
        };
        //--------------Imprimir informe--------------------------------------------------------------------------------------------------------------------
        //Función que ejecuta el boton de vista preliminar
        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL, OpcionListado) {

            $scope.OpcionLista = OpcionListado;

            //Depende del listado seleccionado se enviará el nombre por parametro
            if ($scope.OpcionLista === CODIGO_LISTADO_SEGUIMIENTO_DESPACHOS) {
                $scope.NombreReporte = NOMBRE_LISTADO_SEGUIMIENTO_DESPACHOS;
            }
            if ($scope.OpcionLista === CODIGO_LISTADO_SEGUIMIENTO_ORDENES_CARGUE) {
                $scope.NombreReporte = NOMBRE_LISTADO_SEGUIMIENTO_ORDENES_CARGUE;
            }
            if ($scope.OpcionLista === CODIGO_LISTADO_SEGUIMIENTO_FLOTA_PROPIA) {
                $scope.NombreReporte = NOMBRE_LISTADO_SEGUIMIENTO_FLOTA_PROPIA;
            }

            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        // funcion enviar parametros al proyecto ASP armando el filtro
        $scope.ArmarFiltro = function () {

            // Filtros Despachos----------------------------------------------------------------------------------------------
            if ($scope.OpcionLista === CODIGO_LISTADO_SEGUIMIENTO_DESPACHOS) {
                var anuladodespachos = 0;
                if ($scope.Despachos.Estado == true) {
                    anuladodespachos = 1;
                } else {
                    anuladodespachos = 0;
                }
                var CodigosOficinas = '';
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Estado === true) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });

                if (CodigosOficinas === '-1,') {
                    $scope.ListadoOficinas.forEach(function (item) {
                        if (item.Codigo !== -1) {
                            CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                        }
                    });
                }
                if ($scope.Despachos.Vehiculo !== undefined && $scope.Despachos.Vehiculo !== null && $scope.Despachos.Vehiculo !== '') {
                    $scope.FiltroArmado += '&Vehiculo=' + $scope.Despachos.Vehiculo;

                }
                if ($scope.Despachos.Conductor !== undefined && $scope.Despachos.Conductor !== '' && $scope.Despachos.Conductor !== null) {
                    $scope.FiltroArmado += '&Conductor=' + $scope.Despachos.Conductor.Codigo;
                }
                if ($scope.Despachos.Cliente !== undefined && $scope.Despachos.Cliente !== '' && $scope.Despachos.Cliente !== null) {
                    $scope.FiltroArmado += '&Cliente=' + $scope.Despachos.Cliente.Codigo;
                }
                if ($scope.Despachos.TipoDueno !== undefined && $scope.Despachos.TipoDueno !== '' && $scope.Despachos.TipoDueno !== null) {
                    if ($scope.Despachos.TipoDueno.Codigo != 2100) {
                        $scope.FiltroArmado += '&TipoDueno=' + $scope.Despachos.TipoDueno.Codigo;
                    }
                }
                if ($scope.Despachos.Producto !== undefined && $scope.Despachos.Producto !== '' && $scope.Despachos.Producto !== null) {
                    $scope.FiltroArmado += '&Producto=' + $scope.Despachos.Producto.Codigo;
                }
                if ($scope.Despachos.Origen !== undefined && $scope.Despachos.Origen !== '' && $scope.Despachos.Origen !== null) {
                    $scope.FiltroArmado += '&Origen=' + $scope.Despachos.Origen.Codigo;
                }
                if ($scope.Despachos.Destino !== undefined && $scope.Despachos.Destino !== '' && $scope.Despachos.Destino !== null) {
                    $scope.FiltroArmado += '&Destino=' + $scope.Despachos.Destino.Codigo;
                }
                if ($scope.Despachos.Ruta !== undefined && $scope.Despachos.Ruta !== '' && $scope.Despachos.Ruta !== null) {
                    $scope.FiltroArmado += '&Ruta=' + $scope.Despachos.Ruta.Codigo;
                }
                if ($scope.Despachos.PuestoControl !== undefined && $scope.Despachos.PuestoControl !== null && $scope.Despachos.PuestoControl !== '') {
                    $scope.FiltroArmado += '&PuestoControl=' + $scope.Despachos.PuestoControl.Codigo;
                }
                if ($scope.Despachos.NumeroPlanilla !== undefined && $scope.Despachos.NumeroPlanilla !== '' && $scope.Despachos.NumeroPlanilla !== null && $scope.Despachos.NumeroPlanilla > 0) {
                    $scope.FiltroArmado += '&NumeroPlanilla=' + $scope.Despachos.NumeroPlanilla;
                }
                if ($scope.Despachos.NumeroManifiesto !== undefined && $scope.Despachos.NumeroManifiesto !== '' && $scope.Despachos.NumeroManifiesto !== null && $scope.Despachos.NumeroManifiesto > 0) {
                    $scope.FiltroArmado += '&NumeroManifiesto=' + $scope.Despachos.NumeroManifiesto;
                }
                if ($scope.Despachos.OrigenReporte !== undefined && $scope.Despachos.OrigenReporte !== null && $scope.Despachos.OrigenReporte !== '' && $scope.Despachos.OrigenReporte.Codigo !== 8000) {
                    $scope.FiltroArmado += '&OrigenReporte=' + $scope.Despachos.OrigenReporte.Codigo;
                }
                if ($scope.Despachos.SitioReporte !== undefined && $scope.Despachos.SitioReporte !== null && $scope.Despachos.SitioReporte !== '' && $scope.Despachos.SitioReporte.Codigo !== 8200) {
                    $scope.FiltroArmado += '&SitioReporte=' + $scope.Despachos.SitioReporte.Codigo;
                }
                if ($scope.Despachos.Mostrar.Codigo !== undefined && $scope.Despachos.Mostrar.Codigo !== null && $scope.Despachos.Mostrar.Codigo !== '' && $scope.Despachos.Mostrar.Codigo !== 11300 && $scope.Despachos.Estado === false) {
                    $scope.FiltroArmado += '&Mostrar=' + $scope.Despachos.Mostrar.CampoAuxiliar2;
                }
                $scope.FiltroArmado += '&CodigosOficinas=' + CodigosOficinas;
                $scope.FiltroArmado += '&Anulado=' + anuladodespachos;
                //Filtro Ordenes Cargue---------------------------------------------------------------------------------------------
            }
            else if ($scope.OpcionLista === CODIGO_LISTADO_SEGUIMIENTO_ORDENES_CARGUE) {
                var anuladodespachos = 0;
                if ($scope.Ordenes.Estado == true) {
                    anuladodespachos = 1;
                } else {
                    anuladodespachos = 0;
                }
                var CodigosOficinas = '';
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Estado === true) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });

                if (CodigosOficinas === '-1,') {
                    $scope.ListadoOficinas.forEach(function (item) {
                        if (item.Codigo !== -1) {
                            CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                        }
                    });
                }
                if ($scope.Ordenes.Vehiculo !== undefined && $scope.Ordenes.Vehiculo !== null && $scope.Ordenes.Vehiculo !== '') {
                    $scope.FiltroArmado += '&Vehiculo=' + $scope.Ordenes.Vehiculo;

                }
                if ($scope.Ordenes.Conductor !== undefined && $scope.Ordenes.Conductor !== '' && $scope.Ordenes.Conductor !== null) {
                    $scope.FiltroArmado += '&Conductor=' + $scope.Ordenes.Conductor.Codigo;
                }
                if ($scope.Ordenes.Cliente !== undefined && $scope.Ordenes.Cliente !== '' && $scope.Ordenes.Cliente !== null) {
                    $scope.FiltroArmado += '&Cliente=' + $scope.Ordenes.Cliente.Codigo;
                }
                if ($scope.Ordenes.TipoDueno !== undefined && $scope.Ordenes.TipoDueno !== '' && $scope.Ordenes.TipoDueno !== null) {
                    if ($scope.Ordenes.TipoDueno.Codigo != 2100) {
                        $scope.FiltroArmado += '&TipoDueno=' + $scope.Ordenes.TipoDueno.Codigo;
                    }
                }
                if ($scope.Ordenes.Producto !== undefined && $scope.Ordenes.Producto !== '' && $scope.Ordenes.Producto !== null) {
                    $scope.FiltroArmado += '&Producto=' + $scope.Ordenes.Producto.Codigo;
                }
                if ($scope.Ordenes.Origen !== undefined && $scope.Ordenes.Origen !== '' && $scope.Ordenes.Origen !== null) {
                    $scope.FiltroArmado += '&Origen=' + $scope.Ordenes.Origen.Codigo;
                }
                if ($scope.Ordenes.Destino !== undefined && $scope.Ordenes.Destino !== '' && $scope.Ordenes.Destino !== null) {
                    $scope.FiltroArmado += '&Destino=' + $scope.Ordenes.Destino.Codigo;
                }
                if ($scope.Ordenes.Ruta !== undefined && $scope.Ordenes.Ruta !== '' && $scope.Ordenes.Ruta !== null) {
                    $scope.FiltroArmado += '&Ruta=' + $scope.Ordenes.Ruta.Codigo;
                }
                if ($scope.Ordenes.PuestoControl !== undefined && $scope.Ordenes.PuestoControl !== null && $scope.Ordenes.PuestoControl !== '') {
                    $scope.FiltroArmado += '&PuestoControl=' + $scope.Ordenes.PuestoControl.Codigo;
                }
                if ($scope.Ordenes.NumeroPlanilla !== undefined && $scope.Ordenes.NumeroPlanilla !== '' && $scope.Ordenes.NumeroPlanilla !== null && $scope.Ordenes.NumeroPlanilla > 0) {
                    $scope.FiltroArmado += '&NumeroPlanilla=' + $scope.Ordenes.NumeroPlanilla;
                }
                if ($scope.Ordenes.NumeroManifiesto !== undefined && $scope.Ordenes.NumeroManifiesto !== '' && $scope.Ordenes.NumeroManifiesto !== null && $scope.Ordenes.NumeroManifiesto > 0) {
                    $scope.FiltroArmado += '&NumeroManifiesto=' + $scope.Ordenes.NumeroManifiesto;
                }
                //if ($scope.Ordenes.OrigenReporte !== undefined && $scope.Ordenes.OrigenReporte !== null && $scope.Ordenes.OrigenReporte !== '' && $scope.Ordenes.OrigenReporte.Codigo !== 8000) {
                //    $scope.FiltroArmado += '&OrigenReporte=' + $scope.Ordenes.OrigenReporte.Codigo;
                //}
                if ($scope.Ordenes.SitioReporte !== undefined && $scope.Ordenes.SitioReporte !== null && $scope.Ordenes.SitioReporte !== '' && $scope.Ordenes.SitioReporte.Codigo !== 8200) {
                    $scope.FiltroArmado += '&SitioReporte=' + $scope.Ordenes.SitioReporte.Codigo;
                }
                if ($scope.Ordenes.Mostrar.Codigo !== undefined && $scope.Ordenes.Mostrar.Codigo !== null && $scope.Ordenes.Mostrar.Codigo !== '' && $scope.Ordenes.Mostrar.Codigo !== 11300 && $scope.Despachos.Estado === false) {
                    $scope.FiltroArmado += '&Mostrar=' + $scope.Ordenes.Mostrar.CampoAuxiliar2;
                }
                $scope.FiltroArmado += '&CodigosOficinas=' + CodigosOficinas;
                $scope.FiltroArmado += '&Anulado=' + anuladodespachos;

            }
            else if ($scope.OpcionLista === CODIGO_LISTADO_SEGUIMIENTO_FLOTA_PROPIA) {
                var anuladodespachos = 0;
                if ($scope.FlotaPropia.Estado == true) {
                    anuladodespachos = 1;
                } else {
                    anuladodespachos = 0;
                }
                var CodigosOficinas = '';
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Estado === true) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });

                if (CodigosOficinas === '-1,') {
                    $scope.ListadoOficinas.forEach(function (item) {
                        if (item.Codigo !== -1) {
                            CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                        }
                    });
                }
                if ($scope.FlotaPropia.Vehiculo !== undefined && $scope.FlotaPropia.Vehiculo !== null && $scope.FlotaPropia.Vehiculo !== '') {
                    $scope.FiltroArmado += '&Vehiculo=' + $scope.FlotaPropia.Vehiculo;
                }
                if ($scope.FlotaPropia.Conductor !== undefined && $scope.FlotaPropia.Conductor !== '' && $scope.FlotaPropia.Conductor !== null) {


                    $scope.FiltroArmado += '&Conductor=' + $scope.FlotaPropia.Conductor;
                }
                if ($scope.FlotaPropia.PuestoControl !== undefined && $scope.FlotaPropia.PuestoControl !== null && $scope.FlotaPropia.PuestoControl !== '') {
                    $scope.FiltroArmado += '&PuestoControl=' + $scope.FlotaPropia.PuestoControl.Codigo;
                }

                if ($scope.FlotaPropia.OrigenReporte !== undefined && $scope.FlotaPropia.OrigenReporte !== null && $scope.FlotaPropia.OrigenReporte !== '' && $scope.FlotaPropia.OrigenReporte.Codigo !== 8000) {
                    alert($scope.FlotaPropia.OrigenReporte);
                    console.log($scope.FlotaPropia.OrigenReporte);
                    $scope.FiltroArmado += '&OrigenReporte=' + $scope.FlotaPropia.OrigenReporte.Codigo;
                }
                if ($scope.FlotaPropia.SitioReporte !== undefined && $scope.FlotaPropia.SitioReporte !== null && $scope.FlotaPropia.SitioReporte !== '' && $scope.FlotaPropia.SitioReporte.Codigo !== 8200) {
                    $scope.FiltroArmado += '&SitioReporte=' + $scope.FlotaPropia.SitioReporte.Codigo;
                }
                if ($scope.FlotaPropia.Mostrar.Codigo !== undefined && $scope.FlotaPropia.Mostrar.Codigo !== null && $scope.FlotaPropia.Mostrar.Codigo !== '' && $scope.FlotaPropia.Mostrar.Codigo !== 11300 && $scope.Despachos.Estado === false) {
                    $scope.FiltroArmado += '&Mostrar=' + $scope.FlotaPropia.Mostrar.CampoAuxiliar2;
                }
                $scope.FiltroArmado += '&CodigosOficinas=' + CodigosOficinas;
                $scope.FiltroArmado += '&Anulado=' + anuladodespachos;
            }
        };

        //inicializacion de divisiones
        $scope.MostrarConsultaPlanillasDespachos = function () {
            $('#ConsultaOrdenesCargue').hide();
            $('#ConsultaPlanillasDespachos').show();
            $('#ConsultaRemesasPlanillasDespachos').hide();
            $('#ConsultaFlotaPropia').hide();
            $('#ConsultaRNDC').hide();
            $scope.TipoConsulta = 1; // Planillas despachos
            Find();
        };
        $scope.MostrarConsultaOrdenesCargue = function () {
            $('#ConsultaOrdenesCargue').show();
            $('#ConsultaPlanillasDespachos').hide();
            $('#ConsultaRemesasPlanillasDespachos').hide();
            $('#ConsultaFlotaPropia').hide();
            $('#ConsultaRNDC').hide();
            $scope.TipoConsulta = 0; // Ordenes de cargue
            Find();
        };
        $scope.MostrarConsultaRemesasPlanillasDespachos = function () {
            $('#ConsultaOrdenesCargue').hide();
            $('#ConsultaPlanillasDespachos').hide();
            $('#ConsultaRemesasPlanillasDespachos').show();
            $('#ConsultaFlotaPropia').hide();
            $('#ConsultaRNDC').hide();
           // FindRemesa();
        };
        $scope.MostrarConsultaFlotaPropia = function () {
            $('#ConsultaOrdenesCargue').hide();
            $('#ConsultaPlanillasDespachos').hide();
            $('#ConsultaRemesasPlanillasDespachos').hide();
            $('#ConsultaFlotaPropia').show();
            $('#ConsultaRNDC').hide();
        };
        $scope.MostrarConsultaRNDC = function () {
            $('#ConsultaOrdenesCargue').hide();
            $('#ConsultaPlanillasDespachos').hide();
            $('#ConsultaRemesasPlanillasDespachos').hide();
            $('#ConsultaFlotaPropia').hide();
            $('#ConsultaRNDC').show();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/



        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.NuevoSeguimientoFlotaPropia = function (item) {
            $scope.CodigoVehiculo = '';
            $scope.VariableDetalleFlotaPropia = 'NUEVO';
            $scope.DeshabilitarDetalle = false;
            $scope.MensajesErrorSeguimiento = [];
            $scope.ListaNovedadSeguimiento = [];
            $scope.ListadoDetalleSeguimientosPlanilla = []
            $scope.CodigoNovedadSeguimiento = 0;
            showModal('modalnuevoseguimientoflotapropia');
            $scope.DeshabilitarNovedadesSeguimiento = true;
            $scope.ModalFlotaPropia = {};
            $scope.iniciarMapaFlotaPropia();
            if (item !== undefined) {
                $scope.Modelo.Vehiculo = item.Vehiculo
                $scope.CodigoVehiculo = item.Vehiculo.Codigo;
                $scope.CargarDatosVehiculo(item.Vehiculo)
                var Fecha_inicio = new Date(new Date().setSeconds(-432000))
                Fecha_inicio.setHours(0)
                Fecha_inicio.setMinutes(0)
                Fecha_inicio.setMinutes(0)
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    FlotaPropia: 1,
                    Vehiculo: item.Vehiculo,
                    FechaInicial: Fecha_inicio,
                    Anulado: 0
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                //DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, FlotaPropia: 1, Codigo: $scope.CodigoVehiculo }).
                //    then(function (response) {
                //        if (response.data.ProcesoExitoso === true) {
                //            if (response.data.Datos.Codigo > 0) {
                //                $scope.Modelo.Vehiculo = response.data.Datos.Vehiculo;
                //                $scope.Modelo.Ruta = response.data.Datos.Ruta;
                //                $scope.CargarDatosVehiculo(response.data.Datos.Vehiculo);
                //                $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                //                $scope.DeshabilitarFlotaPropia = true;
                //            }
                //        }
                //    }, function (response) {
                //        ShowError(response.statusText);
                //    });
            } else {
                $scope.Modelo.Vehiculo = '';
                $scope.Modelo.Ruta = '';
                $scope.DeshabilitarFlotaPropia = false;
            }
            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
            $scope.Modelo.TipoOrigenSeguimiento = { Codigo: CODIGO_TIPO_ORIGEN_SEGUIMIENTO_MANUAL };
            $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == 8200');
            $scope.Modelo.NovedadSeguimiento = '';
            $scope.Modelo.PuestoControl = '';
            $scope.Modelo.Ubicacion = '';
            $scope.Modelo.Observaciones = '';
            $scope.Modelo.Latitud = '';
            $scope.Modelo.Longitud = '';
            $scope.Reportar = false;
            $scope.Prioritario = false;
            $scope.Modelo.ReplicarRemesas = false;
        };

        $scope.GuardarSeguimientoFlotaPropia = function () {
            if (DatosRequeridosFlotaPropia()) {
                BloqueoPantalla.start('Guardando Seguimiento, por favor espere...');
                if ($scope.Reportar === true) {
                    $scope.Modelo.ReportarCliente = 1;
                } else {
                    $scope.Modelo.ReportarCliente = 0;
                }
                if ($scope.Prioritario === true) {
                    $scope.Modelo.Prioritario = 1;
                }
                else {
                    $scope.Modelo.Prioritario = 0;
                }
                $scope.Modelo.Latitud = parseFloat($scope.Modelo.Latitud);
                $scope.Modelo.Longitud = parseFloat($scope.Modelo.Longitud);
                $scope.Modelo.FlotaPropia = 1;
                if ($scope.Modelo.PuestoControl.PuestoControl !== undefined) {
                    $scope.Modelo.PuestoControl = { Codigo: $scope.Modelo.PuestoControl.PuestoControl.Codigo };
                }
                $scope.Modelo.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas;

                DetalleSeguimientoVehiculosFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.Datos > 0) {
                            closeModal('modalnuevoseguimientoflotapropia');
                            FindFlotaPropia();
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                BloqueoPantalla.stop();
            }
        };

        function DatosRequeridosFlotaPropia() {
            $scope.MensajesErrorSeguimiento = [];
            var continuar = true;
            if ($scope.Modelo.SitioReporteSeguimiento === undefined || $scope.Modelo.SitioReporteSeguimiento === "" || $scope.Modelo.SitioReporteSeguimiento === null || $scope.Modelo.SitioReporteSeguimiento.Codigo === 8200) {
                $scope.MensajesErrorSeguimiento.push('Debe ingresar el sitio del reporte');
                continuar = false;
            }
            if ($scope.Modelo.Vehiculo === undefined || $scope.Modelo.Vehiculo === "" || $scope.Modelo.Vehiculo === null) {
                $scope.MensajesErrorSeguimiento.push('Debe ingresar la placa del vehiculo');
                continuar = false;
            }
            return continuar;
        }

        $scope.VerSeguimientoFlotaPropia = function (item) {
            $scope.VariableDetalleFlotaPropia = 'DETALLE';
            $scope.DeshabilitarDetalle = true;
            $scope.MensajesErrorSeguimiento = [];
            $scope.CodigoNovedadSeguimiento = 0;
            showModal('modalnuevoseguimientoflotapropia');
            $scope.ModalFlotaPropia = {};
            DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, FlotaPropia: 1, Codigo: item.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            $scope.Modelo.Vehiculo = response.data.Datos.Vehiculo;
                            $scope.Modelo.Ruta = response.data.Datos.Ruta;
                            $scope.CargarDatosVehiculo(response.data.Datos.Vehiculo);
                            $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            $scope.ListadoDetalleSeguimientosPlanilla = []
            var Fecha_inicio = new Date(new Date().setSeconds(-432000))
            Fecha_inicio.setHours(0)
            Fecha_inicio.setMinutes(0)
            Fecha_inicio.setMinutes(0)
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                FlotaPropia: 1,
                Vehiculo: item.Vehiculo,
                FechaInicial: Fecha_inicio,
                Anulado: 0
            }
            DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.Modelo.TipoOrigenSeguimiento = { Codigo: CODIGO_TIPO_ORIGEN_SEGUIMIENTO_MANUAL };
            $scope.CodigoSitioReporteSeguimiento = item.SitioReporteSeguimiento.Codigo;
            if ($scope.ListaSitioReporteModal.length > 0 && $scope.CodigoSitioReporteSeguimiento > 0) {
                $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == ' + item.SitioReporteSeguimiento.Codigo);
            }
            $scope.CodigoNovedadSeguimiento = item.NovedadSeguimiento.Codigo;
            if ($scope.ListaNovedadSeguimiento.length > 0 && $scope.CodigoNovedadSeguimiento > 0) {
                $scope.Modelo.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo == ' + item.NovedadSeguimiento.Codigo);
            }
            $scope.CodigoPuestoControl = item.PuestoControl.Codigo;
            if ($scope.ListaPuntosControl.length > 0 && $scope.CodigoPuestoControl > 0) {
                $scope.Modelo.PuestoControl = $linq.Enumerable().From($scope.ListaPuntosControl).First('$.PuestoControl.Codigo == ' + item.PuestoControl.Codigo);
            }
            $scope.Modelo.NovedadSeguimiento = item.NovedadSeguimiento;
            $scope.Modelo.Ubicacion = item.Ubicacion;
            $scope.Modelo.Observaciones = item.ObservacionCompleta;
            $scope.Modelo.Latitud = item.Latitud;
            $scope.Modelo.Longitud = item.Longitud;
            if (item.ReportarCliente === 1) {
                $scope.Reportar = true;
            } else {
                $scope.Reportar = false;
            }
            if (item.Prioritario === 1) {
                $scope.Prioritario = true;
            } else {
                $scope.Prioritario = false;
            }
            $scope.iniciarMapaFlotaPropia(item.Latitud, item.Longitud);

        };

        $scope.EnviarCorreo = function (item) {
            item.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            item.NombreCortoEmpresa = $scope.pref;
            console.log(item)
            DetalleSeguimientoVehiculosFactory.EnviarCorreoCliente(item).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            $scope.ModalOrdenes = response.data.Datos;
                            $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        //Enviar Email Seguimiento Vehicular
        $scope.EnviarEmailSeguimientoVehicular = function (item) {
            item.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            DetalleSeguimientoVehiculosFactory.EnviarCorreoSeguimiento(item).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            $scope.ModalOrdenes = response.data.Datos;
                            $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.EnviarDocumentosConductor = function (item) {
            BloqueoPantalla.start('Enviando Documentos')
            item.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            DetalleSeguimientoVehiculosFactory.EnviarDocumentosConductor(item).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Los documentos se enviarón correctamente')
                        BloqueoPantalla.stop()
                    }
                    else {
                        ShowError('No se logro enviar el correo, verifique que el conductor tenga parametrizado correctamente un correo electronico, o verifique la configuración del servidor de correos')
                        BloqueoPantalla.stop()
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    BloqueoPantalla.stop()
                });
        }
        $scope.EnviarCorreoAvanceVehiculosCliente = function () {
            if ($scope.Cliente == undefined || $scope.Cliente == '') {
                ShowError('Por Favor ingrese el cliente')
            }
            else {
                var item = {}
                item.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                item.CodigoCliente = $scope.Cliente.Codigo
                DetalleSeguimientoVehiculosFactory.EnviarCorreoAvanceVehiculosCliente(item).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            closeModal('ModalCliente')
                            ShowSuccess('El informe Se envio Correctamente')
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        /*----------------------------------------------------------------------------------NUEVO SEGUIMIENTO DESPACHOS ORDEN DE CARGUE----------------------------------------------------------------------------------------*/
        $scope.NuevoSeguimiento = function (item) {
            $scope.VariableDetalle = 'NUEVO';
            $scope.DeshabilitarDetalle = false;
            $scope.MensajesErrorSeguimiento = [];
            $scope.ListaNovedadSeguimiento = [];
            $scope.ListadoDetalleSeguimientosPlanilla = []
            $scope.CodigoNovedadSeguimiento = 0;
            if (item !== undefined) {
                $scope.Modelo.Vehiculo = item.Vehiculo;
                $scope.Modelo.Ruta = item.Ruta;
                $scope.Modelo.Oficina = item.Oficina;
            }
            if (item.TipoConsulta === 0) {
                showModal('modalnuevoseguimientoorden');
                $scope.DeshabilitarNovedadesSeguimiento = true;
                $scope.ModalOrdenes = {};
                $scope.iniciarMapaOrden();
                DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Codigo, TipoConsulta: item.TipoConsulta }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ModalOrdenes = response.data.Datos;
                                $scope.Modelo.Ruta = response.data.Datos.Ruta;
                                $scope.Modelo.Vehiculo = response.data.Datos.Vehiculo;
                                $scope.Modelo.Oficina = response.data.Datos.Oficina;
                                $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoConsulta: 0,
                    NumeroDocumentoOrden: item.NumeroDocumentoOrden,
                    Anulado: 0
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else if (item.TipoConsulta === 1) {
                $scope.iniciamapa = true
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoConsulta: 1,
                    NumeroDocumentoPlanilla: item.NumeroDocumentoPlanilla,
                    Anulado: 0,
                    ConsultaGPS: 1
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {
                        BloqueoPantalla.stop();
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanillaGPS = response.data.Datos
                                var marker = [];
                                var map, infoWindow;
                                function initMap(posicion) {
                                    if (map === void 0) {
                                        var mapOptions = {
                                            zoom: 15,
                                            center: new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud),
                                            disableDefaultUI: true,
                                            mapTypeId: window.google.maps.MapTypeId.ROADMAP
                                        }
                                        //Asignacion al id gmapsplanilla que se encuentra en el html
                                        map = new window.google.maps.Map(document.getElementById('gmapsplanillaGPS'), mapOptions);
                                    }
                                }

                                function setMarker(map, position, title, content) {
                                    var markers;
                                    var markerOptions = {
                                        position: position,
                                        map: map,
                                        //draggable opcion que permite mover el marcador
                                        draggable: false,
                                        //bounce le da animacion al marcador
                                        //animation: google.maps.Animation.BOUNCE,
                                        title: title,
                                    };

                                    markers = new window.google.maps.Marker(markerOptions);
                                    //dragend permite capturar las coordenadas seleccionadas en el mapa
                                    markers.addListener('dragend', function (event) {
                                        //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                                        $scope.Modelo.Latitud = this.getPosition().lat();
                                        $scope.Modelo.Longitud = this.getPosition().lng();
                                    });
                                    //Variable donde se asignan los datos del marcador
                                    marker.push(markers);

                                    //var infoWindowOptions = {
                                    //    content: content
                                    //};
                                    //infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                                    //infoWindow.open(map, markers);

                                }
                                for (var i = 0; i < $scope.ListadoDetalleSeguimientosPlanillaGPS.length; i++) {
                                    var posicion = $scope.ListadoDetalleSeguimientosPlanillaGPS[i]
                                    if (posicion.Longitud !== 0 && posicion.Latitud !== 0) {
                                        if ($scope.iniciamapa) {
                                            initMap(posicion)
                                            $scope.iniciamapa = false
                                        }
                                        setMarker(map, new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud), '', '');

                                    }
                                }
                                for (var i = 0; i < $scope.ListadoDetalleSeguimientosPlanilla.length; i++) {
                                    var posicion = $scope.ListadoDetalleSeguimientosPlanilla[i]
                                    if (posicion.Longitud !== 0 && posicion.Latitud !== 0) {
                                        if ($scope.iniciamapa) {
                                            initMap(posicion)
                                            $scope.iniciamapa = false
                                        }
                                        setMarker(map, new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud), '', '');
                                    }
                                }
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        BloqueoPantalla.stop();
                    });
                showModal('modalnuevoseguimientoplanilla');
                $scope.DeshabilitarNovedadesSeguimiento = true;
                $scope.ModalDespachos = {};
                //$scope.iniciarMapaPlanilla();
                DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Codigo, TipoConsulta: item.TipoConsulta }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ModalDespachos = response.data.Datos;
                                $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoConsulta: 1,
                    NumeroDocumentoPlanilla: item.NumeroDocumentoPlanilla,
                    Anulado: 0
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }

            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.Modelo.NumeroDocumentoPlanilla = item.NumeroDocumentoPlanilla;
            $scope.Modelo.NumeroDocumentoOrden = item.NumeroDocumentoOrden;
            $scope.Modelo.NumeroPlanilla = item.NumeroPlanilla;
            $scope.Modelo.NumeroOrden = item.NumeroOrden;
            $scope.Modelo.NumeroManifiesto = item.NumeroDocumentoManifiesto;
            $scope.Modelo.TipoOrigenSeguimiento = { Codigo: CODIGO_TIPO_ORIGEN_SEGUIMIENTO_MANUAL };
            $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == 8200');
            $scope.Modelo.PuestoControl = '';
            $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
            $scope.Modelo.Ubicacion = '';
            $scope.Modelo.Observaciones = '';
            $scope.Modelo.Latitud = '';
            $scope.Modelo.Longitud = '';
            $scope.Reportar = false;
            $scope.Prioritario = false;
            $scope.Modelo.ReplicarRemesas = false;
            $scope.Modelo.Orden = 0;
            //$scope.Modelo.EnvioReporteCliente = false 
            ConsultarRemesas();
        };
        console.clear()
        $scope.ConsultarDetaleSeguimientos = function (Planilla) {
            showModal('modaldetalleSeguimientoPlanilla')
            $scope.ListadoDetalleSeguimientosPlanilla = [];
            $scope.ListadoDetalleSeguimientosPlanillaGPS = [];
            $scope.iniciamapa = true
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoConsulta: 1,
                NumeroDocumentoPlanilla: Planilla,
                Anulado: 0
            }
            DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos
                            //filtros.ConsultaGPS = 1
                            //BloqueoPantalla.start('Consultando reportes GPS...');

                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        }
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SITIO_REPORTE_SEGUIMIENDO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSitioReporteSeguimientos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoSitioReporteSeguimientos = response.data.Datos;
                    }
                    else {
                        $scope.ListadoSitioReporteSeguimientos = []
                    }
                }
            }, function (response) {
            });
        $scope.ShowList = function (item) {
            if (item.show == true) {
                item.show = false
            } else {
                item.show = true
            }
        }
        $scope.AbrirTiempos = function (item) {
            //PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.NumeroDocumentoPlanilla, Sync: true }).Datos[0]
            $scope.Planillatemp = PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.NumeroDocumentoPlanilla, Sync: true, Estado: { Codigo: 1 }, TipoDocumento: 150 }).Datos[0]
            //$scope.Planillatemp.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            $scope.ListadoTiemposReporte = []
            $scope.ListadoTiemposRemesas = []
            $scope.ListadoTiemposReporte = $linq.Enumerable().From($scope.ListadoSitioReporteSeguimientos).Where(function (x) { return x.CampoAuxiliar2 > 0 }).OrderBy(function (x) { return x.CampoAuxiliar2 }).Select(function (x) { return x }).ToArray();
            $scope.ListadoTiemposLogisticosRemesas = $linq.Enumerable().From($scope.ListadoTiemposLogisticos).Where(function (x) { return x.CampoAuxiliar2 > 0 }).OrderBy(function (x) { return x.CampoAuxiliar2 }).Select(function (x) { return x }).ToArray();
            for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                $scope.ListadoTiemposReporte[i].Fecha = undefined
            }
            PlanillaDespachosFactory.Obtener_Detalle_Tiempos($scope.Planillatemp).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                            for (var j = 0; j < response.data.Datos.length; j++) {
                                if ($scope.ListadoTiemposReporte[i].Codigo == response.data.Datos[j].SitioReporteSeguimientoVehicular.Codigo) {
                                    $scope.ListadoTiemposReporte[i].Fecha = new Date(response.data.Datos[j].FechaHora)
                                }
                            }
                        }
                    }
                }
            });
            PlanillaDespachosFactory.Obtener_Detalle_Tiempos_Remesas($scope.Planillatemp).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTiemposRemesas = []
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if ($scope.ListadoTiemposRemesas.length == 0) {
                                for (var j = 0; j < $scope.ListadoTiemposLogisticosRemesas.length; j++) {
                                    var ObjetoRemesaXtiempos = angular.copy($scope.ListadoTiemposLogisticosRemesas[j])
                                    ObjetoRemesaXtiempos.Numero = response.data.Datos[i].NumeroDocumentoRemesa
                                    ObjetoRemesaXtiempos.CodigoRemesa = response.data.Datos[i].NumeroRemesa
                                    $scope.ListadoTiemposRemesas.push(ObjetoRemesaXtiempos)
                                }
                            } else {
                                var Count = 0
                                for (var k = 0; k < $scope.ListadoTiemposRemesas.length; k++) {
                                    if ($scope.ListadoTiemposRemesas[k].CodigoRemesa == response.data.Datos[i].NumeroRemesa) {
                                        Count++
                                        break;
                                    }
                                }
                                if (Count == 0) {
                                    for (var j = 0; j < $scope.ListadoTiemposLogisticosRemesas.length; j++) {
                                        var ObjetoRemesaXtiempos = angular.copy($scope.ListadoTiemposLogisticosRemesas[j])
                                        ObjetoRemesaXtiempos.Numero = response.data.Datos[i].NumeroDocumentoRemesa
                                        ObjetoRemesaXtiempos.CodigoRemesa = response.data.Datos[i].NumeroRemesa
                                        $scope.ListadoTiemposRemesas.push(ObjetoRemesaXtiempos)
                                    }
                                }
                            }
                        }
                        $scope.ListadoTiemposRemesas = AgruparListados($scope.ListadoTiemposRemesas, 'Numero');

                        for (var j = 0; j < response.data.Datos.length; j++) {
                            for (var i = 0; i < $scope.ListadoTiemposRemesas.length; i++) {
                                if ($scope.ListadoTiemposRemesas[i].Numero == response.data.Datos[j].NumeroDocumentoRemesa) {
                                    for (var k = 0; k < $scope.ListadoTiemposRemesas[i].Data.length; k++) {
                                        if ($scope.ListadoTiemposRemesas[i].Data[k].Codigo == response.data.Datos[j].SitioReporteSeguimientoVehicular.Codigo && $scope.ListadoTiemposRemesas[i].Data[k].CodigoRemesa == response.data.Datos[j].NumeroRemesa) {
                                            $scope.ListadoTiemposRemesas[i].Data[k].Fecha = new Date(response.data.Datos[j].FechaHora)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
            showModal('modalTiempos')
        }
        $scope.GuardarTiempos = function (opt) {
            $scope.opt = opt
            $scope.Planillatemp.Sync = false
            var MilisegundoXdia = 86400000
            var MilisegundoXminuto = 60000
            var fechaPlanilla = new Date($scope.Planillatemp.Fecha)
            $scope.MensajesErrorModalTiempos = []
            for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                var item = $scope.ListadoTiemposReporte[i]
                //Condicionales Basicos de proceso
                if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (item.Fecha >= new Date()) {
                        $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' no puede ser mayor a la fecha actual')
                    }
                    if (i > 0) {
                        if (item.Fecha < $scope.ListadoTiemposReporte[i - 1].Fecha) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' no puede ser menor a la fecha de ' + $scope.ListadoTiemposReporte[i - 1].Nombre)
                        }
                    }
                }

                //Condicionales Logisticos
                //---------------------------------------------Salida Cargue-----------------------------------------

                if (item.Codigo == 8212 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8203) { //Llegada cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue no puede ser mayor de 3 dias a la fecha de llegada al cargue')
                                }
                            }
                        }
                        if (item2.Codigo == 8211) { //Ingreso cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue debe ser mayor a 15 minutos de la fecha de ingreso cargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Fin Cargue-----------------------------------------

                if (item.Codigo == 8205 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8204) { //Inicio cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue debe ser mayor a 15 minutos de la fecha de inicio cargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Ingreso Cargue-----------------------------------------

                if (item.Codigo == 8211 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de ingreso cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Llegada Cargue-----------------------------------------

                if (item.Codigo == 8203 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de llegada cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------LLegada Descargue-----------------------------------------

                if (item.Codigo == 8207 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8212) { //Salida cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue no puede ser mayor a 15 dias de la fecha de salida cargue')
                                } else if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 30) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue debe ser mayor a 30 minutos de la fecha de salida cargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de llegada descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Ingreso Descargue-----------------------------------------

                if (item.Codigo == 8213 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de ingreso descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Salida Descargue-----------------------------------------

                if (item.Codigo == 8214 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (item2.Codigo == 8207) { //Llegada descargue
                        if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                            if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue no puede ser mayor de 3 dias a la fecha de llegada descargue')
                            }
                        }
                    }
                    if (item2.Codigo == 8213) { //Ingreso descargue
                        if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                            if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue debe ser mayor a 15 minutos de la fecha de ingreso descargue')
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de salida descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Fin Descargue-----------------------------------------

                if (item.Codigo == 8209 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8208) { //Inicio descargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue debe ser mayor a 15 minutos de la fecha de inicio descargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }
            }
            for (var j = 0; j < $scope.ListadoTiemposRemesas.length; j++) {
                for (var i = 0; i < $scope.ListadoTiemposRemesas[j].Data.length; i++) {
                    var item = $scope.ListadoTiemposRemesas[j].Data[i]
                    //Condicionales Basicos de proceso
                    if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (item.Fecha >= new Date()) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor a la fecha actual')
                        }
                        if (i > 0) {
                            if (item.Fecha < $scope.ListadoTiemposRemesas[j].Data[i - 1].Fecha) {
                                $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser menor a la fecha de ' + $scope.ListadoTiemposRemesas[j].Data[i - 1].Nombre)
                            }
                        }
                    }

                    //Condicionales Logisticos
                    //---------------------------------------------Salida Cargue-----------------------------------------

                    if (item.Codigo == 20205 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20201) { //Llegada cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor de 3 dias a la fecha de llegada al cargue')
                                    }
                                }
                            }
                            if (item2.Codigo == 20202) { //Ingreso cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + '  debe ser mayor a 15 minutos de la fecha de ingreso cargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Fin Cargue-----------------------------------------

                    if (item.Codigo == 20204 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20203) { //Inicio cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de inicio cargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Ingreso Cargue-----------------------------------------

                    if (item.Codigo == 20202 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de ingreso cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Llegada Cargue-----------------------------------------

                    if (item.Codigo == 20201 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de llegada cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------LLegada Descargue-----------------------------------------

                    if (item.Codigo == 20206 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20205) { //Salida cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor a 15 dias de la fecha de salida cargue')
                                    } else if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 30) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 30 minutos de la fecha de salida cargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de llegada descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Ingreso Descargue-----------------------------------------

                    if (item.Codigo == 20207 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de ingreso descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Salida Descargue-----------------------------------------

                    if (item.Codigo == 20210 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (item2.Codigo == 20206) { //Llegada descargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor de 3 dias a la fecha de llegada descargue')
                                }
                            }
                        }
                        if (item2.Codigo == 20207) { //Ingreso descargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de ingreso descargue')
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Fin Descargue-----------------------------------------

                    if (item.Codigo == 20209 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20208) { //Inicio descargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de inicio descargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                }
            }
            if ($scope.MensajesErrorModalTiempos.length == 0) {
                $scope.Planillatemp.DetalleTiempos = []
                for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                    var item = $scope.ListadoTiemposReporte[i]
                    if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        $scope.Planillatemp.DetalleTiempos.push({
                            SitioReporteSeguimientoVehicular: { Codigo: item.Codigo },
                            FechaHora: item.Fecha
                        })
                    }
                }
                $scope.Planillatemp.DetalleTiemposRemesa = []
                for (var j = 0; j < $scope.ListadoTiemposRemesas.length; j++) {
                    for (var i = 0; i < $scope.ListadoTiemposRemesas[j].Data.length; i++) {
                        var item = $scope.ListadoTiemposRemesas[j].Data[i]
                        if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                            $scope.Planillatemp.DetalleTiemposRemesa.push({
                                SitioReporteSeguimientoVehicular: { Codigo: item.Codigo },
                                FechaHora: item.Fecha,
                                NumeroRemesa: item.CodigoRemesa
                            })
                        }
                    }
                }
                if ($scope.Planillatemp.DetalleTiempos.length == 0 && $scope.Planillatemp.DetalleTiemposRemesa.length == 0) {
                    ShowError('Debe ingresar al menos una fecha')
                }
                else {
                    $scope.Planillatemp.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    PlanillaDespachosFactory.Insertar_Detalle_Tiempos($scope.Planillatemp).then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('Las fechas se guardaron correctamente')
                                if ($scope.opt > 0) {
                                    closeModal('modalTiempos2', 1)
                                } else {
                                    closeModal('modalTiempos')
                                }
                            }
                        }
                    });
                }
            }
        }
        $scope.GuardarSeguimiento = function () {

            if (DatosRequeridos()) {
                BloqueoPantalla.start('Guardando Seguimiento, por favor espere...');
                var continuar = true 
               
                var Horasminutos = [];
                $scope.Modelo.FechaReporte = new Date($scope.Modelo.FechaReporte)
                if ($scope.Modelo.HoraSeguimiento != undefined) {
                    Horasminutos = $scope.Modelo.HoraSeguimiento.split(':');
                    $scope.Modelo.FechaReporte.setHours(Horasminutos[0]);
                    $scope.Modelo.FechaReporte.setMinutes(Horasminutos[1]); 
                }

                if ($scope.Modelo.SitioReporteSeguimiento.Codigo == 8210 && $scope.Modelo.NumeroDocumentoPlanilla > 0) {
                    $scope.Planillatemp = PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: $scope.Modelo.NumeroDocumentoPlanilla, Sync: true, Estado: { Codigo: 1 }, TipoDocumento: 150 }).Datos[0]
                    $scope.Planillatemp.Sync = true
                    //$scope.Planillatemp.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                    $scope.ListadoTiemposReporte = []
                    $scope.ListadoTiemposRemesas = []
                    $scope.ListadoTiemposReporte = $linq.Enumerable().From($scope.ListadoSitioReporteSeguimientos).Where(function (x) { return x.CampoAuxiliar2 > 0 }).OrderBy(function (x) { return x.CampoAuxiliar2 }).Select(function (x) { return x }).ToArray();
                    $scope.ListadoTiemposLogisticosRemesas = $linq.Enumerable().From($scope.ListadoTiemposLogisticos).Where(function (x) { return x.CampoAuxiliar2 > 0 }).OrderBy(function (x) { return x.CampoAuxiliar2 }).Select(function (x) { return x }).ToArray();

                    for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                        $scope.ListadoTiemposReporte[i].Fecha = undefined
                    }
                    var tiempos = PlanillaDespachosFactory.Obtener_Detalle_Tiempos($scope.Planillatemp).Datos
                    var tiemposRemesas = PlanillaDespachosFactory.Obtener_Detalle_Tiempos_Remesas($scope.Planillatemp).Datos
                    for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                        for (var j = 0; j < tiempos.length; j++) {
                            if ($scope.ListadoTiemposReporte[i].Codigo == tiempos[j].SitioReporteSeguimientoVehicular.Codigo) {
                                $scope.ListadoTiemposReporte[i].Fecha = new Date(tiempos[j].FechaHora)
                            }
                        }
                    }
                    /*if (tiempos.length >= $scope.ListadoTiemposReporte.length) {
                        continuar = true
                    } else {
                        continuar = false
                        showModal('modalTiempos2')
                        ShowError('Por favor registre todos los tiempos de la planilla')
                        BloqueoPantalla.stop();
                    }*/
                    if (tiempos.length < $scope.ListadoTiemposReporte.length) {
                        continuar = false;
                        if ($scope.Sesion.UsuarioAutenticado.ProcesoSugerirTiemposCargueDescargue) {
                            SugerirTiemposCargueDescargue();
                        }
                        showModal('modalTiempos2');
                        ShowError('Por favor registre todos los tiempos de la planilla');
                        BloqueoPantalla.stop();
                    }

                    $scope.ListadoTiemposRemesas = []
                    for (var i = 0; i < tiemposRemesas.length; i++) {
                        if ($scope.ListadoTiemposRemesas.length == 0) {
                            for (var j = 0; j < $scope.ListadoTiemposLogisticosRemesas.length; j++) {
                                var ObjetoRemesaXtiempos = angular.copy($scope.ListadoTiemposLogisticosRemesas[j])
                                ObjetoRemesaXtiempos.Numero = tiemposRemesas[i].NumeroDocumentoRemesa
                                ObjetoRemesaXtiempos.CodigoRemesa = tiemposRemesas[i].NumeroRemesa
                                $scope.ListadoTiemposRemesas.push(ObjetoRemesaXtiempos)
                            }
                        } else {
                            var Count = 0
                            for (var k = 0; k < $scope.ListadoTiemposRemesas.length; k++) {
                                if ($scope.ListadoTiemposRemesas[k].CodigoRemesa == tiemposRemesas[i].NumeroRemesa) {
                                    Count++
                                    break;
                                }
                            }
                            if (Count == 0) {
                                for (var j = 0; j < $scope.ListadoTiemposLogisticosRemesas.length; j++) {
                                    var ObjetoRemesaXtiempos = angular.copy($scope.ListadoTiemposLogisticosRemesas[j])
                                    ObjetoRemesaXtiempos.Numero = tiemposRemesas[i].NumeroDocumentoRemesa
                                    ObjetoRemesaXtiempos.CodigoRemesa = tiemposRemesas[i].NumeroRemesa
                                    $scope.ListadoTiemposRemesas.push(ObjetoRemesaXtiempos)
                                }
                            }
                        }
                    }
                    /*if (tiemposRemesas.length >= $scope.ListadoTiemposRemesas.length) {
                        continuar = true
                    } else {
                        continuar = false
                        showModal('modalTiempos2')
                        ShowError('Por favor registre todos los tiempos de la planilla y las remesas')
                        BloqueoPantalla.stop();
                    }*/
                    /*if (tiemposRemesas.length < $scope.ListadoTiemposRemesas.length) {
                        continuar = false;
                        showModal('modalTiempos2');
                        ShowError('Por favor registre todos los tiempos de la planilla y las remesas');
                        BloqueoPantalla.stop();
                    }*/
                    $scope.ListadoTiemposRemesas = AgruparListados($scope.ListadoTiemposRemesas, 'Numero');
                    for (var j = 0; j < tiemposRemesas.length; j++) {
                        for (var i = 0; i < $scope.ListadoTiemposRemesas.length; i++) {
                            if ($scope.ListadoTiemposRemesas[i].Numero == tiemposRemesas[j].NumeroDocumentoRemesa) {
                                for (var k = 0; k < $scope.ListadoTiemposRemesas[i].Data.length; k++) {
                                    if ($scope.ListadoTiemposRemesas[i].Data[k].Codigo == tiemposRemesas[j].SitioReporteSeguimientoVehicular.Codigo && $scope.ListadoTiemposRemesas[i].Data[k].CodigoRemesa == tiemposRemesas[j].NumeroRemesa) {
                                        $scope.ListadoTiemposRemesas[i].Data[k].Fecha = new Date(tiemposRemesas[j].FechaHora)
                                    }
                                }
                            }
                        }
                    }
                }
                if (continuar) {
                    if ($scope.Reportar === true) {
                        $scope.Modelo.ReportarCliente = 1;
                    } else {
                        $scope.Modelo.ReportarCliente = 0;
                    }
                    if ($scope.Prioritario === true) {
                        $scope.Modelo.Prioritario = 1;
                    }
                    else {
                        $scope.Modelo.Prioritario = 0;
                    }
                    $scope.Modelo.Latitud = parseFloat($scope.Modelo.Latitud);
                    $scope.Modelo.Longitud = parseFloat($scope.Modelo.Longitud);
                    $scope.Modelo.ListaSeguimientosRemesas = [];
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos.forEach(function (item) {
                        item.NumeroPlanilla = $scope.Modelo.NumeroPlanilla;
                        item.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                        item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        item.Latitud = parseFloat($scope.Modelo.Latitud);
                        item.Longitud = parseFloat($scope.Modelo.Longitud);
                        if (item.ReportarCliente === true) {
                            item.ReportarCliente = 1;
                        } else {
                            item.ReportarCliente = 0;
                        }
                        $scope.Modelo.ListaSeguimientosRemesas.push(item);
                    });

                    if ($scope.Modelo.PuestoControl.PuestoControl !== undefined) {
                        $scope.Modelo.PuestoControl = { Codigo: $scope.Modelo.PuestoControl.PuestoControl.Codigo };
                    }
                    if ($scope.Modelo.SitioReporteSeguimiento.Codigo == 8210) {
                        showModal('ModalConfirmacionGuardarSeguimiento');
                                            } else { 
                        console.log('guardar', $scope.Modelo)
                        DetalleSeguimientoVehiculosFactory.Guardar($scope.Modelo).
                            then(function (response) {
                                if (response.data.Datos > 0) {
                                    closeModal('ModalConfirmacionGuardarSeguimiento');
                                    closeModal('modalnuevoseguimientoplanilla');
                                    closeModal('modalnuevoseguimientoorden');
                                    ShowSuccess('El seguimiento se guardó correctamente')
                                    $scope.Modelo.HoraSeguimiento = ''
                                    $scope.Modelo.FechaReporte = ''
                                    Find();

                                }
                            }, function (response) {
                                    ShowError(response.statusText);
                                    BloqueoPantalla.stop();
                            });
                    }
                }
                BloqueoPantalla.stop();
            }
        };

        

        $scope.ConfirmacionGuardarNuevoSeguimientos = function () {

            if ($scope.Modelo.SitioReporteSeguimiento.Codigo === 8210 && $scope.Reportar) {
                filtros = {
                    Vehiculo: $scope.ModalDespachos.Vehiculo
                    , tenedor: $scope.ModalDespachos.Tenedor
                    , Conductor: $scope.ModalDespachos.Conductor
                    , TipoOrigenSeguimiento: $scope.TipoOrigenSeguimiento
                    , NumeroPlanilla: $scope.Modelo.NumeroDocumentoPlanilla
                    , FechaCrea: new Date()
                    , Documento: []
                    , CodigoEvento: 221
                    , Codigo: 0
                    , TipoConsulta: 1

                }
                $scope.EnviarCorreo(filtros);
            }

            DetalleSeguimientoVehiculosFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.Datos > 0) {
                        closeModal('ModalConfirmacionGuardarSeguimiento');
                        closeModal('modalnuevoseguimientoplanilla');
                        closeModal('modalnuevoseguimientoorden');
                        ShowSuccess('El seguimiento se guardó correctamente')
                        Find();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        function DatosRequeridos() {
            $scope.MensajesErrorSeguimiento = [];
            var continuar = true;
            if ($scope.Modelo.SitioReporteSeguimiento === undefined || $scope.Modelo.SitioReporteSeguimiento === "" || $scope.Modelo.SitioReporteSeguimiento === null || $scope.Modelo.SitioReporteSeguimiento.Codigo === 8200) {
                $scope.MensajesErrorSeguimiento.push('Debe ingresar el sitio del reporte');
                continuar = false;
            }
            return continuar;
        }

        $scope.VerSeguimiento = function (item) {
            $scope.VariableDetalle = 'DETALLE'
            $scope.MensajesErrorSeguimiento = [];
            $scope.CodigoNovedadSeguimiento = 0;
            $scope.ListadoDetalleSeguimientosPlanilla = []
            if (item.TipoConsulta === 0) {
                showModal('modalnuevoseguimientoorden');
                $scope.ModalOrdenes = {}
                DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Codigo, TipoConsulta: item.TipoConsulta }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ModalOrdenes = response.data.Datos;
                                $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                                if ($scope.ModalOrdenes.ReportarCliente === 1) {
                                    $scope.Reportar = true;
                                } else {
                                    $scope.Reportar = false;
                                }
                                if ($scope.ModalOrdenes.Prioritario === 1) {
                                    $scope.Prioritario = true;
                                } else {
                                    $scope.Prioritario = false;
                                }
                                $scope.DeshabilitarDetalle = true;
                                $scope.iniciarMapaOrden($scope.ModalOrdenes.Latitud, $scope.ModalOrdenes.Longitud)
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoConsulta: 0,
                    NumeroDocumentoOrden: item.NumeroDocumentoOrden,
                    Anulado: 0
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else if (item.TipoConsulta == 1) {
                $scope.iniciamapa = true
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoConsulta: 1,
                    NumeroDocumentoPlanilla: item.NumeroDocumentoPlanilla,
                    Anulado: 0,
                    ConsultaGPS: 1
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {
                        BloqueoPantalla.stop();
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanillaGPS = response.data.Datos
                                var marker = [];
                                var map, infoWindow;
                                function initMap(posicion) {
                                    if (map === void 0) {
                                        var mapOptions = {
                                            zoom: 15,
                                            center: new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud),
                                            disableDefaultUI: true,
                                            mapTypeId: window.google.maps.MapTypeId.ROADMAP
                                        }
                                        //Asignacion al id gmapsplanilla que se encuentra en el html
                                        map = new window.google.maps.Map(document.getElementById('gmapsplanillaGPS'), mapOptions);
                                    }
                                }

                                function setMarker(map, position, title, content) {
                                    var markers;
                                    var markerOptions = {
                                        position: position,
                                        map: map,
                                        //draggable opcion que permite mover el marcador
                                        draggable: false,
                                        //bounce le da animacion al marcador
                                        //animation: google.maps.Animation.BOUNCE,
                                        title: title,
                                    };

                                    markers = new window.google.maps.Marker(markerOptions);
                                    //dragend permite capturar las coordenadas seleccionadas en el mapa
                                    markers.addListener('dragend', function (event) {
                                        //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                                        $scope.Modelo.Latitud = this.getPosition().lat();
                                        $scope.Modelo.Longitud = this.getPosition().lng();
                                    });
                                    //Variable donde se asignan los datos del marcador
                                    marker.push(markers);

                                    //var infoWindowOptions = {
                                    //    content: content
                                    //};
                                    //infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                                    //infoWindow.open(map, markers);

                                }
                                for (var i = 0; i < $scope.ListadoDetalleSeguimientosPlanillaGPS.length; i++) {
                                    var posicion = $scope.ListadoDetalleSeguimientosPlanillaGPS[i]
                                    if (posicion.Longitud !== 0 && posicion.Latitud !== 0) {
                                        if ($scope.iniciamapa) {
                                            initMap(posicion)
                                            $scope.iniciamapa = false
                                        }
                                        setMarker(map, new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud), '', '');

                                    }
                                }
                                for (var i = 0; i < $scope.ListadoDetalleSeguimientosPlanilla.length; i++) {
                                    var posicion = $scope.ListadoDetalleSeguimientosPlanilla[i]
                                    if (posicion.Longitud !== 0 && posicion.Latitud !== 0) {
                                        if ($scope.iniciamapa) {
                                            initMap(posicion)
                                            $scope.iniciamapa = false
                                        }
                                        setMarker(map, new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud), '', '');
                                    }
                                }
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        BloqueoPantalla.stop();
                    });
                showModal('modalnuevoseguimientoplanilla');
                $scope.ModalDespachos = {}
                DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Codigo, TipoConsulta: item.TipoConsulta }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ModalDespachos = response.data.Datos;
                                $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                                $scope.Modelo.Orden = response.data.Datos.Orden;
                                ConsultarRemesas();
                                if ($scope.ModalDespachos.ReportarCliente === 1) {
                                    $scope.Reportar = true;
                                } else {
                                    $scope.Reportar = false;
                                }
                                if ($scope.ModalDespachos.Prioritario === 1) {
                                    $scope.Prioritario = true;
                                } else {
                                    $scope.Prioritario = false;
                                }
                                $scope.DeshabilitarDetalle = true;
                                //$scope.iniciarMapaPlanilla($scope.ModalDespachos.Latitud, $scope.ModalDespachos.Longitud)
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoConsulta: 1,
                    NumeroDocumentoPlanilla: item.NumeroDocumentoPlanilla,
                    Anulado: 0
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }

            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.Modelo.NumeroDocumentoPlanilla = item.NumeroDocumentoPlanilla;
            $scope.Modelo.NumeroDocumentoOrden = item.NumeroDocumentoOrden;
            $scope.Modelo.NumeroPlanilla = item.NumeroPlanilla;
            $scope.Modelo.NumeroOrden = item.NumeroOrden;
            $scope.Modelo.NumeroManifiesto = item.NumeroManifiesto;
            $scope.Modelo.TipoOrigenSeguimiento = { Codigo: CODIGO_TIPO_ORIGEN_SEGUIMIENTO_MANUAL };

            $scope.CodigoSitioReporteSeguimiento = item.SitioReporteSeguimiento.Codigo;
            if ($scope.ListaSitioReporteModal.length > 0 && $scope.CodigoSitioReporteSeguimiento > 0) {
                $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == ' + item.SitioReporteSeguimiento.Codigo);
            }

            $scope.CodigoNovedadSeguimiento = item.NovedadSeguimiento.Codigo;
            if ($scope.ListaNovedadSeguimiento.length > 0 && $scope.CodigoNovedadSeguimiento > 0) {
                $scope.Modelo.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo == ' + item.NovedadSeguimiento.Codigo);
            }

            $scope.CodigoPuestoControl = item.PuestoControl.Codigo;
            if ($scope.ListaPuntosControl.length > 0 && $scope.CodigoPuestoControl > 0) {
                $scope.Modelo.PuestoControl = $linq.Enumerable().From($scope.ListaPuntosControl).First('$.PuestoControl.Codigo == ' + item.PuestoControl.Codigo);
            }

            $scope.Modelo.Ubicacion = item.Ubicacion;
            $scope.Modelo.Observaciones = item.ObservacionCompleta;
            $scope.Modelo.Latitud = item.Latitud;
            $scope.Modelo.Longitud = item.Longitud;
            //$scope.Modelo.EnvioReporteCliente = false

        }

        $scope.PuestosControl = function (item) {
        $scope.VariableDetalle = 'PUESTOS DE CONTROL' 
            $scope.ModalPuestoControl = item 

            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumentoPlanilla: $scope.ModalPuestoControl.NumeroManifiesto,
                NumeroManifiesto: $scope.ModalPuestoControl.NumeroPlanilla

            }

            $scope.Lista1 = []
            
            DetalleSeguimientoVehiculosFactory.ConsultarPuntosControl(Filtro). 
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.Lista1.push(
                                    {
                                        PuestoControl:   response.data.Datos[i].PuestoControl.Nombre,
                                        TiempoEstimado: response.data.Datos[i].PuestoControl.TiempoEstimado,
                                        FechaPlaneada: response.data.Datos[i].FechaUltimoReporte,
                                        FechaAjustada: response.data.Datos[i].FechaReporte,
                                        FechaIngreso: response.data.Datos[i].FechaReporte,
                                        FechaAnterior: 0,
                                        HorasEstimadas: new Date(response.data.Datos[i].PuestoControl.TiempoEstimado).getHours() + ':' + new Date(response.data.Datos[i].PuestoControl.TiempoEstimado).getMinutes()


                                    } 
                                )

                            }

                        }

                        console.log('listado', $scope.Lista1)
                         //Sacar hora minutos de tiempo estimado
                        var horaArribo = new Date($scope.Lista1[0].TiempoEstimado).getHours()
                        var MinutosArribo = new Date($scope.Lista1[0].TiempoEstimado).getMinutes() 
                        //asignar el primer registro de fecha 
                        var FechaInicio = new Date($scope.Lista1[0].FechaPlaneada) 
                        //agregarle el tiempo estimado 
                        FechaInicio.setHours(FechaInicio.getHours() + horaArribo);
                        FechaInicio.setMinutes(FechaInicio.getMinutes() + MinutosArribo) 
                        //asignar los valores a la primera columna
                        $scope.Lista1[0].FechaAnterior = FechaInicio 
                        $scope.Lista1[0].FechaPlaneada = FechaInicio 

                        if ($scope.Lista1[0].FechaIngreso != '0001-01-01T00:00:00-05:00') {
                            $scope.Lista1[0].FechaAjustada = FechaInicio

                        } else {
                            $scope.Lista1[0].FechaAjustada = $scope.Lista1[0].FechaPlaneada
                            $scope.Lista1[0].FechaIngreso = '0001-01-01T00:00:00-05:00'
                        }

                        //recorremos la lista a partir de la segunda columna
                        for (var i = 1; i < $scope.Lista1.length; i++) {

                            //Sacar hora minutos de tiempo estimado
                            var horaArribo = new Date($scope.Lista1[i].TiempoEstimado).getHours()
                            var MinutosArribo = new Date($scope.Lista1[i].TiempoEstimado).getMinutes()


                            var FechaInicio = new Date($scope.Lista1[i - 1].FechaAnterior)

                            FechaInicio.setHours(FechaInicio.getHours() + horaArribo);
                            FechaInicio.setMinutes(FechaInicio.getMinutes() + MinutosArribo)

                            $scope.Lista1[i].FechaPlaneada = FechaInicio 
                            $scope.Lista1[i].FechaAnterior = FechaInicio



                            if ($scope.Lista1[i - 1].FechaIngreso != '0001-01-01T00:00:00-05:00') {

                                var FechaAjustada = new Date($scope.Lista1[i - 1].FechaIngreso)

                                FechaAjustada.setHours(FechaAjustada.getHours() + horaArribo);
                                FechaAjustada.setMinutes(FechaAjustada.getMinutes() + MinutosArribo)
                                $scope.Lista1[i].FechaAjustada = FechaAjustada

                            }
                            else {

                                var FechaAjustada = new Date($scope.Lista1[i - 1].FechaAjustada)

                                FechaAjustada.setHours(FechaAjustada.getHours() + horaArribo);
                                FechaAjustada.setMinutes(FechaAjustada.getMinutes() + MinutosArribo)
                                $scope.Lista1[i].FechaAjustada = FechaAjustada
                                $scope.Lista1[i].FechaIngreso = '0001-01-01T00:00:00-05:00'
                            }

                        


                        }

                        console.log('lista', $scope.Lista1)

                    }
                }, function (response) {
                    BloqueoPantalla.stop();
                    ShowError(response.statusText);
                });
             
            showModal('modalPuestosControl');




        }

        /*---------------------------------------------------------------------Funciones grid remesas----------------------------------------------------------------------------*/
        function ConsultarRemesas() {
            /*Cargar el combo Novedad seguimiento */
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NOVEDAD_SEGUIMIENTO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaNovedadSeguimiento = [];
                        $scope.ListaNovedadSeguimiento = response.data.Datos;
                        if ($scope.CodigoNovedadSeguimiento !== undefined && $scope.CodigoNovedadSeguimiento !== '' && $scope.CodigoNovedadSeguimiento !== null && $scope.CodigoNovedadSeguimiento !== 0) {
                            $scope.Modelo.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo ==' + $scope.CodigoNovedadSeguimiento);
                        } else {
                            $scope.Modelo.NovedadSeguimiento = '';
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroPlanilla: $scope.Modelo.NumeroPlanilla,
                Orden: $scope.Modelo.Orden
                //Pagina: $scope.paginaActualPlanillaRemesas,
                //RegistrosPagina: $scope.cantidadRegistrosPorPaginasPlanillaRemesas
            };
            DetalleSeguimientoVehiculosFactory.ConsultarRemesas(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoSeguimientosRemesasPlanillaDespachos = [];
                            response.data.Datos.forEach(function (item) {
                                item.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == ' + item.SitioReporteSeguimiento.Codigo);
                                if ($scope.ListaNovedadSeguimiento.length) {
                                    item.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo == ' + item.NovedadSeguimiento.Codigo);
                                } else {
                                    $scope.ListaNovedadSeguimiento = [];
                                    $scope.Modelo.NovedadSeguimiento = '';
                                }
                                if (item.ReportarCliente === 1) {
                                    item.ReportarCliente = true;
                                } else {
                                    item.ReportarCliente = false;
                                }
                                $scope.ListadoSeguimientosRemesasPlanillaDespachos.push(item);
                            });
                            if ($scope.ListadoSeguimientosRemesasPlanillaDespachos.length > 0) {
                                $scope.totalRegistrosPlanillaRemesas = $scope.ListadoSeguimientosRemesasPlanillaDespachos.length;
                                $scope.BuscandoPlanillaRemesas = true;
                                $scope.ResultadoSinRegistrosPlanillaRemesas = '';
                            } else {
                                $scope.totalRegistrosPlanillaRemesas = 0;
                                $scope.paginaActualPlanillaRemesas = 1;
                                $scope.ResultadoSinRegistrosPlanillaRemesas = 'No hay datos para mostrar';
                                $scope.BuscandoPlanillaRemesas = false;
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.AsignarValor = function (item2) {
            for (var j = 0; j < $scope.ListadoSeguimientosRemesasPlanillaDespachos.length; j++) {
                var itemGrid = $scope.ListadoSeguimientosRemesasPlanillaDespachos[j];
                $scope.MensajesErrorSeguimiento = [];
                if (item2.SitioReporteSeguimiento === undefined || item2.SitioReporteSeguimiento === "" || item2.SitioReporteSeguimiento === null) {
                    $scope.MensajesErrorSeguimiento.push('Debe seleccionar una opción, en el sitio del reporte para la remesa' + item2.NumeroDocumentoRemesa);
                } else if (item2.NovedadSeguimiento === undefined || item2.NovedadSeguimiento === "" || item2.NovedadSeguimiento === null) {
                    $scope.MensajesErrorSeguimiento.push('Debe seleccionar una opción, en la novedad para la remesa' + item2.NumeroDocumentoRemesa);
                } else {
                    if (
                        itemGrid.NumeroDocumentoRemesa === item2.NumeroDocumentoRemesa
                        && itemGrid.NumeroRemesa === item2.NumeroRemesa
                    ) {
                        $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].SitioReporteSeguimiento = item2.SitioReporteSeguimiento;
                        $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].NovedadSeguimiento = item2.NovedadSeguimiento;
                        $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].ReportarCliente = item2.ReportarCliente;
                    }
                }
            }
        };

        $scope.ReplicarRemesas = function (modelo, reportar) {
            for (var j = 0; j < $scope.ListadoSeguimientosRemesasPlanillaDespachos.length; j++) {
                $scope.MensajesErrorSeguimiento = [];
                if (modelo.ReplicarRemesas === true) {
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].SitioReporteSeguimiento = modelo.SitioReporteSeguimiento;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].NovedadSeguimiento = modelo.NovedadSeguimiento;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].ReportarCliente = reportar;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Ubicacion = modelo.Ubicacion;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Observaciones = modelo.Observaciones;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Latitud = modelo.Latitud;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Longitud = modelo.Longitud;
                } else {
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == 8200');
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo == 8100');
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].ReportarCliente = false;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Ubicacion = '';
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Observaciones = '';
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Latitud = 0;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Longitud = 0;
                }
            }
        };

        $scope.CopyItem = function (d) {
            var Objet = JSON.parse(JSON.stringify(new Object(d)));
            return Objet;
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.CargarMapa = function (item) {
            showModal('modalMostrarMapa');
            $scope.iniciarMapa(item.Latitud, item.Longitud);
        };
        $scope.CargarMapa2 = function (item) {
            showModal('modalMostrarMapa2');
            $scope.iniciarMapa(item.Latitud, item.Longitud);
        };
        //funcion que muestra la modal distribucion 
        $scope.AbrirDistribucion = function (item) {
            /*Cargar el combo de tipo identificaciones*/
            $scope.MensajesErrorModal = []
            $scope.itemRemesaDistribucion = item
            $scope.NumeroDocumentoRemesa = item.NumeroDocumentoRemesa;
            $scope.Distribucion.NumeroRemesa = item.NumeroRemesa;
            $scope.ResultadoSinRegistrosDistribucion = '';

            //funcion que consulta grid lista distribución remesas
            $scope.ListaDistribucion = [];
            var filtroDistribucion = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: $scope.Distribucion.NumeroRemesa,
            };
            DetalleDistribucionRemesasFactory.Consultar(filtroDistribucion).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Observaciones !== '') {
                                    var str = item.Observaciones;
                                    item.Observaciones = str.substring(0, 10);
                                    item.ObservacionCompleta = str.substring(0, 250);
                                    item.OcultarVerMas = true;
                                } else {
                                    item.OcultarVerMas = false;
                                }
                                $scope.ListaDistribucion.push(item);
                            });
                        } else {
                            $scope.DeshabilitarBotonNuevaDistribucion = true;
                            $scope.ResultadoSinRegistrosDistribucion = 'No hay distribuciones';
                        }
                    }
                }, function (response) {
                });
            showModal('modalDistribuciones');
        };

        //funcion que consulta detalles de la remesa
        $scope.CargarDatosRemesa = function () {
            /*Cargar autocomplete de productos transportados*/
            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaProductoTransportados = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListaProductoTransportados = response.data.Datos;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            //Filtros enviados
            var remesafiltros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Distribucion.NumeroRemesa,
                Obtener: 1
            };
            RemesasFactory.Obtener(remesafiltros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.NombreProductoTransportado = response.data.Datos.ProductoTransportado.Nombre;
                        $scope.NumeroCantidad = response.data.Datos.CantidadCliente;
                        $scope.NumeroPeso = response.data.Datos.PesoCliente;
                    } else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        /*----------------------------------------------------------------------------------------------Funciones mapa----------------------------------------------------------------------------------*/
        $scope.iniciarMapaPlanilla = function (lat, lng) {
            //inicialización de variables
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }
                    //Asignacion al id gmapsplanilla que se encuentra en el html
                    map = new window.google.maps.Map(document.getElementById('gmapsplanilla'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    //draggable opcion que permite mover el marcador
                    draggable: true,
                    //bounce le da animacion al marcador
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                };

                markers = new window.google.maps.Marker(markerOptions);
                //dragend permite capturar las coordenadas seleccionadas en el mapa
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                //Variable donde se asignan los datos del marcador
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                //Condición que muestra la ubicación del api o la ubicación segón latitud y longitud 
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }

        $scope.iniciarMapaOrden = function (lat, lng) {
            //inicialización de variables
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }
                    //Asignacion al id gmapsplanilla que se encuentra en el html
                    map = new window.google.maps.Map(document.getElementById('gmapsorden'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    //draggable opcion que permite mover el marcador
                    draggable: true,
                    //bounce le da animacion al marcador
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                };

                markers = new window.google.maps.Marker(markerOptions);
                //dragend permite capturar las coordenadas seleccionadas en el mapa
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                //Variable donde se asignan los datos del marcador
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                //Condición que muestra la ubicación del api o la ubicación segón latitud y longitud 
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        };

        $scope.iniciarMapaFlotaPropia = function (lat, lng) {
            //inicialización de variables
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }
                    //Asignacion al id gmapsplanilla que se encuentra en el html
                    map = new window.google.maps.Map(document.getElementById('gmapsflotapropia'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    //draggable opcion que permite mover el marcador
                    draggable: true,
                    //bounce le da animacion al marcador
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                };

                markers = new window.google.maps.Marker(markerOptions);
                //dragend permite capturar las coordenadas seleccionadas en el mapa
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                //Variable donde se asignan los datos del marcador
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                //Condición que muestra la ubicación del api o la ubicación segón latitud y longitud 
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }

        $scope.iniciarMapa = function (lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }

                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };

                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }

        /*----------------------------------------------------------------------------------------------Funciones foto----------------------------------------------------------------------------------*/
        $scope.ConsultarFoto = function (detalle) {

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: detalle.NumeroRemesa,
                Codigo: detalle.Codigo
            };

            DetalleDistribucionRemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Fotografia.FotoBits !== undefined && response.data.Datos.Fotografia.FotoBits !== null && response.data.Datos.Fotografia.FotoBits !== '') {
                            $scope.Foto = [];
                            $scope.Foto.push({
                                NombreFoto: response.data.Datos.Fotografia.NombreFoto,
                                TipoFoto: response.data.Datos.Fotografia.TipoFoto,
                                ExtensionFoto: response.data.Datos.Fotografia.ExtensionFoto,
                                FotoBits: response.data.Datos.Fotografia.FotoBits,
                                ValorDocumento: 1,
                                FotoCargada: 'data:' + response.data.Datos.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Fotografia.FotoBits
                            });
                        } else {
                            $scope.Foto = [];
                        }
                    }
                    else {
                        ShowError('No se logró consultar la foto ' + response.data.MensajeOperacion);
                        closeModal('ModalRecibirDistribucion', 1);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    closeModal('ModalRecibirDistribucion', 1);
                });
            showModal('ModalFoto');
        };

        $scope.CerrarModalFoto = function () {
            closeModal('ModalFoto', 1);
        }
        /*----------------------------------------------------------------------------------------------Funciones Firma----------------------------------------------------------------------------------*/
        $scope.ConsultarFirma = function (detalle) {
            $scope.LimpiarFirma();
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: detalle.NumeroRemesa,
                Codigo: detalle.Codigo
            };
            DetalleDistribucionRemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Firma !== undefined && response.data.Datos.Firma !== null && response.data.Datos.Firma !== '') {
                            var image = new Image();
                            image.src = "data:image/png;base64," + response.data.Datos.Firma;
                            var img = cargaContextoCanvas('Firma');
                            img.drawImage(image, 0, 0);
                        }
                        //$('#botonFirma').click();
                    }
                    else {
                        ShowError('No se logró consultar la firma ' + response.data.MensajeOperacion);
                        closeModal('ModalRecibirDistribucion', 1);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    closeModal('ModalRecibirDistribucion', 1);
                });
            showModal('ModalFirma');
            $('#botonFirma').click();
        };

        $scope.LimpiarFirma = function () {
            var canvas = document.getElementById('Firma');
            var context = canvas.getContext("2d");
            context.clearRect(0, 0, canvas.width, canvas.height);
        };

        $scope.CerrarModalFirma = function () {
            closeModal('ModalFirma', 1);
        };

        // function RedimVertical(img) {
        //    //Se carga el contexto vans donde se redimencionara la imagen
        //    var ctx = cargaContextoCanvas('CanvasVertical');
        //    if (ctx) {
        //        // dentro del canvaz se dibija la imagen que se recive por parametro
        //        ctx.drawImage(img, 0, 0, 499, 699);
        //    }
        //}

        //function RedimHorizontal(img) {
        //    //Se carga el contexto vans donde se redimencionara la imagen
        //    var ctx = cargaContextoCanvas('CanvasHorizontal');
        //    if (ctx) {
        //        // dentro del canvaz se dibija la imagen que se recive por parametro
        //        ctx.drawImage(img, 0, 0, 699, 499);
        //    }
        //}
        $scope.ReplicarTiemposRemesas = function () {
            for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                for (var j = 0; j < $scope.ListadoTiemposRemesas.length; j++) {
                    $scope.ListadoTiemposRemesas[j].Data[i].Fecha = angular.copy($scope.ListadoTiemposReporte[i].Fecha)
                }
            }
            ShowSuccess('Los tiempos se replicarón a todas las remesas correctamente ')
        }
        $scope.Replicarplanilla = function (item) {
            $scope.TempItem = item
        }
        $scope.ReplicarTiemposPlanilla = function () {
            var item = $scope.TempItem
            for (var i = 0; i < item.Data.length; i++) {
                $scope.ListadoTiemposReporte[i].Fecha = angular.copy(item.Data[i].Fecha)
            }
            ShowSuccess('Los tiempos se replicarón a la planilla correctamente ')
        }
        $scope.InfoContenedor = function (item) {
            showModal('modalContenedor')
            $scope.Contenedor = {}
            $scope.Contenedor = angular.copy(item)
            $scope.Contenedor.FechaDevolucion = new Date(item.FechaDevolucion)
            try {
                $scope.Contenedor.CiudadDevolucion = $scope.CargarCiudad(item.CiudadDevolucion.Codigo)
            } catch (e) {

            }
        }
        $scope.GuardarContenedor = function () {
            ShowWarningConfirm('¿Esta seguro de modificar la información de contenedor para la remesa seleccionada?', $scope.ConfirmarGuardarContenedor)
        }
        $scope.ConfirmarGuardarContenedor = function () {
            $scope.Contenedor.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            $scope.Contenedor.Codigo = 0
            $scope.Contenedor.Numero = 0
            $scope.Contenedor.ModificaContenedor = 1
            DetalleSeguimientoVehiculosFactory.Guardar($scope.Contenedor).
                then(function (response) {
                    if (response.data.Datos > 0) {
                        closeModal('modalContenedor');
                        ShowSuccess('La información se modificó correctamente')
                        FindRemesa();
                    } else {
                        ShowError('No se logro actualizar la información, por favor consulte con el administrador del sistema');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            NumeroRemesa
        }
        $scope.NewDate = function (date) {
            return new Date(date)
        }
        $scope.GestionarEntrega = function (item) {
            $scope.DeshabilitarDatosDestinatario = true;
            $scope.DeshabilitarDatosRecibe = false;
            $scope.Destinatario = {};
            $scope.Foto = [];
            $scope.FotoAgregada = 0;
            $scope.Modal = {
                FechaRecibe: new Date()
            };
            $scope.NumeroDocumentoCliente = item.DocumentoCliente;
            $scope.Codigo = item.Codigo
            $scope.MensajesErrorRecibir = [];
            $scope.MostrarModalDetalle = true;
            $scope.ListadoTipoIdentificacion = []
            $scope.itemRemesa = item
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {

                            $scope.ListadoTipoIdentificacion = response.data.Datos;

                            if ($scope.CodigoTipoIdentificacionDestinatario !== undefined && $scope.CodigoTipoIdentificacionDestinatario !== '' && $scope.CodigoTipoIdentificacionDestinatario !== null && $scope.CodigoTipoIdentificacionDestinatario !== 0) {
                                $scope.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionDestinatario);
                            } else {
                                $scope.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + CODIGO_NO_APLICA_TIPO_IDENTIFICACION);
                            }

                            if ($scope.CodigoTipoIdentificacionRecibe !== undefined && $scope.CodigoTipoIdentificacionRecibe !== '' && $scope.CodigoTipoIdentificacionRecibe !== null && $scope.CodigoTipoIdentificacionRecibe !== 0) {
                                $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRecibe);
                            } else {
                                $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + CODIGO_NO_APLICA_TIPO_IDENTIFICACION);
                            }

                        }
                        else {
                            $scope.ListadoTipoIdentificacion = []
                        }
                        ObtenerDistribucion();
                        showModal('ModalRecibirDistribucion');

                    }
                }, function (response) {
                });
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 114 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoEstados = []
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if (response.data.Datos[i].Codigo !== 11401) {
                                    $scope.ListadoEstados.push(response.data.Datos[i])
                                }
                            }
                            $scope.Modal.EstadoRecibe = $scope.ListadoEstados[0]
                        }
                    }
                }, function (response) {
                });
        }
        function ObtenerDistribucion() {

            BloqueoPantalla.start('Buscando registros ...');
            $timeout(function () {
                BloqueoPantalla.message('Espere por favor ...');
            }, 100);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: $scope.Distribucion.NumeroRemesa,
                Codigo: $scope.Codigo
            };

            DetalleDistribucionRemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.DeshabilitarDatosRecibe = false;
                        $scope.Destinatario.Producto = response.data.Datos.Producto.Nombre;
                        $scope.DestinatarioCantidad = MascaraValores(response.data.Datos.Cantidad);
                        $scope.DestinatarioPeso = MascaraValores(response.data.Datos.Peso);
                        $scope.Destinatario.Cantidad = response.data.Datos.Cantidad;
                        $scope.Destinatario.Peso = response.data.Datos.Peso;
                        $scope.CodigoTipoIdentificacionDestinatario = response.data.Datos.Destinatario.TipoIdentificacion.Codigo
                        if ($scope.ListadoTipoIdentificacion.length > 0 && $scope.CodigoTipoIdentificacionDestinatario > 0) {
                            $scope.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.Destinatario.TipoIdentificacion.Codigo);
                        }
                        $scope.Destinatario.NumeroIdentificacion = response.data.Datos.Destinatario.NumeroIdentificacion;
                        $scope.Destinatario.NombreCompleto = response.data.Datos.Destinatario.NombreCompleto;
                        $scope.Destinatario.Ciudad = response.data.Datos.Destinatario.Ciudad.Nombre;
                        if (response.data.Datos.ZonaDestinatario === undefined) {
                            $scope.Destinatario.ZonaDestinatario = '(NO APLICA)';
                        } else {
                            $scope.Destinatario.ZonaDestinatario = response.data.Datos.ZonaDestinatario.Nombre;
                        }
                        $scope.Destinatario.SitioEntrega = response.data.Datos.SitioEntrega;
                        $scope.Destinatario.Barrio = response.data.Datos.Destinatario.Barrio;
                        $scope.Destinatario.Direccion = response.data.Datos.Destinatario.Direccion;
                        $scope.Destinatario.CodigoPostal = response.data.Datos.Destinatario.CodigoPostal;
                        $scope.Destinatario.Telefonos = response.data.Datos.Destinatario.Telefonos;
                        $scope.CodigoUsuarioModifica = response.data.Datos.UsuarioModifica.Codigo;
                        if ($scope.CodigoUsuarioModifica > 0) {
                            $scope.CodigoTipoIdentificacionRecibe = response.data.Datos.CodigoTipoIdentificacionRecibe
                            if ($scope.ListadoTipoIdentificacion.length > 0 && $scope.CodigoTipoIdentificacionRecibe > 0) {
                                $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.CodigoTipoIdentificacionRecibe);
                            }
                            $scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);
                            $scope.Modal.NumeroIdentificacion = response.data.Datos.NumeroIdentificacionRecibe;
                            $scope.Modal.Cantidad = response.data.Datos.CantidadRecibe;
                            $scope.Modal.Peso = response.data.Datos.PesoRecibe;
                            $scope.Modal.NombreCompleto = response.data.Datos.NombreRecibe;
                            $scope.Modal.Telefonos = response.data.Datos.TelefonoRecibe;
                            $scope.Modal.Observaciones = response.data.Datos.ObservacionesRecibe;
                            $scope.DeshabilitarDatosRecibe = true;
                        } else {
                            $scope.CodigoTipoIdentificacionRecibe = response.data.Datos.CodigoTipoIdentificacionRecibe
                            if ($scope.ListadoTipoIdentificacion.length > 0 && $scope.CodigoTipoIdentificacionRecibe == 0) {
                                $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.Destinatario.TipoIdentificacion.Codigo);
                            }
                            $scope.Modal.NumeroIdentificacion = response.data.Datos.Destinatario.NumeroIdentificacion;
                            $scope.Modal.Cantidad = response.data.Datos.Cantidad;
                            $scope.Modal.Peso = response.data.Datos.Peso;
                            $scope.Modal.NombreCompleto = response.data.Datos.Destinatario.NombreCompleto;
                            $scope.Modal.Telefonos = response.data.Datos.Destinatario.Telefonos;
                            $scope.Modal.Observaciones = '';
                            $scope.DeshabilitarDatosRecibe = false;
                            $scope.VerificarRecibe($scope.Modal.NumeroIdentificacion);
                        }
                        if (response.data.Datos.Fotografia.FotoBits !== undefined && response.data.Datos.Fotografia.FotoBits !== null && response.data.Datos.Fotografia.FotoBits !== '') {
                            $scope.Foto = [];
                            $scope.Foto.push({
                                NombreFoto: response.data.Datos.Fotografia.NombreFoto,
                                TipoFoto: response.data.Datos.Fotografia.TipoFoto,
                                ExtensionFoto: response.data.Datos.Fotografia.ExtensionFoto,
                                FotoBits: response.data.Datos.Fotografia.FotoBits,
                                ValorDocumento: 1,
                                FotoCargada: 'data:' + response.data.Datos.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Fotografia.FotoBits
                            });
                        } else {
                            $scope.Foto = [];
                        }
                        if ($scope.Foto.length > 0) {
                            $scope.FotoAgregada = 1
                        } else {
                            $scope.FotoAgregada = 0
                        }

                        $scope.LimpiarFirmaRecibe();
                        $timeout(function () {
                            //BloqueoPantalla.message('Espere por favor ...');
                            var image = new Image();
                            image.src = "data:image/png;base64," + response.data.Datos.Firma
                            var img = cargaContextoCanvas('FirmaRecibe')
                            img.drawImage(image, 0, 0)
                        }, 150);

                    }
                    else {
                        ShowError('No se logró consultar la distribución ' + response.data.MensajeOperacion);
                        $('#ModalRecibirDistribucion').hide(100);
                        $('#ResultadoConsulta').show(100);
                        closeModal('ModalRecibirDistribucion', 1);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $('#ModalRecibirDistribucion').hide(100);
                    $('#ResultadoConsulta').show(100);
                });
            BloqueoPantalla.stop();
        };
        $scope.VerificarCantidad = function () {
            if (MascaraDecimales($scope.Modal.Cantidad) > MascaraDecimales($scope.Destinatario.Cantidad)) {
                ShowError('La cantidad recibida no puede ser mayor a la cantidad designada en el producto')
                $scope.Modal.Cantidad = $scope.Destinatario.Cantidad
            }
        }
        $scope.VerificarPeso = function () {
            if (MascaraDecimales($scope.Modal.Peso) > MascaraDecimales($scope.Destinatario.Cantidad)) {
                ShowError('El peso recibido no puede ser mayor al peso designado en el producto')
                $scope.Modal.Cantidad = $scope.Destinatario.Peso
            }
        }
        $scope.VerificarRecibe = function (identificacion) {
            $scope.ValidarIdentificacion = '';
            $scope.ValidarIdentificacion = identificacion;
            if ($scope.ValidarIdentificacion !== '') {
                if ($scope.ValidarIdentificacion === undefined || $scope.ValidarIdentificacion === null || isNaN($scope.ValidarIdentificacion) === true) {
                    ShowInfo('No hay registro de la identificación ingresada , por favor diligenciar todos los campos.');
                    $scope.Modal.NumeroIdentificacion = '';
                    $scope.Modal.NombreCompleto = '';
                    $scope.Modal.Telefonos = '';
                } else {
                    TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroIdentificacion: $scope.ValidarIdentificacion }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    if (response.data.Datos[0].Estado.Codigo === 1) {
                                        item = response.data.Datos[0];
                                        $scope.Modal.NumeroIdentificacion = item.NumeroIdentificacion;
                                        $scope.Modal.NombreCompleto = item.NombreCompleto;
                                        $scope.Modal.Telefonos = item.Telefonos;
                                    } else {
                                        $scope.Modal.NumeroIdentificacion = '';
                                        $scope.Modal.NombreCompleto = '';
                                        $scope.Modal.Telefonos = '';
                                        ShowInfo('El destinatario ingresado se encuentra inactivo en el sistema.');
                                    }
                                } else {
                                    ShowInfo('No hay registro de la identificación ingresada, por favor diligenciar todos los campos.');
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        };
        $scope.CargarImagen = function (element) {
            var continuar = true;
            $scope.TipoImagen = element
            $scope.NombreDocumento = element.files[0].name
            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato JPEG, PNG", false);
                continuar = false;
            }

            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png') {
                    ShowError("Solo se pueden cargar archivos en formato JPEG, PNG ", false);
                    continuar = false;
                }
            }

            if (continuar == true) {
                var reader = new FileReader();
                reader.onload = $scope.AsignarImagenListado;
                reader.readAsDataURL(element.files[0]);
            }

        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.AsignarImagenListado = function (e) {
            try {
                RedimHorizontal('')
                RedimVertical('')
            } catch (e) {
            }
            $scope.$apply(function () {
                $scope.Convertirfoto = null
                var TipoImagen = ''

                var name = $scope.TipoImagen.files[0].name.split('.')

                $scope.NombreFoto = name[0]

                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/jpeg'
                        AsignarFotoListado()

                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/png'
                        AsignarFotoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.Convertirfoto = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoFoto = 'application/pdf'
                    $scope.TipoImagen = TipoImagen
                    AsignarFotoListado()

                }
                var input = document.getElementById('inputfile')
                input.value = ''
            });
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.AsignarFoto = function () {
            closeModal('ModalTomarFoto', 1)
            $scope.video.srcObject = undefined
            $scope.Foto = [];
            $scope.FotoAux = $scope.FotoTomada.toString();
            $scope.FotoBits = $scope.FotoAux.replace(/data:image\/png;base64,/g, '')
            $scope.Foto.push({
                NombreFoto: 'Fotografía',
                TipoFoto: 'image/png',
                ExtensionFoto: 'PNG',
                FotoBits: $scope.FotoBits,
                ValorDocumento: 1,
                FotoCargada: $scope.FotoAux
            });
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        function AsignarFotoListado() {
            $scope.Foto = [];
            $scope.Foto.push({
                NombreFoto: $scope.NombreFoto,
                TipoFoto: $scope.TipoFoto,
                ExtensionFoto: $scope.TipoImagen,
                FotoBits: $scope.Convertirfoto,
                ValorDocumento: 1,
                FotoCargada: 'data:' + $scope.TipoFoto + ';base64,' + $scope.Convertirfoto
            });
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.GuardarFoto = function () {
            if ($scope.ModificarFoto == true && $scope.Foto.length == CERO) {
                closeModal('ModalFoto', 1);
            }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.LimpiarFirmaRecibe = function () {
            try {
                var canvas = document.getElementById('FirmaRecibe');
                var context = canvas.getContext('2d')
                context.clearRect(0, 0, canvas.width, canvas.height);
            } catch (e) {

            }

        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }

        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.EliminarFoto = function (item) {
            item = [];
            $scope.Foto = []
            $scope.ModificarFoto = true;
            //$("#Foto").attr("src", "");
        }
        $scope.ConfirmacionGuardarRecibe = function () {
            if (DatosRequeridosRecibe()) {
                ShowConfirm('¿Está seguro de guardar la información?', $scope.GuardarRecibe)
                //showModal('modalConfirmacionGuardar');
            }
        };
        function DatosRequeridosRecibe() {
            $scope.MensajesErrorRecibir = [];
            var continuar = true;

            if ($scope.Modal.Cantidad === undefined || $scope.Modal.Cantidad === '' || $scope.Modal.Cantidad === null || $scope.Modal.Cantidad === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if ($scope.Modal.Peso === undefined || $scope.Modal.Peso === '' || $scope.Modal.Peso === null || $scope.Modal.Peso === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el peso');
                continuar = false;
            }
            if ($scope.Modal.TipoIdentificacionRecibe === undefined || $scope.Modal.TipoIdentificacionRecibe === '' || $scope.Modal.TipoIdentificacionRecibe === null || $scope.Modal.TipoIdentificacionRecibe.Codigo === CODIGO_NO_APLICA_TIPO_IDENTIFICACION) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el tipo de identificación');
                continuar = false;
            }
            if ($scope.Modal.NumeroIdentificacion === undefined || $scope.Modal.NumeroIdentificacion === '' || $scope.Modal.NumeroIdentificacion === null || $scope.Modal.NumeroIdentificacion === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el número de identificación');
                continuar = false;
            }
            if ($scope.Modal.NombreCompleto === undefined || $scope.Modal.NombreCompleto === '' || $scope.Modal.NombreCompleto === null || $scope.Modal.NombreCompleto === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el nombre de la persona que recibe');
                continuar = false;
            }
            if ($scope.Modal.Telefonos === undefined || $scope.Modal.Telefonos === '' || $scope.Modal.Telefonos === null || $scope.Modal.Telefonos === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el teléfono');
                continuar = false;
            }
            if ($scope.Modal.EstadoRecibe.Codigo == 11403 && ($scope.Modal.Observaciones === undefined || $scope.Modal.Observaciones === '' || $scope.Modal.Observaciones === null || $scope.Modal.Observaciones === 0)) {
                $scope.MensajesErrorRecibir.push('Debe ingresar las observaciones');
                continuar = false;
            }
            return continuar;
        }
        $scope.GuardarRecibe = function () {
            //closeModal('modalConfirmacionGuardar', 1);

            var canvas = document.getElementById('FirmaRecibe');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            $scope.Firma = image.src.replace(/data:image\/png;base64,/g, '');

            $scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);

            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                NumeroRemesa: $scope.Distribucion.NumeroRemesa,
                //FechaRecibe: $scope.Modal.FechaRecibe,
                CantidadRecibe: $scope.Modal.Cantidad,
                PesoRecibe: $scope.Modal.Peso,
                CodigoTipoIdentificacionRecibe: $scope.Modal.TipoIdentificacionRecibe.Codigo,
                NumeroIdentificacionRecibe: $scope.Modal.NumeroIdentificacion,
                NombreRecibe: $scope.Modal.NombreCompleto,
                TelefonoRecibe: $scope.Modal.Telefonos,
                ObservacionesRecibe: $scope.Modal.Observaciones,
                Fotografia: $scope.Foto[0],
                Firma: $scope.Firma,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                EstadoDistribucionRemesas: $scope.Modal.EstadoRecibe
            };

            DetalleDistribucionRemesasFactory.Guardar($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('La entrega de la remesa se guardo correctamente');
                            closeModal('ModalRecibirDistribucion', 1);
                            $scope.AbrirDistribucion($scope.itemRemesaDistribucion)
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };
        //------------CAMBIAR RUTA

        $scope.CambioRuta = function (item) {
            showModal('ModalGestionarCambioRuta'); 
            $scope.ModelCamRut = {
                NumeroDocumentoPlanilla: item.NumeroDocumentoPlanilla,
                NumeroPlanilla: item.NumeroPlanilla,
                Vehiculo: item.Vehiculo.Placa,
                Origen: item.strOrigen,
                Destino: item.strDestino,
                RutaActual: item.Ruta.Nombre,
                ReporteSeguimiento: item.SitioReporteSeguimiento.Nombre,
                CodigoSeguimiento: item.SitioReporteSeguimiento.Codigo

            }


        }

        $scope.ConfirmacionActualizarRuta = function () {
            if ($scope.ModelCamRut.CodigoSeguimiento != 8209) { 

                console.log("envianod", $scope.ModelCamRut);

                var filtros = {
                    UsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Ruta: $scope.ModelCamRut.Ruta,
                    NumeroPlanilla: $scope.ModelCamRut.NumeroPlanilla

                }

                DetalleSeguimientoVehiculosFactory.CambiarRutaSeguimiento(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso) {
                            closeModal('ModalGestionarCambioRuta');
                            ShowSuccess('Se Actualizo la ruta ' + $scope.ModelCamRut.Ruta.Nombre + ' En la planilla ' + $scope.ModelCamRut.NumeroDocumentoPlanilla)
                            Find();

                        }
                        console.log(response);

                    }, function (response) {
                        BloqueoPantalla.stop();
                        ShowError(response.statusText);
                    });
            } else {
                ShowError('no se puede actualizar rutas  Finalizadas ');
            }

        };

        //--------Gestionar Orden Cargue(Cargue y Descargue)-----------//
        $scope.GestionarCargueDescargueOC = function (item) {
            var ExisteConsulta = false;
            var objConsulta = {};
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroPlanilla: item.NumeroDocumentoPlanilla,
                NumeroManifiesto: item.NumeroDocumentoManifiesto,
                Estado: ESTADO_DEFINITIVO,
                AplicaConsultaMaster: 1,
                Sync: true
            };
            var respoCon = OrdenCargueFactory.Consultar(filtro);
            if (respoCon.ProcesoExitoso) {
                if (respoCon.Datos.length > 0) {
                    ExisteConsulta = true;
                    objConsulta = respoCon.Datos[0];
                }
                else {
                    ShowError("No se encontro la orden de cargue del seguimiento");
                }
            }
            else {
                ShowError("No se encontro la orden de cargue del seguimiento");
            }
            if (ExisteConsulta) {
                var response = OrdenCargueFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: objConsulta.Numero,
                    Sync: true
                });
                if (response.ProcesoExitoso) {
                    if (response.Datos.Numero > 0) {
                        $scope.OrdenCargueObj = response.Datos;
                        $scope.ClienteOrdenCargue = response.Datos.TerceroCliente;
                        $scope.ModelOc = {
                            NumeroDocumentoPlanilla: item.NumeroDocumentoPlanilla,
                            Vehiculo: item.Vehiculo.Placa,
                            Origen: item.strOrigen,
                            Destino: item.strDestino,
                            FechaCargue: new Date(response.Datos.FechaCargue),
                            CiudadCargue: response.Datos.CiudadCargue,
                            SitioCargue: response.Datos.SitioCargue,
                            DireccionCargue: response.Datos.DireccionCargue,
                            TelefonoCargue: response.Datos.TelefonosCargue,
                            ContactoCargue: response.Datos.ContactoCargue,
                            FechaDescargue: new Date(response.Datos.FechaDescargue),
                            CiudadDescargue: response.Datos.CiudadDescargue,
                            SitioDescargue: response.Datos.SitioDescargue,
                            DireccionDescargue: response.Datos.DireccionDescargue,
                            TelefonoDescargue: response.Datos.TelefonoDescargue,
                            ContactoDescargue: response.Datos.ContactoDescargue,
                            FechaModifica: response.Datos.FechaModifica == FORMATO_FECHA_EXCEPCION_BD ? "" : new Date(response.Datos.FechaModifica),
                            UsuarioModifica: response.Datos.CodigoUsuarioModifica.Nombre
                        };
                        //--Cargue
                        $scope.ModelOc.SitioCargue.DireccionSitio = $scope.ModelOc.DireccionCargue;
                        $scope.ModelOc.SitioCargue.Telefono = $scope.ModelOc.TelefonoCargue;
                        $scope.ModelOc.SitioCargue.Contacto = $scope.ModelOc.ContactoCargue;
                        $scope.ModelOc.SitioCargue.SitioCliente = {
                            Codigo: $scope.ModelOc.SitioCargue.Codigo,
                            Nombre: $scope.ModelOc.SitioCargue.Nombre,
                            DireccionSitio: $scope.ModelOc.SitioCargue.DireccionSitio,
                            Telefono: $scope.ModelOc.SitioCargue.Telefono,
                            Contacto: $scope.ModelOc.SitioCargue.Contacto
                        };
                        //--Cargue
                        //--Descargue
                        $scope.ModelOc.SitioDescargue.DireccionSitio = $scope.ModelOc.DireccionDescargue;
                        $scope.ModelOc.SitioDescargue.Telefono = $scope.ModelOc.TelefonoDescargue;
                        $scope.ModelOc.SitioDescargue.Contacto = $scope.ModelOc.ContactoDescargue;
                        $scope.ModelOc.SitioDescargue.SitioCliente = {
                            Codigo: $scope.ModelOc.SitioDescargue.Codigo,
                            Nombre: $scope.ModelOc.SitioDescargue.Nombre,
                            DireccionSitio: $scope.ModelOc.SitioDescargue.DireccionSitio,
                            Telefono: $scope.ModelOc.SitioDescargue.Telefono,
                            Contacto: $scope.ModelOc.SitioDescargue.Contacto
                        };
                        //--Descargue
                        showModal("ModalGestionarCargueDescargue");
                    }
                    else {
                        ShowError("No se encontro la orden de cargue del seguimiento");
                    }
                }
                else {
                    ShowError("No se encontro la orden de cargue del seguimiento");
                }
            }
        };
        $scope.ConfirmacionGuardarCargueDescargue = function () {
            if (DatosRequeridosCargueDescargue()) {
                showModal("ModalConfirmacionGuardarCargueDescargue");
            }
        };
        $scope.GuardarCargueDescargue = function () {
            closeModal("ModalConfirmacionGuardarCargueDescargue");
            var ordencargueobj = {
                Numero: $scope.OrdenCargueObj.Numero,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
               
                CiudadCargue: { Codigo: $scope.ModelOc.CiudadCargue.Codigo },
                SitioCargue: { Codigo: $scope.ModelOc.SitioCargue.SitioCliente.Codigo},
                FechaCargue: $scope.ModelOc.FechaCargue,
                DireccionCargue: $scope.ModelOc.DireccionCargue,
                TelefonosCargue: $scope.ModelOc.TelefonoCargue,
                ContactoCargue: $scope.ModelOc.ContactoCargue,

                CiudadDescargue: { Codigo: $scope.ModelOc.CiudadDescargue.Codigo },
                SitioDescargue: { Codigo: $scope.ModelOc.SitioDescargue.SitioCliente.Codigo },
                FechaDescargue: $scope.ModelOc.FechaDescargue,
                DireccionDescargue: $scope.ModelOc.DireccionDescargue,
                TelefonoDescargue: $scope.ModelOc.TelefonoDescargue,
                ContactoDescargue: $scope.ModelOc.ContactoDescargue,

                CodigoUsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Sync: true
            };
            var response = OrdenCargueFactory.ActualizarCargueDescargue(ordencargueobj);
            if (response.ProcesoExitoso) {
                if (response.Datos > 0) {
                    ShowSuccess('Se modificó el cargue y descargue de la orden de cargue No. ' + response.Datos);
                    closeModal("ModalGestionarCargueDescargue");
                }
                else {
                    ShowError("No se actualizar el cargue y descargue");
                }
            }
            else {
                ShowError("No se actualizar el cargue y descargue");
            }
        };
        function DatosRequeridosCargueDescargue() {
            var continuar = true;
            $scope.MensajesErrorCargueDescargue = [];
            var model = $scope.ModelOc;
            if (model.FechaCargue == undefined || model.FechaCargue == null || model.FechaCargue == "") {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar fecha Cargue');
                continuar = false;
            }
            if (model.CiudadCargue == undefined || model.CiudadCargue == null || model.CiudadCargue == "" || model.CiudadCargue.Codigo == 0) {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar la ciudad de cargue');
                continuar = false;
            }
            if (model.SitioCargue == undefined || model.SitioCargue == null || model.SitioCargue == "" || model.SitioCargue.Codigo == 0) {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar sitio de cargue');
                continuar = false;
            }
            if (model.DireccionCargue == undefined || model.DireccionCargue == null || model.DireccionCargue == "") {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar la dirección de cargue');
                continuar = false;
            }
            if (model.TelefonoCargue == undefined || model.TelefonoCargue == null || model.TelefonoCargue == "") {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar teléfono de cargue');
                continuar = false;
            }
            if (model.ContactoCargue == undefined || model.ContactoCargue == null || model.ContactoCargue == "") {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar contacto de cargue');
                continuar = false;
            }

            if (model.FechaDescargue == undefined || model.FechaDescargue == null || model.FechaDescargue == "") {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar fecha descargue');
                continuar = false;
            }
            if (model.CiudadDescargue == undefined || model.CiudadDescargue == null || model.CiudadDescargue == "" || model.CiudadDescargue.Codigo == 0) {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar la ciudad de descargue');
                continuar = false;
            }
            if (model.SitioDescargue == undefined || model.SitioDescargue == null || model.SitioDescargue == "" || model.SitioDescargue.Codigo == 0) {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar sitio de descargue');
                continuar = false;
            }
            if (model.DireccionDescargue == undefined || model.DireccionDescargue == null || model.DireccionDescargue == "") {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar la dirección de descargue');
                continuar = false;
            }
            if (model.TelefonoDescargue == undefined || model.TelefonoDescargue == null || model.TelefonoDescargue == "") {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar teléfono de descargue');
                continuar = false;
            }
            if (model.ContactoDescargue == undefined || model.ContactoDescargue == null || model.ContactoDescargue == "") {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar contacto de descargue');
                continuar = false;
            }
            return continuar;
        }

        function SugerirTiemposCargueDescargue() {
            var FechaCreaPlanilla = new Date($scope.ModalDespachos.FechaCrea)
            var Ruta = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.ModalDespachos.Ruta.Codigo, Sync: true }).Datos

            $scope.ListadoTiemposReporte.forEach(item => {
                if (item.Codigo == 8205) {
                    item.Fecha = new Date(moment($scope.ModalDespachos.FechaCrea).format())
                } else if (item.Codigo == 8204) {
                    item.Fecha = new Date(moment($scope.ModalDespachos.FechaCrea).subtract(2, 'hours').format())
                } else if (item.Codigo == 8203) {
                    item.Fecha = new Date(moment($scope.ModalDespachos.FechaCrea).subtract(2.5, 'hours').format())
                } else if (item.Codigo == 8209) {
                    item.Fecha = new Date(moment($scope.ModalDespachos.FechaCrea).add(Ruta.DuracionHoras, 'hours').format())
                } else if (item.Codigo == 8208) {
                    item.Fecha = new Date(moment($scope.ModalDespachos.FechaCrea).add((Ruta.DuracionHoras - 2), 'hours').format())
                } else if (item.Codigo == 8207) {
                    item.Fecha = new Date(moment($scope.ModalDespachos.FechaCrea).add((Ruta.DuracionHoras - 2.5), 'hours').format())
                } else {
                    item.Fecha = new Date(moment($scope.ModalDespachos.FechaCrea).format())
                }
            })
        }

        //--------Gestionar Orden Cargue(Cargue y Descargue)-----------//
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };

    }]);