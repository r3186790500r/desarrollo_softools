﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" SeccionFormularioInspecciones"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class SeccionFormularioInspecciones
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SeccionFormularioInspecciones"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SeccionFormularioInspecciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoFormulario = Read(lector, "ENFI_Codigo")
            Orden = Read(lector, "Orden_Formulario")
            EstadoDocumento = New ValorCatalogos With {.Codigo = Read(lector, "Estado")}
        End Sub
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property CodigoFormulario As Integer
        <JsonProperty>
        Public Property ListadoItemSeccion As IEnumerable(Of ItemFormularioInspecciones)
        <JsonProperty>
        Public Property Orden As Integer
        <JsonProperty>
        Public Property EstadoDocumento As ValorCatalogos
    End Class
End Namespace
