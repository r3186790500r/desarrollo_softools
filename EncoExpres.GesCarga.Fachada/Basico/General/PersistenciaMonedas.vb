﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General
Namespace Basico.General
    Public NotInheritable Class PersistenciaMonedas
        Implements IPersistenciaBase(Of Monedas)

        Public Function Consultar(filtro As Monedas) As IEnumerable(Of Monedas) Implements IPersistenciaBase(Of Monedas).Consultar
            Return New RepositorioMonedas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Monedas) As Long Implements IPersistenciaBase(Of Monedas).Insertar
            Return New RepositorioMonedas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Monedas) As Long Implements IPersistenciaBase(Of Monedas).Modificar
            Return New RepositorioMonedas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Monedas) As Monedas Implements IPersistenciaBase(Of Monedas).Obtener
            Return New RepositorioMonedas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Monedas) As Boolean
            Return New RepositorioMonedas().Anular(entidad)
        End Function
        Public Function MonedaLocals(entidad As Monedas) As Boolean
            Return New RepositorioMonedas().MonedaLocals(entidad)
        End Function
    End Class

End Namespace

