﻿PRINT 'gsp_insertar_detalle_intento_entrega_guia'
GO
DROP PROCEDURE gsp_insertar_detalle_intento_entrega_guia
GO
CREATE PROCEDURE gsp_insertar_detalle_intento_entrega_guia
(
	@par_EMPR_Codigo SMALLINT,
	@par_ENRE_Numero NUMERIC,
	@par_Observaciones VARCHAR(250),
	@par_USUA_Codigo_Crea SMALLINT,
    @par_Novedad_Intento_Entrega VARCHAR(250)
)
AS
BEGIN
	INSERT INTO
		Detalle_Intentos_Entrega_Guias
    (
    EMPR_Codigo, 
    ENRE_Numero, 
    ID, 
    Observaciones, 
    USUA_Codigo_Crea,
    Fecha_Crea,
    Novedad_Intento_Entrega)
	  VALUES (
    @par_EMPR_Codigo,
    @par_ENRE_Numero, 
    (SELECT COUNT(0) + 1 FROM Detalle_Intentos_Entrega_Guias WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero), 
    @par_Observaciones, 
    @par_USUA_Codigo_Crea, 
    GETDATE(),
    @par_Novedad_Intento_Entrega)

SELECT MAX(ID) AS ID FROM Detalle_Intentos_Entrega_Guias WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero
END
GO
