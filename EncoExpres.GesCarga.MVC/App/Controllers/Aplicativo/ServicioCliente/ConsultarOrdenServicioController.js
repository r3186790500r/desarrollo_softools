﻿EncoExpresApp.controller("ConsultarOrdenServicioCtrl", ['$scope', '$timeout', 'EncabezadoSolicitudOrdenServiciosFactory', '$linq', 'blockUI', '$routeParams', 'TercerosFactory', 'EmpresasFactory', 'ValorCatalogosFactory',
    function ($scope, $timeout, EncabezadoSolicitudOrdenServiciosFactory, $linq, blockUI, $routeParams, TercerosFactory, EmpresasFactory, ValorCatalogosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Servicio al Cliente' }, { Nombre: 'Documentos' }, { Nombre: 'Orden Servicio' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.ModeloFechaInicial = new Date();
        $scope.ModeloFechaFinal = new Date();
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.ListadoOrdenServicios = [];
        $scope.MostrarMensajeError = false
        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.ListadoEstadoSolicitud = []
        $scope.ModalEstadoSolicitus = { Codigo: 0 };
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ORDEN_SERVICIO); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_TRAMITE_SOLICITUD } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEstadoSolicitud = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoEstadoSolicitud.push({ Codigo: 0, Nombre: '(TODAS)' });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoEstadoSolicitud.push(response.data.Datos[i]);

                        }
                        $scope.ModalEstadoSolicitus = $linq.Enumerable().From($scope.ListadoEstadoSolicitud).First('$.Codigo == ' + CERO);
                    }
                    else {
                        $scope.ListadoEstadoSolicitud = []
                    }
                }
            }, function (response) {
            });


        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_ORDEN_SERVICIO;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref + '&TipoDocumento=' + CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref + '&TipoDocumento=' + CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento != undefined && $scope.NumeroDocumento != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }

        }

        /*Consulta Clientes*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCliente = []
                    if (response.data.Datos.length > CERO) {
                        response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                        $scope.ListadoClientes = response.data.Datos;
                    }
                    else {
                        $scope.ListadoCliente = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarOrdenservicio';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    $scope.Codigo = 0
                    Find()
                }
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoOrdenServicios = [];
            if ($scope.Buscando) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Numero,
                    NumeroDocumento: $scope.ModeloNumero,
                    FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaInicial,
                    FechaFinal: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaFinal,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                    Estado: $scope.Estado.Codigo,
                    Cliente: $scope.Cliente,
                    Pagina: $scope.paginaActual,
                    EstadoOrden: { Codigo: $scope.ModalEstadoSolicitus.Codigo },
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                }
                blockUI.delay = 1000;
                EncabezadoSolicitudOrdenServiciosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoOrdenServicios = response.data.Datos
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.stuatusText);
                    });
            }
            blockUI.stop();
            $scope.Numero = '';
        }

        $scope.AnularOrdenServicio = function (Numero, NumeroDocumento) {
            $scope.Numero = Numero

            $scope.ModalErrorCompleto = ''
            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnularOrdenServicio');
        };

        $scope.Anular = function () {
            var aplicaordencargue = false
            var aplicaremesa = false
            var aplicaplanilla = false
            var aplicamanifiesto = false
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.ModeloCausaAnula,
                Numero: $scope.Numero,

            };
            if (DatosRequeridosAnular()) {
                EncabezadoSolicitudOrdenServiciosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló la orden de servicio ' + $scope.NumeroDocumento);
                                closeModal('modalAnularOrdenServicio');
                                
                                Find();
                               
                            } else {
                                var mensaje = 'No se pudo anular la orden de servicio ya que se encuentra relacionado con los siguientes documentos: '
                                var remesas = '\nRemesas: '
                                var Ordencargues = '\nOrdenes de Cargue: '
                                var Manifiestos = '\nManifiestos: '
                                var Planillas = '\nPlanillas: '
                                //var Programacion = '\nPlanillas: '
                                for (var i = 0; i < response.data.Datos.DocumentosRelacionados.length; i++) {
                                    if (response.data.Datos.DocumentosRelacionados[i].OrdenCargue.Numero > 0) {
                                        Ordencargues += response.data.Datos.DocumentosRelacionados[i].OrdenCargue.Numero + ','
                                        aplicaordencargue = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Remesa.Numero > 0) {
                                        remesas += response.data.Datos.DocumentosRelacionados[i].Remesa.Numero + ','
                                        aplicaremesa = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Planilla.Numero > 0) {
                                        Planillas += response.data.Datos.DocumentosRelacionados[i].Planilla.Numero + ','
                                        aplicaplanilla = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero > 0) {
                                        Manifiestos += response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero + ','
                                        aplicamanifiesto = true
                                    }
                                }
                                if (aplicaordencargue) {
                                    mensaje += Ordencargues
                                }
                                if (aplicaremesa) {
                                    mensaje += remesas
                                }
                                if (aplicaplanilla) {
                                    mensaje += Planillas
                                }
                                if (aplicamanifiesto) {
                                    mensaje += Manifiestos
                                }
                                ShowError(mensaje)
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            }


        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            $scope.ModeloNumero = $routeParams.Numero;
            $scope.Cliente = { NombreCompleto: '', Codigo: 0 }
            Find();
        }

        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '')
                && ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '')
                && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                    if ($scope.ModeloFechaInicial < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.ModeloFechaInicial - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }
                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaInicial
                    }
                }
            }
            return continuar
        }

        $scope.ConfirmacionDuplicarOrdenServicio = function (item) {
            $scope.itemActual = item
            showModal('modalDuplicarOrdenServicio')
        }

        $scope.DuplicarOrdenServicio = function (ordenServicio) {
            if (ordenServicio != undefined && ordenServicio != null) {
                var RordenServicio = EncabezadoSolicitudOrdenServiciosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: ordenServicio.Numero, TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO, Sync: true }).Datos
                if (RordenServicio != undefined) {

                    RordenServicio.Estado = ESTADO_BORRADOR
                    RordenServicio.Numero = 0
                    RordenServicio.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    RordenServicio.Sync = true
                    if (RordenServicio.TipoDespacho.Codigo != VALOR_CATALOGO_TIPO_DESPACHO.VIAJES) {
                        RordenServicio.Detalle = [];
                    }
                    var RInsertar = EncabezadoSolicitudOrdenServiciosFactory.Guardar(RordenServicio)
                    if (RInsertar != undefined) {
                        if (RInsertar.ProcesoExitoso == true) {
                            if (RInsertar.Datos > 0) {
                                ShowSuccess('Se generó la orden de servicio No. ' + RInsertar.Datos)
                                Find();
                                closeModal('modalDuplicarOrdenServicio')
                            }
                        }
                    }

                }
            }
        }

        $scope.MaskMayus = function () {
            try { $scope.Cliente = $scope.Cliente.toUpperCase() } catch (e) { }
        };

    }]);