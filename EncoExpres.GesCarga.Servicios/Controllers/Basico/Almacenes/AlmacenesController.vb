﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Negocio.Basico.Almacen

''' <summary>
''' Controlador <see cref="AlmacenesController"/>
''' </summary>
<Authorize>
Public Class AlmacenesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Almacenes) As Respuesta(Of IEnumerable(Of Almacenes))
        Return New LogicaAlmacenes(CapaPersistenciaAlmacenes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Almacenes) As Respuesta(Of Almacenes)
        Return New LogicaAlmacenes(CapaPersistenciaAlmacenes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Almacenes) As Respuesta(Of Long)
        Return New LogicaAlmacenes(CapaPersistenciaAlmacenes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Almacenes) As Respuesta(Of Boolean)
        Return New LogicaAlmacenes(CapaPersistenciaAlmacenes).Anular(entidad)
    End Function


End Class
