﻿/// <reference path="../controllers/portal/paqueteria/documentos/planillaguias/consultarplanillaguiasportalcontroller.js" />
/// <reference path="../controllers/portal/paqueteria/documentos/planillaguias/consultarplanillaguiasportalcontroller.js" />
var EncoExpresApp = angular.module('EncoExpresApp', ['ngRoute', 'ui.bootstrap', 'ngSignaturePad', 'blockUI', 'angular-linq', 'ui.mask', 'LocalStorageModule', 'angularSoap']);

EncoExpresApp.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorFactory');
});

EncoExpresApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.

            when('/Mapa', {
                templateUrl: 'Mapa.html',
                controller: 'MapaCtrl'
            }).
            //------------------------------------------//
            //Basico-General
            //Empresas
            when('/ConsultarEmpresas', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarEmpresas.html',
                controller: 'ConsultarEmpresasCtrl'
            }).
            when('/ConsultarEmpresas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarEmpresas.html',
                controller: 'ConsultarEmpresasCtrl'
            }).
            when('/GestionarEmpresas', {
                templateUrl: 'Aplicativo/Basico/General/GestionarEmpresas.html',
                controller: 'ConsultarEmpresasCtrl'
            }).
            when('/GestionarEmpresas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarEmpresas.html',
                controller: 'ConsultarEmpresasCtrl'
            }).
            //Terceros
            when('/ConsultarTerceros', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarTerceros.html',
                controller: 'ConsultarTercerosCtrl'
            }).
            when('/ConsultarTerceros/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarTerceros.html',
                controller: 'ConsultarTercerosCtrl'
            }).
            when('/GestionarTerceros', {
                templateUrl: 'Aplicativo/Basico/General/GestionarTerceros.html',
                controller: 'GestionarTercerosCtrl'
            }).
            when('/GestionarTerceros/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarTerceros.html',
                controller: 'GestionarTercerosCtrl'
            }).
            //Oficinas
            when('/ConsultarOficinas', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarOficinas.html',
                controller: 'ConsultarOficinasCtrl'
            }).
            when('/ConsultarOficinas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarOficinas.html',
                controller: 'ConsultarOficinasCtrl'
            }).
            when('/GestionarOficinas', {
                templateUrl: 'Aplicativo/Basico/General/GestionarOficinas.html',
                controller: 'GestionarOficinasCtrl'
            }).
            when('/GestionarOficinas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarOficinas.html',
                controller: 'GestionarOficinasCtrl'
            }).
            //Puertos Paises
            when('/ConsultarPuertosPaises', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarPuertosPaises.html',
                controller: 'ConsultarPuertosPaisesCtrl'
            }).
            when('/ConsultarPuertosPaises/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarPuertosPaises.html',
                controller: 'ConsultarPuertosPaisesCtrl'
            }).
            when('/GestionarPuertosPaises', {
                templateUrl: 'Aplicativo/Basico/General/GestionarPuertosPaises.html',
                controller: 'GestionarPuertosPaisesCtrl'
            }).
            when('/GestionarPuertosPaises/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarPuertosPaises.html',
                controller: 'GestionarPuertosPaisesCtrl'
            }).
            //Formatos Inspecciones
            when('/ConsultarFormularioInspecciones', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarFormularioInspecciones.html',
                controller: 'ConsultarFormularioInspeccionesCtrl'
            }).
            when('/ConsultarFormularioInspecciones/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarFormularioInspecciones.html',
                controller: 'ConsultarFormularioInspeccionesCtrl'
            }).
            when('/GestionarFormularioInspecciones', {
                templateUrl: 'Aplicativo/Basico/General/GestionarFormularioInspecciones.html',
                controller: 'GestionarFormularioInspeccionesCtrl'
            }).
            when('/GestionarFormularioInspecciones/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarFormularioInspecciones.html',
                controller: 'GestionarFormularioInspeccionesCtrl'
            }).
            //Catalogos
            when('/ConsultarCatalogos', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarCatalogos.html',
                controller: 'ConsultarCatalogosCtrl'
            }).
            when('/ConsultarCatalogos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarCatalogos.html',
                controller: 'ConsultarCatalogosCtrl'
            }).
            when('/GestionarCatalogos', {
                templateUrl: 'Aplicativo/Basico/General/GestionarCatalogos.html',
                controller: 'GestionarCatalogosCtrl'
            }).
            when('/GestionarCatalogos/:CodigoCatalogo/:Codigo/:Consecutivo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarCatalogos.html',
                controller: 'GestionarCatalogosCtrl'
            }).

            //Ciudades
            when('/ConsultarCiudades', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarCiudades.html',
                controller: 'ConsultarCiudadesCtrl'
            }).
            when('/ConsultarCiudades/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarCiudades.html',
                controller: 'ConsultarCiudadesCtrl'
            }).
            when('/GestionarCiudades', {
                templateUrl: 'Aplicativo/Basico/General/GestionarCiudades.html',
                controller: 'GestionarCiudadesCtrl'
            }).
            when('/GestionarCiudades/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarCiudades.html',
                controller: 'GestionarCiudadesCtrl'
            }).

            //CódigosMunicipiosDane:
            when('/ConsultarCodigosMunicipiosDane', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarCodigosMunicipiosDane.html',
                controller: 'ConsultarCodigosMunicipiosDaneCtrl'
            }).
            when('/ConsultarCodigosMunicipiosDane/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarCodigosMunicipiosDane.html',
                controller: 'ConsultarCodigosMunicipiosDaneCtrl'
            }).
            when('/GestionarCodigosMunicipiosDane', {
                templateUrl: 'Aplicativo/Basico/General/GestionarCodigosMunicipiosDane.html',
                controller: 'GestionarCodigosMunicipiosDaneCtrl'
            }).
            when('/GestionarCodigosMunicipiosDane/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarCodigosMunicipiosDane.html',
                controller: 'GestionarCodigosMunicipiosDaneCtrl'
            }).

            //Moneda y TRM 

            when('/ConsultarMonedas', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarMoneda.html',
                controller: 'ConsultarMonedaCtrl'
            }).

            when('/ConsultarMonedas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarMoneda.html',
                controller: 'ConsultarMonedaCtrl'
            }).

            when('/GestionarMoneda', {
                templateUrl: 'Aplicativo/Basico/General/GestionarMoneda.html',
                controller: 'GestionarMonedaCtrl'
            }).
            when('/GestionarMoneda/:Local', {
                templateUrl: 'Aplicativo/Basico/General/GestionarMoneda.html',
                controller: 'GestionarMonedaCtrl'
            }).
            when('/GestionarMoneda/:Codigo/:Local', {
                templateUrl: 'Aplicativo/Basico/General/GestionarMoneda.html',
                controller: 'GestionarMonedaCtrl'
            }).

            when('/GestionarTRM/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarTRM.html',
                controller: 'GestionarTRMCtrl'
            }).

            //Impuestos 
            when('/ConsultarImpuestos', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarImpuestos.html',
                controller: 'ConsultarImpuestosCtrl'
            }).
            when('/ConsultarImpuestos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarImpuestos.html',
                controller: 'ConsultarImpuestosCtrl'
            }).

            when('/GestionarImpuesto', {
                templateUrl: 'Aplicativo/Basico/General/GestionarImpuestos.html',
                controller: 'GestionarImpuestoCtrl'
            }).
            when('/GestionarImpuesto/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarImpuestos.html',
                controller: 'GestionarImpuestoCtrl'
            }).
            //Listados Basico  
            when('/ConsultarListadoBasico', {
                templateUrl: 'Aplicativo/Basico/Listados/ConsultarListadoBasico.html',
                controller: 'ConsultarListadoBasicoCtrl'
            }).

            //Tipo Documentos 
            when('/ConsultarTipoDocumentos', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarTipoDocumentos.html',
                controller: 'ConsultarTipoDocumentoCtrl'
            }).
            when('/ConsultarTipoDocumentos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarTipoDocumentos.html',
                controller: 'ConsultarTipoDocumentoCtrl'
            }).
            when('/GestionarTipoDocumentos', {
                templateUrl: 'Aplicativo/Basico/General/GestionarTipoDocumentos.html',
                controller: 'GestionarTipoDocumentoCtrl'
            }).
            when('/GestionarTipoDocumentos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarTipoDocumentos.html',
                controller: 'GestionarTipoDocumentoCtrl'
            }).
            //ConceptosLiquidacion 
            when('/ConsultarConceptosLiquidacion', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarConceptoLiquidacion.html',
                controller: 'ConsultarConceptosLiquidacionCrtr'
            }).
            when('/ConsultarConceptosLiquidacion/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarConceptoLiquidacion.html',
                controller: 'ConsultarConceptosLiquidacionCrtr'
            }).
            when('/GestionarConceptosLiquidacion', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarConceptoLiquidacion.html',
                controller: 'GestionarConceptosLiquidacionCtrl'
            }).
            when('/GestionarConceptosLiquidacion/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarConceptoLiquidacion.html',
                controller: 'GestionarConceptosLiquidacionCtrl'
            }).

            ///// Valor Combustible/////
            when('/ConsultarValorCombustible', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultaValorCombustible.html',
                controller: 'ConsultarValorCombustibleCtrl'
            }).

            when('/ConsultarValorCombustible/:Numero', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultaValorCombustible.html',
                controller: 'ConsultarValorCombustibleCtrl'
            }).

            when('/GestionarValorCombustible', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarValorCombustible.html',
                controller: 'GestionarValorCombustibleCtrl'
            }).
            //Combinacion CVT
            when('/ConsultarCombinacionesCvt', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarCombinacionesCvt.html',
                controller: 'ConsultarCombinacionesCvtCtrl'
            }).
            when('/ConsultarCombinacionesCvt/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarCombinacionesCvt.html',
                controller: 'ConsultarCombinacionesCvtCtrl'
            }).
            when('/GestionarCombinacionesCvt', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarCombinacionesCvt.html',
                controller: 'GestionarCombinacionesCvtCtrl'
            }).
            when('/GestionarCombinacionesCvt/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarCombinacionesCvt.html',
                controller: 'GestionarCombinacionesCvtCtrl'
            }).

            //ConceptosFacturacion
            when('/ConsultarConceptosFacturacion', {
                templateUrl: 'Aplicativo/Basico/Facturacion/ConsultarConceptoFacturacion.html',
                controller: 'ConsultarConceptosFacturacionCrtr'
            }).
            when('/ConsultarConceptosFacturacion/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Facturacion/ConsultarConceptoFacturacion.html',
                controller: 'ConsultarConceptosFacturacionCrtr'
            }).
            when('/GestionarConceptosFacturacion', {
                templateUrl: 'Aplicativo/Basico/Facturacion/GestionarConceptoFacturacion.html',
                controller: 'GestionarConceptosFacturacionCtrl'
            }).
            when('/GestionarConceptosFacturacion/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Facturacion/GestionarConceptoFacturacion.html',
                controller: 'GestionarConceptosFacturacionCtrl'
            }).

            //Conceptos Notas Facturas:

            when('/ConsultarConceptosNotasFacturas', {
                templateUrl: 'Aplicativo/Basico/Facturacion/ConsultarConceptosNotasFacturas.html',
                controller: 'ConsultarConceptosNotasFacturasCtrl'
            }).
            when('/ConsultarConceptosNotasFacturas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Facturacion/ConsultarConceptosNotasFacturas.html',
                controller: 'ConsultarConceptosNotasFacturasCtrl'
            }).
            when('/GestionarConceptosNotasFacturas', {
                templateUrl: 'Aplicativo/Basico/Facturacion/GestionarConceptosNotasFacturas.html',
                controller: 'GestionarConceptosNotasFacturasCtrl'
            }).
            when('/GestionarConceptosNotasFacturas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Facturacion/GestionarConceptosNotasFacturas.html',
                controller: 'GestionarConceptosNotasFacturasCtrl'
            }).


            //Precintos
            when('/ConsultarPrecintos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPrecintos.html',
                controller: 'ConsultarPrecintoCtrl'
            }).
            when('/ConsultarPrecintos/:Numero', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPrecintos.html',
                controller: 'ConsultarPrecintoCtrl'
            }).
            when('/GestionarAsignacionPresintos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarAsignacionPrecintos.html',
                controller: 'GestionarAsignacionPrecintosCtrl'
            }).
            when('/GestionarAsignacionPresintos/:Numero', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarAsignacionPrecintos.html',
                controller: 'GestionarAsignacionPrecintosCtrl'
            }).
            when('/GestionarTransladoPresintos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarTransladoPrecintos.html',
                controller: 'GestionarTransladoPrecintosCtrl'
            }).
            when('/GestionarTransladoPresintos/:Numero', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarTransladoPrecintos.html',
                controller: 'GestionarTransladoPrecintosCtrl'
            }).



            //Basico --> Almacenes
            when('/ConsultarAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarAlmacenes.html',
                controller: 'ConsultarAlmacenesCtrl'
            }).
            when('/ConsultarAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarAlmacenes.html',
                controller: 'ConsultarAlmacenesCtrl'
            }).
            when('/GestionarAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarAlmacenes.html',
                controller: 'GestionarAlmacenesCtrl'
            }).
            when('/GestionarAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarAlmacenes.html',
                controller: 'GestionarAlmacenesCtrl'
            }).

            //Básico-->Mantenimiento:

            when('/ConsultarLineaMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarLineaMantenimientoEspecial.html',
                controller: 'ConsultarLineaMantenimientoEspecialCtrl'
            }).
            when('/ConsultarLineaMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarLineaMantenimientoEspecial.html',
                controller: 'ConsultarLineaMantenimientoEspecialCtrl'
            }).
            when('/GestionarLineaMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarLineaMantenimientoEspecial.html',
                controller: 'GestionarLineaMantenimientoEspecialCtrl'
            }).
            when('/GestionarLineaMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarLineaMantenimientoEspecial.html',
                controller: 'GestionarLineaMantenimientoEspecialCtrl'
            }).
            when('/ConsultarSistemas', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarSistemas.html',
                controller: 'ConsultarSistemasCtrl'
            }).
            when('/ConsultarSistemas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarSistemas.html',
                controller: 'ConsultarSistemasCtrl'
            }).
            when('/GestionarSistemas', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarSistemas.html',
                controller: 'GestionarSistemasCtrl'
            }).
            when('/GestionarSistemas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarSistemas.html',
                controller: 'GestionarSistemasCtrl'
            }).
            when('/ConsultarSubsistemas', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarSubsistemas.html',
                controller: 'ConsultarSubsistemasCtrl'
            }).
            when('/ConsultarSubsistemas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarSubsistemas.html',
                controller: 'ConsultarSubsistemasCtrl'
            }).
            when('/GestionarSubsistemas', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarSubsistemas.html',
                controller: 'GestionarSubSistemasCtrl'
            }).
            when('/GestionarSubsistemas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarSubsistemas.html',
                controller: 'GestionarSubSistemasCtrl'
            }).
            when('/ConsultarUnidadFrecuenciaMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarUnidadFrecuenciaMantenimiento.html',
                controller: 'ConsultarUnidadFrecuenciaMantenimientoCtrl'
            }).
            when('/ConsultarUnidadFrecuenciaMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarUnidadFrecuenciaMantenimiento.html',
                controller: 'ConsultarUnidadFrecuenciaMantenimientoCtrl'
            }).
            when('/GestionarUnidadFrecuenciaMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarUnidadFrecuenciaMantenimiento.html',
                controller: 'GestionarUnidadFrecuenciaMantenimientoCtrl'
            }).
            when('/GestionarUnidadFrecuenciaMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarUnidadFrecuenciaMantenimiento.html',
                controller: 'GestionarUnidadFrecuenciaMantenimientoCtrl'
            }).
            when('/ConsultarTipoEquiposMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarTipoEquipoMantenimiento.html',
                controller: 'ConsultarTipoEquipoMantenimientoCtrl'
            }).
            when('/ConsultarTipoEquiposMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarTipoEquipoMantenimiento.html',
                controller: 'ConsultarTipoEquipoMantenimientoCtrl'
            }).
            when('/GestionarTipoEquipoMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarTipoEquipoMantenimiento.html',
                controller: 'GestionarTipoEquipoMantenimientoCtrl'
            }).
            when('/GestionarTipoEquipoMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarTipoEquipoMantenimiento.html',
                controller: 'GestionarTipoEquipoMantenimientoCtrl'
            }).
            when('/ConsultarEstadoEquiposMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarEstadoEquipoMantenimiento.html',
                controller: 'ConsultarEstadoEquipoMantenimientoCtrl'
            }).
            when('/ConsultarEstadoEquiposMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarEstadoEquipoMantenimiento.html',
                controller: 'ConsultarEstadoEquipoMantenimientoCtrl'
            }).
            when('/GestionarEstadoEquipoMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarEstadoEquipoMantenimiento.html',
                controller: 'GestionarEstadoEquipoMantenimientoCtrl'
            }).
            when('/GestionarEstadoEquipoMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarEstadoEquipoMantenimiento.html',
                controller: 'GestionarEstadoEquipoMantenimientoCtrl'
            }).
            when('/ConsultarMarcaEquiposMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarMarcaEquipoMantenimiento.html',
                controller: 'ConsultarMarcaEquipoMantenimientoCtrl'
            }).
            when('/ConsultarMarcaEquiposMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarMarcaEquipoMantenimiento.html',
                controller: 'ConsultarMarcaEquipoMantenimientoCtrl'
            }).
            when('/GestionarMarcaEquipoMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarMarcaEquipoMantenimiento.html',
                controller: 'GestionarMarcaEquipoMantenimientoCtrl'
            }).
            when('/GestionarMarcaEquipoMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarMarcaEquipoMantenimiento.html',
                controller: 'GestionarMarcaEquipoMantenimientoCtrl'
            }).
            when('/ConsultarEquipoMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarEquipoMantenimiento.html',
                controller: 'ConsultarEquipoMantenimientoCtrl'
            }).
            when('/ConsultarEquipoMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/ConsultarEquipoMantenimiento.html',
                controller: 'ConsultarEquipoMantenimientoCtrl'
            }).
            when('/GestionarEquipoMantenimiento', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarEquipoMantenimiento.html',
                controller: 'GestionarEquipoMantenimientoCtrl'
            }).
            when('/GestionarEquipoMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Mantenimiento/GestionarEquipoMantenimiento.html',
                controller: 'GestionarEquipoMantenimientoCtrl'
            }).
            //Grupo Referencias
            when('/ConsultarGrupoReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarGrupoReferencia.html',
                controller: 'ConsultarGrupoReferenciasCtrl'
            }).
            when('/ConsultarGrupoReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarGrupoReferencia.html',
                controller: 'ConsultarGrupoReferenciasCtrl'
            }).
            when('/GestionarGrupoReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarGrupoReferencias.html',
                controller: 'GestionarGrupoReferenciasCtrl'
            }).
            when('/GestionarGrupoReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarGrupoReferencias.html',
                controller: 'GestionarGrupoReferenciasCtrl'
            }).

            //Referencia Almacenes
            when('/ConsultarReferenciaAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferenciaAlmacenes.html',
                controller: 'ConsultarReferenciaAlmacenesCtrl'
            }).
            when('/ConsultarReferenciaAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferenciaAlmacenes.html',
                controller: 'ConsultarReferenciaAlmacenesCtrl'
            }).
            when('/GestionarReferenciaAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferenciaAlmacenes.html',
                controller: 'GestionarReferenciaAlmacenesCtrl'
            }).
            when('/GestionarReferenciaAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferenciaAlmacenes.html',
                controller: 'GestionarReferenciaAlmacenesCtrl'
            }).
            // Referencia Proveedores
            when('/ConsultarReferenciaProveedores', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferenciaProveedores.html',
                controller: 'ConsultarReferenciaProveedoresCtrl'
            }).
            when('/ConsultarReferenciaProveedores/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferenciaProveedores.html',
                controller: 'ConsultarReferenciaProveedoresCtrl'
            }).
            when('/GestionarReferenciaProveedores', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferenciaProveedores.html',
                controller: 'GestionarReferenciaProveedoresCtrl'
            }).
            when('/GestionarReferenciaProveedores/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferenciaProveedores.html',
                controller: 'GestionarReferenciaProveedoresCtrl'
            }).
            //Referencias
            when('/ConsultarReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferencias.html',
                controller: 'ConsultarReferenciasCtrl'
            }).
            when('/ConsultarReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferencias.html',
                controller: 'ConsultarReferenciasCtrl'
            }).
            when('/GestionarReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferencias.html',
                controller: 'GestionarReferenciasCtrl'
            }).
            when('/GestionarReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferencias.html',
                controller: 'GestionarReferenciasCtrl'
            }).
            //Ubicacion Almacenes
            when('/ConsultarUbicacionAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUbicacionAlmacenes.html',
                controller: 'ConsultarUbicacionAlmacenesCtrl'
            }).
            when('/ConsultarUbicacionAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUbicacionAlmacenes.html',
                controller: 'ConsultarUbicacionAlmacenesCtrl'
            }).
            when('/GestionarUbicacionAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUbicacionAlmacenes.html',
                controller: 'GestionarUbicacionAlmacenesCtrl'
            }).
            when('/GestionarUbicacionAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUbicacionAlmacenes.html',
                controller: 'GestionarUbicacionAlmacenesCtrl'
            }).
            //Unidad Empaque Referencia
            when('/ConsultarUnidadEmpaqueReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUnidadEmpaqueReferencias.html',
                controller: 'ConsultarUnidadEmpaqueReferenciasCtrl'
            }).
            when('/ConsultarUnidadEmpaqueReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUnidadEmpaqueReferencias.html',
                controller: 'ConsultarUnidadEmpaqueReferenciasCtrl'
            }).
            when('/GestionarUnidadEmpaqueReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUnidadEmpaqueReferencias.html',
                controller: 'GestionarUnidadEmpaqueReferenciasCtrl'
            }).
            when('/GestionarUnidadEmpaqueReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUnidadEmpaqueReferencias.html',
                controller: 'GestionarUnidadEmpaqueReferenciasCtrl'
            }).


            //Unidades Empaque Productos Transportados:
            when('/ConsultarUnidadesEmpaque', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarUnidadesEmpaque.html',
                controller: 'ConsultarUnidadesEmpaqueCtrl'
            }).
            when('/ConsultarUnidadesEmpaque/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarUnidadesEmpaque.html',
                controller: 'ConsultarUnidadesEmpaqueCtrl'
            }).
            when('/GestionarUnidadesEmpaque', {
                templateUrl: 'Aplicativo/Basico/General/GestionarUnidadesEmpaque.html',
                controller: 'GestionarUnidadesEmpaqueCtrl'
            }).
            when('/GestionarUnidadesEmpaque/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarUnidadesEmpaque.html',
                controller: 'GestionarUnidadesEmpaqueCtrl'
            }).

            //Unidad Medida Referencia
            when('/ConsultarUnidadMedidaReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUnidadMedidaReferencias.html',
                controller: 'ConsultarUnidadMedidaReferenciasCtrl'
            }).
            when('/ConsultarUnidadMedidaReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUnidadMedidaReferencias.html',
                controller: 'ConsultarUnidadMedidaReferenciasCtrl'
            }).
            when('/GestionarUnidadMedidaReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUnidadMedidaReferencias.html',
                controller: 'GestionarUnidadMedidaReferenciasCtrl'
            }).
            when('/GestionarUnidadMedidaReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUnidadMedidaReferencias.html',
                controller: 'GestionarUnidadMedidaReferenciasCtrl'
            }).

            //Productos Ministerio Transporte:

            when('/ConsultarProductosMinisterioTransporte', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarProductosMinisterioTransporte.html',
                controller: 'ConsultarProductosMinisterioTransporteCtrl'
            }).
            when('/ConsultarProductosMinisterioTransporte/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarProductosMinisterioTransporte.html',
                controller: 'ConsultarProductosMinisterioTransporteCtrl'
            }).
            when('/GestionarProductosMinisterioTransporte', {
                templateUrl: 'Aplicativo/Basico/General/GestionarProductosMinisterioTransporte.html',
                controller: 'GestionarProductosMinisterioTransporteCtrl'
            }).
            when('/GestionarProductosMinisterioTransporte/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarProductosMinisterioTransporte.html',
                controller: 'GestionarProductosMinisterioTransporteCtrl'
            }).

            //Productos Transportados:
            when('/ConsultarProductoTransportados', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarProductoTransportados.html',
                controller: 'ConsultarProductoTransportadosCtrl'
            }).
            when('/ConsultarProductoTransportados/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarProductoTransportados.html',
                controller: 'ConsultarProductoTransportadosCtrl'
            }).
            when('/GestionarProductoTransportados', {
                templateUrl: 'Aplicativo/Basico/General/GestionarProductoTransportados.html',
                controller: 'GestionarProductoTransportadosCtrl'
            }).
            when('/GestionarProductoTransportados/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarProductoTransportados.html',
                controller: 'GestionarProductoTransportadosCtrl'
            }).

            //Basico-Despachos
            //ColorVehiculos
            when('/ConsultarColorVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarColorVehiculos.html',
                controller: 'ConsultarColorVehiculosCtrl'
            }).
            when('/ConsultarColorVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarColorVehiculos.html',
                controller: 'ConsultarColorVehiculosCtrl'
            }).
            when('/GestionarColorVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarColorVehiculos.html',
                controller: 'GestionarColorVehiculosCtrl'
            }).
            when('/GestionarColorVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarColorVehiculos.html',
                controller: 'GestionarColorVehiculosCtrl'
            }).
            //Rendimiento Galón
            when('/ConsultarRendimientoGalonCombustible', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarRendimientoGalonCombustible.html',
                controller: 'ConsultarRendimientoGalonCombustibleCtrl'
            }).
            when('/ConsultarRendimientoGalonCombustible/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarRendimientoGalonCombustible.html',
                controller: 'ConsultarRendimientoGalonCombustibleCtrl'
            }).
            when('/GestionarRendimientoGalonCombustible', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarRendimientoGalonCombustible.html',
                controller: 'GestionarRendimientoGalonCombustibleCtrl'
            }).
            when('/GestionarRendimientoGalonCombustible/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarRendimientoGalonCombustible.html',
                controller: 'GestionarRendimientoGalonCombustibleCtrl'
            }).
            //MarcaVehiculos
            when('/ConsultarMarcaVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarMarcaVehiculos.html',
                controller: 'ConsultarMarcaVehiculosCtrl'
            }).
            when('/ConsultarMarcaVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarMarcaVehiculos.html',
                controller: 'ConsultarMarcaVehiculosCtrl'
            }).
            when('/GestionarMarcaVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarMarcaVehiculos.html',
                controller: 'GestionarMarcaVehiculosCtrl'
            }).
            when('/GestionarMarcaVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarMarcaVehiculos.html',
                controller: 'GestionarMarcaVehiculosCtrl'
            }).
            //LineaVehiculos
            when('/ConsultarLineaVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarLineaVehiculos.html',
                controller: 'ConsultarLineaVehiculosCtrl'
            }).
            when('/ConsultarLineaVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarLineaVehiculos.html',
                controller: 'ConsultarLineaVehiculosCtrl'
            }).
            when('/GestionarLineaVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarLineaVehiculos.html',
                controller: 'GestionarLineaVehiculosCtrl'
            }).
            when('/GestionarLineaVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarLineaVehiculos.html',
                controller: 'GestionarLineaVehiculosCtrl'
            }).
            //Rutas
            when('/ConsultarRutas', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarRutas.html',
                controller: 'ConsultarRutasCtrl'
            }).
            when('/ConsultarRutas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarRutas.html',
                controller: 'ConsultarRutasCtrl'
            }).
            when('/GestionarRutas', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarRutas.html',
                controller: 'GestionarRutasCtrl'
            }).
            when('/GestionarRutas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarRutas.html',
                controller: 'GestionarRutasCtrl'
            }).

            /// Gastos Rutas 
            when('/ConsultarGastosRutas', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarGastosRutas.html',
                controller: 'ConsultarGastosRutasCtrl'
            }).
            when('/ConsultarGastosRutas/:Ruta/:Concepto/:Vehiculo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarGastosRutas.html',
                controller: 'ConsultarGastosRutasCtrl'
            }).

            when('/GestionarGastosRutas', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarGastosRutas.html',
                controller: 'GestionarGastosRutasCtrl'
            }).




            //Novedades Despachos
            when('/ConsultarNovedadesDespachos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarNovedadesDespachos.html',
                controller: 'ConsultarNovedadesDespachosCtrl'
            }).
            when('/ConsultarNovedadesDespachos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarNovedadesDespachos.html',
                controller: 'ConsultarNovedadesDespachosCtrl'
            }).
            when('/GestionarNovedadesDespachos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarNovedadesDespachos.html',
                controller: 'GestionarNovedadesDespachosCtrl'
            }).
            when('/GestionarNovedadesDespachos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarNovedadesDespachos.html',
                controller: 'GestionarNovedadesDespachosCtrl'
            }).
            //Sitios Cargue y Descargue
            when('/ConsultarSitiosCargueDescargue', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarSitiosCargueDescargue.html',
                controller: 'ConsultarSitiosCargueDescargueCtrl'
            }).
            when('/ConsultarSitiosCargueDescargue/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarSitiosCargueDescargue.html',
                controller: 'ConsultarSitiosCargueDescargueCtrl'
            }).
            when('/GestionarSitiosCargueDescargue', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarSitiosCargueDescargue.html',
                controller: 'GestionarSitiosCargueDescargueCtrl'
            }).
            when('/GestionarSitiosCargueDescargue/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarSitiosCargueDescargue.html',
                controller: 'GestionarSitiosCargueDescargueCtrl'
            }).
            //Vehiculos
            when('/ConsultarVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarVehiculos.html',
                controller: 'ConsultarVehiculosCtrl'
            }).
            when('/ConsultarVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarVehiculos.html',
                controller: 'ConsultarVehiculosCtrl'
            }).
            when('/GestionarVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarVehiculos.html',
                controller: 'GestionarVehiculosCtrl'
            }).
            when('/GestionarVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarVehiculos.html',
                controller: 'GestionarVehiculosCtrl'
            }).
            //Vehiculos Lista Negra:
            when('/ConsultarVehiculosListaNegra', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarVehiculosListaNegra.html',
                controller: 'ConsultarVehiculosListaNegraCtrl'
            }).
            when('/ConsultarVehiculosListaNegra/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarVehiculosListaNegra.html',
                controller: 'ConsultarVehiculosListaNegraCtrl'
            }).
            when('/GestionarVehiculosListaNegra', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarVehiculosListaNegra.html',
                controller: 'GestionarVehiculosListaNegraCtrl'
            }).
            when('/GestionarVehiculosListaNegra/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarVehiculosListaNegra.html',
                controller: 'GestionarVehiculosListaNegraCtrl'
            }).
            //MarcaSemirremolques
            when('/ConsultarMarcaSemirremolques', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarMarcaSemirremolques.html',
                controller: 'ConsultarMarcaSemirremolquesCtrl'
            }).
            when('/ConsultarMarcaSemirremolques/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarMarcaSemirremolques.html',
                controller: 'ConsultarMarcaSemirremolquesCtrl'
            }).
            when('/GestionarMarcaSemirremolques', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarMarcaSemirremolques.html',
                controller: 'GestionarMarcaSemirremolquesCtrl'
            }).
            when('/GestionarMarcaSemirremolques/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarMarcaSemirremolques.html',
                controller: 'GestionarMarcaSemirremolquesCtrl'
            }).
            //Semirremolques
            when('/ConsultarSemirremolques', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarSemirremolques.html',
                controller: 'ConsultarSemirremolquesCtrl'
            }).
            when('/ConsultarSemirremolques/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarSemirremolques.html',
                controller: 'ConsultarSemirremolquesCtrl'
            }).
            when('/GestionarSemirremolques', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarSemirremolques.html',
                controller: 'GestionarSemirremolquesCtrl'
            }).
            when('/GestionarSemirremolques/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarSemirremolques.html',
                controller: 'GestionarSemirremolquesCtrl'
            }).
            //Puestos de control
            when('/ConsultarPuestosControl', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPuestoControles.html',
                controller: 'ConsultarPuestoControlesCtrl'
            }).
            when('/ConsultarPuestosControl/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPuestoControles.html',
                controller: 'ConsultarPuestoControlesCtrl'
            }).
            when('/GestionarPuestoControles', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarPuestoControles.html',
                controller: 'GestionarPuestoControlesCtrl'
            }).
            when('/GestionarPuestoControles/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarPuestoControles.html',
                controller: 'GestionarPuestoControlesCtrl'
            }).
            //Pejaes
            when('/ConsultarPeajes', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPeajes.html',
                controller: 'ConsultarPeajesCtrl'
            }).
            when('/ConsultarPeajes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPeajes.html',
                controller: 'ConsultarPeajesCtrl'
            }).
            //when('/ConsultarPeajesRutas', {
            //    templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPeajesRutas.html',
            //    controller: 'ConsultarPeajesRutasCtrl'
            //}).
            //when('/ConsultarPeajesRutas/:Codigo/:Peaje', {
            //    templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPeajesRutas.html',
            //    controller: 'ConsultarPeajesRutasCtrl'
            //}).
            when('/GestionarPeajes', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarPeajes.html',
                controller: 'GestionarPeajesCtrl'
            }).
            when('/GestionarPeajes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarPeajes.html',
                controller: 'GestionarPeajesCtrl'
            }).
            //when('/GestionarPeajesRutas', {
            //    templateUrl: 'Aplicativo/Basico/Despachos/GestionarPeajesRutas.html',
            //    controller: 'GestionarPeajesCtrl'
            //}).
            //when('/GestionarPeajesRutas/:Codigo', {
            //    templateUrl: 'Aplicativo/Basico/Despachos/GestionarPeajeRutas.html',
            //    controller: 'GestionarPeajesRutasCtrl'
            //}).

            when('/CargarPeajes', {
                templateUrl: 'Aplicativo/Basico/Procesos/CargarPeajes.html',
                controller: 'CargarPeajesCtrl'
            }).

            //Básico->Tesorería
            //Bancos
            when('/ConsultarBancos', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarBancos.html',
                controller: 'ConsultarBancosCtrl'
            }).
            when('/ConsultarBancos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarBancos.html',
                controller: 'ConsultarBancosCtrl'
            }).
            when('/GestionarBancos', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarBancos.html',
                controller: 'GestionarBancosCtrl'
            }).
            when('/GestionarBancos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarBancos.html',
                controller: 'GestionarBancosCtrl'
            }).
            //Plan Unico Cuentas
            when('/ConsultarPlanUnicoCuentas', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarPlanUnicoCuentas.html',
                controller: 'ConsultarPlanUnicoCuentasCtrl'
            }).
            when('/ConsultarPlanUnicoCuentas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarPlanUnicoCuentas.html',
                controller: 'ConsultarPlanUnicoCuentasCtrl'
            }).
            when('/GestionarPlanUnicoCuentas', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarPlanUnicoCuentas.html',
                controller: 'GestionarPlanUnicoCuentasCtrl'
            }).
            when('/GestionarPlanUnicoCuentas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarPlanUnicoCuentas.html',
                controller: 'GestionarPlanUnicoCuentasCtrl'
            }).
            //Cuenta Bancarias
            when('/ConsultarCuentaBancarias', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarCuentaBancarias.html',
                controller: 'ConsultarCuentaBancariasCtrl'
            }).
            when('/ConsultarCuentaBancarias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarCuentaBancarias.html',
                controller: 'ConsultarCuentaBancariasCtrl'
            }).
            when('/GestionarCuentaBancarias', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarCuentaBancarias.html',
                controller: 'GestionarCuentaBancariasCtrl'
            }).
            when('/GestionarCuentaBancarias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarCuentaBancarias.html',
                controller: 'GestionarCuentaBancariasCtrl'
            }).
            //Cuenta Cambista
            when('/ConsultarCuentasCambista', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarCuentaCambista.html',
                controller: 'ConsultarCuentaCambistaCtrl'
            }).
            when('/ConsultarCuentasCambista/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarCuentaCambista.html',
                controller: 'ConsultarCuentaCambistaCtrl'
            }).
            when('/GestionarCuentasCambista', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarCuentaCambista.html',
                controller: 'GestionarCuentaCambistaCtrl'
            }).
            when('/GestionarCuentasCambista/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarCuentaCambista.html',
                controller: 'GestionarCuentaCambistaCtrl'
            }).
            //Chequera cuenta bancarias
            when('/ConsultarChequeras', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarChequeraCuentaBancaria.html',
                controller: 'ConsultarChequeraCuentaBancariasCtrl'
            }).
            when('/ConsultarChequeras/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarChequeraCuentaBancaria.html',
                controller: 'ConsultarChequeraCuentaBancariasCtrl'
            }).
            when('/GestionarChequeras', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarChequeraCuentaBancarias.html',
                controller: 'GestionarChequeraCuentaBancariasCtrl'
            }).
            when('/GestionarChequeras/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarChequeraCuentaBancarias.html',
                controller: 'GestionarChequeraCuentaBancariasCtrl'
            }).
            //cajas
            when('/ConsultarCajas', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarCajas.html',
                controller: 'ConsultarCajasCtrl'
            }).
            when('/ConsultarCajas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarCajas.html',
                controller: 'ConsultarCajasCtrl'
            }).
            when('/GestionarCajas', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarCajas.html',
                controller: 'GestionarCajasCtrl'
            }).
            when('/GestionarCajas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarCajas.html',
                controller: 'GestionarCajasCtrl'
            }).
            //Conceptos Contables
            when('/ConsultarConceptoContables', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarConceptoContables.html',
                controller: 'ConsultarConceptoContablesCtrl'
            }).
            when('/ConsultarConceptoContables/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarConceptoContables.html',
                controller: 'ConsultarConceptoContablesCtrl'
            }).
            when('/GestionarConceptoContables', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarConceptoContables.html',
                controller: 'GestionarConceptoContablesCtrl'
            }).
            when('/GestionarConceptoContables/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarConceptoContables.html',
                controller: 'GestionarConceptoContablesCtrl'
            }).

            //Sucursales SIESA
            when('/ConsultarSucursalesSIESA', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarSucursalesSIESA.html',
                controller: 'ConsultarSucursalesSIESACtrl'
            }).
            when('/ConsultarSucursalesSIESA/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarSucursalesSIESA.html',
                controller: 'ConsultarSucursalesSIESACtrl'
            }).

            when('/GestionarSucursalSIESA', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarSucursalSIESA.html',
                controller: 'GestionarSucursalSIESACtrl'
            }).

            when('/GestionarSucursalSIESA/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarSucursalSIESA.html',
                controller: 'GestionarSucursalSIESACtrl'
            }).

            //Parametrizacion Contables
            when('/ConsultarParametrizacionContables', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarParametrizacionContables.html',
                controller: 'ConsultarParametrizacionContablesCtrl'
            }).
            when('/ConsultarParametrizacionContables/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarParametrizacionContables.html',
                controller: 'ConsultarParametrizacionContablesCtrl'
            }).
            when('/GestionarParametrizacionContables', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarParametrizacionContables.html',
                controller: 'GestionarParametrizacionContablesCtrl'
            }).
            when('/GestionarParametrizacionContables/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarParametrizacionContables.html',
                controller: 'GestionarParametrizacionContablesCtrl'
            }).

            //Control Viajes
            //Inspección Preoperacional
            when('/ConsultarInspeccionPreoperacional', {
                templateUrl: 'Aplicativo/ControlViajes/Documentos/ConsultarInspeccionPreoperacional.html',
                controller: 'ConsultarInspeccionPreoperacionalCtrl'
            }).
            when('/ConsultarInspeccionPreoperacional/:Numero', {
                templateUrl: 'Aplicativo/ControlViajes/Documentos/ConsultarInspeccionPreoperacional.html',
                controller: 'ConsultarInspeccionPreoperacionalCtrl'
            }).

            when('/ConsultarInspeccionPreoperacional/:Numero/:MeapCodigo', {
                templateUrl: 'Aplicativo/ControlViajes/Documentos/ConsultarInspeccionPreoperacional.html',
                controller: 'ConsultarInspeccionPreoperacionalCtrl'
            }).


            when('/GestionarInspeccionPreoperacional', {
                templateUrl: 'Aplicativo/ControlViajes/Documentos/GestionarInspeccionPreoperacional.html',
                controller: 'GestionarInspeccionPreoperacionalCtrl'
            }).
            when('/GestionarInspeccionPreoperacional/:Numero', {
                templateUrl: 'Aplicativo/ControlViajes/Documentos/GestionarInspeccionPreoperacional.html',
                controller: 'GestionarInspeccionPreoperacionalCtrl'
            }).

            when('/GestionarInspeccionPreoperacional/:Numero/:MeapCodigo', {
                templateUrl: 'Aplicativo/ControlViajes/Documentos/GestionarInspeccionPreoperacional.html',
                controller: 'GestionarInspeccionPreoperacionalCtrl'
            }).

            //Seguimiento Vehículos
            when('/ConsultarSeguimientoVehiculosTransito', {
                templateUrl: 'Aplicativo/ControlViajes/ConsultarSeguimientoVehiculos.html',
                controller: 'ConsultarSeguimientoVehiculosCtrl'
            }).
            //Seguimiento Vehículos - Portal
            when('/ConsultarSeguimientoVehiculosTransitoPortal', {
                templateUrl: 'Portal/ConsultarSeguimientoVehiculosClientes.html',
                controller: 'ConsultarSeguimientoVehiculosClientesCtrl'
            }).
            //Consultas Seguimiento Vehículos - Portal
            when('/ConsultasSeguimientoVehicularHistoricoPortal', {
                templateUrl: 'Portal/ConsultasSeguimientoPortal.html',
                controller: 'ConsultasSeguimientoPortalCtrl'
            }).
            //Consultas Seguimiento Vehículos
            when('/ConsultasSeguimientoVehicular', {
                templateUrl: 'Aplicativo/ControlViajes/ConsultasSeguimiento.html',
                controller: 'ConsultasSeguimientoCtrl'
            }).
            //Autorizaciones
            when('/ConsultarAutorizaciones', {
                templateUrl: 'Aplicativo/ControlViajes/ConsultarSolicitudAutorizaciones.html',
                controller: 'ConsultarSolicitudAutorizacionesCtrl'
            }).
            when('/ConsultarAutorizaciones/:Numero', {
                templateUrl: 'Aplicativo/ControlViajes/ConsultarSolicitudAutorizaciones.html',
                controller: 'ConsultarSolicitudAutorizacionesCtrl'
            }).

            //PAQUETERIA-Documentos
            //Recolecciones
            when('/ConsultarRecolecciones', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Recolecciones/ConsultarRecolecciones.html',
                controller: 'ConsultarRecoleccionesCtrl'
            }).
            when('/ConsultarRecolecciones/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Recolecciones/ConsultarRecolecciones.html',
                controller: 'ConsultarRecoleccionesCtrl'
            }).
            when('/GestionarRecolecciones', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Recolecciones/GestionarRecolecciones.html',
                controller: 'GestionarRecoleccionesCtrl'
            }).
            when('/GestionarRecolecciones/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Recolecciones/GestionarRecolecciones.html',
                controller: 'GestionarRecoleccionesCtrl'
            }).
            //Planilla Recolecciones
            when('/ConsultarPlanillaRecolecciones', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaRecolecciones/ConsultarPlanillaRecolecciones.html',
                controller: 'ConsultarPlanillaRecoleccionesCtrl'
            }).
            when('/ConsultarPlanillaRecolecciones/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaRecolecciones/ConsultarPlanillaRecolecciones.html',
                controller: 'ConsultarPlanillaRecoleccionesCtrl'
            }).
            when('/GestionarPlanillaRecolecciones', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaRecolecciones/GestionarPlanillaRecolecciones.html',
                controller: 'GestionarPlanillaRecoleccionesCtrl'
            }).
            when('/GestionarPlanillaRecolecciones/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaRecolecciones/GestionarPlanillaRecolecciones.html',
                controller: 'GestionarPlanillaRecoleccionesCtrl'
            }).
            //PAQUETERIA - PROCESOS 
            //Planificacion Despachos
            when('/PlanificacionDespachos', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/PlanificacionDespachos.html',
                controller: 'PlanificacionDespachosCtrl'
            }).
            // Adicionar o Cancelar Recolecciones : 
            when('/AdicionaroCancelarRecoleccionesPlanillaEntrega', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/AdicionaroCancelarRecoleccionesPlanillaEntrega.html',
                controller: 'AdicionaroCancelarRecoleccionesPlanillaEntregaCtrl'
            }).
            when('/AdicionaroCancelarRecoleccionesPlanillaRecoleccion', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/AdicionaroCancelarRecoleccionesPlanillaRecoleccion.html',
                controller: 'AdicionaroCancelarRecoleccionesPlanillaRecoleccionCtrl'
            }).

            //Planificacion Despachos v2
            when('/PlanificacionDespachosV2', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/PlanificacionDespachosV2.html',
                controller: 'PlanificacionDespachosV2Ctrl'
            }).

            //Zonificacion Guias

            when('/ZonificacionGuias', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/ConsultarZonificacionGuias.html',
                controller: 'ConsultarZonificacionGuiasCtrl'
            }).

            when('/CambiarEstadoDefinitivoRemesas', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/CambiarEstadoDefinitivoRemesas.html',
                controller: 'CambiarEstadoDefinitivoRemesasCtrl'
            }).

            //CONTABILIDAD
            //Comprobantes Contables
            when('/ConsultarComprobantesContables', {
                templateUrl: 'Aplicativo/Contabilidad/Documentos/ConsultarComprobantesContables.html',
                controller: 'ConsultarComprobantesContablesCtrl'
            }).
            when('/ConsultarComprobantesContables/:Numero', {
                templateUrl: 'Aplicativo/Contabilidad/Documentos/ConsultarComprobantesContables.html',
                controller: 'ConsultarComprobantesContablesCtrl'
            }).
            when('/GestionarComprobantesContables', {
                templateUrl: 'Aplicativo/Contabilidad/Documentos/GestionarComprobantesContables.html',
                controller: 'GestionarComprobantesContablesCtrl'
            }).
            when('/GestionarComprobantesContables/:Numero', {
                templateUrl: 'Aplicativo/Contabilidad/Documentos/GestionarComprobantesContables.html',
                controller: 'GestionarComprobantesContablesCtrl'
            }).
            when('/GenerarInterfazContable', {
                templateUrl: 'Aplicativo/Contabilidad/Procesos/GenerarInterfazContable.html',
                controller: 'GenerarInterfazContableController'
            }).
            when('/GenerarComprobanteContable', {
                templateUrl: 'Aplicativo/Contabilidad/Procesos/GenerarComprobantesContables.html',
                controller: 'GenerarComprobantesContablesCtrl'
            }).

            when('/ConsultarListadoFlotaPropia', {
                templateUrl: 'Aplicativo/FlotaPropia/Listados/ConsultarListadoFlotaPropia.html',
                controller: 'ConsultarListadoFlotaPropiaController'
            }).

            //Fidelizacion Tranportadores
            when('/ConsultarFidelizacion', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/ConsultarFidelizacion.html',
                controller: 'ConsultarFidelizacionCtrl'
            }).
            when('/ConsultarFidelizacion/:Codigo', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/ConsultarFidelizacion.html',
                controller: 'ConsultarFidelizacionCtrl'
            }).
            when('/GestionarFidelizacion', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/GestionarFidelizacion.html',
                controller: 'GestionarFidelizacionCtrl'
            }).
            when('/GestionarFidelizacion/:Codigo', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/GestionarFidelizacion.html',
                controller: 'GestionarFidelizacionCtrl'
            }).

            //Fidelizacion Tranportadores - Causales
            when('/ConsultarCausalesFidelizacion', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/ConsultarCausalesFidelizacion.html',
                controller: 'ConsultarCausalesFidelizacionCtrl'
            }).
            when('/ConsultarCausalesFidelizacion/:Codigo', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/ConsultarCausalesFidelizacion.html',
                controller: 'ConsultarCausalesFidelizacionCtrl'
            }).
            when('/ConsultarCausalesFidelizacion/:Codigo/:Numero', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/ConsultarCausalesFidelizacion.html',
                controller: 'ConsultarCausalesFidelizacionCtrl'
            }).
            when('/GestionarCausalesFidelizacion', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/GestionarCausalesFidelizacion.html',
                controller: 'GestionarCausalesFidelizacionCtrl'
            }).
            when('/GestionarCausalesFidelizacion/:Codigo', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/GestionarCausalesFidelizacion.html',
                controller: 'GestionarCausalesFidelizacionCtrl'
            }).

            //Fidelizacion Tranportadores - Despachos
            when('/ConsultarDespachosFidelizacion', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/ConsultarDespachosFidelizacion.html',
                controller: 'ConsultarDespachosFidelizacionCtrl'
            }).
            when('/ConsultarDespachosFidelizacion/:Codigo', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/ConsultarDespachosFidelizacion.html',
                controller: 'ConsultarDespachosFidelizacionCtrl'
            }).
            when('/ConsultarDespachosFidelizacion/:Codigo/:Numero', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/ConsultarDespachosFidelizacion.html',
                controller: 'ConsultarDespachosFidelizacionCtrl'
            }).
            when('/GestionarDespachosFidelizacion', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/GestionarDespachosFidelizacion.html',
                controller: 'GestionarDespachosFidelizacionCtrl'
            }).
            when('/GestionarDespachosFidelizacion/:Codigo', {
                templateUrl: 'Aplicativo/Fidelizacion/Documentos/GestionarDespachosFidelizacion.html',
                controller: 'GestionarDespachosFidelizacionCtrl'
            }).

            //Fidelizacion - Listados
            when('/ConsultarListadoFidelizaciones', {
                templateUrl: 'Aplicativo/Fidelizacion/Listados/ConsultarListadoFidelizaciones.html',
                controller: 'ConsultarListadoFidelizacionCtrl'
            }).

            //Cierre Contable Documentos
            when('/GestionarCierreContable', {
                templateUrl: 'Aplicativo/Contabilidad/Procesos/GestionarCierreContable.html',
                controller: 'GestionarCierreContableCtrl'
            }).

            //Interfaz Contable PSL
            when('/ConsultarInterfazContablePsl', {
                templateUrl: 'Aplicativo/Contabilidad/Procesos/ConsultarInterfazContablePsl.html',
                controller: 'ConsultarInterfazContablePslCtrl'
            }).

            //Integracion Siesa
            when('/ConsultarIntegracionSiesa', {
                templateUrl: 'Aplicativo/Contabilidad/Procesos/ConsultarIntegracionSiesa.html',
                controller: 'ConsultarIntegracionSiesaCtrl'
            }).


            //SEGURIDAD
            //Estudio Seguridad
            when('/ConsultarEstudioSeguridad', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarEstudioSeguridad.html',
                controller: 'ConsultarEstudioSeguridadCtrl'
            }).
            when('/ConsultarEstudioSeguridad/:Numero', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarEstudioSeguridad.html',
                controller: 'ConsultarEstudioSeguridadCtrl'
            }).
            when('/ConsultarEstudioSeguridad/:Numero/:Aprobacion', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarEstudioSeguridad.html',
                controller: 'ConsultarEstudioSeguridadCtrl'
            }).
            when('/GestionarEstudioSeguridad', {
                templateUrl: 'Aplicativo/Seguridad/GestionarEstudioSeguridad.html',
                controller: 'GestionarEstudioSeguridadCtrl'
            }).
            when('/GestionarEstudioSeguridad/:Numero', {
                templateUrl: 'Aplicativo/Seguridad/GestionarEstudioSeguridad.html',
                controller: 'GestionarEstudioSeguridadCtrl'
            }).
            when('/GestionarEstudioSeguridad/:Numero/:Aprobacion', {
                templateUrl: 'Aplicativo/Seguridad/GestionarEstudioSeguridad.html',
                controller: 'GestionarEstudioSeguridadCtrl'
            }).
            //Aprobar Estudio Seguridad
            when('/AprobarEstudioSeguridad', {
                templateUrl: 'Aplicativo/Seguridad/AprobarEstudioSeguridad.html',
                controller: 'AprobarEstudioSeguridadCtrl'
            }).
            when('/AprobarEstudioSeguridad/:Numero', {
                templateUrl: 'Aplicativo/Seguridad/AprobarEstudioSeguridad.html',
                controller: 'AprobarEstudioSeguridadCtrl'
            }).
            //GESPHONE
            //Entregas
            when('/ConsultarControlRecolecciones', {
                templateUrl: 'Gesphone/Entregas/ConsultarControlRecolecciones.html',
                controller: 'ConsultarControlRecoleccionesCtrl'
            }).
            when('/ConsultarControlEntregasPlanilla', {
                templateUrl: 'Gesphone/Entregas/ConsultarControlEntregasPlanilla.html',
                controller: 'ConsultarControlEntregasPlanillaCtrl'
            }).
            when('/ConsultarControlEntregasPlanillaDomiciliaria', {
                templateUrl: 'Gesphone/Entregas/ConsultarControlEntregasPlanillaDomiciliaria.html',
                controller: 'ConsultarControlEntregasPlanillaDomiciliariaCtrl'
            }).
            when('/ControlEntregaRemesasDistribucion', {
                templateUrl: 'Gesphone/Entregas/ConsultarControlEntregasV2.html',
                controller: 'ConsultarControlEntregasCtrlV2'
            }).
            when('/ControlEntregaRecoleccion', {
                templateUrl: 'Gesphone/Entregas/ConsultarControlEntregasRecoleccion.html',
                controller: 'ConsultarControlEntregasRecoleccionCtrl'
            }).
            when('/ConsultarControlEntregas/:Numero', {
                templateUrl: 'Gesphone/Entregas/ConsultarControlEntregasV2.html',
                controller: 'ConsultarControlEntregasCtrlV2'
            }).
            when('/Enturnamiento', {
                templateUrl: 'Gesphone/Enturnamiento/GestionarEntunramiento.html',
                controller: 'GestionarEntunramientoCtrl'
            }).
            //Documentos Proximos a Vencer
            when('/DocumentosProximosVencer', {
                templateUrl: 'Gesphone/Documentos/DocumentosProximoVencer/DocumentosProximosVencer.html',
                controller: 'DocumentosProximosVencerCtrl'
            }).
            //Entrega Remesa Paqueteria
            when('/ControlEntregaGuias', {
                templateUrl: 'Gesphone/EntregasPaqueteria/ConsultarControlEntregasPaqueteria.html',
                controller: 'ConsultarControlEntregasPaqueteriaCtrl'
            }).
            when('/ControlEntregaGuias/:Numero', {
                templateUrl: 'Gesphone/EntregasPaqueteria/ConsultarControlEntregasPaqueteria.html',
                controller: 'ConsultarControlEntregasPaqueteriaCtrl'
            }).
            when('/GestionarControlEntregasGuias', {
                templateUrl: 'Gesphone/EntregasPaqueteria/GestionarControlEntregasPaqueteria.html',
                controller: 'GestionarControlEntregasPaqueteriaCtrl'
            }).
            when('/GestionarControlEntregasGuias/:Numero', {
                templateUrl: 'Gesphone/EntregasPaqueteria/GestionarControlEntregasPaqueteria.html',
                controller: 'GestionarControlEntregasPaqueteriaCtrl'
            }).
            //Departamentos
            when('/ConsultarDepartamentos', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarDepartamentos.html',
                controller: 'ConsultarDepartamentosCtrl'
            }).
            when('/ConsultarDepartamentos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarDepartamentos.html',
                controller: 'ConsultarDepartamentosCtrl'
            }).
            when('/GestionarDepartamentos', {
                templateUrl: 'Aplicativo/Basico/General/GestionarDepartamentos.html',
                controller: 'GestionarDepartamentosCtrl'
            }).
            when('/GestionarDepartamentos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarDepartamentos.html',
                controller: 'GestionarDepartamentosCtrl'
            }).
            //Paises
            when('/ConsultarPaises', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarPaises.html',
                controller: 'ConsultarPaisesCtrl'
            }).
            when('/ConsultarPaises/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarPaises.html',
                controller: 'ConsultarPaisesCtrl'
            }).
            when('/GestionarPaises', {
                templateUrl: 'Aplicativo/Basico/General/GestionarPaises.html',
                controller: 'GestionarPaisesCtrl'
            }).
            when('/GestionarPaises/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarPaises.html',
                controller: 'GestionarPaisesCtrl'
            }).
            //Novedad Transito
            when('/GestionarNovedadTransito', {
                templateUrl: 'Gesphone/Transito/GestionarNovedadTransito.html',
                controller: 'GestionarNovedadTransitoCtrl'
            }).
            //ListaChequeoCargueDescargues
            when('/ConsultarListaChequeoCargueDescargue', {
                templateUrl: 'Gesphone/ListaChequeo/ConsultarListaChequeCargueDescargues.html',
                controller: 'ConsultarListaChequeoCargueDescarguesCtrl'
            }).
            when('/ConsultarListaChequeoCargueDescargue/:Numero', {
                templateUrl: 'Gesphone/ListaChequeo/ConsultarListaChequeCargueDescargues.html',
                controller: 'ConsultarListaChequeoCargueDescarguesCtrl'
            }).
            when('/GestionarListaChequeoCargueDescargue', {
                templateUrl: 'Gesphone/ListaChequeo/GestionarListaChequeoCargueDescargues.html',
                controller: 'GestionarListaChequeoCargueDescarguesCtrl'
            }).
            when('/GestionarListaChequeoCargueDescargue/:Numero', {
                templateUrl: 'Gesphone/ListaChequeo/GestionarListaChequeoCargueDescargues.html',
                controller: 'GestionarListaChequeoCargueDescarguesCtrl'
            }).
            //ListaChequeoVehiculos
            when('/ConsultarInspeccionesVehiculos', {
                templateUrl: 'Gesphone/ListaChequeo/ConsultarListaChequeoVehiculos.html',
                controller: 'ConsultarListaChequeoVehiculosCtrl'
            }).
            when('/ConsultarInspeccionesVehiculos/:Numero', {
                templateUrl: 'Gesphone/ListaChequeo/ConsultarListaChequeoVehiculos.html',
                controller: 'ConsultarListaChequeoVehiculosCtrl'
            }).
            when('/GestionarInspeccionesVehiculos', {
                templateUrl: 'Gesphone/ListaChequeo/GestionarListaChequeoVehiculos.html',
                controller: 'GestionarListaChequeoVehiculosCtrl'
            }).
            when('/GestionarInspeccionesVehiculos/:Numero', {
                templateUrl: 'Gesphone/ListaChequeo/GestionarListaChequeoVehiculos.html',
                controller: 'GestionarListaChequeoVehiculosCtrl'
            }).

            //TESORERIA
            //Comprobante Egresos
            when('/ConsultarComprobanteEgresos', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarComprobanteEgresos.html',
                controller: 'ConsultarComprobanteEgresosCtrl'
            }).
            when('/ConsultarComprobanteEgresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarComprobanteEgresos.html',
                controller: 'ConsultarComprobanteEgresosCtrl'
            }).
            when('/GestionarComprobanteEgresos', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarComprobanteEgresos.html',
                controller: 'GestionarComprobanteEgresosCtrl'
            }).
            when('/GestionarComprobanteEgresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarComprobanteEgresos.html',
                controller: 'GestionarComprobanteEgresosCtrl'
            }).

            when('/GenerarArchivouiaf', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarArchivoUIAF.html',
                controller: 'GestionarArchivoUIAFController'
            }).

            //Comprobante Ingresos  
            when('/ConsultarComprobanteIngresos', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarComprobanteIngresos.html',
                controller: 'ConsultarComprobanteIngresosCtrl'
            }).
            when('/ConsultarComprobanteIngresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarComprobanteIngresos.html',
                controller: 'ConsultarComprobanteIngresoCtrl'
            }).
            when('/GestionarComprobanteIngresos', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarComprobanteIngresos.html',
                controller: 'GestionarComprobanteIngresosCtrl'
            }).
            when('/GestionarComprobanteIngresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarComprobanteIngresos.html',
                controller: 'GestionarComprobanteIngresosCtrl'
            }).
            //Generar Comprobante Egresos  
            when('/GenerarComprobanteEgresos', {
                templateUrl: 'Aplicativo/Tesoreria/GenerarComprobanteEgresos.html',
                controller: 'GenerarComprobanteEgresosCtrl'
            }).
            when('/GenerarComprobanteEgresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/GenerarComprobanteEgresos.html',
                controller: 'GenerarComprobanteEgresosCtrl'
            }).
            //Generar Comprobante Ingresos  
            when('/GenerarComprobanteIngresos', {
                templateUrl: 'Aplicativo/Tesoreria/GenerarComprobanteIngresos.html',
                controller: 'GenerarComprobanteIngresosCtrl'
            }).
            when('/GenerarComprobanteIngresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/GenerarComprobanteIngresos.html',
                controller: 'GenerarComprobanteIngresosCtrl'
            }).
            //Pagos Masivos:

            when('/GenerarPagosMasivos/', {
                templateUrl: 'Aplicativo/Tesoreria/GenerarPagosMasivos.html',
                controller: 'GenerarPagosMasivosCtrl'
            }).

            //Comprobante Causación:
            when('/ConsultarComprobanteCausacion', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarComprobanteCausacion.html',
                controller: 'ConsultarComprobanteCausacionCtrl'
            }).
            when('/ConsultarComprobanteCausacion/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarComprobanteCausacion.html',
                controller: 'ConsultarComprobanteCausacionCtrl'
            }).
            when('/GestionarComprobanteCausacion', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarComprobanteCausacion.html',
                controller: 'GestionarComprobanteCausacionCtrl'
            }).
            when('/GestionarComprobanteCausacion/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarComprobanteCausacion.html',
                controller: 'GestionarComprobanteCausacionCtrl'
            }).

            //Cuentas Por Cobrar
            when('/ConsultarCuentasPorCobrar', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarCuentasPorCobrar.html',
                controller: 'ConsultarCuentasPorCobrarCtrl'
            }).
            when('/ConsultarCuentasPorCobrar/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarCuentasPorCobrar.html',
                controller: 'ConsultarCuentasPorCobrarCtrl'
            }).
            when('/GestionarCuentasPorCobrar', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarCuentasPorCobrar.html',
                controller: 'GestionarCuentasPorCobrarCtrl'
            }).
            when('/GestionarCuentasPorCobrar/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarCuentasPorCobrar.html',
                controller: 'GestionarCuentasPorCobrarCtrl'
            }).

            //listado Tesoreria 
            when('/ConsultarListadoTesoreria', {
                templateUrl: 'Aplicativo/Tesoreria/Listados/ConsultarListadoTesoreria.html',
                controller: 'ConsultarListadoTesoreriaCtrl'
            }).
            //listado Facturacion  
            when('/ConsultarListadoFacturacion', {
                templateUrl: 'Aplicativo/Facturacion/Listados/ConsultarListadoFacturacion.html',
                controller: 'ConsultarListadoFacturacionCtrl'
            }).
            //listado Facturacion Cartera
            when('/ConsultarListadoFacturaCartera', {
                templateUrl: 'Aplicativo/Facturacion/ListadosCartera/ConsultarListadoFacturaCartera.html',
                controller: 'ConsultarListadoFacturaCarteraCtrl'
            }).
            //---------------COMERCIAL-------------------//
            //Documentos
            //TarifarioVentas
            when('/ConsultarTarifarioVentas', {
                templateUrl: 'Aplicativo/Comercial/Documentos/ConsultarTarifarioVentas.html',
                controller: 'ConsultarTarifarioVentasCtrl'
            }).
            when('/ConsultarTarifarioVentas/:Codigo', {
                templateUrl: 'Aplicativo/Comercial/Documentos/ConsultarTarifarioVentas.html',
                controller: 'ConsultarTarifarioVentasCtrl'
            }).
            when('/GestionarTarifarioVentas', {
                templateUrl: 'Aplicativo/Comercial/Documentos/GestionarTarifarioVentas.html',
                controller: 'GestionarTarifarioVentasCtrl'
            }).
            when('/GestionarTarifarioVentas/:Codigo', {
                templateUrl: 'Aplicativo/Comercial/Documentos/GestionarTarifarioVentas.html',
                controller: 'GestionarTarifarioVentasCtrl'
            }).
            //TarifarioCompras
            when('/ConsultarTarifarioCompras', {
                templateUrl: 'Aplicativo/Comercial/Documentos/ConsultarTarifarioCompras.html',
                controller: 'ConsultarTarifarioComprasCtrl'
            }).
            when('/ConsultarTarifarioCompras/:Codigo', {
                templateUrl: 'Aplicativo/Comercial/Documentos/ConsultarTarifarioCompras.html',
                controller: 'ConsultarTarifarioComprasCtrl'
            }).
            when('/GestionarTarifarioCompras', {
                templateUrl: 'Aplicativo/Comercial/Documentos/GestionarTarifarioCompras.html',
                controller: 'GestionarTarifarioComprasCtrl'
            }).
            when('/GestionarTarifarioCompras/:Codigo', {
                templateUrl: 'Aplicativo/Comercial/Documentos/GestionarTarifarioCompras.html',
                controller: 'GestionarTarifarioComprasCtrl'
            }).
            //--Listados
            when('/ConsultarListadosComercial', {
                templateUrl: 'Aplicativo/Comercial/Listados/ConsultarListadosComercial.html',
                controller: 'ConsultarListadoComercialCtrl'
            }).
            //--Listados
            //---------------COMERCIAL-------------------//
            //Despachos
            //Despachos-Paqueteria
            //ConsultarGuiaPaqueterias
            when('/ConsultarGuiaPaqueterias', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Guias/ConsultarGuiaPaqueterias.html',
                controller: 'ConsultarGuiaPaqueteriasCtrl'
            }).
            when('/ConsultarGuiaPaqueterias/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Guias/ConsultarGuiaPaqueterias.html',
                controller: 'ConsultarGuiaPaqueteriasCtrl'
            }).
            when('/GestionarGuiaPaqueterias', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Guias/GestionarGuiaPaqueterias.html',
                controller: 'GestionarGuiaPaqueteriasCtrl'
            }).
            when('/GestionarGuiaPaqueterias/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Guias/GestionarGuiaPaqueterias.html',
                controller: 'GestionarGuiaPaqueteriasCtrl'
            }).
            when('/ConsultarListadoGuiaPaqueterias', {
                templateUrl: 'Aplicativo/Paqueteria/Listados/ConsultarListadoGuiaPaqueterias.html',
                controller: 'ConsultarListadoGuiaPaqueteriasCtrl'
            }).


            //Recepción Guías Oficina
            when('/RecepcionRemesaOficina', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/RecepcionRemesaOficina.html',
                controller: 'RecepcionRemesaOficinaCtrl'
            }).
            when('/RecepcionRemesaOficina/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/RecepcionRemesaOficina.html',
                controller: 'RecepcionRemesaOficinaCtrl'
            }).


            when('/ConsultarListadoDocumentosDespachos', {
                templateUrl: 'Aplicativo/Despachos/Listados/ConsultarListadoDespachos.html',
                controller: 'ConsultarListadoDespachosCtrl'
            }).


            //Indicadores Despachos:

            when('/ConsultarIndicadoresDespachos', {
                templateUrl: 'Aplicativo/Despachos/Indicadores/ConsultarIndicadoresDespachos.html',
                controller: 'ConsultarIndicadoresDespachosCtrl'
            }).


            //Indicadores Paquetería:

            when('/ConsultarIndicadoresPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Indicadores/ConsultarIndicadoresPaqueteria.html',
                controller: 'ConsultarIndicadoresPaqueteriaCtrl'
            }).
            // Vista de listas Paqueteria

            when('/ConsultarListadoGuiasPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Listados/ConsultarListadoGuiaPaqueterias.html',
                controller: 'ConsultarListadoGuiasPaqueteriasCtrl'
            }).

            //Listados Utilitarios
            when('/ConsultarListadosAuditoriaDocumentos', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarListadosAuditoriaDocumentos.html',
                controller: 'ConsultarListadosAuditoriaDocumentosCtrl'
            }).
            //Auditoria Terceros y vehiculos
            when('/ConsultarAuditoriaTercerosyVehiculos', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarAuditoriaTercerosyVehiculos.html',
                controller: 'ConsultarAuditoriaTerceroVehiculosCtrl'
            }).

            //Consultar Planillas guias
            when('/ConsultarPlanillaGuias', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaGuias/ConsultarPlanillaGuias.html',
                controller: 'ConsultarPlanillaGuiasCtrl'
            }).
            when('/ConsultarPlanillaGuias/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaGuias/ConsultarPlanillaGuias.html',
                controller: 'ConsultarPlanillaGuiasCtrl'
            }).
            when('/GestionarPlanillaGuias', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaGuias/GestionarPlanillaGuias.html',
                controller: 'GestionarPlanillaGuiasCtrl'
            }).
            when('/GestionarPlanillaGuias/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaGuias/GestionarPlanillaGuias.html',
                controller: 'GestionarPlanillaGuiasCtrl'
            }).

            //Consultar Planillas Entregada
            when('/ConsultarPlanillaEntregas', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaEntregas/ConsultarPlanillasEntregadas.html',
                controller: 'ConsultarPlanillaEntregadaCtrl'
            }).
            when('/ConsultarPlanillaEntregas/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaEntregas/ConsultarPlanillasEntregadas.html',
                controller: 'ConsultarPlanillaEntregadaCtrl'
            }).
            when('/GestionarPlanillaEntregadas', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaEntregas/GestionarPlanillasEntregadas.html',
                controller: 'GestionarPlanillaEntregadasCtrl'
            }).
            when('/GestionarPlanillaEntregadas/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/PlanillaEntregas/GestionarPlanillasEntregadas.html',
                controller: 'GestionarPlanillaEntregadasCtrl'
            }).

            //Cumplido Guias
            when('/CumplidoGuiasPaqueteria', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/CumplidoGuiasPaqueteria.html',
                controller: 'CumplidoGuiasPaqueteriaCtrl'
            }).
            //Consultar Liquidación
            when('/ConsultarLiquidarPlanillas', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/LiquidacionGuias/ConsultarLiquidacionGuias.html',
                controller: 'ConsultarLiquidacionGuiasCtrl'
            }).
            when('/ConsultarLiquidarPlanillas/:TIDO_Codigo/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/LiquidacionGuias/ConsultarLiquidacionGuias.html',
                controller: 'ConsultarLiquidacionGuiasCtrl'
            }).
            when('/GestionarLiquidarPlanillas', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/LiquidacionGuias/GestionarLiquidacionGuias.html',
                controller: 'GestionarLiquidacionGuiasCtrl'
            }).
            when('/GestionarLiquidarPlanillas/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/LiquidacionGuias/GestionarLiquidacionGuias.html',
                controller: 'GestionarLiquidacionGuiasCtrl'
            }).
            //-------------------------Paqueteria V2------------------------//
            //--Recolecciones
            when('/ConsultarRecoleccionesPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Recolecciones/ConsultarRecoleccionesPaqueteria.html',
                controller: 'ConsultarRecoleccionesPaqueteriaCtrl'
            }).
            when('/ConsultarRecoleccionesPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Recolecciones/ConsultarRecoleccionesPaqueteria.html',
                controller: 'ConsultarRecoleccionesPaqueteriaCtrl'
            }).
            when('/GestionarRecoleccionesPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Recolecciones/GestionarRecoleccionesPaqueteria.html',
                controller: 'GestionarRecoleccionesPaqueteriaCtrl'
            }).
            when('/GestionarRecoleccionesPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Recolecciones/GestionarRecoleccionesPaqueteria.html',
                controller: 'GestionarRecoleccionesPaqueteriaCtrl'
            }).
            //--Recolecciones
            //-----Remesas
            when('/ConsultarRemesasPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Remesa/ConsultarRemesasPaqueteria.html',
                controller: 'ConsultarRemesasPaqueteriaCtrl'
            }).
            when('/ConsultarRemesasPaqueteria/:Numero/:MeapCodigo', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Remesa/ConsultarRemesasPaqueteria.html',
                controller: 'ConsultarRemesasPaqueteriaCtrl'
            }).
            when('/GestionarRemesasPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Remesa/GestionarRemesasPaqueteria.html',
                controller: 'GestionarRemesasPaqueteriaCtrl'
            }).
            when('/GestionarRemesasPaqueteria/:Numero/:MeapCodigo', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Remesa/GestionarRemesasPaqueteria.html',
                controller: 'GestionarRemesasPaqueteriaCtrl'
            }).
            when('/EntregaOficina/:Codigo', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Remesa/EntregaOficina.html',
                controller: 'EntregasPaqueteriaOficinaCtrl'
            }).

            //--Gesphone
            //when('/ConsultarRemesasPaqueteriaGesphone', {
            //    templateUrl: 'Gesphone/Paqueteria/Remesas/ConsultarRemesasPaqueteriaGesphone.html',
            //    controller: 'ConsultarRemesasPaqueteriaGesphoneCtrl'
            //}).
            //when('/ConsultarRemesasPaqueteriaGesphone/:Numero', {
            //    templateUrl: 'Gesphone/Paqueteria/Remesas/ConsultarRemesasPaqueteriaGesphone.html',
            //    controller: 'ConsultarRemesasPaqueteriaGesphoneCtrl'
            //}).
            //when('/GestionarRemesasPaqueteriaGesphone', {
            //    templateUrl: 'Gesphone/Paqueteria/Remesas/GestionarRemesasPaqueteriaGesphone.html',
            //    controller: 'GestionarRemesasPaqueteriaGesphoneCtrl'
            //}).
            //when('/GestionarRemesasPaqueteriaGesphone/:Numero', {
            //    templateUrl: 'Gesphone/Paqueteria/Remesas/GestionarRemesasPaqueteriaGesphone.html',
            //    controller: 'GestionarRemesasPaqueteriaGesphoneCtrl'
            //}).
            //--Gesphone
            //-----Remesas
            //--Planilla
            when('/ConsultarPlanillaPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/ConsultarPlanillaPaqueteria.html',
                controller: 'ConsultarPlanillaPaqueteriaCtrl'
            }).
            when('/ConsultarPlanillaPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/ConsultarPlanillaPaqueteria.html',
                controller: 'ConsultarPlanillaPaqueteriaCtrl'
            }).
            when('/GestionarPlanillaPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/GestionarPlanillaPaqueteria.html',
                controller: 'GestionarPlanillaPaqueteriaCtrl'
            }).
            when('/GestionarPlanillaPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/GestionarPlanillaPaqueteria.html',
                controller: 'GestionarPlanillaPaqueteriaCtrl'
            }).


            //--Planilla Cargue/Descargue:
            when('/ConsultarPlanillaCargueDescargue', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/ConsultarPlanillaCargueDescargue.html',
                controller: 'ConsultarPlanillaCargueDescargueCtrl'
            }).
            when('/ConsultarPlanillaCargueDescargue/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/ConsultarPlanillaCargueDescargue.html',
                controller: 'ConsultarPlanillaCargueDescargueCtrl'
            }).
            when('/GestionarPlanillaCargueDescargue', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/GestionarPlanillaCargueDescargue.html',
                controller: 'GestionarPlanillaCargueDescargueCtrl'
            }).
            when('/GestionarPlanillaCargueDescargue/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/GestionarPlanillaCargueDescargue.html',
                controller: 'GestionarPlanillaCargueDescargueCtrl'
            }).


            //--Planilla Descargue: 
            when('/ConsultarPlanillaDescargue', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/ConsultarPlanillaDescargue.html',
                controller: 'ConsultarPlanillaDescargueCtrl'
            }).
            when('/ConsultarPlanillaDescargue/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/ConsultarPlanillaDescargue.html',
                controller: 'ConsultarPlanillaDescargueCtrl'
            }).
            when('/GestionarPlanillaDescargue', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/GestionarPlanillaDescargue.html',
                controller: 'GestionarPlanillaDescargueCtrl'
            }).
            when('/GestionarPlanillaDescargue/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/GestionarPlanillaDescargue.html',
                controller: 'GestionarPlanillaDescargueCtrl'
            }).

            //--Planilla Cargue: 
            when('/ConsultarPlanillaCargue', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/ConsultarPlanillaCargue.html',
                controller: 'ConsultarPlanillaCargueCtrl'
            }).
            when('/ConsultarPlanillaCargue/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/ConsultarPlanillaCargue.html',
                controller: 'ConsultarPlanillaCargueCtrl'
            }).
            when('/GestionarPlanillaCargue', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/GestionarPlanillaCargue.html',
                controller: 'GestionarPlanillaCargueCtrl'
            }).
            when('/GestionarPlanillaCargue/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Planilla/GestionarPlanillaCargue.html',
                controller: 'GestionarPlanillaCargueCtrl'
            }).




            //ManifiestoCarga:        
            when('/ConsultarManifiestoCargaPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Manifiestos/ConsultarManifiestoCargaPaqueteria.html',
                controller: 'ConsultarManifiestoCargaPaqueteriaCtrl'
            }).
            when('/ConsultarManifiestoCargaPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Manifiestos/ConsultarManifiestoCargaPaqueteria.html',
                controller: 'ConsultarManifiestoCargaPaqueteriaCtrl'
            }).
            when('/GestionarManifiestoCargaPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Manifiestos/GestionarManifiestoCargaPaqueteria.html',
                controller: 'GestionarManifiestoCargaPaqueteriaCtrl'
            }).
            when('/GestionarManifiestoCargaPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Manifiestos/GestionarManifiestoCargaPaqueteria.html',
                controller: 'GestionarManifiestoCargaPaqueteriaCtrl'
            }).


            //--Liquidacion
            when('/ConsultarLiquidacionPlanillaPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Liquidacion/ConsultarLiquidacionPlanillaPaqueteria.html',
                controller: 'ConsultarLiquidacionPlanillaPaqueteriaCtrl'
            }).
            when('/ConsultarLiquidacionPlanillaPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Liquidacion/ConsultarLiquidacionPlanillaPaqueteria.html',
                controller: 'ConsultarLiquidacionPlanillaPaqueteriaCtrl'
            }).
            when('/GestionarLiquidacionPlanillaPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Liquidacion/GestionarLiquidacionPlanillaPaqueteria.html',
                controller: 'GestionarLiquidacionPlanillaPaqueteriaCtrl'
            }).
            when('/GestionarLiquidacionPlanillaPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Liquidacion/GestionarLiquidacionPlanillaPaqueteria.html',
                controller: 'GestionarLiquidacionPlanillaPaqueteriaCtrl'
            }).
            //--Liquidacion
            when('/ConsultarLegalizacionPlanillaPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Legalizacion/ConsultarLegalizacionPlanillaPaqueteria.html',
                controller: 'ConsultarLegalizacionPlanillaPaqueteriaCtrl'
            }).
            when('/ConsultarLegalizacionPlanillaPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Legalizacion/ConsultarLegalizacionPlanillaPaqueteria.html',
                controller: 'ConsultarLegalizacionPlanillaPaqueteriaCtrl'
            }).
            when('/GestionarLegalizacionPlanillaPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Legalizacion/GestionarLegalizacionPlanillaPaqueteria.html',
                controller: 'GestionarLegalizacionPlanillaPaqueteriaCtrl'
            }).
            when('/GestionarLegalizacionPlanillaPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Legalizacion/GestionarLegalizacionPlanillaPaqueteria.html',
                controller: 'GestionarLegalizacionPlanillaPaqueteriaCtrl'
            }).
            //--Legalizar Guias
            when('/ConsultarLegalizarRemesasPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/LegalizacionRemesas/ConsultarLegalizarRemesasPaqueteria.html',
                controller: 'ConsultarLegalizarRemesasPaqueteriaCtrl'
            }).
            when('/ConsultarLegalizarRemesasPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/LegalizacionRemesas/ConsultarLegalizarRemesasPaqueteria.html',
                controller: 'ConsultarLegalizarRemesasPaqueteriaCtrl'
            }).
            when('/GestionarLegalizarRemesasPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/LegalizacionRemesas/GestionarLegalizarRemesasPaqueteria.html',
                controller: 'GestionarLegalizarRemesasPaqueteriaCtrl'
            }).
            when('/GestionarLegalizarRemesasPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/LegalizacionRemesas/GestionarLegalizarRemesasPaqueteria.html',
                controller: 'GestionarLegalizarRemesasPaqueteriaCtrl'
            }).
            //--Legalizar Guias
            //--Legalizar Recaudo
            when('/ConsultarLegalizarRecaudoRemesasPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/LegalizarRecaudo/ConsultarLegalizarRecaudoRemesasPaqueteria.html',
                controller: 'ConsultarLegalizarRecaudoRemesasPaqueteriaCtrl'
            }).
            when('/ConsultarLegalizarRecaudoRemesasPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/LegalizarRecaudo/ConsultarLegalizarRecaudoRemesasPaqueteria.html',
                controller: 'ConsultarLegalizarRecaudoRemesasPaqueteriaCtrl'
            }).
            when('/GestionarLegalizarRecaudoRemesasPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/LegalizarRecaudo/GestionarLegalizarRecaudoRemesasPaqueteria.html',
                controller: 'GestionarLegalizarRecaudoRemesasPaqueteriaCtrl'
            }).
            when('/GestionarLegalizarRecaudoRemesasPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/LegalizarRecaudo/GestionarLegalizarRecaudoRemesasPaqueteria.html',
                controller: 'GestionarLegalizarRecaudoRemesasPaqueteriaCtrl'
            }).
            //--Legalizar Recaudo
            //--Planificacion Planillas
            when('/PlanificacionDespachosPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/PlanificacionDespachosPaqueteria.html',
                controller: 'PlanificacionDespachosPaqueteriaCtrl'
            }).
            //--Planificacion Planillas
            //--RecepcionPlanilla
            when('/RecepcionPlanillasPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/RecepcionRemesasPaqueteria.html',
                controller: 'RecepcionRemesasPaqueteriaCtrl'
            }).
            when('/RecepcionPlanillasPaqueteria/:Codigo', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/RecepcionRemesasPaqueteria.html',
                controller: 'RecepcionRemesasPaqueteriaCtrl'
            }).

            when('/CambiarEstadoDefinitivoRemesasV2', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/CambiarEstadoDefinitivoRemesasV2.html',
                controller: 'CambiarEstadoDefinitivoRemesasV2Ctrl'
            }).
            when('/ConsultarAsignacionOficinasGuiasPreimpresas', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/ConsultarGuiasPreimpresas.html',
                controller: 'ConsultarGuiasPreimpresasCtrl'
            }).
            when('/ConsultarAsignacionOficinasGuiasPreimpresas/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/ConsultarGuiasPreimpresas.html',
                controller: 'ConsultarGuiasPreimpresasCtrl'
            }).
            when('/GestionarGuiasPreimpresas', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/GestionarGuiasPreimpresas.html',
                controller: 'GestionarGuiasPreimpresasCtrl'
            }).
            when('/GestionarGuiasPreimpresas/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/GestionarGuiasPreimpresas.html',
                controller: 'GestionarGuiasPreimpresasCtrl'
            }).

            when('/ConsultarAsignacionAforadorGuiasPreimpresas', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/AsignarAforadorGuiasPreimpresas.html',
                controller: 'AsignarAforadorGuiasPreimpresasCtrl'
            }).


            when('/ConsultarAsignacionOficinasEtiquetasPreimpresas', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/ConsultarEtiquetasPreimpresas.html',
                controller: 'ConsultarEtiquetasPreimpresasCtrl'
            }).
            when('/ConsultarAsignacionOficinasEtiquetasPreimpresas/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/ConsultarEtiquetasPreimpresas.html',
                controller: 'ConsultarEtiquetasPreimpresasCtrl'
            }).
            when('/GestionarEtiquetasPreimpresas', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/GestionarEtiquetasPreimpresas.html',
                controller: 'GestionarEtiquetasPreimpresasCtrl'
            }).
            when('/GestionarEtiquetasPreimpresas/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/GestionarEtiquetasPreimpresas.html',
                controller: 'GestionarEtiquetasPreimpresasCtrl'
            }).
            when('/ConsultarAsignacionAforadorEtiquetasPreimpresas', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/AsignarAforadorEtiquetasPreimpresas.html',
                controller: 'AsignarAforadorEtiquetasPreimpresasCtrl'
            }).



            //--RecepcionPlanilla

            //CumplidoDespachos:

            when('/ConsultarCumplidoDespachosPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Cumplidos/ConsultarCumplidoDespachosPaqueteria.html',
                controller: 'ConsultarCumplidoDespachosPaqueteriaCtrl'
            }).
            when('/ConsultarCumplidoDespachosPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Cumplidos/ConsultarCumplidoDespachosPaqueteria.html',
                controller: 'ConsultarCumplidoDespachosPaqueteriaCtrl'
            }).
            when('/GestionarCumplidoDespachosPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Cumplidos/GestionarCumplidoDespachosPaqueteria.html',
                controller: 'GestionarCumplidoDespachosPaqueteriaCtrl'
            }).
            when('/GestionarCumplidoDespachosPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Documentos/Cumplidos/GestionarCumplidoDespachosPaqueteria.html',
                controller: 'GestionarCumplidoDespachosPaqueteriaCtrl'
            }).


            //CumplidosPaqueteriaV1:
            when('/ConsultarCumplidoDespachoPaqueteria', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Cumplidos/ConsultarCumplidoDespachoPaqueteria.html',
                controller: 'ConsultarCumplidoDespachoPaqueteriaCtrl'
            }).
            when('/ConsultarCumplidoDespachoPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Cumplidos/ConsultarCumplidoDespachoPaqueteria.html',
                controller: 'ConsultarCumplidoDespachoPaqueteriaCtrl'
            }).
            when('/GestionarCumplidoDespachoPaqueteria', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Cumplidos/GestionarCumplidoDespachoPaqueteria.html',
                controller: 'GestionarCumplidoDespachoPaqueteriaCtrl'
            }).
            when('/GestionarCumplidoDespachoPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Cumplidos/GestionarCumplidoDespachoPaqueteria.html',
                controller: 'GestionarCumplidoDespachoPaqueteriaCtrl'
            }).
            //--Recibir Remesas Oficina Destino
            when('/RecibirGuiasOficinaDestino', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/RecibirRemesasOficinaDestino.html',
                controller: 'RecibirGuiasOficinaDestinoCtrl'
            }).
            when('/RecibirGuiasOficinaDestino/:Numero', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/RecibirRemesasOficinaDestino.html',
                controller: 'RecibirGuiasOficinaDestinoCtrl'
            }).
            //--Recibir Remesas Oficina Destino
            //-------------------------Paqueteria V2------------------------//
            //Despachar Orden Servicio
            when('/ConsultarDespacharOrdenServicios', {
                templateUrl: 'Aplicativo/Despachos/Procesos/DespacharOrdenServicios/ConsultarDespacharOrdenServicios.html',
                controller: 'ConsultarDespacharOrdenServiciosCtrl'
            }).
            when('/ConsultarDespacharOrdenServicios/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Procesos/DespacharOrdenServicios/ConsultarDespacharOrdenServicios.html',
                controller: 'ConsultarDespacharOrdenServiciosCtrl'
            }).
            when('/GestionarDespacharOrdenServicios/:Numero/:Codigo', {
                templateUrl: 'Aplicativo/Despachos/Procesos/DespacharOrdenServicios/GestionarDespacharOrdenServicios.html',
                controller: 'GestionarDespacharOrdenServiciosCtrl'
            }).
            when('/GestionarDespacharOrdenServicios/:Numero/:Codigo/:Programacion/:IdDetalle', {
                templateUrl: 'Aplicativo/Despachos/Procesos/DespacharOrdenServicios/GestionarDespacharOrdenServicios.html',
                controller: 'GestionarDespacharOrdenServiciosCtrl'
            }).
            when('/GestionarDespacharProgramacionOrdenServicios/:Numero/:Codigo', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ProgramarOrdenServicios/GestionarProcesoProgramacionOrdenServicio.html',
                controller: 'GestionarDespacharProgramacionOrdenServiciosCtrl'
            }).
            when('/GestionarDespacharProgramacionOrdenServicios/:Numero/:Codigo/:Programacion/:IdDetalle', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ProgramarOrdenServicios/GestionarProcesoProgramacionOrdenServicio.html',
                controller: 'GestionarDespacharProgramacionOrdenServiciosCtrl'
            }).
            when('/GestionarGenerarDespacharOrdenServicios/:Numero/:Codigo', {
                templateUrl: 'Aplicativo/Despachos/Procesos/DespacharOrdenServicios/GestionarGenerarDespacharOrdenServicios.html',
                controller: 'GestionarDespacharOrdenServiciosCtrl'
            }).
            when('/GestionarGenerarDespacharOrdenServicios/:Numero/:Codigo/:Programacion/:IdDetalle', {
                templateUrl: 'Aplicativo/Despachos/Procesos/DespacharOrdenServicios/GestionarGenerarDespacharOrdenServicios.html',
                controller: 'GestionarDespacharOrdenServiciosCtrl'
            }).
            when('/GestionarEnturnamiento', {
                templateUrl: 'Aplicativo/Despachos/Procesos/Enturnamiento/GestionarConsultaEntunramiento.html',
                controller: 'GestionarConsultaEntunramientoCtrl'
            }).
            //Manifiestos
            when('/ConsultarManifiestos', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Manifiestos/ConsultarManifiestos.html',
                controller: 'ConsultarManifiestosCtrl'
            }).
            when('/ConsultarManifiestos/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Manifiestos/ConsultarManifiestos.html',
                controller: 'ConsultarManifiestosCtrl'
            }).
            when('/GestionarManifiesto', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Manifiestos/GestionarManifiesto.html',
                controller: 'GestionarManifiestoCtrl'
            }).
            when('/GestionarManifiesto/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Manifiestos/GestionarManifiesto.html',
                controller: 'GestionarManifiestoCtrl'
            }).

            // Precintos Despachos - Paqueria
            when('/ConsultarUnicoPrecinto', {
                templateUrl: 'Aplicativo/Despachos/Consultas/ConsultarUnicosPrecintos.html',
                controller: 'ConsultarPrecintosUnicosCtrl'
            }).

            when('/ConsultarUnicoPrecinto/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Consultas/ConsultarUnicosPrecintos.html',
                controller: 'ConsultarPrecintosUnicosCtrl'
            }).

            when('/ConsultarUnicoPrecinto/:Numero/:MeapCodigo', {
                templateUrl: 'Aplicativo/Despachos/Consultas/ConsultarUnicosPrecintos.html',
                controller: 'ConsultarPrecintosUnicosCtrl'
            }).

            //ControlContenedores:
            when('/ConsultarControlContenedores', {
                templateUrl: 'Aplicativo/Despachos/Consultas/ConsultarControlContenedores.html',
                controller: 'ConsultarControlContenedoresCtrl'
            }).


            // Detalle Novedades Despachos 
            when('/ConsultarDetalleNovedadesDespacho', {
                templateUrl: 'Aplicativo/Despachos/Consultas/ConsultarDetalleNovedadesDespacho.html',
                controller: 'ConsultarDetalleNovedadesDespachoCtrl'
            }).

            //Alertas Documentos
            when('/ConsultarAlertasDocumentos', {
                templateUrl: 'Aplicativo/Despachos/Consultas/ConsultarAlertasDocumentos.html',
                controller: 'ConsultarAlertasDocumentosCtrl'
            }).

            // Marcar Remesas Facturadas
            when('/MarcarRemesasFacturadas', {
                templateUrl: 'Aplicativo/Facturacion/Procesos/MarcarRemesasFacturadas.html',
                controller: 'MarcarRemesasFacturadasCtrl'
            }).

            //Orden Cargue
            when('/ConsultarOrdenCargue', {
                templateUrl: 'Aplicativo/Despachos/Documentos/OrdenCargues/ConsultarOrdenCargue.html',
                controller: 'ConsultarOrdenCargueCtrl'
            }).
            when('/ConsultarOrdenCargue/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/OrdenCargues/ConsultarOrdenCargue.html',
                controller: 'ConsultarOrdenCargueCtrl'
            }).
            when('/GestionarOrdenCargue', {
                templateUrl: 'Aplicativo/Despachos/Documentos/OrdenCargues/GestionarOrdenCargue.html',
                controller: 'GestionarOrdenCargueCtrl'
            }).
            when('/GestionarOrdenCargue/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/OrdenCargues/GestionarOrdenCargue.html',
                controller: 'GestionarOrdenCargueCtrl'
            }).
            //Planilla Despacho
            when('/ConsultarPlanillasDespachos', {
                templateUrl: 'Aplicativo/Despachos/Documentos/PlanillaDespachos/ConsultarPlanillaDespachos.html',
                controller: 'ConsultarPlanillaDespachoCtrl'
            }).
            when('/ConsultarPlanillasDespachos/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/PlanillaDespachos/ConsultarPlanillaDespachos.html',
                controller: 'ConsultarPlanillaDespachoCtrl'
            }).
            when('/GestionarPlanillasDespachos', {
                templateUrl: 'Aplicativo/Despachos/Documentos/PlanillaDespachos/GestionarPlanillaDespachos.html',
                controller: 'GestionarPlanillaDespachoCtrl'
            }).
            when('/GestionarPlanillasDespachos/:Codigo', {
                templateUrl: 'Aplicativo/Despachos/Documentos/PlanillaDespachos/GestionarPlanillaDespachos.html',
                controller: 'GestionarPlanillaDespachoCtrl'
            }).

            //Remesas
            when('/ConsultarRemesas', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Remesas/ConsultarRemesas.html',
                controller: 'ConsultarRemesasCtrl'
            }).
            when('/ConsultarRemesas/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Remesas/ConsultarRemesas.html',
                controller: 'ConsultarRemesasCtrl'
            }).
            when('/GestionarRemesa', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Remesas/GestionarRemesa.html',
                controller: 'GestionarRemesaCtrl'
            }).
            when('/GestionarRemesa/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Remesas/GestionarRemesa.html',
                controller: 'GestionarRemesaCtrl'
            }).
            //PROCESOS
            //Cumplir remesas
            when('/CumplidoRemesas', {
                templateUrl: 'Aplicativo/Despachos/Procesos/CumplidoRemesas.html',
                controller: 'CumplidoRemesasCtrl'
            }).

            // Cumplir remesas paquetería V2:
            //when('/CumplirGuiasPaqueteria', {
            //    templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/CumplirRemesasPaqueteria.html',
            //    controller: 'CumplirRemesasPaqueteriaCtrl'
            //}).
            //Aprobar Liquidación
            when('/AprobarLiquidacionesDespacho', {
                templateUrl: 'Aplicativo/Despachos/Procesos/AprobarLiquidaciones/AprobarLiquidaciones.html',
                controller: 'AprobarLiquidacionesCtrl'
            }).
            when('/AprobarLiquidacionesDespacho/:Codigo', {
                templateUrl: 'Aplicativo/Despachos/Procesos/AprobarLiquidaciones/AprobarLiquidaciones.html',
                controller: 'AprobarLiquidacionesCtrl'
            }).
            //Importar Archivo Entregas y DT:
            when('/ImportarArchivoEntregasyDT', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ImportarArchivoEntregasyDT.html',
                controller: 'ImportarArchivoEntregasyDTCtrl'
            }).

            //Cumplido Planilla Despacho Masivo
            when('/ConsultarCumplidos', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Cumplidos/ConsultarCumplidos.html',
                controller: 'ConsultarCumplidosCtrl'
            }).
            when('/ConsultarCumplidos/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Cumplidos/ConsultarCumplidos.html',
                controller: 'ConsultarCumplidosCtrl'
            }).
            when('/GestionarCumplido', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Cumplidos/GestionarCumplido.html',
                controller: 'GestionarCumplidoCtrl'
            }).
            when('/GestionarCumplido/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Cumplidos/GestionarCumplido.html',
                controller: 'GestionarCumplidoCtrl'
            }).

            //Liquidación Planilla Despacho Masivo
            when('/ConsultarLiquidaciones', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Liquidaciones/ConsultarLiquidaciones.html',
                controller: 'ConsultarLiquidacionesCtrl'
            }).
            when('/ConsultarLiquidaciones/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Liquidaciones/ConsultarLiquidaciones.html',
                controller: 'ConsultarLiquidacionesCtrl'
            }).
            when('/GestionarLiquidacion', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Liquidaciones/GestionarLiquidacion.html',
                controller: 'GestionarLiquidacionCtrl'
            }).
            when('/GestionarLiquidacion/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Liquidaciones/GestionarLiquidacion.html',
                controller: 'GestionarLiquidacionCtrl'
            }).

            //Reportar Despachos RNDC
            when('/ReportarDespachosMinisterio', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ReportarDespachosMinisterio.html',
                controller: 'ReportarDespachosMinisterioCtrl'
            }).
            when('/ReportarDespachosMinisterio/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ReportarDespachosMinisterio.html',
                controller: 'ReportarDespachosMinisterioCtrl'
            }).

            //Novedad Despachos
            when('/NovedadesDespacho', {
                templateUrl: 'Aplicativo/Despachos/Procesos/NovedadesDespacho.html',
                controller: 'NovedadesDespachoCtrl'
            }).
            when('/NovedadesDespacho/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Procesos/NovedadesDespacho.html',
                controller: 'NovedadesDespachoCtrl'
            }).

            //Factura Venta
            when('/ConsultarFacturaVentas', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/ConsultarFacturaVentas.html',
                controller: 'ConsultarFacturaVentasCtrl'
            }).
            when('/ConsultarFacturaVentas/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/ConsultarFacturaVentas.html',
                controller: 'ConsultarFacturaVentasCtrl'
            }).
            when('/GestionarFacturaVentas', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/GestionarFacturaVentas.html',
                controller: 'GestionarFacturaVentasCtrl'
            }).
            when('/GestionarFacturaVentas/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/GestionarFacturaVentas.html',
                controller: 'GestionarFacturaVentasCtrl'
            }).

            //Factura Otros Conceptos
            when('/ConsultarFacturaOtrosConceptos', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/ConsultarFacturaOtrosConceptos.html',
                controller: 'ConsultarFacturaOtrosConceptosCtrl'
            }).
            when('/ConsultarFacturaOtrosConceptos/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/ConsultarFacturaOtrosConceptos.html',
                controller: 'ConsultarFacturaOtrosConceptosCtrl'
            }).
            when('/GestionarFacturaOtrosConceptos', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/GestionarFacturaOtrosConceptos.html',
                controller: 'GestionarFacturaOtrosConceptosCtrl'
            }).
            when('/GestionarFacturaOtrosConceptos/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/GestionarFacturaOtrosConceptos.html',
                controller: 'GestionarFacturaOtrosConceptosCtrl'
            }).

            //Notas Credito Factura
            when('/ConsultarNotaFacturas', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/ConsultarNotaFacturas.html',
                controller: 'ConsultarNotaFacturasCtrl'
            }).
            when('/ConsultarNotaFacturas/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/ConsultarNotaFacturas.html',
                controller: 'ConsultarNotaFacturasCtrl'
            }).
            when('/GestionarNotaFacturas', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/GestionarNotaFacturas.html',
                controller: 'GestionarNotaFacturasCtrl'
            }).
            when('/GestionarNotaFacturas/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/Documentos/GestionarNotaFacturas.html',
                controller: 'GestionarNotaFacturasCtrl'
            }).

            //Reporte Masivo Factura Electronica
            when('/ReporteMasivoFacturaElectronica', {
                templateUrl: 'Aplicativo/Facturacion/Procesos/ReporteMasivoFacturaElectronica.html',
                controller: 'ReporteMasivoFacturaElectronicaCtrl'
            }).
            when('/ReporteMasivoFacturaElectronica/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/Procesos/ReporteMasivoFacturaElectronica.html',
                controller: 'ReporteMasivoFacturaElectronicaCtrl'
            }).

            // Seguridad Usuarios Usuarios 
            when('/ConsultarUsuarios', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarUsuarios.html',
                controller: 'ConsultarUsuariosCtrl'
            }).
            when('/ConsultarUsuarios/:Codigo', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarUsuarios.html',
                controller: 'ConsultarUsuariosCtrl'
            }).
            when('/GestionarUsuarios', {
                templateUrl: 'Aplicativo/Seguridad/GestionarUsuarios.html',
                controller: 'GestionarUsuariosCtrl'
            }).
            when('/GestionarUsuarios/:Codigo', {
                templateUrl: 'Aplicativo/Seguridad/GestionarUsuarios.html',
                controller: 'GestionarUsuariosCtrl'
            }).

            //Seguridad Usuarios Perfil/Grupos
            when('/ConsultarGrupoPerfiles', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarGrupos.html',
                controller: 'ConsultarGrupoPerfilesCtrl'
            }).
            when('/ConsultarGrupoPerfiles/:Codigo', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarGrupos.html',
                controller: 'ConsultarGrupoPerfilesCtrl'
            }).
            when('/GestionarGrupoPerfiles', {
                templateUrl: 'Aplicativo/Seguridad/GestionarGrupos.html',
                controller: 'GestionarGrupoPerfilesCtrl'
            }).
            when('/GestionarGrupoPerfiles/:Codigo', {
                templateUrl: 'Aplicativo/Seguridad/GestionarGrupos.html',
                controller: 'GestionarGrupoPerfilesCtrl'
            }).


            //Utilitarios Bandeja Salida Correos
            when('/ConsultarBandejaSalidaCorreos', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarBandejaSalidaCorreo.html',
                controller: 'ConsultarBandejaSalidaCorreosCtrl'
            }).
            when('/ConsultarBandejaSalidaCorreos/:Codigo', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarBandejaSalidaCorreo.html',
                controller: 'ConsultarBandejaSalidaCorreosCtrl'
            }).
            //Utilitarios Configuracion servidor Correos
            when('/ConsultarConfiguracionServidorCorreos', {
                templateUrl: 'Aplicativo/Utilitarios/GestionarConfiguracionServidorCorreos.html',
                controller: 'GestionarConfiguracionServidorCorreosCtrl'
            }).
            when('/ConsultarConfiguracionServidorCorreos/:Codigo', {
                templateUrl: 'Aplicativo/Utilitarios/GestionarConfiguracionServidorCorreos.html',
                controller: 'GestionarConfiguracionServidorCorreosCtrl'
            }).
            //Utilitarios Lista Dristibución Correos
            when('/ConsultarListaDistribucionCorreos', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarListaDistribucionCorreos.html',
                controller: 'ConsultarListaDistribucionCorreosCtrl'
            }).
            when('/ConsultarListaDistribucionCorreos/:Codigo', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarListaDistribucionCorreos.html',
                controller: 'ConsultarListaDistribucionCorreosCtrl'
            }).
            when('/GestionarListaDistribucionCorreos', {
                templateUrl: 'Aplicativo/Utilitarios/GestionarListaDistribucionCorreos.html',
                controller: 'GestionarListaDistribucionCorreosCtrl'
            }).
            when('/GestionarListaDistribucionCorreos/:Codigo', {
                templateUrl: 'Aplicativo/Utilitarios/GestionarListaDistribucionCorreos.html',
                controller: 'GestionarListaDistribucionCorreosCtrl'
            }).

            //Utilitarios Notificaciones :

            when('/ConsultarAsignacionPerfiles', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarAsignacionPerfiles.html',
                controller: 'ConsultarAsignacionPerfilesCtrl'
            }).
            //Orden Servicio
            when('/ConsultarOrdenservicio', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarOrdenServicio.html',
                controller: 'ConsultarOrdenServicioCtrl'
            }).
            when('/ConsultarOrdenservicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarOrdenServicio.html',
                controller: 'ConsultarOrdenServicioCtrl'
            }).
            when('/GestionarOrdenservicio', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarOrdenServicio.html',
                controller: 'GestionarOrdenServicioCtrl'
            }).
            when('/GestionarOrdenservicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarOrdenServicio.html',
                controller: 'GestionarOrdenServicioCtrl'
            }).

            //Solicitud Orden Servicio
            when('/ConsultarSolicitudOrdenServicio', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarSolicitudOrdenServicio.html',
                controller: 'ConsultarSolicitudOrdenServicioCtrl'
            }).
            when('/ConsultarSolicitudOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarSolicitudOrdenServicio.html',
                controller: 'ConsultarSolicitudOrdenServicioCtrl'
            }).
            when('/ConsultarSolicitudOrdenServicio/:Numero/:Tipo', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarSolicitudOrdenServicio.html',
                controller: 'ConsultarSolicitudOrdenServicioCtrl'
            }).
            when('/GestionarSolicitudOrdenservicio', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarSolicitudOrdenServicio.html',
                controller: 'GestionarSolicitudOrdenServicioCtrl'
            }).
            when('/GestionarSolicitudOrdenservicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarSolicitudOrdenServicio.html',
                controller: 'GestionarSolicitudOrdenServicioCtrl'
            }).
            when('/GestionarSolicitudOrdenservicio/:Numero/:Tipo', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarSolicitudOrdenServicio.html',
                controller: 'GestionarSolicitudOrdenServicioCtrl'
            }).

            //Generar Orden Servicio
            when('/ConsultarGenerarOrdenServicio', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarGenerarOrdenServicio.html',
                controller: 'ConsultarGenerarOrdenServicioCtrl'
            }).
            when('/ConsultarGenerarOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarGenerarOrdenServicio.html',
                controller: 'ConsultarGenerarOrdenServicioCtrl'
            }).
            when('/GestionarGenerarOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarGenerarOrdenServicio.html',
                controller: 'GestionarGenerarOrdenServicioCtrl'
            }).
            when('/GestionarGenerarOrdenServicio/:Numero/:Generar', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarGenerarOrdenServicio.html',
                controller: 'GestionarGenerarOrdenServicioCtrl'
            }).

            //Prorgamar Orden Servicio
            when('/ConsultarProgramarOrdenServicio', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarProgramacionOrdenServicio.html',
                controller: 'ConsultarProgramacionOrdenServicioCtrl'
            }).
            when('/ConsultarProgramarOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarProgramacionOrdenServicio.html',
                controller: 'ConsultarProgramacionOrdenServicioCtrl'
            }).
            when('/GestionarPorgrmacionOrdenServicio', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarProgramacionOrdenServicio.html',
                controller: 'GestionarProgramacionOrdenServicioCtrl'
            }).
            when('/GestionarPorgrmacionOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarProgramacionOrdenServicio.html',
                controller: 'GestionarProgramacionOrdenServicioCtrl'
            }).

            //Cierre Orden Servicio
            when('/ConsultarCerrarOrdenServicio', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarCerrarOrdenServicio.html',
                controller: 'ConsultarCierreOrdenServicioCtrl'
            }).
            when('/ConsultarCerrarOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarCerrarOrdenServicio.html',
                controller: 'ConsultarCierreOrdenServicioCtrl'
            }).


            when('/ConsultarProcesoProgramarOrdenServicio', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ProgramarOrdenServicios/ConsultarProcesoProgramacionOrdenServicio.html',
                controller: 'ConsultarProcesoProgramacionOrdenServicioCtrl'
            }).
            when('/ConsultarProcesoProgramarOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ProgramarOrdenServicios/ConsultarProcesoProgramacionOrdenServicio.html',
                controller: 'ConsultarProcesoProgramacionOrdenServicioCtrl'
            }).
            when('/GestionarProcesoPorgrmacionOrdenServicio', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ProgramarOrdenServicios/GestionarProcesoProgramacionOrdenServicio.html',
                controller: 'GestionarProcesoProgramacionOrdenServicioCtrl'
            }).
            when('/GestionarProcesoPorgrmacionOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ProgramarOrdenServicios/GestionarProcesoProgramacionOrdenServicio.html',
                controller: 'GestionarProcesoProgramacionOrdenServicioCtrl'
            }).
            when('/GestionarProcesoPorgrmacionOrdenServicio/:Numero/:IdDetalle', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ProgramarOrdenServicios/GestionarProcesoProgramacionOrdenServicio.html',
                controller: 'GestionarProcesoProgramacionOrdenServicioCtrl'
            }).


            //Cargar Remesas
            when('/CargarArchivoRemesas', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/CargarRemesasPaqueteria.html',
                controller: 'CargarRemesasPaqueteriaCtrl'
            }).

            //Cargar Remesas Paqueteria V2:
            when('/CargarArchivoGuiasPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/CargarArchivoRemesasPaqueteria.html',
                controller: 'CargarArchivoRemesasPaqueteriaCtrl'
            }).
            when('/CargarArchivoGuiasCliente', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/CargueMasivoGuiasCliente.html',
                controller: 'CargueMasivoGuiasClienteCtrl'
            }).
            when('/CargarArchivoRecoleccionesCliente', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/CargueMasivoRecoleccionesCliente.html',
                controller: 'CargueMasivoRecoleccionesClienteCtrl'
            }).
            //Cargar Tarifas
            when('/CargarTarifario', {
                templateUrl: 'Aplicativo/Comercial/Procesos/CargarTarifario.html',
                controller: 'CargarTarifarioCtrl'
            }).
            //Conceptos Gastos
            when('/ConsultarConceptosGastos', {
                templateUrl: 'Aplicativo/Basico/FlotaPropia/ConsultarConceptosGastos.html',
                controller: 'ConsultarConceptosGastosCrtl'
            }).
            when('/ConsultarConceptosGastos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/FlotaPropia/ConsultarConceptosGastos.html',
                controller: 'ConsultarConceptosGastosCrtl'
            }).
            when('/GestionarConceptosGastos', {
                templateUrl: 'Aplicativo/Basico/FlotaPropia/GestionarConceptosGastos.html',
                controller: 'GestionarConceptosGastosCtrl'
            }).
            when('/GestionarConceptosGastos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/FlotaPropia/GestionarConceptosGastos.html',
                controller: 'GestionarConceptosGastosCtrl'
            }).
            //Legalización Gastos
            when('/ConsultarLegalizacionGastos', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/ConsultarLegalizacionGastos.html',
                controller: 'ConsultarLegalizacionGastosCtrl'
            }).
            when('/ConsultarLegalizacionGastos/:Codigo', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/ConsultarLegalizacionGastos.html',
                controller: 'ConsultarLegalizacionGastosCtrl'
            }).
            when('/GestionarLegalizacionGastos', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/GestionarLegalizacionGastos.html',
                controller: 'GestionarLegalizacionGastosCtrl'
            }).
            when('/GestionarLegalizacionGastos/:Codigo', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/GestionarLegalizacionGastos.html',
                controller: 'GestionarLegalizacionGastosCtrl'
            }).
            //Legalización Gastos Varios Conductores

            when('/ConsultarLegalizacionGastosVariosConductores', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/LegalizacionGastosVariosConductores/ConsultarLegalizacionGastosVariosConductores.html',
                controller: 'ConsultarLegalizacionGastosVariosConductoresCtrl'
            }).
            when('/ConsultarLegalizacionGastosVariosConductores/:Codigo', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/LegalizacionGastosVariosConductores/ConsultarLegalizacionGastosVariosConductores.html',
                controller: 'ConsultarLegalizacionGastosVariosConductoresCtrl'
            }).
            when('/GestionarLegalizacionGastosVariosConductores', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/LegalizacionGastosVariosConductores/GestionarLegalizacionGastosVariosConductores.html',
                controller: 'GestionarLegalizacionGastosVariosConductoresCtrl'
            }).
            when('/GestionarLegalizacionGastosVariosConductores/:Codigo', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/LegalizacionGastosVariosConductores/GestionarLegalizacionGastosVariosConductores.html',
                controller: 'GestionarLegalizacionGastosVariosConductoresCtrl'
            }).

            //FlotaPropia->Documentos->PlanillasDespachosOtrasEmpresas
            when('/ConsultarPlanillasDespachosOtrasEmpresas', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/ConsultarPlanillasDespachosOtrasEmpresas.html',
                controller: 'ConsultarPlanillasDespachosOtrasEmpresasCtrl'
            }).when('/ConsultarPlanillasDespachosOtrasEmpresas/:Numero', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/ConsultarPlanillasDespachosOtrasEmpresas.html',
                controller: 'ConsultarPlanillasDespachosOtrasEmpresasCtrl'
            }).when('/GestionarPlanillasDespachosOtrasEmpresas', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/GestionarPlanillasDespachosOtrasEmpresas.html',
                controller: 'GestionarPlanillasDespachosOtrasEmpresasCtrl'
            }).when('/GestionarPlanillasDespachosOtrasEmpresas/:Numero', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/GestionarPlanillasDespachosOtrasEmpresas.html',
                controller: 'GestionarPlanillasDespachosOtrasEmpresasCtrl'
            }).

            //Solicitudes Anticipo:
            when('/ConsultarSolicitudesAnticipo', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/ConsultarSolicitudesAnticipo.html',
                controller: 'ConsultarSolicitudesAnticipoCtrl'
            }).when('/ConsultarSolicitudesAnticipo/:Numero', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/ConsultarSolicitudesAnticipo.html',
                controller: 'ConsultarSolicitudesAnticipoCtrl'
            }).when('/GestionarSolicitudAnticipo', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/GestionarSolicitudAnticipo.html',
                controller: 'GestionarSolicitudAnticipoCtrl'
            }).when('/GestionarSolicitudAnticipo/:Numero', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/GestionarSolicitudAnticipo.html',
                controller: 'GestionarSolicitudAnticipoCtrl'
            }).

            //Proveedores->Documentos->PlanillasDespachosProveedores
            when('/ConsultarPlanillasDespachosProveedores', {
                templateUrl: 'Aplicativo/Proveedores/ConsultarPlanillasDespachosProveedores.html',
                controller: 'ConsultarPlanillasDespachosProveedoresCtrl'
            }).when('/ConsultarPlanillasDespachosProveedores/:Numero', {
                templateUrl: 'Aplicativo/Proveedores/ConsultarPlanillasDespachosProveedores.html',
                controller: 'ConsultarPlanillasDespachosProveedoresCtrl'
            }).when('/GestionarPlanillasDespachosProveedores', {
                templateUrl: 'Aplicativo/Proveedores/GestionarPlanillasDespachosProveedores.html',
                controller: 'GestionarPlanillasDespachosProveedoresCtrl'
            }).when('/GestionarPlanillasDespachosProveedores/:Numero', {
                templateUrl: 'Aplicativo/Proveedores/GestionarPlanillasDespachosProveedores.html',
                controller: 'GestionarPlanillasDespachosProveedoresCtrl'
            }).

            //Proveedores->Documentos->LiquidaciónDespachosProveedores
            when('/ConsultarLiquidacionPlanillasProveedores', {
                templateUrl: 'Aplicativo/Proveedores/ConsultarLiquidacionDespachosProveedores.html',
                controller: 'ConsultarLiquidacionDespachosProveedoresCtrl'
            }).when('/ConsultarLiquidacionPlanillasProveedores/:Numero', {
                templateUrl: 'Aplicativo/Proveedores/ConsultarLiquidacionDespachosProveedores.html',
                controller: 'ConsultarLiquidacionDespachosProveedoresCtrl'
            }).when('/GestionarLiquidacionPlanillasProveedores', {
                templateUrl: 'Aplicativo/Proveedores/GestionarLiquidacionDespachosProveedores.html',
                controller: 'GestionarLiquidacionDespachosProveedoresCtrl'
            }).when('/GestionarLiquidacionPlanillasProveedores/:Numero', {
                templateUrl: 'Aplicativo/Proveedores/GestionarLiquidacionDespachosProveedores.html',
                controller: 'GestionarLiquidacionDespachosProveedoresCtrl'
            }).
            //Proveedores->Listados:
            when('/ConsultarListadosProveedores', {
                templateUrl: 'Aplicativo/Proveedores/ConsultarListadosProveedores.html',
                controller: 'ConsultarListadosProveedoresCtrl'
            }).
            //Listados Contabilidad
            when('/ConsultarListadoContabilidad', {
                templateUrl: 'Aplicativo/Contabilidad/Listados/ConsultarListadoContabilidad.html',
                controller: 'ConsultarListadoContabilidadCtrl'
            }).
            //Listados Servicio al Cliente
            when('/ConsultarListadoServicioCliente', {
                templateUrl: 'Aplicativo/ServicioCliente/Listados/ConsultarListadoServicioCliente.html',
                controller: 'ConsultarListadoServicioClienteCtrl'
            }).
            when('/DocumentosTerceros', {
                templateUrl: 'Gesphone/Documentos/Terceros/DocumentosTerceros.html',
                controller: 'DocumentosTercerosCtrl'
            }).
            when('/DocumentosVehiculos', {
                templateUrl: 'Gesphone/Documentos/Vehiculos/DocumentosVehiculos.html',
                controller: 'DocumentosVehiculosCtrl'
            }).
            when('/DocumentosCuentasXPagar', {
                templateUrl: 'Gesphone/Documentos/Cuentas/ConsultarCuentas.html',
                controller: 'ConsultarCuentasCtrl'
            }).

            //Mantenimiento:
            when('/ConsultarPlanMantenimiento', {
                templateUrl: 'Aplicativo/Mantenimiento/ConsultarPlanMantenimientoEspecial.html',
                controller: 'ConsultarPlanMantenimientoEspecialCtrl'
            }).
            when('/ConsultarPlanMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Mantenimiento/ConsultarPlanMantenimientoEspecial.html',
                controller: 'ConsultarPlanMantenimientoEspecialCtrl'
            }).
            when('/GestionarPlanMantenimiento', {
                templateUrl: 'Aplicativo/Mantenimiento/GestionarPlanMantenimientoEspecial.html',
                controller: 'GestionarPlanMantenimientoEspecialCtrl'
            }).
            when('/GestionarPlanMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Mantenimiento/GestionarPlanMantenimientoEspecial.html',
                controller: 'GestionarPlanMantenimientoEspecialCtrl'
            }).
            when('/ConsultarTareasMantenimiento', {
                templateUrl: 'Aplicativo/Mantenimiento/ConsultarTareaMantenimientoEspecial.html',
                controller: 'ConsultarTareaMantenimientoEspecialCtrl'
            }).
            when('/ConsultarTareasMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Mantenimiento/ConsultarTareaMantenimientoEspecial.html',
                controller: 'ConsultarTareaMantenimientoEspecialCtrl'
            }).
            when('/GestionarTareasMantenimiento', {
                templateUrl: 'Aplicativo/Mantenimiento/GestionarTareaMantenimientoEspecial.html',
                controller: 'GestionarTareaMantenimientoEspecialCtrl'
            }).
            when('/GestionarTareasMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Mantenimiento/GestionarTareaMantenimientoEspecial.html',
                controller: 'GestionarTareaMantenimientoEspecialCtrl'
            }).
            when('/ConsultarOrdenTrabajoMantenimiento', {
                templateUrl: 'Aplicativo/Mantenimiento/ConsultarOrdenTrabajoMantenimientoEspecial.html',
                controller: 'ConsultarOrdenTrabajoMantenimientoEspecialCtrl'
            }).
            when('/ConsultarOrdenTrabajoMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Mantenimiento/ConsultarOrdenTrabajoMantenimientoEspecial.html',
                controller: 'ConsultarOrdenTrabajoMantenimientoEspecialCtrl'
            }).
            when('/GestionarOrdenTrabajoMantenimiento', {
                templateUrl: 'Aplicativo/Mantenimiento/GestionarOrdenTrabajoMantenimientoEspecial.html',
                controller: 'GestionarOrdenTrabajoMantenimientoEspecialCtrl'
            }).
            when('/GestionarOrdenTrabajoMantenimiento/:Codigo', {
                templateUrl: 'Aplicativo/Mantenimiento/GestionarOrdenTrabajoMantenimientoEspecial.html',
                controller: 'GestionarOrdenTrabajoMantenimientoEspecialCtrl'
            }).

            when('/MantenimientosPendientes', {
                templateUrl: 'Aplicativo/Mantenimiento/ConsultarMantenimientoPendienteEspecial.html',
                controller: 'ConsultarMantenimientoPendienteEspecialCtrl'
            }).
            when('/MantenimientosRealizados', {
                templateUrl: 'Aplicativo/Mantenimiento/ConsultarMantenimientoRealizadosEspecial.html',
                controller: 'ConsultarMantenimientoRealizadosEspecialCtrl'
            }).

            //Seguridad Usuarios
            when('/ConsultarListadoSeguridadUsuarios', {
                templateUrl: 'Aplicativo/SeguridadUsuarios/Listados/ConsultarListadoSeguridadUsuarios.html',
                controller: 'ConsultarListadosSeguridadUsuariosCtrl'
            }).

            //Indicadores
            when('/IndicadoresNovedadesPedidos', {
                templateUrl: 'Aplicativo/Paqueteria/Indicadores/ConsultarIndicadorNovedadesPedidos.html',
                controller: 'ConsultarIndicadorNovedadesPedidosCtrl'
            }).
            when('/IndicadoresNovedadesPedidos/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Indicadores/ConsultarIndicadorNovedadesPedidos.html',
                controller: 'ConsultarIndicadorNovedadesPedidosCtrl'
            }).
            when('/IndicadoresEntregas', {
                templateUrl: 'Aplicativo/Paqueteria/Indicadores/ConsultarIndicadorEntregas.html',
                controller: 'ConsultarIndicadorEntregasCtrl'
            }).
            when('/IndicadoresEntregas/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Indicadores/ConsultarIndicadorEntregas.html',
                controller: 'ConsultarIndicadorEntregasCtrl'
            }).
            when('/IndicadoresErroresAtribuibles', {
                templateUrl: 'Aplicativo/Paqueteria/Indicadores/ConsultarIndicadorErroresAtribuibles.html',
                controller: 'ConsultarIndicadorErroresAtribuiblesCtrl'
            }).
            when('/IndicadoresErroresAtribuibles/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Indicadores/ConsultarIndicadorErroresAtribuibles.html',
                controller: 'ConsultarIndicadorErroresAtribuiblesCtrl'
            }).
            when('/IndicadoresPuntosEntregados', {
                templateUrl: 'Aplicativo/Paqueteria/Indicadores/ConsultarIndicadorPuntosEntregados.html',
                controller: 'ConsultarIndicadorPuntosEntregadosCtrl'
            }).
            when('/IndicadoresPuntosEntregados/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Indicadores/ConsultarIndicadorPuntosEntregados.html',
                controller: 'ConsultarIndicadorPuntosEntregadosCtrl'
            }).
            when('/IndicadoresCajasEntregadas', {
                templateUrl: 'Aplicativo/Paqueteria/Indicadores/ConsultarIndicadorCajasEntregadas.html',
                controller: 'ConsultarIndicadorCajasEntregadasCtrl'
            }).
            when('/IndicadoresCajasEntregadas/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Indicadores/ConsultarIndicadorCajasEntregadas.html',
                controller: 'ConsultarIndicadorCajasEntregadasCtrl'
            }).
            when('/CargarTerceros', {
                templateUrl: 'Aplicativo/Basico/Procesos/CargarTerceros.html',
                controller: 'CargarTercerosCtrl'
            }).
            when('/CargarDocumentosTerceros', {
                templateUrl: 'Aplicativo/Basico/Procesos/CargarDocumentosTerceros.html',
                controller: 'CargarDocumentosTercerosCtrl'
            }).
            // Flota Propia -- Cargue Archivo Terpel
            when('/CargarArchivoTerpel', {
                templateUrl: 'Aplicativo/FlotaPropia/Procesos/CargueArchivoTerpel.html',
                controller: 'CargueArchivoTerpelCtrl'
            }).

            when('/CargarVehiculos', {
                templateUrl: 'Aplicativo/Basico/Procesos/CargarVehiculos.html',
                controller: 'CargarVehiculosCtrl'
            }).
            when('/CargarProductosTransportados', {
                templateUrl: 'Aplicativo/Basico/Procesos/CargarProductosTransportados.html',
                controller: 'CargarProductosTransportadosCtrl'
            }).
            when('/CargarDocumentosVehiculos', {
                templateUrl: 'Aplicativo/Basico/Procesos/CargarDocumentosVehiculos.html',
                controller: 'CargarDocumentosVehiculosCtrl'
            }).
            when('/CargarSemirremolques', {
                templateUrl: 'Aplicativo/Basico/Procesos/CargarSemirremolques.html',
                controller: 'CargarSemirremolqueCtrl'
            }).
            when('/CargarRutas', {
                templateUrl: 'Aplicativo/Basico/Procesos/CargarRutas.html',
                controller: 'CargarRutasCtrl'
            }).
            when('/CargarSitios', {
                templateUrl: 'Aplicativo/Basico/Procesos/CargarSitiosCargueDescargue.html',
                controller: 'CargarSitiosCargueDescargueCtrl'
            }).
            when('/CargarPermisosGrupos', {
                templateUrl: 'Aplicativo/Basico/Procesos/CargarPermisosGrupos.html',
                controller: 'CargarPermisosGruposCtrl'
            }).
            when('/AsociarRemesasPlanillas', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/AsociarPlanillasRemesas.html',
                controller: 'AsociarPlanillasRemesasCtrl'
            }).
            when('/AdicionarRemesasPlanillaRecoleccion', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/AsociarRemesasRecolecciones.html',
                controller: 'AsociarRemesasRecoleccionesCtrl'
            }).
            when('/AdicionarRemesasPlanillaEntrega', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/AsociarRemesasPlanillaEntregas.html',
                controller: 'AsociarRemesasEntregasCtrl'
            }).
            when('/AsignacionSecuenciaPlanillas', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/AsignacionSecuenciaPlanillas.html',
                controller: 'AsignacionSecuenciaPlanillasCtrl'
            }).
            when('/AsociarRemesasPlanillas', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/AsociarPlanillasRemesas.html',
                controller: 'AsociarPlanillasRemesasCtrl'
            }).
            when('/AdicionarGuiaPlanillaPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/AdicionarRemesaPlanillaPaqueteria.html',
                controller: 'AdicionarRemesaPlanillaPaqueteriaCtrl'
            }).
            when('/AdicionarRecoleccionesPlanillaPaqueteria', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/AdicionarRecoleccionesPlanillaPaqueteria.html',
                controller: 'AdicionarRecoleccionesPlanillaPaqueteriaCtrl'
            }).
            when('/VerificarProcesoCargue', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/VerificarProcesoCargue.html',
                controller: 'VerificarProcesoCargueCtrl'
            }).
            when('/VerificarProcesoDescargue', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/VerificarProcesoDescargue.html',
                controller: 'VerificarProcesoDescargueCtrl'
            }).

            when('/EntregarGuiasOficina', {
                templateUrl: 'Aplicativo/PaqueteriaV2/Procesos/EntregasOficina.html',
                controller: 'EntregasOficinaCtrl'
            }).

            //Manifiestos
            when('/ManifiestosCargaPaqueteria', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Manifiestos/ConsultarManifiestosPaqueteria.html',
                controller: 'ConsultarManifiestosPaqueteriaCtrl'
            }).
            when('/ManifiestosCargaPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Manifiestos/ConsultarManifiestosPaqueteria.html',
                controller: 'ConsultarManifiestosPaqueteriaCtrl'
            }).
            when('/GestionarManifiestosCargaPaqueteria', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Manifiestos/GestionarManifiestoPaqueteria.html',
                controller: 'GestionarManifiestoPaqueteriaCtrl'
            }).
            when('/GestionarManifiestosCargaPaqueteria/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/Manifiestos/GestionarManifiestoPaqueteria.html',
                controller: 'GestionarManifiestoPaqueteriaCtrl'
            }).


            when('/ConsultarPlanillasPaqueteriaPortal', {
                templateUrl: 'Portal/Paqueteria/Documentos/PlanillaGuias/ConsultarPlanillaGuiasPortal.html',
                controller: 'ConsultarPlanillasPaqueteriaPortalCtrl'
            }).
            when('/ConsultarPlanillasPaqueteriaPortal/:Numero', {
                templateUrl: 'Portal/Paqueteria/Documentos/PlanillaGuias/ConsultarPlanillaGuiasPortal.html',
                controller: 'ConsultarPlanillasPaqueteriaPortalCtrl'
            }).
            when('/GestionarPlanillasPaqueteriaPortal', {
                templateUrl: 'Portal/Paqueteria/Documentos/PlanillaGuias/GestionarPlanillaGuiasPortal.html',
                controller: 'GestionarPlanillasPaqueteriaPortalCtrl'
            }).
            when('/GestionarPlanillasPaqueteriaPortal/:Numero', {
                templateUrl: 'Portal/Paqueteria/Documentos/PlanillaGuias/GestionarPlanillaGuiasPortal.html',
                controller: 'GestionarPlanillasPaqueteriaPortalCtrl'
            }).


            //Remesas Portal:

            when('/ConsultarRemesasPaqueteriaPortal', {
                templateUrl: 'Portal/Paqueteria/Documentos/Guias/ConsultarRemesasPaqueteriaPortal.html',
                controller: 'ConsultarRemesasPaqueteriaPortalCtrl'
            }).
            when('/ConsultarRemesasPaqueteriaPortal/:Codigo', {
                templateUrl: 'Portal/Paqueteria/Documentos/Guias/ConsultarRemesasPaqueteriaPortal.html',
                controller: 'ConsultarRemesasPaqueteriaPortalCtrl'
            }).
            when('/GestionarRemesasPaqueteriaPortal', {
                templateUrl: 'Portal/Paqueteria/Documentos/Guias/GestionarRemesasPaqueteriaPortal.html',
                controller: 'GestionarRemesasPaqueteriaPortalCtrl'
            }).
            when('/GestionarRemesasPaqueteriaPortal/:Codigo', {
                templateUrl: 'Portal/Paqueteria/Documentos/Guias/GestionarRemesasPaqueteriaPortal.html',
                controller: 'GestionarRemesasPaqueteriaPortalCtrl'
            }).

            // Modulo Almacen -- Orden Compra
            when('/ConsultarOrdenCompra', {
                templateUrl: 'Aplicativo/Almacen/Documentos/ConsultarOrdenCompra.html',
                controller: 'ConsultarOrdenCompraCtrl'
            }).
            when('/ConsultarOrdenCompra/:Numero', {
                templateUrl: 'Aplicativo/Almacen/Documentos/ConsultarOrdenCompra.html',
                controller: 'ConsultarOrdenCompraCtrl'
            }).
            when('/GestionarOrdenCompra', {
                templateUrl: 'Aplicativo/Almacen/Documentos/GestionarOrdenCompra.html',
                controller: 'GestionarOrdenCompraCtrl'
            }).
            when('/GestionarOrdenCompra/:Numero', {
                templateUrl: 'Aplicativo/Almacen/Documentos/GestionarOrdenCompra.html',
                controller: 'GestionarOrdenCompraCtrl'
            }).
            // Modulo Almacen -- Entrada Almace
            when('/ConsultarEntradaAlmacenes', {
                templateUrl: 'Aplicativo/Almacen/Documentos/ConsultarEntradaAlmacenes.html',
                controller: 'ConsultarEntradaAlmacenesCtrl'
            }).
            when('/ConsultarEntradaAlmacenes/:Numero', {
                templateUrl: 'Aplicativo/Almacen/Documentos/ConsultarEntradaAlmacenes.html',
                controller: 'ConsultarEntradaAlmacenesCtrl'
            }).
            when('/GestionarEntradaAlmacenes', {
                templateUrl: 'Aplicativo/Almacen/Documentos/GestionarEntradaAlmacenes.html',
                controller: 'GestionarEntradaAlmacenCtrl'
            }).
            when('/GestionarEntradaAlmacenes/:Numero', {
                templateUrl: 'Aplicativo/Almacen/Documentos/GestionarEntradaAlmacenes.html',
                controller: 'GestionarEntradaAlmacenCtrl'
            }).
            // Modulo Almacen -- SALIDA ALMACEN
            when('/ConsultarSalidaAlmacenes', {
                templateUrl: 'Aplicativo/Almacen/Documentos/ConsultarSalidaAlmacenes.html',
                controller: 'ConsultarSalidaAlmacenesCtrl'
            }).
            when('/ConsultarSalidaAlmacenes/:Numero', {
                templateUrl: 'Aplicativo/Almacen/Documentos/ConsultarSalidaAlmacenes.html',
                controller: 'ConsultarSalidaAlmacenesCtrl'
            }).
            when('/GestionarSalidaAlmacenes', {
                templateUrl: 'Aplicativo/Almacen/Documentos/GestionarSalidaAlmacenes.html',
                controller: 'GestionarSalidaAlmacenCtrl'
            }).
            when('/GestionarSalidaAlmacenes/:Numero', {
                templateUrl: 'Aplicativo/Almacen/Documentos/GestionarSalidaAlmacenes.html',
                controller: 'GestionarSalidaAlmacenCtrl'
            }).

            otherwise({
                redirectTo: '/'
            });

    }]);
