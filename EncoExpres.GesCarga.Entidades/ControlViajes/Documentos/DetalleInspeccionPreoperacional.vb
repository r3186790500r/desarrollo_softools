﻿Imports Newtonsoft.Json

Namespace ControlViajes

    ''' <summary>
    ''' Clase <see cref="DetalleInspeccionPreoperacional"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleInspeccionPreoperacional
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="DetalleInspeccionPreoperacional"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleInspeccionPreoperacional"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = lector.Item("EMPR_Codigo")
            NumeroEncabezado = lector.Item("ENIN_Numero")
            Codigo = lector.Item("ID")
            SeccionFormulario = lector.Item("DSFI_Codigo")
            ItemFormulario = lector.Item("DIFI_Codigo")
            Nombre = lector.Item("Nombre")
            OrdenItem = lector.Item("Orden_Seccion")
            Cumple = lector.Item("Cumple")
            NoAplica = lector.Item("No_Aplica")
            Relevante = lector.Item("Relevante")
            OrdenSeccion = lector.Item("Orden_Item")
            Tamano = lector.Item("Tamaño")
            TipoControl = lector.Item("CATA_TCON_Codigo")
            Valor = lector.Item("Valor")
            SeccionFormularioNombre = lector.Item("NombreSeccion")


        End Sub
        <JsonProperty>
        Public Property NoAplica As Short
        <JsonProperty>
        Public Property NumeroEncabezado As Integer
        <JsonProperty>
        Public Property SeccionFormulario As Integer
        <JsonProperty>
        Public Property ItemFormulario As Integer
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property OrdenSeccion As Short
        <JsonProperty>
        Public Property OrdenItem As Short
        <JsonProperty>
        Public Property Relevante As Short
        <JsonProperty>
        Public Property Cumple As Short
        <JsonProperty>
        Public Property TipoControl As Integer
        <JsonProperty>
        Public Property Valor As String
        <JsonProperty>
        Public Property Tamano As Integer
        <JsonProperty>
        Public Property ItemCumple As String
        <JsonProperty>
        Public Property SeccionInspeccion As String
        <JsonProperty>
        Public Property ItemRelevante As String
        <JsonProperty>
        Public Property SeccionFormularioNombre As String
    End Class

End Namespace
