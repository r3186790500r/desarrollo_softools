﻿PRINT 'gsp_obtener_linea_vehiculos'
GO
DROP PROCEDURE gsp_obtener_linea_vehiculos
GO
CREATE PROCEDURE gsp_obtener_linea_vehiculos
(
	@par_EMPR_Codigo SMALLINT,
	@par_Codigo NUMERIC
)
AS 
BEGIN
		SELECT
			1 AS Obtener,
			LIVE.EMPR_Codigo,
			LIVE.Codigo,
			LIVE.Codigo_Alterno,
			LIVE.Nombre,
			LIVE.MAVE_Codigo,
			LIVE.Estado,
			MAVE.Nombre AS NombreMarca,
			ROW_NUMBER() OVER(ORDER BY LIVE.Nombre) AS RowNumber
		FROM
			Linea_Vehiculos LIVE,
			Marca_Vehiculos MAVE
		WHERE
			LIVE.EMPR_Codigo = MAVE.EMPR_Codigo 
			AND LIVE.MAVE_Codigo = MAVE.Codigo

			AND LIVE.EMPR_Codigo = @par_EMPR_Codigo
			AND LIVE.Codigo = @par_Codigo

END
GO