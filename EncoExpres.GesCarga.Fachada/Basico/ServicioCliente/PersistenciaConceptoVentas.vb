﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente
Imports EncoExpres.GesCarga.Repositorio.Basico.ServicioCliente

Namespace Basico.ServicioCliente
    Public NotInheritable Class PersistenciaConceptoVentas
        Implements IPersistenciaBase(Of ConceptoVentas)

        Public Function Consultar(filtro As ConceptoVentas) As IEnumerable(Of ConceptoVentas) Implements IPersistenciaBase(Of ConceptoVentas).Consultar
            Return New RepositorioConceptoVentas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ConceptoVentas) As Long Implements IPersistenciaBase(Of ConceptoVentas).Insertar
            Return New RepositorioConceptoVentas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ConceptoVentas) As Long Implements IPersistenciaBase(Of ConceptoVentas).Modificar
            Return New RepositorioConceptoVentas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ConceptoVentas) As ConceptoVentas Implements IPersistenciaBase(Of ConceptoVentas).Obtener
            Return New RepositorioConceptoVentas().Obtener(filtro)
        End Function
    End Class

End Namespace

