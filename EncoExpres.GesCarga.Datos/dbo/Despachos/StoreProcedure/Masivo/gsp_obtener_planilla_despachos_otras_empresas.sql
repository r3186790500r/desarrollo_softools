﻿
CREATE PROCEDURE gsp_obtener_planilla_despachos_otras_empresas                
(                
@par_EMPR_Codigo smallint,                
@par_Numero numeric = null                
)                
AS                
BEGIN                
                
SELECT                
1 AS Obtener,                
PDOE.Numero,                
PDOE.Numero_Planilla,                
PDOE.Fecha_Planilla,                
PDOE.Numero_Documento_Transporte,                
TERC.Razon_Social As PerfilCliente,            
TERC.Codigo As TERC_Codigo_Cliente,                
PDOE.Documento_Cliente,                
RUTA.Nombre As Ruta,            
RUTA.Codigo As RUTA_Codigo,                
VEHI.Placa As PlacaVehiculo,            
VEHI.Codigo As VEHI_Codigo,                
SEMI.Placa As Semirremolque,            
PDOE.SEMI_Codigo,                
CONCAT(TERC2.Nombre ,' ', TERC2.Apellido1 , ' ' ,TERC2.Apellido2,' ',TERC2.Razon_Social)As Conductor,            
TERC2.Codigo As TERC_Codigo_Conductor,                
PRTR.Nombre As Producto,             
PRTR.Codigo As PRTR_Codigo,               
PDOE.Cantidad,                
PDOE.Peso,                
PDOE.TATC_Codigo,                
PDOE.Valor_Flete_Transportador,                 
PDOE.Valor_Total_Flete,                
PDOE.Valor_Retencion_Fuente,                
PDOE.Valor_Retencion_ICA,                
PDOE.Valor_Anticipo_Cliente,                 
PDOE.Valor_Pagar,                
PDOE.Valor_Anticipo_Propio,                
PDOE.Estado,                
PDOE.Observaciones  As Observaciones ,          
PDOE.Anulado ,            
PDOE.Numero_Documento,      
PDOE.ENFA_Numero As EncabezadoFactura,
CASE WHEN ENFA.Numero IS NULL THEN ''  
ELSE CONVERT(varchar(10),ENFA.Numero_Documento) END AS Facturado
                
FROM Planillas_Despachos_otras_empresas PDOE                
                
LEFT JOIN Terceros TERC ON                
PDOE.EMPR_Codigo = TERC.EMPR_Codigo                
AND PDOE.TERC_Codigo_Cliente = TERC.Codigo                
                
LEFT JOIN Rutas RUTA                
ON PDOE.EMPR_Codigo = RUTA.EMPR_Codigo                
AND PDOE.RUTA_Codigo = RUTA.Codigo                
                
LEFT JOIN Encabezado_Remesas ENRE  
ON ENRE.EMPR_Codigo = PDOE.EMPR_Codigo  
AND ENRE.Numero = PDOE.ENRE_Numero  
  
LEFT JOIN Detalle_Remesas_Facturas DERF  
ON DERF.EMPR_Codigo = ENRE.EMPR_Codigo  
AND DERF.ENRE_Numero = ENRE.Numero  
  
LEFT JOIN Encabezado_Facturas ENFA  
ON ENFA.EMPR_Codigo = DERF.EMPR_Codigo  
AND ENFA.Numero = DERF.ENFA_Numero  

LEFT JOIN Vehiculos VEHI                
ON PDOE.EMPR_Codigo = VEHI.EMPR_Codigo                
AND PDOE.VEHI_Codigo = VEHI.Codigo                
                
LEFT JOIN Semirremolques SEMI ON                
PDOE.EMPR_Codigo = SEMI.EMPR_Codigo                
AND PDOE.SEMI_Codigo = SEMI.Codigo                
                
LEFT JOIN Terceros TERC2 ON                
PDOE.EMPR_Codigo = TERC2.EMPR_Codigo                
AND PDOE.TERC_Codigo_Conductor = TERC2.Codigo                
                
LEFT JOIN Producto_Transportados PRTR                
ON PDOE.EMPR_Codigo = PRTR.EMPR_Codigo                
AND PDOE.PRTR_Codigo = PRTR.Codigo                
                
LEFT JOIN Tarifa_Transporte_Carga TATC                
ON PDOE.EMPR_Codigo = TATC.EMPR_Codigo                
AND PDOE.TATC_Codigo = TATC.Codigo                
                
WHERE PDOE.EMPR_Codigo = @par_EMPR_Codigo AND PDOE.Numero = @par_Numero                
                
END    
GO