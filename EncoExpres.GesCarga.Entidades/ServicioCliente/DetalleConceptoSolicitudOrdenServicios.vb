﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente

Namespace ServicioCliente

    ''' <summary>
    ''' Clase <see cref=" DetalleConceptoSolicitudOrdenServicios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleConceptoSolicitudOrdenServicios
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleConceptoSolicitudOrdenServicios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleConceptoSolicitudOrdenServicios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroDocumento = Read(lector, "ESOS_Numero")
            Codigo = Read(lector, "ID")
            ConceptoVentas = New ConceptoVentas With {.Codigo = Read(lector, "COVE_Codigo"), .Nombre = Read(lector, "Concepto")}
            Descripcion = Read(lector, "Descripcion")
            Valor = Read(lector, "Valor")
        End Sub

        <JsonProperty>
        Public Property ConceptoVentas As ConceptoVentas
        <JsonProperty>
        Public Property Descripcion As String
        <JsonProperty>
        Public Property Valor As Double



    End Class
End Namespace
