﻿PRINT 'gsp_consultar_remesas_pendientes_reportar_rndc'
GO

DROP PROCEDURE gsp_consultar_remesas_pendientes_reportar_rndc
GO

CREATE PROCEDURE gsp_consultar_remesas_pendientes_reportar_rndc
(
	@par_EMPR_Codigo SMALLINT,
	@par_Numero_Inicial NUMERIC = NULL,
	@par_Numero_Final NUMERIC = NULL,
	@par_Fecha_Inicial DATETIME = NULL,
	@par_Fecha_Final DATETIME = NULL,
	@par_ENMC_Numero INT = NULL,
	@par_NumeroPagina INT,
	@par_RegistrosPagina INT
)
AS
BEGIN
SET NOCOUNT ON;	
	DECLARE @CantidadRegistros INT
	SELECT @CantidadRegistros =
	(
		SELECT DISTINCT
			COUNT(1)
		FROM
			Encabezado_Remesas ENRE
		WHERE
			ENRE.EMPR_Codigo = @par_EMPR_Codigo
			AND ENRE.Estado = 1 --Definitivo
			AND ENRE.Anulado = 0 --No anulado
			AND ENRE.TIDO_Codigo = 100 --Remesa
			AND ENRE.CATA_TIRE_Codigo = 8811 --Remesa Nacional
			AND ENRE.Numero >= ISNULL(@par_Numero_Inicial, ENRE.Numero) AND ENRE.Numero <= ISNULL(@par_Numero_Final, ENRE.Numero)
			AND ENRE.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ENRE.Fecha) AND ISNULL(@par_Fecha_Final, ENRE.Fecha)
			AND ENRE.ENMC_Numero = ISNULL(@par_ENMC_Numero, ENRE.ENMC_Numero)
	);
	WITH Pagina
	AS
	(SELECT
			ENRE.EMPR_Codigo,
			ENRE.Numero_Documento,
			ENRE.Fecha,
			CONCAT(CLIE.Nombre,' ',CLIE.Apellido1,' ',CLIE.Apellido2,' ',CLIE.Razon_Social) AS Cliente,
			VEHI.Placa,
			ENRE.ENMC_Numero AS Manifiesto,
			ENRE.ESOS_Numero AS OrdenServicio,
			ISNULL(DMCA.Mensaje_Remesa_Electronico,'') AS MensajeRemesaElectronico,
		    ROW_NUMBER() OVER (ORDER BY ENRE.Numero_Documento) AS RowNumber
		FROM
			Encabezado_Remesas ENRE INNER JOIN Terceros CLIE
			ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo

			INNER JOIN Vehiculos VEHI
			ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo AND ENRE.VEHI_Codigo = VEHI.Codigo

			INNER JOIN Rutas RUTA
			ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo AND ENRE.RUTA_Codigo = RUTA.Codigo

			LEFT JOIN Detalle_Manifiesto_Carga DMCA
			ON ENRE.EMPR_Codigo = DMCA.EMPR_Codigo AND ENRE.ENMC_Numero = DMCA.ENMC_Numero
		WHERE
			ENRE.EMPR_Codigo = @par_EMPR_Codigo
			AND ENRE.Estado = 1 --Definitivo
			AND ENRE.Anulado = 0 --No anulado
			AND ENRE.TIDO_Codigo = 100 --Remesa
			AND ENRE.CATA_TIRE_Codigo = 8811 --Remesa Nacional
			AND ENRE.Numero >= ISNULL(@par_Numero_Inicial, ENRE.Numero) AND ENRE.Numero <= ISNULL(@par_Numero_Final, ENRE.Numero)
			AND ENRE.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ENRE.Fecha) AND ISNULL(@par_Fecha_Final, ENRE.Fecha)
			AND ENRE.ENMC_Numero = ISNULL(@par_ENMC_Numero, ENRE.ENMC_Numero)
		)
	SELECT DISTINCT
		1 AS Obtener,
		EMPR_Codigo,
		Numero_Documento,
		Fecha,
		Cliente,
		Placa,
		Manifiesto,
		OrdenServicio,
		MensajeRemesaElectronico,	
	    @CantidadRegistros AS TotalRegistros,
	    @par_NumeroPagina AS PaginaObtener,
	    @par_RegistrosPagina AS RegistrosPagina
	FROM Pagina
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
	ORDER BY Numero_Documento
END
GO