﻿Print 'V_Perfil_Terceros'
GO
DROP VIEW V_Perfil_Terceros
GO
CREATE VIEW V_Perfil_Terceros 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 14
GO