﻿PRINT 'gsp_anular_detalle_seguimiento_carga_vehiculos'
GO
DROP PROCEDURE gsp_anular_detalle_seguimiento_carga_vehiculos
GO
CREATE PROCEDURE gsp_anular_detalle_seguimiento_carga_vehiculos
(
@par_EMPR_Codigo SMALLINT,
@par_ID NUMERIC,
@par_USUA_Codigo_Anula SMALLINT,
@par_Causa_Anula VARCHAR (150)
)
AS
BEGIN

UPDATE Detalle_Seguimiento_Carga_Vehiculos SET 
USUA_Codigo_Anula = @par_USUA_Codigo_Anula,
Fecha_Anula = GETDATE(),
Causa_Anula = @par_Causa_Anula,
Anulado = 1
WHERE 
EMPR_Codigo = @par_EMPR_Codigo
AND ID = @par_ID

    SELECT @@ROWCOUNT AS ID

END
GO