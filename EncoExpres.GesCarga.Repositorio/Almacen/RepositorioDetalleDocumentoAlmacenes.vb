﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Almacen


Namespace Almacen
    Public NotInheritable Class RepositorioDetalleDocumentoAlmacenes
        Inherits RepositorioBase(Of DetalleDocumentoAlmacenes)
        Public Overrides Function Consultar(filtro As DetalleDocumentoAlmacenes) As IEnumerable(Of DetalleDocumentoAlmacenes)
            Dim lista As New List(Of DetalleDocumentoAlmacenes)
            Dim item As DetalleDocumentoAlmacenes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Numero) Then
                    If filtro.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    End If
                End If
                If filtro.TipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_consultar_detalle_documento_almacenes")

                While resultado.Read
                    item = New DetalleDocumentoAlmacenes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleDocumentoAlmacenes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleDocumentoAlmacenes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleDocumentoAlmacenes) As DetalleDocumentoAlmacenes
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
