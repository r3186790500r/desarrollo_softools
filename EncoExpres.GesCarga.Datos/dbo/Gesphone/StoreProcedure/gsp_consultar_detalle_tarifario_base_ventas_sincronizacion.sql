﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 27/03/2021 
-- Módulo: Gesphone

PRINT 'gsp_consultar_detalle_tarifario_base_ventas_sincronizacion'
GO
DROP PROCEDURE gsp_consultar_detalle_tarifario_base_ventas_sincronizacion
GO
CREATE PROCEDURE dbo.gsp_consultar_detalle_tarifario_base_ventas_sincronizacion      
(                      
  @par_EMPR_Codigo SMALLINT,                      
  @par_ETCV_Numero NUMERIC,  
  @par_SincronizarTodo SMALLINT = 1,                     
  @par_CIUD_Origen INTEGER = null,
  @par_CIUD_Destino INTEGER = null,
  @par_LNTC_Codigo INTEGER = null
)                      
AS                       
BEGIN                      
 
  DECLARE @sql as varchar(max);    
  
  SET @sql = ' SELECT CONVERT(INTEGER,DTCV.Codigo) As Id,  
  DTCV.LNTC_Codigo AS Ln,                         
  DTCV.TLNC_Codigo AS Tln,                         
  DTCV.TATC_Codigo AS Tt,                         
  DTCV.TTTC_Codigo AS Ttt,                        
  --6102 AS CATA_FPCL_Codigo, --> Forma pago contado                        
  ISNULL(DTCV.Valor_Flete,0) AS Vf,                        
  ISNULL(DTCV.Valor_Escolta,0) AS Ve,                        
  ISNULL(DTCV.Valor_Otros1,0) AS O1,                         
  ISNULL(DTCV.Valor_Otros2,0) AS O2,                        
  ISNULL(DTCV.Valor_Otros3,0) AS O3,                        
  ISNULL(DTCV.Valor_Otros4,0) AS O4,                        
  TTTC.Nombre As Nttc,                        
  TTTC.NombreTarifa As Ntc,          
  DTCV.CATA_FPVE_Codigo AS Fpv,              
  TTTC.CATA_Codigo AS Ca,              
  ISNULL(TTTC.VACA_Codigo,0) AS Vc1,              
  TTTC.VACA_Campo2 AS Vc2,              
  TTTC.VACA_Campo3 AS Vc3,              
  ISNULL(TAPV.Valor_Manejo,0) AS Vm,                      
  ISNULL(TAPV.Valor_Seguro,0) AS Vs,              
  ISNULL(DTCV.Porcentaje_Seguro,0) AS Ps,     
  ISNULL(DTCV.Reexpedicion,0) AS Re,        
  ISNULL(DTCV.Porcentaje_Afiliado,0) AS Pr,        
  ISNULL(DTCV.Valor_Cargue,0) AS Vc,        
  ISNULL(DTCV.Valor_Descargue,0) AS Vd,    
  ISNULL(DTCV.CIUD_Codigo_Origen,0) As Co,  
  ISNULL(DTCV.CIUD_Codigo_Destino,0) AS Cd
                        
  FROM Detalle_Tarifario_Carga_Ventas DTCV                        
                        
  INNER JOIN                         
  Encabezado_Tarifario_Carga_Ventas ETCV ON                        
  DTCV.EMPR_Codigo = ETCV.EMPR_Codigo                       
  AND DTCV.ETCV_Numero = ETCV.Numero                        
                        
  INNER JOIN                        
  V_Tipo_Tarifa_Transporte_Carga TTTC ON                      
  DTCV.EMPR_Codigo = TTTC.EMPR_Codigo                        
  AND DTCV.TATC_Codigo = TTTC.TATC_Codigo                        
  AND DTCV.TTTC_Codigo = TTTC.Codigo                        
                        
  AND TTTC.EstadoTarifa = 1 --> Tarifa Activa                        
  --AND TTTC.EstadoTipoTarifa = 1 --> Tipo Tarifa Activa                        
                      
  LEFT JOIN                       
  Tarifas_Paqueteria_Ventas AS TAPV                      
  ON TAPV.EMPR_Codigo = DTCV.EMPR_Codigo                        
  AND TAPV.ETCV_Numero = DTCV.ETCV_Numero      
                            
  WHERE DTCV.EMPR_Codigo = ' + CONVERT(varchar, @par_EMPR_Codigo) + ' AND DTCV.Estado = 1 AND DTCV.ETCV_Numero = ' + CONVERT(varchar, @par_ETCV_Numero) + '';                         
  
  IF (@par_SincronizarTodo = 0)  
  BEGIN
    SET @sql += ' AND DTCV.CIUD_Codigo_Origen = ' + CONVERT(varchar, @par_CIUD_Origen) + '';
    SET @sql += ' AND DTCV.CIUD_Codigo_Destino = ' + CONVERT(varchar, @par_CIUD_Destino) + '';
    SET @sql += ' AND DTCV.LNTC_Codigo = ' + CONVERT(varchar, @par_LNTC_Codigo) + '';
  END
  
  --PRINT @sql; 

  EXEC(@sql);               
                  
END    
GO