﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Repositorio.Facturacion

Namespace Facturacion
    Public NotInheritable Class PersistenciaDetalleImpuestosFacturas
        Implements IPersistenciaBase(Of DetalleImpuestosFacturas)

        Public Function Consultar(filtro As DetalleImpuestosFacturas) As IEnumerable(Of DetalleImpuestosFacturas) Implements IPersistenciaBase(Of DetalleImpuestosFacturas).Consultar
            Return New RepositorioDetalleImpuestosFacturas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleImpuestosFacturas) As Long Implements IPersistenciaBase(Of DetalleImpuestosFacturas).Insertar
            Return New RepositorioDetalleImpuestosFacturas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleImpuestosFacturas) As Long Implements IPersistenciaBase(Of DetalleImpuestosFacturas).Modificar
            Return New RepositorioDetalleImpuestosFacturas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleImpuestosFacturas) As DetalleImpuestosFacturas Implements IPersistenciaBase(Of DetalleImpuestosFacturas).Obtener
            Return New RepositorioDetalleImpuestosFacturas().Obtener(filtro)
        End Function
    End Class

End Namespace
