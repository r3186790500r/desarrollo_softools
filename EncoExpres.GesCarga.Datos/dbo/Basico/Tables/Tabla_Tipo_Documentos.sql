﻿PRINT 'TABLA Tipo_Documentos'
GO
ALTER TABLE [dbo].[Tipo_Documentos] DROP CONSTRAINT [FK_Tipo_Documentos_Empresas]
GO
ALTER TABLE [dbo].[Tipo_Documentos] DROP CONSTRAINT [FK_Tipo_Documentos_Valor_Catalogos]
GO
DROP TABLE [dbo].[Tipo_Documentos]
GO
CREATE TABLE [dbo].[Tipo_Documentos](
	[EMPR_Codigo] [smallint] NOT NULL,
	[Codigo] [numeric](18, 0) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[CATA_TIGN_Codigo] [numeric](18, 0) NULL,
	[Consecutivo] [numeric](18, 0) NOT NULL,
	[Consecutivo_Hasta] [numeric](18, 0) NULL,
	[Controla_Consecutivo_Hasta] [smallint] NULL,
	[Numero_Documentos_Faltantes_Aviso] [int] NULL,
	[Notas_Inicio] [varchar](250) NULL,
	[Notas_Fin] [varchar](250) NULL,
	[Habilitado] [smallint] NULL,
	[Nombre_Tabla] [varchar](100) NULL,
	[Aplica_Movimiento] [smallint] NULL
 CONSTRAINT [PK_Tipo_Documentos] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Tipo_Documentos]  WITH NOCHECK ADD  CONSTRAINT [FK_Tipo_Documentos_Empresas] FOREIGN KEY([EMPR_Codigo])
REFERENCES [dbo].[Empresas] ([Codigo])
GO
ALTER TABLE [dbo].[Tipo_Documentos] CHECK CONSTRAINT [FK_Tipo_Documentos_Empresas]
GO
ALTER TABLE [dbo].[Tipo_Documentos]  WITH NOCHECK ADD  CONSTRAINT [FK_Tipo_Documentos_Valor_Catalogos] FOREIGN KEY([CATA_TIGN_Codigo])
REFERENCES [dbo].[Valor_Catalogos] ([Codigo])
GO
ALTER TABLE [dbo].[Tipo_Documentos] CHECK CONSTRAINT [FK_Tipo_Documentos_Valor_Catalogos]
GO