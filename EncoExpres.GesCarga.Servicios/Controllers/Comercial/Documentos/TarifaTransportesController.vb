﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Negocio.Comercial.Documentos

''' <summary>
''' Controlador <see cref="TarifaTransportesController"/>
''' </summary>
<Authorize>
Public Class TarifaTransportesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TarifaTransportes) As Respuesta(Of IEnumerable(Of TarifaTransportes))
        Return New LogicaTarifaTransportes(CapaPersistenciaTarifaTransportes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TarifaTransportes) As Respuesta(Of TarifaTransportes)
        Return New LogicaTarifaTransportes(CapaPersistenciaTarifaTransportes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TarifaTransportes) As Respuesta(Of Long)
        Return New LogicaTarifaTransportes(CapaPersistenciaTarifaTransportes).Guardar(entidad)
    End Function

End Class
