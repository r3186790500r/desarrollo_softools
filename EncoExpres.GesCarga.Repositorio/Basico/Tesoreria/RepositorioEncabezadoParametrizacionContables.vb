﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports System.Transactions

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioEncabezadoParametrizacionContables"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEncabezadoParametrizacionContables
        Inherits RepositorioBase(Of EncabezadoParametrizacionContables)

        Public Overrides Function Consultar(filtro As EncabezadoParametrizacionContables) As IEnumerable(Of EncabezadoParametrizacionContables)
            Dim lista As New List(Of EncabezadoParametrizacionContables)
            Dim item As EncabezadoParametrizacionContables

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If
                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_parametrizacion_contables]")

                While resultado.Read
                    item = New EncabezadoParametrizacionContables(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As EncabezadoParametrizacionContables) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_CATA_TIDG_Codigo", entidad.TipoDocumentoGenera.Codigo)
                    'conexion.AgregarParametroSQL("@par_Fuente", entidad.Fuente)
                    'conexion.AgregarParametroSQL("@par_Fuente_Anulacion", entidad.FuenteAnula)
                    'conexion.AgregarParametroSQL("@par_CATA_TGDC_Codigo", entidad.TipoGeneraDetalle.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Provision", entidad.Provision)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_parametrizacion_contables")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While


                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        detalle.CodigoParametrizacion = entidad.Codigo

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Function InsertarDetalle(entidad As DetalleParametrizacionContables, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ENPC_Codigo", entidad.CodigoParametrizacion)
            contextoConexion.AgregarParametroSQL("@par_Orden", entidad.Orden)
            contextoConexion.AgregarParametroSQL("@par_CATA_NACC_Codigo", entidad.NaturalezaMovimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
            contextoConexion.AgregarParametroSQL("@par_CATA_TIPC_Codigo", entidad.TipoParametrizacion.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_VTPC_Codigo", entidad.ValorParametrizacion.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_DCPC_Codigo", entidad.DocumentoCruce.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_TEPC_Codigo", entidad.TerceroParametrizacion.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_CCPC_Codigo", entidad.CentroCostoParametrizacion.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_SIPC_Codigo", entidad.SufijoICAParametrizacion.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_TIDU_Codigo", entidad.TipoDueno.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_TICD_Codigo", entidad.TipoConductor.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Prefijo", entidad.Prefijo)
            contextoConexion.AgregarParametroSQL("@par_Codigo_Anexo", entidad.CodigoAnexo)
            contextoConexion.AgregarParametroSQL("@par_Sufijo_Codigo_Anexo", entidad.SufijoCodigoAnexo)
            contextoConexion.AgregarParametroSQL("@par_Campo_Auxiliar", entidad.CampoAuxiliar)
            contextoConexion.AgregarParametroSQL("@par_CATA_TINA_Codigo", entidad.TipoNaturaleza)
            contextoConexion.AgregarParametroSQL("@par_Tercero_Filial", entidad.Tercero_Filial)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_parametrizacion_contables]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function
        Public Overrides Function Modificar(entidad As EncabezadoParametrizacionContables) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_CATA_TIDG_Codigo", entidad.TipoDocumentoGenera.Codigo)
                    conexion.AgregarParametroSQL("@par_Fuente", entidad.Fuente)
                    conexion.AgregarParametroSQL("@par_Fuente_Anulacion", entidad.FuenteAnula)
                    conexion.AgregarParametroSQL("@par_CATA_TGDC_Codigo", entidad.TipoGeneraDetalle.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Provision", entidad.Provision)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_parametrizacion_contables")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While
                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        detalle.CodigoParametrizacion = entidad.Codigo

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Overrides Function Obtener(filtro As EncabezadoParametrizacionContables) As EncabezadoParametrizacionContables
            Dim item As New EncabezadoParametrizacionContables

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("dbo.gsp_obtener_encabezado_parametrizacion_contables")

                While resultado.Read
                    item = New EncabezadoParametrizacionContables(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As EncabezadoParametrizacionContables) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_encabezado_parametrizacion_contables]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace