﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Negocio.Basico.Almacen

''' <summary>
''' Controlador <see cref="ReferenciaProveedoresController"/>
''' </summary>
<Authorize>
Public Class ReferenciaProveedoresController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ReferenciaProveedores) As Respuesta(Of IEnumerable(Of ReferenciaProveedores))
        Return New LogicaReferenciaProveedores(CapaPersistenciaReferenciaProveedores).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ReferenciaProveedores) As Respuesta(Of ReferenciaProveedores)
        Return New LogicaReferenciaProveedores(CapaPersistenciaReferenciaProveedores).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ReferenciaProveedores) As Respuesta(Of Long)
        Return New LogicaReferenciaProveedores(CapaPersistenciaReferenciaProveedores).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ReferenciaProveedores) As Respuesta(Of Boolean)
        Return New LogicaReferenciaProveedores(CapaPersistenciaReferenciaProveedores).Anular(entidad)
    End Function

End Class