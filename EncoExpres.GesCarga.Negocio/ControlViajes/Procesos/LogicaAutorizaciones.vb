﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Fachada.ControlViajes
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace ControlViajes
    Public NotInheritable Class LogicaAutorizaciones
        Inherits LogicaBase(Of Autorizaciones)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Autorizaciones)

        Public Sub New(capaPersistenciaAutorizaciones As IPersistenciaBase(Of Autorizaciones))
            _persistencia = capaPersistenciaAutorizaciones
        End Sub

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos de la notificacion de correo.
        ''' </summary>

        Public Overrides Function Consultar(filtro As Autorizaciones) As Respuesta(Of IEnumerable(Of Autorizaciones))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Autorizaciones))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Autorizaciones))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Autorizaciones) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function
        Public Function Anular(entidad As Autorizaciones) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaAutorizaciones).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Overrides Function Obtener(filtro As Autorizaciones) As Respuesta(Of Autorizaciones)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Autorizaciones)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Autorizaciones)(consulta)
        End Function

        Private Function DatosRequeridos(entidad As Autorizaciones) As Respuesta(Of Autorizaciones)
            Throw New NotImplementedException()
        End Function




    End Class

End Namespace