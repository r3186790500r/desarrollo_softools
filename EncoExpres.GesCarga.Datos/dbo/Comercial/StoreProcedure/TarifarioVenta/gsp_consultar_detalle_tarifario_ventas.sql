﻿PRINT 'gsp_consultar_detalle_tarifario_ventas'
GO
DROP PROCEDURE gsp_consultar_detalle_tarifario_ventas
GO
CREATE PROCEDURE gsp_consultar_detalle_tarifario_ventas    
(    
@par_EMPR_Codigo SMALLINT,    
@par_ETCV_Numero NUMERIC     
)    
AS     
BEGIN    
SELECT     
DTCV.EMPR_Codigo,    
DTCV.Codigo,    
DTCV.ETCV_Numero,    
DTCV.LNTC_Codigo,    
DTCV.TLNC_Codigo,    
DTCV.TATC_Codigo,    
DTCV.TTTC_Codigo,    
DTCV.RUTA_Codigo,    
ISNULL(DTCV.CIUD_Codigo_Origen,0) AS CIUD_Codigo_Origen,
ISNULL(DTCV.CIUD_Codigo_Destino,0) AS CIUD_Codigo_Destino,
isnull(DTCV.Valor_Flete,0) as Valor_Flete,    
isnull(DTCV.Valor_Escolta,0) as Valor_Escolta,    
--isnull(Valor_Kilo_Adicional,0) as Valor_Kilo_Adicional,    
isnull(DTCV.Valor_Otros1,0) as Valor_Otros1,    
isnull(DTCV.Valor_Otros2,0) as Valor_Otros2,    
isnull(DTCV.Valor_Otros3,0) as Valor_Otros3,    
isnull(DTCV.Valor_Otros4,0) as Valor_Otros4,    
isnull(DTCV.Fecha_Vigencia_Inicio,'') as Fecha_Vigencia_Inicio,    
isnull(DTCV.Fecha_Vigencia_Fin,'') as Fecha_Vigencia_Fin,    
DTCV.Estado,
ISNULL(CIOR.Nombre,'') AS CiudadOrigen,
ISNULL(CIDE.Nombre,'') AS CiudadDestino
    
FROM     
Detalle_Tarifario_Carga_Ventas DTCV LEFT JOIN  Ciudades CIOR ON 
DTCV.EMPR_Codigo = CIOR.EMPR_Codigo
AND DTCV.CIUD_Codigo_Origen = CIOR.Codigo

LEFT JOIN  Ciudades CIDE ON
DTCV.EMPR_Codigo = CIDE.EMPR_Codigo
AND DTCV.CIUD_Codigo_Destino = CIDE.Codigo
    
WHERE DTCV.EMPR_Codigo = @par_EMPR_Codigo    
AND DTCV.ETCV_Numero = ISNULL(@par_ETCV_Numero,DTCV.ETCV_Numero)    

END    
GO

