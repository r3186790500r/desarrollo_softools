﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 17/06/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_consultar_seguimiento_manifiesto_reporte_rndc'
GO
DROP PROCEDURE gsp_consultar_seguimiento_manifiesto_reporte_rndc
GO
CREATE PROCEDURE gsp_consultar_seguimiento_manifiesto_reporte_rndc 
(  
 @par_EMPR_Codigo  smallint,           
 @par_ENMC_NumeroDocumento NUMERIC = NULL,    
 @par_Codigos_Oficinas VARCHAR(150),     
 @par_NumeroPagina INT = NULL,              
 @par_RegistrosPagina INT = NULL              
)              
AS              
BEGIN              
      
 DECLARE @CantidadRegistros NUMERIC        
 SELECT @CantidadRegistros = (              
  SELECT DISTINCT COUNT(1)               
  FROM Encabezado_Manifiesto_Carga AS ENMC        
          
  LEFT JOIN Vehiculos AS VEHI         
  ON ENMC.EMPR_Codigo = VEHI.EMPR_Codigo        
  AND ENMC.VEHI_Codigo = VEHI.Codigo      
      
  LEFT JOIN Semirremolques AS SEMI      
  ON ENMC.EMPR_Codigo = SEMI.EMPR_Codigo        
  AND ENMC.SEMI_Codigo = SEMI.Codigo        
          
  LEFT JOIN Rutas AS RUTA         
  ON ENMC.EMPR_Codigo = RUTA.EMPR_Codigo        
  AND ENMC.RUTA_Codigo = RUTA.Codigo        
                  
  WHERE 
  ENMC.EMPR_Codigo = @par_EMPR_Codigo    
  AND ENMC.Estado = 1-- Estado Remesa Definitivo      
  AND ENMC.Anulado = 0  -- Remesa No Anulada      
  AND ENMC.Estado = 1 -- Manifiesto en Definitivo    
  AND (ENMC.Numero_Manifiesto_Electronico = 0  OR ENMC.Numero_Manifiesto_Electronico IS NULL)--Remesa Sin Reportar      
  AND (ENMC.Numero_Cumplido_Electronico = 0 OR ENMC.Numero_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar    
  AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)  
  AND DATEADD(HOUR, 12, ENMC.Fecha_Crea) > GETDATE() 
  AND ENMC.OFIC_Codigo IN (SELECT * FROM dbo.Func_Dividir_String(@par_Codigos_Oficinas,',')) 
 );              
 WITH Pagina              
 AS              
 ( 
 SELECT           
  1 AS SeguimientoRemesaManifiesto,      
  ENMC.EMPR_Codigo,      
  ENMC.Numero,      
  ENMC.Numero_Documento AS NumeroDocumento,      
  ENMC.Fecha,      
  ISNULL(ENMC.VEHI_Codigo, 0) AS VEHI_Codigo,      
  ISNULL(VEHI.Placa,0) AS PlacaVehiculo,      
  ISNULL(ENMC.SEMI_Codigo, 0) AS SEMI_Codigo,      
  ISNULL(SEMI.Placa, 0) AS PlacaSemirremolque,      
  ISNULL(ENMC.RUTA_Codigo,0) AS RUTA_Codigo,      
  ISNULL(RUTA.Nombre, 0) AS NombreRuta,           
  ISNULL(ENMC.Mensaje_Manifiesto_Electronico, '') AS Mensaje,      
        
  ROW_NUMBER() OVER (ORDER BY ENMC.Numero) AS RowNumber              
      
  FROM Encabezado_Manifiesto_Carga AS ENMC        
          
  LEFT JOIN Vehiculos AS VEHI         
  ON ENMC.EMPR_Codigo = VEHI.EMPR_Codigo        
  AND ENMC.VEHI_Codigo = VEHI.Codigo      
      
  LEFT JOIN Semirremolques AS SEMI      
  ON ENMC.EMPR_Codigo = SEMI.EMPR_Codigo        
  AND ENMC.SEMI_Codigo = SEMI.Codigo        
          
  LEFT JOIN Rutas AS RUTA         
  ON ENMC.EMPR_Codigo = RUTA.EMPR_Codigo        
  AND ENMC.RUTA_Codigo = RUTA.Codigo        
                  
  WHERE 
  ENMC.EMPR_Codigo = @par_EMPR_Codigo    
  AND ENMC.Estado = 1-- Estado Remesa Definitivo      
  AND ENMC.Anulado = 0  -- Remesa No Anulada      
  AND ENMC.Estado = 1 -- Manifiesto en Definitivo    
  AND (ENMC.Numero_Manifiesto_Electronico = 0  OR ENMC.Numero_Manifiesto_Electronico IS NULL)--Remesa Sin Reportar      
  AND (ENMC.Numero_Cumplido_Electronico = 0 OR ENMC.Numero_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar    
  AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento) 
  AND DATEADD(HOUR, 12, ENMC.Fecha_Crea) > GETDATE()    
  AND ENMC.OFIC_Codigo IN (SELECT * FROM dbo.Func_Dividir_String(@par_Codigos_Oficinas,','))
 )              
      
 SELECT      
 SeguimientoRemesaManifiesto,      
 EMPR_Codigo,      
 Numero,      
 NumeroDocumento,      
 Fecha,      
 VEHI_Codigo,      
 PlacaVehiculo,      
 SEMI_Codigo,      
 PlacaSemirremolque,      
 RUTA_Codigo,      
 NombreRuta,            
 Mensaje,      
       
 @CantidadRegistros AS TotalRegistros,      
 @par_NumeroPagina AS PaginaObtener,      
 @par_RegistrosPagina AS RegistrosPagina      
      
 FROM Pagina      
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)      
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)       
END  
GO