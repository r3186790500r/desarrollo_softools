﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="NovedadesDespachosController"/>
''' </summary>
<Authorize>
Public Class NovedadesDespachosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As NovedadesDespachos) As Respuesta(Of IEnumerable(Of NovedadesDespachos))
        Return New LogicaNovedadesDespachos(CapaPersistenciaNovedadesDespachos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As NovedadesDespachos) As Respuesta(Of NovedadesDespachos)
        Return New LogicaNovedadesDespachos(CapaPersistenciaNovedadesDespachos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As NovedadesDespachos) As Respuesta(Of Long)
        Return New LogicaNovedadesDespachos(CapaPersistenciaNovedadesDespachos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As NovedadesDespachos) As Respuesta(Of Boolean)
        Return New LogicaNovedadesDespachos(CapaPersistenciaNovedadesDespachos).Anular(entidad)
    End Function

End Class
