﻿Imports Newtonsoft.Json

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref=" LineaNegocioTransportes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class LineaNegocioTransportes
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property TarifaTransporte As TarifaTransportes
    End Class
End Namespace
