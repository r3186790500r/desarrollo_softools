﻿PRINT 'gsp_consultar_listas_correos_terceros'
GO
DROP PROCEDURE gsp_consultar_listas_correos_terceros
GO
CREATE PROCEDURE gsp_consultar_listas_correos_terceros    
(@par_EMPR_Codigo SMALLINT,    
@par_TERC_Codigo NUMERIC)    
    
AS  
BEGIN  
SELECT      
TELC.EMPR_Codigo    
,TELC.TERC_Codigo    
,TELC.EVCO_Codigo    
,TELC.Email    
,EVCO.Nombre
,EVCO.TIDO_Codigo_Documento AS EVCO_TIDO_Codigo
,ISNULL(TELC.TEDI_Codigo, 0) AS TEDI_Codigo
,ISNULL(TEDI.Nombre, '') AS TEDI_Nombre
    
FROM Tercero_Listas_Correos TELC     
    
LEFT JOIN Evento_Correos EVCO ON    
EVCO.EMPR_Codigo = TELC.EMPR_Codigo    
AND EVCO.Codigo = TELC.EVCO_Codigo

LEFT JOIN Tercero_Direcciones TEDI ON    
TELC.EMPR_Codigo = TEDI.EMPR_Codigo    
AND TELC.TEDI_Codigo = TEDI.Codigo    
    
WHERE     
TELC.EMPR_Codigo = @par_EMPR_Codigo    
AND TELC.TERC_Codigo = @par_TERC_Codigo    
    
END
GO  
