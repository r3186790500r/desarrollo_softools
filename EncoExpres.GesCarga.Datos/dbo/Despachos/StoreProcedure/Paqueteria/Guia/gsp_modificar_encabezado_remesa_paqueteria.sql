﻿PRINT 'gsp_modificar_encabezado_remesa_paqueteria'
go
DROP PROCEDURE gsp_modificar_encabezado_remesa_paqueteria
GO
create procedure gsp_modificar_encabezado_remesa_paqueteria
(
@par_EMPR_Codigo smallint 
           ,@par_ENRE_Numero numeric(18,0)
           ,@par_DTCV_Codigo numeric(18,0)= null
           ,@par_TERC_Codigo_Trans_Externa numeric(18,0)= null
           ,@par_Numero_Guia_Externa varchar(50)= null
           ,@par_CATA_TTRP_Codigo numeric(18,0)= null
           ,@par_CATA_TDRP_Codigo numeric(18,0)= null
           ,@par_CATA_TPRP_Codigo numeric(18,0)= null
           ,@par_CATA_TSRP_Codigo numeric(18,0)= null
           ,@par_CATA_TERP_Codigo numeric(18,0)= null
           ,@par_CATA_TIRP_Codigo numeric(18,0)= null
           ,@par_CATA_ESRP_Codigo numeric(18,0)= null
           ,@par_Fecha_Interfaz DATE = null
		   )
as
begin



update Encabezado_Paqueteria_Remesas
          
           SET 
           DTCV_Codigo = @par_DTCV_Codigo
           ,TERC_Codigo_Trans_Externa = ISNULL(@par_TERC_Codigo_Trans_Externa ,0)
           ,Numero_Guia_Externa = ISNULL(@par_Numero_Guia_Externa,0)
           ,CATA_TTRP_Codigo = @par_CATA_TTRP_Codigo
           ,CATA_TDRP_Codigo = @par_CATA_TDRP_Codigo
           ,CATA_TPRP_Codigo = @par_CATA_TPRP_Codigo
           ,CATA_TSRP_Codigo = @par_CATA_TSRP_Codigo
           ,CATA_TERP_Codigo = @par_CATA_TERP_Codigo
           ,CATA_TIRP_Codigo = @par_CATA_TIRP_Codigo
           ,CATA_ESRP_Codigo = @par_CATA_ESRP_Codigo
		   ,Fecha_Interfaz = isnull(@par_Fecha_Interfaz,'')
		  where 

		    EMPR_Codigo = @par_EMPR_Codigo
           AND ENRE_Numero = @par_ENRE_Numero

select @par_ENRE_Numero as Numero

END
GO