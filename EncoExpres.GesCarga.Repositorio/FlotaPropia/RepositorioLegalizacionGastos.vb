﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.FlotaPropia
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Repositorio.Contabilidad
Imports System.Transactions

Namespace FlotaPropia

    ''' <summary>
    ''' Clase <see cref="RepositorioLegalizacionGastos"/>
    ''' </summary>
    Public Class RepositorioLegalizacionGastos
        Inherits RepositorioBase(Of LegalizacionGastos)

        Public Overrides Function Consultar(filtro As LegalizacionGastos) As IEnumerable(Of LegalizacionGastos)
            Dim lista As New List(Of LegalizacionGastos)
            Dim item As LegalizacionGastos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicio", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Fin", filtro.FechaFinal, SqlDbType.Date)
                End If
                If Not IsNothing(filtro.Conductor) Then
                    If Not IsNothing(filtro.Conductor.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.Nombre)
                    End If
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo <> -1 And filtro.Oficina.Codigo > 0 Then 'TODAS
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If
                If filtro.Estado <> -1 Then 'TODOS
                    If filtro.Estado <> 2 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                        conexion.AgregarParametroSQL("@par_Anulado", 0)
                    Else
                        conexion.AgregarParametroSQL("@par_Anulado", 1)

                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Planilla) Then
                    If filtro.Planilla.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_ENPD_Numero_Documento", filtro.Planilla.NumeroDocumento)

                    End If

                End If

                If filtro.TipoDocumento = 235 Then 'legalización paqueteria
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultas_encabezado_Legalizacion_gastos_paqueteria")
                    While resultado.Read
                        item = New LegalizacionGastos(resultado)
                        lista.Add(item)
                    End While
                Else
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultas_encabezado_Legalizacion_gastos")

                    While resultado.Read
                        item = New LegalizacionGastos(resultado)
                        lista.Add(item)
                    End While
                End If




            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As LegalizacionGastos) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    If entidad.TipoDocumento = 235 Then 'legalización paquetería
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    Else
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", 230)
                    End If

                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    If Not IsNothing(entidad.Conductor) Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Valor_Gastos", entidad.ValorGastos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipos", entidad.ValorAnticipos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Reanticipos", entidad.ValorReanticipos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Saldo_Favor_Conductor", entidad.SaldoConductor, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Saldo_Favor_Empresa", entidad.SaldoEmpresa, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Galones", entidad.TotalGalones, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Galones_Autorizados", entidad.TotalGalonesAutorizados, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Peajes", entidad.TotalPeajes, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Combustible", entidad.ValorTotalCombustible, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Promedio_Galon", entidad.ValorPromedioGalon, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    If Not IsNothing(entidad.Planilla) Then
                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Planilla.Numero)
                    End If
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_Legalizacion_gastos")
                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString
                        entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                    End While
                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    If entidad.Numero > Cero Then
                        inserto = InsertarDetalle(entidad, conexion)
                        If Not IsNothing(entidad.DetalleConductores) Then
                            If entidad.DetalleConductores.Count() > 0 Then
                                'inserto = InsertarDetalleConductores(entidad, conexion)

                                For Each item In entidad.DetalleConductores
                                    'AnularDocumentosCuentasPorConductor(entidad.CodigoEmpresa, item.Conductor.Codigo, conexion)
                                    If Not IsNothing(item.CuentaPorPagar) Then
                                        If (item.CuentaPorPagar.Tercero.Codigo > 0) Then
                                            item.CuentaPorPagar.CodigoDocumentoOrigen = entidad.Numero
                                            item.CuentaPorPagar.Numero = entidad.NumeroDocumento
                                            Generar_CxP(item.CuentaPorPagar, conexion)
                                        End If
                                    End If

                                    If Not IsNothing(item.CuentaPorCobrar) Then
                                        If (item.CuentaPorCobrar.Tercero.Codigo > 0) Then
                                            item.CuentaPorCobrar.CodigoDocumentoOrigen = entidad.Numero
                                            item.CuentaPorCobrar.Numero = entidad.NumeroDocumento
                                            Generar_CxP(item.CuentaPorCobrar, conexion)
                                        End If
                                    End If
                                Next

                            End If
                        End If
                        If Not IsNothing(entidad.Comprobantes) Then
                            If entidad.Comprobantes.Count() > 0 Then
                                inserto = AsociarComprobantes(entidad, conexion)
                            End If
                        End If
                        If Not IsNothing(entidad.GastosCobustible) Then
                            If entidad.GastosCobustible.Count() > 0 Then
                                inserto = InsertarDetalleCombustible(entidad, conexion)
                                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                            End If
                        End If
                    End If
                    If entidad.Estado = 1 Then
                        Dim rep As New RepositorioEncabezadoComprobantesContables
                        If entidad.TipoDocumento = 235 Then 'Legalización Paquetería
                            Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Numero, entidad.TipoDocumento, conexion, resultado)
                        Else
                            Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Numero, 230, conexion, resultado)
                        End If

                        'AnularDocumentosCuentas(entidad, conexion)
                        SaldarDocumentosCuentasLegalizacion(entidad, conexion)

                        If Not IsNothing(entidad.CuentaPorPagar) Then
                            entidad.CuentaPorPagar.CodigoDocumentoOrigen = entidad.Numero
                            entidad.CuentaPorPagar.Numero = entidad.NumeroDocumento

                            Generar_CxP(entidad.CuentaPorPagar, conexion)
                        End If

                        If Not IsNothing(entidad.CuentaPorCobrar) Then
                            entidad.CuentaPorCobrar.CodigoDocumentoOrigen = entidad.Numero
                            entidad.CuentaPorCobrar.Numero = entidad.NumeroDocumento
                            Generar_CxP(entidad.CuentaPorCobrar, conexion)
                        End If

                    End If
                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                    entidad.NumeroDocumento = Cero
                End If
            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Function SaldarDocumentosCuentasLegalizacion(entidad As LegalizacionGastos, conexion As DataBaseFactory) As Boolean
            Dim resultado As IDataReader
            Dim modifico As Boolean
            Dim strCodigosDocumentosCuentas As String = ""
            conexion.CleanParameters()
            Dim TipoDocumentoOrigen = 2616
            If entidad.TipoDocumento = 235 Then
                TipoDocumentoOrigen = 2615
            End If
            If Not IsNothing(entidad.DetalleLegalizacionGastos) Then
                For Each item In entidad.DetalleLegalizacionGastos
                    If item.DocumentoCuenta > 0 Then
                        conexion.CleanParameters()

                        strCodigosDocumentosCuentas = strCodigosDocumentosCuentas + item.DocumentoCuenta.ToString() + ","
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", item.Conductor.Codigo)
                        conexion.AgregarParametroSQL("@par_Numero_Documento_Origen", item.DocumentoCuenta)
                        conexion.AgregarParametroSQL("@par_Valor", item.Valor, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.NumeroDocumento)
                        conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", TipoDocumentoOrigen)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_documentos_cuentas_legalizacion")

                        While resultado.Read
                            modifico = IIf(resultado.Item("Codigo") > 0, True, False)
                        End While
                        resultado.Close()

                    End If

                Next

                For Each item2 In entidad.DetalleLegalizacionGastos
                    If item2.DocumentoCuenta = 0 Then
                        conexion.CleanParameters()


                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", item2.Conductor.Codigo)
                        conexion.AgregarParametroSQL("@par_Codigos_Cuentas", strCodigosDocumentosCuentas)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_anular_documentos_cuentas_legalizacion")

                        While resultado.Read
                            modifico = IIf(resultado.Item("Codigo") > 0, True, False)
                        End While
                        resultado.Close()
                    End If

                Next

            End If

            Return modifico
        End Function

        Public Overrides Function Modificar(entidad As LegalizacionGastos) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    If Not IsNothing(entidad.Conductor) Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)

                    End If
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Valor_Gastos", entidad.ValorGastos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipos", entidad.ValorAnticipos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Reanticipos", entidad.ValorReanticipos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Saldo_Favor_Conductor", entidad.SaldoConductor, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Saldo_Favor_Empresa", entidad.SaldoEmpresa, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Galones", entidad.TotalGalones, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Galones_Autorizados", entidad.TotalGalonesAutorizados, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Peajes", entidad.TotalPeajes, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Combustible", entidad.ValorTotalCombustible, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Promedio_Galon", entidad.ValorPromedioGalon, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_Legalizacion_gastos")
                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString
                        entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                    End While
                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    If entidad.Numero > Cero Then
                        inserto = InsertarDetalle(entidad, conexion)
                        If Not IsNothing(entidad.DetalleConductores) Then
                            If entidad.DetalleConductores.Count() > 0 Then
                                inserto = InsertarDetalleConductores(entidad, conexion)

                                For Each item In entidad.DetalleConductores
                                    'AnularDocumentosCuentasPorConductor(entidad.CodigoEmpresa, item.Conductor.Codigo, conexion)
                                    If Not IsNothing(item.CuentaPorPagar) Then
                                        item.CuentaPorPagar.CodigoDocumentoOrigen = entidad.Numero
                                        item.CuentaPorPagar.Numero = entidad.NumeroDocumento
                                        Generar_CxP(item.CuentaPorPagar, conexion)
                                    End If

                                    If Not IsNothing(item.CuentaPorCobrar) Then
                                        item.CuentaPorCobrar.CodigoDocumentoOrigen = entidad.Numero
                                        item.CuentaPorCobrar.Numero = entidad.NumeroDocumento
                                        Generar_CxP(item.CuentaPorCobrar, conexion)
                                    End If
                                Next
                            End If
                        End If
                        If Not IsNothing(entidad.Comprobantes) Then
                            If entidad.Comprobantes.Count() > 0 Then
                                inserto = AsociarComprobantes(entidad, conexion)
                            End If
                        End If
                        If Not IsNothing(entidad.GastosCobustible) Then
                            If entidad.GastosCobustible.Count() > 0 Then
                                inserto = InsertarDetalleCombustible(entidad, conexion)
                                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                            End If
                        End If

                    End If
                    If entidad.Estado = 1 Then
                        Dim rep As New RepositorioEncabezadoComprobantesContables
                        If entidad.TipoDocumento = 235 Then 'Legalización Paquetería
                            Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Numero, entidad.TipoDocumento, conexion, resultado)
                        Else
                            Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Numero, 230, conexion, resultado)
                        End If

                        'AnularDocumentosCuentas(entidad, conexion)
                        SaldarDocumentosCuentasLegalizacion(entidad, conexion)
                        If Not IsNothing(entidad.CuentaPorPagar) Then
                            entidad.CuentaPorPagar.CodigoDocumentoOrigen = entidad.Numero
                            Generar_CxP(entidad.CuentaPorPagar, conexion)
                        End If

                        If Not IsNothing(entidad.CuentaPorCobrar) Then
                            entidad.CuentaPorCobrar.CodigoDocumentoOrigen = entidad.Numero
                            Generar_CxP(entidad.CuentaPorCobrar, conexion)
                        End If

                    End If
                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                    entidad.NumeroDocumento = Cero
                End If
            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As LegalizacionGastos) As LegalizacionGastos
            Dim item As New LegalizacionGastos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_Legalizacion_gastos")

                While resultado.Read
                    item = New LegalizacionGastos(resultado)
                End While

                If item.Numero > 0 Then
                    item.DetalleLegalizacionGastos = ObtenerDetalle(item)
                    item.DetalleConductores = ObtenerDetalleConductores(item)
                    item.GastosCobustible = ObtenerDetalleCombustible(item)
                    item.Comprobantes = ObtenerDetalleComprobantes(item)
                End If

            End Using
            Return item
        End Function

        Public Function ObtenerDetalleConductores(filtro As LegalizacionGastos) As IEnumerable(Of DetalleLegalizacionGastos)
            Dim lista As New List(Of DetalleLegalizacionGastos)
            Dim item As DetalleLegalizacionGastos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ELGC_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_conductores_Legalizacion_gastos")

                While resultado.Read
                    item = New DetalleLegalizacionGastos(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function
        Public Function ObtenerDetalle(filtro As LegalizacionGastos) As IEnumerable(Of DetalleLegalizacionGastos)
            Dim lista As New List(Of DetalleLegalizacionGastos)
            Dim item As DetalleLegalizacionGastos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CLGC_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_Legalizacion_gastos")

                While resultado.Read
                    item = New DetalleLegalizacionGastos(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function
        Public Function ObtenerDetalleCombustible(filtro As LegalizacionGastos) As IEnumerable(Of DetalleLegalizacionGastos)
            Dim lista As New List(Of DetalleLegalizacionGastos)
            Dim item As DetalleLegalizacionGastos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CLGC_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_Detalle_Combustible_Gastos_Conductor")

                While resultado.Read
                    item = New DetalleLegalizacionGastos(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function
        Public Function ObtenerDetalleComprobantes(filtro As LegalizacionGastos) As IEnumerable(Of EncabezadoDocumentoComprobantes)
            Dim lista As New List(Of EncabezadoDocumentoComprobantes)
            Dim item As EncabezadoDocumentoComprobantes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 30)
                conexion.AgregarParametroSQL("@par_ELGC_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_documento_comprobantes")

                While resultado.Read
                    item = New EncabezadoDocumentoComprobantes(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function

        Public Function Anular(entidad As LegalizacionGastos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_encabezado_legalizacion_gastos_conductor")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While
                resultado.Close()
                Dim rep As New RepositorioEncabezadoComprobantesContables()
                rep.GenerarMovimientoContableAnulacion(entidad.CodigoEmpresa, entidad.Numero, entidad.TipoDocumento, conexion, resultado)

            End Using

            Return anulo
        End Function

        Public Function GuardarFacturaTerpel(entidad As DetalleLegalizacionGastos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Planilla.NumeroDocumento)
                conexion.AgregarParametroSQL("@par_COGC_Codigo", entidad.Concepto.Codigo)
                conexion.AgregarParametroSQL("@par_Valor", entidad.Valor)
                conexion.AgregarParametroSQL("@par_TERC_Beneficiario", entidad.Proveedor.Codigo)
                conexion.AgregarParametroSQL("@par_Numero_Factura", entidad.NumeroFactura)
                conexion.AgregarParametroSQL("@par_Numero_Galones", entidad.CantidadGalones, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Valor_Galones", entidad.ValorGalones)
                conexion.AgregarParametroSQL("@par_Nombre_Estacion", entidad.Observaciones)
                conexion.AgregarParametroSQL("@par_Fecha", entidad.FechaFactura, SqlDbType.Date)
                conexion.AgregarParametroSQL("@par_Tiene_Chip", 1)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_insertar_t_detalle_gasto_conductores")

                If resultado.RecordsAffected > 0 Then
                    anulo = True
                Else
                    anulo = False
                End If

            End Using

            Return anulo
        End Function
        Public Function InsertarDetalleConductores(entidad As LegalizacionGastos, ByVal Conexion As Conexion.DataBaseFactory) As Boolean
            Dim lista As New List(Of DetalleLegalizacionGastos)
            Dim item As DetalleLegalizacionGastos
            Dim resultado As IDataReader

            If Not IsNothing(entidad.DetalleConductores) Then
                For Each item In entidad.DetalleConductores
                    Conexion.CleanParameters()
                    Conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    Conexion.AgregarParametroSQL("@par_ELGC_Numero", entidad.Numero)
                    Conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", item.Conductor.Codigo)
                    Conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    Conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    Conexion.AgregarParametroSQL("@par_Valor_Gastos", item.ValorGastos, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Valor_Anticipos", item.ValorAnticipos, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Valor_Reanticipos", item.ValorReanticipos, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Saldo_Favor_Conductor", item.SaldoConductor, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Saldo_Favor_Empresa", item.SaldoEmpresa, SqlDbType.Money)

                    resultado = Conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_Conductores_Legalizacion_Gastos")

                    While resultado.Read
                        InsertarDetalleConductores = IIf(resultado.Item("ID") > 0, True, False)
                    End While
                    resultado.Close()
                Next
            End If

            Return InsertarDetalleConductores
        End Function

        Public Function AsociarComprobantes(entidad As LegalizacionGastos, ByVal Conexion As Conexion.DataBaseFactory) As Boolean
            Dim resultado As IDataReader

            If Not IsNothing(entidad.DetalleConductores) Then
                For Each item In entidad.Comprobantes
                    Conexion.CleanParameters()
                    Conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    Conexion.AgregarParametroSQL("@par_ELGC_Numero", entidad.Numero)
                    Conexion.AgregarParametroSQL("@par_EDCO_Codigo", item.Codigo)
                    Conexion.AgregarParametroSQL("@par_Numero_Documento_Origen", item.NumeroDocumentoOrigen)

                    resultado = Conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_Comprobantes_Legalizacion_Gastos")

                    While resultado.Read
                        AsociarComprobantes = IIf(resultado.Item("ID") > 0, True, False)
                    End While
                    resultado.Close()
                Next
            End If

            Return AsociarComprobantes
        End Function

        Public Function InsertarDetalle(entidad As LegalizacionGastos, ByVal Conexion As Conexion.DataBaseFactory) As Boolean
            Dim lista As New List(Of DetalleLegalizacionGastos)
            Dim item As DetalleLegalizacionGastos
            Dim resultado As IDataReader

            If Not IsNothing(entidad.DetalleLegalizacionGastos) Then
                For Each item In entidad.DetalleLegalizacionGastos
                    Conexion.CleanParameters()
                    Conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    Conexion.AgregarParametroSQL("@par_ELGC_Numero", entidad.Numero)
                    Conexion.AgregarParametroSQL("@par_ENPD_Numero", item.Planilla.Numero)
                    Conexion.AgregarParametroSQL("@par_CLGC_Codigo", item.Concepto.Codigo)
                    Conexion.AgregarParametroSQL("@par_Valor", item.Valor)
                    Conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor", item.Proveedor.Codigo)
                    Conexion.AgregarParametroSQL("@par_Identificacion_Proveedor", item.Proveedor.NumeroIdentificacion)
                    Conexion.AgregarParametroSQL("@par_Nombre_Proveedor", item.Proveedor.NombreCompleto)
                    Conexion.AgregarParametroSQL("@par_Numero_Factura", item.NumeroFactura)
                    If item.FechaFactura > Date.MinValue Then
                        Conexion.AgregarParametroSQL("@par_Fecha_Factura", item.FechaFactura, SqlDbType.Date)
                    End If
                    Conexion.AgregarParametroSQL("@par_Observaciones", item.Observaciones)
                    Conexion.AgregarParametroSQL("@par_Cantidad_Galones", item.CantidadGalones, SqlDbType.Money)
                    If item.TieneChip Then
                        Conexion.AgregarParametroSQL("@par_Tiene_Chip", 1)
                    Else
                        Conexion.AgregarParametroSQL("@par_Tiene_Chip", 0)
                    End If
                    Conexion.AgregarParametroSQL("@par_Valor_Galones", item.ValorGalones, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Nombre_Estacion", item.NombreEstacion)
                    If Not IsNothing(item.Conductor) Then
                        Conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", item.Conductor.Codigo)

                    End If
                    Conexion.AgregarParametroSQL("@par_ENDC_Codigo", item.DocumentoCuenta)
                    resultado = Conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_legalizacion_gastos")

                    While resultado.Read
                        InsertarDetalle = IIf(resultado.Item("ID") > 0, True, False)
                    End While
                    resultado.Close()
                Next
            End If

            Return InsertarDetalle
        End Function
        Public Function InsertarDetalleCombustible(entidad As LegalizacionGastos, ByVal Conexion As Conexion.DataBaseFactory) As Boolean
            Dim lista As New List(Of DetalleLegalizacionGastos)
            Dim item As DetalleLegalizacionGastos
            Dim resultado As IDataReader
            Dim Inserto = False
            If Not IsNothing(entidad.GastosCobustible) Then
                For Each item In entidad.GastosCobustible
                    Conexion.CleanParameters()
                    Conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    Conexion.AgregarParametroSQL("@par_ELGC_Numero", entidad.Numero)
                    Conexion.AgregarParametroSQL("@par_ENPD_Numero", item.Planilla.Numero)
                    Conexion.AgregarParametroSQL("@par_Km_Ruta", item.Km_Ruta, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Km_Galon_Vehiculo", item.Km_Galon, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Galones_Estimado_Vehiculo", item.Gal_Estimados, SqlDbType.Money)

                    Conexion.AgregarParametroSQL("@par_Horas_Inicial_Termo", item.Horas_Inicio, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Horas_Final_Termo", item.Horas_Fin, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Horas_Termo", item.Horas_Termo, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Horas_Galon_Termo", item.Horas_Galon_Termo, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Galones_Estimado_Termo", item.Galon_Estimado_Termo, SqlDbType.Money)

                    Conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    Conexion.AgregarParametroSQL("@par_Km_Inicial", item.Km_Inicial, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Km_Final", item.Km_Final, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Total_Km", item.Total_Km, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Galones_Autorizados", item.Galones_Autorizados, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Total_Galones", item.Total_Galones, SqlDbType.Money)

                    resultado = Conexion.ExecuteReaderStoreProcedure("gsp_insertar_Detalle_Combustible_Gastos_Conductor")

                    While resultado.Read
                        Inserto = IIf(resultado.Item("ID") > 0, True, False)
                    End While
                    resultado.Close()
                Next
            End If

            Return Inserto
        End Function

        Public Function Generar_CxP(entidad As EncabezadoDocumentoCuentas, conexion As DataBaseFactory) As Boolean
            Generar_CxP = False
            'Se quema las observaciones, ya que cuando el documento se guarda de una vez en definitivo,
            'en el controller.js no se tiene aún el número documento generado de la liquidación, es decir, en el proceso de insertar
            entidad.Observaciones = "PAGO LIQUIDACIÓN PLANILLA No. " & entidad.NumeroDocumento & ", VALOR $ " & entidad.ValorPagar

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", 160)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
            conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
            conexion.AgregarParametroSQL("@par_Codigo_Documento_Origen", entidad.CodigoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.Numero)
            conexion.AgregarParametroSQL("@par_Fecha_Documento_Origen", entidad.FechaDocumento, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Fecha_Vence_Documento_Origen", entidad.FechaVenceDocumento, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPuc.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            conexion.AgregarParametroSQL("@par_Abono", entidad.Abono)
            conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)
            conexion.AgregarParametroSQL("@par_Fecha_Cancelacion_Pago", entidad.FechaCancelacionPago, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Aprobado", entidad.Aprobado)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Aprobo", entidad.UsuarioAprobo.Codigo)
            conexion.AgregarParametroSQL("@par_Fecha_Aprobo", entidad.FechaAprobo, SqlDbType.DateTime)
            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_cuentas_legalizacion")

            While resultado.Read
                Generar_CxP = IIf(resultado.Item("Codigo") > 0, True, False)
            End While
            resultado.Close()

            Return Generar_CxP
        End Function

        Public Function AnularDocumentosCuentas(entidad As LegalizacionGastos, conexion As DataBaseFactory) As Boolean
            Dim Anulo As Boolean = False

            If Not IsNothing(entidad.Conductor) Then
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                If entidad.TipoDocumento = 235 Then 'paquetería
                    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", 2615)
                Else
                    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", 2616)
                End If

                conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Conductor.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_documentos_cuentas_legalizacion_gastos")

                While resultado.Read
                    Anulo = IIf(resultado.Item("Codigo") > 0, True, False)
                End While
                resultado.Close()
            End If


            Return Anulo
        End Function

        Public Function AnularDocumentosCuentasPorConductor(Empresa As Integer, Conductor As Long, conexion As DataBaseFactory) As Boolean
            Dim Anulo As Boolean = False


            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa)
            'If entidad.TipoDocumento = 235 Then 'paquetería
            '    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", 2615)
            'Else
            '    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", 2616)
            'End If

            conexion.AgregarParametroSQL("@par_TERC_Codigo", Conductor)


            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_documentos_cuentas_legalizacion_gastos")

            While resultado.Read
                Anulo = IIf(resultado.Item("Codigo") > 0, True, False)
            End While
            resultado.Close()

            Return Anulo
        End Function
    End Class

End Namespace