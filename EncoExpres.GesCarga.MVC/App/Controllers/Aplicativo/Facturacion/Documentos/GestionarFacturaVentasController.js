﻿EncoExpresApp.controller("GestionarFacturaVentasCtrl", ['$scope', '$routeParams', '$linq', 'blockUI', '$timeout', 'ValorCatalogosFactory', 'blockUIConfig',
    'TercerosFactory', 'RemesasFactory', 'FacturasFactory', 'ConceptosFacturacionFactory', 'DetalleNovedadesDespachosFactory', 'ImpuestoFacturasFactory', 'CierreContableDocumentosFactory', 'CiudadesFactory',
    'ProductoTransportadosFactory', 'OficinasFactory', 'RemesaGuiasFactory', 'DocumentosFactory',
    function ($scope, $routeParams, $linq, blockUI, $timeout, ValorCatalogosFactory, blockUIConfig,
        TercerosFactory, RemesasFactory, FacturasFactory, ConceptosFacturacionFactory, DetalleNovedadesDespachosFactory, ImpuestoFacturasFactory, CierreContableDocumentosFactory, CiudadesFactory,
        ProductoTransportadosFactory, OficinasFactory, RemesaGuiasFactory, DocumentosFactory) {

        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'NUEVA FACTURA VENTA';
        $scope.MapaSitio = [{ Nombre: 'Facturación' }, { Nombre: 'Documentos' }, { Nombre: 'Facturas Venta' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_FACTURA_VENTAS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        //--Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $('.CriteriosBusqueda').show();
        $scope.DataRemesasActualizar = []
        var DocumentoGuardado = false;

        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.esOficinaMadreFactuacion = true;

        $scope.ctr = {
            SedeFacturacion: { Visible: true },
            Importar_ExportarExcel: { Visible: false }
        };

        $scope.MensajesErrorConslta = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoOficina: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Fecha: new Date()
        };

        var TMPCodigoCliente = 0;
        $scope.Filtro = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: CERO,
            Remesa: {
                DetalleTarifarioVenta: {},
                Remitente: { Direccion: '' },
                Detalle: { Destinatario: { Direccion: '' }, }
            },
            EstadoRemesaPaqueteria: { Codigo: 6001 }

        };
        $scope.Filtro2 = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: CERO,
            Remesa: {
                DetalleTarifarioVenta: {},
                Remitente: { Direccion: '' },
                Detalle: { Destinatario: { Direccion: '' }, }
            },
            EstadoRemesaPaqueteria: { Codigo: 6001 }
        };

        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo === ' + ESTADO_BORRADOR);

        $scope.ListadoEstadoGuia = [
            { Codigo: -1, Nombre: '(TODOS)' }
        ];
        $scope.EstadoRemesa = $scope.ListadoEstadoGuia[0];

        $scope.ListadoTipoRemesas = [
            { Nombre: "MASIVO", Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
            { Nombre: "PAQUETERIA", Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },
            { Nombre: "OTRAS EMPRESA", Codigo: 118 },
            { Nombre: "PROVEEDORES", Codigo: 112 }
        ];
        $scope.Filtro.TipoRemesa = $linq.Enumerable().From($scope.ListadoTipoRemesas).First('$.Codigo === ' + CODIGO_TIPO_DOCUMENTO_REMESAS);

        //--Valores Iniciales de Factura
        $scope.Modelo.ValorRemesas = 0;
        $scope.Modelo.ValorOtrosConceptos = 0;
        $scope.Modelo.ValorFactura = 0;
        //--Valores Iniciales de Factura
        //--Valores Iniciales de Remesas
        $scope.ListadoRemeConceAutoComplete = [];

        $scope.ListadoRemesas = [];
        $scope.ListadoRemesasFiltradas = [];
        $scope.ListadoRemesasGuardadas = [];
        $scope.PagRemesas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.PagRemesasGuardadas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        //--Valores Iniciales de Remesas
        $scope.ModeloOficina = { Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre };
        //--Cargar Listado Otros Conceptos
        $scope.ListadoConceptosFactura = [];//Carga Complete para agregar conceptos
        $scope.ListadoOtrosConceptosFactura = [];//Lista de Conceptos Agregados
        $scope.ListadoImpuestosConceptosFactura = [];//Impuestos Agrupados de los Conceptos agregados
        $scope.ListaImpuestosAgregadosConceptos = [];//Impuestos Agregados Agrupados de los Conceptos agregados
        $scope.ListadoCliente = [];
        $scope.ListadoTerceros = [];
        $scope.ListaPrecios = [];
        $scope.ListadoFormaPago = [];
        $scope.ListadoFormaPagoFiltrado = [];
        $scope.ListadoOficinas = [];
        $scope.ListadoImpuestosFactura = [];
        $scope.ListadoImpuestosAgregadosFactura = [];
        $scope.TmpEliminarImpuesto = '';
        $scope.ListadoCiudad = [];
        $scope.ListaProductoTransportado = [];
        $scope.DeshabilitarPaginacion = false;
        $scope.ValidarImpuestosTercero = false;
        $scope.ListaImpuestosCliente = [];
        $scope.ListaImpuestosFacturaBase = [];
        var TmpNumeroRemesa = 0;
        //--------------------------Variables-------------------------//
        //--------------------------Controles----------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ListadoControles.length > 0) {
            for (var i = 0; i < $scope.Sesion.UsuarioAutenticado.ListadoControles.length; i++) {
                var control = $scope.Sesion.UsuarioAutenticado.ListadoControles[i]
                if (control.Codigo == Control_SedeFacturacion) {
                    $scope.ctr.SedeFacturacion.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_Importar_Exportar_Excel_FacturaVenta) {
                    $scope.ctr.Importar_ExportarExcel.Visible = control.Visible > 0 ? true : false;
                }
            }
        }
        //--------------------------Controles----------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoNoPermitirFacturarAFacturacion == true) {
            $scope.DeshabilitarFacturarA = true;
        }
        if ($scope.Sesion.UsuarioAutenticado.DeshabilitarPaginacionFacturaRemesas == true) {
            $scope.DeshabilitarPaginacion = true;
        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoValidarImpuestosTercero == true) {
            $scope.ValidarImpuestosTercero = true;
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete-----------//
        //----Clientes
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };

        $scope.InvocarWS = function () {
            console.log('INVOCANDO WB');
            $scope.ListaPrecios = [];
        }


        //----Facturar A
        $scope.AutocompleteTercero = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoTerceros = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTerceros);
                }
            }
            return $scope.ListadoTerceros;
        };
        //----Ciudad
        $scope.AutocompleteCiudad = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    $scope.ListadoCiudad = [];
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudad = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudad);
                }
            }
            return $scope.ListadoCiudad;
        };
        //----Producto
        $scope.AutocompleteProducto = function (value) {
            if (value.length > 0) {
                blockUIConfig.autoBlock = false;
                var Response = ProductoTransportadosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    ValorAutocomplete: value,
                    AplicaTarifario: -1,
                    Sync: true
                });
                $scope.ListaProductoTransportado = ValidarListadoAutocomplete(Response.Datos, $scope.ListaProductoTransportado);
            }
            return $scope.ListaProductoTransportado;
        };
        //----------AutoComplete-----------//
        //-----------Init
        $scope.InitLoad = function () {
            $scope.MostrarSeccionTap(1);
            //--Combo de forma pago
            $scope.ListadoFormaPago = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS },
                Sync: true
            }).Datos;
            $scope.ListadoFormaPago.splice($scope.ListadoFormaPago.findIndex(item => item.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA), 1);
            $scope.Modelo.FormaPago = $scope.ListadoFormaPago[0];
            $scope.AsignarFormaPago($scope.Modelo.FormaPago.Codigo);
            $scope.ListadoOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
            $scope.ConsultarImpuestosDocumento();
            $scope.ConsultarConceptosImpuestos();
            //--Estado Guias
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 60 }, Estado: ESTADO_DEFINITIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if (response.data.Datos[i].Codigo != 6000) {
                                    $scope.ListadoEstadoGuia.push(response.data.Datos[i]);
                                }
                            }
                        }
                        $scope.Filtro.EstadoRemesa = $scope.ListadoEstadoGuia[0];
                    }
                }, function (response) {
                });
            //valida oficina factuacion madre
            $scope.ModeloOficina = $scope.CargarOficina($scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
            if ($scope.ModeloOficina.Codigo !== $scope.ModeloOficina.OficinaFactura.Codigo) {
                $scope.esOficinaMadreFactuacion = false;
                $scope.MensajesError = ['La oficina actual no corresponde a una oficina madre de facturación'];
                ShowError('La oficina actual no corresponde a una oficina madre de facturación');
            }
            //----Obtener parametros
            if ($routeParams.Numero > 0) {
                $scope.Numero = $routeParams.Numero;
                Obtener();
            }
            else {
                $scope.Numero = 0;
                FindCierre();
            }
        };

        if ($scope.Sesion.UsuarioAutenticado.ProcesoListaPreciosWB) {

            console.log('LISTA DE PRECIOS ACTIVA');
            $('#listaPrecios').show();
            $('#InvocarPrecios').show();


        } else {
            $('#listaPrecios').hide();
            $('#InvocarPrecios').hide();
        }
        if ($scope.Sesion.UsuarioAutenticado.ProcesoNumeroOrdenCompra) {
            $('#OrdenCompra').show();

        } else {
            $('#OrdenCompra').hide();

        }
        //-----------Init
        //-----------------------------Informacion Requerida para funcionar---------------------------------//
        //--------------------Controles---------------------//
        //$('.CriteriosBusqueda').hide();
        $('#DiasPlazo').hide();
        $('#Guias').hide();
        //--------------------Controles---------------------//
        //------------------------------------------------Funciones Generales------------------------------------------------//
        //--------------------------PAGINACION-----------------------------//
        function ResetPaginacion(obj, Deshabilitar) {
            obj.totalRegistros = obj.array.length;
            if (Deshabilitar) {
                obj.totalPaginas = 1;
                obj.PagArray = obj.array;
            }
            else {
                obj.totalPaginas = Math.ceil(obj.totalRegistros / 10);
                $scope.PrimerPagina(obj);
            }
        }
        $scope.PrimerPagina = function (obj) {
            if (obj.totalRegistros > 10) {
                obj.PagArray = [];
                obj.paginaActual = 1;
                for (var i = 0; i < 10; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            } else {
                obj.PagArray = obj.array;
            }
        };
        $scope.Anterior = function (obj) {
            if (obj.paginaActual > 1) {
                obj.paginaActual -= 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                }
            }
            else {
                $scope.PrimerPagina(obj);
            }
        };
        $scope.Siguiente = function (obj) {
            if (obj.paginaActual < obj.totalPaginas) {
                obj.paginaActual += 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                } else {
                    $scope.UltimaPagina(obj);
                }
            } else if (obj.paginaActual == obj.totalPaginas) {
                $scope.UltimaPagina(obj);
            }
        };
        $scope.UltimaPagina = function (obj) {
            if (obj.totalRegistros > 10 && obj.totalPaginas > 1) {
                obj.paginaActual = obj.totalPaginas;
                var a = obj.paginaActual * 10;
                obj.PagArray = [];
                for (var i = a - 10; i < obj.array.length; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            }
        }
        //--------------------------PAGINACION-----------------------------//
        //----Navegacion Taps
        $scope.MostrarSeccionTap = function (Seccion) {
            $(".BusquedaRemesas").hide();
            $(".RemesasGuardadas").hide();
            $(".busqueda").removeClass("active");
            $(".guardadas").removeClass("active");
            switch (Seccion) {
                case 1: //--Consulta
                    $(".BusquedaRemesas").show();
                    $(".busqueda").addClass("active");
                    break;
                case 2: //--remesas seleccioandas / guardadas
                    $(".RemesasGuardadas").show();
                    $(".guardadas").addClass("active");
                    break;
            }
        };
        //----Navegacion Taps
        $scope.ConsultarImpuestosDocumento = function () {
            var RespImpuestoFactuas = ImpuestoFacturasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CodigoTipoDocumento: CODIGO_TIPO_DOCUMENTO_FACTURAS,
                Ciudad: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo },
                Sync: true
            });
            if (RespImpuestoFactuas.ProcesoExitoso == true) {
                if (RespImpuestoFactuas.Datos.length > CERO) {
                    RespImpuestoFactuas.Datos.forEach(function (item) {
                        $scope.ListadoImpuestosFactura.push({
                            Codigo: item.Codigo,
                            Nombre: item.Nombre,
                            Operacion: item.Operacion,
                            Valor_base: item.Valor_base,
                            Valor_tarifa: item.Valor_tarifa,
                            Valor_impuesto: CERO
                        });
                    });
                    $scope.ListaImpuestosFacturaBase = angular.copy($scope.ListadoImpuestosFactura);
                    $scope.ListadoImpuestosAgregadosFactura = angular.copy($scope.ListadoImpuestosFactura);
                    //---Inicializa valor impuesto vista Factura 
                    $scope.ListadoImpuestosAgregadosFactura.forEach(function (item) {
                        item.Valor_base = CERO;
                        item.Valor_impuesto = CERO;
                    });
                    //---Inicializa valor impuesto vista Factura
                }
                else {
                    $scope.ListadoImpuestosFactura = [];
                }
            }
            else {
                ShowError(RespImpuestoFactuas.statusText);
            }
        };
        //--Conceptos Factura e impuestos por concepto
        $scope.ConsultarConceptosImpuestos = function () {
            var RespConcetosFactura = ConceptosFacturacionFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: -1,
                Ciudad: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo },
                Sync: true
            });
            if (RespConcetosFactura.ProcesoExitoso == true) {
                if (RespConcetosFactura.Datos.length > CERO) {
                    RespConcetosFactura.Datos.forEach(function (item) {
                        if (item.Estado != ESTADO_BORRADOR) {
                            if (item.Codigo != CODIGO_INICIAL_NO_APLICA) {
                                $scope.ListadoConceptosFactura.push(item);
                            }
                        }
                    });
                    for (var i = 0; i < $scope.ListadoConceptosFactura.length; i++) {
                        var ListadoImpuestosConceptos = $scope.ListadoConceptosFactura[i].ListadoImpuestos;
                        for (var j = 0; j < ListadoImpuestosConceptos.length; j++) {
                            if ($scope.ListadoImpuestosConceptosFactura.length > 0) {
                                var existeItem = false;
                                for (var k = 0; k < $scope.ListadoImpuestosConceptosFactura.length; k++) {
                                    if ($scope.ListadoImpuestosConceptosFactura[k].Codigo == ListadoImpuestosConceptos[j].Codigo) {
                                        existeItem = true;
                                    }
                                }
                                if (existeItem == false) {
                                    $scope.ListadoImpuestosConceptosFactura.push({
                                        Codigo: ListadoImpuestosConceptos[j].Codigo,
                                        Nombre: ListadoImpuestosConceptos[j].Nombre,
                                        Operacion: ListadoImpuestosConceptos[j].Operacion,
                                        valor_base: CERO,
                                        Valor_tarifa: ListadoImpuestosConceptos[j].Valor_tarifa,
                                        Valor_impuesto: CERO
                                    });
                                }
                            }
                            else {
                                $scope.ListadoImpuestosConceptosFactura.push({
                                    Codigo: ListadoImpuestosConceptos[j].Codigo,
                                    Nombre: ListadoImpuestosConceptos[j].Nombre,
                                    Operacion: ListadoImpuestosConceptos[j].Operacion,
                                    valor_base: CERO,
                                    Valor_tarifa: ListadoImpuestosConceptos[j].Valor_tarifa,
                                    Valor_impuesto: CERO
                                });
                            }
                        }
                    }
                    $scope.ListaImpuestosAgregadosConceptos = angular.copy($scope.ListadoImpuestosConceptosFactura);
                }
            }
            else {
                ShowError(RespConcetosFactura.statusText);
            }
        };
        $scope.PeriodoValido = true
        function FindCierre() {
            var anoFecha = $scope.Modelo.Fecha.getFullYear();
            var mesFecha = $scope.Modelo.Fecha.getMonth();
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentos: { Codigo: 170 },
                Ano: anoFecha,
                Mes: { Codigo: ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MES }, CampoAuxiliar2: (mesFecha + 1), Sync: true }).Datos[0].Codigo },
                EstadoCierre: { Codigo: 18002 },
                Sync: true
            };
            var RCierreContable = CierreContableDocumentosFactory.Consultar(filtros)
            if (RCierreContable.ProcesoExitoso === true) {
                if (RCierreContable.Datos.length > 0) {
                    ShowError('La fecha ingresada no se encuentra en un periodo contable abierto');
                    $scope.Modelo.Fecha = '';
                    $scope.PeriodoValido = false
                    //$scope.ListadoTipoDocumentos = RCierreContable.Datos;
                    //$scope.ValidarCierre();
                }
            }
        }

        $scope.ValidarCierre = function () {
            if ($scope.ListadoTipoDocumentos.length > 0) {
                var anoFecha = $scope.Modelo.Fecha.getFullYear();
                var mesFecha = $scope.Modelo.Fecha.getMonth();
                for (var k = 0; k < $scope.ListadoTipoDocumentos.length; k++) {
                    var item = $scope.ListadoTipoDocumentos[k];
                    var mes = MascaraNumero(item.Mes.CampoAuxiliar2);
                    if (item.Ano === anoFecha && mes === (mesFecha + 1) && item.EstadoCierre.Codigo === 18002) {
                        ShowError('La fecha se encuentra en un mes y año con cierre contable');
                        $scope.ModeloFecha = '';
                        $scope.PeriodoValido = false
                        break;
                    }
                }
            }
        };

        //Asignar Cliente
        $scope.AsignarCliente = function (Cliente) {
            if (Cliente !== undefined && Cliente !== null && Cliente !== '') {
                //valida si el dato ingresado hace parte del objeto de la lista
                if (angular.isObject(Cliente)) {
                    if (TMPCodigoCliente !== Cliente.Codigo) {
                        $scope.Modelo.Cliente = angular.copy(Cliente);
                        $scope.Modelo.FacturarA = angular.copy(Cliente);
                        TMPCodigoCliente = Cliente.Codigo;
                        //Inicializa Listas Cuando Cambia de usuario
                        $scope.ListadoRemesas = [];
                        $scope.ListadoRemesasFiltradas = [];
                        $scope.ListadoRemesasGuardadas = [];
                        $scope.ListadoOtrosConceptosFactura = [];
                        $scope.RestablecerImpuestos();
                        $scope.Calcular();
                        AsignarFormaPagoCliente($scope.Modelo.Cliente);
                    }
                }
            }
            else {
                //Inicializa Listas Cuando Cambia de usuario
                $scope.ListadoRemesas = [];
                $scope.ListadoRemesasFiltradas = [];
                $scope.ListadoRemesasGuardadas = [];
                $scope.ListadoOtrosConceptosFactura = [];
                $scope.RestablecerImpuestos();
                $scope.Calcular();
            }
        };
        //Asignar Forma Pago Cliente Predeterminado
        function AsignarFormaPagoCliente(Cliente) {
            var response = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Cliente.Codigo, Sync: true });
            if (response.ProcesoExitoso === true) {
                try {
                    //--Validacion Impuestos
                    if ($scope.ValidarImpuestosTercero) {
                        $scope.ListadoImpuestosFactura = [];
                        $scope.ListaImpuestosCliente = response.Datos.Cliente.Impuestos;
                        for (var i = 0; i < $scope.ListaImpuestosFacturaBase.length; i++) {
                            var item = $scope.ListaImpuestosFacturaBase[i];
                            var existe = false;
                            for (var j = 0; j < $scope.ListaImpuestosCliente.length; j++) {
                                if (item.Codigo == $scope.ListaImpuestosCliente[j].Codigo) {
                                    existe = true;
                                    break;
                                }
                            }
                            if (existe) {
                                $scope.ListadoImpuestosFactura.push(item);
                            }
                        }
                        $scope.ListadoImpuestosAgregadosFactura = angular.copy($scope.ListadoImpuestosFactura);
                        //---Inicializa valor impuesto vista Factura 
                        $scope.ListadoImpuestosAgregadosFactura.forEach(function (item) {
                            item.Valor_base = CERO;
                            item.Valor_impuesto = CERO;
                        });
                        //---Inicializa valor impuesto vista Factura
                    }
                    //--Validacion Impuestos
                    var formaPagoCliente = response.Datos.Cliente;
                    if (formaPagoCliente.FormaPago.Codigo !== CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA) {

                        $scope.AsignarFormasPago(response.Datos.FormasPago);
                        if ($scope.ListadoFormaPagoFiltrado.length > 0) {
                            $scope.Modelo.FormaPago = $scope.ListadoFormaPagoFiltrado[0];
                        }
                        $scope.Modelo.DiasPlazo = formaPagoCliente.DiasPlazo;
                        $scope.AsignarFormaPago($scope.Modelo.FormaPago.Codigo);
                    }

                    response.Datos.Direcciones.push({ Codigo: 0, Nombre: '(NO APLICA)' });
                    $scope.ListaSedesCliente = response.Datos.Direcciones;
                    $scope.Filtro.Sede = $linq.Enumerable().From($scope.ListaSedesCliente).First('$.Codigo ==0');
                } catch (e) {
                    //--error
                }
            }
        }

        $scope.AsignarFormasPago = function (FormasPago) {
            $scope.ListadoFormaPagoFiltrado = []
            if (FormasPago != null) {
                $scope.ListadoFormaPago.forEach(item => {
                    FormasPago.forEach(itemFormasPago => {
                        if (itemFormasPago.Codigo == item.Codigo) {
                            $scope.ListadoFormaPagoFiltrado.push(item)
                        }
                    })
                })
            }
        }

        //Conceptos Remesas
        $scope.SlcTodo = function () {
            for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                $scope.ListadoRemesasGuardadas.push($scope.ListadoRemesasFiltradas[i]);
            }
            $scope.ListadoRemesasFiltradas = [];

            $scope.PagRemesas.array = $scope.ListadoRemesasFiltradas;
            ResetPaginacion($scope.PagRemesas);

            $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
            ResetPaginacion($scope.PagRemesasGuardadas, $scope.DeshabilitarPaginacion);

            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                AgregarNovedadConcepto($scope.ListadoRemesasGuardadas[i].Numero);
            }
        };

        function ValidarOtroConceptoRemesa(ENRE_Numero) {
            var response = DetalleNovedadesDespachosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: ENRE_Numero,
                Sync: true
            });
            if (response.ProcesoExitoso) {
                if (response.Datos.length > CERO) {
                    response.Datos.forEach(function (item) {
                        if (item.Anulado == CERO) {
                            $scope.ListadoOtrosConceptosFactura.push({
                                DetalleNODE: { Codigo: item.Codigo },
                                NumeroRemesa: { Numero: item.NumeroRemesa, NumeroDocumento: item.NumeroDocumentoRemesa },
                                ConceptoVenta: item.ConceptoVenta,
                                Descripcion: item.Observaciones,
                                ValorVenta: item.ValorVenta
                            });
                        }
                    });
                }
            }
            else {
                //ShowError(response.statusText);
            }
        }

        function AgregarNovedadConcepto(ENRE_Numero) {
            var agregadoAConceptos = false;
            var indexAgregado = [];
            for (var i = 0; i < $scope.ListadoOtrosConceptosFactura.length; i++) {
                if ($scope.ListadoOtrosConceptosFactura[i].NumeroRemesa.Numero == ENRE_Numero &&
                    $scope.ListadoOtrosConceptosFactura[i].DetalleNODE.Codigo != CERO) {
                    agregadoAConceptos = true;
                    indexAgregado.push(i);
                }
            }
            if (agregadoAConceptos == false) {
                ValidarOtroConceptoRemesa(ENRE_Numero);
            }
            IndiceOtrosConceptos();
            $scope.Calcular();
        }

        function RemoverNovedadConcepto(ENRE_Numero) {
            var agregadoAConceptos = false;
            var indexAgregado = [];
            for (var i = 0; i < $scope.ListadoOtrosConceptosFactura.length; i++) {
                /*if ($scope.ListadoOtrosConceptosFactura[i].NumeroRemesa.Numero == ENRE_Numero &&
                    $scope.ListadoOtrosConceptosFactura[i].DetalleNODE.Codigo != CERO) {*/
                if ($scope.ListadoOtrosConceptosFactura[i].NumeroRemesa.Numero == ENRE_Numero) {
                    agregadoAConceptos = true;
                    indexAgregado.push(i);
                }
            }
            if (agregadoAConceptos == true) {
                for (var i = 0; i < indexAgregado.length; i++) {
                    $scope.ListadoOtrosConceptosFactura.splice(indexAgregado[i] - i, 1);
                }
            }
            IndiceOtrosConceptos();
            $scope.Calcular();
        }

        $scope.AgregarRemesaFactura = function (ENRE_Numero) {
            var indexReme = 0;
            for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                if ($scope.ListadoRemesasFiltradas[i].Numero == ENRE_Numero) {
                    indexReme = i;
                    break;
                }
            }
            $scope.ListadoRemesasGuardadas.push($scope.ListadoRemesasFiltradas[indexReme]);
            $scope.ListadoRemesasFiltradas.splice(indexReme, 1);

            $scope.PagRemesas.array = $scope.ListadoRemesasFiltradas;
            ResetPaginacion($scope.PagRemesas);

            $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
            ResetPaginacion($scope.PagRemesasGuardadas, $scope.DeshabilitarPaginacion);

            AgregarNovedadConcepto(ENRE_Numero);
        };

        $scope.RemoverRemesaFactura = function (ENRE_Numero) {
            var indexReme = 0;
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                if ($scope.ListadoRemesasGuardadas[i].Numero == ENRE_Numero) {
                    indexReme = i;
                    break;
                }
            }
            $scope.ListadoRemesasGuardadas.splice(indexReme, 1);
            $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
            ResetPaginacion($scope.PagRemesasGuardadas, $scope.DeshabilitarPaginacion);

            RemoverNovedadConcepto(ENRE_Numero);
        };
        //Conceptos Remesas
        function DatosRequeridosConsulta() {
            var continuar = true;
            $scope.MensajesErrorConslta = [];
            var Modelo = $scope.Filtro;

            if ((Modelo.FechaInicial === null || Modelo.FechaInicial === undefined || Modelo.FechaInicial === '')
                && (Modelo.FechaFinal === null || Modelo.FechaFinall === undefined || Modelo.FechaFinal === '')
                && (Modelo.NumeroInicial === null || Modelo.NumeroInicial === undefined || Modelo.NumeroInicial === '' || Modelo.NumeroInicial === 0 || isNaN(Modelo.NumeroInicial) === true)) {
                $scope.MensajesErrorConslta.push('Debe ingresar los filtros de fechas o número');
                continuar = false;
            }
            else if ((Modelo.NumeroInicial !== null && Modelo.NumeroInicial !== undefined && Modelo.NumeroInicial !== '' && Modelo.NumeroInicial !== 0)
                || (Modelo.FechaInicial !== null && Modelo.FechaInicial !== undefined && Modelo.FechaInicial !== '')
                || (Modelo.FechaFinal !== null && Modelo.FechaFinal !== undefined && Modelo.FechaFinal !== '')) {
                if ((Modelo.FechaInicial !== null && Modelo.FechaInicial !== undefined && Modelo.FechaInicial !== '')
                    && (Modelo.FechaFinal !== null && Modelo.FechaFinal !== undefined && Modelo.FechaFinal !== '')) {
                    if (Modelo.FechaFinal < Modelo.FechaInicial) {
                        $scope.MensajesErrorConslta.push('La fecha final debe ser mayor a la fecha inicial');
                        continuar = false;
                    } else if (((Modelo.FechaFinal - Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO_60) {
                        $scope.MensajesErrorConslta.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO_60 + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                        $scope.FechaFinal = $scope.FechaInicial;
                    }
                    else {
                        $scope.FechaInicial = $scope.FechaFinal;
                    }
                }
            }
            return continuar;
        }
        function DatosRequeridosFiltro() {
            var continuar = true
            $scope.MensajesErrorFiltro = [];
            var Modelo = $scope.Filtro2
            if (Modelo.FechaInicial !== undefined && Modelo.FechaFinal !== undefined) {
                if (Modelo.FechaInicial > Modelo.FechaFinal) {
                    $scope.MensajesErrorFiltro.push('La fecha inicial no puede ser mayor a la fecha final')
                    continuar = false
                }
            }
            return continuar
        }
        $scope.Buscar = function () {
            $scope.SeleccionRemesa = false;
            if (DatosRequeridosConsulta()) {
                Find();
            }
        };
        function Find() {
            $scope.PagRemesas.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoRemesas = [];
            $scope.ListadoRemesasFiltradas = [];
            if ($scope.PagRemesas.Buscando) {
                if ($scope.MensajesError.length == CERO) {

                    blockUIConfig.autoBlock = true;
                    blockUIConfig.delay = 0;
                    blockUI.start("Espere...");
                    $timeout(function () { blockUI.message("Buscando registros..."); }, 100);

                    var TraerRemesas = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroDocumento: $scope.Filtro.NumeroInicial,
                        TipoDocumento: { Codigo: $scope.Filtro.TipoRemesa.Codigo },
                        FechaInicial: $scope.Filtro.FechaInicial,
                        FechaFinal: $scope.Filtro.FechaFinal,
                        NumeroDocumentoOrdenServicio: $scope.Filtro.OrdenServicio,
                        NumeroDocumentoPlanillaDespacho: $scope.Filtro.PlanillaDespacho,
                        NumeroDocumentoCliente: $scope.Filtro.NumeroDocumentoCliente,
                        Cliente: $scope.Modelo.Cliente,
                        CodigoSede: $scope.Filtro.Sede == undefined ? 0 : $scope.Filtro.Sede.Codigo,
                        FacturarA: $scope.Filtro.FacturarA == undefined ? 0 : $scope.Filtro.FacturarA.Codigo,
                        CiudadRemitente: $scope.Filtro.CiudadOrigen,
                        CiudadDestinatario: $scope.Filtro.CiudadDestino,
                        ProductoTransportado: $scope.Filtro.Producto,
                        Oficina: $scope.Filtro.Oficina,
                        Estado: $scope.Filtro.EstadoRemesa.Codigo
                    };
                    if ($scope.Filtro.TipoRemesa.Codigo != CODIGO_TIPO_DOCUMENTO_REMESAS) {
                        TraerRemesas.FacturarA = '';
                    }

                    RemesasFactory.ConsultarRemesasPendientesFacturar(TraerRemesas).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                                if (response.data.Datos.length > CERO) {
                                    if ($scope.ListadoRemesasGuardadas != undefined && $scope.ListadoRemesasGuardadas.length > 0) {
                                        //Verifica si existen remesas guardadas con la busqueda actual, para no generar duplicidad
                                        for (var i = 0; i < response.data.Datos.length; i++) {
                                            var Existe = false;
                                            for (var j = 0; j < $scope.ListadoRemesasGuardadas.length; j++) {
                                                if (response.data.Datos[i].Numero == $scope.ListadoRemesasGuardadas[j].Numero) {
                                                    Existe = true;
                                                    break;
                                                }
                                            }
                                            if (!Existe) {
                                                if (response.data.Datos[i].TipoDespacho.Codigo == 18202 && response.data.Datos[i].FacturarPesoCumplido == CODIGO_UNO) {
                                                    response.data.Datos[i].PesoCliente = response.data.Datos[i].CumplidoRemesa.PesoDescargue;
                                                    response.data.Datos[i].TotalFleteCliente = MascaraValores(Math.round((parseInt(MascaraNumero(response.data.Datos[i].PesoCliente)) * parseInt(MascaraNumero(response.data.Datos[i].ValorFleteCliente))) / 1000));
                                                }
                                                response.data.Datos[i].PesoCliente = (response.data.Datos[i].PesoCliente);
                                                response.data.Datos[i].ValorFleteCliente = MascaraValores(response.data.Datos[i].ValorFleteCliente);
                                                response.data.Datos[i].TotalFleteCliente = MascaraValores(response.data.Datos[i].TotalFleteCliente);

                                                response.data.Datos[i].ValorComercialCliente = MascaraValores(response.data.Datos[i].ValorComercialCliente);
                                                response.data.Datos[i].ValorFOB = MascaraValores(response.data.Datos[i].ValorFOB);
                                                $scope.ListadoRemesas.push(response.data.Datos[i]);
                                            }
                                        }
                                        $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas;
                                    }
                                    else {
                                        //Agrega directamente al listado
                                        for (var i = 0; i < response.data.Datos.length; i++) {
                                            if (response.data.Datos[i].TipoDespacho.Codigo == 18202 && response.data.Datos[i].FacturarPesoCumplido == CODIGO_UNO) {
                                                response.data.Datos[i].PesoCliente = response.data.Datos[i].CumplidoRemesa.PesoDescargue;
                                                response.data.Datos[i].TotalFleteCliente = MascaraValores(Math.round((parseInt(MascaraNumero(response.data.Datos[i].PesoCliente)) * parseInt(MascaraNumero(response.data.Datos[i].ValorFleteCliente))) / 1000));
                                            }
                                            response.data.Datos[i].PesoCliente = (response.data.Datos[i].PesoCliente);
                                            response.data.Datos[i].ValorFleteCliente = MascaraValores(response.data.Datos[i].ValorFleteCliente);
                                            response.data.Datos[i].TotalFleteCliente = MascaraValores(response.data.Datos[i].TotalFleteCliente);

                                            response.data.Datos[i].ValorComercialCliente = MascaraValores(response.data.Datos[i].ValorComercialCliente);
                                            response.data.Datos[i].ValorFOB = MascaraValores(response.data.Datos[i].ValorFOB);
                                        }
                                        $scope.ListadoRemesas = response.data.Datos;
                                        $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas;
                                    }

                                    if ($scope.ListadoRemesasFiltradas.length > 0) {
                                        $scope.PagRemesas.array = $scope.ListadoRemesasFiltradas;
                                        $scope.PagRemesas.ResultadoSinRegistros = '';
                                        ResetPaginacion($scope.PagRemesas);
                                    }
                                    else {
                                        $scope.PagRemesas.totalRegistros = 0;
                                        $scope.PagRemesas.totalPaginas = 0;
                                        $scope.PagRemesas.paginaActual = 1;
                                        $scope.PagRemesas.array = [];
                                        $scope.PagRemesas.Buscando = false;
                                        $scope.PagRemesas.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    }
                                }
                                else {
                                    $scope.PagRemesas.totalRegistros = 0;
                                    $scope.PagRemesas.totalPaginas = 0;
                                    $scope.PagRemesas.paginaActual = 1;
                                    $scope.PagRemesas.array = [];
                                    $scope.PagRemesas.Buscando = false;
                                    $scope.PagRemesas.ResultadoSinRegistros = 'No hay datos para mostrar';
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });

                            }
                        }, function (response) {
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                            ShowError(response.statusText);
                        });
                }
            }
        }
        $scope.Filtrar = function () {
            if (DatosRequeridosFiltro()) {
                var Filtro = $scope.Filtro2;
                var nFiltrosAplica = 0;
                if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                    nFiltrosAplica++;
                }
                if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                    nFiltrosAplica++;
                }
                if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                    nFiltrosAplica++;
                }
                if (Filtro.OrdenServicio !== undefined && Filtro.OrdenServicio > 0) {
                    nFiltrosAplica++;
                }
                if (Filtro.PlanillaDespacho !== undefined && Filtro.PlanillaDespacho > 0) {
                    nFiltrosAplica++;
                }
                if (Filtro.DocumentoCliente !== undefined && Filtro.DocumentoCliente > 0) {
                    nFiltrosAplica++;
                }
                $scope.ListadoRemesasFiltradas = [];
                for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                    var cont = 0;
                    var item = $scope.ListadoRemesas[i];
                    if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                        if (item.NumeroDocumento == Filtro.NumeroInicial) {
                            cont++;
                        }
                    }
                    if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                        if (new Date(item.Fecha) >= Filtro.FechaInicial) {
                            cont++;
                        }
                    }
                    if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                        if (new Date(item.Fecha) <= Filtro.FechaFinal) {
                            cont++;
                        }
                    }
                    if (Filtro.OrdenServicio !== undefined && Filtro.OrdenServicio > 0) {
                        if (item.NumeroOrdenServicio == Filtro.OrdenServicio) {
                            cont++;
                        }
                    }
                    if (Filtro.PlanillaDespacho !== undefined && Filtro.PlanillaDespacho > 0) {
                        if (item.Planilla.NumeroDocumento == Filtro.PlanillaDespacho) {
                            cont++;
                        }
                    }
                    if (Filtro.DocumentoCliente !== undefined && Filtro.DocumentoCliente > 0) {
                        if (item.NumeroDocumentoCliente == Filtro.DocumentoCliente) {
                            cont++;
                        }
                    }
                    if (cont == nFiltrosAplica) {
                        for (var j = 0; j < $scope.ListadoRemesasGuardadas.length; j++) {
                            var existe = false;
                            if (item.Numero == $scope.ListadoRemesasGuardadas[j].Numero) {
                                existe = true;
                                break;
                            }
                        }
                        if (!existe) {
                            $scope.ListadoRemesasFiltradas.push(item);
                        }
                    }
                }
                $scope.PagRemesas.array = $scope.ListadoRemesasFiltradas;
                ResetPaginacion($scope.PagRemesas);
            }
        };
        //-------------Obtener
        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                Estado: -1,
                Anulado: -1
            };
            $scope.DeshabilitarCliente = true;

            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Obteniendo documento...");
            $timeout(function () { blockUI.message("Obteniendo documento..."); }, 100);

            FacturasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        DocumentoGuardado = true;
                        $scope.Titulo = 'FACTURA VENTA';
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        $scope.Modelo.NumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.Modelo.Fecha = new Date(response.data.Datos.Fecha);
                        $scope.Modelo.Cliente = $scope.CargarTercero(response.data.Datos.Cliente.Codigo);
                        AsignarFormaPagoCliente($scope.Modelo.Cliente);
                        $scope.Modelo.FacturarA = $scope.CargarTercero(response.data.Datos.FacturarA.Codigo);
                        $scope.Modelo.NOrdenCompra = response.data.Datos.NumeroOrdenCompra
                        $scope.ObtenerSedesTercero();

                        if (response.data.Datos.SedeFacturacion.Codigo > 0) {
                            $scope.Modelo.SedeFacturacion = $linq.Enumerable().From($scope.ListadoSedesTercero).First('$.Codigo ==' + response.data.Datos.SedeFacturacion.Codigo);
                        }


                        $scope.CodigoFormaPago = response.data.Datos.FormaPago.Codigo;
                        if ($scope.ListadoFormaPago !== undefined && $scope.ListadoFormaPago !== null) {
                            if ($scope.ListadoFormaPago.length > 0) {
                                $scope.Modelo.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.CodigoFormaPago);
                                $scope.AsignarFormaPago($scope.CodigoFormaPago);
                            }
                        }

                        $scope.Modelo.DiasPlazo = response.data.Datos.DiasPlazo;

                        if (response.data.Datos.Anulado == 1) {
                            $scope.ListadoEstados.push({ Nombre: 'ANULADO', Codigo: 2 });
                            $scope.Modelo.Estado = $scope.ListadoEstados[2];
                            $scope.Deshabilitar = true;
                            $scope.DeshabilitarCampoDiasPlazo = true;
                            $scope.DeshabilitarFacturarA = true;
                            //$scope.MostrarSeccionTap(2);
                        } else {
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                            if (response.data.Datos.Estado == ESTADO_ACTIVO) {
                                $scope.Deshabilitar = true;
                                $scope.DeshabilitarCampoDiasPlazo = true;
                                $scope.DeshabilitarFacturarA = true;
                                $scope.MostrarSeccionTap(2);
                            }
                            else {
                                $scope.Deshabilitar = false;
                                $scope.MostrarCriteriosBusqueda();
                            }
                        }

                        $scope.Modelo.Observaciones = response.data.Datos.Observaciones;
                        $scope.Modelo.ValorSubtotal = $scope.MaskValoresGrid(response.data.Datos.Subtotal);
                        $scope.Modelo.ValorImpuestos = $scope.MaskValoresGrid(response.data.Datos.ValorImpuestos);
                        $scope.Modelo.ValorRemesas = $scope.MaskValoresGrid(response.data.Datos.ValorRemesas);
                        $scope.Modelo.ValorOtrosConceptos = $scope.MaskValoresGrid(response.data.Datos.ValorOtrosConceptos);
                        $scope.Modelo.ValorFactura = $scope.MaskValoresGrid(response.data.Datos.ValorFactura);
                        response.data.Datos.Remesas.forEach(function (itmRemesa) {
                            var AuxRem = {
                                Seleccionado: true,
                                Numero: itmRemesa.Remesas.Numero,
                                NumeroOrdenServicio: itmRemesa.Remesas.NumeroOrdenServicio,
                                Planilla: itmRemesa.Remesas.Planilla,
                                Manifiesto: itmRemesa.Remesas.Manifiesto,
                                NumeroDocumento: itmRemesa.Remesas.NumeroDocumento,
                                Fecha: itmRemesa.Remesas.Fecha,
                                Ruta: itmRemesa.Remesas.Ruta,
                                NumeroDocumentoCliente: itmRemesa.Remesas.NumeroDocumentoCliente,
                                ValorFleteCliente: MascaraValores(itmRemesa.Remesas.ValorFleteCliente),
                                ProductoTransportado: itmRemesa.Remesas.ProductoTransportado,
                                CantidadCliente: itmRemesa.Remesas.CantidadCliente,
                                PesoCliente: itmRemesa.Remesas.PesoCliente,
                                TotalFleteCliente: MascaraValores(itmRemesa.Remesas.TotalFleteCliente),
                                Observaciones: itmRemesa.Remesas.Observaciones,
                                Remitente: itmRemesa.Remesas.Remitente,
                                Destinatario: itmRemesa.Remesas.Destinatario,
                                TipoDespacho: itmRemesa.Remesas.TipoDespacho,
                                Vehiculo: itmRemesa.Remesas.Vehiculo

                            };
                            $scope.ListadoRemesasGuardadas.push(AuxRem);
                        });
                        $scope.ListadoOtrosConceptosFactura = [];
                        response.data.Datos.OtrosConceptos.forEach(function (itmOtroConcepto) {
                            var auxOtr = {
                                DetalleNODE: { Codigo: itmOtroConcepto.DetalleNODE.Codigo },
                                NumeroRemesa: { Numero: itmOtroConcepto.Remesas.Numero, NumeroDocumento: itmOtroConcepto.Remesas.NumeroDocumento },
                                ConceptoVenta: {
                                    Codigo: itmOtroConcepto.ConceptosVentas.Codigo,
                                    Nombre: itmOtroConcepto.ConceptosVentas.Nombre,
                                    Operacion: itmOtroConcepto.ConceptosVentas.Operacion
                                },
                                Descripcion: itmOtroConcepto.Descripcion,
                                ValorVenta: itmOtroConcepto.Valor_Concepto
                            };
                            $scope.ListadoOtrosConceptosFactura.push(auxOtr);
                        });
                        IndiceOtrosConceptos();
                        $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
                        ResetPaginacion($scope.PagRemesasGuardadas, $scope.DeshabilitarPaginacion);

                        var tmpImpuestoDocu = [];
                        var tmpImpuestoConc = [];

                        var DetalleImpuestos = response.data.Datos.DetalleImpuestosFacturas;
                        for (var i = 0; i < DetalleImpuestos.length; i++) {
                            switch (DetalleImpuestos[i].CATA_TVFI_Codigo) {
                                case CODIGO_CATALOGO_TIPO_VALOR_FACTURA_IMPUESTOS_REMESAS:
                                    for (var j = 0; j < $scope.ListadoImpuestosAgregadosFactura.length; j++) {
                                        if (DetalleImpuestos[i].ImpuestoFacturas.Codigo == $scope.ListadoImpuestosAgregadosFactura[j].Codigo) {
                                            tmpImpuestoDocu.push({
                                                Codigo: DetalleImpuestos[i].ImpuestoFacturas.Codigo,
                                                Nombre: DetalleImpuestos[i].ImpuestoFacturas.Nombre,
                                                Operacion: $scope.ListadoImpuestosAgregadosFactura[j].Operacion,
                                                Valor_base: DetalleImpuestos[i].Valor_base,
                                                Valor_tarifa: DetalleImpuestos[i].Valor_tarifa,
                                                Valor_impuesto: DetalleImpuestos[i].Valor_impuesto
                                            });
                                        }
                                    }
                                    break;
                                case CODIGO_CATALOGO_TIPO_VALOR_FACTURA_IMPUESTOS_OTROS_CONCEPTOS:
                                    for (var j = 0; j < $scope.ListaImpuestosAgregadosConceptos.length; j++) {
                                        if (DetalleImpuestos[i].ImpuestoFacturas.Codigo == $scope.ListaImpuestosAgregadosConceptos[j].Codigo) {
                                            tmpImpuestoConc.push({
                                                Codigo: DetalleImpuestos[i].ImpuestoFacturas.Codigo,
                                                Nombre: DetalleImpuestos[i].ImpuestoFacturas.Nombre,
                                                Operacion: $scope.ListaImpuestosAgregadosConceptos[j].Operacion,
                                                valor_base: DetalleImpuestos[i].Valor_base,
                                                Valor_tarifa: DetalleImpuestos[i].Valor_tarifa,
                                                Valor_impuesto: DetalleImpuestos[i].Valor_impuesto
                                            });
                                        }
                                    }
                                    break;
                            }
                        }
                        $scope.ListadoImpuestosAgregadosFactura = tmpImpuestoDocu;
                        $scope.ListaImpuestosAgregadosConceptos = tmpImpuestoConc;

                        if (response.data.Datos.Estado == ESTADO_BORRADOR) {
                            $scope.Calcular();
                        }
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    }
                    else {
                        ShowError('No se logro consultar la factura No. ' + $scope.Numero + '. ' + response.data.MensajeOperacion);
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); }; document.location.href = '#!ConsultarFacturaVentas'; }, 500);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); }; document.location.href = '#!ConsultarFacturaVentas'; }, 500);
                });
        }
        //-------------Obtener
        //-------------Guardar
        $scope.ConfirmacionGuardar = function () {
            if ($scope.DeshabilitarActualizar) {
                location.href = '#!ConsultarPlanillaGuias/' + $scope.Modelo.Planilla.Numero;
            } else {
                showModal('modalConfirmacionGuardar');
            }
        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                $scope.PeriodoValido = true
                FindCierre();
                if ($scope.PeriodoValido) {


                    blockUIConfig.autoBlock = true;
                    blockUIConfig.delay = 0;
                    blockUI.start("Guardando documento...");
                    $timeout(function () { blockUI.message("Guardando documento..."); }, 100);

                    $scope.ListaAux = [];
                    $scope.ListadoRemesasGuardadas.forEach(function (item) {
                        var Reme = {
                            Remesas: {
                                Numero: item.Numero, NumeroOrdenServicio: item.NumeroOrdenServicio,
                                PesoCliente: item.PesoCliente,
                                ValorFleteCliente: item.ValorFleteCliente,
                                TotalFleteCliente: MascaraNumero(item.TotalFleteCliente),
                                Entrega: item.Entrega,
                                DT: item.DT
                            },
                            Modifica: item.Modifica
                        };
                        $scope.ListaAux.push(Reme);
                    });

                    $scope.ListaAuxOtrosConceptosFactura = [];
                    for (var i = 0; i < $scope.ListadoOtrosConceptosFactura.length; i++) {
                        var TmpListadoImpuestosConceptos = [];
                        var SumImpuestoConcepto = 0;
                        var listaImpuestos = $scope.ListadoOtrosConceptosFactura[i].ListadoImpuestos;
                        if (listaImpuestos !== undefined && listaImpuestos !== null && listaImpuestos !== []) {
                            for (var j = 0; j < listaImpuestos.length; j++) {
                                var existe = false;
                                for (var k = 0; k < $scope.ListaImpuestosAgregadosConceptos.length; k++) {
                                    if (listaImpuestos[j].Codigo == $scope.ListaImpuestosAgregadosConceptos[k].Codigo) {
                                        existe = true;
                                        break;
                                    }
                                }
                                if (existe) {
                                    var objImpuesto = {
                                        ENIM_Codigo: listaImpuestos[j].Codigo,
                                        Valor_tarifa: listaImpuestos[j].Valor_tarifa,
                                        Valor_base: listaImpuestos[j].valor_base,
                                        Valor_impuesto: listaImpuestos[j].Valor_impuesto,
                                    };
                                    switch (listaImpuestos[j].Operacion) {
                                        case 1:
                                            SumImpuestoConcepto += listaImpuestos[j].Valor_impuesto;
                                            break;
                                        case 2:
                                            SumImpuestoConcepto -= listaImpuestos[j].Valor_impuesto;
                                            break;
                                    }
                                    TmpListadoImpuestosConceptos.push(objImpuesto);
                                }
                            }
                        }
                        var otroConcepto = {
                            DetalleNODE: $scope.ListadoOtrosConceptosFactura[i].DetalleNODE,
                            ConceptosVentas: { Codigo: $scope.ListadoOtrosConceptosFactura[i].ConceptoVenta.Codigo },
                            Descripcion: $scope.ListadoOtrosConceptosFactura[i].Descripcion,
                            Valor_Concepto: $scope.ListadoOtrosConceptosFactura[i].ValorVenta,
                            Valor_Impuestos: SumImpuestoConcepto,
                            Remesas: { Numero: $scope.ListadoOtrosConceptosFactura[i].NumeroRemesa.Numero },
                            NumeroOrdenServicio: CERO,
                            Emoe_numero: CERO,
                            DetalleImpuestoConceptosFacturas: TmpListadoImpuestosConceptos
                        };
                        $scope.ListaAuxOtrosConceptosFactura.push(otroConcepto);
                    }

                    var TmpDetalleImpuestosFacturas = [];
                    $scope.ListadoImpuestosAgregadosFactura.forEach(function (item) {
                        if (item.Valor_impuesto > CERO) {
                            var impuesto = {
                                ImpuestoFacturas: { Codigo: item.Codigo },
                                CATA_TVFI_Codigo: CODIGO_CATALOGO_TIPO_VALOR_FACTURA_IMPUESTOS_REMESAS,
                                Valor_tarifa: item.Valor_tarifa,
                                Valor_base: item.Valor_base,
                                Valor_impuesto: item.Valor_impuesto
                            }
                            TmpDetalleImpuestosFacturas.push(impuesto);
                        }
                    });
                    $scope.ListaImpuestosAgregadosConceptos.forEach(function (item) {
                        if (item.Valor_impuesto > CERO) {
                            var impuesto = {
                                ImpuestoFacturas: { Codigo: item.Codigo },
                                CATA_TVFI_Codigo: CODIGO_CATALOGO_TIPO_VALOR_FACTURA_IMPUESTOS_OTROS_CONCEPTOS,
                                Valor_tarifa: item.Valor_tarifa,
                                Valor_base: item.valor_base,
                                Valor_impuesto: item.Valor_impuesto
                            };
                            TmpDetalleImpuestosFacturas.push(impuesto);
                        }
                    });
                    var ingresarFacturarA = false;
                    if (($scope.Modelo.Cliente == undefined || $scope.Modelo.Cliente == '' || $scope.Modelo.Cliente == null) && $scope.Sesion.UsuarioAutenticado.ManejoFacturarA == true) {
                        ingresarFacturarA = true;
                    }
                    $scope.Calcular();
                    var CuentaPorCobrar = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                        CodigoAlterno: '',
                        CataTipoFactura: CODIGO_CATALOGO_TIPO_FACTURA,
                        Fecha: $scope.Modelo.Fecha,
                        Tercero: { Codigo: ingresarFacturarA == true ? $scope.Modelo.FacturarA.Codigo : $scope.Modelo.Cliente.Codigo },
                        DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_FACTURA },
                        CodigoDocumentoOrigen: CERO,
                        NumeroDocumento: CERO,
                        Numeracion: '',
                        CuentaPuc: { Codigo: CERO },
                        ValorTotal: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.ValorFactura)),
                        Abono: CERO,
                        Saldo: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.ValorFactura)),
                        FechaCancelacionPago: $scope.Modelo.Fecha,
                        Aprobado: 1,
                        UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        FechaAprobo: $scope.Modelo.Fecha,
                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        Vehiculo: { Codigo: CERO },
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    };

                    var Factura = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_FACTURAS,
                        Numero: $scope.Numero,
                        NumeroPreImpreso: CERO,
                        CataTipoFactura: CODIGO_CATALOGO_TIPO_FACTURA,
                        Fecha: $scope.Modelo.Fecha,
                        OficinaFactura: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },

                        Cliente: { Codigo: ingresarFacturarA == true ? $scope.Modelo.FacturarA.Codigo : $scope.Modelo.Cliente.Codigo },
                        FacturarA: { Codigo: $scope.Modelo.FacturarA.Codigo },
                        SedeFacturacion: $scope.Modelo.SedeFacturacion,
                        FormaPago: { Codigo: $scope.Modelo.FormaPago.Codigo },
                        DiasPlazo: $scope.Modelo.DiasPlazo,
                        Observaciones: $scope.Modelo.Observaciones,

                        ValorRemesas: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.ValorRemesas)),
                        ValorOtrosConceptos: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.ValorOtrosConceptos)),
                        ValorDescuentos: CERO,
                        Subtotal: $scope.Modelo.ValorSubtotal,
                        ValorImpuestos: $scope.Modelo.ValorImpuestos,

                        ValorFactura: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.ValorFactura)),
                        ValorTRM: CERO,
                        Moneda: { Codigo: 1 },
                        ValorMonedaAlterna: CERO,
                        ValorAnticipo: CERO,

                        ResolucionFacturacion: $scope.Sesion.UsuarioAutenticado.Oficinas.ResolucionFacturacion,
                        ControlImpresion: 1,
                        Estado: $scope.Modelo.Estado.Codigo,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Anulado: CERO,
                        Remesas: $scope.ListaAux,
                        OtrosConceptos: $scope.ListaAuxOtrosConceptosFactura,
                        DetalleImpuestosFacturas: TmpDetalleImpuestosFacturas,
                        CuentaPorCobrar: CuentaPorCobrar,
                        NumeroOrdenCompra: $scope.Modelo.NOrdenCompra
                    };
                    FacturasFactory.Guardar(Factura).
                        then(function (response) {
                            if (response.data.ProcesoExitoso == true) {
                                if (response.data.Datos > CERO) {
                                    if ($scope.Numero == CERO) {
                                        ShowSuccess('Se guardó la factura número ' + response.data.Datos + ' correctamente');
                                    }
                                    else {
                                        ShowSuccess('Se modificó la factura número ' + response.data.Datos + ' correctamente');
                                    }
                                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); }; location.href = '#!ConsultarFacturaVentas/' + response.data.Numero; }, 500);
                                }
                                else {
                                    if (response.data.Datos == -1) {
                                        ShowError("La oficina no cuenta con consecutivo de facturación asignado");
                                    }
                                    else {
                                        ShowError(response.statusText);
                                    }
                                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                                }
                            }
                            else {
                                ShowError(response.statusText);
                                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        });
                }
            }
        };
        //-------------Guardar
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo;

            if (!$scope.esOficinaMadreFactuacion) {
                $scope.MensajesError.push('La oficina actual no corresponde a una oficina madre de facturación');
                continuar = false;
            }

            if (Modelo.Fecha == undefined || Modelo.Fecha == '' || Modelo.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }
            if ($scope.Sesion.UsuarioAutenticado.ManejoFacturarA != true) {
                if (Modelo.Cliente == undefined || Modelo.Cliente == '' || Modelo.Cliente == null) {
                    $scope.MensajesError.push('Debe ingresar el cliente');
                    continuar = false;
                }
            }
            if (Modelo.FacturarA == undefined || Modelo.FacturarA == '' || Modelo.FacturarA == null) {
                $scope.MensajesError.push('Debe ingresar el tercero a facturar');
                continuar = false;
            }
            if (Modelo.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA) {//No Aplica
                $scope.MensajesError.push('Debe ingresar la forma de pago');
                continuar = false;
            }
            if (Modelo.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {//Credito
                if ($scope.Modelo.DiasPlazo <= CERO || $scope.Modelo.DiasPlazo == undefined || $scope.Modelo.DiasPlazo == '' || $scope.Modelo.DiasPlazo == null) {
                    $scope.MensajesError.push('Debe ingresar los días de plazo');
                    continuar = false;
                }
            }
            var cont = 0;
            cont = $scope.ListadoRemesasGuardadas.length;
            if (cont == 0) {
                $scope.MensajesError.push('Debe seleccionar al menos una remesa a facturar');
                continuar = false;
            }
            return continuar;
        }

        $scope.Calcular = function () {
            $scope.Modelo.ValorRemesas = 0;
            $scope.Modelo.ValorOtrosConceptos = 0;
            $scope.Modelo.ValorFactura = 0;
            $scope.Modelo.ValorImpuestos = 0;
            resetVistasImpuestos();

            //---------------------- Remesas ----------------------//
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                var item = $scope.ListadoRemesasGuardadas[i];
                $scope.Modelo.ValorRemesas += Math.round(MascaraNumero(item.TotalFleteCliente));
            }

            TMPREMEImpuestos = [];
            var SumatoriaREME = $scope.Modelo.ValorRemesas;
            if (SumatoriaREME > CERO) {
                for (var j = 0; j < $scope.ListadoImpuestosFactura.length; j++) {
                    var Valor_base = CERO;
                    if (SumatoriaREME >= $scope.ListadoImpuestosFactura[j].Valor_base) {//verifica si aplica o no con el valor existente
                        Valor_base = SumatoriaREME;
                    }
                    else {
                        Valor_base = CERO;
                    }
                    TMPREMEImpuestos.push({
                        Codigo: $scope.ListadoImpuestosFactura[j].Codigo,
                        Valor_base: Valor_base,
                        Valor_impuesto: Math.round(Valor_base * $scope.ListadoImpuestosFactura[j].Valor_tarifa)
                    });
                }

                for (var i = 0; i < $scope.ListadoImpuestosAgregadosFactura.length; i++) {
                    for (var j = 0; j < TMPREMEImpuestos.length; j++) {
                        if ($scope.ListadoImpuestosAgregadosFactura[i].Codigo == TMPREMEImpuestos[j].Codigo) {
                            $scope.ListadoImpuestosAgregadosFactura[i].Valor_base += TMPREMEImpuestos[j].Valor_base;
                            $scope.ListadoImpuestosAgregadosFactura[i].Valor_impuesto += TMPREMEImpuestos[j].Valor_impuesto;
                        }
                    }
                }
            }


            //---------------------- Remesas ----------------------//
            //---------------------- Otros Conceptos ----------------------//
            for (var i = 0; i < $scope.ListadoOtrosConceptosFactura.length; i++) {
                if ($scope.ListadoOtrosConceptosFactura[i].ConceptoVenta.Operacion == 2) {
                    $scope.Modelo.ValorOtrosConceptos -= $scope.ListadoOtrosConceptosFactura[i].ValorVenta;
                } else {
                    $scope.Modelo.ValorOtrosConceptos += $scope.ListadoOtrosConceptosFactura[i].ValorVenta;
                }
            }

            var ImpuestosOtrosConceptos = [];
            for (var i = 0; i < $scope.ListadoOtrosConceptosFactura.length; i++) {
                for (var j = 0; j < $scope.ListadoConceptosFactura.length; j++) {
                    if ($scope.ListadoOtrosConceptosFactura[i].ConceptoVenta.Codigo == $scope.ListadoConceptosFactura[j].Codigo) {
                        $scope.ListadoOtrosConceptosFactura[i].ListadoImpuestos = angular.copy($scope.ListadoConceptosFactura[j].ListadoImpuestos);
                        ImpuestosOtrosConceptos = $scope.ListadoOtrosConceptosFactura[i].ListadoImpuestos;
                        for (var k = 0; k < ImpuestosOtrosConceptos.length; k++) {
                            if ($scope.ListadoOtrosConceptosFactura[i].ValorVenta >= ImpuestosOtrosConceptos[k].valor_base) {
                                ImpuestosOtrosConceptos[k].valor_base = $scope.ListadoOtrosConceptosFactura[i].ValorVenta;
                            }
                            else {
                                ImpuestosOtrosConceptos[k].valor_base = CERO;
                            }
                            ImpuestosOtrosConceptos[k].Valor_impuesto = Math.round(ImpuestosOtrosConceptos[k].valor_base * ImpuestosOtrosConceptos[k].Valor_tarifa);
                        }
                    }
                }
            }

            for (var i = 0; i < $scope.ListaImpuestosAgregadosConceptos.length; i++) {
                for (var j = 0; j < $scope.ListadoOtrosConceptosFactura.length; j++) {
                    ImpuestosOtrosConceptos = $scope.ListadoOtrosConceptosFactura[j].ListadoImpuestos;
                    if (ImpuestosOtrosConceptos != undefined) {
                        for (var k = 0; k < ImpuestosOtrosConceptos.length; k++) {
                            if ($scope.ListaImpuestosAgregadosConceptos[i].Codigo == ImpuestosOtrosConceptos[k].Codigo) {
                                $scope.ListaImpuestosAgregadosConceptos[i].valor_base += ImpuestosOtrosConceptos[k].valor_base;
                                $scope.ListaImpuestosAgregadosConceptos[i].Valor_impuesto += ImpuestosOtrosConceptos[k].Valor_impuesto;
                            }
                        }
                    }
                }
            }
            //---------------------- Otros Conceptos ----------------------//
            for (var i = 0; i < $scope.ListadoImpuestosAgregadosFactura.length; i++) {
                switch ($scope.ListadoImpuestosAgregadosFactura[i].Operacion) {
                    case 1:
                        $scope.Modelo.ValorImpuestos += $scope.ListadoImpuestosAgregadosFactura[i].Valor_impuesto;
                        break;
                    case 2:
                        $scope.Modelo.ValorImpuestos -= $scope.ListadoImpuestosAgregadosFactura[i].Valor_impuesto;
                        break;
                    default:
                        $scope.Modelo.ValorImpuestos += $scope.ListadoImpuestosAgregadosFactura[i].Valor_impuesto;
                        break;
                }
            }

            for (var i = 0; i < $scope.ListaImpuestosAgregadosConceptos.length; i++) {
                switch ($scope.ListaImpuestosAgregadosConceptos[i].Operacion) {
                    case 1:
                        $scope.Modelo.ValorImpuestos += $scope.ListaImpuestosAgregadosConceptos[i].Valor_impuesto;
                        break;
                    case 2:
                        $scope.Modelo.ValorImpuestos -= $scope.ListaImpuestosAgregadosConceptos[i].Valor_impuesto;
                        break;
                    default:
                        $scope.Modelo.ValorImpuestos += $scope.ListaImpuestosAgregadosConceptos[i].Valor_impuesto;
                        break;
                }
            }
            $scope.Modelo.ValorSubtotal = $scope.Modelo.ValorRemesas + $scope.Modelo.ValorOtrosConceptos;
            $scope.Modelo.ValorFactura = $scope.Modelo.ValorRemesas + $scope.Modelo.ValorOtrosConceptos + $scope.Modelo.ValorImpuestos;
            //Agrega Mascaras
            $scope.Modelo.ValorSubtotal = $scope.MaskValoresGrid($scope.Modelo.ValorSubtotal);
            $scope.Modelo.ValorFactura = $scope.MaskValoresGrid($scope.Modelo.ValorFactura);
            $scope.Modelo.ValorOtrosConceptos = $scope.MaskValoresGrid($scope.Modelo.ValorOtrosConceptos);
            $scope.Modelo.ValorImpuestos = $scope.MaskValoresGrid($scope.Modelo.ValorImpuestos);
            $scope.Modelo.ValorRemesas = $scope.MaskValoresGrid($scope.Modelo.ValorRemesas);
        };

        function resetVistasImpuestos() {
            //---Inicializa valor impuesto vista Factura 
            if ($scope.ListadoImpuestosAgregadosFactura != undefined) {
                $scope.ListadoImpuestosAgregadosFactura.forEach(function (item) {
                    item.Valor_base = CERO;
                    item.Valor_impuesto = CERO;
                });
            }
            //---Inicializa valor impuesto vista Factura
            //---Inicializa valor impuesto Concepto vista Factura
            if ($scope.ListaImpuestosAgregadosConceptos != undefined) {
                $scope.ListaImpuestosAgregadosConceptos.forEach(function (item) {
                    item.valor_base = CERO;
                    item.Valor_impuesto = CERO;
                });
            }
            //---Inicializa valor impuesto Concepto vista Factura
        }

        $scope.AsignarFormaPago = function (cod) {
            if (cod == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                $('#DiasPlazo').show();
            } else {
                $('#DiasPlazo').hide();
                $scope.Modelo.DiasPlazo = 0;
            }
        };
        $scope.MostrarCriteriosBusqueda = function () {
            if ($scope.Modelo.Cliente != undefined) {
                $('.CriteriosBusqueda').show();
            } else if ($scope.Modelo.FacturarA.Codigo > 0 && $scope.Sesion.UsuarioAutenticado.ManejoFacturarA == true) {
                $('.CriteriosBusqueda').show();
            }
        };

        $scope.ObtenerSedesTercero = function () {
            //--Carga sedes tercero
            if ($scope.Modelo.FacturarA != undefined && $scope.Modelo.FacturarA != '' && $scope.Modelo.FacturarA != null) {
                if ($scope.ctr.SedeFacturacion.Visible) {
                    var RespoObtTerc = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.FacturarA.Codigo, Sync: true });
                    if (RespoObtTerc.ProcesoExitoso == true) {
                        if (RespoObtTerc.Datos.Codigo > 0) {
                            $scope.ListadoSedesTercero = RespoObtTerc.Datos.Direcciones;
                        }
                    }
                }
            }
        };

        $scope.VolverMaster = function () {
            if ($scope.Numero == 0) {
                document.location.href = '#!ConsultarFacturaVentas';
            }
            else {
                document.location.href = '#!ConsultarFacturaVentas/' + $scope.Numero;
            }
        };
        //----Modal Conceptos
        $scope.TmpEliminarConcepto = {};
        $scope.EsEditarConcepto = false;
        $scope.IndiceEditar = 0;
        function limpiarModalConceptoFactura() {
            $scope.ENRE_Numero_OtrosConceptos = '';
            $scope.Concepto = '';
            $scope.DescripcionConcepto = '';
            $scope.ValorConcepto = '';
            $scope.IndiceEditar = 0;
            $scope.EsEditarConcepto = false;
        }
        //Restaura Todo los Valores
        $scope.InicializarVistaModalConceptoFactura = function () {
            $scope.MensajesErrorConceptos = [];
            $scope.ListadoRemeConceAutoComplete = [];
            $scope.ListadoRemesasGuardadas.forEach(function (itmRemGuia) {
                $scope.ListadoRemeConceAutoComplete.push({
                    Numero: itmRemGuia.Numero,
                    NumeroDocumento: itmRemGuia.NumeroDocumento
                });
            });
            showModal('modalConceptoFactura');
            limpiarModalConceptoFactura();
        }
        // Metodo para cerrar la modal de conceptos
        $scope.AsignarListadoConcepto = function (value) {
            return RetornarListaAutocomplete(value, $scope.ListadoConceptosFactura, 'Nombre', 'CodigoAlterno');
        }
        // Metodo para cerrar la modal de conceptos
        $scope.CerrarModalConcepto = function () {
            $scope.MensajesErrorRecorrido = [];
            closeModal('modalConceptoFactura');
        };

        //--Inserta los conceptos en el grid de conceptos
        $scope.CargarConceptos = function () {
            if (DatosRequeridosConceptos()) {
                closeModal('modalConceptoFactura');
                if ($scope.EsEditarConcepto == true) {
                    $scope.ListadoOtrosConceptosFactura[$scope.IndiceEditar].NumeroRemesa = $scope.ENRE_Numero_OtrosConceptos;
                    $scope.ListadoOtrosConceptosFactura[$scope.IndiceEditar].ConceptoVenta = $scope.Concepto;
                    $scope.ListadoOtrosConceptosFactura[$scope.IndiceEditar].Descripcion = $scope.DescripcionConcepto;
                    $scope.ListadoOtrosConceptosFactura[$scope.IndiceEditar].ValorVenta = MascaraNumero(MascaraValores($scope.ValorConcepto));
                }
                else {
                    $scope.ListadoOtrosConceptosFactura.push({
                        DetalleNODE: { Codigo: CERO },
                        NumeroRemesa: $scope.ENRE_Numero_OtrosConceptos,
                        ConceptoVenta: $scope.Concepto,
                        Descripcion: $scope.DescripcionConcepto,
                        ValorVenta: MascaraNumero(MascaraValores($scope.ValorConcepto))
                    });
                }
                if ($scope.ListaImpuestosAgregadosConceptos.length == 0) {
                    $scope.RestablecerImpuestos(2);
                }
                IndiceOtrosConceptos();
                $scope.Calcular();
            }
        };
        //--Inserta los conceptos en el grid de conceptos
        //--Se establecen los datos requeridos de la modal de conceptos
        function DatosRequeridosConceptos() {
            $scope.MensajesErrorConceptos = [];
            var continuar = true;

            if ($scope.ENRE_Numero_OtrosConceptos == undefined || $scope.ENRE_Numero_OtrosConceptos == "" || $scope.ENRE_Numero_OtrosConceptos == null) {
                $scope.MensajesErrorConceptos.push('Debe ingresar un N. de remesa');
                continuar = false;
            }


            if ($scope.ValorConcepto == undefined || $scope.ValorConcepto == "" || $scope.ValorConcepto == null) {
                $scope.MensajesErrorConceptos.push('Debe ingresar un concepto');
                continuar = false;
            }

            if ($scope.DescripcionConcepto == undefined || $scope.DescripcionConcepto == "" || $scope.DescripcionConcepto == null) {
                $scope.MensajesErrorConceptos.push('Debe ingresar una descripcion del concepto');
                continuar = false;
            }
            if ($scope.ValorConcepto == undefined || $scope.ValorConcepto == "" || $scope.ValorConcepto == null) {
                $scope.MensajesErrorConceptos.push('Debe ingresar  el valor del concepto');
                continuar = false;
            }

            return continuar;
        }
        //--Se establecen los datos requeridos de la modal de conceptos
        //--Eliminar conceptos facturas
        $scope.ConfirmacionEliminarConcepto = function (Concepto, indice) {
            $scope.TmpEliminarConcepto = { Concepto, Indice: indice };
            showModal('modalEliminarConcepto');
        };

        $scope.EliminarConcepto = function (indice) {
            $scope.ListadoOtrosConceptosFactura.splice(indice, 1);
            IndiceOtrosConceptos();
            $scope.Calcular();
            closeModal('modalEliminarConcepto');
        };
        //--Eliminar conceptos facturas
        $scope.EditarConcepto = function (indice) {
            $scope.ENRE_Numero_OtrosConceptos = $scope.ListadoOtrosConceptosFactura[indice].NumeroRemesa;
            $scope.Concepto = $scope.ListadoOtrosConceptosFactura[indice].ConceptoVenta;
            $scope.DescripcionConcepto = $scope.ListadoOtrosConceptosFactura[indice].Descripcion;
            $scope.ValorConcepto = $scope.ListadoOtrosConceptosFactura[indice].ValorVenta;
            $scope.MaskNumero(); $scope.MaskValores();
            $scope.IndiceEditar = indice;
            $scope.EsEditarConcepto = true;
            showModal('modalConceptoFactura');
        };
        function IndiceOtrosConceptos() {
            for (var i = 0; i < $scope.ListadoOtrosConceptosFactura.length; i++) {
                $scope.ListadoOtrosConceptosFactura[i].Indice = i;
            }
        }
        //--Modal Conceptos
        $scope.CalcularFleteCliente = function (item) {
            if (item.TipoDespacho.Codigo == 18202) {
                item.TotalFleteCliente = MascaraValores(Math.round((parseInt(MascaraNumero(item.PesoCliente)) * parseInt(MascaraNumero(item.ValorFleteCliente))) / 1000));
            } else {
                item.TotalFleteCliente = MascaraValores(parseInt(MascaraNumero(item.ValorFleteCliente)));
            }
            $scope.Calcular();
            //$scope.CheckRemesaFactura(item.Seleccionado, item.Numero)
        };
        //---------------------------------Modal Eliminar Impuestos
        $scope.RestablecerImpuestos = function (tipo) {
            switch (tipo) {
                case 1:
                    //--Impuesto documento
                    $scope.ListadoImpuestosAgregadosFactura = angular.copy($scope.ListadoImpuestosFactura);
                    break;
                case 2:
                    //--Impuesto concepto
                    $scope.ListaImpuestosAgregadosConceptos = angular.copy($scope.ListadoImpuestosConceptosFactura);
                    break;
                default:
                    $scope.ListadoImpuestosAgregadosFactura = angular.copy($scope.ListadoImpuestosFactura);
                    $scope.ListaImpuestosAgregadosConceptos = angular.copy($scope.ListadoImpuestosConceptosFactura);
                    break;
            }
            $scope.Calcular();
        };
        $scope.ConfirmacionEliminarImpuesto = function (item, tipo) {
            $scope.TmpEliminarImpuesto = item;
            $scope.TmpEliminarImpuesto.tipo = tipo;
            showModal("modalEliminarImpuesto");
        };
        $scope.EliminarImpuesto = function (item) {
            closeModal("modalEliminarImpuesto");
            var tmp = [];
            switch (item.tipo) {
                case 1:
                    //--Impuesto documento
                    for (var i = 0; i < $scope.ListadoImpuestosAgregadosFactura.length; i++) {
                        if (item.Codigo != $scope.ListadoImpuestosAgregadosFactura[i].Codigo) {
                            tmp.push($scope.ListadoImpuestosAgregadosFactura[i]);
                        }
                    }
                    $scope.ListadoImpuestosAgregadosFactura = tmp;
                    break;
                case 2:
                    //--Impuesto concepto
                    for (var i = 0; i < $scope.ListaImpuestosAgregadosConceptos.length; i++) {
                        if (item.Codigo != $scope.ListaImpuestosAgregadosConceptos[i].Codigo) {
                            tmp.push($scope.ListaImpuestosAgregadosConceptos[i]);
                        }
                    }
                    $scope.ListaImpuestosAgregadosConceptos = tmp;
                    break;
            }
            $scope.Calcular();
        };
        //---------------------------------Modal Eliminar Impuestos


        //--------------------------------------ExportarExcel-------------------------------------------------//

        //Obtiene la URL para llamar el proyecto ASP
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

        //Función que ejecuta el boton de vista preliminar
        $scope.ExportarExcel = function (OpcionPDf, OpcionEXCEL) {

            $scope.FiltroArmado = '';
            //$scope.TipoDocumento = TIPO_FACTURA_TRANSPORTE_ESPECIAL

            //Depende del listado seleccionado se enviará el nombre por parametro

            $scope.NombreListado = NOMBRE_LISTADO_REMESAS_X_FACTURAR
            //Arma el filtro
            $scope.ArmarFiltro();
            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreListado + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreListado + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL);
            }
            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.ArmarFiltro = function () {

            $scope.FiltroArmado = '';
            if (!DocumentoGuardado) {
                if ($scope.Filtro.NumeroInicial !== undefined && $scope.Filtro.NumeroInicial !== '' && $scope.Filtro.NumeroInicial !== null) {
                    $scope.FiltroArmado += '&NumeroDocumento=' + $scope.Filtro.NumeroInicial;
                }
                if ($scope.Filtro.TipoRemesa !== undefined && $scope.Filtro.TipoRemesa !== '' && $scope.Filtro.TipoRemesa !== null) {
                    $scope.FiltroArmado += '&TipoDocumento=' + $scope.Filtro.TipoRemesa.Codigo;
                }
                if ($scope.Filtro.FechaInicial !== undefined && $scope.Filtro.FechaInicial !== '' && $scope.Filtro.FechaInicial !== null) {
                    $scope.FiltroArmado += '&FechaInicial=' + moment(new Date($scope.Filtro.FechaInicial)).format('YYYY-MM-DD');
                }
                if ($scope.Filtro.FechaFinal !== undefined && $scope.Filtro.FechaFinal !== '' && $scope.Filtro.FechaFinal !== null) {
                    $scope.FiltroArmado += '&FechaFinal=' + moment(new Date($scope.Filtro.FechaFinal)).format('YYYY-MM-DD');
                }
                if ($scope.Filtro.OrdenServicio !== undefined && $scope.Filtro.OrdenServicio !== '' && $scope.Filtro.OrdenServicio !== null) {
                    $scope.FiltroArmado += '&OrdenServicio=' + $scope.Filtro.OrdenServicio;
                }
                if ($scope.Filtro.PlanillaDespacho !== undefined && $scope.Filtro.PlanillaDespacho !== '' && $scope.Filtro.PlanillaDespacho !== null) {
                    $scope.FiltroArmado += '&PlanillaDespacho=' + $scope.Filtro.PlanillaDespacho;
                }
                if ($scope.Filtro.NumeroDocumentoCliente !== undefined && $scope.Filtro.NumeroDocumentoCliente !== '' && $scope.Filtro.NumeroDocumentoCliente !== null) {
                    $scope.FiltroArmado += '&NumeroDocumentoCliente=' + $scope.Filtro.NumeroDocumentoCliente;
                }
                if ($scope.Modelo.Cliente !== undefined && $scope.Modelo.Cliente !== '' && $scope.Modelo.Cliente !== null) {
                    $scope.FiltroArmado += '&Cliente=' + $scope.Modelo.Cliente.Codigo;
                }
                if ($scope.Modelo.Cliente !== undefined && $scope.Modelo.Cliente !== '' && $scope.Modelo.Cliente !== null) {
                    $scope.FiltroArmado += '&NombreCliente=' + $scope.Modelo.Cliente.NombreCompleto;
                }

                $scope.FiltroArmado += '&FechaGeneracionDocumento=' + moment(new Date()).format('DD-MM-YYYY_h:mm:ss_a');

                if ($scope.Filtro.Sede !== undefined && $scope.Filtro.Sede !== '' && $scope.Filtro.Sede !== null && $scope.Filtro.Sede > 0) {
                    $scope.FiltroArmado += '&Sede=' + $scope.Filtro.Sede.Codigo;
                }
                if ($scope.Filtro.FacturarA !== undefined && $scope.Filtro.FacturarA !== '' && $scope.Filtro.FacturarA !== null) {
                    $scope.FiltroArmado += '&FacturarA=' + $scope.Filtro.FacturarA.Codigo;
                }
                if ($scope.Filtro.CiudadOrigen !== undefined && $scope.Filtro.CiudadOrigen !== '' && $scope.Filtro.CiudadOrigen !== null) {
                    $scope.FiltroArmado += '&CiudadOrigen=' + $scope.Filtro.CiudadOrigen.Codigo;
                }
                if ($scope.Filtro.CiudadDestino !== undefined && $scope.Filtro.CiudadDestino !== '' && $scope.Filtro.CiudadDestino !== null) {
                    $scope.FiltroArmado += '&CiudadDestino=' + $scope.Filtro.CiudadDestino.Codigo;
                }
                if ($scope.Filtro.Producto !== undefined && $scope.Filtro.Producto !== '' && $scope.Filtro.Producto !== null) {
                    $scope.FiltroArmado += '&Producto=' + $scope.Filtro.Producto.Codigo;
                }
            } else {
                $scope.FiltroArmado += '&NumeroFactura=' + $scope.Numero;
                if ($scope.Modelo.Cliente !== undefined && $scope.Modelo.Cliente !== '' && $scope.Modelo.Cliente !== null) {
                    $scope.FiltroArmado += '&Cliente=' + $scope.Modelo.Cliente.Codigo;
                }
                if ($scope.Modelo.Cliente !== undefined && $scope.Modelo.Cliente !== '' && $scope.Modelo.Cliente !== null) {
                    $scope.FiltroArmado += '&NombreCliente=' + $scope.Modelo.Cliente.NombreCompleto;
                }

                $scope.FiltroArmado += '&FechaGeneracionDocumento=' + moment(new Date()).format('DD-MM-YYYY_h:mm:ss_a');

                if ($scope.Filtro.FacturarA !== undefined && $scope.Filtro.FacturarA !== '' && $scope.Filtro.FacturarA !== null) {
                    $scope.FiltroArmado += '&FacturarA=' + $scope.Filtro.FacturarA.Codigo;
                }
            }


        }

        //Lectura  de archivo:
        /*-----------------------------------------------------------------------funciones de lectura y apertura de archivos----------------------------------------------------------------------------*/

        $scope.ListaProgramaciones = []
        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined) {
                return true
            } else {
                return false
            }
        }
        var strIdentificacionesDestinatarios = '';
        function AsignarValoresTabla() {
            $scope.NumeroCargue = 0

            //if ($scope.DataArchivo.Valores.length > 0) {
            if ($scope.DataArchivo.Hoja1.length > 0) {
                var DataTemporal = []
                //for (var i = 0; i < $scope.DataArchivo.Valores.length; i++) {
                for (var i = 0; i < $scope.DataArchivo.Hoja1.length; i++) {
                    // var item = $scope.DataArchivo.Valores[i];
                    var item = $scope.DataArchivo.Hoja1[i];
                    DataTemporal.push(item);
                }
                $scope.DataArchivo = DataTemporal
                $scope.totalRegistrosCargue = ($scope.DataArchivo.length)
                $scope.paginaActualCargue = 1
                $scope.totalPaginasCargue = Math.ceil($scope.totalRegistrosCargue / 20);
                blockUI.start();
                blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');

                $timeout(function () {
                    try {
                        var data = []
                        var contErrores = 0
                        $scope.DataRemesasActualizar = []
                        try {
                            $scope.DataArchivo.forEach(item => {
                                try {
                                    var CodigoProducto = item.Producto == undefined ? '' : item.Producto.split("-")[0];
                                    $scope.DataRemesasActualizar.push({
                                        NumeroDocumento: parseInt(item.Remesa.split('-')[1]),
                                        CodigoClientePagador: item.Codigo_Cliente_Pagador == undefined ? '' : item.Codigo_Cliente_Pagador,
                                        DT: item.DT == undefined ? '' : item.DT,
                                        Entrega: item.Entrega == undefined ? '' : item.Entrega,
                                        NumeroConfirmacionMinisterio: CodigoProducto,
                                        TotalFleteCliente: item.Flete_Factura == undefined ? '' : parseInt(item.Flete_Factura)
                                    });
                                } catch (e) {
                                    contErrores++;
                                }
                            });
                            showModal("modalActualizarRemesas")
                            console.log("datos: " + JSON.stringify($scope.DataRemesasActualizar));
                        } catch (e) {
                            ShowError("Error al validar el archivo: " + e);
                        }
                        if (contErrores > 0) {
                            closeModal("modalActualizarRemesas")
                            ShowError("La estructura del encabezado no corresponde a los datos requeridos");
                        }
                        if ($scope.totalRegistrosCargue > 20) {
                            for (var i = 0; i < 20; i++) {
                                $scope.DataActual.push($scope.DataArchivo[i])
                            }

                        } else {
                            $scope.DataActual = $scope.DataArchivo
                        }
                        for (var i = 0; i < $scope.DataArchivo.length; i++) {

                            $scope.DataArchivo[i].Pos = i;


                        }

                        if ($scope.DataArchivo.length > 0) {
                            //blockUI.start();
                            //$scope.ValidarRemtientes(0)
                            blockUI.stop();
                        } else {
                            ShowError('El archivo seleccionado no contiene datos para validar, por favor intente con otro archivo')
                            blockUI.stop()
                            $timeout(blockUI.stop(), 1000)

                        }
                    } catch (e) {
                        ShowError('Error en la lectura del archivo por favor intente nuevamente: ' + e)

                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    }



                }, 1000)
            }

        }

        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            //if ($scope.DataArchivo.Valores != undefined) {
            //$('#TablaCarge').show();
            //$('#btnValida').show();

            // $scope.DataArchivo = { Valores: $scope.DataArchivo.Valores }
            AsignarValoresTabla()


            //}
            //else {
            //    ShowError('El arhivo cargado no corresponde al formato de carge masivo de las programaciones')
            //}
            blockUI.stop()

        }

        $scope.handleFile = function (e) {

            var files = e.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = e.target.result;
                $scope.arr = fixdata(data);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(btoa($scope.arr), { type: 'base64' });
                        process_wb($scope.wb);

                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);

            });

        }

        $scope.NuevoProceso = function (e) {
            var fileVal = document.getElementById("fileSecundario");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';


        }

        $scope.ActualizarRemesas = function () {
            $scope.DataArchivo.forEach(item => {

            });
            RemesasFactory.ActualizarRemesasExcel({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Remesas: $scope.DataRemesasActualizar, TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        ShowSuccess('Se actualizaron ' + response.data.Datos + ' remesas');
                        closeModal("modalActualizarRemesas")
                        $scope.Buscar()

                    } else {
                        ShowError('No se actualizaron las remesas, consulte al administrador del sistema: ' + response.statusText + ' ' + response.data.MensajeOperacion);
                    }
                }), function (response) {
                    ShowError(response.statusText);
                };
        }

        //--Gestion Documentos Remesa
        $scope.MostrarGestionDocumentosRemesa = function (item) {
            $scope.ListaGestionDocuRemesa = [];
            $scope.ListaGestionDocuRemesa = angular.copy(item.GestionDocumentosRemesa);
            for (var i = 0; i < $scope.ListaGestionDocuRemesa.length; i++) {
                var gedr = $scope.ListaGestionDocuRemesa[i];
                gedr.Recibido = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Recibido.Codigo);
                gedr.Entregado = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Entregado.Codigo);
                gedr.Legalizado = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Legalizado.Codigo);
            }
            TmpNumeroRemesa = item.Numero;
            showModal('modalGestionDocumentosRemesa');
        };
        //--Gestion Documentos Remesa
        //--Control Entregas
        $scope.ConsultarControlEntregas = function (item) {
            $scope.ListadoDocumentos = [];

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero
            };
            RemesaGuiasFactory.ObtenerControlEntregas(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Remesa.NombreRecibe != null && response.data.Datos.Remesa.NumeroIdentificacionRecibe != null && response.data.Datos.Remesa.TelefonoRecibe != null) {
                            $scope.FotosEntrega = [];
                            $scope.FotosDevolucion = [];
                            //$scope.FotoControlEntrega = response.data.Datos.Remesa.Fotografia;
                            //$scope.FotoControlEntrega.FotoCargada = 'data:' + response.data.Datos.Remesa.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Remesa.Fotografia.FotoBits;
                            $scope.FotosControlEntrega = response.data.Datos.Remesa.Fotografias;
                            $scope.FotosControlEntrega.forEach(foto => {
                                foto.FotoCargada = 'data:' + foto.TipoFoto + ';base64,' + foto.FotoBits;
                                if (foto.EntregaDevolucion == 1) {
                                    $scope.FotosDevolucion.push(foto);
                                } else if (foto.EntregaDevolucion == 0) {
                                    $scope.FotosEntrega.push(foto);
                                }
                            });
                            $scope.ControlEntregaRemesa = response.data.Datos.Remesa;

                            $scope.ControlEntregaRemesa.UsuarioEntrega = response.data.Datos.UsuarioEntrega;
                            $scope.ControlEntregaRemesa.OficinaEntrega = response.data.Datos.OficinaEntrega.Nombre;
                            $scope.ControlEntregaRemesa.MostrarEntregaOFicina = response.data.Datos.OficinaEntrega.Codigo > 0 ? true : false;
                            $scope.ControlEntregaRemesa.Latitud = response.data.Datos.Latitud;
                            $scope.ControlEntregaRemesa.Longitud = response.data.Datos.Longitud;

                            $scope.ControlEntregaRemesa.VehiculoNombre = `${$scope.ControlEntregaRemesa.Vehiculo.Placa} (${$scope.ControlEntregaRemesa.Vehiculo.CodigoAlterno})`
                            $scope.ControlEntregaRemesa.PlanillaEntrega = response.data.Datos.PlanillaEntrega

                            if (response.data.Datos.DocumentosCumplidoRemsa != undefined) {
                                $scope.ListadoDocumentos = response.data.Datos.DocumentosCumplidoRemsa;
                                $scope.ListadoDocumentos.forEach(function (item) {
                                    item.Numero = $scope.ControlEntregaRemesa.Numero;
                                    item.temp = false;
                                    item.ValorDocumento = 1;
                                    item.AplicaEliminar = 1;
                                    item.AplicaArchivo = 1;
                                });
                            }
                            $scope.ControlEntregaRemesa.FirmaImg = "data:image/png;base64," + $scope.ControlEntregaRemesa.Firma;
                            showModal('modalControlEntregas');
                        }
                        else {
                            ShowError('El documento no cuenta con un control de entregas');
                        }
                    }
                    else {
                        ShowError('El documento no cuenta con un control de entregas');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        $scope.VerFotosEntrega = function () {
            $scope.CategoriaFotos = "ENTREGA";
            $scope.Fotos = $scope.FotosEntrega;
            showModal('modalFotos');
        };
        $scope.VerFotosDevolucion = function () {
            $scope.CategoriaFotos = "DEVOLUCIÓN";
            $scope.Fotos = $scope.FotosDevolucion;
            showModal('modalFotos');
        };
        //--Control Entregas
        //-----------------------Documental------------------------------//
        $scope.AgregarFilaDocumento = function (numero) {
            if ($scope.ListadoDocumentos.length < 5) {
                var itmDoc = {
                    Descripcion: '',
                    Numero: numero,
                    AplicaDescargar: 0,
                    AplicaArchivo: 1,
                    ValorDocumento: 0,
                    TipoArchivoDocumento: $scope.CadenaFormatos
                };
                $scope.ListadoDocumentos.push(itmDoc);
            } else {
                ShowError("Máximo se permite cargar cinco (5) documentos");
            }
        };

        $scope.CargarArchivo = function (element) {
            $scope.IdPosiction = parseInt(element.id, 10);
            var continuar = true;
            if (element.files.length > 0) {
                $scope.PDF = element;
                $scope.TipoDocumento = element;
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento = angular.element(this.ItemFoto)[0];
                } else {
                    angular.element(this.item)[0].Temporal = true
                    $scope.Documento = angular.element(this.item)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos: 0");
                    continuar = false;
                }
                if (continuar == true) {
                    if (element.files[0].size >= 3000000) { //3 MB
                        ShowError("El archivo " + element.files[0].name + " supera los " + "3 MB" + ", intente con otro archivo", false);
                        continuar = false;
                    }
                    var Formatos = $scope.CadenaFormatos.split(',');
                    var cont = 0;
                    for (var i = 0; i < Formatos.length; i++) {
                        Formatos[i] = Formatos[i].replace(' ', '');
                        var Extencion = element.files[0].name.split('.');
                        //if (element.files[0].type == Formatos[i]) {
                        if ('.' + (Extencion[Extencion.length - 1].toUpperCase()) == Formatos[i].toUpperCase()) {
                            cont++;
                        }
                    }
                    if (cont == 0) {
                        ShowError("Archivo invalido, por favor verifique el formato del archivo.  Formatos permitidos: " + $scope.CadenaFormatos);
                        continuar = false;
                    }
                }
                if (continuar == true) {
                    if ($scope.Documento.Archivo != undefined && $scope.Documento.Archivo != '' && $scope.Documento.Archivo != null) {
                        $scope.Documento.ArchivoTemporal = element.files[0];
                        showModal('modalConfirmacionRemplazarDocumentoRemesa');
                    } else {
                        var reader = new FileReader();
                        $scope.Documento.ArchivoCargado = element.files[0];
                        reader.onload = $scope.AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }
            }
        };

        $scope.RemplazarDocumentoRemesa = function () {
            var reader = new FileReader();
            $scope.Documento.ArchivoCargado = $scope.Documento.ArchivoTemporal;
            reader.onload = $scope.AsignarArchivo;
            reader.readAsDataURL($scope.Documento.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumentoRemesa');
        };

        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                //imagenes, tienen un proceso aparte ya que se deben redimencionar para que no ocupen mucho espacio en la BD
                if ($scope.Documento.ArchivoCargado.type.includes("image")) {
                    var img = new Image();
                    img.src = e.target.result;
                    $timeout(function () {
                        $('#Foto' + $scope.IdPosiction).show();
                        $('#FotoCargada' + $scope.IdPosiction).hide();
                        if (img.height > img.width) {
                            RedimencionarImagen('CanvasVertical', img, 699, 499);
                            $scope.Documento.Archivo = document.getElementById('CanvasVertical').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                        }
                        //la imagen es horizontal
                        else {
                            RedimencionarImagen('CanvasHorizontal', img, 499, 699);
                            $scope.Documento.Archivo = document.getElementById('CanvasHorizontal').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                        }
                        $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type;
                        var Extencion = $scope.Documento.ArchivoCargado.name.split('.');
                        var tmpNombre = $scope.Documento.ArchivoCargado.name.split('.').slice(0, -1).join('.');
                        $scope.Documento.NombreDocumento = tmpNombre;
                        $scope.Documento.ExtensionDocumento = Extencion[Extencion.length - 1];
                        $scope.InsertarDocumento();
                    }, 100);
                }
                //Resto de archivos
                else {
                    $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                    $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type;
                    var Extencion = $scope.Documento.ArchivoCargado.name.split('.');
                    var tmpNombre = $scope.Documento.ArchivoCargado.name.split('.').slice(0, -1).join('.');
                    $scope.Documento.NombreDocumento = tmpNombre;
                    $scope.Documento.ExtensionDocumento = Extencion[Extencion.length - 1];
                    $scope.InsertarDocumento();
                }
            });
        };

        $scope.SeleccionarObjeto = function (item) {
            $scope.DocumentoSeleccionado = item;
        };

        $scope.InsertarDocumento = function () {
            $timeout(function () {
                blockUI.start("Subiendo Archivo...");
                // Guardar Archivo temporal
                var Documento = {};
                Documento = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Id: $scope.DocumentoSeleccionado.Id ? $scope.DocumentoSeleccionado.Id : 0,
                    Numero: $scope.Documento.Numero,
                    Descripcion: $scope.Documento.Descripcion,
                    Nombre: $scope.Documento.NombreDocumento,
                    Extension: $scope.Documento.ExtensionDocumento,
                    Tipo: $scope.Documento.Tipo,
                    Archivo: $scope.Documento.Archivo,
                    CumplidoRemesa: true,
                };
                DocumentosFactory.InsertarTemporal(Documento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            // Se guardó correctamente el pdf
                            if (response.data.Datos) {
                                $scope.ListadoDocumentos.forEach(function (item) {
                                    if (item.$$hashKey == $scope.DocumentoSeleccionado.$$hashKey) {
                                        item.Id = response.data.Datos;
                                        item.ValorDocumento = 1;
                                        item.AplicaEliminar = 1;
                                        item.temp = true;
                                    }
                                });
                                ShowSuccess('Se cargó el documento adjunto "' + $scope.Documento.NombreDocumento + '"');
                                $scope.Documento = '';
                            }
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                        blockUI.stop();
                    }, function (response) {
                        ShowError(response.statusText);
                        blockUI.stop();
                    });
            }, 200);
        };

        //elimina archivo temporal
        $scope.ElimiarDocumento = function (EliminarDocumento) {
            var EliminarDocu = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Id: EliminarDocumento.Id,
                Numero: EliminarDocumento.Numero,
                CumplidoRemesa: true,
            };

            DocumentosFactory.EliminarDocumento(EliminarDocu).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                            var item = $scope.ListadoDocumentos[i];
                            if (item.Id == EliminarDocumento.Id) {
                                $scope.ListadoDocumentos.splice(i, 1);
                            }
                        }
                    }
                    else {
                        MensajeError("Error", response.data.MensajeError, false);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                if (item.Temp) {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&Temp=1' + '&ENRE_Numero=' + item.Numero);
                }
                else {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&ENRE_Numero=' + item.Numero + '&Temp=0');
                }
            }
            else {
                ShowError("Se debe seleccionar un archivo");
            }
        };

        //valida para guardar archivo como definitivo
        $scope.GuardarArchivoCumplido = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Guardando Documento...");
                $timeout(function () {
                    blockUI.message("Guardando Documento...");
                    TrasladarDocumentoCumplido(item);
                }, 100);
                //Bloqueo Pantalla
            }
        };

        //elimina archivo definitivo
        $scope.EliminarArchivo = function (item) {
            var Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Id: item.Id,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.EliminarDocumentoCumplidoRemesa(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {

                ShowSuccess('Se eliminó el documento adjunto "' + item.Descripcion + '"');
                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                    var item = $scope.ListadoDocumentos[i];
                    if (item.Id == Documento.Id) {
                        $scope.ListadoDocumentos.splice(i, 1);
                    }
                }
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
        }

        //guarda archivo como definitivo
        function TrasladarDocumentoCumplido(item) {
            var Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Id: item.Id,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.InsertarDocumentosCumplidoRemesa(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {
                if (ResponseDocu.Datos > 0) {
                    $scope.ListadoDocumentos.forEach(function (item) {
                        if (item.$$hashKey == $scope.DocumentoSeleccionado.$$hashKey) {
                            item.Id = ResponseDocu.Datos;
                            item.ValorDocumento = 1;
                            item.AplicaEliminar = 1;
                            item.temp = false;
                        }
                    });
                    ShowSuccess('Se guardó el documento adjunto "' + item.Descripcion + '"');
                }
                else {
                    ShowError(ResponseDocu.MensajeOperacion);
                }
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
        }

        $scope.EliminarDocumentosCumplidoTemporal = function (numero) {
            var EliminarDocu = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: numero,
                CumplidoRemesa: true,
            };

            DocumentosFactory.EliminarDocumento(EliminarDocu).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDocumentos = [];
                    }
                    else {
                        MensajeError("Error", response.data.MensajeError, false);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        //-----------------------Documental------------------------------//

        //Fin lectura de Archivo

        //--------------------------------------FinExportarExcel-------------------------------------------------//

        //--Mascaras
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item);
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope);
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item);
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item);
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope);
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item);
        };
        $scope.MaskDecimalesGrid = function (item) {
            return MascaraDecimales(item);
        };
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope);
        };
        //$scope.InvocarWS = function () {
        //    console.log('InvocarWS');

        //    var url = "https://143.0.93.187:9092/WebServicePlexa.asmx";
        //    var respuesta;

        //    respuesta = $soap.post(base_url, "ListaPrecio");
        //    //respuesta = $soap.post(base_url, "ListaPrecio", { ID: '900396759' });

        //    //$soap.post(url, action, params).then(function (response) {

        //    //});

        //    return respuesta;
        //};
    }]);
