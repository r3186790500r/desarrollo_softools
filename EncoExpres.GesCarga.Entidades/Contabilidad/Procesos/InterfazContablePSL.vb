﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Namespace Contabilidad.Procesos
    Public NotInheritable Class InterfazContablePSL
        Inherits BaseDocumento

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CATATipoDocumento = New ValorCatalogos With {.Codigo = Read(lector, "TIDO_Codigo_Origen"), .Nombre = Read(lector, "TIDO_Nombre_Origen")}
            NumeroOrigen = Read(lector, "Numero_Origen")
            NumeroDocumentoOrigen = Read(lector, "Numero_Documento_Origen")
            Fecha = Read(lector, "Fecha_Crea")
            TipoDocumentoPSL = Read(lector, "Tipo_Documento_PSL")
            MensajePSL = Read(lector, "Mensaje_PSL")
            FechaMensajePSL = Read(lector, "Fecha_Mensaje_PSL")
            NumeroProcesoPLS = Read(lector, "Numero_Proceso_PSL")
            ProcesoPSL = Read(lector, "Proceso_PSL")
            EstadoProcesoPSL = Read(lector, "Estado_Proceso_PSL")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub

        <JsonProperty>
        Public Property CATATipoDocumento As ValorCatalogos
        <JsonProperty>
        Public Property NumeroOrigen As Integer
        <JsonProperty>
        Public Property NumeroDocumentoOrigen As Integer
        <JsonProperty>
        Public Property TipoDocumentoPSL As String
        <JsonProperty>
        Public Property MensajePSL As String
        <JsonProperty>
        Public Property FechaMensajePSL As DateTime
        <JsonProperty>
        Public Property NumeroProcesoPLS As Integer
        <JsonProperty>
        Public Property ProcesoPSL As Integer
        <JsonProperty>
        Public Property EstadoProcesoPSL As String

    End Class
End Namespace

