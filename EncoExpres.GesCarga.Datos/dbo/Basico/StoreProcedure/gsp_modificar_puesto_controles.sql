﻿PRINT 'gsp_modificar_puesto_controles'
GO
DROP PROCEDURE gsp_modificar_puesto_controles
GO
CREATE PROCEDURE gsp_modificar_puesto_controles
(
@par_EMPR_Codigo	smallint,
@par_Codigo	numeric  =NULL,
@par_Nombre	varchar(100) =NULL,
@par_Codigo_Alterno	varchar(20)  =NULL,
@par_TERC_Codigo_Proveedor	numeric  =NULL,
@par_Contacto	varchar(100) =NULL,
@par_Telefono	varchar(30) =NULL,
@par_Celular	varchar(30) =NULL,
@par_Ubicacion	varchar(300) =NULL,
@par_Estado	smallint,
@par_USUA_Codigo_Modifica	smallint
)
AS BEGIN
declare @codigo as numeric =(select ISNULL(max(Codigo),1) from Puesto_Controles WHERE EMPR_Codigo = @par_EMPR_Codigo)
UPDATE Puesto_Controles
           SET Nombre =ISNULL(@par_Nombre,'')
           ,Codigo_Alterno =@par_Codigo_Alterno
           ,TERC_Codigo_Proveedor =ISNULL(@par_TERC_Codigo_Proveedor,0)
           ,Contacto =ISNULL(@par_Contacto,'')
           ,Telefono =ISNULL(@par_Telefono,'')
           ,Celular =ISNULL(@par_Celular,'')
           ,Ubicacion =ISNULL(@par_Ubicacion,'')
           ,Fecha_Modifica = GETDATE()
           ,USUA_Codigo_Modifica =@par_USUA_Codigo_Modifica
		   ,Estado = @par_Estado
		   WHERE EMPR_Codigo = @par_EMPR_Codigo
		   AND Codigo = @par_Codigo

SELECT @par_Codigo AS Codigo
END
GO
