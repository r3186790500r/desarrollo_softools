﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class PersistenciaValidacionPlanillaPaqueteria
        Implements IPersistenciaBase(Of EncabezadoValidacionPlanillaPaqueteria)

        Public Function Consultar(filtro As EncabezadoValidacionPlanillaPaqueteria) As IEnumerable(Of EncabezadoValidacionPlanillaPaqueteria) Implements IPersistenciaBase(Of EncabezadoValidacionPlanillaPaqueteria).Consultar
            Return New RepositorioValidacionPlanillaPaqueteria().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoValidacionPlanillaPaqueteria) As Long Implements IPersistenciaBase(Of EncabezadoValidacionPlanillaPaqueteria).Insertar
            Return New RepositorioValidacionPlanillaPaqueteria().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoValidacionPlanillaPaqueteria) As Long Implements IPersistenciaBase(Of EncabezadoValidacionPlanillaPaqueteria).Modificar
            Return New RepositorioValidacionPlanillaPaqueteria().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoValidacionPlanillaPaqueteria) As EncabezadoValidacionPlanillaPaqueteria Implements IPersistenciaBase(Of EncabezadoValidacionPlanillaPaqueteria).Obtener
            Return New RepositorioValidacionPlanillaPaqueteria().Obtener(filtro)
        End Function
        Public Function Anular(filtro As EncabezadoValidacionPlanillaPaqueteria) As EncabezadoValidacionPlanillaPaqueteria
            Return New RepositorioValidacionPlanillaPaqueteria().Anular(filtro)
        End Function
        Public Function EliminarGuia(entidad As EncabezadoValidacionPlanillaPaqueteria) As Boolean
            Return New RepositorioValidacionPlanillaPaqueteria().EliminarGuia(entidad)
        End Function
        Public Function EliminarRecoleccion(entidad As EncabezadoValidacionPlanillaPaqueteria) As Boolean
            Return New RepositorioValidacionPlanillaPaqueteria().EliminarRecoleccion(entidad)
        End Function
    End Class
End Namespace

