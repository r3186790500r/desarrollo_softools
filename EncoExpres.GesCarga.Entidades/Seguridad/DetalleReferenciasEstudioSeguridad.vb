﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Seguridad

    ''' <summary>
    ''' Clase <see cref="DetalleReferenciasEstudioSeguridad"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleReferenciasEstudioSeguridad
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleReferenciasEstudioSeguridad"/>
        ''' </summary>
        Sub New()
        End Sub
        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleReferenciasEstudioSeguridad"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroEstudio = New EstudioSeguridad With {.Numero = Read(lector, "ENES_Numero")}
            Referencia = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TRES_Codigo"), .Nombre = Read(lector, "Nombre_Tipo_Referencia")}
            ObservacionReferencia = Read(lector, "Observaciones")

        End Sub

        <JsonProperty>
        Public Property NumeroEstudio As EstudioSeguridad
        <JsonProperty>
        Public Property ObservacionReferencia As String
        <JsonProperty>
        Public Property Referencia As ValorCatalogos

    End Class
End Namespace
