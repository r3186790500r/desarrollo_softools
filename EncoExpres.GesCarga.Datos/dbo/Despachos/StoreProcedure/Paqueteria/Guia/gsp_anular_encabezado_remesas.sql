﻿Print 'gsp_anular_encabezado_remesas'
GO
DROP PROCEDURE gsp_anular_encabezado_remesas
GO
CREATE PROCEDURE gsp_anular_encabezado_remesas (
@par_EMPR_Codigo SMALLINT,
@par_Numero NUMERIC,
@par_USUA_Codigo SMALLINT,
@par_OFIC_Codigo SMALLINT,
@par_Causa_Anula VARCHAR(150)

)
AS 
BEGIN
	UPDATE Encabezado_Remesas SET Anulado = 1,
	Fecha_Anula = GETDATE(), USUA_Codigo_Anula = @par_USUA_Codigo,
	Causa_Anula = @par_Causa_Anula, OFIC_Codigo_Anula = @par_OFIC_Codigo

	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND Numero = @par_Numero

	SELECT Numero FROM Encabezado_Remesas
	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND Numero = @par_Numero
	AND Anulado = 1
END
GO