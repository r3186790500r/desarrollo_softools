﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaCatalogos
        Implements IPersistenciaBase(Of Catalogos)

        Public Function Consultar(filtro As Catalogos) As IEnumerable(Of Catalogos) Implements IPersistenciaBase(Of Catalogos).Consultar
            Return New RepositorioCatalogos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Catalogos) As Long Implements IPersistenciaBase(Of Catalogos).Insertar
            Return New RepositorioCatalogos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Catalogos) As Long Implements IPersistenciaBase(Of Catalogos).Modificar
            Return New RepositorioCatalogos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Catalogos) As Catalogos Implements IPersistenciaBase(Of Catalogos).Obtener
            Return New RepositorioCatalogos().Obtener(filtro)
        End Function

    End Class

End Namespace

