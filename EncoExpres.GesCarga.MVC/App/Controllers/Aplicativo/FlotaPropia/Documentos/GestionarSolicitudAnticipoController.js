﻿EncoExpresApp.controller('GestionarSolicitudAnticipoCtrl', ['$scope', '$linq', '$timeout', 'blockUI', 'blockUIConfig', '$routeParams', 'VehiculosFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'SemirremolquesFactory', 'PlanillaDespachosFactory', 'RutasFactory', 'SitiosCargueDescargueFactory', 'PeajesFactory', 'SolicitudesAnticipoFactory',
    function ($scope, $linq, $timeout, blockUI, blockUIConfig, $routeParams, VehiculosFactory, ValorCatalogosFactory, TercerosFactory, SemirremolquesFactory, PlanillaDespachosFactory, RutasFactory, SitiosCargueDescargueFactory, PeajesFactory, SolicitudesAnticipoFactory) {
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo==' + OPCION_MENU_SOLICITUDES_ANTICIPO)
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta página');
            document.location.href = '#'
            console.log(e)
        }

        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.MapaSitio = [{ Nombre: 'Flota Propia' }, { Nombre: 'Documentos' }, { Nombre: 'Solicitudes Anticipo' }, { Nombre: 'Gestionar' }];

        $scope.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
        $scope.Numero = '';
        $scope.NumeroDocumento = '';
        $scope.Fecha = new Date();
        $scope.ListadoTipoSolicitud = [];
        $scope.ListadoSolicitudes = [];
        $scope.item = '';
        $scope.Observaciones = '';
        $scope.Estado = '';
        $scope.DeshabilitarActualizar = false;
        $scope.MensajesError = [];
        $scope.ListadoSolicitudesPendientes = [];

        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: 'DEFINITIVO' },
            { Codigo: 0, Nombre: 'BORRADOR' },
        ];

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==0');

        $scope.VolverMaster = function () {
            if ($scope.NumeroDocumento != '') {
                document.location.href = '#!ConsultarSolicitudesAnticipo/' + $scope.NumeroDocumento;
            } else {
                document.location.href = '#!ConsultarSolicitudesAnticipo'
            }
        }

        //Autocompletes:

        $scope.AutocompleteVehiculos = function (value) {
            var ListadoVehiculosPropios = [];
            var ListadoVehiculos = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.CodigoEmpresa, ValorAutocomplete: value, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
            ListadoVehiculos.forEach(item => {
                if (item.TipoDueno.Codigo == 2102/*Propio*/) {
                    ListadoVehiculosPropios.push(item);
                }
            });
            return ListadoVehiculosPropios
        }

        $scope.AutocompleteSemirremolques = function (value) {
            return SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.CodigoEmpresa, ValorAutocomplete: value, Sync: true }).Datos;
        }

        $scope.AutocompleteConductores = function (value) {
            return TercerosFactory.Consultar({ CodigoEmpresa: $scope.CodigoEmpresa, ValorAutocomplete: value, CadenaPerfiles: PERFIL_CONDUCTOR, Sync: true }).Datos;
        }

        $scope.AutocompleteRutas = function (value) {
            return RutasFactory.Consultar({ CodigoEmpresa: $scope.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
        }

        $scope.AutocompleteSitiosCargueDescargue = function (item) {
            if (item.Ruta != undefined && item.Ruta.Codigo != 0) {
                var Sitios = SitiosCargueDescargueFactory.ConsultarSitiosRutas({ CodigoEmpresa: $scope.CodigoEmpresa, Codigo: item.Ruta.Codigo, Sync: true }).Datos;
                var SitiosFiltroVehiculo = [];

                //Si el campo vehiculo está vacío, se llena el autocomplete con los sitios con tipovehiculo = NO APLICA(2200), de lo contrario se llena con los sitios que coincidan con el tipoVehículo:
                if (item.Vehiculo != undefined && item.Vehiculo != null && item.Vehiculo != {} && item.Vehiculo.Codigo != 0 && item.Vehiculo != '' && item.Vehiculo.Placa != '') {
                    Sitios.forEach(itemSitio => {
                        if (item.Vehiculo.TipoVehiculo.Codigo == itemSitio.TipoVehiculo.Codigo) {
                            //itemSitio.Sitio.Nombre = itemSitio.Sitio.Nombre + '(' + itemSitio.TipoVehiculo.Nombre + ')';
                            SitiosFiltroVehiculo.push(itemSitio);
                        }
                    });
                }
                return SitiosFiltroVehiculo;
            }
        }

        //Fin Autocompletes

        function Obtener() {
            SolicitudesAnticipoFactory.Obtener({ CodigoEmpresa: $scope.CodigoEmpresa, Numero: $scope.Numero, UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        $scope.NumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.Numero = response.data.Datos.Numero;
                        $scope.Fecha = new Date(response.data.Datos.Fecha);
                        $scope.Observaciones = response.data.Datos.Observaciones,
                            $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + response.data.Datos.Estado);
                        if ($scope.Estado.Codigo == ESTADO_DEFINITIVO) {
                            $scope.DeshabilitarActualizar = true;
                        }

                        response.data.Datos.Solicitudes.forEach(item => {
                            $scope.ListadoSolicitudes.push({
                                Vehiculo: $scope.CargarVehiculos(item.Vehiculo.Codigo),
                                Semirremolque: item.Semirremolque.Codigo == 0 ? '' : $scope.CargarSemirremolqueCodigo(item.Semirremolque.Codigo),
                                TipoSolicitud: $linq.Enumerable().From($scope.ListadoTipoSolicitud).First('$.Codigo==' + item.TipoSolicitud.Codigo),
                                Planilla: item.Planilla,
                                Conductor: $scope.CargarTercero(item.Conductor.Codigo),
                                IdentificacionConductor: $scope.CargarTercero(item.Conductor.Codigo).NumeroIdentificacion,
                                Ruta: RutasFactory.Obtener({ CodigoEmpresa: $scope.CodigoEmpresa, Codigo: item.Ruta.Codigo, Sync: true }).Datos,
                                SitioCargue: item.SitioCargue.Codigo == 0 ? '' : $linq.Enumerable().From(SitiosCargueDescargueFactory.ConsultarSitiosRutas({ CodigoEmpresa: $scope.CodigoEmpresa, Codigo: item.Ruta.Codigo, Sync: true }).Datos).First('$.Sitio.Codigo==' + item.SitioCargue.Codigo),
                                SitioDescargue: item.SitioDescargue.Codigo == 0 ? '' : $linq.Enumerable().From(SitiosCargueDescargueFactory.ConsultarSitiosRutas({ CodigoEmpresa: $scope.CodigoEmpresa, Codigo: item.Ruta.Codigo, Sync: true }).Datos).First('$.Sitio.Codigo==' + item.SitioDescargue.Codigo),
                                ValorSugerido: MascaraValores(item.ValorSugerido),
                                ValorRegistrado: MascaraValores(item.ValorRegistrado),
                                Observaciones: item.Observaciones

                            });

                        });
                        if ($scope.Estado.Codigo == ESTADO_ACTIVO) {
                            $scope.DeshabilitarActualizar = true;
                        }

                    } else {
                        ShowError(response.statusText);
                    }

                    blockUI.stop();
                }, function (response) {
                    ShowError(response.statusText);
                });
        }


        $scope.ValidarPlanilla = function (item) {
            if (item.Planilla != 0 && item.Planilla != '0' && item.Planilla != '' && item.Planilla != undefined) {
                var ResponsePlanilla = PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.CodigoEmpresa, Numero: item.Planilla, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true });
                if (ResponsePlanilla.Datos.length <= 0) {
                    ShowError('Esta planilla no es válida');
                    item.Planilla = '';
                } else if (ResponsePlanilla.Datos[0].Cumplido.Numero > 0) {
                    ShowError('Esta planilla ya se encuentra cumplida');
                    item.Planilla = '';
                } else {
                    var Plan = PlanillaDespachosFactory.Obtener({ CodigoEmpresa: $scope.CodigoEmpresa, Numero: ResponsePlanilla.Datos[0].Numero, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Sync: true }).Datos;
                    var Vehiculo = $scope.CargarVehiculos(Plan.Vehiculo.Codigo);
                    if (Vehiculo.TipoDueno.Codigo == CODIGO_CATALOGO_TIPO_DUENO_PROPIO) {
                        item.Conductor = $scope.CargarTercero(Plan.Conductor.Codigo);
                        item.Ruta = RutasFactory.Obtener({ CodigoEmpresa: $scope.CodigoEmpresa, Codigo: Plan.Ruta.Codigo, Sync: true }).Datos;
                        item.Vehiculo = Vehiculo;
                        item.Semirremolque = $scope.CargarSemirremolqueCodigo(Plan.Semirremolque.Codigo);
                    
                        $scope.VerificarSitiosRuta(item);
                        $scope.CalcularAnticipo(item);
                    } else {
                        ShowError('Esta planilla tiene asignado un vehículo que NO es propio');
                        item.Planilla = '';
                    }
                }
            }
        }

        $scope.ListadoTipoSolicitud = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SOLICITUD_ANTICIPO }, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
        $scope.TipoSolicitud = $scope.ListadoTipoSolicitud[0];

        $scope.AsignarSemirremolqueyConductor = function (item, index) {
            //primero se verifica que la llave vehículo/tipo solicitud, no esté duplicada:
            var duplicados = 0;
            for (var i = 0; i < $scope.ListadoSolicitudes.length; i++) {
                if (i != index) {
                    if ($scope.ListadoSolicitudes[i].Vehiculo.Codigo == item.Vehiculo.Codigo) {
                        if ($scope.ListadoSolicitudes[i].TipoSolicitud.Codigo == item.TipoSolicitud.Codigo) {
                            duplicados++;
                        }
                    }
                }
            }

            if (duplicados == 0) {

                if (item.Vehiculo.Semirremolque != null && item.Vehiculo.Semirremolque.Codigo != 0) {
                    item.Semirremolque = $scope.CargarSemirremolqueCodigo(item.Vehiculo.Semirremolque.Codigo);
                } else {
                    item.Semirremolque = ''
                }

                if (item.Vehiculo.Conductor != null) {
                    item.Conductor = $scope.CargarTercero(item.Vehiculo.Conductor.Codigo);
                    $scope.AsignarIdentificacionConductor(item);
                }

                //Verificar si los campos de Sitios Cargue y descargue están vacíos, si no verificar que el tipo vehículo coincida, para no borrar los campos:
                if (item.SitioCargue != undefined && item.SitioCargue != null && item.SitioCargue != {} && item.SitioCargue != '') {
                    if (item.SitioCargue.Sitio.Codigo != 0 && item.SitioCargue.Sitio.Nombre != '') {
                        if (item.Vehiculo != '') {
                            if (item.SitioCargue.TipoVehiculo.Codigo != item.Vehiculo.TipoVehiculo.Codigo) {
                                if (item.SitioCargue.TipoVehiculo.Codigo != CODIGO_TIPO_VEHICULO_NO_APLICA) {
                                    item.SitioCargue = {
                                        Sitio: { Nombre: '' }
                                    }
                                }
                            }
                        }
                    }
                }
                if (item.SitioDescargue != undefined && item.SitioDescargue != null && item.SitioDescargue != {} && item.SitioDescargue.Sitio.Codigo != 0 && item.SitioDescargue != '' && item.SitioDescargue.Sitio.Nombre != '') {
                    if (item.SitioDescargue.Sitio.Codigo != 0 && item.SitioDescargue.Sitio.Nombre != '') {
                        if (item.Vehiculo != '') {
                            if (item.SitioDescargue.TipoVehiculo.Codigo != item.Vehiculo.TipoVehiculo.Codigo) {
                                if (item.SitioDescargue.TipoVehiculo.Codigo != CODIGO_TIPO_VEHICULO_NO_APLICA) {
                                    item.SitioDescargue = {
                                        Sitio: { Nombre: '' }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                ShowError('Este Vehículo y este tipo de solicitud ya se encuentran en la lista');
                item.Vehiculo = '';
            }

        }

        $scope.VerificarSitiosRuta = function (item) {

            //Verificar si los campos de Sitios Cargue y descargue están vacíos, si no verificar que el código de la ruta coincida, para no borrar los campos:
            if (item.SitioCargue != undefined && item.SitioCargue != null && item.SitioCargue != {} && item.SitioCargue != '') {
                if (item.SitioCargue.Sitio.Codigo != 0 && item.SitioCargue.Sitio.Nombre != '') {
                    if (item.SitioCargue.Ruta.Codigo != item.Ruta.Codigo) {
                        item.SitioCargue = {
                            Sitio: { Nombre: '' }
                        }
                    }
                }
            }
            if (item.SitioDescargue != undefined && item.SitioDescargue != null && item.SitioDescargue != {} && item.SitioDescargue != '') {
                if (item.SitioDescargue.Sitio.Codigo != 0 && item.SitioDescargue.Sitio.Nombre != '') {
                    if (item.SitioDescargue.Ruta.Codigo != item.Ruta.Codigo) {
                        item.SitioDescargue = {
                            Sitio: { Nombre: '' }
                        }
                    }
                }
            }
        }

        $scope.VerificarSitio = function (Object) {
            if (Object == '' || Object == undefined || Object == null) {
                return Object = ''
            } else if (Object.Sitio != undefined) {
                if ((Object.Sitio.Codigo == '' || Object.Sitio.Codigo == undefined || Object.Sitio.Codigo == null || Object.Sitio.Codigo == 0 || Object.Sitio.Codigo == '0') && (Object.Sitio.Numero == '' || Object.Sitio.Numero == undefined || Object.Sitio.Numero == null || Object.Sitio.Numero == 0 || Object.Sitio.Numero == '0')) {
                    return Object = '';
                } else {
                    return Object = Object;
                }
            } else {
                return Object = '';
            }
        }

        $scope.CalcularAnticipo = function (item) {
            if (item.Ruta != undefined && item.Ruta != null && item.Ruta != {} && item.Ruta.Codigo != 0 && item.Ruta != '' && item.Ruta.Nombre != '') {
                var sumValores = 0
                //Obtener la sumatoria total de los gastos parametrizados en la ruta:
                var ListaGastosRuta = RutasFactory.Obtener({ CodigoEmpresa: $scope.CodigoEmpresa, Codigo: item.Ruta.Codigo, ConsultaLegalizacionGastosRuta: true, Sync: true }).Datos.LegalizacionGastosRuta;

                ListaGastosRuta.forEach(itemGasto => {
                    if (itemGasto.TipoVehiculo.Codigo == item.Vehiculo.TipoVehiculo.Codigo) {
                        if (itemGasto.AplicaAnticipo == ESTADO_ACTIVO) {
                            sumValores += itemGasto.Valor;
                        }
                    }
                });

                //Adicionar el valor de los sitios cargue y descargue:
                if (item.SitioCargue != undefined && item.SitioCargue != null && item.SitioCargue != {} != 0 && item.SitioCargue != '') {
                    if (item.SitioCargue.Sitio.Codigo != 0 && item.SitioCargue.Sitio.Nombre != '') {
                        if (item.SitioCargue.AplicaAnticipo == ESTADO_ACTIVO) {
                            sumValores += item.SitioCargue.ValorCargue;
                        }
                    }
                }
                if (item.SitioDescargue != undefined && item.SitioDescargue != null && item.SitioDescargue != {} != 0 && item.SitioDescargue != '') {
                    if (item.SitioDescargue.Sitio.Codigo != 0 && item.SitioDescargue.Sitio.Nombre != '') {
                        if (item.SitioDescargue.AplicaAnticipo == ESTADO_ACTIVO) {
                            sumValores += item.SitioDescargue.ValorDescargue;
                        }
                    }
                }


                //Obtener el valor total de los peajes:
                //Obtener Tipo de Vehículo y Número total de Ejes del Semirremolque de la solicitud:
                if (item.Vehiculo != undefined && item.Vehiculo != null && item.Vehiculo != {} && item.Vehiculo != '') {
                    if (item.Vehiculo.Codigo != 0 && item.Vehiculo.Placa != '') {
                        var responseVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.CodigoEmpresa, Codigo: item.Vehiculo.Codigo, Sync: true });
                        var responseSemirremolque = {};
                        var TotalEjes = 0;
                        if (responseVehiculo.ProcesoExitoso == true) {

                            if (item.Semirremolque != undefined && item.Semirremolque != null && item.Semirremolque != {} && item.Semirremolque != '') {
                                if (item.Semirremolque.Codigo != 0 && item.Semirremolque.Placa != '') {

                                    responseSemirremolque = SemirremolquesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Semirremolque.Codigo, Sync: true });
                                    if (responseSemirremolque.ProcesoExitoso == true) {

                                        // ejes del Semirremolque:
                                        TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                                    }

                                } else {
                                    TotalEjes = 0;
                                }
                            }


                            $scope.TotalPeajes = 0;


                            //Consultar los peajes de la ruta de la solicitud:
                            var ResponseRutas = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Ruta.Codigo, ConsultarPeajes: true, Sync: true });

                            if (ResponseRutas.ProcesoExitoso == true) {

                                var ListaTarifasPeajesRuta = [];
                                var PeajesRutasSize = ResponseRutas.Datos.PeajeRutas.length;

                                $scope.ListadoPeajes = [];

                                //Por cada peaje, consultar las tarifas parametrizadas:
                                for (let i = 0; i < ResponseRutas.Datos.PeajeRutas.length; i++) {
                                    if (ResponseRutas.Datos.PeajeRutas[i].Peaje.Estado == ESTADO_ACTIVO) {
                                        let NombrePeaje = ResponseRutas.Datos.PeajeRutas[i].Peaje.Nombre;

                                        var ResponsePeajes = PeajesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: ResponseRutas.Datos.PeajeRutas[i].Peaje.Codigo, ConsultarTarifasPeajes: true, Sync: true })


                                        //Comparar si el tipo de vehículo de la solicitud, coincide con las tarifas parametrizadas en el peaje:
                                        for (let j = 0; j < ResponsePeajes.Datos.TarifasPeajes.length; j++) {
                                            if (ResponsePeajes.Datos.TarifasPeajes[j].TipoVehiculo.Codigo == responseVehiculo.Datos.TipoVehiculo.Codigo) {

                                                // si coincide, verificar primero que la solicitud no tenga semirremolque:
                                                if (TotalEjes > 0) {
                                                    //si lo tiene agregar el valor según el número de ejes del semirremolque:                                                                

                                                    if (TotalEjes == 1) {
                                                        sumValores += ResponsePeajes.Datos.TarifasPeajes[j].ValorRemolque1Eje;
                                                    } else if (TotalEjes == 2) {
                                                        sumValores += ResponsePeajes.Datos.TarifasPeajes[j].ValorRemolque2Ejes;
                                                    } else if (TotalEjes == 3) {
                                                        sumValores += ResponsePeajes.Datos.TarifasPeajes[j].ValorRemolque3Ejes;
                                                    } else if (TotalEjes >= 4) {
                                                        sumValores += ResponsePeajes.Datos.TarifasPeajes[j].ValorRemolque4Ejes;
                                                    }


                                                } else {
                                                    //si no lo tiene, agregar el valor parametrizado por defecto:

                                                    sumValores += ResponsePeajes.Datos.TarifasPeajes[j].Valor;

                                                }
                                            }

                                        }
                                    }
                                }


                            }
                        }



                    }
                }
                //Fin Conceptos de Peajes//


                item.ValorSugerido = MascaraValores(sumValores);
                item.ValorRegistrado = MascaraValores(sumValores);

            } else {
                item.ValorSugerido = '';
                item.ValorRegistrado = '';
            }

        }

        $scope.AsignarIdentificacionConductor = function (item) {
            if (item.Conductor != undefined && item.Conductor.NumeroIdentificacion != null) {
                item.IdentificacionConductor = item.Conductor.NumeroIdentificacion;
            }
        }

        $scope.AgregarSolicitud = function () {
            $scope.ListadoSolicitudes.push({
                Vehiculo: { Placa: '' },
                Semirremolque: { Placa: '' },
                TipoSolicitud: $scope.ListadoTipoSolicitud[0],
                Planilla: '',
                Conductor: { NombreCompleto: '' },
                IdentificacionConductor: '',
                Ruta: { Nombre: '' },
                SitioCargue: {
                    Sitio: { Nombre: '' }
                },
                SitioDescargue: {
                    Sitio: { Nombre: '' }
                },
                ValorSugerido: '',
                ValorRegistrado: '',
                Observaciones: ''
            });
        }

        $scope.ConfirmacionEliminarSolicitud = function (index) {
            $scope.item = index;
            showModal('modalConfirmacionEliminarSolicitud');
        }

        $scope.EliminarSolicitud = function () {
            $scope.ListadoSolicitudes.splice($scope.item, 1);
            closeModal('modalConfirmacionEliminarSolicitud');
        }

        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var contSolicitudespendientes = 0;
            if ($scope.ListadoSolicitudes.length > 0) {
                $scope.ListadoSolicitudes.forEach(item => {

                    if (item.Vehiculo == undefined || item.Vehiculo == null || item.Vehiculo == {} || item.Vehiculo == '') {
                        continuar = false;
                    } else if (item.Vehiculo.Codigo == 0 || item.Vehiculo.Placa == '') {
                        continuar = false;
                    }
                    if (item.Conductor == undefined || item.Conductor == null || item.Conductor == {} || item.Conductor == '') {
                        continuar = false;
                    } else if (item.Conductor.Codigo == 0 || item.Conductor.NombreCompleto == '') {
                        continuar = false;
                    } else {
                        $scope.ConsultarSolicitudAnticipo();
                        var inserto = false;
                        $scope.ListadoSolicitudesPendientes.forEach(solicitud => {
                            if (solicitud.Conductor.Codigo == item.Conductor.Codigo && inserto == false) {
                                if (item.TipoSolicitud.Codigo == 22701/*Anticipo*/) {
                                    inserto = true
                                    $scope.MensajesError.push('El conductor ' + item.Conductor.NombreCompleto + ' ya tiene solicitudes de anticipo pendientes.');
                                    contSolicitudespendientes++;
                                }
                            }
                        });           

                        
                    }
                    if (item.Ruta == undefined || item.Ruta == null || item.Ruta == {} || item.Ruta == '') {
                        continuar = false;
                    } else if (item.Ruta.Codigo == 0 || item.Ruta.Nombre == '') {
                        continuar = false;
                    }
                    //if (item.TipoSolicitud.Codigo != 22702) {
                    //    if (item.SitioCargue == undefined || item.SitioCargue == null || item.SitioCargue == {} || item.SitioCargue == '') {
                    //        continuar = false;
                    //    } else if (item.SitioCargue.Sitio.Codigo == 0 || item.SitioCargue.Sitio.Nombre == '') {
                    //        continuar = false;
                    //    }
                    //    if (item.SitioDescargue == undefined || item.SitioDescargue == null || item.SitioDescargue == {} || item.SitioDescargue == '') {
                    //        continuar = false;
                    //    } else if (item.SitioDescargue.Sitio.Codigo == 0 || item.SitioDescargue.Sitio.Nombre == '') {
                    //        continuar = false;
                    //    }
                    //}

                    if (item.TipoSolicitud.Codigo == 22702) {
                        if (item.Planilla == undefined || item.Planilla == null || item.Planilla == '' || item.Planilla == 0)
                            continuar = false;
                    }

                    if (item.ValorRegistrado == undefined || item.ValorRegistrado == null || item.ValorRegistrado == '' || item.ValorRegistrado == 0 || item.ValorRegistrado == '0') {                        
                            continuar = false;
                    }

                    if (continuar == false) {                      
                        $scope.MensajesError.push('Debe Ingresar todos los datos en la lista de solicitudes');                      

                    }
                    if (contSolicitudespendientes > 0) {
                        continuar = false;
                    }
                });
            } else {
                $scope.MensajesError.push('Debe adicionar al menos un detalle');
                continuar = false
            }
            return continuar;
        }

        $scope.Guardar = function () {

            closeModal('modalConfirmacionGuardar');

            var ListadoSolicitudes = [];
            $scope.ListadoSolicitudes.forEach(item => {
                ListadoSolicitudes.push({
                    Vehiculo: { Codigo: item.Vehiculo.Codigo },
                    Semirremolque: { Codigo: item.Semirremolque == undefined || item.Semirremolque == '' ? 0 : item.Semirremolque.Codigo},
                    TipoSolicitud: { Codigo: item.TipoSolicitud.Codigo },
                    Planilla: item.Planilla == '' ? 0 : item.Planilla,
                    Conductor: { Codigo: item.Conductor.Codigo },
                    Ruta: { Codigo: item.Ruta.Codigo },
                    SitioCargue: { Codigo: item.SitioCargue.Sitio == undefined ? 0: item.SitioCargue.Sitio.Codigo },
                    SitioDescargue: { Codigo: item.SitioCargue.Sitio == undefined ? 0 :item.SitioDescargue.Sitio.Codigo },
                    ValorSugerido: MascaraNumero(item.ValorSugerido),
                    ValorRegistrado: item.ValorRegistrado == '' ? 0 : MascaraNumero(item.ValorRegistrado),
                    Observaciones : item.Observaciones
                });
            });


            var Entidad = {
                CodigoEmpresa: $scope.CodigoEmpresa,
                Numero: $scope.Numero,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_SOLICITUD_ANTICIPO,
                Fecha: $scope.Fecha,
                Observaciones: $scope.Observaciones,
                Estado: $scope.Estado.Codigo,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Solicitudes: ListadoSolicitudes
            }

            SolicitudesAnticipoFactory.Guardar(Entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if ($scope.Numero > 0) {
                            ShowSuccess('Se modificó la solicitud de anticipo No. ' + $scope.NumeroDocumento);
                            document.location.href = '#!ConsultarSolicitudesAnticipo/' + $scope.NumeroDocumento;
                        } else {
                            ShowSuccess('Se guardó la solicitud de anticipo No. ' + response.data.Datos);
                            document.location.href = '#!ConsultarSolicitudesAnticipo/' + response.data.Datos;
                        }
                    } else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            
        }

        $scope.ValidarValorRegistrado = function (item) {
            if (item.TipoSolicitud.Codigo == CODIGO_VALOR_CATALOGO_TIPO_SOLICITUD_ANTICIPO) {
                var ValorExcedente = MascaraNumero(item.ValorSugerido) * 0.15
                var ValorMaximoRegistrado = MascaraNumero(item.ValorSugerido) + ValorExcedente

                if (MascaraNumero(item.ValorRegistrado) > ValorMaximoRegistrado) {
                    ShowError('El valor registrado no debe superar un 15% más del valor sugerido')
                    item.ValorRegistrado = MascaraValores(item.ValorSugerido)
                }
            }
        }


        $scope.ConsultarSolicitudAnticipo = function () {
            
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoSolicitud: { Codigo: CODIGO_VALOR_CATALOGO_TIPO_SOLICITUD_ANTICIPO },
                Planilla: CERO,
                EstadoEncabezado: ESTADO_DEFINITIVO,
                EstadoERP : -1,
                Sync: true
            }

            var ResponseListadoSolicitudes = SolicitudesAnticipoFactory.ConsultarListaDetalles(filtros).Datos;

            if (ResponseListadoSolicitudes != undefined && ResponseListadoSolicitudes.length > 0) {
                $scope.ListadoSolicitudesPendientes = ResponseListadoSolicitudes;                    
            }
            
        }


        if ($routeParams.Numero > 0) {
            $scope.Numero = $routeParams.Numero;

            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Obteniendo Documento...");
            $timeout(function () { blockUI.message("Obteniendo Documento..."); Obtener(); }, 100);
           
        }

        $scope.MaskPlacaGrid = function (placa) {
            return MascaraPlaca(placa);
        }
        $scope.MaskNumeroGrid = function (planilla) {
            return MascaraNumero(planilla);
        }
        $scope.MaskMayusGrid = function (value) {
            return MascaraMayus(value);
        }
        $scope.MaskValoresGrid = function (value) {
            return MascaraValores(value);
        }
    }
]);