﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Operacion

    ''' <summary>
    ''' Clase <see cref=" SitiosTerceroCliente"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class SitiosTerceroCliente
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SitiosTerceroCliente"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SitiosTerceroCliente"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        ''' 
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo"), .Nombre = Read(lector, "Nombre_Cliente")}
            SitioCliente = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo"), .Nombre = Read(lector, "Nombre_Sitio")}
            Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo"), .Nombre = Read(lector, "Nombre_Ciudad")}
            Zona = New Zonas With {.Codigo = Read(lector, "ZOCI_Codigo"), .Nombre = Read(lector, "Nombre_Zona")}
            DireccionSitio = Read(lector, "Direccion_Sitio")
            Telefono = Read(lector, "Telefono")
            Contacto = Read(lector, "Contacto")
            ValorCargueCliente = Read(lector, "Valor_Cargue_Cliente")
            ValorDescargueCliente = Read(lector, "Valor_Descargue_Cliente")
            ValorCargueTransportador = Read(lector, "Valor_Cargue_Transportador")
            ValorDescargueTransportador = Read(lector, "Valor_Descargue_Transportador")
            Estado = Read(lector, "Estado")

        End Sub

        <JsonProperty>
        Public Property SitioCliente As SitiosCargueDescargue
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property CiudadCargue As Ciudades
        <JsonProperty>
        Public Property Ciudad As Ciudades
        <JsonProperty>
        Public Property Zona As Zonas
        <JsonProperty>
        Public Property DireccionSitio As String
        <JsonProperty>
        Public Property Telefono As String
        <JsonProperty>
        Public Property Contacto As String
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property ValorCargueCliente As Double
        <JsonProperty>
        Public Property ValorDescargueCliente As Double
        <JsonProperty>
        Public Property ValorCargueTransportador As Double
        <JsonProperty>
        Public Property ValorDescargueTransportador As Double
        <JsonProperty>
        Public Property Nuevo As Double

    End Class
End Namespace
