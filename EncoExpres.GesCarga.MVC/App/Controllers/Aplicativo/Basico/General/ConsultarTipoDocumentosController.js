﻿EncoExpresApp.controller("ConsultarTipoDocumentoCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'TipoDocumentosFactory', 'ValorCatalogosFactory', 'ImpuestosFactory', 'blockUI',
    function ($scope, $routeParams, $timeout, $linq, TipoDocumentosFactory, ValorCatalogosFactory, ImpuestosFactory, blockUI) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Tipo Documentos' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = ''; 
        $scope.MensajesError = [];
        var filtros = {};
        $scope.ListadoEstados = []; 
        $scope.ListaCiudad = [];
        $scope.ListadoImpuestosCiudades = [];
        $scope.ListadoImpuestosDepartamento = [];
        $scope.ListadoRecorridosTotal = [];
        $scope.NombreConcepto = '';
        $scope.ModeloNombre = "";
        $scope.MensajesErrorCiudad = [];
        $scope.MensajesErrorDepartamento = [];
        $scope.ListaDepartamento = [];
        $scope.MostrarMensajeError = false
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.Codigo = 0;
        $scope.CodigoImpuesto = 0;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_TIPO_DOCUMENTOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta página')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ModalTipoNumeracion = ''

        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'Activa', Codigo: 1 },
            { Nombre: 'Inactiva', Codigo: 0 }
        ];
        $scope.ListadoEstadosCiudades = [
            { Nombre: '(Seleccione item)', Codigo: -1 },
            { Nombre: 'Activo', Codigo: 1 },
            { Nombre: 'Inactivo', Codigo: 0 },


        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        $scope.ListadoTipoNumeracion = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_NUMERACION } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoNumeracion = [];
                    if (response.data.Datos.length > 0) {

                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != CODIGO_TIPO_IMPUESTO_NINGUNO) {
                                $scope.ListadoTipoNumeracion.push(response.data.Datos[i]);
                               
                            }
                            
                        }
                        $scope.ModalTipoNumeracion = $linq.Enumerable().From($scope.ListadoTipoNumeracion).First('$.Codigo == 1100');
                        
                    }
                    else {
                        $scope.ListadoTipoNumeracion = []
                    }
                }
            }, function (response) {
            });


        

        if ($routeParams.Codigo !== undefined) {
            $scope.Codigo = $routeParams.Codigo;
           
            Find();
        }

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            $routeParams.Codigo = null;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                $routeParams.Codigo = null;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                $routeParams.Codigo = null;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            $routeParams.Codigo = null;
            Find();
        };
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

        /*Metodo buscar*/
        $scope.Buscar = function () {

            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $routeParams.Codigo = null;
                $scope.Codigo = 0
                Find()
            }

        };

        
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarTipoDocumentos';
            }
        };

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO_LUGAR } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRecaudo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoRecaudo.push({ Codigo: 0, Nombre: '(TODAS)' });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoTipoRecaudo.push(response.data.Datos[i]);

                        }
                        $scope.ModalRecaudo = $linq.Enumerable().From($scope.ListadoTipoRecaudo).First('$.Codigo == ' + CERO);
                    }
                    else {
                        $scope.ListadoTipoRecaudo = []
                    }
                }
            }, function (response) {
            });

        //




        $scope.AbrirModal = function (Nombre) {
            $scope.NombreConcepto = Nombre
            showModal('ModalImpuestos');

        }

        $scope.BuscarConcepto = function (Codigo) {
            $scope.Codigo = Codigo;

            Obtener()

        }


        $scope.AgregarImpuesto = function () {
            
            if ($scope.ModeloImpuesto != '' && $scope.ModeloImpuesto2 != undefined && $scope.ModeloImpuesto2 != null && $scope.ModeloImpuesto2.Codigo != 0 && $scope.ModeloImpuesto2.Codigo != undefined) {
                var concidencias = 0
                if ($scope.ListadoImpuestos.length > 0) {
                    for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                        if ($scope.ListadoImpuestos[i].CodigoImpuesto == $scope.ModeloImpuesto2.Codigo) {
                            concidencias++
                            break;
                        }
                    }
                    if (concidencias > 0) {
                        ShowError('El Impuesto ya fue ingresado')
                        $scope.ModeloImpuesto2 = '';
                    } else {
                        $scope.ListadoImpuestos.push({ CodigoImpuesto: $scope.ModeloImpuesto2.Codigo, Nombre: $scope.ModeloImpuesto2.Nombre, Operacion: $scope.ModeloImpuesto2.Operacion, Valor_tarifa: $scope.ModeloImpuesto2.Valor_tarifa, valor_base: $scope.ModeloImpuesto2.valor_base, Estado: $scope.ModeloImpuesto2.Estado, Obtenido: ESTADO_INACTIVO });
                        $scope.ModeloImpuesto2 = '';
                    }
                } else {
                    $scope.ListadoImpuestos.push({ CodigoImpuesto: $scope.ModeloImpuesto2.Codigo, Nombre: $scope.ModeloImpuesto2.Nombre, Operacion: $scope.ModeloImpuesto2.Operacion, Valor_tarifa: $scope.ModeloImpuesto2.Valor_tarifa, valor_base: $scope.ModeloImpuesto2.valor_base, Estado: $scope.ModeloImpuesto2.Estado, Obtenido: ESTADO_INACTIVO });
                    $scope.ModeloImpuesto2 = '';
                }
            } else {
                ShowError('Debe ingresar un impuesto válido');
            }
            console.log("ListadoImpuestos: ", $scope.ListadoImpuestos);
        }

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoImpuesto = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoImpuesto.push({ Codigo: 0, Nombre: '(TODAS)' })

                        $scope.ModalImpuesto = $scope.ListadoTipoImpuesto[0];
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != CODIGO_TIPO_IMPUESTO_NINGUNO) {
                                $scope.ListadoTipoImpuesto.push(response.data.Datos[i]);
                            }
                        }


                    }
                    else {
                        $scope.ListadoTipoImpuesto = []
                    }
                }
            }, function (response) {
            });




        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoTipoDocumentos = [];

            DatosRequeridos();

            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Nombre: $scope.ModeloNombre,
                Estado: $scope.ModalEstado.Codigo,
                AplicaConsultaMaster: 1,
                TipoNumeracion: $scope.ModalTipoNumeracion.Codigo
            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                TipoDocumentosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoTipoDocumentos = [];
                                response.data.Datos.forEach(function (item) {
                                    if (item.Codigo > 0) {
                                        $scope.ListadoTipoDocumentos.push(item);
                                    }
                                });
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoTipoDocumentos = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };
        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarConcepto');

            $scope.ListadoImpuestos.forEach(function (item) {

                item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;

            })

            $scope.objEnviar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                ListadoImpuestos: $scope.ListadoImpuestos,
                AplicaDetalle: CODIGO_DOS,
            };

            TipoDocumentosFactory.Guardar($scope.objEnviar).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Numero == 0) {
                                ShowSuccess('Se guardaron los impuestos ');

                            }
                            else {
                                ShowSuccess('Se guardaron los impuestos ');

                            }
                            location.href = '#!ConsultarTipoDocumentos/' + $scope.Codigo;
                            closeModal('ModalImpuestos');
                            closeModal('modalAnularImpuesto');
                            closeModal('modalMensajeAnuloImpuesto');
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);

                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);

                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            $scope.NombreConcepto = '';
        };

        ImpuestosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: ESTADO_ACTIVO

        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListaImpuesto2 = response.data.Datos;


                        if ($scope.CodigoImpuesto != undefined && $scope.CodigoImpuesto != '' && $scope.CodigoImpuesto != 0 && $scope.CodigoImpuesto != null) {
                            if ($scope.CodigoImpuesto > 0) {
                                $scope.ModeloImpuesto2 = $linq.Enumerable().From($scope.ListaImpuesto2).First('$.Codigo ==' + $scope.CodigoImpuesto);
                            } else {
                                $scope.ModeloImpuesto2 = $linq.Enumerable().From($scope.ListaImpuesto2).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.ModeloImpuesto2 = $linq.Enumerable().From($scope.ListaImpuesto2).First('$.Codigo ==0');
                        }

                    }
                    else {
                        $scope.ListaImpuesto2 = [];
                        $scope.ModeloImpuesto2 = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        function DatosRequeridos() {
            if (filtros.Codigo == undefined) {
                filtros.Codigo = 0;
            }
        }

        $scope.Anular = function (codigo, CodigoAlterno) {
            $scope.Codigo = codigo
            $scope.NumeroDocumento = CodigoAlterno
            $scope.ModalErrorCompleto = ''
            $scope.CodigoAlterno = CodigoAlterno
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };

        $scope.ConfirmaAnular = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Observaciones: $scope.ModeloCausaAnula,
                Codigo: $scope.Codigo,
                AplicaImpuesto: ESTADO_INACTIVO
            };
            ConceptoLiquidacionFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se Eliminó el concepto: ' + $scope.NumeroDocumento);
                        closeModal('modalAnular');

                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeConfirmacionEliminoImpuesto');
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'No se puede Eliminar el concepto' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });



        };

        /*------------------------------------------------------------------------------------Eliminar Oficinas-----------------------------------------------------------------------*/
        $scope.EliminarImpuesto = function (codigo, CodigoImpuesto, Nombre) {
            $scope.Nombre = Nombre
            $scope.ModalErrorCompleto = ''
            $scope.CodigoImpuesto = CodigoImpuesto
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnularImpuesto');
        };

        $scope.Eliminar = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                CodigoImpuesto: $scope.CodigoImpuesto,
                AplicaDetalle: CODIGO_DOS
            };

         
            TipoDocumentosFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se Eliminó el Impuesto ' + $scope.Nombre);
                        closeModal('modalAnularImpuesto');
                        for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                            if ($scope.ListadoImpuestos[i].CodigoImpuesto == entidad.CodigoImpuesto) {
                                $scope.ListadoImpuestos.splice(i, 1);
                            }
                        }

                                   
                    }
                    else {
                        for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                            if ($scope.ListadoImpuestos[i].CodigoImpuesto == $scope.CodigoImpuesto) {

                                $scope.ListadoImpuestos.splice(i, 1);
                                i = $scope.ListadoImpuestos.length;
                                closeModal('modalAnularImpuesto ');
                            }
                        }


                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeAnuloImpuesto');
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'No se puede anular el impuesto ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });
              
            

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/



        function Obtener() {
            blockUI.start('Cargando Concepto Código ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Concepto Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
            };
            $scope.ListadoImpuestos = [];
            blockUI.delay = 1000;
            TipoDocumentosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.ListaRecorridosConsultados = response.data.Datos.ListadoImpuestos;
                        $scope.ListaRecorridosConsultados.forEach(function (item) {
                            item.Obtenido = ESTADO_ACTIVO;
                        });

                        $scope.ListadoRecorridosTotal = $scope.ListaRecorridosConsultados;


                        if ($scope.ListadoRecorridosTotal.length > 0) {

                            //Se muestran la cantidad de recorridos por pagina
                            //En este punto se muestra siempre desde la primera pagina
                            var i = 0;

                            for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                    $scope.ListadoImpuestos.push($scope.ListadoRecorridosTotal[i])
                                }
                            }



                            //Se operan los totales de registros y de paginas
                            if ($scope.ListadoRecorridosTotal.length > 0) {
                                $scope.totalRegistros = $scope.ListadoRecorridosTotal.length;
                                $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';

                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }


                        }
                        $scope.ListadoAuxiliar = response.data.Datos.ListadoImpuestos;

                    }
                    else {
                        ShowError('No se logró consultar la Orden de Cargue ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarTipoDocumentos';

                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarTipoDocumentos';
                    closeModal('modalAnularImpuesto');
                    closeModal('modalMensajeAnuloImpuesto');

                });
            blockUI.stop();
        };

        $scope.CerrarModal = function () {
            closeModal('ModalImpuestos');
            closeModal('modalAnularImpuesto');
            closeModal('modalMensajeAnuloImpuesto');

            Find();
            $scope.Codigo = 0;
            $scope.ListadoImpuestosCiudades = [];


        }







        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskDecimalesGrid = function (item) {
            return MascaraDecimales(item)
        };
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope)
        }


    }]);