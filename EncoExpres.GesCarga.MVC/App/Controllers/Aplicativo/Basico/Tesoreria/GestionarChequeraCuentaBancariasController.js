﻿EncoExpresApp.controller("GestionarChequeraCuentaBancariasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'CuentaBancariasFactory', 'ChequeraCuentaBancariasFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, CuentaBancariasFactory, ChequeraCuentaBancariasFactory) {

    $scope.Titulo = 'GESTIONAR CHEQUERAS';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Chequeras' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CHEQUERAS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.ListadoCuentasBancarias = [];

    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0
    }

    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVA" },
        { Codigo: 1, Nombre: "ACTIVA" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    /*Cargar el combo de cuentas BANCARIAS*/
    CuentaBancariasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCuentasBancarias = response.data.Datos

                    if ($scope.CodigoCuentaBancaria !== undefined && $scope.CodigoCuentaBancaria !== '' && $scope.CodigoCuentaBancaria !== null) {
                        $scope.Modelo.CuentaBancaria = $linq.Enumerable().From($scope.ListadoCuentasBancarias).First('$.Codigo ==' + $scope.CodigoCuentaBancaria)
                    }
                    else {
                        $scope.Modelo.CuentaBancaria = $scope.ListadoCuentasBancarias[0]
                    }
                }
                else {
                    $scope.ListadoCuentasBancarias = [];
                }
            }
        }, function (response) {
        });
    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando chequera código No. ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando chequera código No.' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        ChequeraCuentaBancariasFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.Modelo.Codigo = response.data.Datos.Codigo;
                    $scope.Modelo.NumeroCuenta = response.data.Datos.NumeroCuenta
                    $scope.Modelo.ChequeInicial = response.data.Datos.ChequeInicial
                    $scope.Modelo.ChequeFinal = response.data.Datos.ChequeFinal
                    $scope.Modelo.ChequeActual = response.data.Datos.ChequeActual
                    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                    $scope.CodigoCuentaBancaria = response.data.Datos.CuentaBancaria.Codigo

                    if ($scope.ListadoCuentasBancarias.length > 0) {
                        $scope.Modelo.CuentaBancaria = $linq.Enumerable().From($scope.ListadoCuentasBancarias).First('$.Codigo ==' + $scope.CodigoCuentaBancaria)
                    }
                }
                else {
                    ShowError('No se logro consultar la cuenta bancaria código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarChequeras';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarChequeras';
            });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {
            $scope.MaskNumero();
            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

            ChequeraCuentaBancariasFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó la chequera asociada a la cuenta bancaria "' + $scope.Modelo.CuentaBancaria.Nombre + '"');
                            }
                            else {
                                ShowSuccess('Se modificó la chequera asociada a la cuenta bancaria "' + $scope.Modelo.CuentaBancaria.Nombre + '"');
                            }
                            location.href = '#!ConsultarChequeras/' + $scope.Modelo.Codigo;
                        }
                    }
                    $scope.MaskValores();
                }, function (response) {
                    ShowError('No se pudo guardar la chequera, porfavor contacte al administrador del sistema');
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.CuentaBancaria == undefined || $scope.Modelo.CuentaBancaria == '' || $scope.Modelo.CuentaBancaria == null || $scope.Modelo.CuentaBancaria.Codigo == 0) {
            $scope.MensajesError.push('Debe seleccionar una cuenta bancaria');
            continuar = false;
        }
        if ($scope.Modelo.ChequeInicial == '' || $scope.Modelo.ChequeInicial == null || $scope.Modelo.ChequeInicial == undefined) {
            $scope.Modelo.ChequeInicial = 0;
        }
        if ($scope.Modelo.ChequeFinal == '' || $scope.Modelo.ChequeFinal == null || $scope.Modelo.ChequeFinal == undefined) {
            $scope.Modelo.ChequeFinal = 0;
        }
        if ($scope.Modelo.ChequeActual == '' || $scope.Modelo.ChequeActual == null || $scope.Modelo.ChequeActual == undefined) {
            $scope.Modelo.ChequeActual = 0;
        }
        if ($scope.Modelo.ChequeInicial > $scope.Modelo.ChequeFinal) {
            $scope.MensajesError.push('el cheque inicial debe ser menor al cheque final');
            continuar = false;
        }
        if ($scope.Modelo.ChequeActual > $scope.Modelo.ChequeFinal) {
            $scope.MensajesError.push('el cheque actual debe ser menor o igual  al cheque final ');
            continuar = false;
        }
        if ($scope.Modelo.ChequeActual < $scope.Modelo.ChequeInicial) {
            $scope.MensajesError.push('el cheque actual debe ser mayor o igual  al cheque inicial');
            continuar = false;
        }
        return continuar;
    }

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarChequeras/' + $scope.Modelo.Codigo;
    };


    $scope.MaskNumero = function (option) {
        try { $scope.Modelo.ChequeInicial = MascaraNumero($scope.Modelo.ChequeInicial) } catch (e) { }
        try { $scope.Modelo.ChequeFinal = MascaraNumero($scope.Modelo.ChequeFinal) } catch (e) { }
        try { $scope.Modelo.ChequeActual = MascaraNumero($scope.Modelo.ChequeActual) } catch (e) { }
    };

}]);