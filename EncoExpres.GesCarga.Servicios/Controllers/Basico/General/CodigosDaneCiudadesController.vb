﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General


<Authorize>
Public Class CodigosDaneCiudadesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As CodigoDaneCiudades) As Respuesta(Of IEnumerable(Of CodigoDaneCiudades))
        Return New LogicaCodigosDaneCiudades(CapaPersistenciaCodigosDaneCiudades).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As CodigoDaneCiudades) As Respuesta(Of CodigoDaneCiudades)
        Return New LogicaCodigosDaneCiudades(CapaPersistenciaCodigosDaneCiudades).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As CodigoDaneCiudades) As Respuesta(Of Long)
        Return New LogicaCodigosDaneCiudades(CapaPersistenciaCodigosDaneCiudades).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As CodigoDaneCiudades) As Respuesta(Of Boolean)
        Return New LogicaCodigosDaneCiudades(CapaPersistenciaCodigosDaneCiudades).Anular(entidad)
    End Function

End Class
