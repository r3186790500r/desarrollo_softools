﻿EncoExpresApp.controller("ConsultarNovedadesDespachosCtrl", ['$scope', '$timeout', 'NovedadesDespachosFactory', '$linq', 'blockUI', '$routeParams', function ($scope, $timeout, NovedadesDespachosFactory, $linq, blockUI, $routeParams) {

    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Novedades Despachos' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

    var filtros = {};
    $scope.cantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.ModalErrorCompleto = ''
    $scope.ModalError = ''
    $scope.MostrarMensajeError = false
    $scope.ListadoEstados = [
        { Nombre: '(TODOS)', Codigo: -1 },
        { Nombre: 'ACTIVO', Codigo: 1 },
        { Nombre: 'INACTIVO', Codigo: 0 }
    ];
    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_NOVEDADES_DESPACHOS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }
    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };


    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
            document.location.href = '#!GestionarNovedadesDespachos';
        }
    };
    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
            $scope.Codigo = 0
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoNovedadesDespachos = [];
        if ($scope.Buscando) {
            if ($scope.MensajesError.length === 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoAlterno: $scope.CodigoAlterno,
                    Nombre: $scope.Nombre,
                    Codigo: $scope.Codigo,
                    Estado: $scope.ModalEstado.Codigo,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                }
                blockUI.delay = 1000;
                NovedadesDespachosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoNovedadesDespachos = response.data.Datos
                               
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        blockUI.stop();
    }
    /*------------------------------------------------------------------------------------Eliminar Pais-----------------------------------------------------------------------*/
    $scope.EliminarNovedadesDespachos = function (codigo, Nombre, CodigoAlterno) {
        $scope.CodigoNovedadesDespachos = codigo
        $scope.NombreNovedadesDespachos = Nombre
        $scope.CodigoAlterno = CodigoAlterno
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        showModal('modalEliminarNovedadesDespachos');
    };

    $scope.Eliminar = function () {
        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.CodigoNovedadesDespachos,
        };

        NovedadesDespachosFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se eliminó la novedad despacho ' + $scope.CodigoAlterno + ' - ' + $scope.NombreNovedadesDespachos);
                    closeModal('modalEliminarNovedadesDespachos');
                    $scope.CodigoAlterno = '';
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                var result = ''
                result = InternalServerError(response.statusText)
                showModal('modalMensajeEliminarNovedadesDespachos');
                $scope.ModalError = 'No se puede eliminar la novedad despacho  ' + $scope.NombreNovedadesDespachos + ' ya que se encuentra relacionada con ' + result;
                $scope.ModalErrorCompleto = response.statusText

            });

    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /* Obtener parametros*/
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }

    $scope.CerrarModal = function () {
        closeModal('modalEliminarNovedadesDespachos');
        closeModal('modalMensajeEliminarNovedadesDespachos');
    }

    $scope.MaskNumero = function () {
        $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
    };
    $scope.MaskMayus = function () {
        try { $scope.Nombre = $scope.Nombre.toUpperCase() } catch (e) { }
    };

}]);