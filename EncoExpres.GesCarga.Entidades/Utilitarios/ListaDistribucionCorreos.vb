﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Utilitarios

    ''' <summary>
    ''' Clase <see cref="ListaDistribucionCorreos"></see>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ListaDistribucionCorreos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ListaDistribucionCorreos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ListaDistribucionCorreos"/>
        ''' </summary>
        ''' <param name="lector">Objeto DataReader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "ID")
            NumeroEvento = Read(lector, "EVCO_Codigo")
            Empleado = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Empleado"), .Nombre = Read(lector, "Tercero"), .NombreCompleto = Read(lector, "Tercero")}
            Estado = Read(lector, "Estado")
            Email = Read(lector, "Email")

        End Sub

        <JsonProperty>
        Public Property Email As String
        <JsonProperty>
        Public Property NumeroEvento As Integer
        <JsonProperty>
        Public Property Empleado As Tercero
        <JsonProperty>
        Public Listado As IEnumerable(Of ListaDistribucionCorreos)

    End Class

End Namespace