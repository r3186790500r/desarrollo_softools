﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Almacen

    ''' <summary>
    ''' Clase <see cref="DocumentoAlmacenes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DocumentoAlmacenes
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DocumentoAlmacenes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DocumentoAlmacenes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")
            Almacen = New Almacenes With {.Codigo = Read(lector, "ALMA_Codigo"), .Nombre = Read(lector, "NombreAlmacen")}
            Estado = New ValorCatalogos With {.Codigo = Read(lector, "Estado")}
            Anulado = Read(lector, "Anulado")
            Obtener = Read(lector, "Obtener")
            NumeroOrdenCompra = Read(lector, "EOCA_Numero")
            Observaciones = Read(lector, "Observaciones")
            TotalRegistros = Read(lector, "TotalRegistros")
            ReferenciaAlmacen = New ReferenciaAlmacenes With {.Codigo = Read(lector, "REAL_Codigo"), .Referencia = Read(lector, "Referencia")}
            TipoDocumentos = New TipoDocumentos With {.Codigo = Read(lector, "TIDO_Codigo"), .Nombre = Read(lector, "TipoDocumento")}
            UnidadMedida = Read(lector, "Nombre_Corto")
            TotalRegistros = Read(lector, "TotalRegistros")
            Cantidad = Read(lector, "Cantidad")
            ValorUnitario = Read(lector, "Valor_Unitario")
            ValorTotal = Read(lector, "Valor_Total")
            Responsable = Read(lector, "Responsable")
            NombreResibeSalida = Read(lector, "Nombre_Recibe_Salida")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            ValorTotal = Read(lector, "Valor_Total")
            NumeroRemision = Read(lector, "Numero_Remision")
            NombreRecibe = Read(lector, "Nombre_Recibe")
            Proveedor = New Tercero With {.Codigo = Read(lector, "TERC_Proveedor"), .Nombre = Read(lector, "Proveedor")}

        End Sub

        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Almacen As Almacenes
        <JsonProperty>
        Public Property TipoDocumentos As TipoDocumentos
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Almacentraslado As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property ENDATraslado As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property NumeroOrdenCompra As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property NumeroRemision As String
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property NombreRecibe As String
        ''' <summary>
        ''' Obtiene o establece el nombre resposable
        ''' </summary>
        <JsonProperty>
        Public Property Responsable As String
        ''' <summary>
        ''' Obtiene o establece el nombre recibe salida
        ''' </summary>
        <JsonProperty>
        Public Property NombreResibeSalida As String
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property ValorTotal As Double

        <JsonProperty>
        Public Property ReferenciaAlmacen As ReferenciaAlmacenes

        <JsonProperty>
        Public Property Cantidad As Integer

        <JsonProperty>
        Public Property UnidadMedida As String

        <JsonProperty>
        Public Property ValorUnitario As Double

        ''' <summary>
        ''' Obtiene o establece el detalle de la orden de compra
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty>
        Public Property Detalles As IEnumerable(Of DetalleDocumentoAlmacenes)

        <JsonProperty>
        Public Property NombreAlmacen As String

        <JsonProperty>
        Public Property NombreReferencia As String
        <JsonProperty>
        Public Property Referencia As String
        <JsonProperty>
        Public Property ConsultaKardex As Integer

        <JsonProperty>
        Public Property Estado As ValorCatalogos

        <JsonProperty>
        Public Property Proveedor As Tercero

    End Class
End Namespace
