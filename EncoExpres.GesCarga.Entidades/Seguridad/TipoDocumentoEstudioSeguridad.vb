﻿Imports Newtonsoft.Json

Namespace Seguridad

    ''' <summary>
    ''' Clase <see cref="TipoDocumentoEstudioSeguridad"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TipoDocumentoEstudioSeguridad
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoDocumentoEstudioSeguridad"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoDocumentoEstudioSeguridad"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            EOESCodigo = Read(lector, "EOES_Codigo")
            Nombre = Read(lector, "Nombre")
            AplicaReferencia = Read(lector, "Aplica_Referencia")
            AplicaEmisor = Read(lector, "Aplica_Emisor")
            AplicaFechaEmision = Read(lector, "Aplica_Fecha_Emision")
            AplicaFechaVence = Read(lector, "Aplica_Fecha_Vence")

        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property EOESCodigo As Integer
        <JsonProperty>
        Public Property AplicaReferencia As Short
        <JsonProperty>
        Public Property AplicaEmisor As Short
        <JsonProperty>
        Public Property AplicaFechaEmision As Short
        <JsonProperty>
        Public Property AplicaFechaVence As Short

    End Class
End Namespace
