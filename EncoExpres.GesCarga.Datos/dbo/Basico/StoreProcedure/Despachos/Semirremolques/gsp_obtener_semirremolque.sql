﻿print 'gsp_obtener_semirremolque'
go
drop procedure gsp_obtener_semirremolque
go
create procedure gsp_obtener_semirremolque
(
@par_EMPR_Codigo smallint,
@par_Codigo numeric = null,
@par_Placa varchar(20) = null
)
as 
begin

select 
1 AS Obtener,
EMPR_Codigo,
Codigo,
Placa,
ISNULL(Codigo_Alterno,'') AS Codigo_Alterno,
ISNULL(Modelo,0) AS Modelo,
'' AS EstadoSemirremolque,
TERC_Codigo_Propietario,
Equipo_Propio,
MASE_Codigo,
CATA_TISE_Codigo,
CATA_TICA_Codigo,
CATA_TIEQ_Codigo,
ISNULL(Numero_Ejes,0) AS Numero_Ejes,
ISNULL(Levanta_Ejes,0) AS Levanta_Ejes,
ISNULL(Ancho,0) AS Ancho,
ISNULL(Alto,0) AS Alto,
ISNULL(Largo,0) AS Largo,
ISNULL(Peso_Vacio,0) AS Peso_Vacio,
ISNULL(Capacidad_Kilos,0) AS Capacidad_Kilos,
ISNULL(Capacidad_Galones,0) AS Capacidad_Galones,
ISNULL(Justificacion_Bloqueo,'') AS Justificacion_Bloqueo,
Estado


from 
Semirremolques 

where EMPR_Codigo = @par_EMPR_Codigo
and Codigo = ISNULL(@par_Codigo,Codigo)
and Placa = ISNULL (@par_Placa,Placa)


end
GO
