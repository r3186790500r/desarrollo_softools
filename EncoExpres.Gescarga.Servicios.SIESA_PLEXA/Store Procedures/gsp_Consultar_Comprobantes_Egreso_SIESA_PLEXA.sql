CREATE PROCEDURE [dbo].[gsp_Consultar_Comprobantes_Egreso_SIESA_PLEXA]          
(          
@par_EMPR_Codigo smallint          
)          
AS          
BEGIN          
	SELECT         
	ENCC.EMPR_Codigo, ENCC.Numero, ENCC.Fecha, ENCC.Numero_Documento, ENCC.Fecha_Documento, ENCC.TERC_Documento,          
	ENCC.OFIC_Documento, ENCC.CATA_DOOR_Codigo, ENCC.Observaciones, ENCC.Valor_Comprobante, ENCC.OFIC_Codigo,          
	ISNULL(ENCC.Interfase_Contable, 0) AS Interfase_Contable, ENCC.Anulado,          
	DECC.PLUC_Codigo, PLUC.Codigo_Cuenta, DECC.Valor_Base, DECC.Valor_Credito, DECC.Valor_Debito,          
	DECC.Documento_Cruce, TERC.Numero_Identificacion AS Identificacion_Tercero,      
	VACA.Campo2 AS Centro_Operacion_Movimiento      
   
	FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Terceros AS TERC,   
	Plan_Unico_Cuentas As PLUC, Encabezado_Documento_Comprobantes AS ENDC, Valor_Catalogos AS VACA      
   
	WHERE ENCC.EMPR_Codigo = @par_EMPR_Codigo          
	AND ENCC.EMPR_Codigo = DECC.EMPR_Codigo          
	AND ENCC.Numero = DECC.ENCC_Numero  
   
	AND ENCC.EMPR_Codigo = TERC.EMPR_Codigo          
	AND ENCC.TERC_Documento = TERC.Codigo  
   
	AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo          
	AND DECC.PLUC_Codigo = PLUC.Codigo  
   
	AND ENCC.EMPR_Codigo = ENDC.EMPR_Codigo       
	AND ENCC.Codigo_Documento = ENDC.Codigo      
	AND ENCC.Numero_Documento = ENDC.Numero   
   
	AND ENDC.EMPR_Codigo = VACA.EMPR_Codigo      
	AND ENDC.CATA_CCCE_Codigo = VACA.Codigo  
   
	AND ISNULL(ENCC.Interfase_Contable, 0) = 0          
	AND ISNULL(ENCC.Intentos_Interfase_Contable, 0) < 5          
	AND ENCC.Anulado = 0          
   
	ORDER BY ENCC.Numero DESC -- Temporal Pruebas         
END 
GO


