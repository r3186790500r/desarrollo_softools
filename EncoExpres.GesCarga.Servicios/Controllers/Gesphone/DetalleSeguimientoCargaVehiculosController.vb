﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Negocio.Gesphone

''' <summary>
''' Controlador <see cref="DetalleSeguimientoCargaVehiculosController"/>
''' </summary>
<Authorize>
Public Class DetalleSeguimientoCargaVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleSeguimientoCargaVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoCargaVehiculos))
        Return New LogicaDetalleSeguimientoCargaVehiculos(CapaPersistenciaDetalleSeguimientoCargaVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleSeguimientoCargaVehiculos) As Respuesta(Of DetalleSeguimientoCargaVehiculos)
        Return New LogicaDetalleSeguimientoCargaVehiculos(CapaPersistenciaDetalleSeguimientoCargaVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleSeguimientoCargaVehiculos) As Respuesta(Of Long)
        Return New LogicaDetalleSeguimientoCargaVehiculos(CapaPersistenciaDetalleSeguimientoCargaVehiculos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleSeguimientoCargaVehiculos) As Respuesta(Of Boolean)
        Return New LogicaDetalleSeguimientoCargaVehiculos(CapaPersistenciaDetalleSeguimientoCargaVehiculos).Anular(entidad)
    End Function
End Class
