﻿print 'gsp_consultar_tarifa_transportes'
go
drop procedure gsp_consultar_tarifa_transportes
go
create procedure gsp_consultar_tarifa_transportes
(
@par_EMPR_Codigo smallint,
@par_Estado smallint
)
as begin
select 
TATC.EMPR_Codigo,
TATC.Nombre,
TATC.Codigo,
TATC.Estado,
LNTT.LNTC_Codigo
from Tarifa_Transporte_Carga TATC
LEFT JOIN Linea_Negocio_Tarifa_Transporte LNTT
ON TATC.Codigo = LNTT.TATC_Codigo
AND TATC.EMPR_Codigo = LNTT.EMPR_Codigo

where TATC.EMPR_Codigo = @par_EMPR_Codigo
and TATC.Estado = @par_Estado
end
go
