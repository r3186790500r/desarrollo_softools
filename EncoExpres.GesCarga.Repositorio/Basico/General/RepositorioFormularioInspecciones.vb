﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioFormularioInspecciones"/>
    ''' </summary>
    Public NotInheritable Class RepositorioFormularioInspecciones
        Inherits RepositorioBase(Of FormularioInspecciones)

        Public Overrides Function Consultar(filtro As FormularioInspecciones) As IEnumerable(Of FormularioInspecciones)
            Dim lista As New List(Of FormularioInspecciones)
            Dim item As FormularioInspecciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If
                If Not IsNothing(filtro.Nombre) Then
                    If Len(filtro.Nombre) > 0 Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not IsNothing(filtro.TipoDocumentoOrigen) Then
                    If filtro.TipoDocumentoOrigen.Codigo > 13000 Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", filtro.TipoDocumentoOrigen.Codigo)
                    End If
                End If

                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_formularios_inspecciones]")

                While resultado.Read
                    item = New FormularioInspecciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As FormularioInspecciones) As Long
            Dim inserto As Boolean = True
            Dim insertoSeccion As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_Version", entidad.Version)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", entidad.TipoDocumentoOrigen.Codigo)
                    'conexion.AgregarParametroSQL("@par_Preoperacional", entidad.Preoperacional)
                    conexion.AgregarParametroSQL("@par_CATA_TIIN_Codigo", entidad.TipoInspeccion.Codigo)
                    conexion.AgregarParametroSQL("@par_Firma", entidad.Firma)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_formularios_inspecciones")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.SeccionFormulario
                        detalle.CodigoFormulario = entidad.Codigo
                        detalle.UsuarioCrea = entidad.UsuarioCrea
                        If Not InsertarSeccion(detalle, conexion) Then
                            insertoSeccion = False
                            inserto = False
                            Exit For
                        End If
                    Next


                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As FormularioInspecciones) As Long

            Dim inserto As Boolean = True
            Dim insertoSeccion As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", entidad.TipoDocumentoOrigen.Codigo)
                    'conexion.AgregarParametroSQL("@par_Preoperacional", entidad.Preoperacional)
                    conexion.AgregarParametroSQL("@par_CATA_TIIN_Codigo", entidad.TipoInspeccion.Codigo)
                    conexion.AgregarParametroSQL("@par_Firma", entidad.Firma)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                    conexion.AgregarParametroSQL("@par_Version", entidad.Version)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_formularios_inspecciones")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While
                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.SeccionFormulario
                        detalle.CodigoFormulario = entidad.Codigo
                        detalle.UsuarioCrea = entidad.UsuarioModifica
                        If Not ModificarSeccion(detalle, conexion) Then
                            insertoSeccion = False
                            inserto = False
                            Exit For
                        End If
                    Next


                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Function InsertarSeccion(entidad As SeccionFormularioInspecciones, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True
            Dim insertoItem As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                contextoConexion.CleanParameters()

                contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                contextoConexion.AgregarParametroSQL("@par_ENFI_Codigo", entidad.CodigoFormulario)
                contextoConexion.AgregarParametroSQL("@par_Orden_Formulario", entidad.Orden)
                contextoConexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                contextoConexion.AgregarParametroSQL("@par_Estado", entidad.EstadoDocumento.Codigo)
                contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_secciones_formularios_inspecciones]")

                While resultado.Read
                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

                For Each detalle In entidad.ListadoItemSeccion
                    detalle.CodigoFormulario = entidad.CodigoFormulario
                    detalle.CodigoEmpresa = entidad.CodigoEmpresa
                    detalle.UsuarioCrea = entidad.UsuarioCrea
                    detalle.Seccion = entidad.Codigo
                    If Not InsertarItem(detalle, contextoConexion) Then
                        insertoItem = False
                        inserto = False
                        Exit For
                    End If
                Next

                If inserto Then
                    transaccion.Complete()
                Else
                    inserto = False
                End If
            End Using
            Return inserto
        End Function
        Public Function InsertarItem(entidad As ItemFormularioInspecciones, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ENFI_Codigo", entidad.CodigoFormulario)
            contextoConexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
            contextoConexion.AgregarParametroSQL("@par_Orden_Formulario", entidad.Orden)
            contextoConexion.AgregarParametroSQL("@par_SFIN_Codigo", entidad.Seccion)
            contextoConexion.AgregarParametroSQL("@par_Relevante", entidad.Relevante)
            contextoConexion.AgregarParametroSQL("@par_Tamaño", entidad.Tamano)
            contextoConexion.AgregarParametroSQL("@par_Exige_Cumplimiento", entidad.ExigeCumplimiento)
            contextoConexion.AgregarParametroSQL("@par_CATA_TCFI_Codigo", entidad.TipoControl.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Estado", entidad.EstadoDocumento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_items_secciones_formularios_inspecciones]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Function ModificarSeccion(entidad As SeccionFormularioInspecciones, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True
            Dim insertoItem As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                contextoConexion.CleanParameters()

                contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                contextoConexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                contextoConexion.AgregarParametroSQL("@par_ENFI_Codigo", entidad.CodigoFormulario)
                contextoConexion.AgregarParametroSQL("@par_Orden_Formulario", entidad.Orden)
                contextoConexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                contextoConexion.AgregarParametroSQL("@par_Estado", entidad.EstadoDocumento.Codigo)
                contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_modificar_detalle_secciones_formularios_inspecciones]")

                While resultado.Read
                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

                For Each detalle In entidad.ListadoItemSeccion
                    detalle.CodigoFormulario = entidad.CodigoFormulario
                    detalle.CodigoEmpresa = entidad.CodigoEmpresa
                    detalle.UsuarioCrea = entidad.UsuarioCrea
                    detalle.Seccion = entidad.Codigo
                    If Not ModificarItem(detalle, contextoConexion) Then
                        insertoItem = False
                        inserto = False
                        Exit For
                    End If
                Next

                If inserto Then
                    transaccion.Complete()
                Else
                    inserto = False
                End If
            End Using
            Return inserto
        End Function
        Public Function ModificarItem(entidad As ItemFormularioInspecciones, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_ENFI_Codigo", entidad.CodigoFormulario)
            contextoConexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
            contextoConexion.AgregarParametroSQL("@par_Orden_Formulario", entidad.Orden)
            contextoConexion.AgregarParametroSQL("@par_Exige_Cumplimiento", entidad.ExigeCumplimiento)
            contextoConexion.AgregarParametroSQL("@par_SFIN_Codigo", entidad.Seccion)
            contextoConexion.AgregarParametroSQL("@par_Relevante", entidad.Relevante)
            contextoConexion.AgregarParametroSQL("@par_Tamaño", entidad.Tamano)
            contextoConexion.AgregarParametroSQL("@par_CATA_TCFI_Codigo", entidad.TipoControl.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Estado", entidad.EstadoDocumento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)


            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_modificar_items_secciones_formularios_inspecciones]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Overrides Function Obtener(filtro As FormularioInspecciones) As FormularioInspecciones
            Dim item As New FormularioInspecciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_formularios_inspecciones]")


                While resultado.Read
                    item = New FormularioInspecciones(resultado)
                End While
                If filtro.ConsularDetalle > 0 Then
                    Dim lista As New List(Of SeccionFormularioInspecciones)
                    Dim Seccion As SeccionFormularioInspecciones
                    resultado.Close()
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_secciones_formularios_inspecciones]")
                    While resultado.Read
                        Seccion = New SeccionFormularioInspecciones(resultado)
                        lista.Add(Seccion)
                    End While
                    item.SeccionFormulario = lista
                    Dim listaItem As New List(Of ItemFormularioInspecciones)
                    Dim ItemFormulario As ItemFormularioInspecciones
                    resultado.Close()
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_items_secciones_formularios_inspecciones]")
                    While resultado.Read
                        ItemFormulario = New ItemFormularioInspecciones(resultado)
                        listaItem.Add(ItemFormulario)
                    End While
                    item.itemFormulario = listaItem
                End If
            End Using

            Return item
        End Function
        Public Function Anular(entidad As FormularioInspecciones) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_encabezado_formularios_inspecciones]")

                While resultado.Read
                    anulo = IIf(Convert.ToInt64(resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace