﻿PRINT 'gsp_eliminar_listas_referencias'
GO
DROP PROCEDURE gsp_eliminar_listas_referencias
GO
CREATE PROCEDURE gsp_eliminar_listas_referencias    
(@par_EMPR_Codigo SMALLINT,    
@par_Numero_Estudio NUMERIC)    
AS  
BEGIN    
DELETE Detalle_Referencias_Estudio_Seguridad    
WHERE     
EMPR_Codigo = @par_EMPR_Codigo    
AND     
ENES_Numero = @par_Numero_Estudio    
END    
GO