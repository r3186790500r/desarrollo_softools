﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports System.Transactions

Namespace Mantenimiento
    Public NotInheritable Class RepositorioTareaMantenimiento
        Inherits RepositorioBase(Of TareaMantenimiento)

        Public Overrides Function Consultar(filtro As TareaMantenimiento) As IEnumerable(Of TareaMantenimiento)
            Dim lista As New List(Of TareaMantenimiento)
            Dim item As TareaMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Pagina) Then
                    If (filtro.Pagina) Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If (filtro.RegistrosPagina) Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If (filtro.FechaInicial > Date.MinValue) And (filtro.FechaFinal >= filtro.FechaInicial) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If
                If Not IsNothing(filtro.Numero) Then
                    If filtro.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    End If
                End If
                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado <> -1 Then 'NO APLICA
                        conexion.AgregarParametroSQL("@par_CATA_ESDO_Codigo", filtro.Estado)
                    End If
                End If
                If Not IsNothing(filtro.EquipoMantenimiento) Then
                    If filtro.EquipoMantenimiento.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_EQMA_Codigo", filtro.EquipoMantenimiento.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Vehiculo) Then
                    If Not IsNothing(filtro.Vehiculo.Codigo) Then
                        If filtro.Vehiculo.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo", filtro.Vehiculo.Codigo)
                        End If
                    End If
                End If

                If Not IsNothing(filtro.Nombre) Then
                    If filtro.Nombre <> "" Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If

                If Not IsNothing(filtro.Placa) Then
                    If filtro.Placa <> "" Then
                        conexion.AgregarParametroSQL("@par_Placa", filtro.Placa)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_encabezado_tarea_equipo_mantenimientos]")

                While resultado.Read
                    item = New TareaMantenimiento(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As TareaMantenimiento) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_EQMA_Codigo", entidad.EquipoMantenimiento.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_ESDO_Codigo", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_encabezado_tarea_equipo_mantenimientos]")

                    While resultado.Read
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While

                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalles
                        detalle.Numero = entidad.Numero
                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto And insertoRecorrido Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using
            Return entidad.Numero
        End Function

        Public Function InsertarDetalle(entidad As DetalleTareaMantenimiento, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ETEM_Numero", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_SUMA_SIMA_Codigo", entidad.SistemaMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_SUMA_Codigo", entidad.SubsistemaMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TIME_Codigo", entidad.TipoMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
            contextoConexion.AgregarParametroSQL("@par_UNFM_Codigo", entidad.UnidadReferenciaMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TERC_Proveedor", entidad.Proveedor.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Numero_Factura", entidad.NumeroFactura)
            contextoConexion.AgregarParametroSQL("@par_Fecha_Factura", entidad.FechaFactura, SqlDbType.DateTime)
            contextoConexion.AgregarParametroSQL("@par_Fecha_Vence_Garantia", entidad.FechaVenceGarantia, SqlDbType.DateTime)
            contextoConexion.AgregarParametroSQL("@par_Valor", entidad.Valor)
            contextoConexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
            contextoConexion.AgregarParametroSQL("@par_Valor_IVA", entidad.ValorIva)
            contextoConexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            contextoConexion.AgregarParametroSQL("@par_ENDA_TIDO_Codigo", entidad.ENDATiDOCodigo)
            contextoConexion.AgregarParametroSQL("@par_ENDA_Numero", entidad.ENDANumero)
            contextoConexion.AgregarParametroSQL("@par_KM_Uso", entidad.KmUso)


            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_detalle_tarea_equipo_mantenimientos]")

            While resultado.Read
                inserto = IIf(resultado.Item("ID") > Cero, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As TareaMantenimiento) As Long

            Dim Modificodetalle As Boolean = True
            Dim ModificoRecargo As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_EQMA_Codigo", entidad.EquipoMantenimiento.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_ESDO_Codigo", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_modificar_encabezado_tarea_equipo_mantenimientos]")

                    While resultado.Read
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While

                    resultado.Close()

                    If resultado.RecordsAffected > 0 Then
                        Modificar = resultado.RecordsAffected
                        resultado.Close()
                    End If

                    For Each detalle In entidad.Detalles
                        detalle.Numero = entidad.Numero
                        If Not InsertarDetalle(detalle, conexion) Then
                            Modificodetalle = False

                            Exit For
                        End If
                    Next

                    If Modificodetalle Then
                        transaccion.Complete()
                    End If

                End Using
            End Using
            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As TareaMantenimiento) As TareaMantenimiento
            Dim item As New TareaMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_encabezado_tarea_equipo_mantenimientos]")

                While resultado.Read
                    item = New TareaMantenimiento(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As TareaMantenimiento) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_anular_encabezado_tarea_equipo_mantenimiento]")

                While resultado.Read
                    anulo = IIf(Convert.ToInt16(resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class
End Namespace
