﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos
    <JsonObject>
    Public NotInheritable Class ValorCombustible
        Inherits BaseBasico
        Sub New()
        End Sub
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Fecha = Read(lector, "Fecha")
            TipoCombustible = New ValorCatalogos With {
            .Codigo = Read(lector, "CATA_TICO_Codigo"),
            .Nombre = Read(lector, "Combustible")
            }

            Valor = Read(lector, "Valor")
            UsuarioCrea = New SeguridadUsuarios.Usuarios With {
             .Codigo = Read(lector, "USUA_Codigo_Crea"),
            .Nombre = Read(lector, "Usuario")}
            FechaCrea = Read(lector, "Fecha_Crea")
            UsuarioModifica = New SeguridadUsuarios.Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Modifica")}
            FechaModifica = Read(lector, "Fecha_Modifica")


        End Sub
        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property TipoCombustible As ValorCatalogos
        <JsonProperty>
        Public Property Valor As Double

        <JsonProperty>
        Public Property Fecha As Date

        <JsonProperty>
        Public Property FechaInicio As Date

        <JsonProperty>
        Public Property FechaFin As Date



    End Class

End Namespace

