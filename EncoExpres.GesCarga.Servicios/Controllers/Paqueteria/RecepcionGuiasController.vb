﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Negocio.Paqueteria




Public Class RecepcionGuiasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Anular(entidad)
    End Function


    <HttpPost>
    <ActionName("RecibirGuias")>
    Public Function RecibirGuias(ByVal filtro As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRecepcionGuias(CapaPersistenciaRecepcionGuias).RecibirGuias(filtro)
    End Function
End Class
