﻿EncoExpresApp.controller("ConsultarEquipoMantenimientoCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', 'ValorCatalogosFactory', '$linq', 'EstadoEquipoMantenimientoFactory', 'EquipoMantenimientoFactory', function ($scope, $timeout, $routeParams, blockUI, ValorCatalogosFactory, $linq, EstadoEquipoMantenimientoFactory, EquipoMantenimientoFactory) {
    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
    $scope.Buscando = false;
    $scope.ResultadoSinRegistros = ''; 
    $scope.MensajesError = [];
    var filtros = {};
    $scope.ListadoEstados = [];
    $scope.totalRegistros = 0;
    $scope.paginaActual = 1;
    $scope.cantidadRegistrosPorPagina = 10;
    $scope.totalPaginas = 0;
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Mantenimiento' }, { Nombre: 'Equipos' }];
    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_EQUIPO_MANTENIMIENTO);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    // -------------------------- Constantes ---------------------------------------------------------------------//

    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };

    /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
    EstadoEquipoMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoEstadoMantenimiento = [];
                $scope.ListadoEstadoMantenimiento.push({ Codigo: -1, Nombre: '(TODOS)' })
                response.data.Datos.forEach(function (item) {
                    if (item.Codigo != ESTADO_REGISTRO_NO_APLICA) {
                        $scope.ListadoEstadoMantenimiento.push(item);
                    }
                });
                $scope.EstadoMantenimiento = $linq.Enumerable().From($scope.ListadoEstadoMantenimiento).First('$.Codigo == -1');
                //$scope.Estado = $linq.Enumerable.From($scope.ListadoEstadoMantenimiento).First('$.Codigo == -1');
            }
        }, function (response) {
            ShowError(response.statusText);
        });
    /*Cargar el combo de estados*/ 
    $scope.ListadoEstados = [];
    $scope.ListadoEstados = [
        { Codigo: -1, Nombre: '(TODOS)' },
        { Codigo: 0, Nombre: 'Inactivo' },
        { Codigo: 1, Nombre: 'Activo' }
    ];
    $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First("$.Codigo == -1");
    /*---------------------------------------------------------------------Funcion Buscar Linea de Mantenimiento-----------------------------------------------------------------------------*/
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = 0;
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);

        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoEquipoMantenimiento = [];
        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Codigo,
            Nombre: $scope.Nombre,
            CodigoAlterno: $scope.Codigo_Alterno,
            EstadoDocumento: $scope.Estado,
            EstadoEquipoMantenimiento: $scope.EstadoMantenimiento,
            Pagina: $scope.paginaActual,
            RegistrosPagina: $scope.cantidadRegistrosPorPagina
        };


        if ($scope.MensajesError.length == 0) {
            blockUI.delay = 1000;

            EquipoMantenimientoFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoEquipoMantenimiento = response.data.Datos

                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        else {
            $scope.Buscando = false;
        }
        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarEquipoMantenimiento';
        }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Codigo = MascaraNumero($scope.Codigo) } catch (e) { }
    };

    if ($routeParams.Codigo > 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }




}]);