﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Utilitarios
Imports EncoExpres.GesCarga.Repositorio.Utilitarios

Namespace Utilitarios
    Public NotInheritable Class PersistenciaListadosAuditoriaDocumentos
        Implements IPersistenciaBase(Of ListadosAuditoriaDocumentos)
        Public Function Consultar(filtro As ListadosAuditoriaDocumentos) As IEnumerable(Of ListadosAuditoriaDocumentos) Implements IPersistenciaBase(Of ListadosAuditoriaDocumentos).Consultar
            Return New RepositorioListadosAuditoriaDocumentos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ListadosAuditoriaDocumentos) As Long Implements IPersistenciaBase(Of ListadosAuditoriaDocumentos).Insertar
            Return New RepositorioListadosAuditoriaDocumentos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ListadosAuditoriaDocumentos) As Long Implements IPersistenciaBase(Of ListadosAuditoriaDocumentos).Modificar
            Return New RepositorioListadosAuditoriaDocumentos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ListadosAuditoriaDocumentos) As ListadosAuditoriaDocumentos Implements IPersistenciaBase(Of ListadosAuditoriaDocumentos).Obtener
            Return New RepositorioListadosAuditoriaDocumentos().Obtener(filtro)
        End Function
    End Class
End Namespace
