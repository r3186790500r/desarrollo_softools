﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Utilitarios

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" ListasCorreos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ListasCorreos
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ListasCorreos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ListasCorreos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Tercero = New Tercero With {.Codigo = Read(lector, "TERC_Codigo")}
            EventoCorreo = New EventoCorreos With {.Codigo = Read(lector, "EVCO_Codigo"), .Nombre = Read(lector, "Nombre"), .TipoDocumento = Read(lector, "EVCO_TIDO_Codigo")}
            Email = Read(lector, "Email")
            TerceroDireccion = New TerceroDirecciones With {.Codigo = Read(lector, "TEDI_Codigo"), .Nombre = Read(lector, "TEDI_Nombre")}

        End Sub

        <JsonProperty>
        Public Property Tercero As Tercero
        <JsonProperty>
        Public Property Email As String
        <JsonProperty>
        Public Property EventoCorreo As EventoCorreos
        <JsonProperty>
        Public Property TerceroDireccion As TerceroDirecciones

    End Class
End Namespace
