﻿Print 'V_Categoria_Tipo_Sangre'
GO
DROP VIEW V_Categoria_Tipo_Sangre
GO
CREATE VIEW V_Categoria_Tipo_Sangre 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 19
GO