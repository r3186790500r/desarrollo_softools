﻿

PRINT 'gsp_consultar_master_novedades_planilla_despachos'
GO
DROP PROCEDURE gsp_consultar_master_novedades_planilla_despachos
GO
CREATE PROCEDURE gsp_consultar_master_novedades_planilla_despachos 
(                    
 @par_EMPR_Codigo SMALLINT,                    
 @par_Fecha_Inicial DATETIME = NULL,           
 @par_Fecha_Final DATETIME = NULL,                  
 @par_Numero_Remesa NUMERIC = NULL,                    
 @par_Codigo_Novedad NUMERIC = NULL,                    
 @par_Numero_Planilla NUMERIC = NULL,                  
 @par_Numero_Manifiesto NUMERIC = NULL,       
 @par_NumeroPagina INT = NULL,                    
 @par_RegistrosPagina INT = NULL                  
)                    
AS                    
BEGIN      

 set @par_Fecha_Inicial = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Inicial)                       
 set @par_Fecha_Inicial = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Inicial)),@par_Fecha_Inicial)                              
 set @par_Fecha_Inicial = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Inicial)),@par_Fecha_Inicial)                                
 set @par_Fecha_Final = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Final)),@par_Fecha_Final)                              
 set @par_Fecha_Final = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Final)),@par_Fecha_Final)                              
 set @par_Fecha_Final = DATEADD(MILLISECOND, 998,@par_Fecha_Final)                              
 set @par_Fecha_Final = DATEADD(SECOND,59,@par_Fecha_Final)                              
 set @par_Fecha_Final = DATEADD(MINUTE,59,@par_Fecha_Final)                              
 set @par_Fecha_Final = DATEADD(HOUR,23,@par_Fecha_Final)  
              
 SET NOCOUNT ON;                    
  DECLARE                    
   @CantidadRegistros INT                    
  SELECT @CantidadRegistros = (                    
    SELECT DISTINCT                     
     COUNT(1)                     
 FROM                    
 		 Detalle_Novedades_Despacho as DEND            
         
		 LEFT JOIN Encabezado_Planilla_Despachos AS ENPD            
		 ON DEND.EMPR_Codigo = ENPD.EMPR_Codigo            
		 AND DEND.ENPD_Numero = ENPD.Numero            
      
		 LEFT JOIN Encabezado_Remesas AS ENRE            
		 ON DEND.EMPR_Codigo = ENRE.EMPR_Codigo            
		 AND DEND.ENRE_Numero = ENRE.Numero            
      
		 LEFT JOIN Novedades_Despacho AS NDES            
		 ON DEND.EMPR_Codigo = NDES.EMPR_Codigo            
		 AND DEND.NODE_Codigo = NDES.Codigo   

		 LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC            
		 ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo            
		 AND ENRE.ENMC_Numero = ENMC.Numero 
      
		 LEFT JOIN Terceros AS PROV            
		 ON DEND.EMPR_Codigo = PROV.EMPR_Codigo            
		 AND DEND.TERC_Codigo_Proveedor = PROV.Codigo            
      
		 LEFT JOIN Usuarios AS USUA            
		 ON DEND.EMPR_Codigo = USUA.EMPR_Codigo            
		 AND DEND.USUA_Codigo_Crea = USUA.Codigo  
		                
    WHERE                    
     DEND.EMPR_Codigo = @par_EMPR_Codigo
     AND DEND.Fecha_Crea BETWEEN ISNULL(@par_Fecha_Inicial, DEND.Fecha_Crea) AND ISNULL(@par_Fecha_Final, DEND.Fecha_Crea) 
     AND ENRE.Numero_Documento = ISNULL(@par_Numero_Remesa, ENRE.Numero_Documento)                    
     AND NDES.Codigo = ISNULL(@par_Codigo_Novedad, NDES.Codigo)
	 AND ENPD.Numero_Documento = ISNULL(@par_Numero_Planilla, ENPD.Numero_Documento)
	 AND ENMC.Numero_Documento = ISNULL(@par_Numero_Manifiesto, ENMC.Numero_Documento) 
	 AND DEND.Anulado = 0                   
     );                    
                                
    WITH Pagina AS                    
    (                
		 SELECT                  
		 DEND.EMPR_Codigo,                  
		 DEND.ID,                  
		 DEND.ENPD_Numero,            
		 ENPD.Numero_Documento AS ENPD_Numero_Documento,          
		 ISNULL(DEND.ENRE_Numero, 0) AS ENRE_Numero,          
		 ISNULL(ENRE.Numero_Documento, 0) AS ENRE_Numero_Documento,            
		 DEND.NODE_Codigo,          
		 ISNULL(NDES.Nombre, '') As Nombre_Novedad,   
		 ISNULL(ENRE.ENMC_Numero, 0) AS ENMC_Numero,    
		 ISNULL(ENMC.Numero_Documento, 0) AS ENMC_Numero_Documento,        
		 DEND.Observaciones,             
		 ISNULL(DEND.Valor_Compra, 0) AS Valor_Compra,          
		 ISNULL(DEND.Valor_Venta, 0) AS Valor_Venta,      
		 ISNULL(DEND.Valor_Costo, 0) AS Valor_Costo,           
		 ISNULL(DEND.TERC_Codigo_Proveedor, 0) AS TERC_Codigo_Proveedor,           
		 ISNULL(PROV.Razon_Social,'') + ' ' + ISNULL(PROV.Nombre,'') + ' ' + ISNULL(PROV.Apellido1,'') + ' ' + ISNULL(PROV.Apellido2,'') As Nombre_Proveedor,           
		 ISNULL(DEND.USUA_Codigo_Crea, 0) AS USUA_Codigo_Crea,           
		 ISNULL(USUA.Codigo_Usuario,'') As Nombre_Usuario_Crea,          
		 ISNULL(DEND.Fecha_Crea,'') As Fecha_Crea,          
		 DEND.Anulado,
		 ROW_NUMBER() OVER(ORDER BY DEND.Fecha_Crea) AS RowNumber             
       
		 FROM                  
		 Detalle_Novedades_Despacho as DEND            
         
		 LEFT JOIN Encabezado_Planilla_Despachos AS ENPD            
		 ON DEND.EMPR_Codigo = ENPD.EMPR_Codigo            
		 AND DEND.ENPD_Numero = ENPD.Numero            
      
		 LEFT JOIN Encabezado_Remesas AS ENRE            
		 ON DEND.EMPR_Codigo = ENRE.EMPR_Codigo            
		 AND DEND.ENRE_Numero = ENRE.Numero            
      
		 LEFT JOIN Novedades_Despacho AS NDES            
		 ON DEND.EMPR_Codigo = NDES.EMPR_Codigo            
		 AND DEND.NODE_Codigo = NDES.Codigo   
      
	     LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC            
		 ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo            
		 AND ENRE.ENMC_Numero = ENMC.Numero 

		 LEFT JOIN Terceros AS PROV            
		 ON DEND.EMPR_Codigo = PROV.EMPR_Codigo            
		 AND DEND.TERC_Codigo_Proveedor = PROV.Codigo            
      
		 LEFT JOIN Usuarios AS USUA            
		 ON DEND.EMPR_Codigo = USUA.EMPR_Codigo            
		 AND DEND.USUA_Codigo_Crea = USUA.Codigo   
          
    WHERE                    
     DEND.EMPR_Codigo = @par_EMPR_Codigo
     AND DEND.Fecha_Crea BETWEEN ISNULL(@par_Fecha_Inicial, DEND.Fecha_Crea) AND ISNULL(@par_Fecha_Final, DEND.Fecha_Crea) 
     AND ENRE.Numero_Documento = ISNULL(@par_Numero_Remesa, ENRE.Numero_Documento)                    
     AND NDES.Codigo = ISNULL(@par_Codigo_Novedad, NDES.Codigo)
	 AND ENPD.Numero_Documento = ISNULL(@par_Numero_Planilla, ENPD.Numero_Documento)
	 AND ENMC.Numero_Documento = ISNULL(@par_Numero_Manifiesto, ENMC.Numero_Documento)  
     AND DEND.Anulado = 0                
  )                    
  SELECT DISTINCT                    
         1 AS ConsultaNovedadesDespacho,                    
		 EMPR_Codigo,                  
		 ID,                  
		 ENPD_Numero,            
		 ENPD_Numero_Documento,          
		 ENRE_Numero,          
		 ENRE_Numero_Documento,            
		 NODE_Codigo,          
		 Nombre_Novedad,   
		 ENMC_Numero, 
		 ENMC_Numero_Documento,         
		 Observaciones,             
		 Valor_Compra,          
		 Valor_Venta,      
		 Valor_Costo,           
		 TERC_Codigo_Proveedor,           
		 Nombre_Proveedor,           
		 USUA_Codigo_Crea,           
		 Nombre_Usuario_Crea,          
		 Fecha_Crea,          
		 Anulado,        
          
  @CantidadRegistros AS TotalRegistros,                    
  @par_NumeroPagina AS PaginaObtener,                    
  @par_RegistrosPagina AS RegistrosPagina                    
  FROM                    
  Pagina                    
  WHERE                    
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                    
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                    
  ORDER BY Fecha_Crea                    
END      
GO