﻿EncoExpresApp.controller("ConsultarGrupoPerfilesCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'GrupoUsuariosFactory', 'PermisoGrupoUsuariosFactory',
    function ($scope, $timeout, $linq, blockUI, GrupoUsuariosFactory, PermisoGrupoUsuariosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Seguridad' }, { Nombre: 'Grupo/Perfiles' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.Seleccionado = false;
        $scope.TipoAplicacion = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;


        $scope.Auxtree = [];
        $scope.tree = [];
        $scope.FiltroOpcion = '';
       

        $scope.ListadoAplicativos = [
            {Nombre: '(Seleccione...)', Codigo : -1},
            { Nombre: 'APLICATIVO', Codigo: 220 },
            { Nombre: 'PORTAL', Codigo: 210 },
            { Nombre: 'GESPHONE', Codigo: 150 },
        ];
        $scope.TipoAplicativo = $linq.Enumerable().From($scope.ListadoAplicativos).First('$.Codigo == -1');
        $scope.changeTree = function () {

        }

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        };


        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GRUPOS_PERFILES); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN E IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarGrupoPerfiles';
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.CodigoInicial != undefined && $scope.CodigoFinal == undefined) {
                $scope.CodigoFinal = $scope.CodigoInicial
            }
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                Find()
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGrupos = [];

            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Nombre: $scope.Nombre,

                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }
                    blockUI.delay = 1000;
                    GrupoUsuariosFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    response.data.Datos.forEach(function (item) {
                                        var registro = item;
                                        $scope.ListadoGrupos.push(registro);
                                    });

                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }

        $scope.ActualizarMenu = function () {
           // $scope.Auxtree = [];
           // $scope.tree = [];
            
           // $scope.CargarModulos();
           // $scope.CargarMenu();

            //if ($scope.FiltroOpcion != undefined && $scope.FiltroOpcion != null && $scope.FiltroOpcion != '' && !isNaN($scope.FiltroOpcion)) {
                $('#jstree').jstree(true).search($scope.FiltroOpcion);
            //}
            
           // console.log($('#jstree').html());
           // $('#jstree').jstree('refresh');
        }

        $scope.CargarModulos = function () {
           
            filtrosModulo = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.TipoAplicativo.Codigo
            }
            blockUI.delay = 1000;
            PermisoGrupoUsuariosFactory.ConsultarModulo(filtrosModulo).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                //if (item.lineaServicioTransporte == $scope.Empresa.CodigoTipoTransporte) {
                                if (item.Aplicacion == CODIGO_APLICACION_APLICATIVO && item.Codigo !== OPCION_MODULO_APLICATIVO && item.Estado == ESTADO_ACTIVO) {
                                    $scope.Auxtree.push({ id: item.Codigo, parent: "#", text: item.Nombre, Padre: OPCION_MODULO_APLICATIVO });
                                }
                                else if (item.Aplicacion == CODIGO_APLICACION_GESPHONE && item.Codigo !== OPCION_MODULO_GESPHONE && item.Estado == ESTADO_ACTIVO) {
                                    $scope.Auxtree.push({ id: item.Codigo, parent: "#", text: item.Nombre, Padre: OPCION_MODULO_GESPHONE });
                                }
                                else if (item.Aplicacion == CODIGO_APLICACION_PORTAL && item.Codigo !== OPCION_MODULO_PORTAL && item.Estado == ESTADO_ACTIVO) {
                                    $scope.Auxtree.push({ id: item.Codigo, parent: "#", text: item.Nombre, Padre: OPCION_MODULO_PORTAL });
                                }
                                else {
                                    if (item.Estado == ESTADO_ACTIVO) {
                                        $scope.tree.push({ id: item.Codigo, parent: "#", text: item.Nombre });
                                    }
                                    
                                }
                                //}
                            });

                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        }

        $scope.CargarMenu = function () {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Grupo: $scope.Grupo,
            }
            blockUI.delay = 1000;
            PermisoGrupoUsuariosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.tree.forEach(function (item) {
                                $scope.Auxtree.forEach(function (modulo) {
                                    if (item.id == modulo.Padre) {
                                        $scope.tree.push({ id: modulo.id, parent: modulo.Padre, text: modulo.text });
                                    }
                                });
                            });

                            var menu = response.data.Datos;
                            //console.log("menu: ",menu);
                            for (var i = 0; i < menu.length; i++) {
                                for (var j = 0; j < $scope.tree.length; j++) {
                                    if ($scope.tree[j].id == menu[i].Menu.Padre) {
                                        if (menu[i].Modulo.Codigo == 10 && menu[i].Menu.OpcionListado == 1) {
                                            $scope.tree.push({ id: menu[i].Menu.Codigo, parent: 1007, text: menu[i].Menu.Nombre });
                                        }
                                        else {
                                            $scope.tree.push({ id: menu[i].Menu.Codigo, parent: menu[i].Menu.Padre, text: menu[i].Menu.Nombre });
                                        }
                                    }
                                }
                            }
                            /*response.data.Datos.forEach(function (menu) {
                                //EG: Solo se adicionan las opciones de menú que ya cuenten con padre
                                //Con el fin de que no se quede el arbol buscando padres cuando no los hay
                                $scope.tree.forEach(function (item) {
                                    if (item.id == menu.Menu.Padre) {
                                        if (menu.Modulo.Codigo == 10 && menu.Menu.OpcionListado == 1) {
                                            $scope.tree.push({ id: menu.Menu.Codigo, parent: 1007, text: menu.Menu.Nombre });
                                        }
                                        else {
                                            $scope.tree.push({ id: menu.Menu.Codigo, parent: menu.Menu.Padre, text: menu.Menu.Nombre });
                                        }
                                    }
                                });

                            });*/
                            $('#jstree').jstree({
                                'core': {
                                    'data': $scope.tree
                                },
                                "plugins": ["search", "wholerow"]
                            });
                            $('#jstree')
                                // listen for event
                                .on('changed.jstree', function (e, data) {
                                    $scope.Nodo = { Codigo: data.node.id, Nombre: data.node.text, Padre: data.node.parent };
                                    if ($scope.Nodo.Padre != "#") {
                                        blockUI.start('Cargando ...');
                                        $timeout(function () {
                                            blockUI.message('Espere por favor ...');
                                        }, 100);
                                        if (data.node.id.length > 3) {
                                            showModal('modalModificarPermisos');
                                        } else if (data.node.text == "Listados") {
                                            showModal('modalModificarPermisos');
                                        }
                                        $scope.CargarPermisos();
                                        CargarPermisosEspecificos();
                                        blockUI.stop();
                                    }
                                })
                                // create the instance
                                .jstree();

                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';

                            //console.log("jstree: ", $scope.tree);
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        }

        $scope.CargarPermisos = function () {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //Menu: $scope.Nodo,
                Menu: { Codigo: $scope.Nodo.Codigo},
                Grupo: $scope.Grupo,
            }
            blockUI.delay = 1000;
            PermisoGrupoUsuariosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Habilitar == 1) {
                                    $scope.ModalHabilitar = true;
                                } else {
                                    $scope.ModalHabilitar = false;
                                }

                                if (item.Consultar == 1) {
                                    $scope.ModalConsultar = true;
                                } else {
                                    $scope.ModalConsultar = false;
                                }

                                if (item.Actualizar == 1) {
                                    $scope.ModalActualizar = true;
                                } else {
                                    $scope.ModalActualizar = false;
                                }

                                if (item.Crear == 1) {
                                    $scope.ModalCrear = true;
                                } else {
                                    $scope.ModalCrear = false;
                                }

                                if (item.EliminarAnular == 1) {
                                    $scope.ModalEliminar = true;
                                } else {
                                    $scope.ModalEliminar = false;
                                }

                                if (item.Imprimir == 1) {
                                    $scope.ModalImprimir = true;
                                } else {
                                    $scope.ModalImprimir = false;
                                }

                            });

                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        }

        function CargarPermisosEspecificos() {
            $scope.ListadoPermisos = [];
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Menu: { Codigo: 0, Padre: $scope.Nodo.Codigo },
                Grupo: $scope.Grupo,
            }
            blockUI.delay = 1000;
            PermisoGrupoUsuariosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoPermisos = [];
                            response.data.Datos.forEach(function (item) {
                                if (item.Permiso == 1) {
                                    if (item.Habilitar == 1) {
                                        item.Habilitado = true;
                                    } else {
                                        item.Habilitado = false;
                                    }
                                    $scope.ListadoPermisos.push(item)
                                }
                            });



                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        }


        /*------------------------------------------------------------------------------------Eliminar Almacen-----------------------------------------------------------------------*/
        $scope.EliminarGrupo = function (codigo, Nombre, CodigoAlterno) {
            $scope.AuxCodigo = codigo
            $scope.AuxNombre = Nombre
            $scope.CodigoAlterno = CodigoAlterno
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarGrupo');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.AuxCodigo,
            };

            GrupoUsuariosFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó el grupo ' + $scope.CodigoAlterno + ' - ' + $scope.AuxNombre);
                        closeModal('modalEliminarGrupo');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarGrupo');
                    $scope.ModalError = 'No se puede eliminar el grupo ' + $scope.AuxNombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        //----------------------------PERMISOS----------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.CerrarModal = function () {
            closeModal('modalEliminarGrupo');
            closeModal('modalMensajeEliminarGrupo');
        }

        $scope.MaskNumero = function () {
            $scope.Modelo.CodigoGrupo = MascaraNumero($scope.Modelo.CodigoGrupo)
        };

        /*Funcion que solicita los permisos */
        $scope.ModificarPermisos = function (Numero, Nombre) {
            $scope.Grupo = { Codigo: Numero, Nombre: Nombre };
            $scope.tree = [];
            $scope.CargarModulos();
            $scope.CargarMenu();

            showModal('modalPermisos');
            $scope.ListadoPermisos = [];




        };

        $scope.changedCB = function (e, data) {
            $scope.Nodo = { Codigo: data.node.id, Nombre: data.node.text, Padre: data.node.parent };
            if ($scope.Nodo.Padre != "#") {
                blockUI.start('Cargando ...');

                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);




                showModal('modalModificarPermisos');
                $scope.CargarPermisos();
                blockUI.stop();
            }



        };

        $scope.GuardarPermisos = function () {
            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            //$scope.Modelo.Menu = $scope.Nodo;
            $scope.Modelo.Menu = { Codigo: $scope.Nodo.Codigo };
            $scope.Modelo.Grupo = $scope.Grupo;

            if ($scope.ModalHabilitar == true) {
                $scope.Modelo.Habilitar = 1;
            }
            else {
                $scope.Modelo.Habilitar = 0;
            }

            if ($scope.ModalConsultar == true) {
                $scope.Modelo.Consultar = 1;
            }
            else {
                $scope.Modelo.Consultar = 0;
            }

            if ($scope.ModalActualizar == true) {
                $scope.Modelo.Actualizar = 1;
            }
            else {
                $scope.Modelo.Actualizar = 0;
            }

            if ($scope.ModalCrear == true) {
                $scope.Modelo.Crear = 1;
            }
            else {
                $scope.Modelo.Crear = 0;
            }

            if ($scope.ModalEliminar == true) {
                $scope.Modelo.EliminarAnular = 1;
            }
            else {
                $scope.Modelo.EliminarAnular = 0;
            }

            if ($scope.ModalImprimir == true) {
                $scope.Modelo.Imprimir = 1;
            }
            else {
                $scope.Modelo.Imprimir = 0;
            }


            PermisoGrupoUsuariosFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('Se guardó el permiso');
                            closeModal('modalModificarPermisos', 1);
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            $scope.ListadoPermisos.forEach(function (item) {
                if (item.Habilitado == true) {
                    item.Habilitar = 1;
                } else {
                    item.Habilitar = 0;
                }
                PermisoGrupoUsuariosFactory.Guardar(item).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            })
        };
    }]);
