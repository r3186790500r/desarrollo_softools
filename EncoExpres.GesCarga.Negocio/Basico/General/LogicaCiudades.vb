﻿Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Fachada.Basico.General
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Basico.General
    Public NotInheritable Class LogicaCiudades
        Inherits LogicaBase(Of Ciudades)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Ciudades)

        Sub New(persistencia As IPersistenciaBase(Of Ciudades))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Ciudades) As Respuesta(Of IEnumerable(Of Ciudades))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Ciudades))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Ciudades))(consulta)
        End Function

        Public Function ConsultarAlterno(filtro As Ciudades) As Respuesta(Of IEnumerable(Of Ciudades))
            Dim consulta = CType(_persistencia, PersistenciaCiudades).ConsultarAlterno(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Ciudades))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Ciudades))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Ciudades) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.CodigoCiudad = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Ciudades) As Respuesta(Of Ciudades)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Ciudades)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Ciudades)(consulta)
        End Function
        Public Function ConsultarCodigoDane(entidad As CodigoDaneCiudades) As Respuesta(Of IEnumerable(Of CodigoDaneCiudades))
            Dim consulta = CType(_persistencia, PersistenciaCiudades).ConsultarCodigoDane(entidad)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of CodigoDaneCiudades))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of CodigoDaneCiudades))(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As Ciudades) As Respuesta(Of Ciudades)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Ciudades) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Ciudades) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As Ciudades) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaCiudades).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function ConsultarOficinas(filtro As Ciudades) As Respuesta(Of IEnumerable(Of Oficinas))
            Dim consulta = CType(_persistencia, PersistenciaCiudades).ConsultarOficinas(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Oficinas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Oficinas))(consulta)
        End Function


        Public Function GuardarOficinas(entidad As Ciudades) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaCiudades).GuardarOficinas(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If
            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function

        Public Function ConsultarRegiones(filtro As Ciudades) As Respuesta(Of IEnumerable(Of RegionesPaises))
            Dim consulta = CType(_persistencia, PersistenciaCiudades).ConsultarRegiones(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of RegionesPaises))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RegionesPaises))(consulta)
        End Function


        Public Function GuardarRegiones(entidad As Ciudades) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaCiudades).GuardarRegiones(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If
            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function
    End Class

End Namespace