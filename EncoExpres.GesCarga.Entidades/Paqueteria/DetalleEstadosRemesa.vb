﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos

Namespace Paqueteria
    <JsonObject>
    Public NotInheritable Class DetalleEstadosRemesa
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            ID = Read(lector, "ID")
            NumeroRemesa = Read(lector, "ENRE_Numero")
            EstadoRemesa = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESPR_Codigo"), .Nombre = Read(lector, "Estado")}
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Actual"), .Nombre = Read(lector, "Oficina")}
            FechaEstado = Read(lector, "Fecha_Estado")
            FechaCrea = Read(lector, "Fecha_Crea")
            DocumentoPlanilla = Read(lector, "Num_Planilla")
            Vehiculo = Read(lector, "vehiculo")
            COD_vehiculo = Read(lector, "COD_vehiculo")
            Conductor = Read(lector, "NombreConductor")
            Aforador = Read(lector, "NombreAforador")
            UsuarioCreaPlanilla = Read(lector, "UsuarioCrea")
            UsuarioCreaGuia = Read(lector, "UsuarioCreaGuia")
            Anulado = Read(lector, "Anulado")
            EstadoPlanilla = Read(lector, "EstadoPLanilla")
            Observaciones = Read(lector, "Observaciones")
            NombreUsuarioCrea = Read(lector, "Usua_Crea")



        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        ''' 
        <JsonProperty>
        Public Property NombreUsuarioCrea As String

        <JsonProperty>
        Public Property ID As Integer
        <JsonProperty>
        Public Property EstadoPlanilla As Integer
        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property EstadoRemesa As ValorCatalogos
        <JsonProperty>
        Public Property FechaEstado As DateTime
        <JsonProperty>
        Public Property DocumentoPlanilla As String
        <JsonProperty>
        Public Property COD_vehiculo As String
        <JsonProperty>
        Public Property Vehiculo As String
        <JsonProperty>
        Public Property Aforador As String
        <JsonProperty>
        Public Property Conductor As String
        <JsonProperty>
        Public Property UsuarioCreaPlanilla As String
        <JsonProperty>
        Public Property UsuarioCreaGuia As String

    End Class
End Namespace
