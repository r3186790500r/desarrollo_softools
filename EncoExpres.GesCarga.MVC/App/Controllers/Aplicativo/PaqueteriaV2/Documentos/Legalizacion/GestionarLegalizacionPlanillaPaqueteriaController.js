﻿EncoExpresApp.controller("GestionarLegalizacionPlanillaPaqueteriaCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'blockUIConfig', 'LegalizacionGastosFactory',
    'PlanillaDespachosFactory', 'RutasFactory', 'VehiculosFactory', 'DocumentoComprobantesFactory', 'TercerosFactory', 'ConceptoGastosFactory',
    'SemirremolquesFactory', 'ValorCatalogosFactory', 'PeajesFactory', 'PlanillaPaqueteriaFactory', 'DocumentoCuentasFactory', 'RendimientoGalonCombustibleFactory', 'ValorCombustibleFactory',
    'DetalleNovedadesDespachosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, blockUIConfig, LegalizacionGastosFactory,
        PlanillaDespachosFactory, RutasFactory, VehiculosFactory, DocumentoComprobantesFactory, TercerosFactory, ConceptoGastosFactory,
        SemirremolquesFactory, ValorCatalogosFactory, PeajesFactory, PlanillaPaqueteriaFactory, DocumentoCuentasFactory, RendimientoGalonCombustibleFactory, ValorCombustibleFactory,
        DetalleNovedadesDespachosFactory) {
        $scope.Titulo = 'GESTIONAR LEGALIZACIÓN PLANILLA PAQUETERÍA';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Legalización Gastos' }, { Nombre: 'Gestionar' }];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.LEGALIZACION);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.IdentificacionConductor = "";
        $scope.HabilitarCantidadGalones = false;
        $scope.HabilitarTieneChip = false;
        var DocumentosCuentasAgregados = false;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Fecha: new Date(),
            Comprobantes: '',
            DetalleConductores: []
        };

        var OficinaUsuario = {
            Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
        };
        $scope.DeshabilitarControlPlanilla = false;
        $scope.DeshabilitarGuardar = false;
        $scope.ListadoEstados = [
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR },
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO }
        ];
        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_BORRADOR);

        $scope.ListaPlanillaConceptos = [];
        $scope.ListadoConceptos = [];
        $scope.ListaNovedades = [];
        $scope.TotalCostoNovedades = 0;
        //-------------------------------------Carga De Informacion Necesaria para listas y datos--------------------------------//
        //------- AutoCompletes -------//
        $scope.ListadoConductores = [];
        //--Conductores
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true });
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores);
                }
            }
            return $scope.ListadoConductores;
        };
        $scope.ListadoProveedores = [];
        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR, ValorAutocomplete: value, Sync: true });
                    $scope.ListadoProveedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedores);
                }
            }
            return $scope.ListadoProveedores;
        };
        //------- AutoCompletes -------//
        $scope.InitLoad = function () {
            var ListacomboConceptos = ConceptoGastosFactory.Consultar({ Estado: 1, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true });
            if (ListacomboConceptos.ProcesoExitoso == true) {
                if (ListacomboConceptos.Datos.length > 0) {
                    $scope.ListadoConceptos = ListacomboConceptos.Datos;
                }
            }
            if ($routeParams.Numero > 0) {
                $scope.ModeloNumero = $routeParams.Numero;
                Obtener();
            }
            else {
                $scope.ModeloNumero = 0;
                $scope.ModeloOficina = OficinaUsuario.Nombre;
                AsignarDetallePlanillaConceptos();
            }
        };

        function AsignarDetallePlanillaConceptos() {
            try {
                if ($scope.Modelo.DetalleLegalizacionGastos != undefined && $scope.Modelo.DetalleLegalizacionGastos != null) {
                    for (var i = 0; i < $scope.Modelo.DetalleLegalizacionGastos.length; i++) {
                        // $scope.Modelo.DetalleLegalizacionGastos[i].Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + $scope.Modelo.DetalleLegalizacionGastos[i].Concepto.Codigo);
                        $scope.Modelo.DetalleLegalizacionGastos[i].Concepto = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.DetalleLegalizacionGastos[i].Concepto.Codigo, Sync: true }).Datos


                        var ResponsePlanilla = PlanillaPaqueteriaFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: $scope.Modelo.DetalleLegalizacionGastos[i].Planilla.Numero, Sync: true }).Datos;
                        $scope.Modelo.DetalleLegalizacionGastos[i].Planilla.ValorAnticipo = ResponsePlanilla.Planilla.ValorAnticipo;
                    }
                    $scope.ListaPlanillaConceptos = AgruparListados($scope.Modelo.DetalleLegalizacionGastos, 'Planilla');
                    $scope.AsignarFechas();
                }

                if ($scope.Modelo.GastosCobustible != undefined && $scope.Modelo.GastosCobustible != null &&
                    $scope.ListaPlanillaConceptos != undefined && $scope.ListaPlanillaConceptos != null) {
                    for (var i = 0; i < $scope.Modelo.GastosCobustible.length; i++) {
                        for (var j = 0; j < $scope.ListaPlanillaConceptos.length; j++) {
                            if ($scope.ListaPlanillaConceptos[j].Planilla.Codigo == $scope.Modelo.GastosCobustible[i].Planilla.Codigo) {
                                $scope.ListaPlanillaConceptos[j].Planilla.Km_Ruta = MascaraValores($scope.Modelo.GastosCobustible[i].Km_Ruta)
                                $scope.ListaPlanillaConceptos[j].Planilla.Km_Galon = MascaraValores($scope.Modelo.GastosCobustible[i].Km_Galon)
                                $scope.ListaPlanillaConceptos[j].Planilla.Gal_Estimados = MascaraValores($scope.Modelo.GastosCobustible[i].Gal_Estimados)
                                $scope.ListaPlanillaConceptos[j].Planilla.Km_Inicial = MascaraValores($scope.Modelo.GastosCobustible[i].Km_Inicial)
                                $scope.ListaPlanillaConceptos[j].Planilla.Km_Final = MascaraValores($scope.Modelo.GastosCobustible[i].Km_Final)
                                $scope.ListaPlanillaConceptos[j].Planilla.Total_Km = MascaraValores($scope.Modelo.GastosCobustible[i].Total_Km)
                                $scope.ListaPlanillaConceptos[j].Planilla.Galones_Autorizados = MascaraValores($scope.Modelo.GastosCobustible[i].Galones_Autorizados)
                                $scope.ListaPlanillaConceptos[j].Planilla.Horas_Inicio = MascaraValores($scope.Modelo.GastosCobustible[i].Horas_Inicio)
                                $scope.ListaPlanillaConceptos[j].Planilla.Horas_Fin = MascaraValores($scope.Modelo.GastosCobustible[i].Horas_Fin)
                                $scope.ListaPlanillaConceptos[j].Planilla.Horas_Termo = MascaraValores($scope.Modelo.GastosCobustible[i].Horas_Termo)
                                $scope.ListaPlanillaConceptos[j].Planilla.Horas_Galon_Termo = MascaraValores($scope.Modelo.GastosCobustible[i].Horas_Galon_Termo)
                                $scope.ListaPlanillaConceptos[j].Planilla.Galon_Estimado_Termo = MascaraValores($scope.Modelo.GastosCobustible[i].Galon_Estimado_Termo)
                                $scope.ListaPlanillaConceptos[j].Planilla.Total_Galones = MascaraValores($scope.Modelo.GastosCobustible[i].Total_Galones)

                            }
                        }
                    }
                }
            }
            catch (e) {
                console.log("Error en AsignarDetallePlanillaConceptos : " + e);
            }
        }

        function Obtener() {
            TotalBaseImpuestos = 0;
            TotalValorImpuestos = 0;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ModeloNumero
            };
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Obteniendo documento...");
            $timeout(function () { blockUI.message("Obteniendo documento..."); }, 100);
            LegalizacionGastosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.Fecha = new Date(response.data.Datos.Fecha);
                        OficinaUsuario = response.data.Datos.Oficina
                        $scope.ModeloOficina = response.data.Datos.Oficina.Nombre;
                        $scope.Modelo.ValorGastos = MascaraValores($scope.Modelo.ValorGastos)
                        $scope.Modelo.SaldoEmpresa = MascaraValores($scope.Modelo.SaldoEmpresa)
                        $scope.Modelo.SaldoConductor = MascaraValores($scope.Modelo.SaldoConductor)
                        $scope.Modelo.ValorAnticipos = MascaraValores($scope.Modelo.ValorAnticipos)
                        $scope.Modelo.TotalGalones = MascaraValores($scope.Modelo.TotalGalones)
                        $scope.Modelo.ValorReanticipos = MascaraValores($scope.Modelo.ValorReanticipos)
                        $scope.Modelo.TotalGalonesAutorizados = MascaraValores($scope.Modelo.TotalGalonesAutorizados)
                        $scope.Modelo.ValorTotalCombustible = MascaraValores($scope.Modelo.ValorTotalCombustible)
                        $scope.Modelo.ValorPromedioGalon = MascaraValores($scope.Modelo.ValorPromedioGalon)

                        // $scope.ObtenerConductor(true)



                        $scope.IdentificacionConductor = $scope.Modelo.Conductor.NumeroIdentificacion;

                        //if ($scope.IdentificacionConductor !== undefined && $scope.IdentificacionConductor !== '' && !isNaN($scope.IdentificacionConductor)) {
                        //    TercerosFactory.Consultar({
                        //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        //        NumeroIdentificacion: $scope.IdentificacionConductor
                        //    }).
                        //        then(function (response) {
                        //            if (response.data.ProcesoExitoso === true) {
                        //                if (response.data.Datos.length > 0) {

                        //                    $scope.Modelo.Conductor = response.data.Datos[0];
                        //                    ConsultarPlanillasConductor(false);
                        //                }
                        //                else {
                        //                    ShowError('No se encontro un conductor asociado a la identifiación ingresada ')
                        //                }
                        //            }
                        //            else {
                        //                ShowError('No se logro consultar el tercero');
                        //            }
                        //        }, function (response) {
                        //            ShowError('No se logro consultar el tercero  Por favor contacte el administrador del sistema.');
                        //        });
                        //}



                        AsignarDetallePlanillaConceptos();
                        try {
                            for (var i = 0; i < $scope.Modelo.DetalleLegalizacionGastos.length; i++) {
                                try {
                                    $scope.Modelo.DetalleLegalizacionGastos[i].Proveedor = $scope.CargarTercero($scope.Modelo.DetalleLegalizacionGastos[i].Proveedor.Codigo);



                                } catch (e) {

                                }
                            }

                            try {
                                for (var i = 0; i < $scope.ListaPlanillaConceptos.length; i++) {
                                    try {
                                        var FiltroComprobantes = {
                                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            Estado: 1,
                                            //CodigoTercero: $scope.Modelo.Conductor.Codigo,
                                            NumeroDocumentoOrigen: $scope.ListaPlanillaConceptos[i].Planilla.NumeroDocumento,
                                            CodigoLegalizacionGastos: 1,
                                            Sync: true
                                        }
                                        var responseCO = DocumentoComprobantesFactory.Consultar(FiltroComprobantes)

                                        if (responseCO.Datos.length > 0) {
                                            for (var c = 0; c < responseCO.Datos.length; c++) {
                                                if (responseCO.Datos[c].DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA || responseCO.Datos[c].DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_SOBREANTICIPO_PAQUETERIA || responseCO.Datos[c].DocumentoOrigen.Codigo == 2609) {
                                                    $scope.Comprobantes.push(responseCO.Datos[c])
                                                }

                                            }
                                        }

                                    } catch (e) {

                                    }
                                }
                            } catch (e) {

                            }

                            $scope.ListaPlanillaConceptos = AgruparListados($scope.Modelo.DetalleLegalizacionGastos, 'Planilla');
                            try {
                                $scope.AsignarFechas()

                            } catch (e) {

                            }

                            for (var i = 0; i < $scope.ListaPlanillaConceptos.length; i++) {
                                for (var j = 0; j < $scope.ListaPlanillaConceptos[i].Data.length; j++) {
                                    var item = $scope.ListaPlanillaConceptos[i].Data[j];
                                    if (item.Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_PEAJES || item.Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_SALDO_A_FAVOR_CONDUCTOR || item.Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_SALDO_A_FAVOR_EMPRESA) {

                                        item.Concepto = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Concepto.Codigo, Sync: true }).Datos;

                                    } else {
                                        item.Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + item.Concepto.Codigo);
                                    }
                                    //item.Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + item.Concepto.Codigo);
                                    item.Valor = MascaraValores(item.Valor);
                                    item.ValorGalones = MascaraValores(item.ValorGalones);
                                    item.CantidadGalones = MascaraValores(item.CantidadGalones);
                                }
                            }


                        } catch (e) {
                        }
                        try {
                            for (var i = 0; i < $scope.Modelo.GastosCobustible.length; i++) {
                                for (var j = 0; j < $scope.ListaPlanillaConceptos.length; j++) {
                                    if ($scope.ListaPlanillaConceptos[j].Planilla.Codigo == $scope.Modelo.GastosCobustible[i].Planilla.Codigo) {
                                        $scope.ListaPlanillaConceptos[j].Planilla.Km_Ruta = MascaraValores($scope.Modelo.GastosCobustible[i].Km_Ruta)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Km_Galon = MascaraValores($scope.Modelo.GastosCobustible[i].Km_Galon)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Gal_Estimados = MascaraValores($scope.Modelo.GastosCobustible[i].Gal_Estimados)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Km_Inicial = MascaraValores($scope.Modelo.GastosCobustible[i].Km_Inicial)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Km_Final = MascaraValores($scope.Modelo.GastosCobustible[i].Km_Final)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Total_Km = MascaraValores($scope.Modelo.GastosCobustible[i].Total_Km)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Galones_Autorizados = MascaraValores($scope.Modelo.GastosCobustible[i].Galones_Autorizados)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Horas_Inicio = MascaraValores($scope.Modelo.GastosCobustible[i].Horas_Inicio)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Horas_Fin = MascaraValores($scope.Modelo.GastosCobustible[i].Horas_Fin)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Horas_Termo = MascaraValores($scope.Modelo.GastosCobustible[i].Horas_Termo)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Horas_Galon_Termo = MascaraValores($scope.Modelo.GastosCobustible[i].Horas_Galon_Termo)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Galon_Estimado_Termo = MascaraValores($scope.Modelo.GastosCobustible[i].Galon_Estimado_Termo)
                                        $scope.ListaPlanillaConceptos[j].Planilla.Total_Galones = MascaraValores($scope.Modelo.GastosCobustible[i].Total_Galones)
                                    }
                                }
                            }
                        } catch (e) {

                        }

                        if (response.data.Datos.Anulado == ESTADO_ANULADO) {
                            $scope.ListadoEstados.push({ Nombre: 'ANULADO', Codigo: 2 });
                            $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + 2);
                            $scope.DeshabilitarGuardar = true;
                        }
                        else {
                            $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.DeshabilitarGuardar = true;
                            }
                            else {
                                ConsultarPlanillasConductor(true);
                            }
                        }

                        $timeout(function () { $scope.Calcular(); }, 500);
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 500);
                    }
                    else {
                        ShowError('No se logro consultar la legalizacion. ' + response.data.MensajeOperacion);
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; document.location.href = '#!ConsultarLegalizacionPlanillaPaqueteria'; }, 500);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; document.location.href = '#!ConsultarLegalizacionPlanillaPaqueteria'; }, 500);
                });
            blockUI.stop();
        };

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar')
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            window.scrollTo(top, top);
            if (DatosRequeridos()) {

                $scope.Modelo.Oficina = OficinaUsuario
                $scope.Modelo.Estado = $scope.ModeloEstado.Codigo
                $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                $scope.Modelo.DetalleLegalizacionGastos = []
                $scope.Modelo.GastosCobustible = []
                $scope.Modelo.TipoDocumento = CODIGO_CATALOGO_CATEGORIAS_VIAJES_CONDUCTOR
                for (var i = 0; i < $scope.ListaPlanillaConceptos.length; i++) {
                    $scope.Modelo.GastosCobustible.push({
                        Planilla: { Numero: $scope.ListaPlanillaConceptos[i].Planilla.Numero },
                        Km_Ruta: $scope.ListaPlanillaConceptos[i].Planilla.Km_Ruta,
                        Km_Galon: $scope.ListaPlanillaConceptos[i].Planilla.Km_Galon,
                        Gal_Estimados: $scope.ListaPlanillaConceptos[i].Planilla.Gal_Estimados,
                        Km_Inicial: $scope.ListaPlanillaConceptos[i].Planilla.Km_Inicial,
                        Km_Final: $scope.ListaPlanillaConceptos[i].Planilla.Km_Final,
                        Total_Km: $scope.ListaPlanillaConceptos[i].Planilla.Total_Km,
                        Galones_Autorizados: $scope.ListaPlanillaConceptos[i].Planilla.Galones_Autorizados,
                        Horas_Inicio: $scope.ListaPlanillaConceptos[i].Planilla.Horas_Inicio,
                        Horas_Fin: $scope.ListaPlanillaConceptos[i].Planilla.Horas_Fin,
                        Horas_Termo: $scope.ListaPlanillaConceptos[i].Planilla.Horas_Termo,
                        Horas_Galon_Termo: $scope.ListaPlanillaConceptos[i].Planilla.Horas_Galon_Termo,
                        Galon_Estimado_Termo: $scope.ListaPlanillaConceptos[i].Planilla.Galon_Estimado_Termo,
                        Total_Galones: $scope.ListaPlanillaConceptos[i].Planilla.Total_Galones,
                    })
                    for (var j = 0; j < $scope.ListaPlanillaConceptos[i].Data.length; j++) {
                        var item = $scope.ListaPlanillaConceptos[i].Data[j]
                        $scope.Modelo.DetalleLegalizacionGastos.push({
                            Planilla: { Numero: $scope.ListaPlanillaConceptos[i].Planilla.Numero },
                            Concepto: { Codigo: item.Concepto.Codigo },
                            Observaciones: item.Observaciones,
                            Valor: parseInt(RevertirMV(item.Valor)),
                            Proveedor: item.Proveedor,
                            NumeroFactura: item.NumeroFactura,
                            FechaFactura: item.FechaFactura,
                            CantidadGalones: item.CantidadGalones,
                            TieneChip: item.TieneChip,
                            CantidadGalones: RevertirMV(item.CantidadGalones),
                            ValorGalones: RevertirMV(item.ValorGalones),
                            NombreEstacion: item.NombreEstacion,
                            DocumentoCuenta: item.DocumentoCuenta == undefined ? 0 : item.DocumentoCuenta,
                            Conductor: { Codigo: $scope.Modelo.Conductor.Codigo }
                        })
                    }
                }
                if ($scope.Sesion.UsuarioAutenticado.ManejoCuentasPorCobrarPagarLegalizacionesGastos) {
                    if (parseInt($scope.Modelo.SaldoConductor) > 0) {

                        $scope.CuentaPorPagar = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                            CodigoAlterno: '',
                            Fecha: new Date(),
                            Tercero: { Codigo: $scope.Modelo.Conductor == undefined ? 0 : $scope.Modelo.Conductor.Codigo },
                            DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_PAQUETERIA },
                            FechaDocumento: $scope.Modelo.Fecha,
                            FechaVenceDocumento: $scope.Modelo.Fecha,
                            Numeracion: '',
                            CuentaPuc: { Codigo: CERO },
                            ValorTotal: $scope.Modelo.SaldoConductor,
                            Abono: 0,
                            Saldo: $scope.Modelo.SaldoConductor,
                            FechaCancelacionPago: $scope.Modelo.Fecha,
                            Aprobado: 1,
                            UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            FechaAprobo: $scope.Modelo.Fecha,
                            Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                            Vehiculo: { Codigo: 0 },
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        }
                    }

                    if (parseInt($scope.Modelo.SaldoEmpresa) > 0) {

                       $scope.CuentaPorCobrar = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                            CodigoAlterno: '',
                            Fecha: new Date(),
                            Tercero: { Codigo: $scope.Modelo.Conductor == undefined ? 0 : $scope.Modelo.Conductor.Codigo },
                            DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_PAQUETERIA },
                            FechaDocumento: $scope.Modelo.Fecha,
                            FechaVenceDocumento: $scope.Modelo.Fecha,
                            Numeracion: '',
                            CuentaPuc: { Codigo: CERO },
                            ValorTotal: $scope.Modelo.SaldoEmpresa,
                            Abono: 0,
                            Saldo: $scope.Modelo.SaldoEmpresa,
                            FechaCancelacionPago: $scope.Modelo.Fecha,
                            Aprobado: 1,
                            UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            FechaAprobo: $scope.Modelo.Fecha,
                            Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                            Vehiculo: { Codigo: 0 },
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                       }
                        
                    }

                    var cuentas = {
                        CuentaPorPagar: $scope.CuentaPorPagar,
                        CuentaPorCobrar: $scope.CuentaPorCobrar
                    }

                    $scope.Modelo.DetalleConductores.push(cuentas)

                }

                $scope.Modelo.ParametrosCalculos = {
                    ListaPlanillaConceptos: $scope.ListaPlanillaConceptos.map(a => {
                        return {
                            Planilla: {
                                ValorAnticipo: RevertirMV(a.Planilla.ValorAnticipo),
                                Total_Galones: RevertirMV(a.Planilla.Total_Galones),
                                Total_Km: RevertirMV(a.Planilla.Total_Km),
                                Km_Inicial: RevertirMV(a.Planilla.Km_Inicial),
                                Km_Final: RevertirMV(a.Planilla.Km_Final),
                                Gal_Estimados: RevertirMV(a.Planilla.Gal_Estimados),
                                Km_Galon: RevertirMV(a.Planilla.Km_Galon),
                                Galon_Estimado_Termo: RevertirMV(a.Planilla.Galon_Estimado_Termo),
                                Horas_Termo: RevertirMV(a.Planilla.Horas_Termo),
                                Galones_Adicionales_Termo: RevertirMV(a.Planilla.Galones_Adicionales_Termo),
                                Horas_Fin: RevertirMV(a.Planilla.Horas_Fin),
                                Horas_Inicio: RevertirMV(a.Planilla.Horas_Inicio),
                                Horas_Galon_Termo: RevertirMV(a.Planilla.Horas_Galon_Termo),
                                Horas_Adicionales_Termo: RevertirMV(a.Planilla.Horas_Adicionales_Termo)
                            },
                            Data: a.Data.map(b => {
                                return {
                                    Valor: RevertirMV(b.Valor),
                                    TieneChip: b.TieneChip,
                                    ConceptoNombre: b.Concepto.Nombre,
                                    CantidadGalones: RevertirMV(b.CantidadGalones),
                                    ValorGalones: RevertirMV(b.ValorGalones)
                                };
                            })
                        };
                    }),
                    Comprobantes: $scope.Comprobantes.map(a => {
                        return {
                            DocumentoOrigenCodigo: a.DocumentoOrigen.Codigo,
                            ValorPagoTotal: RevertirMV(a.ValorPagoTotal)
                        }
                    })
                }

                $scope.Modelo.Comprobantes = $scope.Comprobantes

                LegalizacionGastosFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos === -1) {
                                ShowError('La planilla ya se encuentra liquidada');
                            } else {
                                if (response.data.Datos > 0) {
                                    if ($scope.ModeloNumero === 0) {
                                        ShowSuccess('Se guardó la legalización de gastos No. ' + response.data.Datos);
                                    }
                                    else {
                                        ShowSuccess('Se modificó la la legalización de gastos No. ' + response.data.Datos);
                                    }
                                    location.href = '#!ConsultarLegalizacionPlanillaPaqueteria/' + response.data.Datos;
                                } else {
                                    ShowError(response.data.MensajeOperacion);
                                }
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var Continuar = true;
            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == '' || $scope.Modelo.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                Continuar = false
            }
            if ($scope.Modelo.Conductor == undefined || $scope.Modelo.Conductor == '' || $scope.Modelo.Conductor == null) {
                $scope.MensajesError.push('Debe ingresar el conductor');
                Continuar = false
            }
            if ($scope.ListaPlanillaConceptos == undefined || $scope.ListaPlanillaConceptos == [] || $scope.ListaPlanillaConceptos == null || $scope.ListaPlanillaConceptos.length == 0) {
                $scope.MensajesError.push('Debe adicionar al menos una planilla');
                Continuar = false
            } else {
                var ConceptoNoasociado = false
                var faltavalor = false
                var faltaconcepto = false
                var faltaproveedor = false
                for (var i = 0; i < $scope.ListaPlanillaConceptos.length; i++) {
                    if ($scope.ListaPlanillaConceptos[i].Data.length > 0) {
                        for (var j = 0; j < $scope.ListaPlanillaConceptos[i].Data.length; j++) {
                            var item = $scope.ListaPlanillaConceptos[i].Data[j]
                            if (item.Valor == undefined || item.Valor == '' || item.Valor == 0 || item.Valor == '0') {
                                faltavalor = true
                            }
                            if (item.Concepto == undefined || item.Concepto == '' || item.Concepto == null) {
                                faltaconcepto = true
                            }
                            if (item.Proveedor == undefined || item.Proveedor == '' || item.Proveedor == null) {
                                faltaproveedor = true
                            }
                        }
                    }
                    else {
                        ConceptoNoasociado = true
                    }
                    if ($scope.Sesion.UsuarioAutenticado.ManejoGalonCombustible) {
                        if ($scope.ListaPlanillaConceptos[i].Planilla.Km_Ruta == 0 || $scope.ListaPlanillaConceptos[i].Planilla.Km_Ruta == undefined || $scope.ListaPlanillaConceptos[i].Planilla.Km_Ruta == '') {
                            $scope.MensajesError.push('Debe ingresar los Km de la Ruta de la planilla ' + $scope.ListaPlanillaConceptos[i].Planilla.NumeroDocumento);
                            Continuar = false
                        }
                        if ($scope.ListaPlanillaConceptos[i].Planilla.Km_Galon == 0 || $scope.ListaPlanillaConceptos[i].Planilla.Km_Galon == undefined || $scope.ListaPlanillaConceptos[i].Planilla.Km_Galon == '') {
                            $scope.MensajesError.push('Debe ingresar los Km por galón de la planilla ' + $scope.ListaPlanillaConceptos[i].Planilla.NumeroDocumento);
                            Continuar = false
                        }
                        if ($scope.ListaPlanillaConceptos[i].Planilla.Km_Inicial == 0 || $scope.ListaPlanillaConceptos[i].Planilla.Km_Inicial == undefined || $scope.ListaPlanillaConceptos[i].Planilla.Km_Inicial == '') {
                            $scope.MensajesError.push('Debe ingresar el Km inicial de la planilla ' + $scope.ListaPlanillaConceptos[i].Planilla.NumeroDocumento);
                            Continuar = false
                        }
                        if ($scope.ListaPlanillaConceptos[i].Planilla.Km_Inicial > 0 && ($scope.ListaPlanillaConceptos[i].Planilla.Km_Final == 0 || $scope.ListaPlanillaConceptos[i].Planilla.Km_Final == undefined || $scope.ListaPlanillaConceptos[i].Planilla.Km_Final == '')) {
                            $scope.MensajesError.push('Debe ingresar el Km final de la planilla ' + $scope.ListaPlanillaConceptos[i].Planilla.NumeroDocumento);
                            Continuar = false
                        }
                        if ($scope.ListaPlanillaConceptos[i].Planilla.Horas_Inicio > 0 || $scope.ListaPlanillaConceptos[i].Planilla.Horas_Fin > 0 || $scope.ListaPlanillaConceptos[i].Planilla.Horas_Galon_Termo > 0) {
                            if ($scope.ListaPlanillaConceptos[i].Planilla.Horas_Fin == 0 || $scope.ListaPlanillaConceptos[i].Planilla.Horas_Fin == undefined || $scope.ListaPlanillaConceptos[i].Planilla.Horas_Fin == '') {
                                $scope.MensajesError.push('Debe ingresar las horas fin del termo de la planilla ' + $scope.ListaPlanillaConceptos[i].Planilla.NumeroDocumento);
                                Continuar = false
                            }
                            if ($scope.ListaPlanillaConceptos[i].Planilla.Horas_Fin == 0 || $scope.ListaPlanillaConceptos[i].Planilla.Horas_Galon_Termo == undefined || $scope.ListaPlanillaConceptos[i].Planilla.Horas_Galon_Termo == '') {
                                $scope.MensajesError.push('Debe ingresar las horas por galón termo de la planilla ' + $scope.ListaPlanillaConceptos[i].Planilla.NumeroDocumento);
                                Continuar = false
                            }
                        }
                    }

                }
                if (ConceptoNoasociado) {
                    $scope.MensajesError.push('Debe asociar al menos un concepto a cada planilla asociada');
                    Continuar = false
                }
                if (faltaconcepto) {
                    $scope.MensajesError.push('Debe seleccionar los conceptos de todos los detalle asociados');
                    Continuar = false
                }
                if (faltavalor) {
                    $scope.MensajesError.push('Debe ingresar todos los valores de los conceptos asociados');
                    Continuar = false
                }
                if (faltaproveedor) {
                    $scope.MensajesError.push('Debe ingresar todos los proveedores de los conceptos asociados');
                    Continuar = false
                }
            }
            return Continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarLegalizacionPlanillaPaqueteria/' + $scope.Modelo.NumeroDocumento;
        };
        $scope.Calcular = function () {
            $scope.Modelo.ValorGastos = 0
            $scope.Modelo.ValorAnticipos = 0
            $scope.Modelo.ValorReanticipos = 0
            $scope.Modelo.SaldoConductor = 0
            $scope.Modelo.SaldoEmpresa = 0
            $scope.Modelo.TotalGalones = 0
            $scope.Modelo.ValorTotalCombustible = 0
            $scope.Modelo.ValorPromedioGalon = 0
            $scope.Modelo.TotalGalonesAutorizados = 0
            $scope.TotalGalonesEstimados = 0
            $scope.Modelo.TotalPeajes = 0
            // Funcion que recalcula valores pertinentes monetarios para mostrarlos en el front
            // Utima modificacion por: Felipe Pieschacon
            // Fecha ultimos cambios: 2022 02 17
            var resVal
            resVal = LegalizacionGastosFactory.CalcularValores({
                ParametrosCalculos: {
                    ListaPlanillaConceptos: $scope.ListaPlanillaConceptos.map(a => {
                        return {
                            Planilla: {
                                ValorAnticipo: RevertirMV(a.Planilla.ValorAnticipo),
                                Total_Galones: RevertirMV(a.Planilla.Total_Galones),
                                Total_Km: RevertirMV(a.Planilla.Total_Km),
                                Km_Inicial: RevertirMV(a.Planilla.Km_Inicial),
                                Km_Final: RevertirMV(a.Planilla.Km_Final),
                                Gal_Estimados: RevertirMV(a.Planilla.Gal_Estimados),
                                Km_Galon: RevertirMV(a.Planilla.Km_Galon),
                                Galon_Estimado_Termo: RevertirMV(a.Planilla.Galon_Estimado_Termo),
                                Horas_Termo: RevertirMV(a.Planilla.Horas_Termo),
                                Galones_Adicionales_Termo: RevertirMV(a.Planilla.Galones_Adicionales_Termo),
                                Horas_Fin: RevertirMV(a.Planilla.Horas_Fin),
                                Horas_Inicio: RevertirMV(a.Planilla.Horas_Inicio),
                                Horas_Galon_Termo: RevertirMV(a.Planilla.Horas_Galon_Termo),
                                Horas_Adicionales_Termo: RevertirMV(a.Planilla.Horas_Adicionales_Termo)
                            },
                            Data: a.Data.map(b => {
                                return {
                                    Valor: RevertirMV(b.Valor),
                                    TieneChip: b.TieneChip,
                                    ConceptoNombre: b.Concepto.Nombre,
                                    CantidadGalones: RevertirMV(b.CantidadGalones),
                                    ValorGalones: RevertirMV(b.ValorGalones)
                                };
                            })
                        };
                    }),
                    Comprobantes: $scope.Comprobantes.map(a => {
                        return {
                            DocumentoOrigenCodigo: a.DocumentoOrigen.Codigo,
                            ValorPagoTotal: RevertirMV(a.ValorPagoTotal)
                        }
                    })
                },
                Sync: true
            });

            if (resVal.ProcesoExitoso) {
                $scope.Modelo.ValorPromedioGalon = MascaraValores(resVal.Datos.ValorPromedioGalon);
                $scope.Modelo.TotalGalonesAutorizados = MascaraValores(resVal.Datos.TotalGalonesAutorizados);
                $scope.Modelo.SaldoEmpresa = MascaraValores(resVal.Datos.SaldoEmpresa);
                $scope.Modelo.SaldoConductor = MascaraValores(resVal.Datos.SaldoConductor);
                $scope.Modelo.ValorGastos = MascaraValores(resVal.Datos.ValorGastos);
                $scope.Modelo.ValorAnticipos = MascaraValores(resVal.Datos.ValorAnticipos);
                $scope.Modelo.ValorReanticipos = MascaraValores(resVal.Datos.ValorReanticipos);
                $scope.Modelo.TotalGalones = MascaraValores(resVal.Datos.TotalGalones);
                $scope.Modelo.TotalGalonesAutorizados = MascaraValores(resVal.Datos.TotalGalonesAutorizados);
                $scope.Modelo.TotalPeajes = MascaraValores(resVal.Datos.TotalPeajes);
                $scope.Modelo.ValorPromedioGalon = MascaraValores(resVal.Datos.ValorPromedioGalon);
                $scope.Modelo.ValorTotalCombustible = MascaraValores(resVal.Datos.ValorTotalCombustible);
            }
        }

        $scope.AsignarIdentificacion = function (Conductor) {
            if (Conductor.NumeroIdentificacion != 0 && Conductor.NumeroIdentificacion != null && Conductor.NumeroIdentificacion != undefined) {
                $scope.IdentificacionConductor = Conductor.NumeroIdentificacion;
                ConsultarPlanillasConductor(false);
            }
        };
        $scope.ObtenerConductor = function (obtener) {
            if ($scope.IdentificacionConductor !== undefined && $scope.IdentificacionConductor !== '' && !isNaN($scope.IdentificacionConductor)) {
                TercerosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.IdentificacionConductor
                }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {

                                $scope.Modelo.Conductor = response.data.Datos[0];
                                ConsultarPlanillasConductor(false);
                            }
                            else {
                                ShowError('No se encontro un conductor asociado a la identifiación ingresada ')
                            }
                        }
                        else {
                            ShowError('No se logro consultar el tercero');
                        }
                    }, function (response) {
                        ShowError('No se logro consultar el tercero  Por favor contacte el administrador del sistema.');
                    });
            }
        }
        $scope.CalcularTermo = function (item) {
            try {
                item.Planilla.Horas_Termo = 0
                item.Planilla.Galones_Adicionales_Termo = 0
                item.Planilla.Galon_Estimado_Termo = 0
                item.Planilla.Galones_Adicionales_Termo = 0
                if (parseFloat(MascaraDecimales(item.Planilla.Horas_Fin)) >= 0 && parseFloat(MascaraDecimales(item.Planilla.Horas_Inicio)) >= 0) {
                    if (parseFloat(MascaraDecimales(item.Planilla.Horas_Fin)) > parseFloat(MascaraDecimales(item.Planilla.Horas_Inicio))) {
                        item.Planilla.Horas_Termo = MascaraValores(parseFloat(MascaraDecimales(item.Planilla.Horas_Fin)) - parseFloat(MascaraDecimales(item.Planilla.Horas_Inicio)))
                        if (parseFloat(MascaraDecimales(item.Planilla.Horas_Galon_Termo)) > 0) {
                            item.Planilla.Galon_Estimado_Termo = MascaraValores(parseFloat(parseFloat(MascaraDecimales(item.Planilla.Horas_Termo)) / parseFloat(MascaraDecimales(item.Planilla.Horas_Galon_Termo))).toFixed(2))
                            if (parseFloat(MascaraDecimales(item.Planilla.Horas_Adicionales_Termo)) > 0) {
                                item.Planilla.Galones_Adicionales_Termo = MascaraValores(parseFloat(parseFloat(MascaraDecimales(item.Planilla.Horas_Adicionales_Termo)) / parseFloat(MascaraDecimales(item.Planilla.Horas_Galon_Termo))).toFixed(2))
                            }
                        }
                    } else {
                        ShowError('Las horas fin deben ser mayor a las horas inicio')
                        item.Planilla.Horas_Fin = undefined
                    }
                }

            } catch (e) {

            }

        }

        function ConsultarPlanillasConductor(Obtener) {
            $scope.obtener = Obtener
            $scope.ListadoPlanillaDespacho = [];
            $scope.ListadoManifiesto = [];
            if (!Obtener) {
                $scope.Modelo.ValorGastos = 0
                $scope.Modelo.ValorAnticipos = 0
                $scope.Modelo.ValorReanticipos = 0
                $scope.Modelo.SaldoConductor = 0
                $scope.Modelo.SaldoEmpresa = 0
            }
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Conductor: { Codigo: $scope.Modelo.Conductor.Codigo },
                AplicaConsultaMaster: 1,
                ConsultaLegalización: 1,
                Estado: { Codigo: 1 },
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                ConsultarLegalizacionGastosPaqueteria: true,
                Sync: true
            };
            var response = PlanillaDespachosFactory.Consultar(filtros);
            //var response = PlanillaPaqueteriaFactory.Consultar(filtros);
            if (response.ProcesoExitoso == true) {
                if (response.Datos.length > 0) {
                    $scope.ListadoPlanillaDespacho = response.Datos;
                    var ResponsePlanilla = {};
                    $scope.ListadoPlanillaDespacho.forEach(item => {
                        ResponsePlanilla = PlanillaPaqueteriaFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.Numero, Sync: true }).Datos;
                        item.ValorAnticipo = ResponsePlanilla.Planilla.ValorAnticipo;
                    });
                    for (var i = 0; i < $scope.ListadoPlanillaDespacho.length; i++) {
                        if ($scope.ListadoPlanillaDespacho[i].Manifiesto != '' && $scope.ListadoPlanillaDespacho[i].Manifiesto != null && $scope.ListadoPlanillaDespacho[i].Manifiesto != undefined) {
                            var manifiesto = $scope.ListadoPlanillaDespacho[i].Manifiesto;
                            $scope.ListadoManifiesto.push(manifiesto);
                        }
                    }
                    if (!$scope.obtener) {
                        try {
                            for (var i = 0; i < $scope.Modelo.DetalleLegalizacionGastos.length; i++) {
                                $scope.Modelo.DetalleLegalizacionGastos[i].Planilla = $linq.Enumerable().From($scope.ListadoPlanillaDespacho).First('$.Numero ==' + $scope.Modelo.DetalleLegalizacionGastos[i].Planilla.Codigo);
                                $scope.Modelo.DetalleLegalizacionGastos[i].Planilla.Codigo = $scope.Modelo.DetalleLegalizacionGastos[i].Planilla.Numero
                            }
                            try {
                                $scope.ListaPlanillaConceptos = AgruparListados($scope.Modelo.DetalleLegalizacionGastos, 'Planilla');
                                $scope.AsignarFechas()
                            } catch (e) {
                                $scope.ListaPlanillaConceptos = []
                            }
                        } catch (e) {
                            $scope.ListaPlanillaConceptos = []
                        }
                    }
                }
                else {
                    if (!$scope.ListaPlanillaConceptos.length > 0) {
                        ShowError('El conductor ingresado no tiene planillas asociadas o tienen vehículos de terceros')
                        $scope.Modelo.Conductor = '';
                    }
                }
            }
        }

        $scope.AsignarFechas = function () {
            for (var i = 0; i < $scope.ListaPlanillaConceptos.length; i++) {
                for (var j = 0; j < $scope.ListaPlanillaConceptos[i].Data.length; j++) {
                    var item = $scope.ListaPlanillaConceptos[i].Data[j]
                    try {
                        //if (new Date(item.FechaFactura) > MIN_DATE) {
                        item.FechaFactura = new Date(item.FechaFactura)
                        //}
                    } catch (e) {

                    }
                    try {
                        item.Valor = MascaraValores(item.Valor)
                        item.CantidadGalones = MascaraValores(item.CantidadGalones)
                        item.ValorGalones = MascaraValores(item.ValorGalones)
                    } catch (e) {

                    }

                }
            }
        }
        $scope.ListConceptos = function (item) {
            if (item.lstData == true) {
                item.lstData = false
            } else {
                item.lstData = true
            }
        }
        $scope.Comprobantes = []
        $scope.AgregarPlanilla = function () {
            var cont = 0
            if ($scope.Planilla == undefined || $scope.Planilla == '') {
                ShowError('Debe seleccionar una planilla')
            } else {
                for (var i = 0; i < $scope.ListaPlanillaConceptos.length; i++) {
                    if ($scope.Planilla.NumeroDocumento == $scope.ListaPlanillaConceptos[i].Planilla.NumeroDocumento) {
                        cont++
                    }
                } if (cont > 0) {
                    ShowError('La planilla seleccionada ya se encuentra asociada ')
                    $scope.Planilla = ''
                } else {
                    cargarPlanilla($scope.Planilla);
                }
                $scope.Planilla = ''
            }
        }

        $scope.AgregarManifiesto = function () {
            var cont = 0;
            var tmpPlanilla;
            if ($scope.Manifiesto == undefined || $scope.Manifiesto == '') {
                ShowError('Debe seleccionar un Manifiesto')
            }
            else {
                //--Enlaza Manifiesto con planilla respectiva
                for (var i = 0; i < $scope.ListadoPlanillaDespacho.length; i++) {
                    if ($scope.ListadoPlanillaDespacho[i].Manifiesto.Numero == $scope.Manifiesto.Numero) {
                        tmpPlanilla = $scope.ListadoPlanillaDespacho[i];
                        break;
                    }
                }
                //--Valida existencia con planilla respectiva
                for (var i = 0; i < $scope.ListaPlanillaConceptos.length; i++) {
                    if (tmpPlanilla.NumeroDocumento == $scope.ListaPlanillaConceptos[i].Planilla.NumeroDocumento) {
                        cont++
                    }
                }
                if (cont > 0) {
                    ShowError('El Manifiesto seleccionado ya se encuentra asociado')
                    $scope.Manifiesto = ''
                }
                else {
                    cargarPlanilla(tmpPlanilla);
                }
                $scope.Manifiesto = ''
            }
        }

        function cargarPlanilla(planilla) {
            var Data = []
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: planilla.Ruta.Codigo,
                ConsultaLegalizacionGastosRuta: true,
                Sync: true
            }
            var response = RutasFactory.Obtener(filtros)
            if (response.ProcesoExitoso === true) {
                if (response.Datos.LegalizacionGastosRuta !== undefined) {
                    planilla.Vehiculo = $scope.CargarVehiculos(planilla.Vehiculo.Codigo);
                    for (var i = 0; i < response.Datos.LegalizacionGastosRuta.length; i++) {
                        if (response.Datos.LegalizacionGastosRuta[i].TipoVehiculo.Codigo == planilla.Vehiculo.TipoVehiculo.Codigo || response.Datos.LegalizacionGastosRuta[i].TipoVehiculo.Codigo == CODIGO_TIPO_VEHICULO_NO_APLICA) {
                            if (response.Datos.LegalizacionGastosRuta[i].Concepto.Codigo == CODIGO_GASTO_COMBUSTIBLE_RENDIMIENTO_GALON) {
                                //var concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + response.Datos.LegalizacionGastosRuta[i].Concepto.Codigo)
                                //Data.push({ Concepto: concepto, Valor: MascaraValores(response.Datos.LegalizacionGastosRuta[i].Valor) })

                                try {
                                    //Calcular el concepto combustible según el tipo de combustible del vehículo:
                                    var rVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 0, Placa: planilla.Vehiculo.Placa, Sync: true }).Datos;
                                    var ResponseUltimoValorCombustible = ValorCombustibleFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoCombustible: { Codigo: rVehiculo.TipoCombustible.Codigo }, Sync: true }).Datos;
                                    var ResponseRendimientoGalon = RendimientoGalonCombustibleFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Ruta: { Codigo: planilla.Ruta.Codigo }, TipoVehiculo: { Codigo: rVehiculo.TipoVehiculo.Codigo }, Estado: ESTADO_ACTIVO, Sync: true });

                                    if (ResponseRendimientoGalon != undefined) {
                                        if (ResponseRendimientoGalon.Datos.length > 0) {
                                            filtros = {
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                Codigo: planilla.Ruta.Codigo,
                                                Sync: true
                                            }

                                            var responseR = RutasFactory.Obtener(filtros).Datos
                                            var ValorRendimientoGalon = ResponseRendimientoGalon.Datos[0].RendimientoGalon == undefined ? 0 : ResponseRendimientoGalon.Datos[0].RendimientoGalon;
                                            var mult = parseFloat(responseR.Kilometros / ValorRendimientoGalon).toFixed(2)
                                            var ValorConceptoCombustible = parseInt(mult * parseFloat(ResponseUltimoValorCombustible.Valor));
                                            if (ResponseUltimoValorCombustible != undefined && ResponseUltimoValorCombustible != null) {
                                                var tempc = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 10003, Sync: true }).Datos
                                                Data.push({
                                                    Concepto: tempc,
                                                    Valor: MascaraValores(ValorConceptoCombustible),
                                                    Observaciones: '',
                                                    Proveedor: $scope.CargarTercero(1)
                                                });
                                            }
                                        }
                                    }
                                } catch (e) {
                                    console.log('Error en la consulta del concepto combustible(rendimiento galón) : ' + e)
                                }
                            } else if (response.Datos.LegalizacionGastosRuta[i].Concepto.Codigo == CODIGO_GASTO_VIATICOS) {
                                var responseR = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: planilla.Ruta.Codigo, Sync: true }).Datos;
                                var conceptoviatico = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_VIATICOS, Sync: true }).Datos;
                                var ValorConceptoViatico = parseInt(responseR.DuracionHoras / 24 * conceptoviatico.ValorFijo);
                                var tempc = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.Datos.LegalizacionGastosRuta[i].Concepto.Codigo, Sync: true }).Datos
                                var item = { Concepto: tempc, Valor: MascaraValores(ValorConceptoViatico) };
                                Data.push(item);
                            } else {
                                var concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + response.Datos.LegalizacionGastosRuta[i].Concepto.Codigo)
                                Data.push({ Concepto: concepto, Valor: MascaraValores(response.Datos.LegalizacionGastosRuta[i].Valor) })
                            }

                        }
                    }
                }

            }

            // Añadir a los conceptos el valor total de los peajes:

            //Obtener Tipo de Vehículo y Número total de Ejes del Semirremolque de la Planilla:
            var responseVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 0, Placa: planilla.Vehiculo.Placa, Sync: true });
            if (responseVehiculo.Datos.TipoDueno.Codigo !== CODIGO_CATALOGO_TIPO_DUENO_TERCERO) {
                var responseSemirremolque = {};
                var TotalEjes = 0;
                if (responseVehiculo.ProcesoExitoso == true) {

                    if (planilla.Semirremolque.Codigo != 0) {
                        responseSemirremolque = SemirremolquesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: planilla.Semirremolque.Codigo, Sync: true });
                        if (responseSemirremolque.ProcesoExitoso == true) {

                            // ejes del Semirremolque:
                            TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                        }
                    }
                    else {
                        //TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                    }


                    $scope.TotalPeajes = 0;


                    //Consultar los peajes de la ruta de la planilla:
                    RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: planilla.Ruta.Codigo, ConsultarPeajes: true }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso == true) {

                                var ListaTarifasPeajesRuta = [];
                                var PeajesRutasSize = response.data.Datos.PeajeRutas.length;

                                $scope.ListadoPeajes = [];

                                //Por cada peaje, consultar las tarifas parametrizadas:
                                for (let i = 0; i < response.data.Datos.PeajeRutas.length; i++) {
                                    if (response.data.Datos.PeajeRutas[i].Peaje.Estado == ESTADO_ACTIVO) {
                                        let NombrePeaje = response.data.Datos.PeajeRutas[i].Peaje.Nombre;

                                        PeajesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.PeajeRutas[i].Peaje.Codigo, ConsultarTarifasPeajes: true }).
                                            then(function (response) {

                                                //Comparar si el tipo de vehículo de la planilla, coincide con las tarifas parametrizadas en el peaje:
                                                for (let j = 0; j < response.data.Datos.TarifasPeajes.length; j++) {
                                                    if (response.data.Datos.TarifasPeajes[j].TipoVehiculo.Codigo == responseVehiculo.Datos.TipoVehiculo.Codigo) {

                                                        // si coincide, verificar primero que la planilla no tenga semirremolque:
                                                        if (TotalEjes > 0) {
                                                            //si lo tiene agregar el valor a los conceptos segun el número de ejes del semirremolque:
                                                            if (Data[0].Concepto == undefined || Data[0].Concepto == null) {
                                                                if (TotalEjes == 1) {
                                                                    Data[0] = {
                                                                        Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque1Eje), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                                                                    }
                                                                } else if (TotalEjes == 2) {
                                                                    Data[0] = {
                                                                        Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque2Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                                                                    }
                                                                } else if (TotalEjes == 3) {
                                                                    Data[0] = {
                                                                        Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque3Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                                                                    }
                                                                } else if (TotalEjes >= 4) {
                                                                    Data[0] = {
                                                                        Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque4Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                                                                    }
                                                                }


                                                            } else {

                                                                if (TotalEjes == 1) {
                                                                    Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque1Eje), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo) });
                                                                } else if (TotalEjes == 2) {
                                                                    Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque2Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo) });
                                                                } else if (TotalEjes == 3) {
                                                                    Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque3Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo) });
                                                                } else if (TotalEjes >= 4) {
                                                                    Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque4Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo) });
                                                                }

                                                            }
                                                        } else {
                                                            //si no lo tiene, agregar el valor parametrizado por defecto:
                                                            if (Data[0].Concepto == undefined || Data[0].Concepto == null) {
                                                                Data[0] = {
                                                                    Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].Valor), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                                                                }
                                                            } else {
                                                                Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].Valor), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo) });
                                                            }
                                                        }
                                                    }

                                                }
                                                ListaTarifasPeajesRuta.push(response.data.Datos);
                                            });
                                    }


                                }


                            }
                        });



                    //Fin Conceptos de Peajes//

                }
                if (Data.length == 0) {
                    Data.push({})
                }
                try {
                    planilla.Km_Ruta = MascaraValores(planilla.Km_Ruta)
                    planilla.Km_Galon = MascaraValores(planilla.Km_Galon)
                    planilla.Km_Inicial = MascaraValores(planilla.Km_Inicial)
                } catch (e) {

                }

                $scope.ListaPlanillaConceptos.push({ Planilla: planilla, Data: Data })

                //------Novedades de la planilla
                var filtroNovedades = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroPlanilla: planilla.Numero
                };
                DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaNovedades = $scope.ListaNovedades.concat(response.data.Datos.filter(item => item.Anulado == 0));
                                $scope.TotalCostoNovedades += response.data.Datos.filter(item => item.Anulado == 0).reduce((total, item) => total + item.ValorCosto, 0)
                            }
                        }
                    }, function (response) {
                    });
                //------Novedades de la planilla

                if ($scope.Sesion.UsuarioAutenticado.ManejoCuentasPorCobrarPagarLegalizacionesGastos) {
                    if (!DocumentosCuentasAgregados) {
                        var ResponseDocumentosCuentaporCobrarLegalizacion = DocumentoCuentasFactory.Consultar({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Tercero: { Codigo: $scope.Modelo.Conductor.Codigo },
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                            DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_PAQUETERIA },
                            CreadaManual: CERO,
                            Sync: true
                        }).Datos;
                        if (ResponseDocumentosCuentaporCobrarLegalizacion != undefined) {
                            ResponseDocumentosCuentaporCobrarLegalizacion.forEach(item => {
                                if (item.Saldo > 0) {

                                    $scope.ListaPlanillaConceptos.forEach(item1 => {

                                        item1.Data.push({
                                            Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_SALDO_A_FAVOR_EMPRESA, Sync: true }).Datos,
                                            Valor: MascaraValores(item.ValorTotal),
                                            DocumentoCuenta: item.Codigo,
                                            Observaciones: item.Observaciones
                                        });
                                    });

                                    DocumentosCuentasAgregados = true;
                                }
                            });
                        }

                        var ResponseDocumentosCuentaporPagarLegalizacion = DocumentoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.Modelo.Conductor.Codigo }, TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR, DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_PAQUETERIA }, Sync: true }).Datos;
                        if (ResponseDocumentosCuentaporPagarLegalizacion != undefined) {
                            ResponseDocumentosCuentaporPagarLegalizacion.forEach(item => {
                                if (item.Saldo > 0) {

                                    $scope.ListaPlanillaConceptos.forEach(item1 => {
                                        item1.Data.push({
                                            Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_SALDO_A_FAVOR_CONDUCTOR, Sync: true }).Datos,
                                            Valor: MascaraValores(item.ValorTotal),
                                            DocumentoCuenta: item.Codigo,
                                            Observaciones: item.Observaciones
                                        });
                                    });

                                    DocumentosCuentasAgregados = true;

                                }
                            });
                        }
                    }
                }
                //$scope.ListaPlanillaConceptos.forEach(item => {
                //    item.Data.push({
                //        Concepto: $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo==')
                //    });
                //});
                try {
                    $timeout(function () {
                        $scope.Calcular();
                    }, 1000)
                } catch (e) { }

                try {

                    var FiltroComprobantes = {
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Estado: 1,
                        CodigoTercero: $scope.Modelo.Conductor.Codigo,
                        NumeroDocumentoOrigen: planilla.NumeroDocumento,
                        //CodigoLegalizacionGastos: 1,
                        Sync: true
                    }
                    var responseCO = DocumentoComprobantesFactory.Consultar(FiltroComprobantes)

                    if (responseCO.Datos.length > 0) {
                        for (var c = 0; c < responseCO.Datos.length; c++) {
                            if (responseCO.Datos[c].DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA || responseCO.Datos[c].DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_SOBREANTICIPO_PAQUETERIA || responseCO.Datos[c].DocumentoOrigen.Codigo == 2609) {
                                $scope.Comprobantes.push(responseCO.Datos[c])
                            }

                        }
                    }

                } catch (e) {

                }
            } else {
                ShowError('No se puede legalizar una planilla con vehículo de tercero')
            }
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item)
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskDecimalesGrid = function (item) {
            return MascaraDecimales(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };
        $scope.EliminarConcepto = function (item, index) {
            item.Data.splice($index, 1)
        }

        $scope.CalcularValorGalon = function (Cantidad, Valor) {
            try {
                valor = parseFloat(parseFloat(MascaraDecimales(Valor)) * parseFloat(MascaraDecimales(Cantidad))).toFixed(2)
                return MascaraValores(valor)
            } catch (e) {
                return 0
            }

        }
        $scope.CalcularKm = function (item) {
            item.Total_Km = 0
            if (item.Km_Inicial !== undefined && item.Km_Final !== undefined) {
                if (parseFloat(MascaraDecimales(item.Km_Inicial)) >= 0 && parseFloat(MascaraDecimales(item.Km_Final)) >= 0) {
                    if (parseFloat(MascaraDecimales(item.Km_Inicial)) > parseFloat(MascaraDecimales(item.Km_Final))) {
                        ShowError('El Km Inicial debe ser menor al Km Final')
                        item.Km_Final = 0
                    } else {
                        item.Total_Km = MascaraValores(parseFloat(MascaraDecimales(item.Km_Final)) - parseFloat(MascaraDecimales(item.Km_Inicial)))
                        item.Gal_Estimados = Math.round(parseFloat(MascaraDecimales(item.Total_Km)) / parseFloat(MascaraDecimales(item.Km_Galon)))
                        if (parseFloat(MascaraDecimales(item.Galon_Estimado_Termo)) >= 0) {
                            item.Total_Galones = MascaraValores(parseFloat(parseFloat(MascaraDecimales(item.Gal_Estimados)) + parseFloat(MascaraDecimales(item.Galon_Estimado_Termo))).toFixed(2))
                        } else {
                            item.Total_Galones = MascaraValores(parseFloat(MascaraDecimales(item.Gal_Estimados)).toFixed(2))
                        }
                    }
                }
            }
            $scope.Calcular();
        }
        $scope.CalcularTotalesGalon = function (item) {
            if (item.Total_Km != undefined) {
                if (parseFloat(MascaraDecimales(item.Total_Km)) >= 0) {
                    item.Gal_Estimados = MascaraValores(Math.round(parseFloat(MascaraDecimales(item.Total_Km)) / parseFloat(MascaraDecimales(item.Km_Galon))))
                    if (item.Galon_Estimado_Termo >= 0) {
                        item.Total_Galones = MascaraValores(parseFloat(parseFloat(MascaraDecimales(item.Gal_Estimados)) + parseFloat(MascaraDecimales(item.Galon_Estimado_Termo))).toFixed(2))
                    } else {
                        item.Total_Galones = MascaraValores(parseFloat(MascaraDecimales(item.Gal_Estimados)).toFixed(2))
                    }
                } else {
                    //item.Km_Ruta = MascaraValores(item.Km_Ruta_Base)
                    ShowError('Debe ingresar los km iniciales y los Km finales')
                }
            }
            $scope.Calcular();
        }
        $scope.Recalcular = function () {
            $scope.Comprobantes = []
            for (var i = 0; i < $scope.ListaPlanillaConceptos.length; i++) {
                try {
                    var FiltroComprobantes = {
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Estado: 1,
                        //CodigoTercero: $scope.Modelo.Conductor.Codigo,
                        NumeroDocumentoOrigen: $scope.ListaPlanillaConceptos[i].Planilla.NumeroDocumento,
                        CodigoLegalizacionGastos: 1,
                        Sync: true
                    }
                    var responseCO = DocumentoComprobantesFactory.Consultar(FiltroComprobantes)

                    if (responseCO.Datos.length > 0) {
                        for (var c = 0; c < responseCO.Datos.length; c++) {
                            if (responseCO.Datos[c].DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA || responseCO.Datos[c].DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_SOBREANTICIPO_PAQUETERIA || responseCO.Datos[c].DocumentoOrigen.Codigo == 2609) {
                                $scope.Comprobantes.push(responseCO.Datos[c])
                            }

                        }
                    }

                } catch (e) {

                }
            }
            $scope.Calcular()
        }

        $scope.SeccionCombustible = function (item) {
            if (item.ShowCombustible != undefined) {
                if (item.ShowCombustible == false) {
                    item.ShowCombustible = true
                } else {
                    item.ShowCombustible = false
                }
            } else {
                item.ShowCombustible = true
            }
        }

        $scope.EliminarNovedades = function (numero, all) {
            if (all) {
                $scope.TotalCostoNovedades -= $scope.ListaNovedades.filter(item => item.NumeroDocumentoPlanilla === numero).reduce((total, item) => total + item.ValorCosto, 0);
                $scope.ListaNovedades = $scope.ListaNovedades.filter(item => item.NumeroDocumentoPlanilla !== numero);
            } else {
                $scope.TotalCostoNovedades -= $scope.ListaNovedades[numero].ValorCosto;
                $scope.ListaNovedades.splice(numero, 1);
            }
        };

    }
]);