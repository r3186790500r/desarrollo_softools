﻿Imports Newtonsoft.Json

Namespace Despachos.Remesa
    <JsonObject>
    Public Class FotoDetalleDistribucionRemesas
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="FotoDetalleDistribucionRemesas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            NombreFoto = Read(lector, "Nombre_Foto")
            TipoFoto = Read(lector, "Tipo")
            ExtensionFoto = Read(lector, "Extension")
            FotoBits = Read(lector, "Foto")
            EntregaDevolucion = Read(lector, "Entrega_Devolucion")


        End Sub

        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property NombreFoto As String
        <JsonProperty>
        Public Property TipoFoto As String
        <JsonProperty>
        Public Property ExtensionFoto As String
        <JsonProperty>
        Public Property FotoBits As Byte()
        <JsonProperty>
        Public Property CodigoUsuarioCrea As Integer
        <JsonProperty>
        Public Property ListaFotos As Boolean
        <JsonProperty>
        Public Property EntregaDevolucion As Short

    End Class
End Namespace
