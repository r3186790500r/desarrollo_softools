﻿EncoExpresApp.controller("ConsultarDespacharOrdenServiciosCtrl", ['$scope', '$timeout', 'EncabezadoSolicitudOrdenServiciosFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'ValorCatalogosFactory', 'OficinasFactory', 'TercerosFactory', 'EmpresasFactory', 'ManifiestoFactory',
    function ($scope, $timeout, EncabezadoSolicitudOrdenServiciosFactory, $linq, blockUI, blockUIConfig, $routeParams, ValorCatalogosFactory, OficinasFactory, TercerosFactory, EmpresasFactory, ManifiestoFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Procesos' }, { Nombre: 'Despachos Ordenes de Servicio' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 20;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.Numeroremesa = 0;
        $scope.NumeroDocumento = 0;
        $scope.MensajesError = [];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoOficinas = [];
        $scope.pref = '';
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_DESPCHAR_ORDEN_SERVICIOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ConsultaInicial = 0;
        $scope.ModeloCliente = { Codigo: 0 }
        $scope.ModeloEstadoDespacho = { Codigo: 0 }

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {

                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    $scope.Codigo = 0
                    Find()
                }

            }
        };
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            if ($routeParams.Numero > 0) {
                $scope.ConsultaInicial = 0;
                $scope.Numero = $routeParams.Numero;
                $scope.FechaInicio = new Date();
                $scope.FechaFin = new Date();
                Find();
            }
            else {
                //$scope.ConsultaInicial = 1;
                $scope.FechaInicio = new Date();
                $scope.FechaFin = new Date();
                Find();
            }

        } else {
            //$scope.ConsultaInicial = 1;
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
            Find();
        }

        //--------------- CARGAR COMBOS ---------------------------------//

        //Autocomplete Tenedor
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE }).
            then(function (response) {
                if (true) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaClientes = [];
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.push({ Codigo: 0, NombreCompleto: '' });
                            $scope.ListaClientes = response.data.Datos;
                            $scope.ModeloCliente = $scope.ListaClientes[$scope.ListaClientes.length - 1];
                        }

                    }
                }
            });

        /*Cargar el combo Estado Despacho */
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_DESPACHO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaEstadosDespacho = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaEstadosDespacho.push({ Codigo: 0, Nombre: '(TODOS)' });
                        response.data.Datos.forEach(function (item) {
                            $scope.ListaEstadosDespacho.push(item);
                        });
                        $scope.ModeloEstadoDespacho = $linq.Enumerable().From($scope.ListaEstadosDespacho).First('$.Codigo ==' + 0);
                    }
                    else {
                        $scope.ListaEstadosDespacho = []
                    }
                }
            }, function (response) {
            });

        $scope.ListaOficinas = []

        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListaOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListaOficinas.push(item)
                        });
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo == -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListaOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListaOficinas.push(item)
                });
                $scope.ModeloOficina = $scope.ListaOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }

        function Find() {
            if (parseInt($scope.Numero) > 0) {
                //Filtro de volver a master
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    DesdeDespachoSolicitud: true,
                    Numero: $scope.Numero,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas
                };
                $scope.Numero = 0;
            } else {
                //Filtro normal

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: $scope.ModeloNumero,
                    FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.FechaInicio,
                    FechaFinal: $scope.ModeloNumero > 0 ? undefined : $scope.FechaFin,
                    OficinaDespacha: $scope.ModeloOficina,
                    CodigoCliente: $scope.ModeloCliente.Codigo,
                    DesdeDespachoSolicitud: true,
                    Estado: ESTADO_ACTIVO,
                    EstadoSolicitud: $scope.ModeloEstadoDespacho.Codigo,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    ConsultaInicial: $scope.ConsultaInicial,
                    UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
                };
            }
            //Bloqueo Pantalla
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando...");
            $timeout(function () { blockUI.message("Buscando..."); }, 100);
            //Bloqueo Pantalla

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoOrdenesServicios = [];
            $scope.ListadoOrdenesServiciosGrid = [];
            $scope.ListadoDetalleOrdenServicio = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    blockUI.delay = 1000;
                    $scope.Pagina = $scope.paginaActual
                    $scope.RegistrosPagina = 10

                    EncabezadoSolicitudOrdenServiciosFactory.Consultar(filtros).
                        then(function (response) {
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Numero = 0;
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoOrdenesServicios = response.data.Datos;
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                $scope.ListadoOrdenesServicios.forEach(function (itemData) {
                                    itemData.NumeroDocumento = { Codigo: itemData.NumeroDocumento }
                                });
                                $scope.ListadoOrdenesServiciosGrid = AgruparListados($scope.ListadoOrdenesServicios, 'NumeroDocumento')
                                //$scope.ModeloEstadoDespacho = $linq.Enumerable().From($scope.ListaEstadosDespacho).First('$.Codigo ==' + CERO);
                                //Coloca el boton collapse con flecha a la derecha por defecto
                                $scope.ListadoOrdenesServiciosGrid.forEach(function (item) {
                                    item.ClassButton = 'glyphicon glyphicon-menu-right';
                                    item.MostrarDetalle = false;
                                })

                                //Recorre el listado de la orden de servicio para asignar los elementos adicionales que muestra en pantalla
                                $scope.ListadoOrdenesServicios.forEach(function (itemData) {
                                    $scope.ListadoOrdenesServiciosGrid.forEach(function (itemGrid) {
                                        if (itemData.NumeroDocumento.Codigo == itemGrid.NumeroDocumento.Codigo) {
                                            itemGrid.Numero = itemData.Numero;
                                            itemGrid.Fecha = itemData.Fecha;
                                            itemGrid.NombreCliente = itemData.NombreCliente;
                                            itemGrid.Observaciones = itemData.Observaciones;
                                            itemGrid.TipoDespacho = itemData.TipoDespacho;
                                            if (itemData.ValorTipoDespacho > 0) {
                                                itemGrid.ValorTipoDespacho = itemData.ValorTipoDespacho;
                                                itemGrid.SaldoTipoDespacho = itemData.SaldoTipoDespacho;
                                            }
                                            itemGrid.CodigoEstadoSolicitud = itemData.CodigoEstadoSolicitud;
                                            itemGrid.NombreUsuarioCrea = itemData.NombreUsuarioCrea;

                                            //Inicializa una lista para embeber el detalle
                                            itemGrid.ListaDetalleOrden = [];
                                            itemGrid.TablaDetalle = 'TablaDetalle';
                                            //Hace el recorrido a ".Data" porque aquí queda los datos guardados producto de la agrupación hecha,
                                            //llena la lista definida para mostrar el detalle embebido.
                                            itemGrid.Data.forEach(function (itmDeta) {
                                                itemGrid.ListaDetalleOrden.push(itmDeta);
                                            });


                                        }
                                    });
                                });
                            }

                        }, function (response) {
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                            ShowError(response.statusText);
                        });
                }
            }
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        }

        $scope.ListItem = function (item) {

            //Despliega o repliega el detalle al cual le hizo clic (>, V)
            if (item.MostrarDetalle == true) {
                //$('#TablaDetalle').hide(500);
                $('#' + item.TablaDetalle).hide(500);
                item.ClassButton = 'glyphicon glyphicon-menu-right';
                item.MostrarDetalle = false

            } else {
                //$('#TablaDetalle').show(500);
                $('#' + item.TablaDetalle).show(500);
                item.ClassButton = 'glyphicon glyphicon-menu-down';
                item.MostrarDetalle = true
            }


        }


        $scope.DesplegarDespacharViaje = function (NumeroSolicitud, IDCodigo) {
            if (IDCodigo > 0) {
                document.location.href = '#!GestionarDespacharOrdenServicios/' + NumeroSolicitud + '/' + IDCodigo;
            } else {
                document.location.href = '#!GestionarGenerarDespacharOrdenServicios/' + NumeroSolicitud + '/' + IDCodigo;
            }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.DesplegarInformeOrdenServicio = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_ORDEN_SERVICIO;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref + '&TipoDocumento=' + CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref + '&TipoDocumento=' + CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.DesplegarInformeOrdenCargue = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_ORCA;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.DesplegarInformeRemesa = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_REME;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.DesplegarInformePlanillaDespacho = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_PLDC;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.DesplegarInformeManifiesto = function (Numero, OpcionPDf, OpcionEXCEL, NumeroDocumento) {

            $scope.urlASP = '';
            $scope.Numero = Numero;
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = NumeroDocumento;
            $scope.NombreReporte = NOMBRE_REPORTE_MANI;
            //Arma el filtro
            $scope.ArmarFiltroManifiesto();
            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';
        };

        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento != undefined && $scope.NumeroDocumento != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }

        }

        $scope.ArmarFiltroManifiesto = function () {
            if ($scope.Numero != undefined && $scope.Numero != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.Numero;
            }

        }

        //------------------------------------- MASCARAS ---------------------------------------//
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };

        //JV: Condición para mostrar la columna de manifiesto si la empresa aplica la generación del manifiesto
        if ($scope.Sesion.Empresa.GeneraManifiesto === 1) {
            $scope.MostrarCampoManifiesto = true;
        };
        $scope.MostrarRemesa = function (Numero) {
            showModal('modalDocumentoDespacho')
            $scope.URL = $scope.Sesion.Main + 'GestionarRemesa/' + Numero
        }
        $scope.MostrarOrdenCargue = function (Numero) {
            //showModal('modalDocumentoDespacho')
            $scope.URL = $scope.Sesion.Main + 'GestionarOrdenCargue/' + Numero
        }
        $scope.MostrarPlanillaDespacho = function (Numero) {
            showModal('modalDocumentoDespacho')
            $scope.URL = $scope.Sesion.Main + 'GestionarPlanillasDespachos/' + Numero
        }
        $scope.MostrarManifiesto = function (Numero) {
            showModal('modalDocumentoDespacho')
            $scope.URL = $scope.Sesion.Main + 'GestionarManifiesto/' + Numero
        }
        $scope.Cerrar = function () {
            closeModal('modalDocumentoDespacho')
        }

    }]);