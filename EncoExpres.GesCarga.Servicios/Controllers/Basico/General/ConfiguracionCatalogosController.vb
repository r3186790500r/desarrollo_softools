﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="ConfiguracionCatalogosController"/>
''' </summary>
<Authorize>
Public Class ConfiguracionCatalogosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ConfiguracionCatalogos) As Respuesta(Of IEnumerable(Of ConfiguracionCatalogos))
        Return New LogicaConfiguracionCatalogos(CapaPersistenciaConfiguracionCatalogos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ConfiguracionCatalogos) As Respuesta(Of ConfiguracionCatalogos)
        Return New LogicaConfiguracionCatalogos(CapaPersistenciaConfiguracionCatalogos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ConfiguracionCatalogos) As Respuesta(Of Long)
        Return New LogicaConfiguracionCatalogos(CapaPersistenciaConfiguracionCatalogos).Guardar(entidad)
    End Function

End Class
