﻿PRINT 'gsp_consultar_recolecciones'
GO
DROP PROCEDURE gsp_consultar_recolecciones
GO
CREATE PROCEDURE gsp_consultar_recolecciones(
  @par_EMPR_Codigo SMALLINT,             
  @par_Numero NUMERIC = NULL,          
  @par_Numero_Documento NUMERIC = NULL,            
  @par_Fecha_Inicial DATETIME =NULL,                                
  @par_Fecha_Final DATETIME =NULL,                        
  @par_OFIC_Codigo NUMERIC = NULL,         
  @par_CIUD_Codigo NUMERIC = NULL, 
  @par_ZOCI_Codigo NUMERIC = NULL,                  
  @par_Nombre_Zona VARCHAR(50) = NULL,              
  @par_Nombre_Cliente VARCHAR(100) = NULL,            
  @par_Estado SMALLINT = NULL,           
  @par_Anulado SMALLINT = NULL,           
  @par_Planillado SMALLINT = NULL,                    
  @par_NumeroPagina INT = NULL,                 
  @par_RegistrosPagina INT = NULL                 
  )                
AS                
BEGIN              
          
 set @par_Fecha_Inicial = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Inicial)                           
 set @par_Fecha_Inicial = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Inicial)),@par_Fecha_Inicial)                                  
 set @par_Fecha_Inicial = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Inicial)),@par_Fecha_Inicial)                                    
 set @par_Fecha_Final = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Final)                                
 set @par_Fecha_Final = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Final)),@par_Fecha_Final)                                  
 set @par_Fecha_Final = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Final)),@par_Fecha_Final)                                  
 set @par_Fecha_Final = DATEADD(MILLISECOND, 998,@par_Fecha_Final)                                  
 set @par_Fecha_Final = DATEADD(SECOND,59,@par_Fecha_Final)                                  
 set @par_Fecha_Final = DATEADD(MINUTE,59,@par_Fecha_Final)                                  
 set @par_Fecha_Final = DATEADD(HOUR,23,@par_Fecha_Final)             
            
 IF (@par_Planillado = 0)        
         
 BEGIN          
        
  SET NOCOUNT ON;                
  DECLARE @CantidadRegistros INT                
                             
   SELECT @CantidadRegistros = (         
    SELECT DISTINCT                 
    COUNT(1)                 
    FROM                 
          
     Encabezado_Recolecciones ENRE                    
            
     LEFT JOIN Zona_Ciudades AS ZOCI                             
     ON ENRE.EMPR_Codigo = ZOCI.EMPR_Codigo                              
     AND ENRE.ZOCI_Codigo = ZOCI.Codigo            
            
     INNER JOIN Oficinas AS OFIC                             
     ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                              
     AND ENRE.OFIC_Codigo = OFIC.Codigo              
            
     INNER JOIN Ciudades AS CIUD                             
     ON ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                              
     AND ENRE.CIUD_Codigo = CIUD.Codigo            
          
     INNER JOIN Terceros AS TERC                             
     ON ENRE.EMPR_Codigo = TERC.EMPR_Codigo                              
     AND ENRE.TERC_Codigo_Cliente = TERC.Codigo            
                
     INNER JOIN Unidad_Empaque_Producto_Transportados AS UEPT                             
     ON ENRE.EMPR_Codigo = UEPT.EMPR_Codigo                              
     AND ENRE.UEPT_Codigo = UEPT.Codigo            
          
     LEFT JOIN Encabezado_Planilla_Recolecciones AS ENPR                             
     ON ENRE.EMPR_Codigo = ENPR.EMPR_Codigo                              
     AND ENRE.ENPR_Numero = ENPR.Numero            
          
     LEFT JOIN Vehiculos AS VEHI                             
     ON ENPR.EMPR_Codigo = VEHI.EMPR_Codigo                              
     AND ENPR.VEHI_Codigo = VEHI.Codigo           
             
   WHERE           
  ENRE.ENPR_Numero = ISNULL(@par_Planillado, ENRE.ENPR_Numero)    
  AND ENRE.EMPR_Codigo = @par_EMPR_Codigo            
     AND ENRE.Numero = ISNULL(@par_Numero, ENRE.Numero)          
  AND ENRE.Numero_Documento = ISNULL(@par_Numero_Documento, ENRE.Numero_Documento)                     
     AND ENRE.Fecha >= (ISNULL(@par_Fecha_Inicial, ENRE.Fecha))                                  
     AND ENRE.Fecha <= (ISNULL(@par_Fecha_Final, ENRE.Fecha))                
     AND OFIC.Codigo = ISNULL(@par_OFIC_Codigo, OFIC.Codigo)      
	   AND ENRE.ZOCI_Codigo = ISNULL(@par_ZOCI_Codigo, ENRE.ZOCI_Codigo)       
     AND ENRE.CIUD_Codigo = ISNULL(@par_CIUD_Codigo, ENRE.CIUD_Codigo)          
  AND ((ZOCI.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Zona)) + '%' OR (@par_Nombre_Zona IS NULL)))                  
     AND ((TERC.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%') OR (@par_Nombre_Cliente IS NULL))                
     AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                
     AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)             
   );                       
  WITH Pagina AS                
  (               
    SELECT DISTINCT           
      1 AS Obtener          
   ,ENRE.EMPR_Codigo        
     ,ENRE.Numero          
     ,ENRE.Numero_Documento                
     ,ENRE.Fecha                 
     ,ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente           
     ,ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1, '')+ ' ' + ISNULL(TERC.Apellido2,'')+ ' ' + ISNULL(TERC.Razon_Social,'') AS Nombre_Cliente                
     ,ISNULL(ENRE.Nombre_Contacto,'') AS Nombre_Contacto               
     ,ISNULL(ZOCI.Codigo, 0) AS Codigo_Zona              
     ,ISNULL(ZOCI.Nombre,'') AS Nombre_Zona             
     ,ISNULL(OFIC.CIUD_Codigo, 0) AS Codigo_Oficina          
     ,ISNULL(OFIC.Nombre,'') AS Nombre_Oficina          
     ,ISNULL(CIUD.Codigo, 0) AS Codigo_Ciudad          
     ,ISNULL(CIUD.Nombre,'') AS Nombre_Ciudad          
     ,ISNULL(ENRE.Barrio, '') AS Barrio                
     ,ISNULL(ENRE.Direccion, '') AS Direccion                
     ,ISNULL(ENRE.Telefonos, 0) AS Telefonos            
     ,ISNULL(ENRE.Mercancia, '') AS Mercancia              
     ,ISNULL(UEPT.Codigo, 0) AS UEPT_Codigo             
     ,ISNULL(UEPT.Descripcion, '') AS Unidad_Empaque                
     ,ISNULL(ENRE.Cantidad, 0) AS Cantidad           
     ,ISNULL(ENRE.Peso, 0) AS Peso                
     ,ISNULL(ENRE.Observaciones, '') AS Observaciones                
     ,ISNULL(ENPR.Numero_Documento, 0) AS Numero_Planilla               
     ,ISNULL(ENPR.Fecha, '') AS Fecha_Planilla           
     ,ISNULL(VEHI.Placa, '') AS Placa_Vehiculo            
     ,CASE WHEN (ENRE.Estado = 1 AND @par_Estado = 1) AND (ENRE.Anulado = 0 AND @par_Anulado = 0) AND (ENRE.ENPR_Numero = 0 AND @par_Planillado = 0) THEN 1           
     WHEN (ENRE.Estado = 1 AND @par_Estado = 1) AND (ENRE.Anulado = 0 AND @par_Anulado = 0) AND (ENRE.ENPR_Numero > 0 AND @par_Planillado > 0) THEN 2           
     WHEN ENRE.Anulado = 1 AND @par_Anulado = 1 THEN 3         
  END AS Estado                     
     ,ROW_NUMBER() OVER(ORDER BY ENRE.Numero) AS RowNumber                
                       
  FROM                 
          
     Encabezado_Recolecciones ENRE                    
            
     INNER JOIN Zona_Ciudades AS ZOCI                             
     ON ENRE.EMPR_Codigo = ZOCI.EMPR_Codigo                              
     AND ENRE.ZOCI_Codigo = ZOCI.Codigo            
            
     INNER JOIN Oficinas AS OFIC                             
     ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                              
     AND ENRE.OFIC_Codigo = OFIC.Codigo              
            
     INNER JOIN Ciudades AS CIUD                   
     ON ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                              
     AND ENRE.CIUD_Codigo = CIUD.Codigo            
          
     INNER JOIN Terceros AS TERC                             
     ON ENRE.EMPR_Codigo = TERC.EMPR_Codigo                              
     AND ENRE.TERC_Codigo_Cliente = TERC.Codigo            
                
     INNER JOIN Unidad_Empaque_Producto_Transportados AS UEPT                             
     ON ENRE.EMPR_Codigo = UEPT.EMPR_Codigo                              
     AND ENRE.UEPT_Codigo = UEPT.Codigo            
          
     LEFT JOIN Encabezado_Planilla_Recolecciones AS ENPR                             
     ON ENRE.EMPR_Codigo = ENPR.EMPR_Codigo                              
     AND ENRE.ENPR_Numero = ENPR.Numero            
          
     LEFT JOIN Vehiculos AS VEHI                             
     ON ENPR.EMPR_Codigo = VEHI.EMPR_Codigo                              
     AND ENPR.VEHI_Codigo = VEHI.Codigo          
        
   WHERE         
     ENRE.ENPR_Numero = ISNULL(@par_Planillado, ENRE.ENPR_Numero)        
  AND ENRE.EMPR_Codigo = @par_EMPR_Codigo            
    AND ENRE.Numero = ISNULL(@par_Numero, ENRE.Numero)          
  AND ENRE.Numero_Documento = ISNULL(@par_Numero_Documento, ENRE.Numero_Documento)                          
     AND ENRE.Fecha >= (ISNULL(@par_Fecha_Inicial, ENRE.Fecha))                                  
     AND ENRE.Fecha <= (ISNULL(@par_Fecha_Final, ENRE.Fecha))                
     AND OFIC.Codigo = ISNULL(@par_OFIC_Codigo, OFIC.Codigo)   
	 	   AND ENRE.ZOCI_Codigo = ISNULL(@par_ZOCI_Codigo, ENRE.ZOCI_Codigo)       
     AND ENRE.CIUD_Codigo = ISNULL(@par_CIUD_Codigo, ENRE.CIUD_Codigo)          
  AND ((ZOCI.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Zona)) + '%' OR (@par_Nombre_Zona IS NULL)))                  
     AND ((TERC.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%') OR (@par_Nombre_Cliente IS NULL))                
     AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                
     AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                     
)        
 SELECT            
      Obtener          
   ,EMPR_Codigo          
     ,Numero             
     ,Numero_Documento                
     ,Fecha           
  ,TERC_Codigo_Cliente                 
     ,Nombre_Cliente                
     ,Nombre_Contacto           
  ,Codigo_Zona             
  ,Nombre_Zona           
  ,Codigo_Oficina           
  ,Nombre_Oficina          
  ,Codigo_Ciudad          
  ,Nombre_Ciudad          
   ,Barrio                
     ,Direccion                
     ,Telefonos            
  ,Mercancia              
  ,UEPT_Codigo            
     ,Unidad_Empaque                
     ,Cantidad           
  ,Peso                
     ,Observaciones                
     ,Numero_Planilla               
     ,Fecha_Planilla           
  ,Placa_Vehiculo            
  ,Estado                 
     ,@CantidadRegistros AS TotalRegistros                
     ,@par_NumeroPagina AS PaginaObtener                
     ,@par_RegistrosPagina AS RegistrosPagina                
   FROM                
    Pagina                
   WHERE                
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)                
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)                
   ORDER BY Numero ASC           
   END        
        
    ELSE IF (@par_Planillado > 0)        
        
    BEGIN        
        
    SET NOCOUNT ON;                
    DECLARE @CantidadRegistrosDos INT                
                        
    SELECT @CantidadRegistrosDos = (                
    SELECT DISTINCT                 
    COUNT(1)    
    FROM                 
          
     Encabezado_Recolecciones ENRE                    
            
     INNER JOIN Zona_Ciudades AS ZOCI                             
     ON ENRE.EMPR_Codigo = ZOCI.EMPR_Codigo                              
     AND ENRE.ZOCI_Codigo = ZOCI.Codigo            
            
     INNER JOIN Oficinas AS OFIC                             
     ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                              
     AND ENRE.OFIC_Codigo = OFIC.Codigo              
            
     INNER JOIN Ciudades AS CIUD                             
     ON ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                              
     AND ENRE.CIUD_Codigo = CIUD.Codigo            
          
     INNER JOIN Terceros AS TERC                             
     ON ENRE.EMPR_Codigo = TERC.EMPR_Codigo                              
     AND ENRE.TERC_Codigo_Cliente = TERC.Codigo            
                
     INNER JOIN Unidad_Empaque_Producto_Transportados AS UEPT                             
     ON ENRE.EMPR_Codigo = UEPT.EMPR_Codigo                              
     AND ENRE.UEPT_Codigo = UEPT.Codigo            
          
     LEFT JOIN Encabezado_Planilla_Recolecciones AS ENPR                             
     ON ENRE.EMPR_Codigo = ENPR.EMPR_Codigo                              
     AND ENRE.ENPR_Numero = ENPR.Numero            
          
     LEFT JOIN Vehiculos AS VEHI                             
     ON ENPR.EMPR_Codigo = VEHI.EMPR_Codigo                              
     AND ENPR.VEHI_Codigo = VEHI.Codigo           
             
   WHERE           
  ENRE.ENPR_Numero >= ISNULL(@par_Planillado, ENRE.ENPR_Numero)        
  AND ENRE.EMPR_Codigo = @par_EMPR_Codigo            
     AND ENRE.Numero = ISNULL(@par_Numero, ENRE.Numero)          
  AND ENRE.Numero_Documento = ISNULL(@par_Numero_Documento, ENRE.Numero_Documento)                        
     AND ENRE.Fecha >= (ISNULL(@par_Fecha_Inicial, ENRE.Fecha))           
     AND ENRE.Fecha <= (ISNULL(@par_Fecha_Final, ENRE.Fecha))                
     AND OFIC.Codigo = ISNULL(@par_OFIC_Codigo, OFIC.Codigo)          
     AND ENRE.CIUD_Codigo = ISNULL(@par_CIUD_Codigo, ENRE.CIUD_Codigo)          
  AND ((ZOCI.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Zona)) + '%' OR (@par_Nombre_Zona IS NULL)))                  
     AND ((TERC.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%') OR (@par_Nombre_Cliente IS NULL))                
     AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                
     AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                
   );                       
  WITH Pagina AS                
  (               
    SELECT DISTINCT           
      1 AS Obtener          
   ,ENRE.EMPR_Codigo        
     ,ENRE.Numero          
     ,ENRE.Numero_Documento                
     ,ENRE.Fecha                 
     ,ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente           
     ,ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1, '')+ ' ' + ISNULL(TERC.Apellido2,'')+ ' ' + ISNULL(TERC.Razon_Social,'') AS Nombre_Cliente                
     ,ISNULL(ENRE.Nombre_Contacto,'') AS Nombre_Contacto               
     ,ISNULL(ZOCI.Codigo, 0) AS Codigo_Zona              
     ,ISNULL(ZOCI.Nombre,'') AS Nombre_Zona             
     ,ISNULL(OFIC.CIUD_Codigo, 0) AS Codigo_Oficina          
     ,ISNULL(OFIC.Nombre,'') AS Nombre_Oficina          
     ,ISNULL(CIUD.Codigo, 0) AS Codigo_Ciudad          
     ,ISNULL(CIUD.Nombre,'') AS Nombre_Ciudad          
     ,ISNULL(ENRE.Barrio, '') AS Barrio                
     ,ISNULL(ENRE.Direccion, '') AS Direccion                
     ,ISNULL(ENRE.Telefonos, 0) AS Telefonos          
     ,ISNULL(ENRE.Mercancia, '') AS Mercancia              
     ,ISNULL(UEPT.Codigo, 0) AS UEPT_Codigo             
     ,ISNULL(UEPT.Descripcion, '') AS Unidad_Empaque                
     ,ISNULL(ENRE.Cantidad, 0) AS Cantidad           
     ,ISNULL(ENRE.Peso, 0) AS Peso                
     ,ISNULL(ENRE.Observaciones, '') AS Observaciones                
     ,ISNULL(ENPR.Numero_Documento, 0) AS Numero_Planilla               
     ,ISNULL(ENPR.Fecha, '') AS Fecha_Planilla           
     ,ISNULL(VEHI.Placa, '') AS Placa_Vehiculo            
     ,CASE WHEN (ENRE.Estado = 1 AND @par_Estado = 1) AND (ENRE.Anulado = 0 AND @par_Anulado = 0) AND (ENRE.ENPR_Numero = 0 AND @par_Planillado = 0) THEN 1           
     WHEN (ENRE.Estado = 1 AND @par_Estado = 1) AND (ENRE.Anulado = 0 AND @par_Anulado = 0) AND (ENRE.ENPR_Numero > 0 AND @par_Planillado > 0) THEN 2           
     WHEN ENRE.Anulado = 1 AND @par_Anulado = 1 THEN 3         
  END AS Estado                     
     ,ROW_NUMBER() OVER(ORDER BY ENRE.Numero) AS RowNumber                
                       
  FROM                 
          
     Encabezado_Recolecciones ENRE                    
            
     INNER JOIN Zona_Ciudades AS ZOCI                             
     ON ENRE.EMPR_Codigo = ZOCI.EMPR_Codigo                              
     AND ENRE.ZOCI_Codigo = ZOCI.Codigo            
            
     INNER JOIN Oficinas AS OFIC                             
     ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                              
     AND ENRE.OFIC_Codigo = OFIC.Codigo              
            
     INNER JOIN Ciudades AS CIUD                             
     ON ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                              
     AND ENRE.CIUD_Codigo = CIUD.Codigo            
          
     INNER JOIN Terceros AS TERC                             
     ON ENRE.EMPR_Codigo = TERC.EMPR_Codigo                              
     AND ENRE.TERC_Codigo_Cliente = TERC.Codigo            
                
     INNER JOIN Unidad_Empaque_Producto_Transportados AS UEPT                             
     ON ENRE.EMPR_Codigo = UEPT.EMPR_Codigo                              
     AND ENRE.UEPT_Codigo = UEPT.Codigo            
          
     LEFT JOIN Encabezado_Planilla_Recolecciones AS ENPR        
     ON ENRE.EMPR_Codigo = ENPR.EMPR_Codigo                              
     AND ENRE.ENPR_Numero = ENPR.Numero            
          
     LEFT JOIN Vehiculos AS VEHI                             
     ON ENPR.EMPR_Codigo = VEHI.EMPR_Codigo                              
     AND ENPR.VEHI_Codigo = VEHI.Codigo          
        
   WHERE         
     ENRE.ENPR_Numero >= ISNULL(@par_Planillado, ENRE.ENPR_Numero)        
  AND ENRE.EMPR_Codigo = @par_EMPR_Codigo            
     AND ENRE.Numero = ISNULL(@par_Numero, ENRE.Numero)          
  AND ENRE.Numero_Documento = ISNULL(@par_Numero_Documento, ENRE.Numero_Documento)                     
     AND ENRE.Fecha >= (ISNULL(@par_Fecha_Inicial, ENRE.Fecha))                                  
     AND ENRE.Fecha <= (ISNULL(@par_Fecha_Final, ENRE.Fecha))                
     AND OFIC.Codigo = ISNULL(@par_OFIC_Codigo, OFIC.Codigo)          
     AND ENRE.CIUD_Codigo = ISNULL(@par_CIUD_Codigo, ENRE.CIUD_Codigo)          
  AND ((ZOCI.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Zona)) + '%' OR (@par_Nombre_Zona IS NULL)))                  
     AND ((TERC.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%') OR (@par_Nombre_Cliente IS NULL))                
     AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                
     AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                    
)        
 SELECT            
      Obtener          
   ,EMPR_Codigo          
     ,Numero             
     ,Numero_Documento                
     ,Fecha           
  ,TERC_Codigo_Cliente                 
     ,Nombre_Cliente                
     ,Nombre_Contacto           
  ,Codigo_Zona             
  ,Nombre_Zona           
  ,Codigo_Oficina           
  ,Nombre_Oficina          
  ,Codigo_Ciudad          
  ,Nombre_Ciudad          
   ,Barrio                
     ,Direccion                
     ,Telefonos            
  ,Mercancia              
  ,UEPT_Codigo            
     ,Unidad_Empaque                
     ,Cantidad           
  ,Peso     
     ,Observaciones                
     ,Numero_Planilla               
     ,Fecha_Planilla           
  ,Placa_Vehiculo            
  ,Estado                 
     ,@CantidadRegistrosDos AS TotalRegistros                
     ,@par_NumeroPagina AS PaginaObtener                
     ,@par_RegistrosPagina AS RegistrosPagina                
   FROM                
    Pagina                
   WHERE                
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)                
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)                
   ORDER BY Numero ASC           
   END          
        
   ELSE IF (@par_Planillado = -1)        
        
    BEGIN        
        
    SET NOCOUNT ON;                
    DECLARE @CantidadRegistrosTres INT                
                        
    SELECT @CantidadRegistrosTres = (                
    SELECT DISTINCT                 
    COUNT(1)                 
    FROM                 
          
     Encabezado_Recolecciones ENRE                    
            
     INNER JOIN Zona_Ciudades AS ZOCI                             
     ON ENRE.EMPR_Codigo = ZOCI.EMPR_Codigo                              
     AND ENRE.ZOCI_Codigo = ZOCI.Codigo            
            
     INNER JOIN Oficinas AS OFIC                             
     ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                              
     AND ENRE.OFIC_Codigo = OFIC.Codigo              
            
     INNER JOIN Ciudades AS CIUD                             
     ON ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                              
     AND ENRE.CIUD_Codigo = CIUD.Codigo            
          
     INNER JOIN Terceros AS TERC                             
     ON ENRE.EMPR_Codigo = TERC.EMPR_Codigo                              
     AND ENRE.TERC_Codigo_Cliente = TERC.Codigo            
                
     INNER JOIN Unidad_Empaque_Producto_Transportados AS UEPT                             
     ON ENRE.EMPR_Codigo = UEPT.EMPR_Codigo                              
     AND ENRE.UEPT_Codigo = UEPT.Codigo            
          
     LEFT JOIN Encabezado_Planilla_Recolecciones AS ENPR                             
     ON ENRE.EMPR_Codigo = ENPR.EMPR_Codigo                              
     AND ENRE.ENPR_Numero = ENPR.Numero            
          
     LEFT JOIN Vehiculos AS VEHI                             
     ON ENPR.EMPR_Codigo = VEHI.EMPR_Codigo                              
     AND ENPR.VEHI_Codigo = VEHI.Codigo           
             
   WHERE           
  ENRE.ENPR_Numero >= ISNULL(@par_Planillado, ENRE.ENPR_Numero)        
  AND ENRE.EMPR_Codigo = @par_EMPR_Codigo            
     AND ENRE.Numero = ISNULL(@par_Numero, ENRE.Numero)          
  AND ENRE.Numero_Documento = ISNULL(@par_Numero_Documento, ENRE.Numero_Documento)                        
     AND ENRE.Fecha >= (ISNULL(@par_Fecha_Inicial, ENRE.Fecha))                                  
     AND ENRE.Fecha <= (ISNULL(@par_Fecha_Final, ENRE.Fecha))                
     AND OFIC.Codigo = ISNULL(@par_OFIC_Codigo, OFIC.Codigo)          
     AND ENRE.CIUD_Codigo = ISNULL(@par_CIUD_Codigo, ENRE.CIUD_Codigo)          
  AND ((ZOCI.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Zona)) + '%' OR (@par_Nombre_Zona IS NULL)))                  
     AND ((TERC.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%') OR (@par_Nombre_Cliente IS NULL))                
     AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                
     AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)               
   );                       
  WITH Pagina AS                
  (               
    SELECT DISTINCT           
      1 AS Obtener          
   ,ENRE.EMPR_Codigo        
     ,ENRE.Numero          
     ,ENRE.Numero_Documento               
     ,ENRE.Fecha                 
     ,ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente           
     ,ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1, '')+ ' ' + ISNULL(TERC.Apellido2,'')+ ' ' + ISNULL(TERC.Razon_Social,'') AS Nombre_Cliente                
     ,ISNULL(ENRE.Nombre_Contacto,'') AS Nombre_Contacto               
     ,ISNULL(ZOCI.Codigo, 0) AS Codigo_Zona              
     ,ISNULL(ZOCI.Nombre,'') AS Nombre_Zona             
     ,ISNULL(OFIC.CIUD_Codigo, 0) AS Codigo_Oficina          
     ,ISNULL(OFIC.Nombre,'') AS Nombre_Oficina          
     ,ISNULL(CIUD.Codigo, 0) AS Codigo_Ciudad          
     ,ISNULL(CIUD.Nombre,'') AS Nombre_Ciudad          
     ,ISNULL(ENRE.Barrio, '') AS Barrio                
     ,ISNULL(ENRE.Direccion, '') AS Direccion                
     ,ISNULL(ENRE.Telefonos, 0) AS Telefonos            
     ,ISNULL(ENRE.Mercancia, '') AS Mercancia              
     ,ISNULL(UEPT.Codigo, 0) AS UEPT_Codigo             
     ,ISNULL(UEPT.Descripcion, '') AS Unidad_Empaque                
     ,ISNULL(ENRE.Cantidad, 0) AS Cantidad           
     ,ISNULL(ENRE.Peso, 0) AS Peso                
     ,ISNULL(ENRE.Observaciones, '') AS Observaciones                
     ,ISNULL(ENPR.Numero_Documento, 0) AS Numero_Planilla               
     ,ISNULL(ENPR.Fecha, '') AS Fecha_Planilla           
     ,ISNULL(VEHI.Placa, '') AS Placa_Vehiculo            
     ,CASE WHEN ENRE.Estado = 1 AND ENRE.Anulado = 0 AND ENRE.ENPR_Numero = 0 THEN 1           
     WHEN ENRE.Estado = 1 AND ENRE.Anulado = 0 AND ENRE.ENPR_Numero > 0 THEN 2           
     WHEN ENRE.Anulado = 1 THEN 3         
  END AS Estado                     
     ,ROW_NUMBER() OVER(ORDER BY ENRE.Numero) AS RowNumber                
                       
  FROM                 
          
     Encabezado_Recolecciones ENRE                    
            
     INNER JOIN Zona_Ciudades AS ZOCI                             
     ON ENRE.EMPR_Codigo = ZOCI.EMPR_Codigo                              
     AND ENRE.ZOCI_Codigo = ZOCI.Codigo            
            
     INNER JOIN Oficinas AS OFIC                             
     ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                              
     AND ENRE.OFIC_Codigo = OFIC.Codigo              
            
     INNER JOIN Ciudades AS CIUD                             
     ON ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                              
     AND ENRE.CIUD_Codigo = CIUD.Codigo            
          
     INNER JOIN Terceros AS TERC                             
     ON ENRE.EMPR_Codigo = TERC.EMPR_Codigo                              
     AND ENRE.TERC_Codigo_Cliente = TERC.Codigo            
                
     INNER JOIN Unidad_Empaque_Producto_Transportados AS UEPT                             
     ON ENRE.EMPR_Codigo = UEPT.EMPR_Codigo                              
     AND ENRE.UEPT_Codigo = UEPT.Codigo            
          
     LEFT JOIN Encabezado_Planilla_Recolecciones AS ENPR                             
     ON ENRE.EMPR_Codigo = ENPR.EMPR_Codigo                              
     AND ENRE.ENPR_Numero = ENPR.Numero            
          
     LEFT JOIN Vehiculos AS VEHI                             
     ON ENPR.EMPR_Codigo = VEHI.EMPR_Codigo                                   
	 AND ENPR.VEHI_Codigo = VEHI.Codigo         
        
   WHERE         
     ENRE.ENPR_Numero >= ISNULL(@par_Planillado, ENRE.ENPR_Numero)        
  AND ENRE.EMPR_Codigo = @par_EMPR_Codigo            
     AND ENRE.Numero = ISNULL(@par_Numero, ENRE.Numero)          
  AND ENRE.Numero_Documento = ISNULL(@par_Numero_Documento, ENRE.Numero_Documento)                     
     AND ENRE.Fecha >= (ISNULL(@par_Fecha_Inicial, ENRE.Fecha))                                  
     AND ENRE.Fecha <= (ISNULL(@par_Fecha_Final, ENRE.Fecha))                
     AND OFIC.Codigo = ISNULL(@par_OFIC_Codigo, OFIC.Codigo)          
     AND ENRE.CIUD_Codigo = ISNULL(@par_CIUD_Codigo, ENRE.CIUD_Codigo)          
  AND ((ZOCI.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Zona)) + '%' OR (@par_Nombre_Zona IS NULL)))                  
     AND ((TERC.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%') OR (@par_Nombre_Cliente IS NULL))                
     AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                
     AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                     
)        
 SELECT            
      Obtener          
   ,EMPR_Codigo          
     ,Numero             
     ,Numero_Documento                
     ,Fecha           
  ,TERC_Codigo_Cliente                 
     ,Nombre_Cliente                
     ,Nombre_Contacto           
  ,Codigo_Zona             
  ,Nombre_Zona           
  ,Codigo_Oficina           
  ,Nombre_Oficina          
  ,Codigo_Ciudad          
  ,Nombre_Ciudad          
   ,Barrio                
     ,Direccion                
     ,Telefonos            
  ,Mercancia              
  ,UEPT_Codigo            
     ,Unidad_Empaque                
     ,Cantidad           
  ,Peso                
     ,Observaciones                
     ,Numero_Planilla               
     ,Fecha_Planilla           
  ,Placa_Vehiculo            
  ,Estado                 
     ,@CantidadRegistrosTres AS TotalRegistros                
     ,@par_NumeroPagina AS PaginaObtener                
     ,@par_RegistrosPagina AS RegistrosPagina                
   FROM                
    Pagina                
   WHERE                
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)                
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)                
   ORDER BY Numero ASC           
   END              
   END    
          
   
GO