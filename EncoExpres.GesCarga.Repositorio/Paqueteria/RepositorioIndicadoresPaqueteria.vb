﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Repositorio.ControlViajes


Namespace Paqueteria
    Public Class RepositorioIndicadoresPaqueteria

        Inherits RepositorioBase(Of PlanillaDespachos)

        Public Overrides Function Consultar(filtro As PlanillaDespachos) As IEnumerable(Of PlanillaDespachos)

            Dim lista As New List(Of PlanillaDespachos)
            Dim item As PlanillaDespachos


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Cliente) Then
                    If Not IsNothing(filtro.Oficina) Then
                        If filtro.Oficina.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Oficina", filtro.Oficina.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Ruta) Then
                        If filtro.Ruta.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Ruta", filtro.Ruta.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Vehiculo) Then
                        If filtro.Vehiculo.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Vehiculo", filtro.Vehiculo.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Semirremolque) Then
                        If filtro.Semirremolque.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Semirremolque", filtro.Semirremolque.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Producto) Then
                        If filtro.Producto.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Producto", filtro.Producto.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Conductor) Then
                        If filtro.Conductor.Codigo Then
                            conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Tenedor) Then
                        If filtro.Tenedor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Tenedor", filtro.Tenedor.Codigo)
                        End If
                    End If

                    If filtro.Cliente.Codigo > 0 Then

                        conexion.AgregarParametroSQL("@par_Cliente", filtro.Cliente.Codigo)
                    End If

                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If

                    If filtro.FechaInicial > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                    End If
                    If filtro.FechaFinal > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                    End If

                    Dim resultado As IDataReader

                    If filtro.IndicadorDespachos = 1 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Cantidad_Despachos_Oficina")
                    ElseIf filtro.IndicadorDespachos = 2 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Cantidad_Despachos_Cliente")
                    ElseIf filtro.IndicadorDespachos = 3 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Cantidad_Despachos_Ruta")
                    ElseIf filtro.IndicadorDespachos = 4 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Cantidad_Despachos_Producto")
                    ElseIf filtro.IndicadorDespachos = 5 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Peso_Despachos_Oficina")
                    ElseIf filtro.IndicadorDespachos = 6 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Peso_Despachos_Cliente")
                    ElseIf filtro.IndicadorDespachos = 7 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Peso_Despachos_Ruta")
                    ElseIf filtro.IndicadorDespachos = 8 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Peso_Despachos_Producto")

                    Else
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Planilla_Despachos")
                    End If



                    While resultado.Read
                        item = New PlanillaDespachos(resultado)
                        lista.Add(item)
                    End While

                Else
                    If Not IsNothing(filtro) Then
                        If filtro.Numero > 0 Then
                            conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                        End If
                        If filtro.NumeroDocumento > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroInternoPlanilla", filtro.NumeroDocumento)
                        End If
                        If filtro.NumeroManifiesto > 0 Then
                            conexion.AgregarParametroSQL("@par_Numero_Manifiesto", filtro.NumeroManifiesto)
                        End If
                        If filtro.FechaInicial > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                        End If
                        If filtro.FechaFinal > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                        End If

                        If Not IsNothing(filtro.Ruta) Then
                            If filtro.Ruta.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_Ruta_Codigo", filtro.Ruta.Codigo)
                            End If
                            If Not IsNothing(filtro.Ruta.Nombre) Then
                                conexion.AgregarParametroSQL("@par_Ruta", filtro.Ruta.Nombre)
                            End If
                        End If

                        If Not IsNothing(filtro.Vehiculo) Then
                            If filtro.Vehiculo.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_Codigo_Placa", filtro.Vehiculo.Codigo)
                            End If
                            If Not IsNothing(filtro.Vehiculo.Placa) Then
                                conexion.AgregarParametroSQL("@par_Placa", filtro.Vehiculo.Placa)
                            End If
                        End If

                        If Not IsNothing(filtro.Conductor) Then
                            If filtro.Conductor.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_Codigo_Conductor", filtro.Conductor.Codigo)
                            End If
                            If Not IsNothing(filtro.Conductor.Nombre) Then
                                conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.Nombre)
                            End If
                        End If

                        If (filtro.Estado.Codigo = 0 Or filtro.Estado.Codigo = 1) And filtro.Estado.Codigo <> 2 Then
                            conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                            conexion.AgregarParametroSQL("@par_Anulado", 0)

                        ElseIf filtro.Estado.Codigo = 2 Then

                            conexion.AgregarParametroSQL("@par_Anulado", 1)
                        End If

                        'If filtro.NumeroGuia > 0 Then
                        '    conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", filtro.NumeroGuia)
                        'End If
                        'If filtro.TipoDocumento > 0 Then
                        '    conexion.AgregarParametroSQL("@par_Tipo_Documento", filtro.TipoDocumento)
                        'End If

                    End If

                    If Not IsNothing(filtro.Oficina) Then
                        If filtro.Oficina.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.UsuarioConsulta) Then
                        If filtro.UsuarioConsulta.Codigo > Cero Then
                            conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                        End If
                    End If

                    conexion.AgregarParametroObjetoSQL("@par_TIDO_Codigo", filtro.TipoDocumento)

                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If
                    If filtro.ConsultaLegalización > 0 Then
                        conexion.AgregarParametroSQL("@par_ELGC_Numero", 0)

                    End If
                    Dim resultado As IDataReader
                    If filtro.ConsultaLegalizaciónVariosConductores > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_planillas_legalizacion_varios_conductores]")
                    Else
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_planilla_despachos_masivo]")
                    End If

                    While resultado.Read
                        item = New PlanillaDespachos(resultado)
                        lista.Add(item)
                    End While
                End If
                conexion.CloseConnection()

            End Using


            Return lista
        End Function

        Public Overrides Function Insertar(entidad As PlanillaDespachos) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()


                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    If entidad.Fecha > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    End If
                    If entidad.FechaHoraSalida > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Salida", entidad.FechaHoraSalida, SqlDbType.DateTime)
                    End If

                    If Not IsNothing(entidad.Ruta) Then
                        conexion.AgregarParametroSQL("@par_RUTA_Codigo ", entidad.Ruta.Codigo)
                    End If
                    If Not IsNothing(entidad.Vehiculo) Then
                        If entidad.Vehiculo.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo ", entidad.Vehiculo.Codigo)
                        End If
                    End If

                    If Not IsNothing(entidad.Tenedor) Then
                        If entidad.Tenedor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor ", entidad.Tenedor.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Conductor) Then
                        If entidad.Conductor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor ", entidad.Conductor.Codigo)
                        End If
                    End If

                    If Not IsNothing(entidad.Semirremolque) Then
                        conexion.AgregarParametroSQL("@par_SEMI_Codigo ", entidad.Semirremolque.Codigo)
                    End If

                    conexion.AgregarParametroSQL("@par_Cantidad ", entidad.Cantidad, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso ", entidad.Peso, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador ", entidad.ValorFleteTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipo ", entidad.ValorAnticipo, SqlDbType.Money)

                    conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.ValorImpuestos, SqlDbType.Money)

                    conexion.AgregarParametroSQL("@par_Valor_Pagar_Transportador ", entidad.ValorPagarTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente ", entidad.ValorFleteCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Seguro_Mercancia ", entidad.ValorSeguroMercancia, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Otros_Cobros ", entidad.ValorOtrosCobros, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Credito ", entidad.ValorTotalCredito, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Contado ", entidad.ValorTotalContado, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Alcobro ", entidad.ValorTotalAlcobro, SqlDbType.Money)

                    If Not IsNothing(entidad.TarifaTransportes) Then
                        If entidad.TarifaTransportes.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TATC_Codigo_Compra", entidad.TarifaTransportes.Codigo)
                        End If

                    End If
                    If Not IsNothing(entidad.TipoTarifaTransportes) Then
                        If entidad.TipoTarifaTransportes.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TTTC_Codigo_Compra", entidad.TipoTarifaTransportes.Codigo)
                        End If

                    End If

                    If Not IsNothing(entidad.LineaNegocioTransportes) Then
                        If entidad.LineaNegocioTransportes.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_LNTC_Codigo_Compra", entidad.LineaNegocioTransportes.Codigo)
                        End If
                    End If

                    If Not IsNothing(entidad.TipoLineaNegocioTransportes) Then
                        If entidad.TipoLineaNegocioTransportes.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TLNC_Codigo_Compra", entidad.TipoLineaNegocioTransportes.Codigo)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_ETCC_Numero", entidad.NumeroTarifarioCompra)
                    conexion.AgregarParametroSQL("@par_Valor_Auxiliares", entidad.ValorAuxiliares)
                    conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)


                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                    Else
                        conexion.AgregarParametroSQL("@par_Observaciones ", VACIO)
                    End If


                    If Not IsNothing(entidad.Estado) Then
                        conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Seguro_Poliza", entidad.ValorSeguroPoliza)
                    conexion.AgregarParametroSQL("@par_Anticipo_Pagado_A", entidad.AnticipoPagadoA)
                    conexion.AgregarParametroSQL("@par_ID_Solicitud", entidad.IDDetalleSolicitud)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_planilla_despachos")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                    End While

                    resultado.Close()

                    If entidad.Numero > 0 Then

                        For Each lista In entidad.Detalles
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Numero) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", lista.Remesa.NumeroDocumento) 'SMALLINT,

                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_planilla_despachos")

                            While resultado.Read
                                If Not (resultado.Item("Numero") > 0) Then
                                    inserto = False
                                    transaccion.Dispose()
                                    Return -2
                                    Exit For
                                End If
                            End While

                            resultado.Close()
                        Next
                        If entidad.Estado.Codigo = 1 And entidad.ValorAnticipo > 0 And inserto Then
                            entidad.CuentaPorPagar.CodigoDocumentoOrigen = entidad.Numero
                            entidad.CuentaPorPagar.Numero = entidad.NumeroDocumento
                            entidad.CuentaPorPagar.TipoDocumentoOrigen = entidad.TipoDocumento
                            entidad.CuentaPorPagar.Observaciones = "PAGO ANTICIPO PLANILLA No. " + entidad.NumeroDocumento.ToString() + " VALOR $ " + entidad.ValorAnticipo.ToString()
                            If entidad.AutorizacionAnticipo > 0 Then
                                Dim Autorizacion = New Autorizaciones
                                Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                                Autorizacion.PlanillaDespacho = New PlanillaDespachos With {.Codigo = entidad.Numero}
                                Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18402}
                                Autorizacion.CuentaPorPagar = entidad.CuentaPorPagar
                                Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                                Dim repAutorizacion = New RepositorioAutorizaciones()
                                inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                            Else
                                inserto = Generar_CxP_Anticipo(entidad.CuentaPorPagar, conexion)
                            End If
                        End If
                        If entidad.Estado.Codigo = 1 And inserto Then
                            If entidad.AutorizacionFlete > 0 Then
                                Dim Autorizacion = New Autorizaciones
                                Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                                Autorizacion.PlanillaDespacho = New PlanillaDespachos With {.Codigo = entidad.Numero}
                                Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18406}
                                Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                                Autorizacion.ValorFlete = entidad.ValorFleteAutorizacion
                                Autorizacion.ValorFleteReal = entidad.ValorFleteTransportador
                                Autorizacion.Observaciones = "Solicitud cambio flete en planilla " + entidad.NumeroDocumento.ToString() + " Flete estipulado: " + entidad.strValorFleteTransportador + " Flete Solicitado: " + entidad.strValorFleteAutorizacion
                                Dim repAutorizacion = New RepositorioAutorizaciones()
                                inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                            End If
                        End If
                        If entidad.DesdeDespacharSolicitud Then
                            If Not ActualizarDespachoSolicitudServicio(entidad, conexion) Then
                                inserto = False
                                transaccion.Dispose()
                                Return -1
                            End If
                        End If
                        If entidad.NumeroDocumento.Equals(Cero) Then
                            Return Cero
                        End If
                        If inserto Then
                            'Impuestos
                            If Not IsNothing(entidad.DetallesAuxiliares) Then
                                If entidad.DetallesAuxiliares.Count > 0 Then
                                    For Each lista In entidad.DetallesAuxiliares
                                        conexion.CleanParameters()
                                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Funcionario", lista.Funcionario.Codigo) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_Numero_Horas_Trabajadas", lista.NumeroHorasTrabajadas) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_Valor", lista.Valor, SqlDbType.Decimal) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_Observaciones", lista.Observaciones) 'SMALLINT,
                                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_auxiliares_planilla_despachos")
                                        While resultado.Read
                                            If Not (resultado.Item("Numero") > 0) Then
                                                inserto = False
                                                Exit For
                                            End If
                                        End While

                                        resultado.Close()
                                    Next
                                End If
                            End If
                            If Not IsNothing(entidad.Conductores) Then
                                If entidad.Conductores.Count > 0 Then
                                    For Each lista In entidad.Conductores
                                        conexion.CleanParameters()
                                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", lista.Codigo) 'SMALLINT,
                                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Detalle_Conductores_Planilla_Despachos")
                                        While resultado.Read
                                            If Not (resultado.Item("Numero") > 0) Then
                                                inserto = False
                                                Exit For
                                            End If
                                        End While

                                        resultado.Close()
                                    Next
                                End If
                            End If
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_totales_fletes_auxiliares")
                            resultado.Close()
                            'Call ActualizarDespachoSolicitudServicio(entidad, conexion)
                            If Not IsNothing(entidad.DetalleImpuesto) Then
                                If entidad.DetalleImpuesto.Count > 0 Then
                                    For Each lista In entidad.DetalleImpuesto
                                        conexion.CleanParameters()
                                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero)
                                        conexion.AgregarParametroSQL("@par_ENIM_Codigo", lista.CodigoImpuesto)
                                        conexion.AgregarParametroSQL("@par_Valor_Tarifa", lista.ValorTarifa, SqlDbType.Decimal)
                                        conexion.AgregarParametroSQL("@par_Valor_Base", lista.ValorBase, SqlDbType.Money)
                                        conexion.AgregarParametroSQL("@par_Valor_Impuesto", lista.ValorImpuesto, SqlDbType.Money)

                                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_impuestos_planilla_despachos")

                                        While resultado.Read
                                            If Not (resultado.Item("Numero") > 0) Then
                                                inserto = False
                                                Exit For
                                            End If
                                        End While

                                        resultado.Close()
                                    Next
                                End If
                            End If
                            If inserto Then
                                transaccion.Complete()
                            End If
                        End If
                    End If
                End Using
            End Using
            Return entidad.Numero
        End Function

        Public Overrides Function Modificar(entidad As PlanillaDespachos) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()


                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_Numero ", entidad.Numero)
                    'Inserta la Planilla'
                    If entidad.Fecha > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    End If
                    If entidad.FechaHoraSalida > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Salida", entidad.FechaHoraSalida, SqlDbType.DateTime)
                    End If
                    If Not IsNothing(entidad.Ruta) Then
                        conexion.AgregarParametroSQL("@par_RUTA_Codigo ", entidad.Ruta.Codigo)
                    End If
                    If Not IsNothing(entidad.Vehiculo) Then
                        If entidad.Vehiculo.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo ", entidad.Vehiculo.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Tenedor) Then
                        If entidad.Tenedor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor ", entidad.Tenedor.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Conductor) Then
                        If entidad.Conductor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor ", entidad.Conductor.Codigo)
                        End If
                    End If

                    If Not IsNothing(entidad.Semirremolque) Then
                        conexion.AgregarParametroSQL("@par_SEMI_Codigo ", entidad.Semirremolque.Codigo)
                    End If

                    conexion.AgregarParametroSQL("@par_Cantidad ", entidad.Cantidad, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso ", entidad.Peso, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador ", entidad.ValorFleteTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipo ", entidad.ValorAnticipo, SqlDbType.Money)

                    conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.ValorImpuestos, SqlDbType.Money)

                    conexion.AgregarParametroSQL("@par_Valor_Pagar_Transportador ", entidad.ValorPagarTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente ", entidad.ValorFleteCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Seguro_Mercancia ", entidad.ValorSeguroMercancia, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Otros_Cobros ", entidad.ValorOtrosCobros, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Credito ", entidad.ValorTotalCredito, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Contado ", entidad.ValorTotalContado, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Alcobro ", entidad.ValorTotalAlcobro, SqlDbType.Money)

                    If Not IsNothing(entidad.TarifaTransportes) Then
                        If entidad.TarifaTransportes.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TATC_Codigo", entidad.TarifaTransportes.Codigo)
                        End If

                    End If
                    If Not IsNothing(entidad.TipoTarifaTransportes) Then
                        If entidad.TipoTarifaTransportes.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TTTC_Codigo", entidad.TipoTarifaTransportes.Codigo)
                        End If

                    End If

                    If Not IsNothing(entidad.LineaNegocioTransportes) Then
                        If entidad.LineaNegocioTransportes.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_LNTC_Codigo_Compra", entidad.LineaNegocioTransportes.Codigo)
                        End If
                    End If

                    If Not IsNothing(entidad.TipoLineaNegocioTransportes) Then
                        If entidad.TipoLineaNegocioTransportes.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TLNC_Codigo_Compra", entidad.TipoLineaNegocioTransportes.Codigo)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_ETCC_Numero", entidad.NumeroTarifarioCompra)
                    conexion.AgregarParametroSQL("@par_Valor_Auxiliares", entidad.ValorAuxiliares)
                    conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)


                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                    Else
                        conexion.AgregarParametroSQL("@par_Observaciones ", VACIO)
                    End If


                    If Not IsNothing(entidad.Estado) Then
                        conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Seguro_Poliza", entidad.ValorSeguroPoliza)
                    conexion.AgregarParametroSQL("@par_Anticipo_Pagado_A", entidad.AnticipoPagadoA)
                    conexion.AgregarParametroSQL("@par_ID_Solicitud", entidad.IDDetalleSolicitud)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_planilla_despachos")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                    End While

                    resultado.Close()

                    For Each lista In entidad.Detalles
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Numero) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", lista.Remesa.NumeroDocumento) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo) 'SMALLINT,

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_planilla_despachos")

                        While resultado.Read
                            If Not (resultado.Item("Numero") > 0) Then
                                inserto = False
                                Exit For
                            End If
                        End While

                        resultado.Close()
                    Next
                    If entidad.Estado.Codigo = 1 And entidad.ValorAnticipo > 0 And inserto Then
                        entidad.CuentaPorPagar.CodigoDocumentoOrigen = entidad.Numero
                        entidad.CuentaPorPagar.Numero = entidad.NumeroDocumento
                        entidad.CuentaPorPagar.TipoDocumentoOrigen = entidad.TipoDocumento
                        entidad.CuentaPorPagar.Observaciones = "PAGO ANTICIPO PLANILLA No. " + entidad.NumeroDocumento.ToString() + " VALOR $ " + entidad.ValorAnticipo.ToString()
                        If entidad.AutorizacionAnticipo > 0 Then
                            Dim Autorizacion = New Autorizaciones
                            Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                            Autorizacion.PlanillaDespacho = New PlanillaDespachos With {.Codigo = entidad.Numero}
                            Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18402}
                            Autorizacion.CuentaPorPagar = entidad.CuentaPorPagar
                            Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                            Dim repAutorizacion = New RepositorioAutorizaciones()
                            inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                        Else
                            inserto = Generar_CxP_Anticipo(entidad.CuentaPorPagar, conexion)
                        End If
                    End If
                    If entidad.Estado.Codigo = 1 And inserto Then
                        If entidad.AutorizacionFlete > 0 Then
                            Dim Autorizacion = New Autorizaciones
                            Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                            Autorizacion.PlanillaDespacho = New PlanillaDespachos With {.Codigo = entidad.Numero}
                            Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18406}
                            Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                            Autorizacion.ValorFlete = entidad.ValorFleteAutorizacion
                            Autorizacion.ValorFleteReal = entidad.ValorFleteTransportador
                            Autorizacion.Observaciones = "Solicitud cambio flete en planilla " + entidad.NumeroDocumento.ToString() + " Flete estipulado: " + entidad.strValorFleteTransportador + " Flete Solicitado: " + entidad.strValorFleteAutorizacion
                            Dim repAutorizacion = New RepositorioAutorizaciones()
                            inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                        End If
                    End If
                    If inserto Then
                        If Not IsNothing(entidad.DetallesAuxiliares) Then
                            If entidad.DetallesAuxiliares.Count > 0 Then
                                For Each lista In entidad.DetallesAuxiliares
                                    conexion.CleanParameters()
                                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Funcionario", lista.Funcionario.Codigo) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_Numero_Horas_Trabajadas", lista.NumeroHorasTrabajadas) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_Valor", lista.Valor, SqlDbType.Decimal) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_Observaciones", lista.Observaciones) 'SMALLINT,
                                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_auxiliares_planilla_despachos")
                                    While resultado.Read
                                        If Not (resultado.Item("Numero") > 0) Then
                                            inserto = False
                                            Exit For
                                        End If
                                    End While

                                    resultado.Close()
                                Next
                            End If
                        End If
                        If Not IsNothing(entidad.Conductores) Then
                            If entidad.Conductores.Count > 0 Then
                                For Each lista In entidad.Conductores
                                    conexion.CleanParameters()
                                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", lista.Codigo) 'SMALLINT,
                                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Detalle_Conductores_Planilla_Despachos")
                                    While resultado.Read
                                        If Not (resultado.Item("Numero") > 0) Then
                                            inserto = False
                                            Exit For
                                        End If
                                    End While

                                    resultado.Close()
                                Next
                            End If
                        End If
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_totales_fletes_auxiliares")
                        resultado.Close()
                        If Not IsNothing(entidad.DetalleImpuesto) Then
                            If entidad.DetalleImpuesto.Count > 0 Then
                                For Each lista In entidad.DetalleImpuesto
                                    conexion.CleanParameters()
                                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                                    conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero)
                                    conexion.AgregarParametroSQL("@par_ENIM_Codigo", lista.CodigoImpuesto)
                                    conexion.AgregarParametroSQL("@par_Valor_Tarifa", lista.ValorTarifa, SqlDbType.Decimal)
                                    conexion.AgregarParametroSQL("@par_Valor_Base", lista.ValorBase, SqlDbType.Money)
                                    conexion.AgregarParametroSQL("@par_Valor_Impuesto", lista.ValorImpuesto, SqlDbType.Money)

                                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_impuestos_planilla_despachos")

                                    While resultado.Read
                                        If Not (resultado.Item("Numero") > 0) Then
                                            inserto = False
                                            Exit For
                                        End If
                                    End While

                                    resultado.Close()
                                Next
                            End If
                        End If
                    End If

                    If inserto Then
                        transaccion.Complete()
                    End If
                End Using
            End Using
            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As PlanillaDespachos) As PlanillaDespachos
            Dim item As New PlanillaDespachos
            Dim Doc As New Documentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                If Not IsNothing(filtro.Manifiesto) Then
                    conexion.AgregarParametroSQL("@par_Numero_Manifiesto", filtro.Manifiesto.NumeroDocumento)
                End If
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)

                'JV: Se utilizan los siguientes filtros para realizar el obtener a partir del número de documento
                'desde el cumplido de la planilla.

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.NumeroDocumento > Cero And filtro.Oficina.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If
                Dim resultado As IDataReader
                If Not IsNothing(filtro.Manifiesto) Then
                    If filtro.Manifiesto.NumeroDocumento > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_planilla_despachos_x_manifiesto]")
                    Else
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_planilla_despachos]")

                    End If
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_planilla_despachos]")
                End If

                While resultado.Read
                    filtro.Numero = resultado.Item("Numero")
                    item = New PlanillaDespachos(resultado)
                End While


                If item.Numero > 0 Then
                    item.ListadoAuxiliar = Obtener_Detalle_Auxiliar(filtro)
                    item.ListadoImpuestos = Obtener_Detalle_Impuestos(filtro)
                    item.ListadoRemesas = Obtener_Detalle_Remesas(filtro)

                    Dim Conductores As Terceros
                    Dim ListaConductores As New List(Of Terceros)
                    conexion.CleanParameters()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_conductores_planilla_guia_paqueteria]")
                    While resultado.Read
                        Conductores = New Terceros(resultado)
                        ListaConductores.Add(Conductores)
                    End While
                    If ListaConductores.Count() > 0 Then
                        item.Conductores = ListaConductores
                    End If

                End If




                conexion.CloseConnection()
                resultado.Close()

                'Obtener el detalle de la planilla (En el D.O.S no es necesario)
                'Dim Detalleguia As RemesaPaqueteria
                'Dim Detalle As DetallePlanillas
                'Dim ListaDetalle As New List(Of DetallePlanillas)
                'conexion.CleanParameters()
                'conexion.CreateConnection()
                'conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                'conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)

                'resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_guias_paqueteria_planilla]")

                'While resultado.Read
                '    Detalleguia = New RemesaPaqueteria(resultado)
                '    Detalle = New DetallePlanillas With {.Remesa = Detalleguia.Remesa}
                '    ListaDetalle.Add(Detalle)
                'End While
                'item.Planilla.Detalles = ListaDetalle
                'conexion.CloseConnection()
                'resultado.Close()

                ''Consulta detalles auxiliares
                'Dim DetalleAuxuliares As DetalleAuxiliaresPlanillas
                'Dim ListaDetalleAuxiliares As New List(Of DetalleAuxiliaresPlanillas)
                'conexion.CleanParameters()
                'conexion.CreateConnection()
                'conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                'conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)
                'resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_Auxiliares_planilla_guia_paqueteria]")

                'While resultado.Read
                '    DetalleAuxuliares = New DetalleAuxiliaresPlanillas(resultado)
                '    ListaDetalleAuxiliares.Add(DetalleAuxuliares)
                'End While
                'If ListaDetalleAuxiliares.Count() > 0 Then
                '    item.Planilla.DetallesAuxiliares = ListaDetalleAuxiliares
                'End If


                'Consulta detalles Impuestos
                'Dim DetalleImpuestos As DetalleImpuestosPlanillas
                'Dim ListaDetalleImpuestos As New List(Of DetalleImpuestosPlanillas)
                'conexion.CleanParameters()
                'conexion.CreateConnection()

                'conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                'conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)

                'resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_impuestos_planilla_despachos]")

                'While resultado.Read
                '    DetalleImpuestos = New DetalleImpuestosPlanillas(resultado)
                '    ListaDetalleImpuestos.Add(DetalleImpuestos)
                'End While
                'If ListaDetalleImpuestos.Count() > 0 Then
                '    item.Planilla.DetalleImpuestoPlanilla = ListaDetalleImpuestos
                'End If


            End Using

            Return item
        End Function
        Public Function Obtener_Detalle_Auxiliar(filtro As PlanillaDespachos) As IEnumerable(Of DetalleAuxiliaresPlanillaDespachos)
            Dim lista As New List(Of DetalleAuxiliaresPlanillaDespachos)
            Dim item As DetalleAuxiliaresPlanillaDespachos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_auxiliar_planilla_despachos")

                While resultado.Read
                    item = New DetalleAuxiliaresPlanillaDespachos(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function
        Public Function Obtener_Detalle_Tiempos(filtro As PlanillaDespachos) As IEnumerable(Of DetalleTiemposPlanillaDespachos)
            Dim lista As New List(Of DetalleTiemposPlanillaDespachos)
            Dim item As DetalleTiemposPlanillaDespachos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_tiempos_planilla_despachos")

                While resultado.Read
                    item = New DetalleTiemposPlanillaDespachos(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function
        Public Function Obtener_Detalle_Tiempos_Remesas(filtro As PlanillaDespachos) As IEnumerable(Of DetalleTiemposPlanillaDespachos)
            Dim lista As New List(Of DetalleTiemposPlanillaDespachos)
            Dim item As DetalleTiemposPlanillaDespachos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_tiempos_Remesas_planilla_despachos")

                While resultado.Read
                    item = New DetalleTiemposPlanillaDespachos(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function
        Public Function Insertar_Detalle_Tiempos(Entidad As PlanillaDespachos) As Long
            Dim inserto As Boolean = True
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()
                If Entidad.DetalleTiempos.Count() > 0 Then
                    For Each lista In Entidad.DetalleTiempos
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", Entidad.UsuarioCrea.Codigo)
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_ENPD_Numero", Entidad.Numero) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_CATA_SRSV_Codigo", lista.SitioReporteSeguimientoVehicular.Codigo) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_Fecha_Hora", lista.FechaHora, SqlDbType.DateTime) 'SMALLINT,

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_tiempos_planilla_despachos")

                        While resultado.Read
                            If Not (resultado.Item("Numero") > 0) Then
                                inserto = False
                                Exit For
                            End If
                        End While

                        resultado.Close()
                    Next
                End If
                If Entidad.DetalleTiemposRemesa.Count() > 0 Then
                    For Each lista In Entidad.DetalleTiemposRemesa
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", Entidad.UsuarioCrea.Codigo)
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.NumeroRemesa) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_CATA_SRSV_Codigo", lista.SitioReporteSeguimientoVehicular.Codigo) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_Fecha_Hora", lista.FechaHora, SqlDbType.DateTime) 'SMALLINT,

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_tiempos_Remesas_planilla_despachos")

                        While resultado.Read
                            If Not (resultado.Item("Numero") > 0) Then
                                inserto = False
                                Exit For
                            End If
                        End While

                        resultado.Close()
                    Next
                End If
            End Using
            If inserto Then
                Return 1
            Else
                Return 0
            End If
        End Function

        Public Function Obtener_Detalle_Remesas(filtro As PlanillaDespachos) As IEnumerable(Of DetallePlanillas)
            Dim lista As New List(Of DetallePlanillas)
            Dim item As DetallePlanillas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_remesas_planilla_despachos")

                While resultado.Read
                    item = New DetallePlanillas(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function
        Public Function Obtener_Detalle_Impuestos(filtro As PlanillaDespachos) As IEnumerable(Of DetalleImpuestosPlanilla)
            Dim lista As New List(Of DetalleImpuestosPlanilla)
            Dim item As DetalleImpuestosPlanilla

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_impuestos_planilla_despacho")

                While resultado.Read
                    item = New DetalleImpuestosPlanilla(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function
        Public Function Anular(entidad As PlanillaDespachos) As PlanillaDespachos
            Dim item As New PlanillaDespachos
            Dim itemDocumento As EncabezadoDocumentos
            Dim lista As New List(Of EncabezadoDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_planilla_despachos")

                While resultado.Read
                    itemDocumento = New EncabezadoDocumentos With {
                     .Cumplido = New Cumplido With {.Numero = Read(resultado, "ECPD_Numero_Documento")},
                     .Liquidacion = New Liquidacion With {.Numero = Read(resultado, "ELPD_Numero_Documento")},
                     .Comprobante = New EncabezadoDocumentoComprobantes With {.Numero = Read(resultado, "ENDC_Numero_Documento")}
                     }
                    lista.Add(itemDocumento)
                    item.Anulado = Read(resultado, "Anulo")

                End While
                item.DocumentosRelacionados = lista

            End Using

            Return item
        End Function

        Private Function ActualizarDespachoSolicitudServicio(entidad As PlanillaDespachos, contextoConexion As Conexion.DataBaseFactory) As Boolean

            Dim Actualizo As Boolean

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ESOS_Codigo", entidad.NumeroSolicitud)
            contextoConexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            contextoConexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
            contextoConexion.AgregarParametroSQL("@par_ID", entidad.IDDetalleSolicitud)
            contextoConexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("gsp_actualizar_documento_solicitud_Servicios")

            While resultado.Read
                Actualizo = IIf(resultado.Item("Cantidad") > Cero, True, False)
            End While

            resultado.Close()

            Return Actualizo

        End Function
        Public Function Generar_CxP_Anticipo(entidad As EncabezadoDocumentoCuentas, conexion As DataBaseFactory) As Boolean
            Generar_CxP_Anticipo = False

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
            conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
            conexion.AgregarParametroSQL("@par_Codigo_Documento_Origen", entidad.CodigoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.Numero)
            conexion.AgregarParametroSQL("@par_Fecha_Documento_Origen", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Fecha_Vence_Documento_Origen", DateAdd(DateInterval.Day, 30, entidad.Fecha), SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPuc.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            conexion.AgregarParametroSQL("@par_Abono", entidad.Abono)
            conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)
            conexion.AgregarParametroSQL("@par_Fecha_Cancelacion_Pago", entidad.FechaCancelacionPago, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Aprobado", entidad.Aprobado)
            conexion.AgregarParametroSQL("@par_AplicaPSL", entidad.AplicaPSL)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", entidad.TipoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Aprobo", entidad.UsuarioAprobo.Codigo)
            conexion.AgregarParametroSQL("@par_Fecha_Aprobo", entidad.FechaAprobo, SqlDbType.DateTime)
            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_cuentas")

            While resultado.Read
                Generar_CxP_Anticipo = IIf(resultado.Item("Codigo") > 0, True, False)
            End While
            resultado.Close()

            Return Generar_CxP_Anticipo
        End Function


    End Class
End Namespace