﻿print 'gsp_obtener_documento_semirremolques'
go
drop procedure gsp_obtener_documento_semirremolques
go
create procedure gsp_obtener_documento_semirremolques
(
@par_EMPR_Codigo smallint ,
@par_SEMI_Codigo numeric,
@par_CDGD_Codigo numeric
)
as
begin
select
	EMPR_Codigo ,
	SEMI_Codigo ,
	CDGD_Codigo ,
	Referencia,
	Emisor,
	Fecha_Emision,
	Fecha_Vence,
	CASE WHEN Archivo IS NULL THEN 0 ELSE 1 END AS ValorDocumento,
	Archivo,
	Nombre_Documento,
	Extension,
	Tipo
from Semirremolque_Documentos
WHERE EMPR_Codigo = @par_EMPR_Codigo  AND SEMI_Codigo = @par_SEMI_Codigo  AND CDGD_Codigo = @par_CDGD_Codigo
end
go

