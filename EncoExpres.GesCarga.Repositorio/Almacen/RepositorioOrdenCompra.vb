﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Almacen

Imports System.Transactions


Namespace Almacen
    Public NotInheritable Class RepositorioOrdenCompra
        Inherits RepositorioBase(Of OrdenCompra)
        Public Overrides Function Consultar(filtro As OrdenCompra) As IEnumerable(Of OrdenCompra)
            Dim lista As New List(Of OrdenCompra)
            Dim item As OrdenCompra

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.NombreProveedor) And filtro.NombreProveedor <> "" Then
                    conexion.AgregarParametroSQL("@par_Proveedor", filtro.NombreProveedor)
                End If
                If Not IsNothing(filtro.Numero) Then
                    If filtro.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    End If
                End If
                If (filtro.FechaInicial > Date.MinValue) And (filtro.FechaFinal >= filtro.FechaInicial) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If
                If Not IsNothing(filtro.CodigoEstadoDocumento) Then
                    If filtro.CodigoEstadoDocumento >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.CodigoEstadoDocumento)
                    End If
                End If
                If Not IsNothing(filtro.Almacen) Then
                    If filtro.Almacen.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_ALMA_Codigo", filtro.Almacen.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_consultar_encabezado_orden_compra_almacenes")

                While resultado.Read
                    item = New OrdenCompra(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As OrdenCompra) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha ", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_ALMA_Codigo", entidad.Almacen.Codigo)

                    conexion.AgregarParametroSQL("@par_Contacto_Recibe ", entidad.ContactoRecibe)
                    conexion.AgregarParametroSQL("@par_TERC_Proveedor", entidad.Proveedor.Codigo)
                    conexion.AgregarParametroSQL("@par_Contacto_Entrega ", entidad.ContactoEntrega)
                    conexion.AgregarParametroSQL("@par_Fecha_Entrega ", entidad.FechaEntrega, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_FPOC_Codigo ", entidad.FormaPago.Codigo)

                    conexion.AgregarParametroSQL("@par_Dias_Credito ", entidad.DiasCredito)
                    conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Porcentaje_Anticipo  ", entidad.PorcentajeAnticipo)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipo ", entidad.ValorAnticipo)
                    conexion.AgregarParametroSQL("@par_Subtotal ", entidad.Subtotal)


                    conexion.AgregarParametroSQL("@par_Valor_Antes_Descuento ", entidad.ValorAntesDescuento)
                    conexion.AgregarParametroSQL("@par_Porcentaje_Descuento ", entidad.PorcentajeDescuento)
                    conexion.AgregarParametroSQL("@par_Valor_Descuento ", entidad.ValorDescuento)
                    conexion.AgregarParametroSQL("@par_Valor_IVA ", entidad.ValorIva)
                    conexion.AgregarParametroSQL("@par_Valor_Total ", entidad.ValorTotal)

                    conexion.AgregarParametroSQL("@par_Valor_Flete ", entidad.ValorFlete)
                    conexion.AgregarParametroSQL("@par_Cumplido ", entidad.Cumplido)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.EstadoDocumento.Codigo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)


                    'conexion.AgregarParametroSQL("@par_Valor_Transporte ", entidad.ValorTransporte)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_encabezado_orden_compra_almacenes]")

                    While resultado.Read
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While

                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If
                    For Each detalle In entidad.Detalles
                        detalle.NumeroOrden = entidad.Numero
                        detalle.TipoDocumento = entidad.TipoDocumento
                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto And insertoRecorrido Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using
            Return entidad.Numero
        End Function
        Public Function InsertarDetalle(entidad As DetalleOrdenCompra, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_EOCA_Numero", entidad.NumeroOrden)
            contextoConexion.AgregarParametroSQL("@par_REAL_Codigo", entidad.ReferenciaAlmacen.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
            contextoConexion.AgregarParametroSQL("@par_Valor_Unitario", entidad.ValorUnitario)

            contextoConexion.AgregarParametroSQL("@par_Valor_Antes_Descuento", entidad.ValorAntesDescuento)
            contextoConexion.AgregarParametroSQL("@par_Porcentaje_Descuento", entidad.PorcentajeDescuento)
            contextoConexion.AgregarParametroSQL("@par_Valor_Descuento", entidad.ValorDescuento)
            contextoConexion.AgregarParametroSQL("@par_Valor_Neto", entidad.ValorNeto)
            contextoConexion.AgregarParametroSQL("@par_Porcentaje_IVA", entidad.PorcentajeIva, SqlDbType.Decimal)

            contextoConexion.AgregarParametroSQL("@par_Valor_IVA", entidad.ValorIva)
            contextoConexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            contextoConexion.AgregarParametroSQL("@par_Cantidad_Recibida", entidad.CantidadRecibida)
            contextoConexion.AgregarParametroSQL("@par_Cantidad_Faltante", entidad.CantidadFaltante)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_detalle_orden_compra_almacenes]")

            While resultado.Read
                inserto = IIf(resultado.Item("EOCA_Numero") > Cero, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function
        Public Overrides Function Modificar(entidad As OrdenCompra) As Long
            Dim modifico As Boolean = True
            Dim modificoorden As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Numero ", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha ", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_ALMA_Codigo", entidad.Almacen.Codigo)
                    conexion.AgregarParametroSQL("@par_Contacto_Recibe ", entidad.ContactoRecibe)
                    conexion.AgregarParametroSQL("@par_TERC_Proveedor", entidad.Proveedor.Codigo)
                    conexion.AgregarParametroSQL("@par_Contacto_Entrega ", entidad.ContactoEntrega)
                    conexion.AgregarParametroSQL("@par_Fecha_Entrega ", entidad.FechaEntrega, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_CATA_FOCO_Codigo ", entidad.FormaPago.Codigo)
                    conexion.AgregarParametroSQL("@par_Dias_Credito ", entidad.DiasCredito)
                    conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Porcentaje_Anticipo  ", entidad.PorcentajeAnticipo)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipo ", entidad.ValorAnticipo)
                    conexion.AgregarParametroSQL("@par_Subtotal ", entidad.Subtotal)
                    conexion.AgregarParametroSQL("@par_Valor_Descuento ", entidad.ValorDescuento)
                    conexion.AgregarParametroSQL("@par_Valor_IVA ", entidad.ValorIva)
                    conexion.AgregarParametroSQL("@par_Valor_Total ", entidad.ValorTotal)
                    conexion.AgregarParametroSQL("@par_Valor_Flete ", entidad.ValorFlete)
                    conexion.AgregarParametroSQL("@par_Cumplido ", entidad.Cumplido)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.EstadoDocumento.Codigo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica ", entidad.UsuarioModifica.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Antes_Descuento ", entidad.ValorAntesDescuento)
                    conexion.AgregarParametroSQL("@par_Porcentaje_Descuento ", entidad.PorcentajeDescuento)
                    'conexion.AgregarParametroSQL("@par_Valor_Transporte ", entidad.ValorTransporte)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_modificar_encabezado_orden_compra_almacenes")

                    While resultado.Read
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While

                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If
                    For Each detalle In entidad.Detalles
                        detalle.NumeroOrden = entidad.Numero
                        detalle.TipoDocumento = entidad.TipoDocumento
                        If Not InsertarDetalle(detalle, conexion) Then
                            modificoorden = False
                            modifico = False
                            Exit For
                        End If
                    Next

                End Using

                If modifico And modificoorden Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using
            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As OrdenCompra) As OrdenCompra
            Dim item As New OrdenCompra

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_obtener_encabezado_orden_compra_almacenes")

                While resultado.Read
                    item = New OrdenCompra(resultado)
                End While


                If item.Numero > 0 Then
                    Dim DetalleOrdenCompra As New DetalleOrdenCompra With {.CodigoEmpresa = item.CodigoEmpresa, .Numero = item.Numero}
                    item.Detalles = New RepositorioDetalleOrdenCompra().Consultar(DetalleOrdenCompra)
                End If

            End Using

            Return item
        End Function


        Public Function Anular(entidad As OrdenCompra) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_anular_encabezado_orden_compra_almacenes]")

                While resultado.Read
                    anulo = IIf(Convert.ToInt16(resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class
End Namespace
