﻿EncoExpresApp.controller("ConsultarPrecintoCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'ValorCatalogosFactory', 'blockUI', 'PrecintosFactory', 'OficinasFactory',
    function ($scope, $routeParams, $timeout, $linq, ValorCatalogosFactory, blockUI, PrecintosFactory, OficinasFactory) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Asignación Precintos' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        var filtros = {};
        $scope.ListadoEstados = [];
        $scope.MostrarMensajeError = false;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.Codigo = 0;
        $scope.ListadoOficinas = [];
        $scope.ListadoOficinasDestino = [];
        $scope.ListadoTipoPrecinto = [];
        $scope.ModeloFecha = new Date();
        $scope.ModeloFechaInicial = new Date();
        $scope.ModeloFechaFinal = new Date();
        $scope.ModeloTipoPrecinto = [];
        $scope.ModeloOficinaDestino = [];
        $scope.ModeloOficina = [];
        $scope.ModeloTipoAsignacionPrecinto = ''
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PRECINTOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoTipoPrecinto.push({ Nombre: '(TODOS)', Codigo: 0 });

        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        var ResponseTiposAsignacion = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ASIGNACION_PRECINTO }, Sync: true }).Datos;
        if (ResponseTiposAsignacion != undefined) {
            ResponseTiposAsignacion.push({ Codigo: -1, Nombre: '(TODOS)' });
            $scope.ListadoTiposAsignacionPrecinto = ResponseTiposAsignacion
            $scope.ModeloTipoAsignacionPrecinto = $linq.Enumerable().From($scope.ListadoTiposAsignacionPrecinto).First('$.Codigo==-1')
        }

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PRECINTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoTipoPrecinto.push(item);
                        });
                        $scope.ModeloTipoPrecinto = $linq.Enumerable().From($scope.ListadoTipoPrecinto).First('$.Codigo ==' + CERO)
                    }
                    else {
                        $scope.ListadoTipoPrecinto = []
                    }
                }
            }, function (response) {
            });

        //----------Autocomplete
        //----Oficina
        $scope.AutocompleteOficina = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    var Response = OficinasFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Nombre: value,
                        Estado: -1,
                        Pagina: 1,
                        RegistrosPagina: 10,
                        Sync: true
                    });
                    $scope.ListadoOficinas = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoOficinas);
                }
            }
            return $scope.ListadoOficinas;
        };
        //----Oficina
        //----Oficina Destino
        $scope.AutocompleteOficinaDestino = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    var Response = OficinasFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Nombre: value,
                        Estado: -1,
                        Pagina: 1,
                        RegistrosPagina: 10,
                        Sync: true
                    });
                    $scope.ListadoOficinasDestino = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoOficinasDestino);
                }
            }
            return $scope.ListadoOficinasDestino;
        };
        //----Oficina Destino
        //----------Autocomplete

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        /*Metodo buscar*/
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Numero = 0;
                Find();
            }
        };

        //Funcion Nuevo Documento
        $scope.Asignacion = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarAsignacionPresintos';
            }
        };

        //Funcion Nuevo Documento
        $scope.Translado = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarTransladoPresintos';
            }
        };

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoImpuesto = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoImpuesto.push({ Codigo: 0, Nombre: '(TODAS)' })
                        $scope.ModalImpuesto = $scope.ListadoTipoImpuesto[0];
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != CODIGO_TIPO_IMPUESTO_NINGUNO) {
                                $scope.ListadoTipoImpuesto.push(response.data.Datos[i]);
                            }
                        }
                    }
                    else {
                        $scope.ListadoTipoImpuesto = []
                    }
                }
            }, function (response) {
            });

        /*-------------------------------------------------------------------------------------------Funcion $routeParams----------------------------------------------------------------*/
        if ($routeParams.Numero !== undefined) {
            $scope.Numero = $routeParams.Numero;
            $scope.ModeloFecha = null;
            $scope.ModeloFechaInicial = null;
            $scope.ModeloFechaFinal = null;
            $scope.ModeloTipoPrecinto = [];
            $scope.ModeloOficinaDestino = [];
            $scope.ModeloOficina = [];
            Find();
        }

        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoPrecintos = [];
           // $scope.ModeloFecha = RetornarFechaEspecificaSinTimeZone($scope.ModeloFecha);
            //$scope.ModeloFecha =new Date($scope.ModeloFecha);
           // $scope.FiltroFecha = new Date($scope.ModeloFecha.getTime() - $scope.ModeloFecha.getTimezoneOffset() * 60000).toISOString();
            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                Numero: $scope.Numero,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //Fecha_Entrega: Formatear_Fecha($scope.ModeloFecha, FORMATO_FECHA_s).split("T")[0],
                FechaInicial: $scope.ModeloFechaInicial,
                FechaFinal: $scope.ModeloFechaFinal,
                TAPR: { Codigo: $scope.ModeloTipoAsignacionPrecinto.Codigo },
                Oficina: { Codigo: $scope.ModeloOficina.Codigo },
                OficinaDestino: { Codigo: $scope.ModeloOficinaDestino.Codigo },
                Tipo_Presinto: { Codigo: $scope.ModeloTipoPrecinto.Codigo }
            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                PrecintosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoPrecintos = [];
                                response.data.Datos.forEach(function (item) {
                                    if (item.Numero > 0) {
                                        $scope.ListadoPrecintos.push(item);
                                    }
                                });
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoPrecintos = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };
        Find();

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskDecimalesGrid = function (item) {
            return MascaraDecimales(item)
        };
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope)
        }
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };

    }]);