﻿PRINT 'gsp_consultar_chequeras'
GO
DROP PROCEDURE gsp_consultar_chequeras
GO
CREATE PROCEDURE gsp_consultar_chequeras
(

	@par_EMPR_Codigo SMALLINT,
	@par_Codigo NUMERIC = NULL,
	@par_Nombre_Cuenta_Bancaria VARCHAR(50) = NULL,
	@par_Estado SMALLINT = NULL,
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
		DECLARE
			@CantidadRegistros	INT
		SELECT @CantidadRegistros = (
				SELECT DISTINCT 
					COUNT(1) 
					FROM
					Chequera_Cuenta_Bancarias CCBA,
					Cuenta_Bancarias CUBA

					WHERE

					CCBA.EMPR_Codigo = @par_EMPR_Codigo 

					AND CCBA.EMPR_Codigo = CUBA.EMPR_Codigo
					AND CCBA.CUBA_Codigo = CUBA.Codigo
					AND CCBA.Codigo = ISNULL(@par_Codigo, CCBA.Codigo)
					AND CCBA.Estado = ISNULL(@par_Estado, CCBA.Estado)
					AND ((CUBA.Nombre LIKE '%' + @par_Nombre_Cuenta_Bancaria + '%') OR (@par_Nombre_Cuenta_Bancaria IS NULL))
					);
					       
				WITH Pagina AS
				(

					SELECT 
					CCBA.EMPR_Codigo,
					CCBA.Codigo,
					CCBA.CUBA_Codigo,
					CCBA.Cheque_Inicial,
					CCBA.Cheque_Final,
					CCBA.Cheque_Actual,
					CCBA.Estado,
					CUBA.Nombre AS CuentaBancaria,
					ROW_NUMBER() OVER(ORDER BY CCBA.Codigo) AS RowNumber

					FROM
					Chequera_Cuenta_Bancarias CCBA,
					Cuenta_Bancarias CUBA

					WHERE

					CCBA.EMPR_Codigo = @par_EMPR_Codigo

					AND CCBA.EMPR_Codigo = CUBA.EMPR_Codigo
					AND CCBA.CUBA_Codigo = CUBA.Codigo

					AND CCBA.Codigo = ISNULL(@par_Codigo, CCBA.Codigo)
					AND CCBA.Estado = ISNULL(@par_Estado, CCBA.Estado)
					AND ((CUBA.Nombre LIKE '%' + @par_Nombre_Cuenta_Bancaria + '%') OR (@par_Nombre_Cuenta_Bancaria IS NULL))
		)
		SELECT DISTINCT
		0 AS Obtener,
		EMPR_Codigo,
		Codigo,
		CUBA_Codigo,
		Cheque_Inicial,
		Cheque_Final,
		Cheque_Actual,
		Estado,
		CuentaBancaria,
		@CantidadRegistros AS TotalRegistros,
		@par_NumeroPagina AS PaginaObtener,
		@par_RegistrosPagina AS RegistrosPagina
		FROM
			Pagina
		WHERE
			RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
			AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
		order by Codigo
END 
GO