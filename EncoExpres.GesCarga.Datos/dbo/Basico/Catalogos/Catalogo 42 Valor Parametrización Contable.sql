﻿----------------------------------------------------------------------------------------------------------
Print 'Catalogo 42 Valor Parametrización Contable'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 42
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 42
GO
DELETE Catalogos WHERE Codigo = 42
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,42,'VPCO','Valor Parametrización Contable',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4200,'(NO APLICA)','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4201,'Total Documentos','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4202,'Flete Transportador','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4203,'Seguro','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4204,'Otros Descuentos','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4205,'Faltantes ','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4206,'Rete ICA ','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4207,'Rete IVA ','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4208,'Subtotal ','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4209,'Valor Factura ','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4210,'Valor Anticipo ','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4211,'Otros Cobros','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,42,4212,'Subtotal','','','',1,GETDATE())
GO
-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,42,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0)
GO 
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,42,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,42,3,'Tipo Documento Genera',1,1,1,1,0,0,0,0,0,1,0)
GO 
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,42,4,'Tipo Parametrización Contable',1,1,1,1,0,0,0,0,0,1,0)
GO 
----------------------------------------------------------------------------------------------------------