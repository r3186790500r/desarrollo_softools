﻿Imports Newtonsoft.Json

Namespace Utilitarios

    ''' <summary>
    ''' Clase <see cref=" ConfiguracionServidorCorreos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ConfiguracionServidorCorreos
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConfiguracionServidorCorreos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConfiguracionServidorCorreos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader, Optional ConsultaCombo As Boolean = False)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "ID")
            Smtp = Read(lector, "Smtp")
            AutenticacionRequerida = Read(lector, "RequiereAutenticacionSmtp")
            SmtpPuerto = Read(lector, "SmtpPuerto")
            Usuario = Read(lector, "UsuarioCorreo")
            Contraseña = Read(lector, "ContrasenaCorreo")
            AutenticacionSSL = Read(lector, "AutenticacionSSL")
            Remitente = Read(lector, "Remitente")
        End Sub


        <JsonProperty>
        Public Property Codigo As Short
        <JsonProperty>
        Public Property Smtp As String
        <JsonProperty>
        Public Property AutenticacionRequerida As Short
        <JsonProperty>
        Public Property SmtpPuerto As Integer
        <JsonProperty>
        Public Property Usuario As String
        <JsonProperty>
        Public Property Contraseña As String
        <JsonProperty>
        Public Property AutenticacionSSL As Short
        <JsonProperty>
        Public Property Remitente As String
        <JsonProperty>
        Public Property Receptor As String
#Region "Filtros de Búsqueda"

        <JsonProperty>
        Public Property CodigoInicial As Integer

        <JsonProperty>
        Public Property CodigoFinal As Integer


#End Region
    End Class
End Namespace
