﻿PRINT 'gsp_consultar_entidad_origen_estudio_seguridad'
GO
DROP PROCEDURE gsp_consultar_entidad_origen_estudio_seguridad
GO
CREATE PROCEDURE gsp_consultar_entidad_origen_estudio_seguridad
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo SMALLINT = NULL,
@par_Nombre VARCHAR(50) = NULL  
)  
AS  
BEGIN  
 SELECT  
  EOES.EMPR_Codigo,    
  EOES.Codigo,    
  ISNULL(EOES.Nombre, '') AS Nombre  
 FROM  
  Entidad_Origen_Estudio_Seguridad AS EOES
 WHERE  
  EOES.EMPR_Codigo = @par_EMPR_Codigo    
  AND EOES.Codigo = ISNULL(@par_Codigo, EOES.Codigo)  
  AND EOES.Nombre = ISNULL(@par_Nombre, EOES.Nombre)  
END  
GO