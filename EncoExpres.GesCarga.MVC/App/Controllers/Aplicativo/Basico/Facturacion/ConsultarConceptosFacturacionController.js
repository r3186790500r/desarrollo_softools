﻿EncoExpresApp.controller("ConsultarConceptosFacturacionCrtr", ['$scope', '$timeout', 'TarifarioVentasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'ConceptosFacturacionFactory', 'OficinasFactory', 'ImpuestosFactory',
    function ($scope, $timeout, TarifarioVentasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, ConceptosFacturacionFactory, OficinasFactory, ImpuestosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Facturación' }, { Nombre: 'Concepto Factura' }, { Nombre: 'Consultar' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.MensajesErrorAnula = [];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListaRecorridosConsultados = [];
        $scope.ListaResultadoFiltroConsulta = [];
        $scope.ListadoRecorridosTotal = [];
        $scope.Codigo = 0;
        $scope.CodigoImpuesto = 0;
        $scope.ListadoImpuestos = []; 
        $scope.NombreConcepto = '';
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: '',
            PlanillaDespacho: '',
            PlanillaRecogida: '',
            Guia: '',
            Cliente: '',
            ListadoGuias: []
        }
        $scope.ListadoConceptoLiquidacion = [];
        $scope.ListadoAuxiliar = [];
        $scope.NumeroDocumento = 0;
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 },


        ];


        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONCEPTOS_FACTURA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarConceptosFacturacion';
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.ModeloCodigo = $routeParams.Codigo;

                Find();
            }
        }
        ImpuestosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: ESTADO_ACTIVO

        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListaImpuesto = response.data.Datos;


                        if ($scope.CodigoImpuesto != undefined && $scope.CodigoImpuesto != '' && $scope.CodigoImpuesto != 0 && $scope.CodigoImpuesto != null) {
                            if ($scope.CodigoImpuesto > 0) {
                                $scope.ModeloImpuesto = $linq.Enumerable().From($scope.ListaImpuesto).First('$.Codigo ==' + $scope.CodigoImpuesto);
                            } else {
                                $scope.ModeloImpuesto = $linq.Enumerable().From($scope.ListaImpuesto).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.ModeloImpuesto = $linq.Enumerable().From($scope.ListaImpuesto).First('$.Codigo ==0');
                        }

                    }
                    else {
                        $scope.ListaImpuesto = [];
                        $scope.ModeloImpuesto = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });



        function Find() {
            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoConceptoLiquidacion = [];
            if ($scope.Buscando) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModeloCodigo,
                    CodigoAlterno: $scope.ModeloCodigoAlterno,
                    Pagina: $scope.paginaActual,
                    Nombre: $scope.ModeloNombre,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                    Estado: $scope.ModalEstado.Codigo,
                    AplicaConsultaMaster: 1,
                };
                if ($scope.MensajesError.length == 0) {
                    blockUI.delay = 1000;


                    ConceptosFacturacionFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) {
                                        response.data.Datos.forEach(function (item) {
                                            $scope.ListadoConceptoLiquidacion.push(item)
                                        });

                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                        $scope.Buscando = true;
                                        $scope.ResultadoSinRegistros = '';

                                    }
                                    else {
                                        $scope.ListadoConceptoLiquidacion = "";
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
            $scope.ModeloCodigo = ''
            $scope.ModeloNombre = '' 
            
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.Anular = function (codigo, CodigoAlterno) {
            $scope.Codigo = codigo
            $scope.NumeroDocumento = CodigoAlterno
            $scope.ModalErrorCompleto = ''
            $scope.CodigoAlterno = CodigoAlterno
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };

        $scope.BuscarConcepto = function (Codigo) {
            $scope.Codigo = Codigo;

            Obtener()

        }

        $scope.ModalImpuesto = function (Nombre) {
            $scope.NombreConcepto = Nombre
            showModal('ModalImpuesto');

        }

        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarConcepto');

            $scope.ListadoImpuestos.forEach(function (item) {

                item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;

            })

            $scope.objEnviar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                ListadoImpuestos: $scope.ListadoImpuestos,
                AplicaImpuesto: ESTADO_ACTIVO,

            };

            ConceptosFacturacionFactory.Guardar($scope.objEnviar).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Numero == 0) {
                                ShowSuccess('Se guardaron los impuestos ');

                            }
                            else {
                                ShowSuccess('Se guardaron los impuestos ');

                            }
                            location.href = '#!ConsultarConceptosFacturacion/' + $scope.Codigo;
                            closeModal('ModalImpuesto');
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);

                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);

                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            $scope.NombreConcepto = '';
        };

        function Obtener() {
            blockUI.start('Cargando Concepto Código ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Concepto Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
            };
            $scope.ListadoImpuestos = [];
            blockUI.delay = 1000;
            ConceptosFacturacionFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.ListaRecorridosConsultados = response.data.Datos.ListadoImpuestos;
                        $scope.ListaRecorridosConsultados.forEach(function (item) {
                            item.Obtenido = ESTADO_ACTIVO;
                        });

                        $scope.ListadoRecorridosTotal = $scope.ListaRecorridosConsultados;


                        if ($scope.ListadoRecorridosTotal.length > 0) {

                            //Se muestran la cantidad de recorridos por pagina
                            //En este punto se muestra siempre desde la primera pagina
                            var i = 0;

                            for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                    $scope.ListadoImpuestos.push($scope.ListadoRecorridosTotal[i])
                                }
                            }



                            //Se operan los totales de registros y de paginas
                            if ($scope.ListadoRecorridosTotal.length > 0) {
                                $scope.totalRegistros = $scope.ListadoRecorridosTotal.length;


                                $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';

                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }


                        }
                        $scope.ListadoAuxiliar = response.data.Datos.ListadoImpuestos;




                    }
                    else {
                        ShowError('No se logro consultar la Orden de Cargue ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarOrdenCargue';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarOrdenCargue';
                });
            blockUI.stop();
        };



        $scope.AgregarImpuesto = function () {
            if ($scope.ModeloImpuesto != '' && $scope.ModeloImpuesto != undefined && $scope.ModeloImpuesto != null && $scope.ModeloImpuesto.Codigo != 0 && $scope.ModeloImpuesto.Codigo != undefined) {
                var concidencias = 0
                if ($scope.ListadoImpuestos.length > 0) {
                    for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                        if ($scope.ListadoImpuestos[i].Codigo == $scope.ModeloImpuesto.Codigo) {
                            concidencias++
                            break;
                        }
                    }
                    if (concidencias > 0) {
                        ShowError('El Impuesto ya fue ingresado')
                        $scope.ModeloImpuesto = '';
                    } else {
                        $scope.ListadoImpuestos.push({ Codigo: $scope.ModeloImpuesto.Codigo, Nombre: $scope.ModeloImpuesto.Nombre, Operacion: $scope.ModeloImpuesto.Operacion, Valor_tarifa: $scope.ModeloImpuesto.Valor_tarifa, valor_base: $scope.ModeloImpuesto.valor_base, Estado: $scope.ModeloImpuesto.Estado, Obtenido: ESTADO_INACTIVO });
                        $scope.ModeloImpuesto = '';
                    }
                } else {
                    $scope.ListadoImpuestos.push({ Codigo: $scope.ModeloImpuesto.Codigo, Nombre: $scope.ModeloImpuesto.Nombre, Operacion: $scope.ModeloImpuesto.Operacion, Valor_tarifa: $scope.ModeloImpuesto.Valor_tarifa, valor_base: $scope.ModeloImpuesto.valor_base, Estado: $scope.ModeloImpuesto.Estado, Obtenido: ESTADO_INACTIVO });
                    $scope.ModeloImpuesto = '';
                }
            } else {
                ShowError('Debe ingresar un impuesto valido');
            }
        }

        $scope.CerrarModal = function () {
            closeModal('modalAnularConcepto');
            closeModal('modalMensajeAnularConcepto');
            closeModal('modalMensajeEliminar');
            closeModal('ModalImpuesto');
            $scope.Codigo = 0;

        }
        $scope.EliminarImpuesto = function (codigo, index) {
            $scope.CodigoImpuesto = codigo
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false;
            //showModal('modalEliminarImpuesto');
            $scope.ListadoImpuestos.splice(index, 1)
        };

        $scope.ConfirmarEliminarImpuesto = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                CodigoImpuesto: $scope.CodigoImpuesto,
                AplicaImpuesto: ESTADO_ACTIVO,
            };

            ConceptosFacturacionFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se Borro el Impuesto  ');
                        closeModal('modalEliminarImpuesto ');
                        Obtener();

                    }
                    else {
                        for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                            if ($scope.ListadoImpuestos[i].Codigo == $scope.CodigoImpuesto) {

                                $scope.ListadoImpuestos.splice(i, 1);
                                i = $scope.ListadoImpuestos.length;
                                closeModal('modalEliminarImpuesto');
                            }
                        }

                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeEliminarImpuesto ');
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'no se puede Eliminar el Impuesto ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };


        $scope.ConfirmaAnular = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Observaciones: $scope.ModeloCausaAnula,
                Codigo: $scope.Codigo,
                AplicaImpuesto: ESTADO_INACTIVO
            };
            ConceptosFacturacionFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se Elimino el concepto: ' + $scope.NumeroDocumento);
                        closeModal('modalAnular');

                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeConfirmacionEliminoImpuesto');
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'No se puede Eliminar el concepto' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });



        };


        $scope.ArmarFiltro = function () {
            if ($scope.Codigo != undefined && $scope.Codigo != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.Codigo;
            }
            $scope.FiltroArmado += '&Oficina=' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo;

        }

        $scope.DatosRequeridos = function () {
            DatosRequeridos = true;
            $scope.MensajesError = [];
            $scope.Rango = 0;


            if (($scope.ModeloFechaInicial == null || $scope.ModeloFechaInicial == undefined || $scope.ModeloFechaInicial == '')
                && ($scope.ModeloFechaFinal == null || $scope.ModeloFechaFinal == undefined || $scope.ModeloFechaFinal == '')

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de números o fechas');
                DatosRequeridos = false

            } else if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        DatosRequeridos = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > 60) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a 60 dias');
                        DatosRequeridos = false
                    }
                }

                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
            }

            return DatosRequeridos;
        }

    }]);