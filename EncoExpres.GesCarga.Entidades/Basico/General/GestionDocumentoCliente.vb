﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos

Namespace Basico.General
    Public Class GestionDocumentoCliente
        Inherits Base

        Sub New()
        End Sub

        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Tercero = New Terceros With {.Codigo = Read(lector, "TERC_Codigo")}
            Estado = New Estado With {.Codigo = Read(lector, "Estado")}
            TipoDocumento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TDGC_Codigo"), .Nombre = Read(lector, "CATA_TDGC_Nombre")}
            TipoGestion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TGCD_Codigo"), .Nombre = Read(lector, "CATA_TGCD_Nombre")}
            GeneraCobro = Read(lector, "Genera_Cobro")

        End Sub

        <JsonProperty>
        Public Property Tercero As Terceros
        <JsonProperty>
        Public Property TipoDocumento As ValorCatalogos
        <JsonProperty>
        Public Property TipoGestion As ValorCatalogos
        <JsonProperty>
        Public Property GeneraCobro As Short
        <JsonProperty>
        Public Property Estado As Estado
    End Class
End Namespace
