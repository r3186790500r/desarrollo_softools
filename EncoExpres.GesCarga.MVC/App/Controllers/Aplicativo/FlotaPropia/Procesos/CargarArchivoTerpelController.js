﻿EncoExpresApp.controller("CargueArchivoTerpelCtrl", ['$scope', '$timeout', '$linq', 'TercerosFactory', 'LegalizacionGastosFactory', 'blockUI', 'TercerosFactory', 'blockUIConfig', 'GestionDocumentosFactory', 'DocumentosFactory',
    function ($scope, $timeout, $linq, TercerosFactory, LegalizacionGastosFactory, blockUI, TercerosFactory, blockUIConfig, GestionDocumentosFactory, DocumentosFactory) {
        console.clear()


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.continuar = false
        $scope.VerErrores = false;
        $scope.DeshabilitarCliente = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.ListadoRutasPuntosGestion = []
        $scope.Modelo = {
            Fecha: new Date(),
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,

        }
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.MapaSitio = [{ Nombre: 'Basico' }, { Nombre: 'FlotaPropia' }, { Nombre: 'Cargue Plantilla Terpel' }];
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/


        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ];
        GestionDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, EntidadDocumento: { Codigo: 2 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDocumentos = []
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            var item = response.data.Datos[i]
                            if (item.Habilitado == 1) {
                                if (item.Documento.Codigo == 1) {
                                }
                                else {
                                    $scope.ListadoDocumentos.push(item)
                                }
                            }

                        }
                    }
                    else {
                        $scope.Documentos = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.ListadoTerceros = [];
        $scope.AutocompleteTerceros = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoTerceros = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTerceros)
                }
            }
            return $scope.ListadoTerceros
        }
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.DataActual = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.DataActual = []
                for (var i = a - 10; i < $scope.DataArchivo.length; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }

        //Paginacion MOdal de errores

        $scope.PrimerPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10) {
                $scope.DataErrorActual = []
                $scope.paginaActualError = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.AnteriorModalError = function () {
            if ($scope.paginaActualError > 1) {
                $scope.paginaActualError -= 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteModalError = function () {
            if ($scope.paginaActualError < $scope.totalPaginasErrores) {
                $scope.paginaActualError += 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    $scope.UltimaPaginaModalError()

                }
            } else if ($scope.paginaActualError == $scope.totalPaginasErrores) {
                $scope.UltimaPaginaModalError()
            }
        }
        $scope.UltimaPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10 && $scope.totalPaginasErrores > 1) {
                $scope.paginaActualError = $scope.totalPaginasErrores
                var a = $scope.paginaActualError * 10
                $scope.DataErrorActual = []
                for (var i = a - 10; i < $scope.ListaErrores.length; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10) {
                $scope.ProgramacionesNoGuardadasActual = []
                $scope.paginaActualNoGuardadas = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }
        $scope.AnteriorNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas -= 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas < $scope.totalPaginasNoGuardadas) {
                $scope.paginaActualNoGuardadas += 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardados()

                }
            } else if ($scope.paginaActualNoGuardadas == $scope.totalPaginasNoGuardadas) {
                $scope.UltimaPaginaNoGuardados()
            }
        }
        $scope.UltimaPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10 && $scope.totalPaginasNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas = $scope.totalPaginasNoGuardadas
                var a = $scope.paginaActualNoGuardadas * 10
                $scope.ProgramacionesNoGuardadasActual = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10) {
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                $scope.paginaActualNoGuardadasRemplazo = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }
        $scope.AnteriorNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo -= 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo < $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.paginaActualNoGuardadasRemplazo += 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardadosRemplazo()

                }
            } else if ($scope.paginaActualNoGuardadasRemplazo == $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.UltimaPaginaNoGuardadosRemplazo()
            }
        }
        $scope.UltimaPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10 && $scope.totalPaginasNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo = $scope.totalPaginasNoGuardadasRemplazo
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }


        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardaras = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardadasRemplazo = function (item) {
            if ($scope.Option.name == 'Omitido') {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Omitido'
                }
            } else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Remplazado'
                }
            }

        }
        $scope.LimpiarData = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            $scope.DataVerificada = []
            $scope.DataActual = []
            $scope.DataErrorActual = []
            $scope.DataArchivo = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            $scope.VerErrores = false
        }

        /*-----------------------------------------------------------------------funciones de lectura y apertura de archivos----------------------------------------------------------------------------*/

        $scope.ListaProgramaciones = []
        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined || item == '') {
                return true
            } else {
                return false
            }
        }
        function AsignarValoresTabla() {
            $scope.NumeroCargue = 0

            if ($scope.DataArchivo.Valores.length > 0) {
                var DataTemporal = []
                var DataError = []
                $scope.error = 0
                for (var i = 0; i < $scope.DataArchivo.Valores.length; i++) {
                    var item = $scope.DataArchivo.Valores[i]
                    if (!Eval(item.No_Venta)) {
                        try {
                            if (item.No_Venta != "0") {
                                item.Placa = MascaraPlaca(item.Placa)
                                item.Cantidad = MascaraValores(item.Cantidad)
                                item.Precio = MascaraValores(item.Precio)
                                item.No_Planilla = MascaraNumero(item.No_Planilla)
                                if (!Eval(item.Fecha)) {
                                    try {
                                        var fechatem = ''
                                        var dias = item.Fecha[0].toString() + item.Fecha[1].toString()
                                        var Mes = item.Fecha[3].toString() + item.Fecha[4].toString()
                                        fechatem = Mes[0].toString()
                                        fechatem += Mes[1].toString()
                                        fechatem +='/'
                                        fechatem += dias[0].toString()
                                        fechatem += dias[1].toString()
                                        fechatem +='/'
                                        fechatem += item.Fecha[6]
                                        fechatem += item.Fecha[7]
                                        fechatem += item.Fecha[8]
                                        fechatem += item.Fecha[9]
                                        //item.Fecha[5] = dias[1].toString()
                                        if (new Date(fechatem) > MIN_DATE) {
                                            item.FechaFactura = new Date(fechatem);
                                        }
                                    } catch (e) { }
                                }
                                DataTemporal.push(item)
                            } else {
                                $scope.error++
                                DataError.push(item)
                            }
                        } catch (e) {
                            $scope.error++
                            DataError.push(item)
                        }
                    }
                }

                $scope.DataArchivo = DataTemporal
                $scope.DataError = DataError
                $scope.totalRegistros = ($scope.DataArchivo.length)
                $scope.paginaActual = 1
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                blockUI.start();
                blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');
                //blockUI.stop()
                //$timeout(blockUI.stop(), 1000)
                $timeout(function () {
                    try {
                        var data = []
                        if ($scope.totalRegistros > 10) {
                            for (var i = 0; i < 10; i++) {
                                $scope.DataActual.push($scope.DataArchivo[i])
                            }
                        } else {
                            $scope.DataActual = $scope.DataArchivo
                        }
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    } catch (e) {
                        ShowError('Error en la lectura del archivo por favor intente nuevamente')
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    }
                    if ($scope.error > 0) {
                        showModal('ModalErrorCargue')
                        ShowError('No se cargarón (' + $scope.error.toString() + ') Restristos dado que el número de identificación diligenciado no corresponde a ningun tercero registrado en el sistema')
                    }
                }, 1000)
            }

        }
        var conterroresprevalidar = 0

        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            if ($scope.DataArchivo.Valores != undefined) {
                $scope.DataArchivo = { Valores: $scope.DataArchivo.Valores }
                AsignarValoresTabla()
            }
            else {
                ShowError('El arhivo cargado no corresponde al formato de carge masivo del tarifario')
            }
            blockUI.stop()

        }

        $scope.handleFile = function (e) {
            var files = e.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = e.target.result;
                $scope.arr = fixdata(data);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(btoa($scope.arr), { type: 'base64' });
                        process_wb($scope.wb);
                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);
            });
        }

        function restrow(item) {
            item.st = {}
            item.ms = {}
        }

        $scope.ValidarDocumento = function () {
            $scope.ListaErrores = []
            $scope.MensajesError = []
            /*
                        blockUI.start();
                        $timeout(function() {
                            blockUI.message('Validando Archivo');
                        }, 1000);*/
            var contadorgeneral = 0
            var contadorRegistros = 0
            $scope.DataVerificada = []
            $scope.DataVerificadatmp = []
            for (var i = 0; i < $scope.DataArchivo.length; i++) {
                var item = $scope.DataArchivo[i]
                item.ID = i
                //resetea todos los estilos de la fila que se va a evaluar
                restrow(item);
                var errores = 0
                //Condicion para saber si aplican las validaciones (Si el registro no tiene una ruta seleccionada no validara el resto de campos)
                if (item.Omitido !== true) {
                    contadorRegistros++
                    if (item.No_Venta == undefined || item.No_Venta == '') {
                        item.st.No_Venta = stError
                        item.ms.No_Venta = 'Ingrese Numero de Venta'
                        errores++
                    }

                    if (ValidarFecha(item.FechaFactura) == 1) {
                        if (item.Configuracion.AplicaFechaFactura == CAMPO_DOCUMENTO_OBLIGATORIO) {
                            item.st.FechaFactura = stError
                            item.ms.FechaFactura = 'Ingrese la fecha de Factura'
                            errores++
                        } else {
                            item.FechaEmision = ''
                        }
                    } else {
                        var f = new Date();
                        if (item.FechaFactura > f) {
                            item.st.FechaFactura = stError
                            item.ms.FechaFactura = 'La fecha de factura debe ser menor a la fecha actual'
                            errores++
                        }
                    }

                    if (item.Estacion == undefined || item.Estacion == '') {
                        item.st.Estacion = stError
                        item.ms.Estacion = 'Ingrese la Estacion'
                        errores++
                    }

                    if (item.Placa == undefined || item.Placa == '') {
                        item.st.Placa = stError
                        item.ms.Placa = 'Ingrese la Placa'
                        errores++
                    }

                    if (item.Cantidad == undefined || item.Cantidad == '') {
                        item.st.Cantidad = stError
                        item.ms.Cantidad = 'Ingrese la Cantidad'
                        errores++
                    }

                    if (item.Precio == undefined || item.Precio == '') {
                        item.st.Precio = stError
                        item.ms.Precio = 'Ingrese el Precio'
                        errores++
                    }

                    if (item.No_Planilla == undefined || item.No_Planilla == '') {
                        item.st.No_Planilla = stError
                        item.ms.No_Planilla = 'Ingrese Numero de Planilla'
                        errores++
                    }

                    if (errores > 0) {
                        item.st.Row = stwarning
                        $scope.ListaErrores.push(item)
                        contadorgeneral++
                    }
                    else {
                        item.stRow = ''
                        $scope.DataVerificada.push(item)
                    }
                }
                else {
                    restrow(item);
                }
            }
            //blockUI.stop();

            if (contadorgeneral > 0) {
                ShowError('Se encontraron inconsistencias en los datos ingresados, por favor corrija los datos marcados o seleccione omitirlos.');
                $scope.paginaActualError = 1
                $scope.DataErrorActual = []
                $scope.totalRegistrosErrores = $scope.ListaErrores.length
                $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 10);
                if ($scope.totalRegistrosErrores > 10) {
                    for (var i = 0; i < 10; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    for (var i = 0; i < $scope.ListaErrores.length; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                if (contadorRegistros == 0) {
                    ShowError('No se encontraron registros para validar, por favor revise que la casilla de omitir no se encuentre marcada en al menos un registro')
                } else {
                    ShowWarningConfirm('Los datos se verificaron correctamente.¿Desea continuar con el cargue de la información?', $scope.Guardar)
                }
            }


        }


        $scope.Guardar = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        $scope.GuardarErrados = function () {
            //closeModal('ModalErrores')
            data = []
            for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                if (!$scope.ProgramacionesNoGuardadas[i].Omitido && $scope.ProgramacionesNoGuardadas[i].Option != 'Omitido') {
                    if ($scope.ProgramacionesNoGuardadas[i].Option == 'Remplazado') {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 1
                    } else {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 0
                    }
                    data.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
            $scope.DataVerificada = data
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        var contGuard = 0
        var contGuardGeneral = 0
        var contGeneral = 0
        var Guardaros = 0
        function GuardarCompleto(inicio, opt) {
            contGuard++
            if (contGuard > $scope.DataVerificada.length) {

                ShowSuccess('Se guardaron ' + contGuardGeneral + ' de ' + contGeneral + ' Documentos')
                closeModal('modalConfirmacionRemplazarProgramacion');
                closeModal('ModalErrores');
                $scope.LimpiarData()

            } else {

                blockUI.start();
                $timeout(function () {
                    blockUI.message('Guardando Facturas');
                }, 1000);

                var Factura = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Concepto: { Codigo: new String("43") }, // COMBUSTIBLE
                    Planilla: { NumeroDocumento: $scope.DataVerificada[inicio].No_Planilla },
                    Observaciones: $scope.DataVerificada[inicio].Estacion,
                    Valor: parseFloat(parseFloat(MascaraDecimales($scope.DataVerificada[inicio].Precio)) * parseFloat(MascaraDecimales($scope.DataVerificada[inicio].Cantidad))).toFixed(2),
                    Proveedor: { Codigo: new String("1002795") },
                    NumeroFactura: $scope.DataVerificada[inicio].No_Venta,
                    ValorGalones: $scope.DataVerificada[inicio].Precio,
                    FechaFactura: new Date($scope.DataVerificada[inicio].FechaFactura),
                    CantidadGalones: $scope.DataVerificada[inicio].Cantidad,
                    Estacion: $scope.DataVerificada[inicio].Estacion,
                    Placa: $scope.DataVerificada[inicio].Placa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }
                LegalizacionGastosFactory.GuardarFacturaTerpel(Factura).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        Guardaros++
                        contGuardGeneral++
                        GuardarCompleto(contGuard)
                        blockUI.stop();
                    }
                    else {
                        GuardarCompleto(contGuard)
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $scope.ProgramacionesNoGuardadas.push($scope.DataVerificada[inicio])
                    GuardarCompleto(contGuard)
                    blockUI.stop();
                })
            }
        }

        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (o) {
            return MascaraNumero(o)
        }
        $scope.MaskValoresGrid = function (o) {
            return MascaraValores(o)
        }
        $scope.MaskMayusGrid = function (o) {
            return MascaraMayus(o)
        }
        $scope.MaskplacaGrid = function (o) {
            return MascaraPlaca(o)
        }
        $scope.MaskplacaSemiremolqueGrid = function (o) {
            return MascaraPlacaSemirremolque(o)
        }

        $scope.GenerarPlanilla = function () {
            var entidad = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, PlantillaDocumento: 1 }
            blockUI.start();
            $timeout(function () {
                blockUI.message('Generando Plantilla..');
            }, 1000);
            TercerosFactory.GenerarPlanitilla(entidad).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var pdfAsDataUri = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data.Datos.Planitlla;
                    var link = document.createElement('a');
                    link.href = pdfAsDataUri;
                    link.download = "Plantilla_Cargue_Masivo_Documentos_Terceros.xlsx";
                    link.click();
                    //window.open(pdfAsDataUri);
                    blockUI.stop();
                    ShowSuccess('La plantilla se generó correctamente')
                }
            }, function (response) {
                blockUI.stop();
                ShowError(response.statusText)
            })

        }

        $scope.DataActual = []

        function ValidarCampo(objeto, minlength, Esobjeto) {
            var resultado = 0
            if (objeto == undefined || objeto == '' || objeto == null || objeto == 0 || objeto == '0') {
                resultado = 1
            } else {
                if (resultado == 0) {
                    if (minlength !== undefined) {
                        if (objeto.length < minlength) {
                            resultado = 3
                        }
                        else {
                            resultado = 0
                        }
                    }
                } if (resultado == 0) {
                    if (Esobjeto) {
                        if (objeto.Codigo == undefined || objeto.Codigo == '' || objeto.Codigo == null || objeto.Codigo == 0) {
                            resultado = 2
                        }
                    }
                }
            }
            return resultado
        }
        //$scope.DataActual.push({})


        $scope.ModalConfirmacionNuevoProceso = function () {
            showModal('modalConfirmacionCancelarProceso');
        }

        $scope.CerrarModalNuevoProceso = function (e) {
            var fileVal = document.getElementById("fileModal");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
            closeModal('modalConfirmacionCancelarProceso');


        }
    }]);

