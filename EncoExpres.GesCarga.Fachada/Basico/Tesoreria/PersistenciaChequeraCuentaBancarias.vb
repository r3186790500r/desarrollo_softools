﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Repositorio.Basico.Tesoreria

Namespace Basico.Tesoreria
    Public NotInheritable Class PersistenciaChequeraCuentaBancarias
        Implements IPersistenciaBase(Of ChequeraCuentaBancarias)

        Public Function Consultar(filtro As ChequeraCuentaBancarias) As IEnumerable(Of ChequeraCuentaBancarias) Implements IPersistenciaBase(Of ChequeraCuentaBancarias).Consultar
            Return New RepositorioChequeraCuentaBancarias().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ChequeraCuentaBancarias) As Long Implements IPersistenciaBase(Of ChequeraCuentaBancarias).Insertar
            Return New RepositorioChequeraCuentaBancarias().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ChequeraCuentaBancarias) As Long Implements IPersistenciaBase(Of ChequeraCuentaBancarias).Modificar
            Return New RepositorioChequeraCuentaBancarias().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ChequeraCuentaBancarias) As ChequeraCuentaBancarias Implements IPersistenciaBase(Of ChequeraCuentaBancarias).Obtener
            Return New RepositorioChequeraCuentaBancarias().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ChequeraCuentaBancarias) As Boolean
            Return New RepositorioChequeraCuentaBancarias().Anular(entidad)
        End Function
    End Class

End Namespace

