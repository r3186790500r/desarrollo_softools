﻿DROP PROCEDURE gsp_actualizar_documento_solicitud_Servicios        
GO
CREATE PROCEDURE gsp_actualizar_documento_solicitud_Servicios        
(                
@par_EMPR_Codigo SMALLINT,                
@par_ESOS_Codigo NUMERIC = null,                
@par_TIDO_Codigo SMALLINT,                
@par_Numero NUMERIC,                
@par_Numero_Documento NUMERIC,                
@par_ID NUMERIC = null,            
@par_ENRE_Numero NUMERIC = null,            
@par_VEHI_Codigo INT = NULL                
)                
AS                
BEGIN                
                 
 --Orden Cargue                
 IF @par_TIDO_Codigo = 95 BEGIN                
              
   IF (SELECT ENOC_Numero FROM Detalle_Despacho_Orden_Servicios  WHERE EMPR_Codigo = @par_EMPR_Codigo AND ESOS_Numero = @par_ESOS_Codigo AND ID = @par_ID  ) > 0        
   BEGIN        
   SELECT 0 AS Cantidad        
   END        
   ELSE         
   BEGIN        
    UPDATE Encabezado_Solicitud_Orden_Servicios SET CATA_ESSO_Codigo = 9201 WHERE EMPR_Codigo = @par_EMPR_Codigo              
    
    AND Numero = @par_ESOS_Codigo              
              
    UPDATE Detalle_Despacho_Orden_Servicios SET ENOC_Numero = @par_Numero, ENOC_Numero_Documento = @par_Numero_Documento,            
    VEHI_Codigo = @par_VEHI_Codigo                
    WHERE EMPR_Codigo = @par_EMPR_Codigo                
    AND ESOS_Numero = @par_ESOS_Codigo                
    AND ID = @par_ID                
              
    SELECT @@ROWCOUNT As Cantidad              
     
   END        
 END                
                
 --Remesa                
 IF @par_TIDO_Codigo = 100 BEGIN                
      IF (SELECT ENRE_Numero FROM Detalle_Despacho_Orden_Servicios  WHERE EMPR_Codigo = @par_EMPR_Codigo AND ESOS_Numero = @par_ESOS_Codigo AND ID = @par_ID  ) > 0        
   BEGIN        
   SELECT 0 AS Cantidad        
   END        
   ELSE         
   BEGIN        
   UPDATE Encabezado_Solicitud_Orden_Servicios SET CATA_ESSO_Codigo = 9201 WHERE EMPR_Codigo = @par_EMPR_Codigo              
   AND Numero = @par_ESOS_Codigo             
              
   UPDATE Detalle_Despacho_Orden_Servicios SET ENRE_Numero = @par_Numero, ENRE_Numero_Documento = @par_Numero_Documento,            
   VEHI_Codigo = @par_VEHI_Codigo              
   WHERE EMPR_Codigo = @par_EMPR_Codigo                
   AND ESOS_Numero = @par_ESOS_Codigo                
   AND ID = @par_ID                
   SELECT @@ROWCOUNT As Cantidad                
      END                
              
              
 END                
                
 --Planilla Despacho                
 IF @par_TIDO_Codigo = 150     
 BEGIN                
      IF (SELECT ENPD_Numero FROM Detalle_Despacho_Orden_Servicios  WHERE EMPR_Codigo = @par_EMPR_Codigo AND ESOS_Numero = @par_ESOS_Codigo AND ID = @par_ID  ) > 0        
     BEGIN        
     SELECT 0 AS Cantidad        
     END        
    ELSE         
    BEGIN        
   DECLARE @numContinuar SMALLINT = 0            
            
   UPDATE Detalle_Despacho_Orden_Servicios SET ENPD_Numero = @par_Numero, ENPD_Numero_Documento = @par_Numero_Documento      
   WHERE EMPR_Codigo = @par_EMPR_Codigo                
   AND ESOS_Numero = @par_ESOS_Codigo   
   AND ID = @par_ID   
   SELECT @numContinuar = @@ROWCOUNT            
            
   DECLARE @numContDeta NUMERIC = 0            
   DECLARE @numContDetaDespa NUMERIC = 0            
           
   IF @par_ESOS_Codigo > 0      
   BEGIN      
   SELECT @numContDeta = COUNT(1) FROM Detalle_Despacho_Orden_Servicios            
   WHERE EMPR_Codigo = @par_EMPR_Codigo                
   AND ESOS_Numero = @par_ESOS_Codigo                
            
   SELECT @numContDetaDespa = COUNT(1) FROM Detalle_Despacho_Orden_Servicios            
   WHERE EMPR_Codigo = @par_EMPR_Codigo                
   AND ESOS_Numero = @par_ESOS_Codigo            
            
   IF ISNULL(@numContDeta,0) = ISNULL(@numContDetaDespa,0) BEGIN            
   UPDATE Encabezado_Solicitud_Orden_Servicios SET CATA_ESSO_Codigo = 9202 WHERE EMPR_Codigo = @par_EMPR_Codigo              
   AND Numero = @par_ESOS_Codigo and (Saldo_Tipo_Despacho = 0 or Saldo_Tipo_Despacho is null )          
   END            
 END            
           
 SELECT @numContinuar  As Cantidad              
              
                
   END                
 END                
                  
END                
GO