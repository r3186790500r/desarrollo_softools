﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Fachada.ServicioCliente

Namespace ServicioCliente

    Public NotInheritable Class LogicaGestionDocumentalDocumentos
        Inherits LogicaBase(Of GestionDocumentalDocumentos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of GestionDocumentalDocumentos)

        Sub New(persistencia As IPersistenciaBase(Of GestionDocumentalDocumentos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As GestionDocumentalDocumentos) As Respuesta(Of IEnumerable(Of GestionDocumentalDocumentos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of GestionDocumentalDocumentos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of GestionDocumentalDocumentos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As GestionDocumentalDocumentos) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As GestionDocumentalDocumentos) As Respuesta(Of GestionDocumentalDocumentos)
            Throw New NotImplementedException()
        End Function
        Private Function DatosRequeridos(entidad As GestionDocumentalDocumentos) As Respuesta(Of GestionDocumentalDocumentos)
            Throw New NotImplementedException()

        End Function
        Public Function InsertarTemporal(entidad As GestionDocumentalDocumentos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long
            valor = CType(_persistencia, PersistenciaGestionDocumentalDocumentos).InsertarTemporal(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function
        Public Function EliminarDocumento(entidad As GestionDocumentalDocumentos) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaGestionDocumentalDocumentos).EliminarDocumento(entidad)

            If Not resultado Then
                mensaje = String.Format("El documento no se logro eliminar, Contacte con el administrador del sistema")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function

        Public Function EliminarDocumentoDefinitivo(entidad As GestionDocumentalDocumentos) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaGestionDocumentalDocumentos).EliminarDocumentoDefinitivo(entidad)

            If Not resultado Then
                mensaje = String.Format("El documento no se logro eliminar, Contacte con el administrador del sistema")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function

        Public Function LimpiarDocumentoTemporalUsuario(entidad As GestionDocumentalDocumentos) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaGestionDocumentalDocumentos).LimpiarDocumentoTemporalUsuario(entidad)

            If Not resultado Then
                mensaje = String.Format("El documento no se logro eliminar, Contacte con el administrador del sistema")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function

    End Class

End Namespace
