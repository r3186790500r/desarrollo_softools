﻿Print 'gsp_insertar_t_documento_cumplido_despachos'
GO
DROP PROCEDURE gsp_insertar_t_documento_cumplido_despachos
GO
CREATE PROCEDURE gsp_insertar_t_documento_cumplido_despachos (
	
    @par_EMPR_Codigo SMALLINT,
	@par_USUA_Codigo NUMERIC,
	@par_ENCU_Numero NUMERIC,
	@par_Nombre VARCHAR(100) = NULL,
	@par_NombreArchivo VARCHAR(200),
	@par_Archivo	VARBINARY(MAX) = NULL,
	@par_ExtensionArchivo VARCHAR(20),
	@par_Tipo VARCHAR(200) = NULL
	

)
AS
BEGIN



	DECLARE @AuxID NUMERIC = 0
	SELECT @AuxID = ISNULL(MAX(ID), 0) + 1 FROM T_Documento_Cumplido_Despachos
	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND ENCU_Numero = @par_ENCU_Numero

	
	INSERT INTO T_Documento_Cumplido_Despachos (
			EMPR_Codigo,
			ID,
			USUA_Codigo,
			ENCU_Numero,
			Nombre,
			NombreArchivo,
			Archivo,
			ExtensionArchivo,
			Tipo,
			Fecha_Crea
	)
	VALUES (
			@par_EMPR_Codigo,
			@AuxID,
			@par_USUA_Codigo,
			@par_ENCU_Numero,
			@par_Nombre,
			@par_NombreArchivo,
			@par_Archivo,
			@par_ExtensionArchivo,
			@par_Tipo,
			GETDATE()
	)

	SELECT ID As Codigo FROM T_Documento_Cumplido_Despachos
	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND ID = @AuxID

END
GO