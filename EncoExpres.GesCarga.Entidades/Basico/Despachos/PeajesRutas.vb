﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" Rutas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class PeajesRutas
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Rutas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Rutas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Ruta = New Rutas With {.Codigo = Read(lector, "Ruta_Codigo"), .Nombre = Read(lector, "Ruta_Nombre"),
                .CiudadOrigen = New Ciudades With {.Nombre = Read(lector, "CIOR_Nombre")},
                .CiudadDestino = New Ciudades With {.Nombre = Read(lector, "CIDE_Nombre")}}
            Peaje = New Peajes With {.Codigo = Read(lector, "Peaje_Codigo"), .Nombre = Read(lector, "Peaje_Nombre")}
            Estado = Read(lector, "Estado")
            DuracionHoras = Read(lector, "Duracion_Horas")
            Orden = Read(lector, "Orden")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub

        ''' <summary>
        ''' Obtiene o establece el nombre de la ruta
        ''' </summary>
        <JsonProperty>
        Public Property Ruta As Rutas
        ''' <summary>
        ''' Obtiene o establece la ciudad origen de la ruta
        ''' </summary>
        <JsonProperty>
        Public Property Peaje As Peajes
        ''' <summary>
        ''' Obtiene o establece la ciudad destino de la ruta
        ''' </summary>
        <JsonProperty>
        Public Property DuracionHoras As Double
        <JsonProperty>
        Public Property Orden As Integer
        Public Property TipoRuta As Object
    End Class
End Namespace
