module.exports = function (grunt) {

    // Configuración Grunt.
    grunt.initConfig({
        // Concatenar archivos.
        concat: {
            // Concatenar archivos css.
            css: {
                src: [
                    'css/Bootstrap/bootstrap.css',
                    'css/Bootstrap//datepiker.css',
                    'css/Bootstrap/bootstrap-select.css',
                    'css/Angular/angular-block-ui.css',
                    'css/jsTree/style.css',
                    'css/Metro/metro-icons.css',
                    'css/EncoExpres/EncoExpres.css',
                    'css/EncoExpres/sweetalert.css',
                    'css/EncoExpres/select2.css',
                    'css/EncoExpres/_all.css',
                    'css/skins/skin-blue.css',
                    'css/skins/facebook.css',
                    'css/Toastr/toastr.css'
                ],
                dest: 'build/css/app.css' // Archivo css generado.
            },
            // Concatenar archivos js.
            js: {
                src: [
                    'App/Cross/*',
                    'App/Controllers/*.js',
                ],
                // Archivos js generados.
                dest: 'build/js/app.js'
            }           
        },

        // Minificado de archivos css.  
        cssmin: {
            css: {
                src: 'build/css/app.css',
                dest: 'build/css/app.min.css'
            }
        },
        copy: {
            files: {
                cwd: 'css/fonts/',
                src: '**/*',    
                expand: true,
                dest: 'build/fonts/',
            },
        },
        // Minificado de archivos js.
        /*uglify: {     
          js: {
              files: {
                  'build/js/app.min.js': ['build/js/app.js']
              }
          }
        },*/
        // Genración de archivo principal html optimizado para despliegue productivo.
        //processhtml: {
        //    dist: {
        //        files: {
        //            'App/Views/index.html': 'App/Views/Main.html'
        //        }
        //    }
        //}
    });

    // Carga de los plugins.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    //grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-processhtml');

    // Ejecución de todas las tareas.
    grunt.registerTask('default', [
        'concat:css',
        'concat:js',
        'cssmin:css',
        'copy:files',
        //'uglify:js',
        'processhtml:dist'
    ]);
};