﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 05/06/2021 
-- Módulo: Gesphone

PRINT 'gsp_consultar_sitios_cargue_descargue_Autocomplete_gesphone'
GO
DROP PROCEDURE gsp_consultar_sitios_cargue_descargue_Autocomplete_gesphone
GO
CREATE PROCEDURE dbo.gsp_consultar_sitios_cargue_descargue_Autocomplete_gesphone
(                      
	@par_EMPR_Codigo SMALLINT,                                          
	@par_Filtro VARCHAR(100) = NULL,                                          
	@par_Codigo NUMERIC = null,        
	@par_TipoSitio numeric = null,        
	@par_Ciudad numeric = null,
	@par_Estado NUMERIC = NULL                       
)                      
AS                      
BEGIN                      
	IF @par_TipoSitio > 0        
	BEGIN        
		SELECT                 
		0 AS Obtener,                      
		SICD.EMPR_Codigo,                      
		SICD.Codigo,                          
		SICD.Nombre,                     
		SICD.CATA_TSCD_Codigo,                      
		SICD.PAIS_Codigo,                    
		PAIS.Nombre AS Nombre_Pais,                     
		SICD.CIUD_Codigo,                    
		CIUD.Nombre AS Nombre_Ciudad,             
		SICD.ZOCI_Codigo,            
		ZOCI.Nombre AS Nombre_Zona,                   
		SICD.Direccion,      
		SICD.Telefono,      
		SICD.Valor_Cargue,      
		SICD.Valor_Descargue,      
		ISNULL(SICD.Contacto, '') AS Contacto,                    
		SICD.Estado      
        
		FROM                      
		Sitios_Cargue_Descargue AS SICD                       
                    
		LEFT JOIN Valor_Catalogos AS VACA                                       
		ON SICD.EMPR_Codigo = VACA.EMPR_Codigo                                        
		AND SICD.CATA_TSCD_Codigo = VACA.Codigo                      
                    
		LEFT JOIN Paises AS PAIS                                       
		ON SICD.EMPR_Codigo = PAIS.EMPR_Codigo                                        
		AND SICD.PAIS_Codigo = PAIS.Codigo                      
                    
		LEFT JOIN Ciudades AS CIUD                                       
		ON SICD.EMPR_Codigo = CIUD.EMPR_Codigo                                        
		AND SICD.CIUD_Codigo = CIUD.Codigo               
              
		LEFT JOIN Zona_Ciudades ZOCI ON                    
		ZOCI.EMPR_Codigo = SICD.EMPR_Codigo                    
		AND ZOCI.Codigo = SICD.ZOCI_Codigo                   
                    
		WHERE                      
		SICD.Codigo <> 0                    
		AND SICD.EMPR_Codigo = @par_EMPR_Codigo                    
		AND SICD.Codigo = ISNULL(@par_Codigo, SICD.Codigo)                    
		AND ((SICD.Nombre LIKE  '%' + @par_Filtro + '%') OR (@par_Filtro IS NULL))           
		AND SICD.CATA_TSCD_Codigo = ISNULL(@par_TipoSitio,SICD.CATA_TSCD_Codigo)        
		AND SICD.CIUD_Codigo = ISNULL(@par_Ciudad,SICD.CIUD_Codigo)
		AND (SICD.Estado = @par_Estado OR @par_Estado IS NULL)
	END        
	ELSE        
	BEGIN        
		SELECT                  
		0 AS Obtener,                      
		SICD.EMPR_Codigo,                      
		SICD.Codigo,                          
		SICD.Nombre,                     
		SICD.CATA_TSCD_Codigo,                      
		SICD.PAIS_Codigo,                    
		PAIS.Nombre AS Nombre_Pais,                     
		SICD.CIUD_Codigo,                    
		CIUD.Nombre AS Nombre_Ciudad,             
		SICD.ZOCI_Codigo,            
		ZOCI.Nombre AS Nombre_Zona,                   
		SICD.Direccion,      
		SICD.Telefono,      
		SICD.Valor_Cargue,      
		SICD.Valor_Descargue,      
		ISNULL(SICD.Contacto, '') AS Contacto,                    
		SICD.Estado          
		FROM                      
		Sitios_Cargue_Descargue AS SICD                       
                    
		LEFT JOIN Valor_Catalogos AS VACA                                       
		ON SICD.EMPR_Codigo = VACA.EMPR_Codigo                                        
		AND SICD.CATA_TSCD_Codigo = VACA.Codigo                      
                    
		LEFT JOIN Paises AS PAIS                                       
		ON SICD.EMPR_Codigo = PAIS.EMPR_Codigo                                        
		AND SICD.PAIS_Codigo = PAIS.Codigo                      
                    
		LEFT JOIN Ciudades AS CIUD                                       
		ON SICD.EMPR_Codigo = CIUD.EMPR_Codigo                                        
		AND SICD.CIUD_Codigo = CIUD.Codigo               
              
		LEFT JOIN Zona_Ciudades ZOCI ON                    
		ZOCI.EMPR_Codigo = SICD.EMPR_Codigo                    
		AND ZOCI.Codigo = SICD.ZOCI_Codigo             
                    
		WHERE                      
		SICD.Codigo <> 0                    
		AND SICD.EMPR_Codigo = @par_EMPR_Codigo                    
		AND SICD.Codigo = ISNULL(@par_Codigo, SICD.Codigo)                    
		AND ((SICD.Nombre LIKE '%' + @par_Filtro + '%') OR (@par_Filtro IS NULL))           
		--AND SICD.CATA_TSCD_Codigo = ISNULL(@par_TipoSitio,SICD.CATA_TSCD_Codigo)      
		AND SICD.CIUD_Codigo = ISNULL(@par_Ciudad,SICD.CIUD_Codigo)
		AND (SICD.Estado = @par_Estado OR @par_Estado IS NULL)      
	END        
END
GO