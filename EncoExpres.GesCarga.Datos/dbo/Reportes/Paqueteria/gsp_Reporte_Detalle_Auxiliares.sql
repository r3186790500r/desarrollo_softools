﻿PRINT 'gsp_Reporte_Detalle_Auxiliares'
GO
DROP PROCEDURE gsp_Reporte_Detalle_Auxiliares
GO
CREATE PROCEDURE gsp_Reporte_Detalle_Auxiliares(  
@par_EMPR_Codigo NUMERIC,    
@par_Numero_Planilla  NUMERIC
) 
  
  AS        
  BEGIN  
  SELECT DISTINCT
      
 DAPR.EMPR_Codigo AS EMPR_Codigo_Auxiliares
,DAPR.ENPR_Numero AS ENPR_Numero_Auxiliares
,ISNULL(TFUN.Nombre,'') + ' ' + ISNULL(TFUN.Apellido1, '')+ ' ' + ISNULL(TFUN.Apellido2,'')+ ' ' + ISNULL(TFUN.Razon_Social,'') AS Nombre_Funcionario_Auxiliares   
,DAPR.TERC_Codigo_Funcionario AS TERC_Codigo_Funcionario_Auxiliares 
,DAPR.Numero_Horas_Trabajadas AS Numero_Horas_Trabajadas_Auxiliares 
,DAPR.Valor AS Valor_Auxiliares 
,DAPR.Observaciones AS Observaciones_Auxiliares   
               
  FROM         
    	  
      Detalle_Auxiliares_Planilla_Recolecciones DAPR,
	  Encabezado_Planilla_Recolecciones ENPR,
	  Terceros TFUN
	 
  WHERE

     DAPR.EMPR_Codigo = ENPR.EMPR_Codigo                              
	 
	 AND DAPR.EMPR_Codigo = TFUN.EMPR_Codigo                      
     AND DAPR.TERC_Codigo_Funcionario = TFUN.Codigo 
	 	 
	 AND DAPR.EMPR_Codigo = @par_EMPR_Codigo  
     AND DAPR.ENPR_Numero = @par_Numero_Planilla     
      
END  
GO