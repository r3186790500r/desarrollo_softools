﻿EncoExpresApp.controller("GestionarReferenciasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ReferenciasFactory', 'GrupoReferenciasFactory', 'UnidadEmpaqueReferenciasFactory', 'UnidadMedidaReferenciasFactory', 'PlanUnicoCuentasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ReferenciasFactory, GrupoReferenciasFactory, UnidadEmpaqueReferenciasFactory, UnidadMedidaReferenciasFactory, PlanUnicoCuentasFactory) {

        $scope.Titulo = 'GESTIONAR REFERENCIAS';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Almacén' }, { Nombre: 'Referencias' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_REFERENCIAS);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.MaxLength = '';
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,
            Referencia: '',
        }

        $scope.ListadoGrupoReferencias = [];
        $scope.ListadoUnidadMedida = [];
        $scope.ListadoUnidadEmpaques = [];
        $scope.ListadoCuentasPUC = [];

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVA" },
            { Codigo: 1, Nombre: "ACTIVA" },
        ]
       
        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;      
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }

        $scope.LongitudCuentaPUC = 0;// verifica la cantidad de datos ingresados  de la ciudad
        $scope.IngresoValidaCuentaPUC = true

        $scope.AsignarCuentaPUC = function (CuentaPUC) {
            $scope.IngresoValidaCuentaPUC = true
            if (CuentaPUC != undefined && CuentaPUC != null && CuentaPUC != "" && CuentaPUC.Codigo != 0) {
                if (angular.isObject(CuentaPUC)) {
                    $scope.LongitudCuentaPUC = CuentaPUC.length;
                    $scope.IngresoValidaCuentaPUC = true;
                    $scope.NombreCuentaPUC = CuentaPUC.Nombre
                }
                else if (angular.isString(CuentaPUC)) {
                    $scope.LongitudCuentaPUC = CuentaPUC.length;
                    $scope.IngresoValidaCuentaPUC = false;
                    $scope.NombreCuentaPUC = ''
                }
            } else {
                $scope.NombreCuentaPUC = ''
            }

        };

    /*Cargar el combo de grupo referencias*/
        GrupoReferenciasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoGrupoReferencias =[]
                    $scope.ListadoGrupoReferencias = response.data.Datos;

                    if ($scope.GrupoReferencias > 0) {
                        $scope.Modelo.GrupoReferencias = $linq.Enumerable().From($scope.ListadoGrupoReferencias).First('$.Codigo ==' + $scope.GrupoReferencias);
                    } else {
                        $scope.Modelo.GrupoReferencias = $scope.ListadoGrupoReferencias[0]
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de unidad empaque*/
        UnidadEmpaqueReferenciasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoUnidadEmpaques = [];
                    $scope.ListadoUnidadEmpaques = response.data.Datos;

                    if ($scope.UnidadEmpaque > 0) {
                        $scope.Modelo.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaques).First('$.Codigo ==' + $scope.UnidadEmpaque);
                    } else {
                        $scope.Modelo.UnidadEmpaque = $scope.ListadoUnidadEmpaques[0]
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de unidad medida*/
        UnidadMedidaReferenciasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoUnidadMedida = [];
                    $scope.ListadoUnidadMedida = response.data.Datos;

                    if ($scope.UnidadMedida > 0) {
                        $scope.Modelo.UnidadMedida = $linq.Enumerable().From($scope.ListadoUnidadMedida).First('$.Codigo ==' + $scope.UnidadMedida);
                    } else {
                        $scope.Modelo.UnidadMedida = $scope.ListadoUnidadMedida[0]
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar Autocomplete de Cuentas PUC*/
        PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
         then(function (response) {
             if (response.data.ProcesoExitoso === true) {
                 if (response.data.Datos.length > 0) {
                     response.data.Datos.push({ CodigoCuenta: '', Codigo: 0 })
                     $scope.ListadoCuentasPUC = response.data.Datos;

                     if ($scope.CuentaPUC > 0) {
                         $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CuentaPUC);
                         $scope.NombreCuentaPUC = $scope.Modelo.CuentaPUC.Nombre
                     } else {
                         $scope.Modelo.CuentaPUC = '';
                     }
                 }
                 else {
                     $scope.ListadoCuentasPUC = [];
                     $scope.Modelo.CuentaPUC = null;
                 }
             }
         }, function (response) {
             ShowError(response.statusText);
         });

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando referencia código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando referencia Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            ReferenciasFactory.Obtener(filtros).
               then(function (response) {
                   if (response.data.ProcesoExitoso === true) {

                       $scope.Modelo = response.data.Datos;
                       $scope.Modelo.Codigo = response.data.Datos.Codigo;

                       $scope.UnidadMedida = response.data.Datos.UnidadMedida.Codigo;
                       if ($scope.ListadoUnidadMedida.length > 0 && $scope.UnidadMedida > 0) {
                           $scope.Modelo.UnidadMedida = $linq.Enumerable().From($scope.ListadoUnidadMedida).First('$.Codigo ==' + response.data.Datos.UnidadMedida.Codigo);
                       }

                       $scope.UnidadEmpaque = response.data.Datos.UnidadEmpaque.Codigo;
                       if ($scope.ListadoUnidadEmpaques.length > 0 && $scope.UnidadEmpaque > 0) {
                           $scope.Modelo.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaques).First('$.Codigo ==' + response.data.Datos.UnidadEmpaque.Codigo);
                       }


                       $scope.GrupoReferencias = response.data.Datos.GrupoReferencias.Codigo;
                       if ($scope.ListadoGrupoReferencias.length > 0 && $scope.GrupoReferencias > 0) {
                           $scope.Modelo.GrupoReferencias = $linq.Enumerable().From($scope.ListadoGrupoReferencias).First('$.Codigo ==' + response.data.Datos.GrupoReferencias.Codigo);
                       }
                       $scope.CuentaPUC = response.data.Datos.CuentaPUC.Codigo;
                       if ($scope.ListadoCuentasPUC.length > 0 && $scope.CuentaPUC > 0) {
                           $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + response.data.Datos.CuentaPUC.Codigo);
                           $scope.NombreCuentaPUC = $scope.Modelo.CuentaPUC.Nombre
                       }

                        
                       $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                   }
                   else {
                       ShowError('No se logro consultar la referencia código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                       document.location.href = '#!ConsultarReferencias';
                   }
               }, function (response) {
                   ShowError(response.statusText);
                   document.location.href = '#!ConsultarReferencias';
               });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
                $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                $scope.Modelo.PorcentajeIva = parseFloat($scope.Modelo.PorcentajeIva);
                ReferenciasFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó la referencia "' + $scope.Modelo.Nombre + '"');
                                    $scope.Modelo.Codigo = response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó la referencia "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = '#!ConsultarReferencias/' + $scope.Modelo.Codigo;
                            }                          
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        
        
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            
            if ($scope.Modelo.CodigoReferencia == undefined || $scope.Modelo.CodigoReferencia == '' || $scope.Modelo.CodigoReferencia == null) {
                $scope.MensajesError.push('Debe ingresar el código de la Referencia');
                continuar = false;
            }

            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
                $scope.MensajesError.push('Debe ingresar el nombre de la Referencia');
                continuar = false;
            }
            if ($scope.Modelo.Marca == undefined || $scope.Modelo.Marca == '') {
                $scope.MensajesError.push('Debe ingresar el marca de la Referencia');
                continuar = false;
            }
            if ($scope.Modelo.UnidadMedida == undefined || $scope.Modelo.UnidadMedida == '' || $scope.Modelo.UnidadMedida.Codigo == 0 || $scope.Modelo.UnidadMedida == null) {
                $scope.MensajesError.push('Debe seleccionar la unidad de medida de la referencia');
                continuar = false;
            }
            if ($scope.Modelo.UnidadEmpaque == undefined || $scope.Modelo.UnidadEmpaque == '' || $scope.Modelo.UnidadEmpaque.Codigo == 0 || $scope.Modelo.UnidadEmpaque == null) {
                $scope.MensajesError.push('Debe seleccionar la unidad empaque de la referencia');
                continuar = false;
            }
            if ($scope.Modelo.GrupoReferencias == undefined || $scope.Modelo.GrupoReferencias == '' || $scope.Modelo.GrupoReferencias.Codigo == 0 || $scope.Modelo.GrupoReferencias == null) {
                $scope.MensajesError.push('Debe seleccionar el grupo de la referencia');
                continuar = false;
            }
            if ($scope.Modelo.Descripcion == undefined || $scope.Modelo.Descripcion == null) {
                $scope.Modelo.Descripcion = ""
            }
            if ($scope.Modelo.PorcentajeIva == undefined || $scope.Modelo.PorcentajeIva == null) {
                $scope.Modelo.PorcentajeIva = 0
            }
            if ($scope.IngresoValidaCuentaPUC == false) {
                $scope.MensajesError.push('Debe ingresar una cuenta PUC valida');
                continuar = false;
            }
            if ($scope.Modelo.CuentaPUC == undefined || $scope.Modelo.CuentaPUC == '' || $scope.Modelo.CuentaPUC == null) {
                $scope.Modelo.CuentaPUC = { Codigo: 0, CodigoCuenta: '', Nombre: '' }
            }

            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarReferencias/' + $scope.Modelo.Codigo;
        };
        $scope.MaskMayus = function () {
            // try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
            MascaraMayusGeneral($scope);
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
            try { $scope.Modelo.PorcentajeIva = MascaraNumero($scope.Modelo.PorcentajeIva) } catch (e) { }
        };



        $scope.MaskPorcentaje = function (value) {
            var numeroarray = [];
            for (var i = 0; i < value.length; i++) {
                patron = /[0123456789.]/
                if (i == 0 && value[i] == '-') {
                    numeroarray.push(value[i])
                }
                else if (patron.test(value[i])) {
                    if (patron.test(value[i]) == ' ') {
                        option[i] = '';
                    } else {
                        numeroarray.push(value[i])
                    }
                }
            }

            var val = numeroarray.join('');
            $scope.Modelo.PorcentajeIva = val;
            var nval = [];
            if (val[1] != '.') {
                if (val.length == 3) {
                    if (val[0] != 1 && val[0] != "1" && val[2] != ".") {
                        $scope.MaxLength = 3;
                        nval.push(val[0]);
                        nval.push(val[1]);
                        $scope.Modelo.PorcentajeIva = nval.join('');
                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.PorcentajeIva = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.PorcentajeIva = nval.join('');
                        }
                    } else if (val[2] == ".") {
                        $scope.MaxLength = 5;
                    }
                    else if ((val[0] == 1 || val[0] == "1") && val[2] != ".") {
                        $scope.MaxLength = 3;

                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.PorcentajeIva = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.PorcentajeIva = nval.join('');
                        }
                    }

                }
            } else {
                $scope.MaxLength = 4;
            }
        }
}]);