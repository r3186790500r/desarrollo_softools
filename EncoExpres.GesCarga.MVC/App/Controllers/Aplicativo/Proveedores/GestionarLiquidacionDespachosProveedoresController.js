﻿EncoExpresApp.controller("GestionarLiquidacionDespachosProveedoresCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'LiquidacionDespachosProveedoresFactory', 'blockUI', 'PlanillasDespachosProveedoresFactory', 'TercerosFactory', 'blockUIConfig', 'TipoDocumentosFactory', 'ImpuestosFactory','blockUI','PlanUnicoCuentasFactory',
    function ($scope, $routeParams, $timeout, $linq, LiquidacionDespachosProveedoresFactory, blockUI, PlanillasDespachosProveedoresFactory, TercerosFactory, blockUIConfig, TipoDocumentosFactory, ImpuestosFactory, blockUI, PlanUnicoCuentasFactory) {
        $scope.MapaSitio = [{ Nombre: 'Proveedores' }, { Nombre: 'Documentos' }, { Nombre: 'Liquidación Despachos Proveedores' }, { Nombre: 'Gestionar' }];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LIQUIDACION_DESPACHOS_PROVEEDORES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.ListadosImpuestosPlanillasProveedores = [];
        $scope.deshabililitarTransportador = false;
        $scope.Modelo = {
            Numero : 0,
            NumeroDocumento: '',
            Fecha: new Date(),
            Transportador: '',
            Observaciones: '',
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            ValorOtrosConceptos : ''
        };

        $scope.ModeloProveedores = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            FechaInicial: new Date(),
            FechaFinal: new Date(),
            Numero: '',
            Estado: 1,
            Liquidacion : 1
        };

        $scope.ListadoImpuestosPlanilla = TipoDocumentosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_PROVEEDORES, Estado: 1, Sync: true }).Datos.ListadoImpuestos;
        $scope.ListadoImpuestosPlanilla.forEach(item => {
            item = ImpuestosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.CodigoImpuesto,Pagina: 1, RegistrosPagina: 10, Sync: true }).Datos;
            var tarifa = 0;
            if (item.Tipo_Recaudo.Codigo == 803/*Ciudad*/) {
                item.ListadoCiudades.forEach(itemCiudad => {
                    if (itemCiudad.Codigo == $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo) {
                        tarifa = itemCiudad.ValorTarifa;
                    }
                });
                item.Valor_tarifa = tarifa;
            }
            $scope.ListadosImpuestosPlanillasProveedores.push(item);
        });

        $scope.ListadoLiquidacionesProveedores = [];
        $scope.ListadoLiquidacionesProveedoresGrid = [];
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.SeleccionarTodos = false;
        //Paginación Planillas Pendientes:
        $scope.PrimerPagina = function () {
            if ($scope.paginaActual > 1) {
                if ($scope.ListadoLiquidacionesProveedores.length > 0) {
                    $scope.paginaActual = 1;
                    $scope.ListadoLiquidacionesProveedoresGrid = [];
                    var i = 0;
                    for (i = 0; i <= $scope.ListadoLiquidacionesProveedores.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina) {
                            $scope.ListadoLiquidacionesProveedoresGrid.push($scope.ListadoLiquidacionesProveedores[i])
                        }
                    }
                }
            } else if ($scope.ListadoLiquidacionesProveedores.length > 0) {

                $scope.paginaActual = 1;
                $scope.ListadoLiquidacionesProveedoresGrid = [];
                var i = 0;
                for (i = 0; i <= $scope.ListadoLiquidacionesProveedores.length - 1; i++) {
                    if (i < $scope.cantidadRegistrosPorPagina) {
                        $scope.ListadoLiquidacionesProveedoresGrid.push($scope.ListadoLiquidacionesProveedores[i])
                    }
                }

            }
        };
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.ListadoLiquidacionesProveedoresGrid = [];
                $scope.PaginaAuxiliar = $scope.paginaActual;
                $scope.paginaActual += 1;
                if ($scope.ListadoLiquidacionesProveedores.length > 0) {
                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoLiquidacionesProveedores.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoLiquidacionesProveedoresGrid.push($scope.ListadoLiquidacionesProveedores[i]);
                        }
                    }
                } /*else {
                    if ($scope.ListadoRecoleccionesTotal.length > 0) {
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) {
                                $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i]);
                            }
                        }
                    }
                }*/
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                if ($scope.ListadoLiquidacionesProveedores.length > 0) {
                    $scope.ListadoLiquidacionesProveedoresGrid = [];
                    $scope.paginaActual -= 1;
                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListadoLiquidacionesProveedores.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoLiquidacionesProveedoresGrid.push($scope.ListadoLiquidacionesProveedores[i]);
                        }
                    }
                } /*else {
                    if ($scope.ListadoRecoleccionesTotal.length > 0) {
                        $scope.ListaRecoleccionesGrid = [];
                        $scope.paginaActualRecolecciones -= 1;
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) {
                                $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i]);
                            }
                        }
                    }
                }*/
            }
        };
        $scope.UltimaPagina = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                if ($scope.ListadoLiquidacionesProveedores.length > 0) {
                    if ($scope.ListadoLiquidacionesProveedores.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListadoLiquidacionesProveedoresGrid = [];
                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListadoLiquidacionesProveedores.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoLiquidacionesProveedoresGrid.push($scope.ListadoLiquidacionesProveedores[i])
                            }
                        }
                    }
                } /*else {
                    if ($scope.ListadoRecoleccionesTotal.length > 0) {
                        $scope.paginaActualRecolecciones = $scope.totalPaginas;
                        $scope.ListaRecoleccionesGrid = [];
                        var i = 0;
                        for (i = ($scope.paginaActualRecolecciones * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) {
                                $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i])
                            }
                        }
                    }
                }*/
            }
        }

        $scope.AutocompleteTransportadores = function (value) {
            if (value.length > 0) {

                blockUIConfig.autoBlock = false;
                var Response = TercerosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CadenaPerfiles: PERFIL_EMPRESA_TRANSPORTADORA,
                    ValorAutocomplete: value,
                    Sync: true
                });
                $scope.ListaTransportadores = Response.Datos;

            }
            return $scope.ListaTransportadores
        }

        $scope.BuscarPlanillas = function () {
            if (DatosRequeridosPlanillas()) {
                PlanillasDespachosProveedoresFactory.Consultar($scope.ModeloProveedores).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoLiquidacionesProveedores = response.data.Datos;
                                $scope.ListadoLiquidacionesProveedores.forEach(item => {
                                    item.ValorTotalFlete = item.FleteTransportador;
                                    item.Seleccionado = false;
                                });
                                $scope.totalRegistros = $scope.ListadoLiquidacionesProveedores.length;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.PrimerPagina();
                                $scope.Buscando = true;
                            } else {
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.totalRegistros = 0;
                                $scope.Buscando = false;
                            }
                        }
                    });
            }
        }

        function DatosRequeridosPlanillas() {
            $scope.MensajesError = [];
            var Continuar = true;
            if ($scope.Modelo.Transportador == undefined || $scope.Modelo.Transportador == null || $scope.Modelo.Transportador == '') {
                $scope.MensajesError.push('Debe seleccionar un transportador');
                Continuar = false;
            } else {
                $scope.ModeloProveedores.Transportador = { Nombre: $scope.Modelo.Transportador.NombreCompleto }
            }
            return Continuar;
        }

        $scope.ValidarSeleccion = function (SeleccionarTodos) {
            $scope.SeleccionarTodos = SeleccionarTodos
            if ($scope.SeleccionarTodos) {
                $scope.ListadoLiquidacionesProveedores.forEach(item => {
                    item.Seleccionado = true
                });
            } else {
                $scope.ListadoLiquidacionesProveedores.forEach(item => {
                    item.Seleccionado = false
                });
            }
            $scope.CalcularTotales();
        }

        $scope.CalcularTotales = function () {
            $scope.Modelo.Cantidad = 0;
            $scope.Modelo.Peso = 0;
            $scope.Modelo.FleteTransportador = 0;
            $scope.Modelo.AnticipoTransportador = 0;
            $scope.Modelo.ValorImpuestos = 0;
            $scope.Modelo.ValorPagar = 0;
            $scope.ListadoLiquidacionesProveedores.forEach(item => {
                if (item.Seleccionado) {
                    $scope.Modelo.Cantidad += MascaraNumero(item.Cantidad);
                    $scope.Modelo.Peso += MascaraNumero(item.Peso);
                    $scope.Modelo.FleteTransportador += MascaraNumero(item.ValorTotalFlete);
                    $scope.Modelo.AnticipoTransportador += MascaraNumero(item.AnticipoTransportador);
                }
            });
            var Otrosconceptos = 0;
            if ($scope.Modelo.ValorOtrosConceptos != null && $scope.Modelo.ValorOtrosConceptos != '' && !isNaN(MascaraNumero($scope.Modelo.ValorOtrosConceptos))) {
                Otrosconceptos = MascaraNumero($scope.Modelo.ValorOtrosConceptos);
            }

            $scope.Modelo.Total = MascaraValores($scope.Modelo.FleteTransportador - $scope.Modelo.AnticipoTransportador + Otrosconceptos);
            $scope.Modelo.ValorBaseImpuestos = MascaraValores($scope.Modelo.FleteTransportador + Otrosconceptos);
            $scope.ListadosImpuestosPlanillasProveedores.forEach(item => {
                item.ValorBase = MascaraNumero($scope.Modelo.ValorBaseImpuestos);
                item.ValorImpuesto = item.ValorBase * item.Valor_tarifa;
                item.ValorImpuesto = item.ValorImpuesto.toFixed(2);
                if (item.Operacion == 2/*Resta*/) {
                    $scope.Modelo.ValorImpuestos -= item.ValorImpuesto;
                } else {
                    $scope.Modelo.ValorImpuestos += item.ValorImpuesto;
                }
                item.ValorImpuesto = parseInt(item.ValorImpuesto);
            });

            var impuestos = $scope.Modelo.ValorImpuestos;
            if ($scope.Modelo.ValorImpuestos < 0) {
                $scope.Modelo.ValorImpuestos = $scope.Modelo.ValorImpuestos * (-1);
            }

            $scope.Modelo.ValorPagar = MascaraNumero($scope.Modelo.Total) + impuestos;
            MascaraValores($scope.Modelo.Cantidad);
            MascaraValores($scope.Modelo.Peso);
            MascaraValores($scope.Modelo.FleteTransportador);
            MascaraValores($scope.Modelo.AnticipoTransportador);
            MascaraValores($scope.Modelo.ValorOtrosConceptos);
            MascaraValores($scope.Modelo.Total);
            MascaraValores($scope.Modelo.ValorBaseImpuestos);
            MascaraValores($scope.Modelo.ValorImpuestos);
            MascaraValores($scope.Modelo.ValorPagar);
            $scope.MaskValores();
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarLiquidacionPlanillasProveedores/' + $scope.Modelo.NumeroDocumento;
        };

        $scope.ListaEstados = [
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'DEFINITIVO', Codigo: 1 }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo==0')

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                $scope.ListadoPlanillasGuardar = [];
                $scope.ListadoLiquidacionesProveedores.forEach(item => {
                    if (item.Seleccionado) {
                        $scope.ListadoPlanillasGuardar.push(item);
                    }
                })
                $scope.Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Modelo.Numero,
                    Fecha: $scope.Modelo.Fecha,
                    Transportador: { Codigo: $scope.Modelo.Transportador.Codigo },
                    Observaciones: $scope.Modelo.Observaciones,
                    ListadoPlanillas: $scope.ListadoPlanillasGuardar,
                    ListadoImpuestos: $scope.ListadosImpuestosPlanillasProveedores,
                    Cantidad: MascaraNumero($scope.Modelo.Cantidad),
                    Peso: MascaraNumero($scope.Modelo.Peso),
                    FleteTransportador: MascaraNumero($scope.Modelo.FleteTransportador),
                    AnticipoTransportador: MascaraNumero($scope.Modelo.AnticipoTransportador),
                    ValorOtrosConceptos: MascaraNumero($scope.Modelo.ValorOtrosConceptos),
                    Total: MascaraNumero($scope.Modelo.Total),
                    ValorBaseImpuestos: MascaraNumero($scope.Modelo.ValorBaseImpuestos),
                    ValorImpuestos: MascaraNumero($scope.Modelo.ValorImpuestos),
                    ValorPagar: MascaraNumero($scope.Modelo.ValorPagar),
                    Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                    Estado: $scope.Modelo.Estado.Codigo,
                    CodigoUsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }

                if ($scope.Sesion.UsuarioAutenticado.ManejoCuentasPorCobrarPagarLiquidacionesPlanilla) {
                    $scope.Entidad.CuentaPorPagar = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                        CodigoAlterno: '',
                        Fecha: $scope.Modelo.Fecha,
                        Tercero: { Codigo: $scope.Modelo.Transportador == undefined ? 0 : $scope.Modelo.Transportador.Codigo },
                        DocumentoOrigen: { Codigo: 2612 },
                        CodigoDocumentoOrigen: 2612,
                        FechaDocumento: $scope.Modelo.Fecha,
                        FechaVenceDocumento: $scope.Modelo.Fecha,
                        Numeracion: '',
                        CuentaPuc: { Codigo: PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoCuenta: '22050100', Estado: 1, Sync: true }).Datos[0].Codigo },
                        ValorTotal: $scope.Entidad.ValorPagar,
                        Abono: 0,
                        Saldo: $scope.Entidad.ValorPagar,
                        FechaCancelacionPago: $scope.Modelo.Fecha,
                        Aprobado: 1,
                        UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        FechaAprobo: $scope.Modelo.Fecha,
                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        Vehiculo: { Codigo: 0 },
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    }
                }

                LiquidacionDespachosProveedoresFactory.Guardar($scope.Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if ($scope.Modelo.Numero > 0) {
                                ShowSuccess('Se modificó la planilla despachos proveedores con el número ' + $scope.Modelo.NumeroDocumento + '');
                                location.href = '#!ConsultarLiquidacionPlanillasProveedores/' + $scope.Modelo.NumeroDocumento;
                            } else {
                                ShowSuccess('Se guardó la planilla despachos proveedores con el número ' + response.data.Datos + '');
                                location.href = '#!ConsultarLiquidacionPlanillasProveedores/' + response.data.Datos;
                            }

                        } else {
                            console.log(response.statusText);
                        }
                    }, function (response) {
                            console.log(response.statusText);
                    }
                    );
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == null || $scope.Modelo.Fecha == '') {
                $scope.MensajesError.push('Debe seleccionar una fecha');
                continuar = false;
            }
            if ($scope.Modelo.Transportador == undefined || $scope.Modelo.Transportador == null || $scope.Modelo.Transportador == '') {
                $scope.MensajesError.push('Debe seleccionar un Transportador');
                continuar = false;
            }
            if ($scope.ListadoLiquidacionesProveedores == undefined || $scope.ListadoLiquidacionesProveedores == null || $scope.ListadoLiquidacionesProveedores == '') {
                scope.MensajesError.push('Debe seleccionar al menos una planilla');
                continuar = false;
            } else {
                var seleccionado = false;
                $scope.ListadoLiquidacionesProveedores.forEach(item => {
                    if (item.Seleccionado) {
                        seleccionado = true;
                    }
                });

                if (!seleccionado) {
                    $scope.MensajesError.push('Debe seleccionar al menos una planilla');
                    continuar = false;
                }
            }
            return continuar;
        }

        function Obtener() {
            blockUI.start('Cargando Planilla Despachos...');

            $timeout(function () {
                blockUI.message('Cargando Planilla Despachos...');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
            };

            blockUI.delay = 1000;

            LiquidacionDespachosProveedoresFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.Datos.Anulado == 1 || response.data.Datos.Estado == 1) {
                        $scope.DeshabilitarActualizar = true;
                    }
                    $scope.ListadoImpuestosPlanilla = [];
                    $scope.Modelo = response.data.Datos;
                    $scope.Modelo.Transportador = $scope.CargarTercero($scope.Modelo.Transportador.Codigo)
                    $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo ==' + $scope.Modelo.Estado);

                    $scope.ListadoLiquidacionesProveedores = response.data.Datos.ListadoPlanillas;
                    $scope.ListadoImpuestosPlanilla = response.data.Datos.ListadoImpuestos;

                    $scope.ListadoLiquidacionesProveedores.forEach(item => {
                        item.Seleccionado = true;

                    });
                    $scope.totalRegistros = $scope.ListadoLiquidacionesProveedores.length;
                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);

                    $scope.ListadoImpuestosPlanilla.forEach(item => {
                        item.Nombre = ImpuestosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Codigo, Pagina: 1, RegistrosPagina: 10, Sync: true }).Datos.Nombre;
                    });

                    $scope.Modelo = {
                        Numero: $scope.Modelo.Numero,
                        NumeroDocumento: $scope.Modelo.NumeroDocumento,
                        Fecha: new Date($scope.Modelo.Fecha),
                        Transportador: $scope.CargarTercero($scope.Modelo.Transportador.Codigo),
                        Observaciones: $scope.Modelo.Observaciones,
                        ListadoPlanillas: $scope.ListadoLiquidacionesProveedores,
                        ListadoImpuestos: $scope.ListadosImpuestosPlanillasProveedores,
                        Cantidad: MascaraValores($scope.Modelo.Cantidad),
                        Peso: MascaraValores($scope.Modelo.Peso),
                        FleteTransportador: MascaraValores($scope.Modelo.FleteTransportador),
                        //AnticipoTransportador: MascaraValores($scope.Modelo.AnticipoTransportador),
                        ValorOtrosConceptos: MascaraValores($scope.Modelo.ValorOtrosConceptos),
                        Total: MascaraValores(MascaraNumero($scope.Modelo.FleteTransportador) - MascaraNumero($scope.Modelo.AnticipoTransportador) + MascaraNumero($scope.Modelo.ValorOtrosConceptos)),
                        ValorBaseImpuestos: MascaraValores($scope.Modelo.ValorBaseImpuestos),
                        ValorImpuestos: MascaraValores($scope.Modelo.ValorImpuestos),
                        ValorPagar: MascaraValores($scope.Modelo.ValorPagar),
                        Estado: $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo ==' + $scope.Modelo.Estado.Codigo),
                        Oficina: {Codigo: $scope.Modelo.Oficina.Codigo, Nombre:$scope.Modelo.Oficina.Nombre}
                    }

                    $scope.ListadoLiquidacionesProveedores.forEach(item => {
                        $scope.Modelo.AnticipoTransportador += item.AnticipoTransportador
                    });

                    $scope.CalcularTotales();

                    $scope.PrimerPagina();


                });
            blockUI.stop();
        }

        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        }

        if ($routeParams.Numero > CERO) {
            $scope.Modelo.Numero = $routeParams.Numero;
            Obtener();
            $scope.deshabililitarTransportador = true;
        }

    }

]);