﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Negocio.Mantenimiento

<Authorize>
Public Class EstadoEquipoMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EstadoEquipoMantenimiento) As Respuesta(Of IEnumerable(Of EstadoEquipoMantenimiento))
        Return New LogicaEstadoEquipoMantenimiento(CapaPersistenciaEstadoEquipoMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EstadoEquipoMantenimiento) As Respuesta(Of EstadoEquipoMantenimiento)
        Return New LogicaEstadoEquipoMantenimiento(CapaPersistenciaEstadoEquipoMantenimiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EstadoEquipoMantenimiento) As Respuesta(Of Long)
        Return New LogicaEstadoEquipoMantenimiento(CapaPersistenciaEstadoEquipoMantenimiento).Guardar(entidad)
    End Function
End Class