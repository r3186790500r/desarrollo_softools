﻿Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Fachada.Basico.General
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios


Namespace Basico.General
    Public NotInheritable Class LogicaTerceros
        Inherits LogicaBase(Of Terceros)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Terceros)

        Sub New(persistencia As IPersistenciaBase(Of Terceros))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Terceros) As Respuesta(Of IEnumerable(Of Terceros))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Terceros))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Terceros))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Terceros) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function GuardarNovedad(entidad As Terceros) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            Dim consulta = CType(_persistencia, PersistenciaTerceros).GuardarNovedad(entidad)

            If consulta.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function


        Public Overrides Function Obtener(filtro As Terceros) As Respuesta(Of Terceros)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Terceros)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Terceros)(consulta)
        End Function

        Public Function InsertarDirecciones(entidad As TerceroDirecciones) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Inserto As Boolean = False

            Inserto = CType(_persistencia, PersistenciaTerceros).InsertarDirecciones(entidad)

            If Not Inserto Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Tercero.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function ConsultarTerceroGeneral(filtro As Tercero) As Respuesta(Of IEnumerable(Of Tercero))
            Dim consulta = CType(_persistencia, PersistenciaTerceros).ConsultarTerceroGeneral(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Tercero))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Tercero))(consulta)
        End Function

        Public Function ObtenerTerceroEstudioSeguridad(filtro As EstudioSeguridad) As Respuesta(Of EstudioSeguridad)

            Dim consulta = CType(_persistencia, PersistenciaTerceros).ConsultarTerceroEstudioSeguridad(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EstudioSeguridad)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EstudioSeguridad)(consulta)
        End Function

        Public Function ConsultarDocumentosProximosVencer(filtro As TercerosDocumentos) As Respuesta(Of IEnumerable(Of TercerosDocumentos))
            Dim consulta = CType(_persistencia, PersistenciaTerceros).ConsultarDocumentosProximosVencer(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of TercerosDocumentos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of TercerosDocumentos))(consulta)
        End Function

        Public Function ConsultarDocumentos(filtro As TercerosDocumentos) As Respuesta(Of IEnumerable(Of TercerosDocumentos))
            Dim consulta = CType(_persistencia, PersistenciaTerceros).ConsultarDocumentos(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of TercerosDocumentos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of TercerosDocumentos))(consulta)
        End Function
        Public Function GenerarPlanitilla(entidad As Terceros) As Respuesta(Of Terceros)
            Dim mensajeGuardo = "Genero Archivo"
            Dim url As New Terceros
            url = CType(_persistencia, PersistenciaTerceros).GenerarPlanitilla(entidad)
            Return New Respuesta(Of Terceros)(url)
        End Function
        Public Function ConsultarAuditoria(filtro As Tercero) As Respuesta(Of IEnumerable(Of Tercero))
            Dim consulta = CType(_persistencia, PersistenciaTerceros).ConsultarAuditoria(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Tercero))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Tercero))(consulta)
        End Function
    End Class

End Namespace