﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente
Imports EncoExpres.GesCarga.Repositorio.Basico.ServicioCliente

Namespace Basico.ServicioCliente
    Public NotInheritable Class PersistenciaTarifaTransporteCarga
        Implements IPersistenciaBase(Of TarifaTransporteCarga)

        Public Function Consultar(filtro As TarifaTransporteCarga) As IEnumerable(Of TarifaTransporteCarga) Implements IPersistenciaBase(Of TarifaTransporteCarga).Consultar
            Return New RepositorioTarifaTransporteCarga().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TarifaTransporteCarga) As Long Implements IPersistenciaBase(Of TarifaTransporteCarga).Insertar
            Return New RepositorioTarifaTransporteCarga().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TarifaTransporteCarga) As Long Implements IPersistenciaBase(Of TarifaTransporteCarga).Modificar
            Return New RepositorioTarifaTransporteCarga().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TarifaTransporteCarga) As TarifaTransporteCarga Implements IPersistenciaBase(Of TarifaTransporteCarga).Obtener
            Return New RepositorioTarifaTransporteCarga().Obtener(filtro)
        End Function

    End Class

End Namespace

