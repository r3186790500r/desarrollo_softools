﻿Print 'V_Tipo_Contrato_Conductores'
GO
DROP VIEW V_Tipo_Contrato_Conductores
GO
CREATE VIEW V_Tipo_Contrato_Conductores 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 17
GO