﻿EncoExpresApp.controller("GestionarConfiguracionServidorCorreosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ConfiguracionServidorCorreosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, ConfiguracionServidorCorreosFactory) {

    $scope.Titulo = 'GESTIONAR CONFIGURACIÓN SERVIDOR CORREOS';
    $scope.MapaSitio = [{ Nombre: 'Utilitarios' }, { Nombre: 'Configuración Servidor Correos' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONFIGURACION_SERVIDOR_CORREOS); } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    $scope.DepartamentoValido = true;
    $scope.ListadoDepartamentos = [];
    $scope.Codigo = 1;
    ObtenerConfiguracion();
    $scope.Insertar = 0;

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Codigo = $routeParams.Codigo;
        $scope.DeshabilitarCodigo = true;
        $scope.Insertar = 0;
        ObtenerConfiguracion();
    }
    else {
        $scope.Codigo = 0;
        $scope.Insertar = 1;
        $scope.DeshabilitarCodigo = false;
    }

    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function ObtenerConfiguracion() {
        blockUI.start('Cargando Configuracion código ' + $scope.Codigo);

        $timeout(function () {
            blockUI.message('Cargando Configuracion Código ' + $scope.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Codigo,
        };

        blockUI.delay = 1000;
        ConfiguracionServidorCorreosFactory.Obtener(filtros).
           then(function (response) {
               if (response.data.ProcesoExitoso === true) {
                   $scope.Codigo = response.data.Datos.Codigo;
                   $scope.Smtp = response.data.Datos.Smtp;
                   if (response.data.Datos.AutenticacionRequerida == 1) {
                       $scope.AutenticacionRequerida = true
                   } else {
                       $scope.AutenticacionRequerida = false
                   }
                   $scope.SmtpPuerto = response.data.Datos.SmtpPuerto;
                   $scope.Usuario = response.data.Datos.Usuario;
                   $scope.Contrasena = response.data.Datos.Contraseña;
                   if (response.data.Datos.AutenticacionSSL == 1) {
                       $scope.AutenticacionSSL = true
                   } else {
                       $scope.AutenticacionSSL = false
                   }
                   $scope.Remitente = response.data.Datos.Remitente;
               }
               else {
                   ShowError('No se logro consultar Configuracion código ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
               }
           }, function (response) {
               ShowError(response.statusText);
           });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    $scope.ConfirmacionGuardarConfiguracion = function () {
        showModal('modalConfirmacionGuardarBanco');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardarBanco');
        if (DatosRequeridos()) {
            var aut = 0
            var autssl = 0
            if ($scope.AutenticacionRequerida) {
                aut = 1
            } if ($scope.AutenticacionSSL) {
                autssl = 1
            }
            filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Smtp: $scope.Smtp,
                AutenticacionRequerida: aut,
                SmtpPuerto: $scope.SmtpPuerto,
                Usuario: $scope.Usuario,
                Contraseña: $scope.Contrasena,
                AutenticacionSSL: autssl,
                Remitente: $scope.Remitente,
            }
            ConfiguracionServidorCorreosFactory.Guardar(filtro).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    if (response.data.Datos > 0) {
                        ShowSuccess('Se guardó la configuración del servidor de correos');
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        }

    };

    $scope.ProbarConfiguracion = function () {
        if (DatosRequeridos()) {
            var aut = 0
            var autssl = 0
            if ($scope.AutenticacionRequerida) {
                aut = 1
            } if ($scope.AutenticacionSSL) {
                autssl = 1
            }
            Enditad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Smtp: $scope.Smtp,
                AutenticacionRequerida: aut,
                SmtpPuerto: $scope.SmtpPuerto,
                Usuario: $scope.Usuario,
                Contraseña: $scope.Contrasena,
                AutenticacionSSL: autssl,
                Remitente: $scope.Remitente,
                Receptor: $scope.Receptor
            }
            ConfiguracionServidorCorreosFactory.EnviarCorreoPrueba(Enditad).
            then(function (response) {
                    if (response.data.MensajeOperacion.length == 0) {
                        ShowSuccess('Se envio el correo de prueba a ' + $scope.Receptor );
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
            }, function (response) {
                ShowError(response.statusText);
            });
        }

    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;


        //if ($scope.Codigo == undefined || $scope.Codigo == '') {
        //    $scope.MensajesError.push('Debe ingresar el código del banco');
        //    continuar = false;
        //}
        if ($scope.Smtp == undefined || $scope.Smtp == '') {
            $scope.MensajesError.push('Debe ingresar el Smtp ');
            continuar = false;
        }
        if ($scope.SmtpPuerto == undefined || $scope.SmtpPuerto == '') {
            $scope.MensajesError.push('Debe ingresar el puerto Smtp ');
            continuar = false;
        }
        if ($scope.Usuario == undefined || $scope.Usuario == '') {
            $scope.MensajesError.push('Debe ingresar el usuario ');
            continuar = false;
        }
        if ($scope.Contrasena == undefined || $scope.Contrasena == '') {
            $scope.MensajesError.push('Debe ingresar la contraseña ');
            continuar = false;
        }
        return continuar;
    }

    //$scope.VolverMaster = function () {
    //    document.location.href = '#ConsultarBancos';
    //};
    $scope.MaskMayus = function () {
        try { $scope.Nombre = $scope.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Codigo = MascaraNumero($scope.Codigo) } catch (e) { }
    };
}]);