﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Despachos.Paqueteria

Namespace Despachos.Paqueteria
    Public NotInheritable Class PersistenciaDetallePlanillaDespachos
        Implements IPersistenciaBase(Of DetallePlanillaDespachos)

        Public Function Consultar(filtro As DetallePlanillaDespachos) As IEnumerable(Of DetallePlanillaDespachos) Implements IPersistenciaBase(Of DetallePlanillaDespachos).Consultar
            Return New RepositorioDetallePlanillaDespachos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetallePlanillaDespachos) As Long Implements IPersistenciaBase(Of DetallePlanillaDespachos).Insertar
            Return New RepositorioDetallePlanillaDespachos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetallePlanillaDespachos) As Long Implements IPersistenciaBase(Of DetallePlanillaDespachos).Modificar
            Throw New NotImplementedException()
        End Function

        Public Function Obtener(filtro As DetallePlanillaDespachos) As DetallePlanillaDespachos Implements IPersistenciaBase(Of DetallePlanillaDespachos).Obtener
            Return New RepositorioDetallePlanillaDespachos().Obtener(filtro)
        End Function

    End Class

End Namespace

