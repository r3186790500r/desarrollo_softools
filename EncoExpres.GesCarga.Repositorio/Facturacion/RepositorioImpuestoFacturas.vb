﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Facturacion
Namespace Facturacion
    ''' <summary>
    ''' Clase <see cref="RepositorioImpuestoFacturas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioImpuestoFacturas
        Inherits RepositorioBase(Of ImpuestoFacturas)

        Public Overrides Function Consultar(filtro As ImpuestoFacturas) As IEnumerable(Of ImpuestoFacturas)
            Dim lista As New List(Of ImpuestoFacturas)
            Dim item As ImpuestoFacturas
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Ciudad) Then
                    If filtro.Ciudad.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.Ciudad.Codigo)
                    End If
                End If
                If filtro.CodigoTipoDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.CodigoTipoDocumento)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_impuestos_Tipo_documento]")

                While resultado.Read
                    item = New ImpuestoFacturas(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ImpuestoFacturas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ImpuestoFacturas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ImpuestoFacturas) As ImpuestoFacturas
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace

