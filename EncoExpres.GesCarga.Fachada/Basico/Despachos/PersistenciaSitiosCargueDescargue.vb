﻿Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaSitiosCargueDescargue
        Implements IPersistenciaBase(Of SitiosCargueDescargue)

        Public Function Consultar(filtro As SitiosCargueDescargue) As IEnumerable(Of SitiosCargueDescargue) Implements IPersistenciaBase(Of SitiosCargueDescargue).Consultar
            Return New RepositorioSitiosCargueDescargue().Consultar(filtro)
        End Function

        Public Function ConsultarSitiosRutas(filtro As Rutas) As IEnumerable(Of SitiosCargueDescargueRutas)
            Return New RepositorioSitiosCargueDescargue().ConsultarSitiosRutas(filtro)
        End Function

        Public Function Insertar(entidad As SitiosCargueDescargue) As Long Implements IPersistenciaBase(Of SitiosCargueDescargue).Insertar
            Return New RepositorioSitiosCargueDescargue().Insertar(entidad)
        End Function
        Public Function GuardarSitiosRuta(entidad As IEnumerable(Of SitiosCargueDescargueRutas)) As Long
            Return New RepositorioSitiosCargueDescargue().GuardarSitiosRuta(entidad)
        End Function

        Public Function Modificar(entidad As SitiosCargueDescargue) As Long Implements IPersistenciaBase(Of SitiosCargueDescargue).Modificar
            Return New RepositorioSitiosCargueDescargue().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As SitiosCargueDescargue) As SitiosCargueDescargue Implements IPersistenciaBase(Of SitiosCargueDescargue).Obtener
            Return New RepositorioSitiosCargueDescargue().Obtener(filtro)
        End Function
        Public Function Anular(entidad As SitiosCargueDescargue) As Boolean
            Return New RepositorioSitiosCargueDescargue().Anular(entidad)
        End Function
        Public Function GenerarPlanitilla(filtro As SitiosCargueDescargue) As SitiosCargueDescargue
            Return New RepositorioSitiosCargueDescargue().GenerarPlanitilla(filtro)
        End Function
    End Class

End Namespace

