﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Almacen
Imports EncoExpres.GesCarga.Repositorio.Almacen

Namespace Almacen
    Public NotInheritable Class PersistenciaDetalleOrdenCompra
        Implements IPersistenciaBase(Of DetalleOrdenCompra)
        Public Function Consultar(filtro As DetalleOrdenCompra) As IEnumerable(Of DetalleOrdenCompra) Implements IPersistenciaBase(Of DetalleOrdenCompra).Consultar
            Return New RepositorioDetalleOrdenCompra().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleOrdenCompra) As Long Implements IPersistenciaBase(Of DetalleOrdenCompra).Insertar
            Return New RepositorioDetalleOrdenCompra().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleOrdenCompra) As Long Implements IPersistenciaBase(Of DetalleOrdenCompra).Modificar
            Return New RepositorioDetalleOrdenCompra().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleOrdenCompra) As DetalleOrdenCompra Implements IPersistenciaBase(Of DetalleOrdenCompra).Obtener
            Return New RepositorioDetalleOrdenCompra().Obtener(filtro)
        End Function
    End Class
End Namespace
