﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Almacen
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Fachada.Almacen
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Almacen
    Public NotInheritable Class LogicaDocumentoAlmacenes
        Inherits LogicaBase(Of DocumentoAlmacenes)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of DocumentoAlmacenes)
        ReadOnly _persistenciaDetalle As IPersistenciaBase(Of DetalleDocumentoAlmacenes)

        Sub New(persistencia As IPersistenciaBase(Of DocumentoAlmacenes), persistenciaDetalle As IPersistenciaBase(Of DetalleDocumentoAlmacenes))
            _persistencia = persistencia
            _persistenciaDetalle = persistenciaDetalle
        End Sub

        Public Overrides Function Consultar(filtro As DocumentoAlmacenes) As Respuesta(Of IEnumerable(Of DocumentoAlmacenes))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DocumentoAlmacenes))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DocumentoAlmacenes))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As DocumentoAlmacenes) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Or Len(Trim(entidad.MensajeSQL)) > 0 Then
                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As DocumentoAlmacenes) As Respuesta(Of DocumentoAlmacenes)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of DocumentoAlmacenes)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            If consulta.Numero = 0 Then
                Return New Respuesta(Of DocumentoAlmacenes)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            If consulta.Numero > 0 Then
                consulta.Detalles = _persistenciaDetalle.Consultar(New DetalleDocumentoAlmacenes With {.CodigoEmpresa = consulta.CodigoEmpresa, .Numero = consulta.Numero, .TipoDocumento = consulta.TipoDocumento})
            End If


            Return New Respuesta(Of DocumentoAlmacenes)(consulta)
        End Function

        Public Function Anular(entidad As DocumentoAlmacenes) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaDocumentoAlmacenes).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
    End Class
End Namespace
