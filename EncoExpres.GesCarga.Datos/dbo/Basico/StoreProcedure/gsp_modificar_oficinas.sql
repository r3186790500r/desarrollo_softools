﻿PRINT 'gsp_modificar_oficinas'
GO
DROP PROCEDURE gsp_modificar_oficinas
GO
CREATE PROCEDURE gsp_modificar_oficinas   
(          
@par_EMPR_Codigo SMALLINT,          
@par_Codigo SMALLINT,          
@par_Codigo_Alterno VARCHAR(20),          
@par_Nombre VARCHAR(50),          
@par_CIUD_Codigo NUMERIC,          
@par_Direccion VARCHAR(150),          
@par_Telefono VARCHAR(20),          
@par_Email VARCHAR(150),          
@par_CATA_TIOF_Codigo NUMERIC = NULL,  
@par_REPA_Codigo  NUMERIC = NULL,           
@par_Resolucion_Facturacion VARCHAR(250) = NULL,
@par_Aplica_Enturnamiento NUMERIC,
@par_Estado SMALLINT,          
@par_USUA_Modifica SMALLINT,      
@par_ZOCI_Codigo NUMERIC         
)          
AS           
BEGIN           
 UPDATE Oficinas          
 SET          
  EMPR_Codigo = @par_EMPR_Codigo,          
  Codigo_Alterno = @par_Codigo_Alterno,          
  Nombre = @par_Nombre,          
  CIUD_Codigo = @par_CIUD_Codigo ,          
  Direccion = @par_Direccion,          
  Telefono = @par_Telefono,          
  Email = @par_Email,          
  CATA_TIOF_Codigo = @par_CATA_TIOF_Codigo,    
  REPA_Codigo = @par_REPA_Codigo,         
  Resolucion_Facturacion = ISNULL(@par_Resolucion_Facturacion,''),
  Aplica_Enturnamiento = @par_Aplica_Enturnamiento,       
  Estado = @par_Estado,          
  ZOCI_Codigo= @par_ZOCI_Codigo,      
  USUA_Modifica = @par_USUA_Modifica,          
  Fecha_Modifica = GETDATE()          
 WHERE           
 EMPR_Codigo = @par_EMPR_Codigo          
 AND Codigo = @par_Codigo          
          
 SELECT @par_Codigo AS Codigo          
END          