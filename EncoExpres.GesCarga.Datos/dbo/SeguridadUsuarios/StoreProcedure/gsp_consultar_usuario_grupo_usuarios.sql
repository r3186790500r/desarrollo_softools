﻿PRINT 'gsp_consultar_usuario_grupo_usuarios'
GO
DROP PROCEDURE gsp_consultar_usuario_grupo_usuarios
GO
CREATE PROCEDURE gsp_consultar_usuario_grupo_usuarios(
  @par_EMPR_Codigo SMALLINT,
  @par_USUA_Codigo SMALLINT
  )
AS
BEGIN  
    SELECT EMPR_Codigo, GRUS_Codigo, USUA_Codigo, NombreGrupo, NombreUsuario
    FROM V_Usuario_Grupo_Usuarios 
    WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND USUA_Codigo = @par_USUA_Codigo
END
GO