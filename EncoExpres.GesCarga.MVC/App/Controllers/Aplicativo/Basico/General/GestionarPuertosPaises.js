﻿EncoExpresApp.controller("GestionarPuertosPaisesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'PaisesFactory', 'PuertosPaisesFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, PaisesFactory, PuertosPaisesFactory) {
        $scope.Titulo = 'GESTIONAR PUERTOS PAÍS';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Puertos País' }, { Nombre: 'Gestionar' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PUERTOS_PAISES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoEstados = [
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];

        $scope.ListadoPaises = [];
        // Se crea la propiedad modelo en el ambito  
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            Estado: $scope.ListadoEstados[0],
        }
        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Codigo = 0;
        }

        $scope.LongitudPais = 0;// verifica la cantidad de datos ingresados  de la país
        $scope.CodigoPais = 0;
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar Autocomplete de Ciudades*/
        PaisesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoPaises = [];
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListadoPaises = response.data.Datos;
                        if ($scope.CodigoPais > 0) {
                            $scope.Modelo.Pais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.CodigoPais);
                        }
                    }
                    else {
                        $scope.ListadoPaises = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR PUERTO PAÍS';
            $scope.Deshabilitar = true;
            Obtener();
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando puerto código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando puerto código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            PuertosPaisesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = response.data.Datos.Codigo;
                        $scope.Modelo.Nombre = response.data.Datos.Nombre;
                        $scope.Modelo.Contacto = response.data.Datos.Contacto;
                        $scope.CodigoPais = response.data.Datos.Pais.Codigo;
                        if ($scope.ListadoPaises.length > 0 && $scope.CodigoPais > 0) {
                            $scope.Modelo.Pais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + response.data.Datos.Pais.Codigo);
                        }
                        $scope.Modelo.NombreCiudad = response.data.Datos.NombreCiudad;
                        $scope.Modelo.Direccion = response.data.Datos.Direccion;
                        $scope.Modelo.Telefono = response.data.Datos.Telefono;
                        $scope.Modelo.Observaciones = response.data.Datos.Observaciones;
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);

                    }
                    else {
                        ShowError('No se logro consultar el puerto No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarPuertosPaises';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarPuertosPaises';
                });
            blockUI.stop();
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardarPuertosPaises = function () {
            if (DatosRequeridosPuertosPaises()) {
                showModal('modalConfirmacionGuardarPuertosPaises');
            }
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarPuertosPaises');

            $scope.Modelo.Estado = $scope.Modelo.Estado.Codigo
            PuertosPaisesFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó el puerto: ' + $scope.Modelo.Nombre);
                            }
                            else {
                                ShowSuccess('Se modificó el puerto: ' + $scope.Modelo.Nombre);
                            }
                            location.href = '#!ConsultarPuertosPaises/' + response.data.Datos;
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridosPuertosPaises() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == "") {
                $scope.MensajesError.push('Debe ingresar el nombre del puerto');
                continuar = false;
            }
            if ($scope.Modelo.Contacto == undefined || $scope.Modelo.Contacto == "") {
                $scope.MensajesError.push('Debe ingresar el nombre del contacto');
                continuar = false;
            }
            if ($scope.Modelo.Pais == undefined || $scope.Modelo.Pais == "" || $scope.Modelo.Pais.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar el país del puerto');
                continuar = false;
            }
            if ($scope.Modelo.Direccion == undefined || $scope.Modelo.Direccion == "") {
                $scope.MensajesError.push('Debe ingresar la dirección del puerto');
                continuar = false;
            }
            if ($scope.Modelo.Telefono != undefined && $scope.Modelo.Telefono != "") {
                if ($scope.Modelo.Telefono.length < 7) {
                    $scope.MensajesError.push('El teléfono debe tener al menos 7 digitos');
                    continuar = false;
                }
            }
            return continuar;
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function () {
            $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno)
        };
        $scope.MaskTelefono = function () {
            $scope.Modelo.Telefono = MascaraTelefono($scope.Modelo.Telefono)
        };
        $scope.MaskDireccion = function (option) {
            try { $scope.Modelo.Direccion = MascaraDireccion($scope.Modelo.Direccion) } catch (e) { }
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($routeParams.Codigo > 0) {
                document.location.href = '#!ConsultarPuertosPaises/' + $routeParams.Codigo;
            } else {
                document.location.href = '#!ConsultarPuertosPaises';
            }
        };
    }]);