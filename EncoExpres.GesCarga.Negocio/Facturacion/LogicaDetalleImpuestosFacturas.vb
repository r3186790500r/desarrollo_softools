﻿Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Namespace Facturacion
    Public NotInheritable Class LogicaDetalleImpuestosFacturas
        Inherits LogicaBase(Of DetalleImpuestosFacturas)
        ReadOnly _persistencia As IPersistenciaBase(Of DetalleImpuestosFacturas)
        Sub New(persistencia As IPersistenciaBase(Of DetalleImpuestosFacturas))
            _persistencia = persistencia
        End Sub
        Public Overrides Function Consultar(filtro As DetalleImpuestosFacturas) As Respuesta(Of IEnumerable(Of DetalleImpuestosFacturas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleImpuestosFacturas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleImpuestosFacturas))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As DetalleImpuestosFacturas) As Respuesta(Of DetalleImpuestosFacturas)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Guardar(entidad As DetalleImpuestosFacturas) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace

