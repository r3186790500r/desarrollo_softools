﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports System.Transactions

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioTRM"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTRM
        Inherits RepositorioBase(Of TRM)

        Public Overrides Function Consultar(filtro As TRM) As IEnumerable(Of TRM)
            Dim lista As New List(Of TRM)
            Dim item As TRM

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Codigo) Then
                    If Len(filtro.Codigo) > Cero Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_TRM")

                While resultado.Read
                    item = New TRM(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As TRM) As Long

            Dim lonResultado As Double = 0
            Dim inserto As Boolean = True


            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                For Each TRM In entidad.Lista
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", TRM.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_MONE_Codigo", TRM.Codigo)
                        conexion.AgregarParametroSQL("@par_Fecha", TRM.Fecha, SqlDbType.DateTime)
                        conexion.AgregarParametroSQL("@par_Valor_Moneda_Local", TRM.Valor_Moneda_Local)
                        conexion.AgregarParametroSQL("@par_Usua_Modifica", TRM.UsuarioCrea.Codigo)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_guardar_TRM")
                        While resultado.Read
                            entidad.Codigo = resultado.Item("MONE_Codigo").ToString()
                        End While
                        resultado.Close()
                    End Using

                Next

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using


            Return entidad.Codigo

        End Function

        Public Overrides Function Modificar(entidad As TRM) As Long

            Dim lonResultado As Double = 0
            Dim inserto As Boolean = True


            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                For Each TRM In entidad.Lista
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", TRM.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_MONE_Codigo", TRM.Codigo)
                        conexion.AgregarParametroSQL("@par_Fecha", TRM.Fecha, SqlDbType.DateTime)
                        conexion.AgregarParametroSQL("@par_Valor_Moneda_Local", TRM.Valor_Moneda_Local)
                        conexion.AgregarParametroSQL("@par_Usua_Modifica", TRM.UsuarioModifica.Codigo)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Modificar_TRM")
                        While resultado.Read
                            entidad.Codigo = resultado.Item("@par_MONE_Codigo").ToString()
                        End While
                        resultado.Close()
                    End Using

                Next

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using


            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As TRM) As TRM
            Dim item As New TRM

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_MONE_Codigo", filtro.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Fecha", filtro.CodigoEmpresa)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Obtener_TRM")

                While resultado.Read
                    item = New trm(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As TRM) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_MONE_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_FECHA", entidad.Fecha, SqlDbType.DateTime)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Eliminar_TRM")

                While resultado.Read
                    anulo = IIf((resultado.Item("MONE_Codigo")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace
