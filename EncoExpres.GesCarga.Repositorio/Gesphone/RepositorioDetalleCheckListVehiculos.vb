﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Gesphone

Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleCheckListVehiculos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleCheckListVehiculos
        Inherits RepositorioBase(Of DetalleCheckListVehiculos)

        Public Overrides Function Consultar(filtro As DetalleCheckListVehiculos) As IEnumerable(Of DetalleCheckListVehiculos)
            Dim lista As New List(Of DetalleCheckListVehiculos)
            Dim item As DetalleCheckListVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_ECLV_Numero", filtro.NumeroDocumento)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_check_list_vehiculos]")

                While resultado.Read
                    item = New DetalleCheckListVehiculos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleCheckListVehiculos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleCheckListVehiculos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleCheckListVehiculos) As DetalleCheckListVehiculos
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleCheckListVehiculos) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace