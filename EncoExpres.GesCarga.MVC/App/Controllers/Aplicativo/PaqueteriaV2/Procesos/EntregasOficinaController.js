﻿EncoExpresApp.controller("EntregasOficinaCtrl", ['$scope', '$timeout', 'TarifarioVentasFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'ValorCatalogosFactory',
    'RemesaGuiasFactory', 'EmpresasFactory', 'OficinasFactory', 'RemesasFactory', 'DocumentosFactory',
    function ($scope, $timeout, TarifarioVentasFactory, $linq, blockUI, blockUIConfig, $routeParams, ValorCatalogosFactory,
        RemesaGuiasFactory, EmpresasFactory, OficinasFactory, RemesasFactory, DocumentosFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONSULTAR GUÍAS';
        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Procesos' }, { Nombre: 'Entregas en Oficina' }];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.ENTREGAS_OFICINA);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.OPCION_MENU = OPCION_MENU_PAQUETERIA.REMESAS;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.pref = '';
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} },
            Numeracion: ''
        };
        $scope.HabilitarEliminarDocumento = false;
        $scope.ListadoEstadoGuia = [
            { Codigo: -1, Nombre: '(TODOS)' }
        ];
        $scope.ListadoOficinas = [];
        $scope.ListadoOficinasActual = [];
        $scope.ListadoTipoIdentificacion = [];
        $scope.ListadoNovedades = [];
        $scope.ModalEntrega = {};
        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }, { Codigo: 2, Nombre: 'ANULADO' }];
        $scope.Foto = [];
        $scope.ModeloEstado = $scope.ListadoEstadoRemesaPaqueteria[0];
        $scope.ResponseGuia = '';
        //-----------Control Entregas
        $scope.DeshabilitarEntregar = true;
        $scope.MensajesErrorRecibir = [];
        $scope.MensajesErrorDocu = [];
        $scope.Modal = {};
        $scope.ModalDestinatario = {};
        $scope.DevolucionRemesa = 0;
        var ListaFormaPago = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS },
            Sync: true
        }).Datos;
        ListaFormaPago.splice(ListaFormaPago.findIndex(item => item.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA), 1);
        //--Documental
        $scope.ListadoDocumentos = [];
        $scope.CadenaFormatos = ".pdf";
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Init
        $scope.InitLoad = function () {

            //ListadoNovedadesRemesas: 
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 203 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNovedades = response.data.Datos;
                        }
                        else {
                            $scope.ListadoNovedades = []
                        }
                    }
                }, function (response) {
                });

            //--Estado Guias
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 60 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if (response.data.Datos[i].Codigo != 6000) {
                                    $scope.ListadoEstadoGuia.push(response.data.Datos[i]);
                                }
                            }
                        }
                        $scope.Modelo.EstadoRemesaPaqueteria = $scope.ListadoEstadoGuia[0];
                    }
                }, function (response) {
                });

            //Listado tipo identificación:
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {

                            $scope.ListadoTipoIdentificacion = response.data.Datos;

                        }
                    }
                }, function (response) {
                });

            //--Oficinas
            var responseOficina = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true });
            if (responseOficina.ProcesoExitoso) {
                $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' });
                responseOficina.Datos.forEach(function (item) {
                    $scope.ListadoOficinas.push(item);
                });
                $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
            }

            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                $scope.ListadoOficinasActual = $scope.ListadoOficinas;
                $scope.Modelo.OficinaActual = $linq.Enumerable().From($scope.ListadoOficinasActual).First('$.Codigo === -1');
            }
            else {
                $scope.ListadoOficinasActual.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.Modelo.OficinaActual = $scope.ListadoOficinasActual[0];
            }
            //--Obtener Parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
                $scope.Modelo.NumeroInicial = $routeParams.Numero;
                $scope.Modelo.NumeroFinal = $routeParams.Numero;
                PantallaBloqueo(Find);
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            PantallaBloqueo(Find);
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            PantallaBloqueo(Find);
        };
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        //--Funcion Nuevo Documento
        //$scope.NuevoDocumento = function () {
        //    if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
        //        document.location.href = '#!GestionarRemesasPaqueteria';
        //    }
        //};
        $scope.CopiarDocumento = function (Codigo) {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRemesasPaqueteria/' + Codigo.toString() + ',1';
            }
        };
        //--Funcion Buscar
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO && DatosRequeridos()) {
                $scope.Codigo = 0;
                PantallaBloqueo(Find);
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroInicial === null || $scope.Modelo.NumeroInicial === undefined || $scope.Modelo.NumeroInicial === '' || $scope.Modelo.NumeroInicial === 0 || isNaN($scope.Modelo.NumeroInicial) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.Modelo.NumeroInicial !== null && $scope.Modelo.NumeroInicial !== undefined && $scope.Modelo.NumeroInicial !== '' && $scope.Modelo.NumeroInicial !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }

                else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial;
                    } else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal;
                    }
                }
            }
            return continuar;
        }
        function Find() {
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            $scope.Modelo.Pagina = $scope.paginaActual;
            $scope.Modelo.RegistrosPagina = 10;
            $scope.Modelo.NumeroPlanilla = -1;
            $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
            if ($scope.ModeloEstado !== undefined && $scope.ModeloEstado !== null) {
                $scope.Modelo.Estado = $scope.ModeloEstado.Codigo;
            } else {
                $scope.Modelo.Estado = -1;
            }
            $scope.Modelo.OficinaActual = { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo }
            var filtro = angular.copy($scope.Modelo);
            filtro.Anulado = -1;
            RemesaGuiasFactory.Consultar(filtro).
                then(function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = 0;
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoGuias = response.data.Datos;
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                        var listado = [];
                        response.data.Datos.forEach(function (item) {
                            listado.push(item);
                        });
                    }
                }, function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    ShowError(response.statusText);
                });

        }
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });

        $scope.filtroListadoGuias = function (item) {
            if (item.EstadoGuia.Codigo != 6030) {
                return item;
            }
        }

        $scope.DesplegarArchivo = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                if (item.Temp) {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&Temp=1' + '&ENRE_Numero=' + item.Numero);
                }
                else {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&ENRE_Numero=' + item.Numero + '&Temp=0');
                }
            }
            else {
                ShowError("Se debe seleccionar un archivo");
            }
        };
        //--Gestionar Entregas
        $scope.GestionarControlEntregas = function (item, intento) {
            if (item.Estado === 1 && item.Anulado === 0 && (item.EstadoGuia.Codigo == 6010 || item.EstadoGuia.Codigo == 6005)) {
                //var url = document.location.href;
                //url = url.replace("GestionarLegalizarRemesasPaqueteria", "EntregaOficina");
                //window.open(url + "/" + item.Remesa.NumeroDocumento, '_blank');
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Detalles...");
                $scope.DeshabilitarEntregar = intento;
                $timeout(function () { blockUI.message("Cargando Detalles..."); $scope.BuscarRemesa(item.Remesa.Numero); }, 100);
            }
            else {
                ShowError('No se puede entregar la Remesa porque su estado debe corresponder al de “OFICINA” 0 “DESPACHADA”');
            }
        };
        //--Gestionar Entregas
        //---------------Gestion Control Entregas
        $scope.BuscarRemesa = function (Numero) {
            var response = RemesaGuiasFactory.Obtener({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: Numero,
                Sync: true
            });
            if (response.ProcesoExitoso) {
                // limpia campos
                $scope.MensajesErrorRecibir = [];
                $scope.MensajesErrorDocu = [];
                $scope.Modal.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas;
                $scope.Modal.FechaRecibe = new Date();
                $scope.Modal.NumeroIdentificacion = '';
                $scope.Modal.Nombre = '';
                $scope.Modal.Telefono = '';
                $scope.Modal.NovedadEntrega = '';
                $scope.Modal.Observaciones = '';
                $scope.Modal.UsuarioEntrega = $scope.Sesion.UsuarioAutenticado;
                $scope.ListadoDocumentos = [];
                $scope.MensajesErrorAdjuntar = [];
                // limpia campos

                $scope.Modal.Remitente = response.Datos.Remesa.Remitente.Nombre;
                $scope.Modal.CiudadOrigen = $scope.CargarCiudad(response.Datos.Remesa.Remitente.Ciudad.Codigo).Nombre;
                $scope.Modal.CiudadDestino = $scope.CargarCiudad(response.Datos.Remesa.Destinatario.Ciudad.Codigo).Nombre;

                $scope.Modal.FormaPago = $linq.Enumerable().From(ListaFormaPago).First('$.Codigo ==' + response.Datos.Remesa.FormaPago.Codigo).Nombre;
                $scope.Modal.Producto = $scope.CargarProducto(response.Datos.Remesa.ProductoTransportado.Codigo).Nombre;
                $scope.Modal.TotalValor = $scope.MaskValoresGrid(response.Datos.Remesa.TotalFleteCliente);
                $scope.Modal.Peso = $scope.MaskValoresGrid(response.Datos.Remesa.PesoCliente);
                $scope.Modal.Cantidad = $scope.MaskValoresGrid(response.Datos.Remesa.CantidadCliente);
                $scope.Modal.Remesa = parseInt(response.Datos.Remesa.Numero);
                $scope.Modal.NumeroDocumento = response.Datos.Remesa.NumeroDocumento;
                $scope.Modal.Fecha = response.Datos.Remesa.Fecha;

                $scope.ListaGestionDocuCliente = response.Datos.Remesa.GestionDocumentosRemesa;
                if ($scope.ListaGestionDocuCliente.length > 0) {
                    for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                        if ($scope.ListaGestionDocuCliente[i].TipoGestion.Nombre.toLowerCase().includes("firma")) {
                            $scope.MensajesErrorAdjuntar.push('Adjuntar soporte de gestión documental');
                            break;
                        }
                    }
                }

                $scope.ModalDestinatario.Nombre = response.Datos.Remesa.Destinatario.Nombre;
                $scope.ModalDestinatario.Telefono = response.Datos.Remesa.Destinatario.Telefonos;
                $scope.ModalDestinatario.Direccion = response.Datos.Remesa.Destinatario.Direccion;
                $scope.ModalDestinatario.NumeroIdentificacion = response.Datos.Remesa.Destinatario.NumeroIdentificacion;
                $scope.ModalDestinatario.Barrio = response.Datos.Remesa.BarrioDestinatario;
                $scope.CodigoTipoIdentificacionRecibe = response.Datos.CodigoTipoIdentificacionDestinatario;
                $scope.ModalDestinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRecibe);
                showModal('modalGestionarControlEntregas');

            }
            else {
                ShowError(response.statusText);
            }
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
        };
        $scope.LimpiarFirma = function () {
            try {
                var canvas = document.getElementById('Firma');
                var context = canvas.getContext('2d');
                context.clearRect(0, 0, canvas.width, canvas.height);
            } catch (e) {

            }
        };
        $scope.ConfirmacionGuardarPaqueteria = function () {
            if (DatosRequeridosEntregas()) {
                ShowConfirm('¿Está seguro de guardar la información?', $scope.GuardarPaqueteria);
            }
        };
        $scope.GuardarPaqueteria = function () {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Guardando Entrega...");
            $timeout(function () { blockUI.message("Guardando Entrega..."); }, 100);
            //closeModal('modalConfirmacionGuardarPaqueteria', 1);
            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            $scope.Firma = image.src.replace(/data:image\/png;base64,/g, '');

            $scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);

            var GestionDocumentosRemesa = [];
            if ($scope.ListaGestionDocuCliente.length > 0) {
                for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                    var item = $scope.ListaGestionDocuCliente[i];
                    GestionDocumentosRemesa.push({
                        TipoDocumento: item.TipoDocumento,
                        TipoGestion: item.TipoGestion,
                        Entregado: item.Entregado.Codigo
                    });
                }
            }

            //--Documentos Remesas
            if ($scope.ListadoDocumentos != undefined && $scope.ListadoDocumentos != null) {
                if ($scope.ListadoDocumentos.length > 0) {
                    for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                        if ($scope.ListadoDocumentos[i].Id != undefined && $scope.ListadoDocumentos[i].Id != null && $scope.ListadoDocumentos[i].Id != '') {
                            DocumentosFactory.InsertarDocumentosCumplidoRemesa({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Id: $scope.ListadoDocumentos[i].Id,
                                Numero: $scope.Modal.Remesa,
                            }).then(function (response) {
                                if (response.data.ProcesoExitoso === false) {
                                    ShowError(response.data.MensajeError);
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                        }
                    }
                }
            }
            //--Documentos Remesas

            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //Codigo: $scope.Codigo,
                Numero: $scope.Modal.Remesa,
                Oficina: $scope.Modal.Oficina,
                FechaRecibe: $scope.Modal.FechaRecibe,
                CantidadRecibe: $scope.Modal.Cantidad,
                PesoRecibe: $scope.Modal.Peso,
                CodigoTipoIdentificacionRecibe: $scope.Modal.TipoIdentificacionRecibe.Codigo,
                NumeroIdentificacionRecibe: parseInt($scope.Modal.NumeroIdentificacion),
                NombreRecibe: $scope.Modal.Nombre,
                TelefonoRecibe: $scope.Modal.Telefono,
                NovedadEntrega: $scope.Modal.NovedadEntrega,
                ObservacionesRecibe: $scope.Modal.Observaciones,
                //Fotografia: $scope.Foto[0],
                Fotografias: $scope.Foto,
                Firma: $scope.Firma,
                //UsuarioCrea: { Codigo: $scope.Modal.UsuarioEntrega.Codigo },
                UsuarioModifica: { Codigo: $scope.Modal.UsuarioEntrega.Codigo },
                DevolucionRemesa: $scope.DevolucionRemesa,
                GestionDocumentosRemesa: GestionDocumentosRemesa
            };

            RemesaGuiasFactory.GuardarEntrega($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.DevolucionRemesa == 0) {
                                ShowSuccess('La remesa fue recibida por ' + $scope.Modal.Nombre);
                            } else {
                                ShowSuccess('Se realizó la devolución de la remesa satisfactoriamente');
                            }

                            $scope.Foto = [];
                            $scope.LimpiarFirma();

                            $scope.PrimerPagina(Find);
                            closeModal('modalGestionarControlEntregas');
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                }, function (response) {
                    ShowError(response.statusText);
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);

                });
        };


        $scope.CargarImagen = function (element) {
            var continuar = true;
            $scope.TipoImagen = element
            $scope.NombreDocumento = element.files[0].name
            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato JPEG, PNG", false);
                continuar = false;
            }

            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png') {
                    ShowError("Solo se pueden cargar archivos en formato JPEG, PNG ", false);
                    continuar = false;
                }
            }

            if (continuar == true) {
                var reader = new FileReader();
                reader.onload = $scope.AsignarImagenListado;
                reader.readAsDataURL(element.files[0]);
            }

        }

        $scope.AsignarImagenListado = function (e) {
            try {
                RedimHorizontal('')
                RedimVertical('')
            } catch (e) {
            }
            $scope.$apply(function () {
                $scope.Convertirfoto = null
                var TipoImagen = ''

                var name = $scope.TipoImagen.files[0].name.split('.')

                $scope.NombreFoto = name[0]

                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/jpeg'
                        AsignarFotoListado()

                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/png'
                        AsignarFotoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.Convertirfoto = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoFoto = 'application/pdf'
                    $scope.TipoImagen = TipoImagen
                    AsignarFotoListado()

                }
                var input = document.getElementById('inputfile')
                input.value = ''
            });
        }

        $scope.AsignarFoto = function () {
            closeModal('ModalTomarFoto', 1)
            $scope.video.srcObject = undefined
            // $scope.Foto = [];
            $scope.FotoAux = $scope.FotoTomada.toString();
            $scope.FotoBits = $scope.FotoAux.replace(/data:image\/png;base64,/g, '')
            $scope.Foto.push({
                NombreFoto: 'Fotografía',
                TipoFoto: 'image/png',
                ExtensionFoto: 'PNG',
                FotoBits: $scope.FotoBits,
                ValorDocumento: 1,
                FotoCargada: $scope.FotoAux,
                id: $scope.Foto.length
            });
        }

        function AsignarFotoListado() {
            // $scope.Foto = [];
            $scope.Foto.push({
                NombreFoto: $scope.NombreFoto,
                TipoFoto: $scope.TipoFoto,
                ExtensionFoto: $scope.TipoImagen,
                FotoBits: $scope.Convertirfoto,
                ValorDocumento: 1,
                FotoCargada: 'data:' + $scope.TipoFoto + ';base64,' + $scope.Convertirfoto,
                id: $scope.Foto.length
            });
        }

        $scope.EliminarFoto = function (item) {
            $scope.Foto.splice(item.id, 1);
            for (var i = 0; i < $scope.Foto.length; i++) {
                $scope.Foto[i].id = i;
            }
            // $scope.Foto = []
            $scope.ModificarFoto = true;
            //$("#Foto").attr("src", "");
        }

        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvas se dibuja la imagen que se recibe por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }

        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }

        function DatosRequeridosEntregas() {
            $scope.MensajesErrorRecibir = [];
            $scope.MensajesErrorDocu = [];
            var continuar = true;

            if ($scope.Modal.Cantidad === undefined || $scope.Modal.Cantidad === '' || $scope.Modal.Cantidad === null || $scope.Modal.Cantidad === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if ($scope.Modal.Peso === undefined || $scope.Modal.Peso === '' || $scope.Modal.Peso === null || $scope.Modal.Peso === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el peso');
                continuar = false;
            }
            if ($scope.Modal.TipoIdentificacionRecibe === undefined || $scope.Modal.TipoIdentificacionRecibe === '' || $scope.Modal.TipoIdentificacionRecibe === null || $scope.Modal.TipoIdentificacionRecibe.Codigo === CODIGO_NO_APLICA_TIPO_IDENTIFICACION) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el tipo de identificación de quien recibe');

                continuar = false;
            }
            if ($scope.Modal.NumeroIdentificacion === undefined || $scope.Modal.NumeroIdentificacion === '' || $scope.Modal.NumeroIdentificacion === null || $scope.Modal.NumeroIdentificacion === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el número de identificación de quien recibe');
                continuar = false;
            }
            if ($scope.Modal.Nombre === undefined || $scope.Modal.Nombre === '' || $scope.Modal.Nombre === null || $scope.Modal.Nombre === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el nombre de quien recibe');
                continuar = false;
            }
            if ($scope.Modal.Telefono === undefined || $scope.Modal.Telefono === '' || $scope.Modal.Telefono === null || $scope.Modal.Telefono === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el teléfono de quien recibe');
                continuar = false;
            }
            if ($scope.Modal.NovedadEntrega === undefined || $scope.Modal.NovedadEntrega === '' || $scope.Modal.NovedadEntrega === null || $scope.Modal.NovedadEntrega === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la novedad de entrega');
                continuar = false;
            }
            if ($scope.Modal.UsuarioEntrega === undefined || $scope.Modal.UsuarioEntrega === '' || $scope.Modal.UsuarioEntrega === null || $scope.Modal.UsuarioEntrega === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el usuario que entrega');
                continuar = false;
            }
            //if ($scope.Modal.Observaciones === undefined || $scope.Modal.Observaciones === '' || $scope.Modal.Observaciones === null || $scope.Modal.Observaciones === 0) {
            //    $scope.MensajesErrorRecibir.push('Debe ingresar las observaciones');
            //    continuar = false;
            //}
            if ($scope.ListaGestionDocuCliente.length > 0) {
                for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                    if ($scope.ListaGestionDocuCliente[i].Entregado == 0 || $scope.ListaGestionDocuCliente[i].Entregado.Codigo == -1) {
                        $scope.MensajesErrorDocu.push('Se debe seleccionar en todos los documentos si fueron entregados o no');
                        continuar = false;
                        break;
                    }
                }
            }
            return continuar;
        }
        //-----------------------Documental------------------------------//
        $scope.AgregarFilaDocumento = function () {
            if ($scope.ListadoDocumentos.length < 5) {
                var itmDoc = {
                    Descripcion: '',
                    AplicaDescargar: 0,
                    AplicaArchivo: 1,
                    ValorDocumento: 0,
                    TipoArchivoDocumento: $scope.CadenaFormatos
                };
                $scope.ListadoDocumentos.push(itmDoc);
            } else {
                ShowError("Máximo se permite cargar cinco (5) documentos");
            }
        };

        $scope.CargarArchivo = function (element) {
            $scope.IdPosiction = parseInt(element.id, 10);
            var continuar = true;
            if (element.files.length > 0) {
                $scope.PDF = element;
                $scope.TipoDocumento = element;
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento = angular.element(this.ItemFoto)[0];
                } else {
                    angular.element(this.item)[0].Temporal = true
                    $scope.Documento = angular.element(this.item)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos: 0");
                    continuar = false;
                }
                if (continuar == true) {
                    if (element.files[0].size >= 3000000) { //3 MB
                        ShowError("El archivo " + element.files[0].name + " supera los " + "3 MB" + ", intente con otro archivo", false);
                        continuar = false;
                    }
                    var Formatos = $scope.CadenaFormatos.split(',');
                    var cont = 0;
                    for (var i = 0; i < Formatos.length; i++) {
                        Formatos[i] = Formatos[i].replace(' ', '');
                        var Extencion = element.files[0].name.split('.');
                        //if (element.files[0].type == Formatos[i]) {
                        if ('.' + (Extencion[Extencion.length - 1].toUpperCase()) == Formatos[i].toUpperCase()) {
                            cont++;
                        }
                    }
                    if (cont == 0) {
                        ShowError("Archivo invalido, por favor verifique el formato del archivo.  Formatos permitidos: " + $scope.CadenaFormatos);
                        continuar = false;
                    }
                }
                if (continuar == true) {
                    if ($scope.Documento.Archivo != undefined && $scope.Documento.Archivo != '' && $scope.Documento.Archivo != null) {
                        $scope.Documento.ArchivoTemporal = element.files[0];
                        showModal('modalConfirmacionRemplazarDocumentoRemesa');
                    } else {
                        var reader = new FileReader();
                        $scope.Documento.ArchivoCargado = element.files[0];
                        reader.onload = $scope.AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }
            }
        };

        $scope.RemplazarDocumentoRemesa = function () {
            var reader = new FileReader();
            $scope.Documento.ArchivoCargado = $scope.Documento.ArchivoTemporal;
            reader.onload = $scope.AsignarArchivo;
            reader.readAsDataURL($scope.Documento.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumentoRemesa');
        };

        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                //imagenes, tienen un proceso aparte ya que se deben redimencionar para que no ocupen mucho espacio en la BD
                if ($scope.Documento.ArchivoCargado.type.includes("image")) {
                    var img = new Image();
                    img.src = e.target.result;
                    $timeout(function () {
                        $('#Foto' + $scope.IdPosiction).show();
                        $('#FotoCargada' + $scope.IdPosiction).hide();
                        if (img.height > img.width) {
                            RedimencionarImagen('CanvasVertical', img, 699, 499);
                            $scope.Documento.Archivo = document.getElementById('CanvasVertical').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                        }
                        //la imagen es horizontal
                        else {
                            RedimencionarImagen('CanvasHorizontal', img, 499, 699);
                            $scope.Documento.Archivo = document.getElementById('CanvasHorizontal').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                        }
                        $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type;
                        var Extencion = $scope.Documento.ArchivoCargado.name.split('.');
                        var tmpNombre = $scope.Documento.ArchivoCargado.name.split('.').slice(0, -1).join('.');
                        $scope.Documento.NombreDocumento = tmpNombre;
                        $scope.Documento.ExtensionDocumento = Extencion[Extencion.length - 1];
                        $scope.InsertarDocumento();
                    }, 100);
                }
                //Resto de archivos
                else {
                    $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                    $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type;
                    var Extencion = $scope.Documento.ArchivoCargado.name.split('.');
                    var tmpNombre = $scope.Documento.ArchivoCargado.name.split('.').slice(0, -1).join('.');
                    $scope.Documento.NombreDocumento = tmpNombre;
                    $scope.Documento.ExtensionDocumento = Extencion[Extencion.length - 1];
                    $scope.InsertarDocumento();
                }
            });
        };

        $scope.SeleccionarObjeto = function (item) {
            $scope.DocumentoSeleccionado = item;
        };

        $scope.InsertarDocumento = function () {
            $timeout(function () {
                blockUI.start("Subiendo Archivo...");
                // Guardar Archivo temporal
                var Documento = {};
                Documento = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    //UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Id: $scope.DocumentoSeleccionado.Id ? $scope.DocumentoSeleccionado.Id : 0,
                    Numero: $scope.Modal.Remesa,
                    //TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                    Descripcion: $scope.Documento.Descripcion,
                    Nombre: $scope.Documento.NombreDocumento,
                    Extension: $scope.Documento.ExtensionDocumento,
                    Tipo: $scope.Documento.Tipo,
                    Archivo: $scope.Documento.Archivo,
                    CumplidoRemesa: true,
                    //Temporal: true
                };
                DocumentosFactory.InsertarTemporal(Documento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            // Se guardó correctamente el pdf
                            if (response.data.Datos) {
                                $scope.ListadoDocumentos.forEach(function (item) {
                                    if (item.$$hashKey == $scope.DocumentoSeleccionado.$$hashKey) {
                                        item.Id = response.data.Datos;
                                        item.ValorDocumento = 1;
                                        item.AplicaEliminar = 1;
                                        item.temp = true;
                                    }
                                });
                                ShowSuccess('Se cargó el documento adjunto "' + $scope.Documento.NombreDocumento + '"');
                                $scope.Documento = '';
                            }
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                        blockUI.stop();
                    }, function (response) {
                        ShowError(response.statusText);
                        blockUI.stop();
                    });
            }, 200);
        };

        $scope.ElimiarDocumento = function (EliminarDocumento) {
            var EliminarDocu = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Id: EliminarDocumento.Id,
                Numero: $scope.Modal.Remesa,
                //TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                CumplidoRemesa: true,
                //Temporal: EliminarDocumento.temp
            };

            DocumentosFactory.EliminarDocumento(EliminarDocu).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                            var item = $scope.ListadoDocumentos[i];
                            if (item.Id == EliminarDocumento.Id) {
                                $scope.ListadoDocumentos.splice(i, 1);
                            }
                        }
                    }
                    else {
                        MensajeError("Error", response.data.MensajeError, false);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        //$scope.DesplegarArchivo = function (item) {
        //    if (item.temp) {
        //        window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Remesa&ID=' + item.Id + '&Temp=1' + '&TIDO_Codigo=' + CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA);
        //    }
        //    else {
        //        window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Remesa&ID=' + item.Id + '&ENRE_Numero=' + parseInt($scope.Modal.Remesa) + '&Temp=0' + '&TIDO_Codigo=' + CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA);
        //    }
        //};
        //-----------------------Documental------------------------------//
        //---------------Gestion Control Entregas

        //--------------------Intento Entrega
        function DatosRequeridosIntentos() {
            $scope.MensajesErrorRecibir = [];
            var continuar = true;
            if ($scope.Modal.NovedadEntrega === undefined || $scope.Modal.NovedadEntrega === '' || $scope.Modal.NovedadEntrega === null || $scope.Modal.NovedadEntrega === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la novedad de entrega');
                continuar = false;
            }
            if ($scope.Modal.Observaciones === undefined || $scope.Modal.Observaciones === '' || $scope.Modal.Observaciones === null || $scope.Modal.Observaciones === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar las observaciones');
                continuar = false;
            }
            return continuar;
        }
        $scope.ConfirmacionGuardarIntento = function () {
            if (DatosRequeridosIntentos()) {
                ShowConfirm('¿Está seguro de guardar la información?', $scope.GuardarIntento);
            }
        };
        $scope.GuardarIntento = function () {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Guardando Intento...");
            $timeout(function () { blockUI.message("Guardando Intento..."); }, 100);

            filtros = {
                NumeroRemesa: $scope.Modal.Remesa,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CodigoUsuarioCrea: $scope.Sesion.UsuarioAutenticado.Codigo,
                Observaciones: $scope.Modal.Oficina.Nombre + " - " + $scope.Modal.Observaciones,
                NovedadIntentoEntrega: $scope.Modal.NovedadEntrega.Nombre,
                Fotografias: $scope.Foto,
            }

            RemesaGuiasFactory.InsertarIntentoEntrega(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        $scope.Foto = [];
                        ShowSuccess('Se guardó el intento de entrega de la guia: ' + $scope.Modal.Remesa + ' con éxito');
                        closeModal('modalGestionarControlEntregas');
                    } else {
                        ShowError(response.statusText)
                    }
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                }, function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    ShowError(response.statusText);
                });
        };
        //--------------------Intento Entrega


        //--Mascaras
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item);
        };
        //--Mascaras
        //----------------------------Funciones Generales---------------------------------//
    }
]);