﻿
Print 'CREATE PROCEDURE gsp_obtener_documento_factura'
GO
DROP PROCEDURE gsp_obtener_documento_factura
GO

CREATE PROCEDURE gsp_obtener_documento_factura (
@par_EMPR_Codigo  smallint,
@par_Numero		 NUMERIC
)
AS
BEGIN
	
	SELECT EMPR_codigo, 
	ENFA_Numero as Numero, 
	Archivo,
	2 AS ObtFacEle
	FROM Documentos_Factura 
	WHERE EMPR_codigo = @par_EMPR_Codigo 
	AND ENFA_Numero = @par_Numero
	
END
GO