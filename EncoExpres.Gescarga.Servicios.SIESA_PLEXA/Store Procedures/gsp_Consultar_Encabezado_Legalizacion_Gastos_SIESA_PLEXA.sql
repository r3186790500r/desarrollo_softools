DROP PROCEDURE [dbo].[gsp_Consultar_Encabezado_Legalizacion_Gastos_SIESA_PLEXA]  
GO

CREATE PROCEDURE [dbo].[gsp_Consultar_Encabezado_Legalizacion_Gastos_SIESA_PLEXA]    
(      
 @par_EMPR_Codigo smallint,   
 @par_ELGC_Numero numeric = NULL  
)      
AS      
BEGIN    
    
SELECT TOP 10     
ELGC.EMPR_Codigo,      
ELGC.Numero,      
ELGC.Numero_Documento,      
ELGC.Fecha,      
ELGC.TERC_Codigo_Conductor,    
COND.Numero_Identificacion Identificacion_Tercero,    
ELGC.OFIC_Codigo,    
ELGC.Observaciones,    
ISNULL(ELGC.Interfaz_Contable_Legalizacion, 0 ) Interfaz_Contable_Legalizacion,      
ISNULL(ELGC.Intentos_Interfase_Contable, 0 ) Intentos_Interfase_Contable,    
VEHI.Placa AS Placa,  
ENPD.Numero_Documento AS ENPD_Numero,
DEPD.ENRE_Numero AS Numero_Remesa, 
ENRE.Numero_Documento AS Numero_Documento_Remesa, 
ENRE.PRTR_Codigo AS Codigo_Producto, 
LIPT.Campo1 AS Tipo_Operacion, 
LIPT.Campo2 AS Codigo_Centro_Operacion

FROM Encabezado_Legalizacion_Gastos_Conductor AS ELGC    
    
INNER JOIN Encabezado_Planilla_Despachos AS ENPD    
ON ENPD.EMPR_Codigo = ELGC.EMPR_Codigo    
AND ENPD.Numero = ELGC.ENPD_Numero    
    
INNER JOIN Vehiculos AS VEHI     
ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo
AND ENPD.VEHI_Codigo = VEHI.Codigo
    
INNER JOIN Terceros AS COND ON      
ENPD.EMPR_Codigo = COND.EMPR_Codigo      
AND ENPD.TERC_Codigo_Conductor = COND.Codigo      
 
INNER JOIN Detalle_Planilla_Despachos AS DEPD
ON ENPD.EMPR_Codigo = DEPD.EMPR_Codigo
AND ENPD.Numero = DEPD.ENPD_Numero

INNER JOIN Encabezado_Remesas AS ENRE
ON DEPD.EMPR_Codigo = ENRE.EMPR_Codigo
AND DEPD.ENRE_Numero = ENRE.Numero

INNER JOIN Producto_Transportados AS PRTR
ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
AND ENRE.PRTR_Codigo = PRTR.Codigo

INNER JOIN Valor_Catalogos AS LIPT --Linea Producto (PGR,GLP,SECA,etc)
ON PRTR.EMPR_Codigo = LIPT.EMPR_Codigo
AND PRTR.CATA_LIPT_Codigo = LIPT.Codigo
    
WHERE ELGC.EMPR_Codigo = @par_EMPR_Codigo    
AND (ELGC.Numero = @par_ELGC_Numero OR @par_ELGC_Numero IS NULL)   
AND ISNULL(ELGC.Interfaz_Contable_Legalizacion, 0 ) = 0      
AND ISNULL(ELGC.Intentos_Interfase_Contable, 0 ) < 5      
AND ELGC.Anulado = 0  
AND ELGC.Estado = 1  
  
END    



