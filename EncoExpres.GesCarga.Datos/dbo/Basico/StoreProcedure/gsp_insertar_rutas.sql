﻿PRINT 'gsp_insertar_rutas'
GO
DROP PROCEDURE gsp_insertar_rutas
GO
CREATE PROCEDURE gsp_insertar_rutas 
(    
@par_EMPR_Codigo SMALLINT,    
@par_Codigo_Alterno VARCHAR (20),    
@par_Nombre VARCHAR (100),    
@par_CIUD_Codigo_Origen NUMERIC,    
@par_CIUD_Codigo_Destino NUMERIC,    
@par_CATA_TIRU_Codigo NUMERIC = NULL,  
@par_Duracion_Horas NUMERIC(18,2) = NULL,  
@par_Estado SMALLINT,  
@par_USUA_Codigo_Crea SMALLINT    
)    
AS     
BEGIN    
    
INSERT INTO  Rutas    
(    
EMPR_Codigo ,    
Codigo_Alterno,    
Nombre,    
CIUD_Codigo_Origen,    
CIUD_Codigo_Destino,    
CATA_TIRU_Codigo, 
Duracion_Horas,  
Estado,   
USUA_Codigo_Crea,    
Fecha_Crea    
)    
VALUES     
(    
@par_EMPR_Codigo ,     
@par_Codigo_Alterno,    
@par_Nombre,    
@par_CIUD_Codigo_Origen,    
@par_CIUD_Codigo_Destino,    
@par_CATA_TIRU_Codigo,
ISNULL(@par_Duracion_Horas, 0),    
@par_Estado,   
@par_USUA_Codigo_Crea,    
GETDATE()    
)    
    
SELECT @@IDENTITY AS Codigo    
    
END    
GO