﻿PRINT 'gsp_obtener_detalle_liquidacion_planilla_despacho'
GO

DROP PROCEDURE gsp_obtener_detalle_liquidacion_planilla_despacho
GO

CREATE PROCEDURE gsp_obtener_detalle_liquidacion_planilla_despacho
(
	@par_EMPR_Codigo NUMERIC,
	@par_ELPD_Numero NUMERIC
)
AS
BEGIN
	SELECT
		DLPD.EMPR_Codigo,
		DLPD.ELPD_Numero,
		ELPD.Numero_Documento,
		DLPD.CLPD_Codigo,
		CLPD.Nombre,
		DLPD.Observaciones,
		DLPD.Valor,
		CLPD.Operacion,
		CLPD.Concepto_Sistema,
		CLPD.Aplica_Valor_Base_Impuestos
	FROM
		Detalle_Liquidacion_Planilla_Despachos DLPD

		INNER JOIN Encabezado_Liquidacion_Planilla_Despachos ELPD
		ON DLPD.EMPR_Codigo = ELPD.EMPR_Codigo AND DLPD.ELPD_Numero = ELPD.Numero

		LEFT JOIN Conceptos_Liquidacion_Planilla_Despachos CLPD
		ON DLPD.EMPR_Codigo = CLPD.EMPR_Codigo AND DLPD.CLPD_Codigo = CLPD.Codigo

	WHERE
		DLPD.EMPR_Codigo = @par_EMPR_Codigo
		AND DLPD.ELPD_Numero = @par_ELPD_Numero
END
GO