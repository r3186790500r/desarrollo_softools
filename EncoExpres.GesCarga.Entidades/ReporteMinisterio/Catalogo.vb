﻿'Imports System.Data
'Imports System.Data.SqlClient
'Namespace Entidades.ReporteMinisterio
'    Public Class Catalogo

'#Region "Declaracion Variables"

'        Dim strSQL As String
'        Dim objGeneral As General
'        Public strError As String
'        Public bolModificar As Boolean

'        Private objEmpresa As Empresas
'        Private objCatalogoGeneral As CatalogoGeneral
'        Private strCampo1 As String
'        Private strCampo2 As String
'        Private strCampo3 As String
'        Private strCampo4 As String
'        Private strCampo5 As String
'        Private strCampo6 As String
'        Private strCampo7 As String
'        Private strCampo8 As String
'        Private strCampo9 As String

'        Private intLlaves As Integer
'        Public intCampoCodigo As Integer
'        Public intCampoNombre As Integer

'        Public Const LLAVE_NORMAL As Short = 1
'        Public Const LLAVE_COMPUESTA As Short = 2

'        Public Const CUBA_CUENTA_BANCARIA_ANTICIPOS As Short = 1

'#End Region

'#Region "Constructor"

'        Sub New()
'            Try
'                Me.objEmpresa = New Empresas()
'                Me.objCatalogoGeneral = New CatalogoGeneral(Me.objEmpresa)
'                Me.intCampoNombre = 2
'                Me.intCampoCodigo = 1
'                Me.strCampo1 = ""
'                Me.strCampo2 = ""
'                Me.strCampo3 = ""
'                Me.strCampo4 = ""
'                Me.strCampo5 = ""
'                Me.strCampo6 = ""
'                Me.strCampo7 = ""
'                Me.strCampo8 = ""
'                Me.strCampo9 = ""
'                Me.objGeneral = New General
'                Me.intLlaves = 1
'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try


'        End Sub

'        Sub New(ByRef Empresa As Empresas)
'            Try
'                Me.objEmpresa = Empresa
'                Me.objCatalogoGeneral = New CatalogoGeneral(Me.objEmpresa)
'                Me.strCampo1 = ""
'                Me.strCampo2 = ""
'                Me.strCampo3 = ""
'                Me.strCampo4 = ""
'                Me.strCampo5 = ""
'                Me.strCampo6 = ""
'                Me.strCampo7 = ""
'                Me.strCampo8 = ""
'                Me.strCampo9 = ""
'                Me.intCampoNombre = 2
'                Me.intCampoCodigo = 1
'                Me.intLlaves = 1
'                Me.objGeneral = New General
'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try
'        End Sub

'        Sub New(ByVal CodigoCatalogo As String, ByVal CampoCodigo As Integer, ByVal CampoNombre As Integer)
'            Try
'                Me.objEmpresa = New Empresas()
'                Me.objCatalogoGeneral = New CatalogoGeneral(Me.objEmpresa)
'                Me.objCatalogoGeneral.Codigo = CodigoCatalogo
'                Me.intCampoNombre = CampoNombre
'                Me.intCampoCodigo = CampoCodigo
'                Me.strCampo1 = ""
'                Me.strCampo2 = ""
'                Me.strCampo3 = ""
'                Me.strCampo4 = ""
'                Me.strCampo5 = ""
'                Me.strCampo6 = ""
'                Me.strCampo7 = ""
'                Me.strCampo8 = ""
'                Me.strCampo9 = ""
'                Me.objGeneral = New General
'                Me.intLlaves = 1
'                Call Consultar()
'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try
'        End Sub

'        Sub New(ByVal Empresa As Empresas, ByVal CodigoCatalogo As String, ByVal CampoCodigo As Integer, ByVal CampoNombre As Integer)
'            Try
'                Me.objEmpresa = Empresa
'                Me.objCatalogoGeneral = New CatalogoGeneral(Me.objEmpresa)
'                Me.objCatalogoGeneral.Codigo = CodigoCatalogo
'                Me.intCampoNombre = CampoNombre
'                Me.intCampoCodigo = CampoCodigo
'                Me.objGeneral = New General
'                Me.intLlaves = 1
'                Me.strCampo1 = ""
'                Me.strCampo2 = ""
'                Me.strCampo3 = ""
'                Me.strCampo4 = ""
'                Me.strCampo5 = ""
'                Me.strCampo6 = ""
'                Me.strCampo7 = ""
'                Me.strCampo8 = ""
'                Me.strCampo9 = ""
'                Call Consultar()
'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try
'        End Sub

'        Sub New(ByVal Empresa As Empresas, ByVal CodigoCatalogo As String, ByVal Codigo As String)
'            Try
'                Me.objEmpresa = Empresa
'                Me.objCatalogoGeneral = New CatalogoGeneral(Me.objEmpresa)
'                Me.objCatalogoGeneral = New CatalogoGeneral(Me.objEmpresa)
'                Me.objCatalogoGeneral.Codigo = CodigoCatalogo
'                Me.strCampo1 = Codigo
'                Me.objGeneral = New General
'                Me.intLlaves = 1
'                Me.strCampo1 = ""
'                Me.strCampo2 = ""
'                Me.strCampo3 = ""
'                Me.strCampo4 = ""
'                Me.strCampo5 = ""
'                Me.strCampo6 = ""
'                Me.strCampo7 = ""
'                Me.strCampo8 = ""
'                Me.strCampo9 = ""
'                Call Consultar()
'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try
'        End Sub

'        Sub New(ByVal CodigoCatalogo As String, ByVal Codigo As String)
'            Try
'                Me.objEmpresa = New Empresas
'                Me.objCatalogoGeneral = New CatalogoGeneral(Me.objEmpresa)
'                Me.objCatalogoGeneral.Codigo = CodigoCatalogo
'                Me.strCampo1 = Codigo
'                Me.objGeneral = New General
'                Me.intLlaves = 1
'                Me.strCampo1 = ""
'                Me.strCampo2 = ""
'                Me.strCampo3 = ""
'                Me.strCampo4 = ""
'                Me.strCampo5 = ""
'                Me.strCampo6 = ""
'                Me.strCampo7 = ""
'                Me.strCampo8 = ""
'                Me.strCampo9 = ""
'                Call Consultar()
'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try
'        End Sub

'        Sub New(ByVal Empresa As Empresas, ByVal CodigoCatalogo As String)
'            Try
'                Me.objEmpresa = Empresa
'                Me.objCatalogoGeneral = New CatalogoGeneral(Me.objEmpresa)
'                Me.objCatalogoGeneral.Codigo = CodigoCatalogo
'                Me.intCampoNombre = 2
'                Me.intCampoCodigo = 1
'                Me.intLlaves = 1
'                Me.strCampo1 = ""
'                Me.strCampo2 = ""
'                Me.strCampo3 = ""
'                Me.strCampo4 = ""
'                Me.strCampo5 = ""
'                Me.strCampo6 = ""
'                Me.strCampo7 = ""
'                Me.strCampo8 = ""
'                Me.strCampo9 = ""
'                Me.objGeneral = New General
'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try

'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property General() As General
'            Get
'                Return Me.objGeneral
'            End Get
'            Set(ByVal value As General)
'                Me.objGeneral = value
'            End Set
'        End Property

'        Public Property Campo1() As String
'            Get
'                Return Me.strCampo1
'            End Get
'            Set(ByVal value As String)
'                Me.strCampo1 = value
'            End Set
'        End Property

'        Public Property Campo2() As String
'            Get
'                Return Me.strCampo2
'            End Get
'            Set(ByVal value As String)
'                Me.strCampo2 = value
'            End Set
'        End Property

'        Public Property Campo3() As String
'            Get
'                Return Me.strCampo3
'            End Get
'            Set(ByVal value As String)
'                Me.strCampo3 = value
'            End Set
'        End Property

'        Public Property Campo4() As String
'            Get
'                Return Me.strCampo4
'            End Get
'            Set(ByVal value As String)
'                Me.strCampo4 = value
'            End Set
'        End Property

'        Public Property Campo5() As String
'            Get
'                Return Me.strCampo5
'            End Get
'            Set(ByVal value As String)
'                Me.strCampo5 = value
'            End Set
'        End Property

'        Public Property Campo6() As String
'            Get
'                Return Me.strCampo6
'            End Get
'            Set(ByVal value As String)
'                Me.strCampo6 = value
'            End Set
'        End Property

'        Public Property Campo7() As String
'            Get
'                Return Me.strCampo7
'            End Get
'            Set(ByVal value As String)
'                Me.strCampo7 = value
'            End Set
'        End Property

'        Public Property Campo8() As String
'            Get
'                Return Me.strCampo8
'            End Get
'            Set(ByVal value As String)
'                Me.strCampo8 = value
'            End Set
'        End Property

'        Public Property Campo9() As String
'            Get
'                Return Me.strCampo9
'            End Get
'            Set(ByVal value As String)
'                Me.strCampo9 = value
'            End Set
'        End Property

'        Public Property Llaves() As Integer
'            Get
'                Return Me.intLlaves
'            End Get
'            Set(ByVal value As Integer)
'                Me.intLlaves = value
'            End Set
'        End Property

'        Public Property CatalogoGeneral() As CatalogoGeneral
'            Get
'                Return Me.objCatalogoGeneral
'            End Get
'            Set(ByVal value As CatalogoGeneral)
'                Me.objCatalogoGeneral = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Retorna_Codigo() As String
'            Try
'                Select Case Me.intCampoCodigo
'                    Case 1
'                        Retorna_Codigo = RTrim(Me.strCampo1)
'                    Case 2
'                        Retorna_Codigo = Me.strCampo2
'                    Case 3
'                        Retorna_Codigo = Me.strCampo3
'                    Case 4
'                        Retorna_Codigo = Me.strCampo4
'                    Case 5
'                        Retorna_Codigo = Me.strCampo5
'                    Case 6
'                        Retorna_Codigo = Me.strCampo6
'                    Case 7
'                        Retorna_Codigo = Me.strCampo7
'                    Case 8
'                        Retorna_Codigo = Me.strCampo8
'                    Case 9
'                        Retorna_Codigo = Me.strCampo9
'                    Case Else
'                        Retorna_Codigo = Me.strCampo1
'                End Select
'            Catch ex As Exception
'                Retorna_Codigo = False
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Retorna_Codigo: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Retorna_Nombre() As String
'            Try
'                Select Case Me.intCampoNombre
'                    Case 1
'                        Retorna_Nombre = Me.strCampo1
'                    Case 2
'                        Retorna_Nombre = Me.strCampo2
'                    Case 3
'                        Retorna_Nombre = Me.strCampo3
'                    Case 4
'                        Retorna_Nombre = Me.strCampo4
'                    Case 5
'                        Retorna_Nombre = Me.strCampo5
'                    Case 6
'                        Retorna_Nombre = Me.strCampo6
'                    Case 7
'                        Retorna_Nombre = Me.strCampo7
'                    Case 8
'                        Retorna_Nombre = Me.strCampo8
'                    Case 9
'                        Retorna_Nombre = Me.strCampo9
'                    Case Else
'                        Retorna_Nombre = Me.strCampo2
'                End Select
'            Catch ex As Exception
'                Retorna_Nombre = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Retorna_Codigo: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Consultar() As Boolean
'            Try
'                Consultar = True
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT * FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo1 = '" & Me.Retorna_Codigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Me.strCampo1 = sdrCatalogo("Campo1").ToString()
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Me.strCampo3 = sdrCatalogo("Campo3").ToString()
'                    Me.strCampo4 = sdrCatalogo("Campo4").ToString()
'                    Me.strCampo5 = sdrCatalogo("Campo5").ToString()
'                    Me.strCampo6 = sdrCatalogo("Campo6").ToString()
'                    Me.strCampo7 = sdrCatalogo("Campo7").ToString()
'                    Me.strCampo8 = sdrCatalogo("Campo8").ToString()
'                    Me.strCampo9 = sdrCatalogo("Campo9").ToString()

'                    Me.objCatalogoGeneral.Consultar()
'                    Me.intLlaves = Me.objCatalogoGeneral.CamposLlave
'                    Consultar = True
'                Else
'                    Consultar = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Campo1 As String, Optional ByVal Campo2 As String = "") As Boolean
'            Try
'                Dim sdrCatalogo As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                Consultar = True

'                Me.objEmpresa = Empresa
'                Me.strCampo1 = Campo1
'                If Len(Trim(Campo2)) > 0 Then
'                    Me.strCampo2 = Campo2
'                Else
'                    Me.strCampo2 = Campo2
'                End If

'                strSQL = "SELECT * FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo1 = '" & Me.strCampo1 & "'"
'                If Len(Trim(strCampo2)) > 0 Then
'                    strSQL += " AND Campo2 = '" & Me.strCampo2 & "'"
'                End If

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Me.strCampo1 = Trim(sdrCatalogo("Campo1").ToString())
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Me.strCampo3 = sdrCatalogo("Campo3").ToString()
'                    Me.strCampo4 = sdrCatalogo("Campo4").ToString()
'                    Me.strCampo5 = sdrCatalogo("Campo5").ToString()
'                    Me.strCampo6 = sdrCatalogo("Campo6").ToString()
'                    Me.strCampo7 = sdrCatalogo("Campo7").ToString()
'                    Me.strCampo8 = sdrCatalogo("Campo8").ToString()
'                    Me.strCampo9 = sdrCatalogo("Campo9").ToString()

'                    Consultar = True
'                Else
'                    Consultar = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Existe_Registro() As Boolean
'            Try
'                Existe_Registro = True
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo1 = '" & Me.Retorna_Codigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Existe_Registro = True
'                Else
'                    Existe_Registro = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Consultar: " & ex.Message.ToString()
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Campo1 As String, Optional ByVal Campo2 As String = "") As Boolean
'            Try
'                Dim sdrCatalogo As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                Existe_Registro = True

'                Me.objEmpresa = Empresa
'                Me.strCampo1 = Campo1
'                If Len(Trim(Campo2)) > 0 Then
'                    Me.strCampo2 = Campo2
'                Else
'                    Me.strCampo2 = Campo2
'                End If

'                strSQL = "SELECT EMPR_Codigo, Campo2 FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo1 = '" & Me.strCampo1 & "'"
'                If Len(Trim(strCampo2)) > 0 Then
'                    strSQL += " AND Campo2 = '" & Me.strCampo2 & "'"
'                End If

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Existe_Registro = True
'                Else
'                    Existe_Registro = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Registro: " & ex.Message.ToString()
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Existe_Registro(ByVal Codigo As String) As Boolean
'            Try
'                Existe_Registro = True
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.strCampo1 = Codigo

'                strSQL = "SELECT EMPR_Codigo FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo1 = '" & Me.strCampo1 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Existe_Registro = True
'                Else
'                    Existe_Registro = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Registro: " & ex.Message.ToString()
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByVal Codigo As String) As Boolean
'            Try
'                Consultar = True
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.strCampo1 = Codigo

'                strSQL = "SELECT * FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo1 = '" & Me.strCampo1 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Me.strCampo1 = RTrim(sdrCatalogo("Campo1").ToString())
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Me.strCampo3 = sdrCatalogo("Campo3").ToString()
'                    Me.strCampo4 = sdrCatalogo("Campo4").ToString()
'                    Me.strCampo5 = sdrCatalogo("Campo5").ToString()
'                    Me.strCampo6 = sdrCatalogo("Campo6").ToString()
'                    Me.strCampo7 = sdrCatalogo("Campo7").ToString()
'                    Me.strCampo8 = sdrCatalogo("Campo8").ToString()
'                    Me.strCampo9 = sdrCatalogo("Campo9").ToString()

'                    'Me.objCatalogoGeneral.Consultar()
'                    'Me.intLlaves = Me.objCatalogoGeneral.CamposLlave
'                    Consultar = True
'                Else
'                    Consultar = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Registro: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByVal Codigo As String, ByVal CodigoEmpresa As Integer) As Boolean
'            Try
'                Consultar = True
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.strCampo1 = Codigo

'                strSQL = "SELECT * FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & CodigoEmpresa
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo1 = '" & Me.strCampo1 & "'"
'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Me.strCampo1 = sdrCatalogo("Campo1").ToString()
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Me.strCampo3 = sdrCatalogo("Campo3").ToString()
'                    Me.strCampo4 = sdrCatalogo("Campo4").ToString()
'                    Me.strCampo5 = sdrCatalogo("Campo5").ToString()
'                    Me.strCampo6 = sdrCatalogo("Campo6").ToString()
'                    Me.strCampo7 = sdrCatalogo("Campo7").ToString()
'                    Me.strCampo8 = sdrCatalogo("Campo8").ToString()
'                    Me.strCampo9 = sdrCatalogo("Campo9").ToString()

'                    Consultar = True
'                Else
'                    Consultar = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByVal Codigo1 As String, ByVal Codigo2 As String) As Boolean
'            Try
'                Consultar = True
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.strCampo1 = Codigo1
'                Me.strCampo2 = Codigo2
'                Me.intCampoCodigo = 2
'                Me.intCampoNombre = 3

'                strSQL = "SELECT * FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo1 = '" & Me.strCampo1 & "'"
'                strSQL += " AND Campo2 = '" & Me.strCampo2 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Me.strCampo1 = sdrCatalogo("Campo1").ToString()
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Me.strCampo3 = sdrCatalogo("Campo3").ToString()
'                    Me.strCampo4 = sdrCatalogo("Campo4").ToString()
'                    Me.strCampo5 = sdrCatalogo("Campo5").ToString()
'                    Me.strCampo6 = sdrCatalogo("Campo6").ToString()
'                    Me.strCampo7 = sdrCatalogo("Campo7").ToString()
'                    Me.strCampo8 = sdrCatalogo("Campo8").ToString()
'                    Me.strCampo9 = sdrCatalogo("Campo9").ToString()

'                    Me.objCatalogoGeneral.Consultar()
'                    Me.intLlaves = Me.objCatalogoGeneral.CamposLlave
'                    Consultar = True
'                Else
'                    Consultar = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_X_Valor_Entre_Campo3_Campo4(ByVal Valor As Double) As Boolean
'            Try
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.intCampoCodigo = 1
'                Me.intCampoNombre = 2

'                strSQL = "SELECT TOP 1 * FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Convert(Numeric,Campo3) <= " & Valor
'                strSQL += " AND Convert(Numeric,Campo4) >= " & Valor

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Me.strCampo1 = sdrCatalogo("Campo1").ToString()
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Me.strCampo3 = sdrCatalogo("Campo3").ToString()
'                    Me.strCampo4 = sdrCatalogo("Campo4").ToString()
'                    Me.strCampo5 = sdrCatalogo("Campo5").ToString()
'                    Me.objCatalogoGeneral.Consultar()
'                    Me.intLlaves = Me.objCatalogoGeneral.CamposLlave
'                    Cargar_X_Valor_Entre_Campo3_Campo4 = True
'                Else
'                    Cargar_X_Valor_Entre_Campo3_Campo4 = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Cargar_X_Valor_Entre_Campo3_Campo4: " & ex.Message.ToString()
'                Cargar_X_Valor_Entre_Campo3_Campo4 = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_X_Discriminante_Campo3(ByVal ValorCampo3 As String) As Boolean
'            Try
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.intCampoCodigo = 1
'                Me.intCampoNombre = 2

'                strSQL = "SELECT TOP 1 * FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo3 = '" & ValorCampo3 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Me.strCampo1 = sdrCatalogo("Campo1").ToString()
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Me.strCampo3 = sdrCatalogo("Campo3").ToString()
'                    Me.strCampo4 = sdrCatalogo("Campo4").ToString()
'                    Me.strCampo5 = sdrCatalogo("Campo5").ToString()
'                    Me.strCampo6 = sdrCatalogo("Campo6").ToString()
'                    Me.strCampo7 = sdrCatalogo("Campo7").ToString()
'                    Me.strCampo8 = sdrCatalogo("Campo8").ToString()
'                    Me.strCampo9 = sdrCatalogo("Campo9").ToString()

'                    Me.objCatalogoGeneral.Consultar()
'                    Me.intLlaves = Me.objCatalogoGeneral.CamposLlave
'                    Cargar_X_Discriminante_Campo3 = True
'                Else
'                    Cargar_X_Discriminante_Campo3 = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Cargar_X_Discriminante_Campo3: " & ex.Message.ToString()
'                Cargar_X_Discriminante_Campo3 = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_X_Discriminante_Campo2(ByVal CataCodigo As String, ByVal ValorCampo2 As String) As Boolean
'            Try
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.objCatalogoGeneral.Codigo = CataCodigo

'                strSQL = "SELECT TOP 1 * FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo2 = '" & ValorCampo2 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Me.strCampo1 = sdrCatalogo("Campo1").ToString()
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Me.strCampo3 = sdrCatalogo("Campo3").ToString()
'                    Me.strCampo4 = sdrCatalogo("Campo4").ToString()
'                    Me.strCampo5 = sdrCatalogo("Campo5").ToString()
'                    Me.strCampo6 = sdrCatalogo("Campo6").ToString()
'                    Me.strCampo7 = sdrCatalogo("Campo7").ToString()
'                    Me.strCampo8 = sdrCatalogo("Campo8").ToString()
'                    Me.strCampo9 = sdrCatalogo("Campo9").ToString()

'                    Me.objCatalogoGeneral.Consultar()
'                    Me.intLlaves = Me.objCatalogoGeneral.CamposLlave
'                    Cargar_X_Discriminante_Campo2 = True
'                Else
'                    Cargar_X_Discriminante_Campo2 = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Cargar_X_Discriminante_Campo2: " & ex.Message.ToString()
'                Cargar_X_Discriminante_Campo2 = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_X_Discriminantes_Campo3_Campo4(ByVal ValorCampo3 As String, ByVal ValorCampo4 As String) As Boolean
'            Try
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.intCampoCodigo = 1
'                Me.intCampoNombre = 2

'                strSQL = "SELECT TOP 1 * FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo3 = '" & ValorCampo3 & "'"
'                strSQL += " AND Campo4 = '" & ValorCampo4 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Me.strCampo1 = sdrCatalogo("Campo1").ToString()
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Me.strCampo3 = sdrCatalogo("Campo3").ToString()
'                    Me.strCampo4 = sdrCatalogo("Campo4").ToString()
'                    Me.strCampo5 = sdrCatalogo("Campo5").ToString()
'                    Me.strCampo6 = sdrCatalogo("Campo6").ToString()
'                    Me.strCampo7 = sdrCatalogo("Campo7").ToString()
'                    Me.strCampo8 = sdrCatalogo("Campo8").ToString()
'                    Me.strCampo9 = sdrCatalogo("Campo9").ToString()

'                    Me.objCatalogoGeneral.Consultar()
'                    Me.intLlaves = Me.objCatalogoGeneral.CamposLlave
'                    Cargar_X_Discriminantes_Campo3_Campo4 = True
'                Else
'                    Cargar_X_Discriminantes_Campo3_Campo4 = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Cargar_X_Discriminante_Campo3_Campo4: " & ex.Message.ToString()
'                Cargar_X_Discriminantes_Campo3_Campo4 = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_X_Discriminantes_Campo4_Campo5(ByVal ValorCampo4 As String, ByVal ValorCampo5 As String) As Boolean
'            Try
'                Dim sdrCatalogo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.intCampoCodigo = 1
'                Me.intCampoNombre = 2

'                strSQL = "SELECT TOP 1 * FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo4 = '" & ValorCampo4 & "'"
'                strSQL += " AND Campo5 = '" & ValorCampo5 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogo.Read() Then

'                    Me.strCampo1 = sdrCatalogo("Campo1").ToString()
'                    Me.strCampo2 = sdrCatalogo("Campo2").ToString()
'                    Me.strCampo3 = sdrCatalogo("Campo3").ToString()
'                    Me.strCampo4 = sdrCatalogo("Campo4").ToString()
'                    Me.strCampo5 = sdrCatalogo("Campo5").ToString()
'                    Me.strCampo6 = sdrCatalogo("Campo6").ToString()
'                    Me.strCampo7 = sdrCatalogo("Campo7").ToString()
'                    Me.strCampo8 = sdrCatalogo("Campo8").ToString()
'                    Me.strCampo9 = sdrCatalogo("Campo9").ToString()

'                    Me.objCatalogoGeneral.Consultar()
'                    Me.intLlaves = Me.objCatalogoGeneral.CamposLlave
'                    Cargar_X_Discriminantes_Campo4_Campo5 = True
'                Else
'                    Cargar_X_Discriminantes_Campo4_Campo5 = False
'                End If

'                sdrCatalogo.Close()
'                sdrCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Cargar_X_Discriminantes_Campo4_Campo5: " & ex.Message.ToString()
'                Cargar_X_Discriminantes_Campo4_Campo5 = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                If Datos_Requeridos(Mensaje) Then
'                    If Not bolModificar Then
'                        Guardar = Insertar_SQL()
'                    Else
'                        Guardar = Modificar_SQL()
'                    End If
'                Else
'                    Guardar = False
'                End If
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Guardar = False
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Guardar: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Puede_Eliminar(ByRef Mensaje As String) As Boolean
'            Try
'                Select Case Me.objCatalogoGeneral.Codigo
'                    Case "CARG"
'                        If Existe_Cargo_Asignado_Empleados(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case "CABL"
'                        If Existe_Causa_Bloqueo_Asignado_Terceros(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case "COLO"
'                        If Existe_Color_Asignado_Vehiculos(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case "MARC"
'                        If Existe_Marca_Asignada_Vehiculos(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case "LINE"
'                        If Existe_Linea_Asignada_Vehiculos(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case "TICA"
'                        If Existe_Tipo_Carroceria_Asignada_Vehiculos(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case "TIVE"
'                        If Existe_Tipo_Vehiculo_Asignado_Vehiculos(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case "CIVE"
'                        If Existe_Causa_Inactividad_Asignado_Vehiculos(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case "MASE"
'                        If Existe_Marca_Asignada_Semirremolques(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case "OCFA"
'                        If Existe_Otro_Concepto_Asignado_Factura(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case "REGE"
'                        If Existe_Region_Asignada_Departamento(Mensaje) Then
'                            Puede_Eliminar = False
'                        Else
'                            Puede_Eliminar = True
'                        End If
'                    Case Else : Puede_Eliminar = True

'                End Select

'            Catch ex As Exception
'                Puede_Eliminar = False
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Puede_Eliminar: " & ex.Message.ToString()
'            End Try

'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                If Me.Puede_Eliminar(Me.strError) Then
'                    Eliminar = Borrar_SQL()
'                Else
'                    Eliminar = False
'                End If
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Eliminar = False
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Puede_Eliminar: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Datos_Requeridos(ByRef Mesaje As String) As Boolean
'            Try

'                Dim intCont As Short = 0
'                Dim strMensaje As String = ""
'                Dim objConfiguracionCatalogo As ConfiguracionCatalogo

'                Me.strError = ""
'                objConfiguracionCatalogo = New ConfiguracionCatalogo(Me.objEmpresa)
'                Datos_Requeridos = True

'                objConfiguracionCatalogo.Consultar(Me.objEmpresa, Me.objCatalogoGeneral, 1)

'                If objConfiguracionCatalogo.Obligatorio = 1 Then
'                    If Len(Trim(Me.strCampo1)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe Ingresar el " & objConfiguracionCatalogo.Nombre
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    Else
'                        If objConfiguracionCatalogo.Tipo = 1 Then
'                            If Not IsNumeric(Me.strCampo1) Then
'                                intCont += 1
'                                Me.strError += intCont & ". El campo " & objConfiguracionCatalogo.Nombre & " debe ser numérico"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                    End If
'                    If Len(Trim(Me.strCampo1)) > objConfiguracionCatalogo.LongitudCampo Then
'                        intCont += 1
'                        Me.strError += intCont & ". El longitud del campo " & objConfiguracionCatalogo.Nombre & " no puede ser superior a " & objConfiguracionCatalogo.LongitudCampo
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                objConfiguracionCatalogo.Consultar(Me.objEmpresa, Me.objCatalogoGeneral, 2)
'                If objConfiguracionCatalogo.Obligatorio = 1 Then
'                    If Len(Trim(Me.strCampo2)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El campo " & objConfiguracionCatalogo.Nombre & " debe ser numérico"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    Else
'                        If objConfiguracionCatalogo.Tipo = 1 Then
'                            If Not IsNumeric(Me.strCampo2) Then
'                                intCont += 1
'                                Me.strError += intCont & ". Debe Ingresar el " & objConfiguracionCatalogo.Nombre
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                        If Len(Trim(Me.strCampo2)) > objConfiguracionCatalogo.LongitudCampo Then
'                            intCont += 1
'                            Me.strError += intCont & ". El longitud del campo " & objConfiguracionCatalogo.Nombre & " no puede ser superior a " & objConfiguracionCatalogo.LongitudCampo
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If

'                        'No se pude tener esta restricción debido a que los catalogos son muy variables para ser controlados
'                        'If Len(Trim(Me.strCampo2)) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then
'                        '    intCont += 1
'                        '    Me.strError += intCont & ". El " & objConfiguracionCatalogo.Nombre & " no puede tener menos de cuatro (4) carácteres"
'                        '    Me.strError += " <Br />"
'                        '    Datos_Requeridos = False
'                        'End If
'                    End If
'                End If

'                If Me.objCatalogoGeneral.NumeroColumnas >= 3 Then

'                    objConfiguracionCatalogo.Consultar(Me.objEmpresa, Me.objCatalogoGeneral, 3)
'                    If objConfiguracionCatalogo.Obligatorio = 1 Then
'                        If Len(Trim(Me.strCampo3)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe Ingresar el " & objConfiguracionCatalogo.Nombre
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    Else
'                        If objConfiguracionCatalogo.Tipo = 1 Then
'                            If Not IsNumeric(Me.strCampo3) Then
'                                intCont += 1
'                                Me.strError += intCont & ". El campo " & objConfiguracionCatalogo.Nombre & " debe ser numérico"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                        If Len(Trim(Me.strCampo3)) > objConfiguracionCatalogo.LongitudCampo Then
'                            intCont += 1
'                            Me.strError += intCont & ". El longitud del campo " & objConfiguracionCatalogo.Nombre & " no puede ser superior a " & objConfiguracionCatalogo.LongitudCampo
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If

'                End If

'                If Me.objCatalogoGeneral.NumeroColumnas >= 4 Then
'                    objConfiguracionCatalogo.Consultar(Me.objEmpresa, Me.objCatalogoGeneral, 4)
'                    If objConfiguracionCatalogo.Obligatorio = 1 Then
'                        If Len(Trim(Me.strCampo4)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe Ingresar el " & objConfiguracionCatalogo.Nombre
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        Else


'                            If objConfiguracionCatalogo.Tipo = 1 Then
'                                If Not IsNumeric(Me.strCampo4) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". El campo " & objConfiguracionCatalogo.Nombre & " debe ser numérico"
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos = False
'                                End If
'                            End If
'                            If Len(Trim(Me.strCampo4)) > objConfiguracionCatalogo.LongitudCampo Then
'                                intCont += 1
'                                Me.strError += intCont & ". El longitud del campo " & objConfiguracionCatalogo.Nombre & " no puede ser superior a " & objConfiguracionCatalogo.LongitudCampo
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                    End If
'                End If

'                If Me.objCatalogoGeneral.NumeroColumnas >= 5 Then
'                    objConfiguracionCatalogo.Consultar(Me.objEmpresa, Me.objCatalogoGeneral, 5)
'                    If objConfiguracionCatalogo.Obligatorio = 1 Then
'                        If Len(Trim(Me.strCampo5)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe Ingresar el " & objConfiguracionCatalogo.Nombre
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        Else
'                            If objConfiguracionCatalogo.Tipo = 1 Then
'                                If Not IsNumeric(Me.strCampo5) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". El campo " & objConfiguracionCatalogo.Nombre & " debe ser numérico"
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos = False
'                                End If
'                            End If
'                            If Len(Trim(Me.strCampo5)) > objConfiguracionCatalogo.LongitudCampo Then
'                                intCont += 1
'                                Me.strError += intCont & ". El longitud del campo " & objConfiguracionCatalogo.Nombre & " no puede ser superior a " & objConfiguracionCatalogo.LongitudCampo
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                    End If
'                End If

'                If Me.objCatalogoGeneral.NumeroColumnas >= 6 Then
'                    objConfiguracionCatalogo.Consultar(Me.objEmpresa, Me.objCatalogoGeneral, 6)
'                    If objConfiguracionCatalogo.Obligatorio = 1 Then
'                        If Len(Trim(Me.strCampo6)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe Ingresar el " & objConfiguracionCatalogo.Nombre
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        Else
'                            If objConfiguracionCatalogo.Tipo = 1 Then
'                                If Not IsNumeric(Me.strCampo6) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". El campo " & objConfiguracionCatalogo.Nombre & " debe ser numérico"
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos = False
'                                End If
'                            End If
'                            If Len(Trim(Me.strCampo6)) > objConfiguracionCatalogo.LongitudCampo Then
'                                intCont += 1
'                                Me.strError += intCont & ". El longitud del campo " & objConfiguracionCatalogo.Nombre & " no puede ser superior a " & objConfiguracionCatalogo.LongitudCampo
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                    End If
'                End If

'                If Me.objCatalogoGeneral.NumeroColumnas >= 7 Then
'                    objConfiguracionCatalogo.Consultar(Me.objEmpresa, Me.objCatalogoGeneral, 7)
'                    If objConfiguracionCatalogo.Obligatorio = 1 Then
'                        If Len(Trim(Me.strCampo7)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe Ingresar el " & objConfiguracionCatalogo.Nombre
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        Else
'                            If objConfiguracionCatalogo.Tipo = 1 Then
'                                If Not IsNumeric(Me.strCampo7) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". El campo " & objConfiguracionCatalogo.Nombre & " debe ser numérico"
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos = False
'                                End If
'                            End If
'                            If Len(Trim(Me.strCampo7)) > objConfiguracionCatalogo.LongitudCampo Then
'                                intCont += 1
'                                Me.strError += intCont & ". El longitud del campo " & objConfiguracionCatalogo.Nombre & " no puede ser superior a " & objConfiguracionCatalogo.LongitudCampo
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                    End If
'                End If

'                If Me.objCatalogoGeneral.NumeroColumnas >= 8 Then
'                    objConfiguracionCatalogo.Consultar(Me.objEmpresa, Me.objCatalogoGeneral, 8)
'                    If objConfiguracionCatalogo.Obligatorio = 1 Then
'                        If Len(Trim(Me.strCampo8)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe Ingresar la " & objConfiguracionCatalogo.Nombre
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        Else
'                            If objConfiguracionCatalogo.Tipo = 1 Then
'                                If Not IsNumeric(Me.strCampo8) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". El campo " & objConfiguracionCatalogo.Nombre & " debe ser numérico"
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos = False
'                                End If
'                            End If
'                            If Len(Trim(Me.strCampo8)) > objConfiguracionCatalogo.LongitudCampo Then
'                                intCont += 1
'                                Me.strError += intCont & ". El longitud del campo " & objConfiguracionCatalogo.Nombre & " no puede ser superior a " & objConfiguracionCatalogo.LongitudCampo
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                    End If
'                End If

'                If Me.objCatalogoGeneral.NumeroColumnas >= 9 Then
'                    objConfiguracionCatalogo.Consultar(Me.objEmpresa, Me.objCatalogoGeneral, 9)
'                    If objConfiguracionCatalogo.Obligatorio = 1 Then
'                        If Len(Trim(Me.strCampo9)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe Ingresar la " & objConfiguracionCatalogo.Nombre
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        Else
'                            If objConfiguracionCatalogo.Tipo = 1 Then
'                                If Not IsNumeric(Me.strCampo9) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". El campo " & objConfiguracionCatalogo.Nombre & " debe ser numérico"
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos = False
'                                End If
'                            End If
'                            If Len(Trim(Me.strCampo9)) > objConfiguracionCatalogo.LongitudCampo Then
'                                intCont += 1
'                                Me.strError += intCont & ". El longitud del campo " & objConfiguracionCatalogo.Nombre & " no puede ser superior a " & objConfiguracionCatalogo.LongitudCampo
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                    End If
'                End If

'                'Dato requerido para Concepto_Descuento_Liquidacion_Manifiestos - CDLM
'                If Me.objCatalogoGeneral.Codigo = "CDLM" Then
'                    If Val(Me.strCampo5) <> 0 And Val(strCampo9) <> 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar sólo uno de los dos valores para el concepto, Valor Fijo o Valor Porcentaje"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Val(strCampo9) > General.FACTOR_CONVERSION_PORCENTAJE Then
'                        intCont += 1
'                        Me.strError += intCont & ". El porcentaje no puede exceder del 100%"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                'Dato requerido para Concepto_Descuento_Liquidacion_Manifiestos - ETLA
'                If Me.objCatalogoGeneral.Codigo = "ETLA" Then
'                    If Val(Me.strCampo3) > General.CERO Then
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Etiquetamiento_Listado_Auditorias", "Aplica", "Aplica", General.CAMPO_NUMERICO, General.UNO, "") Then
'                            intCont += 1
'                            Me.strError += intCont & ". Ya existe un etiquetamiento aplicado a los listados de auditoría"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If


'                If Not Me.bolModificar And Existe_Valor_Catalogo(strMensaje) Then
'                    intCont += 1
'                    Me.strError += intCont & strMensaje
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If
'            Catch ex As Exception
'                Datos_Requeridos = False
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Datos_Requeridos: " & ex.Message.ToString()
'            End Try

'        End Function
'#End Region

'#Region "Funciones Privadas"

'        Private Function Insertar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_valor_catalogos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_CATA_Codigo", SqlDbType.Char, 4) : ComandoSQL.Parameters("@par_CATA_Codigo").Value = Me.objCatalogoGeneral.Codigo
'                ComandoSQL.Parameters.Add("@par_Campo1", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Campo1").Value = Me.strCampo1
'                ComandoSQL.Parameters.Add("@par_Campo2", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo2").Value = Me.strCampo2
'                ComandoSQL.Parameters.Add("@par_Campo3", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo3").Value = Me.strCampo3
'                If Me.CatalogoGeneral.Codigo = "TITE" Then
'                    ComandoSQL.Parameters.Add("@par_Campo4", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo4").Value = ""
'                    ComandoSQL.Parameters.Add("@par_Campo5", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo5").Value = ""
'                    ComandoSQL.Parameters.Add("@par_Campo6", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo6").Value = ""
'                    ComandoSQL.Parameters.Add("@par_Campo7", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo7").Value = ""
'                Else
'                    ComandoSQL.Parameters.Add("@par_Campo4", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo4").Value = Me.strCampo4
'                    ComandoSQL.Parameters.Add("@par_Campo5", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo5").Value = Me.strCampo5
'                    ComandoSQL.Parameters.Add("@par_Campo6", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo6").Value = Me.strCampo6
'                    ComandoSQL.Parameters.Add("@par_Campo7", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo7").Value = Me.strCampo7
'                End If

'                ComandoSQL.Parameters.Add("@par_Campo8", SqlDbType.VarChar, 1000) : ComandoSQL.Parameters("@par_Campo8").Value = Me.strCampo8
'                ComandoSQL.Parameters.Add("@par_Campo9", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo9").Value = Me.strCampo9

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                Else
'                    Insertar_SQL = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Insertar_SQL = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Insertar_SQL: " & ex.Message.ToString()
'                Catch Exc As Exception
'                    Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Private Function Modificar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_valor_catalogos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_CATA_Codigo", SqlDbType.Char, 4) : ComandoSQL.Parameters("@par_CATA_Codigo").Value = Me.objCatalogoGeneral.Codigo
'                ComandoSQL.Parameters.Add("@par_Campo1", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Campo1").Value = Me.strCampo1
'                ComandoSQL.Parameters.Add("@par_Campo2", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo2").Value = Me.strCampo2
'                ComandoSQL.Parameters.Add("@par_Campo3", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo3").Value = Me.strCampo3

'                If Me.CatalogoGeneral.Codigo = "TITE" Then
'                    ComandoSQL.Parameters.Add("@par_Campo4", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo4").Value = ""
'                    ComandoSQL.Parameters.Add("@par_Campo5", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo5").Value = ""
'                    ComandoSQL.Parameters.Add("@par_Campo6", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo6").Value = ""
'                    ComandoSQL.Parameters.Add("@par_Campo7", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo7").Value = ""
'                Else
'                    ComandoSQL.Parameters.Add("@par_Campo4", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo4").Value = Me.strCampo4
'                    ComandoSQL.Parameters.Add("@par_Campo5", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo5").Value = Me.strCampo5
'                    ComandoSQL.Parameters.Add("@par_Campo6", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo6").Value = Me.strCampo6
'                    ComandoSQL.Parameters.Add("@par_Campo7", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo7").Value = Me.strCampo7
'                End If

'                ComandoSQL.Parameters.Add("@par_Campo8", SqlDbType.VarChar, 1000) : ComandoSQL.Parameters("@par_Campo8").Value = Me.strCampo8
'                ComandoSQL.Parameters.Add("@par_Campo9", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Campo9").Value = Me.strCampo9
'                ComandoSQL.Parameters.Add("@par_Campos_Llave", SqlDbType.Int) : ComandoSQL.Parameters("@par_Campos_Llave").Value = Me.intLlaves


'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Modificar_SQL = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Modificar_SQL: " & ex.Message.ToString()
'                Catch Exc As Exception
'                    Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Private Function Borrar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_valor_catalogos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_CATA_Codigo", SqlDbType.Char, 4) : ComandoSQL.Parameters("@par_CATA_Codigo").Value = Me.objCatalogoGeneral.Codigo
'                ComandoSQL.Parameters.Add("@par_Campo1", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Campo1").Value = Me.strCampo1
'                ComandoSQL.Parameters.Add("@par_Campo2", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Campo2").Value = Me.strCampo2
'                ComandoSQL.Parameters.Add("@par_Opcion", SqlDbType.Int) : ComandoSQL.Parameters("@par_Opcion").Value = Me.intLlaves

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Borrar_SQL = False
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Borrar_SQL: " & ex.Message.ToString()
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try

'        End Function

'        Private Function Existe_Cargo_Asignado_Empleados(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT NombreEmpleado FROM V_Empleados"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CARG_Codigo = " & Me.Campo1 & ""

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- El cargo " & Me.Campo2 & " se encuentra relacionado al empleado " & sdrConsulta("NombreEmpleado")
'                    Existe_Cargo_Asignado_Empleados = True
'                Else
'                    Existe_Cargo_Asignado_Empleados = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Cargo_Asignado_Empleados = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Cargo_Asignado_Empleados: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Causa_Bloqueo_Asignado_Terceros(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre + Apellido1 AS NombreTercero FROM Terceros"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CABL_Codigo = " & Me.Campo1 & ""

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- La causa de bloqueo " & Me.Campo2 & " se encuentra relacionado a " & sdrConsulta("NombreTercero")
'                    Existe_Causa_Bloqueo_Asignado_Terceros = True
'                Else
'                    Existe_Causa_Bloqueo_Asignado_Terceros = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Causa_Bloqueo_Asignado_Terceros = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Causa_Bloqueo_Asignado_Terceros: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Color_Asignado_Vehiculos(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Placa FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND COLO_Codigo = " & Me.Campo1 & ""

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- El color " & Me.Campo2 & " se encuentra relacionado al vehículo " & sdrConsulta("Placa")
'                    Existe_Color_Asignado_Vehiculos = True
'                Else
'                    Existe_Color_Asignado_Vehiculos = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Color_Asignado_Vehiculos = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Color_Asignado_Vehiculos: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Marca_Asignada_Vehiculos(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Placa FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND MARC_Codigo = '" & Me.Campo1 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- La marca " & Me.Campo2 & " se encuentra relacionada al vehículo " & sdrConsulta("Placa")
'                    Existe_Marca_Asignada_Vehiculos = True
'                Else
'                    Existe_Marca_Asignada_Vehiculos = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Marca_Asignada_Vehiculos = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Marca_Asignada_Vehiculos: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Linea_Asignada_Vehiculos(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Placa FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND MARC_Codigo = '" & Me.Campo1 & "'"
'                strSQL += " AND LINE_Codigo = '" & Me.Campo2 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- La linea " & Me.Campo2 & " se encuentra relacionado al vehículo " & sdrConsulta("Placa")
'                    Existe_Linea_Asignada_Vehiculos = True
'                Else
'                    Existe_Linea_Asignada_Vehiculos = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Linea_Asignada_Vehiculos = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Linea_Asignada_Vehiculos: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Tipo_Vehiculo_Asignado_Vehiculos(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Placa FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TIVE_Codigo = '" & Me.Campo1 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- El tipo vehículo " & Me.Campo2 & " se encuentra relacionado al vehículo " & sdrConsulta("Placa")
'                    Existe_Tipo_Vehiculo_Asignado_Vehiculos = True
'                Else
'                    Existe_Tipo_Vehiculo_Asignado_Vehiculos = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Tipo_Vehiculo_Asignado_Vehiculos = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Tipo_Vehiculo_Asignado_Vehiculos: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Tipo_Carroceria_Asignada_Vehiculos(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Placa FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TICA_Codigo = '" & Me.Campo1 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- El tipo de carroceria " & Me.Campo2 & " se encuentra relacionado al vehículo " & sdrConsulta("Placa")
'                    Existe_Tipo_Carroceria_Asignada_Vehiculos = True
'                Else
'                    Existe_Tipo_Carroceria_Asignada_Vehiculos = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Tipo_Carroceria_Asignada_Vehiculos = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Tipo_Carroceria_Asignada_Vehiculos: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Causa_Inactividad_Asignado_Vehiculos(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Placa FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CIVE_Codigo = " & Me.Campo1 & ""

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- La causa de inactividad " & Me.Campo2 & " se encuentra relacionada al vehículo " & sdrConsulta("Placa")
'                    Existe_Causa_Inactividad_Asignado_Vehiculos = True
'                Else
'                    Existe_Causa_Inactividad_Asignado_Vehiculos = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Causa_Inactividad_Asignado_Vehiculos = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Causa_Inactividad_Asignado_Vehiculos: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Marca_Asignada_Semirremolques(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Placa FROM Semiremolques"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND MASE_Codigo = '" & Me.Campo1 & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- La marca " & Me.Campo2 & " se encuentra relacionada al semirremolque " & sdrConsulta("Placa")
'                    Existe_Marca_Asignada_Semirremolques = True
'                Else
'                    Existe_Marca_Asignada_Semirremolques = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Marca_Asignada_Semirremolques = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Marca_Asignada_Semirremolques: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Otro_Concepto_Asignado_Factura(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Codigo FROM Detalle_Otro_Concepto_Facturas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.Campo1

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- el concepto " & Me.Campo2 & " se encuentra asignado en Facturas de otros conceptos "
'                    Existe_Otro_Concepto_Asignado_Factura = True
'                Else
'                    Existe_Otro_Concepto_Asignado_Factura = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Otro_Concepto_Asignado_Factura = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Otro_Concepto_Asignado_Factura: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Region_Asignada_Departamento(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre FROM Departamentos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND REGE_Codigo = " & Me.Campo1

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    strMensaje = "- la región " & Me.Campo2 & " se encuentra asignada al departamento " & sdrConsulta("Nombre")
'                    Existe_Region_Asignada_Departamento = True
'                Else
'                    Existe_Region_Asignada_Departamento = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Region_Asignada_Departamento = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la Clase " & Me.ToString & " en la función Existe_Region_Asignada_Departamento: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Existe_Valor_Catalogo(ByRef strMensaje As String) As Boolean
'            Try
'                Dim sdrConsulta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Campo1 FROM Valor_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Campo1 = '" & Me.Campo1.Trim & "'"
'                If Me.objCatalogoGeneral.CamposLlave = 2 Then
'                    strSQL += " AND Campo2 = '" & Me.Campo2.Trim & "'"
'                End If
'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrConsulta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConsulta.Read() Then
'                    If Me.objCatalogoGeneral.CamposLlave = 1 Then
'                        strMensaje = "- El valor del catálogo " & Me.Campo1 & " ya se encuentra definido en el catálogo " & Me.objCatalogoGeneral.Nombre
'                    Else
'                        strMensaje = "- Los valores del catálogo " & Me.Campo1 & "-" & Me.Campo2 & " ya se encuentran definidos en el catálogo " & Me.objCatalogoGeneral.Nombre
'                    End If
'                    Existe_Valor_Catalogo = True
'                Else
'                    Existe_Valor_Catalogo = False
'                End If
'                sdrConsulta.Close()
'                sdrConsulta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Existe_Valor_Catalogo = False
'                Me.strCampo1 = ""
'                Me.strError = "Sucedió un error en la página " & Me.ToString & " en la función Existe_Valor_Catalogo: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'#End Region
'    End Class

'End Namespace
