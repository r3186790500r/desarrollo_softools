﻿print ('gsp_consultar_impuestos_terceros') 
go
drop procedure gsp_consultar_impuestos_terceros
go
create procedure gsp_consultar_impuestos_terceros
(@par_EMPR_Codigo SMALLINT,
@par_TERC_Codigo numeric,
@par_TIIM_Codigo numeric
)
as
begin
select  
ENIM.EMPR_Codigo,
ENIM.Codigo,
ENIM.Nombre,
ENIM.CATA_TIIM_Codigo,
ENIM.Estado
from Tercero_Impuestos TEIM

left join Encabezado_Impuestos ENIM ON
TEIM.EMPR_Codigo = ENIM.EMPR_Codigo
AND TEIM.ENIM_Codigo = ENIM.Codigo

where 
TEIM.EMPR_Codigo = @par_EMPR_Codigo
AND TEIM.TERC_Codigo = @par_TERC_Codigo
AND ENIM.CATA_TIIM_Codigo = @par_TIIM_Codigo
end
go