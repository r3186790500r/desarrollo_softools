﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos


Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref="EquipoMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EquipoMantenimiento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EquipoMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EquipoMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Nombre = Read(lector, "Nombre")
            Codigo = Read(lector, "Codigo")
            Codigo_Alterno = Read(lector, "Codigo_Alterno")
            KmUso = Read(lector, "KM_Uso")
            Valor_Uso = Read(lector, "Valor_Uso")
            CapacidadCargue = Read(lector, "CapacidadCargue")
            Ancho = Read(lector, "Ancho")
            Largo = Read(lector, "Largo")
            Alto = Read(lector, "Alto")
            Serie = Read(lector, "Serie")
            Modelo = Read(lector, "Modelo")
            TiempoUso = Read(lector, "Tiempo_Uso")
            Estado = Read(lector, "Estado")
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Placa")}
            Propietario = New Tercero With {.Codigo = Read(lector, "TERC_Propietario")}
            EstadoEquipoMantenimiento = New EstadoEquipoMantenimiento With {.Codigo = Read(lector, "ESEM_Codigo"), .Nombre = Read(lector, "EstadoMantenimiento")}
            MarcaEquipoMantenimiento = New MarcaEquipoMantenimiento With {.Codigo = Read(lector, "MAEM_Codigo")}
            TipoEquipoMantenimiento = New TipoEquipoMantenimiento With {.Codigo = Read(lector, "TIEM_Codigo"), .Nombre = Read(lector, "Tipo_Equipo")}
            UnidadReferenciaMantenimiento = New UnidadReferenciaMantenimiento With {.Codigo = Read(lector, "UNFM_Codigo")}
            Try
                TotalRegistros = Read(lector, "TotalRegistros")
            Catch
                TotalRegistros = 0
            End Try

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Codigo_Alterno As String

        ''' <summary>
        ''' Obtiene o establece el contacto del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Vehiculo As Vehiculos


        ''' <summary>
        ''' Obtiene o establece la ciudad del almacen
        ''' </summary>
        <JsonProperty>
        Public Property EstadoEquipoMantenimiento As EstadoEquipoMantenimiento


        ''' <summary>
        ''' Obtiene o establece la direccion del almacen
        ''' </summary>
        <JsonProperty>
        Public Property MarcaEquipoMantenimiento As MarcaEquipoMantenimiento


        ''' <summary>
        ''' Obtiene o establece el telefono del almacen
        ''' </summary>
        <JsonProperty>
        Public Property TipoEquipoMantenimiento As TipoEquipoMantenimiento


        ''' <summary>
        ''' Obtiene o estableceel fax del almacen
        ''' </summary>
        <JsonProperty>
        Public Property UnidadReferenciaMantenimiento As UnidadReferenciaMantenimiento

        ''' <summary>
        ''' Obtiene o establece el estado del almacen
        ''' </summary>
        <JsonProperty>
        Public Property KmUso As Integer
        <JsonProperty>
        Public Property Valor_Uso As Integer

        <JsonProperty>
        Public Property CapacidadCargue As Integer
        <JsonProperty>
        Public Property Ancho As Integer
        <JsonProperty>
        Public Property Largo As Integer
        <JsonProperty>
        Public Property Alto As Integer
        ''' <summary>
        ''' Obtiene o estableceel fax del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Propietario As Tercero

        ''' <summary>
        ''' Obtiene o establece el estado del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Serie As String
        ''' <summary>
        ''' Obtiene o estableceel fax del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Modelo As String

        ''' <summary>
        ''' Obtiene o establece el estado del almacen
        ''' </summary>
        <JsonProperty>
        Public Property TiempoUso As Integer
        <JsonProperty>
        Public Property DocumentosEquipoMantenimiento As IEnumerable(Of EquipoMantenimientoDocumento)

        <JsonProperty>
        Public Property ID_Documentos_Eliminar As IEnumerable(Of Integer)
    End Class
End Namespace
