﻿Imports EncoExpres.GesCarga.Fachada.Despachos
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Fachada.Basico.Despachos

Namespace Despachos
    Public NotInheritable Class LogicaLiquidaciones
        Inherits LogicaBase(Of Liquidacion)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Liquidacion)

        Sub New(persistencia As IPersistenciaBase(Of Liquidacion))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Liquidacion) As Respuesta(Of IEnumerable(Of Liquidacion))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Liquidacion))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Liquidacion))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Liquidacion) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long
            Dim validarDatos As Respuesta(Of Liquidacion)

            If Not IsNothing(entidad.ParametrosCalculos) Then
                validarDatos = CalcularValores(entidad)
                If validarDatos.ProcesoExitoso Then
                    entidad = validarDatos.Datos
                End If
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Numero, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Liquidacion) As Respuesta(Of Liquidacion)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Liquidacion)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Liquidacion)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>

        Public Function Anular(entidad As Liquidacion) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaLiquidaciones).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function ObtenerPlanilla(filtro As Liquidacion) As Respuesta(Of PlanillaDespachos)
            Dim consulta As PlanillaDespachos
            consulta = New PersistenciaLiquidaciones().ObtenerPlanillaDespacho(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PlanillaDespachos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of PlanillaDespachos)(consulta)
        End Function

        Public Function Rechazar(entidad As Liquidacion) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim rechazo As Boolean = False

            rechazo = CType(_persistencia, PersistenciaLiquidaciones).Rechazar(entidad)

            If Not rechazo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function Aprobar(entidad As Liquidacion) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim aprobo As Boolean = False

            aprobo = CType(_persistencia, PersistenciaLiquidaciones).Aprobar(entidad)

            If Not aprobo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        ' Funcion que recalcula valores pertinentes monetarios para mostrarlos en el front y otra vez cuando se llame guardar
        ' Utima modificacion por: Felipe Pieschacon
        ' Fecha ultimos cambios: 2021 12 09
        Public Function CalcularValores(entidad As Liquidacion) As Respuesta(Of Liquidacion)
            Dim strError As String = String.Empty

            If IsNothing(entidad) Then
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
                Return New Respuesta(Of Liquidacion)(entidad) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Dim TotalConceptosSuma As Double
            Dim TotalConceptosResta As Double
            Dim ValorConceptos As Double
            Dim TotalBaseImpuestos As Double

            entidad.ParametrosCalculos.BaseFleteTranportadorCVT = 0

            'Total conceptos
            For Each item In entidad.ListaDetalleLiquidacion
                If IsNothing(item.Valor) Then
                    ValorConceptos += 0
                Else
                    If item.Operacion = 2 Then 'Resta
                        TotalConceptosResta += item.Valor
                    ElseIf item.Operacion = 1 Then 'Suma
                        TotalConceptosSuma += item.Valor
                        entidad.ParametrosCalculos.BaseFleteTranportadorCVT += item.Valor
                    End If
                End If

                If item.Valor > 0 Then
                    If item.AplicaValorBase = 1 Then
                        TotalBaseImpuestos += item.Valor
                    End If
                End If
            Next
            If entidad.ParametrosCalculos.ManejoCombinacionCVT Then
                If Not entidad.ParametrosCalculos.Cambiocvt Then
                    entidad.ParametrosCalculos.BaseFleteTranportadorCVT += entidad.ParametrosCalculos.TotalFleteTransportador
                    Dim respuestaCvt = CargarCombinacionesCvt(entidad)
                    If respuestaCvt.ProcesoExitoso Then
                        entidad.ParametrosCalculos.BaseFleteTranportadorCVT = respuestaCvt.Datos.ParametrosCalculos.BaseFleteTranportadorCVT
                        entidad.ValorCvt = respuestaCvt.Datos.ValorCvt
                    End If
                Else
                    If IsNothing(entidad.ValorCvt) Then
                        entidad.ValorCvt = 0
                    End If
                End If

            End If

            '------------------------------
            entidad.ListaImpuestosConceptosLiquidacion = {}

            '    //Agregar los impuestos de cada concepto liquidación
            For Each item In entidad.ListaDetalleLiquidacion
                If Not IsNothing(item.ConceptoLiquidacion.ListadoImpuestos) Then
                    For Each itemImpuesto In item.ConceptoLiquidacion.ListadoImpuestos
                        itemImpuesto.Valor_base = item.Valor
                        itemImpuesto.ValorImpuesto = itemImpuesto.Valor_base * itemImpuesto.Valor_tarifa
                    Next
                End If
            Next
            '    //Fin impuestos conceptos

            For Each item In entidad.ListaDetalleLiquidacion
                For Each itemListTemp In entidad.ParametrosCalculos.ListadoImpuestostemp
                    If item.Valor > 0 Then
                        If itemListTemp.ConceptoLiquidacion.Codigo = item.ConceptoLiquidacion.Codigo Then
                            If entidad.ListaImpuestosConceptosLiquidacion.Count = 0 Then
                                If item.Valor > itemListTemp.Valor_base Then
                                    itemListTemp.Valor_base = item.Valor
                                End If
                                entidad.ListaImpuestosConceptosLiquidacion = {itemListTemp}
                                TotalBaseImpuestos = itemListTemp.Valor_base
                            Else
                                If item.Valor > itemListTemp.Valor_base Then
                                    itemListTemp.Valor_base = item.Valor
                                End If
                                Dim cont = 0
                                For Each itemConcep In entidad.ListaImpuestosConceptosLiquidacion
                                    If itemConcep.Codigo = itemListTemp.Codigo Then
                                        itemConcep.Valor_base += itemListTemp.Valor_base
                                        TotalBaseImpuestos += itemConcep.Valor_base
                                        cont += 1
                                    End If
                                Next
                                If cont = 0 Then
                                    entidad.ListaImpuestosConceptosLiquidacion = entidad.ListaImpuestosConceptosLiquidacion.Concat({itemListTemp})
                                    TotalBaseImpuestos = itemListTemp.Valor_base
                                End If
                            End If
                        End If
                    End If
                Next
            Next

            ValorConceptos = TotalConceptosResta - TotalConceptosSuma + entidad.ValorCvt
            entidad.ValorConceptosLiquidacion = ValorConceptos
            For Each item In entidad.ParametrosCalculos.ListadoImpuestostemp
                If item.ConceptoLiquidacion.Codigo = 0 Then
                    If entidad.ValorFleteTranportador > item.Valor_base Then
                        item.Valor_base = Math.Round(entidad.ValorFleteTranportador)
                    End If
                    If entidad.ListaImpuestosConceptosLiquidacion.Count = 0 Then
                        entidad.ListaImpuestosConceptosLiquidacion = {item}
                    Else
                        Dim cont = 0
                        For Each itemConcep In entidad.ListaImpuestosConceptosLiquidacion
                            If itemConcep.Codigo = item.Codigo Then
                                itemConcep.Valor_base += item.Valor_base
                                cont += 1
                            End If
                        Next
                        If cont = 0 Then
                            entidad.ListaImpuestosConceptosLiquidacion = entidad.ListaImpuestosConceptosLiquidacion.Concat({item})
                        End If
                    End If
                End If
            Next

            Dim TotalValorImpuestos = 0
            For Each item In entidad.ListaImpuestosConceptosLiquidacion
                item.ValorImpuesto = Math.Round(item.Valor_base * item.Valor_tarifa)
                TotalValorImpuestos += item.ValorImpuesto
            Next

            entidad.ValorConceptosLiquidacion = (TotalConceptosResta - TotalConceptosSuma + entidad.ValorCvt) * -1
            entidad.ValorImpuestos = Math.Round(TotalValorImpuestos)
            Dim PlanillaValidada = True

            '    //------------------------------

            If entidad.ParametrosCalculos.CodDespacho = 301 AndAlso entidad.ParametrosCalculos.ManejoRecalculoFleteLiquidacionPesoCumplido Then
                entidad.ValorBaseImpuestos = Math.Round(entidad.ValorFleteTranportador * entidad.PesoCumplido / 1000)
                entidad.ValorPagar = Math.Round((entidad.ValorFleteTranportador * entidad.PesoCumplido / 1000) - ValorConceptos - entidad.ValorImpuestos)
            Else
                entidad.ValorBaseImpuestos = entidad.ValorFleteTranportador
                entidad.ValorPagar = entidad.ValorFleteTranportador - ValorConceptos - entidad.ValorImpuestos
            End If

            Return New Respuesta(Of Liquidacion)(entidad) With {.ProcesoExitoso = True}

        End Function

        ' Funcion para calcular cvt, usada en front y por CalcularValores al guardar
        ' Utima modificacion por: Felipe Pieschacon
        ' Fecha ultimos cambios: 2021 12 09
        Public Function CargarCombinacionesCvt(entidad As Liquidacion) As Respuesta(Of Liquidacion)
            Dim strError As String = String.Empty

            If IsNothing(entidad) Then
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
                Return New Respuesta(Of Liquidacion)(entidad) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Dim ExisteCombinacion = False

            Dim ValorCvt As Double = 0

            Dim filtroCombinacionContratoVinculacion As New CombinacionContratoVinculacion
            With filtroCombinacionContratoVinculacion
                .CodigoEmpresa = entidad.CodigoEmpresa
                .Estado = 1 'ESTADO_DEFINITIVO
            End With
            Dim persistenciaCombinacionContratoVinculacion As IPersistenciaBase(Of CombinacionContratoVinculacion)
            Dim listaCombinacionCvt = CType(persistenciaCombinacionContratoVinculacion, PersistenciaCombinacionContratoVinculacion).Consultar(filtroCombinacionContratoVinculacion)

            '    //--Valida Tabla de Combinaciones
            If Not IsNothing(listaCombinacionCvt) Then
                If listaCombinacionCvt.Count > 0 Then
                    Dim listaPesos As New List(Of Object)
                    Dim listaPesosValidos As New List(Of Object)
                    For Each item In listaCombinacionCvt
                        Dim pesoValido = 0
                        Dim peso = 0
                        Dim codigo = 0
                        '--Pesos Validos
                        If Not item.Cliente.Codigo = 0 Then
                            pesoValido += 7
                        End If
                        If Not item.Remitente.Codigo = 0 Then
                            pesoValido += 5
                        End If
                        If Not item.Tenedor.Codigo = 0 Then
                            pesoValido += 3
                        End If
                        If Not item.Producto.Codigo = 0 Then
                            pesoValido += 1
                        End If
                        '--Pesos Validos
                        '--Pesos Compara
                        If Not item.Cliente.Codigo = entidad.ParametrosCalculos.CodCliente Then
                            peso += 7
                        End If
                        If Not item.Remitente.Codigo = entidad.ParametrosCalculos.CodRemitente Then
                            peso += 5
                        End If
                        If Not item.Tenedor.Codigo = entidad.ParametrosCalculos.CodTenedor Then
                            peso += 3
                        End If
                        If Not item.Producto.Codigo = entidad.ParametrosCalculos.CodProducto Then
                            peso += 1
                        End If
                        '--Pesos Compara
                        Dim temp As New Object
                        With temp
                            .Codigo = item.Codigo
                            .Peso = peso
                            .PesoValido = pesoValido
                        End With
                        listaPesos.Add(temp)
                    Next
                    listaPesos.Sort(Function(x, y) y.Peso.CompareTo(x.Peso)) 'ordena lista segun peso de forma descendente
                    For Each item In listaPesos
                        If item.Peso > 0 AndAlso item.Peso = item.PesoValido Then
                            listaPesosValidos.Add(item)
                        End If
                    Next
                    If listaPesosValidos.Count > 0 Then
                        For Each item In listaCombinacionCvt
                            If item.Codigo = listaPesosValidos(0).Codigo Then
                                If item.TipoCobro.Codigo = 23401 Then 'Valor
                                    ValorCvt = item.Valor
                                ElseIf item.TipoCobro.Codigo = 23402 Then 'Porcentaje
                                    ValorCvt = Math.Round((entidad.ParametrosCalculos.BaseFleteTranportadorCVT * item.Valor) / 100)
                                End If
                                ExisteCombinacion = True
                                Exit For
                            End If
                        Next
                    End If
                End If
            End If
            '    //--Valida Tabla de Combinaciones
            '    //--Valida Por Concepto Liquidacion
            If Not ExisteCombinacion Then
                Dim filtroConceptoLiquidacion As New ConceptoLiquidacionPlanillaDespacho
                With filtroConceptoLiquidacion
                    .CodigoEmpresa = entidad.CodigoEmpresa
                    .Estado = 1 'ESTADO_DEFINITIVO
                    .Codigo = 6 'CONTRATO DE VINCULACIÓN TEMPORAL
                End With
                Dim persistenciaConceptoLiquidacion As IPersistenciaBase(Of ConceptoLiquidacionPlanillaDespacho)
                Dim res = CType(persistenciaConceptoLiquidacion, PersistenciaConceptoLiquidacion).Obtener(filtroConceptoLiquidacion)
                If Not IsNothing(res) Then
                    Dim tmpValor = Math.Round(entidad.ParametrosCalculos.BaseFleteTranportadorCVT * res.ValorPorcentaje / 100)
                    If tmpValor < res.ValorMinimo Then
                        tmpValor = res.ValorMinimo
                    End If
                    If tmpValor > res.ValorMaximo Then
                        tmpValor = res.ValorMaximo
                    End If
                    ValorCvt = tmpValor
                End If
            End If
            '    //--Valida Por Concepto Liquidacion
            ValorCvt *= -1
            entidad.ValorCvt = ValorCvt

            Return New Respuesta(Of Liquidacion)(entidad) With {.ProcesoExitoso = True}

        End Function

    End Class
End Namespace