﻿Print 'CREATE PROCEDURE gsp_suma_plan_puntos_vehiculo'
GO
DROP PROCEDURE gsp_suma_plan_puntos_vehiculo
GO
CREATE PROCEDURE gsp_suma_plan_puntos_vehiculo
AS
BEGIN
	Declare @FechaVigenciaHoy DateTime = GETDATE();
	Declare @EstadoAnulado int = 1; -- Valor Anulacion
	Declare @EstadoActivo int = 1; -- Valor Activo
	Declare @EstadoInactivo int = 0; -- Valor Inactivo

	DECLARE @tmpTablaPuntos TABLE (
		EMPR_Codigo smallint,
		VEHI_Codigo numeric,
		PuntosCausales numeric,
		PuntosDespachos numeric
	)

	--Validar fecha vigencia causales
	UPDATE Detalle_Causales_Plan_Puntos_Vehiculos SET Estado = @EstadoInactivo
	WHERE Estado = @EstadoActivo
	AND Anulado <> @EstadoAnulado
	AND Fecha_Vence < @FechaVigenciaHoy


	--Validar fecha vigencia Despachos
	UPDATE Detalle_Despachos_Plan_Puntos_Vehiculos SET Estado = @EstadoInactivo
	WHERE Estado = @EstadoActivo
	AND Anulado <> @EstadoAnulado
	AND Fecha_Vence < @FechaVigenciaHoy


	-- Calcula Temporalmente Puntos Causales y Puntos Despachos
	INSERT INTO @tmpTablaPuntos
	SELECT 
	PPVE.EMPR_Codigo,
	PPVE.VEHI_Codigo,
	ISNULL((SELECT SUM(ISNULL(Puntos,0)) FROM Detalle_Causales_Plan_Puntos_Vehiculos 
		WHERE PPVE.EMPR_Codigo = EMPR_Codigo 
		AND PPVE.VEHI_Codigo = VEHI_Codigo 
		AND Estado = @EstadoActivo
		--AND @FechaVigenciaHoy >= Fecha_Inicio AND @FechaVigenciaHoy <= Fecha_Vence
		AND Anulado <> @EstadoAnulado),0) AS PuntosCausales,
	ISNULL((SELECT SUM(ISNULL(Puntos,0)) FROM Detalle_Despachos_Plan_Puntos_Vehiculos 
		WHERE PPVE.EMPR_Codigo = EMPR_Codigo
		AND PPVE.VEHI_Codigo = VEHI_Codigo
		AND Estado = @EstadoActivo
		--AND @FechaVigenciaHoy >= Fecha_Inicio AND @FechaVigenciaHoy <= Fecha_Vence
		AND Anulado <> @EstadoAnulado),0) AS PuntosDespachos
	FROM Plan_Puntos_Vehiculos PPVE


	UPDATE
		Plan_Puntos_Vehiculos 
	SET
		Total_Puntos = TMPPunto.PuntosCausales + TMPPunto.PuntosDespachos,
		Fecha_Actualiza_Puntos = Getdate()
	FROM
		Plan_Puntos_Vehiculos AS PPVE

		INNER JOIN @tmpTablaPuntos AS TMPPunto
		ON PPVE.EMPR_Codigo = TMPPunto.EMPR_Codigo
		AND PPVE.VEHI_Codigo = TMPPunto.VEHI_Codigo
END
GO