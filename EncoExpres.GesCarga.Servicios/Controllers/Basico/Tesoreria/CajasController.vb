﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Negocio.Basico.Tesoreria

''' <summary>
''' Controlador <see cref="CajasController"/>
''' </summary>
<Authorize>
Public Class CajasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Cajas) As Respuesta(Of IEnumerable(Of Cajas))
        Return New LogicaCajas(CapaPersistenciaCajas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Cajas) As Respuesta(Of Cajas)
        Return New LogicaCajas(CapaPersistenciaCajas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Cajas) As Respuesta(Of Long)
        Return New LogicaCajas(CapaPersistenciaCajas).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Cajas) As Respuesta(Of Boolean)
        Return New LogicaCajas(CapaPersistenciaCajas).Anular(entidad)
    End Function
End Class
