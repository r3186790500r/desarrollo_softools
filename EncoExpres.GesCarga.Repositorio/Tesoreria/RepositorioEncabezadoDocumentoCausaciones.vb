﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Repositorio.Contabilidad
Imports System.Transactions

Namespace Tesoreria


    Public NotInheritable Class RepositorioEncabezadoDocumentoCausaciones
        Inherits RepositorioBase(Of EncabezadoDocumentoCausaciones)

        Public Overrides Function Consultar(filtro As EncabezadoDocumentoCausaciones) As IEnumerable(Of EncabezadoDocumentoCausaciones)
            Dim lista As New List(Of EncabezadoDocumentoCausaciones)
            Dim item As EncabezadoDocumentoCausaciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                'If filtro.TipoDocumento > 0 Then
                '    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                'End If

                If Not String.IsNullOrWhiteSpace(filtro.NombreTercero) Then
                    conexion.AgregarParametroSQL("@par_Tercero", filtro.NombreTercero)
                End If

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If Not IsNothing(filtro.NumeroFactura) Then
                    If filtro.NumeroFactura > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Factura", filtro.NumeroFactura)
                    End If
                End If

                If (filtro.FechaInicial > Date.MinValue) And (filtro.FechaFinal >= filtro.FechaInicial) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                'If Not String.IsNullOrWhiteSpace(filtro.NombreBeneficiario) Then
                '    conexion.AgregarParametroSQL("@par_Beneficiario", filtro.NombreBeneficiario)
                'End If
                'If Not IsNothing(filtro.DocumentoOrigen) Then
                '    If filtro.DocumentoOrigen.Codigo <> 2600 Then
                '        conexion.AgregarParametroSQL("@par_Codigo_Documento", filtro.DocumentoOrigen.Codigo)
                '    End If
                'End If
                If Not IsNothing(filtro.TipoComprobante) Then
                    If filtro.TipoComprobante.Codigo > 2700 Then
                        conexion.AgregarParametroSQL("@par_CATA_TICC_Codigo", filtro.TipoComprobante.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.OficinaDestino) Then
                    If filtro.OficinaDestino.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina_Destino", filtro.OficinaDestino.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina", filtro.Oficina.Codigo)
                    End If
                End If
                If filtro.Estado >= 0 And filtro.Estado <> 2 And filtro.Estado <> 3 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If
                If filtro.Estado = 3 Then
                    conexion.AgregarParametroSQL("@par_Estado", 2)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If
                'If filtro.CodigoTercero > 0 Then
                '    conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.CodigoTercero)
                'End If
                'If filtro.CodigoLegalizacionGastos > 0 Then
                '    conexion.AgregarParametroSQL("@par_ELGC_Numero", 0)
                'End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_documento_causaciones")

                While resultado.Read
                    item = New EncabezadoDocumentoCausaciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As EncabezadoDocumentoCausaciones) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_CATA_TICC_Codigo", entidad.TipoComprobante.Codigo)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", 35) 'Comprobante Causacion
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Titular", entidad.Proveedor.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
                    conexion.AgregarParametroSQL("@par_Numero_Factura", entidad.NumeroFactura)
                    conexion.AgregarParametroSQL("@par_Fecha_Factura", entidad.FechaFactura, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Vence_Factura", entidad.FechaVencimientoFactura, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Subtotal", entidad.Subtotal)
                    conexion.AgregarParametroSQL("@par_Valor_IVA", entidad.ValorIva)
                    conexion.AgregarParametroSQL("@par_Total", entidad.ValorTotal)
                    conexion.AgregarParametroSQL("@par_Valor_Debito", entidad.Debito)
                    conexion.AgregarParametroSQL("@par_Valor_Credito", entidad.Credito)
                    conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.NumeroDocumentoOrigen)
                    conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Creacion", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Destino", entidad.OficinaDestino.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_causaciones")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero").ToString()
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While


                    resultado.Close()


                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.Numero = entidad.Codigo

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                    If entidad.Estado = 1 Then

                        If Not IsNothing(entidad.CuentaPorPagar) Then ' And entidad.ValorPagar > 0
                            entidad.CuentaPorPagar.Numero = entidad.NumeroDocumento
                            inserto = Generar_CxP(entidad.CuentaPorPagar, conexion)
                        End If

                        Dim rep As New RepositorioEncabezadoComprobantesContables
                        Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Codigo, 35, conexion, resultado)
                    End If
                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using

            Return entidad.NumeroDocumento
        End Function
        Public Function InsertarDetalle(entidad As DetalleDocumentoCausaciones, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ECCA_Codigo", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            contextoConexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Centro_Costo", entidad.CentroCosto)
            contextoConexion.AgregarParametroSQL("@par_Valor_Base", entidad.ValorBase)
            contextoConexion.AgregarParametroSQL("@par_Valor_Debito", entidad.ValorDebito)
            contextoConexion.AgregarParametroSQL("@par_Valor_Credito", entidad.ValorCredito)
            contextoConexion.AgregarParametroSQL("@par_Genera_Cuenta", entidad.GeneraCuenta)
            contextoConexion.AgregarParametroSQL("@par_Nota_Detalle", entidad.NotaDetalle)
            contextoConexion.AgregarParametroSQL("@par_Prefijo", entidad.Prefijo)
            contextoConexion.AgregarParametroSQL("@par_Codigo_Anexo", entidad.CodigoAnexo)
            contextoConexion.AgregarParametroSQL("@par_Sufijo", entidad.SufijoCodigoAnexo)
            contextoConexion.AgregarParametroSQL("@par_Campo_Auxiliar", entidad.CampoAuxiliar)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_documento_causaciones]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function
        Public Overrides Function Modificar(entidad As EncabezadoDocumentoCausaciones) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", 35) 'Comprobante Causacion
                    conexion.AgregarParametroSQL("@par_CATA_TICC_Codigo", entidad.TipoComprobante.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Titular", entidad.Proveedor.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
                    conexion.AgregarParametroSQL("@par_Numero_Factura", entidad.NumeroFactura)
                    conexion.AgregarParametroSQL("@par_Fecha_Factura", entidad.FechaFactura, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Vence_Factura", entidad.FechaVencimientoFactura, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Subtotal", entidad.Subtotal)
                    conexion.AgregarParametroSQL("@par_Valor_IVA", entidad.ValorIva)
                    conexion.AgregarParametroSQL("@par_Total", entidad.ValorTotal)
                    conexion.AgregarParametroSQL("@par_Valor_Debito", entidad.Debito)
                    conexion.AgregarParametroSQL("@par_Valor_Credito", entidad.Credito)
                    conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.NumeroDocumentoOrigen)
                    conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Creacion", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Destino", entidad.OficinaDestino.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_documento_causaciones")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.Numero = entidad.Codigo

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                    If entidad.Estado = 1 Then

                        If Not IsNothing(entidad.CuentaPorPagar) Then ' And entidad.ValorPagar > 0
                            entidad.CuentaPorPagar.Numero = entidad.Numero

                            inserto = Generar_CxP(entidad.CuentaPorPagar, conexion)
                        End If

                        Dim rep As New RepositorioEncabezadoComprobantesContables
                        Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Codigo, 35, conexion, resultado)
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using

            Return entidad.Numero
        End Function
        Public Overrides Function Obtener(filtro As EncabezadoDocumentoCausaciones) As EncabezadoDocumentoCausaciones
            Dim item As New EncabezadoDocumentoCausaciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_documento_causaciones")

                While resultado.Read
                    item = New EncabezadoDocumentoCausaciones(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As EncabezadoDocumentoCausaciones) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 35)
                conexion.AgregarParametroSQL("@par_USUA_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_OFIC_Anula", entidad.OficinaAnula.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_encabezado_documento_causaciones")

                While resultado.Read
                    anulo = IIf(Convert.ToInt64(resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
        Public Function InsertarDetalleCuentas(entidad As DetalleDocumentoCuentaComprobantes, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim InsertoDetallCuentas As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ENDC_Codigo", entidad.CodigoDocumentoCuenta)
            contextoConexion.AgregarParametroSQL("@par_EDCO_Codigo", entidad.CodigoDocumentoComprobante)
            contextoConexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo", entidad.ValorPago)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_documento_cuenta_comprobantes]")

            While resultado.Read
                InsertoDetallCuentas = IIf((resultado.Item("Codigo")) > 0, True, False)
            End While
            resultado.Close()
            Return InsertoDetallCuentas
        End Function
        Public Function ActualizarCuentas(entidad As EncabezadoDocumentoCuentas, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim Actualizo As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Valor_Abono", entidad.ValorPagar)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Aprueba", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_actualizar_encabezado_documento_cuentas]")

            While resultado.Read
                Actualizo = IIf((resultado.Item("Codigo")) > 0, True, False)
            End While
            resultado.Close()

            Return Actualizo
        End Function

        Public Function Generar_CxP(entidad As EncabezadoDocumentoCuentas, conexion As DataBaseFactory) As Boolean
            Generar_CxP = False
            'Se quema las observaciones, ya que cuando el documento se guarda de una vez en definitivo,
            'en el controller.js no se tiene aún el número documento generado de la liquidación, es decir, en el proceso de insertar
            entidad.Observaciones = "PAGO LIQUIDACIÓN PLANILLA No. " & entidad.NumeroDocumento & ", VALOR $ " & entidad.ValorPagar

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", 160)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
            conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
            conexion.AgregarParametroSQL("@par_Codigo_Documento_Origen", entidad.CodigoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.Numero)
            conexion.AgregarParametroSQL("@par_Fecha_Documento_Origen", entidad.FechaDocumento, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Fecha_Vence_Documento_Origen", entidad.FechaVenceDocumento, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPuc.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            conexion.AgregarParametroSQL("@par_Abono", entidad.Abono)
            conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)
            conexion.AgregarParametroSQL("@par_Fecha_Cancelacion_Pago", entidad.FechaCancelacionPago, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Aprobado", entidad.Aprobado)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Aprobo", entidad.UsuarioAprobo.Codigo)
            conexion.AgregarParametroSQL("@par_Fecha_Aprobo", entidad.FechaAprobo, SqlDbType.DateTime)
            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_cuentas_causacion")

            While resultado.Read
                Generar_CxP = IIf(resultado.Item("Codigo") > 0, True, False)
            End While
            resultado.Close()

            Return Generar_CxP
        End Function

    End Class

End Namespace