﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="ValorCombustibleController"/>
''' </summary>
<Authorize>
Public Class ValorCombustibleController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ValorCombustible) As Respuesta(Of IEnumerable(Of ValorCombustible))
        Return New LogicaValorCombustible(CapaPersistenciaValorCombustible).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ValorCombustible) As Respuesta(Of ValorCombustible)
        Return New LogicaValorCombustible(CapaPersistenciaValorCombustible).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ValorCombustible) As Respuesta(Of Long)
        Return New LogicaValorCombustible(CapaPersistenciaValorCombustible).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ValorCombustible) As Respuesta(Of Boolean)
        Return New LogicaValorCombustible(CapaPersistenciaValorCombustible).Anular(entidad)
    End Function
End Class
