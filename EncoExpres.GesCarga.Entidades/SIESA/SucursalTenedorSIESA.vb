﻿Imports Newtonsoft.Json

Public Class SIESA
    <JsonObject>
    Public NotInheritable Class SucursalTenedorSIESA

        <JsonProperty>
        Public Property Nit As String
        <JsonProperty>
        Public Property Sucursal As String
    End Class
End Class
