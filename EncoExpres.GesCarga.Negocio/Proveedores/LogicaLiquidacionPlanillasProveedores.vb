﻿Imports EncoExpres.GesCarga.Fachada.Proveedores
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.Proveedores


Namespace Proveedores

    Public NotInheritable Class LogicaLiquidacionPlanillasProveedores
        Inherits LogicaBase(Of LiquidacionPlanillasProveedores)

        ReadOnly _persistencia As IPersistenciaBase(Of LiquidacionPlanillasProveedores)

        Sub New(persistencia As IPersistenciaBase(Of LiquidacionPlanillasProveedores))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As LiquidacionPlanillasProveedores) As Respuesta(Of IEnumerable(Of LiquidacionPlanillasProveedores))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of LiquidacionPlanillasProveedores))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of LiquidacionPlanillasProveedores))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As LiquidacionPlanillasProveedores) As Respuesta(Of LiquidacionPlanillasProveedores)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of LiquidacionPlanillasProveedores)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of LiquidacionPlanillasProveedores)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As LiquidacionPlanillasProveedores) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long


            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Numero, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}
        End Function

        Private Function DatosRequeridos(entidad As LiquidacionPlanillasProveedores) As Respuesta(Of LiquidacionPlanillasProveedores)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of LiquidacionPlanillasProveedores) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of LiquidacionPlanillasProveedores) With {.ProcesoExitoso = True}

        End Function


        Public Function Anular(entidad As LiquidacionPlanillasProveedores) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaLiquidacionPlanillasProveedores).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
    End Class
End Namespace

