﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 19/10/2021 
-- Módulo: Gesphone

PRINT 'gsp_insertar_foto_detalle_distribucion_remesas_gesphone'
GO
DROP PROCEDURE gsp_insertar_foto_detalle_distribucion_remesas_gesphone
GO
CREATE PROCEDURE dbo.gsp_insertar_foto_detalle_distribucion_remesas_gesphone   
(        
@par_EMPR_Codigo SMALLINT,        
@par_ENRE_Numero NUMERIC,             
@par_Foto VARBINARY(max),   
@par_Nombre_Foto VARCHAR(50),   
@par_Extension_Foto VARCHAR(50),        
@par_Tipo_Foto VARCHAR(50),        
@par_USUA_Codigo_Crea SMALLINT   ,
@par_Entrega_Devolucion SMALLINT = NULL     
)        
AS        
BEGIN        
        
 IF NOT EXISTS(SELECT * FROM        
Foto_Detalle_Distribucion_Remesas        
WHERE         
EMPR_Codigo = @par_EMPR_Codigo        
AND ENRE_Numero = @par_ENRE_Numero         
AND Nombre_Foto = @par_Nombre_Foto
)    
 BEGIN        
    
INSERT INTO Foto_Detalle_Distribucion_Remesas        
  (     
   EMPR_Codigo,         
   ENRE_Numero,
   Foto,   
   Nombre_Foto,       
   Extension,        
   Tipo,        
   USUA_Codigo_Crea,        
   Fecha_Crea   ,
   Entrega_Devolucion
  )        
   VALUES          
  (        
@par_EMPR_Codigo,        
@par_ENRE_Numero,               
@par_Foto,   
@par_Nombre_Foto,   
@par_Extension_Foto,        
@par_Tipo_Foto,        
@par_USUA_Codigo_Crea,        
GETDATE()        ,
@par_Entrega_Devolucion
  )        
  SELECT @@ROWCOUNT AS Codigo         
        
 END       
         
END   
GO