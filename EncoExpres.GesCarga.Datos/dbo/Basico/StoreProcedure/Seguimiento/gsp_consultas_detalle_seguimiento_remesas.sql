﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 04/06/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


PRINT 'gsp_consultas_detalle_seguimiento_remesas'
GO
DROP PROCEDURE gsp_consultas_detalle_seguimiento_remesas
GO  
CREATE PROCEDURE gsp_consultas_detalle_seguimiento_remesas   
(                        
@par_EMPR_Codigo SMALLINT, 
@par_Fecha_Inicial DATETIME = NULL,                                
@par_Fecha_Final DATETIME = NULL,         
@par_Vehiculo VARCHAR(10) = NULL,      
@par_Ruta NUMERIC = NULL,     
@par_Numero_Documento_Remesa NUMERIC = NULL,        
@par_Fecha_Remesa DATETIME =NULL,     
@par_Cliente VARCHAR(100) = NULL,       
@par_Documento_Cliente VARCHAR(50) = NULL,    
@par_Documento_Distribucion_Cliente VARCHAR(50) = NULL,                                         
@par_CATA_TOSV_Codigo NUMERIC = NULL,        
@par_CATA_SRSV_Codigo NUMERIC = NULL,      
@par_Mostrar NUMERIC = NULL,       
@par_Anulado NUMERIC = NULL,                   
@par_NumeroPagina INT = NULL,                            
@par_RegistrosPagina INT = NULL                            
)                        
AS                        
BEGIN       
    
 set @par_Fecha_Remesa = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Remesa)                                   
 set @par_Fecha_Remesa = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Remesa)),@par_Fecha_Remesa)                                          
 set @par_Fecha_Remesa = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Remesa)),@par_Fecha_Remesa)                                                             
                        
DECLARE @IDMAX NUMERIC  = 0                        
                        
SET NOCOUNT ON;                           
  DECLARE                              
   @CantidadRegistros INT                              
  SELECT @CantidadRegistros = (                              
    SELECT DISTINCT                               
     COUNT(1)                               
         FROM                              
   Detalle_Seguimiento_Remesas DESR                        
                      
   LEFT JOIN Encabezado_Remesas AS ENRE                   
   ON DESR.EMPR_Codigo = ENRE.EMPR_Codigo                        
   AND DESR.ENRE_Numero = ENRE.Numero    
   
   LEFT JOIN Detalle_Distribucion_Remesas AS DDRE                   
   ON ENRE.EMPR_Codigo = DDRE.EMPR_Codigo                        
   AND ENRE.Numero = DDRE.ENRE_Numero                      
                                          
   LEFT JOIN (SELECT EMPR_Codigo, ISNULL(MAX(Orden),0) AS Orden, ENRE_Numero FROM Detalle_Seguimiento_Remesas GROUP BY EMPR_Codigo, ENRE_Numero)  AS DESE                        
   ON DESR.EMPR_Codigo = DESE.EMPR_Codigo                         
   AND DESR.ENRE_Numero = DESE.ENRE_Numero                         
   AND DESR.Orden > (DESE.Orden - ISNULL(@par_Mostrar, DESE.Orden))                
                                     
   LEFT JOIN Valor_Catalogos AS TOSV                        
   ON DESR.EMPR_Codigo = TOSV.EMPR_Codigo                        
   AND DESR.CATA_TOSV_Codigo  = TOSV.Codigo                        
                        
   LEFT JOIN Valor_Catalogos AS SRSV                        
   ON DESR.EMPR_Codigo = SRSV.EMPR_Codigo                        
   AND DESR.CATA_SRSV_Codigo  = SRSV.Codigo                        
                        
   LEFT JOIN Valor_Catalogos AS NOSV                        
   ON DESR.EMPR_Codigo = NOSV.EMPR_Codigo                        
   AND DESR.CATA_NOSV_Codigo  = NOSV.Codigo                              
                      
   LEFT JOIN Terceros AS CLIE                        
   ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                        
   AND ENRE.TERC_Codigo_Cliente  = CLIE.Codigo    
                           
   LEFT JOIN Vehiculos AS VEHI                        
   ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                        
   AND ENRE.VEHI_Codigo  = VEHI.Codigo                              
             
   LEFT JOIN Rutas AS RUTA                        
   ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                        
   AND ENRE.RUTA_Codigo  = RUTA.Codigo    
     
   LEFT JOIN Cumplido_Remesas AS CURE                            
   ON ENRE.EMPR_Codigo = CURE.EMPR_Codigo                            
   AND ENRE.Numero  = CURE.ENRE_Numero                      
                        
     WHERE                        
   DESR.EMPR_Codigo = @par_EMPR_Codigo     
   AND DESR.Fecha_Reporte BETWEEN ISNULL(@par_Fecha_Inicial, DESR.Fecha_Reporte) AND ISNULL(@par_Fecha_Final, DESR.Fecha_Reporte)  
   AND (VEHI.Placa = @par_Vehiculo or @par_Vehiculo is null)      
   AND (RUTA.Codigo = @par_Ruta or @par_Ruta is null)    
   AND (ENRE.Numero_Documento = @par_Numero_Documento_Remesa or @par_Numero_Documento_Remesa is null)  
   AND (DDRE.Documento_Cliente = @par_Documento_Distribucion_Cliente or @par_Documento_Distribucion_Cliente is null)      
   AND ENRE.Fecha = ISNULL(@par_Fecha_Remesa, ENRE.Fecha)      
   AND(ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') + ' ' + ISNULL(CLIE.Razon_Social, '')  LIKE           
   CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Cliente, ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') + ' ' + ISNULL(CLIE.Razon_Social, '')))), '%'))                                                   
   AND ((ENRE.Documento_Cliente LIKE '%' + RTRIM(LTRIM(@par_Documento_Cliente)) + '%' OR (@par_Documento_Cliente IS NULL)))                          
   AND (TOSV.Codigo = @par_CATA_TOSV_Codigo or @par_CATA_TOSV_Codigo is null)        
   AND (SRSV.Codigo = @par_CATA_SRSV_Codigo or @par_CATA_SRSV_Codigo is null)          
   AND DESR.Anulado = ISNULL(@par_Anulado, 0)            
   AND DESR.Orden > (DESE.Orden - ISNULL(@par_Mostrar,DESE.Orden))         
   AND ENRE.Anulado = 0          
   AND CURE.ENRE_Numero IS NULL               
     );                                     
    WITH Pagina AS                              
    (                              
                        
 SELECT                        
  DESR.EMPR_Codigo,         
  DESR.ID,                        
  ENRE.Numero AS ENRE_Numero,           
  ENRE.Numero_Documento AS Numero_Documento_Remesa,             
  ENRE.Fecha AS Fecha_Remesa,  
  DESR.Fecha_Reporte,                       
  ISNULL(CLIE.Razon_Social, '')+ISNULL(CLIE.Nombre, '') + ' '+ ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS Nombre_Cliente,                      
  ENRE.Documento_Cliente,                              
  DESR.Longitud,                        
  DESR.Latitud,                   
  DESR.CATA_TOSV_Codigo,                 
  DESR.CATA_SRSV_Codigo,                        
  DESR.CATA_NOSV_Codigo,    
  ISNULL(TOSV.Campo1, ' ') AS Tipo_Origen_Seguimiento,                            
  ISNULL(SRSV.Campo1, ' ') AS Sitio_Reporte_Seguimiento,                            
  ISNULL(NOSV.Campo1, ' ') AS Novedad_Seguimiento,                          
  DESR.Observaciones,                               
  DESR.Reportar_Cliente,                                                            
  ENRE.VEHI_Codigo,                                            
  ENRE.RUTA_Codigo,                                             
  ISNULL(DESR.Fecha_Crea,'')AS Fecha_Ultimo_Reporte,                                                 
  ISNULL(VEHI.Placa, '') AS Vehiculo,                
  ISNULL(RUTA.Nombre, '') AS Ruta,                       
  DESE.Orden ,                             
  ROW_NUMBER() OVER(ORDER BY DESR.ID DESC) AS RowNumber                              
       FROM                              
   Detalle_Seguimiento_Remesas DESR                        
                      
   LEFT JOIN Encabezado_Remesas AS ENRE                   
   ON DESR.EMPR_Codigo = ENRE.EMPR_Codigo                        
   AND DESR.ENRE_Numero = ENRE.Numero     
   
   LEFT JOIN Detalle_Distribucion_Remesas AS DDRE                   
   ON ENRE.EMPR_Codigo = DDRE.EMPR_Codigo                        
   AND ENRE.Numero = DDRE.ENRE_Numero                      
                                          
   LEFT JOIN (SELECT EMPR_Codigo, ISNULL(MAX(Orden),0) AS Orden, ENRE_Numero FROM Detalle_Seguimiento_Remesas GROUP BY EMPR_Codigo, ENRE_Numero)  AS DESE                        
   ON DESR.EMPR_Codigo = DESE.EMPR_Codigo                         
   AND DESR.ENRE_Numero = DESE.ENRE_Numero                         
   AND DESR.Orden > (DESE.Orden - ISNULL(@par_Mostrar, DESE.Orden))                
                                     
   LEFT JOIN Valor_Catalogos AS TOSV                        
   ON DESR.EMPR_Codigo = TOSV.EMPR_Codigo                        
   AND DESR.CATA_TOSV_Codigo  = TOSV.Codigo                        
                        
   LEFT JOIN Valor_Catalogos AS SRSV                        
   ON DESR.EMPR_Codigo = SRSV.EMPR_Codigo                        
   AND DESR.CATA_SRSV_Codigo  = SRSV.Codigo                        
                        
   LEFT JOIN Valor_Catalogos AS NOSV                        
   ON DESR.EMPR_Codigo = NOSV.EMPR_Codigo                        
   AND DESR.CATA_NOSV_Codigo  = NOSV.Codigo                              
                      
   LEFT JOIN Terceros AS CLIE                        
   ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                        
   AND ENRE.TERC_Codigo_Cliente  = CLIE.Codigo    
                           
   LEFT JOIN Vehiculos AS VEHI                        
   ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                        
   AND ENRE.VEHI_Codigo  = VEHI.Codigo                   
             
   LEFT JOIN Rutas AS RUTA                        
   ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                        
   AND ENRE.RUTA_Codigo  = RUTA.Codigo   
     
   LEFT JOIN Cumplido_Remesas AS CURE                            
   ON ENRE.EMPR_Codigo = CURE.EMPR_Codigo                            
   AND ENRE.Numero  = CURE.ENRE_Numero                     
                        
      WHERE                        
   DESR.EMPR_Codigo = @par_EMPR_Codigo     
   AND DESR.Fecha_Reporte BETWEEN ISNULL(@par_Fecha_Inicial, DESR.Fecha_Reporte) AND ISNULL(@par_Fecha_Final, DESR.Fecha_Reporte)  
   AND (VEHI.Placa = @par_Vehiculo or @par_Vehiculo is null)      
   AND (RUTA.Codigo = @par_Ruta or @par_Ruta is null)    
   AND (ENRE.Numero_Documento = @par_Numero_Documento_Remesa or @par_Numero_Documento_Remesa is null)    
   AND ENRE.Fecha = ISNULL(@par_Fecha_Remesa, ENRE.Fecha)      
   AND(ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') + ' ' + ISNULL(CLIE.Razon_Social, '')  LIKE           
   CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Cliente, ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') + ' ' + ISNULL(CLIE.Razon_Social, '')))), '%'))                                                   
   AND ((ENRE.Documento_Cliente LIKE '%' + RTRIM(LTRIM(@par_Documento_Cliente)) + '%' OR (@par_Documento_Cliente IS NULL))) 
   AND (DDRE.Documento_Cliente = @par_Documento_Distribucion_Cliente or @par_Documento_Distribucion_Cliente is null)                         
   AND (TOSV.Codigo = @par_CATA_TOSV_Codigo or @par_CATA_TOSV_Codigo is null)        
   AND (SRSV.Codigo = @par_CATA_SRSV_Codigo or @par_CATA_SRSV_Codigo is null)          
   AND DESR.Anulado = ISNULL(@par_Anulado, 0)            
   AND DESR.Orden > (DESE.Orden - ISNULL(@par_Mostrar,DESE.Orden))     
   AND ENRE.Anulado = 0     
   AND CURE.ENRE_Numero IS NULL         
   )          
  SELECT        
  4 AS Tipo_Consulta,                        
  EMPR_Codigo,   
  ID,                         
  ENRE_Numero,           
  Numero_Documento_Remesa,             
  Fecha_Remesa, 
  Fecha_Reporte,                        
  Nombre_Cliente,                      
  Documento_Cliente,                              
  Longitud,                        
  Latitud,                   
  CATA_TOSV_Codigo,                 
  CATA_SRSV_Codigo,                        
  CATA_NOSV_Codigo,      
  Tipo_Origen_Seguimiento,                            
  Sitio_Reporte_Seguimiento,                            
  Novedad_Seguimiento,                        
  Observaciones,                               
  Reportar_Cliente,                                                      
  VEHI_Codigo,                                            
  RUTA_Codigo,                                             
  Fecha_Ultimo_Reporte,                                                 
  Vehiculo,                
  Ruta,                         
  Orden,              
  @CantidadRegistros AS TotalRegistros,                              
  @par_NumeroPagina AS PaginaObtener,                              
  @par_RegistrosPagina AS RegistrosPagina                              
   FROM                              
    Pagina                          
   WHERE                              
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                            
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                            
 ORDER BY Fecha_Ultimo_Reporte DESC                     
END    
GO     