﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Negocio.ServicioCliente

''' <summary>
''' Controlador <see cref="DetallePolizasSolicitudOrdenServiciosController"/>
''' </summary>
<Authorize>
Public Class DetallePolizasSolicitudOrdenServiciosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetallePolizasSolicitudOrdenServicios) As Respuesta(Of IEnumerable(Of DetallePolizasSolicitudOrdenServicios))
        Return New LogicaDetallePolizasSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetallePolizasSolicitudOrdenServicios) As Respuesta(Of DetallePolizasSolicitudOrdenServicios)
        Return New LogicaDetallePolizasSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetallePolizasSolicitudOrdenServicios) As Respuesta(Of Long)
        Return New LogicaDetallePolizasSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetallePolizasSolicitudOrdenServicios) As Respuesta(Of Boolean)
        Return New LogicaDetallePolizasSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Anular(entidad)
    End Function
End Class
