﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Mantenimiento

Namespace Mantenimiento
    Public NotInheritable Class RepositorioDocumentoEquipoMantenimiento
        Inherits RepositorioBase(Of EquipoMantenimientoDocumento)
        Public Overrides Function Consultar(filtro As EquipoMantenimientoDocumento) As IEnumerable(Of EquipoMantenimientoDocumento)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Insertar(entidad As EquipoMantenimientoDocumento) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As EquipoMantenimientoDocumento) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As EquipoMantenimientoDocumento) As EquipoMantenimientoDocumento
            Throw New NotImplementedException()
        End Function
        Public Function InsertarTemporal(entidad As EquipoMantenimientoDocumento) As Long
            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Documento", entidad.Documento, SqlDbType.VarBinary)
                'conexion.AgregarParametroSQL("@par_Extension_Documento", entidad.ExtensionDocumento)
                'conexion.AgregarParametroSQL("@par_Nombre_Documento", entidad.NombreDocumento)
                If entidad.ID_Documento_Modifica > 0 Then
                    conexion.AgregarParametroSQL("@par_ID_Documento_Modifica", entidad.ID_Documento_Modifica)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_t_equipo_documentos]")

                While resultado.Read
                    entidad.ID_Documento_temporal = Convert.ToInt64(resultado.Item("ID_Documento_temporal").ToString())
                End While

            End Using
            Return entidad.ID_Documento_temporal
        End Function
        Public Function EliminarTemporal(entidad As EquipoMantenimientoDocumento) As Boolean
            Throw New NotImplementedException()
        End Function
        Public Function LimpiarTemporal(codigoEmpresa As Short, codigoUsuarioCrea As Short, DestinoRegistro As Short, ByRef contextoConexion As DataBaseFactory) As Boolean
            Throw New NotImplementedException()
        End Function

        Public Function TrasladarDocumento(entidad As EquipoMantenimientoDocumento, ByRef contextoConexion As DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ID_Documento_Temporal", entidad.ID_Documento_temporal)
            contextoConexion.AgregarParametroSQL("@par_ID_Documento", entidad.ID_Documento)


            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_trasladar_t_documento_equipo_mantenimiento]")

            While resultado.Read
                inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
            End While
            resultado.Close()

            Return inserto
        End Function


    End Class
End Namespace
