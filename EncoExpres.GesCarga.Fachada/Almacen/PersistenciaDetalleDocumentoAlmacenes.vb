﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Almacen
Imports EncoExpres.GesCarga.Repositorio.Almacen

Namespace Almacen
    Public NotInheritable Class PersistenciaDetalleDocumentoAlmacenes
        Implements IPersistenciaBase(Of DetalleDocumentoAlmacenes)
        Public Function Consultar(filtro As DetalleDocumentoAlmacenes) As IEnumerable(Of DetalleDocumentoAlmacenes) Implements IPersistenciaBase(Of DetalleDocumentoAlmacenes).Consultar
            Return New RepositorioDetalleDocumentoAlmacenes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleDocumentoAlmacenes) As Long Implements IPersistenciaBase(Of DetalleDocumentoAlmacenes).Insertar
            Return New RepositorioDetalleDocumentoAlmacenes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleDocumentoAlmacenes) As Long Implements IPersistenciaBase(Of DetalleDocumentoAlmacenes).Modificar
            Return New RepositorioDetalleDocumentoAlmacenes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleDocumentoAlmacenes) As DetalleDocumentoAlmacenes Implements IPersistenciaBase(Of DetalleDocumentoAlmacenes).Obtener
            Return New RepositorioDetalleDocumentoAlmacenes().Obtener(filtro)
        End Function
    End Class
End Namespace
