﻿EncoExpresApp.controller('ConsultarControlEntregasRecoleccionCtrl', ['$scope', '$timeout', 'DetalleDistribucionRemesasFactory', 'OficinasFactory', '$linq', 'blockUIConfig', 'blockUI','CiudadesFactory', 'RemesasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'RemesaGuiasFactory', 'UsuariosFactory', 'PlanillaGuiasFactory', 'RecoleccionesFactory',
    function ($scope, $timeout, DetalleDistribucionRemesasFactory, OficinasFactory,$linq, blockUIConfig, blockUI, CiudadesFactory, RemesasFactory, ValorCatalogosFactory, TercerosFactory, RemesaGuiasFactory, UsuariosFactory, PlanillaGuiasFactory, RecoleccionesFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONTROL ENTREGAS ';
        $scope.MapaSitio = [{ Nombre: 'Entregas' }, { Nombre: 'Control Entregas' }];

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONTROL_ENTREGAS_RECOLECCION); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.ListaPlanillas = [];
        $scope.ListaPlanillasEntregadas = [];
        $scope.ListadoRemesas = [];
        $scope.ListadoRecolecciones = [];
        $scope.ListadoDetallesPlanilla = [];
        $scope.ListadoTipoIdentificacion = [];
        $scope.ListadoNovedadesRecolecciones = [];
        $scope.Foto = [];
        $scope.Modal = {};
        $scope.DevolucionRemesa = 0;
        $('.modalEntregas').hide();
        $('.modalDevolucionRemesas').hide();
        $('#modalRecibe').hide();
        $('#modalRemitente').hide();
        $('.modalRecolecciones').hide();
        $('#modalDestinatario').hide();
        $scope.ListadoTipoPlanillas = [
            { Nombre: 'Entrega', Codigo: 210 },
            { Nombre: 'Recolección', Codigo: 205 }
        ];
        $scope.TipoPlanilla = $linq.Enumerable().From($scope.ListadoTipoPlanillas).First('$.Codigo == 210');

        $scope.ListaGestionDocuCliente = [];
        $scope.ListadoConfirmaEntrega = [
            { Codigo: -1, Nombre: "SELECCIONE" },
            { Codigo: 0, Nombre: "NO" },
            { Codigo: 1, Nombre: "SI" }
        ]; 

        $scope.Modelo = {
            Oficina: ''
        }
        $scope.ListadoOficinasActual = [];
       
        $scope.ListadoCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        }; 
        var fechaActual = new Date();
        $scope.MensajesErrorDocu = [];
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Init
        $scope.InitLoad = function () {
            //ListadoNovedadesRemesas: 
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 203 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNovedades = response.data.Datos;
                        }
                        else {
                            $scope.ListadoNovedades = []
                        }
                    }
                }, function (response) {
                });
            //ListadoNovedadesRecoleccion:
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 219 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNovedadesRecolecciones = response.data.Datos;
                        }
                        else {
                            $scope.ListadoNovedadesRecolecciones = []
                        }
                    }
                }, function (response) {
                });
            //Listado tipo identificación:
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {

                            $scope.ListadoTipoIdentificacion = response.data.Datos;

                        }
                    }
                }, function (response) {
                });
            //Obtiene Informacion Tercero Conductor
            try {
                $scope.Conductor = UsuariosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Sesion.UsuarioAutenticado.Codigo, Sync: true }).Datos.Conductor.Codigo
            } catch (e) {
                console.log(e);
            }
            //planillas Entregas:
            filtro = {
                FechaInicial: fechaActual,
                FechaFinal: fechaActual,
                Conductor: { Codigo: $scope.Conductor },
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Planilla: { Estado: { Codigo: 1 } },
                TipoDocumento: 210,
                Sync: true
            };
            try {
                $scope.ListaPlanillasEntregadas = PlanillaGuiasFactory.Consultar(filtro).Datos;
                $scope.ListaPlanillasEntregadas.push({ Planilla: { NumeroDocumento: '(Seleccione)', Numero: 0 } });
                // $scope.ListaPlanillas = $scope.ListaPlanillasEntregadas;
            } catch (e) {
                console.log("error consulta planilla entregas: ", e);
            }
            //planillas Recolecciones:
            filtroRecoleccion = {
                FechaInicial: fechaActual,
                FechaFinal: fechaActual,
                Conductor: { Codigo: $scope.Conductor },
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Planilla: { Estado: { Codigo: 1 } },
                TipoDocumento: 135,
                Sync: true
            }
            try {
                $scope.ListaPlanillasRecolecciones = PlanillaGuiasFactory.Consultar(filtroRecoleccion).Datos;
                $scope.ListaPlanillasRecolecciones.push({ Planilla: { NumeroDocumento: '(Seleccione)', Numero: 0 } })
                $scope.ListaPlanillas = $scope.ListaPlanillasRecolecciones;
            } catch (e) {
                console.log("error consulta planilla recolecciones: ", e);
            }
            //$scope.ListaPlanillas = $scope.ListaPlanillasEntregadas;
            $scope.Planilla = $linq.Enumerable().From($scope.ListaPlanillas).First('$.Planilla.Numero == 0');

            //Otras Oficinas 
            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                    then(function (response) {
                        $scope.ListadoOficinas.push({ Nombre: '(No aplica)', Codigo: -1 })
                        if (response.data.ProcesoExitoso === true) {
                            response.data.Datos.forEach(function (item) {
                                $scope.ListadoOficinasActual.push(item)
                            });
                            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else { 
                if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) { 
                    $scope.ListadoOficinasActual.push({ Nombre: '(No aplica)', Codigo: -1 })
                    $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                        $scope.ListadoOficinasActual.push(item)
                    });
                    $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinasActual).First('$.Codigo === -1');
                } else {
                    ShowError('El usuario no tiene oficinas asociadas')
                }
            } 
        };
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        $scope.ConsultarPlanillas = function () {
            $scope.ListadoDetallesPlanilla = [];
            $scope.ListaPlanillas = [];
            $scope.Planilla = '';
            if ($scope.TipoPlanilla != undefined || $scope.TipoPlanilla != null || $scope.TipoPlanilla != '') {

                if ($scope.TipoPlanilla.Codigo == 210) {
                    $scope.ListaPlanillas = $scope.ListaPlanillasEntregadas;
                    $scope.Planilla = $linq.Enumerable().From($scope.ListaPlanillas).First('$.Planilla.Numero == 0');
                } else if ($scope.TipoPlanilla.Codigo == 205) {
                    $scope.ListaPlanillas = $scope.ListaPlanillasRecolecciones;
                    $scope.Planilla = $linq.Enumerable().From($scope.ListaPlanillas).First('$.Planilla.Numero == 0');
                }
            }
        }
         
        $scope.Buscar = function () {  
            $scope.ConsultarDetallesPlanilla()
        }

        $scope.ConsultarDetallesPlanilla = function () {
            if ($scope.Planilla != undefined && $scope.Planilla != null && $scope.Planilla != '') {
                if ($scope.Planilla.Planilla.Numero != 0) {
                    var filtroRemesas = {} 
                    filtroRemesas = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Remesa: {
                            NumeroplanillaDespacho: $scope.Planilla.Planilla.Numero,
                            CiudadDestinatario: $scope.Modelo.CiudadDestinatario
                        },
                        Estado: 1,
                        NumeroPlanilla: -1,
                        Planilla: { TipoDocumento: 135 },
                        OficinaRecibe: $scope.Modelo.Oficina,
                        BarrioDestinatario: $scope.Modelo.Barrio,
                        RecogerOficinaDestino: $scope.Modelo.RecogerOficina == true ? 1 : 0,
                        ConsultarControlEntregas: 1, 
                        Sync: true
                    }; 
                    try {
                        $scope.ListadoRemesas = RemesaGuiasFactory.Consultar(filtroRemesas).Datos;

                       
                        $scope.ListadoRemesas.forEach(item => {
                            item.Tipo = 1;
                            item.NumeroDocumento = item.Remesa.NumeroDocumento;
                            item.Estado = item.EstadoGuia.Codigo;
                            item.Destinatario = { Nombre: item.Remesa.Destinatario.Nombre, Telefono: item.Remesa.Destinatario.Telefonos, Direccion: item.Remesa.Destinatario.Direccion, NumeroIdentificacion: item.Remesa.Destinatario.NumeroIdentificacion };
                            item.Barrio = item.Remesa.BarrioDestinatario;
                            item.Cantidad = item.Remesa.CantidadCliente;
                            item.Peso = item.Remesa.PesoCliente;
                            $scope.ListadoDetallesPlanilla.push(item);
                        });
                        $scope.ListadoDetallesPlanilla = $scope.ListadoRemesas;
                        console.log($scope.ListadoDetallesPlanilla )

                    } catch (e) {
                        console.log('error consulta remesas: ', e);
                    } 
                    filtroRecolecciones = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Planilla: $scope.Planilla.Planilla.Numero,

                        Sync: true
                    }
                    try {
                        $scope.ListadoRecolecciones = RecoleccionesFactory.Consultar(filtroRecolecciones).Datos;
                        $scope.ListadoRecolecciones.forEach(item => {
                            item.Tipo = 2;
                            item.Estado = item.EstadoRecoleccion;
                            item.Destinatario = { Nombre: item.NombreContacto, Telefono: item.Telefonos, Direccion: item.Direccion };
                            item.Zona = { Nombre: item.Zonas.Nombre };
                            $scope.ListadoDetallesPlanilla.push(item);
                        });
                    } catch (e) {
                        console.log('error consulta recolecciones: ', e);
                    }

                }
            }
        }




        $scope.Entrega = function (item) {
            console.log(' remesas', item)

            $scope.Modal = [];
            $scope.Foto = [];
            $scope.DevolucionRemesa = 0;
            $('.modalEntregas').show();
            $('.modalDevolucionRemesas').hide();
            $('#modalRemitente').hide();
            $('#modalRecibe').hide();
            $('.modalRecolecciones').hide();
            $('#modalDestinatario').hide();
            var topEntregas = $('.modalEntregas').offset().top;
            var offset = 0;
            $('#window').scrollTop(topEntregas);
            if (topEntregas > 100) {
                offset = topEntregas - 100;
                $('#window').scrollTop($('#window').scrollTop() + offset);
            } else if (topEntregas < 100) {
                offset = 100 - topEntregas;
                $('#window').scrollTop($('#window').scrollTop() - offset);
            }
            $scope.Modal.NumeroIdentificacion = item.Destinatario.NumeroIdentificacion;
            $scope.Modal.Nombre = item.Destinatario.Nombre;
            $scope.Modal.Telefono = item.Destinatario.Telefono;
            $scope.Modal.NovedadEntrega = $scope.ListadoNovedades[0];
            //$scope.Modal.TipoIdentificacionRecibe = { Codigo: item.Remesa.Destinatario.TipoIdentificacion.Codigo };
            $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo==' + item.Remesa.Destinatario.TipoIdentificacion.Codigo)
            $scope.Modal.Peso = item.PesoCobrar;
            $scope.Modal.Cantidad = item.Cantidad;
            $scope.Modal.Remesa = item.Remesa.Numero;
            $scope.Modal.NumeroDocumento = item.Remesa.NumeroDocumento;
            $scope.Modal.FechaRecibe = new Date();
            $scope.ListaGestionDocuCliente = [];
            for (var i = 0; i < item.GestionDocumentosRemesa.length; i++) {
                var itGe = item.GestionDocumentosRemesa[i];
                if (itGe.Recibido == ESTADO_ACTIVO) {
                    $scope.ListaGestionDocuCliente.push({
                        Entregado: $linq.Enumerable().From($scope.ListadoConfirmaEntrega).First('$.Codigo ==-1'),
                        NumeroDocumentoGestion: itGe.NumeroDocumentoGestion,
                        TipoDocumento: itGe.TipoDocumento,
                        TipoGestion: itGe.TipoGestion,
                        NumeroHojas: itGe.NumeroHojas,
                        ValorRecaudar: itGe.ValorRecaudar,
                        GeneraCobro: itGe.GeneraCobro
                    });
                }
            }
        };

        $scope.Devolucion = function (item) {
            $scope.Modal = [];
            $scope.Foto = [];
            $scope.DevolucionRemesa = 1;
            $('.modalEntregas').hide();
            $('#modalRecibe').hide();
            $('#modalRemitente').hide();
            $('.modalRecolecciones').hide();
            $('#modalDestinatario').hide();
            $scope.LimpiarFirma();
            $('.modalDevolucionRemesas').show();
            var topDevolucion = $('.modalDevolucionRemesas').offset().top;
            var offset = 0;
            $('#window').scrollTop(topDevolucion);
            if (topDevolucion > 100) {
                offset = topDevolucion - 100;
                $('#window').scrollTop($('#window').scrollTop() + offset);
            } else if (topDevolucion < 100) {
                offset = 100 - topDevolucion;
                $('#window').scrollTop($('#window').scrollTop() - offset);
            }
            $scope.Modal.NumeroIdentificacion = item.Destinatario.NumeroIdentificacion;
            $scope.Modal.Nombre = item.Destinatario.Nombre;
            $scope.Modal.Telefono = item.Destinatario.Telefono;
            $scope.Modal.NovedadEntrega = $scope.ListadoNovedades[0];
            $scope.Modal.TipoIdentificacionRecibe = { Codigo: item.Remesa.Destinatario.TipoIdentificacion.Codigo };
            $scope.Modal.Peso = item.PesoCobrar;
            $scope.Modal.Cantidad = item.Cantidad;
            $scope.Modal.Remesa = item.Remesa.Numero;
            $scope.Modal.NumeroDocumento = item.Remesa.NumeroDocumento;

        }

        $scope.Recibe = function (item) {
            $scope.Modal = [];
            $scope.Foto = [];
            $scope.DevolucionRemesa = 0;
            var novedad = item.Tipo == 1 ? item.Remesa.NovedadEntrega.Codigo : item.NovedadEntrega.Codigo
            $('.modalEntregas').hide();
            $('.modalDevolucionRemesas').hide();
            $('#modalRemitente').hide();
            $('.modalRecolecciones').hide();
            $('#modalRecibe').show();
            $('#modalDestinatario').hide();
            var topRecibe = $('#modalRecibe').offset().top;
            var offset = 0;
            $('#window').scrollTop(topRecibe);
            if (topRecibe > 100) {
                offset = topRecibe - 100;
                $('#window').scrollTop($('#window').scrollTop() + offset);
            } else if (topRecibe < 100) {
                offset = 100 - topRecibe;
                $('#window').scrollTop($('#window').scrollTop() - offset);
            }
            $scope.Modal.NumeroIdentificacion = item.Destinatario.NumeroIdentificacion;
            $scope.Modal.Nombre = item.Destinatario.Nombre;
            $scope.Modal.Telefono = item.Destinatario.Telefono;
            $scope.Modal.NovedadEntrega = item.Tipo == 1 ? $linq.Enumerable().From($scope.ListadoNovedades).First('$.Codigo ==' + novedad) : $linq.Enumerable().From($scope.ListadoNovedadesRecolecciones).First('$.Codigo ==' + novedad);
            //$scope.Modal.TipoIdentificacionRecibe = { Codigo: item.Remesa.Destinatario.TipoIdentificacion.Codigo };
            $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + item.Remesa.Destinatario.TipoIdentificacion.Codigo);
            $scope.Modal.Peso = item.PesoCobrar;
            $scope.Modal.Cantidad = item.Cantidad;
            $scope.Modal.Remesa = item.Remesa.Numero;
            $scope.Modal.NumeroDocumento = item.Remesa.NumeroDocumento;
            $scope.Modal.FechaRecibe = new Date();
            $scope.Modal.Observaciones = item.Remesa.ObservacionesRecibe;
        }

        $scope.Destinatario = function (item) {
            $scope.ModalDestinatario = [];
            $scope.Foto = [];
            $scope.DevolucionRemesa = 0;
            var novedad = item.Tipo == 1 ? item.Remesa.NovedadEntrega.Codigo : item.NovedadEntrega.Codigo
            $('.modalEntregas').hide();
            $('.modalDevolucionRemesas').hide();
            $('#modalRemitente').hide();
            $('.modalRecolecciones').hide();
            $('#modalRecibe').hide();
            $('#modalDestinatario').show()

            $("#window").animate({
                scrollTop: $('#modalDestinatario').offset().top
            }, 100);

            $scope.ModalDestinatario.Nombre = item.Destinatario.Nombre;
            $scope.ModalDestinatario.Telefono = item.Destinatario.Telefono;


            $scope.ModalDestinatario.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + item.Remesa.Destinatario.TipoIdentificacion.Codigo);
            $scope.ModalDestinatario.Peso = item.PesoCobrar;
            $scope.ModalDestinatario.Cantidad = item.Cantidad;
            $scope.Modal.Remesa = item.Remesa.Numero;
            $scope.ModalDestinatario.NumeroDocumento = item.Remesa.NumeroDocumento;
            $scope.ModalDestinatario.Direccion = item.Destinatario.Direccion;
            $scope.ModalDestinatario.Barrio = item.Barrio;
        }

        $scope.Remitente = function (item) {
            $scope.Modal = [];
            $scope.Foto = [];
            $scope.DevolucionRemesa = 0;
            var tipoIdentificacion = item.Tipo == 1 ? item.Remesa.Remitente.TipoIdentificacion.Codigo : item.Remitente.TipoIdentificacion.Codigo;
            $('.modalEntregas').hide();
            $('.modalDevolucionRemesas').hide();
            $('#modalRecibe').hide();
            $('.modalRecolecciones').hide();
            $('#modalRemitente').show();
            $('#modalDestinatario').hide();
            var topRecibe = $('#modalRecibe').offset().top;
            var offset = 0;
            $('#window').scrollTop(topRecibe);
            if (topRecibe > 100) {
                offset = topRecibe - 100;
                $('#window').scrollTop($('#window').scrollTop() + offset);
            } else if (topRecibe < 100) {
                offset = 100 - topRecibe;
                $('#window').scrollTop($('#window').scrollTop() - offset);
            }
            $scope.Modal.NumeroIdentificacion = item.Tipo == 1 ? item.Remesa.Remitente.NumeroIdentificacion : item.Remitente.NumeroIdentificacion;
            $scope.Modal.Nombre = item.Tipo == 1 ? item.Remesa.Remitente.Nombre : item.Remitente.Nombre;
            $scope.Modal.Telefono = item.Tipo == 1 ? item.Remesa.Remitente.Telefono : item.Remitente.Telefonos;
            $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + tipoIdentificacion);
            $scope.Modal.Peso = item.PesoCobrar;
            $scope.Modal.Cantidad = item.Cantidad;
            $scope.Modal.Remesa = item.Tipo == 1 ? item.Remesa.Numero : item.Numero;
            $scope.Modal.NumeroDocumento = item.Tipo == 1 ? item.Remesa.NumeroDocumento : item.NumeroDocumento;
            $scope.Modal.FechaRecibe = new Date();
            $scope.Modal.Observaciones = item.Tipo == 1 ? item.Remesa.ObservacionesRecibe : '';
        }

        $scope.Recolectar = function (item) {
            $scope.ModalRecoleccion = [];
            $scope.Foto = [];
            $scope.DevolucionRemesa = 0;
            $('.modalEntregas').hide();
            $('.modalDevolucionRemesas').hide();
            $('#modalRemitente').hide();
            $('#modalRecibe').hide();
            $('.modalRecolecciones').show();
            $('#modalDestinatario').hide();
            var topRecoleccion = $('.modalRecolecciones').offset().top;
            var offset = 0;
            $('#window').scrollTop(topRecoleccion);
            if (topRecoleccion > 100) {
                offset = topRecoleccion - 100;
                $('#window').scrollTop($('#window').scrollTop() + offset);
            } else if (topRecoleccion < 100) {
                offset = 100 - topRecoleccion;
                $('#window').scrollTop($('#window').scrollTop() - offset);
            }
            $scope.ModalRecoleccion.NumeroIdentificacion = item.Destinatario.NumeroIdentificacion;
            $scope.ModalRecoleccion.Nombre = item.Destinatario.Nombre;
            $scope.ModalRecoleccion.Telefono = item.Destinatario.Telefono;
            $scope.ModalRecoleccion.NovedadEntrega = $scope.ListadoNovedadesRecolecciones[0];
            // $scope.ModalRecoleccion.TipoIdentificacionRecibe = { Codigo: item.Destinatario.TipoIdentificacion.Codigo };
            $scope.ModalRecoleccion.Direccion = item.Destinatario.Direccion;
            $scope.ModalRecoleccion.Peso = item.PesoCobrar;
            $scope.ModalRecoleccion.Cantidad = item.Cantidad;
            $scope.ModalRecoleccion.Recoleccion = item.Numero;
            $scope.ModalRecoleccion.NumeroDocumento = item.NumeroDocumento;
            $scope.ModalRecoleccion.FechaRecibe = new Date();
        }

        $scope.LimpiarFirma = function () {
            try {
                var canvas = document.getElementById('Firma');
                var context = canvas.getContext('2d')
                context.clearRect(0, 0, canvas.width, canvas.height);
            } catch (e) {

            }

        }

        $scope.ConfirmacionGuardarPaqueteria = function () {
            if (DatosRequeridos()) {
                ShowConfirm('¿Está seguro de guardar la información?', $scope.GuardarPaqueteria)
            }
        };

        $scope.ConfirmacionGuardarRecoleccion = function () {
            if (DatosRequeridosRecoleccion()) {
                ShowConfirm('¿Está seguro de guardar la información?', $scope.GuardarRecoleccion)
            }
        };

        $scope.GuardarPaqueteria = function () {
            //closeModal('modalConfirmacionGuardarPaqueteria', 1);
            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            $scope.Firma = image.src.replace(/data:image\/png;base64,/g, '');

            $scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);

            var GestionDocumentosRemesa = [];
            if ($scope.ListaGestionDocuCliente.length > 0) {
                for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                    var item = $scope.ListaGestionDocuCliente[i];
                    GestionDocumentosRemesa.push({
                        TipoDocumento: item.TipoDocumento,
                        TipoGestion: item.TipoGestion,
                        Entregado: item.Entregado.Codigo
                    });
                }
            }


            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //Codigo: $scope.Codigo,
                Numero: $scope.Modal.Remesa,
                //FechaRecibe: $scope.Modal.FechaRecibe,
                CantidadRecibe: $scope.Modal.Cantidad,
                PesoRecibe: $scope.Modal.Peso,
                CodigoTipoIdentificacionRecibe: $scope.Modal.TipoIdentificacionRecibe.Codigo,
                NumeroIdentificacionRecibe: parseInt($scope.Modal.NumeroIdentificacion),
                NombreRecibe: $scope.Modal.Nombre,
                TelefonoRecibe: $scope.Modal.Telefono,
                NovedadEntrega: $scope.Modal.NovedadEntrega,
                ObservacionesRecibe: $scope.Modal.Observaciones,
                //Fotografia: $scope.Foto[0],
                Fotografias: $scope.Foto,
                Firma: $scope.Firma,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                DevolucionRemesa: $scope.DevolucionRemesa,
                GestionDocumentosRemesa: GestionDocumentosRemesa,
                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                Planilla: { Codigo: $scope.Planilla.Planilla.Numero }
            };

            RemesaGuiasFactory.GuardarEntrega($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.DevolucionRemesa == 0) {
                                ShowSuccess('La remesa fue recibida por ' + $scope.Modal.Nombre);
                                $scope.Foto = [];
                                $scope.LimpiarFirma();
                                $('.modalEntregas').hide();
                            } else {
                                ShowSuccess('Se realizó la devolución de la remesa satisfactoriamente');
                                $scope.Foto = [];
                                $scope.LimpiarFirma();
                                $('.modalEntregas').hide();
                                $('.modalDevolucionRemesas').hide();
                            }

                            $('#ModalRecibirDistribucionPaqueteria').hide(100);
                            $('.modalDevolucionRemesas').hide();
                            $('#ResultadoConsulta').show(100);
                            //closeModal('ModalRecibirDistribucionPaqueteria', 1);
                            $scope.ConsultarDetallesPlanilla();
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.GuardarRecoleccion = function () {
            //closeModal('modalConfirmacionGuardarPaqueteria', 1);
            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            $scope.Firma = image.src.replace(/data:image\/png;base64,/g, '');

            $scope.ModalRecoleccion.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);
            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //Codigo: $scope.Codigo,
                Numero: $scope.ModalRecoleccion.Recoleccion,
                //FechaRecibe: $scope.Modal.FechaRecibe,
                Cantidad: $scope.ModalRecoleccion.Cantidad,
                Peso: $scope.ModalRecoleccion.Peso,
                //CodigoTipoIdentificacionRecibe: $scope.ModalRecoleccion.TipoIdentificacionRecibe.Codigo,
                //NumeroIdentificacionRecibe: parseInt($scope.ModalRecoleccion.NumeroIdentificacion),
                NombreContacto: $scope.ModalRecoleccion.Nombre,
                Telefonos: $scope.ModalRecoleccion.Telefono,
                NovedadRecoleccion: $scope.ModalRecoleccion.NovedadEntrega,
                Observaciones: $scope.ModalRecoleccion.Observaciones,
                //Fotografia: $scope.Foto[0],
                Fotografias: $scope.Foto,
                Firma: $scope.Firma,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                //DevolucionRemesa: $scope.DevolucionRemesa
            };

            RecoleccionesFactory.GuardarEntrega($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.DevolucionRemesa == 0) {
                                ShowSuccess('La recolección fue recibida por ' + $scope.ModalRecoleccion.Nombre);
                                $scope.Foto = [];
                                $scope.LimpiarFirma();
                                $('.modalRecolecciones').hide();
                            } else {
                                ShowSuccess('Se realizó la devolución de la recolección satisfactoriamente');
                                $scope.Foto = [];
                                $scope.LimpiarFirma();
                                $('.modalRecolecciones').hide();
                            }

                            $('#ModalRecibirDistribucionPaqueteria').hide(100);
                            $('.modalDevolucionRemesas').hide();
                            $('#ResultadoConsulta').show(100);
                            //closeModal('ModalRecibirDistribucionPaqueteria', 1);
                            $scope.ConsultarDetallesPlanilla();
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.CargarImagen = function (element) {
            var continuar = true;
            $scope.TipoImagen = element
            $scope.NombreDocumento = element.files[0].name
            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato JPEG, PNG", false);
                continuar = false;
            }

            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png') {
                    ShowError("Solo se pueden cargar archivos en formato JPEG, PNG ", false);
                    continuar = false;
                }
            }

            if (continuar == true) {
                var reader = new FileReader();
                reader.onload = $scope.AsignarImagenListado;
                reader.readAsDataURL(element.files[0]);
            }

        }

        $scope.AsignarImagenListado = function (e) {
            try {
                RedimHorizontal('')
                RedimVertical('')
            } catch (e) {
            }
            $scope.$apply(function () {
                $scope.Convertirfoto = null
                var TipoImagen = ''

                var name = $scope.TipoImagen.files[0].name.split('.')

                $scope.NombreFoto = name[0]

                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/jpeg'
                        AsignarFotoListado()

                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/png'
                        AsignarFotoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.Convertirfoto = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoFoto = 'application/pdf'
                    $scope.TipoImagen = TipoImagen
                    AsignarFotoListado()

                }
                var input = document.getElementById('inputfile')
                input.value = ''
            });
        }

        $scope.AsignarFoto = function () {
            closeModal('ModalTomarFoto', 1)
            $scope.video.srcObject = undefined
            // $scope.Foto = [];
            $scope.FotoAux = $scope.FotoTomada.toString();
            $scope.FotoBits = $scope.FotoAux.replace(/data:image\/png;base64,/g, '')
            $scope.Foto.push({
                NombreFoto: 'Fotografía',
                TipoFoto: 'image/png',
                ExtensionFoto: 'PNG',
                FotoBits: $scope.FotoBits,
                ValorDocumento: 1,
                FotoCargada: $scope.FotoAux,
                id: $scope.Foto.length
            });
        }

        function AsignarFotoListado() {
            // $scope.Foto = [];
            $scope.Foto.push({
                NombreFoto: $scope.NombreFoto,
                TipoFoto: $scope.TipoFoto,
                ExtensionFoto: $scope.TipoImagen,
                FotoBits: $scope.Convertirfoto,
                ValorDocumento: 1,
                FotoCargada: 'data:' + $scope.TipoFoto + ';base64,' + $scope.Convertirfoto,
                id: $scope.Foto.length
            });
        }

        $scope.EliminarFoto = function (item) {
            $scope.Foto.splice(item.id, 1);
            for (var i = 0; i < $scope.Foto.length; i++) {
                $scope.Foto[i].id = i;
            }
            // $scope.Foto = []
            $scope.ModificarFoto = true;
            //$("#Foto").attr("src", "");
        }

        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvas se dibuja la imagen que se recibe por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }

        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }

        function DatosRequeridos() {
            console.log('confirmando')
            $scope.MensajesErrorRecibir = [];
            $scope.MensajesErrorDocu = [];
            var continuar = true;
            if ($scope.Sesion.UsuarioAutenticado.Oficinas === undefined) {
                $scope.MensajesErrorRecibir.push('El usuario no cuenta con una oficina asignada');
                continuar = false;
            }

            if ($scope.Modal.Cantidad === undefined || $scope.Modal.Cantidad === '' || $scope.Modal.Cantidad === null || $scope.Modal.Cantidad === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if ($scope.Modal.Peso === undefined || $scope.Modal.Peso === '' || $scope.Modal.Peso === null || $scope.Modal.Peso === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el peso');
                continuar = false;
            }
            if ($scope.Modal.TipoIdentificacionRecibe === undefined || $scope.Modal.TipoIdentificacionRecibe === '' || $scope.Modal.TipoIdentificacionRecibe === null || $scope.Modal.TipoIdentificacionRecibe.Codigo === CODIGO_NO_APLICA_TIPO_IDENTIFICACION) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el tipo de identificación');
                continuar = false;
            }
            if ($scope.Modal.NumeroIdentificacion === undefined || $scope.Modal.NumeroIdentificacion === '' || $scope.Modal.NumeroIdentificacion === null || $scope.Modal.NumeroIdentificacion === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el número de identificación');
                continuar = false;
            }
            if ($scope.Modal.Nombre === undefined || $scope.Modal.Nombre === '' || $scope.Modal.Nombre === null || $scope.Modal.Nombre === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el nombre de la persona que recibe');
                continuar = false;
            }
            if ($scope.Modal.Telefono === undefined || $scope.Modal.Telefono === '' || $scope.Modal.Telefono === null || $scope.Modal.Telefono === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el teléfono');
                continuar = false;
            }
            if ($scope.Modal.Observaciones === undefined || $scope.Modal.Observaciones === '' || $scope.Modal.Observaciones === null || $scope.Modal.Observaciones === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar las observaciones');
                console.log('observaciones')
                continuar = false;
            }
            if ($scope.ListaGestionDocuCliente.length > 0) {
                for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                    if ($scope.ListaGestionDocuCliente[i].Entregado.Codigo == -1) {
                        $scope.MensajesErrorDocu.push('Se debe seleccionar en todos los documentos si fueron entregados o no');
                        continuar = false;
                        break;
                    }
                }
            }
            console.log('CONTINUANDO', $scope.MensajesErrorRecibir)
            return continuar;
        }

        function DatosRequeridosRecoleccion() {
            $scope.MensajesErrorRecibir = [];
            var continuar = true;

            if ($scope.ModalRecoleccion.Cantidad === undefined || $scope.ModalRecoleccion.Cantidad === '' || $scope.ModalRecoleccion.Cantidad === null || $scope.Modal.Cantidad === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la cantidad');
                continuar = false;
            }
               if ($scope.Sesion.UsuarioAutenticado.Oficinas === undefined ) {
                $scope.MensajesErrorRecibir.push('El usuario no cuenta con una oficina asignada');
                continuar = false;
            }
            if ($scope.ModalRecoleccion.Peso === undefined || $scope.ModalRecoleccion.Peso === '' || $scope.ModalRecoleccion.Peso === null || $scope.ModalRecoleccion.Peso === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el peso');
                continuar = false;
            }

            if ($scope.ModalRecoleccion.Nombre === undefined || $scope.ModalRecoleccion.Nombre === '' || $scope.ModalRecoleccion.Nombre === null || $scope.ModalRecoleccion.Nombre === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el nombre de la persona que recibe');
                continuar = false;
            }
            if ($scope.ModalRecoleccion.Telefono === undefined || $scope.Modal.Telefono === '' || $scope.ModalRecoleccion.Telefono === null || $scope.ModalRecoleccion.Telefono === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el teléfono');
                continuar = false;
            }
            if ($scope.ModalRecoleccion.Observaciones === undefined || $scope.ModalRecoleccion.Observaciones === '' || $scope.ModalRecoleccion.Observaciones === null || $scope.ModalRecoleccion.Observaciones === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar las observaciones');
                continuar = false;
            }
            return continuar;
        }
        //----------------------------Funciones Generales---------------------------------//
    }]);