﻿
Print 'CREATE PROCEDURE gsp_obtener_encabezado_facturas'
GO
DROP PROCEDURE gsp_obtener_encabezado_facturas
GO
  
CREATE PROCEDURE gsp_obtener_encabezado_facturas (    
@par_EMPR_Codigo SMALLINT,    
@par_Numero NUMERIC = NULL,
@par_NumeroDocumento NUMERIC = NULL,
@par_TERC_Cliente NUMERIC = NULL,
@par_Estado NUMERIC = NULL,
@par_Anulado NUMERIC = NULL
)    
AS    
BEGIN    
    
 SELECT     
  1 As Obtener,    
  ENFA.EMPR_Codigo,    
  ENFA.Numero,    
  ENFA.TIDO_Codigo,    
  ENFA.Numero_Documento,    
  ENFA.Fecha,    
  ENFA.OFIC_Factura,    
  ENFA.TERC_Cliente,    
  ENFA.TERC_Facturar,
  ENFA.CATA_TIFA_Codigo,
  ENFA.CATA_FPVE_Codigo,    
  ISNULL(ENFA.Dias_Plazo,0) As Dias_Plazo,    
  ISNULL(ENFA.Observaciones,'') As Observaciones,    
  ISNULL(ENFA.Valor_Remesas,0) As Valor_Remesas,    
  ISNULL(ENFA.Valor_Otros_Conceptos,0) As Valor_Otros_Conceptos,    
  ISNULL(ENFA.Valor_Descuentos,0) As Valor_Descuentos,    
  ISNULL(ENFA.Subtotal,0) As Subtotal,    
  ISNULL(ENFA.Valor_Impuestos,0) As Valor_Impuestos,    
  ISNULL(ENFA.Valor_Factura,0) As Valor_Factura,    
  ISNULL(ENFA.Valor_TRM,0) As Valor_TRM,    
  ISNULL(ENFA.MONE_Codigo_Alterna,0) As MONE_Codigo_Alterna,    
  ISNULL(ENFA.Valor_Moneda_Alterna,0) As Valor_Moneda_Alterna,    
  ISNULL(ENFA.Valor_Anticipo,0) As Valor_Anticipo,    
  ISNULL(ENFA.Factura_Electronica,0) As Estado_FAEL,    
  ISNULL(ENFA.Estado,0) As Estado,    
    
  ISNULL(ENFA.Anulado,0) As Anulado,    
  ISNULL(ENFA.Fecha_Anula,'01/01/1900') As Fecha_Anula,    
  ISNULL(ENFA.USUA_Codigo_Anula,0) As USUA_Codigo_Anula,    
  ISNULL(ENFA.Causa_Anula,'') As Causa_Anula,  
  0 AS ObtFacEle  
    
  FROM Encabezado_Facturas ENFA    
  WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo    
  AND ENFA.Numero = ISNULL(@par_Numero, ENFA.Numero) 
  AND ENFA.Numero_Documento = ISNULL (@par_NumeroDocumento,ENFA.Numero_Documento)
  AND ENFA.TERC_Cliente = ISNULL (@par_TERC_Cliente,ENFA.TERC_Cliente)
  AND ENFA.Estado = ISNULL (@par_Estado,ENFA.Estado)
  AND ENFA.Anulado = ISNULL (@par_Anulado,ENFA.Anulado)
    
 END    
GO
