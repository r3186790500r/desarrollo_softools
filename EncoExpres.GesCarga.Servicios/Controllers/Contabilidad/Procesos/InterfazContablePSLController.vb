﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Contabilidad.Procesos
Imports EncoExpres.GesCarga.Negocio.Contabilidad

<Authorize>
Public Class InterfazContablePSLController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As InterfazContablePSL) As Respuesta(Of IEnumerable(Of InterfazContablePSL))
        Return New LogicaInterfazContablePSL(CapaPersistenciaInterfazContablePSL).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As InterfazContablePSL) As Respuesta(Of InterfazContablePSL)
        Return New LogicaInterfazContablePSL(CapaPersistenciaInterfazContablePSL).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As InterfazContablePSL) As Respuesta(Of Long)
        Return New LogicaInterfazContablePSL(CapaPersistenciaInterfazContablePSL).Guardar(entidad)
    End Function
End Class
