﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaUnidadMedida
        Implements IPersistenciaBase(Of UnidadMedida)

        Public Function Consultar(filtro As UnidadMedida) As IEnumerable(Of UnidadMedida) Implements IPersistenciaBase(Of UnidadMedida).Consultar
            Return New RepositorioUnidadMedida().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As UnidadMedida) As Long Implements IPersistenciaBase(Of UnidadMedida).Insertar
            Return New RepositorioUnidadMedida().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As UnidadMedida) As Long Implements IPersistenciaBase(Of UnidadMedida).Modificar
            Return New RepositorioUnidadMedida().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As UnidadMedida) As UnidadMedida Implements IPersistenciaBase(Of UnidadMedida).Obtener
            Return New RepositorioUnidadMedida().Obtener(filtro)
        End Function
    End Class

End Namespace

