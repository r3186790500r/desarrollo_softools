﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	PRINT 'gsp_consultar_TRM'
GO
DROP PROCEDURE gsp_consultar_TRM
GO
CREATE PROCEDURE gsp_consultar_TRM(  
  @par_EMPR_Codigo SMALLINT,  
  @par_Codigo NUMERIC = NULL,  
  @par_NumeroPagina INT = NULL,  
  @par_RegistrosPagina INT = NULL  
 )  
 AS  
 BEGIN  
  SET NOCOUNT ON;  
   DECLARE  
    @CantidadRegistros INT  
   SELECT @CantidadRegistros = (  
     SELECT DISTINCT   
      COUNT(1)   
      FROM Tasa_Cambio_Monedas  TRM
      , usuarios USCR 
      , usuarios USMO 
	  , monedas MONE
      WHERE  
      TRM.EMPR_Codigo = @par_EMPR_Codigo  
      AND MONE_Codigo > 0  
      AND MONE_Codigo = ISNULL(@par_Codigo, MONE_Codigo) 
	  AND TRM.EMPR_Codigo = USCR.EMPR_Codigo
      AND TRM.USUA_Codigo_Crea = USCR.Codigo
	  AND TRM.EMPR_Codigo = USMO.EMPR_Codigo
      AND TRM.USUA_Codigo_Crea = USMO.Codigo
	  AND TRM.MONE_Codigo = MONE.Codigo
	        );         
     WITH Pagina AS  
     (  
      SELECT   
      TRM.EMPR_Codigo,  
	  MONE.Nombre_Corto,
	  MONE_Codigo, 
	  Fecha,
	  Valor_Moneda_Local,
      USCR.Codigo USUA_Codigo_Crea ,
	  TRM.Fecha_Crea ,
	  USMO.Codigo USUA_Codigo_Modifica ,
	  TRM.Fecha_Modifica,

      ROW_NUMBER() OVER(ORDER BY Fecha) AS RowNumber  
      FROM  
      Tasa_Cambio_Monedas TRM
	  , usuarios USCR 
      , usuarios USMO 
	  , Monedas MONE
      WHERE  
      TRM.EMPR_Codigo = @par_EMPR_Codigo  
      AND MONE_Codigo > 0  
      AND MONE_Codigo = ISNULL(@par_Codigo, MONE_Codigo)
	    AND TRM.EMPR_Codigo = USCR.EMPR_Codigo
      AND TRM.USUA_Codigo_Crea = USCR.Codigo
	  AND TRM.EMPR_Codigo = USMO.EMPR_Codigo
      AND TRM.USUA_Codigo_Crea = USMO.Codigo  
	  AND TRM.MONE_Codigo = MONE.Codigo
     ) 
   SELECT DISTINCT  
	 0 AS Obtener, 
	 EMPR_Codigo, 
	 Nombre_Corto, 
     MONE_Codigo, 
	 Fecha,
	 Valor_Moneda_Local,
	 USUA_Codigo_Crea,
	 Fecha_Crea,
	 USUA_Codigo_Modifica,
	 Fecha_Modifica,
   @CantidadRegistros AS TotalRegistros,  
   @par_NumeroPagina AS PaginaObtener,  
   @par_RegistrosPagina AS RegistrosPagina  
   FROM  
    Pagina  
   WHERE  
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
   order by MONE_Codigo  
   END   
GO