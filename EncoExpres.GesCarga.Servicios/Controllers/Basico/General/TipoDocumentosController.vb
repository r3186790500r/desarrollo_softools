﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="TipoDocumentosController"/>
''' </summary>
<Authorize>
Public Class TipoDocumentosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TipoDocumentos) As Respuesta(Of IEnumerable(Of TipoDocumentos))
        Return New LogicaTipoDocumentos(CapaPersistenciaTipoDocumentos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TipoDocumentos) As Respuesta(Of TipoDocumentos)
        Return New LogicaTipoDocumentos(CapaPersistenciaTipoDocumentos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TipoDocumentos) As Respuesta(Of Long)
        Return New LogicaTipoDocumentos(CapaPersistenciaTipoDocumentos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As TipoDocumentos) As Respuesta(Of Boolean)
        Return New LogicaTipoDocumentos(CapaPersistenciaTipoDocumentos).Anular(entidad)
    End Function
End Class
