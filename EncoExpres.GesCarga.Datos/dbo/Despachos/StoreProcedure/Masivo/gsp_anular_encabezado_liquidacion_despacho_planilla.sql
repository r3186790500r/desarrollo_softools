﻿PRINT 'gsp_anular_encabezado_liquidacion_despacho_planilla'
GO

DROP PROCEDURE gsp_anular_encabezado_liquidacion_despacho_planilla
GO

CREATE PROCEDURE gsp_anular_encabezado_liquidacion_despacho_planilla
(
	@par_EMPR_Codigo SMALLINT,
	@par_Numero NUMERIC,
	@par_USUA_Codigo_Anula NUMERIC,
	@par_Causa_Anula VARCHAR(150)
)
AS
BEGIN
	UPDATE
		Encabezado_Liquidacion_Planilla_Despachos
	SET
		Anulado = 1,
		Fecha_Anula = GETDATE(),
		USUA_Codigo_Anula = @par_USUA_Codigo_Anula,
		Causa_Anula = @par_Causa_Anula
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Numero = @par_Numero

	--Se desasocia la liquidación de la planilla de despacho
	UPDATE
		Encabezado_Planilla_Despachos
	SET
		ELPD_Numero = 0
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND ELPD_Numero = @par_Numero

	SELECT @par_Numero AS Numero
END
GO