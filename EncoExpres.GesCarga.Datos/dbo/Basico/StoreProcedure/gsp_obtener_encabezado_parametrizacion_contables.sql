﻿PRINT 'gsp_obtener_encabezado_parametrizacion_contables'
GO
DROP PROCEDURE gsp_obtener_encabezado_parametrizacion_contables
GO
CREATE PROCEDURE gsp_obtener_encabezado_parametrizacion_contables
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC
)
AS
BEGIN

	SELECT 
	1 AS Obtener,
	ENPC.EMPR_Codigo,
	ENPC.Codigo,
	ENPC.Codigo_Alterno,
	ENPC.Nombre,
	ENPC.CATA_TIDG_Codigo,
	ENPC.Fuente,
	ENPC.Fuente_Anulacion,
	ENPC.CATA_TGDC_Codigo,
	ENPC.Observaciones,
	ENPC.Provision,
	ENPC.Estado,
	TIDG.Nombre AS TipoDocumentoGenera,
	TGDC.Nombre AS TipoGeneraDetalleContable

	FROM
	Encabezado_Parametrizacion_Contables ENPC,
	V_Tipo_Documento_Genera TIDG,
	V_Tipo_Generacion_Detalle_Contable TGDC

	WHERE 
	ENPC.EMPR_Codigo = @par_EMPR_Codigo
	AND ENPC.Codigo = @par_Codigo 

	AND ENPC.EMPR_Codigo = TIDG.EMPR_Codigo
	AND ENPC.CATA_TIDG_Codigo = TIDG.Codigo

	AND ENPC.EMPR_Codigo = TGDC.EMPR_Codigo
	AND ENPC.CATA_TGDC_Codigo = TGDC.Codigo

END
GO