﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class RepositorioDetalleSolicitudOrdenServicios
        Inherits RepositorioBase(Of DetalleSolicitudOrdenServicios)
        Public Overrides Function Consultar(filtro As DetalleSolicitudOrdenServicios) As IEnumerable(Of DetalleSolicitudOrdenServicios)
            Dim lista As New List(Of DetalleSolicitudOrdenServicios)
            Dim item As DetalleSolicitudOrdenServicios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_ESOS_Numero", filtro.Numero)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_solicitud_orden_servicios")
                While resultado.Read
                    item = New DetalleSolicitudOrdenServicios(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleSolicitudOrdenServicios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleSolicitudOrdenServicios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleSolicitudOrdenServicios) As DetalleSolicitudOrdenServicios
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleSolicitudOrdenServicios) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
