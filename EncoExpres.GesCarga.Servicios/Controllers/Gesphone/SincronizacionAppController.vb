﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Negocio.Aplicativo.SeguridadUsuarios
Imports EncoExpres.GesCarga.Negocio.Basico.General
Imports EncoExpres.GesCarga.Negocio.Despachos
Imports EncoExpres.GesCarga.Negocio.Gesphone
Imports EncoExpres.GesCarga.Negocio.Paqueteria

''' <summary>
''' Controlador <see cref="SincronizacionAppController"/>.
''' </summary>

Public Class SincronizacionAppController
    Inherits Base

#Region "OffLine"

    <HttpGet>
    <ActionName("GenerarEstructuraBaseOffLine")>
    Public Function GenerarEstructuraBaseOffLine() As DataBaseOffLine
        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).GenerarEstructuraBaseOffLine()
    End Function

#End Region

#Region "Usuario"

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Usuarios) As Respuesta(Of Usuarios)
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).Obtener(filtro)
    End Function

#End Region

#Region "Catalogos - Info Basica"

    <HttpPost>
    <ActionName("ConsultarCatalogos")>
    Public Function ConsultarCatalogos(ByVal filtro As ValorCatalogos) As Respuesta(Of IEnumerable(Of ValorCatalogos))
        Return New LogicaValorCatalogos(CapaPersistenciaValorCatalogos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarTiposIdentificacion")>
    Public Function ConsultarTiposIdentificacion(ByVal filtro As TipoDocumentos) As Respuesta(Of IEnumerable(Of TipoDocumentos))
        Return New LogicaTipoDocumentos(CapaPersistenciaTipoDocumentos).Consultar(filtro)
    End Function

#End Region

#Region "Tarifario"

    <HttpGet>
    <ActionName("SincronizarTarifario")>
    Public Function SincronizarTarifario(codigoEmpresa As Short, top As Integer) As Respuesta(Of SyncEncabezadoTarifarioVenta)
        Dim filtro As New FiltroSyncApp With {
            .CodigoEmpresa = codigoEmpresa,
            .SincronizarTodo = 1
        }

        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).SincronizarTarifario(filtro)
    End Function

    <HttpGet>
    <ActionName("ConsultarTarifarioCliente")>
    Public Function ConsultarTarifarioCliente(codigoEmpresa As Short, codigoCliente As Integer, codigoCiudadOrigen As Integer, codigoCiudadDestino As Integer, codigoLineaNegocio As Integer) As Respuesta(Of SyncEncabezadoTarifarioVenta)
        Dim filtro As New FiltroSyncApp With {
            .CodigoEmpresa = codigoEmpresa,
            .CodigoCliente = codigoCliente,
            .CodigoCiudadOrigen = codigoCiudadOrigen,
            .CodigoCiudadDestino = codigoCiudadDestino,
            .CodigoLineaNegocio = codigoLineaNegocio,
            .SincronizarTodo = 0
        }

        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).ConsultarTarifarioCliente(filtro)
    End Function

#End Region

#Region "Planillas"

    <HttpGet>
    <ActionName("ConsultarPlanillas")>
    Public Function ConsultarPlanillas(codigoEmpresa As Short, codigoConductor As Integer, codigoOficina As Short) As Respuesta(Of List(Of SyncEncabezadoPlanilla))
        Dim filtro As New FiltroSyncApp With {
            .CodigoEmpresa = codigoEmpresa,
            .CodigoConductor = codigoConductor,
            .CodigoOficina = codigoOficina
        }

        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).ConsultarPlanillasPendienteEntrega(filtro)
    End Function

    <HttpGet>
    <ActionName("ConsultarImagenesRemesaPaqueteria")>
    Public Function ConsultarImagenesRemesaPaqueteria(codigoEmpresa As Short, numeroRemesa As Long) As Respuesta(Of IEnumerable(Of SyncDetallePlanillaImagenes))
        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).ConsultarImagenesRemesaPaqueteria(codigoEmpresa, numeroRemesa)
    End Function

#End Region

#Region "Entrega/Devoluciones"

    <ActionName("GuardarEntrega")>
    Public Function GuardarEntrega(objRegistro As Remesas) As Respuesta(Of Long)

        '' Consultamos la informacion complementaria de la remesa
        Dim logicaNegocio = New LogicaRemesas(CapaPersistenciaRemesas)
        Dim objRemesa = logicaNegocio.Obtener(objRegistro)

        objRemesa.Datos.CodigoTipoIdentificacionRecibe = objRegistro.CodigoTipoIdentificacionRecibe
        objRemesa.Datos.NumeroIdentificacionRecibe = objRegistro.NumeroIdentificacionRecibe
        objRemesa.Datos.NombreRecibe = objRegistro.NombreRecibe
        objRemesa.Datos.TelefonoRecibe = objRegistro.TelefonoRecibe

        objRemesa.Datos.NovedadEntrega = New Basico.General.ValorCatalogos()
        objRemesa.Datos.NovedadEntrega.Codigo = objRegistro.NovedadEntrega.Codigo
        objRemesa.Datos.ObservacionesRecibe = objRegistro.ObservacionesRecibe
        objRemesa.Datos.Firma = objRegistro.Firma

        If IsNothing(objRemesa.Datos.UsuarioModifica) Then
            objRemesa.Datos.UsuarioModifica = New Usuarios()
        End If

        objRemesa.Datos.UsuarioModifica.Codigo = objRegistro.UsuarioModifica.Codigo

        objRemesa.Datos.Fotografias = objRegistro.Fotografias
        objRemesa.Datos.DevolucionRemesa = objRegistro.DevolucionRemesa
        objRemesa.Datos.GestionDocumentosRemesa = objRegistro.GestionDocumentosRemesa
        objRemesa.Datos.CantidadRecibe = objRemesa.Datos.CantidadCliente
        objRemesa.Datos.PesoRecibe = objRemesa.Datos.PesoCliente

        If IsNothing(objRemesa.Datos.UsuarioCrea) Then
            objRemesa.Datos.UsuarioCrea = New Usuarios()
        End If

        objRemesa.Datos.UsuarioCrea.Codigo = objRegistro.UsuarioCrea.Codigo
        objRemesa.Datos.Planilla.Codigo = objRegistro.Planilla.Codigo

        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).GuardarEntrega(objRemesa.Datos)
    End Function


    <ActionName("GuardarListadoEntregas")>
    Public Function GuardarListadoEntregas(listado As List(Of Remesas)) As Respuesta(Of Long)

        '' Consultamos la informacion complementaria de la remesa
        Dim logicaNegocio = New LogicaRemesas(CapaPersistenciaRemesas)
        Dim resultado As New Respuesta(Of Long)

        For Each objRegistro In listado
            Dim objRemesa = logicaNegocio.Obtener(objRegistro)

            If objRemesa.Datos.Cumplido = 0 Then

                objRemesa.Datos.CodigoTipoIdentificacionRecibe = objRegistro.CodigoTipoIdentificacionRecibe
                objRemesa.Datos.NumeroIdentificacionRecibe = objRegistro.NumeroIdentificacionRecibe
                objRemesa.Datos.NombreRecibe = objRegistro.NombreRecibe
                objRemesa.Datos.TelefonoRecibe = objRegistro.TelefonoRecibe

                objRemesa.Datos.NovedadEntrega = New ValorCatalogos()
                objRemesa.Datos.NovedadEntrega.Codigo = objRegistro.NovedadEntrega.Codigo
                objRemesa.Datos.ObservacionesRecibe = objRegistro.ObservacionesRecibe
                objRemesa.Datos.Firma = objRegistro.Firma

                If IsNothing(objRemesa.Datos.UsuarioModifica) Then
                    objRemesa.Datos.UsuarioModifica = New Usuarios()
                End If

                objRemesa.Datos.UsuarioModifica.Codigo = objRegistro.UsuarioModifica.Codigo

                objRemesa.Datos.Fotografias = objRegistro.Fotografias
                objRemesa.Datos.DevolucionRemesa = objRegistro.DevolucionRemesa
                objRemesa.Datos.GestionDocumentosRemesa = objRegistro.GestionDocumentosRemesa
                objRemesa.Datos.CantidadRecibe = objRemesa.Datos.CantidadCliente
                objRemesa.Datos.PesoRecibe = objRemesa.Datos.PesoCliente

                If IsNothing(objRemesa.Datos.UsuarioCrea) Then
                    objRemesa.Datos.UsuarioCrea = New Usuarios()
                End If

                objRemesa.Datos.UsuarioCrea.Codigo = objRegistro.UsuarioCrea.Codigo
                objRemesa.Datos.Planilla.Codigo = objRegistro.Planilla.Codigo

                Try
                    resultado = New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).GuardarEntrega(objRemesa.Datos)
                Catch ex As Exception
                End Try
            End If
        Next

        Return New Respuesta(Of Long) With {.ProcesoExitoso = True}

    End Function

    <ActionName("GuardarImagenesRemesa")>
    Public Function GuardarImagenesRemesa(registro As SyncFotografiaRemesa) As Respuesta(Of Long)

        '' Consultamos la informacion de la remesa
        Dim logicaNegocio = New LogicaRemesas(CapaPersistenciaRemesas)

        Dim objRemesa = logicaNegocio.Obtener(New Remesas() With {.CodigoEmpresa = registro.CodigoEmpresa, .Numero = registro.NumeroRemesa})

        If registro.Esfirma And IsNothing(objRemesa.Datos.Firma) Then
            objRemesa.Datos.Firma = registro.Foto.FotoBits
            Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).GuardarEntrega(objRemesa.Datos)
        ElseIf registro.Esfirma = False Then
            objRemesa.Datos.Firma = Nothing
            objRemesa.Datos.Fotografia = registro.Foto
            Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).GuardarEntrega(objRemesa.Datos)
        End If

        Return New Respuesta(Of Long) With {.ProcesoExitoso = True}

    End Function

#End Region

#Region "Terceros"

    Public Function ConsultarTerceros(filtro As Terceros) As Respuesta(Of IEnumerable(Of SyncTerceros))
        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).ConsultarTerceros(filtro)
    End Function

#End Region

#Region "Ciudades"

    Public Function ConsultarCiudades(filtro As Ciudades) As Respuesta(Of IEnumerable(Of SyncCiudades))
        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).ConsultarCiudades(filtro)
    End Function

#End Region

#Region "Sitios Cargue Descargue"

    Public Function ConsultarSitiosCargueDescargue(filtro As SitiosCargueDescargue) As Respuesta(Of IEnumerable(Of SyncSitiosCargueDescargue))
        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).ConsultarSitiosCargueDescargue(filtro)
    End Function

#End Region

#Region "Productos Transportados"

    Public Function ConsultarProductorTransportados(ByVal filtro As ProductoTransportados) As Respuesta(Of IEnumerable(Of SyncProductosTransportados))
        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).ConsultarProductosTransportador(filtro)
    End Function

#End Region

#Region "Guias"

    <HttpPost>
    <ActionName("GuardarGuiaRecogida")>
    Public Function GuardarGuiaRecogida(entidad As RemesaGesphone) As Respuesta(Of Long)

        Dim resultado = New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Guardar(entidad.Remesa)

        If entidad.NumeroRecoleccion > 0 Then
            If IsNothing(resultado) = False And resultado.ProcesoExitoso Then

                '' Cambiar el estado a la recolección
                Dim cambioEstado = New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).ModificarEstadoRecoleccion(entidad.Remesa.CodigoEmpresa, entidad.NumeroRecoleccion, 21801)
            End If
        End If

        Return resultado
    End Function

    <HttpPost>
    <ActionName("GuardarListadoGuiasRecogidas")>
    Public Function GuardarListadoGuiasRecogidas(listado As IEnumerable(Of RemesaGesphone)) As Respuesta(Of List(Of SyncRemesaRelacionPlanila))
        Dim respuesta As New Respuesta(Of List(Of SyncRemesaRelacionPlanila)) With {
            .Datos = New List(Of SyncRemesaRelacionPlanila)
        }

        For Each guia In listado
            Dim resultado = New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Guardar(guia.Remesa)

            If guia.NumeroRecoleccion > 0 Then
                If IsNothing(resultado) = False And resultado.ProcesoExitoso Then
                    '' Cambiar el estado a la recolección
                    Dim cambioEstado = New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).ModificarEstadoRecoleccion(guia.Remesa.CodigoEmpresa, guia.NumeroRecoleccion, 21801)
                End If
            End If

            respuesta.Datos.Add(New SyncRemesaRelacionPlanila With {.Numero = resultado.Numero, .NumeroDocumento = resultado.Datos})
            respuesta.MensajeOperacion = "OK"
            respuesta.ProcesoExitoso = True
        Next

        Return respuesta
    End Function

    <HttpPost>
    <ActionName("AsociarGuiaRecogidaPlanilla")>
    Public Function AsociarGuiaRecogidaPlanilla(entidad As RemesaPaqueteria) As Respuesta(Of Boolean)

        '' Consultar los datos completos de la planilla
        'Dim objEntidadPlanilla As New Despachos.Masivo.PlanillaDespachos With {
        '    .CodigoEmpresa = entidad.CodigoEmpresa,
        '    .Numero = entidad.NumeroPlanilla,
        '    .TipoDocumento = 135
        '}

        'Dim objPlanilla = CapaPersistenciaPlanillaDespachos.Obtener(objEntidadPlanilla)
        'entidad.Oficina = New Oficinas()
        'entidad.Oficina.Codigo = objPlanilla.Oficina.Codigo

        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).AsociarGuiaPlanilla(entidad)
    End Function

    <HttpPost>
    <ActionName("AsociarListadoEtiquetas")>
    Public Function AsociarListadoEtiquetas(listado As IEnumerable(Of SyncEtiquetaPreimpresa)) As Respuesta(Of Boolean)
        Dim logicaNegocio = New LogicaRemesas(CapaPersistenciaRemesas)
        Dim resultado As New Respuesta(Of Boolean)

        For Each eliminar In listado.Where(Function(x) x.Eliminar = True)
            Dim registro As New EtiquetaPreimpresa With {.CodigoEmpresa = eliminar.CodigoEmpresa, .ENRE_Numero = eliminar.NumeroRemesa, .Numero = eliminar.Etiqueta}
            Dim res = New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).LiberarEtiquetas(registro)
        Next

        resultado = New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).InsertarDetalleEtiquetasPreimpresas(listado.Where(Function(x) x.Eliminar = False))

        Return resultado
    End Function

    <HttpPost>
    <ActionName("AsociarEtiquetas")>
    Public Function AsociarEtiquetas(entidad As IEnumerable(Of SyncEtiquetaPreimpresa)) As Respuesta(Of Boolean)
        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).InsertarDetalleEtiquetasPreimpresas(entidad)
    End Function

    <HttpPost>
    <ActionName("LiberarEtiquetas")>
    Public Function LiberarEtiquetas(entidad As EtiquetaPreimpresa) As Respuesta(Of Boolean)
        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).LiberarEtiquetas(entidad)
    End Function

    <HttpPost>
    <ActionName("ListadoIntentoEntregaGuia")>
    Public Function ListadoIntentoEntregaGuia(listado As IEnumerable(Of DetalleIntentoEntregaGuia)) As Respuesta(Of Boolean)

        Dim logicaNegocio = New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp)
        Dim resultado As New Respuesta(Of Boolean)

        For Each intento In listado
            resultado = logicaNegocio.InsertarDetalleIntentoEntregaGuia(intento)
        Next

        Return resultado
    End Function

    <HttpPost>
    <ActionName("IntentoEntregaGuia")>
    Public Function IntentoEntregaGuia(entidad As DetalleIntentoEntregaGuia) As Respuesta(Of Boolean)
        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).InsertarDetalleIntentoEntregaGuia(entidad)
    End Function

    <HttpPost>
    <ActionName("ModificarEstadoRecoleccion")>
    Public Function ModificarEstadoRecoleccion(entidad As RecoleccionEstado) As Respuesta(Of Boolean)
        Return New LogicaSincronizacionApp(CapaPersistenciaSincronizacionApp).ModificarEstadoRecoleccion(entidad.CodigoEmpresa, entidad.NumeroRecoleccion, entidad.Estado)
    End Function

#End Region

End Class
