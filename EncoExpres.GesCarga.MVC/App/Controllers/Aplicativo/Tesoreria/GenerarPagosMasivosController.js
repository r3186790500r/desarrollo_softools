﻿EncoExpresApp.controller("GenerarPagosMasivosCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'OficinasFactory', 'DocumentoCuentasFactory', 'blockUIConfig', 'TercerosFactory', 'BancosFactory', 'CajasFactory', 'CuentaBancariasFactory', 'ConceptoContablesFactory', 'DetalleConceptoContablesFactory', 'DocumentoComprobantesFactory', 'LiquidacionesFactory', 'PlanillaDespachosFactory', 'EmpresasFactory',
    function ($scope, $timeout, $linq, blockUI, ValorCatalogosFactory, OficinasFactory, DocumentoCuentasFactory, blockUIConfig, TercerosFactory, BancosFactory, CajasFactory, CuentaBancariasFactory, ConceptoContablesFactory, DetalleConceptoContablesFactory, DocumentoComprobantesFactory, LiquidacionesFactory, PlanillaDespachosFactory, EmpresasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Tesoreria' }, { Nombre: 'Procesos' }, { Nombre: 'Generar Pagos Masivos' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + CODIGO_MENU_GENERAR_PAGOS_MASIVOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }


        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.strCodigosCuentas = ''
        $scope.DataArchivo = [];
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoOficinas = [];
        $scope.ListaCXP = [];
        $scope.ListaCXPconEgresos = [];
        $scope.ListadoTodaCajaOficinas = [];
        $scope.ListadoConceptoContable = [];
        $scope.cantidadRegistrosPorPagina = 1000
        $scope.paginaActual = 1
        $scope.paginaActualconEgresos = 1
        
        var strCodigosCuentas = ''
        var SumatoriaCreditos = 0
        $scope.Modelo = {
            DocumentoOrigen: '',
            FechaInicioEgresos: new Date(),
            FechaFinEgresos: new Date(),
            FechaInicio: new Date(),
            FechaFin: new Date()
        }
        $scope.ListaCodigosRespuesta = DocumentoComprobantesFactory.ConsultarCodigosRespuesaBancolombia({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos;

        $('#TabGeneraryCargarArchivoBancolombia').hide();
        $('#TabGenerarEgresos').show();
        $scope.MostrarTabGenerarEgresos = function () {
            $('#TabGeneraryCargarArchivoBancolombia').hide();
            $('#TabGenerarEgresos').show();

        };
        $scope.MostrarTabGeneraryCargarArchivoBancolombia = function () {
            $('#TabGeneraryCargarArchivoBancolombia').show();
            $('#TabGenerarEgresos').hide();
        }


        $scope.MensajesErrorcxp = [];
        /*Cargar el combo de Documenteos Origen*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDocumentosOrigen = []
                    $scope.ListadoDocumentosOrigen.push({ Codigo: 2600, Nombre: "(TODOS)" });
                    response.data.Datos.forEach(function (item) {
                        if (item.CampoAuxiliar2.match(/EGR/) && item.CampoAuxiliar2.match(/GEN/)) {
                            // se filtra para que solo salgan documentos de anticipos y liquidaciones:
                            if (item.Codigo == 2602 || item.Codigo == 2604) {
                                $scope.ListadoDocumentosOrigen.push(item);
                            }
                        }
                    });
                    if ($scope.CodigoDocumentoOrigen !== undefined && $scope.CodigoDocumentoOrigen !== '' && $scope.CodigoDocumentoOrigen !== null) {
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + $scope.CodigoDocumentoOrigen);
                        $scope.Modelo.DocumentoOrigenEgresos = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + $scope.CodigoDocumentoOrigen);
                    }
                    else {
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == 2600');
                        $scope.Modelo.DocumentoOrigenEgresos = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == 2600');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        /*Cargar el combo de Oficinas*/
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                $scope.ListadoOficinas = [];
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoOficinas = response.data.Datos
                        $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' })
                        if ($scope.CodigoOficinaDestino !== undefined && $scope.CodigoOficinaDestino !== '' && $scope.CodigoOficinaDestino !== null) {
                            $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficinaDestino)
                            $scope.Modelo.OficinaDestinoEgresos = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficinaDestino)
                        }
                        else {
                            $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==-1 ');
                            $scope.Modelo.OficinaDestinoEgresos = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==-1 ');
                        }

                        try {
                            $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                        } catch (e) {
                            $scope.Oficina = $scope.ListadoOficina[$scope.ListadoOficinas.length - 1];
                        }
                    }
                    else {
                        $scope.ListadoOficinas = [];
                    }
                }
            }, function (response) {
            });

        $scope.ListadoTerceros = []
        $scope.AutocompleteTerceros = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            //CadenaPerfiles: PERFIL_TENEDOR,
                            ValorAutocomplete: value, Sync: true
                        })
                    $scope.ListadoTerceros = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTerceros)
                }
            }
            return $scope.ListadoTerceros
        }

        /*Cargar el listado de Bancos*/
        BancosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoBancos = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoBancos.push(item);
                    });
                    try {
                        $scope.Modelo.Cliente.Impuestos
                        $scope.Modelo.Banco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + $scope.Modelo.Banco.Codigo);
                    } catch (e) {
                        $scope.Modelo.Banco = $scope.ListadoBancos[0]
                    }
                }
            }, function (response) {
            });

        //Cargar Listado Tipos Cuentas Bancarias:
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CUENTA_BANCARIA }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoBancos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoBancos = response.data.Datos;
                        try {
                            $scope.Modelo.TipoBanco = $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo ==' + $scope.Modelo.TipoBanco.Codigo);
                        } catch (e) {
                            $scope.Modelo.TipoBanco = $scope.ListadoTipoBancos[0]
                        }
                    }
                    else {
                        $scope.ListadoTipoBancos = []
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de Forma de pago*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MEDIO_PAGO } }).
            then(function (response) {
                $scope.ListaFormaPago = []
                if (response.data.ProcesoExitoso === true) {

                    for (var i = 0; i < response.data.Datos.length; i++) {
                        if (response.data.Datos[i].Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA || response.data.Datos[i].Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                            $scope.ListaFormaPago.push(response.data.Datos[i])
                        }
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //Cargar Cajas:
        CajasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTodaCajaOficinas = [];
                    $scope.ListadoTodaCajaOficinas = response.data.Datos;

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar Combo Conceptos Contables*/
        ConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoConceptoContable = response.data.Datos;
                    }
                    else {
                        $scope.ListadoConceptoContable = [];
                    }
                }
            }, function (response) {
                ShowError("Error Listado Conceptos Contables : " + response.statusText);
            });

        //Cargar Listados Conceptos Contables : 
        $scope.ListadoMovimientosAnticipo = []
        DetalleConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 13 /*Anticipo*/ }).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    $scope.ListadoMovimientosAnticipo = response.data.Datos;
                }
            });

        $scope.ListadoMovimientosLiquidacion = []
        DetalleConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 14 /*Liquidación*/ }).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    $scope.ListadoMovimientosLiquidacion = response.data.Datos;
                }
            });


        function DatosRequeridosCXP() {
            $scope.MensajesErrorcxp = [];
            var continuar = true

            //if ($scope.Modelo.Tercero == undefined || $scope.Modelo.Tercero == null || $scope.Modelo.Tercero == "" || $scope.Modelo.Tercero.Codigo == 0) {
            //    $scope.MensajesErrorcxp.push('Debe ingresar un tercero');
            //    continuar = false;
            //}
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')

            ) {

            } else if (
                ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesErrorcxp.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorcxp.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar
        }

        function DatosRequeridosCXPconEgresos() {
            $scope.MensajesErrorcxp = [];
            var continuar = true

            //if ($scope.Modelo.Tercero == undefined || $scope.Modelo.Tercero == null || $scope.Modelo.Tercero == "" || $scope.Modelo.Tercero.Codigo == 0) {
            //    $scope.MensajesErrorcxp.push('Debe ingresar un tercero');
            //    continuar = false;
            //}
            if (($scope.FechaInicioEgresos === null || $scope.FechaInicioEgresos === undefined || $scope.FechaInicioEgresos === '')
                && ($scope.FechaFinEgresos === null || $scope.FechaFinEgresos === undefined || $scope.FechaFinEgresos === '')

            ) {

            } else if (
                ($scope.FechaInicioEgresos !== null && $scope.FechaInicioEgresos !== undefined && $scope.FechaInicioEgresos !== '')
                || ($scope.FechaFinEgresos !== null && $scope.FechaFinEgresos !== undefined && $scope.FechaFinEgresos !== '')

            ) {
                if (($scope.FechaInicioEgresos !== null && $scope.FechaInicioEgresos !== undefined && $scope.FechaInicioEgresos !== '')
                    && ($scope.FechaFinEgresos !== null && $scope.FechaFinEgresos !== undefined && $scope.FechaFinEgresos !== '')) {
                    if ($scope.FechaFinEgresos < $scope.FechaInicioEgresos) {
                        $scope.MensajesErrorcxp.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFinEgresos - $scope.FechaInicioEgresos) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorcxp.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicioEgresos !== null && $scope.FechaInicioEgresos !== undefined && $scope.FechaInicioEgresos !== '')) {
                        $scope.FechaFinEgresos = $scope.FechaInicioEgresos
                    } else {
                        $scope.FechaInicioEgresos = $scope.FechaFinEgresos
                    }
                }
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar
        }

        /*Consultar Cuentas Pendientes por pagar*/

        $scope.ConsultarCXP = function () {
            $scope.ListaCXP = [];
            $scope.totalRegistros = 0
            if (DatosRequeridosCXP()) {
                $scope.PagoUnificado = false;
                if (($scope.Modelo.Tercero != undefined && $scope.Modelo.Tercero != '' && $scope.Modelo.Tercero != null) && $scope.Modelo.DocumentoOrigen.Codigo != 2600) {
                    $scope.PagoUnificado = true
                }
                if ($scope.Modelo.DocumentoOrigen.Codigo != 2600) {
                    var Filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR, //CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR
                        Tercero: { Codigo: $scope.Modelo.Tercero == undefined ? 0 : $scope.Modelo.Tercero.Codigo },
                        DocumentoOrigen: { Codigo: $scope.Modelo.DocumentoOrigen.Codigo },
                        Oficina: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                        FechaInicial: $scope.Modelo.FechaInicio,
                        FechaFinal: $scope.Modelo.FechaFin,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                        CreadaManual: -1
                    }
                    //Consulta Cuentas pendientes:

                    DocumentoCuentasFactory.Consultar(Filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) {
                                        $scope.ListaCXP = response.data.Datos
                                        $scope.ListaCXP.forEach(function (item) {
                                            item.ValorPagar = MascaraValores(item.Saldo)
                                            item.NumeroDocumentoGrid = $scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2613 ? 'A - ' + item.NumeroDocumento : 'L - ' + item.NumeroDocumento
                                            item.NombreDocumentoOrigen = $scope.Modelo.DocumentoOrigen.Nombre
                                            item.Tercero = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Tercero.Codigo, Sync: true }).Datos
                                            item.NombreBanco = item.Tercero.Banco.Codigo == 0 ? 'SIN PARAMETRIZAR' : $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + item.Tercero.Banco.Codigo).Nombre
                                            item.NombreTipoCuenta = item.Tercero.TipoBanco.Codigo == 0 ? 'SIN PARAMETRIZAR' : $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo==' + item.Tercero.TipoBanco.Codigo).Nombre
                                            item.NumeroCuentaBancaria = item.Tercero.CuentaBancaria
                                            item.FechaPago = new Date()
                                            item.MedioPago = $scope.ListaFormaPago[1]
                                            item.ConceptoContable = item.ConceptoContable == undefined ? $scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2613 ? $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo==13') : $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo==14') : item2.ConceptoContable
                                            item.MovimientosConcepto = item.MovimientosConcepto == undefined ? $scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2613 ? $scope.ListadoMovimientosAnticipo : $scope.ListadoMovimientosLiquidacion : item.MovimientosConcepto
                                            item.GeneraEgreso = (item.Tercero.CuentaBancaria == 0 || item.Tercero.CuentaBancaria == '' || item.Tercero.CuentaBancaria == null || item.Tercero.CuentaBancaria == undefined) || (item.NombreBanco == '' || item.NombreBanco == null || item.NombreBanco == undefined || item.NombreBanco == '(NO APLICA)') || item.NombreTipoCuenta == '(NO APLICA)' || item.Tercero.Banco.Codigo == 0 || item.Tercero.TipoBanco.Codigo == 0 ? false : true
                                            item.DeshabilitarCheckEgreso = (item.Tercero.CuentaBancaria == 0 || item.Tercero.CuentaBancaria == '' || item.Tercero.CuentaBancaria == null || item.Tercero.CuentaBancaria == undefined) || (item.NombreBanco == '' || item.NombreBanco == null || item.NombreBanco == undefined || item.NombreBanco == '(NO APLICA)') || item.NombreTipoCuenta == '(NO APLICA)' || item.Tercero.Banco.Codigo == 0 || item.Tercero.TipoBanco.Codigo == 0 ? true : false
                                            $scope.CuentasSeleccionados = true
                                            item.Vehiculo = $scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2613 ? PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Numero: item.NumeroDocumento, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true }).Datos[0] == undefined ? {Codigo : 0} : PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Numero: item.NumeroDocumento, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true }).Datos[0].Vehiculo : $scope.CargarVehiculosPlaca(LiquidacionesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Aprobado: -1, NumeroDocumento: item.NumeroDocumento, Estado: -1, TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO }, Sync: true }).Datos[0].PlacaVehiculo)
                                            $scope.CargarCuentasOrigen(item)
                                            item.CuentaOrigen = item.ListadoCuentasOrigen[0]
                                            item.Observaciones = (item.Tercero.CuentaBancaria == 0 || item.Tercero.CuentaBancaria == '' || item.Tercero.CuentaBancaria == null || item.Tercero.CuentaBancaria == undefined) || (item.NombreBanco == '' || item.NombreBanco == null || item.NombreBanco == undefined) || item.Tercero.Banco.Codigo == 0 || item.Tercero.TipoBanco.Codigo == 0 ? item.Observaciones + '. Inhabilitado para generar egreso ya que no se ha parametrizado Numero de cuenta, Banco ó Tipo Cuenta válidas al tercero.' : item.Observaciones
                                        });
                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                        //$scope.MaskValores()
                                    } else {
                                        $scope.ListaCXP = [];
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                }
                                else {
                                    $scope.ListaCXP = [];
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                            }
                            $scope.CalcularTotales();
                        }, function (response) {
                        });
                } else {
                    $scope.ListadoDocumentosOrigen.forEach(item => {
                        var Filtros = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR, //CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR
                            Tercero: { Codigo: $scope.Modelo.Tercero == undefined ? 0 : $scope.Modelo.Tercero.Codigo },
                            DocumentoOrigen: { Codigo: item.Codigo },
                            Oficina: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                            FechaInicial: $scope.Modelo.FechaInicio,
                            FechaFinal: $scope.Modelo.FechaFin,
                            Pagina: $scope.paginaActual,
                            RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                            CreadaManual: -1
                        }

                        DocumentoCuentasFactory.Consultar(Filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {

                                            if ($scope.ListaCXP.length > 0) {
                                                response.data.Datos.forEach(data => {
                                                    $scope.ListaCXP.push(data)
                                                });
                                            } else {
                                                $scope.ListaCXP = response.data.Datos
                                            }
                                            $scope.ListaCXP.forEach(function (item2) {

                                                item2.ValorPagar = MascaraValores(item2.Saldo)
                                                item2.NumeroDocumentoGrid = item2.NumeroDocumentoGrid == undefined ? item.Codigo == 2604 || item.Codigo == 2613 ? 'A - ' + item2.NumeroDocumento : 'L - ' + item2.NumeroDocumento : item2.NumeroDocumentoGrid
                                                item2.NombreDocumentoOrigen = item2.NombreDocumentoOrigen == undefined ? item.Nombre : item2.NombreDocumentoOrigen
                                                item2.Tercero = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item2.Tercero.Codigo, Sync: true }).Datos
                                                item2.NombreBanco = item2.Tercero.Banco.Codigo == 0 ? 'SIN PARAMETRIZAR' : $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + item2.Tercero.Banco.Codigo).Nombre
                                                item2.NombreTipoCuenta = item2.Tercero.TipoBanco.Codigo == 0 ? 'SIN PARAMETRIZAR' : $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo==' + item2.Tercero.TipoBanco.Codigo).Nombre
                                                item2.NumeroCuentaBancaria = item2.Tercero.CuentaBancaria
                                                item2.FechaPago = new Date()
                                                item2.MedioPago = $scope.ListaFormaPago[1]
                                                item2.ConceptoContable = item2.ConceptoContable == undefined ? item.Codigo == 2604 || item.Codigo == 2613 ? $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo==13') : $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo==14') : item2.ConceptoContable
                                                item2.MovimientosConcepto = item2.MovimientosConcepto == undefined ? item.Codigo == 2604 || item.Codigo == 2613 ? $scope.ListadoMovimientosAnticipo : $scope.ListadoMovimientosLiquidacion : item2.MovimientosConcepto
                                                item2.GeneraEgreso = (item2.Tercero.CuentaBancaria == 0 || item2.Tercero.CuentaBancaria == '' || item2.Tercero.CuentaBancaria == null || item2.Tercero.CuentaBancaria == undefined) || (item2.NombreBanco == '' || item2.NombreBanco == null || item2.NombreBanco == undefined || item2.NombreBanco == '(NO APLICA)') || item2.NombreTipoCuenta == '(NO APLICA)' || item2.Tercero.Banco.Codigo == 0 || item2.Tercero.TipoBanco.Codigo == 0 ? false : true
                                                item2.DeshabilitarCheckEgreso = (item2.Tercero.CuentaBancaria == 0 || item2.Tercero.CuentaBancaria == '' || item2.Tercero.CuentaBancaria == null || item2.Tercero.CuentaBancaria == undefined) || (item2.NombreBanco == '' || item2.NombreBanco == null || item2.NombreBanco == undefined || item2.NombreBanco == '(NO APLICA)') || item2.NombreTipoCuenta == '(NO APLICA)' || item2.Tercero.Banco.Codigo == 0 || item2.Tercero.TipoBanco.Codigo == 0 ? true : false
                                                $scope.CuentasSeleccionados = true
                                                item2.Vehiculo =item.Codigo == 2604 || item.Codigo == 2613 ? PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Numero: item2.NumeroDocumento, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true }).Datos[0] == undefined ? { Codigo: 0 } : PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Numero: item2.NumeroDocumento, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true }).Datos[0].Vehiculo : LiquidacionesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Aprobado: -1, NumeroDocumento: item2.NumeroDocumento, Estado: -1, TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO }, Sync: true }).Datos[0] == undefined ? {Codigo:0} : $scope.CargarVehiculosPlaca(LiquidacionesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Aprobado: -1, NumeroDocumento: item2.NumeroDocumento, Estado: -1, TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO }, Sync: true }).Datos[0].PlacaVehiculo)
                                                $scope.CargarCuentasOrigen(item2)
                                                item2.CuentaOrigen = item2.ListadoCuentasOrigen[0]
                                                item2.Observaciones = (item2.Tercero.CuentaBancaria == 0 || item2.Tercero.CuentaBancaria == '' || item2.Tercero.CuentaBancaria == null || item2.Tercero.CuentaBancaria == undefined) || (item2.NombreBanco == '' || item2.NombreBanco == null || item2.NombreBanco == undefined) || item2.Tercero.Banco.Codigo == 0 || item2.Tercero.TipoBanco.Codigo == 0 ? item2.Observaciones + '. Inhabilitado para generar egreso ya que no se ha parametrizado Numero de cuenta, Banco ó Tipo Cuenta válidas al tercero.' : item2.Observaciones

                                            });
                                            $scope.totalRegistros = $scope.totalRegistros + response.data.Datos[0].TotalRegistros;
                                            $scope.totalPaginas = $scope.totalPaginas + Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                            $scope.Buscando = false;
                                            $scope.ResultadoSinRegistros = '';

                                        } else {
                                            if ($scope.ListaCXP.length == 0) {
                                                $scope.ListaCXP = [];
                                                $scope.totalRegistros = 0;
                                                $scope.totalPaginas = 0;
                                                $scope.paginaActual = 1;
                                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                                $scope.Buscando = false;
                                            }
                                        }
                                    }
                                    else {
                                        if ($scope.ListaCXP.length == 0) {
                                            $scope.ListaCXP = [];
                                            $scope.totalRegistros = 0;
                                            $scope.totalPaginas = 0;
                                            $scope.paginaActual = 1;
                                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                            $scope.Buscando = false;
                                        }
                                    }
                                }
                                $scope.CalcularTotales();
                            }, function (response) {
                            });


                    });


                }

            }
        }

        $scope.ConsultarCXPconEgresos = function () {
            $scope.ListaCXPconEgresos = [];
            $scope.totalRegistros = 0
            if (DatosRequeridosCXPconEgresos()) {

                if ($scope.Modelo.DocumentoOrigenEgresos.Codigo != 2600) {
                    var Filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                        Tercero: { Codigo: $scope.Modelo.TerceroEgresos == undefined ? 0 : $scope.Modelo.TerceroEgresos.Codigo },
                        DocumentoOrigen: { Codigo: $scope.Modelo.DocumentoOrigenEgresos.Codigo },
                        Oficina: { Codigo: $scope.Modelo.OficinaDestinoEgresos.Codigo },
                        FechaInicial: $scope.Modelo.FechaInicioEgresos,
                        FechaFinal: $scope.Modelo.FechaFinEgresos,
                        Pagina: $scope.paginaActualEgresos,
                        RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                    }
                    //Consulta Cuentas pendientes:

                    DocumentoComprobantesFactory.ConsultarCuentasconEgresos(Filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) {
                                        strCodigosCuentas = ''
                                        SumatoriaCreditos = 0
                                        $scope.ListaCXPconEgresos = response.data.Datos
                                        $scope.ListaCXPconEgresos.forEach(function (item) {
                                            SumatoriaCreditos += item.ValorPagoTotal
                                            item.NumeroDocumentoGrid = item.NumeroDocumentoGrid == undefined ? $scope.Modelo.DocumentoOrigenEgresos.Codigo == 2604 || $scope.Modelo.DocumentoOrigenEgresos.Codigo == 2613 ? 'A - ' + item.NumeroDocumentoOrigen : 'L - ' + item.NumeroDocumentoOrigen : item.NumeroDocumentoGrid
                                            strCodigosCuentas = item.MensajeProcesoTransferencia != null ? item.MensajeProcesoTransferencia.indexOf('OK') == -1 ? strCodigosCuentas + item.Codigo + ',' : strCodigosCuentas : strCodigosCuentas + item.Codigo + ','
                                            item.Tercero = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Tercero.Codigo, Sync: true }).Datos
                                            try { item.NombreBanco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + item.Banco.Codigo).Nombre } catch{ item.NombreBanco = '' }
                                            try { item.NombreTipoCuenta = $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo==' + item.TipoCuentaBancaria.Codigo).Nombre } catch{ item.NombreTipoCuenta = '' }
                                            try { item.NumeroCuentaBancaria = item.NumeroCuentaBancaria } catch{ item.NumeroCuentaBancaria = 0 }
                                            item.FechaPago = new Date(item.Fecha)
                                            item.GeneraPlano = item.MensajeProcesoTransferencia != null ? item.MensajeProcesoTransferencia.indexOf('OK') == -1 ? true : false : true
                                            item.DeshabilitarCheckPlano = item.MensajeProcesoTransferencia != null ? item.MensajeProcesoTransferencia.indexOf('OK') == -1 ? false : true : false
                                            $scope.EgresosSeleccionados = true
                                        });
                                        $scope.strCodigosCuentas = strCodigosCuentas
                                        $scope.totalRegistrosconEgresos = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginasconEgresos = Math.ceil($scope.totalRegistrosconEgresos / $scope.cantidadRegistrosPorPagina);
                                        $scope.BuscandoconEgresos = false;
                                        $scope.ResultadoSinRegistrosconEgresos = '';
                                        //$scope.MaskValores()
                                    } else {
                                        $scope.ListaCXPconEgresos = [];
                                        $scope.totalRegistrosconEgresos = 0;
                                        $scope.totalPaginasconEgresos = 0;
                                        $scope.paginaActualconEgresos = 1;
                                        $scope.ResultadoSinRegistrosconEgresos = 'No hay datos para mostrar';
                                        $scope.BuscandoconEgresos = false;
                                    }
                                }
                                else {
                                    $scope.ListaCXPconEgresos = [];
                                    $scope.totalRegistrosconEgresos = 0;
                                    $scope.totalPaginasconEgresos = 0;
                                    $scope.paginaActualconEgresos = 1;
                                    $scope.ResultadoSinRegistrosconEgresos = 'No hay datos para mostrar';
                                    $scope.BuscandoconEgresos = false;
                                }
                            }
                            $scope.CalcularTotales();
                        }, function (response) {
                        });
                } else {
                    $scope.ListadoDocumentosOrigen.forEach(item => {
                        var Filtros = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                            Tercero: { Codigo: $scope.Modelo.TerceroEgresos == undefined ? 0 : $scope.Modelo.TerceroEgresos.Codigo },
                            DocumentoOrigen: { Codigo: item.Codigo },
                            Oficina: { Codigo: $scope.Modelo.OficinaDestinoEgresos.Codigo },
                            FechaInicial: $scope.Modelo.FechaInicioEgresos,
                            FechaFinal: $scope.Modelo.FechaFinEgresos,
                            Pagina: $scope.paginaActualEgresos,
                            RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                        }

                        DocumentoComprobantesFactory.ConsultarCuentasconEgresos(Filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            strCodigosCuentas = ''
                                            SumatoriaCreditos = 0
                                            if ($scope.ListaCXPconEgresos.length > 0) {
                                                response.data.Datos.forEach(data => {
                                                    $scope.ListaCXPconEgresos.push(data)
                                                });
                                            } else {
                                                $scope.ListaCXPconEgresos = response.data.Datos
                                            }
                                            $scope.ListaCXPconEgresos.forEach(function (item2) {

                                                SumatoriaCreditos += item2.ValorPagoTotal
                                                item2.NumeroDocumentoGrid = item2.NumeroDocumentoGrid == undefined ? item.Codigo == 2604 || item.Codigo == 2613 ? 'A - ' + item2.NumeroDocumentoOrigen : 'L - ' + item2.NumeroDocumentoOrigen : item2.NumeroDocumentoGrid
                                                strCodigosCuentas = item2.MensajeProcesoTransferencia != null ? item2.MensajeProcesoTransferencia.indexOf('OK') == -1 ? strCodigosCuentas + item2.Codigo + ',' : strCodigosCuentas : strCodigosCuentas + item2.Codigo + ','
                                                item2.Tercero = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item2.Tercero.Codigo, Sync: true }).Datos
                                                try { item2.NombreBanco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + item2.Banco.Codigo).Nombre } catch{ item2.NombreBanco = '' }
                                                try { item2.NombreTipoCuenta = $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo==' + item2.TipoCuentaBancaria.Codigo).Nombre } catch{ item2.NombreTipoCuenta = '' }
                                                try { item2.NumeroCuentaBancaria = item2.NumeroCuentaBancaria } catch{ item2.NumeroCuentaBancaria = 0 }
                                                item2.FechaPago = new Date(item2.Fecha)
                                                item2.GeneraPlano = item2.MensajeProcesoTransferencia != null ? item2.MensajeProcesoTransferencia.indexOf('OK') == -1 ? true : false : true
                                                item2.DeshabilitarCheckPlano = item2.MensajeProcesoTransferencia != null ? item2.MensajeProcesoTransferencia.indexOf('OK') == -1 ? false : true : false
                                                $scope.EgresosSeleccionados = true

                                            });
                                            $scope.strCodigosCuentas = strCodigosCuentas
                                            $scope.totalRegistrosconEgresos = $scope.totalRegistrosconEgresos + response.data.Datos[0].TotalRegistros;
                                            $scope.totalPaginasconEgresos = $scope.totalPaginasconEgresos + Math.ceil($scope.totalRegistrosconEgresos / $scope.cantidadRegistrosPorPagina);
                                            $scope.BuscandoconEgresos = false;
                                            $scope.ResultadoSinRegistrosconEgresos = '';

                                        } else {
                                            if ($scope.ListaCXPconEgresos.length == 0) {
                                                $scope.ListaCXPconEgresos = [];
                                                $scope.totalRegistrosconEgresos = 0;
                                                $scope.totalPaginasconEgresos = 0;
                                                $scope.paginaActualconEgresos = 1;
                                                $scope.ResultadoSinRegistrosconEgresos = 'No hay datos para mostrar';
                                                $scope.BuscandoconEgresos = false;
                                            }
                                        }
                                    }
                                    else {
                                        if ($scope.ListaCXPconEgresos.length == 0) {
                                            $scope.ListaCXPconEgresos = [];
                                            $scope.totalRegistrosconEgresos = 0;
                                            $scope.totalPaginasconEgresos = 0;
                                            $scope.paginaActualconEgresos = 1;
                                            $scope.ResultadoSinRegistrosconEgresos = 'No hay datos para mostrar';
                                            $scope.BuscandoconEgresos = false;
                                        }
                                    }
                                }
                                $scope.CalcularTotales();
                            }, function (response) {
                            });


                    });


                }

            }
        }

        $scope.ObtenerTercero = function () {
            if ($scope.Modelo.Tercero != undefined) {
                if ($scope.Modelo.Tercero.Codigo > 0) {
                    $scope.Modelo.Tercero = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.Tercero.Codigo, Sync: true }).Datos;
                }
            }
        }
        $scope.ObtenerTerceroEgresos = function () {
            if ($scope.Modelo.TerceroEgresos != undefined) {
                if ($scope.Modelo.TerceroEgresos.Codigo > 0) {
                    $scope.Modelo.TerceroEgresos = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.TerceroEgresos.Codigo, Sync: true }).Datos;
                }
            }
        }

        $scope.CargarCuentasOrigen = function (item) {
            if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                item.ListadoCuentasOrigen = $scope.CargarCajas()
                //Calcular()
            }
            else if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                item.ListadoCuentasOrigen = CargarCuentasBancarias()
                //Calcular()
            }
        }

        $scope.CargarCajas = function () {
            $scope.ListadoCajasOficina = [];
            $scope.MensajesErrorcxp = [];


            $scope.ListadoCajasOficina = $linq.Enumerable().From($scope.ListadoTodaCajaOficinas).Where('$.Oficina.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo).ToArray();
            $scope.ListadoCajasOficina = $linq.Enumerable().From($scope.ListadoTodaCajaOficinas).Select(x => { x.NombreCuenta = x.Nombre; return x }).ToArray()
            if ($scope.ListadoCajasOficina == [] || $scope.ListadoCajasOficina == undefined || $scope.ListadoCajasOficina == null || $scope.ListadoCajasOficina.length == 0) {
                $scope.MensajesErrorcxp.push('La oficina actual no tiene ninguna caja asociada');
                return [];
            } else {
                return $scope.ListadoCajasOficina
            }


        }


        function CargarCuentasBancarias() {
            $scope.MensajesErrorcxp = [];
            /*Cargar el combo Cuentas Bancarias por oficina*/
            var ListaCuentas = CuentaBancariasFactory.CuentaBancariaOficinas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo, Sync: true }).Datos;
            if (ListaCuentas != undefined) {
                if (ListaCuentas.length != 0) {

                    $scope.ListadoCuentaBancariaOficinas = [];
                    $scope.ListadoCuentaBancariaOficinas = ListaCuentas;
                    $scope.ListadoCuentaBancarias = $scope.ListadoCuentaBancariaOficinas
                    return $scope.ListadoCuentaBancarias

                } else {
                    $scope.MensajesErrorcxp.push('La oficina actual no tiene ninguna cuenta bancaria asociada');
                    return [];
                }
            } else {
                $scope.MensajesErrorcxp.push('La oficina actual no tiene ninguna cuenta bancaria asociada');
                return [];
            }

        }

        $scope.GenerarEgresos = function () {
            var ListaSeleccionados = [];

            ListaSeleccionados = $linq.Enumerable().From($scope.ListaCXP).Where('$.GeneraEgreso==true').ToArray();
            if (ListaSeleccionados != undefined) {
                if (ListaSeleccionados.length > 0) {
                    if (DatosRequeridos(ListaSeleccionados)) {
                        ListaSeleccionados.forEach(item => {
                            item.MovimientosConcepto.forEach(itemDetalle => {
                                if (itemDetalle.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_DEBITO) {
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                                        itemDetalle.ValorDebito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar);
                                    }
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                                        itemDetalle.ValorDebito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar)
                                    }
                                    itemDetalle.ValorCredito = 0;
                                    if (itemDetalle.CuentaPUC.AplicarMedioPago > 0) {

                                        if (item.ActivarEfectivo) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaPUC
                                        }
                                        if (item.ActivarConsignacion) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaBancaria.CuentaPUC
                                        }

                                    } else {
                                        itemDetalle.CuentaPUC = itemDetalle.CuentaPUC;
                                    }
                                } else if (itemDetalle.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_CREDITO) {
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                                        itemDetalle.ValorCredito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar);
                                    }
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                                        itemDetalle.ValorCredito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar)
                                    }
                                    itemDetalle.ValorDebito = 0;
                                    if (itemDetalle.CuentaPUC.AplicarMedioPago > 0) {

                                        if (item.ActivarEfectivo) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaPUC
                                        }
                                        if (item.ActivarConsignacion) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaBancaria.CuentaPUC
                                        }

                                    } else {
                                        itemDetalle.CuentaPUC = itemDetalle.CuentaPUC;
                                    }
                                }
                                itemDetalle.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                                itemDetalle.Tercero = { Codigo: item.Tercero.Codigo };
                                itemDetalle.TerceroParametrizacion = { Codigo: 3100 }
                                itemDetalle.DocumentoCruce = { Codigo: 2800 }
                                itemDetalle.CentroCostoParametrizacion = { Codigo: 3200 }
                                itemDetalle.Diferencia = 0
                            });



                            var DetallesMovimientoGuardar = []
                            item.MovimientosConcepto.forEach(itemMovimiento => {
                                DetallesMovimientoGuardar.push({
                                    CodigoEmpresa: itemMovimiento.CodigoEmpresa,
                                    Numero: itemMovimiento.Numero,
                                    CuentaPUC: { Codigo: itemMovimiento.CuentaPUC.Codigo },
                                    Tercero: { Codigo: itemMovimiento.Tercero.Codigo },
                                    ValorBase: itemMovimiento.ValorBase,
                                    ValorDebito: itemMovimiento.ValorDebito,
                                    ValorCredito: itemMovimiento.ValorCredito,
                                    GeneraCuenta: MascaraNumero(itemMovimiento.GeneraCuenta),
                                    Observaciones: itemMovimiento.Observaciones,
                                    TerceroParametrizacion: { Codigo: itemMovimiento.TerceroParametrizacion.Codigo },
                                    CentroCostoParametrizacion: { Codigo: itemMovimiento.CentroCostoParametrizacion.Codigo },
                                    DocumentoCruce: { Codigo: itemMovimiento.DocumentoCruce.Codigo }
                                });
                            });

                            $scope.Modelo.Cuentas = [];

                            $scope.Modelo.Cuentas.push(
                                {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Codigo: item.Codigo,
                                    ValorPagar: MascaraNumero(item.ValorPagar),
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                }
                            )


                            $scope.ListaDetalleCuentas = [];
                            $scope.ListaDetalleCuentas.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoDocumentoCuenta: item.Codigo, ValorPago: MascaraNumero(item.ValorPagar) })


                            //Preparar Objeto Guardar:
                            var Entidad = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                CodigoAlterno: 0,
                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                                FormaPagoDocumento: { Codigo: item.MedioPago.Codigo },
                                DocumentoOrigen: { Codigo: item.DocumentoOrigen.Codigo },
                                NumeroDocumentoOrigen: item.NumeroDocumento,
                                Fecha: item.FechaPago,
                                Estado: item.NombreBanco.indexOf('BANCOLOMBIA') != -1 ? ESTADO_INACTIVO : ESTADO_ACTIVO,
                                ValorAlterno: 0,
                                ConceptoContable: item.ConceptoContable.Codigo,
                                Beneficiario: { Codigo: item.Tercero.Codigo },
                                Tercero: { Codigo: item.Tercero.Codigo },
                                Caja: { Codigo: item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO ? item.CuentaOrigen.Codigo : 0 },
                                ValorPagoEfectivo: item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO ? MascaraNumero(item.ValorPagar) : 0,
                                CuentaBancaria: { Codigo: item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO ? 0 : item.CuentaOrigen.CuentaBancaria.Codigo },
                                Vehiculo: { Codigo: item.Vehiculo.Codigo },
                                Observaciones: item.Observaciones,
                                FechaPagoRecaudo: new Date('01/01/1900'),
                                DestinoIngreso: { Codigo: item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO ? 4801 : 4802 }, //Codigo Destino ingreso Caja\Cuenta Bancaria
                                ValorPagoTotal: MascaraNumero(item.ValorPagar),
                                NumeroPagoRecaudo: 0,
                                ValorPagoTransferencia: item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO ? 0 : MascaraNumero(item.ValorPagar),
                                CuentaBancariaCheque: { Codigo: item.CuentaOrigen.CuentaBancaria == undefined ? 0 : item.CuentaOrigen.CuentaBancaria.Codigo },
                                CuentaBancariaConsignacion: { Codigo: item.CuentaOrigen.CuentaBancaria == undefined ? 0 : item.CuentaOrigen.CuentaBancaria.Codigo },
                                GeneraConsignacion: "",
                                Detalle: DetallesMovimientoGuardar,
                                DetalleCuentas: $scope.ListaDetalleCuentas,
                                Cuentas: $scope.Modelo.Cuentas,
                                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                OficinaDestino: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                Sync: true
                            }
                            //Fin Objeto Guardar

                            var Errores = 0
                            var ResponseGuardar = DocumentoComprobantesFactory.Guardar(Entidad);

                            if (ResponseGuardar != undefined) {
                                if (ResponseGuardar.ProcesoExitoso === true) {
                                    console.log('Se guardó el comprobante de egreso N.' + ResponseGuardar.Datos)
                                } else {
                                    Errores++;
                                    console.log('No se pudo guardar el comprobante, consulte con el administrador del sistema :' + ResponseGuardar.MensajeOperacion);
                                }
                            } else {
                                Errores++;
                                console.log('Error al guardar, consulte con el administrador del sistema');
                            }
                            if (Errores > 0) {
                                ShowError('Algunos detalles no se guardaron correctamente, verifique la información de los detalles')
                            } else {
                                $scope.ConsultarCXP()
                                ShowSuccess('Se generaron ' + ListaSeleccionados.length + ' comprobantes de egreso')
                            }

                        });
                    }
                } else {
                    ShowError('Debe seleccionar al menos una cuenta por pagar')
                }
            } else {
                ShowError('Debe seleccionar al menos una cuenta por pagar')
            }
        }

        function DatosRequeridos(ListaCuentas) {
            var continuar = true
            $scope.MensajesErrorcxp = [];
            ListaCuentas.forEach(item => {
                if (MascaraNumero(item.ValorPagar) == 0 || MascaraNumero(item.ValorPagar) == undefined || MascaraNumero(item.ValorPagar) == null) {
                    continuar = false
                }
                if (item.MedioPago == undefined || item.MedioPago == null || item.MedioPago == '') {
                    continuar = false
                } else if (item.MedioPago.Codigo == undefined || item.MedioPago.Codigo == null || item.MedioPago.Codigo == '' || item.MedioPago.Codigo == 0) {
                    continuar = false
                }
                if (item.CuentaOrigen == undefined || item.CuentaOrigen == null || item.CuentaOrigen == '') {
                    continuar = false
                } else if (item.CuentaOrigen.Codigo == undefined || item.CuentaOrigen.Codigo == null || item.CuentaOrigen.Codigo == '' || item.CuentaOrigen.Codigo == 0) {

                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                        if (item.CuentaOrigen.CuentaBancaria.Codigo == undefined || item.CuentaOrigen.CuentaBancaria.Codigo == null || item.CuentaOrigen.CuentaBancaria.Codigo == '' || item.CuentaOrigen.CuentaBancaria.Codigo == 0) {
                            continuar = false
                        }
                    } else {
                        if (item.CuentaOrigen.Codigo == undefined || item.CuentaOrigen.Codigo == null || item.CuentaOrigen.Codigo == '' || item.CuentaOrigen.Codigo == 0) {
                            continuar = false
                        }
                    }
                }
            });

            if (!continuar) {
                $scope.MensajesErrorcxp.push('Debe llenar toda la información de los items seleccionados')
            }

            return continuar
        }

        $scope.GenerarPagoUnificado = function () {
            var ListaSeleccionados = [];
            var TotalPagarCuentas = 0
            var FormaPago = 0
            var DocumentoOrigen = 0
            var ConceptoContable = 0
            var Tercero = 0
            var CuentaOrigen = 0
            var CuentaBancaria = 0
            var Vehiculo = 0
            var TotalMovimientoDebito = 0
            var TotalMovimientoCredito = 0
            var CuentaPUCMovimientos = 0
            var MovimientosConcepto = []
            var strDocumentos = ''
            var NombreDocumentos = $scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2613 ? 'Anticipos' : 'Liquidaciones'
            $scope.Modelo.Cuentas = [];
            $scope.ListaDetalleCuentas = [];
            ListaSeleccionados = $linq.Enumerable().From($scope.ListaCXP).Where('$.GeneraEgreso==true').ToArray();
            if (ListaSeleccionados != undefined) {
                if (ListaSeleccionados.length > 0) {
                    if (DatosRequeridos(ListaSeleccionados)) {
                        ListaSeleccionados.forEach(item => {
                            strDocumentos = strDocumentos + item.NumeroDocumento +','
                            item.MovimientosConcepto.forEach(itemDetalle => {
                                if (itemDetalle.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_DEBITO) {
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                                        itemDetalle.ValorDebito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar);
                                        TotalMovimientoDebito += MascaraNumero(item.ValorPagar);
                                    }
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                                        itemDetalle.ValorDebito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar)
                                        TotalMovimientoDebito += MascaraNumero(item.ValorPagar);
                                    }
                                    itemDetalle.ValorCredito = 0;
                                    if (itemDetalle.CuentaPUC.AplicarMedioPago > 0) {

                                        if (item.ActivarEfectivo) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaPUC
                                        }
                                        if (item.ActivarConsignacion) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaBancaria.CuentaPUC
                                        }

                                    } else {
                                        itemDetalle.CuentaPUC = itemDetalle.CuentaPUC;
                                    }
                                } else if (itemDetalle.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_CREDITO) {
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                                        itemDetalle.ValorCredito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar);
                                        TotalMovimientoCredito += MascaraNumero(item.ValorPagar);
                                    }
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                                        itemDetalle.ValorCredito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar)
                                        TotalMovimientoCredito += MascaraNumero(item.ValorPagar);
                                    }
                                    itemDetalle.ValorDebito = 0;
                                    if (itemDetalle.CuentaPUC.AplicarMedioPago > 0) {

                                        if (item.ActivarEfectivo) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaPUC
                                        }
                                        if (item.ActivarConsignacion) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaBancaria.CuentaPUC
                                        }

                                    } else {
                                        itemDetalle.CuentaPUC = itemDetalle.CuentaPUC;
                                    }
                                }
                                itemDetalle.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                                itemDetalle.Tercero = { Codigo: item.Tercero.Codigo };
                                itemDetalle.TerceroParametrizacion = { Codigo: 3100 }
                                itemDetalle.DocumentoCruce = { Codigo: 2800 }
                                itemDetalle.CentroCostoParametrizacion = { Codigo: 3200 }
                                itemDetalle.Diferencia = 0
                            });



                           

                            

                            $scope.Modelo.Cuentas.push(
                                {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Codigo: item.Codigo,
                                    ValorPagar: MascaraNumero(item.ValorPagar),
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                }
                            )


                           
                            $scope.ListaDetalleCuentas.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoDocumentoCuenta: item.Codigo, ValorPago: MascaraNumero(item.ValorPagar) })

                            TotalPagarCuentas += MascaraNumero(item.ValorPagar)
                            FormaPago = item.MedioPago.Codigo
                            DocumentoOrigen = item.DocumentoOrigen.Codigo
                            ConceptoContable = item.ConceptoContable.Codigo
                            Tercero = item.Tercero.Codigo
                            CuentaOrigen = item.CuentaOrigen.Codigo
                            CuentaBancaria = item.CuentaOrigen.CuentaBancaria.Codigo
                            Vehiculo = item.Vehiculo.Codigo 
                            MovimientosConcepto = item.MovimientosConcepto
                        });

                        //Preparar Objeto Guardar:

                        var DetallesMovimientoGuardar = []
                        MovimientosConcepto.forEach(itemMovimiento => {
                            DetallesMovimientoGuardar.push({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Numero: itemMovimiento.Numero,
                                CuentaPUC: { Codigo: itemMovimiento.CuentaPUC.Codigo },
                                Tercero: { Codigo: Tercero },
                                ValorBase: TotalMovimientoDebito,
                                ValorDebito: itemMovimiento.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_DEBITO ? TotalMovimientoDebito : 0,
                                ValorCredito: itemMovimiento.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_DEBITO ? 0 : TotalMovimientoCredito,
                                GeneraCuenta: MascaraNumero(itemMovimiento.GeneraCuenta),
                                Observaciones: itemMovimiento.Observaciones,
                                TerceroParametrizacion: { Codigo: itemMovimiento.TerceroParametrizacion.Codigo },
                                CentroCostoParametrizacion: { Codigo: itemMovimiento.CentroCostoParametrizacion.Codigo },
                                DocumentoCruce: { Codigo: itemMovimiento.DocumentoCruce.Codigo }
                            });
                        });


                        var Entidad = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            CodigoAlterno: 0,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                            FormaPagoDocumento: { Codigo: FormaPago },
                            DocumentoOrigen: { Codigo: DocumentoOrigen },
                            NumeroDocumentoOrigen: 0,
                            Fecha: new Date(),
                            Estado: ESTADO_INACTIVO,
                            ValorAlterno: 0,
                            ConceptoContable: ConceptoContable,
                            Beneficiario: { Codigo: Tercero },
                            Tercero: { Codigo: Tercero},
                            Caja: { Codigo: FormaPago == CODIGO_MEDIO_PAGO_EFECTIVO ? CuentaOrigen : 0 },
                            ValorPagoEfectivo: 0,
                            CuentaBancaria: { Codigo: FormaPago == CODIGO_MEDIO_PAGO_EFECTIVO ? 0 : CuentaBancaria },
                            Vehiculo: { Codigo: Vehiculo },
                            Observaciones: 'Pago Unificado Cuentas por Pagar ' + NombreDocumentos+' : ' + strDocumentos.substring(0, strDocumentos.length - 1),
                            FechaPagoRecaudo: new Date('01/01/1900'),
                            DestinoIngreso: { Codigo: FormaPago == CODIGO_MEDIO_PAGO_EFECTIVO ? 4801 : 4802 }, //Codigo Destino ingreso Caja\Cuenta Bancaria
                            ValorPagoTotal: MascaraNumero(TotalPagarCuentas),
                            NumeroPagoRecaudo: 0,
                            ValorPagoTransferencia: MascaraNumero(TotalPagarCuentas),
                            CuentaBancariaCheque: { Codigo: CuentaBancaria == undefined ? 0 : CuentaBancaria },
                            CuentaBancariaConsignacion: { Codigo: CuentaBancaria == undefined ? 0 : CuentaBancaria },
                            GeneraConsignacion: "",
                            Detalle: DetallesMovimientoGuardar,
                            DetalleCuentas: $scope.ListaDetalleCuentas,
                            Cuentas: $scope.Modelo.Cuentas,
                            Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                            OficinaDestino: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            Sync: true
                        }
                        //Fin Objeto Guardar

                        var Errores = 0
                        var ResponseGuardar = DocumentoComprobantesFactory.Guardar(Entidad);

                        if (ResponseGuardar != undefined) {
                            if (ResponseGuardar.ProcesoExitoso === true) {
                                console.log('Se guardó el comprobante de egreso N.' + ResponseGuardar.Datos)
                            } else {
                                Errores++;
                                console.log('No se pudo guardar el comprobante, consulte con el administrador del sistema :' + ResponseGuardar.MensajeOperacion);
                            }
                        } else {
                            Errores++;
                            console.log('Error al guardar, consulte con el administrador del sistema');
                        }
                        if (Errores > 0) {
                            ShowError('Algunos detalles no se guardaron correctamente, verifique la información de los detalles')
                        } else {
                            $scope.ConsultarCXP()
                            ShowSuccess('Se generó el comprobante de egreso N.' + ResponseGuardar.Datos)
                        }

                    }
                } else {
                    ShowError('Debe seleccionar al menos una cuenta por pagar')
                }
            } else {
                ShowError('Debe seleccionar al menos una cuenta por pagar')
            }
        }

        $scope.MarcarEgresos = function () {
            $scope.ListaCXP = $linq.Enumerable().From($scope.ListaCXP).Select(function (x) {
                x.GeneraEgreso = x.DeshabilitarCheckEgreso == true ? false : $scope.CuentasSeleccionados
                return x
            }).ToArray()

            $scope.CalcularTotales();
        }

        $scope.MarcarPlanos = function () {
            strCodigosCuentas = ''
            SumatoriaCreditos = 0
            $scope.ListaCXPconEgresos = $linq.Enumerable().From($scope.ListaCXPconEgresos).Select(function (x) {
                x.GeneraPlano = x.DeshabilitarCheckPlano == true ? false : $scope.EgresosSeleccionados
                strCodigosCuentas = x.GeneraPlano == true ? strCodigosCuentas + x.Codigo + ',' : strCodigosCuentas
                SumatoriaCreditos += x.ValorPagoTotal
                return x
            }).ToArray()
            $scope.strCodigosCuentas = strCodigosCuentas
        }

        $scope.AgregarCuentaArchivoPlano = function (item) {

            strCodigosCuentas = ''
            SumatoriaCreditos = 0
            $scope.ListaCXPconEgresos.forEach(item1 => {
                if (item1.GeneraPlano == true) {
                    strCodigosCuentas = strCodigosCuentas + item1.Codigo + ',';
                    SumatoriaCreditos += item1.ValorPagoTotal
                }
            })
            $scope.strCodigosCuentas = strCodigosCuentas
        }


        $scope.CalcularTotales = function () {
            var TotalPendiente = 0
            var TotalTraslado = 0

            $scope.ListaCXP.forEach(item => {
                if (item.GeneraEgreso) {
                    TotalTraslado = TotalTraslado + MascaraNumero(item.ValorPagar)
                } else {
                    TotalPendiente = TotalPendiente + MascaraNumero(item.ValorPagar)
                }
                $scope.Modelo.TotalPendiente = TotalPendiente
                $scope.Modelo.TotalTraslado = TotalTraslado
            });
        }


        $scope.ValidarSecuenciaLotes = function () {
            var Secuencia = 0
            Secuencia = DocumentoComprobantesFactory.ValidarSecuenciaLotes({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Fecha: new Date(), Sync: true }).Datos
            if (Secuencia == 52) {
                ShowError('Ya se ha alcanzado el número máximo de lotes por día');
            } else {
                $scope.Generar();
            }
        }

        $scope.Generar = function () {
            blockUI.start('Generando Archivo ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Generando = true;
            $scope.MensajesError = [];
            $scope.ListadoComprobantes = [];
            $scope.pref = EmpresasFactory.Consultar({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos[0].Prefijo
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            var fechainicial = $scope.Modelo.FechaInicioEgresos != undefined ? Formatear_Fecha_Mes_Dia_Ano($scope.Modelo.FechaInicioEgresos) : '';
            var fechafinal = $scope.Modelo.FechaFinEgresos != undefined ? Formatear_Fecha_Mes_Dia_Ano($scope.Modelo.FechaFinEgresos) : '';
            var OficinasCodigo = $scope.Modelo.OficinaDestinoEgresos.Codigo;
            var CodigoUsuario = $scope.Sesion.UsuarioAutenticado.Codigo;
            var CodigoTercero = $scope.Modelo.TerceroEgresos == undefined || $scope.Modelo.TerceroEgresos == '' ? '' : $scope.Modelo.TerceroEgresos.Codigo;
            var DocumentoOrigen = $scope.Modelo.DocumentoOrigenEgresos.Codigo
            var strCodigosComprobantes = strCodigosCuentas.substr(0, strCodigosCuentas.length - 1)
            window.open($scope.urlASP + '/InterfazBancos/GenerarinterfazBancos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&FechaExportar=' + moment(new Date()).format('YYYY-MM-DD HH:mm:ss').replace('T', ' ') + '&UsuarioExporta=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&strCodigosComprobantes=' + strCodigosComprobantes + '&Documento_Origen=' + DocumentoOrigen + '&SumatoriaCreditos=' + SumatoriaCreditos + '&Tercero=' + CodigoTercero + '&FechInic=' + fechainicial + '&FechFina=' + fechafinal + '&CodigoUsuario=' + CodigoUsuario + '&Oficina=' + OficinasCodigo + '&Prefijo=' + $scope.pref);

            blockUI.stop();
        };

        $scope.NuevoProceso = function (e) {
            var fileVal = document.getElementById("fileSecundario");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
        }



        $scope.DataArchivo = [];
        $scope.ListaErroneos = [];
        $scope.listaCorrectos = []
        //$scope.ListaTransacciones = [];
        var arrayRegistros = []
        $scope.handleFile = function (e) {
            $scope.DataArchivo = [];

            var archivo = e.files[0];
            if (!archivo) {
                return;
            } else {
                var nombreArchivo = archivo.name
                var extension = nombreArchivo.split('.').slice(-1);
                extension = extension[0]
                var extensiones = ['txt'];
                if (extensiones.indexOf(extension) === -1) {
                    ShowError('El tipo de archivo no es permitido, debe ser un archivo plano (.txt)');
                    return
                }
            }
            $scope.ListaErroneos = [];
            $scope.listaCorrectos = []
            $scope.ListaTransacciones = [];

          
            var lector = new FileReader();
            arrayRegistros = []
            lector.readAsText(archivo);
            lector.onload = function (e) {
                var contenido = e.target.result;
                console.log(contenido);
                arrayRegistros = contenido.split("\n")
                console.log("arrayRegistros : " + arrayRegistros);
                if (arrayRegistros.length > 0) {
                    CargarDatos();
                }
               
            };
            


        }

        function CargarDatos() {
            $scope.$apply(function () {
                var patronSecuenciaPago = /[^ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/
                var CadenaComprobantes = ''
                var ErroresPlano = 0
                $scope.conErrores = false
                arrayRegistros.forEach(item => {
                    if (item.trim() != "") {
                        if (item.trim().length == 144) {
                            try {
                                item = {
                                    registroOriginal: item
                                }


                                $scope.DataArchivo.push(item)
                            } catch (e) {
                                ShowError(e)
                                return
                            }

                            item.Valido = true

                            try {
                                item.IdentificacionEmpresa = MascaraNumero(item.registroOriginal.substring(0, 13))
                            } catch (e) {
                                ErroresPlano++;
                                item.stIdentificacionEmpresa = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'la estructura del campo "Nit Empresa" no corresponde a la indicada en el formato SAP'
                                item.Valido = false
                            }
                            try {
                                item.FechaTransmisionPagos = MascaraNumero(item.registroOriginal.substring(13, 21))
                                item.FechaTransmisionPagosOriginal = item.registroOriginal.substring(13, 21)
                                item.FechaTransmisionPagosGrid = moment(new Date(item.registroOriginal.substring(13, 21)[2] + item.registroOriginal.substring(13, 21)[3] + '-' + item.registroOriginal.substring(13, 21)[0] + item.registroOriginal.substring(13, 21)[1] + '-' + item.registroOriginal.substring(13, 21)[4] + item.registroOriginal.substring(13, 21)[5] + item.registroOriginal.substring(13, 21)[6] + item.registroOriginal.substring(13, 21)[7])).format('DD/MM/YYYY')

                            } catch (e) {
                                ErroresPlano++;
                                item.stFechaTransmisionPagos = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'la estructura del campo "Fecha Transmisión Pagos" no corresponde a la indicada en el formato SAP'
                                item.Valido = false
                            }

                            if (!patronSecuenciaPago.test(item.registroOriginal.substring(21, 22))) {
                                item.SecuenciaPago = item.registroOriginal.substring(21, 22)
                            } else {
                                item.SecuenciaPago = item.registroOriginal.substring(21, 22)
                                ErroresPlano++;
                                item.stSecuenciaPago = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'El campo "Secuencia Pago" sólo debe incluir letras de la a -z ó A_- Z sin incluir la letras ñ ó Ñ'
                                item.Valido = false
                            }
                            try {
                                item.TipoRegistro = MascaraNumero(item.registroOriginal.substring(22, 23))
                                item.TipoRegistroOriginal = item.registroOriginal.substring(22, 23)
                                if (item.TipoRegistro != 6) {
                                    ErroresPlano++;
                                    item.stTipoRegistro = 'background : red';
                                    item.estilo = 'background : yellow'
                                    item.msGeneral = 'El contenido del campo "Tipo Registro" no corresponde al indicado en el formato SAP'
                                    item.Valido = false
                                }
                            } catch (e) {
                                ErroresPlano++;
                                item.stTipoRegistro = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'la estructura del campo "Tipo Registro" no corresponde a la indicada en el formato SAP'
                                item.Valido = false
                            }
                            try {
                                item.NitBeneficiario = MascaraNumero(item.registroOriginal.substring(23, 38))
                                item.NitBeneficiarioOriginal = item.registroOriginal.substring(23, 38)
                                item.NombreBeneficiario = item.registroOriginal.substring(38, 56)
                            } catch (e) {
                                ErroresPlano++;
                                item.stNitBeneficiario = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'la estructura del campo "Nit Beneficiario" no corresponde a la indicada en el formato SAP'
                                item.Valido = false
                            }
                            try {
                                item.CodigoBanco = MascaraNumero(item.registroOriginal.substring(56, 65))
                                item.CodigoBancoOriginal = item.registroOriginal.substring(56, 65)
                            } catch (e) {
                                ErroresPlano++;
                                item.stCodigoBanco = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'la estructura del campo "Codigo Banco" no corresponde a la indicada en el formato SAP'
                                item.Valido = false
                            }
                            try {
                                item.CuentaBeneficiario = MascaraNumero(item.registroOriginal.substring(65, 82))
                                item.CuentaBeneficiarioOriginal = item.registroOriginal.substring(65, 82)
                            } catch (e) {
                                ErroresPlano++;
                                item.stCuentaBeneficiario = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'la estructura del campo "Cuenta Beneficiario" no corresponde a la indicada en el formato SAP'
                                item.Valido = false
                            }

                            item.TipoCuentaLocal = item.registroOriginal.substring(82, 83)
                            if (item.TipoCuentaLocal != 'S') {
                                ErroresPlano++;
                                item.stTipoCuentaLocal = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'El contenido del campo "Tipo Cuenta Local" no corresponde al indicado en el formato SAP'
                                item.Valido = false
                            }
                            try {
                                item.TipoTransaccion = MascaraNumero(item.registroOriginal.substring(83, 85))
                                item.TipoTransaccionOriginal = item.registroOriginal.substring(83, 85)
                            } catch (e) {
                                ErroresPlano++;
                                item.stTipoTransaccion = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'La estructura campo "Tipo Transaccion" no corresponde a la indicada en el formato SAP'
                                item.Valido = false
                            }
                            try {
                                item.Valor = parseFloat(item.registroOriginal.substring(85, 103)).toFixed(2)
                                item.ValorOriginal = item.registroOriginal.substring(85, 103)
                            } catch (e) {
                                ErroresPlano++;
                                item.stValor = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'La estructura campo "Valor" no corresponde a la indicada en el formato SAP'
                                item.Valido = false
                            }

                            item.Concepto = item.registroOriginal.substring(103, 112)
                            try {
                                item.ComprobanteRegistro = MascaraNumero(item.registroOriginal.substring(103, 110))
                                CadenaComprobantes = CadenaComprobantes + item.registroOriginal.substring(103, 110) + ','
                            } catch (e) {
                                ErroresPlano++;
                                item.stConcepto = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'El documento del campo "Concepto" no tiene la estructura adecuada'
                                item.Valido = false
                            }

                            item.Referencia = item.registroOriginal.substring(112, 125)
                            item.CodigoRespuesta = item.registroOriginal.substring(125, 128).trim()

                            try {
                                var CodigoRespuesta = $linq.Enumerable().From($scope.ListaCodigosRespuesta).First('$.CodigoRespuesta=="' + item.CodigoRespuesta + '"');
                                console.log(CodigoRespuesta)
                            } catch (e) {
                                ErroresPlano++;
                                item.stCodigoRespuesta = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'El código de respuesta no es válido'
                                item.Valido = false
                                console.log('Error código respuesta documento ' + item.ComprobanteRegistro + ' : ' + e)
                            }


                            try {
                                item.NumeroCheque = MascaraNumero(item.registroOriginal.substring(128, 136))
                                item.NumeroChequeOriginal = item.registroOriginal.substring(128, 136)
                            } catch (e) {
                                ErroresPlano++;
                                item.stNumeroCheque = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'La estructura campo "Numero Cheque" no corresponde a la indicada en el formato SAP'
                                item.Valido = false
                            }

                            try {
                                item.FechaAplicacion = MascaraNumero(item.registroOriginal.substring(136, 144))
                                item.FechaAplicacionOriginal = item.registroOriginal.substring(136, 144)
                                item.FechaAplicacionGrid = moment(new Date(item.registroOriginal.substring(136, 144)[2] + item.registroOriginal.substring(136, 144)[3] + '-' + item.registroOriginal.substring(136, 144)[0] + item.registroOriginal.substring(136, 144)[1] + '-' + item.registroOriginal.substring(136, 144)[4] + item.registroOriginal.substring(136, 144)[5] + item.registroOriginal.substring(136, 144)[6] + item.registroOriginal.substring(136, 144)[7])).format('DD/MM/YYYY')
                            } catch (e) {
                                ErroresPlano++;
                                item.stFechaAplicacion = 'background : red';
                                item.estilo = 'background : yellow'
                                item.msGeneral = 'La estructura campo "Fecha Aplicacion" no corresponde a la indicada en el formato SAP'
                                item.Valido = false
                            }



                        } else {
                            item = {
                                registroOriginal: item
                            }
                            item.msGeneral = 'El registro no tiene la estructura adecuada'
                            item.Valido = false
                            item.estilo = 'background:yellow'
                            $scope.ListaErroneos.push(item)
                        }
                        $scope.ListaTransacciones.push(item)
                    }
                })
                var ListaCadenaEgresos = DocumentoComprobantesFactory.ConsultarCadenaEgresos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaEgresos: CadenaComprobantes, Sync: true }).Datos;
                if (ListaCadenaEgresos != undefined) {
                    $scope.ListaTransacciones.forEach(itemGrid => {
                        if (itemGrid.ComprobanteRegistro != undefined) {
                            try {
                                var RegistroComprobante = $linq.Enumerable().From(ListaCadenaEgresos).First('$.Numero==' + itemGrid.ComprobanteRegistro)
                                if (RegistroComprobante.MensajeProcesoTransferencia != null) {
                                    if (RegistroComprobante.MensajeProcesoTransferencia.indexOf('OK', 0) > -1) {
                                        ErroresPlano++;
                                        itemGrid.stConcepto = 'background : red';
                                        itemGrid.estilo = 'background : yellow'
                                        itemGrid.msGeneral = 'El comprobante ya tiene un proceso exitoso'
                                        itemGrid.Valido = false
                                    }
                                }
                            } catch (e) {
                                ErroresPlano++;
                                itemGrid.stConcepto = 'background : red';
                                itemGrid.estilo = 'background : yellow'
                                itemGrid.msGeneral = 'El documento de comprobante en el registro, no se encuentra en el sistema'
                                itemGrid.Valido = false
                                console.log('error validacon documento ' + itemGrid.ComprobanteRegistro + ' :' + e)
                            }
                        }
                    });
                }

                if ($scope.ListaErroneos.length > 0) {
                    $scope.conErrores = true
                    $scope.BotonGuardar = 'Actualizar válidos'
                } else {
                    $scope.BotonGuardar = 'Actualizar'
                }

                $scope.ListaTransacciones.forEach(item => {
                    if (item.Valido == true) {
                        $scope.listaCorrectos.push(item)
                    }
                });
                //if ($scope.ListaErroneos.length > 0) {
                //    ShowError('La estructura de algunos registros no corresponde a un archivo de respuesta de Bancolombia')
                //} else {
                //    showModal('modalActualizarEgresos');
                //}
                showModal('modalActualizarEgresos');
                console.log('ListaTransacciones : ' + $scope.ListaTransacciones)
                //console.log(JSON.stringify($scope.DataArchivo));
            })
        }

        $scope.ActualizarEgresos = function () {
            var EgresosActualizar = []

            //$scope.DataArchivo.forEach(item => {
            //    var strFechaRecaudo = item.FechaAplicacion.toString().length == 8 ? item.FechaAplicacion.toString()[0] + item.FechaAplicacion.toString()[1] + '-' + item.FechaAplicacion.toString()[2] + item.FechaAplicacion.toString()[3] + '-' + item.FechaAplicacion.toString()[4] + item.FechaAplicacion.toString()[5] + item.FechaAplicacion.toString()[6] + item.FechaAplicacion.toString()[7] : item.FechaAplicacion.toString()[0] + '-' + item.FechaAplicacion.toString()[1] + item.FechaAplicacion.toString()[2] + '-' + item.FechaAplicacion.toString()[3] + item.FechaAplicacion.toString()[4] + item.FechaAplicacion.toString()[5] + item.FechaAplicacion.toString()[6]
            //    EgresosActualizar.push({
            //        Numero: item.Concepto.indexOf('-') != -1 ? item.Concepto.split('-')[0] : item.Concepto.substring(0, 7),
            //        DocumentoOrigen: item.Referencia.indexOf('P') != -1 ? { Codigo: 2604 } : { Codigo: 2602 },
            //        FechaPagoRecaudo: new Date(strFechaRecaudo),
            //        ValorPagoTransferencia: item.Valor,
            //        MensajeProcesoTransferencia: item.CodigoRespuesta

            //    })
            //})
            $scope.listaCorrectos.forEach(item => {
                var strFechaRecaudo = item.FechaAplicacion.toString().length == 8 ? item.FechaAplicacion.toString()[2] + item.FechaAplicacion.toString()[3] + '-' + item.FechaAplicacion.toString()[0] + item.FechaAplicacion.toString()[1] + '-' + item.FechaAplicacion.toString()[4] + item.FechaAplicacion.toString()[5] + item.FechaAplicacion.toString()[6] + item.FechaAplicacion.toString()[7] : item.FechaAplicacion.toString()[2] + '-' + item.FechaAplicacion.toString()[3] + item.FechaAplicacion.toString()[0] + '-' + item.FechaAplicacion.toString()[1] + item.FechaAplicacion.toString()[4] + item.FechaAplicacion.toString()[5] + item.FechaAplicacion.toString()[6]
                EgresosActualizar.push({
                    Numero: item.Concepto.indexOf('-') != -1 ? item.Concepto.split('-')[0] : item.Concepto.substring(0, 7),
                    DocumentoOrigen: item.Referencia.indexOf('P') != -1 ? { Codigo: 2604 } : { Codigo: 2602 },
                    FechaPagoRecaudo: new Date(strFechaRecaudo),
                    NumeroPagoRecaudo : item.NumeroCheque,
                    ValorPagoTransferencia: item.Valor,
                    MensajeProcesoTransferencia: item.CodigoRespuesta

                })
            })

            var Entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Comprobantes: EgresosActualizar,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            }

            DocumentoComprobantesFactory.ActualizarEgresosArchivoRespuesta(Entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        closeModal('modalActualizarEgresos')
                        ShowSuccess('Se actualizaron ' + response.data.Datos + ' Egresos')
                        $scope.ConsultarCXPconEgresos();
                    } else {
                        closeModal('modalActualizarEgresos')
                        ShowError('Error al actualizar : ' + response.data.MensajeOperacion)
                    }
                }), function (response) {
                    closeModal('modalActualizarEgresos')
                    showError(response.statusText)
                }
        }


        //Máscaras:
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        }
    }
]);