﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Repositorio.Facturacion

Namespace Facturacion
    Public NotInheritable Class PersistenciaNotasFacturas
        Implements IPersistenciaBase(Of NotasFacturas)

        Public Function Consultar(filtro As NotasFacturas) As IEnumerable(Of NotasFacturas) Implements IPersistenciaBase(Of NotasFacturas).Consultar
            Return New RepositorioNotasFacturas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As NotasFacturas) As Long Implements IPersistenciaBase(Of NotasFacturas).Insertar
            Return New RepositorioNotasFacturas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As NotasFacturas) As Long Implements IPersistenciaBase(Of NotasFacturas).Modificar
            Return New RepositorioNotasFacturas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As NotasFacturas) As NotasFacturas Implements IPersistenciaBase(Of NotasFacturas).Obtener
            Return New RepositorioNotasFacturas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As NotasFacturas) As Boolean
            Return New RepositorioNotasFacturas().Anular(entidad)
        End Function

        Public Function ObtenerDatosNotaElectronicaSaphety(filtro As NotasFacturas) As NotasFacturas
            Return New RepositorioNotasFacturas().ObtenerDatosNotaElectronicaSaphety(filtro)
        End Function
        Public Function GuardarNotaElectronica(entidad As NotasFacturas) As Long
            Return New RepositorioNotasFacturas().GuardarNotaElectronica(entidad)
        End Function
        Public Function ObtenerDocumentoReporteSaphety(filtro As NotasFacturas) As NotasFacturas
            Return New RepositorioNotasFacturas().ObtenerDocumentoReporteSaphety(filtro)
        End Function
    End Class
End Namespace