﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Fachada.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class LogicaLegalizarRecaudoRemesas
        Inherits LogicaBase(Of LegalizarRecaudoRemesas)

        ReadOnly _persistencia As IPersistenciaBase(Of LegalizarRecaudoRemesas)

        Sub New(persistencia As IPersistenciaBase(Of LegalizarRecaudoRemesas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As LegalizarRecaudoRemesas) As Respuesta(Of IEnumerable(Of LegalizarRecaudoRemesas))
            Dim consulta = _persistencia.Consultar(filtro)
            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of LegalizarRecaudoRemesas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of LegalizarRecaudoRemesas))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As LegalizarRecaudoRemesas) As Respuesta(Of LegalizarRecaudoRemesas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of LegalizarRecaudoRemesas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of LegalizarRecaudoRemesas)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As LegalizarRecaudoRemesas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            If valor <= 0 Then
                Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = entidad.MensajeSQL}
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Private Function DatosRequeridos(entidad As LegalizarRecaudoRemesas) As Respuesta(Of LegalizarRecaudoRemesas)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of LegalizarRecaudoRemesas) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of LegalizarRecaudoRemesas) With {.ProcesoExitoso = True}

        End Function

        Public Function EliminarGuia(filtro As DetalleLegalizarRecaudoRemesas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim BorrarGuia As Boolean = False

            BorrarGuia = CType(_persistencia, PersistenciaLegalizarRecaudoRemesas).EliminarGuia(filtro)

            If Not BorrarGuia Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, filtro.Numero, Recursos.OperacionAnulo)}
        End Function
    End Class
End Namespace