﻿Print 'CREATE PROCEDURE gsp_consultar_encabezado_Notas_Credito'
GO
DROP PROCEDURE gsp_consultar_encabezado_Notas_Credito
GO

CREATE PROCEDURE gsp_consultar_encabezado_Notas_Credito (        
	@par_EMPR_Codigo  smallint,        
	@par_Numero    numeric = NULL,        
	@par_Numero_Documento numeric = NULL,
	@par_Fecha_Inicial  date = NULL,        
	@par_Fecha_Final  date = NULL,        
	@par_TERC_Cliente  numeric = NULL,  
	@par_TERC_Facturar  numeric = NULL,       
	@par_Estado    smallint = NULL,        
	@par_Anulado   smallint = NULL,        
	@par_OFIC_Nota  smallint = NULL,        
	@par_NumeroPagina INT = NULL,        
	@par_RegistrosPagina INT = NULL        
)        
AS        
BEGIN        

	DECLARE @CantidadRegistros NUMERIC  
	SELECT @CantidadRegistros = (        
		SELECT DISTINCT COUNT(1)         
		FROM Encabezado_Notas_Facturas ENNF     

		INNER JOIN 
		Empresas AS EMPR ON
		ENNF.EMPR_Codigo = EMPR.Codigo
		
		INNER JOIN
		V_Tipo_Documento_Empresas AS TIDE ON
		EMPR.Codigo = TIDE.EMPR_Codigo
		AND EMPR.CATA_TIID_Codigo = TIDE.Codigo

		INNER JOIN        
		Oficinas OFIC ON        
		ENNF.EMPR_Codigo = OFIC.EMPR_Codigo        
		AND ENNF.OFIC_Nota = OFIC.Codigo

		INNER JOIN        
		Terceros CLIE ON        
		ENNF.EMPR_Codigo = CLIE.EMPR_Codigo        
		AND ENNF.TERC_Cliente = CLIE.Codigo  

		INNER JOIN
		V_Tipo_Naturaleza_Terceros AS TINT ON
		CLIE.EMPR_Codigo = TINT.EMPR_Codigo
		AND CLIE.CATA_TINT_Codigo = TINT.Codigo

		INNER JOIN
		V_Tipo_Documento_Terceros AS TIDC ON
		CLIE.EMPR_Codigo = TIDC.EMPR_Codigo
		AND CLIE.CATA_TIID_Codigo = TIDC.Codigo

		INNER JOIN        
		Terceros TEFA ON        
		ENNF.EMPR_Codigo = TEFA.EMPR_Codigo        
		AND ENNF.TERC_Facturar = TEFA.Codigo
		
		INNER JOIN
		Ciudades AS CIUD ON
		CLIE.EMPR_Codigo = CIUD.EMPR_Codigo
		AND CLIE.CIUD_Codigo = CIUD.Codigo

		INNER JOIN
		Departamentos AS DEPA ON
		CIUD.EMPR_Codigo = DEPA.EMPR_Codigo
		AND CIUD.DEPA_Codigo = DEPA.Codigo
		
		INNER JOIN
		Conceptos_Notas_Facturas AS CONF ON
		ENNF.EMPR_Codigo = CONF.EMPR_Codigo
		AND ENNF.CONF_Codigo = CONF.Codigo
		
		INNER JOIN
		Encabezado_Facturas AS ENFA ON
		ENNF.EMPR_Codigo = ENFA.EMPR_Codigo
		AND ENNF.ENFA_Numero = ENFA.Numero
		
		WHERE
		ENNF.EMPR_Codigo = @par_EMPR_Codigo
		AND (ENNF.Numero = @par_Numero OR @par_Numero IS NULL)
		AND (ENNF.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)
		AND (ENNF.Fecha >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)
		AND (ENNF.Fecha <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)
		AND (ENNF.TERC_Cliente = @par_TERC_Cliente OR @par_TERC_Cliente IS NULL)
		AND (ENNF.TERC_Facturar = @par_TERC_Facturar OR @par_TERC_Facturar IS NULL)
		AND (ENNF.OFIC_Nota = @par_OFIC_Nota OR @par_OFIC_Nota IS NULL)        
		AND (ENNF.Estado = @par_Estado OR @par_Estado IS NULL)  
		AND (ENNF.Anulado = @par_Anulado OR @par_Anulado IS NULL)
		AND (ENFA.Estado = 1)--Aplica para Facturas En Definitivo
		AND (ENFA.Anulado = 0)--Aplica para Facturas Sin Anular
	);        
	WITH Pagina        
	AS        
	(    
		SELECT
		0 As Obtener,        
		ENNF.EMPR_Codigo,        
		ENNF.Numero,
		ENNF.Numero_Documento AS ENNF_NumeroDocumento,
		ENNF.Numero_Preimpreso AS ENNF_NumeroPreimpreso,
		ENNF.TIDO_Codigo,        
		ENNF.Fecha AS ENNF_Fecha,
		
		ENFA.Numero AS ENFA_Numero,
		ENFA.Numero_Documento AS ENFA_NumeroDocumento,
		ENFA.Valor_Factura,
		ENFA.Fecha AS ENFA_Fecha,

		EMPR.Numero_Identificacion AS EMPR_Identificacion,
		CASE TIDE.Codigo 
		WHEN 101 THEN 'IdentityCard' 
		WHEN 102 THEN 'NIT' 
		WHEN 103 THEN 'ForeignerIdentification' 
		ELSE TIDE.Nombre END  As EMPR_TipoDocumento,
		
		ENNF.OFIC_Nota,
		OFIC.Nombre As OFIC_NombreNota,
		
		CASE TINT.Codigo
		WHEN 501 THEN 'Natural' 
		WHEN 502 THEN 'Legal' 
		ELSE TINT.Nombre END As CLIE_TipoNaturaleza,
		CASE TIDC.Codigo 
		WHEN 101 THEN 'IdentityCard' 
		WHEN 102 THEN 'NIT' 
		WHEN 103 THEN 'ForeignerIdentification' 
		ELSE TIDC.Nombre END  As CLIE_TipoDocumento,  
		
		ENNF.TERC_Cliente,
		CLIE.Numero_Identificacion AS CLIE_Identificacion,
		CLIE.Direccion AS CLIE_Direccion,
		CLIE.Codigo_Postal AS CLIE_CodigoPostal,
		CLIE.Emails As CLIE_Email,
		LTRIM(RTRIM(ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'')))  As CLIE_NombreCompleto,
		
		ENNF.TERC_Facturar,
		LTRIM(RTRIM(ISNULL(TEFA.Razon_Social,'') + ISNULL(TEFA.Nombre,'') + ' ' + ISNULL(TEFA.Apellido1,'') + ' ' + ISNULL(TEFA.Apellido2,'')))  As TEFA_NombreCompleto,
		
		ENNF.Valor_Nota AS ENNF_Valor_Nota, 
		ISNULL(ENNF.Nota_Electronica,0) As Estado_NOEL,
		ISNULL(ENNF.ID_Numero_Documento_Proveedor_Electronico,'') As ID_NUM_DOC_PRO_ELE,
		
		ENNF.Observaciones AS ENNF_Observaciones,
		ENNF.Estado AS ENNF_Estado,
		ENNF.Anulado AS ENNF_Anulado,
		
		CIUD.Nombre AS CUID_ClienteNombre,
		DEPA.Nombre AS DEPA_ClienteNombre,
		
		ENNF.Fecha_Crea,        
		ENNF.USUA_Codigo_Crea,        
		ENNF.Fecha_Modifica,        
		ENNF.USUA_Codigo_Modifica,

		ENNF.CONF_Codigo As CONF_Codigo,
		CONF.Nombre As CONF_Nombre,

		ROW_NUMBER() OVER (ORDER BY ENNF.Numero) AS RowNumber        

		FROM Encabezado_Notas_Facturas ENNF 
		
		INNER JOIN 
		Empresas AS EMPR ON
		ENNF.EMPR_Codigo = EMPR.Codigo
		
		INNER JOIN
		V_Tipo_Documento_Empresas AS TIDE ON
		EMPR.Codigo = TIDE.EMPR_Codigo
		AND EMPR.CATA_TIID_Codigo = TIDE.Codigo

		INNER JOIN        
		Oficinas OFIC ON        
		ENNF.EMPR_Codigo = OFIC.EMPR_Codigo        
		AND ENNF.OFIC_Nota = OFIC.Codigo

		INNER JOIN        
		Terceros CLIE ON        
		ENNF.EMPR_Codigo = CLIE.EMPR_Codigo        
		AND ENNF.TERC_Cliente = CLIE.Codigo  

		INNER JOIN
		V_Tipo_Naturaleza_Terceros AS TINT ON
		CLIE.EMPR_Codigo = TINT.EMPR_Codigo
		AND CLIE.CATA_TINT_Codigo = TINT.Codigo

		INNER JOIN
		V_Tipo_Documento_Terceros AS TIDC ON
		CLIE.EMPR_Codigo = TIDC.EMPR_Codigo
		AND CLIE.CATA_TIID_Codigo = TIDC.Codigo

		INNER JOIN        
		Terceros TEFA ON        
		ENNF.EMPR_Codigo = TEFA.EMPR_Codigo        
		AND ENNF.TERC_Facturar = TEFA.Codigo
		
		INNER JOIN
		Ciudades AS CIUD ON
		CLIE.EMPR_Codigo = CIUD.EMPR_Codigo
		AND CLIE.CIUD_Codigo = CIUD.Codigo

		INNER JOIN
		Departamentos AS DEPA ON
		CIUD.EMPR_Codigo = DEPA.EMPR_Codigo
		AND CIUD.DEPA_Codigo = DEPA.Codigo
		
		INNER JOIN
		Conceptos_Notas_Facturas AS CONF ON
		ENNF.EMPR_Codigo = CONF.EMPR_Codigo
		AND ENNF.CONF_Codigo = CONF.Codigo
		
		INNER JOIN
		Encabezado_Facturas AS ENFA ON
		ENNF.EMPR_Codigo = ENFA.EMPR_Codigo
		AND ENNF.ENFA_Numero = ENFA.Numero
		
		WHERE
		ENNF.EMPR_Codigo = @par_EMPR_Codigo
		AND (ENNF.Numero = @par_Numero OR @par_Numero IS NULL)
		AND (ENNF.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)
		AND (ENNF.Fecha >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)
		AND (ENNF.Fecha <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)
		AND (ENNF.TERC_Cliente = @par_TERC_Cliente OR @par_TERC_Cliente IS NULL)
		AND (ENNF.TERC_Facturar = @par_TERC_Facturar OR @par_TERC_Facturar IS NULL)
		AND (ENNF.OFIC_Nota = @par_OFIC_Nota OR @par_OFIC_Nota IS NULL)
		AND (ENNF.Estado = @par_Estado OR @par_Estado IS NULL)
		AND (ENNF.Anulado = @par_Anulado OR @par_Anulado IS NULL)
		AND (ENFA.Estado = 1)--Aplica para Facturas En Definitivo
		AND (ENFA.Anulado = 0)--Aplica para Facturas Sin Anular
	)        

	SELECT
	Obtener,
	EMPR_Codigo,
	Numero,
	ENNF_NumeroDocumento,
	ENNF_NumeroPreimpreso,
	TIDO_Codigo,
	ENNF_Fecha,
	ENFA_Numero,
	ENFA_NumeroDocumento,
	Valor_Factura,
	ENFA_Fecha,
	EMPR_Identificacion,
	EMPR_TipoDocumento,
	OFIC_Nota,
	OFIC_NombreNota,
	CLIE_TipoNaturaleza,
	CLIE_TipoDocumento,
	TERC_Cliente,
	CLIE_Identificacion,
	CLIE_Direccion,
	CLIE_CodigoPostal,
	CLIE_Email,
	CLIE_NombreCompleto,
	TERC_Facturar,
	TEFA_NombreCompleto,
	ENNF_Valor_Nota,
	Estado_NOEL,
	ID_NUM_DOC_PRO_ELE,
	ENNF_Observaciones,
	ENNF_Estado,
	ENNF_Anulado,
	CUID_ClienteNombre,
	DEPA_ClienteNombre,
	Fecha_Crea,
	USUA_Codigo_Crea,
	Fecha_Modifica,
	USUA_Codigo_Modifica,
	CONF_Codigo,
	CONF_Nombre,
	@CantidadRegistros AS TotalRegistros,
	@par_NumeroPagina AS PaginaObtener,
	@par_RegistrosPagina AS RegistrosPagina

	FROM Pagina
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros) 
END
GO