﻿PRINT 'gsp_eliminar_ciudades'
GO
DROP PROCEDURE gsp_eliminar_ciudades
GO
CREATE PROCEDURE gsp_eliminar_ciudades(
  @par_EMPR_Codigo SMALLINT,
  @par_Codigo NUMERIC
  )
AS
BEGIN
  
  DELETE Ciudades 
  WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND Codigo = @par_Codigo

    SELECT @@ROWCOUNT AS Numero

END
GO