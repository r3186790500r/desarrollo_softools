﻿Print 'gsp_consultar_detalle_causales_fidelizacion_puntos_vehiculos'
GO
DROP PROCEDURE gsp_consultar_detalle_causales_fidelizacion_puntos_vehiculos
GO
CREATE PROCEDURE gsp_consultar_detalle_causales_fidelizacion_puntos_vehiculos
(
	@par_EMPR_Codigo smallint,
	@par_Codigo smallint = NULL,
	@par_Vehi_Codigo numeric = NULL,
	@par_TENE_Codigo numeric = NULL,
	@par_COND_Codigo numeric = NULL,
	@par_Cata_CPPV numeric = NULL,  
	@par_Fecha_Inicio_Vigencia_Desde datetime = NULL,
	@par_Fecha_Inicio_Vigencia_Hasta datetime = NULL,
	@par_Fecha_Fin_Vigencia_Desde datetime = NULL,
	@par_Fecha_Fin_Vigencia_Hasta datetime = NULL,
	@par_Estado smallint = NULL,
	@par_Anulado smallint = NULL,
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL  
)
AS                                 
BEGIN                        
	SET NOCOUNT ON;                              
	DECLARE @CantidadRegistros INT                        
	SELECT                        
	@CantidadRegistros = (SELECT COUNT(1)

	FROM Detalle_Causales_Plan_Puntos_Vehiculos DECA

	INNER JOIN Vehiculos AS VEHI
	ON DECA.EMPR_Codigo = VEHI.EMPR_Codigo
	AND DECA.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS TENE
	ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Terceros AS COND
	ON VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo

	LEFT JOIN Valor_Catalogos AS CPPV
	ON DECA.EMPR_Codigo = CPPV.EMPR_Codigo
	AND DECA.CATA_CPPV_Codigo = CPPV.Codigo

	WHERE DECA.EMPR_Codigo = @par_EMPR_Codigo
	AND DECA.ID = ISNULL(@par_Codigo, DECA.ID)
	AND DECA.VEHI_Codigo = ISNULL(@par_Vehi_Codigo, DECA.VEHI_Codigo)
	AND TENE.Codigo = ISNULL(@par_TENE_Codigo, TENE.Codigo)
	AND COND.Codigo = ISNULL(@par_COND_Codigo, COND.Codigo)
	AND DECA.CATA_CPPV_Codigo = ISNULL(@par_Cata_CPPV, DECA.CATA_CPPV_Codigo)
	AND DECA.Estado = ISNULL(@par_Estado, DECA.Estado)
	AND DECA.Anulado = ISNULL(@par_Anulado, DECA.Anulado)
	AND DECA.Fecha_Inicio >= ISNULL(@par_Fecha_Inicio_Vigencia_Desde, DECA.Fecha_Inicio)
	AND DECA.Fecha_Inicio <= ISNULL(@par_Fecha_Inicio_Vigencia_Hasta, DECA.Fecha_Inicio)
	AND DECA.Fecha_Vence >= ISNULL(@par_Fecha_Fin_Vigencia_Desde, DECA.Fecha_Vence)
	AND DECA.Fecha_Vence <= ISNULL(@par_Fecha_Fin_Vigencia_Hasta, DECA.Fecha_Vence)
);                        
WITH Pagina
AS
(
	SELECT
	DECA.EMPR_Codigo,
	DECA.ID AS Codigo,
	VEHI.Codigo AS VEHI_Codigo,
	VEHI.Placa AS VEHI_Placa,
	ISNULL(TENE.Codigo, 0) AS TENE_Codigo,
	IIF(TENE.Nombre is NULL OR TENE.Nombre = '', TENE.Razon_Social, CONCAT(TENE.Nombre,' ', TENE.Apellido1)) AS TENE_Nombre,
	ISNULL(COND.Codigo, 0) AS COND_Codigo,
	IIF(COND.Nombre is NULL OR COND.Nombre = '', COND.Razon_Social, CONCAT(COND.Nombre,' ', COND.Apellido1)) AS COND_Nombre,
	ISNULL(CPPV.Codigo, 0) AS CPPV_Codigo,
	ISNULL(CPPV.Campo1, '') AS CPPV_Campo1,
	ISNULL(DECA.Fecha_Inicio,'') AS DECA_FechaInicio,
	ISNULL(DECA.Fecha_Vence,'') AS DECA_FechaFin,
	ISNULL(DECA.Puntos, 0) AS Puntos,
	DECA.Estado,
	DECA.Anulado,
	USUA.Codigo_Usuario as USUA_CodigoUsuario,
	DECA.Fecha_Crea,
	ROW_NUMBER() OVER (ORDER BY DECA.ID) AS RowNumber
	        
	FROM Detalle_Causales_Plan_Puntos_Vehiculos DECA

	INNER JOIN Vehiculos AS VEHI
	ON DECA.EMPR_Codigo = VEHI.EMPR_Codigo
	AND DECA.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS TENE
	ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Terceros AS COND
	ON VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo

	LEFT JOIN Valor_Catalogos AS CPPV
	ON DECA.EMPR_Codigo = CPPV.EMPR_Codigo
	AND DECA.CATA_CPPV_Codigo = CPPV.Codigo

	LEFT JOIN Usuarios AS USUA
	ON DECA.EMPR_Codigo = USUA.EMPR_Codigo
	AND DECA.USUA_Codigo_Crea = USUA.Codigo

	WHERE DECA.EMPR_Codigo = @par_EMPR_Codigo
	AND DECA.ID = ISNULL(@par_Codigo, DECA.ID)
	AND DECA.VEHI_Codigo = ISNULL(@par_Vehi_Codigo, DECA.VEHI_Codigo)
	AND TENE.Codigo = ISNULL(@par_TENE_Codigo, TENE.Codigo)
	AND COND.Codigo = ISNULL(@par_COND_Codigo, COND.Codigo)
	AND DECA.CATA_CPPV_Codigo = ISNULL(@par_Cata_CPPV, DECA.CATA_CPPV_Codigo)
	AND DECA.Estado = ISNULL(@par_Estado, DECA.Estado)
	AND DECA.Anulado = ISNULL(@par_Anulado, DECA.Anulado)
	AND DECA.Fecha_Inicio >= ISNULL(@par_Fecha_Inicio_Vigencia_Desde, DECA.Fecha_Inicio)
	AND DECA.Fecha_Inicio <= ISNULL(@par_Fecha_Inicio_Vigencia_Hasta, DECA.Fecha_Inicio)
	AND DECA.Fecha_Vence >= ISNULL(@par_Fecha_Fin_Vigencia_Desde, DECA.Fecha_Vence)
	AND DECA.Fecha_Vence <= ISNULL(@par_Fecha_Fin_Vigencia_Hasta, DECA.Fecha_Vence)
)                        
	SELECT                     
	EMPR_Codigo,
	Codigo,
	VEHI_Codigo,
	VEHI_Placa,
	TENE_Codigo,
	TENE_Nombre,
	COND_Codigo,
	COND_Nombre,
	CPPV_Codigo,
	CPPV_Campo1,
	DECA_FechaInicio,
	DECA_FechaFin,
	Puntos,
	Estado,
	Anulado,
	USUA_CodigoUsuario,
	Fecha_Crea,
	@CantidadRegistros AS TotalRegistros,
	@par_NumeroPagina AS PaginaObtener,
	@par_RegistrosPagina AS RegistrosPagina

	FROM Pagina                        
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                        
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                        
	--ORDER BY DECA_FechaInicio DESC
END         