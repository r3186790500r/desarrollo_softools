﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Repositorio.Mantenimiento
Imports EncoExpres.GesCarga.Entidades.MantenimientosPendientes


Namespace Mantenimiento

    Public NotInheritable Class PersistenciaPlanEquipoMantenimiento
        Implements IPersistenciaBase(Of PlanEquipoMantenimiento)
        Public Function Consultar(filtro As PlanEquipoMantenimiento) As IEnumerable(Of PlanEquipoMantenimiento) Implements IPersistenciaBase(Of PlanEquipoMantenimiento).Consultar
            Return New RepositorioPlanEquipoMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PlanEquipoMantenimiento) As Long Implements IPersistenciaBase(Of PlanEquipoMantenimiento).Insertar
            Return New RepositorioPlanEquipoMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PlanEquipoMantenimiento) As Long Implements IPersistenciaBase(Of PlanEquipoMantenimiento).Modificar
            Return New RepositorioPlanEquipoMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PlanEquipoMantenimiento) As PlanEquipoMantenimiento Implements IPersistenciaBase(Of PlanEquipoMantenimiento).Obtener
            Return New RepositorioPlanEquipoMantenimiento().Obtener(filtro)
        End Function

        Public Function Anular(entidad As PlanEquipoMantenimiento) As Boolean
            Return New RepositorioPlanEquipoMantenimiento().Anular(entidad)
        End Function
        Public Function ConsultarEquipos(entidad As PlanEquipoMantenimiento) As IEnumerable(Of EquipoPlanEquipoMantenimiento)
            Return New RepositorioPlanEquipoMantenimiento().ConsultarEquipos(entidad)
        End Function
        Public Function InsertarEquipos(entidad As IEnumerable(Of EquipoPlanEquipoMantenimiento)) As Long
            Return New RepositorioPlanEquipoMantenimiento().InsertarEquipos(entidad)
        End Function
        Public Function ConsultarMantenimientosPendientes(entidad As MantenimientosPendientes) As IEnumerable(Of MantenimientosPendientes)
            Return New RepositorioPlanEquipoMantenimiento().ConsultarMantenimientosPendientes(entidad)
        End Function
        Public Function CambiarEstado(entidad As PlanEquipoMantenimiento) As Boolean
            Return New RepositorioPlanEquipoMantenimiento().CambiarEstado(entidad)
        End Function
    End Class
End Namespace
