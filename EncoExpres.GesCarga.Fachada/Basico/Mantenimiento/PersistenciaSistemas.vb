﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico
Imports EncoExpres.GesCarga.Repositorio.Basico

Namespace Basico
    Public NotInheritable Class PersistenciaSistemas
        Implements IPersistenciaBase(Of Sistemas)
        Public Function Consultar(filtro As Sistemas) As IEnumerable(Of Sistemas) Implements IPersistenciaBase(Of Sistemas).Consultar
            Return New RepositorioSistemas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Sistemas) As Long Implements IPersistenciaBase(Of Sistemas).Insertar
            Return New RepositorioSistemas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Sistemas) As Long Implements IPersistenciaBase(Of Sistemas).Modificar
            Return New RepositorioSistemas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Sistemas) As Sistemas Implements IPersistenciaBase(Of Sistemas).Obtener
            Return New RepositorioSistemas().Obtener(filtro)
        End Function
    End Class
End Namespace
