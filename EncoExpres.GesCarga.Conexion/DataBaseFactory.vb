﻿''' <summary>
''' Class <see cref="DataBaseFactory"/>
''' </summary>
Public MustInherit Class DataBaseFactory : Implements IDisposable

    ''' <summary>
    ''' To detect redundant calls
    ''' </summary>
    Private disposedValue As Boolean

    ''' <summary>
    ''' The connection string data base
    ''' </summary>
    Protected ConnectionStringDataBase As String

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DataBaseFactory"/> class.
    ''' </summary>
    ''' <param name="connection">The connection.</param>
    Public Sub New(ByVal connection As String)
        ConnectionStringDataBase = connection
    End Sub

    ''' <summary>
    ''' Creates the connection.
    ''' </summary>
    MustOverride Sub CreateConnection()

    ''' <summary>
    ''' Creates the connection transactional.
    ''' </summary>
    MustOverride Sub BeginTransact()

    ''' <summary>
    ''' Commit the transaction and close connection.
    ''' </summary>
    MustOverride Sub CommitTransact()

    ''' <summary>
    ''' Rollback the transaction and close connection.
    ''' </summary>
    MustOverride Sub RollbackTransact()


    ''' <summary>
    ''' Closes the connection.
    ''' </summary>
    MustOverride Sub CloseConnection()

    ''' <summary>
    ''' Executes the reader.
    ''' </summary>
    ''' <param name="commandText">The command text.</param>
    ''' <returns>IDataReader.</returns>
    MustOverride Function ExecuteReader(commandText As String) As IDataReader

    ''' <summary>
    ''' Limpia los parametros 
    ''' </summary>
    ''' <remarks></remarks>
    MustOverride Sub CleanParameters()
    ''' <summary>
    ''' Executes the store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    ''' <returns>IDataReader.</returns>
    MustOverride Function ExecuteReaderStoreProcedure(ByVal procedureName As String) As IDataReader

    ''' <summary>
    ''' Executes the store procedure Transact.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    ''' <returns>IDataReader.</returns>
    MustOverride Function ExecuteReaderStoreProcedureTransact(ByVal procedureName As String) As IDataReader

    ''' <summary>
    ''' Executes the store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    ''' <returns>IDataReader.</returns>
    MustOverride Function ExecuteReaderStoreProcedureAsync(ByVal procedureName As String) As Task(Of IDataReader)

    ''' <summary>
    ''' Executes the non query store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    MustOverride Function ExecuteNonQueryStoreProcedureReturnResult(ByVal procedureName As String) As Integer

    ''' <summary>
    ''' Executes the non query store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    MustOverride Sub ExecuteNonQueryStoreProcedure(ByVal procedureName As String)

    ''' <summary>
    ''' Executes the non query store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    MustOverride Sub ExecuteNonQueryStoreProcedureTransact(ByVal procedureName As String)

    ''' <summary>
    ''' Executes the non query.
    ''' </summary>
    ''' <param name="commandText">The command text.</param>
    MustOverride Sub ExecuteNonQuery(commandText As String)

    ''' <summary>
    ''' Agregars the parametro objeto SQL.
    ''' </summary>
    ''' <param name="nombre">The nombre.</param>
    ''' <param name="valor">The valor.</param>
    MustOverride Sub AgregarParametroObjetoSQL(ByVal nombre As String, ByVal valor As Object)

    ''' <summary>
    ''' Agregars the parametro SQL.
    ''' </summary>
    ''' <param name="nombre">The nombre.</param>
    ''' <param name="valor">The valor.</param>
    ''' <param name="tipo">The tipo.</param>
    MustOverride Sub AgregarParametroSQL(ByVal nombre As String, ByVal valor As Object, Optional ByVal tipo As SqlDbType = Nothing)

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    ''' </summary>
    Sub Dispose() Implements System.IDisposable.Dispose
        Me.Dispose(True)
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    ''' <paramref name="disposing">indicador </paramref>
    ''' </summary>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                CloseConnection()
            End If
        End If
        Me.disposedValue = True
    End Sub
End Class