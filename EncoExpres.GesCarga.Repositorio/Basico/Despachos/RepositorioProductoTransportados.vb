﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioProductoTransportados"/>
    ''' </summary>
    Public NotInheritable Class RepositorioProductoTransportados
        Inherits RepositorioBase(Of ProductoTransportados)

        Public Overrides Function Consultar(filtro As ProductoTransportados) As IEnumerable(Of ProductoTransportados)
            Dim lista As New List(Of ProductoTransportados)
            Dim item As ProductoTransportados

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.ValorAutocomplete) Or filtro.Sync = True Then
                    conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)

                    If filtro.Estado >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_autocomplete_producto_transportados]")

                    While resultado.Read
                        item = New ProductoTransportados(resultado)
                        lista.Add(item)
                    End While


                Else
                    If filtro.Estado >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_producto_transportados]")

                    While resultado.Read
                        item = New ProductoTransportados(resultado)
                        lista.Add(item)
                    End While
                End If

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ProductoTransportados) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_producto")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As ProductoTransportados) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ProductoTransportados) As ProductoTransportados
            Throw New NotImplementedException()


        End Function
        Public Function Anular(entidad As ProductoTransportados) As Boolean
            Throw New NotImplementedException()


        End Function
    End Class

End Namespace