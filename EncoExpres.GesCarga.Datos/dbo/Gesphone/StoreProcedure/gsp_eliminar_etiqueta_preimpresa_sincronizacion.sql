﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 22/09/2021 
-- Módulo: Gesphone

PRINT 'gsp_eliminar_etiqueta_preimpresa_sincronizacion'
GO
DROP PROCEDURE gsp_eliminar_etiqueta_preimpresa_sincronizacion
GO
CREATE PROCEDURE dbo.gsp_eliminar_etiqueta_preimpresa_sincronizacion      
(                      
  @par_EMPR_Codigo SMALLINT,
  @par_ENRE_Numero NUMERIC,  
  @par_Numero_Etiqueta NUMERIC
)                      
AS                       
BEGIN

  UPDATE Detalle_Asignacion_Etiquetas_Preimpresas SET ENRE_Numero = 0 WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero_Etiqueta = @par_Numero_Etiqueta AND ENRE_Numero = @par_ENRE_Numero
  DELETE Detalle_Etiquetas_Remesas_Paqueteria WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero AND Numero_Etiqueta = @par_Numero_Etiqueta
  SELECT @@ROWCOUNT AS Numero   
                  
END    
GO