﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

print 'gps_Reporte_Guias'

DROP PROCEDURE gps_Reporte_Guias
GO

CREATE PROCEDURE gps_Reporte_Guias(
@par_EMPR_Codigo numeric, 
@par_Numero_Documento numeric
)
AS BEGIN 
SELECT 
ENRE.Numero_Documento
,EMPR.Numero_Identificacion NitEmpresa
,ENRE.Fecha AS Fecha
,OFIC.Nombre AS NombreOficina
,OFIC.Direccion AS DireccionOficina
,OFIC.Telefono AS TelefonoOficina
,REMI.Numero_Identificacion AS IdenRemitente
,ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')  
+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'') AS NombreRemitente
,ENRE.Direccion_Remitente 
,ENRE.Telefonos_Remitente
,CIRE.Nombre CiudadRemitente
,DEST.Numero_Identificacion AS IdenDestinatario
,ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')  
+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'') AS NombreDestinatario
,ENRE.Direccion_Destinatario 
,ENRE.Telefonos_Destinatario
,CIDE.Nombre CiudadDestinatario
,VEHI.Placa
,ENRE.Peso_Cliente
,ENRE.Cantidad_Cliente
,ENRE.Valor_Flete_Cliente
,ENRE.Valor_Manejo_Cliente 
,ENRE.Valor_Comercial_Cliente AS DECLARADO
,ENRE.Total_Flete_Cliente AS TOTAL_PAGAR
,PRTR.Nombre Producto
,USCR.Nombre
 AS Elaboro 

FROM  Encabezado_Remesas ENRE, 
Empresas EMPR
,Oficinas OFIC
,Vehiculos VEHI
,Terceros REMI
,Terceros DEST
,Ciudades CIRE
,Ciudades CIDE
,Producto_Transportados PRTR
,Usuarios USCR
,Encabezado_Tarifario_Carga_Ventas ENTA

WHERE ENRE.EMPR_Codigo = EMPR.Codigo
AND ENRE.EMPR_Codigo = OFIC.EMPR_Codigo
AND ENRE.OFIC_Codigo = OFIC.Codigo 
AND ENRE.EMPR_Codigo = VEHI.EMPR_Codigo
AND ENRE.VEHI_Codigo = VEHI.Codigo 

AND ENRE.EMPR_Codigo = REMI.EMPR_Codigo
AND ENRE.TERC_Codigo_Remitente = REMI.Codigo 
AND ENRE.EMPR_Codigo = DEST.EMPR_Codigo
AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo 

AND ENRE.EMPR_Codigo = CIRE.EMPR_Codigo
AND ENRE.CIUD_Codigo_Remitente = CIRE.Codigo 
AND ENRE.EMPR_Codigo = CIDE.EMPR_Codigo
AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo 

AND ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
AND ENRE.PRTR_Codigo = PRTR.Codigo


AND ENRE.EMPR_Codigo = USCR.EMPR_Codigo 
AND ENRE.USUA_Codigo_Crea = USCR.Codigo

AND ENRE.EMPR_Codigo = ENTA.EMPR_Codigo
AND ENRE.ETCV_Numero = ENTA.Numero
AND ENRE.TIDO_Codigo = 110

AND ENRE.EMPR_Codigo = @par_EMPR_Codigo
AND ENRE.Numero = @par_Numero_Documento
END
GO