﻿Print 'V_Tipo_Documento_Cruce'
GO
DROP VIEW V_Tipo_Documento_Cruce
GO
CREATE VIEW V_Tipo_Documento_Cruce 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 28
GO