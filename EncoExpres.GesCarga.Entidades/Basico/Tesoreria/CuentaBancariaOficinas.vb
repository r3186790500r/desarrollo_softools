﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref=" CuentaBancariaOficinas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class CuentaBancariaOficinas
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CuentaBancariaOficinas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CuentaBancariaOficinas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            CuentaBancaria = New CuentaBancarias With {.Codigo = Read(lector, "CUBA_Codigo"), .Nombre = Read(lector, "NombreCuenta")}
            CuentaBancaria.CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .Nombre = Read(lector, "NombreCuentaPUC"), .NumeroCuenta = Read(lector, "NumeroCuentaPUC"), .CodigoCuenta = Read(lector, "NumeroCuentaPUC")}
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "Oficina")}
            NombreCuenta = Read(lector, "NombreCuenta")
            NumeroCuenta = Read(lector, "NumeroCuenta")
        End Sub

        ''' <summary>
        ''' Obtiene o establece  la cuenta bancaria
        ''' </summary>
        <JsonProperty>
        Public Property CuentaBancaria As CuentaBancarias
        ''' <summary>
        ''' Obtiene o establece  la oficina de la cuenta bancaria
        ''' </summary>
        <JsonProperty>
        Public Property Oficina As Oficinas

        <JsonProperty>
        Public Property CodigoEstado As Integer
        <JsonProperty>
        Public Property NombreCuenta As String
        <JsonProperty>
        Public Property NumeroCuenta As String

    End Class

End Namespace
