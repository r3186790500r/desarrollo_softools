﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports System.Transactions

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref="RepositorioEncabezadoUbicacionAlmacenes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEncabezadoUbicacionAlmacenes
        Inherits RepositorioBase(Of EncabezadoUbicacionAlmacenes)

        Public Overrides Function Consultar(filtro As EncabezadoUbicacionAlmacenes) As IEnumerable(Of EncabezadoUbicacionAlmacenes)
            Dim lista As New List(Of EncabezadoUbicacionAlmacenes)
            Dim item As EncabezadoUbicacionAlmacenes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Pagina) Then
                    If (filtro.Pagina) Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If (filtro.RegistrosPagina) Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If filtro.Estado > -1 Then 'NO APLICA
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Almacen) Then
                    If filtro.Almacen.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_ALMA_Codigo", filtro.Almacen.Codigo)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_encabezado_ubicacion_referencia_almacenes]")

                While resultado.Read
                    item = New EncabezadoUbicacionAlmacenes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As EncabezadoUbicacionAlmacenes) As Long
            Dim inserto As Boolean = True
            Dim insertoDetalle As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ALMA_Codigo", entidad.Almacen.Codigo)
                    conexion.AgregarParametroSQL("@par_Numero_Filas", entidad.NumeroFilas)
                    conexion.AgregarParametroSQL("@par_Numero_Columnas", entidad.NumeroColumnas)
                    conexion.AgregarParametroSQL("@par_Numero_Niveles", entidad.NumeroNiveles)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_encabezado_ubicacion_referencia_almacenes]")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While

                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalles
                        detalle.Codigo = entidad.Codigo
                        detalle.Almacen = entidad.Almacen
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoDetalle = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto And insertoDetalle Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using
            Return entidad.Codigo
        End Function

        Public Function InsertarDetalle(entidad As DetalleUbicacionAlmacenes, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_EURA_Codigo", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_ALMA_Codigo", entidad.Almacen.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Fila", entidad.Fila)
            contextoConexion.AgregarParametroSQL("@par_Columna", entidad.Columna)
            contextoConexion.AgregarParametroSQL("@par_Nivel", entidad.Nivel)
            contextoConexion.AgregarParametroSQL("@par_CATA_TIUA_Codigo", entidad.CodigoTipoUbicacion)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_detalle_ubicacion_referencia_almacenes]")

            While resultado.Read
                inserto = IIf(resultado.Item("Codigo") > Cero, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As EncabezadoUbicacionAlmacenes) As Long
            Dim inserto As Boolean = True
            Dim insertoDetalle As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_ALMA_Codigo", entidad.Almacen.Codigo)
                    conexion.AgregarParametroSQL("@par_Numero_Filas", entidad.NumeroFilas)
                    conexion.AgregarParametroSQL("@par_Numero_Columnas", entidad.NumeroColumnas)
                    conexion.AgregarParametroSQL("@par_Numero_Niveles", entidad.NumeroNiveles)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_modificar_encabezado_ubicacion_referencia_almacenes]")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While

                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalles
                        detalle.Codigo = entidad.Codigo
                        detalle.Almacen = entidad.Almacen
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoDetalle = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto And insertoDetalle Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As EncabezadoUbicacionAlmacenes) As EncabezadoUbicacionAlmacenes
            Dim item As New EncabezadoUbicacionAlmacenes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_encabezado_ubicacion_referencia_almacenes]")

                While resultado.Read
                    item = New EncabezadoUbicacionAlmacenes(resultado)
                End While

                Dim duaFiltro As DetalleUbicacionAlmacenes = New DetalleUbicacionAlmacenes With {.CodigoEmpresa = filtro.CodigoEmpresa, .Codigo = filtro.Codigo}

                item.Detalles = New RepositorioDetalleUbicacionAlmacenes().Consultar(duaFiltro)


            End Using

            Return item
        End Function

        Public Function Eliminar(entidad As EncabezadoUbicacionAlmacenes) As Boolean
            Dim Elimino As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_eliminar_encabezado_ubicacion_referencia_almacenes]")

                While resultado.Read
                    Elimino = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return Elimino
        End Function

    End Class

End Namespace