﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos

Namespace Basico.Despachos

    Public NotInheritable Class RepositorioVehiculosListaNegra
        Inherits RepositorioBase(Of Vehiculos)


        Public Overrides Function Insertar(entidad As Vehiculos) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    'conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_VEHI_Placa", entidad.Placa)
                    conexion.AgregarParametroSQL("@par_Causa", entidad.Causa)
                    conexion.AgregarParametroSQL("@par_organismoTransito", entidad.OrganismoTransito)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.EstadoListaNegra)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_vehiculos_lista_negra")
                    While resultado.Read

                        entidad.CodigoListaNegra = resultado.Item("CodigoListaNegra").ToString
                    End While
                    resultado.Close()

                    If inserto Then
                        transaccion.Complete()
                    Else
                        entidad.CodigoListaNegra = Cero
                    End If

                End Using
            End Using
            Return entidad.CodigoListaNegra
        End Function

        Public Overrides Function Modificar(entidad As Vehiculos) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    'conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_VEHI_Placa", entidad.Placa)
                    conexion.AgregarParametroSQL("@par_Causa", entidad.Causa)
                    conexion.AgregarParametroSQL("@par_organismoTransito", entidad.OrganismoTransito)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.EstadoListaNegra)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_vehiculos_lista_negra")
                    While resultado.Read

                        entidad.CodigoListaNegra = resultado.Item("CodigoListaNegra")
                    End While
                    resultado.Close()

                    If inserto Then
                        transaccion.Complete()
                    Else
                        entidad.CodigoListaNegra = Cero
                    End If

                End Using
            End Using
            Return entidad.CodigoListaNegra
        End Function

        Public Overrides Function Consultar(filtro As Vehiculos) As IEnumerable(Of Vehiculos)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As Vehiculos) As Vehiculos
            Throw New NotImplementedException()
        End Function

        Public Function Anular(filtro As Vehiculos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_Vehiculos_Lista_Negra")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class
End Namespace
