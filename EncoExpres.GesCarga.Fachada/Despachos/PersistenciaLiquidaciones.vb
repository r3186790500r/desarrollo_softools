﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Repositorio.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo

Namespace Despachos
    Public NotInheritable Class PersistenciaLiquidaciones
        Implements IPersistenciaBase(Of Liquidacion)

        Public Function Consultar(filtro As Liquidacion) As IEnumerable(Of Liquidacion) Implements IPersistenciaBase(Of Liquidacion).Consultar
            Return New RepositorioLiquidaciones().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Liquidacion) As Long Implements IPersistenciaBase(Of Liquidacion).Insertar
            Return New RepositorioLiquidaciones().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Liquidacion) As Long Implements IPersistenciaBase(Of Liquidacion).Modificar
            Return New RepositorioLiquidaciones().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Liquidacion) As Liquidacion Implements IPersistenciaBase(Of Liquidacion).Obtener
            Return New RepositorioLiquidaciones().Obtener(filtro)
        End Function

        Public Function Anular(entidad As Liquidacion) As Boolean
            Return New RepositorioLiquidaciones().Anular(entidad)
        End Function

        Public Function ObtenerPlanillaDespacho(filtro As Liquidacion) As PlanillaDespachos
            Return New RepositorioLiquidaciones().ObtenerPlanillaDespacho(filtro)
        End Function

        Public Function Rechazar(entidad As Liquidacion) As Boolean
            Return New RepositorioLiquidaciones().Rechazar(entidad)
        End Function

        Public Function Aprobar(entidad As Liquidacion) As Boolean
            Return New RepositorioLiquidaciones().Aprobar(entidad)
        End Function

    End Class

End Namespace

