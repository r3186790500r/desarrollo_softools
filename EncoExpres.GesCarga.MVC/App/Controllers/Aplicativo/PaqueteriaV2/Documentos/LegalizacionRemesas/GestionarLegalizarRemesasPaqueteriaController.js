﻿EncoExpresApp.controller("GestionarLegalizarRemesasPaqueteriaCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'LegalizarRemesasPaqueteriaFactory',
    'ValorCatalogosFactory', 'OficinasFactory', 'RemesaGuiasFactory', 'TercerosFactory', 'DocumentosFactory', 'UsuariosFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, LegalizarRemesasPaqueteriaFactory,
        ValorCatalogosFactory, OficinasFactory, RemesaGuiasFactory, TercerosFactory, DocumentosFactory, UsuariosFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'LEGALIZACIÓN GUÍAS PAQUETERÍA';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Legalizar Guías' }, { Nombre: 'Gestionar' }];
        $scope.Master = '#!ConsultarLegalizarRemesasPaqueteria';
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.LEGALIZACION_REMESAS);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.MENU_OPCION_GUIA = OPCION_MENU_PAQUETERIA.REMESAS;
        $scope.Deshabilitar = false;
        $scope.Filtro = {
            NumeroInicial: '',
            NumeroFinal: ''
        };
        $scope.Modelo = {
            Numero: 0,
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Fecha: new Date()
        };
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_BORRADOR);
        $scope.ListadoOficinas = [];
        $scope.PagRemesas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.PagRemesasGuardadas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.ListadoRemesas = [];
        $scope.ListadoRemesasGuardadas = [];
        $scope.ListadoNovedadLegaliza = [];
        $scope.ListadoConductor = [];
        $scope.ListadoConfirma = [
            { Codigo: -1, Nombre: "(SELECCIONAR)" },
            { Codigo: 0, Nombre: "NO" },
            { Codigo: 1, Nombre: "SI" }
        ];
        $scope.ListaGestionDocuRemesa = [];
        var TmpNumeroRemesa = 0;
        $scope.ListaOtrasLegalizaciones = [];
        $scope.MensajesErrorOtrLegaliza = [];
        //CODIGO_TIPO_DOCUMENTO_LEGALIZACION_GUIAS
        //-----------Control Entregas
        $scope.ListaPlanillas = [];
        $scope.ListaPlanillasEntregadas = [];
        $scope.ListadoTipoIdentificacion = [];
        $scope.ListadoNovedadesRecolecciones = [];
        $scope.Foto = [];
        $scope.Modal = {};
        $scope.DevolucionRemesa = 0;
        var fechaActual = new Date();
        $scope.MensajesErrorDocu = [];
        $scope.ModalDestinatario = {};
        //-----------Control Entregas
        var ListaFormaPago = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS },
            Sync: true
        }).Datos;
        ListaFormaPago.splice(ListaFormaPago.findIndex(item => item.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA), 1);
        //--Documental
        $scope.ListadoDocumentos = [];
        $scope.CadenaFormatos = ".pdf";
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete
        $scope.AutocompleteOficinas = function (value) {

            $scope.ListadoOficinasDestino = [];
            $scope.ListadoOficinasDestino = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
            return $scope.ListadoOficinasDestino;
        }

        //--Conductor
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoConductor = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CONDUCTOR,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoConductor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductor);
                }
            }
            return $scope.ListadoConductor;
        };
        //--Conductor
        //----------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //--Novedades Legalizacion Guias
            $scope.ListadoNovedadLegaliza = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_NOVEDAD_LEGALIZAR_GUIA },
                Sync: true
            }).Datos;
            //----Oficinas
            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                var responseOficina = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true });
                if (responseOficina.ProcesoExitoso === true) {
                    $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' });
                    responseOficina.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });
                    $scope.Filtro.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
                }
            }
            else {
                $scope.ListadoOficinas.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.Filtro.Oficina = $scope.ListadoOficinas[0];
            }
            //------------Entregas
            //--tipo identificaciones Tercero
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoIdentificacion = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoIdentificacion = response.data.Datos;
                            $scope.ListadoTipoIdentificacion.splice(0, 1);
                            $scope.Modal.TipoIdentificacionRecibe = $scope.ListadoTipoIdentificacion[0];
                        }
                        else {
                            $scope.ListadoTipoIdentificacion = [];
                        }
                    }
                }, function (response) {
                });
            //--Novedades Entregas
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 203 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNovedades = response.data.Datos;

                        }
                        else {
                            $scope.ListadoNovedades = []
                        }
                    }
                }, function (response) {
                });
            //--Novedades Entregas
            //--ListadoNovedadesRecoleccion
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 219 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNovedadesRecolecciones = response.data.Datos;
                        }
                        else {
                            $scope.ListadoNovedadesRecolecciones = []
                        }
                    }
                }, function (response) {
                });
            //--ListadoNovedadesRecoleccion
            //----Obtener Parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
                if ($routeParams.Numero > 0) {
                    $scope.Modelo.Numero = $routeParams.Numero;
                    PantallaBloqueo(Obtener, 'Obteniendo Documento');
                }
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //--Limpiar AutoCompletes
        $scope.limpiarControl = function (Control) {
            switch (Control) {
                case "Conductor":
                    $scope.Filtro.Conductor = "";
                    break;
            }
        };
        //--Limpiar Autocompletes
        //--------------------------PAGINACION-----------------------------//
        function ResetPaginacion(obj) {
            obj.totalRegistros = obj.array.length;
            obj.totalPaginas = Math.ceil(obj.totalRegistros / 10);
            $scope.PrimerPagina(obj);
        }
        $scope.PrimerPagina = function (obj) {
            if (obj.totalRegistros > 10) {
                obj.PagArray = [];
                obj.paginaActual = 1;
                for (var i = 0; i < 10; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            } else {
                obj.PagArray = obj.array;
            }
        };
        $scope.Anterior = function (obj) {
            if (obj.paginaActual > 1) {
                obj.paginaActual -= 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                }
            }
            else {
                $scope.PrimerPagina(obj);
            }
        };
        $scope.Siguiente = function (obj) {
            if (obj.paginaActual < obj.totalPaginas) {
                obj.paginaActual += 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                } else {
                    $scope.UltimaPagina(obj);
                }
            } else if (obj.paginaActual == obj.totalPaginas) {
                $scope.UltimaPagina(obj);
            }
        };
        $scope.UltimaPagina = function (obj) {
            if (obj.totalRegistros > 10 && obj.totalPaginas > 1) {
                obj.paginaActual = obj.totalPaginas;
                var a = obj.paginaActual * 10;
                obj.PagArray = [];
                for (var i = a - 10; i < obj.array.length; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            }
        }
        //--------------------------PAGINACION-----------------------------//
        //--Pantalla Bloqueo
        function PantallaBloqueo(fun, mensaje) {
            if (mensaje == undefined) {
                mensaje = "Espere por favor...";
            }
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start(mensaje);
            $timeout(function () { blockUI.message(mensaje); fun(); }, 100);
        }
        //--Pantalla Bloqueo
        //----------------------------REMESAS-------------------------------//
        //--Cargar Remesas
        function DatosRequeridosFiltro() {
            var continuar = true;
            $scope.MensajesErrorConslta = [];
            var Modelo = $scope.Filtro;

            if ((Modelo.FechaInicial === null || Modelo.FechaInicial === undefined || Modelo.FechaInicial === '')
                && (Modelo.FechaFinal === null || Modelo.FechaFinall === undefined || Modelo.FechaFinal === '')
                && (Modelo.NumeroInicial === null || Modelo.NumeroInicial === undefined || Modelo.NumeroInicial === '' || Modelo.NumeroInicial === 0 || isNaN(Modelo.NumeroInicial) === true)
                && (Modelo.NumeroDocumentoPlanilla === null || Modelo.NumeroDocumentoPlanilla === undefined || Modelo.NumeroDocumentoPlanilla === '' || Modelo.NumeroDocumentoPlanilla === 0 || isNaN(Modelo.NumeroDocumentoPlanilla) === true)) {
                $scope.MensajesErrorConslta.push('Debe ingresar los filtros de fechas o número');
                continuar = false;
            }
            else if ((Modelo.NumeroInicial !== null && Modelo.NumeroInicial !== undefined && Modelo.NumeroInicial !== '' && Modelo.NumeroInicial !== 0)
                || (Modelo.NumeroDocumentoPlanilla !== null && Modelo.NumeroDocumentoPlanilla !== undefined && Modelo.NumeroDocumentoPlanilla !== '' && Modelo.NumeroDocumentoPlanilla !== 0)
                || (Modelo.FechaInicial !== null && Modelo.FechaInicial !== undefined && Modelo.FechaInicial !== '')
                || (Modelo.FechaFinal !== null && Modelo.FechaFinal !== undefined && Modelo.FechaFinal !== '')) {
                if ((Modelo.FechaInicial !== null && Modelo.FechaInicial !== undefined && Modelo.FechaInicial !== '')
                    && (Modelo.FechaFinal !== null && Modelo.FechaFinal !== undefined && Modelo.FechaFinal !== '')) {
                    if (Modelo.FechaFinal < Modelo.FechaInicial) {
                        $scope.MensajesErrorConslta.push('La fecha final debe ser mayor a la fecha inicial');
                        continuar = false;
                    } else if (((Modelo.FechaFinal - Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorConslta.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                        $scope.FechaFinal = $scope.FechaInicial;
                    }
                    else {
                        $scope.FechaInicial = $scope.FechaFinal;
                    }
                }
            }

            return continuar;
        }
        $scope.CargarRemesas = function () {
            if (DatosRequeridosFiltro()) {
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Guías...");
                $timeout(function () { blockUI.message("Cargando Guías..."); ObtenerRemesas(); }, 100);
            }
        };
        function ObtenerRemesas() {
            $scope.PagRemesas.Buscando = true;
            $scope.ListadoRemesas = [];
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                FechaInicial: $scope.Filtro.FechaInicial,
                FechaFinal: $scope.Filtro.FechaFinal,
                NumeroInicial: $scope.Filtro.NumeroInicial,
                NumeroPlanilla: $scope.Filtro.NumeroDocumentoPlanilla,
                Remesa: {
                    Conductor: $scope.Filtro.Conductor
                },
                Estado: ESTADO_DEFINITIVO,
                Anulado: ESTADO_NO_ANULADO,
                OficinaActual: $scope.Filtro.Oficina,
                Sync: true
            };
            var response = RemesaGuiasFactory.ConsultarRemesasPorLegalizar(filtro);
            if (response.ProcesoExitoso === true) {
                if (response.Datos.length > 0) {
                    $scope.ListadoRemesas = response.Datos;
                    //--Validaciones Legalizacion 
                    for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                        $scope.ListadoRemesas[i].DeshabilitarLegalizar = true;//--Deshabilitar Legalizar
                        $scope.ListadoRemesas[i].DeshabilitarGestionDocumentos = false;//--Deshabilitar Legalizar
                        $scope.ListadoRemesas[i].DeshabilitarEntregaRecaudo = false;//--Deshabilitar Legalizar
                        $scope.ListadoRemesas[i].Novedad = $linq.Enumerable().From($scope.ListadoNovedadLegaliza).First('$.Codigo ==' + 22800);

                        //--CONTADO: Marca Entrega Recaudo por Defecto
                        if ($scope.ListadoRemesas[i].Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO) {
                            $scope.ListadoRemesas[i].EntregaRecaudo = true;
                            //--CONTADO creada en ruta NO Entrega Recaudo
                            if ($scope.ListadoRemesas[i].GuiaCreadaRutaConductor == CODIGO_UNO) {
                                $scope.ListadoRemesas[i].DeshabilitarEntregaRecaudo = true;
                            }
                            //--Habilitar Legalizar
                            if ($scope.ListadoRemesas[i].GestionDocumentosRemesa.length == 0) {
                                $scope.ListadoRemesas[i].DeshabilitarLegalizar = false;
                            }
                        }


                        //--CONTRA ENTREGA Marca entrega recaudo cuando (filtro conductor, entregada)
                        if ($scope.ListadoRemesas[i].Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA) {
                            if ($scope.Filtro.Conductor != undefined && $scope.Filtro.Conductor != null && $scope.Filtro.Conductor != "") {
                                if ($scope.ListadoRemesas[i].EstadoGuia.Codigo == 6030) {
                                    $scope.ListadoRemesas[i].EntregaRecaudo = true;
                                }
                            }
                            //--Habilitar Legalizar
                            if ($scope.ListadoRemesas[i].GestionDocumentosRemesa.length == 0) {
                                $scope.ListadoRemesas[i].DeshabilitarLegalizar = false;
                            }
                        }

                        //--Gestion Documentos Remesa
                        for (var j = 0; j < $scope.ListadoRemesas[i].GestionDocumentosRemesa.length; j++) {
                            var gedr = $scope.ListadoRemesas[i].GestionDocumentosRemesa[j];
                            gedr.Recibido = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Recibido);
                            gedr.Entregado = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Entregado);
                            gedr.Legalizado = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Legalizado);
                        }
                        //--Habilitar Legalizar Credito
                        if ($scope.ListadoRemesas[i].Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                            $scope.ListadoRemesas[i].EntregaCartera = true;
                            if ($scope.ListadoRemesas[i].GestionDocumentosRemesa.length == 0) {
                                $scope.ListadoRemesas[i].DeshabilitarLegalizar = false;
                            }
                            else if ($scope.ListadoRemesas[i].GestionDocumentosLegalizar > 0) {
                                $scope.ListadoRemesas[i].DeshabilitarLegalizar = false;
                            }
                        }
                        //----Legalizaciones Previas
                        if ($scope.ListadoRemesas[i].GestionDocumentosLegalizar > 0) { //--Legalizacion Previa Con Gestion Documentos Documentos
                            $scope.ListadoRemesas[i].DeshabilitarGestionDocumentos = true;
                            $scope.ListadoRemesas[i].GestionDocumentos = true;
                        }

                        if ($scope.ListadoRemesas[i].EntregaRecaudoLegalizar > 0) { //--Legalizacion Previa Recaudo
                            $scope.ListadoRemesas[i].DeshabilitarEntregaRecaudo = true;
                            $scope.ListadoRemesas[i].EntregaRecaudo = true;
                        }
                        if ($scope.ListadoRemesas[i].GestionDocumentosLegalizar > 0 && $scope.ListadoRemesas[i].EntregaRecaudoLegalizar > 0) {
                            $scope.ListadoRemesas[i].DeshabilitarLegalizar = false;
                        }
                    }
                    $scope.PagRemesas.array = $scope.ListadoRemesas;
                    $scope.PagRemesas.ResultadoSinRegistros = '';
                    ResetPaginacion($scope.PagRemesas);
                }
                else {
                    $scope.PagRemesas.totalRegistros = 0;
                    $scope.PagRemesas.totalPaginas = 0;
                    $scope.PagRemesas.paginaActual = 1;
                    $scope.PagRemesas.array = [];
                    ResetPaginacion($scope.PagRemesas);
                    $scope.PagRemesas.ResultadoSinRegistros = 'No hay datos para mostrar';
                }
                $scope.Calcular();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
            }
            else {
                ShowError(response.statusText);
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
            }
            $scope.PagRemesas.Buscando = false;
        }
        //--Cargar Remesas
        //--Limpiar Filtro Remesas
        $scope.LimpiarRemesas = function () {
            $scope.ListadoRemesas = [];
        };
        //--Limpiar Filtro Remesas
        //--Eliminar Remesa
        $scope.EliminarGuiaGuardada = function (item) {
            var index = 0;
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                if ($scope.ListadoRemesasGuardadas[i].Remesa.Numero == item.Remesa.Numero) {
                    index = i;
                    break;
                }
            }
            var ResponBorraGuia = LegalizarRemesasPaqueteriaFactory.EliminarRemesa({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                Detalle: [{ Remesa: { Remesa: { Numero: item.Remesa.Numero } } }],
                Sync: true
            });
            if (ResponBorraGuia.ProcesoExitoso == true) {
                $scope.ListadoRemesasGuardadas.splice(index, 1);
                $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
                var tmpPagAct = $scope.PagRemesasGuardadas.paginaActual;
                $scope.Calcular();
                ResetPaginacion($scope.PagRemesasGuardadas);
                if (tmpPagAct != 1) {
                    $scope.PagRemesasGuardadas.paginaActual = tmpPagAct - 1;
                    $scope.Siguiente($scope.PagRemesasGuardadas);
                }
            }
            else {
                ShowError(ResponBorraGuia.statusText);
            }
        };
        //--Eliminar Remesa
        //----------------------------REMESAS-------------------------------//
        //--Validar Legalizacion
        $scope.validarLegalizcion = function (Reme) {
            switch (Reme.Remesa.FormaPago.Codigo) {
                case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO:
                    if (Reme.GestionDocumentos) {
                        Reme.DeshabilitarLegalizar = false;
                    }
                    else if (Reme.GestionDocumentosRemesa.length > 0) {
                        Reme.DeshabilitarLegalizar = true;
                        Reme.Legalizo = false;
                    }
                    break;
                case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO:
                case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA:
                    if (Reme.EntregaRecaudo && (Reme.GestionDocumentosRemesa.length == 0 || Reme.GestionDocumentos)) {
                        Reme.DeshabilitarLegalizar = false;
                    }
                    else {
                        Reme.DeshabilitarLegalizar = true;
                        Reme.Legalizo = false;
                    }
                    break;
            }
            $scope.Calcular();
        };
        //--Validar Legalizacion
        //--Calcular
        $scope.Calcular = function () {
            var Cantidad = 0;
            var Peso = 0;
            var ContraEntregas = 0;
            var Contado = 0;
            var Credito = 0;
            //--Nuevas Remesas
            for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                var Reme = $scope.ListadoRemesas[i];
                var AplicaCalculo = false;
                var CountValido = 0;
                switch (Reme.Remesa.FormaPago.Codigo) {
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO:
                        if (Reme.GestionDocumentos || Reme.GestionDocumentosRemesa.length == 0) {
                            Credito += Reme.Remesa.TotalFleteCliente;
                            AplicaCalculo = true;
                        }
                        break;
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO:
                        CountValido += Reme.EntregaRecaudo ? 1 : 0;
                        if (Reme.GestionDocumentosRemesa.length > 0) {
                            if (Reme.GestionDocumentos) {
                                CountValido += 1;
                            }
                        }
                        if (CountValido > 0) {
                            Contado += Reme.Remesa.TotalFleteCliente;
                            AplicaCalculo = true;
                        }
                        break;
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA:
                        CountValido += Reme.EntregaRecaudo ? 1 : 0;
                        if (Reme.GestionDocumentosRemesa.length > 0) {
                            if (Reme.GestionDocumentos) {
                                CountValido += 1;
                            }
                        }
                        if (CountValido > 0) {
                            ContraEntregas += Reme.Remesa.TotalFleteCliente;
                            AplicaCalculo = true;
                        }
                        break;
                }
                if (AplicaCalculo) {
                    Cantidad += Reme.Remesa.CantidadCliente;
                    Peso += Reme.Remesa.PesoCliente;
                }
            }
            //--Nuevas Remesas
            //--Remesas Guardadas
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                var Reme = $scope.ListadoRemesasGuardadas[i];
                var AplicaCalculo = false;
                var CountValido = 0;
                switch (Reme.Remesa.FormaPago.Codigo) {
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO:
                        if (Reme.GestionDocumentos || Reme.GestionDocumentosRemesa.length == 0) {
                            Credito += Reme.Remesa.TotalFleteCliente;
                            AplicaCalculo = true;
                        }
                        break;
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO:
                        CountValido += Reme.EntregaRecaudo ? 1 : 0;
                        if (Reme.GestionDocumentosRemesa.length > 0) {
                            if (Reme.GestionDocumentos) {
                                CountValido += 1;
                            }
                        }
                        if (CountValido > 0) {
                            Contado += Reme.Remesa.TotalFleteCliente;
                            AplicaCalculo = true;
                        }
                        break;
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA:
                        CountValido += Reme.EntregaRecaudo ? 1 : 0;
                        if (Reme.GestionDocumentosRemesa.length > 0) {
                            if (Reme.GestionDocumentos) {
                                CountValido += 1;
                            }
                        }
                        if (CountValido > 0) {
                            ContraEntregas += Reme.Remesa.TotalFleteCliente;
                            AplicaCalculo = true;
                        }
                        break;
                }
                if (AplicaCalculo) {
                    Cantidad += Reme.Remesa.CantidadCliente;
                    Peso += Reme.Remesa.PesoCliente;
                }
            }
            //--Remesas Guardadas
            $scope.Modelo.Cantidad = MascaraValores(Cantidad);
            $scope.Modelo.Peso = MascaraValores(Peso);
            $scope.Modelo.TotalRecaudado = MascaraValores(ContraEntregas + Contado);
            $scope.Modelo.TotalContraEntregas = MascaraValores(ContraEntregas);
            $scope.Modelo.TotalContado = MascaraValores(Contado);
            $scope.Modelo.TotalCredito = MascaraValores(Credito);
        };
        //--Calcular
        //--Obtener
        function Obtener() {
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero
            };
            LegalizarRemesasPaqueteriaFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = angular.copy(response.data.Datos);
                        $scope.Modelo.Fecha = new Date($scope.Modelo.Fecha);
                        for (var i = 0; i < response.data.Datos.Detalle.length; i++) {
                            var det = response.data.Datos.Detalle[i];
                            for (var j = 0; j < det.Remesa.GestionDocumentosRemesa.length; j++) {
                                var gedr = det.Remesa.GestionDocumentosRemesa[j];
                                gedr.Recibido = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Recibido);
                                gedr.Entregado = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Entregado);
                                gedr.Legalizado = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Legalizado);
                            }

                            $scope.ListadoRemesasGuardadas.push({
                                DeshabilitarLegalizar: true,
                                Remesa: det.Remesa.Remesa,
                                GestionDocumentosRemesa: det.Remesa.GestionDocumentosRemesa,
                                OficinaActual: det.Remesa.OficinaActual,
                                NumeroPlanilla: det.Remesa.NumeroPlanilla,
                                FechaPlanillaInicial: det.Remesa.FechaPlanillaInicial,
                                EstadoGuia: det.Remesa.EstadoGuia,
                                NumeroDocumentoLegalizarRecaudo: det.Remesa.NumeroDocumentoLegalizarRecaudo,
                                GuiaCreadaRutaConductor: det.Remesa.GuiaCreadaRutaConductor,
                                Novedad: $linq.Enumerable().From($scope.ListadoNovedadLegaliza).First('$.Codigo ==' + det.NovedadLegalizarRemesas.Codigo),
                                Legalizo: det.Legalizo == 1 ? true : false,
                                GestionDocumentos: det.GestionDocumentos == 1 ? true : false,
                                EntregaRecaudo: det.EntregaRecaudo == 1 ? true : false,
                                EntregaArchivo: det.EntregaArchivo == 1 ? true : false,
                                EntregaCartera: det.EntregaCartera == 1 ? true : false,
                                Observaciones: det.Observaciones
                            });
                        }
                        //--Habilita o no Legalizar
                        for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                            $scope.validarLegalizcion($scope.ListadoRemesasGuardadas[i]);
                        }

                        $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
                        ResetPaginacion($scope.PagRemesasGuardadas);

                        if (response.data.Datos.Anulado == ESTADO_ANULADO) {
                            $scope.ListadoEstados.push({ Nombre: 'ANULADO', Codigo: 2 });
                            $scope.Modelo.Estado = $scope.ListadoEstados[2];
                            $scope.Deshabilitar = true;
                        } else {
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.Deshabilitar = true;
                            }
                            else {
                                $scope.Calcular();
                            }
                        }

                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        $scope.MaskValores();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    blockUI.stop();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } document.location.href = '#!ConsultarPlanillaPaqueteria'; }, 500);
                });
        }
        //--Obtener
        //--Guardar
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo;
            //--Conteo Remesas
            var countReme = 0;
            //--Remesas Nuevas
            for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                var Reme = $scope.ListadoRemesas[i];
                var CountValido = 0;
                var RomperFor = false;
                switch (Reme.Remesa.FormaPago.Codigo) {
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO:
                        if (Reme.GestionDocumentos || Reme.GestionDocumentosRemesa.length == 0) {
                            CountValido += 1;
                            countReme += 1;
                        }
                        break;
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO:
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA:
                        CountValido += Reme.EntregaRecaudo ? 1 : 0;
                        if (Reme.GestionDocumentosRemesa.length > 0) {
                            if (Reme.GestionDocumentos) {
                                CountValido += 1;
                            }
                        }
                        if (CountValido > 0) {
                            countReme += 1;
                        }
                        break;
                }
                if (Reme.GestionDocumentosRemesa.length > 0 && CountValido > 0 && Reme.GestionDocumentos) {
                    for (var j = 0; j < Reme.GestionDocumentosRemesa.length; j++) {
                        var GestionDocumentos = Reme.GestionDocumentosRemesa[j];
                        if (GestionDocumentos.Legalizado.Codigo == -1) {
                            RomperFor = true;
                            $scope.MensajesError.push('Falta Realizar la Gestion Documentos en la remesa ' + Reme.Remesa.NumeroDocumento);
                            continuar = false;
                            break;
                        }
                    }
                }
                if (RomperFor) {
                    break;
                }
            }
            //--Remesas Nuevas
            //--Remesas Guardadas
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                var Reme = $scope.ListadoRemesasGuardadas[i];
                var CountValido = 0;
                var RomperFor = false;
                switch (Reme.Remesa.FormaPago.Codigo) {
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO:
                        if (Reme.GestionDocumentos || Reme.GestionDocumentosRemesa.length == 0) {
                            CountValido += 1;
                            countReme += 1;
                        }
                        break;
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO:
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA:
                        CountValido += Reme.EntregaRecaudo ? 1 : 0;
                        if (Reme.GestionDocumentosRemesa.length > 0) {
                            if (Reme.GestionDocumentos) {
                                CountValido += 1;
                            }
                        }
                        if (CountValido > 0) {
                            countReme += 1;
                        }
                        break;
                }
                if (Reme.GestionDocumentosRemesa.length > 0 && CountValido > 0 && Reme.GestionDocumentos) {
                    for (var j = 0; j < Reme.GestionDocumentosRemesa.length; j++) {
                        var GestionDocumentos = Reme.GestionDocumentosRemesa[j];
                        if (GestionDocumentos.Legalizado.Codigo == -1) {
                            RomperFor = true;
                            $scope.MensajesError.push('Falta Realizar la Gestion Documentos en la remesa guardada ' + Reme.Remesa.NumeroDocumento);
                            continuar = false;
                            break;
                        }
                    }
                }
                if (RomperFor) {
                    break;
                }
            }
            //--Remesas Guardadas
            if (countReme <= 0) {
                $scope.MensajesError.push('Debe ingresar al menos una guía');
                continuar = false;
            }
            //--Conteo Remesas
            return continuar;
        }
        $scope.ConfirmacionGuardar = function () {
            if ($scope.DeshabilitarActualizar) {
                location.href = $scope.Master + '/' + $scope.Modelo.NumeroDocumento;
            } else {
                showModal('modalConfirmacionGuardar');
            }
        };
        $scope.ValidarGuardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                PantallaBloqueo(Guardar, 'Generando Legalizacion...');
            }
        };
        function Guardar() {
            //--Remesas
            $scope.Calcular();
            var ListaDetalleRemesas = [];
            var tmpGestionDocumentos = [];
            var guia;
            if ($scope.ListadoRemesasGuardadas !== undefined && $scope.ListadoRemesasGuardadas !== null && $scope.ListadoRemesasGuardadas !== '') {
                for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                    guia = $scope.ListadoRemesasGuardadas[i];
                    var CountValido = 0;
                    switch (guia.Remesa.FormaPago.Codigo) {
                        case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO:
                            if (guia.GestionDocumentos || guia.GestionDocumentosRemesa.length == 0) {
                                CountValido += 1;
                            }
                            break;
                        case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO:
                        case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA:
                            CountValido += guia.EntregaRecaudo ? 1 : 0;
                            if (guia.GestionDocumentosRemesa.length > 0) {
                                if (guia.GestionDocumentos) {
                                    CountValido += 1;
                                }
                            }
                            break;
                    }

                    if (CountValido > 0) {
                        tmpGestionDocumentos = [];
                        for (var j = 0; j < guia.GestionDocumentosRemesa.length; j++) {
                            var item = guia.GestionDocumentosRemesa[j];
                            tmpGestionDocumentos.push({
                                TipoDocumento: { Codigo: item.TipoDocumento.Codigo },
                                TipoGestion: { Codigo: item.TipoGestion.Codigo },
                                Legalizado: item.Legalizado.Codigo,
                                Observaciones: item.Observaciones
                            });
                        }
                        ListaDetalleRemesas.push({
                            Remesa: {
                                Remesa: { Numero: guia.Remesa.Numero },
                                GestionDocumentosRemesa: tmpGestionDocumentos
                            },
                            NovedadLegalizarRemesas: { Codigo: guia.Novedad.Codigo },
                            GestionDocumentos: guia.GestionDocumentos == true ? 1 : 0,
                            EntregaRecaudo: guia.EntregaRecaudo == true ? 1 : 0,
                            EntregaArchivo: guia.EntregaArchivo == true ? 1 : 0,
                            EntregaCartera: guia.EntregaCartera == true ? 1 : 0,
                            Observaciones: guia.Observaciones,
                            Legalizo: guia.Legalizo == true ? 1 : 0
                        });
                    }
                }
            }
            //--Remesas Guardadas
            //--Remesas Nuevas
            if ($scope.ListadoRemesas !== undefined && $scope.ListadoRemesas !== null && $scope.ListadoRemesas !== '') {
                for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                    guia = $scope.ListadoRemesas[i];
                    var CountValido = 0;
                    switch (guia.Remesa.FormaPago.Codigo) {
                        case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO:
                            if (guia.GestionDocumentos || guia.GestionDocumentosRemesa.length == 0) {
                                CountValido += 1;
                            }
                            break;
                        case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO:
                        case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA:
                            CountValido += guia.EntregaRecaudo ? 1 : 0;
                            if (guia.GestionDocumentosRemesa.length > 0) {
                                if (guia.GestionDocumentos) {
                                    CountValido += 1;
                                }
                            }
                            break;
                    }

                    if (CountValido > 0) {
                        tmpGestionDocumentos = [];
                        for (var j = 0; j < guia.GestionDocumentosRemesa.length; j++) {
                            var item = guia.GestionDocumentosRemesa[j];
                            tmpGestionDocumentos.push({
                                TipoDocumento: { Codigo: item.TipoDocumento.Codigo },
                                TipoGestion: { Codigo: item.TipoGestion.Codigo },
                                Legalizado: item.Legalizado.Codigo,
                                Observaciones: item.Observaciones
                            });
                        }
                        ListaDetalleRemesas.push({
                            Remesa: {
                                Remesa: { Numero: guia.Remesa.Numero },
                                GestionDocumentosRemesa: tmpGestionDocumentos
                            },
                            NovedadLegalizarRemesas: { Codigo: guia.Novedad.Codigo },
                            GestionDocumentos: guia.GestionDocumentos == true ? 1 : 0,
                            EntregaRecaudo: guia.EntregaRecaudo == true ? 1 : 0,
                            EntregaArchivo: guia.EntregaArchivo == true ? 1 : 0,
                            EntregaCartera: guia.EntregaCartera == true ? 1 : 0,
                            Observaciones: guia.Observaciones,
                            Legalizo: guia.Legalizo == true ? 1 : 0
                        });
                    }
                }
            }
            //--Remesas Nuevas
            //--Genera Obj Legalizacion
            var Legalizacion = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_LEGALIZACION_REMESAS,
                Numero: $scope.Modelo.Numero,
                Fecha: $scope.Modelo.Fecha,
                Observaciones: $scope.Modelo.Observaciones,
                Cantidad: $scope.Modelo.Cantidad,
                Peso: $scope.Modelo.Peso,
                TotalContado: $scope.Modelo.TotalContado,
                TotalContraEntregas: $scope.Modelo.TotalContraEntregas,
                TotalCredito: $scope.Modelo.TotalCredito,
                TotalRecaudoTercero: $scope.Modelo.TotalRecaudoTercero,
                Estado: $scope.Modelo.Estado.Codigo,
                Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Detalle: ListaDetalleRemesas
            };
            LegalizarRemesasPaqueteriaFactory.Guardar(Legalizacion).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > CERO) {
                            if ($scope.Modelo.Numero == CERO) {
                                ShowSuccess('Se guardó la legalización con el número ' + response.data.Datos);
                            }
                            else {
                                ShowSuccess('Se modificó la legalización con el número ' + response.data.Datos);
                            }
                            $timeout(function () {
                                blockUI.stop();
                                blockUIConfig.autoBlock = false;
                                if (blockUI.state().blocking == true) { blockUI.reset(); }
                                location.href = $scope.Master + '/' + response.data.Datos;
                            }, 500);
                        }
                        else {
                            if (response.data.Datos == -2) {
                                ShowError("Existen guías ya asociadas a otra legalización");
                            }
                            else {
                                ShowError(response.statusText);
                            }

                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                });
        }
        //--Guardar
        ////--Controles
        //$scope.listaDocumentosGestion = function (str) {
        //    if ($(str).hasClass('visible')) {
        //        $(str).removeClass('visible');
        //    }
        //    else {
        //        $(str).addClass('visible');
        //    }
        //};
        ////--Controles
        //--Control Entregas
        $scope.ConsultarControlEntregas = function (item) {
            $scope.ListadoDocumentos = [];

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            };
            RemesaGuiasFactory.ObtenerControlEntregas(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Remesa.NombreRecibe != null && response.data.Datos.Remesa.NumeroIdentificacionRecibe != null && response.data.Datos.Remesa.TelefonoRecibe != null) {
                            $scope.FotosEntrega = [];
                            $scope.FotosDevolucion = [];
                            //$scope.FotoControlEntrega = response.data.Datos.Remesa.Fotografia;
                            //$scope.FotoControlEntrega.FotoCargada = 'data:' + response.data.Datos.Remesa.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Remesa.Fotografia.FotoBits;
                            $scope.FotosControlEntrega = response.data.Datos.Remesa.Fotografias;
                            $scope.FotosControlEntrega.forEach(foto => {
                                foto.FotoCargada = 'data:' + foto.TipoFoto + ';base64,' + foto.FotoBits;
                                if (foto.EntregaDevolucion == 1) {
                                    $scope.FotosDevolucion.push(foto);
                                } else if (foto.EntregaDevolucion == 0) {
                                    $scope.FotosEntrega.push(foto);
                                }
                            });
                            $scope.ControlEntregaRemesa = response.data.Datos.Remesa;

                            $scope.ControlEntregaRemesa.UsuarioEntrega = response.data.Datos.UsuarioEntrega;
                            $scope.ControlEntregaRemesa.OficinaEntrega = response.data.Datos.OficinaEntrega.Nombre;
                            $scope.ControlEntregaRemesa.MostrarEntregaOFicina = response.data.Datos.OficinaEntrega.Codigo > 0 ? true : false;
                            $scope.ControlEntregaRemesa.Latitud = response.data.Datos.Latitud;
                            $scope.ControlEntregaRemesa.Longitud = response.data.Datos.Longitud;

                            $scope.ControlEntregaRemesa.VehiculoNombre = `${$scope.ControlEntregaRemesa.Vehiculo.Placa} (${$scope.ControlEntregaRemesa.Vehiculo.CodigoAlterno})`
                            $scope.ControlEntregaRemesa.PlanillaEntrega = response.data.Datos.PlanillaEntrega

                            if (response.data.Datos.DocumentosCumplidoRemsa != undefined) {
                                $scope.ListadoDocumentos = response.data.Datos.DocumentosCumplidoRemsa;
                                $scope.ListadoDocumentos.forEach(function (item) {
                                    item.Numero = $scope.ControlEntregaRemesa.Numero;
                                    item.temp = false;
                                    item.ValorDocumento = 1;
                                    item.AplicaEliminar = 1;
                                    item.AplicaArchivo = 1;
                                });
                            }
                            $scope.ControlEntregaRemesa.FirmaImg = "data:image/png;base64," + $scope.ControlEntregaRemesa.Firma;
                            showModal('modalControlEntregas');
                        }
                        else {
                            ShowError('El documento no cuenta con un control de entregas');
                        }
                    }
                    else {
                        ShowError('El documento no cuenta con un control de entregas');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        $scope.VerFotosEntrega = function () {
            showModal('modalFotosEntregas');
        };
        $scope.VerFotosDevolucion = function () {
            showModal('modalFotosDevolucion');
        };
        //--Control Entregas
        //--Gestionar Entregas
        $scope.GestionarControlEntregas = function (item) {
            if (item.Estado === 1 && item.Anulado === 0 && (item.EstadoGuia.Codigo == 6010 || item.EstadoGuia.Codigo == 6005)) {
                //var url = document.location.href;
                //url = url.replace("GestionarLegalizarRemesasPaqueteria", "EntregaOficina");
                //window.open(url + "/" + item.Remesa.NumeroDocumento, '_blank');
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Detalles...");
                $timeout(function () { blockUI.message("Cargando Detalles..."); $scope.BuscarRemesa(item.Remesa.Numero); }, 100);
            }
            else {
                ShowError('No se puede entregar la Remesa porque su estado debe corresponder al de “OFICINA” 0 “DESPACHADA”');
            }
        };
        //--Gestionar Entregas
        //--Gestion Documentos Remesa
        $scope.MostrarGestionDocumentosRemesa = function (item) {
            $scope.ListaGestionDocuRemesa = [];
            $scope.ListaGestionDocuRemesa = angular.copy(item.GestionDocumentosRemesa);
            for (var i = 0; i < $scope.ListaGestionDocuRemesa.length; i++) {
                var gedr = $scope.ListaGestionDocuRemesa[i];
                gedr.Recibido = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Recibido.Codigo);
                gedr.Entregado = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Entregado.Codigo);
                gedr.Legalizado = $linq.Enumerable().From($scope.ListadoConfirma).First('$.Codigo ===' + gedr.Legalizado.Codigo);
            }
            TmpNumeroRemesa = item.Remesa.Numero;
            showModal('modalGestionDocumentosRemesa');
        };
        $scope.ValidarGuardarGestionDocumentos = function () {
            for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                if (TmpNumeroRemesa == $scope.ListadoRemesas[i].Remesa.Numero) {
                    $scope.ListadoRemesas[i].GestionDocumentosRemesa = $scope.ListaGestionDocuRemesa;
                    break;
                }
            }
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                if (TmpNumeroRemesa == $scope.ListadoRemesasGuardadas[i].Remesa.Numero) {
                    $scope.ListadoRemesasGuardadas[i].GestionDocumentosRemesa = $scope.ListaGestionDocuRemesa;
                    break;
                }
            }
            closeModal('modalGestionDocumentosRemesa');
        };
        //--Gestion Documentos Remesa
        //--Mostrar Legalizaciones
        $scope.MostrarLegalizarGuias = function (item) {
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                DetalleRemesa: { Remesa: { Numero: item.Remesa.Numero } },
                Sync: true
            };
            var response = LegalizarRemesasPaqueteriaFactory.ConsultarOtrasLegalizaciones(filtro);
            if (response.ProcesoExitoso === true) {
                if (response.Datos.length > 0) {
                    $scope.ListaOtrasLegalizaciones = response.Datos;
                    for (var i = 0; i < $scope.ListaOtrasLegalizaciones.length; i++) {
                        var det = $scope.ListaOtrasLegalizaciones[i];
                        det.DetalleRemesa.EntregaRecaudo = det.DetalleRemesa.EntregaRecaudo == 1 ? true : false;
                        det.DetalleRemesa.GestionDocumentos = det.DetalleRemesa.GestionDocumentos == 1 ? true : false;
                        det.DetalleRemesa.EntregaArchivo = det.DetalleRemesa.EntregaArchivo == 1 ? true : false;
                        det.DetalleRemesa.EntregaCartera = det.DetalleRemesa.EntregaCartera == 1 ? true : false;
                        det.DetalleRemesa.Novedad = $linq.Enumerable().From($scope.ListadoNovedadLegaliza).First('$.Codigo ==' + det.DetalleRemesa.NovedadLegalizarRemesas.Codigo);
                    }
                }
                else {
                    $scope.MensajesErrorOtrLegaliza.push("No se encontraron Legalizaciones relacionadas");
                }
            }
            else {
                $scope.MensajesErrorOtrLegaliza.push("No se encontraron Legalizaciones relacionadas");
            }
            showModal('modalMostrarOtrasLegalizaciones');
        };
        //--Mostrar Legalizaciones
        //--Limpiar Oficinas
        $scope.LimpiarOficina = function () {
            if ($scope.Filtro.Conductor != undefined && $scope.Filtro.Conductor != '' && $scope.Filtro.Conductor != null) {
                $scope.Filtro.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
            }
        };
        //--Limpiar Conductor
        $scope.LimpiarConductor = function () {
            if ($scope.Filtro.Oficina != undefined && $scope.Filtro.Oficina != '' && $scope.Filtro.Oficina != null) {
                if ($scope.Filtro.Oficina.Codigo != -1) {
                    $scope.Filtro.Conductor = "";
                }
            }
        };
        //--Validar Entrega Archivo
        $scope.ValidarEntregaArchivo = function (item) {
            if (item.EntregaArchivo == true) {
                item.EntregaCartera = false;
            }
        };
        //--Validar Entrega Cartera
        $scope.ValidarEntregaCartera = function (item) {
            if (item.EntregaCartera == true) {
                item.EntregaArchivo = false;
            }
        };
        //---------------Gestion Control Entregas
        $scope.BuscarRemesa = function (Numero) {
            var response = RemesaGuiasFactory.Obtener({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: Numero,
                Sync: true
            });
            if (response.ProcesoExitoso) {
                // limpia campos
                $scope.Modal.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas;
                $scope.Modal.FechaRecibe = new Date();
                $scope.Modal.NumeroIdentificacion = '';
                $scope.Modal.Nombre = '';
                $scope.Modal.Telefono = '';
                $scope.Modal.NovedadEntrega = '';
                $scope.Modal.Observaciones = '';
                $scope.Modal.UsuarioEntrega = ''
                $scope.ListadoDocumentos = [];
                $scope.MensajesErrorAdjuntar = [];
                // limpia campos

                $scope.Modal.Remitente = response.Datos.Remesa.Remitente.Nombre;
                $scope.Modal.CiudadOrigen = $scope.CargarCiudad(response.Datos.Remesa.Remitente.Ciudad.Codigo).Nombre;
                $scope.Modal.CiudadDestino = $scope.CargarCiudad(response.Datos.Remesa.Destinatario.Ciudad.Codigo).Nombre;

                $scope.Modal.FormaPago = $linq.Enumerable().From(ListaFormaPago).First('$.Codigo ==' + response.Datos.Remesa.FormaPago.Codigo).Nombre;
                $scope.Modal.Producto = $scope.CargarProducto(response.Datos.Remesa.ProductoTransportado.Codigo).Nombre;
                $scope.Modal.TotalValor = $scope.MaskValoresGrid(response.Datos.Remesa.TotalFleteCliente);
                $scope.Modal.Peso = $scope.MaskValoresGrid(response.Datos.Remesa.PesoCliente);
                $scope.Modal.Cantidad = $scope.MaskValoresGrid(response.Datos.Remesa.CantidadCliente);
                $scope.Modal.Remesa = parseInt(response.Datos.Remesa.Numero);
                $scope.Modal.NumeroDocumento = response.Datos.Remesa.NumeroDocumento;
                $scope.Modal.Fecha = response.Datos.Remesa.Fecha;

                $scope.ListaGestionDocuCliente = response.Datos.Remesa.GestionDocumentosRemesa;
                if ($scope.ListaGestionDocuCliente.length > 0) {
                    for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                        if ($scope.ListaGestionDocuCliente[i].TipoGestion.Nombre.toLowerCase().includes("firma")) {
                            $scope.MensajesErrorAdjuntar.push('Adjuntar soporte de gestión documental');
                            break;
                        }
                    }
                }

                // cargue usuarios que reciben
                $scope.ListadoUsuariosEntrega = [];
                $scope.ListadoUsuariosEntrega.push({ Nombre: $scope.Sesion.UsuarioAutenticado.Nombre, Codigo: $scope.Sesion.UsuarioAutenticado.Codigo });
                var ListadoEstadoGuias = RemesaGuiasFactory.ConsultarEstadosRemesa({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Modal.Remesa
                    , Sync: true
                }).Datos
                var estadoUltimoResponsable = ListadoEstadoGuias[ListadoEstadoGuias.length - 1]
                var resp = $scope.Sesion.UsuarioAutenticado.Nombre
                if (estadoUltimoResponsable.EstadoRemesa.Codigo == 6005) {
                    resp = estadoUltimoResponsable.Aforador
                } else if (estadoUltimoResponsable.EstadoRemesa.Codigo == 6010) {
                    resp = estadoUltimoResponsable.Conductor
                } else if (estadoUltimoResponsable.EstadoRemesa.Codigo == 6035) {
                    resp = estadoUltimoResponsable.RespOficina
                } else if (estadoUltimoResponsable.EstadoRemesa.Codigo == 6030) {
                    resp = respestadoUltimoResponsable.UsuarioCreaPlanilla
                }
                if (!resp.includes($scope.Sesion.UsuarioAutenticado.Nombre) && resp !== '' && resp !== "   " && resp !== undefined && resp !== null) {
                    var responsable = UsuariosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Nombre: resp.trim(),
                        Login: -1,
                        CodigoUsuario: undefined,
                        Codigo: undefined,
                        AplicacionUsuario: 204,
                        Pagina: 1,
                        RegistrosPagina: 1,
                        Sync: true
                    }).Datos;
                    if (responsable.length > 0) {
                        $scope.ListadoUsuariosEntrega.push(responsable[0])
                    }
                }
                // cargue usuarios que reciben

                $scope.ModalDestinatario.Nombre = response.Datos.Remesa.Destinatario.Nombre;
                $scope.ModalDestinatario.Telefono = response.Datos.Remesa.Destinatario.Telefonos;
                $scope.ModalDestinatario.Direccion = response.Datos.Remesa.Destinatario.Direccion;
                $scope.ModalDestinatario.NumeroIdentificacion = response.Datos.Remesa.Destinatario.NumeroIdentificacion;
                $scope.ModalDestinatario.Barrio = response.Datos.Remesa.BarrioDestinatario;
                $scope.CodigoTipoIdentificacionRecibe = response.Datos.CodigoTipoIdentificacionDestinatario;
                $scope.ModalDestinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRecibe);
                showModal('modalGestionarControlEntregas');

            }
            else {
                ShowError(response.statusText);
            }
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
        };
        $scope.LimpiarFirma = function () {
            try {
                var canvas = document.getElementById('Firma');
                var context = canvas.getContext('2d');
                context.clearRect(0, 0, canvas.width, canvas.height);
            } catch (e) {

            }
        };
        $scope.ConfirmacionGuardarPaqueteria = function () {
            if (DatosRequeridosEntregas()) {
                ShowConfirm('¿Está seguro de guardar la información?', $scope.GuardarPaqueteria);
            }
        };
        $scope.GuardarPaqueteria = function () {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Guardando Entrega...");
            $timeout(function () { blockUI.message("Guardando Entrega..."); }, 100);
            //closeModal('modalConfirmacionGuardarPaqueteria', 1);
            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            $scope.Firma = image.src.replace(/data:image\/png;base64,/g, '');

            $scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);

            var GestionDocumentosRemesa = [];
            if ($scope.ListaGestionDocuCliente.length > 0) {
                for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                    var item = $scope.ListaGestionDocuCliente[i];
                    GestionDocumentosRemesa.push({
                        TipoDocumento: item.TipoDocumento,
                        TipoGestion: item.TipoGestion,
                        Entregado: item.Entregado.Codigo
                    });
                }
            }

            //--Documentos Remesas
            if ($scope.ListadoDocumentos != undefined && $scope.ListadoDocumentos != null) {
                if ($scope.ListadoDocumentos.length > 0) {
                    for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                        if ($scope.ListadoDocumentos[i].Id != undefined && $scope.ListadoDocumentos[i].Id != null && $scope.ListadoDocumentos[i].Id != '') {
                            DocumentosFactory.InsertarDocumentosCumplidoRemesa({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Id: $scope.ListadoDocumentos[i].Id,
                                Numero: $scope.Modal.Remesa,
                            }).then(function (response) {
                                if (response.data.ProcesoExitoso === false) {
                                    ShowError(response.data.MensajeError);
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                        }
                    }
                }
            }
            //--Documentos Remesas

            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //Codigo: $scope.Codigo,
                Numero: $scope.Modal.Remesa,
                Oficina: $scope.Modal.Oficina,
                FechaRecibe: $scope.Modal.FechaRecibe,
                CantidadRecibe: $scope.Modal.Cantidad,
                PesoRecibe: $scope.Modal.Peso,
                CodigoTipoIdentificacionRecibe: $scope.Modal.TipoIdentificacionRecibe.Codigo,
                NumeroIdentificacionRecibe: parseInt($scope.Modal.NumeroIdentificacion),
                NombreRecibe: $scope.Modal.Nombre,
                TelefonoRecibe: $scope.Modal.Telefono,
                NovedadEntrega: $scope.Modal.NovedadEntrega,
                ObservacionesRecibe: $scope.Modal.Observaciones,
                //Fotografia: $scope.Foto[0],
                Fotografias: $scope.Foto,
                Firma: $scope.Firma,
                //UsuarioCrea: { Codigo: $scope.Modal.UsuarioEntrega.Codigo },
                UsuarioModifica: { Codigo: $scope.Modal.UsuarioEntrega.Codigo },
                DevolucionRemesa: $scope.DevolucionRemesa,
                GestionDocumentosRemesa: GestionDocumentosRemesa
            };

            RemesaGuiasFactory.GuardarEntrega($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.DevolucionRemesa == 0) {
                                ShowSuccess('La remesa fue recibida por ' + $scope.Modal.Nombre);
                            } else {
                                ShowSuccess('Se realizó la devolución de la remesa satisfactoriamente');
                            }

                            $scope.Foto = [];
                            $scope.LimpiarFirma();

                            $scope.CargarRemesas();
                            closeModal('modalGestionarControlEntregas');
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                }, function (response) {
                    ShowError(response.statusText);
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);

                });
        };


        $scope.CargarImagen = function (element) {
            var continuar = true;
            $scope.TipoImagen = element
            $scope.NombreDocumento = element.files[0].name
            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato JPEG, PNG", false);
                continuar = false;
            }

            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png') {
                    ShowError("Solo se pueden cargar archivos en formato JPEG, PNG ", false);
                    continuar = false;
                }
            }

            if (continuar == true) {
                var reader = new FileReader();
                reader.onload = $scope.AsignarImagenListado;
                reader.readAsDataURL(element.files[0]);
            }

        }

        $scope.AsignarImagenListado = function (e) {
            try {
                RedimHorizontal('')
                RedimVertical('')
            } catch (e) {
            }
            $scope.$apply(function () {
                $scope.Convertirfoto = null
                var TipoImagen = ''

                var name = $scope.TipoImagen.files[0].name.split('.')

                $scope.NombreFoto = name[0]

                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/jpeg'
                        AsignarFotoListado()

                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/png'
                        AsignarFotoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.Convertirfoto = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoFoto = 'application/pdf'
                    $scope.TipoImagen = TipoImagen
                    AsignarFotoListado()

                }
                var input = document.getElementById('inputfile')
                input.value = ''
            });
        }

        $scope.AsignarFoto = function () {
            closeModal('ModalTomarFoto', 1)
            $scope.video.srcObject = undefined
            // $scope.Foto = [];
            $scope.FotoAux = $scope.FotoTomada.toString();
            $scope.FotoBits = $scope.FotoAux.replace(/data:image\/png;base64,/g, '')
            $scope.Foto.push({
                NombreFoto: 'Fotografía',
                TipoFoto: 'image/png',
                ExtensionFoto: 'PNG',
                FotoBits: $scope.FotoBits,
                ValorDocumento: 1,
                FotoCargada: $scope.FotoAux,
                id: $scope.Foto.length
            });
        }

        function AsignarFotoListado() {
            // $scope.Foto = [];
            $scope.Foto.push({
                NombreFoto: $scope.NombreFoto,
                TipoFoto: $scope.TipoFoto,
                ExtensionFoto: $scope.TipoImagen,
                FotoBits: $scope.Convertirfoto,
                ValorDocumento: 1,
                FotoCargada: 'data:' + $scope.TipoFoto + ';base64,' + $scope.Convertirfoto,
                id: $scope.Foto.length
            });
        }

        $scope.EliminarFoto = function (item) {
            $scope.Foto.splice(item.id, 1);
            for (var i = 0; i < $scope.Foto.length; i++) {
                $scope.Foto[i].id = i;
            }
            // $scope.Foto = []
            $scope.ModificarFoto = true;
            //$("#Foto").attr("src", "");
        }

        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvas se dibuja la imagen que se recibe por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }

        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }

        function DatosRequeridosEntregas() {
            $scope.MensajesErrorRecibir = [];
            $scope.MensajesErrorDocu = [];
            var continuar = true;

            if ($scope.Modal.Cantidad === undefined || $scope.Modal.Cantidad === '' || $scope.Modal.Cantidad === null || $scope.Modal.Cantidad === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if ($scope.Modal.Peso === undefined || $scope.Modal.Peso === '' || $scope.Modal.Peso === null || $scope.Modal.Peso === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el peso');
                continuar = false;
            }
            if ($scope.Modal.TipoIdentificacionRecibe === undefined || $scope.Modal.TipoIdentificacionRecibe === '' || $scope.Modal.TipoIdentificacionRecibe === null || $scope.Modal.TipoIdentificacionRecibe.Codigo === CODIGO_NO_APLICA_TIPO_IDENTIFICACION) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el tipo de identificación de quien recibe');

                continuar = false;
            }
            if ($scope.Modal.NumeroIdentificacion === undefined || $scope.Modal.NumeroIdentificacion === '' || $scope.Modal.NumeroIdentificacion === null || $scope.Modal.NumeroIdentificacion === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el número de identificación de quien recibe');
                continuar = false;
            }
            if ($scope.Modal.Nombre === undefined || $scope.Modal.Nombre === '' || $scope.Modal.Nombre === null || $scope.Modal.Nombre === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el nombre de quien recibe');
                continuar = false;
            }
            if ($scope.Modal.Telefono === undefined || $scope.Modal.Telefono === '' || $scope.Modal.Telefono === null || $scope.Modal.Telefono === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el teléfono de quien recibe');
                continuar = false;
            }
            if ($scope.Modal.NovedadEntrega === undefined || $scope.Modal.NovedadEntrega === '' || $scope.Modal.NovedadEntrega === null || $scope.Modal.NovedadEntrega === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la novedad de entrega');
                continuar = false;
            }
            if ($scope.Modal.UsuarioEntrega === undefined || $scope.Modal.UsuarioEntrega === '' || $scope.Modal.UsuarioEntrega === null || $scope.Modal.UsuarioEntrega === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el usuario que entrega');
                continuar = false;
            }
            //if ($scope.Modal.Observaciones === undefined || $scope.Modal.Observaciones === '' || $scope.Modal.Observaciones === null || $scope.Modal.Observaciones === 0) {
            //    $scope.MensajesErrorRecibir.push('Debe ingresar las observaciones');
            //    continuar = false;
            //}
            if ($scope.ListaGestionDocuCliente.length > 0) {
                for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                    if ($scope.ListaGestionDocuCliente[i].Entregado == 0 || $scope.ListaGestionDocuCliente[i].Entregado.Codigo == -1) {
                        $scope.MensajesErrorDocu.push('Se debe seleccionar en todos los documentos si fueron entregados o no');
                        continuar = false;
                        break;
                    }
                }
            }
            return continuar;
        }
        //-----------------------Documental------------------------------//
        $scope.AgregarFilaDocumento = function (numero) {
            if ($scope.ListadoDocumentos.length < 5) {
                var itmDoc = {
                    Descripcion: '',
                    Numero: numero,
                    AplicaDescargar: 0,
                    AplicaArchivo: 1,
                    ValorDocumento: 0,
                    TipoArchivoDocumento: $scope.CadenaFormatos
                };
                $scope.ListadoDocumentos.push(itmDoc);
            } else {
                ShowError("Máximo se permite cargar cinco (5) documentos");
            }
        };

        $scope.CargarArchivo = function (element) {
            $scope.IdPosiction = parseInt(element.id, 10);
            var continuar = true;
            if (element.files.length > 0) {
                $scope.PDF = element;
                $scope.TipoDocumento = element;
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento = angular.element(this.ItemFoto)[0];
                } else {
                    angular.element(this.item)[0].Temporal = true
                    $scope.Documento = angular.element(this.item)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos: 0");
                    continuar = false;
                }
                if (continuar == true) {
                    if (element.files[0].size >= 3000000) { //3 MB
                        ShowError("El archivo " + element.files[0].name + " supera los " + "3 MB" + ", intente con otro archivo", false);
                        continuar = false;
                    }
                    var Formatos = $scope.CadenaFormatos.split(',');
                    var cont = 0;
                    for (var i = 0; i < Formatos.length; i++) {
                        Formatos[i] = Formatos[i].replace(' ', '');
                        var Extencion = element.files[0].name.split('.');
                        //if (element.files[0].type == Formatos[i]) {
                        if ('.' + (Extencion[Extencion.length - 1].toUpperCase()) == Formatos[i].toUpperCase()) {
                            cont++;
                        }
                    }
                    if (cont == 0) {
                        ShowError("Archivo invalido, por favor verifique el formato del archivo.  Formatos permitidos: " + $scope.CadenaFormatos);
                        continuar = false;
                    }
                }
                if (continuar == true) {
                    if ($scope.Documento.Archivo != undefined && $scope.Documento.Archivo != '' && $scope.Documento.Archivo != null) {
                        $scope.Documento.ArchivoTemporal = element.files[0];
                        showModal('modalConfirmacionRemplazarDocumentoRemesa');
                    } else {
                        var reader = new FileReader();
                        $scope.Documento.ArchivoCargado = element.files[0];
                        reader.onload = $scope.AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }
            }
        };

        $scope.RemplazarDocumentoRemesa = function () {
            var reader = new FileReader();
            $scope.Documento.ArchivoCargado = $scope.Documento.ArchivoTemporal;
            reader.onload = $scope.AsignarArchivo;
            reader.readAsDataURL($scope.Documento.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumentoRemesa');
        };

        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                //imagenes, tienen un proceso aparte ya que se deben redimencionar para que no ocupen mucho espacio en la BD
                if ($scope.Documento.ArchivoCargado.type.includes("image")) {
                    var img = new Image();
                    img.src = e.target.result;
                    $timeout(function () {
                        $('#Foto' + $scope.IdPosiction).show();
                        $('#FotoCargada' + $scope.IdPosiction).hide();
                        if (img.height > img.width) {
                            RedimencionarImagen('CanvasVertical', img, 699, 499);
                            $scope.Documento.Archivo = document.getElementById('CanvasVertical').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                        }
                        //la imagen es horizontal
                        else {
                            RedimencionarImagen('CanvasHorizontal', img, 499, 699);
                            $scope.Documento.Archivo = document.getElementById('CanvasHorizontal').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                        }
                        $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type;
                        var Extencion = $scope.Documento.ArchivoCargado.name.split('.');
                        var tmpNombre = $scope.Documento.ArchivoCargado.name.split('.').slice(0, -1).join('.');
                        $scope.Documento.NombreDocumento = tmpNombre;
                        $scope.Documento.ExtensionDocumento = Extencion[Extencion.length - 1];
                        $scope.InsertarDocumento();
                    }, 100);
                }
                //Resto de archivos
                else {
                    $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                    $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type;
                    var Extencion = $scope.Documento.ArchivoCargado.name.split('.');
                    var tmpNombre = $scope.Documento.ArchivoCargado.name.split('.').slice(0, -1).join('.');
                    $scope.Documento.NombreDocumento = tmpNombre;
                    $scope.Documento.ExtensionDocumento = Extencion[Extencion.length - 1];
                    $scope.InsertarDocumento();
                }
            });
        };

        $scope.SeleccionarObjeto = function (item) {
            $scope.DocumentoSeleccionado = item;
        };

        $scope.InsertarDocumento = function () {
            $timeout(function () {
                blockUI.start("Subiendo Archivo...");
                // Guardar Archivo temporal
                var Documento = {};
                Documento = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Id: $scope.DocumentoSeleccionado.Id ? $scope.DocumentoSeleccionado.Id : 0,
                    Numero: $scope.Documento.Numero,
                    Descripcion: $scope.Documento.Descripcion,
                    Nombre: $scope.Documento.NombreDocumento,
                    Extension: $scope.Documento.ExtensionDocumento,
                    Tipo: $scope.Documento.Tipo,
                    Archivo: $scope.Documento.Archivo,
                    CumplidoRemesa: true,
                };
                DocumentosFactory.InsertarTemporal(Documento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            // Se guardó correctamente el pdf
                            if (response.data.Datos) {
                                $scope.ListadoDocumentos.forEach(function (item) {
                                    if (item.$$hashKey == $scope.DocumentoSeleccionado.$$hashKey) {
                                        item.Id = response.data.Datos;
                                        item.ValorDocumento = 1;
                                        item.AplicaEliminar = 1;
                                        item.temp = true;
                                    }
                                });
                                ShowSuccess('Se cargó el documento adjunto "' + $scope.Documento.NombreDocumento + '"');
                                $scope.Documento = '';
                            }
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                        blockUI.stop();
                    }, function (response) {
                        ShowError(response.statusText);
                        blockUI.stop();
                    });
            }, 200);
        };

        //elimina archivo temporal
        $scope.ElimiarDocumento = function (EliminarDocumento) {
            var EliminarDocu = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Id: EliminarDocumento.Id,
                Numero: EliminarDocumento.Numero,
                CumplidoRemesa: true,
            };

            DocumentosFactory.EliminarDocumento(EliminarDocu).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                            var item = $scope.ListadoDocumentos[i];
                            if (item.Id == EliminarDocumento.Id) {
                                $scope.ListadoDocumentos.splice(i, 1);
                            }
                        }
                    }
                    else {
                        MensajeError("Error", response.data.MensajeError, false);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                if (item.Temp) {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&Temp=1' + '&ENRE_Numero=' + item.Numero);
                }
                else {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&ENRE_Numero=' + item.Numero + '&Temp=0');
                }
            }
            else {
                ShowError("Se debe seleccionar un archivo");
            }
        };

        //valida para guardar archivo como definitivo
        $scope.GuardarArchivoCumplido = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Guardando Documento...");
                $timeout(function () {
                    blockUI.message("Guardando Documento...");
                    TrasladarDocumentoCumplido(item);
                }, 100);
                //Bloqueo Pantalla
            }
        };

        //elimina archivo definitivo
        $scope.EliminarArchivo = function (item) {
            var Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Id: item.Id,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.EliminarDocumentoCumplidoRemesa(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {

                ShowSuccess('Se eliminó el documento adjunto "' + item.Descripcion + '"');
                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                    var item = $scope.ListadoDocumentos[i];
                    if (item.Id == Documento.Id) {
                        $scope.ListadoDocumentos.splice(i, 1);
                    }
                }
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
        }

        //guarda archivo como definitivo
        function TrasladarDocumentoCumplido(item) {
            var Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Id: item.Id,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.InsertarDocumentosCumplidoRemesa(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {
                if (ResponseDocu.Datos > 0) {
                    $scope.ListadoDocumentos.forEach(function (item) {
                        if (item.$$hashKey == $scope.DocumentoSeleccionado.$$hashKey) {
                            item.Id = ResponseDocu.Datos;
                            item.ValorDocumento = 1;
                            item.AplicaEliminar = 1;
                            item.temp = false;
                        }
                    });
                    ShowSuccess('Se guardó el documento adjunto "' + item.Descripcion + '"');
                }
                else {
                    ShowError(ResponseDocu.MensajeOperacion);
                }
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
        }

        $scope.EliminarDocumentosCumplidoTemporal = function (numero) {
            var EliminarDocu = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: numero,
                CumplidoRemesa: true,
            };

            DocumentosFactory.EliminarDocumento(EliminarDocu).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDocumentos = [];
                    }
                    else {
                        MensajeError("Error", response.data.MensajeError, false);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        //-----------------------Documental------------------------------//
        //---------------Gestion Control Entregas
        //--Volver mastes
        $scope.VolverMaster = function () {
            if ($scope.Modelo.NumeroDocumento == undefined) {
                document.location.href = $scope.Master;
            }
            else {
                document.location.href = $scope.Master + '/' + $scope.Modelo.NumeroDocumento;
            }
        };
        //--Mascaras
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item);
        };
        //--Mascaras
        //----------------------------Funciones Generales---------------------------------//
    }]);