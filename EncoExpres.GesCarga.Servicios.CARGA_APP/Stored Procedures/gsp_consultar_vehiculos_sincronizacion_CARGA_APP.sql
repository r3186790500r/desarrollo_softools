PRINT 'gsp_consultar_vehiculos_sincronizacion_CARGA_APP'
GO
DROP PROCEDURE gsp_consultar_vehiculos_sincronizacion_CARGA_APP
GO
CREATE PROCEDURE gsp_consultar_vehiculos_sincronizacion_CARGA_APP(
@par_EMPR_Codigo smallint
)
AS
BEGIN
SELECT 
VEHI.Placa,
VEHI.Codigo,
VEHI.Estado,
VEHI.Modelo,
VEHI.CATA_TIVE_Codigo,
TIVE.Campo1 As Tipo_Vehiculo,
VEHI.Numero_Ejes,
ISNULL(SEMI.Placa,'') AS SEMI_Placa,
ISNULL(SEMI.CATA_TICA_Codigo,0) AS SEMI_CATA_TICA_Codigo,
ISNULL(TICA.Campo1,'') AS TipoCarroceriaSemirremolque,
ISNULL(SEMI.Numero_Ejes,0) AS EjesSemirremolque,
SOAT.Fecha_Vence AS fechaExpiracionSOAT,
TEME.Fecha_Vence AS fechaExpiracionTecnicoMecanica,
0 As ReporteExterno ,
0 AS ReporteInterno,
1 AS Satelital,
VEHI.CATA_TICO_Codigo,
TICO.Campo1 AS TipoCombustible,
ISNULL(PGPS.Codigo_Alterno,0) AS CodigoAlternoProveedorGPS,
LTRIM(RTRIM(CONCAT(ISNULL(PGPS.Razon_Social,''),' ',ISNULL(PGPS.Nombre,''),' ',ISNULL(PGPS.Apellido1,''),' ',ISNULL(PGPS.Apellido2,'')))) As NombreEmpresaGPS,
ISNULL(COND.Codigo_Alterno,'') AS CodigoAlternoConductor,
ISNULL(TENE.Codigo_Alterno,'') AS CodigoAlternoTenedor,
ISNULL(PROP.Codigo_Alterno,'') AS CodigoAlternoPropietario,
ISNULL(PROP.Codigo_Alterno,'') AS CodigoAlternoAdministrador,
'userGPS' AS usuarioGPS,
'passwordGPS' AS passwordGPS,
0 AS Articulado
FROM Vehiculos VEHI

LEFT JOIN Valor_Catalogos TIVE ON
VEHI.EMPR_Codigo = TIVE.EMPR_Codigo AND
VEHI.CATA_TIVE_Codigo = TIVE.Codigo

LEFT JOIN Semirremolques SEMI ON
VEHI.EMPR_Codigo = SEMI.EMPR_Codigo AND
VEHI.SEMI_Codigo = SEMI.Codigo

LEFT JOIN Valor_Catalogos TICA ON
VEHI.EMPR_Codigo = TICA.EMPR_Codigo AND
SEMI.CATA_TICA_Codigo = TICA.Codigo

LEFT JOIN GESCARGA50_DOCU_DESARROLLO.dbo.Vehiculo_Documentos TEME ON
VEHI.EMPR_Codigo = TEME.EMPR_Codigo AND
VEHI.Codigo = TEME.VEHI_Codigo AND
TEME.CDGD_Codigo = 103 --TecnicoMecánica

LEFT JOIN GESCARGA50_DOCU_DESARROLLO.dbo.Vehiculo_Documentos SOAT ON
VEHI.EMPR_Codigo = SOAT.EMPR_Codigo AND
VEHI.Codigo = SOAT.VEHI_Codigo AND
SOAT.CDGD_Codigo = 102 --SOAT

LEFT JOIN Valor_Catalogos TICO ON
VEHI.EMPR_Codigo = TICO.EMPR_Codigo AND
VEHI.CATA_TICO_Codigo = TICO.Codigo

LEFT JOIN Terceros PGPS ON
VEHI.EMPR_Codigo = PGPS.EMPR_Codigo AND
VEHI.TERC_Codigo_Proveedor_GPS = PGPS.Codigo

LEFT JOIN Terceros COND ON
VEHI.EMPR_Codigo = COND.EMPR_Codigo AND
VEHI.TERC_Codigo_Conductor = COND.Codigo

LEFT JOIN Terceros TENE ON
VEHI.EMPR_Codigo = TENE.EMPR_Codigo AND
VEHI.TERC_Codigo_Tenedor = TENE.Codigo

LEFT JOIN Terceros PROP ON
VEHI.EMPR_Codigo = PROP.EMPR_Codigo AND
VEHI.TERC_Codigo_Propietario = PROP.Codigo

LEFT JOIN Registro_Sincronizacion_APP_CARGA RSAC ON
VEHI.EMPR_Codigo = RSAC.EMPR_Codigo AND
VEHI.Codigo = RSAC.Codigo AND
RSAC.CATA_OSCA_Codigo = 23803 -- Origen Sincronización CARGA APP Vehículos

WHERE VEHI.EMPR_Codigo = @par_EMPR_Codigo
AND ISNULL(RSAC.Sincronizado,0) = 0
END
GO