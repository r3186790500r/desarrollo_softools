﻿PRINT 'gsp_liberar_guias_recolecciones'
GO
DROP PROCEDURE gsp_liberar_guias_recolecciones
GO
CREATE PROCEDURE gsp_liberar_guias_recolecciones (  
@par_EMPR_Codigo NUMERIC,   
@par_ENPR_Numero NUMERIC,  
@par_ENRE_Numero NUMERIC  
)  
AS BEGIN   
UPDATE Encabezado_Remesas SET ENPR_Numero = 0   
WHERE TIDO_Codigo = 110 AND Numero = @par_ENRE_Numero  AND ENPR_Numero = @par_ENPR_Numero   
  
 SELECT @@ROWCOUNT As CantidadRegistros  
END   
GO