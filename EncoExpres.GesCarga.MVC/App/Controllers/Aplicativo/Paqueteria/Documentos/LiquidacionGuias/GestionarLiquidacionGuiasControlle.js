﻿EncoExpresApp.controller("GestionarLiquidacionGuiasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'LiquidacionesFactory', 'PlanillaDespachosFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'OficinasFactory', 'DetalleNovedadesDespachosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, LiquidacionesFactory, PlanillaDespachosFactory, ValorCatalogosFactory, TercerosFactory, OficinasFactory, DetalleNovedadesDespachosFactory) {
        console.clear()
        $scope.Titulo = 'GESTIONAR LIQUIDACIÓN DESPACHOS';
        $scope.MapaSitio = $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Paqueteria' }, { Nombre: 'Liquidación Planillas Paquetería' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GUIAS_PAQUETERIA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        var TerceroLiquidar = 0;
        var PlanillaValidada = false;
        var TotalBaseImpuestos;
        var TotalValorImpuestos;
        var TotalConceptosSuma = 0;
        var TotalConceptosResta = 0;
        var OficinaUsuario = {
            Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
        };

        $scope.ModeloFecha = new Date();
        $scope.DeshabilitarControlPlanilla = false;
        $scope.DeshabilitarGuardar = false;
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/

        if ($routeParams.Numero > 0) {
            $scope.ModeloNumero = $routeParams.Numero;
            Obtener();
        }
        else {
            $scope.ModeloNumero = 0;
            $scope.ModeloOficina = OficinaUsuario.Nombre;
        };

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: CERO },
            { Nombre: 'DEFINITIVO', Codigo: 1 }
        ]
        $scope.ListadoDocumentos = [
            { Nombre: 'PLANILLA DESPACHOS', Codigo: 130 },
            { Nombre: 'PLANILLA ENTREGAS', Codigo: 210 },
            { Nombre: 'PLANILLA RECOLECCIONES', Codigo: 205 }
        ]
        $scope.ModeloEstado = $scope.ListadoEstados[2]
        $scope.TipoDocuumeto = $scope.ListadoDocumentos[0]

        function Obtener() {
            blockUI.start('Cargando Liquidación Despachos...');

            $timeout(function () {
                blockUI.message('Cargando Liquidación Despachos...');
            }, 200);

            TotalBaseImpuestos = 0;
            TotalValorImpuestos = 0;

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ModeloNumero,
                Obtener: 1
            };
            blockUI.delay = 1000;
            LiquidacionesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ModeloNumero = response.data.Datos.Numero;
                        $scope.ModeloNumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.ModeloFecha = new Date(response.data.Datos.Fecha);
                        $scope.ModeloNumeroPlanilla = response.data.Datos.NumeroPlanilla;
                        $scope.ModeloNumeroManifiesto = response.data.Datos.NumeroManifiesto;
                        $scope.ModeloNumeroCumplido = response.data.Datos.NumeroCumplido;
                        $scope.ModeloPlacaVehiculo = response.data.Datos.PlacaVehiculo;
                        $scope.ModeloSemirremolque = response.data.Datos.PlacaSemirremolque;
                        TerceroLiquidar = response.data.Datos.Tenedor.Codigo;
                        $scope.ModeloTenedor = response.data.Datos.Tenedor.NombreCompleto;
                        $scope.ModeloConductor = response.data.Datos.Conductor.NombreCompleto;
                        $scope.ModeloObservaciones = response.data.Datos.Observaciones;
                        $scope.ModeloValorFlete = $scope.MaskValoresGrid(response.data.Datos.ValorFleteTranportador);
                        $scope.ModeloValorConceptos = $scope.MaskValoresGrid(response.data.Datos.ValorConceptosLiquidacion);
                        $scope.ModeloValorBase = $scope.MaskValoresGrid(response.data.Datos.ValorBaseImpuestos);
                        $scope.ModeloValorImpuestos = $scope.MaskValoresGrid(response.data.Datos.ValorImpuestos);
                        $scope.ModeloValorPagar = $scope.MaskValoresGrid(response.data.Datos.ValorPagar);
                        $scope.ModeloListadoConceptos = response.data.Datos.ListaDetalleLiquidacion;

                        //for (i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                        //    if ($scope.ModeloListadoConceptos[i].ConceptoSistema == 1) {
                        //        $scope.DeshabilitarCampo = true;
                        //    } else {
                        //        $scope.DeshabilitarCampo = true;
                        //    }
                        //}

                        for (i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                            $scope.ModeloListadoConceptos[i].Valor = $scope.MaskValoresGrid($scope.ModeloListadoConceptos[i].Valor);
                        }

                        $scope.ModeloListadoImpuestos = response.data.Datos.ListaImpuestosConceptosLiquidacion;

                        //for (i = 0; i < $scope.ModeloListadoImpuestos.length; i++) {
                        //    $scope.ModeloListadoImpuestos[i].ValorImpuesto = parseFloat($scope.ModeloListadoImpuestos[i].Valor_base) * parseFloat($scope.ModeloListadoImpuestos[i].Valor_tarifa);
                        //    $scope.ModeloListadoImpuestos[i].Valor_base = $scope.ModeloListadoImpuestos[i].Valor_base;
                        //    $scope.ModeloListadoImpuestos[i].ValorImpuesto = $scope.ModeloListadoImpuestos[i].ValorImpuesto;
                        //}

                        $scope.ModeloOficina = response.data.Datos.Oficina.Nombre;
                        $scope.ModeloEstado = response.data.Datos.Estado;

                        if ($scope.ListadoEstados !== undefined && $scope.ListadoEstados !== null) {
                            $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.ModeloEstado);
                        }
                        $scope.TipoDocuumeto = $linq.Enumerable().From($scope.ListadoDocumentos).First('$.Codigo ==' + response.data.Datos.TipoDocumento.Codigo);

                        $scope.DeshabilitarControlPlanilla = true;
                        if ($scope.ModeloEstado.Codigo === ESTADO_DEFINITIVO) {
                            $scope.DeshabilitarCampo = true;
                            $scope.DeshabilitarGuardar = true;
                        }

                        PlanillaValidada = true;
                    }
                    else {
                        ShowError('No se logro consultar la liquidación número ' + $scope.ModeloNumeroDocumento + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarLiquidarPlanillas';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarLiquidarPlanillas';
                });

            blockUI.stop();
        };

        $scope.ValidarPlanilla = function (Planilla, opt) {
            if (Planilla !== undefined && Planilla !== '' && Planilla !== null && Planilla > 0) {
                TotalBaseImpuestos = 0;
                TotalValorImpuestos = 0;
                PlanillaValidada = false;
                if (opt > 0) {//Buscar por manifiesto
                    var filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: { Codigo: $scope.TipoDocuumeto.Codigo },
                        NumeroManifiesto: Planilla,
                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        CodigoUsuario: $scope.Sesion.UsuarioAutenticado.Codigo,
                        HaciaTemporal: 1,
                        Obtener: 1
                    }
                } else {
                    var filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: { Codigo: $scope.TipoDocuumeto.Codigo },
                        NumeroDocumento: Planilla,
                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        CodigoUsuario: $scope.Sesion.UsuarioAutenticado.Codigo,
                        HaciaTemporal: 1,
                        Obtener: 1
                    }
                }
                LiquidacionesFactory.ObtenerPlanilla(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true && response.data.Datos.Numero > 0) {
                            if (response.data.Datos.Anulado == 1 || response.data.Datos.Estado.Codigo == ESTADO_BORRADOR) {
                                ShowError('La planilla número ' + Planilla + ' se encuentra anulada o en estado borrador');
                                PlanillaValidada = false;
                            } else if (response.data.Datos.Liquidacion.Numero !== 0) {
                                ShowError('La planilla ya se encuentra liquidada');
                                PlanillaValidada = false;
                            } else {
                                $scope.NumeroInternoPlanilla = response.data.Datos.Numero;
                                $scope.ModeloNumeroManifiesto = response.data.Datos.Manifiesto.NumeroDocumento;
                                $scope.ModeloNumeroPlanilla = response.data.Datos.NumeroDocumento;
                                $scope.ModeloNumeroCumplido = response.data.Datos.Cumplido.NumeroDocumento;
                                $scope.ModeloPlacaVehiculo = response.data.Datos.Vehiculo.Placa;
                                $scope.ModeloSemirremolque = response.data.Datos.Semirremolque.Placa;
                                $scope.ModeloTenedor = response.data.Datos.NombreTenedor;
                                TerceroLiquidar = response.data.Datos.Tenedor.Codigo;
                                $scope.ModeloConductor = response.data.Datos.NombreConductor;
                                $scope.ModeloListadoConceptos = response.data.Datos.ListaConceptosLiquidacion;
                                $scope.ModeloListadoImpuestostemp = JSON.parse(JSON.stringify(response.data.Datos.ListaImpuestosConceptosLiquidacion))

                                for (i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                                    if ($scope.ModeloListadoConceptos[i].Operacion == 2) { //Resta
                                        TotalConceptosResta += $scope.ModeloListadoConceptos[i].Valor;
                                    } else if ($scope.ModeloListadoConceptos[i].Operacion == 1) { //Suma
                                        TotalConceptosSuma += $scope.ModeloListadoConceptos[i].Valor;
                                    }

                                    if ($scope.ModeloListadoConceptos[i].AplicaValorBase == 1) {
                                        //if ($scope.ModeloListadoConceptos[i].AplicaValorBase == 1) {//PAIS
                                        //}
                                        //if ($scope.ModeloListadoConceptos[i].AplicaValorBase == 1) {//DEPARTAMENTO
                                        //}
                                        //if ($scope.ModeloListadoConceptos[i].AplicaValorBase == 1) {//CIUDAD
                                        //}
                                        TotalBaseImpuestos += $scope.ModeloListadoConceptos[i].Valor;
                                    }

                                    //if ($scope.ModeloListadoConceptos[i].ConceptoSistema == 1) {
                                    //    $scope.DeshabilitarFila = true;
                                    //} else {
                                    //    $scope.DeshabilitarFila = true;
                                    //}
                                    $scope.ModeloListadoConceptos[i].Valor = $scope.MaskValoresGrid($scope.ModeloListadoConceptos[i].Valor);
                                }

                                for (i = 0; i < $scope.ModeloListadoImpuestostemp.length; i++) {
                                    $scope.ModeloListadoImpuestostemp[i].ValorImpuesto = parseFloat($scope.ModeloListadoImpuestostemp[i].Valor_base * $scope.ModeloListadoImpuestostemp[i].Valor_tarifa);
                                    TotalValorImpuestos += $scope.ModeloListadoImpuestostemp[i].ValorImpuesto;
                                    $scope.ModeloListadoImpuestostemp[i].Valor_base = $scope.MaskValoresGrid($scope.ModeloListadoImpuestostemp[i].Valor_base);
                                    $scope.ModeloListadoImpuestostemp[i].ValorImpuesto = $scope.MaskValoresGrid($scope.ModeloListadoImpuestostemp[i].ValorImpuesto);
                                }

                                $scope.ModeloValorFlete = response.data.Datos.ValorFleteTransportador;
                                $scope.ModeloValorConceptos = TotalConceptosResta - TotalConceptosSuma;
                                $scope.ModeloValorBase = response.data.Datos.ValorFleteTransportador + TotalBaseImpuestos;
                                $scope.ModeloValorImpuestos = TotalValorImpuestos;
                                $scope.ModeloValorPagar = $scope.ModeloValorFlete - $scope.ModeloValorConceptos - $scope.ModeloValorImpuestos;
                                PlanillaValidada = true;

                                //Formatear campos
                                $scope.ModeloValorFlete = $scope.MaskValoresGrid($scope.ModeloValorFlete);
                                $scope.ModeloValorConceptos = $scope.MaskValoresGrid($scope.ModeloValorConceptos);
                                $scope.ModeloValorBase = $scope.MaskValoresGrid($scope.ModeloValorBase);
                                $scope.ModeloValorImpuestos = $scope.MaskValoresGrid($scope.ModeloValorImpuestos);
                                $scope.ModeloValorPagar = $scope.MaskValoresGrid($scope.ModeloValorPagar);
                                var filtroNovedades = {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    NumeroPlanilla: $scope.NumeroInternoPlanilla
                                }
                                DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                                    then(function (response) {
                                        if (response.data.ProcesoExitoso === true) {
                                            if (response.data.Datos.length > 0) {
                                                $scope.ListaNovedades = []
                                                $scope.ModeloListadoImpuestos = []
                                                for (var i = 0; i < response.data.Datos.length; i++) {
                                                    if (response.data.Datos[i].Anulado == 0 && response.data.Datos[i].Novedad.Conceptos.ConceptoLiquidacion.Codigo > 0) {
                                                        $scope.ListaNovedades.push(response.data.Datos[i])
                                                    }
                                                }
                                                for (var i = 0; i < $scope.ListaNovedades.length; i++) {
                                                    for (var j = 0; j < $scope.ModeloListadoConceptos.length; j++) {
                                                        if ($scope.ListaNovedades[i].Novedad.Conceptos.ConceptoLiquidacion.Codigo == $scope.ModeloListadoConceptos[j].ConceptoLiquidacion.Codigo) {
                                                            $scope.ModeloListadoConceptos[j].Valor = $scope.MaskValoresGrid(parseInt($scope.ListaNovedades[i].ValorCosto) + parseInt($scope.ModeloListadoConceptos[j].Valor))
                                                        }
                                                    }
                                                }
                                                for (var i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                                                    for (var j = 0; j < $scope.ModeloListadoImpuestostemp.length; j++) {
                                                        if (parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor)) > 0) {
                                                            var itemtmp = JSON.parse(JSON.stringify($scope.ModeloListadoImpuestostemp[j]))
                                                            if (itemtmp.ConceptoLiquidacion.Codigo == $scope.ModeloListadoConceptos[i].ConceptoLiquidacion.Codigo) {
                                                                if ($scope.ModeloListadoImpuestos.length == 0) {
                                                                    if ($scope.ModeloListadoConceptos[i].Valor > itemtmp.Valor_base) {
                                                                        itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor))
                                                                    }
                                                                    $scope.ModeloListadoImpuestos.push(itemtmp)
                                                                } else {
                                                                    if ($scope.ModeloListadoConceptos[i].Valor > itemtmp.Valor_base) {
                                                                        itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor))
                                                                    }
                                                                    for (var k = 0; k < $scope.ModeloListadoImpuestos.length; k++) {
                                                                        if ($scope.ModeloListadoImpuestos[k].Codigo == itemtmp.Codigo) {
                                                                            $scope.ModeloListadoImpuestos[k].Valor_base = parseInt(MascaraNumero($scope.ModeloListadoImpuestos[k].Valor_base)) + parseInt(MascaraNumero(itemtmp.Valor_base))
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                for (var i = 0; i < $scope.ModeloListadoImpuestostemp.length; i++) {
                                                    if ($scope.ModeloListadoImpuestostemp[i].ConceptoLiquidacion.Codigo == 0) {
                                                        var itemtmp = JSON.parse(JSON.stringify($scope.ModeloListadoImpuestostemp[i]))
                                                        if (parseInt(MascaraNumero($scope.ModeloValorFlete)) > parseInt(MascaraNumero(itemtmp.Valor_base))) {
                                                            itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloValorFlete))
                                                        }
                                                        if ($scope.ModeloListadoImpuestos.length == 0) {
                                                            $scope.ModeloListadoImpuestos.push(itemtmp)
                                                        } else {

                                                            for (var k = 0; k < $scope.ModeloListadoImpuestos.length; k++) {
                                                                if ($scope.ModeloListadoImpuestos[k].Codigo == itemtmp.Codigo) {
                                                                    $scope.ModeloListadoImpuestos[k].Valor_base = parseInt(MascaraNumero($scope.ModeloListadoImpuestos[k].Valor_base)) + parseInt(MascaraNumero(itemtmp.Valor_base))
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            $scope.Calcular()
                                        }
                                    }, function (response) {
                                    });
                            }
                        }
                        else {
                            ShowError('La planilla número ' + Planilla + ' no se encuentra en el sistema');
                            PlanillaValidada = false;
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        document.location.href = '#!ConsultarLiquidarPlanillas';
                    });
            }
        }

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar')
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            window.scrollTo(top, top);
            if (DatosRequeridos()) {

                var Modelo = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHO },
                    Fecha: $scope.ModeloFecha,
                    Numero: $scope.ModeloNumero,
                    NumeroDocumento: $scope.ModeloNumeroDocumento,
                    NumeroPlanilla: $scope.NumeroInternoPlanilla,
                    NumeroManifiesto: $scope.ModeloNumeroManifiesto,
                    FechaEntrega: $scope.ModeloFecha,
                    Observaciones: $scope.ModeloObservaciones,
                    ValorFleteTranportador: $scope.ModeloValorFlete,
                    ValorConceptosLiquidacion: $scope.ModeloValorConceptos,
                    ValorBaseImpuestos: $scope.ModeloValorBase,
                    ValorImpuestos: $scope.ModeloValorImpuestos,
                    ValorPagar: $scope.ModeloValorPagar,
                    Anulado: 0,
                    Estado: $scope.ModeloEstado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Oficina: { Codigo: OficinaUsuario.Codigo },
                    Numeracion: '',
                    ListaDetalleLiquidacion: $scope.ModeloListadoConceptos,
                    ListaImpuestosConceptosLiquidacion: $scope.ModeloListadoImpuestos,
                    HaciaTemporal: 0,
                    //CuentaPorPagar: $scope.ModeloCxP
                }

                //$scope.ModeloCxP = {};

                if ($scope.ModeloEstado.Codigo === ESTADO_DEFINITIVO && $scope.MaskNumeroGrid($scope.ModeloValorPagar) > CERO && $scope.Sesion.Empresa.AprobarLiquidacion == 0) {
                    Modelo.CuentaPorPagar = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                        CodigoAlterno: '',
                        Fecha: $scope.ModeloFecha,
                        Tercero: { Codigo: TerceroLiquidar },
                        DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION },
                        CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION,
                        Numero: $scope.ModeloNumero,
                        Fecha: $scope.ModeloFecha,
                        Numeracion: '',
                        CuentaPuc: { Codigo: 0 },
                        ValorTotal: $scope.ModeloValorPagar,
                        Abono: 0,
                        Saldo: $scope.ModeloValorPagar,
                        FechaCancelacionPago: $scope.ModeloFecha,
                        Aprobado: 1,
                        UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        FechaAprobo: $scope.ModeloFecha,
                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        Vehiculo: { Codigo: 0 },
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    }
                }



                LiquidacionesFactory.Guardar(Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos === -1) {
                                ShowError('La planilla ya se encuentra liquidada');
                            } else {
                                if (response.data.Datos > 0) {
                                    if ($scope.ModeloNumero === 0) {
                                        ShowSuccess('Se guardó la liquidación ' + response.data.Datos);
                                    }
                                    else {
                                        ShowSuccess('Se modificó la liquidación ' + response.data.Datos);
                                    }
                                    document.location.href = '#!ConsultarLiquidarPlanillas/' + $scope.TipoDocuumeto.Codigo + '/' + response.data.Datos;
                                } else {
                                    ShowError(response.data.MensajeOperacion);
                                }
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var Continuar = true;

            if ($scope.ModeloFecha === undefined || $scope.ModeloFecha === '' || $scope.ModeloFecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                Continuar = false
            }
            if ($scope.ModeloNumeroPlanilla === undefined || $scope.ModeloNumeroPlanilla === '' || $scope.ModeloNumeroPlanilla == null || PlanillaValidada == false) {
                $scope.MensajesError.push('Debe ingresar y validar el número de la planilla');
                Continuar = false
            }
            if ($scope.ModeloObservaciones == undefined || $scope.ModeloObservaciones == '' || $scope.ModeloObservaciones == null) {
                $scope.ModeloObservaciones = '';
            }
            if ($scope.ModeloOficina.Codigo == -1) { //TODAS
                Continuar = false;
                $scope.MensajesError.push('Debe seleccionar la oficina');
            }
            if ($scope.ModeloEstado.Codigo == -1) { //TODOS
                Continuar = false;
                $scope.MensajesError.push('Debe seleccionar el estado');
            }
            return Continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarLiquidarPlanillas/' + $scope.TipoDocuumeto.Codigo + '/' + $scope.ModeloNumeroDocumento;
        };
        console.clear();
        $scope.Calcular = function () {
            var BaseActual;
            TotalConceptosSuma = 0;
            TotalConceptosResta = 0;

            BaseActual = $scope.ModeloValorBase;
            ValorConceptos = 0;
            TotalBaseImpuestos = 0;

            //Total conceptos
            for (i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                if ($scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor) === '' || $scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor) === null || $scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor) === undefined || isNaN($scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor))) {
                    ValorConceptos += 0;
                } else {
                    if ($scope.ModeloListadoConceptos[i].Operacion == 2) { //Resta
                        TotalConceptosResta += $scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor);
                    } else if ($scope.ModeloListadoConceptos[i].Operacion == 1) { //Suma
                        TotalConceptosSuma += $scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor);
                    }
                }

                if ($scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor > 0)) {
                    if ($scope.ModeloListadoConceptos[i].AplicaValorBase === 1) {
                        TotalBaseImpuestos += parseFloat($scope.ModeloListadoConceptos[i].Valor);
                    }
                }
            }
            //ValorConceptos += $scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor);

            //------------------------------
            $scope.ModeloListadoImpuestos = []
            for (var i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                for (var j = 0; j < $scope.ModeloListadoImpuestostemp.length; j++) {
                    if (parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor)) > 0) {
                        var itemtmp = JSON.parse(JSON.stringify($scope.ModeloListadoImpuestostemp[j]))
                        if (itemtmp.ConceptoLiquidacion.Codigo == $scope.ModeloListadoConceptos[i].ConceptoLiquidacion.Codigo) {
                            if ($scope.ModeloListadoImpuestos.length == 0) {
                                if (parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor)) > parseInt(MascaraNumero(itemtmp.Valor_base))) {
                                    itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor))
                                }
                                $scope.ModeloListadoImpuestos.push(itemtmp)
                                TotalBaseImpuestos = parseInt(MascaraNumero(itemtmp.Valor_base))
                            } else {
                                if (parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor)) > parseInt(MascaraNumero(itemtmp.Valor_base))) {
                                    itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor))
                                }
                                var cont = 0
                                for (var k = 0; k < $scope.ModeloListadoImpuestos.length; k++) {
                                    if ($scope.ModeloListadoImpuestos[k].Codigo == itemtmp.Codigo) {
                                        $scope.ModeloListadoImpuestos[k].Valor_base = parseInt(MascaraNumero($scope.ModeloListadoImpuestos[k].Valor_base)) + parseInt(MascaraNumero(itemtmp.Valor_base))
                                        TotalBaseImpuestos += parseInt(MascaraNumero($scope.ModeloListadoImpuestos[k].Valor_base));
                                        cont++
                                    }
                                }
                                if (cont == 0) {
                                    $scope.ModeloListadoImpuestos.push(itemtmp)
                                    TotalBaseImpuestos = parseInt(MascaraNumero(itemtmp.Valor_base))
                                }
                            }
                        }
                    }
                }
            }
            ValorConceptos = TotalConceptosResta - TotalConceptosSuma;
            $scope.ModeloValorConceptos = ValorConceptos;
            for (var i = 0; i < $scope.ModeloListadoImpuestostemp.length; i++) {
                if ($scope.ModeloListadoImpuestostemp[i].ConceptoLiquidacion.Codigo == 0) {
                    var itemtmp = JSON.parse(JSON.stringify($scope.ModeloListadoImpuestostemp[i]))
                    if (parseInt(MascaraNumero($scope.ModeloValorFlete)) > parseInt(MascaraNumero(itemtmp.Valor_base))) {
                        itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloValorFlete))
                    }
                    if ($scope.ModeloListadoImpuestos.length == 0) {
                        $scope.ModeloListadoImpuestos.push(itemtmp)
                    } else {

                        var cont = 0
                        for (var k = 0; k < $scope.ModeloListadoImpuestos.length; k++) {
                            if ($scope.ModeloListadoImpuestos[k].Codigo == itemtmp.Codigo) {
                                $scope.ModeloListadoImpuestos[k].Valor_base = parseInt(MascaraNumero($scope.ModeloListadoImpuestos[k].Valor_base)) + parseInt(MascaraNumero(itemtmp.Valor_base))
                                cont++
                            }
                        }
                        if (cont == 0) {
                            $scope.ModeloListadoImpuestos.push(itemtmp)
                        }
                    }
                }
            }
            TotalValorImpuestos = 0
            for (i = 0; i < $scope.ModeloListadoImpuestos.length; i++) {
                $scope.ModeloListadoImpuestos[i].ValorImpuesto = Math.round(parseInt(MascaraNumero($scope.ModeloListadoImpuestos[i].Valor_base)) * parseFloat($scope.ModeloListadoImpuestos[i].Valor_tarifa))
                TotalValorImpuestos += $scope.ModeloListadoImpuestos[i].ValorImpuesto;
                $scope.ModeloListadoImpuestos[i].Valor_base = $scope.MaskValoresGrid($scope.ModeloListadoImpuestos[i].Valor_base);
                $scope.ModeloListadoImpuestos[i].ValorImpuesto = $scope.MaskValoresGrid($scope.ModeloListadoImpuestos[i].ValorImpuesto);
            }

            $scope.ModeloValorConceptos = (TotalConceptosResta - TotalConceptosSuma) * -1;
            $scope.ModeloValorImpuestos = Math.round(TotalValorImpuestos);
            PlanillaValidada = true;

            //Formatear campos
            $scope.ModeloValorFlete = $scope.MaskValoresGrid($scope.ModeloValorFlete);
            $scope.ModeloValorImpuestos = $scope.MaskValoresGrid($scope.ModeloValorImpuestos);

            //------------------------------


            $scope.ModeloValorBase = parseInt(MascaraNumero($scope.ModeloValorFlete)) + parseInt(MascaraNumero(TotalBaseImpuestos));
            $scope.ModeloValorPagar = parseInt($scope.MaskNumeroGrid($scope.ModeloValorFlete)) - ValorConceptos - $scope.MaskNumeroGrid($scope.ModeloValorImpuestos);

            //Formatear Campos
            $scope.ModeloValorConceptos = $scope.MaskValoresGrid($scope.ModeloValorConceptos);
            $scope.ModeloValorBase = $scope.MaskValoresGrid($scope.ModeloValorBase);
            $scope.ModeloValorPagar = $scope.MaskValoresGrid($scope.ModeloValorPagar);
        }



        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item)
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };

        /* Obtener parametros*/
        try {
            var Parametros = $routeParams.Codigo.split(',')
            if (Parametros.length > 1) {
                $scope.ModeloNumero = Parametros[0];
                $scope.aplicabase = true
                Obtener();
            } else {
                if ($routeParams.Codigo > 0) {
                    $scope.ModeloNumero = $routeParams.Codigo;
                    Obtener();
                }
                else {
                    $scope.ModeloNumero = 0;
                }

            }
        } catch (e) {
            if ($routeParams.Codigo > 0) {
                $scope.ModeloNumero = $routeParams.Codigo;
                Obtener();
            }
            else {
                $scope.ModeloNumero = 0;
            }

        }
    }]);