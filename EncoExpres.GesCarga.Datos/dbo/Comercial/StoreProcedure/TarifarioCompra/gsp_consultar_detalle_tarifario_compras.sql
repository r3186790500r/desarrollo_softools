﻿Print 'gsp_consultar_detalle_tarifario_compras'
GO
DROP PROCEDURE gsp_consultar_detalle_tarifario_compras
GO
CREATE PROCEDURE gsp_consultar_detalle_tarifario_compras  
(  
	@par_EMPR_Codigo smallint,  
	@par_ETCC_Numero numeric   
)  
AS   
BEGIN  
  
	SELECT   
	DTCC.EMPR_Codigo,  
	DTCC.Codigo,  
	DTCC.ETCC_Numero,  
	DTCC.LNTC_Codigo,  
	DTCC.TLNC_Codigo,  
	DTCC.TATC_Codigo,  
	DTCC.TTTC_Codigo,  
	DTCC.RUTA_Codigo,  
	isnull(DTCC.Valor_Flete,0) as Valor_Flete,  
	isnull(DTCC.Valor_Escolta,0) as Valor_Escolta,  
	--isnull(Valor_Kilo_Adicional,0) as Valor_Kilo_Adicional,  
	isnull(DTCC.Valor_Otros1,0) as Valor_Otros1,  
	isnull(DTCC.Valor_Otros2,0) as Valor_Otros2,  
	isnull(DTCC.Valor_Otros3,0) as Valor_Otros3,  
	isnull(DTCC.Valor_Otros4,0) as Valor_Otros4,  
	isnull(DTCC.Fecha_Vigencia_Inicio,'') as Fecha_Vigencia_Inicio,  
	isnull(DTCC.Fecha_Vigencia_Fin,'') as Fecha_Vigencia_Fin,  
	DTCC.Estado,
	TATC.Nombre As NombreTarifa,
	TATC.Estado As EstadoTarifa,
	TTTC.Nombre As NombreTipoTarifa
	  
	FROM   
	Detalle_Tarifario_Carga_compras DTCC, Tarifa_Transporte_Carga TATC,
	V_Tipo_Tarifa_Transporte_Carga TTTC
	WHERE DTCC.EMPR_Codigo = TATC.EMPR_Codigo
	AND DTCC.TATC_Codigo = TATC.Codigo
	AND DTCC.EMPR_Codigo = TTTC.EMPR_Codigo
	AND DTCC.TTTC_Codigo = TTTC.Codigo
	AND DTCC.EMPR_Codigo = @par_EMPR_Codigo  
	AND DTCC.ETCC_Numero = ISNULL(@par_ETCC_Numero, ETCC_Numero)
   
  
END  
GO