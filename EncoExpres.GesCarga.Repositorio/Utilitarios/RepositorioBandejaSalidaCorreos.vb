﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Utilitarios

Namespace Utilitarios

    ''' <summary>
    ''' Clase <see cref="RepositorioBandejaSalidaCorreos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioBandejaSalidaCorreos
        Inherits RepositorioBase(Of BandejaSalidaCorreos)

        ''' <summary>
        ''' Consultar información de bandeja de salida de correos
        ''' </summary>
        ''' <param name="filtro">Filtros de búsqueda</param>
        ''' <returns>Lista de tipo <see cref="BandejaSalidaCorreos"/></returns>
        Public Overrides Function Consultar(filtro As BandejaSalidaCorreos) As IEnumerable(Of BandejaSalidaCorreos)
            Dim lista As New List(Of BandejaSalidaCorreos)
            Dim item As BandejaSalidaCorreos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.TipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                End If
                If filtro.NumeroInicial > 0 And (filtro.NumeroFinal >= filtro.NumeroInicial) Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Inicial", filtro.NumeroInicial)
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Final", filtro.NumeroFinal)
                End If

                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If (filtro.FechaInicial > Date.MinValue) And (filtro.FechaFinal >= filtro.FechaInicial) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_bandeja_salida_correos]")

                While resultado.Read
                    item = New BandejaSalidaCorreos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        ''' <summary>
        ''' Inserta un registro en la tabla bandeja_salida_correos
        ''' </summary>
        ''' <param name="entidad">Entidad a persistir en la base de datos</param>
        ''' <returns>Retorna consecutivo del nuevo registro</returns>
        Public Overrides Function Insertar(entidad As BandejaSalidaCorreos) As Long
            Dim registro As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                conexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumeto)
                conexion.AgregarParametroSQL("@par_Cuenta_Correo_De", entidad.CuentaCorreoDe)
                conexion.AgregarParametroSQL("@par_Cuenta_Correo_Para", entidad.CuentaCorreoPara)
                conexion.AgregarParametroSQL("@par_Cuenta_Correo_Copia", entidad.CuentaCorreoCopia)
                conexion.AgregarParametroSQL("@par_Asunto_Correo", entidad.AsuntoCorreo)
                conexion.AgregarParametroSQL("@par_Texto_Correo", entidad.Contenido)
                conexion.AgregarParametroSQL("@par_CATA_ESCO_Codigo", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_bandeja_salida_correos]")

                While resultado.Read
                    registro = resultado.Item("TotalRegistros")
                End While

                resultado.Close()

            End Using

            Return registro
        End Function

        ''' <summary>
        ''' Modifico un registro en la tabla bandeja_salida_correos
        ''' </summary>
        ''' <param name="entidad">Entidad a persistir en la base de datos</param>
        ''' <returns>Retorna consecutivo el identificador del registro</returns>
        Public Overrides Function Modificar(entidad As BandejaSalidaCorreos) As Long
            Throw New NotImplementedException()
        End Function

        ''' <summary>
        ''' Obtiene información de bandeja de salida de correos
        ''' </summary>
        ''' <param name="filtro">Filtros de búsqueda</param>
        ''' <returns>Objeto de tipo <see cref="BandejaSalidaCorreos"/></returns>
        Public Overrides Function Obtener(filtro As BandejaSalidaCorreos) As BandejaSalidaCorreos
            Throw New NotImplementedException()
        End Function

        Public Function InsertarCorreo(entidad As BandejaSalidaCorreos, ByRef conexion As DataBaseFactory) As Long
            Dim registro As Long

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            conexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumeto)
            conexion.AgregarParametroSQL("@par_Cuenta_Correo_De", entidad.CuentaCorreoDe)
            conexion.AgregarParametroSQL("@par_Cuenta_Correo_Para", entidad.CuentaCorreoPara)
            conexion.AgregarParametroSQL("@par_Cuenta_Correo_Copia", entidad.CuentaCorreoCopia)
            conexion.AgregarParametroSQL("@par_Asunto_Correo", entidad.AsuntoCorreo)
            conexion.AgregarParametroSQL("@par_Texto_Correo", entidad.Contenido)
            conexion.AgregarParametroSQL("@par_CATA_ESCO_Codigo", entidad.Estado)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_bandeja_salida_correos]")

            While resultado.Read
                registro = resultado.Item("TotalRegistros")
            End While


            resultado.Close()

            Return registro
        End Function
    End Class

End Namespace
