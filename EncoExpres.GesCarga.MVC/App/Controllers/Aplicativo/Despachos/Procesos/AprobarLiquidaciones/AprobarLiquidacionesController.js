﻿EncoExpresApp.controller("AprobarLiquidacionesCtrl", ['$scope', '$timeout', 'LiquidacionesFactory', 'TercerosFactory', '$linq', 'blockUI', 'OficinasFactory', 'EmpresasFactory',
    function ($scope, $timeout, LiquidacionesFactory, TercerosFactory, $linq, blockUI, OficinasFactory, EmpresasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Procesos' }, { Nombre: 'Aprobar Liquidaciones Despacho' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.MostrarMensajeError = false;
        $scope.DeshabilitarBotones = false;
        $scope.NumeroAnular;
        $scope.NumeroDocumentoAnular;

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoOficinas = [];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_APROBAR_LIQUIDACIONES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaFinal = new Date();
        }
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        //Condicion para habilitar botones de aprobar liquidacion despachos segun empresa
        EmpresasFactory.Obtener({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.AprobarLiquidacion == 1) {
                        $scope.DeshabilitarBotones = true;
                    }
                }
            }, function (response) {
                ShowError(response.statusText)
            });
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarLiquidacion';
            }
        };
        $scope.ModeloOficina = {}
        $scope.ModeloEstado = {}
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: CERO },
            { Nombre: 'DEFINITIVO', Codigo: 1 }
        ]

        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.ModeloOficina = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }



        TercerosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: { Codigo: ESTADO_ACTIVO },
            CadenaPerfiles: PERFIL_CONDUCTOR
        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                        $scope.ListaConductor = response.data.Datos;


                        if ($scope.CodigoConductor !== undefined && $scope.CodigoConductor !== '' && $scope.CodigoConductor !== 0 && $scope.CodigoConductor !== null) {
                            if ($scope.CodigoConductor > 0) {
                                $scope.ModeloConductor = $linq.Enumerable().From($scope.ListaConductor).First('$.Codigo ==' + $scope.CodigoConductor);
                            } else {
                                $scope.ModeloConductor = $linq.Enumerable().From($scope.ListaConductor).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.ModeloConductor = $linq.Enumerable().From($scope.ListaConductor).First('$.Codigo ==0');
                        }

                    }
                    else {
                        $scope.ListaConductor = [];
                        $scope.ModeloConductor = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        TercerosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: { Codigo: ESTADO_ACTIVO },
            CadenaPerfiles: PERFIL_TENEDOR
        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                        $scope.ListaTenedor = response.data.Datos;


                        if ($scope.CodigoTenedor !== undefined && $scope.CodigoTenedor !== '' && $scope.CodigoTenedor !== 0 && $scope.CodigoTenedor !== null) {
                            if ($scope.CodigoTenedor > 0) {
                                $scope.ModeloTenedor = $linq.Enumerable().From($scope.ListaTenedor).First('$.Codigo ==' + $scope.CodigoTenedor);
                            } else {
                                $scope.ModeloTenedor = $linq.Enumerable().From($scope.ListaTenedor).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.ModeloTenedor = $linq.Enumerable().From($scope.ListaTenedor).First('$.Codigo ==0');
                        }

                    }
                    else {
                        $scope.ListaTenedor = [];
                        $scope.ModeloTenedor = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '')
                && ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '')
                && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
            }
            return continuar
        }

        function Find() {
            if (DatosRequeridos()) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: $scope.ModeloNumero,
                    FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaInicial,
                    FechaFinal: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaFinal,
                    NumeroPlanilla: $scope.ModeloPlanilla,
                    NumeroManifiesto: $scope.ModeloManifiesto,
                    Vehiculo: $scope.ModeloPlaca,
                    Conductor: $scope.ModeloConductor,
                    Tenedor: $scope.ModeloTenedor,
                    Oficina: $scope.ModeloOficina,
                    Estado: 1,
                    Anulado: 0,
                    Aprobado: 0,
                    Pagina: $scope.paginaActual,
                    Numero: 0,
                    UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas
                };

                blockUI.start('Buscando registros ...');
                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);
                $scope.Buscando = true;
                $scope.MensajesError = [];
                $scope.ListadoLiquidaciones = [];
                if ($scope.Buscando) {
                    if ($scope.MensajesError.length === 0) {
                        blockUI.delay = 1000;
                        $scope.Pagina = $scope.paginaActual
                        $scope.RegistrosPagina = 10
                        LiquidacionesFactory.Consultar(filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    $scope.Numero = 0
                                    if (response.data.Datos.length > 0) {
                                        $scope.ListadoLiquidaciones = response.data.Datos
                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                    }
                                    else {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                    var listado = [];
                                    response.data.Datos.forEach(function (item) {
                                        listado.push(item);
                                    });
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }
                blockUI.stop();
            }
        }

        //----------------------------------------------Funcion Rechazar Liquidación---------------------------------------------------
        $scope.Rechazar = function (Numero, NumeroDocumento) {
            $scope.MensajesErrorRechazo = [];
            $scope.ModeloCausaRechazo = '';
            $scope.NumeroRechazo = 0;
            $scope.NumeroDocumentoRechazo = 0;
            $scope.NumeroRechazo = Numero;
            $scope.NumeroDocumentoRechazo = NumeroDocumento;
            showModal('modalRechazar')
        }

        $scope.ConfirmarRechazo = function () {
            if ($scope.ModeloCausaRechazo !== undefined && $scope.ModeloCausaRechazo !== null && $scope.ModeloCausaRechazo !== '') {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.NumeroRechazo,
                    CausaRechazo: $scope.ModeloCausaRechazo
                }
                LiquidacionesFactory.Rechazar(filtros).
                    then(function (response) {
                        closeModal('modalRechazar')
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.ProcesoExitoso === true) {
                                ShowSuccess('Liquidación rechazada');
                                Find();
                            }
                            else {
                                ShowError('Se presentó un error al rechazadar la liquidación')
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.MensajesErrorRechazo.push('Debe ingresar la causa de rechazo');
            }
        }

        //----------------------------------------------Funcion Aprobar Liquidación---------------------------------------------------
        $scope.Aprobar = function (Numero, NumeroDocumento, item) {
            $scope.tmpLiquidacion = item
            $scope.NumeroApruebo = 0;
            $scope.NumeroDocumentoApruebo = 0;
            $scope.NumeroApruebo = Numero;
            $scope.NumeroDocumentoApruebo = NumeroDocumento;
            showModal('modalAprobar')
        }

        $scope.ConfirmarApruebo = function () {
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.NumeroApruebo,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CuentaPorPagar: {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                    CodigoAlterno: '',
                    Fecha: $scope.tmpLiquidacion.Fecha,
                    Tercero: { Codigo: $scope.tmpLiquidacion.Tenedor.Codigo },
                    DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION },
                    CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION,
                    Numero: $scope.tmpLiquidacion.NumeroDocumento,
                    NumeroDocumento: $scope.tmpLiquidacion.NumeroDocumento,
                    Fecha: $scope.tmpLiquidacion.Fecha,
                    Numeracion: '',
                    CuentaPuc: { Codigo: 0 },
                    ValorTotal: $scope.tmpLiquidacion.ValorPagar,
                    ValorPagar: $scope.tmpLiquidacion.ValorPagar,
                    Abono: 0,
                    Saldo: $scope.tmpLiquidacion.ValorPagar,
                    FechaCancelacionPago: $scope.tmpLiquidacion.Fecha,
                    Aprobado: 1,
                    UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    FechaAprobo: $scope.tmpLiquidacion.Fecha,
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                    Vehiculo: { Codigo: 0 },
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }
            }

            LiquidacionesFactory.Aprobar(filtros).
                then(function (response) {
                    closeModal('modalAprobar')
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.ProcesoExitoso === true) {

                            ShowSuccess('La liquidación se aprobó y se generó la cuenta por pagar');
                            Find();
                        }
                        else {
                            ShowError('Se presentó un error al aprobar la liquidación')
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        //--------------------------------------------------------------------------------------------------------------------
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskValoresGrid = function (valor) {
            return MascaraValores(valor)
        };

    }]);