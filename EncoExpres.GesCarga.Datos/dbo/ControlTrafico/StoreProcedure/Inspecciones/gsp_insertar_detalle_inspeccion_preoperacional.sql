﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 09/01/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_insertar_detalle_inspeccion_preoperacional'
GO
DROP PROCEDURE gsp_insertar_detalle_inspeccion_preoperacional
GO
CREATE PROCEDURE gsp_insertar_detalle_inspeccion_preoperacional 
(    
@par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC, 
@par_ENIN_Numero NUMERIC,       
@par_DSFI_Codigo NUMERIC,    
@par_DIFI_Codigo NUMERIC,    
@par_Nombre VARCHAR(50),    
@par_Relevante SMALLINT,    
@par_Cumple SMALLINT,    
@par_Orden_Seccion SMALLINT,    
@par_Orden_Item SMALLINT,    
@par_Tamaño NUMERIC = NULL,    
@par_CATA_TCON_Codigo NUMERIC,    
@par_Valor VARCHAR(200) = NULL    
)    
AS    
BEGIN   
 
IF @par_Codigo > 0   
 BEGIN  
   
  UPDATE Detalle_Inspecciones 
  SET 
    ENIN_Numero  = @par_ENIN_Numero,
    DSFI_Codigo  = @par_DSFI_Codigo,  
    DIFI_Codigo = @par_DIFI_Codigo,  
    Nombre = @par_Nombre,  
    Relevante = @par_Relevante, 
	Cumple = @par_Cumple,   
    Orden_Seccion = @par_Orden_Seccion,  
    Orden_Item = @par_Orden_Item,  
    Tamaño = @par_Tamaño,  
    CATA_TCON_Codigo  = @par_CATA_TCON_Codigo,  
    Valor = @par_Valor

  WHERE  
     EMPR_Codigo = @par_EMPR_Codigo  
     AND ID = @par_Codigo   
 END  
ELSE   
 BEGIN  
  INSERT INTO Detalle_Inspecciones    
  (    
   EMPR_Codigo, 
   ENIN_Numero,
   DSFI_Codigo,     
   DIFI_Codigo,   
   Nombre,    
   Relevante,    
   Cumple,    
   Orden_Seccion,    
   Orden_Item,    
   Tamaño,    
   CATA_TCON_Codigo,    
   Valor
  )    
    
  VALUES     
  (    
   @par_EMPR_Codigo,    
   @par_ENIN_Numero,    
   @par_DSFI_Codigo,    
   @par_DIFI_Codigo,    
   @par_Nombre,    
   @par_Relevante, 
   @par_Cumple,       
   @par_Orden_Seccion,    
   @par_Orden_Item,    
   ISNULL(@par_Tamaño,0),    
   @par_CATA_TCON_Codigo,    
   ISNULL(@par_Valor,'')     
  )    
  SELECT @@IDENTITY AS Codigo    
 END    
END  
GO