﻿Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Fachada.SeguridadUsuarios
Imports EncoExpres.GesCarga.Fachada.Basico.General
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Seguridad
    Public NotInheritable Class LogicaUsuarioGrupoUsuarios
        Inherits LogicaBase(Of UsuarioGrupoUsuarios)

        Private Const GrupoActivo As Byte = 1 'Grupo Logueado
        Private Const CuentaActiva As Byte = 1
        Private Const GrupoExterno As Byte = 1
        Private Const NumeroMaximoIntentosIngresoGrupo As Byte = 3
        Private Const GrupoAdministrador As String = "ADMIN"
        Private Const GrupoDeslogueo As String = "LOGOUT"

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As PersistenciaUsuarioGrupoUsuarios

        ''' <summary>
        '''  Variable de entorno para el manejo de la persistencia de datos de empresa
        ''' </summary>
        Private _persistenciaEmpresa As PersistenciaEmpresas

        Sub New(persistencia As PersistenciaUsuarioGrupoUsuarios)
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As UsuarioGrupoUsuarios) As Respuesta(Of IEnumerable(Of UsuarioGrupoUsuarios))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of UsuarioGrupoUsuarios))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of UsuarioGrupoUsuarios))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As UsuarioGrupoUsuarios) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As UsuarioGrupoUsuarios) As Respuesta(Of UsuarioGrupoUsuarios)
            Throw New NotImplementedException()
        End Function

        Public Function Eliminar(filtro As UsuarioGrupoUsuarios) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaUsuarioGrupoUsuarios).Eliminar(filtro)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, filtro.CodigoGrupoUsuarios, Recursos.OperacionAnulo)}
        End Function

    End Class
End Namespace
