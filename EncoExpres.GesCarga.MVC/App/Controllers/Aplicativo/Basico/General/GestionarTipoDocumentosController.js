﻿EncoExpresApp.controller("GestionarTipoDocumentoCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'TipoDocumentosFactory', 'PlanUnicoCuentasFactory', 'OficinasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ValorCatalogosFactory, TipoDocumentosFactory, PlanUnicoCuentasFactory, OficinasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Tipo Documento' }, { Nombre: 'Gestionar' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
 

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_TIPO_DOCUMENTOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular 
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Codigo = 0;
        $scope.ModeloCodigo = 0;
        //$scope.ListadoTipoNumeracion = [];
        $scope.ListaPuc = [];
        $scope.ModeloEstado = [];
        //$scope.CodigoTipoNumeracion = [];
        //$scope.CodigoTipoNumeracion = 0;
        $scope.ListadoEstados = [
            { Nombre: 'Seleccione Item', Codigo: -1 },
            { Nombre: 'Activa', Codigo: 1 },
            { Nombre: 'Inactiva', Codigo: 0 }
        ];


        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');


        // Se crea la propiedad modelo en el ambito
        //$scope.Modelo = {
        //    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        //    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        //    Codigo: 0,
        //    Estado: $scope.ListadoEstados[0],

        //}
        /* Obtener parametros del enrutador*/
        //-----Asignaciones ---------------------

        $scope.AsignarCodigoAlterno = function (CodigoAlterno) {
            $scope.ModeloCodigoAlterno = CodigoAlterno;
        }

        //----------------------------------------
               $scope.ListadoTipoNumeracion = [];
    
        $scope.ListadoTipoNumeracion = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_NUMERACION },
            Sync: true
        }).Datos;

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando Impuesto Código ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Impuesto Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            blockUI.delay = 1000;
            TipoDocumentosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        
                        $scope.ModeloCodigo = response.data.Datos.Codigo;

                        $scope.ModeloNombre = response.data.Datos.Nombre;

                        $scope.ModeloCodigo = response.data.Datos.Codigo;

                        $scope.ModeloConsecutivo = response.data.Datos.Consecutivo;
                        $scope.ModeloConsecutivoHasta = response.data.Datos.ConsecutivoFin;
                        $scope.ModeloControlConsecutivo = response.data.Datos.ControladorConsecutivo;
                        $scope.ModeloDocumentosFaltantes = response.data.Datos.DocumentosFaltantesAviso;
                        $scope.ModeloNotaInicio = response.data.Datos.NotaInicio;
                        $scope.ModeloNotaFin = response.data.Datos.NotaFin;
                        $scope.ModalTipoNumeracion = $linq.Enumerable().From($scope.ListadoTipoNumeracion).First('$.Codigo == ' + response.data.Datos.TipoNumeracion);
                        $scope.ModeloGeneraComprobanteContable = response.data.Datos.GeneraComprobanteContable == 1 ? true : false;
                 
                        $scope.CodigoEstado = response.data.Datos.Estado;

                        if ($scope.ListadoEstados !== null && $scope.ListadoEstados !== undefined) {
                            if ($scope.ListadoEstados.length > 0) {
                                $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + $scope.CodigoEstado);
                            }
                        }

                    }
                    else {
                        ShowError('No se logro consultar el Tipo Documento ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarTipoDocumentos';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarTipoDocumentos';
                });
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardarImpuesto = function () {
            showModal('modalConfirmacionGuardarImpuesto');

        };

        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarImpuesto');
            if (DatosRequeridosImpuesto()) {
                $scope.objEnviar = {


                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModeloCodigo,
                    Nombre: $scope.ModeloNombre,
                    Consecutivo: $scope.ModeloConsecutivo,
                    ConsecutivoFin: $scope.ModeloConsecutivoHasta,
                    ControladorConsecutivo: $scope.ModeloControlConsecutivo,
                    DocumentosFaltantesAviso: $scope.ModeloDocumentosFaltantes,
                    NotaInicio: $scope.ModeloNotaInicio,
                    NotaFin: $scope.ModeloNotaFin,
                    TipoNumeracion: $scope.ModalTipoNumeracion.Codigo,
                    Estado: $scope.ModeloEstado.Codigo,
                    AplicaDetalle: CERO,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    GeneraComprobanteContable: $scope.ModeloGeneraComprobanteContable == true || $scope.ModeloGeneraComprobanteContable == ESTADO_ACTIVO ? ESTADO_ACTIVO : 0

                };

                $scope.ModeloEstado = $scope.ModeloEstado.Codigo
                TipoDocumentosFactory.Guardar($scope.objEnviar).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.ModeloCodigo == 0) {
                                    ShowSuccess('Se guardó el Tipo Documento: ' + $scope.ModeloNombre);
                                }
                                else {
                                    ShowSuccess('Se modificó el Tipo Documento: ' + $scope.ModeloNombre);
                                }
                                location.href = '#!ConsultarTipoDocumentos/' + $scope.Codigo;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridosImpuesto() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;


            if ($scope.ModeloNombre == undefined || $scope.ModeloNombre == "") {
                $scope.MensajesError.push('Debe ingresar el nombre del Impuesto');
                continuar = false;
            }

            if ($scope.ModalTipoNumeracion.Codigo == -1 || $scope.ModalTipoNumeracion.Codigo == undefined || $scope.ModalTipoNumeracion.Codigo == null || $scope.ModalTipoNumeracion.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar el Tipo del Impuesto');
                continuar = false;
            }



            return continuar;
        }
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);

        };

        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope)
        }


        if ($routeParams.Codigo !== undefined) {
            $scope.Codigo = $routeParams.Codigo;
            Obtener();
        }

        else {
            $scope.Codigo = 0;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($routeParams.Codigo > 0) {
                document.location.href = '#!ConsultarTipoDocumentos/';
            } else {
                document.location.href = '#!ConsultarTipoDocumentos';
            }
        };
    }]);