﻿PRINT 'gsp_insertar_novedades_despachos'
GO
DROP PROCEDURE gsp_insertar_novedades_despachos
GO
CREATE PROCEDURE gsp_insertar_novedades_despachos   
(    
@par_EMPR_Codigo SMALLINT,    
@par_Codigo_Alterno VARCHAR(50) = NULL,    
@par_Nombre VARCHAR(50),    
@par_Estado SMALLINT,      
@par_USUA_Codigo_Crea SMALLINT    
)    
AS BEGIN   
  
DECLARE @intCodigo NUMERIC = (select isnull(MAX(Codigo), 0) + 1 from Novedades_Despacho)    
   
INSERT INTO Novedades_Despacho    
(    
EMPR_Codigo,   
Codigo,   
Codigo_Alterno,    
Nombre,   
Novedad_Sistema,   
Estado,     
USUA_Codigo_Crea,    
Fecha_Crea    
)    
VALUES     
(    
@par_EMPR_Codigo,    
@intCodigo,  
@par_Codigo_Alterno,  
@par_Nombre,   
0,    
@par_Estado,    
@par_USUA_Codigo_Crea,    
GETDATE()    
)    
    
SELECT @intCodigo AS Codigo    
END    
GO