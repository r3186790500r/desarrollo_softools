﻿Print 'gsp_consultar_interfaz_contable_psl'
GO
DROP PROCEDURE gsp_consultar_interfaz_contable_psl
GO
CREATE PROCEDURE gsp_consultar_interfaz_contable_psl
(
	@par_EMPR_Codigo smallint,
	@par_TIDO_Codigo numeric = NULL,
	@par_Fecha_Inicial datetime = NULL,
	@par_Fecha_Final datetime = NULL,
	@par_Numero_Documento_Origen numeric = NULL,
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL  
)
AS                                 
BEGIN                        
	SET NOCOUNT ON;                              
	DECLARE @CantidadRegistros INT                        
	SELECT                        
	@CantidadRegistros = (SELECT COUNT(1)

	FROM Documentos_Interfaz_Contable_PSL DPSL

	LEFT JOIN Valor_Catalogos as CATA_DPSL ON
	DPSL.EMPR_Codigo = CATA_DPSL.EMPR_Codigo
	AND convert(varchar,  DPSL.TIDO_Codigo_Origen) = CATA_DPSL.Campo1

	WHERE DPSL.EMPR_Codigo = @par_EMPR_Codigo
	AND DPSL.Numero_Documento_Origen = ISNULL(@par_Numero_Documento_Origen, DPSL.Numero_Documento_Origen)
	AND DPSL.TIDO_Codigo_Origen = ISNULL(@par_TIDO_Codigo, DPSL.TIDO_Codigo_Origen)
	AND DPSL.Fecha_Crea >= ISNULL(@par_Fecha_Inicial, DPSL.Fecha_Crea)
	AND DPSL.Fecha_Crea <= ISNULL(@par_Fecha_Final, DPSL.Fecha_Crea)
);                        
WITH Pagina
AS
(
	SELECT
	DPSL.EMPR_Codigo,
	DPSL.Codigo,
	ISNULL(DPSL.Numero_Origen, 0) AS Numero_Origen,
	ISNULL(DPSL.Numero_Documento_Origen, 0) AS Numero_Documento_Origen,
	DPSL.Fecha_Crea,
	ISNULL(DPSL.TIDO_Codigo_Origen, 0) AS TIDO_Codigo_Origen,
	ISNULL(CATA_DPSL.Campo1, '') AS TIDO_Nombre_Origen,
	ISNULL(DPSL.Tipo_Documento_PSL, '') AS Tipo_Documento_PSL,
	ISNULL(DPSL.Mensaje_PSL, '') AS Mensaje_PSL,
	ISNULL(DPSL.Fecha_Mensaje_PSL, '') AS Fecha_Mensaje_PSL,
	ISNULL(DPSL.Numero_Proceso_PSL, 0) AS Numero_Proceso_PSL,
	ISNULL(DPSL.Proceso_PSL, 0) AS Proceso_PSL,
	ISNULL(DPSL.Estado_Proceso_PSL, '') AS Estado_Proceso_PSL,
	ROW_NUMBER() OVER (ORDER BY DPSL.Numero_Origen) AS RowNumber
	        
	FROM Documentos_Interfaz_Contable_PSL DPSL

	LEFT JOIN Valor_Catalogos as CATA_DPSL ON
	DPSL.EMPR_Codigo = CATA_DPSL.EMPR_Codigo
	AND convert(varchar,  DPSL.TIDO_Codigo_Origen) = CATA_DPSL.Campo2

	WHERE DPSL.EMPR_Codigo = @par_EMPR_Codigo
	AND DPSL.Numero_Documento_Origen = ISNULL(@par_Numero_Documento_Origen, DPSL.Numero_Documento_Origen)
	AND DPSL.TIDO_Codigo_Origen = ISNULL(@par_TIDO_Codigo, DPSL.TIDO_Codigo_Origen)
	AND DPSL.Fecha_Crea >= ISNULL(@par_Fecha_Inicial, DPSL.Fecha_Crea)
	AND DPSL.Fecha_Crea <= ISNULL(@par_Fecha_Final, DPSL.Fecha_Crea)
)                        
	SELECT
	EMPR_Codigo,
	Codigo,
	Numero_Origen,
	Numero_Documento_Origen,
	Fecha_Crea,
	TIDO_Codigo_Origen,
	TIDO_Nombre_Origen,
	Tipo_Documento_PSL,
	Mensaje_PSL,
	Fecha_Mensaje_PSL,
	Numero_Proceso_PSL,
	Proceso_PSL,
	Estado_Proceso_PSL,

	@CantidadRegistros AS TotalRegistros,
	@par_NumeroPagina AS PaginaObtener,
	@par_RegistrosPagina AS RegistrosPagina

	FROM Pagina                        
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                        
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
END         
GO