﻿PRINT 'Opción Menú Fidelizacion - Documentos'
GO
DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 6501
GO
DELETE Menu_Aplicaciones WHERE Codigo = 6501
GO
INSERT INTO Menu_Aplicaciones( 
EMPR_Codigo,
Codigo,
MOAP_Codigo,
Nombre,
Padre_Menu,
Orden_Menu,
Mostrar_Menu,
Aplica_Habilitar,
Aplica_Consultar,
Aplica_Actualizar,
Aplica_Eliminar_Anular,
Aplica_Imprimir,
Aplica_Contabilidad,
Opcion_Permiso,
Opcion_Listado,
Aplica_Ayuda,
Url_Pagina,
Url_Ayuda) 
SELECT Codigo
,6501
,65
,'Documentos'
,65
,10
,1
,1
,1
,1
,1
,1
,1
,0
,0
,0
,'#'
,'#' 
FROM Empresas
GO
---------------------------------------------------------------------------------------------------
PRINT 'Opción Menú Fidelizacion - Fidelizacion'
GO
DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 650101
GO
DELETE Menu_Aplicaciones WHERE Codigo = 650101
GO
INSERT INTO Menu_Aplicaciones( 
EMPR_Codigo,
Codigo,
MOAP_Codigo,
Nombre,
Padre_Menu,
Orden_Menu,
Mostrar_Menu,
Aplica_Habilitar,
Aplica_Consultar,
Aplica_Actualizar,
Aplica_Eliminar_Anular,
Aplica_Imprimir,
Aplica_Contabilidad,
Opcion_Permiso,
Opcion_Listado,
Aplica_Ayuda,
Url_Pagina,
Url_Ayuda) 
SELECT Codigo
,650101
,65
,'Fidelización'
,6501
,10
,1
,1
,1
,1
,1
,1
,1
,0
,0
,0
,'#!ConsultarFidelizacion'
,'#' 
FROM Empresas
GO
---------------------------------------------------------------------------------------------------
PRINT 'Opción Menú Fidelizacion - Causales Fidelizacion'
GO
DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 650102
GO
DELETE Menu_Aplicaciones WHERE Codigo = 650102
GO
INSERT INTO Menu_Aplicaciones( 
EMPR_Codigo,
Codigo,
MOAP_Codigo,
Nombre,
Padre_Menu,
Orden_Menu,
Mostrar_Menu,
Aplica_Habilitar,
Aplica_Consultar,
Aplica_Actualizar,
Aplica_Eliminar_Anular,
Aplica_Imprimir,
Aplica_Contabilidad,
Opcion_Permiso,
Opcion_Listado,
Aplica_Ayuda,
Url_Pagina,
Url_Ayuda) 
SELECT Codigo
,650102
,65
,'Causales Fidelización'
,6501
,20
,1
,1
,1
,1
,1
,1
,1
,0
,0
,0
,'#!ConsultarCausalesFidelizacion'
,'#' 
FROM Empresas
GO
---------------------------------------------------------------------------------------------------
PRINT 'Opción Menú Fidelizacion - Despachos Fidelizacion'
GO
DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 650103
GO
DELETE Menu_Aplicaciones WHERE Codigo = 650103
GO
INSERT INTO Menu_Aplicaciones( 
EMPR_Codigo,
Codigo,
MOAP_Codigo,
Nombre,
Padre_Menu,
Orden_Menu,
Mostrar_Menu,
Aplica_Habilitar,
Aplica_Consultar,
Aplica_Actualizar,
Aplica_Eliminar_Anular,
Aplica_Imprimir,
Aplica_Contabilidad,
Opcion_Permiso,
Opcion_Listado,
Aplica_Ayuda,
Url_Pagina,
Url_Ayuda) 
SELECT Codigo
,650103
,65
,'Despachos Fidelización'
,6501
,30
,1
,1
,1
,1
,1
,1
,1
,0
,0
,0
,'#!ConsultarDespachosFidelizacion'
,'#' 
FROM Empresas
GO