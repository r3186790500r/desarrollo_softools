﻿EncoExpresApp.controller("GestionarGrupoReferenciasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'GrupoReferenciasFactory', 'PlanUnicoCuentasFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, GrupoReferenciasFactory, PlanUnicoCuentasFactory) {

        $scope.Titulo = 'GESTIONAR GRUPO REFERENCIAS';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Almacén' }, { Nombre: 'Grupo Referencias' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GRUPO_REFERENCIAS);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }
        $scope.ListadoCuentasPUC = [];
        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVO" },
            { Codigo: 1, Nombre: "ACTIVO" },
        ]
       
        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;      
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }

        $scope.LongitudCuentaPUC = 0;// verifica la cantidad de datos ingresados  de la ciudad
        $scope.IngresoValidaCuentaPUC = true

        $scope.AsignarCuentaPUC = function (CuentaPUC) {
            $scope.IngresoValidaCuentaPUC = true
            if (CuentaPUC != undefined && CuentaPUC != null && CuentaPUC != "" && CuentaPUC.Codigo != 0) {
                if (angular.isObject(CuentaPUC)) {
                    $scope.LongitudCuentaPUC = CuentaPUC.length;
                    $scope.IngresoValidaCuentaPUC = true;
                    $scope.NombreCuentaPUC = CuentaPUC.Nombre
                }
                else if (angular.isString(CuentaPUC)) {
                    $scope.LongitudCuentaPUC = CuentaPUC.length;
                    $scope.IngresoValidaCuentaPUC = false;
                    $scope.NombreCuentaPUC = ''
                }
            } else {
                $scope.NombreCuentaPUC = ''
            }

        };

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*Cargar Autocomplete de Cuentas PUC*/
        PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
         then(function (response) {
             if (response.data.ProcesoExitoso === true) {
                 if (response.data.Datos.length > 0) {
                     response.data.Datos.push({ CodigoCuenta: '', Codigo: 0 })
                     $scope.ListadoCuentasPUC = response.data.Datos;

                     if ($scope.CuentaPUC > 0) {
                         $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CuentaPUC);
                         $scope.NombreCuentaPUC = $scope.Modelo.CuentaPUC.Nombre
                     } else {
                         $scope.Modelo.CuentaPUC = '';
                     }
                 }
                 else {
                     $scope.ListadoCuentasPUC = [];
                     $scope.Modelo.CuentaPUC = null;
                 }
             }
         }, function (response) {
             ShowError(response.statusText);
         });

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando Grupo Referencia código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Grupo Referencia Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            GrupoReferenciasFactory.Obtener(filtros).
               then(function (response) {
                   if (response.data.ProcesoExitoso === true) {

                       $scope.Modelo.Codigo = response.data.Datos.Codigo;
                       $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                       $scope.Modelo.Nombre = response.data.Datos.Nombre;
                       $scope.Modelo.Marca = response.data.Datos.Nombre;

                       $scope.CuentaPUC = response.data.Datos.CuentaPUC.Codigo;
                       if ($scope.ListadoCuentasPUC.length > 0 && $scope.CuentaPUC > 0) {
                           $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + response.data.Datos.CuentaPUC.Codigo);
                           $scope.NombreCuentaPUC = $scope.Modelo.CuentaPUC.Nombre
                       }

                        
                       $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                   }
                   else {
                       ShowError('No se logro consultar el grupo referencia código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                       document.location.href = '#!ConsultarGrupoReferencias';
                   }
               }, function (response) {
                   ShowError(response.statusText);
                   document.location.href = '#!ConsultarGrupoReferencias';
               });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
                GrupoReferenciasFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el grupo referencia "' + $scope.Modelo.Nombre + '"');
                                    $scope.Modelo.Codigo = response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó el grupo referencia "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = '#!ConsultarGrupoReferencias/' + $scope.Modelo.Codigo;
                            }                          
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        
        
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
                $scope.MensajesError.push('Debe ingresar el nombre del grupo referencia');
                continuar = false;
            }
            if ($scope.IngresoValidaCuentaPUC == false) {
                $scope.MensajesError.push('Debe ingresar una cuenta PUC valida');
                continuar = false;
            }
            if ($scope.Modelo.CuentaPUC == undefined || $scope.Modelo.CuentaPUC == '' || $scope.Modelo.CuentaPUC == null) {
                $scope.Modelo.CuentaPUC = { Codigo: 0, CodigoCuenta: '', Nombre : '' }
            }

            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarGrupoReferencias/' + $scope.Modelo.Codigo;
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
        };
}]);