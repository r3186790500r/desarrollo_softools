﻿PRINT 'Tabla Rutas'
GO
CREATE TABLE [dbo].[Rutas](
	[EMPR_Codigo] [smallint] NOT NULL,
	[Codigo] [numeric](18, 0) NOT NULL,
	[Codigo_Alterno] [varchar](20) NOT NULL,
	[Nombre] [varchar]	(100) NOT NULL,
	[CIUD_Codigo_Origen] [numeric](18, 0) NOT NULL,
	[CIUD_Codigo_Destino] [numeric](18, 0) NOT NULL,
	[Estado] [smallint] NOT NULL,
	[USUA_Codigo_Crea] [smallint] NOT NULL,
	[Fecha_Crea] [datetime] NOT NULL,
	[USUA_Codigo_Modifica] [smallint] NULL,
	[Fecha_Modifica] [datetime] NULL,
 CONSTRAINT [PK_Rutas] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO