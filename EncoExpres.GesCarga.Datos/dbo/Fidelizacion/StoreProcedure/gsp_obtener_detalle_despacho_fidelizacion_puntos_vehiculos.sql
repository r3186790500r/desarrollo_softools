﻿Print 'gsp_obtener_detalle_despacho_fidelizacion_puntos_vehiculos'
GO
DROP PROCEDURE gsp_obtener_detalle_despacho_fidelizacion_puntos_vehiculos
GO
CREATE PROCEDURE gsp_obtener_detalle_despacho_fidelizacion_puntos_vehiculos
(
	@par_EMPR_Codigo smallint,
	@par_Codigo numeric
)
AS                                 
BEGIN                        
	SELECT
	DDPV.EMPR_Codigo,
	DDPV.ID AS Codigo,
	VEHI.Codigo AS VEHI_Codigo,
	VEHI.Placa AS VEHI_Placa,
	ENPD.Numero AS ENPD_Numero,
	ENPD.Numero_Documento AS ENPD_NumeroDocumento,
	ENMC.Numero AS ENMC_Numero,
	ENMC.Numero_Documento AS ENMC_NumeroDocumento,
	RUTA.Codigo AS RUTA_Codigo,
	RUTA.Nombre AS RUTA_Nombre,
	ISNULL(TENE.Codigo, 0) AS TENE_Codigo,
	IIF(TENE.Nombre is NULL OR TENE.Nombre = '', TENE.Razon_Social, CONCAT(TENE.Nombre,' ', TENE.Apellido1)) AS TENE_Nombre,
	ISNULL(COND.Codigo, 0) AS COND_Codigo,
	IIF(COND.Nombre is NULL OR COND.Nombre = '', COND.Razon_Social, CONCAT(COND.Nombre,' ', COND.Apellido1)) AS COND_Nombre,
	ISNULL(DDPV.Fecha_Inicio,'') AS FechaInicio,
	ISNULL(DDPV.Fecha_Vence,'') AS FechaFin,
	ISNULL(DDPV.Puntos, 0) AS Puntos,
	DDPV.Estado,
	DDPV.Anulado
		        
	FROM Detalle_Despachos_Plan_Puntos_Vehiculos DDPV

	INNER JOIN Vehiculos AS VEHI
	ON DDPV.EMPR_Codigo = VEHI.EMPR_Codigo
	AND DDPV.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS TENE
	ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Terceros AS COND
	ON VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo

	INNER JOIN Rutas AS RUTA
	ON DDPV.EMPR_Codigo = RUTA.EMPR_Codigo
	AND DDPV.RUTA_Codigo = RUTA.Codigo

	INNER JOIN Encabezado_Planilla_Despachos ENPD
	ON DDPV.EMPR_Codigo = ENPD.EMPR_Codigo
	AND DDPV.ENPD_Numero = ENPD.Numero

	INNER JOIN Encabezado_Manifiesto_Carga ENMC
	ON DDPV.EMPR_Codigo = ENMC.EMPR_Codigo
	AND DDPV.ENMC_Numero = ENMC.Numero

	LEFT JOIN Usuarios AS USUA
	ON DDPV.EMPR_Codigo = USUA.EMPR_Codigo
	AND DDPV.USUA_Codigo_Crea = USUA.Codigo

	WHERE DDPV.EMPR_Codigo = @par_EMPR_Codigo
	AND DDPV.ID = @par_Codigo
END
GO 