﻿Print 'V_Tipo_Documento_Empresas'
GO
DROP VIEW V_Tipo_Documento_Empresas
GO
CREATE VIEW V_Tipo_Documento_Empresas
AS
	SELECT EMPR_Codigo, Codigo, CATA_Codigo, Campo1 As Nombre FROM
	Valor_Catalogos
	WHERE CATA_Codigo = 1
GO