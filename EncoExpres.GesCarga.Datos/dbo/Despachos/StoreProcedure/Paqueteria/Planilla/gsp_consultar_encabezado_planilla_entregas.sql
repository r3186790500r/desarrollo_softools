﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_consultar_encabezado_planilla_entregas'
DROP PROCEDURE gsp_consultar_encabezado_planilla_entregas 
GO

CREATE PROCEDURE gsp_consultar_encabezado_planilla_entregas	(  
     @par_EMPR_Codigo SMALLINT,        
     @par_Numero_Documento NUMERIC = null,  
     @par_Fecha_incial DATE = null,  
	 @par_Fecha_fin DATE = null,  
     @par_Placa VARCHAR (50) = null,  
     @par_Conductor VARCHAR (150) = null,  
     @par_Estado NUMERIC = null,  
	 @par_Oficina NUMERIC = null,
	  @par_Anulado NUMERIC = null, 
     @par_NumeroPagina NUMERIC = NULL,      
     @par_RegistrosPagina NUMERIC = NULL
	
 )  
 AS  
 BEGIN  
  SET NOCOUNT ON;  
   DECLARE  
    @CantidadRegistros INT  
   SELECT @CantidadRegistros = (  
     SELECT DISTINCT   
      COUNT(1)   
      FROM Encabezado_Planilla_Entregas EPEN
	  ,Vehiculos VEHI
	  , Terceros TECO 
      WHERE  EPEN.EMPR_Codigo = VEHI.EMPR_Codigo
	  AND EPEN.VEHI_Codigo = VEHI.Codigo
	 
	 AND EPEN.EMPR_Codigo = TECO.EMPR_Codigo 
	 AND EPEN.TERC_Codigo_Conductor  = TECO.Codigo

	  AND EPEN.Numero_Documento = ISNULL (@par_Numero_Documento,EPEN.Numero_Documento)
	 AND EPEN.FECHA >= ISNULL(@par_Fecha_incial ,EPEN.FECHA)  
	 AND EPEN.FECHA <=ISNULL(@par_Fecha_fin,EPEN.FECHA ) 
	 AND VEHI.PLACA =  ISNULL(@par_Placa, VEHI.Placa)  
	 AND EPEN.OFIC_Codigo = ISNULL(@par_Oficina,EPEN.OFIC_Codigo)
	 AND EPEN.Estado = ISNULL(@par_Estado, EPEN.Estado )  
	 AND EPEN.Anulado = ISNULL(@par_Anulado,EPEN.Anulado)  

	 AND CONCAT(TECO.Razon_Social,' ',TECO.Nombre,' ',TECO.Apellido1,' ',TECO.Apellido2)   
    LIKE '%'+ ISNULL(@par_Conductor, CONCAT(TECO.Razon_Social,' ',TECO.Nombre,' ',TECO.Apellido1,' ',TECO.Apellido2))+'%'   

	        );         
     WITH Pagina AS  
     (  
      SELECT 
	  EPEN.Numero,
	  EPEN.Numero_Documento,
	  EPEN.Fecha,
	  VEHI.Placa,
	  EPEN.VEHI_Codigo ,
	   ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')  
	  +' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS NombreCondutor,
	  EPEN.Cantidad_Total,
	  EPEN.Peso_Total,
	  EPEN.Estado, 
	  EPEN.Anulado,
	  0 AS Obtener,
	  EPEN.OFIC_Codigo AS CodigoOficina,
	  EPEN.TERC_Codigo_Conductor,
      ROW_NUMBER() OVER ( ORDER BY EPEN.NUMERO) AS RowNumber  
	  
      FROM  
     Encabezado_Planilla_Entregas EPEN
	  ,Vehiculos VEHI
	  , Terceros TECO 
      WHERE  EPEN.EMPR_Codigo = VEHI.EMPR_Codigo
	  AND EPEN.VEHI_Codigo = VEHI.Codigo
	 
	 AND EPEN.EMPR_Codigo = TECO.EMPR_Codigo 
	 AND EPEN.TERC_Codigo_Conductor  = TECO.Codigo  
	 AND EPEN.EMPR_Codigo = @par_EMPR_Codigo
	 AND EPEN.Numero_Documento = ISNULL (@par_Numero_Documento,EPEN.Numero_Documento)
	 AND EPEN.FECHA >= ISNULL(@par_Fecha_incial ,EPEN.FECHA)  
	 AND EPEN.FECHA <=ISNULL(@par_Fecha_fin,EPEN.FECHA ) 
	 AND VEHI.PLACA =  ISNULL(@par_Placa, VEHI.Placa)  
	 AND EPEN.OFIC_Codigo = ISNULL(@par_Oficina,EPEN.OFIC_Codigo)
	 AND EPEN.Estado = ISNULL(@par_Estado, EPEN.Estado )  
	 AND EPEN.Anulado = ISNULL(@par_Anulado,EPEN.Anulado)  

	 AND CONCAT(TECO.Razon_Social,' ',TECO.Nombre,' ',TECO.Apellido1,' ',TECO.Apellido2)   
    LIKE '%'+ ISNULL(@par_Conductor, CONCAT(TECO.Razon_Social,' ',TECO.Nombre,' ',TECO.Apellido1,' ',TECO.Apellido2))+'%'   
	      ) 
   SELECT DISTINCT  
   Numero,
   Numero_Documento,
   Fecha,
   Placa,
   NombreCondutor,
   Cantidad_Total,
   Peso_Total,
   Estado,
   Anulado,
   Obtener,
   CodigoOficina,
   TERC_Codigo_Conductor,
   VEHI_Codigo,
	@CantidadRegistros AS TotalRegistros,  
    @par_NumeroPagina AS PaginaObtener,  
    @par_RegistrosPagina AS RegistrosPagina  
    FROM  
    Pagina  
    WHERE  
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    order by Numero_Documento  
   END   
GO