﻿Imports Newtonsoft.Json


Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="CheckListVehiculos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class CheckListVehiculos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CheckListVehiculos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CheckListVehiculos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Version = Read(lector, "Version")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
        End Sub
        <JsonProperty>
        Public Property Nombre As Integer
        <JsonProperty>
        Public Property Version As Integer

    End Class
End Namespace
