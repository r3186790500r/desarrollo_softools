﻿print 'gsp_insertar_documento_vehiculos'
go
drop procedure gsp_insertar_documento_vehiculos
go
create procedure gsp_insertar_documento_vehiculos
(
@par_EMPR_Codigo smallint ,
	@par_VEHI_Codigo numeric,
	@par_CDGD_Codigo numeric,
	@par_Elimina_Archivo numeric = null,
	@par_Referencia varchar(50) = NULL,
	@par_Emisor varchar(50) =NULL,
	@par_Fecha_Emision datetime =NULL,
	@par_Fecha_Vence datetime= NULL,
	@par_Nombre_Documento varchar(150)= NULL,
	@par_Archivo varbinary(MAX)= NULL,
	@par_Extension varchar(20)= NULL,
	@par_Tipo varchar(200)= NULL,
	@par_USUA_Codigo_Crea smallint
)
as
begin
IF	EXISTS(SELECT VEHI_Codigo FROM Vehiculo_Documentos WHERE EMPR_Codigo = @par_EMPR_Codigo AND CDGD_Codigo = @par_CDGD_Codigo AND VEHI_Codigo = @par_VEHI_Codigo )	
	BEGIN
	if	@par_Elimina_Archivo = 0
		BEGIN
					UPDATE Vehiculo_Documentos
					SET Referencia = @par_Referencia
				   ,Emisor = @par_Emisor
				   ,Fecha_Emision = @par_Fecha_Emision
				   ,Fecha_Vence = @par_Fecha_Vence
				   ,USUA_Codigo_Modifica = @par_USUA_Codigo_Crea
				   ,Fecha_Modifica = GETDATE()
				   ,Archivo = NULL
				   ,Nombre_Documento = NULL
				   ,Extension = NULL
				   ,Tipo = NULL
				   WHERE EMPR_Codigo = @par_EMPR_Codigo 
				   AND CDGD_Codigo = @par_CDGD_Codigo 
				   AND VEHI_Codigo = @par_VEHI_Codigo 
		END
	ELSE
		BEGIN
		UPDATE Vehiculo_Documentos
		SET Referencia = @par_Referencia
				   ,Emisor = @par_Emisor
				   ,Fecha_Emision = @par_Fecha_Emision
				   ,Fecha_Vence = @par_Fecha_Vence
				   ,USUA_Codigo_Modifica = @par_USUA_Codigo_Crea
				   ,Fecha_Modifica = GETDATE()
				   WHERE EMPR_Codigo = @par_EMPR_Codigo 
				   AND CDGD_Codigo = @par_CDGD_Codigo 
				   AND VEHI_Codigo = @par_VEHI_Codigo 
		END
	END
ELSE
BEGIN
INSERT INTO [dbo].[Vehiculo_Documentos]
           (
		    [EMPR_Codigo]
           ,[VEHI_Codigo]
           ,[CDGD_Codigo]
           ,[Referencia]
           ,[Emisor]
           ,[Fecha_Emision]
           ,[Fecha_Vence]
           ,[Nombre_Documento]
           ,[Archivo]
           ,[Extension]
           ,[Tipo]
           ,[USUA_Codigo_Crea]
           ,[Fecha_Crea]
           )
     VALUES
           (
		   @par_EMPR_Codigo ,
			@par_VEHI_Codigo ,
			@par_CDGD_Codigo ,
			@par_Referencia ,
			@par_Emisor ,
			@par_Fecha_Emision ,
			@par_Fecha_Vence,
			@par_Nombre_Documento ,
			@par_Archivo ,
			@par_Extension ,
			@par_Tipo ,
			@par_USUA_Codigo_Crea ,
			GETDATE()
			)
			
END
select @@ROWCOUNT as RegistrosAfectados
END
GO