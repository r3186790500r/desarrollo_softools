USE [ENCOEXPRES]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]
DROP CONSTRAINT if exists [FK_Encabezado_Planilla_Despachos_Oficinas];
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]
DROP COLUMN if exists [OFIC_Destino];
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]
ADD [OFIC_Destino] smallint;
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]  WITH CHECK 
ADD CONSTRAINT [FK_Encabezado_Planilla_Despachos_Oficinas] FOREIGN KEY([EMPR_Codigo], [OFIC_Destino])
REFERENCES [dbo].[Oficinas] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos] 
CHECK CONSTRAINT [FK_Encabezado_Planilla_Despachos_Oficinas]
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_obtener_planilla_paqueteria]
(                                        
 @par_EMPR_Codigo SMALLINT,                                        
 @par_Numero NUMERIC = NULL,                
 @par_Numero_Documento NUMERIC = NULL,                
 @par_TIDO_Codigo NUMERIC = NULL                      
)                                        
AS                                   
BEGIN                                    
 SELECT    
 ENPD.EMPR_Codigo,                                    
 ENPD.Numero,                                    
 ENPD.TIDO_Codigo,                                    
 ENPD.Numero_Documento,                                    
 ENPD.Fecha_Hora_Salida,                                    
 ENPD.Fecha,                                    
 ENPD.RUTA_Codigo,    
 RUTA.Nombre AS RUTA_Nombre,     
 RUTA.CATA_TIRU_Codigo,    
 ENPD.VEHI_Codigo,    
 ISNULL(ENPD.SEMI_Codigo, 0) AS SEMI_Codigo,                                   
 ENPD.Cantidad,    
 ENPD.Peso AS Peso,                                    
 ENPD.TATC_Codigo_Compra TATC_Codigo,     
 TATC.Nombre AS TATC_Nombre,    
 ENPD.TTTC_Codigo_Compra TTTC_Codigo,    
 TTTC.Nombre AS TTTC_Nombre,    
 ISNULL(ENPD.LNTC_Codigo_Compra, 0) AS LNTC_Codigo,                                  
 ISNULL(ENPD.TLNC_Codigo_Compra,0)AS TLNC_Codigo,                                  
 ISNULL(ENPD.ETCC_Numero, 0) AS ETCC_Numero,                                  
 ENPD.Valor_Flete_Transportador,                                    
 ENPD.Valor_Anticipo,                                    
 ENPD.Valor_Impuestos,                                    
 ENPD.Valor_Pagar_Transportador,
 ENPD.Valor_Flete_Cliente,    
 ENPD.Observaciones,                                    
 ENPD.Anulado,    
 ENPD.OFIC_Codigo,    
 ENPD.Estado,    
 ENPD.TERC_Codigo_Conductor,    
 ENPD.TERC_Codigo_Tenedor    
 ,ENPD.Anticipo_Pagado_A                        
 ,CASE WHEN ENDC.Codigo_Alterno = 'TRANSFERENCIA' THEN 1 ELSE 0 END AS PagoAnticipo                      
 ,ENRE_Numero_Masivo                    
 ,ENRE_Numero_Documento_Masivo                    
 ,ENPD.ECPD_Numero                
 ,ENPD.ELPD_Numero    
 ,ELGC.Numero AS ELGC_Numero          
 ,ENPD.Gastos_Agencia    
 ,ENPD.Recibido_Oficina
 ,ENPD.Precinto_Paqueteria
 ,ENPD.OFIC_Destino
                
 FROM Encabezado_Planilla_Despachos ENPD                                    
 LEFT JOIN Rutas AS RUTA                                    
 ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo                                    
 AND ENPD.RUTA_Codigo = RUTA.Codigo                         
        
 LEFT JOIN Tarifa_Transporte_Carga AS TATC        
 ON ENPD.EMPR_Codigo = TATC.EMPR_Codigo        
 AND ENPD.TATC_Codigo_Compra = TATC.Codigo        
        
 LEFT JOIN V_Tipo_Tarifa_Transporte_Carga AS TTTC        
 ON ENPD.EMPR_Codigo = TTTC.EMPR_Codigo        
 AND ENPD.TTTC_Codigo_Compra = TTTC.Codigo        
 AND ENPD.TATC_Codigo_Compra = TTTC.TATC_Codigo        
                      
 LEFT JOIN Encabezado_Documento_Cuentas AS ENDC                      
 ON ENDC.CATA_DOOR_Codigo = 2604                      
 AND ENDC.EMPR_Codigo = ENPD.EMPR_Codigo                   
 AND ENDC.Codigo_Documento_Origen = ENPD.Numero            
            
 LEFT JOIN Encabezado_Legalizacion_Gastos_Conductor AS ELGC            
 ON ENPD.EMPR_Codigo = ELGC.EMPR_Codigo            
 AND ENPD.Numero = ELGC.ENPD_Numero            
 AND ELGC.Anulado = 0            
                      
 WHERE                                  
 ENPD.EMPR_Codigo = @par_EMPR_Codigo                                    
 and (ENPD.Numero = @par_Numero OR @par_Numero IS NULL)                
 and (ENPD.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)                
 and (ENPD.TIDO_Codigo = @par_TIDO_Codigo OR @par_TIDO_Codigo IS NULL)                
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_encabezado_planilla_paqueteria_v2]
(        
 @par_EMPR_Codigo smallint        
 ,@par_Numero numeric(18,0) = 0        
 ,@par_TIDO_Codigo numeric(18,0)        
 ,@par_Fecha datetime  = NULL        
 ,@par_Fecha_Hora_Salida datetime  = NULL        
          
 ,@par_RUTA_Codigo numeric(18,0) = NULL        
 ,@par_VEHI_Codigo numeric(18,0) = NULL        
 ,@par_TERC_Codigo_Tenedor numeric(18,0) = NULL        
 ,@par_TERC_Codigo_Conductor numeric(18,0) = NULL        
 ,@par_SEMI_Codigo numeric(18,0) = NULL        
          
 ,@par_Cantidad numeric(18,2) = NULL        
 ,@par_Peso numeric(18,2) = NULL        
 ,@par_Valor_Flete_Transportador money = NULL        
 ,@par_Valor_Anticipo money = NULL        
 ,@par_Valor_Impuestos money = NULL        
        
 ,@par_Valor_Pagar_Transportador money = NULL  
 ,@par_Valor_Flete_Cliente money = NULL          
 ,@par_Observaciones varchar(200) = NULL        
        
 ,@par_Estado smallint = NULL        
 ,@par_TATC_Codigo_Compra smallint = NULL        
 ,@par_TTTC_Codigo_Compra smallint = NULL        
        
 ,@par_USUA_Codigo_Crea smallint         
 ,@par_OFIC_Codigo numeric        
 ,@par_ENRE_Numero_Documento_Masivo numeric = null        
 ,@par_ENRE_Numero_Masivo numeric = null    
    
 ,@par_LNTC_Codigo_Compra smallint = NULL        
 ,@par_TLNC_Codigo_Compra smallint = NULL        
 ,@par_ETCC_Numero numeric = NULL        
 ,@par_Valor_Auxiliares money  = NULL        
 ,@par_Numeracion varchar(50) = NULL
 ,@par_Precinto NUMERIC = NULL

 ,@par_OFIC_Destino smallint = null
)               
AS        
BEGIN        
 --SE VALIDA SI EL VEH�CULO TIENE CUMPLIDOS PENTIENDES POR REALIZAR                   
 IF (          
 SELECT COUNT (*) FROM Encabezado_Planilla_Despachos WHERE EMPR_CODIGO = @par_EMPR_Codigo AND VEHI_CODIGO = @par_VEHI_Codigo AND Anulado = 0           
 AND (ECPD_Numero = 0 OR ECPD_Numero IS NULL))>0           
 AND EXISTS (SELECT * FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = 3 AND Estado = 1         
 )             
 BEGIN             
  SELECT -3 AS Numero, -3 AS Numero_Documento -- EL VEH�CULO TIENE CUMPLIDOS PENDIENTES POR REALIZAR            
 END             
 ELSE         
 BEGIN                
  EXEC gsp_generar_consecutivo        
  @par_EMPR_Codigo,            
  @par_TIDO_Codigo,          
  @par_OFIC_Codigo, --> Oficina          
  @par_Numero OUTPUT         
        
  INSERT INTO Encabezado_Planilla_Despachos    
  (            
  EMPR_Codigo          
  ,TIDO_Codigo        
  ,Numero_Documento         
  ,ENMC_Numero         
  ,Fecha_Hora_Salida        
  ,Fecha        
  ,RUTA_Codigo        
  ,VEHI_Codigo        
  ,SEMI_Codigo         
  ,TERC_Codigo_Tenedor        
  ,TERC_Codigo_Conductor        
  ,Cantidad        
  ,Peso        
  ,TATC_Codigo_Compra        
  ,TTTC_Codigo_Compra           
  ,LNTC_Codigo_Compra        
  ,TLNC_Codigo_Compra        
  ,ETCC_Numero        
  ,Valor_Auxiliares        
  ,Numeracion        
  ,Valor_Flete_Transportador        
  ,Valor_Anticipo        
  ,Valor_Impuestos          
  ,Valor_Pagar_Transportador
  ,Valor_Flete_Cliente    
  ,Valor_Seguro_Mercancia    
  ,Valor_Otros_Cobros    
  ,Valor_Total_Credito    
  ,Valor_Total_Contado    
  ,Valor_Total_Alcobro    
  ,Observaciones        
  ,Anulado                       
  ,Estado        
  ,Fecha_Crea        
  ,USUA_Codigo_Crea        
  ,OFIC_Codigo          
  ,Ultimo_Seguimiento_Vehicular             
  ,ENRE_Numero_Masivo            
  ,ENRE_Numero_Documento_Masivo
  ,Precinto_Paqueteria
  ,OFIC_Destino
  )           
  VALUES        
  (@par_EMPR_Codigo          
  ,@par_TIDO_Codigo         
  ,@par_Numero          
  ,0          
  ,ISNULL(@par_Fecha_Hora_Salida ,'')        
  ,ISNULL(@par_Fecha,'')        
  ,ISNULL(@par_RUTA_Codigo ,0)        
  ,ISNULL(@par_VEHI_Codigo ,0)        
  ,ISNULL(@par_SEMI_Codigo ,0)        
  ,ISNULL(@par_TERC_Codigo_Tenedor ,0)        
  ,ISNULL(@par_TERC_Codigo_Conductor ,0)        
  ,ISNULL(@par_Cantidad ,0)          
  ,ISNULL(@par_Peso ,0)        
  ,@par_TATC_Codigo_Compra         
  ,@par_TTTC_Codigo_Compra        
  ,@par_LNTC_Codigo_Compra        
  ,@par_TLNC_Codigo_Compra        
  ,@par_ETCC_Numero        
  ,@par_Valor_Auxiliares        
  ,@par_Numeracion        
  ,ISNULL(@par_Valor_Flete_Transportador ,0)        
  ,ISNULL(@par_Valor_Anticipo ,0)           
  ,ISNULL(@par_Valor_Impuestos,0)          
  ,ISNULL(@par_Valor_Pagar_Transportador ,0)   
  ,ISNULL(@par_Valor_Flete_Cliente ,0)    
  ,0--Seguro Mercancia       
  ,0--Otros Cobros    
  ,0--Total Credito    
  ,0--Total Contado    
  ,0--Total al cobro    
  ,ISNULL(@par_Observaciones,'')        
  ,0         
  ,ISNULL(@par_Estado ,1)        
  ,GETDATE()         
  ,@par_USUA_Codigo_Crea           
  ,@par_OFIC_Codigo             
  ,GETDATE()                
  ,@par_ENRE_Numero_Masivo            
  ,@par_ENRE_Numero_Documento_Masivo
  ,@par_Precinto
  ,@par_OFIC_Destino
  )        
        
  Declare @NumeroInternoPlanilla Numeric = scope_identity()
        
  IF(@NumeroInternoPlanilla > 0)
  BEGIN

  IF @par_Precinto IS NOT NULL
  BEGIN
	UPDATE Detalle_Asignacion_Precintos_Oficina set ENPD_Numero = @NumeroInternoPlanilla
	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND Numero_Precinto = @par_Precinto
  END

  INSERT INTO Detalle_Seguimiento_Vehiculos (        
  EMPR_Codigo,          
  ENPD_Numero,          
  ENOC_Numero,          
  ENMC_Numero,          
  Fecha_Reporte,          
  CATA_TOSV_Codigo,          
  Ubicacion,          
  Longitud,            
  Latitud,          
  PUCO_Codigo,           
  CATA_SRSV_Codigo,          
  CATA_NOSV_Codigo,          
  Observaciones,          
  Kilometros_Vehiculo,          
  Reportar_Cliente,          
  Envio_Reporte_Cliente,            
  Fecha_Reporte_Cliente,          
  USUA_Codigo_Crea,          
  Fecha_Crea,          
  Anulado,          
  Orden)          
  VALUES           
  (          
  @par_EMPR_Codigo,          
  @NumeroInternoPlanilla,          
  0,          
  0,          
  getdate(),          
  8001,          
  '',          
  0,          
  0,          
  0,          
  8202,          
  8100,          
  'PLANILLA DESPACHOS',          
  0,          
  0,          
  0,          
  '',          
  @par_USUA_Codigo_Crea,          
  GETDATE(),          
  0,          
  1          
  )        
  END        
 END        
              
 SELECT TOP(1) Numero, Numero_Documento FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo        
 AND Numero_Documento = @par_Numero        
 AND TIDO_Codigo = @par_TIDO_Codigo        
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_modificar_encabezado_planilla_paqueteria]
(                                  
 @par_EMPR_Codigo smallint                                   
 ,@par_Numero numeric(18,0)                                   
 ,@par_Fecha datetime  = NULL                                  
 ,@par_Fecha_Hora_Salida datetime  = NULL                                  
 ,@par_RUTA_Codigo numeric(18,0) = NULL                                  
                                  
 ,@par_VEHI_Codigo numeric(18,0) = NULL                                  
 ,@par_SEMI_Codigo numeric(18,0) = NULL                                  
 ,@par_TERC_Codigo_Tenedor numeric(18,0) = NULL                                  
 ,@par_TERC_Codigo_Conductor numeric(18,0) = NULL                                  
 ,@par_Cantidad numeric(18,2) = NULL                                  
                                  
 ,@par_Peso numeric(18,2) = NULL                                  
 ,@par_TATC_Codigo smallint = NULL                                  
 ,@par_TTTC_Codigo smallint = NULL    
 ,@par_Valor_Flete_Transportador money = NULL                            
                                  
 ,@par_Valor_Anticipo money = NULL                                  
 ,@par_Valor_Impuestos money = NULL                                  
 ,@par_Valor_Pagar_Transportador money = NULL                                  
 ,@par_Valor_Flete_Cliente money = NULL                                  
      
 ,@par_LNTC_Codigo_Compra NUMERIC = NULL      
 ,@par_TLNC_Codigo_Compra NUMERIC = NULL      
 ,@par_ETCC_Numero NUMERIC = NULL      
 ,@par_Valor_Auxiliares money  = NULL        
 ,@par_Numeracion varchar(50) = NULL      
                        
 ,@par_Observaciones varchar(200) = NULL                                  
 ,@par_Estado smallint = NULL                                  
 ,@par_USUA_Codigo_Crea smallint    
 ,@par_ENRE_Numero_Documento_Masivo  numeric = null                            
 ,@par_ENRE_Numero_Masivo  numeric = null
 ,@par_Precinto NUMERIC = NULL       
 
 ,@par_OFIC_Destino smallint = null
)                                  
AS                                  
BEGIN      
 UPDATE Encabezado_Remesas SET ENPD_Numero = 0 where EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero    
 UPDATE Encabezado_Recolecciones SET ENPR_Numero = 0 where  EMPR_Codigo = @par_EMPR_Codigo AND ENPR_Numero = @par_Numero    
 DELETE Detalle_Planilla_Despachos where  EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero                                  
 DELETE Detalle_Planilla_Recolecciones where  EMPR_Codigo = @par_EMPR_Codigo AND ENPR_Numero = @par_Numero                                  
 DELETE Detalle_Auxiliares_Planilla_Despachos where  EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero                                  
 DELETE Detalle_Impuestos_Planilla_Despachos where  EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero                                  
 DELETE Detalle_Conductores_Planilla_Despachos where  EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero       
 
update Detalle_Asignacion_Precintos_Oficina SET ENPD_Numero = 0
where EMPR_Codigo = @par_EMPR_Codigo
and ENPD_Numero = @par_Numero
 
                        
 UPDATE Encabezado_Planilla_Despachos                               
 set Fecha_Hora_Salida = ISNULL(@par_Fecha_Hora_Salida ,'')                                  
 ,Fecha = ISNULL(@par_Fecha,'')                                  
 ,RUTA_Codigo = ISNULL(@par_RUTA_Codigo ,0)                                  
 ,VEHI_Codigo = ISNULL(@par_VEHI_Codigo ,0)          
 ,SEMI_Codigo = ISNULL(@par_SEMI_Codigo ,0)                                  
 ,TERC_Codigo_Tenedor =  ISNULL(@par_TERC_Codigo_Tenedor ,0)                                  
 ,TERC_Codigo_Conductor = ISNULL(@par_TERC_Codigo_Conductor ,0)                               
 ,Cantidad = ISNULL(@par_Cantidad ,0)                                  
 ,Peso = ISNULL(@par_Peso ,0)                                  
 ,Valor_Flete_Transportador =  ISNULL(@par_Valor_Flete_Transportador ,0)                                  
 ,Valor_Anticipo = ISNULL(@par_Valor_Anticipo ,0)                                  
 ,Valor_Impuestos  = ISNULL(@par_Valor_Impuestos ,0)                                  
                            
 ,Valor_Pagar_Transportador = ISNULL(@par_Valor_Pagar_Transportador ,0)  
 ,Valor_Flete_Cliente = ISNULL(@par_Valor_Flete_Cliente ,0)    
 ,Observaciones = ISNULL(@par_Observaciones,'')                                  
 ,Estado =   ISNULL(@par_Estado ,0)                                  
 ,Fecha_Modifica = GETDATE()                                   
 ,USUA_Codigo_Modifica = @par_USUA_Codigo_Crea    
 ,ENRE_Numero_Masivo =@par_ENRE_Numero_Masivo                    
 ,ENRE_Numero_Documento_Masivo = @par_ENRE_Numero_Documento_Masivo    
 ,TATC_Codigo_Compra = ISNULL(@par_TATC_Codigo ,TATC_Codigo_Compra)      
 ,TTTC_Codigo_Compra = ISNULL(@par_TTTC_Codigo ,TTTC_Codigo_Compra)      
 ,LNTC_Codigo_Compra = ISNULL(@par_LNTC_Codigo_Compra ,LNTC_Codigo_Compra)      
 ,TLNC_Codigo_Compra = ISNULL(@par_TLNC_Codigo_Compra ,TLNC_Codigo_Compra)      
 ,Valor_Auxiliares = ISNULL(@par_Valor_Auxiliares ,Valor_Auxiliares)      
 ,Numeracion = ISNULL(@par_Numeracion ,Numeracion)
 ,Precinto_Paqueteria = @par_Precinto
 ,OFIC_Destino = @par_OFIC_Destino
 where  EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_Numero
 
 IF @par_Precinto IS NOT NULL
 BEGIN
	update Detalle_Asignacion_Precintos_Oficina SET ENPD_Numero = @par_Numero
	where EMPR_Codigo = @par_EMPR_Codigo
	and Numero_Precinto = @par_Precinto
 END                              
     
 select Numero, Numero_Documento from Encabezado_Planilla_Despachos  where EMPR_Codigo = @par_EMPR_Codigo AND  Numero = @par_Numero                             
END
GO


