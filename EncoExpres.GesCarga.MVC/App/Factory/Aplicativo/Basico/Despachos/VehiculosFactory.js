﻿EncoExpresApp.factory('VehiculosFactory', ['$http', function ($http) {

    var urlService = GetUrl('Vehiculos');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    } 

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar'; 
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ConsultarPropietarios = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarPropietarios';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.GuardarPropietarios = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GuardarPropietarios';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ObtenerVehiculoEstudioSeguridad = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerVehiculoEstudioSeguridad';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ConsultarDocumentosProximosVencer = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarDocumentosProximosVencer';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.ConsultarDocumentosSoatRTM = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarDocumentosSoatRTM';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request) }

    };
    factory.GenerarPlanitilla = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GenerarPlanitilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ConsultarListaNegra = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarListaNegra';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ConsultarEstadoListaNegra = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarEstadoListaNegra';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request) }

    };
    factory.GuardarListaNegra = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GuardarListaNegra';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.ConsultarListaDocumentosPendientes = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarDocumentosPendientes';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request) }
    };

    factory.ConsultarAuditoria = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarAuditoria';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.GuardarNovedad = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/GuardarNovedad';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.ConsultarDocumentos = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarDocumentos';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    return factory;
}]);