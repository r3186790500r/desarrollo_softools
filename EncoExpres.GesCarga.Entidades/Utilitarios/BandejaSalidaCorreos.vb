﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Utilitarios

    ''' <summary>
    ''' Clase <see cref="BandejaSalidaCorreos"></see>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class BandejaSalidaCorreos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="BandejaSalidaCorreos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="BandejaSalidaCorreos"/>
        ''' </summary>
        ''' <param name="lector">Objeto DataReader</param>
        Sub New(lector As IDataReader)
            Id = Read(lector, "ID")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            NumeroDocumeto = Read(lector, "Numero_Documento")
            CuentaCorreoDe = Read(lector, "Cuenta_Correo_De")
            CuentaCorreoPara = Read(lector, "Cuenta_Correo_Para")
            CuentaCorreoCopia = Read(lector, "Cuenta_Correo_Copia")
            AsuntoCorreo = Read(lector, "Asunto_Correo")
            Contenido = Read(lector, "Texto_Correo")
            FechaCrea = Read(lector, "Fecha_Crea")
            CodigoUsuarioCrea = Read(lector, "USUA_Codigo_Crea")
            FechaModifica = Read(lector, "Fecha_Modifica")
            FechaEnvio = Read(lector, "Fecha_Envio")
            NumeroIntentosEnvio = Read(lector, "Numero_Intentos_Envio")
            MensajeErrorSMTP = Read(lector, "Mensaje_Error_SMTP")
            Estado = Read(lector, "Estado")


        End Sub

        ''' <summary>
        ''' Obtiene o establece el Identificador unico del registro
        ''' </summary>
        <JsonProperty>
        Public Property Id As Long

        ''' <summary>
        ''' Obtiene o establece el numero del documento
        ''' </summary>
        <JsonProperty>
        Public Property NumeroDocumeto As Long

        ''' <summary>
        ''' Obtiene o establece la cuenta de correo remitente
        ''' </summary>
        <JsonProperty>
        Public Property CuentaCorreoDe As String

        ''' <summary>
        ''' Obtiene o establece la(s) cuentas de correo destinatario(s)
        ''' </summary>
        <JsonProperty>
        Public Property CuentaCorreoPara As String

        ''' <summary>
        ''' Obtiene o establece la(s) cuentas de correo copia
        ''' </summary>
        <JsonProperty>
        Public Property CuentaCorreoCopia As String

        ''' <summary>
        ''' Obtiene o establece el asunto del correo
        ''' </summary>
        <JsonProperty>
        Public Property AsuntoCorreo As String

        ''' <summary>
        ''' Obtiene o establece el contenido del mensaje
        ''' </summary>
        <JsonProperty>
        Public Property Contenido As String

        ''' <summary>
        ''' Obtiene o establece el codigo del usuario que creo al registro
        ''' </summary>
        <JsonProperty>
        Public Property CodigoUsuarioCrea As Short

        ''' <summary>
        ''' Obtiene o establece el estado del Correo
        ''' </summary>
        <JsonProperty>
        Public Property EstadoCorreo As ValorCatalogos

        ''' <summary>
        ''' Obtiene o establece la fecha de envio
        ''' </summary>
        <JsonProperty>
        Public Property FechaEnvio As DateTime

        ''' <summary>
        ''' Obtiene o establece numero intentos de envio
        ''' </summary>
        <JsonProperty>
        Public Property NumeroIntentosEnvio As Short

        ''' <summary>
        ''' Obtiene o establece el mensaje de error SMTP
        ''' </summary>
        <JsonProperty>
        Public Property MensajeErrorSMTP As String

    End Class

End Namespace