﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Gesphone

Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="RepositorioNovedadTransito"/>
    ''' </summary>
    Public NotInheritable Class RepositorioNovedadTransito
        Inherits RepositorioBase(Of NovedadTransito)

        Public Overrides Function Consultar(filtro As NovedadTransito) As IEnumerable(Of NovedadTransito)
            Dim lista As New List(Of NovedadTransito)
            Dim item As NovedadTransito

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Estado >= Cero Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_Novedades_Transito]")

                While resultado.Read
                    item = New NovedadTransito(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As NovedadTransito) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_numero", entidad.NumeroPlanilla)
                conexion.AgregarParametroSQL("@par_ENMC_numero", entidad.NumeroManifiesto)
                conexion.AgregarParametroSQL("@par_CATA_NOSV_Codigo", entidad.Novedad.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_SRSV_Codigo", entidad.sitio.Codigo)
                conexion.AgregarParametroSQL("@par_Ubicacion", entidad.Ubicacion)
                If Not IsNothing(entidad.Longitud) Then
                    conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Float)
                End If
                If Not IsNothing(entidad.Latitud) Then
                    conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Float)
                End If
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                conexion.AgregarParametroSQL("@par_Aplica_Seguimiento", entidad.AplicaSeguimiento)
                conexion.AgregarParametroSQL("@par_Usuario_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_novedad_transito_seguimiento_vehicular")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Numero").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As NovedadTransito) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As NovedadTransito) As NovedadTransito
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As NovedadTransito) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace