﻿PRINT 'gsp_modificar_encabezado_liquidacion_planilla_despachos'
GO

DROP PROCEDURE gsp_modificar_encabezado_liquidacion_planilla_despachos
GO

CREATE PROCEDURE gsp_modificar_encabezado_liquidacion_planilla_despachos
(
	@par_EMPR_Codigo NUMERIC,
	@par_Numero NUMERIC,
	@par_Fecha DATE,
	@par_Observaciones VARCHAR(500),
	@par_Valor_Flete_Transportador MONEY,
	@par_Valor_Conceptos_Liquidacion MONEY,
	@par_Valor_Base_Impuestos MONEY,
	@par_Valor_Impuestos MONEY,
	@par_Valor_Pagar MONEY,
	@par_Estado SMALLINT,
	@par_USUA_Codigo_Modifica SMALLINT,
	@par_OFIC_Codigo SMALLINT
)
AS
BEGIN
	UPDATE
		Encabezado_Liquidacion_Planilla_Despachos
	SET
		Fecha						=	@par_Fecha,
		Observaciones				=	@par_Observaciones,
		Valor_Flete_Transportador	=	@par_Valor_Flete_Transportador,
		Valor_Conceptos_Liquidacion	=	@par_Valor_Conceptos_Liquidacion,
		Valor_Base_Impuestos		=	@par_Valor_Base_Impuestos,
		Valor_Impuestos				=	@par_Valor_Impuestos,
		Valor_Pagar					=	@par_Valor_Pagar,
		Estado						=	@par_Estado,
		Fecha_Modifica				=	GETDATE(),
		USUA_Codigo_Modifica		=	@par_USUA_Codigo_Modifica,
		OFIC_Codigo					=	@par_OFIC_Codigo
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Numero = @par_Numero


	SELECT
		Numero,
		Numero_Documento
	FROM
		Encabezado_Liquidacion_Planilla_Despachos
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Numero = @par_Numero
END
GO