﻿PRINT 'gsp_consultar_listas_referencias'
GO
DROP PROCEDURE gsp_consultar_listas_referencias
GO
CREATE PROCEDURE gsp_consultar_listas_referencias    
(@par_EMPR_Codigo SMALLINT,    
@par_Numero_Estudio NUMERIC)    
    
AS  
BEGIN  
SELECT      
DRES.EMPR_Codigo    
,DRES.ENES_Numero    
,DRES.CATA_TRES_Codigo    
,DRES.Observaciones    
,VACA.Campo1 AS Nombre_Tipo_Referencia    
    
FROM Detalle_Referencias_Estudio_Seguridad DRES     
    
LEFT JOIN Valor_Catalogos VACA ON    
VACA.EMPR_Codigo = DRES.EMPR_Codigo    
AND VACA.Codigo = DRES.CATA_TRES_Codigo    
    
WHERE     
DRES.EMPR_Codigo = @par_EMPR_Codigo    
AND DRES.ENES_Numero = @par_Numero_Estudio    
    
END    
GO