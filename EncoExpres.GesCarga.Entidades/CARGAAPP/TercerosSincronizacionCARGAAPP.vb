﻿Imports Newtonsoft.Json

<JsonObject>
Public Class TercerosSincronizacionCARGAAPP


    Sub New()
    End Sub

    Public Function Read(Lector As IDataReader, strCampo As String) As Object
        Dim Tabla As DataTable = Lector.GetSchemaTable
        Dim ExisteCampo As Boolean = False
        For Each Campo As DataRow In Tabla.Rows
            If Campo("ColumnName").ToString() = strCampo Then
                ExisteCampo = True
                Exit For
            End If
        Next
        If ExisteCampo Then
            If Not IsDBNull(Lector.Item(strCampo)) Then
                If IsDate(Lector.Item(strCampo)) Then
                    If Lector.Item(strCampo) > Date.MinValue Then
                        Return Lector.Item(strCampo)
                    Else
                        Return Nothing
                    End If
                Else
                    Return Lector.Item(strCampo)
                End If
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Sub New(lector As IDataReader)
        identificacion = Read(lector, "identificacion")
        nombres = Read(lector, "nombres")
        apellidos = Read(lector, "apellidos")
        esConductor = IIf(Read(lector, "esConductor") > 0, True, False)
        esPropietario = IIf(Read(lector, "esPropietario") > 0, True, False)
        esAdministrador = IIf(Read(lector, "esAdministrador") > 0, True, False)
        esTenedor = IIf(Read(lector, "esTenedor") > 0, True, False)
    End Sub
    <JsonProperty>
    Public Property identificacion As String
    <JsonProperty>
    Public Property nombres As String
    <JsonProperty>
    Public Property apellidos As String
    <JsonProperty>
    Public Property esConductor As Boolean

    <JsonProperty>
    Public Property esPropietario As Boolean

    <JsonProperty>
    Public Property esAdministrador As Boolean

    <JsonProperty>
    Public Property esTenedor As Boolean
End Class
