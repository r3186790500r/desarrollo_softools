﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Repositorio.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class PersistenciaGestionDocumentalDocumentos
        Implements IPersistenciaBase(Of GestionDocumentalDocumentos)

        Public Function Consultar(filtro As GestionDocumentalDocumentos) As IEnumerable(Of GestionDocumentalDocumentos) Implements IPersistenciaBase(Of GestionDocumentalDocumentos).Consultar
            Return New RepositorioGestionDocumentalDocumentos().Consultar(filtro)
        End Function
        Public Function Insertar(entidad As GestionDocumentalDocumentos) As Long Implements IPersistenciaBase(Of GestionDocumentalDocumentos).Insertar
            Return New RepositorioGestionDocumentalDocumentos().Insertar(entidad)
        End Function
        Public Function Modificar(entidad As GestionDocumentalDocumentos) As Long Implements IPersistenciaBase(Of GestionDocumentalDocumentos).Modificar
            Return New RepositorioGestionDocumentalDocumentos().Modificar(entidad)
        End Function
        Public Function Obtener(filtro As GestionDocumentalDocumentos) As GestionDocumentalDocumentos Implements IPersistenciaBase(Of GestionDocumentalDocumentos).Obtener
            Return New RepositorioGestionDocumentalDocumentos().Obtener(filtro)
        End Function
        Public Function InsertarTemporal(entidad As GestionDocumentalDocumentos) As Long
            Return New RepositorioGestionDocumentalDocumentos().InsertarTemporal(entidad)
        End Function
        Public Function EliminarDocumento(entidad As GestionDocumentalDocumentos) As Boolean
            Return New RepositorioGestionDocumentalDocumentos().EliminarDocumento(entidad)
        End Function
        Public Function EliminarDocumentoDefinitivo(entidad As GestionDocumentalDocumentos) As Boolean
            Return New RepositorioGestionDocumentalDocumentos().EliminarDocumentoDefinitivo(entidad)
        End Function
        Public Function LimpiarDocumentoTemporalUsuario(entidad As GestionDocumentalDocumentos) As Boolean
            Return New RepositorioGestionDocumentalDocumentos().LimpiarDocumentoTemporalUsuario(entidad)
        End Function

    End Class
End Namespace
