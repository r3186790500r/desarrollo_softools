﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace ServicioCliente

    ''' <summary>
    ''' Clase <see cref=" DetallePolizasSolicitudOrdenServicios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetallePolizasSolicitudOrdenServicios
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePolizasSolicitudOrdenServicios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePolizasSolicitudOrdenServicios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroDocumento = Read(lector, "ESOS_Numero")
            Codigo = Read(lector, "ID")
            TipoPolizaOrdenServicio = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TPOS_Codigo"), .Nombre = Read(lector, "TipoPoliza")}
            Poliza = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Poliza"), .NombreCompleto = Read(lector, "Poliza")}
            ExigeEscolta = Read(lector, "Exige_Escolta")
            EdadVehiculos = Read(lector, "Edad_Vehiculos")
            ValorCobertura = Read(lector, "Valor_Cobertura")
            ValorDeducible = Read(lector, "Valor_Deducible")
            HorarioAutorizado = New ValorCatalogos With {.Codigo = Read(lector, "CATA_HOAP_Codigo"), .Nombre = Read(lector, "HorarioPoliza")}
            Observaciones = Read(lector, "Observaciones")


        End Sub
        <JsonProperty>
        Public Property TipoPolizaOrdenServicio As ValorCatalogos
        <JsonProperty>
        Public Property Poliza As Terceros
        <JsonProperty>
        Public Property ExigeEscolta As Integer
        <JsonProperty>
        Public Property EdadVehiculos As Integer
        <JsonProperty>
        Public Property ValorCobertura As Double
        <JsonProperty>
        Public Property ValorDeducible As Double
        <JsonProperty>
        Public Property HorarioAutorizado As ValorCatalogos

    End Class
End Namespace
