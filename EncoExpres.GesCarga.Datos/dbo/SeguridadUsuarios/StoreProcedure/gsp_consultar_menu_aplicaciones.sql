﻿PRINT 'gsp_consultar_menu_aplicaciones'
GO
DROP PROCEDURE gsp_consultar_menu_aplicaciones
GO
CREATE PROCEDURE gsp_consultar_menu_aplicaciones
(
@par_EMPR_Codigo SMALLINT,
@par_USUA_Codigo INT 
)
AS
BEGIN
SELECT 
MOAP.EMPR_Codigo,
MEAP.Codigo AS CodigoMenu,
MEAP.MOAP_Codigo,
MEAP.Nombre AS NombreMenu,
MEAP.Url_Pagina,
MEAP.Mostrar_Menu,
MEAP.Padre_Menu,
MEAP.Aplica_Habilitar,
MEAP.Aplica_Consultar,
MEAP.Aplica_Actualizar,
MEAP.Aplica_Eliminar_Anular,
MEAP.Aplica_Imprimir,
MEAP.Aplica_Contabilidad,
MEAP.Opcion_Permiso AS OpcionPermiso,
MEAP.Opcion_Listado AS OpcionListado,
MOAP.Estado AS EstadoModulo, 
MOAP.Nombre AS NombreModulo,
MOAP.CATA_APLI_Codigo,
CASE WHEN SUM(PGUS.Consultar) >= 1 THEN 1 ELSE 0 END AS Consultar, 
CASE WHEN SUM(PGUS.Actualizar) >= 1 THEN 1 ELSE 0 END AS Actualizar, 
CASE WHEN SUM(PGUS.Eliminar_Anular) >= 1 THEN 1 ELSE 0 END AS Eliminar_Anular, 
CASE WHEN SUM(PGUS.Imprimir) >= 1 THEN 1 ELSE 0 END AS Imprimir, 
CASE WHEN SUM(PGUS.Habilitar) >= 1 THEN 1 ELSE 0 END AS Habilitar,
CASE WHEN SUM(PGUS.Contabilidad) >= 1 THEN 1 ELSE 0 END AS Contabilidad


FROM  Menu_Aplicaciones AS MEAP,
Modulo_Aplicaciones AS MOAP,
Usuario_Grupo_Usuarios AS USGU,
Permiso_Grupo_Usuarios AS PGUS

WHERE 
MEAP.EMPR_Codigo =1
AND USGU.USUA_Codigo = 0
AND MEAP.Aplica_Habilitar = 1
AND MOAP.Estado = 1

AND MEAP.EMPR_Codigo = MOAP.EMPR_Codigo
AND MEAP.MOAP_Codigo = MOAP.Codigo 

AND USGU.EMPR_Codigo = PGUS.EMPR_Codigo
AND USGU.GRUS_Codigo = PGUS.GRUS_Codigo

AND PGUS.EMPR_Codigo =MEAP.EMPR_Codigo 
AND PGUS.MEAP_Codigo = MEAP.Codigo 

GROUP BY MOAP.EMPR_Codigo,
MEAP.Codigo,
MEAP.MOAP_Codigo,
MEAP.Nombre,
MEAP.Url_Pagina,
MEAP.Mostrar_Menu,
MEAP.Padre_Menu,
MEAP.Aplica_Habilitar,
MEAP.Aplica_Consultar,
MEAP.Aplica_Actualizar,
MEAP.Aplica_Eliminar_Anular,
MEAP.Aplica_Imprimir,
MEAP.Aplica_Contabilidad,
MEAP.Opcion_Permiso,
MEAP.Opcion_Listado,
MEAP.Orden_Menu,
MOAP.Estado, 
MOAP.CATA_APLI_Codigo,
MOAP.Nombre

ORDER BY MEAP.Orden_Menu
END
GO