﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa

Namespace Gesphone

    <JsonObject>
    Public Class SyncFotografiaRemesa

        ''' <summary>
        ''' Obtiene o establece el codigo de la empresa.
        ''' </summary>
        ''' <returns></returns>
        Public Property CodigoEmpresa As Short

        ''' <summary>
        ''' Obtiene o establece el numero de la remesa.
        ''' </summary>
        ''' <returns></returns>
        Public Property NumeroRemesa As Long

        ''' <summary>
        ''' Obtiene o establece si la imagen corresponde a la firma.
        ''' </summary>
        ''' <returns></returns>
        Public Property Esfirma As Short

        ''' <summary>
        ''' Obtiene o establece la foto.
        ''' </summary>
        ''' <returns></returns>
        Public Property Foto As FotoDetalleDistribucionRemesas



    End Class

    ''' <summary>
    ''' Clase que tiene los filtros para buscar los tarifarios validos para sincronizar.
    ''' </summary>
    <JsonObject>
    Public Class FiltroSyncApp

        ''' <summary>
        ''' Obtiene o establece el codigo de la empresa.
        ''' </summary>
        ''' <returns></returns>
        Public Property CodigoEmpresa As Short

        ''' <summary>
        ''' Obtiene o establece el codigo de la oficina.
        ''' </summary>
        ''' <returns></returns>
        Public Property CodigoOficina As Short

        ''' <summary>
        ''' Obtiene o establece el numero del encabezado tarifario.
        ''' </summary>
        ''' <returns></returns>
        Public Property NumeroTarifario As Integer

        ''' <summary>
        ''' Obtiene o establece si debe sincronizar todo el tarifario o solo las últimas modificaciones.
        ''' </summary>
        ''' <returns></returns>
        Public Property SincronizarTodo As Short

        ''' <summary>
        ''' Obtiene o establece la fecha de la ultima sincronización.
        ''' </summary>
        ''' <returns></returns>
        Public Property FechaUltimaSincronizacion As DateTime?

        ''' <summary>
        ''' Obtiene o establece el código del conductor.
        ''' </summary>
        ''' <returns></returns>
        Public Property CodigoConductor As Integer

        ''' <summary>
        ''' Obtiene o establece el código del tercero cliente.
        ''' </summary>
        ''' <returns></returns>
        Public Property CodigoCliente As Integer

        ''' <summary>
        ''' Obtiene o establece el código de ciudad origen.
        ''' </summary>
        ''' <returns></returns>
        Public Property CodigoCiudadOrigen As Integer

        ''' <summary>
        ''' Obtiene o establece el código de ciudad destino.
        ''' </summary>
        ''' <returns></returns>
        Public Property CodigoCiudadDestino As Integer

        ''' <summary>
        ''' Obtiene o establece el código de linea de negocio.
        ''' </summary>
        ''' <returns></returns>
        Public Property CodigoLineaNegocio As Integer

    End Class

End Namespace
