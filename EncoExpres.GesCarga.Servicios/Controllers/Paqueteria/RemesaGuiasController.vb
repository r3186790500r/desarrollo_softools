﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Negocio.Paqueteria

''' <summary>
''' Controlador <see cref="RemesaPaqueteriaController"/>
''' </summary>
<Authorize>
Public Class RemesaPaqueteriaController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarGuiasEtiquetasPreimpresas")>
    Public Function ConsultarGuiasEtiquetasPreimpresas(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarGuiasEtiquetasPreimpresas(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarGuiasPlanillaEntrega")>
    Public Function ConsultarGuiasPlanillaEntrega(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarGuiasPlanillaEntrega(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("GuardarBitacoraGuias")>
    Public Function GuardarBitacoraGuias(ByVal entidad As DetalleEstadosRemesa) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).GuardarBitacoraGuias(entidad)
    End Function
    <HttpPost>
    <ActionName("InsertarIntentoEntrega")>
    Public Function InsertarIntentoEntrega(ByVal entidad As DetalleIntentoEntregaGuia) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).InsertarIntentoEntrega(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("CumplirGuias")>
    Public Function CumplirGuias(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).CumplirGuias(entidad)
    End Function
    <ActionName("GenerarPlanitilla")>
    Public Function GenerarPlanitilla(ByVal Filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).GenerarPlanitilla(Filtro)
    End Function
    <ActionName("GuardarEntrega")>
    Public Function GuardarEntrega(ByVal Entidad As Remesas) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).GuardarEntrega(Entidad)
    End Function
    <ActionName("ConsultarIndicadores")>
    Public Function ConsultarIndicadores(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarIndicadores(Entidad)
    End Function
    <HttpPost>
    <ActionName("AsociarGuiaPlanilla")>
    Public Function AsociarGuiaPlanilla(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).AsociarGuiaPlanilla(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarEstadosRemesa")>
    Public Function ConsultarEstadosRemesa(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarEstadosRemesa(Entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarBitacoraGuias")>
    Public Function ConsultarBitacoraGuias(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarBitacoraGuias(Entidad)
    End Function
    <ActionName("ObtenerControlEntregas")>
    Public Function ObtenerControlEntregas(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ObtenerControlEntregas(Entidad)
    End Function
    <ActionName("ConsultarRemesasPendientesControlEntregas")>
    Public Function ConsultarRemesasPendientesControlEntregas(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasPendientesControlEntregas(Entidad)
    End Function
    <ActionName("ConsultarRemesasRecogerOficina")>
    Public Function ConsultarRemesasRecogerOficina(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasRecogerOficina(Entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasPorPlanillar")>
    Public Function ConsultarRemesasPorPlanillar(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasPorPlanillar(filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasRetornoContado")>
    Public Function ConsultarRemesasRetornoContado(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasRetornoContado(filtro)
    End Function
    <HttpPost>
    <ActionName("CambiarEstadoGuia")>
    Public Function CambiarEstadoGuiaPaqueteria(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).CambiarEstadoGuiaPaqueteria(entidad)
    End Function
    <HttpPost>
    <ActionName("CambiarEstadoTrazabilidadGuia")>
    Public Function CambiarEstadoTrazabilidadGuia(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).CambiarEstadoTrazabilidadGuia(entidad)
    End Function
    <HttpPost>
    <ActionName("CambiarEstadoRemesasPaqueteria")>
    Public Function CambiarEstadoRemesasPaqueteria(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).CambiarEstadoRemesasPaqueteria(entidad)
    End Function

    <HttpPost>
    <ActionName("ValidarPreimpreso")>
    Public Function ValidarPreimpreso(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ValidarPreimpreso(entidad)
    End Function
    <HttpPost>
    <ActionName("RecibirRemesaPaqueteriaOficinaDestino")>
    Public Function RecibirRemesaPaqueteriaOficinaDestino(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).RecibirRemesaPaqueteriaOficinaDestino(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasPendientesRecibirOficina")>
    Public Function ConsultarRemesasPendientesRecibirOficina(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasPendientesRecibirOficina(filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasPorLegalizar")>
    Public Function ConsultarRemesasPorLegalizar(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasPorLegalizar(filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasPorLegalizarRecaudo")>
    Public Function ConsultarRemesasPorLegalizarRecaudo(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasPorLegalizarRecaudo(filtro)
    End Function

    <HttpPost>
    <ActionName("ActualizarSecuencia")>
    Public Function ActualizarSecuencia(ByVal filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ActualizarSecuencia(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarDetalleUnidadesDescargue")>
    Public Function ConsultarDetalleUnidadesDescargue(ByVal entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleUnidades))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarDetalleUnidadesDescargue(entidad)
    End Function

    <HttpPost>
    <ActionName("ValidarPesoVolumetrico")>
    Public Function ValidarPesoVolumetrico(ByVal entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ValidarPesoVolumetrico(entidad)
    End Function

    <HttpPost>
    <ActionName("CalcularValores")>
    Public Function CalcularValores(ByVal entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).CalcularValores(entidad)
    End Function

    <HttpPost>
    <ActionName("InsertarEstadosRemesasDescargas")>
    Public Function InsertarEstadosRemesasDescargas(ByVal entidad As RemesaPaqueteria) As Boolean
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).InsertarEstadosRemesasDescargas(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarFoto")>
    Public Function ConsultarFoto(ByVal filtro As FotoDetalleDistribucionRemesas) As Respuesta(Of IEnumerable(Of FotoDetalleDistribucionRemesas))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarFoto(filtro)
    End Function
End Class
