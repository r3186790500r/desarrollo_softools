﻿PRINT 'gsp_modificar_encabezado_formularios_inspecciones'
GO
DROP PROCEDURE gsp_modificar_encabezado_formularios_inspecciones
GO
CREATE PROCEDURE gsp_modificar_encabezado_formularios_inspecciones 
(  
@par_EMPR_Codigo  SMALLINT,  
@par_Codigo NUMERIC,  
@par_Codigo_Alterno VARCHAR(20),  
@par_Nombre VARCHAR(50),  
@par_Estado SMALLINT,  
@par_Version VARCHAR(50),  
@par_TIDO_Codigo_Origen NUMERIC,  
@par_USUA_Codigo_Modifica SMALLINT
)  
AS  
  
BEGIN  
  
UPDATE Encabezado_Formularios_Inspecciones  
SET  
Codigo_Alterno = @par_Codigo_Alterno,  
Nombre = @par_Nombre,  
Estado = @par_Estado,  
Version = @par_Version,  
TIDO_Codigo_Origen = @par_TIDO_Codigo_Origen,  
Fecha_Modifica = GETDATE(),  
USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica
WHERE  
EMPR_Codigo =  @par_EMPR_Codigo  
AND Codigo = @par_Codigo  
  
SELECT @par_Codigo AS Codigo  
  
END  
GO