﻿EncoExpresApp.controller("ConsultasSeguimientoPortalCtrl", ['$scope', 'RutasFactory', '$linq', 'DetalleSeguimientoVehiculosFactory', 'blockUIConfig', 'ValorCatalogosFactory', 'TercerosFactory', 'EventoCorreosFactory', 'DetalleDistribucionRemesasFactory', 'ProductoTransportadosFactory', 'RemesasFactory', 'VehiculosFactory', 'ReporteMinisterioFactory', 'OficinasFactory', 'CiudadesFactory', 'PuestoControlesFactory', 'PlanillaDespachosFactory', 'blockUI', '$timeout',
    function ($scope, RutasFactory, $linq, DetalleSeguimientoVehiculosFactory, blockUIConfig, ValorCatalogosFactory, TercerosFactory, EventoCorreosFactory, DetalleDistribucionRemesasFactory, ProductoTransportadosFactory, RemesasFactory, VehiculosFactory, ReporteMinisterioFactory, OficinasFactory, CiudadesFactory, PuestoControlesFactory, PlanillaDespachosFactory, blockUI, $timeout) {

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        // Se crea la propiedad modelo en el ambito
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.VerOpcionPDF = false;
        $('#TabPlanillas').show()
        $('#TabPlanillas2').show()

        //Obtiene la URL para llamar el proyecto ASP
        $scope.MensajesError = [];
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DeshabilitarDetalle = false;
        $scope.DeshabilitarPuestoControl = true;
        $scope.DeshabilitarFlotaPropia = false;
        var filtros = {};
        $scope.ListadoOficinas = [];
        $scope.cantidadRegistrosPorPaginas = 20;
        $scope.paginaActual = 1;
        $scope.paginaActualOrden = 1;
        $scope.paginaActualRemesa = 1;
        $scope.paginaActualFlotaPropia = 1;
        $scope.paginaActualRNDC = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.ListaPuntosControl = [];
        $scope.ListaSitioReporteModal = [];
        $scope.CodigoSitioReporteSeguimiento = 0;
        $scope.ListaNovedadSeguimiento = [];
        $scope.CodigoNovedadSeguimiento = 0;
        $scope.ListadoSeguimientosPlanilla = [];
        $scope.ListadoSeguimientosOrden = [];
        $scope.MensajesErrorSeguimiento = [];
        $scope.ListaCantidadSeguimientos = [];
        $scope.ListaTipoOrigenSeguimiento = [];
        $scope.ConsultaProgramacion = {};
        $scope.MensajesErrorAnular = [];
        $scope.CorreosTransportador = [];
        $scope.CorreosClientes = [];
        $scope.TipoConsulta = 1;
        $scope.ValorBanderaAnularRemesa = 0;
        $scope.ListaPuntosGestionModal = [];
        $scope.ListaProductoTransportados = [];
        $scope.ListadoSeguimientosRemesasPlanillaDespachos = [];
        $scope.Correo = {};
        $scope.Modelo = {
            Orden: 0
        };
        $scope.Remesas = {
            RegistrosPagina: 20,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Cliente : ''
        };
        $scope.Distribucion = {};
        $scope.Despachos = {
            Estado: false
        };
        $scope.Ordenes = {
            Estado: false
        };
        $scope.FlotaPropia = {
            Estado: false
        };
        $scope.RNDC = {};

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            try {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONSULTAS_SEGUIMIENTO_VEHICULAR);
                $scope.MapaSitio = [{ Nombre: 'Portal' }, { Nombre: 'Control Trafico' }, { Nombre: 'Consultas' }, { Nombre: 'Seguimiento Vehicular Histórico' }];
                $('#ConsultaOrdenesCargue').hide(); $('#ConsultaPlanillasDespachos').show(); $('#ConsultaRemesasPlanillasDespachos').hide(); $('#ConsultaFlotaPropia').hide(); $('#ConsultaRNDC').hide();
            } catch (e) {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONSULTAS_SEGUIMIENTO_VEHICULAR_PORTAL);
                $scope.MapaSitio = [{ Nombre: 'Portal' }, { Nombre: 'Control Trafico' }, { Nombre: 'Consultas' }, { Nombre: 'Seguimiento Vehicular Histórico' }];
                $('#ConsultaOrdenesCargue').hide(); $('#ConsultaPlanillasDespachos').show(); $('#ConsultaRemesasPlanillasDespachos').hide(); $('#ConsultaFlotaPropia').hide(); $('#ConsultaRNDC').hide();
            }
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR PLANILLA--------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR ORDEN--------------------------------------------------------------------------------- */
        $scope.PrimerPaginaOrden = function () {
            $scope.paginaActualOrden = 1;
            Find();
        };

        $scope.SiguienteOrden = function () {
            if ($scope.paginaActualOrden < $scope.totalPaginasOrden) {
                $scope.paginaActualOrden = $scope.paginaActualOrden + 1;
                Find();
            }
        };

        $scope.AnteriorOrden = function () {
            if ($scope.paginaActualOrden > 1) {
                $scope.paginaActualOrden = $scope.paginaActualOrden - 1;
                Find();
            }
        };

        $scope.UltimaPaginaOrden = function () {
            $scope.paginaActualOrden = $scope.totalPaginasOrden;
            Find();
        };

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN REMESAS--------------------------------------------------------------------------------- */
        $scope.PrimerPaginaRemesa = function () {
            $scope.paginaActualRemesa = 1;
            FindRemesa();
        };

        $scope.SiguienteRemesa = function () {
            if ($scope.paginaActualRemesa < $scope.totalPaginasRemesa) {
                $scope.paginaActualRemesa = $scope.paginaActualRemesa + 1;
                FindRemesa();
            }
        };

        $scope.AnteriorRemesa = function () {
            if ($scope.paginaActualRemesa > 1) {
                $scope.paginaActualRemesa = $scope.paginaActualRemesa - 1;
                FindRemesa();
            }
        };

        $scope.UltimaPaginaRemesa = function () {
            $scope.paginaActualRemesa = $scope.totalPaginasRemesa;
            FindRemesa();
        };
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN FLOTA PROPIA--------------------------------------------------------------------------------- */
        $scope.PrimerPaginaFlotaPropia = function () {
            $scope.paginaActualFlotaPropia = 1;
            FindFlotaPropia();
        };

        $scope.SiguienteFlotaPropia = function () {
            if ($scope.paginaActualFlotaPropia < $scope.totalPaginasFlotaPropia) {
                $scope.paginaActualFlotaPropia = $scope.paginaActualFlotaPropia + 1;
                FindFlotaPropia();
            }
        };

        $scope.AnteriorFlotaPropia = function () {
            if ($scope.paginaActualFlotaPropia > 1) {
                $scope.paginaActualFlotaPropia = $scope.paginaActualFlotaPropia - 1;
                FindFlotaPropia();
            }
        };

        $scope.UltimaPaginaFlotaPropia = function () {
            $scope.paginaActualFlotaPropia = $scope.totalPaginasFlotaPropia;
            FindFlotaPropia();
        };
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN RNDC--------------------------------------------------------------------------------- */
        $scope.PrimerPaginaRNDC = function () {
            $scope.paginaActualRNDC = 1;
            FindRNDC();
        };

        $scope.SiguienteRNDC = function () {
            if ($scope.paginaActualRNDC < $scope.totalPaginasRNDC) {
                $scope.paginaActualRNDC = $scope.paginaActualRNDC + 1;
                FindRNDC();
            }
        };

        $scope.AnteriorRNDC = function () {
            if ($scope.paginaActualRNDC > 1) {
                $scope.paginaActualRNDC = $scope.paginaActualRNDC - 1;
                FindRNDC();
            }
        };

        $scope.UltimaPaginaRNDC = function () {
            $scope.paginaActualRNDC = $scope.totalPaginasRNDC;
            FindRNDC();
        };

        /*-----------------------------------------------------------------COMBOS Y AUTOCOMPLETE--------------------------------------------------------------------------------- */
        ///*Verifica oficinas */
        $scope.AsignarOficina = function (ofic) {
            if (ofic.Codigo === -1) {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo === -1) {
                        item.Estado = true;
                    } else {
                        item.Estado = false;
                    }
                });
            } else {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo === -1) {
                        item.Estado = false;
                    }
                });
            }
        };

        /*Verifica rAutocomplete Puesto Control */
        $scope.VerificarAutocompletePuestoControl = function (puestocontrol) {
            if (puestocontrol.Codigo === undefined) {
                $scope.Modelo.PuestoControl = '';
            }
        };

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 });
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo === $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo) {
                            item.Estado = true;
                        } else {
                            item.Estado = false;
                        }
                        $scope.ListadoOficinas.push(item);
                    });
                    /*Cargar el combo  cantidad seguimientos */
                    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CANTIDAD_LISTA_SEGUIMIENTOS } }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.ListaCantidadSeguimientos = response.data.Datos;
                                $scope.Despachos.Mostrar = $scope.ListaCantidadSeguimientos[1];
                                $scope.Ordenes.Mostrar = $scope.ListaCantidadSeguimientos[1];
                                $scope.Remesas.Mostrar = $scope.ListaCantidadSeguimientos[1];
                                $scope.FlotaPropia.Mostrar = $scope.ListaCantidadSeguimientos[1];

                                if ($scope.Sesion.UsuarioAutenticado.Cliente.Codigo > 0 && $scope.Sesion.UsuarioAutenticado.Cliente.Codigo != undefined && $scope.Sesion.UsuarioAutenticado.Cliente.Codigo != '') {
                                    $scope.Despachos.Cliente = $scope.CargarTercero($scope.Sesion.UsuarioAutenticado.Cliente.Codigo);
                                    $scope.Remesas.Cliente = $scope.CargarTercero($scope.Sesion.UsuarioAutenticado.Cliente.Codigo);
                                    $scope.DeshabilitarCliente = true;
                                }
                                //Find();
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DUEÑO_VEHICULO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoDueno = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoDueno = response.data.Datos;
                        $scope.Despachos.TipoDueno = $scope.ListadoTipoDueno[0]
                        $scope.Ordenes.TipoDueno = $scope.ListadoTipoDueno[0]
                    }
                    else {
                        $scope.ListadoTipoDueno = []
                    }
                }
            }, function (response) {
            });
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de Conductor*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }
        // Vehiculo
        $scope.ListaPlaca = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 2) == 0 || value.length == 1) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaPlaca = ValidarListadoAutocomplete(Response.Datos, $scope.ListaPlaca);
                }
            }
            return $scope.ListaPlaca;
        };
        //-- Productos Transportados --//

        $scope.AutocompleteProductos = function (value) {
            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1, ValorAutocomplete: value }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ProductoTranportadoCarga = true;
                        if (response.data.Datos.length > 0) {
                            $scope.ListaProductoTransportado = response.data.Datos;
                        }
                        else {
                            $scope.ListaProductoTransportado = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            return $scope.ListaProductoTransportado
        }
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ProductoTranportadoCarga = true;
                    if (response.data.Datos.length > 0) {
                        $scope.ListaProductoTransportado = response.data.Datos;
                    }
                    else {
                        $scope.ListaProductoTransportado = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //Cargar datos vehiculo
        $scope.CargarDatosVehiculo = function (vehiculo) {
            if (vehiculo !== '' && vehiculo !== null && vehiculo !== undefined) {
                $scope.CodigoVehiculo = vehiculo.Codigo;
                VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.CodigoVehiculo }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ModalFlotaPropia = {};
                            $scope.ModalFlotaPropia = response.data.Datos;
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        $scope.ListadoCliente = []
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente)
                }
            }
            return $scope.ListadoCliente
        }
        $scope.ListadoCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }
        /*Cargar el combo Tipo Origen */
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ORIGEN_SEGUIMIENTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaTipoOrigenSeguimiento = [];
                    $scope.ListaTipoOrigenSeguimiento = response.data.Datos;
                    $scope.Despachos.OrigenReporte = $scope.ListaTipoOrigenSeguimiento[0];
                    $scope.Ordenes.OrigenReporte = $scope.ListaTipoOrigenSeguimiento[0];
                    $scope.Remesas.TipoOrigenSeguimiento = $scope.ListaTipoOrigenSeguimiento[0];
                    $scope.FlotaPropia.OrigenReporte = $scope.ListaTipoOrigenSeguimiento[0];
                    //Find();
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo sitio reporte estado*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SITIO_REPORTE_SEGUIMIENTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaSitioReporte = [];
                    $scope.ListaSitioReporteModal = [];
                    $scope.ListaSitioReporte = response.data.Datos;
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo !== 8201) {
                            $scope.ListaSitioReporteModal.push(item);
                        }
                    });
                    $scope.Despachos.SitioReporte = $scope.ListaSitioReporte[0];
                    $scope.Ordenes.SitioReporte = $scope.ListaSitioReporte[0];
                    $scope.Remesas.SitioReporteSeguimiento = $scope.ListaSitioReporte[0];
                    $scope.FlotaPropia.SitioReporte = $scope.ListaSitioReporte[0];
                    //Find();
                    if ($scope.CodigoSitioReporteSeguimiento !== undefined && $scope.CodigoSitioReporteSeguimiento !== '' && $scope.CodigoSitioReporteSeguimiento !== null && $scope.CodigoSitioReporteSeguimiento !== 0) {
                        $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo ==' + $scope.CodigoSitioReporteSeguimiento);
                    } else {
                        $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == 8200');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });



        //Carga autocomplete de rutas
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaRutas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaRutas = response.data.Datos;
                    }
                    else {
                        $scope.ListaRutas = [];
                    }
                }
            }, function (response) {
            });
        //-- Puestos Control --//
        PuestoControlesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaPuestosControl = response.data.Datos;
                    }
                    else {
                        $scope.ListaPuestosControl = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIEMPOS_LOGISTICOS_REMESA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTiemposLogisticos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTiemposLogisticos = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTiemposLogisticos = []
                    }
                }
            }, function (response) {
            });

        //-------------------------------------------------FUNCIONES BUSCAR FLOTA PROPIA--------------------------------------------------------
        $scope.BuscarFlotaPropia = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                FindFlotaPropia();
            }
        };

        function FindFlotaPropia() {
            $scope.MensajesError = [];
            $scope.ListadoSeguimientosFlotaPropia = [];

            var CodigosOficinas = '';
            $scope.ListadoOficinas.forEach(function (item) {
                if (item.Estado === true) {
                    CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                }
            });

            if (CodigosOficinas === '-1,') {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo !== -1) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });
            }

            var anuladoflotapropia = 0;
            if ($scope.FlotaPropia.Estado === true) {
                anuladoflotapropia = 1;
            } else {
                anuladoflotapropia = 0;
            }

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Pagina: $scope.paginaActualFlotaPropia,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                TipoConsulta: 7,
                Vehiculo: { Placa: $scope.FlotaPropia.Vehiculo },
                Conductor: { NombreCompleto: $scope.FlotaPropia.Conductor },
                Ruta: $scope.FlotaPropia.Ruta,
                PuestoControl: $scope.FlotaPropia.PuestoControl,
                SitioReporteSeguimiento: $scope.FlotaPropia.SitioReporte,
                TipoOrigenSeguimiento: $scope.FlotaPropia.OrigenReporte,
                Mostrar: $scope.FlotaPropia.Mostrar,
                Anulado: anuladoflotapropia,
                CodigosOficinas: CodigosOficinas,
                FlotaPropia: 1,
                Historico: 1,
                FechaInicial: $scope.FechaInicio,
                FechaFinal: $scope.FechaFin,
            };

            DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Observaciones !== '' && item.Observaciones !== undefined && item.Observaciones !== null) {
                                    var str = item.Observaciones;
                                    item.Observaciones = str.substring(0, 10);
                                    item.ObservacionCompleta = str.substring(0, 250);
                                    item.OcultarVerMas = true;
                                } else {
                                    item.OcultarVerMas = false;
                                }
                            });
                            $scope.ListadoSeguimientosFlotaPropia = response.data.Datos;
                            $scope.totalRegistrosFlotaPropia = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasFlotaPropia = Math.ceil($scope.totalRegistrosFlotaPropia / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoFlotaPropia = false;
                            $scope.ResultadoSinRegistrosFlotaPropia = '';
                        }
                        else {
                            $scope.totalRegistrosFlotaPropia = 0;
                            $scope.totalPaginasFlotaPropia = 0;
                            $scope.paginaActualFlotaPropia = 1;
                            $scope.ResultadoSinRegistrosFlotaPropia = 'No hay datos para mostrar';
                            $scope.BuscandoFlotaPropia = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        //-------------------------------------------------FUNCIONES BUSCAR REMESAS RNDC--------------------------------------------------------
        $scope.BuscarRNDC = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                FindRNDC();
            }
        };

        function FindRNDC() {
            $scope.MensajesError = [];
            $scope.ListadoRemesasRNDC = [];

            var CodigosOficinas = '';
            $scope.ListadoOficinas.forEach(function (item) {
                if (item.Estado === true) {
                    CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                }
            });

            if (CodigosOficinas === '-1,') {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo !== -1) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });
            }

            $scope.RNDC.CodigosOficinas = CodigosOficinas;
            $scope.RNDC.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.RNDC.Pagina = $scope.paginaActualRNDC;
            $scope.RNDC.RegistrosPagina = $scope.cantidadRegistrosPorPaginas;

            ReporteMinisterioFactory.ConsultarRemesasSinManifiesto($scope.RNDC).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoRemesasRNDC = [];
                            response.data.Datos.forEach(function (item) {
                                item.Remesa.Mensaje = item.Mensaje;
                                $scope.ListadoRemesasRNDC.push(item.Remesa);
                            });

                            $scope.totalRegistrosRNDC = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasRNDC = Math.ceil($scope.totalRegistrosRNDC / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoRNDC = false;
                            $scope.ResultadoSinRegistrosRNDC = '';
                        }
                        else {
                            $scope.totalRegistrosRNDC = 0;
                            $scope.totalPaginasRNDC = 0;
                            $scope.paginaActualRNDC = 1;
                            $scope.ResultadoSinRegistrosRNDC = 'No hay datos para mostrar';
                            $scope.BuscandoRNDC = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        //-------------------------------------------------FUNCIONES BUSCAR REMESAS--------------------------------------------------------
        function ValidacionesConsultaRemesa() {
            $scope.MensajesError = [];
            var consultaRemesa = true;
            if ($scope.Remesas.NumeroDocumentoRemesa == undefined || $scope.Remesas.NumeroDocumentoRemesa == null || $scope.Remesas.NumeroDocumentoRemesa == '') {
                if (($scope.FechaInicioRemesa == undefined || $scope.FechaInicioRemesa == null || $scope.FechaInicioRemesa == '') || ($scope.FechaFinRemesa == undefined || $scope.FechaFinRemesa == null || $scope.FechaFinRemesa == '')) {
                    $scope.MensajesError.push("debe ingresar un rango de fechas o No. Documento");
                    consultaRemesa = false;
                } else if ((($scope.FechaFinRemesa - $scope.FechaInicioRemesa) / (1000 * 60 * 60 * 24)) > 60) {
                    $scope.MensajesError.push('El rango de fechas no puede ser superior a 60 días');
                    consultaRemesa = false;
                } else {
                    consultaRemesa = true;
                }


            }
            return consultaRemesa;
        }

        $scope.BuscarRemesa = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                if (ValidacionesConsultaRemesa() == true) {

                    FindRemesa();

                }

            }
        };

        function FindRemesa() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {

                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.MensajesError = [];
            $scope.ListadoSeguimientosRemesa = [];

            if ($scope.RemesasEstado === true) {
                $scope.Remesas.Anulado = 1;
            } else {
                $scope.Remesas.Anulado = 0;
            }

            var CodigosOficinas = '';
            $scope.ListadoOficinas.forEach(function (item) {
                if (item.Estado === true) {
                    CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                }
            });

            if (CodigosOficinas === '-1,') {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo !== -1) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });
            }

            $scope.Remesas.CodigosOficinas = CodigosOficinas;
            $scope.Remesas.Pagina = $scope.paginaActualRemesa;
            $scope.Remesas.FechaInicial = $scope.FechaInicioRemesa;
            $scope.Remesas.FechaFinal = $scope.FechaFinRemesa;

            DetalleSeguimientoVehiculosFactory.ConsultarMasterRemesa($scope.Remesas).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoSeguimientosRemesa = response.data.Datos;
                            $scope.totalRegistrosRemesa = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasRemesa = Math.ceil($scope.totalRegistrosRemesa / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoRemesa = false;
                            $scope.ResultadoSinRegistrosRemesa = '';
                        }
                        else {
                            $scope.totalRegistrosRemesa = 0;
                            $scope.totalPaginasRemesa = 0;
                            $scope.paginaActualRemesa = 1;
                            $scope.ResultadoSinRegistrosRemesa = 'No hay datos para mostrar';
                            $scope.BuscandoRemesa = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        }

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.CargarNovedadesSitioReporte = function (sitioReporte) {
            $scope.ListaNovedadSeguimiento = [];
            $scope.Modelo.NovedadSeguimiento = '';
            if (sitioReporte.Codigo !== undefined && sitioReporte.Codigo !== 8200) {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    SitioReporteSeguimiento: sitioReporte
                };
                DetalleSeguimientoVehiculosFactory.ConsultarNovedadesSitiosReporte(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaNovedadSeguimiento = response.data.Datos;
                                $scope.DeshabilitarNovedadesSeguimiento = false;
                            } else {
                                $scope.DeshabilitarNovedadesSeguimiento = true;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                $scope.DeshabilitarNovedadesSeguimiento = true;
            }
        };

        $scope.CargarPuestoControlRutas = function (ruta) {
            $scope.ListaPuntosControl = [];
            $scope.Modelo.PuestoControl = '';
            if (ruta !== undefined && ruta !== null && ruta !== '') {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Ruta: ruta
                };
                DetalleSeguimientoVehiculosFactory.ConsultarPuestoControlRutas(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaPuntosControl = response.data.Datos;
                                if ($scope.CodigoPuestoControl !== undefined && $scope.CodigoPuestoControl !== '' && $scope.CodigoPuestoControl !== null && $scope.CodigoPuestoControl !== 0) {
                                    if (!$scope.DeshabilitarDetalle) {
                                        $scope.Modelo.PuestoControl = $linq.Enumerable().From($scope.ListaPuntosControl).First('$.PuestoControl.Codigo ==' + $scope.CodigoPuestoControl);
                                    } else {
                                        $scope.Modelo.PuestoControl = '';
                                    }
                                } else {
                                    $scope.Modelo.PuestoControl = '';
                                }
                                $scope.DeshabilitarPuestoControl = false;
                            } else {
                                if (!$scope.DeshabilitarDetalle) {
                                    $scope.DeshabilitarPuestoControl = true;
                                    ShowError('La ruta no tiene puntos de control asociados');
                                }
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                $scope.DeshabilitarPuestoControl = true;
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        function ValidacionesConsulta() {
            $scope.MensajesError = [];
            var Consultar = true;
            //  if ($scope.Despachos.Vehiculo == null || $scope.Despachos.Vehiculo == undefined || $scope.Despachos.Vehiculo == '') {
            if ($scope.Despachos.NumeroManifiesto == null || $scope.Despachos.NumeroManifiesto == undefined || $scope.Despachos.NumeroManifiesto == '' || isNaN($scope.Despachos.NumeroManifiesto)) {
                if ($scope.Despachos.NumeroPlanilla == null || $scope.Despachos.NumeroPlanilla == undefined || $scope.Despachos.NumeroPlanilla == '' || isNaN($scope.Despachos.NumeroPlanilla)) {
                    if ($scope.FechaInicio == null || $scope.FechaInicio == undefined || $scope.FechaInicio == '') {
                        $scope.MensajesError.push("Debe Ingresar un rango de fechas o No. Documento");
                        Consultar = false

                    } else if ($scope.FechaFin == null || $scope.FechaFin == undefined || $scope.FechaFin == '') {
                        $scope.MensajesError.push("Debe Ingresar un rango de fechas o No. Documento");
                        Consultar = false

                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > 60) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a 60 días');
                        Consultar = false
                    } else {
                        Consultar = true
                    }

                }
            }
            //  }
            return Consultar;
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                if (ValidacionesConsulta()) {
                    Find();
                }

            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                //NumeroDocumento
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.ListadoSeguimientosPlanilla = [];
            $scope.ListadoSeguimientosOrden = [];
            $scope.MensajesError = [];
            if ($scope.MensajesError.length == 0) {
                filtros = {};

                var anuladodespachos = 0;
                if ($scope.Despachos.Estado == true) {
                    anuladodespachos = 1;
                } else {
                    anuladodespachos = 0;
                }
                var anuladoorden = 0;
                if ($scope.Ordenes.Estado == true) {
                    anuladoorden = 1;
                } else {
                    anuladoorden = 0;
                }

                var CodigosOficinas = '';
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Estado === true) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });

                if (CodigosOficinas === '-1,') {
                    $scope.ListadoOficinas.forEach(function (item) {
                        if (item.Codigo !== -1) {
                            CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                        }
                    });
                }

                if ($scope.TipoConsulta == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Pagina: $scope.paginaActualOrden,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        TipoConsulta: $scope.TipoConsulta,
                        Vehiculo: { Placa: $scope.Ordenes.Vehiculo },
                        Conductor: $scope.Ordenes.Conductor,
                        Cliente: $scope.Ordenes.Cliente,
                        TipoDueno: $scope.Ordenes.TipoDueno,
                        Producto: $scope.Ordenes.Producto,
                        Origen: $scope.Ordenes.Origen,
                        Destino: $scope.Ordenes.Destino,
                        Ruta: $scope.Ordenes.Ruta,
                        PuestoControl: $scope.Ordenes.PuestoControl,
                        NumeroDocumentoOrden: $scope.Ordenes.NumeroOrden,
                        NumeroManifiesto: $scope.Ordenes.NumeroManifiesto,
                        SitioReporteSeguimiento: $scope.Ordenes.SitioReporte,
                        TipoOrigenSeguimiento: $scope.Ordenes.OrigenReporte,
                        Mostrar: $scope.Ordenes.Mostrar,
                        Anulado: anuladoorden,
                        Historico: 1,
                        FechaInicial: $scope.FechaInicio,
                        FechaFinal: $scope.FechaFin,
                        CodigosOficinas: CodigosOficinas
                    };
                }
                else if ($scope.TipoConsulta == 1) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        TipoConsulta: $scope.TipoConsulta,
                        Vehiculo: { Placa: $scope.Despachos.Vehiculo },
                        Conductor: $scope.Despachos.Conductor,
                        Cliente: $scope.Despachos.Cliente,
                        TipoDueno: $scope.Despachos.TipoDueno,
                        Producto: $scope.Despachos.Producto,
                        Origen: $scope.Despachos.Origen,
                        Destino: $scope.Despachos.Destino,
                        Ruta: $scope.Despachos.Ruta,
                        PuestoControl: $scope.Despachos.PuestoControl,
                        NumeroDocumentoPlanilla: $scope.Despachos.NumeroPlanilla,
                        NumeroManifiesto: $scope.Despachos.NumeroManifiesto,
                        SitioReporteSeguimiento: $scope.Despachos.SitioReporte,
                        TipoOrigenSeguimiento: $scope.Despachos.OrigenReporte,
                        Mostrar: $scope.Despachos.Mostrar,
                        Anulado: anuladodespachos,
                        CodigosOficinas: CodigosOficinas,
                        Historico: 1,
                        FechaInicial: $scope.FechaInicio,
                        FechaFinal: $scope.FechaFin,
                        Listado: 1
                    };
                }

                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoSeguimientosPlanilla = [];
                                $scope.ListadoSeguimientosOrden = [];

                                if ($scope.TipoConsulta == 0) {
                                    var concidencias = 0
                                    if (filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                        concidencias++
                                    }
                                    if (filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                        concidencias++
                                    }
                                    if (filtros.PuestoControl !== undefined) {
                                        if (filtros.PuestoControl.Codigo !== undefined) {
                                            concidencias++
                                        }
                                    }
                                    if (filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                        concidencias++
                                    }
                                    response.data.Datos.forEach(function (item) {
                                        var con = 0
                                        if (filtros.SitioReporteSeguimiento.Codigo == item.SitioReporteSeguimiento.Codigo && filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                            con++
                                        }
                                        if (filtros.TipoOrigenSeguimiento.Codigo == item.TipoOrigenSeguimiento.Codigo && filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                            con++
                                        }
                                        if (filtros.PuestoControl !== undefined) {
                                            if (filtros.PuestoControl.Codigo == item.PuestoControl.Codigo && filtros.PuestoControl.Codigo !== undefined) {
                                                con++
                                            }
                                        }
                                        if (filtros.Vehiculo.Placa == item.Vehiculo.Placa && filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                            con++
                                        }
                                        if (con == concidencias) {
                                            $scope.ListadoSeguimientosOrden.push(item)
                                        }
                                        if (item.Observaciones !== '') {
                                            var str = item.Observaciones;
                                            item.Observaciones = str.substring(0, 10);
                                            item.ObservacionCompleta = str.substring(0, 250);
                                            item.OcultarVerMas = true;
                                        } else {
                                            item.OcultarVerMas = false;
                                        }

                                    })
                                    if ($scope.ListadoSeguimientosOrden.length == 0) {
                                        $scope.totalRegistrosOrden = 0;
                                        $scope.totalPaginasOrden = 0;
                                        $scope.paginaActualOrden = 1;
                                        $scope.ResultadoSinRegistrosOrden = 'No hay datos para mostrar';
                                        $scope.BuscandoOrden = false;

                                    } else {
                                        $scope.totalRegistrosOrden = $scope.ListadoSeguimientosOrden[0].TotalRegistros;
                                        $scope.totalPaginasOrden = Math.ceil($scope.totalRegistrosOrden / $scope.cantidadRegistrosPorPaginas);
                                        $scope.BuscandoOrden = false;
                                        $scope.ResultadoSinRegistrosOrden = '';
                                    }

                                } else if ($scope.TipoConsulta == 1) {
                                    var concidencias = 0
                                    if (filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                        concidencias++
                                    }
                                    if (filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                        concidencias++
                                    }
                                    if (filtros.PuestoControl !== undefined) {
                                        if (filtros.PuestoControl.Codigo !== undefined) {
                                            concidencias++
                                        }
                                    }
                                    if (filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                        concidencias++
                                    }
                                    response.data.Datos.forEach(function (item) {
                                        var con = 0
                                        if (filtros.SitioReporteSeguimiento.Codigo == item.SitioReporteSeguimiento.Codigo && filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                            con++
                                        }
                                        if (filtros.TipoOrigenSeguimiento.Codigo == item.TipoOrigenSeguimiento.Codigo && filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                            con++
                                        }
                                        if (filtros.PuestoControl !== undefined) {
                                            if (filtros.PuestoControl.Codigo == item.PuestoControl.Codigo && filtros.PuestoControl.Codigo !== undefined) {
                                                con++
                                            }
                                        }
                                        if (filtros.Vehiculo.Placa == item.Vehiculo.Placa && filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                            con++
                                        }
                                        if (con == concidencias) {
                                            $scope.ListadoSeguimientosPlanilla.push(item)
                                        }
                                        if (item.Observaciones !== '') {
                                            var str = item.Observaciones;
                                            item.Observaciones = str.substring(0, 10);
                                            item.ObservacionCompleta = str.substring(0, 250);
                                            item.OcultarVerMas = true;
                                        } else {
                                            item.OcultarVerMas = false;
                                        }

                                    })
                                    if ($scope.ListadoSeguimientosPlanilla.length == 0) {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;

                                    } else {
                                        $scope.totalRegistros = $scope.ListadoSeguimientosPlanilla[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                    }
                                }
                            } else {
                                if ($scope.TipoConsulta == 1) {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                } else {
                                    $scope.totalRegistrosOrden = 0;
                                    $scope.totalPaginasOrden = 0;
                                    $scope.paginaActualOrden = 1;
                                    $scope.ResultadoSinRegistrosOrden = 'No hay datos para mostrar';
                                    $scope.BuscandoOrden = false;
                                }
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            blockUI.stop();
        }

        /*---------------------------------------------------------------------Funcion Anular Seguimiento----------------------------------------------------------------------------*/
        /*Validacion boton anular */
        $scope.ConfirmacionAnularSeguimiento = function (item) {
            if (item.NumeroDocumentoRemesa !== null && item.NumeroDocumentoRemesa !== undefined && item.NumeroDocumentoRemesa !== '' && item.NumeroDocumentoRemesa !== 0) {
                $scope.ValorBanderaAnularRemesa = item.NumeroPlanilla;
            }
            if ($scope.ValidarPermisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
                $scope.CodigoSeguimiento = item.Codigo;
                if (item.Anulado !== 1) {
                    showModal('modalConfirmacionAnularSeguimiento');
                }
                else {
                    closeModal('modalConfirmacionAnularSeguimiento');
                }
            }
        };

        /*Funcion que solicita los datos de la anulación */
        $scope.SolicitarDatosAnulacion = function () {
            $scope.FechaAnulacion = new Date();
            closeModal('modalConfirmacionAnularSeguimiento');
            showModal('modalDatosAnularSeguimiento');
        };

        $scope.Anular = function () {
            if (DatosRequeridosAnular()) {
                var entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.CodigoSeguimiento,
                    NumeroDocumentoRemesa: $scope.ValorBanderaAnularRemesa,
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnulacion: $scope.CausaAnulacion
                };

                DetalleSeguimientoVehiculosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anuló el seguimiento');
                            closeModal('modalDatosAnularSeguimiento');
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridosAnular() {
            $scope.MensajesErrorAnular = [];
            var continuar = true;
            if ($scope.CausaAnulacion == undefined || $scope.CausaAnulacion == '' || $scope.CausaAnulacion == null) {
                $scope.MensajesErrorAnular.push('Debe ingresar la causa anulación');
                continuar = false;
            }
            return continuar
        }

        /*---------------------------------------------------------------------Funcion Anular Seguimiento flota propia----------------------------------------------------------------------------*/
        /*Validacion boton anular */
        $scope.ConfirmacionAnularSeguimientoFlotaPropia = function (item) {
            if ($scope.ValidarPermisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
                $scope.CodigoSeguimiento = item.Codigo;
                if (item.Anulado !== 1) {
                    showModal('modalConfirmacionAnularSeguimientoflotapropia');
                } else {
                    closeModal('modalConfirmacionAnularSeguimientoflotapropia');
                }
            }
        };

        /*Funcion que solicita los datos de la anulación */
        $scope.SolicitarDatosAnulacionFlotaPropia = function () {
            $scope.FechaAnulacion = new Date();
            closeModal('modalConfirmacionAnularSeguimientoflotapropia');
            showModal('modalDatosAnularSeguimientoflotapropia');
        };

        $scope.AnularFlotaPropia = function () {
            if (DatosRequeridosAnular()) {
                var entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.CodigoSeguimiento,
                    FlotaPropia: 1,
                    NumeroDocumentoRemesa: $scope.ValorBanderaAnularRemesa,
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnulacion: $scope.CausaAnulacion
                };

                DetalleSeguimientoVehiculosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anuló el seguimiento');
                            closeModal('modalDatosAnularSeguimientoflotapropia');
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        //--------------funciones de mascaras html-angular------------------------------------------------------------------------------------------------------------------
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope)
        };
        //--------------Imprimir informe--------------------------------------------------------------------------------------------------------------------
        //Función que ejecuta el boton de vista preliminar
        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL, OpcionListado) {

            $scope.OpcionLista = OpcionListado;

            //Depende del listado seleccionado se enviará el nombre por parametro
            if ($scope.OpcionLista === CODIGO_LISTADO_SEGUIMIENTO_DESPACHOS) {
                $scope.NombreReporte = NOMBRE_LISTADO_SEGUIMIENTO_DESPACHOS;
            }
            if ($scope.OpcionLista === CODIGO_LISTADO_SEGUIMIENTO_ORDENES_CARGUE) {
                $scope.NombreReporte = NOMBRE_LISTADO_SEGUIMIENTO_ORDENES_CARGUE;
            }

            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        // funcion enviar parametros al proyecto ASP armando el filtro
        $scope.ArmarFiltro = function () {

            // Filtros Despachos----------------------------------------------------------------------------------------------
            if ($scope.OpcionLista === CODIGO_LISTADO_SEGUIMIENTO_DESPACHOS) {
                var anuladodespachos = 0;
                if ($scope.Despachos.Estado == true) {
                    anuladodespachos = 1;
                } else {
                    anuladodespachos = 0;
                }
                var CodigosOficinas = '';
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Estado === true) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });

                if (CodigosOficinas === '-1,') {
                    $scope.ListadoOficinas.forEach(function (item) {
                        if (item.Codigo !== -1) {
                            CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                        }
                    });
                }
                if ($scope.Despachos.Vehiculo !== undefined && $scope.Despachos.Vehiculo !== null && $scope.Despachos.Vehiculo !== '') {
                    $scope.FiltroArmado += '&Vehiculo=' + $scope.Despachos.Vehiculo;
                }
                if ($scope.Despachos.Conductor !== undefined && $scope.Despachos.Conductor !== '' && $scope.Despachos.Conductor !== null) {
                    $scope.FiltroArmado += '&Conductor=' + $scope.Despachos.Conductor.Codigo;
                }
                if ($scope.Despachos.Cliente !== undefined && $scope.Despachos.Cliente !== '' && $scope.Despachos.Cliente !== null) {
                    $scope.FiltroArmado += '&Cliente=' + $scope.Despachos.Cliente.Codigo;
                }
                if ($scope.Despachos.TipoDueno !== undefined && $scope.Despachos.TipoDueno !== '' && $scope.Despachos.TipoDueno !== null) {
                    if ($scope.Despachos.TipoDueno.Codigo != 2100) {
                        $scope.FiltroArmado += '&TipoDueno=' + $scope.Despachos.TipoDueno.Codigo;
                    }
                }
                if ($scope.Despachos.Producto !== undefined && $scope.Despachos.Producto !== '' && $scope.Despachos.Producto !== null) {
                    $scope.FiltroArmado += '&Producto=' + $scope.Despachos.Producto.Codigo;
                }
                if ($scope.Despachos.Origen !== undefined && $scope.Despachos.Origen !== '' && $scope.Despachos.Origen !== null) {
                    $scope.FiltroArmado += '&Origen=' + $scope.Despachos.Origen.Codigo;
                }
                if ($scope.Despachos.Destino !== undefined && $scope.Despachos.Destino !== '' && $scope.Despachos.Destino !== null) {
                    $scope.FiltroArmado += '&Destino=' + $scope.Despachos.Destino.Codigo;
                }
                if ($scope.Despachos.Ruta !== undefined && $scope.Despachos.Ruta !== '' && $scope.Despachos.Ruta !== null) {
                    $scope.FiltroArmado += '&Ruta=' + $scope.Despachos.Ruta.Codigo;
                }
                if ($scope.Despachos.PuestoControl !== undefined && $scope.Despachos.PuestoControl !== null && $scope.Despachos.PuestoControl !== '') {
                    $scope.FiltroArmado += '&PuestoControl=' + $scope.Despachos.PuestoControl.Codigo;
                }
                if ($scope.Despachos.NumeroPlanilla !== undefined && $scope.Despachos.NumeroPlanilla !== '' && $scope.Despachos.NumeroPlanilla !== null && $scope.Despachos.NumeroPlanilla > 0) {
                    $scope.FiltroArmado += '&NumeroPlanilla=' + $scope.Despachos.NumeroPlanilla;
                }
                if ($scope.Despachos.NumeroManifiesto !== undefined && $scope.Despachos.NumeroManifiesto !== '' && $scope.Despachos.NumeroManifiesto !== null && $scope.Despachos.NumeroManifiesto > 0) {
                    $scope.FiltroArmado += '&NumeroManifiesto=' + $scope.Despachos.NumeroManifiesto;
                }
                if ($scope.Despachos.OrigenReporte !== undefined && $scope.Despachos.OrigenReporte !== null && $scope.Despachos.OrigenReporte !== '' && $scope.Despachos.OrigenReporte.Codigo !== 8000) {
                    $scope.FiltroArmado += '&OrigenReporte=' + $scope.Despachos.OrigenReporte.Codigo;
                }
                if ($scope.Despachos.SitioReporte !== undefined && $scope.Despachos.SitioReporte !== null && $scope.Despachos.SitioReporte !== '' && $scope.Despachos.SitioReporte.Codigo !== 8200) {
                    $scope.FiltroArmado += '&SitioReporte=' + $scope.Despachos.SitioReporte.Codigo;
                }
                if ($scope.Despachos.Mostrar.Codigo !== undefined && $scope.Despachos.Mostrar.Codigo !== null && $scope.Despachos.Mostrar.Codigo !== '' && $scope.Despachos.Mostrar.Codigo !== 11300 && $scope.Despachos.Estado === false) {
                    $scope.FiltroArmado += '&Mostrar=' + $scope.Despachos.Mostrar.CampoAuxiliar2;
                }
                $scope.FiltroArmado += '&CodigosOficinas=' + CodigosOficinas;
                $scope.FiltroArmado += '&Anulado=' + anuladodespachos;
                //Filtro Ordenes Cargue---------------------------------------------------------------------------------------------
            } else if ($scope.OpcionLista === CODIGO_LISTADO_SEGUIMIENTO_ORDENES_CARGUE) {

                if ($scope.Ordenes.Vehiculo !== undefined && $scope.Ordenes.Vehiculo !== null && $scope.Ordenes.Vehiculo !== '') {
                    $scope.FiltroArmado += '&Vehiculo=' + $scope.Ordenes.Vehiculo;
                }
                if ($scope.Ordenes.Conductor !== undefined && $scope.Ordenes.Conductor !== '' && $scope.Ordenes.Conductor !== null) {
                    $scope.FiltroArmado += '&Conductor=' + $scope.Ordenes.Conductor;
                }
                if ($scope.Ordenes.Ruta !== undefined && $scope.Despachos.Ruta !== null && $scope.Despachos.Ruta !== '') {
                    $scope.FiltroArmado += '&Ruta=' + $scope.Ordenes.Ruta.Codigo;
                }
                if ($scope.Ordenes.PuestoControl !== undefined && $scope.Ordenes.PuestoControl !== null && $scope.Ordenes.PuestoControl !== '') {
                    $scope.FiltroArmado += '&PuestoControl=' + $scope.Ordenes.PuestoControl.Codigo;
                }
                if ($scope.Ordenes.NumeroOrden !== undefined && $scope.Ordenes.NumeroOrden !== '' && $scope.Ordenes.NumeroOrden !== null && $scope.Ordenes.NumeroOrden > 0) {
                    $scope.FiltroArmado += '&NumeroOrden=' + $scope.Ordenes.NumeroOrden;
                }
                if ($scope.Ordenes.NumeroManifiesto !== undefined && $scope.Ordenes.NumeroManifiesto !== '' && $scope.Ordenes.NumeroManifiesto !== null && $scope.Ordenes.NumeroManifiesto > 0) {
                    $scope.FiltroArmado += '&NumeroManifiesto=' + $scope.Despachos.NumeroManifiesto;
                }
                if ($scope.Ordenes.OrigenReporte !== undefined && $scope.Ordenes.OrigenReporte !== null && $scope.Ordenes.OrigenReporte !== 0 && $scope.Ordenes.OrigenReporte.Codigo !== 8000) {
                    $scope.FiltroArmado += '&OrigenReporte=' + $scope.Despachos.OrigenReporte.Codigo;
                }
                if ($scope.Ordenes.SitioReporte !== undefined && $scope.Ordenes.SitioReporte !== null && $scope.Ordenes.SitioReporte !== '' && $scope.Ordenes.SitioReporte.Codigo !== 8200) {
                    $scope.FiltroArmado += '&SitioReporte=' + $scope.Despachos.SitioReporte.Codigo;
                }
                if ($scope.Ordenes.Mostrar.Codigo !== undefined && $scope.Ordenes.Mostrar.Codigo !== null && $scope.Ordenes.Mostrar.Codigo !== '' && $scope.Ordenes.Mostrar.Codigo !== 11300 && $scope.Ordenes.Estado === false) {
                    $scope.FiltroArmado += '&Mostrar=' + $scope.Ordenes.Mostrar.CampoAuxiliar2;
                }
                if ($scope.Ordenes.Estado === true) {
                    $scope.FiltroArmado += '&Anulado=1';
                } else {
                    $scope.FiltroArmado += '&Anulado=0';
                }
            }
        };

        //inicializacion de divisiones
        $scope.MostrarConsultaPlanillasDespachos = function () {
            $('#ConsultaOrdenesCargue').hide();
            $('#ConsultaPlanillasDespachos').show();
            $('#ConsultaRemesasPlanillasDespachos').hide();
            $('#ConsultaFlotaPropia').hide();
            $('#ConsultaRNDC').hide();
            $scope.TipoConsulta = 1; // Planillas despachos
            //Find();
        };
        $scope.MostrarConsultaOrdenesCargue = function () {
            $('#ConsultaOrdenesCargue').show();
            $('#ConsultaPlanillasDespachos').hide();
            $('#ConsultaRemesasPlanillasDespachos').hide();
            $('#ConsultaFlotaPropia').hide();
            $('#ConsultaRNDC').hide();
            $scope.TipoConsulta = 0; // Ordenes de cargue
            //Find();
        };
        $scope.MostrarConsultaRemesasPlanillasDespachos = function () {
            $('#ConsultaOrdenesCargue').hide();
            $('#ConsultaPlanillasDespachos').hide();
            $('#ConsultaRemesasPlanillasDespachos').show();
            $('#ConsultaFlotaPropia').hide();
            $('#ConsultaRNDC').hide();
            //FindRemesa();
        };
        $scope.MostrarConsultaFlotaPropia = function () {
            $('#ConsultaOrdenesCargue').hide();
            $('#ConsultaPlanillasDespachos').hide();
            $('#ConsultaRemesasPlanillasDespachos').hide();
            $('#ConsultaFlotaPropia').show();
            $('#ConsultaRNDC').hide();
        };
        $scope.MostrarConsultaRNDC = function () {
            $('#ConsultaOrdenesCargue').hide();
            $('#ConsultaPlanillasDespachos').hide();
            $('#ConsultaRemesasPlanillasDespachos').hide();
            $('#ConsultaFlotaPropia').hide();
            $('#ConsultaRNDC').show();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        //$scope.EnviarCorreo = function (Cliente, Transportador, NumeroViaje, Seguimiento, Vehiculo, Fecha) {
        //    if (Cliente.Codigo > 0) {
        //        var FiltroTercero = {
        //            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //            Codigo: Cliente.Codigo,
        //            EventoCorreo: CODIGO_EVENTO_CORREO_SEGUIMIENTO_VIAJES,
        //        }
        //        TercerosFactory.ConsultarEmailTercero(FiltroTercero).then(function (response) {
        //            if (response.data.ProcesoExitoso == true) {
        //                if (response.data.Datos.length > 0) {
        //                    $scope.CorreosClientes = response.data.Datos

        //                    if (Transportador.Codigo > 0) {
        //                        var FiltroTercero = {
        //                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //                            Codigo: Transportador.Codigo,
        //                            EventoCorreo: CODIGO_EVENTO_CORREO_SEGUIMIENTO_VIAJES,
        //                        }
        //                        TercerosFactory.ConsultarEmailTercero(FiltroTercero).then(function (response) {
        //                            if (response.data.ProcesoExitoso == true) {
        //                                if (response.data.Datos.length > 0) {
        //                                    $scope.CorreosTransportador = response.data.Datos

        //                                    var Filtros = {
        //                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //                                        Codigo: CODIGO_EVENTO_CORREO_SEGUIMIENTO_VIAJES,
        //                                        ConsultarEventoFuncionCorreo: 1,

        //                                    }
        //                                    EventoCorreosFactory.Obtener(Filtros).then(function (response) {
        //                                        if (response.data.ProcesoExitoso == true) {
        //                                            $scope.Correo.EventoCorreo = response.data.Datos

        //                                            if ($scope.Correo.EventoCorreo !== undefined && $scope.Correo.EventoCorreo !== null && $scope.Correo.EventoCorreo !== '') {
        //                                                if ($scope.Correo.EventoCorreo.ListaDistrubicionCorreos !== undefined && $scope.Correo.EventoCorreo.ListaDistrubicionCorreos !== null && $scope.Correo.EventoCorreo.ListaDistrubicionCorreos !== '') {
        //                                                    $scope.Correo.EventoCorreo.ListaDistrubicionCorreos.forEach(function (itemEvento) {
        //                                                        if (itemEvento.Email == CORREO_CLIENTE) {
        //                                                            if ($scope.CorreosClientes.length == 0) {
        //                                                                itemEvento.Email = ''
        //                                                            }
        //                                                            else {
        //                                                                $scope.CorreosClientes.forEach(function (itemCond) {
        //                                                                    $scope.Correo.EventoCorreo.ListaDistrubicionCorreos.push({ Email: itemCond.Correo })
        //                                                                    itemEvento.Email = ''
        //                                                                })
        //                                                            }
        //                                                        }
        //                                                        else if (itemEvento.Email == CORREO_TRANSPORTADOR) {

        //                                                            if ($scope.CorreosTransportador.length == 0) {
        //                                                                itemEvento.Email = ''
        //                                                            }
        //                                                            else {
        //                                                                $scope.CorreosTransportador.forEach(function (itemTrans) {
        //                                                                    $scope.Correo.EventoCorreo.ListaDistrubicionCorreos.push({ Email: itemTrans.Correo })
        //                                                                    itemEvento.Email = ''
        //                                                                })
        //                                                            }
        //                                                        }
        //                                                    });
        //                                                    $scope.urlASP = '';
        //                                                    $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);


        //                                                    $scope.NombreReporte = 'RepSeguimientoVehicularViajes'

        //                                                    $scope.Correo.EventoCorreo.MensajeCorreo += '  <br />  <br /><a href ="' + $scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&Numero=' + NumeroViaje + '&Codigo=' + Seguimiento + '&OpcionExportarPdf=' + 1 + '"> Ver Reporte</a>   <br />  <br />  <br />'

        //                                                    $scope.Correo.Vehiculo = Vehiculo
        //                                                    $scope.Correo.Numero = Seguimiento
        //                                                    $scope.Correo.Fecha = Fecha
        //                                                    $scope.Correo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
        //                                                    $scope.Correo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa

        //                                                    DetalleSeguimientoVehiculosFactory.EncolarCorreo($scope.Correo).
        //                                                        then(function (response) {
        //                                                            if (response.data.ProcesoExitoso === true) {
        //                                                                ShowSuccess('Se realizo el envio del correo al cliente ' + Cliente.NombreCompleto);
        //                                                            }
        //                                                            else {
        //                                                                ShowError(response.data.MensajeOperacion);
        //                                                            }
        //                                                        }, function (response) {
        //                                                            ShowError(response.statusText);
        //                                                        });
        //                                                }
        //                                            }

        //                                        }
        //                                    }, function (response) {
        //                                    })

        //                                }
        //                                else {

        //                                    var Filtros = {
        //                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //                                        Codigo: CODIGO_EVENTO_CORREO_SEGUIMIENTO_VIAJES,
        //                                        ConsultarEventoFuncionCorreo: 1,

        //                                    }
        //                                    EventoCorreosFactory.Obtener(Filtros).then(function (response) {
        //                                        if (response.data.ProcesoExitoso == true) {
        //                                            $scope.Correo.EventoCorreo = response.data.Datos

        //                                            if ($scope.Correo.EventoCorreo !== undefined && $scope.Correo.EventoCorreo !== null && $scope.Correo.EventoCorreo !== '') {
        //                                                if ($scope.Correo.EventoCorreo.ListaDistrubicionCorreos !== undefined && $scope.Correo.EventoCorreo.ListaDistrubicionCorreos !== null && $scope.Correo.EventoCorreo.ListaDistrubicionCorreos !== '') {
        //                                                    $scope.Correo.EventoCorreo.ListaDistrubicionCorreos.forEach(function (itemEvento) {
        //                                                        if (itemEvento.Email == CORREO_CLIENTE) {
        //                                                            if ($scope.CorreosClientes.length == 0) {
        //                                                                itemEvento.Email = ''
        //                                                            }
        //                                                            else {
        //                                                                $scope.CorreosClientes.forEach(function (itemCond) {
        //                                                                    $scope.Correo.EventoCorreo.ListaDistrubicionCorreos.push({ Email: itemCond.Correo })
        //                                                                    itemEvento.Email = ''
        //                                                                })
        //                                                            }
        //                                                        }
        //                                                        else if (itemEvento.Email == CORREO_TRANSPORTADOR) {

        //                                                            if ($scope.CorreosTransportador.length == 0) {
        //                                                                itemEvento.Email = ''
        //                                                            }
        //                                                            else {
        //                                                                $scope.CorreosTransportador.forEach(function (itemTrans) {
        //                                                                    $scope.Correo.EventoCorreo.ListaDistrubicionCorreos.push({ Email: itemTrans.Correo })
        //                                                                    itemEvento.Email = ''
        //                                                                })
        //                                                            }
        //                                                        }
        //                                                    });
        //                                                    $scope.urlASP = '';
        //                                                    $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);


        //                                                    $scope.NombreReporte = 'RepSeguimientoVehicularViajes'

        //                                                    $scope.Correo.EventoCorreo.MensajeCorreo += '  <br />  <br /><a href ="' + $scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&Numero=' + NumeroViaje + '&Codigo=' + Seguimiento + '&OpcionExportarPdf=' + 1 + '"> Ver Reporte</a>   <br />  <br />  <br />'

        //                                                    $scope.Correo.Vehiculo = Vehiculo
        //                                                    $scope.Correo.Numero = Seguimiento
        //                                                    $scope.Correo.Fecha = Fecha
        //                                                    $scope.Correo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
        //                                                    $scope.Correo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa

        //                                                    DetalleSeguimientoVehiculosFactory.EncolarCorreo($scope.Correo).
        //                                                        then(function (response) {
        //                                                            if (response.data.ProcesoExitoso === true) {
        //                                                                ShowSuccess('Se realizo el envio del correo al cliente ' + Cliente.NombreCompleto);
        //                                                            }
        //                                                            else {
        //                                                                ShowError(response.data.MensajeOperacion);
        //                                                            }
        //                                                        }, function (response) {
        //                                                            ShowError(response.statusText);
        //                                                        });


        //                                                }
        //                                            }
        //                                        }
        //                                    }, function (response) {
        //                                    })

        //                                }
        //                            }
        //                        }, function (response) {
        //                        })
        //                    }
        //                    else {
        //                        var Filtros = {
        //                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //                            Codigo: CODIGO_EVENTO_CORREO_SEGUIMIENTO_VIAJES,
        //                            ConsultarEventoFuncionCorreo: 1,

        //                        }
        //                        EventoCorreosFactory.Obtener(Filtros).then(function (response) {
        //                            if (response.data.ProcesoExitoso == true) {
        //                                $scope.Correo.EventoCorreo = response.data.Datos

        //                                if ($scope.Correo.EventoCorreo !== undefined && $scope.Correo.EventoCorreo !== null && $scope.Correo.EventoCorreo !== '') {
        //                                    if ($scope.Correo.EventoCorreo.ListaDistrubicionCorreos !== undefined && $scope.Correo.EventoCorreo.ListaDistrubicionCorreos !== null && $scope.Correo.EventoCorreo.ListaDistrubicionCorreos !== '') {
        //                                        $scope.Correo.EventoCorreo.ListaDistrubicionCorreos.forEach(function (itemEvento) {
        //                                            if (itemEvento.Email == CORREO_CLIENTE) {
        //                                                if ($scope.CorreosClientes.length == 0) {
        //                                                    itemEvento.Email = ''
        //                                                }
        //                                                else {
        //                                                    $scope.CorreosClientes.forEach(function (itemCond) {
        //                                                        $scope.Correo.EventoCorreo.ListaDistrubicionCorreos.push({ Email: itemCond.Correo })
        //                                                        itemEvento.Email = ''
        //                                                    })
        //                                                }
        //                                            }
        //                                            else if (itemEvento.Email == CORREO_TRANSPORTADOR) {

        //                                                if ($scope.CorreosTransportador.length == 0) {
        //                                                    itemEvento.Email = ''
        //                                                }
        //                                                else {
        //                                                    $scope.CorreosTransportador.forEach(function (itemTrans) {
        //                                                        $scope.Correo.EventoCorreo.ListaDistrubicionCorreos.push({ Email: itemTrans.Correo })
        //                                                        itemEvento.Email = ''
        //                                                    })
        //                                                }
        //                                            }
        //                                        });
        //                                        $scope.urlASP = '';
        //                                        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);


        //                                        $scope.NombreReporte = 'RepSeguimientoVehicularViajes'

        //                                        $scope.Correo.EventoCorreo.MensajeCorreo += '  <br />  <br /><a href ="' + $scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&Numero=' + NumeroViaje + '&Codigo=' + Seguimiento + '&OpcionExportarPdf=' + 1 + '"> Ver Reporte</a>   <br />  <br />  <br />'

        //                                        $scope.Correo.Vehiculo = Vehiculo
        //                                        $scope.Correo.Numero = Seguimiento
        //                                        $scope.Correo.Fecha = Fecha
        //                                        $scope.Correo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
        //                                        $scope.Correo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa

        //                                        DetalleSeguimientoVehiculosFactory.EncolarCorreo($scope.Correo).
        //                                            then(function (response) {
        //                                                if (response.data.ProcesoExitoso === true) {
        //                                                    ShowSuccess('Se realizo el envio del correo al cliente ' + Cliente.NombreCompleto);
        //                                                }
        //                                                else {
        //                                                    ShowError(response.data.MensajeOperacion);
        //                                                }
        //                                            }, function (response) {
        //                                                ShowError(response.statusText);
        //                                            });
        //                                    }
        //                                }
        //                            }
        //                        }, function (response) {
        //                        })
        //                    }
        //                }
        //                else {
        //                    ShowError("No se puede enviar el correo al cliente " + Cliente.NombreCompleto + " ya que no tiene correos asignados");
        //                }
        //            }
        //        }, function (response) {
        //        })
        //    }
        //}

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.NuevoSeguimientoFlotaPropia = function (item) {
            $scope.CodigoVehiculo = '';
            $scope.VariableDetalleFlotaPropia = 'NUEVO';
            $scope.DeshabilitarDetalle = false;
            $scope.MensajesErrorSeguimiento = [];
            $scope.ListaNovedadSeguimiento = [];
            $scope.ListadoDetalleSeguimientosPlanilla = []
            $scope.CodigoNovedadSeguimiento = 0;
            showModal('modalnuevoseguimientoflotapropia');
            $scope.DeshabilitarNovedadesSeguimiento = true;
            $scope.ModalFlotaPropia = {};
            $scope.iniciarMapaFlotaPropia();
            if (item !== undefined) {
                $scope.Modelo.Vehiculo = item.Vehiculo
                $scope.CodigoVehiculo = item.Vehiculo.Codigo;
                $scope.CargarDatosVehiculo(item.Vehiculo)
                var Fecha_inicio = new Date(new Date().setSeconds(-432000))
                Fecha_inicio.setHours(0)
                Fecha_inicio.setMinutes(0)
                Fecha_inicio.setMinutes(0)
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    FlotaPropia: 1,
                    Vehiculo: item.Vehiculo,
                    //FechaInicial: Fecha_inicio,
                    Historico: 1,
                    Anulado: 0
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                //DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, FlotaPropia: 1, Codigo: $scope.CodigoVehiculo }).
                //    then(function (response) {
                //        if (response.data.ProcesoExitoso === true) {
                //            if (response.data.Datos.Codigo > 0) {
                //                $scope.Modelo.Vehiculo = response.data.Datos.Vehiculo;
                //                $scope.Modelo.Ruta = response.data.Datos.Ruta;
                //                $scope.CargarDatosVehiculo(response.data.Datos.Vehiculo);
                //                $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                //                $scope.DeshabilitarFlotaPropia = true;
                //            }
                //        }
                //    }, function (response) {
                //        ShowError(response.statusText);
                //    });
            } else {
                $scope.Modelo.Vehiculo = '';
                $scope.Modelo.Ruta = '';
                $scope.DeshabilitarFlotaPropia = false;
            }
            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
            $scope.Modelo.TipoOrigenSeguimiento = { Codigo: CODIGO_TIPO_ORIGEN_SEGUIMIENTO_MANUAL };
            $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == 8200');
            $scope.Modelo.NovedadSeguimiento = '';
            $scope.Modelo.PuestoControl = '';
            $scope.Modelo.Ubicacion = '';
            $scope.Modelo.Observaciones = '';
            $scope.Modelo.Latitud = '';
            $scope.Modelo.Longitud = '';
            $scope.Reportar = false;
            $scope.Prioritario = false;
            $scope.Modelo.ReplicarRemesas = false;
        };

        $scope.GuardarSeguimientoFlotaPropia = function () {
            if (DatosRequeridosFlotaPropia()) {
                if ($scope.Reportar === true) {
                    $scope.Modelo.ReportarCliente = 1;
                } else {
                    $scope.Modelo.ReportarCliente = 0;
                }
                if ($scope.Prioritario === true) {
                    $scope.Modelo.Prioritario = 1;
                }
                else {
                    $scope.Modelo.Prioritario = 0;
                }
                $scope.Modelo.Latitud = parseFloat($scope.Modelo.Latitud);
                $scope.Modelo.Longitud = parseFloat($scope.Modelo.Longitud);
                $scope.Modelo.FlotaPropia = 1;
                if ($scope.Modelo.PuestoControl.PuestoControl !== undefined) {
                    $scope.Modelo.PuestoControl = { Codigo: $scope.Modelo.PuestoControl.PuestoControl.Codigo };
                }
                $scope.Modelo.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas;

                DetalleSeguimientoVehiculosFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.Datos > 0) {
                            closeModal('modalnuevoseguimientoflotapropia');
                            FindFlotaPropia();
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridosFlotaPropia() {
            $scope.MensajesErrorSeguimiento = [];
            var continuar = true;
            if ($scope.Modelo.SitioReporteSeguimiento === undefined || $scope.Modelo.SitioReporteSeguimiento === "" || $scope.Modelo.SitioReporteSeguimiento === null || $scope.Modelo.SitioReporteSeguimiento.Codigo === 8200) {
                $scope.MensajesErrorSeguimiento.push('Debe ingresar el sitio del reporte');
                continuar = false;
            }
            if ($scope.Modelo.Vehiculo === undefined || $scope.Modelo.Vehiculo === "" || $scope.Modelo.Vehiculo === null) {
                $scope.MensajesErrorSeguimiento.push('Debe ingresar la placa del vehiculo');
                continuar = false;
            }
            return continuar;
        }

        $scope.VerSeguimientoFlotaPropia = function (item) {
            $scope.VariableDetalleFlotaPropia = 'DETALLE';
            $scope.DeshabilitarDetalle = true;
            $scope.MensajesErrorSeguimiento = [];
            $scope.CodigoNovedadSeguimiento = 0;
            showModal('modalnuevoseguimientoflotapropia');
            $scope.ModalFlotaPropia = {};
            DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, FlotaPropia: 1, Codigo: item.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            $scope.Modelo.Vehiculo = response.data.Datos.Vehiculo;
                            $scope.Modelo.Ruta = response.data.Datos.Ruta;
                            $scope.CargarDatosVehiculo(response.data.Datos.Vehiculo);
                            $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.Modelo.TipoOrigenSeguimiento = { Codigo: CODIGO_TIPO_ORIGEN_SEGUIMIENTO_MANUAL };
            $scope.CodigoSitioReporteSeguimiento = item.SitioReporteSeguimiento.Codigo;
            if ($scope.ListaSitioReporteModal.length > 0 && $scope.CodigoSitioReporteSeguimiento > 0) {
                $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == ' + item.SitioReporteSeguimiento.Codigo);
            }
            $scope.CodigoNovedadSeguimiento = item.NovedadSeguimiento.Codigo;
            if ($scope.ListaNovedadSeguimiento.length > 0 && $scope.CodigoNovedadSeguimiento > 0) {
                $scope.Modelo.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo == ' + item.NovedadSeguimiento.Codigo);
            }
            $scope.CodigoPuestoControl = item.PuestoControl.Codigo;
            if ($scope.ListaPuntosControl.length > 0 && $scope.CodigoPuestoControl > 0) {
                $scope.Modelo.PuestoControl = $linq.Enumerable().From($scope.ListaPuntosControl).First('$.PuestoControl.Codigo == ' + item.PuestoControl.Codigo);
            }
            $scope.Modelo.NovedadSeguimiento = item.NovedadSeguimiento;
            $scope.Modelo.Ubicacion = item.Ubicacion;
            $scope.Modelo.Observaciones = item.ObservacionCompleta;
            $scope.Modelo.Latitud = item.Latitud;
            $scope.Modelo.Longitud = item.Longitud;
            if (item.ReportarCliente === 1) {
                $scope.Reportar = true;
            } else {
                $scope.Reportar = false;
            }
            if (item.Prioritario === 1) {
                $scope.Prioritario = true;
            } else {
                $scope.Prioritario = false;
            }
            $scope.iniciarMapaFlotaPropia(item.Latitud, item.Longitud);

        };

        $scope.EnviarCorreo = function (item) {
            item.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            DetalleSeguimientoVehiculosFactory.EnviarCorreoCliente(item).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            $scope.ModalOrdenes = response.data.Datos;
                            $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.EnviarCorreoAvanceVehiculosCliente = function () {
            if ($scope.Cliente == undefined || $scope.Cliente == '') {
                ShowError('Por Favor ingrese el cliente')
            }
            else {
                var item = {}
                item.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                item.CodigoCliente = $scope.Cliente.Codigo
                DetalleSeguimientoVehiculosFactory.EnviarCorreoAvanceVehiculosCliente(item).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            closeModal('ModalCliente')
                            ShowSuccess('El informe Se envio Correctamente')
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        /*----------------------------------------------------------------------------------NUEVO SEGUIMIENTO DESPACHOS ORDEN DE CARGUE----------------------------------------------------------------------------------------*/
        $scope.NuevoSeguimiento = function (item) {
            $scope.VariableDetalle = 'NUEVO';
            $scope.DeshabilitarDetalle = false;
            $scope.MensajesErrorSeguimiento = [];
            $scope.ListaNovedadSeguimiento = [];
            $scope.CodigoNovedadSeguimiento = 0;
            if (item.TipoConsulta === 0) {
                showModal('modalnuevoseguimientoorden');
                $scope.DeshabilitarNovedadesSeguimiento = true;
                $scope.ModalOrdenes = {};
                $scope.iniciarMapaOrden();
                DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Codigo, TipoConsulta: item.TipoConsulta }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ModalOrdenes = response.data.Datos;
                                $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoConsulta: 0,
                    NumeroDocumentoOrden: item.NumeroDocumentoOrden,
                    Historico: 1,
                    Anulado: 0
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else if (item.TipoConsulta === 1) {
                showModal('modalnuevoseguimientoplanilla');
                $scope.DeshabilitarNovedadesSeguimiento = true;
                $scope.ModalDespachos = {};
                //$scope.iniciarMapaPlanilla();
                DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Codigo, TipoConsulta: item.TipoConsulta }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ModalDespachos = response.data.Datos;
                                $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoConsulta: 1,
                    NumeroDocumentoPlanilla: item.NumeroDocumentoPlanilla,
                    Historico: 1,
                    Anulado: 0
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos
                                filtros.ConsultaGPS = 1
                                BloqueoPantalla.start('Consultando reportes GPS...');
                                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                                    then(function (response) {
                                        BloqueoPantalla.stop();
                                        if (response.data.ProcesoExitoso === true) {
                                            if (response.data.Datos.length > 0) {
                                                $scope.ListadoDetalleSeguimientosPlanillaGPS = response.data.Datos
                                                var marker = [];
                                                var map, infoWindow;
                                                function initMap(posicion) {
                                                    if (map === void 0) {
                                                        var mapOptions = {
                                                            zoom: 15,
                                                            center: new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud),
                                                            disableDefaultUI: true,
                                                            mapTypeId: window.google.maps.MapTypeId.ROADMAP
                                                        }
                                                        //Asignacion al id gmapsplanilla que se encuentra en el html
                                                        map = new window.google.maps.Map(document.getElementById('gmapsplanillaGPS'), mapOptions);
                                                    }
                                                }

                                                function setMarker(map, position, title, content) {
                                                    var markers;
                                                    var markerOptions = {
                                                        position: position,
                                                        map: map,
                                                        //draggable opcion que permite mover el marcador
                                                        draggable: false,
                                                        //bounce le da animacion al marcador
                                                        //animation: google.maps.Animation.BOUNCE,
                                                        title: title,
                                                    };

                                                    markers = new window.google.maps.Marker(markerOptions);
                                                    //dragend permite capturar las coordenadas seleccionadas en el mapa
                                                    markers.addListener('dragend', function (event) {
                                                        //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                                                        $scope.Modelo.Latitud = this.getPosition().lat();
                                                        $scope.Modelo.Longitud = this.getPosition().lng();
                                                    });
                                                    //Variable donde se asignan los datos del marcador
                                                    marker.push(markers);

                                                    //var infoWindowOptions = {
                                                    //    content: content
                                                    //};
                                                    //infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                                                    //infoWindow.open(map, markers);

                                                }
                                                for (var i = 0; i < $scope.ListadoDetalleSeguimientosPlanillaGPS.length; i++) {
                                                    var posicion = $scope.ListadoDetalleSeguimientosPlanillaGPS[i]
                                                    if (posicion.Longitud !== 0 && posicion.Latitud !== 0) {
                                                        if ($scope.iniciamapa) {
                                                            initMap(posicion)
                                                            $scope.iniciamapa = false
                                                        }
                                                        setMarker(map, new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud), '', '');

                                                    }
                                                }
                                                for (var i = 0; i < $scope.ListadoDetalleSeguimientosPlanilla.length; i++) {
                                                    var posicion = $scope.ListadoDetalleSeguimientosPlanilla[i]
                                                    if (posicion.Longitud !== 0 && posicion.Latitud !== 0) {
                                                        if ($scope.iniciamapa) {
                                                            initMap(posicion)
                                                            $scope.iniciamapa = false
                                                        }
                                                        setMarker(map, new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud), '', '');
                                                    }
                                                }
                                            }
                                        }
                                    }, function (response) {
                                        ShowError(response.statusText);
                                        BloqueoPantalla.stop();
                                    });
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }

            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.Modelo.NumeroDocumentoPlanilla = item.NumeroDocumentoPlanilla;
            $scope.Modelo.NumeroDocumentoOrden = item.NumeroDocumentoOrden;
            $scope.Modelo.NumeroPlanilla = item.NumeroPlanilla;
            $scope.Modelo.NumeroOrden = item.NumeroOrden;
            $scope.Modelo.NumeroManifiesto = item.NumeroDocumentoManifiesto;
            $scope.Modelo.TipoOrigenSeguimiento = { Codigo: CODIGO_TIPO_ORIGEN_SEGUIMIENTO_MANUAL };
            $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == 8200');
            $scope.Modelo.PuestoControl = '';
            $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
            $scope.Modelo.Ubicacion = '';
            $scope.Modelo.Observaciones = '';
            $scope.Modelo.Latitud = '';
            $scope.Modelo.Longitud = '';
            $scope.Reportar = false;
            $scope.Prioritario = false;
            $scope.Modelo.ReplicarRemesas = false;
            $scope.Modelo.Orden = 0;
            //$scope.Modelo.EnvioReporteCliente = false 
            ConsultarRemesas();
        };
        console.clear()
        $scope.ConsultarDetaleSeguimientos = function (Planilla) {
            showModal('modaldetalleSeguimientoPlanilla')
            $scope.ListadoDetalleSeguimientosPlanilla = [];
            $scope.ListadoDetalleSeguimientosPlanillaGPS = [];
            $scope.iniciamapa = true
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoConsulta: 1,
                NumeroDocumentoPlanilla: Planilla,
                Historico: 1,
                Anulado: 0
            }
            DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        }
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SITIO_REPORTE_SEGUIMIENDO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSitioReporteSeguimientos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoSitioReporteSeguimientos = response.data.Datos;
                    }
                    else {
                        $scope.ListadoSitioReporteSeguimientos = []
                    }
                }
            }, function (response) {
            });
        $scope.ShowList = function (item) {
            if (item.show == true) {
                item.show = false
            } else {
                item.show = true
            }
        }
        $scope.AbrirTiempos = function (item) {
            //PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.NumeroDocumentoPlanilla, Sync: true }).Datos[0]
            $scope.Planillatemp = PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.NumeroDocumentoPlanilla, Sync: true, Estado: { Codigo: 1 }, TipoDocumento: 150 }).Datos[0]
            //$scope.Planillatemp.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            $scope.ListadoTiemposReporte = []
            $scope.ListadoTiemposRemesas = []
            $scope.ListadoTiemposReporte = $linq.Enumerable().From($scope.ListadoSitioReporteSeguimientos).Where(function (x) { return x.CampoAuxiliar2 > 0 }).OrderBy(function (x) { return x.CampoAuxiliar2 }).Select(function (x) { return x }).ToArray();
            $scope.ListadoTiemposLogisticosRemesas = $linq.Enumerable().From($scope.ListadoTiemposLogisticos).Where(function (x) { return x.CampoAuxiliar2 > 0 }).OrderBy(function (x) { return x.CampoAuxiliar2 }).Select(function (x) { return x }).ToArray();
            for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                $scope.ListadoTiemposReporte[i].Fecha = undefined
            }
            PlanillaDespachosFactory.Obtener_Detalle_Tiempos($scope.Planillatemp).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                            for (var j = 0; j < response.data.Datos.length; j++) {
                                if ($scope.ListadoTiemposReporte[i].Codigo == response.data.Datos[j].SitioReporteSeguimientoVehicular.Codigo) {
                                    $scope.ListadoTiemposReporte[i].Fecha = new Date(response.data.Datos[j].FechaHora)
                                }
                            }
                        }
                    }
                }
            });
            PlanillaDespachosFactory.Obtener_Detalle_Tiempos_Remesas($scope.Planillatemp).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTiemposRemesas = []
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if ($scope.ListadoTiemposRemesas.length == 0) {
                                for (var j = 0; j < $scope.ListadoTiemposLogisticosRemesas.length; j++) {
                                    var ObjetoRemesaXtiempos = angular.copy($scope.ListadoTiemposLogisticosRemesas[j])
                                    ObjetoRemesaXtiempos.Numero = response.data.Datos[i].NumeroDocumentoRemesa
                                    ObjetoRemesaXtiempos.CodigoRemesa = response.data.Datos[i].NumeroRemesa
                                    $scope.ListadoTiemposRemesas.push(ObjetoRemesaXtiempos)
                                }
                            } else {
                                var Count = 0
                                for (var k = 0; k < $scope.ListadoTiemposRemesas.length; k++) {
                                    if ($scope.ListadoTiemposRemesas[k].CodigoRemesa == response.data.Datos[i].NumeroRemesa) {
                                        Count++
                                        break;
                                    }
                                }
                                if (Count == 0) {
                                    for (var j = 0; j < $scope.ListadoTiemposLogisticosRemesas.length; j++) {
                                        var ObjetoRemesaXtiempos = angular.copy($scope.ListadoTiemposLogisticosRemesas[j])
                                        ObjetoRemesaXtiempos.Numero = response.data.Datos[i].NumeroDocumentoRemesa
                                        ObjetoRemesaXtiempos.CodigoRemesa = response.data.Datos[i].NumeroRemesa
                                        $scope.ListadoTiemposRemesas.push(ObjetoRemesaXtiempos)
                                    }
                                }
                            }
                        }
                        $scope.ListadoTiemposRemesas = AgruparListados($scope.ListadoTiemposRemesas, 'Numero');

                        for (var j = 0; j < response.data.Datos.length; j++) {
                            for (var i = 0; i < $scope.ListadoTiemposRemesas.length; i++) {
                                if ($scope.ListadoTiemposRemesas[i].Numero == response.data.Datos[j].NumeroDocumentoRemesa) {
                                    for (var k = 0; k < $scope.ListadoTiemposRemesas[i].Data.length; k++) {
                                        if ($scope.ListadoTiemposRemesas[i].Data[k].Codigo == response.data.Datos[j].SitioReporteSeguimientoVehicular.Codigo && $scope.ListadoTiemposRemesas[i].Data[k].CodigoRemesa == response.data.Datos[j].NumeroRemesa) {
                                            $scope.ListadoTiemposRemesas[i].Data[k].Fecha = new Date(response.data.Datos[j].FechaHora)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
            showModal('modalTiempos')
        }
        $scope.GuardarTiempos = function (opt) {
            $scope.opt = opt
            $scope.Planillatemp.Sync = false
            var MilisegundoXdia = 86400000
            var MilisegundoXminuto = 60000
            var fechaPlanilla = new Date($scope.Planillatemp.Fecha)
            $scope.MensajesErrorModalTiempos = []
            for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                var item = $scope.ListadoTiemposReporte[i]
                //Condicionales Basicos de proceso
                if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (item.Fecha >= new Date()) {
                        $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' no puede ser mayor a la fecha actual')
                    }
                    if (i > 0) {
                        if (item.Fecha < $scope.ListadoTiemposReporte[i - 1].Fecha) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' no puede ser menor a la fecha de ' + $scope.ListadoTiemposReporte[i - 1].Nombre)
                        }
                    }
                }

                //Condicionales Logisticos
                //---------------------------------------------Salida Cargue-----------------------------------------

                if (item.Codigo == 8212 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8203) { //Llegada cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue no puede ser mayor de 3 dias a la fecha de llegada al cargue')
                                }
                            }
                        }
                        if (item2.Codigo == 8211) { //Ingreso cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue debe ser mayor a 15 minutos de la fecha de ingreso cargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Fin Cargue-----------------------------------------

                if (item.Codigo == 8205 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8204) { //Inicio cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue debe ser mayor a 15 minutos de la fecha de inicio cargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Ingreso Cargue-----------------------------------------

                if (item.Codigo == 8211 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de ingreso cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Llegada Cargue-----------------------------------------

                if (item.Codigo == 8203 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de llegada cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------LLegada Descargue-----------------------------------------

                if (item.Codigo == 8207 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8212) { //Salida cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue no puede ser mayor a 15 dias de la fecha de salida cargue')
                                } else if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue debe ser mayor a 15 minutos de la fecha de salida cargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de llegada descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Ingreso Descargue-----------------------------------------

                if (item.Codigo == 8213 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de ingreso descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Salida Descargue-----------------------------------------

                if (item.Codigo == 8214 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (item2.Codigo == 8207) { //Llegada descargue
                        if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                            if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue no puede ser mayor de 3 dias a la fecha de llegada descargue')
                            }
                        }
                    }
                    if (item2.Codigo == 8213) { //Ingreso descargue
                        if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                            if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue debe ser mayor a 15 minutos de la fecha de ingreso descargue')
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de salida descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Fin Descargue-----------------------------------------

                if (item.Codigo == 8209 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8208) { //Inicio descargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue debe ser mayor a 15 minutos de la fecha de inicio descargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }
            }
            for (var j = 0; j < $scope.ListadoTiemposRemesas.length; j++) {
                for (var i = 0; i < $scope.ListadoTiemposRemesas[j].Data.length; i++) {
                    var item = $scope.ListadoTiemposRemesas[j].Data[i]
                    //Condicionales Basicos de proceso
                    if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (item.Fecha >= new Date()) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor a la fecha actual')
                        }
                        if (i > 0) {
                            if (item.Fecha < $scope.ListadoTiemposRemesas[j].Data[i - 1].Fecha) {
                                $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser menor a la fecha de ' + $scope.ListadoTiemposRemesas[j].Data[i - 1].Nombre)
                            }
                        }
                    }

                    //Condicionales Logisticos
                    //---------------------------------------------Salida Cargue-----------------------------------------

                    if (item.Codigo == 20205 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20201) { //Llegada cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor de 3 dias a la fecha de llegada al cargue')
                                    }
                                }
                            }
                            if (item2.Codigo == 20202) { //Ingreso cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + '  debe ser mayor a 15 minutos de la fecha de ingreso cargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Fin Cargue-----------------------------------------

                    if (item.Codigo == 20204 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20203) { //Inicio cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de inicio cargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Ingreso Cargue-----------------------------------------

                    if (item.Codigo == 20202 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de ingreso cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Llegada Cargue-----------------------------------------

                    if (item.Codigo == 20201 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de llegada cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------LLegada Descargue-----------------------------------------

                    if (item.Codigo == 20206 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20205) { //Salida cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor a 15 dias de la fecha de salida cargue')
                                    } else if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de salida cargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de llegada descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Ingreso Descargue-----------------------------------------

                    if (item.Codigo == 20207 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de ingreso descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Salida Descargue-----------------------------------------

                    if (item.Codigo == 20210 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (item2.Codigo == 20206) { //Llegada descargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor de 3 dias a la fecha de llegada descargue')
                                }
                            }
                        }
                        if (item2.Codigo == 20207) { //Ingreso descargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de ingreso descargue')
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Fin Descargue-----------------------------------------

                    if (item.Codigo == 20209 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20208) { //Inicio descargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de inicio descargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                }
            }
            if ($scope.MensajesErrorModalTiempos.length == 0) {
                $scope.Planillatemp.DetalleTiempos = []
                for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                    var item = $scope.ListadoTiemposReporte[i]
                    if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        $scope.Planillatemp.DetalleTiempos.push({
                            SitioReporteSeguimientoVehicular: { Codigo: item.Codigo },
                            FechaHora: item.Fecha
                        })
                    }
                }
                $scope.Planillatemp.DetalleTiemposRemesa = []
                for (var j = 0; j < $scope.ListadoTiemposRemesas.length; j++) {
                    for (var i = 0; i < $scope.ListadoTiemposRemesas[j].Data.length; i++) {
                        var item = $scope.ListadoTiemposRemesas[j].Data[i]
                        if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                            $scope.Planillatemp.DetalleTiemposRemesa.push({
                                SitioReporteSeguimientoVehicular: { Codigo: item.Codigo },
                                FechaHora: item.Fecha,
                                NumeroRemesa: item.CodigoRemesa
                            })
                        }
                    }
                }
                if ($scope.Planillatemp.DetalleTiempos.length == 0 && $scope.Planillatemp.DetalleTiemposRemesa.length == 0) {
                    ShowError('Debe ingresar al menos una fecha')
                }
                else {
                    $scope.Planillatemp.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    PlanillaDespachosFactory.Insertar_Detalle_Tiempos($scope.Planillatemp).then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('Las fechas se guardaron correctamente')
                                if ($scope.opt > 0) {
                                    closeModal('modalTiempos2', 1)
                                } else {
                                    closeModal('modalTiempos')
                                }
                            }
                        }
                    });
                }
            }
        }
        $scope.GuardarSeguimiento = function () {

            if (DatosRequeridos()) {
                var continuar = true
                if ($scope.Modelo.SitioReporteSeguimiento.Codigo == 8210 && $scope.Modelo.NumeroDocumentoPlanilla > 0) {
                    $scope.Planillatemp = PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: $scope.Modelo.NumeroDocumentoPlanilla, Sync: true, Estado: { Codigo: 1 }, TipoDocumento: 150 }).Datos[0]
                    $scope.Planillatemp.Sync = true
                    //$scope.Planillatemp.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                    $scope.ListadoTiemposReporte = []
                    $scope.ListadoTiemposRemesas = []
                    $scope.ListadoTiemposReporte = $linq.Enumerable().From($scope.ListadoSitioReporteSeguimientos).Where(function (x) { return x.CampoAuxiliar2 > 0 }).OrderBy(function (x) { return x.CampoAuxiliar2 }).Select(function (x) { return x }).ToArray();
                    $scope.ListadoTiemposLogisticosRemesas = $linq.Enumerable().From($scope.ListadoTiemposLogisticos).Where(function (x) { return x.CampoAuxiliar2 > 0 }).OrderBy(function (x) { return x.CampoAuxiliar2 }).Select(function (x) { return x }).ToArray();

                    for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                        $scope.ListadoTiemposReporte[i].Fecha = undefined
                    }
                    var tiempos = PlanillaDespachosFactory.Obtener_Detalle_Tiempos($scope.Planillatemp).Datos
                    var tiemposRemesas = PlanillaDespachosFactory.Obtener_Detalle_Tiempos_Remesas($scope.Planillatemp).Datos
                    for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                        for (var j = 0; j < tiempos.length; j++) {
                            if ($scope.ListadoTiemposReporte[i].Codigo == tiempos[j].SitioReporteSeguimientoVehicular.Codigo) {
                                $scope.ListadoTiemposReporte[i].Fecha = new Date(tiempos[j].FechaHora)
                            }
                        }
                    }
                    if (tiempos.length == $scope.ListadoTiemposReporte.length) {
                        continuar = true
                    } else {
                        continuar = false
                        showModal('modalTiempos2')
                        ShowError('Por favor registre todos los tiempos de la planilla')
                    }

                    $scope.ListadoTiemposRemesas = []
                    for (var i = 0; i < tiemposRemesas.length; i++) {
                        if ($scope.ListadoTiemposRemesas.length == 0) {
                            for (var j = 0; j < $scope.ListadoTiemposLogisticosRemesas.length; j++) {
                                var ObjetoRemesaXtiempos = angular.copy($scope.ListadoTiemposLogisticosRemesas[j])
                                ObjetoRemesaXtiempos.Numero = tiemposRemesas[i].NumeroDocumentoRemesa
                                ObjetoRemesaXtiempos.CodigoRemesa = tiemposRemesas[i].NumeroRemesa
                                $scope.ListadoTiemposRemesas.push(ObjetoRemesaXtiempos)
                            }
                        } else {
                            var Count = 0
                            for (var k = 0; k < $scope.ListadoTiemposRemesas.length; k++) {
                                if ($scope.ListadoTiemposRemesas[k].CodigoRemesa == tiemposRemesas[i].NumeroRemesa) {
                                    Count++
                                    break;
                                }
                            }
                            if (Count == 0) {
                                for (var j = 0; j < $scope.ListadoTiemposLogisticosRemesas.length; j++) {
                                    var ObjetoRemesaXtiempos = angular.copy($scope.ListadoTiemposLogisticosRemesas[j])
                                    ObjetoRemesaXtiempos.Numero = tiemposRemesas[i].NumeroDocumentoRemesa
                                    ObjetoRemesaXtiempos.CodigoRemesa = tiemposRemesas[i].NumeroRemesa
                                    $scope.ListadoTiemposRemesas.push(ObjetoRemesaXtiempos)
                                }
                            }
                        }
                    }
                    if (tiemposRemesas.length == $scope.ListadoTiemposRemesas.length) {
                        continuar = true

                    } else {
                        continuar = false
                        showModal('modalTiempos2')
                        ShowError('Por favor registre todos los tiempos de la planilla y las remesas')

                    }
                    $scope.ListadoTiemposRemesas = AgruparListados($scope.ListadoTiemposRemesas, 'Numero');
                    for (var j = 0; j < tiemposRemesas.length; j++) {
                        for (var i = 0; i < $scope.ListadoTiemposRemesas.length; i++) {
                            if ($scope.ListadoTiemposRemesas[i].Numero == tiemposRemesas[j].NumeroDocumentoRemesa) {
                                for (var k = 0; k < $scope.ListadoTiemposRemesas[i].Data.length; k++) {
                                    if ($scope.ListadoTiemposRemesas[i].Data[k].Codigo == tiemposRemesas[j].SitioReporteSeguimientoVehicular.Codigo && $scope.ListadoTiemposRemesas[i].Data[k].CodigoRemesa == tiemposRemesas[j].NumeroRemesa) {
                                        $scope.ListadoTiemposRemesas[i].Data[k].Fecha = new Date(tiemposRemesas[j].FechaHora)
                                    }
                                }
                            }
                        }
                    }
                }
                if (continuar) {
                    if ($scope.Reportar === true) {
                        $scope.Modelo.ReportarCliente = 1;
                    } else {
                        $scope.Modelo.ReportarCliente = 0;
                    }
                    if ($scope.Prioritario === true) {
                        $scope.Modelo.Prioritario = 1;
                    }
                    else {
                        $scope.Modelo.Prioritario = 0;
                    }

                    $scope.Modelo.Latitud = parseFloat($scope.Modelo.Latitud);
                    $scope.Modelo.Longitud = parseFloat($scope.Modelo.Longitud);

                    $scope.Modelo.ListaSeguimientosRemesas = [];
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos.forEach(function (item) {
                        item.NumeroPlanilla = $scope.Modelo.NumeroPlanilla;
                        item.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                        item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        item.Latitud = parseFloat($scope.Modelo.Latitud);
                        item.Longitud = parseFloat($scope.Modelo.Longitud);
                        if (item.ReportarCliente === true) {
                            item.ReportarCliente = 1;
                        } else {
                            item.ReportarCliente = 0;
                        }
                        $scope.Modelo.ListaSeguimientosRemesas.push(item);
                    });

                    if ($scope.Modelo.PuestoControl.PuestoControl !== undefined) {
                        $scope.Modelo.PuestoControl = { Codigo: $scope.Modelo.PuestoControl.PuestoControl.Codigo };
                    }
                    if ($scope.Modelo.SitioReporteSeguimiento.Codigo == 8210) {
                        showModal('ModalConfirmacionGuardarSeguimiento');
                    } else {
                        DetalleSeguimientoVehiculosFactory.Guardar($scope.Modelo).
                            then(function (response) {
                                if (response.data.Datos > 0) {
                                    closeModal('ModalConfirmacionGuardarSeguimiento');
                                    closeModal('modalnuevoseguimientoplanilla');
                                    closeModal('modalnuevoseguimientoorden');
                                    Find();
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }
            }
        };
        $scope.ConfirmacionGuardarNuevoSeguimientos = function () {

            DetalleSeguimientoVehiculosFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.Datos > 0) {
                        closeModal('ModalConfirmacionGuardarSeguimiento');
                        closeModal('modalnuevoseguimientoplanilla');
                        closeModal('modalnuevoseguimientoorden');
                        Find();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        function DatosRequeridos() {
            $scope.MensajesErrorSeguimiento = [];
            var continuar = true;
            if ($scope.Modelo.SitioReporteSeguimiento === undefined || $scope.Modelo.SitioReporteSeguimiento === "" || $scope.Modelo.SitioReporteSeguimiento === null || $scope.Modelo.SitioReporteSeguimiento.Codigo === 8200) {
                $scope.MensajesErrorSeguimiento.push('Debe ingresar el sitio del reporte');
                continuar = false;
            }
            return continuar;
        }

        $scope.VerSeguimiento = function (item) {
            $scope.VariableDetalle = 'DETALLE'
            $scope.MensajesErrorSeguimiento = [];
            $scope.ListadoDetalleSeguimientosPlanilla = []
            $scope.CodigoNovedadSeguimiento = 0;
            if (item.TipoConsulta === 0) {
                showModal('modalnuevoseguimientoorden');
                $scope.ModalOrdenes = {}
                DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Codigo, TipoConsulta: item.TipoConsulta }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ModalOrdenes = response.data.Datos;
                                $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                                if ($scope.ModalOrdenes.ReportarCliente === 1) {
                                    $scope.Reportar = true;
                                } else {
                                    $scope.Reportar = false;
                                }
                                if ($scope.ModalOrdenes.Prioritario === 1) {
                                    $scope.Prioritario = true;
                                } else {
                                    $scope.Prioritario = false;
                                }
                                $scope.DeshabilitarDetalle = true;
                                $scope.iniciarMapaOrden($scope.ModalOrdenes.Latitud, $scope.ModalOrdenes.Longitud)
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoConsulta: 0,
                    NumeroDocumentoOrden: item.NumeroDocumentoOrden,
                    Anulado: 0
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else if (item.TipoConsulta == 1) {
                showModal('modalnuevoseguimientoplanilla');
                $scope.ModalDespachos = {}
                DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Codigo, TipoConsulta: item.TipoConsulta }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ModalDespachos = response.data.Datos;
                                $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                                $scope.Modelo.Orden = response.data.Datos.Orden;
                                ConsultarRemesas();
                                if ($scope.ModalDespachos.ReportarCliente === 1) {
                                    $scope.Reportar = true;
                                } else {
                                    $scope.Reportar = false;
                                }
                                if ($scope.ModalDespachos.Prioritario === 1) {
                                    $scope.Prioritario = true;
                                } else {
                                    $scope.Prioritario = false;
                                }
                                $scope.DeshabilitarDetalle = true;
                                $scope.iniciarMapaPlanilla($scope.ModalDespachos.Latitud, $scope.ModalDespachos.Longitud)
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoConsulta: 1,
                    NumeroDocumentoPlanilla: item.NumeroDocumentoPlanilla,
                    Anulado: 0
                }
                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }

            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.Modelo.NumeroDocumentoPlanilla = item.NumeroDocumentoPlanilla;
            $scope.Modelo.NumeroDocumentoOrden = item.NumeroDocumentoOrden;
            $scope.Modelo.NumeroPlanilla = item.NumeroPlanilla;
            $scope.Modelo.NumeroOrden = item.NumeroOrden;
            $scope.Modelo.NumeroManifiesto = item.NumeroManifiesto;
            $scope.Modelo.TipoOrigenSeguimiento = { Codigo: CODIGO_TIPO_ORIGEN_SEGUIMIENTO_MANUAL };

            $scope.CodigoSitioReporteSeguimiento = item.SitioReporteSeguimiento.Codigo;
            if ($scope.ListaSitioReporteModal.length > 0 && $scope.CodigoSitioReporteSeguimiento > 0) {
                $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == ' + item.SitioReporteSeguimiento.Codigo);
            }

            $scope.CodigoNovedadSeguimiento = item.NovedadSeguimiento.Codigo;
            if ($scope.ListaNovedadSeguimiento.length > 0 && $scope.CodigoNovedadSeguimiento > 0) {
                $scope.Modelo.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo == ' + item.NovedadSeguimiento.Codigo);
            }

            $scope.CodigoPuestoControl = item.PuestoControl.Codigo;
            if ($scope.ListaPuntosControl.length > 0 && $scope.CodigoPuestoControl > 0) {
                $scope.Modelo.PuestoControl = $linq.Enumerable().From($scope.ListaPuntosControl).First('$.PuestoControl.Codigo == ' + item.PuestoControl.Codigo);
            }

            $scope.Modelo.Ubicacion = item.Ubicacion;
            $scope.Modelo.Observaciones = item.ObservacionCompleta;
            $scope.Modelo.Latitud = item.Latitud;
            $scope.Modelo.Longitud = item.Longitud;
            //$scope.Modelo.EnvioReporteCliente = false

        }

        /*---------------------------------------------------------------------Funciones grid remesas----------------------------------------------------------------------------*/
        function ConsultarRemesas() {
            /*Cargar el combo Novedad seguimiento */
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NOVEDAD_SEGUIMIENTO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaNovedadSeguimiento = [];
                        $scope.ListaNovedadSeguimiento = response.data.Datos;
                        if ($scope.CodigoNovedadSeguimiento !== undefined && $scope.CodigoNovedadSeguimiento !== '' && $scope.CodigoNovedadSeguimiento !== null && $scope.CodigoNovedadSeguimiento !== 0) {
                            $scope.Modelo.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo ==' + $scope.CodigoNovedadSeguimiento);
                        } else {
                            $scope.Modelo.NovedadSeguimiento = '';
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroPlanilla: $scope.Modelo.NumeroPlanilla,
                Orden: $scope.Modelo.Orden
                //Pagina: $scope.paginaActualPlanillaRemesas,
                //RegistrosPagina: $scope.cantidadRegistrosPorPaginasPlanillaRemesas
            };
            DetalleSeguimientoVehiculosFactory.ConsultarRemesas(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoSeguimientosRemesasPlanillaDespachos = [];
                            response.data.Datos.forEach(function (item) {
                                item.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == ' + item.SitioReporteSeguimiento.Codigo);
                                if ($scope.ListaNovedadSeguimiento.length) {
                                    item.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo == ' + item.NovedadSeguimiento.Codigo);
                                } else {
                                    $scope.ListaNovedadSeguimiento = [];
                                    $scope.Modelo.NovedadSeguimiento = '';
                                }
                                if (item.ReportarCliente === 1) {
                                    item.ReportarCliente = true;
                                } else {
                                    item.ReportarCliente = false;
                                }
                                $scope.ListadoSeguimientosRemesasPlanillaDespachos.push(item);
                            });
                            if ($scope.ListadoSeguimientosRemesasPlanillaDespachos.length > 0) {
                                $scope.totalRegistrosPlanillaRemesas = $scope.ListadoSeguimientosRemesasPlanillaDespachos.length;
                                $scope.BuscandoPlanillaRemesas = true;
                                $scope.ResultadoSinRegistrosPlanillaRemesas = '';
                            } else {
                                $scope.totalRegistrosPlanillaRemesas = 0;
                                $scope.paginaActualPlanillaRemesas = 1;
                                $scope.ResultadoSinRegistrosPlanillaRemesas = 'No hay datos para mostrar';
                                $scope.BuscandoPlanillaRemesas = false;
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.AsignarValor = function (item2) {
            for (var j = 0; j < $scope.ListadoSeguimientosRemesasPlanillaDespachos.length; j++) {
                var itemGrid = $scope.ListadoSeguimientosRemesasPlanillaDespachos[j];
                $scope.MensajesErrorSeguimiento = [];
                if (item2.SitioReporteSeguimiento === undefined || item2.SitioReporteSeguimiento === "" || item2.SitioReporteSeguimiento === null) {
                    $scope.MensajesErrorSeguimiento.push('Debe seleccionar una opción, en el sitio del reporte para la remesa' + item2.NumeroDocumentoRemesa);
                } else if (item2.NovedadSeguimiento === undefined || item2.NovedadSeguimiento === "" || item2.NovedadSeguimiento === null) {
                    $scope.MensajesErrorSeguimiento.push('Debe seleccionar una opción, en la novedad para la remesa' + item2.NumeroDocumentoRemesa);
                } else {
                    if (
                        itemGrid.NumeroDocumentoRemesa === item2.NumeroDocumentoRemesa
                        && itemGrid.NumeroRemesa === item2.NumeroRemesa
                    ) {
                        $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].SitioReporteSeguimiento = item2.SitioReporteSeguimiento;
                        $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].NovedadSeguimiento = item2.NovedadSeguimiento;
                        $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].ReportarCliente = item2.ReportarCliente;
                    }
                }
            }
        };

        $scope.ReplicarRemesas = function (modelo, reportar) {
            for (var j = 0; j < $scope.ListadoSeguimientosRemesasPlanillaDespachos.length; j++) {
                $scope.MensajesErrorSeguimiento = [];
                if (modelo.ReplicarRemesas === true) {
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].SitioReporteSeguimiento = modelo.SitioReporteSeguimiento;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].NovedadSeguimiento = modelo.NovedadSeguimiento;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].ReportarCliente = reportar;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Ubicacion = modelo.Ubicacion;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Observaciones = modelo.Observaciones;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Latitud = modelo.Latitud;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Longitud = modelo.Longitud;
                } else {
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == 8200');
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo == 8100');
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].ReportarCliente = false;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Ubicacion = '';
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Observaciones = '';
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Latitud = 0;
                    $scope.ListadoSeguimientosRemesasPlanillaDespachos[j].Longitud = 0;
                }
            }
        };

        $scope.CopyItem = function (d) {
            var Objet = JSON.parse(JSON.stringify(new Object(d)));
            return Objet;
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.CargarMapa = function (item) {
            showModal('modalMostrarMapa');
            $scope.iniciarMapa(item.Latitud, item.Longitud);
        };
        $scope.CargarMapa2 = function (item) {
            showModal('modalMostrarMapa2');
            $scope.iniciarMapa(item.Latitud, item.Longitud);
        };

        /*----------------------------------------------------------------------------------------------Funciones mapa----------------------------------------------------------------------------------*/
        $scope.iniciarMapaPlanilla = function (lat, lng) {
            //inicialización de variables
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }
                    //Asignacion al id gmapsplanilla que se encuentra en el html
                    map = new window.google.maps.Map(document.getElementById('gmapsplanilla'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    //draggable opcion que permite mover el marcador
                    draggable: true,
                    //bounce le da animacion al marcador
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                };

                markers = new window.google.maps.Marker(markerOptions);
                //dragend permite capturar las coordenadas seleccionadas en el mapa
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                //Variable donde se asignan los datos del marcador
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                //Condición que muestra la ubicación del api o la ubicación segón latitud y longitud 
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }

        $scope.iniciarMapaOrden = function (lat, lng) {
            //inicialización de variables
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }
                    //Asignacion al id gmapsplanilla que se encuentra en el html
                    map = new window.google.maps.Map(document.getElementById('gmapsorden'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    //draggable opcion que permite mover el marcador
                    draggable: true,
                    //bounce le da animacion al marcador
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                };

                markers = new window.google.maps.Marker(markerOptions);
                //dragend permite capturar las coordenadas seleccionadas en el mapa
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                //Variable donde se asignan los datos del marcador
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                //Condición que muestra la ubicación del api o la ubicación segón latitud y longitud 
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        };

        $scope.iniciarMapaFlotaPropia = function (lat, lng) {
            //inicialización de variables
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }
                    //Asignacion al id gmapsplanilla que se encuentra en el html
                    map = new window.google.maps.Map(document.getElementById('gmapsflotapropia'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    //draggable opcion que permite mover el marcador
                    draggable: true,
                    //bounce le da animacion al marcador
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                };

                markers = new window.google.maps.Marker(markerOptions);
                //dragend permite capturar las coordenadas seleccionadas en el mapa
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                //Variable donde se asignan los datos del marcador
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                //Condición que muestra la ubicación del api o la ubicación segón latitud y longitud 
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }

        $scope.iniciarMapa = function (lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }

                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };

                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }

        $scope.ReplicarTiemposRemesas = function () {
            for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                for (var j = 0; j < $scope.ListadoTiemposRemesas.length; j++) {
                    $scope.ListadoTiemposRemesas[j].Data[i].Fecha = angular.copy($scope.ListadoTiemposReporte[i].Fecha)
                }
            }
            ShowSuccess('Los tiempos se replicarón a todas las remesas correctamente ')
        }
        $scope.Replicarplanilla = function (item) {
            $scope.TempItem = item
        }
        $scope.ReplicarTiemposPlanilla = function () {
            var item = $scope.TempItem
            for (var i = 0; i < item.Data.length; i++) {
                $scope.ListadoTiemposReporte[i].Fecha = angular.copy(item.Data[i].Fecha)
            }
            ShowSuccess('Los tiempos se replicarón a la planilla correctamente ')
        }
        /*----------------------------------------------------------------------------------------------Funciones Seguimientos Remesas----------------------------------------------------------------------------------*/

        $scope.AbrirDistribucion = function (item) {
            /*Cargar el combo de tipo identificaciones*/
            $scope.MensajesErrorModal = [];

            $scope.NumeroDocumentoRemesa = item.NumeroDocumentoRemesa;
            $scope.Distribucion.NumeroRemesa = item.NumeroRemesa;
            $scope.ResultadoSinRegistrosDistribucion = '';

            //funcion que consulta grid lista distribución remesas
            $scope.ListaDistribucion = [];
            var filtroDistribucion = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: $scope.Distribucion.NumeroRemesa
            };
            DetalleDistribucionRemesasFactory.Consultar(filtroDistribucion).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Observaciones !== '') {
                                    var str = item.Observaciones;
                                    item.Observaciones = str.substring(0, 10);
                                    item.ObservacionCompleta = str.substring(0, 250);
                                    item.OcultarVerMas = true;
                                } else {
                                    item.OcultarVerMas = false;
                                }
                                $scope.ListaDistribucion.push(item);
                            });
                        } else {
                            $scope.DeshabilitarBotonNuevaDistribucion = true;
                            $scope.ResultadoSinRegistrosDistribucion = 'No hay distribuciones';
                        }
                    }
                }, function (response) {
                });
            showModal('modalDistribuciones');
        };
        $scope.CargarDatosRemesa = function () {
            /*Cargar autocomplete de productos transportados*/
            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaProductoTransportados = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListaProductoTransportados = response.data.Datos;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            //Filtros enviados
            var remesafiltros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Distribucion.NumeroRemesa,
                Obtener: 1
            };
            RemesasFactory.Obtener(remesafiltros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.NombreProductoTransportado = response.data.Datos.ProductoTransportado.Nombre;
                        $scope.NumeroCantidad = response.data.Datos.CantidadCliente;
                        $scope.NumeroPeso = response.data.Datos.PesoCliente;
                    } else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        $scope.ConsultarFoto = function (detalle) {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: detalle.NumeroRemesa,
                Codigo: detalle.Codigo
            };
            DetalleDistribucionRemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Fotografia.FotoBits !== undefined && response.data.Datos.Fotografia.FotoBits !== null && response.data.Datos.Fotografia.FotoBits !== '') {
                            $scope.Foto = [];
                            $scope.Foto.push({
                                NombreFoto: response.data.Datos.Fotografia.NombreFoto,
                                TipoFoto: response.data.Datos.Fotografia.TipoFoto,
                                ExtensionFoto: response.data.Datos.Fotografia.ExtensionFoto,
                                FotoBits: response.data.Datos.Fotografia.FotoBits,
                                ValorDocumento: 1,
                                FotoCargada: 'data:' + response.data.Datos.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Fotografia.FotoBits
                            });
                        } else {
                            $scope.Foto = [];
                        }
                    }
                    else {
                        ShowError('No se logró consultar la foto ' + response.data.MensajeOperacion);
                        closeModal('ModalRecibirDistribucion', 1);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    closeModal('ModalRecibirDistribucion', 1);
                });
            showModal('ModalFoto');
        };
        $scope.CerrarModalFoto = function () {
            closeModal('ModalFoto', 1);
        };
        $scope.ConsultarFirma = function (detalle) {
            $scope.LimpiarFirma();
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: detalle.NumeroRemesa,
                Codigo: detalle.Codigo
            };
            DetalleDistribucionRemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Firma !== undefined && response.data.Datos.Firma !== null && response.data.Datos.Firma !== '') {
                            var image = new Image();
                            image.src = "data:image/png;base64," + response.data.Datos.Firma;
                            var img = cargaContextoCanvas('Firma');
                            img.drawImage(image, 0, 0);
                        }
                        $('#botonFirma').click();
                    }
                    else {
                        ShowError('No se logró consultar la firma ' + response.data.MensajeOperacion);
                        closeModal('ModalRecibirDistribucion', 1);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    closeModal('ModalRecibirDistribucion', 1);
                });
            showModal('ModalFirma');
            $('#botonFirma').click();
        };
        $scope.LimpiarFirma = function () {
            var canvas = document.getElementById('Firma');
            var context = canvas.getContext("2d");
            context.clearRect(0, 0, canvas.width, canvas.height);
        };
        $scope.CerrarModalFirma = function () {
            closeModal('ModalFirma', 1);
        };

    }]);