﻿EncoExpresApp.controller("ConsultarListadosAuditoriaDocumentosCtrl", ['$scope', '$timeout', 'blockUI', 'ValorCatalogosFactory', 'ListadosAuditoriaDocumentosFactory', '$linq',
    function ($scope, $timeout, blockUI, ValorCatalogosFactory, ListadosAuditoriaDocumentosFactory, $linq) {
        $scope.MapaSitio = [{ Nombre: 'Utilitarios' }, { Nombre: 'Auditoria' }, { Nombre: 'Consultar Auditoria Documentos' }];

        $scope.cantidadRegistrosPorPaginas = 20;
        $scope.ResultadoSinRegistros = '';
        //-----------------------Cargar Combos y Autocomplete --------------------------------------------------//
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONSULTAR_AUDITORIA_DOCUMENTOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Cargar el combo de Estados */
                
        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];

       

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
       
        // Cargar combos de módulo y listados

        $scope.TipoDocumentos = [];
        for (var i = 0; i < $scope.ListadoMenuListados.length; i++) {
            if ($scope.ListadoMenuListados[i].Padre == OCION_MENU_LISTADOS_AUDITORIA_DOCUMENTOS) {
                $scope.TipoDocumentos.push({ Nombre: $scope.ListadoMenuListados[i].Nombre, Codigo: $scope.ListadoMenuListados[i].Codigo })
            }
        }
        
      

        $scope.TipoDocumento = $linq.Enumerable().From($scope.TipoDocumentos).First('$.Codigo == 140020101');//--Predeterminado Comprobante egreso
        $scope.NombreSeccion = $scope.TipoDocumento.Nombre;

      

        $scope.AsignarFiltro = function () {
            $scope.NombreSeccion = $scope.TipoDocumento.Nombre;
            console.log("TipoDocumento: ", $scope.TipoDocumento);
        }

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //-----------------------------Funcion Consultar---------------------------------------------------

        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        function Find() {
            $scope.FiltroArmado = '';
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoAuditoria = [];
            if (DatosRequeridos()) {
                if ($scope.Buscando) {
                    if ($scope.MensajesError.length === 0) {
                        var estado
                        var anulado

                       
                        if ($scope.Estado.Codigo === 2) {
                            anulado = 1
                            estado = -1
                        } else {
                            estado = $scope.Estado.Codigo
                            anulado = 0
                        }
                        if ($scope.Estado.Codigo === -1) {
                            anulado = -1
                            estado = -1
                        }
                        

                        

                        if ($scope.TipoDocumento.Codigo != undefined && $scope.TipoDocumento.Codigo != null && $scope.TipoDocumento.Codigo != '' || $scope.TipoDocumento.Codigo == 0) {
                            $scope.FiltroArmado = '&TipoConsulta=' + $scope.TipoDocumento.Codigo;
                        }
                        if ($scope.NumeroInicial != undefined && $scope.NumeroInicial != null && $scope.NumeroInicial != '') {
                            $scope.FiltroArmado += '&NumeroInicio=' + $scope.NumeroInicial;
                        }
                        if ($scope.NumeroFinal != undefined && $scope.NumeroFinal != null && $scope.NumeroFinal != '') {
                            $scope.FiltroArmado += '&NumeroFin=' + $scope.NumeroFinal;
                        }                        
                        if ($scope.FechaInicial != undefined && $scope.FechaInicial != null && $scope.FechaInicial != '') {
                            var fI = $scope.FechaInicial.toISOString().replace(/T/g, " ");
                            fI = fI.replace(/Z/, "");
                            $scope.FiltroArmado += '&FechaInicio=' +fI;
                        }
                        if ($scope.FechaFinal != undefined && $scope.FechaFinal != null && $scope.FechaFinal != '') {
                            var fF = $scope.FechaFinal.toISOString().replace(/T/g, " ");
                            fF = fF.replace(/Z/g,"");
                            $scope.FiltroArmado += '&FechaFin=' + fF
                        }
                        if ($scope.NombreUsuario != undefined && $scope.NombreUsuario != null && $scope.NombreUsuario != '') {
                            
                            $scope.FiltroArmado += '&NombreUsuario=' + $scope.NombreUsuario;
                        }
                        if (estado != undefined && estado != null && estado != '' || estado == 0) {
                            $scope.FiltroArmado += '&Estado=' + estado;
                        }
                        if (anulado != undefined && anulado != null && anulado != '' || anulado == 0) {
                            $scope.FiltroArmado += '&Anulado=' + anulado;
                        }
                       


                        filtros = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoConsulta: $scope.TipoDocumento.Codigo,
                            NumeroInicio: $scope.NumeroInicial,
                            NumeroFin: $scope.NumeroFinal,
                            FechaInicio: $scope.FechaInicial,
                            FechaFin: $scope.FechaFinal,
                            NombreUsuario: $scope.NombreUsuario,
                            Estado: estado,
                            Anulado: anulado,
                            Pagina: $scope.paginaActual,
                            RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        }
                        blockUI.delay = 1000;
                        ListadosAuditoriaDocumentosFactory.Consultar(filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) {

                                        $scope.ListadoAuditoria = response.data.Datos;
                                        console.log("listadoAuditoría: ",$scope.ListadoAuditoria);
                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';

                                        for (var i = 0; i < $scope.ListadoAuditoria.length; i++) {
                                            if (new Date($scope.ListadoAuditoria[i].FechaUsuarioModifica) < MIN_DATE || $scope.ListadoAuditoria[i].FechaUsuarioModifica === undefined) {
                                                $scope.ListadoAuditoria[i].FechaUsuarioModifica = ''
                                            }
                                        }

                                        for (var j = 0; j < $scope.ListadoAuditoria.length; j++) {
                                            if (new Date($scope.ListadoAuditoria[j].FechaUsuarioAnula) < MIN_DATE || $scope.ListadoAuditoria[j].FechaUsuarioAnula === undefined) {
                                                $scope.ListadoAuditoria[j].FechaUsuarioAnula = ''
                                            }
                                        }

                                    }
                                    else {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                    var listado = [];
                                    response.data.Datos.forEach(function (item) {
                                        listado.push(item);
                                    });
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }
            }
            blockUI.stop();
        }

        //--------------------------------------------------------------------------------------------------------------------

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if (($scope.FechaInicial === null || $scope.FechaInicial === undefined || $scope.FechaInicial === '')
                && ($scope.FechaFinal === null || $scope.FechaFinal === undefined || $scope.FechaFinal === '')
                && ($scope.NumeroInicial === null || $scope.NumeroInicial === undefined || $scope.NumeroInicial === '' || $scope.NumeroInicial === 0)
                && ($scope.NumeroFinal === null || $scope.NumeroFinal === undefined || $scope.NumeroFinal === '' || $scope.NumeroFinal === 0)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de números o fechas');
                continuar = false

            } else if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)
                || ($scope.NumeroFinal !== null && $scope.NumeroFinal !== undefined && $scope.NumeroFinal !== '' && $scope.NumeroFinal !== 0)
                || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')

            ) {
                if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)
                    && ($scope.NumeroFinal !== null && $scope.NumeroFinal !== undefined && $scope.NumeroFinal !== '' && $scope.NumeroFinal !== 0)) {
                    if ($scope.NumeroFinal < $scope.NumeroInicial) {
                        $scope.MensajesError.push('El número final debe ser mayor al número inicial');
                        continuar = false
                    }
                } else {
                    if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)) {
                        $scope.NumeroFinal = $scope.NumeroInicial
                    } else {
                        $scope.NumeroInicial = $scope.NumeroFinal
                    }
                }
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                    && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')) {
                    if ($scope.FechaFinal < $scope.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO+' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                        $scope.FechaFinal = $scope.FechaInicial
                    } else {
                        $scope.FechaInicial = $scope.FechaFinal
                    }
                }
            }
            return continuar
        }

        //Reporte:

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            
            $scope.NombreReporte = NOMBRE_LISTADO_AUDITORIA_DOCUMENTOS;

            //Arma el filtro
            //$scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo);
            }

            //Se limpia el filtro para poder hacer mas consultas
            //$scope.FiltroArmado = '';

        };

        //Fin Reporte



        $scope.MaskMayus = function (option) {
            MascaraMayus(option)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumero(option)
        };
        //--------------------------------------------------------------------------------------------------------//
    }]);