﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Negocio.Mantenimiento

<Authorize>
Public Class DocumentoEquipoMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EquipoMantenimientoDocumento) As Respuesta(Of IEnumerable(Of EquipoMantenimientoDocumento))
        Return New LogicaDocumentoEquipoMantenimiento(CapaPersistenciaDocumentoEquipoMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EquipoMantenimientoDocumento) As Respuesta(Of EquipoMantenimientoDocumento)
        Return New LogicaDocumentoEquipoMantenimiento(CapaPersistenciaDocumentoEquipoMantenimiento).Obtener(filtro)
    End Function
    <HttpPost>
    <ActionName("InsertarTemporal")>
    Public Function Guardar(ByVal entidad As EquipoMantenimientoDocumento) As Respuesta(Of Long)
        Return New LogicaDocumentoEquipoMantenimiento(CapaPersistenciaDocumentoEquipoMantenimiento).InsertarTemporal(entidad)
    End Function
    <HttpPost>
    <ActionName("EliminarDocumento")>
    Public Function EliminarFoto(ByVal entidad As EquipoMantenimientoDocumento) As Respuesta(Of Boolean)
        Return New LogicaDocumentoEquipoMantenimiento(CapaPersistenciaDocumentoEquipoMantenimiento).EliminarDocumento(entidad)
    End Function

End Class
