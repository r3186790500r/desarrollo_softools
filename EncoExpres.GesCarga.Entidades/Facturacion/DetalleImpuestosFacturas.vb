﻿Imports Newtonsoft.Json
Namespace Facturacion
    <JsonObject>
    Public NotInheritable Class DetalleImpuestosFacturas
        Inherits BaseDetalleDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroEncabezado = Read(lector, "ENFA_Numero")
            ImpuestoFacturas = New ImpuestoFacturas With {.Codigo = Read(lector, "ENIM_Codigo"), .Nombre = Read(lector, "ENIM_Nombre")}
            CATA_TVFI_Codigo = Read(lector, "CATA_TVFI_Codigo")
            Valor_tarifa = Read(lector, "Valor_Tarifa")
            Valor_base = Read(lector, "Valor_Base")
            Valor_impuesto = Read(lector, "Valor_Impuesto")
        End Sub
        <JsonProperty>
        Public Property ImpuestoFacturas As ImpuestoFacturas
        <JsonProperty>
        Public Property CATA_TVFI_Codigo As Integer
        <JsonProperty>
        Public Property Valor_tarifa As Double
        <JsonProperty>
        Public Property Valor_base As Long
        <JsonProperty>
        Public Property Valor_impuesto As Long
    End Class
End Namespace

