﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo
Imports EncoExpres.GesCarga.Negocio.Despachos

''' <summary>
''' Controlador <see cref="LiquidacionesController"/>
''' </summary>
<Authorize>
Public Class LiquidacionesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Liquidacion) As Respuesta(Of IEnumerable(Of Liquidacion))
        Return New LogicaLiquidaciones(CapaPersistenciaLiquidaciones).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Liquidacion) As Respuesta(Of Liquidacion)
        Return New LogicaLiquidaciones(CapaPersistenciaLiquidaciones).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Liquidacion) As Respuesta(Of Long)
        Return New LogicaLiquidaciones(CapaPersistenciaLiquidaciones).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Liquidacion) As Respuesta(Of Boolean)
        Return New LogicaLiquidaciones(CapaPersistenciaLiquidaciones).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerPlanilla")>
    Public Function ObtenerPlanilla(ByVal filtro As Liquidacion) As Respuesta(Of PlanillaDespachos)
        Return New LogicaLiquidaciones(CapaPersistenciaLiquidaciones).ObtenerPlanilla(filtro)
    End Function

    <HttpPost>
    <ActionName("Rechazar")>
    Public Function Rechazar(ByVal filtro As Liquidacion) As Respuesta(Of Boolean)
        Return New LogicaLiquidaciones(CapaPersistenciaLiquidaciones).Rechazar(filtro)
    End Function

    <HttpPost>
    <ActionName("Aprobar")>
    Public Function Aprobar(ByVal filtro As Liquidacion) As Respuesta(Of Boolean)
        Return New LogicaLiquidaciones(CapaPersistenciaLiquidaciones).Aprobar(filtro)
    End Function

    <HttpPost>
    <ActionName("CalcularValores")>
    Public Function CalcularValores(ByVal entidad As Liquidacion) As Respuesta(Of Liquidacion)
        Return New LogicaLiquidaciones(CapaPersistenciaLiquidaciones).CalcularValores(entidad)
    End Function

    <HttpPost>
    <ActionName("CargarCombinacionesCvt")>
    Public Function CargarCombinacionesCvt(ByVal entidad As Liquidacion) As Respuesta(Of Liquidacion)
        Return New LogicaLiquidaciones(CapaPersistenciaLiquidaciones).CargarCombinacionesCvt(entidad)
    End Function
End Class
