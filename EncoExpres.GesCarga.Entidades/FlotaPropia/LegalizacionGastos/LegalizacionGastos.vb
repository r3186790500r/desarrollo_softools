﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo
Imports EncoExpres.GesCarga.Entidades.Tesoreria

Namespace FlotaPropia
    <JsonObject>
    Public NotInheritable Class LegalizacionGastos
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LegalizacionGastos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")
            Conductor = New Terceros With {.NombreCompleto = Read(lector, "Conductor"), .Codigo = Read(lector, "TERC_Codigo_Conductor"), .NumeroIdentificacion = Read(lector, "Indentificacion")}
            Observaciones = Read(lector, "Observaciones")
            ValorGastos = Read(lector, "Valor_Gastos")
            ValorAnticipos = Read(lector, "Valor_Anticipos")
            ValorReanticipos = Read(lector, "Valor_Reanticipos")
            SaldoConductor = Read(lector, "Saldo_Favor_Conductor")
            SaldoEmpresa = Read(lector, "Saldo_Favor_Empresa")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            Oficina = New Oficinas With {.Nombre = Read(lector, "Oficina"), .Codigo = Read(lector, "OFIC_Codigo")}
            TotalRegistros = Read(lector, "TotalRegistros")
            TotalGalones = Read(lector, "Total_Galones")
            TotalGalonesAutorizados = Read(lector, "Total_Galones_Autorizados")
            TotalPeajes = Read(lector, "Total_Peajes")
            Planilla = New PlanillaDespachos With {.Numero = Read(lector, "ENPD_Numero"), .NumeroDocumento = Read(lector, "ENPD_Numero_Documento"), .Conductor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Conductor")}, .Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}}
            ValorTotalCombustible = Read(lector, "Valor_Total_Combustible")
            ValorPromedioGalon = Read(lector, "Valor_Promedio_Galon")
        End Sub
#Region "Propiedades"

        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas

        <JsonProperty>
        Public Property CuentaPorCobrar As EncabezadoDocumentoCuentas
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property ValorGastos As Double
        <JsonProperty>
        Public Property ValorAnticipos As Double
        <JsonProperty>
        Public Property ValorReanticipos As Double
        <JsonProperty>
        Public Property SaldoConductor As Double
        <JsonProperty>
        Public Property SaldoEmpresa As Double
        <JsonProperty>
        Public Property DetalleLegalizacionGastos As IEnumerable(Of DetalleLegalizacionGastos)
        <JsonProperty>
        Public Property DetalleConductores As IEnumerable(Of DetalleLegalizacionGastos)
        <JsonProperty>
        Public Property GastosCobustible As IEnumerable(Of DetalleLegalizacionGastos)
        <JsonProperty>
        Public Property Comprobantes As IEnumerable(Of EncabezadoDocumentoComprobantes)
        <JsonProperty>
        Public Property TotalGalones As Double
        <JsonProperty>
        Public Property TotalGalonesAutorizados As Double
        <JsonProperty>
        Public Property TotalPeajes As Double
        <JsonProperty>
        Public Property Planilla As PlanillaDespachos
        <JsonProperty>
        Public Property ValorTotalCombustible As Double
        <JsonProperty>
        Public Property ValorPromedioGalon As Double

        <JsonProperty>
        Public Property Proveedor As Tercero
        <JsonProperty>
        Public Property ParametrosCalculos As ParametrosCalculos

#End Region

    End Class
    Public Class ParametrosCalculos
        Inherits Base

        Sub New()
        End Sub

        <JsonProperty>
        Public Property ListaPlanillaConceptos As IEnumerable(Of ListaPlanillaConceptos)
        <JsonProperty>
        Public Property Comprobantes As IEnumerable(Of Comprobantes)


    End Class
    Public Class ListaPlanillaConceptos
        Inherits Base

        Sub New()
        End Sub

        <JsonProperty>
        Public Property Planilla As Planilla
        <JsonProperty>
        Public Property Data As IEnumerable(Of Data)

    End Class
    Public Class Planilla
        Inherits Base

        Sub New()
        End Sub

        <JsonProperty>
        Public Property ValorAnticipo As Double
        <JsonProperty>
        Public Property Total_Galones As Double
        <JsonProperty>
        Public Property Total_Km As Double
        <JsonProperty>
        Public Property Km_Inicial As Double
        <JsonProperty>
        Public Property Km_Final As Double
        <JsonProperty>
        Public Property Gal_Estimados As Double
        <JsonProperty>
        Public Property Km_Galon As Double
        <JsonProperty>
        Public Property Galon_Estimado_Termo As Double
        <JsonProperty>
        Public Property Horas_Termo As Double
        <JsonProperty>
        Public Property Galones_Adicionales_Termo As Double
        <JsonProperty>
        Public Property Horas_Fin As Double
        <JsonProperty>
        Public Property Horas_Inicio As Double
        <JsonProperty>
        Public Property Horas_Galon_Termo As Double
        <JsonProperty>
        Public Property Horas_Adicionales_Termo As Double

    End Class
    Public Class Data
        Inherits Base

        Sub New()
        End Sub

        <JsonProperty>
        Public Property Valor As Double
        <JsonProperty>
        Public Property TieneChip As Boolean
        <JsonProperty>
        Public Property ConceptoNombre As String
        <JsonProperty>
        Public Property CantidadGalones As Double
        <JsonProperty>
        Public Property ValorGalones As Double

    End Class
    Public Class Comprobantes
        Inherits Base

        Sub New()
        End Sub

        <JsonProperty>
        Public Property DocumentoOrigenCodigo As Integer
        <JsonProperty>
        Public Property ValorPagoTotal As Double

    End Class
End Namespace
