﻿Print 'V_Tipo_Tipo_Conductor'
GO
DROP VIEW V_Tipo_Tipo_Conductor
GO
CREATE VIEW V_Tipo_Tipo_Conductor 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre, Campo2,Campo3
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 39
GO