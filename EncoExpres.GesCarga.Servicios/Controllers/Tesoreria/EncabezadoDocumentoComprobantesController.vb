﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Negocio.Tesoreria

''' <summary>
''' Controlador <see cref="EncabezadoDocumentoComprobantesController"/>
''' </summary>
<Authorize>
Public Class EncabezadoDocumentoComprobantesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoDocumentoComprobantes) As Respuesta(Of IEnumerable(Of EncabezadoDocumentoComprobantes))
        Return New LogicaEncabezadoDocumentoComprobantes(CapaPersistenciaEncabezadoDocumentoComprobantes, CapaPersistenciaDetalleDocumentoComprobantes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoDocumentoComprobantes) As Respuesta(Of EncabezadoDocumentoComprobantes)
        Return New LogicaEncabezadoDocumentoComprobantes(CapaPersistenciaEncabezadoDocumentoComprobantes, CapaPersistenciaDetalleDocumentoComprobantes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoDocumentoComprobantes) As Respuesta(Of Long)
        Return New LogicaEncabezadoDocumentoComprobantes(CapaPersistenciaEncabezadoDocumentoComprobantes, CapaPersistenciaDetalleDocumentoComprobantes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EncabezadoDocumentoComprobantes) As Respuesta(Of Boolean)
        Return New LogicaEncabezadoDocumentoComprobantes(CapaPersistenciaEncabezadoDocumentoComprobantes, CapaPersistenciaDetalleDocumentoComprobantes).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarCuentasconEgresos")>
    Public Function ConsultarCuentasconEgresos(ByVal filtro As EncabezadoDocumentoComprobantes) As Respuesta(Of IEnumerable(Of EncabezadoDocumentoComprobantes))
        Return New LogicaEncabezadoDocumentoComprobantes(CapaPersistenciaEncabezadoDocumentoComprobantes, CapaPersistenciaDetalleDocumentoComprobantes).ConsultarCuentasconEgresos(filtro)
    End Function
    <HttpPost>
    <ActionName("ActualizarEgresosArchivoRespuesta")>
    Public Function ActualizarEgresosArchivoRespuesta(ByVal filtro As EncabezadoDocumentoComprobantes) As Respuesta(Of Long)
        Return New LogicaEncabezadoDocumentoComprobantes(CapaPersistenciaEncabezadoDocumentoComprobantes, CapaPersistenciaDetalleDocumentoComprobantes).ActualizarEgresosArchivoRespuesta(filtro)
    End Function


    <HttpPost>
    <ActionName("ValidarSecuenciaLotes")>
    Public Function ValidarSecuenciaLotes(ByVal filtro As EncabezadoDocumentoComprobantes) As Respuesta(Of Long)
        Return New LogicaEncabezadoDocumentoComprobantes(CapaPersistenciaEncabezadoDocumentoComprobantes, CapaPersistenciaDetalleDocumentoComprobantes).ValidarSecuenciaLotes(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarCodigosRespuesaBancolombia")>
    Public Function ConsultarCodigosRespuesaBancolombia(ByVal filtro As EncabezadoDocumentoComprobantes) As Respuesta(Of IEnumerable(Of CodigoRespuestaBancolombia))
        Return New LogicaEncabezadoDocumentoComprobantes(CapaPersistenciaEncabezadoDocumentoComprobantes, CapaPersistenciaDetalleDocumentoComprobantes).ConsultarCodigosRespuesaBancolombia(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarCadenaEgresos")>
    Public Function ConsultarCadenaEgresos(ByVal filtro As EncabezadoDocumentoComprobantes) As Respuesta(Of IEnumerable(Of EncabezadoDocumentoComprobantes))
        Return New LogicaEncabezadoDocumentoComprobantes(CapaPersistenciaEncabezadoDocumentoComprobantes, CapaPersistenciaDetalleDocumentoComprobantes).ConsultarCadenaEgresos(filtro)
    End Function
End Class
