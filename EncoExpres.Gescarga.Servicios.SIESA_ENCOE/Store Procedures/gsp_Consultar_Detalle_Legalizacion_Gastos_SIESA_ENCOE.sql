CREATE PROCEDURE [dbo].[gsp_Consultar_Detalle_Legalizacion_Gastos_SIESA_ENCOE]            
(            
@par_EMPR_Codigo SMALLINT,              
@par_ELGC_Numero NUMERIC  
)            
AS             
BEGIN            
 SELECT PLUC.Codigo_Cuenta,            
 CLGC.Codigo AS Codigo_Concepto,           
 CLGC.Nombre AS Nombre_Concepto,        
 CASE WHEN CLGC.Operacion = 2 THEN DLGC.Valor            
 ELSE 0 END AS Valor_Debito,            
 CASE WHEN CLGC.Operacion = 1 THEN DLGC.Valor            
 ELSE 0 END AS Valor_Credito,            
 CLGC.Nombre + ' ' + ISNULL(DLGC.Observaciones,'') Observaciones,            
 PROV.Numero_Identificacion AS Identificacion_Proveedor,          
 DLGC.ENDC_Codigo,      
 COND.Numero_Identificacion AS Identificacion_Conductor,    
 ENPD.Numero_Documento AS ENPD_Numero_Documento,    
 VEHI.Placa    
           
 FROM Detalle_Legalizacion_Gastos_Conductor DLGC             
          
 INNER JOIN Conceptos_Legalizacion_Gastos_Conductor CLGC            
 ON CLGC.EMPR_Codigo = DLGC.EMPR_Codigo            
 AND CLGC.Codigo = DLGC.CLGC_Codigo            
            
 INNER JOIN Plan_Unico_Cuentas PLUC            
 ON PLUC.EMPR_Codigo = CLGC.EMPR_Codigo            
 AND PLUC.Codigo = CLGC.PLUC_Codigo            
            
 INNER JOIN Terceros PROV            
 ON DLGC.EMPR_Codigo = PROV.EMPR_Codigo          
 AND DLGC.TERC_Codigo_Proveedor = PROV.Codigo            
           
 INNER JOIN Encabezado_Planilla_Despachos ENPD            
 ON DLGC.EMPR_Codigo = ENPD.EMPR_Codigo            
 AND DLGC.ENPD_Numero = ENPD.Numero            
            
 INNER JOIN Vehiculos VEHI             
 ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo            
 AND ENPD.VEHI_Codigo = VEHI.Codigo    
            
 INNER JOIN Terceros COND     
 ON ENPD.EMPR_Codigo = COND.EMPR_Codigo              
 AND ENPD.TERC_Codigo_Conductor = COND.Codigo     
    
 WHERE DLGC.EMPR_Codigo = @par_EMPR_Codigo           
 AND DLGC.ELGC_Numero = @par_ELGC_Numero          
             
END 