﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Repositorio.ControlViajes
Imports EncoExpres.GesCarga.Entidades.Despachos

Namespace Paqueteria

    ''' <summary>
    ''' Clase <see cref="RepositorioPlanillaGuias"/>
    ''' </summary>
    Public NotInheritable Class RepositorioPlanillaGuias
        Inherits RepositorioBase(Of PlanillaGuias)
        Dim inserto As Boolean = True
        Public Overrides Function Consultar(filtro As PlanillaGuias) As IEnumerable(Of PlanillaGuias)
            Dim lista As New List(Of PlanillaGuias)
            Dim item As PlanillaGuias
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    Dim resultado As IDataReader
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    If filtro.ConsultaPLanillasenRuta > 0 Then
                        conexion.AgregarParametroSQL("@par_Tipo_Documento", filtro.TipoDocumento)
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_planilla_guias_paqueteria_en_ruta]")

                    Else

                        If Not IsNothing(filtro.Planilla) Then
                            If filtro.Planilla.Numero > 0 Then
                                conexion.AgregarParametroSQL("@par_Numero", filtro.Planilla.Numero)
                            End If
                            If filtro.FechaInicial > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                            End If
                            If filtro.FechaFinal > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                            End If
                            If Not IsNothing(filtro.Planilla.Ruta) Then
                                If filtro.Planilla.Ruta.Nombre <> "" Then
                                    conexion.AgregarParametroSQL("@par_Ruta", filtro.Planilla.Ruta.Nombre)
                                End If
                            End If
                            If Not IsNothing(filtro.Planilla.strVehiculo) Then
                                conexion.AgregarParametroSQL("@par_Vehiculo", filtro.Planilla.strVehiculo)
                            End If
                            If Not IsNothing(filtro.Planilla.strTenedor) Then
                                conexion.AgregarParametroSQL("@par_Tenedor", filtro.Planilla.strTenedor)
                            End If
                            If Not IsNothing(filtro.Planilla.strConductor) Then
                                conexion.AgregarParametroSQL("@par_Conductor", filtro.Planilla.strConductor)
                            End If
                            If Not IsNothing(filtro.Conductor) Then
                                If filtro.Conductor.Codigo > 0 Then
                                    conexion.AgregarParametroSQL("@par_Codigo_Conductor", filtro.Conductor.Codigo)
                                End If
                            End If
                            If Not IsNothing(filtro.Planilla.Estado) Then
                                If filtro.Planilla.Estado.Codigo > -1 Then
                                    If filtro.Planilla.Estado.Codigo = 2 Then 'ANULADO
                                        conexion.AgregarParametroSQL("@par_Anulado", 1)
                                    Else
                                        conexion.AgregarParametroSQL("@par_Estado", filtro.Planilla.Estado.Codigo)
                                        conexion.AgregarParametroSQL("@par_Anulado", 0)
                                    End If

                                End If
                            End If
                            If filtro.Planilla.NumeroGuia > 0 Then
                                conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", filtro.Planilla.NumeroGuia)
                            End If
                            If filtro.Planilla.NumeroManifiesto > 0 Then
                                conexion.AgregarParametroSQL("@par_Numero_Manifiesto", filtro.Planilla.NumeroManifiesto)
                            End If

                        End If
                        If Not IsNothing(filtro.Oficina) Then
                            If filtro.Oficina.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                            End If
                        End If
                        conexion.AgregarParametroSQL("@par_Tipo_Documento", filtro.TipoDocumento)

                        If Not IsNothing(filtro.Pagina) Then
                            If filtro.Pagina > 0 Then
                                conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                            End If
                        End If
                        If Not IsNothing(filtro.RegistrosPagina) Then
                            If filtro.RegistrosPagina > 0 Then
                                conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                            End If
                        End If

                        If Not IsNothing(filtro.Cliente) Then
                            If filtro.Cliente.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.Cliente.Codigo)
                            End If
                        End If
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_planilla_guias_paqueteria]")
                    End If


                    While resultado.Read
                        item = New PlanillaGuias(resultado)
                        lista.Add(item)
                    End While

                    conexion.CloseConnection()

                End Using
            End Using

            Return lista
        End Function


        Public Overrides Function Insertar(entidad As PlanillaGuias) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()


                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    'Inserta la Planilla'
                    If Not IsNothing(entidad.Planilla) Then

                        If entidad.Planilla.FechaHoraSalida > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Hora_Salida ", entidad.Planilla.FechaHoraSalida, SqlDbType.DateTime)
                        End If
                        If entidad.Planilla.Fecha > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha", entidad.Planilla.Fecha, SqlDbType.Date)
                        End If
                        If Not IsNothing(entidad.Planilla.Ruta) Then
                            conexion.AgregarParametroSQL("@par_RUTA_Codigo ", entidad.Planilla.Ruta.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.Vehiculo) Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo ", entidad.Planilla.Vehiculo.Codigo)
                            If Not IsNothing(entidad.Planilla.Vehiculo.Tenedor) Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor ", entidad.Planilla.Vehiculo.Tenedor.Codigo)
                            End If
                            If Not IsNothing(entidad.Planilla.Vehiculo.Conductor) Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor ", entidad.Planilla.Vehiculo.Conductor.Codigo)
                            End If
                        End If
                        If Not IsNothing(entidad.Planilla.Semirremolque) Then
                            conexion.AgregarParametroSQL("@par_SEMI_Codigo ", entidad.Planilla.Semirremolque.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Cantidad ", entidad.Planilla.Cantidad, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Peso ", entidad.Planilla.Peso, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador ", entidad.Planilla.ValorFleteTransportador, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Anticipo ", entidad.Planilla.ValorAnticipo, SqlDbType.Money)

                        conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.Planilla.ValorImpuestos, SqlDbType.Money)

                        conexion.AgregarParametroSQL("@par_Valor_Pagar_Transportador ", entidad.Planilla.ValorPagarTransportador, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente ", entidad.Planilla.ValorFleteCliente, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Seguro_Mercancia ", entidad.Planilla.ValorSeguroMercancia, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Otros_Cobros ", entidad.Planilla.ValorOtrosCobros, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Total_Credito ", entidad.Planilla.ValorTotalCredito, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Total_Contado ", entidad.Planilla.ValorTotalContado, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Total_Alcobro ", entidad.Planilla.ValorTotalAlcobro, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Planilla.Observaciones)
                        If Not IsNothing(entidad.Planilla.Estado) Then
                            conexion.AgregarParametroSQL("@par_Estado ", entidad.Planilla.Estado.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.TarifaTransportes) Then
                            conexion.AgregarParametroSQL("@par_TATC_Codigo_Compra", entidad.Planilla.TarifaTransportes.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.TipoTarifaTransportes) Then
                            conexion.AgregarParametroSQL("@par_TTTC_Codigo_Compra", entidad.Planilla.TipoTarifaTransportes.Codigo)
                        End If

                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo ", entidad.Oficina.Codigo)
                        conexion.AgregarParametroSQL("@par_Valor_Seguro_Poliza", entidad.Planilla.ValorSeguroPoliza)
                        conexion.AgregarParametroSQL("@par_Anticipo_Pagado_A", entidad.Planilla.AnticipoPagadoA)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento_Masivo", entidad.Planilla.RemesaPadre) ''Documento
                        conexion.AgregarParametroSQL("@par_ENRE_Numero_Masivo", entidad.Planilla.RemesaMasivo) ''Codigo
                        conexion.AgregarParametroSQL("@par_ValorContraEntrega", entidad.Planilla.ValorContraEntrega, SqlDbType.Money) ''Codigo

                        conexion.AgregarParametroSQL("@par_Valor_Bruto", entidad.Planilla.ValorBruto, SqlDbType.Money) ''Codigo
                        conexion.AgregarParametroSQL("@par_Porcentaje", entidad.Planilla.Porcentaje, SqlDbType.Money) ''Codigo
                        conexion.AgregarParametroSQL("@par_Valor_Descuentos", entidad.Planilla.ValorDescuentos, SqlDbType.Money) ''Codigo
                        conexion.AgregarParametroSQL("@par_Flete_Sugerido", entidad.Planilla.FleteSugerido, SqlDbType.Money) ''Codigo
                        conexion.AgregarParametroSQL("@par_Valor_Fondo_Ayuda_Mutua", entidad.Planilla.ValorFondoAyuda, SqlDbType.Money) ''Codigo
                        conexion.AgregarParametroSQL("@par_Valor_Neto_Pagar", entidad.Planilla.ValorNetoPagar, SqlDbType.Money) ''Codigo
                        conexion.AgregarParametroSQL("@par_Valor_Utilidad", entidad.Planilla.ValorUtilidad, SqlDbType.Money) ''Codigo
                        conexion.AgregarParametroSQL("@par_Valor_Estampilla", entidad.Planilla.ValorEstampilla, SqlDbType.Money) ''Codigo

                        conexion.AgregarParametroSQL("@par_Gastos_Agencia", entidad.Planilla.GastosAgencia, SqlDbType.Money) ''Codigo
                        conexion.AgregarParametroSQL("@par_Valor_Reexpedicion", entidad.Planilla.ValorReexpedicion, SqlDbType.Money) ''Codigo
                        conexion.AgregarParametroSQL("@par_Valor_Planilla_Adicional", entidad.Planilla.ValorPlanillaAdicional, SqlDbType.Money) ''Codigo
                        conexion.AgregarParametroSQL("@par_Calcular_Contra_Entrega", entidad.Planilla.CalcularContraentregas, SqlDbType.Money) ''Codigo

                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_planilla_paqueteria")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While

                    resultado.Close()
                    If entidad.Numero > 0 Then

                        For Each lista In entidad.Planilla.Detalles
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero)
                            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Planilla", entidad.TipoDocumento)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Numero)
                            If Not IsNothing(lista.RemesaPaqueteria) Then
                                If lista.RemesaPaqueteria.ValorReexpedicion > 0 Then
                                    conexion.AgregarParametroSQL("@par_ENRE_Valor_Reexpedicion", lista.RemesaPaqueteria.ValorReexpedicion, SqlDbType.Money)
                                End If
                            End If

                            conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", lista.Remesa.NumeroDocumento)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento_Masivo", entidad.Planilla.RemesaPadre)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero_Masivo", entidad.Planilla.RemesaMasivo)
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Planilla.Vehiculo.Codigo)
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Planilla.Vehiculo.Conductor.Codigo)
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                            conexion.AgregarParametroSQL("@par_Estado_Planilla", entidad.Planilla.Estado.Codigo)
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_planilla_paqueteria")

                            While resultado.Read
                                If Not (resultado.Item("Numero") > 0) Then
                                    inserto = False
                                    transaccion.Dispose()
                                    Return -2
                                    Exit For
                                End If
                            End While
                            resultado.Close()

                            If Not IsNothing(lista.ListaReexpedicionOficinas) Then
                                If lista.ListaReexpedicionOficinas.Count > 0 Then
                                    For Each Reex In lista.ListaReexpedicionOficinas
                                        If Reex.Modificar = 1 Then
                                            conexion.CleanParameters()
                                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                                            conexion.AgregarParametroSQL("@par_ENRE_Numero", Reex.CodigoRemesaPaqueteria)
                                            conexion.AgregarParametroSQL("@par_OFIC_Codigo", Reex.Oficina.Codigo)
                                            conexion.AgregarParametroSQL("@par_CATA_TRRP_Codigo", Reex.TipoReexpedicion.Codigo)
                                            conexion.AgregarParametroSQL("@par_Valor_Flete", Reex.Valor)
                                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_detalle_reexpedicion_remesas_paqueteria")
                                            resultado.Close()
                                        End If
                                    Next
                                End If
                            End If

                            'If inserto Then

                            '    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                            '    conexion.AgregarParametroSQL("@par_ESOS_Codigo", 0)
                            '    conexion.AgregarParametroSQL("@par_TIDO_Codigo", 150)
                            '    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                            '    conexion.AgregarParametroSQL("@par_Numero_Documento", 0)
                            '    conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Numero)
                            '    resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_documento_solicitud_Servicios")
                            '    resultado.Close()
                            'End If
                        Next
                        If inserto Then
                            If Not IsNothing(entidad.ListadoPlanillaRecolecciones) Then
                                Call BorrarListaPlanillaRecolecciones(entidad.CodigoEmpresa, entidad.Numero, conexion)
                                For Each ListadoPlanillaRecolecciones In entidad.ListadoPlanillaRecolecciones

                                    ListadoPlanillaRecolecciones.CodigoEmpresa = entidad.CodigoEmpresa
                                    ListadoPlanillaRecolecciones.NumeroPlanilla = entidad.Numero

                                    If Not InsertarListaPlanillaRecolecciones(ListadoPlanillaRecolecciones, conexion) Then
                                        Exit For
                                    End If
                                Next
                            End If
                            If Not IsNothing(entidad.Planilla.DetallesAuxiliares) Then
                                If entidad.Planilla.DetallesAuxiliares.Count > 0 Then
                                    For Each lista In entidad.Planilla.DetallesAuxiliares
                                        conexion.CleanParameters()
                                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Funcionario", lista.Funcionario.Codigo) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_Numero_Horas_Trabajadas", lista.NumeroHorasTrabajadas) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_Valor", lista.Valor, SqlDbType.Decimal) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_Observaciones", lista.Observaciones) 'SMALLINT,
                                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_auxiliares_planilla_despachos")
                                        While resultado.Read
                                            If Not (resultado.Item("Numero") > 0) Then
                                                inserto = False
                                                Exit For
                                            End If
                                        End While

                                        resultado.Close()
                                    Next
                                End If
                            End If
                            If Not IsNothing(entidad.Conductores) Then
                                If entidad.Conductores.Count > 0 Then
                                    For Each lista In entidad.Conductores
                                        conexion.CleanParameters()
                                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", lista.Codigo) 'SMALLINT,
                                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Detalle_Conductores_Planilla_Despachos")
                                        While resultado.Read
                                            If Not (resultado.Item("Numero") > 0) Then
                                                inserto = False
                                                Exit For
                                            End If
                                        End While

                                        resultado.Close()
                                    Next
                                End If
                            End If
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_totales_fletes_auxiliares")
                            resultado.Close()

                            If entidad.Planilla.Estado.Codigo = 1 And entidad.Planilla.ValorAnticipo > 0 Then
                                entidad.CuentaPorPagar.CodigoDocumentoOrigen = entidad.Numero
                                entidad.CuentaPorPagar.TipoDocumentoOrigen = entidad.TipoDocumento
                                entidad.CuentaPorPagar.Numero = entidad.NumeroDocumento
                                entidad.CuentaPorPagar.Observaciones = "PAGO ANTICIPO PLANILLA No. " + entidad.NumeroDocumento.ToString() + " VALOR $ " + entidad.Planilla.ValorAnticipo.ToString()
                                If entidad.Planilla.AutorizacionAnticipo > 0 Then
                                    Dim Autorizacion = New Autorizaciones
                                    Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                                    Autorizacion.PlanillaDespacho = New PlanillaDespachos With {.Codigo = entidad.Numero}
                                    Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18402}
                                    Autorizacion.CuentaPorPagar = entidad.CuentaPorPagar
                                    Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                                    Dim repAutorizacion = New RepositorioAutorizaciones()
                                    inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                                Else
                                    inserto = Generar_CxP_Anticipo(entidad.CuentaPorPagar, conexion)
                                End If
                            End If
                            If entidad.Planilla.Estado.Codigo = 1 And inserto Then
                                If entidad.Planilla.AutorizacionFlete > 0 Then
                                    Dim Autorizacion = New Autorizaciones
                                    Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                                    Autorizacion.PlanillaDespacho = New PlanillaDespachos With {.Codigo = entidad.Numero}
                                    Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18406}
                                    Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                                    Autorizacion.ValorFlete = entidad.Planilla.ValorFleteAutorizacion
                                    Autorizacion.ValorFleteReal = entidad.Planilla.ValorFleteTransportador
                                    Autorizacion.Observaciones = "Solicitud cambio flete en planilla " + entidad.NumeroDocumento.ToString() + " Flete estipulado: " + entidad.Planilla.strValorFleteTransportador + " Flete Solicitado: " + entidad.Planilla.strValorFleteAutorizacion
                                    Dim repAutorizacion = New RepositorioAutorizaciones()
                                    inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                                    'Else
                                    '    inserto = Generar_CxP_Anticipo(entidad.CuentaPorPagar, conexion)
                                End If
                            End If
                            'Impuestos
                            If Not IsNothing(entidad.Planilla.DetalleImpuesto) Then
                                If entidad.Planilla.DetalleImpuesto.Count > 0 Then
                                    For Each lista In entidad.Planilla.DetalleImpuesto
                                        conexion.CleanParameters()
                                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero)

                                        conexion.AgregarParametroSQL("@par_ENIM_Codigo", lista.CodigoImpuesto)
                                        conexion.AgregarParametroSQL("@par_Valor_Tarifa", lista.ValorTarifa, SqlDbType.Decimal)
                                        conexion.AgregarParametroSQL("@par_Valor_Base", lista.ValorBase, SqlDbType.Money)
                                        conexion.AgregarParametroSQL("@par_Valor_Impuesto", lista.ValorImpuesto, SqlDbType.Money)

                                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_impuestos_planilla_despachos")

                                        While resultado.Read
                                            If Not (resultado.Item("Numero") > 0) Then
                                                inserto = False
                                                Exit For
                                            End If
                                        End While

                                        resultado.Close()
                                    Next
                                End If
                            End If
                            If entidad.Numero > 0 Then
                                transaccion.Complete()
                            End If
                        Else
                            transaccion.Dispose()
                        End If
                    Else
                        transaccion.Dispose()
                    End If
                End Using
            End Using
            Return entidad.NumeroDocumento

        End Function

        Private Function ActualizarDespachoSolicitudServicio(entidad As PlanillaDespachos, contextoConexion As Conexion.DataBaseFactory) As Boolean

            Dim Actualizo As Boolean

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ESOS_Codigo", 0)
            contextoConexion.AgregarParametroSQL("@par_TIDO_Codigo", 150)
            contextoConexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_Numero_Documento", 0)
            contextoConexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Vehiculo.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("gsp_actualizar_documento_solicitud_Servicios")

            While resultado.Read
                Actualizo = IIf(resultado.Item("Cantidad") > Cero, True, False)
            End While

            resultado.Close()

            Return Actualizo

        End Function


        Public Function InsertarListaPlanillaRecolecciones(entidad As DetallePlanillaRecolecciones, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Numero", entidad.NumeroPlanilla)
            conexion.AgregarParametroSQL("@par_EREC_Numero", entidad.Numero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_lista_planilla_recolecciones]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function BorrarListaPlanillaRecolecciones(CodiEmpresa As Short, NumePlanilla As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Codigo", NumePlanilla)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_lista_planilla_recolecciones]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function
        Public Overrides Function Modificar(entidad As PlanillaGuias) As Long
            Dim inserto As Boolean = True
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()


                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                conexion.AgregarParametroSQL("@par_Numero ", entidad.Planilla.Numero)
                'Inserta la Planilla'
                If Not IsNothing(entidad.Planilla) Then

                    If entidad.Planilla.FechaHoraSalida > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Salida ", entidad.Planilla.FechaHoraSalida, SqlDbType.DateTime)
                    End If
                    If entidad.Planilla.Fecha > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha", entidad.Planilla.Fecha, SqlDbType.Date)
                    End If
                    If Not IsNothing(entidad.Planilla.Ruta) Then
                        conexion.AgregarParametroSQL("@par_RUTA_Codigo ", entidad.Planilla.Ruta.Codigo)
                    End If
                    If Not IsNothing(entidad.Planilla.Vehiculo) Then
                        conexion.AgregarParametroSQL("@par_VEHI_Codigo ", entidad.Planilla.Vehiculo.Codigo)
                        If Not IsNothing(entidad.Planilla.Vehiculo.Tenedor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor ", entidad.Planilla.Vehiculo.Tenedor.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.Vehiculo.Conductor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor ", entidad.Planilla.Vehiculo.Conductor.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Planilla.Semirremolque) Then
                        conexion.AgregarParametroSQL("@par_SEMI_Codigo ", entidad.Planilla.Semirremolque.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_Cantidad ", entidad.Planilla.Cantidad, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso ", entidad.Planilla.Peso, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador ", entidad.Planilla.ValorFleteTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipo ", entidad.Planilla.ValorAnticipo, SqlDbType.Money)

                    conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.Planilla.ValorImpuestos, SqlDbType.Money)

                    conexion.AgregarParametroSQL("@par_Valor_Pagar_Transportador ", entidad.Planilla.ValorPagarTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente ", entidad.Planilla.ValorFleteCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Seguro_Mercancia ", entidad.Planilla.ValorSeguroMercancia, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Otros_Cobros ", entidad.Planilla.ValorOtrosCobros, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Credito ", entidad.Planilla.ValorTotalCredito, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Contado ", entidad.Planilla.ValorTotalContado, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Total_Alcobro ", entidad.Planilla.ValorTotalAlcobro, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Planilla.Observaciones)
                    If Not IsNothing(entidad.Planilla.Estado) Then
                        conexion.AgregarParametroSQL("@par_Estado ", entidad.Planilla.Estado.Codigo)
                    End If
                    If Not IsNothing(entidad.Planilla.TarifaTransportes) Then
                        conexion.AgregarParametroSQL("@par_TATC_Codigo ", entidad.Planilla.TarifaTransportes.Codigo)
                    End If
                    If Not IsNothing(entidad.Planilla.TipoTarifaTransportes) Then
                        conexion.AgregarParametroSQL("@par_TTTC_Codigo ", entidad.Planilla.TipoTarifaTransportes.Codigo)
                    End If
                    If Not IsNothing(entidad.Planilla.DetalleTarifarioCompra) Then
                        conexion.AgregarParametroSQL("@par_DTCC_Codigo ", entidad.Planilla.DetalleTarifarioCompra.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Anticipo_Pagado_A", entidad.Planilla.AnticipoPagadoA)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento_Masivo", entidad.Planilla.RemesaPadre) ''Documento
                    conexion.AgregarParametroSQL("@par_ENRE_Numero_Masivo", entidad.Planilla.RemesaMasivo) ''Codigo
                    conexion.AgregarParametroSQL("@par_ValorContraEntrega", entidad.Planilla.ValorContraEntrega, SqlDbType.Money) ''Codigo


                    conexion.AgregarParametroSQL("@par_Valor_Bruto", entidad.Planilla.ValorBruto, SqlDbType.Money) ''Codigo
                    conexion.AgregarParametroSQL("@par_Porcentaje", entidad.Planilla.Porcentaje, SqlDbType.Money) ''Codigo
                    conexion.AgregarParametroSQL("@par_Valor_Descuentos", entidad.Planilla.ValorDescuentos, SqlDbType.Money) ''Codigo
                    conexion.AgregarParametroSQL("@par_Flete_Sugerido", entidad.Planilla.FleteSugerido, SqlDbType.Money) ''Codigo
                    conexion.AgregarParametroSQL("@par_Valor_Fondo_Ayuda_Mutua", entidad.Planilla.ValorFondoAyuda, SqlDbType.Money) ''Codigo
                    conexion.AgregarParametroSQL("@par_Valor_Neto_Pagar", entidad.Planilla.ValorNetoPagar, SqlDbType.Money) ''Codigo
                    conexion.AgregarParametroSQL("@par_Valor_Utilidad", entidad.Planilla.ValorUtilidad, SqlDbType.Money) ''Codigo
                    conexion.AgregarParametroSQL("@par_Valor_Estampilla", entidad.Planilla.ValorEstampilla, SqlDbType.Money) ''Codigo

                    conexion.AgregarParametroSQL("@par_Gastos_Agencia", entidad.Planilla.GastosAgencia, SqlDbType.Money) ''Codigo
                    conexion.AgregarParametroSQL("@par_Valor_Reexpedicion", entidad.Planilla.ValorReexpedicion, SqlDbType.Money) ''Codigo
                    conexion.AgregarParametroSQL("@par_Valor_Planilla_Adicional", entidad.Planilla.ValorPlanillaAdicional, SqlDbType.Money) ''Codigo
                    conexion.AgregarParametroSQL("@par_Calcular_Contra_Entrega", entidad.Planilla.CalcularContraentregas, SqlDbType.Money) ''Codigo

                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_planilla_despachos")

                While resultado.Read
                    entidad.Numero = resultado.Item("Numero").ToString()
                    entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                End While

                resultado.Close()

                For Each lista In entidad.Planilla.Detalles
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Planilla.Numero)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo_Planilla", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Numero)
                    If Not IsNothing(lista.RemesaPaqueteria) Then
                        If lista.RemesaPaqueteria.ValorReexpedicion > 0 Then
                            conexion.AgregarParametroSQL("@par_ENRE_Valor_Reexpedicion", lista.RemesaPaqueteria.ValorReexpedicion, SqlDbType.Money)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", lista.Remesa.NumeroDocumento)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento_Masivo", entidad.Planilla.RemesaPadre)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero_Masivo", entidad.Planilla.RemesaMasivo)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Planilla.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Planilla.Vehiculo.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado_Planilla", entidad.Planilla.Estado.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_planilla_paqueteria")

                    While resultado.Read
                        If Not (resultado.Item("Numero") > 0) Then
                            inserto = False
                            Exit For
                        End If
                    End While

                    resultado.Close()

                    If Not IsNothing(lista.ListaReexpedicionOficinas) Then
                        If lista.ListaReexpedicionOficinas.Count > 0 Then
                            For Each Reex In lista.ListaReexpedicionOficinas
                                If Reex.Modificar = 1 Then
                                    conexion.CleanParameters()
                                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                                    conexion.AgregarParametroSQL("@par_ENRE_Numero", Reex.CodigoRemesaPaqueteria)
                                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", Reex.Oficina.Codigo)
                                    conexion.AgregarParametroSQL("@par_CATA_TRRP_Codigo", Reex.TipoReexpedicion.Codigo)
                                    conexion.AgregarParametroSQL("@par_Valor_Flete", Reex.Valor)
                                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_detalle_reexpedicion_remesas_paqueteria")
                                    resultado.Close()
                                End If
                            Next
                        End If
                    End If
                Next
                If Not IsNothing(entidad.Planilla.DetallesAuxiliares) Then
                    If entidad.Planilla.DetallesAuxiliares.Count > 0 Then
                        For Each lista In entidad.Planilla.DetallesAuxiliares
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Planilla.Numero) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Funcionario", lista.Funcionario.Codigo) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_Numero_Horas_Trabajadas", lista.NumeroHorasTrabajadas) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_Valor", lista.Valor, SqlDbType.Decimal) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_Observaciones", lista.Observaciones) 'SMALLINT,
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_auxiliares_planilla_despachos")
                            While resultado.Read
                                If Not (resultado.Item("Numero") > 0) Then
                                    inserto = False
                                    Exit For
                                End If
                            End While

                            resultado.Close()
                        Next
                    End If
                End If
                If Not IsNothing(entidad.Conductores) Then
                    If entidad.Conductores.Count > 0 Then
                        For Each lista In entidad.Conductores
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", lista.Codigo) 'SMALLINT,
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Detalle_Conductores_Planilla_Despachos")
                            While resultado.Read
                                If Not (resultado.Item("Numero") > 0) Then
                                    inserto = False
                                    Exit For
                                End If
                            End While

                            resultado.Close()
                        Next
                    End If
                End If
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_totales_fletes_auxiliares")
                resultado.Close()
                If Not IsNothing(entidad.ListadoPlanillaRecoleccionesNoCkeck) Then
                    For Each listarecolecciones In entidad.ListadoPlanillaRecoleccionesNoCkeck
                        inserto = LiberarRecolecciones(listarecolecciones, entidad.Planilla.Numero, conexion)
                    Next
                End If

                If Not IsNothing(entidad.ListadoPlanillaRecolecciones) Then
                    Call BorrarListaPlanillaRecolecciones(entidad.CodigoEmpresa, entidad.Planilla.Numero, conexion)
                    For Each ListadoPlanillaRecolecciones In entidad.ListadoPlanillaRecolecciones

                        ListadoPlanillaRecolecciones.CodigoEmpresa = entidad.CodigoEmpresa
                        ListadoPlanillaRecolecciones.NumeroPlanilla = entidad.Planilla.Numero

                        If Not InsertarListaPlanillaRecolecciones(ListadoPlanillaRecolecciones, conexion) Then
                            Exit For
                        End If
                    Next
                End If
                If Not IsNothing(entidad.Planilla.DetalleImpuesto) Then
                    If entidad.Planilla.DetalleImpuesto.Count > 0 Then
                        For Each lista In entidad.Planilla.DetalleImpuesto
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Planilla.Numero)

                            conexion.AgregarParametroSQL("@par_ENIM_Codigo", lista.CodigoImpuesto)
                            conexion.AgregarParametroSQL("@par_Valor_Tarifa", lista.ValorTarifa, SqlDbType.Decimal)
                            conexion.AgregarParametroSQL("@par_Valor_Base", lista.ValorBase, SqlDbType.Money)
                            conexion.AgregarParametroSQL("@par_Valor_Impuesto", lista.ValorImpuesto, SqlDbType.Money)

                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_impuestos_planilla_despachos")

                            While resultado.Read
                                If Not (resultado.Item("Numero") > 0) Then
                                    inserto = False
                                    Exit For
                                End If
                            End While

                            resultado.Close()
                        Next
                    End If
                End If
                If entidad.Planilla.Estado.Codigo = 1 And entidad.Planilla.ValorAnticipo > 0 Then
                    entidad.CuentaPorPagar.Numero = entidad.Planilla.NumeroDocumento
                    entidad.CuentaPorPagar.CodigoDocumentoOrigen = entidad.Planilla.Numero
                    entidad.CuentaPorPagar.TipoDocumentoOrigen = entidad.TipoDocumento
                    entidad.CuentaPorPagar.Observaciones = "PAGO ANTICIPO PLANILLA No. " + entidad.Planilla.NumeroDocumento.ToString() + " VALOR $ " + entidad.Planilla.ValorAnticipo.ToString()
                    If entidad.Planilla.AutorizacionAnticipo > 0 Then
                        Dim Autorizacion = New Autorizaciones
                        Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                        Autorizacion.PlanillaDespacho = New PlanillaDespachos With {.Codigo = entidad.Planilla.Numero}
                        Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18402}
                        Autorizacion.CuentaPorPagar = entidad.CuentaPorPagar
                        Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                        Dim repAutorizacion = New RepositorioAutorizaciones()
                        inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                    Else
                        inserto = Generar_CxP_Anticipo(entidad.CuentaPorPagar, conexion)
                    End If
                End If
            End Using
            Return entidad.NumeroDocumento
        End Function
        Public Function LiberarRecolecciones(entidad As DetallePlanillaRecolecciones, Numero As Integer, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Numero", entidad.NumeroPlanilla)
            conexion.AgregarParametroSQL("@par_EREC_Numero", entidad.Numero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_liberar_recolecciones")

            While resultado.Read

                entidad.Numero = resultado.Item("CantidadRegistros").ToString()
            End While
            resultado.Close()

            If entidad.Numero.Equals(Cero) Then
                inserto = False

            End If

            Return inserto

        End Function

        Public Overrides Function Obtener(filtro As PlanillaGuias) As PlanillaGuias
            Dim item As New PlanillaGuias
            Dim Doc As New Documentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                'Obtiene planilla encabezado
                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If filtro.TipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                End If
                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_planilla_guia_paqueteria]")

                While resultado.Read
                    item = New PlanillaGuias(resultado)
                    filtro.Numero = item.Planilla.Numero
                End While
                conexion.CloseConnection()
                resultado.Close()
                'Consulta las guias
                If item.TipoDocumento = 130 Or item.TipoDocumento = 210 Or item.TipoDocumento = 205 Then

                    Dim Detalleguia As RemesaPaqueteria
                    Dim Detalle As DetallePlanillas
                    Dim ListaDetalle As New List(Of DetallePlanillas)
                    conexion.CleanParameters()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", item.TipoDocumento)
                    If Not IsNothing(filtro.Cliente) Then
                        If filtro.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.Cliente.Codigo)
                        End If
                    End If

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_guias_paqueteria_planilla]")

                    While resultado.Read
                        Detalleguia = New RemesaPaqueteria(resultado)
                        If Detalleguia.Reexpedicion = 1 Then
                            Detalle = New DetallePlanillas With {.Remesa = Detalleguia.Remesa, .RemesaPaqueteria = Detalleguia, .ListaReexpedicionOficinas = ConsultarDetalleReexpedicionConsulta(filtro.CodigoEmpresa, Detalleguia.Remesa.Numero)}
                        Else
                            Detalle = New DetallePlanillas With {.Remesa = Detalleguia.Remesa, .RemesaPaqueteria = Detalleguia}
                            Detalle.Remesa.PesoVolumetricoCliente = Detalleguia.PesoVolumetrico
                        End If
                        ListaDetalle.Add(Detalle)
                    End While
                    item.Planilla.Detalles = ListaDetalle
                    conexion.CloseConnection()
                    resultado.Close()
                ElseIf item.TipoDocumento = 150 Then
                    Dim DetalleRemesa As Remesas
                    Dim Detalle As DetallePlanillas
                    Dim ListaDetalle As New List(Of DetallePlanillas)
                    conexion.CleanParameters()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_remesas_planilla]")

                    While resultado.Read
                        DetalleRemesa = New Remesas(resultado)
                        'DetalleRemesa.
                        Detalle = New DetallePlanillas With {.Remesa = DetalleRemesa}

                        ListaDetalle.Add(Detalle)
                    End While
                    item.Planilla.Detalles = ListaDetalle
                    conexion.CloseConnection()
                    resultado.Close()

                End If
                If filtro.Numero > 0 Then
                    Dim lista As New List(Of DetallePlanillaRecolecciones)
                    Dim Recoleccion As DetallePlanillaRecolecciones
                    conexion.CleanParameters()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENPR_Codigo", filtro.Numero)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_planilla_recolecciones]")

                    While resultado.Read
                        Recoleccion = New DetallePlanillaRecolecciones(resultado)
                        lista.Add(Recoleccion)
                    End While
                    item.ListadoPlanillaRecolecciones = lista
                    conexion.CloseConnection()
                    resultado.Close()

                End If
                'Consulta detalles auxiliares
                Dim DetalleAuxuliares As DetalleAuxiliaresPlanillas
                Dim ListaDetalleAuxiliares As New List(Of DetalleAuxiliaresPlanillas)
                conexion.CleanParameters()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_Auxiliares_planilla_guia_paqueteria]")


                While resultado.Read
                    DetalleAuxuliares = New DetalleAuxiliaresPlanillas(resultado)
                    ListaDetalleAuxiliares.Add(DetalleAuxuliares)
                End While
                If ListaDetalleAuxiliares.Count() > 0 Then
                    item.Planilla.DetallesAuxiliares = ListaDetalleAuxiliares
                End If


                Dim Conductores As Terceros
                Dim ListaConductores As New List(Of Terceros)
                conexion.CleanParameters()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_conductores_planilla_guia_paqueteria]")
                While resultado.Read
                    Conductores = New Terceros(resultado)
                    ListaConductores.Add(Conductores)
                End While
                If ListaConductores.Count() > 0 Then
                    item.Conductores = ListaConductores
                End If

                ''Consulta detalles Impuestos
                Dim DetalleImpuestos As DetalleImpuestosPlanillas
                Dim ListaDetalleImpuestos As New List(Of DetalleImpuestosPlanillas)
                conexion.CleanParameters()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_impuestos_planilla_despachos]")

                While resultado.Read
                    DetalleImpuestos = New DetalleImpuestosPlanillas(resultado)
                    ListaDetalleImpuestos.Add(DetalleImpuestos)
                End While
                If ListaDetalleImpuestos.Count() > 0 Then
                    item.Planilla.DetalleImpuesto = ListaDetalleImpuestos
                End If


            End Using

            Return item
        End Function

        Public Function ConsultarDetalleReexpedicionConsulta(CodiEmpresa As Short, CodRemesa As Integer) As IEnumerable(Of ReexpedicionRemesasPaqueteria)

            Dim lista As New List(Of ReexpedicionRemesasPaqueteria)
            Dim item As ReexpedicionRemesasPaqueteria
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", CodRemesa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_reexpedicion_remesas_paqueteria]")

                While resultado.Read
                    item = New ReexpedicionRemesasPaqueteria(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function

        Public Function ConsultarListaPlanillaRecolecciones(CodiEmpresa As Short, NumePlanilla As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of DetallePlanillaRecolecciones)
            Dim lista As New List(Of DetallePlanillaRecolecciones)
            Dim item As DetallePlanillaRecolecciones
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Codigo", NumePlanilla)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_planilla_recolecciones]")

            While resultado.Read
                item = New DetallePlanillaRecolecciones(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function
        Public Function Anular(entidad As PlanillaGuias) As PlanillaGuias
            Dim anulo As Boolean = False
            Dim item As New PlanillaGuias
            Dim itemDocumento As EncabezadoDocumentos
            Dim lista As New List(Of EncabezadoDocumentos)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Anular_Planilla_Guias_Paqueteria")

                While resultado.Read
                    itemDocumento = New EncabezadoDocumentos With {
                     .Cumplido = New Cumplido With {.Numero = Read(resultado, "ECPD_Numero_Documento")},
                     .Liquidacion = New Liquidacion With {.Numero = Read(resultado, "ELPD_Numero_Documento")},
                     .Comprobante = New EncabezadoDocumentoComprobantes With {.Numero = Read(resultado, "ENDC_Numero_Documento")}
                     }
                    lista.Add(itemDocumento)
                    item.Anulado = Read(resultado, "Anulo")

                End While
                item.DocumentosRelacionados = lista

            End Using

            Return item
        End Function
        Public Function Generar_CxP_Anticipo(entidad As EncabezadoDocumentoCuentas, conexion As DataBaseFactory) As Boolean
            Generar_CxP_Anticipo = False

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
            conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
            conexion.AgregarParametroSQL("@par_Codigo_Documento_Origen", entidad.CodigoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.Numero)
            conexion.AgregarParametroSQL("@par_Fecha_Documento_Origen", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Fecha_Vence_Documento_Origen", DateAdd(DateInterval.Day, 30, entidad.Fecha), SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPuc.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            conexion.AgregarParametroSQL("@par_Abono", entidad.Abono)
            conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)
            conexion.AgregarParametroSQL("@par_Fecha_Cancelacion_Pago", entidad.FechaCancelacionPago, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Aprobado", entidad.Aprobado)
            conexion.AgregarParametroSQL("@par_AplicaPSL", entidad.AplicaPSL)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", entidad.TipoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Aprobo", entidad.UsuarioAprobo.Codigo)
            conexion.AgregarParametroSQL("@par_Fecha_Aprobo", entidad.FechaAprobo, SqlDbType.DateTime)
            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_cuentas")

            While resultado.Read
                Generar_CxP_Anticipo = IIf(resultado.Item("Codigo") > 0, True, False)
            End While
            resultado.Close()

            Return Generar_CxP_Anticipo
        End Function

        Public Function EliminarGuia(entidad As PlanillaGuias) As Boolean
            Dim Eliminar As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                Dim resultado As IDataReader

                For Each lista In entidad.Planilla.Detalles
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Numero)
                    If entidad.TipoDocumento = 205 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_guia_planilla_recoleccion")
                    Else
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_guia_planilla_paqueteria")
                    End If
                    If resultado.RecordsAffected > 0 Then
                        Eliminar = True
                    End If
                    resultado.Close()
                Next

            End Using

            Return Eliminar
        End Function

        Public Function EliminarRecoleccion(entidad As PlanillaGuias) As Boolean
            Dim Eliminar As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                Dim resultado As IDataReader

                For Each lista In entidad.ListadoPlanillaRecolecciones
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENPR_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_EREC_Numero", lista.Numero)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_recoleccion_planilla_recoleccion")

                    If resultado.RecordsAffected > 0 Then
                        Eliminar = True
                    End If
                    resultado.Close()
                Next

            End Using

            Return Eliminar
        End Function

    End Class

End Namespace