﻿PRINT 'gsp_generar_consecutivo'
GO
DROP PROCEDURE gsp_generar_consecutivo
GO
CREATE PROCEDURE gsp_generar_consecutivo (  
@par_EMPR_Codigo numeric,  
@par_TIDO_Codigo numeric,  
@par_OFIC_Codigo  numeric,  
@par_Consecutivo  numeric OUTPUT  
)  
AS   
BEGIN   
 DECLARE @intTipoNume smallint  
  
 SET @intTipoNume = (SELECT CATA_TIGN_Codigo   
 FROM Tipo_Documentos  
 WHERE EMPR_Codigo = @par_EMPR_Codigo  
 AND Codigo = @par_TIDO_Codigo)  
  
 IF @intTipoNume = 1101 --Consecutivo por Empresa  
 BEGIN  
  SET @par_Consecutivo = (SELECT Consecutivo + 1   
  FROM Tipo_Documentos  
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
  AND Codigo = @par_TIDO_Codigo)  
    
  UPDATE Tipo_Documentos SET Consecutivo = Consecutivo + 1   
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
  AND Codigo = @par_TIDO_Codigo   
 END  
  
 ELSE IF @intTipoNume = 1102 --Consecutivo por Oficina  
 BEGIN  
  SET @par_Consecutivo = (SELECT Consecutivo + 1   
  FROM Consecutivo_Documento_Oficinas  
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
  AND TIDO_Codigo = @par_TIDO_Codigo   
  AND OFIC_Codigo = @par_OFIC_Codigo)  
  
  UPDATE Consecutivo_Documento_Oficinas SET Consecutivo = Consecutivo + 1   
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
  AND TIDO_Codigo = @par_TIDO_Codigo   
  AND OFIC_Codigo = @par_OFIC_Codigo  
 END  
END  
  GO
  