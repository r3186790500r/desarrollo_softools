﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Tesoreria


Namespace Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleDocumentoCuentaComprobantes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleDocumentoCuentaComprobantes
        Inherits RepositorioBase(Of DetalleDocumentoCuentaComprobantes)

        Public Overrides Function Consultar(filtro As DetalleDocumentoCuentaComprobantes) As IEnumerable(Of DetalleDocumentoCuentaComprobantes)
            Dim lista As New List(Of DetalleDocumentoCuentaComprobantes)
            Dim item As DetalleDocumentoCuentaComprobantes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_ECCO_Codigo", filtro.Codigo)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_concepto_contables]")

                While resultado.Read
                    item = New DetalleDocumentoCuentaComprobantes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As DetalleDocumentoCuentaComprobantes) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Modificar(entidad As DetalleDocumentoCuentaComprobantes) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As DetalleDocumentoCuentaComprobantes) As DetalleDocumentoCuentaComprobantes
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleDocumentoCuentaComprobantes) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace