﻿EncoExpresApp.controller("ConsultarImpuestosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'ImpuestosFactory', 'PlanUnicoCuentasFactory', 'ValorCatalogosFactory', 'CiudadesFactory', 'DepartamentosFactory', 'blockUI', 'blockUIConfig',
    function ($scope, $routeParams, $timeout, $linq, ImpuestosFactory, PlanUnicoCuentasFactory, ValorCatalogosFactory, CiudadesFactory, DepartamentosFactory, blockUI, blockUIConfig) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Impuestos' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.ResultadoSinRegistrosDetalles = 'No hay datos para mostrar';
        $scope.MensajesError = [];
        var filtros = {};
        $scope.ListadoEstados = [];
        $scope.ListaCiudad = [];
        $scope.ListadoImpuestosCiudades = [];
        $scope.ListadoImpuestosDepartamento = [];
        $scope.listadoAuxiliarDepartamento = [];
        $scope.listadoAuxiliarCiudad = [];
        $scope.listadoInicialCiud = [];
        $scope.listadoInicialDepa = [];
        $scope.ModeloNombre = "";
        $scope.MensajesErrorCiudad = [];
        $scope.MensajesErrorDepartamento = [];
        $scope.ListaDepartamento = [];
        $scope.CodigoCiudad = 0;
        $scope.CodigoDepartamento = 0;
        $scope.MostrarMensajeError = false
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.Codigo = 0;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_IMPUESTOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar


        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ListadoEstadosCiudades = [
            { Nombre: '(SELECCIONE ITEM)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 },
        ];
        $scope.ListadoEstadosDepartamentos = [
            { Nombre: '(SELECCIONE ITEM)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 },
        ];
        $scope.ListadoEstados2 = [
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 },
        ];

        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.ModalEstadoDepartamento = $linq.Enumerable().From($scope.ListadoEstadosDepartamentos).First('$.Codigo == 1');
        $scope.ModalEstadoCiudades = $linq.Enumerable().From($scope.ListadoEstadosCiudades).First('$.Codigo == 1');

       // Find();

        if ($routeParams.Codigo !== undefined) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        /*Metodo buscar*/
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarImpuesto';
            }
        };

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO_LUGAR } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRecaudo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoRecaudo.push({ Codigo: 0, Nombre: '(TODAS)' });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoTipoRecaudo.push(response.data.Datos[i]);

                        }
                        $scope.ModalRecaudo = $linq.Enumerable().From($scope.ListadoTipoRecaudo).First('$.Codigo == ' + CERO);
                    }
                    else {
                        $scope.ListadoTipoRecaudo = []
                    }
                }
            }, function (response) {
            });

        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListaCiudad = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCiudad)
                }
            }
            return $scope.ListaCiudad
        }

        DepartamentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListaDepartamento = response.data.Datos;
                        if ($scope.CodigoCiudadCargue != undefined && $scope.CodigoCiudadCargue != '' && $scope.CodigoCiudadCargue != 0 && $scope.CodigoCiudadCargue != null) {
                            if ($scope.CodigoCiudadCargue > 0) {
                                $scope.ModeloDepartamento = $linq.Enumerable().From($scope.ListaDepartamento).First('$.Codigo ==' + $scope.CodigoCiudadCargue);
                            } else {
                                $scope.ModeloDepartamento = $linq.Enumerable().From($scope.ListaDepartamento).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.ModeloDepartamento = $linq.Enumerable().From($scope.ListaDepartamento).First('$.Codigo ==0');
                        }
                    }
                    else {
                        $scope.ListaDepartamento = [];
                        $scope.ModeloDepartamento = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.AgregarDepartamento = function () {
            var continuar = true
            $scope.MensajesErrorDepartamento = [];
            if ($scope.ModeloDepartamento.Codigo == 0 || isNaN($scope.ModeloDepartamento.Codigo)) {
                continuar = false;
                $scope.MensajesErrorDepartamento.push('Ingrese un Departamento')
            }
            if ($scope.ModeloCuentaDepartamento.Codigo == 0 || isNaN($scope.ModeloCuentaDepartamento.Codigo)) {
                continuar = false;
                $scope.MensajesErrorDepartamento.push('Ingrese una cuenta')
            }
            if ($scope.ModeloValorBaseDepartamento < 0 || isNaN($scope.ModeloValorBaseDepartamento)) {
                continuar = false;
                $scope.MensajesErrorDepartamento.push('Ingrese un valor base')
            }
            if ($scope.ModeloValorTarifaDepartamento <= 0 || isNaN($scope.ModeloValorTarifaDepartamento)) {
                continuar = false;
                $scope.MensajesErrorDepartamento.push('Ingrese un valor tarifa')
            }
            if ($scope.ModalEstadoDepartamento.Codigo == -1 || isNaN($scope.ModalEstadoDepartamento.Codigo)) {
                continuar = false;
                $scope.MensajesErrorDepartamento.push('Ingrese un Estado')
            }
            if (continuar == true) {
                if ($scope.ModeloDepartamento != '' && $scope.ModeloDepartamento != undefined && $scope.ModeloDepartamento != null && $scope.ModeloDepartamento.Codigo != 0 && $scope.ModeloDepartamento.Codigo != undefined) {
                    var concidencias = 0
                    if ($scope.ListadoImpuestosDepartamento.length > 0) {
                        for (var i = 0; i < $scope.ListadoImpuestosDepartamento.length; i++) {
                            if ($scope.ListadoImpuestosDepartamento[i].Codigo == $scope.ModeloDepartamento.Codigo) {
                                concidencias++
                                break;
                            }
                        }
                        if (concidencias > 0) {
                            ShowError('El Departamento ya fue ingresada')
                            $scope.ModeloFuncionario = '';
                        } else {
                            $scope.ListadoImpuestosDepartamento.push({ Codigo: $scope.ModeloDepartamento.Codigo, Nombre: $scope.ModeloDepartamento.Nombre, CuentaPUC: { Codigo: $scope.ModeloCuentaDepartamento.Codigo, CodigoNombreCuenta: $scope.ModeloCuentaDepartamento.CodigoNombreCuenta }, ValorTarifa: $scope.ModeloValorTarifaDepartamento, ValorBase: $scope.ModeloValorBaseDepartamento, Estado: $scope.ModalEstadoDepartamento });
                            $scope.totalRegistrosDepartamentos = $scope.totalRegistrosDepartamentos + 1;
                            $scope.ModeloFuncionario = '';
                            $scope.ModeloDepartamento = '';
                            $scope.ModeloCuentaDepartamento = $linq.Enumerable().From($scope.ListaPucCiudades).First('$.Codigo == 0');
                            $scope.ModeloValorBaseDepartamento = 0;
                            $scope.ModeloValorTarifaDepartamento = 0;
                            $scope.ModalEstadoDepartamento = $linq.Enumerable().From($scope.ListadoEstadosDepartamentos).First('$.Codigo == 1');
                        }
                    } else {
                        $scope.ListadoImpuestosDepartamento.push({ Codigo: $scope.ModeloDepartamento.Codigo, Nombre: $scope.ModeloDepartamento.Nombre, CuentaPUC: { Codigo: $scope.ModeloCuentaDepartamento.Codigo, CodigoNombreCuenta: $scope.ModeloCuentaDepartamento.CodigoNombreCuenta }, ValorTarifa: $scope.ModeloValorTarifaDepartamento, ValorBase: $scope.ModeloValorBaseDepartamento, Estado: $scope.ModalEstadoDepartamento });
                        $scope.totalRegistrosDepartamentos = $scope.totalRegistrosDepartamentos + 1;
                        $scope.ModeloFuncionario = '';
                        $scope.ModeloDepartamento = '';
                        $scope.ModeloCuentaDepartamento = $linq.Enumerable().From($scope.ListaPucCiudades).First('$.Codigo == 0');
                        $scope.ModeloValorBaseDepartamento = 0;
                        $scope.ModeloValorTarifaDepartamento = 0;
                        $scope.ModalEstadoDepartamento = $linq.Enumerable().From($scope.ListadoEstadosDepartamentos).First('$.Codigo == 1');
                    }
                }
                else {
                    ShowError('Debe ingresar un Departamento Valido');
                }
            }
        }

        $scope.AgregarCiudades = function () {
            var continuar = true
            $scope.MensajesErrorCiudad = [];
            if ($scope.ModeloCiudades === undefined || $scope.ModeloCiudades.Codigo == 0 || isNaN($scope.ModeloCiudades.Codigo)) {
                continuar = false;
                $scope.MensajesErrorCiudad.push('Ingrese una Ciudad')
            }
            if ($scope.ModeloCuentaCiudades === undefined || $scope.ModeloCuentaCiudades.Codigo == 0 || isNaN($scope.ModeloCuentaCiudades.Codigo)) {
                continuar = false;
                $scope.MensajesErrorCiudad.push('Ingrese una cuenta')
            }
            if ($scope.ModeloValorBaseCiudades < 0 || isNaN($scope.ModeloValorBaseCiudades) || $scope.ModeloValorBaseCiudades == undefined) {
                $scope.ModeloValorBaseCiudades = 0
            }
            if ($scope.ModeloValorTarifaCiudades <= 0 || isNaN($scope.ModeloValorTarifaCiudades)) {
                continuar = false;
                $scope.MensajesErrorCiudad.push('Ingrese una valor tarifa')
            }
            if ($scope.ModalEstadoCiudades === undefined || $scope.ModalEstadoCiudades.Codigo == -1 || isNaN($scope.ModalEstadoCiudades.Codigo)) {
                continuar = false;
                $scope.MensajesErrorCiudad.push('Ingrese una Estado')
            }
            if (continuar == true) {
                if ($scope.ModeloCiudades != '' && $scope.ModeloCiudades != undefined && $scope.ModeloCiudades != null && $scope.ModeloCiudades.Codigo != 0 && $scope.ModeloCiudades.Codigo != undefined) {
                    var concidencias = 0
                    if ($scope.ListadoImpuestosCiudades.length > 0) {
                        for (var i = 0; i < $scope.ListadoImpuestosCiudades.length; i++) {
                            if ($scope.ListadoImpuestosCiudades[i].Codigo == $scope.ModeloCiudades.Codigo) {
                                concidencias++
                                break;
                            }
                        }
                        if (concidencias > 0) {
                            ShowError('La Ciudad ya fue ingresada')
                            $scope.ModeloFuncionario = '';
                        } else {
                            $scope.ListadoImpuestosCiudades.push({ Codigo: $scope.ModeloCiudades.Codigo, Nombre: $scope.ModeloCiudades.Nombre, CuentaPUC: { Codigo: $scope.ModeloCuentaCiudades.Codigo, CodigoNombreCuenta: $scope.ModeloCuentaCiudades.CodigoNombreCuenta }, ValorTarifa: $scope.ModeloValorTarifaCiudades, ValorBase: $scope.ModeloValorBaseCiudades, Estado: $scope.ModalEstadoCiudades });
                            $scope.totalRegistrosCiudades = $scope.totalRegistrosCiudades + 1;
                            $scope.ModeloFuncionario = '';
                            $scope.ModeloCiudades = '';
                            $scope.ModeloCuentaCiudades = $linq.Enumerable().From($scope.ListaPucCiudades).First('$.Codigo == 0');
                            $scope.ModeloValorBaseCiudades = 0;
                            $scope.ModeloValorTarifaCiudades = 0;
                            $scope.ModalEstadoCiudades = $linq.Enumerable().From($scope.ListadoEstadosCiudades).First('$.Codigo == 1');
                        }
                    } else {
                        $scope.ListadoImpuestosCiudades.push({ Codigo: $scope.ModeloCiudades.Codigo, Nombre: $scope.ModeloCiudades.Nombre, CuentaPUC: { Codigo: $scope.ModeloCuentaCiudades.Codigo, CodigoNombreCuenta: $scope.ModeloCuentaCiudades.CodigoNombreCuenta }, ValorTarifa: $scope.ModeloValorTarifaCiudades, ValorBase: $scope.ModeloValorBaseCiudades, Estado: $scope.ModalEstadoCiudades });
                        $scope.ModeloFuncionario = '';
                        $scope.ModeloCiudades = '';
                        $scope.ModeloCuentaCiudades = $linq.Enumerable().From($scope.ListaPucCiudades).First('$.Codigo == 0');
                        $scope.ModeloValorBaseCiudades = 0;
                        $scope.ModeloValorTarifaCiudades = 0;
                        $scope.ModalEstadoCiudades = $linq.Enumerable().From($scope.ListadoEstadosCiudades).First('$.Codigo == 1');
                    }
                }
                else {
                    ShowError('Debe ingresar una Ciudad Valida');
                }
            }
        }

        PlanUnicoCuentasFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: ESTADO_ACTIVO
        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ CodigoNombreCuenta: '', CodigoCuenta: '', Codigo: 0 })
                        $scope.ListaPucDepartamento = response.data.Datos;
                        $scope.ModeloCuentaDepartamento = $scope.ListaPucDepartamento[$scope.ListaPucDepartamento.length - 1];
                        if ($scope.CodigoPuc !== undefined && $scope.CodigoPuc !== '' && $scope.CodigoPuc !== 0 && $scope.CodigoPuc !== null) {
                            if ($scope.CodigoPuc > 0) {
                                $scope.ModeloCuentaDepartamento = $linq.Enumerable().From($scope.ListaPucDepartamento).First('$.Codigo ==' + $scope.CodigoPuc);
                            }
                            else {
                                $scope.ModeloCuentaDepartamento = $linq.Enumerable().From($scope.ListaPucDepartamento).First('$.Codigo ==0');
                            }
                        }
                        else {
                            $scope.ModeloCuentaDepartamento = $linq.Enumerable().From($scope.ListaPucDepartamento).First('$.Codigo ==0');
                        }
                    }
                    else {
                        $scope.ListaPucDepartamento = [];
                        $scope.ModeloCuentaDepartamento = null;
                    }
                }
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ CodigoNombreCuenta: '', CodigoCuenta: '', Codigo: 0 })
                        $scope.ListaPucCiudades = response.data.Datos;
                        $scope.ModeloCuentaCiudades = $scope.ListaPucCiudades[$scope.ListaPucCiudades.length - 1];
                        if ($scope.CodigoPuc !== undefined && $scope.CodigoPuc !== '' && $scope.CodigoPuc !== 0 && $scope.CodigoPuc !== null) {
                            if ($scope.CodigoPuc > 0) {
                                $scope.ModeloCuentaCiudades = $linq.Enumerable().From($scope.ListaPucCiudades).First('$.Codigo ==' + $scope.CodigoPuc);
                            }
                            else {
                                $scope.ModeloCuentaCiudades = $linq.Enumerable().From($scope.ListaPucCiudades).First('$.Codigo ==0');
                            }
                        }
                        else {
                            $scope.ModeloCuentaCiudades = $linq.Enumerable().From($scope.ListaPucCiudades).First('$.Codigo ==0');
                        }
                    }
                    else {
                        $scope.ListaPucCiudades = [];
                        $scope.ModeloCuentaCiudades = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoImpuesto = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoImpuesto.push({ Codigo: 0, Nombre: '(TODAS)' })
                        $scope.ModalImpuesto = $scope.ListadoTipoImpuesto[0];
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != CODIGO_TIPO_IMPUESTO_NINGUNO) {
                                $scope.ListadoTipoImpuesto.push(response.data.Datos[i]);
                            }
                        }
                    }
                    else {
                        $scope.ListadoTipoImpuesto = []
                    }
                }
            }, function (response) {
            });


        $scope.ValidarModificacionCiudades = function (Item) {
            $scope.MensajesErrorCiudad = [];
            if (Item.ValorTarifa <= 0 || Item.ValorTarifa === '' || Item.ValorTarifa === undefined || isNaN(Item.ValorTarifa)) {
                $scope.MensajesErrorCiudad.push('Ingresar un valor tarifa válido');
            } else {
                if (Item.ValorBase < 0 || Item.ValorBase === '' || Item.ValorBase === undefined || isNaN(Item.ValorBase)) {
                    $scope.MensajesErrorCiudad.push('Ingresar un valor base válido');
                }
                else {
                    Item.editable = false;
                }
            };
        }

        $scope.ValidarModificacionDepartamento = function (Item) {
            $scope.MensajesErrorDepartamento = [];
            if (Item.ValorTarifa <= 0 || Item.ValorTarifa === '' || Item.ValorTarifa === undefined || isNaN(Item.ValorTarifa)) {
                $scope.MensajesErrorDepartamento.push('Ingresar un valor tarifa válido');
            } else {
                if (Item.ValorBase < 0 || Item.ValorBase === '' || Item.ValorBase === undefined || isNaN(Item.ValorBase)) {
                    $scope.MensajesErrorDepartamento.push('Ingresar un valor base válido');
                }
                else {
                    Item.editable = false;
                }
            };
        }
        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoImpuestos = [];

            DatosRequeridos();

            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                Codigo: $scope.Codigo,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CodigoAlterno: $scope.CodigoAlterno,
                Nombre: $scope.Nombre,
                Tipo_Impuesto: $scope.ModalImpuesto,
                Tipo_Recaudo: $scope.ModalRecaudo,
                Estado: $scope.ModalEstado.Codigo,
                AplicaConsultaMaster: 1,
            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                ImpuestosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoImpuestos = [];
                                response.data.Datos.forEach(function (item) {
                                    if (item.Codigo > 0) {
                                        $scope.ListadoImpuestos.push(item);
                                    }
                                });
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoImpuestos = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        function DatosRequeridos() {
            if (filtros.Codigo === undefined) {
                filtros.Codigo = 0;
            }

        }

        /*------------------------------------------------------------------------------------Eliminar Oficinas-----------------------------------------------------------------------*/
        $scope.EliminarImpuesto = function (codigo, Nombre, CodigoAlterno) {
            $scope.Codigo = codigo
            $scope.Nombre = Nombre
            $scope.ModalErrorCompleto = ''

            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnularImpuesto');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                AplicaDetalle: CERO,
            };

            ImpuestosFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Codigo = '';
                        ShowSuccess('Se eliminó el Impuesto ' + $scope.Nombre);
                        closeModal('modalAnularImpuesto');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeAnuloImpuesto');
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'No se puede anular el impuesto ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.EliminarImpuestoCiudad = function (codigo, nombre, index) {
            $scope.CodigoCiudad = codigo
            $scope.NombreCiudadEliminar = nombre
            $scope.index = index
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false;
            showModal('modalEliminarImpuestoCiudad');
        };

        $scope.ConfirmarEliminarImpuestoCiudad = function () {
            $scope.ListadoImpuestosCiudades.splice($scope.index, 1);
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                CodigoCiudad: $scope.CodigoCiudad,
                AplicaDetalle: CODIGO_DOS
            };
            ImpuestosFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se Borro la Ciudad ' + $scope.NombreCiudadEliminar);
                        $scope.totalRegistrosCiudades = $scope.totalRegistrosCiudades - 1;
                        closeModal('modalEliminarImpuestoCiudad', 1);
                        //Obtener();
                    } else {
                        for (var i = 0; i < $scope.ListadoImpuestosCiudades.length; i++) {
                            if ($scope.ListadoImpuestosCiudades[i].Codigo === $scope.CodigoCiudad) {
                                $scope.ListadoImpuestosCiudades.splice(i, 1);
                                $scope.totalRegistrosCiudades = $scope.totalRegistrosCiudades - 1;
                                i = $scope.ListadoImpuestosCiudades.length;
                                closeModal('modalEliminarImpuestoCiudad', 1);
                            }
                        }
                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeEliminarImpuesto', 1);
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'no se puede Eliminar la Ciudad ' + $scope.NombreCiudadEliminar + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText
                });
        };


        $scope.EliminarImpuestoDepartamento = function (codigo, nombre) {
            $scope.CodigoDepartamento = codigo
            $scope.NombreDepartamentoEliminar = nombre
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false;
            showModal('modalEliminarImpuestoDepartamento');
        };

        $scope.ConfirmarEliminarImpuestoDepartamento = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                CodigoDepartamento: $scope.CodigoDepartamento,
                AplicaDetalle: CODIGO_TRES
            };
            ImpuestosFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se Borro el Departamento ' + $scope.NombreDepartamentoEliminar);
                        $scope.ListadoImpuestosDepartamento.splice(i, 1);
                        $scope.totalRegistrosDepartamentos = $scope.totalRegistrosDepartamentos - 1;
                        closeModal('modalEliminarImpuestoDepartamento', 1);
                        //Obtener();
                    }
                    else {
                        for (var i = 0; i < $scope.ListadoImpuestosDepartamento.length; i++) {
                            if ($scope.ListadoImpuestosDepartamento[i].Codigo === $scope.CodigoImpuesto) {
                                $scope.ListadoImpuestosDepartamento.splice(i, 1);
                                $scope.totalRegistrosDepartamentos = $scope.totalRegistrosDepartamentos - 1;
                                i = $scope.ListadoImpuestosDepartamento.length;
                                closeModal('modalEliminarImpuestoDepartamento', 1);
                            }
                        }

                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeEliminarImpuesto ');
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'no se puede Eliminar el Departamento ' + $scope.NombreDepartamentoEliminar + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };



        function Obtener(TotalR) {
            blockUI.start('Cargando Concepto Código ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Concepto Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Pagina: 1,
                RegistrosPagina: 1000,
            };
            $scope.ListadoImpuestos = [];
            blockUI.delay = 1000;
            ImpuestosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.listadoInicialCiud = response.data.Datos.ListadoCiudades;
                        $scope.ListadoImpuestosCiudades = angular.copy($scope.listadoInicialCiud);
                        for (var i = 0; i < $scope.ListadoImpuestosCiudades.length; i++) {
                            $scope.ListadoImpuestosCiudades[i].Estado = $linq.Enumerable().From($scope.ListadoEstados2).First('$.Codigo ==' + $scope.ListadoImpuestosCiudades[i].Estado);
                        }

                        $scope.listadoInicialDepa = response.data.Datos.ListadoDepartamentos;
                        $scope.ListadoImpuestosDepartamento = angular.copy($scope.listadoInicialDepa);
                        for (var i = 0; i < $scope.ListadoImpuestosDepartamento.length; i++) {
                            $scope.ListadoImpuestosDepartamento[i].Estado = $linq.Enumerable().From($scope.ListadoEstados2).First('$.Codigo ==' + $scope.ListadoImpuestosDepartamento[i].Estado);
                        }

                        if (TotalR === CODIGO_UNO) {
                            $scope.totalRegistrosCiudades = $scope.ListadoImpuestosCiudades.length;
                        } else {
                            $scope.totalRegistrosDepartamentos = $scope.ListadoImpuestosDepartamento.length;
                        }

                    }
                    else {
                        ShowError('No se logro consultar el impuesto ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!Consultar';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarImouestos';
                    closeModal('modalAnularImpuesto');
                    closeModal('modalMensajeAnuloImpuesto');
                    closeModal('ModalCiudades');
                    closeModal('ModalDepartamento');
                });
            blockUI.stop();
        };

        $scope.CerrarModal = function () {
            Find();
            $scope.Codigo = 0;
            closeModal('modalAnularImpuesto');
            closeModal('modalMensajeAnuloImpuesto');
            closeModal('ModalCiudades');
            closeModal('ModalDepartamento');
            $scope.MensajesErrorCiudad = [];
            $scope.MensajesErrorDepartamento = [];
            $scope.ModeloFuncionario = '';
            if ($scope.ModeloDepartamento != undefined && $scope.ModeloDepartamento != null && $scope.ModeloDepartamento.length > 0) {
                $scope.ListadoImpuestosDepartamento = [];
                $scope.ModeloValorBaseDepartamento = 0;
                $scope.ModeloValorTarifaDepartamento = 0;
            }
            if ($scope.ModeloCiudades != undefined && $scope.ModeloCiudades != null && $scope.ModeloCiudades.length > 0) {
                $scope.ListadoImpuestosCiudades = [];
                $scope.ModeloValorBaseCiudades = 0;
                $scope.ModeloValorTarifaCiudades = 0;
            }
        }
        $scope.ModalCiudades = function (Codigo, tarifa, base, Nombre) {
            $scope.Codigo = Codigo
            $scope.ModeloValorTarifaCiudades = tarifa
            $scope.ModeloValorBaseCiudades = base
            $scope.ModeloNombre = Nombre
            Obtener(CODIGO_UNO);
            showModal('ModalCiudades');
        }

        $scope.ModalDepartamentos = function (Codigo, tarifa, base, Nombre) {
            $scope.Codigo = Codigo
            $scope.ModeloValorTarifaDepartamento = tarifa
            $scope.ModeloValorBaseDepartamento = base
            $scope.ModeloNombre = Nombre
            Obtener(CODIGO_TRES);
            showModal('ModalDepartamento');
        }

        $scope.GuardarDepartamento = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('ModalDepartamento');
            var list = [];

            for (var i = 0; i < $scope.ListadoImpuestosDepartamento.length; i++) {
                var item = $scope.ListadoImpuestosDepartamento[i]
                item.Estado = $scope.ListadoImpuestosDepartamento[i].Estado.Codigo;
                list.push(item)
            }

            $scope.objEnviar = {
                Codigo: $scope.Codigo,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo_USUA_Crea: $scope.Sesion.UsuarioAutenticado.Codigo,
                ListadoDepartamentos: list,
                AplicaDetalle: CODIGO_TRES
            };

            ImpuestosFactory.Guardar($scope.objEnviar).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.ModeloCodigo == 0) {
                                ShowSuccess('Se guardaron las tarifas del impuesto  ' + $scope.ModeloNombre + ' asociadas a los departamentos');
                                $scope.ModeloNombre = "";
                                closeModal('ModalDepartamento');
                            }
                            else {
                                ShowSuccess('Se guardaron las tarifas del  ' + $scope.ModeloNombre + ' asociadas a los departamentos');
                                $scope.ModeloNombre = "";
                                closeModal('ModalCiudades');
                                closeModal('ModalDepartamento');
                            }
                            location.href = '#!ConsultarImpuestos/' + $scope.Codigo;
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.GuardarCiudad = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('ModalDepartamento');
            var list = [];

            for (var i = 0; i < $scope.ListadoImpuestosCiudades.length; i++) {
                var item = $scope.ListadoImpuestosCiudades[i]
                item.Estado = $scope.ListadoImpuestosCiudades[i].Estado.Codigo;
                item.ValorTarifa = parseFloat(item.ValorTarifa);
                list.push(item)
            }

            $scope.objEnviar = {
                Codigo: $scope.Codigo,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo_USUA_Crea: $scope.Sesion.UsuarioAutenticado.Codigo,
                ListadoCiudades: list,
                AplicaDetalle: CODIGO_DOS
            };

            ImpuestosFactory.Guardar($scope.objEnviar).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.ModeloCodigo == 0) {
                                ShowSuccess('Se guardaron las tarifas del impuesto  ' + $scope.ModeloNombre + ' asociadas a las ciudades');
                                $scope.ModeloNombre = "";
                                closeModal('ModalCiudades');
                            }
                            else {
                                ShowSuccess('Se guardaron las tarifas del  ' + $scope.ModeloNombre + ' asociadas a las ciudades');
                                $scope.ModeloNombre = "";
                                closeModal('ModalCiudades');
                            }
                            location.href = '#!ConsultarImpuestos/' + $scope.Codigo;
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.filtrotabla = function (opcion, Codigo) {
            if (Codigo != undefined) {
                if (Codigo > 0) {
                    $scope.listadofiltro = [];

                    if (opcion == 1) {
                        $scope.ListadoImpuestosDepartamento.forEach(function (item) {
                            if (item.Codigo == Codigo) {
                                $scope.listadofiltro.push(item);
                            }
                        })
                        if ($scope.listadofiltro.length > 0) {
                            $scope.ListadoImpuestosDepartamento = $scope.listadofiltro;
                        } else {
                            $scope.ModalDepartamentos($scope.Codigo, $scope.ModeloValorTarifaDepartamento, $scope.ModeloValorBaseDepartamento, $scope.ModeloNombre)
                        }
                    } else {
                        $scope.ListadoImpuestosCiudades.forEach(function (item) {
                            if (item.Codigo == Codigo) {
                                $scope.listadofiltro.push(item);
                            }
                        })
                        if ($scope.listadofiltro.length > 0) {
                            $scope.ListadoImpuestosCiudades = $scope.listadofiltro;
                        } else {
                            $scope.ModalCiudades($scope.Codigo, $scope.ModeloValorTarifaCiudades, $scope.ModeloValorBaseCiudades, $scope.ModeloNombre)
                        }
                    }
                } else {
                    $scope.ListadoImpuestosDepartamento = [];
                    $scope.ListadoImpuestosCiudades = [];
                    if (opcion == 1) {
                        $scope.ModalDepartamentos($scope.Codigo, $scope.ModeloValorTarifaDepartamento, $scope.ModeloValorBaseDepartamento, $scope.ModeloNombre)
                    } else {
                        $scope.ModalCiudades($scope.Codigo, $scope.ModeloValorTarifaCiudades, $scope.ModeloValorBaseCiudades, $scope.ModeloNombre)
                    }
                }
            } else {
                if (opcion == 1) {
                    $scope.ModalDepartamentos($scope.Codigo, $scope.ModeloValorTarifaDepartamento, $scope.ModeloValorBaseDepartamento, $scope.ModeloNombre)
                } else {
                    $scope.ModalCiudades($scope.Codigo, $scope.ModeloValorTarifaCiudades, $scope.ModeloValorBaseCiudades, $scope.ModeloNombre)
                }
            }
        };

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskDecimalesGrid = function (item) {
            return MascaraDecimales(item)
        };
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope)
        }
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };

    }]);