﻿PRINT 'gsp_obtener_planilla_guia_paqueteria'
GO
DROP PROCEDURE gsp_obtener_planilla_guia_paqueteria
GO
CREATE PROCEDURE gsp_obtener_planilla_guia_paqueteria    
(    
@par_EMPR_Codigo smallint,    
@par_Numero Numeric   
)    
as
begin
select

1 as Obtener,
ENPD.EMPR_Codigo,
ENPD.Numero,
ENPD.TIDO_Codigo,
ENPD.Numero_Documento,
ENPD.Fecha_Hora_Salida,
ENPD.Fecha,
ENPD.RUTA_Codigo,
RUTA.CATA_TIRU_Codigo,
ENPD.VEHI_Codigo,
ENPD.SEMI_Codigo,
ENPD.Cantidad,
ENPD.peso,
ENPD.TATC_Codigo,
ENPD.TTTC_Codigo,
ENPD.Valor_Flete_Transportador,
ENPD.Valor_Anticipo,
ENPD.Valor_Retencion_Fuente,
ENPD.Valor_Retencion_ICA,
ENPD.Valor_Pagar_Transportador,
ENPD.Valor_Flete_Cliente,
ENPD.Valor_Seguro_Mercancia,
ENPD.Valor_Otros_Cobros,
ENPD.Valor_Total_Credito,
ENPD.Valor_Total_Contado,
ENPD.Valor_Total_Alcobro,
ENPD.Observaciones,
ENPD.Anulado,
ENPD.Estado


from Encabezado_Planilla_Despachos ENPD
LEFT JOIN Rutas AS RUTA
ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo
AND ENPD.RUTA_Codigo = RUTA.Codigo

where 
ENPD.EMPR_Codigo = @par_EMPR_Codigo
and ENPD.Numero = @par_Numero
end
GO