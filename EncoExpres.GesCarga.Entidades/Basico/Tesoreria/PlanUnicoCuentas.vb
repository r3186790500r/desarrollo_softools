﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="PlanUnicoCuentas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class PlanUnicoCuentas
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="PlanUnicoCuentas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PlanUnicoCuentas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoCuenta = Read(lector, "Codigo_Cuenta")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            ExigeCentroCosto = Read(lector, "Exige_Centro_Costo")
            ExigeTercero = Read(lector, "Exige_Tercero")
            ExigeValorBase = Read(lector, "Exige_Valor_Base")
            ValorBase = Read(lector, "Valor_Base")
            ExigeDocumentoCruce = Read(lector, "Exige_Documento_Cruce")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            ClaseCuentaPUC = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CCPU_Codigo"), .Nombre = Read(lector, "ClasePlanUnicoCuenta")}
            CodigoNombreCuenta = Read(lector, "CodigoNombreCuenta")
            AplicarMedioPago = Read(lector, "Aplicar_Medio_Pago")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            Else


            End If
        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property ClaseCuentaPUC As ValorCatalogos
        <JsonProperty>
        Public Property CodigoCuenta As String
        <JsonProperty>
        Public Property NumeroCuenta As String
        <JsonProperty>
        Public Property ExigeCentroCosto As Short
        <JsonProperty>
        Public Property ExigeTercero As Short
        <JsonProperty>
        Public Property ExigeValorBase As Short
        <JsonProperty>
        Public Property ValorBase As Double
        <JsonProperty>
        Public Property ExigeDocumentoCruce As Short
        <JsonProperty>
        Public Property CodigoNombreCuenta As String
        <JsonProperty>
        Public Property AplicarMedioPago As Short
    End Class

End Namespace
