﻿PRINT 'gsp_modificar_encabezado_tarifario_ventas'
GO
DROP PROCEDURE gsp_modificar_encabezado_tarifario_ventas
GO
CREATE PROCEDURE gsp_modificar_encabezado_tarifario_ventas  
(  
@par_EMPR_Codigo smallint  
,@par_Numero numeric(18,0) = null  
,@par_Tarifario_Base smallint = null  
,@par_Fecha_Vigencia_Inicio date = null  
,@par_Fecha_Vigencia_Fin date = null  
,@par_Estado smallint = null  
,@par_USUA_Codigo_Modifica smallint = null  
,@par_Aplica_Paqueteria numeric= null  
,@par_Minimo_Kilo_Cobro numeric(18, 2)= null  
,@par_Maximo_Kilo_Cobro numeric(18, 2)= null  
,@par_Valor_Kilo_Adicional money= null  
,@par_Valor_Manejo money= null  
,@par_Porcentaje_Manejo numeric(18, 2)= null  
,@par_Minimo_Valor_Manejo money= null  
,@par_Valor_Seguro money= null  
,@par_Porcentaje_Seguro numeric(18, 2)= null  
,@par_Minimo_Valor_Seguro money= null  
,@par_Minimo_Valor_Unidad money= null  
)  
AS BEGIN  
  
  
update Encabezado_Tarifario_Carga_Ventas  
           set Tarifario_Base = @par_Tarifario_Base  
           ,Fecha_Vigencia_Inicio = @par_Fecha_Vigencia_Inicio  
           ,Fecha_Vigencia_Fin = @par_Fecha_Vigencia_Fin  
           ,Estado = @par_Estado  
           ,USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica  
           ,Fecha_Crea = getdate()  
  
     where EMPR_Codigo = @par_EMPR_Codigo  
     and Numero  = @par_Numero  
delete Tarifas_Paqueteria_Ventas where ETCV_Numero = @par_Numero and EMPR_Codigo = @par_EMPR_Codigo  
if  @par_Aplica_Paqueteria = 1   
BEGIN  
INSERT INTO Tarifas_Paqueteria_Ventas  
           (EMPR_Codigo  
           ,ETCV_Numero  
           ,Minimo_Kilo_Cobro  
           ,Maximo_Kilo_Cobro  
           ,Valor_Kilo_Adicional  
           ,Valor_Manejo  
           ,Porcentaje_Manejo  
           ,Minimo_Valor_Manejo  
           ,Valor_Seguro  
           ,Porcentaje_Seguro  
           ,Minimo_Valor_Seguro  
           ,Minimo_Valor_Unidad  
           ,USUA_Codigo_Crea  
           ,Fecha_Crea  
           )  
     VALUES  
           (@par_EMPR_Codigo  
           ,@par_Numero  
   ,isnull(@par_Minimo_Kilo_Cobro,0)  
   ,isnull(@par_Maximo_Kilo_Cobro,0)  
   ,isnull(@par_Valor_Kilo_Adicional,0)  
   ,isnull(@par_Valor_Manejo,0)  
   ,isnull(@par_Porcentaje_Manejo,0)  
   ,isnull(@par_Minimo_Valor_Manejo,0)  
   ,isnull(@par_Valor_Seguro,0)  
   ,isnull(@par_Porcentaje_Seguro,0)  
   ,isnull(@par_Minimo_Valor_Seguro,0)  
   ,isnull(@par_Minimo_Valor_Unidad,0)  
           ,isnull(@par_USUA_Codigo_Modifica,0)  
           ,GETDATE())  
END  
SELECT @par_Numero AS Codigo  
  
END  
GO