﻿DROP PROCEDURE gsp_insertar_encabezado_planilla_despachos     
GO
CREATE PROCEDURE gsp_insertar_encabezado_planilla_despachos     
(                    
@par_EMPR_Codigo   smallint                     
,@par_Numero    numeric(18,0) = 0                    
,@par_TIDO_Codigo   numeric(18,0)                     
,@par_Fecha     datetime  = NULL                    
,@par_Fecha_Hora_Salida  datetime  = NULL                    
                  
,@par_RUTA_Codigo   numeric(18,0) = NULL                    
,@par_VEHI_Codigo   numeric(18,0) = NULL                    
,@par_SEMI_Codigo   numeric(18,0) = NULL                    
,@par_TERC_Codigo_Tenedor numeric(18,0) = NULL                    
,@par_TERC_Codigo_Conductor numeric(18,0) = NULL                    
                  
,@par_Cantidad    numeric(18,2) = NULL                    
,@par_Peso     numeric(18,2) = NULL                    
,@par_TATC_Codigo_Compra smallint = NULL                    
,@par_TTTC_Codigo_Compra smallint = NULL                    
                
---                
,@par_LNTC_Codigo_Compra smallint = NULL                
,@par_TLNC_Codigo_Compra smallint = NULL                
,@par_ETCC_Numero   numeric = NULL                
,@par_Valor_Auxiliares      money  = NULL                
,@par_Numeracion   varchar(50) = NULL                      
--                
                  
,@par_Valor_Flete_Transportador money = NULL                    
,@par_Valor_Anticipo   money = NULL                    
,@par_Valor_Impuestos   money = NULL                  
,@par_Valor_Pagar_Transportador money = NULL                    
,@par_Valor_Flete_Cliente  money = NULL                    
                  
,@par_Valor_Seguro_Mercancia money = NULL                    
,@par_Valor_Otros_Cobros  money = NULL                    
,@par_Valor_Total_Credito  money = NULL                    
,@par_Valor_Total_Contado  money = NULL                    
,@par_Valor_Total_Alcobro  money = NULL                    
                  
,@par_Observaciones    varchar(200) = NULL                    
,@par_Estado     smallint = NULL                    
,@par_USUA_Codigo_Crea   smallint                     
,@par_OFIC_Codigo numeric            
,@par_Valor_Seguro_Poliza numeric = null  
            
)                    
AS                    
BEGIN                    
                  
EXEC gsp_generar_consecutivo                    
@par_EMPR_Codigo,                   
@par_TIDO_Codigo,                  
@par_OFIC_Codigo, --> Oficina                  
@par_Numero OUTPUT                      
                    
INSERT INTO Encabezado_Planilla_Despachos                    
           (EMPR_Codigo                      
           ,TIDO_Codigo                    
           ,Numero_Documento                 
     ,ENMC_Numero                 
           ,Fecha_Hora_Salida                    
           ,Fecha                    
           ,RUTA_Codigo                    
           ,VEHI_Codigo                    
           ,SEMI_Codigo                    
           ,TERC_Codigo_Tenedor                    
           ,TERC_Codigo_Conductor                    
           ,Cantidad                    
           ,Peso                    
           ,TATC_Codigo_Compra                    
           ,TTTC_Codigo_Compra                   
                      
   ,LNTC_Codigo_Compra                
   ,TLNC_Codigo_Compra                
   ,ETCC_Numero                
   ,Valor_Auxiliares                
   ,Numeracion                
                
           ,Valor_Flete_Transportador                    
           ,Valor_Anticipo                    
     ,Valor_Impuestos                  
           ,Valor_Pagar_Transportador                    
           ,Valor_Flete_Cliente                    
           ,Valor_Seguro_Mercancia                    
           ,Valor_Otros_Cobros                    
           ,Valor_Total_Credito                    
           ,Valor_Total_Contado                    
           ,Valor_Total_Alcobro                    
           ,Observaciones                    
           ,Anulado         
           ,Estado                    
           ,Fecha_Crea                    
           ,USUA_Codigo_Crea            
     ,OFIC_Codigo    
  ,Ultimo_Seguimiento_Vehicular    
  ,Valor_Seguro_Poliza  
           )     
     VALUES                    
           (@par_EMPR_Codigo                      
           ,@par_TIDO_Codigo                     
           ,@par_Numero              
     ,0              
           ,ISNULL(@par_Fecha_Hora_Salida ,'')                    
           ,ISNULL(@par_Fecha,'')                    
           ,ISNULL(@par_RUTA_Codigo ,0)                    
           ,ISNULL(@par_VEHI_Codigo ,0)                    
           ,ISNULL(@par_SEMI_Codigo ,0)                    
           ,ISNULL(@par_TERC_Codigo_Tenedor ,0)                    
           ,ISNULL(@par_TERC_Codigo_Conductor ,0)                    
           ,ISNULL(@par_Cantidad ,0)                    
,ISNULL(@par_Peso ,0)                    
                
           ,@par_TATC_Codigo_Compra                     
           ,@par_TTTC_Codigo_Compra                
     ,@par_LNTC_Codigo_Compra                
     ,@par_TLNC_Codigo_Compra                
     ,@par_ETCC_Numero                
     ,@par_Valor_Auxiliares                
     ,@par_Numeracion                
           ,ISNULL(@par_Valor_Flete_Transportador ,0)                    
           ,ISNULL(@par_Valor_Anticipo ,0)                   
     ,ISNULL(@par_Valor_Impuestos,0)                  
           ,ISNULL(@par_Valor_Pagar_Transportador ,0)                    
           ,ISNULL(@par_Valor_Flete_Cliente ,0)                    
           ,ISNULL(@par_Valor_Seguro_Mercancia ,0)                    
           ,ISNULL(@par_Valor_Otros_Cobros ,0)                    
           ,ISNULL(@par_Valor_Total_Credito ,0)                    
           ,ISNULL(@par_Valor_Total_Contado ,0)                               
     ,ISNULL(@par_Valor_Total_Alcobro ,0)                    
           ,ISNULL(@par_Observaciones,'')                    
           ,0                     
           ,ISNULL(@par_Estado ,1)                    
           ,GETDATE()                     
           ,@par_USUA_Codigo_Crea               
     ,@par_OFIC_Codigo       
  ,GETDATE()        
  ,@par_Valor_Seguro_Poliza  
   )                    
IF @par_TIDO_Codigo = 150        
BEGIN        
INSERT INTO Detalle_Seguimiento_Vehiculos (     
 EMPR_Codigo,                  
 ENPD_Numero,                  
 ENOC_Numero,                  
 ENMC_Numero,                  
 Fecha_Reporte,                  
 CATA_TOSV_Codigo,                  
 Ubicacion,                  
 Longitud,                  
 Latitud,                  
 PUCO_Codigo,                  
 CATA_SRSV_Codigo,                  
 CATA_NOSV_Codigo,                  
 Observaciones,                  
 Kilometros_Vehiculo,                  
 Reportar_Cliente,                  
 Envio_Reporte_Cliente,                  
 Fecha_Reporte_Cliente,                  
 USUA_Codigo_Crea,                  
 Fecha_Crea,                  
 Anulado,                  
 Orden)                  
 VALUES                   
 (                  
 @par_EMPR_Codigo,                  
 (SELECT Numero FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo                
 AND Numero_Documento = @par_Numero                
 AND TIDO_Codigo = @par_TIDO_Codigo),                  
 0,                  
 0,                  
 getdate(),                  
 8001,                  
 '',                  
 0,                  
 0,                  
 0,                  
 8202,                  
 8100,                  
 'PLANILLA DESPACHOS',                  
 0,                  
 0,                  
 0,                  
 '',                  
 @par_USUA_Codigo_Crea,                  
 GETDATE(),                  
 0,                  
 1                  
 )       
     
 INSERT INTO Detalle_Seguimiento_Remesas       
(                    
EMPR_Codigo,                    
ENPD_Numero,            
ENRE_Numero,                  
Fecha_Reporte,                    
CATA_TOSV_Codigo,                    
Ubicacion,                    
Longitud,                    
Latitud,                    
PUCO_Codigo,                    
CATA_SRSV_Codigo,                    
CATA_NOSV_Codigo,                    
Observaciones,                           
Reportar_Cliente,                    
Envio_Reporte_Cliente,                    
Fecha_Reporte_Cliente,                    
USUA_Codigo_Crea,                    
Fecha_Crea,                  
Anulado,                    
Orden                
)                    
SELECT                  
@par_EMPR_Codigo,                    
(SELECT Numero FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo                
 AND Numero_Documento = @par_Numero AND TIDO_Codigo = @par_TIDO_Codigo),                    
Numero,                        
GETDATE(),                    
8003,                    
'',                    
0,                    
0,                    
0,                    
8201,                    
8100,                    
ISNULL(@par_Observaciones,''),                          
0,                    
0,                    
GETDATE(),                      
@par_USUA_Codigo_Crea,                    
GETDATE(),                    
0,                    
1                    
FROM Encabezado_Remesas       
WHERE EMPR_Codigo = @par_EMPR_codigo       
AND ENPD_Numero = (SELECT Numero FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo                
 AND Numero_Documento = @par_Numero AND TIDO_Codigo = @par_TIDO_Codigo)           
               
END                
        
 SELECT Numero, Numero_Documento FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo                
 AND Numero_Documento = @par_Numero                
 AND TIDO_Codigo = @par_TIDO_Codigo                
END           
GO