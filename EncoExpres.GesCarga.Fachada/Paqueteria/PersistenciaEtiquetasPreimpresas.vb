﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaEtiquetasPreimpresas
        Implements IPersistenciaBase(Of EtiquetaPreimpresa)


        Public Function Consultar(filtro As EtiquetaPreimpresa) As IEnumerable(Of EtiquetaPreimpresa) Implements IPersistenciaBase(Of EtiquetaPreimpresa).Consultar
            Return New RepositorioEtiquetasPreimpresas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EtiquetaPreimpresa) As Long Implements IPersistenciaBase(Of EtiquetaPreimpresa).Insertar
            Return New RepositorioEtiquetasPreimpresas().Insertar(entidad)
        End Function

        Public Function InsertarPrecintos(entidad As EtiquetaPreimpresa) As EtiquetaPreimpresa
            Return New RepositorioEtiquetasPreimpresas().InsertarPrecintos(entidad)
        End Function

        Public Function Modificar(entidad As EtiquetaPreimpresa) As Long Implements IPersistenciaBase(Of EtiquetaPreimpresa).Modificar
            Return New RepositorioEtiquetasPreimpresas().Modificar(entidad)
        End Function

        Public Function ModificarPrecintos(entidad As EtiquetaPreimpresa) As EtiquetaPreimpresa
            Return New RepositorioEtiquetasPreimpresas().ModificarPrecintos(entidad)
        End Function

        Public Function Obtener(filtro As EtiquetaPreimpresa) As EtiquetaPreimpresa Implements IPersistenciaBase(Of EtiquetaPreimpresa).Obtener
            Return New RepositorioEtiquetasPreimpresas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EtiquetaPreimpresa) As Boolean
            Return New RepositorioEtiquetasPreimpresas().Anular(entidad)
        End Function
        Public Function LiberarPrecinto(entidad As EtiquetaPreimpresa) As Boolean
            Return New RepositorioEtiquetasPreimpresas().LiberarPrecinto(entidad)
        End Function

        Public Function ConsultarUnicoPrecinto(filtro As DetalleEtiquetaPreimpresa) As IEnumerable(Of DetalleEtiquetaPreimpresa)
            Return New RepositorioEtiquetasPreimpresas().ConsultarDetallePrecinto(filtro)
        End Function
        Public Function ValidarPrecintoPlanillaPaqueteria(entidad As EtiquetaPreimpresa) As EtiquetaPreimpresa
            Return New RepositorioEtiquetasPreimpresas().ValidarPrecintoPlanillaPaqueteria(entidad)
        End Function

        Public Function ActualizarResponsable(entidad As EtiquetaPreimpresa) As Long
            Return New RepositorioEtiquetasPreimpresas().ActualizarTercero(entidad)
        End Function
    End Class

End Namespace

