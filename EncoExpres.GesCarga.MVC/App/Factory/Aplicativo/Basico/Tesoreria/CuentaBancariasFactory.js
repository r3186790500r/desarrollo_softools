﻿EncoExpresApp.factory('CuentaBancariasFactory', ['$http', function ($http) {

    var urlService = GetUrl('CuentaBancarias');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.ConsultarCuentaOficinas = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarCuentaOficinas';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.GuardarCuentaOficinas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GuardarCuentaOficinas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.CuentaBancariaOficinas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CuentaBancariaOficinas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    return factory;
}]);