﻿EncoExpresApp.controller("ConsultarLegalizarRemesasPaqueteriaCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'LegalizarRemesasPaqueteriaFactory',
    'OficinasFactory', 'EmpresasFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, LegalizarRemesasPaqueteriaFactory,
        OficinasFactory, EmpresasFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONSULTAR LEGALIZACIÓN GUÍAS';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Legalizacion Guías' }];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.LEGALIZACION_REMESAS);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.pref = '';
        $scope.Modelo = {
            NumeroDocumento: '',
            Planilla: '',
            Conductor: ''
        };
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.ListadoOficinas = [];
        $scope.ListadoEstados = [
            { Codigo: -1, Nombre: "(TODAS)" },
            { Codigo: 0, Nombre: "BORRADOR" },
            { Codigo: 1, Nombre: "DEFINITIVO" },
            { Codigo: 2, Nombre: "ANULADA" }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo === -1');
        //Obtiene la URL para llamar el proyecto ASP
        $scope.urlASP = '';
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy === true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete
        //----------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //-- cargar Prefijo EmPrefpresa --//
            EmpresasFactory.Consultar(
                { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
            ).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.pref = response.data.Datos[0].Prefijo;
                    //console.log("Empresa Prefijo: ", $scope.pref);
                }
            });
            //--Oficinas
            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                var responseOficina = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true });
                if (responseOficina.ProcesoExitoso === true) {
                    $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' });
                    responseOficina.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });
                    $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
                }
            }
            else {
                $scope.ListadoOficinas.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.Modelo.Oficina = $scope.ListadoOficinas[0];
            }
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            //--Obtener Parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
                $scope.Modelo.NumeroDocumento = $routeParams.Numero;
                PantallaBloqueo(Find);
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            PantallaBloqueo(Find);
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            PantallaBloqueo(Find);
        };
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        //--Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarLegalizarRemesasPaqueteria';
            }
        };
        //--Funcion Buscar
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO && DatosRequeridos()) {
                $scope.Modelo.Numero = 0;
                PantallaBloqueo(Find);
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroDocumento === null || $scope.Modelo.NumeroDocumento === undefined || $scope.Modelo.NumeroDocumento === '' || $scope.Modelo.NumeroDocumento === 0 || isNaN($scope.Modelo.NumeroDocumento) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.Modelo.NumeroDocumento !== null && $scope.Modelo.NumeroDocumento !== undefined && $scope.Modelo.NumeroDocumento !== '' && $scope.Modelo.NumeroDocumento !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' días');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial;
                    } else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal;
                    }
                }
            }
            return continuar;
        }
        function Find() {
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.Listadolegalizacion = [];
            if ($scope.Buscando) {
                var Filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: $scope.Modelo.NumeroDocumento,
                    FechaInicial: $scope.Modelo.NumeroDocumento !== '' ? undefined : $scope.Modelo.FechaInicial,
                    FechaFinal: $scope.Modelo.NumeroDocumento !== '' ? undefined : $scope.Modelo.FechaFinal,
                    Oficina: $scope.Modelo.Oficina,
                    Estado: $scope.Modelo.Estado.Codigo,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas
                };
                LegalizarRemesasPaqueteriaFactory.Consultar(Filtro).
                    then(function (response) {
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.Listadolegalizacion = response.data.Datos;
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        ShowError(response.statusText);
                    });
            }
        }
        //--Funcion Buscar
        //--Función que ejecuta el boton de vista preliminar
        $scope.DesplegarInforme = function (Numero, OpcionExportar, TipoReporte) {
            if ($scope.ValidarPermisos.AplicaImprimir === PERMISO_ACTIVO) {
                $scope.FiltroArmado = '';
                $scope.Numero = Numero;
                switch (TipoReporte) {
                    case 1://--Recaudo Contra Entrega / Contado
                        $scope.NombreReporte = REPORTE_LEGALIZACION_GUIAS.RECAUDO;
                        break;
                    case 2://--Entrega Cartera
                        $scope.NombreReporte = REPORTE_LEGALIZACION_GUIAS.ENTREGA_CARTERA;
                        break;
                    case 3://--Entrega Archivo
                        $scope.NombreReporte = REPORTE_LEGALIZACION_GUIAS.ENTREGA_ARCHIVO;
                        break;
                }
                //Arma el filtro
                $scope.ArmarFiltro();
                //Llama a la pagina ASP con el filtro por GET
                if (OpcionExportar === 1) {//PDF
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + 1 + '&Prefijo=' + $scope.pref);
                }
                if (OpcionExportar === 2) {//EXCEL
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + 1 + '&Prefijo=' + $scope.pref);
                }
                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';
            }
        };
        //--funcion enviar parametros al proyecto ASP armando el filtro
        $scope.ArmarFiltro = function () {
            $scope.FiltroArmado = '';
            if ($scope.Numero !== undefined && $scope.Numero !== '' && $scope.Numero !== null) {
                $scope.FiltroArmado += '&Numero=' + $scope.Numero;
            }
            $scope.FiltroArmado += '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo;
        };
        //----------------------------Funciones Generales---------------------------------//
    }]);