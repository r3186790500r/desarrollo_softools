﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Facturacion
Imports System.Transactions
Namespace Basico.Facturacion
    Public NotInheritable Class RepositorioConceptoNotasFacturas
        Inherits RepositorioBase(Of ConceptoNotasFacturas)

        Public Overrides Function Consultar(filtro As ConceptoNotasFacturas) As IEnumerable(Of ConceptoNotasFacturas)
            Dim lista As New List(Of ConceptoNotasFacturas)
            Dim item As ConceptoNotasFacturas
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Codigo) Then
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If

                If filtro.TipoDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                End If

                If Not IsNothing(filtro.Nombre) Then
                    If filtro.Nombre <> "" Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If
                If Not IsNothing(filtro.ConceptoSistema) Then
                    If filtro.ConceptoSistema > 0 Then
                        conexion.AgregarParametroSQL("@par_ConceptoSistema", filtro.ConceptoSistema)
                    End If
                End If

                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado > 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_conceptos_Notas_factura")

                While resultado.Read
                    item = New ConceptoNotasFacturas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista

        End Function

        Public Overrides Function Insertar(entidad As ConceptoNotasFacturas) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Tipo_Nota", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Concepto_Nota_Facturas")
                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While


                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As ConceptoNotasFacturas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Tipo_Nota", entidad.TipoDocumento)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_Concepto_Notas_Facturas")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As ConceptoNotasFacturas) As ConceptoNotasFacturas
            Dim item As New ConceptoNotasFacturas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_Concepto_Notas_Facturas")

                While resultado.Read
                    item = New ConceptoNotasFacturas(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As ConceptoNotasFacturas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_Conceptos_Notas_Facturas")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class
End Namespace
