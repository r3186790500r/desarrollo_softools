﻿PRINT 'gsp_eliminar_oficinas'
GO
DROP PROCEDURE gsp_eliminar_oficinas
GO
CREATE PROCEDURE gsp_eliminar_oficinas(
  @par_EMPR_Codigo SMALLINT,
  @par_Codigo NUMERIC
  )
AS
BEGIN
  
  DELETE Oficinas 
  WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND Codigo = @par_Codigo

    SELECT @@ROWCOUNT AS Numero

END
GO