﻿EncoExpresApp.controller("ConsultarLegalizarRecaudoRemesasPaqueteriaCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig',
    'ValorCatalogosFactory', 'OficinasFactory', 'TercerosFactory', 'LegalizarRecaudoRemesasFactory', 'EmpresasFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig,
        ValorCatalogosFactory, OficinasFactory, TercerosFactory, LegalizarRecaudoRemesasFactory, EmpresasFactory ) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONSULTAR LEGALIZACIÓN RECAUDO GUÍAS';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Legalizar Recaudo Guías' }];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.LEGALIZAR_RECAUDO_REMESAS);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.pref = '';
        $scope.Modelo = {
            NumeroDocumento: '',
            Planilla: '',
            Conductor: ''
        };
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.ListadoOficinas = [];
        $scope.ListadoConductor = [];
        $scope.ListadoEstados = [
            { Codigo: -1, Nombre: "(TODAS)" },
            { Codigo: 0, Nombre: "BORRADOR" },
            { Codigo: 1, Nombre: "DEFINITIVO" },
            { Codigo: 2, Nombre: "ANULADA" }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo === -1');
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy === true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete
        //--Conductor
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CONDUCTOR,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoConductor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductor);
                }
            }
            return $scope.ListadoConductor;
        };
        //--Conductor
        //----------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //-- cargar Prefijo EmPrefpresa --//
            EmpresasFactory.Consultar(
                { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
            ).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.pref = response.data.Datos[0].Prefijo;
                    //console.log("Empresa Prefijo: ", $scope.pref);
                }
            });
            //--Oficinas
            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                var responseOficina = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true });
                if (responseOficina.ProcesoExitoso === true) {
                    $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' });
                    responseOficina.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });
                    $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
                }
            }
            else {
                $scope.ListadoOficinas.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.Modelo.Oficina = $scope.ListadoOficinas[0];
            }
            //--Obtener Parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
                $scope.Modelo.NumeroDocumento = $routeParams.Numero;
                PantallaBloqueo(Find);
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            PantallaBloqueo(Find);
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            PantallaBloqueo(Find);
        };
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        //--Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarLegalizarRecaudoRemesasPaqueteria';
            }
        };
        //--Funcion Buscar
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO && DatosRequeridos()) {
                $scope.Modelo.Numero = 0;
                PantallaBloqueo(Find);
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroDocumento === null || $scope.Modelo.NumeroDocumento === undefined || $scope.Modelo.NumeroDocumento === '' || $scope.Modelo.NumeroDocumento === 0 || isNaN($scope.Modelo.NumeroDocumento) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.Modelo.NumeroDocumento !== null && $scope.Modelo.NumeroDocumento !== undefined && $scope.Modelo.NumeroDocumento !== '' && $scope.Modelo.NumeroDocumento !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }

                else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial;
                    } else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal;
                    }
                }
            }
            return continuar;
        }
        function Find() {
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.Listadolegalizacion = [];
            if ($scope.Buscando) {
                var Filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: $scope.Modelo.NumeroDocumento,
                    FechaInicial: $scope.Modelo.NumeroDocumento !== '' ? undefined : $scope.Modelo.FechaInicial,
                    FechaFinal: $scope.Modelo.NumeroDocumento !== '' ? undefined: $scope.Modelo.FechaFinal,
                    Oficina: $scope.Modelo.Oficina,
                    Estado: $scope.Modelo.Estado.Codigo,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas
                };
                LegalizarRecaudoRemesasFactory.Consultar(Filtro).
                    then(function (response) {
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.Listadolegalizacion = response.data.Datos;
                                for (var i = 0; i < $scope.Listadolegalizacion.length; i++) {
                                    if ($scope.Listadolegalizacion[i].FechaInterfazContable === FORMATO_FECHA_EXCEPCION_BD) {
                                        $scope.Listadolegalizacion[i].FechaInterfazContable = '';
                                    }
                                }
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        ShowError(response.statusText);
                    });
            }
        }
        //--Funcion Buscar
        //----------------------------Funciones Generales---------------------------------//

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroFactura = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_LEGALIZACION_RECAUDO_GUIAS;


            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            $scope.FiltroArmado = '';
            if ($scope.NumeroFactura != undefined && $scope.NumeroFactura != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroFactura;
            }
            $scope.FiltroArmado += '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo;
        };
    }]);