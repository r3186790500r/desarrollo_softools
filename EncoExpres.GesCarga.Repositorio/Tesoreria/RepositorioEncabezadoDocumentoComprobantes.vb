﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Repositorio.Contabilidad
Imports EncoExpres.GesCarga.Repositorio.ControlViajes
Imports System.Transactions

Namespace Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioEncabezadoDocumentoComprobantes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEncabezadoDocumentoComprobantes
        Inherits RepositorioBase(Of EncabezadoDocumentoComprobantes)

        Public Overrides Function Consultar(filtro As EncabezadoDocumentoComprobantes) As IEnumerable(Of EncabezadoDocumentoComprobantes)
            Dim lista As New List(Of EncabezadoDocumentoComprobantes)
            Dim item As EncabezadoDocumentoComprobantes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.TipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.NombreTercero) Then
                    conexion.AgregarParametroSQL("@par_Tercero", filtro.NombreTercero)
                End If

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If (filtro.FechaInicial > Date.MinValue) And (filtro.FechaFinal >= filtro.FechaInicial) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.NombreBeneficiario) Then
                    conexion.AgregarParametroSQL("@par_Beneficiario", filtro.NombreBeneficiario)
                End If
                If Not IsNothing(filtro.DocumentoOrigen) Then
                    If filtro.DocumentoOrigen.Codigo <> 2600 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Documento", filtro.DocumentoOrigen.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.NumeroDocumentoOrigen) Then
                    If filtro.NumeroDocumentoOrigen > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumentoOrigen)
                    End If
                End If
                If Not IsNothing(filtro.OficinaDestino) Then
                    If filtro.OficinaDestino.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina", filtro.OficinaDestino.Codigo)
                    End If
                End If
                If filtro.Estado >= 0 And filtro.Estado <> 2 And filtro.Estado <> 3 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If
                If filtro.Estado = 3 Then
                    conexion.AgregarParametroSQL("@par_Estado", 2)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If
                If filtro.CodigoTercero > 0 Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.CodigoTercero)
                End If
                If Not IsNothing(filtro.DocumentoOrigen) Then
                    If filtro.DocumentoOrigen.Codigo > 2600 Then
                        conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", filtro.DocumentoOrigen.Codigo)
                    End If
                End If
                If filtro.CodigoLegalizacionGastos > 0 Then
                    conexion.AgregarParametroSQL("@par_ELGC_Numero", 0)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_documento_comprobantes]")

                While resultado.Read
                    item = New EncabezadoDocumentoComprobantes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarCadenaEgresos(filtro As EncabezadoDocumentoComprobantes) As IEnumerable(Of EncabezadoDocumentoComprobantes)
            Dim lista As New List(Of EncabezadoDocumentoComprobantes)
            Dim item As EncabezadoDocumentoComprobantes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Cadena_Egresos", filtro.CadenaEgresos)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_cadena_egresos")

                While resultado.Read
                    item = New EncabezadoDocumentoComprobantes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarCodigosRespuesaBancolombia(filtro As EncabezadoDocumentoComprobantes) As IEnumerable(Of CodigoRespuestaBancolombia)
            Dim lista As New List(Of CodigoRespuestaBancolombia)
            Dim item As CodigoRespuestaBancolombia

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_codigos_respuesta_bancolombia")

                While resultado.Read
                    item = New CodigoRespuestaBancolombia(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarCuentasconEgresos(filtro As EncabezadoDocumentoComprobantes) As IEnumerable(Of EncabezadoDocumentoComprobantes)
            Dim lista As New List(Of EncabezadoDocumentoComprobantes)
            Dim item As EncabezadoDocumentoComprobantes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_codigo", filtro.CodigoEmpresa)

                If Not String.IsNullOrWhiteSpace(filtro.NumeroDocumento) Then
                    If filtro.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                    End If
                End If

                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                    End If
                End If
                If Not IsNothing(filtro.Tercero) Then
                    If filtro.Tercero.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Tercero.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.DocumentoOrigen) Then
                    If filtro.DocumentoOrigen.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_DOOR_Codigo", filtro.DocumentoOrigen.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_documento_cuentas_con_egresos")

                While resultado.Read
                    item = New EncabezadoDocumentoComprobantes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As EncabezadoDocumentoComprobantes) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Titular", entidad.Tercero.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Beneficiario", entidad.Beneficiario.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_FPDC_Codigo", entidad.FormaPagoDocumento.Codigo)
                    If Not IsNothing(entidad.DestinoIngreso) Then
                        conexion.AgregarParametroSQL("@par_CATA_DEIN_Codigo", entidad.DestinoIngreso.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.NumeroDocumentoOrigen)
                    conexion.AgregarParametroSQL("@par_CUBA_Codigo", entidad.CuentaBancaria.Codigo)
                    If entidad.Caja.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CAJA_Codigo", entidad.Caja.Codigo)
                    End If

                    conexion.AgregarParametroSQL("@par_Numero_Pago_Recaudo", entidad.NumeroPagoRecaudo)
                    conexion.AgregarParametroSQL("@par_Fecha_Pago_Recaudo", entidad.FechaPagoRecaudo, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo_Transferencia", entidad.ValorPagoTransferencia)
                    conexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo_Efectivo", entidad.ValorPagoEfectivo)
                    conexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo_Cheque", entidad.ValorPagocheque)
                    conexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo_Total", entidad.ValorPagoTotal)
                    conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Creacion", entidad.OficinaDestino.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Destino", entidad.OficinaDestino.Codigo)
                    conexion.AgregarParametroSQL("@par_Genero_Consignacion", entidad.GeneraConsignacion)
                    conexion.AgregarParametroSQL("@par_Valor_Alterno", entidad.ValorAlterno)
                    conexion.AgregarParametroSQL("@par_ECCO_Codigo", entidad.ConceptoContable)
                    If Not IsNothing(entidad.CentroCostos) Then
                        If entidad.CentroCostos.Codigo <> 21500 Then
                            conexion.AgregarParametroSQL("@par_Centro_Costos", entidad.CentroCostos.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Vehiculo) Then
                        conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    End If
                    If entidad.Autorizacion > 0 And entidad.Estado = 1 Then
                        conexion.AgregarParametroSQL("@par_Estado", 2)
                    Else
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    If entidad.ValorAnticipoPlanilla > 0 Then
                        conexion.AgregarParametroSQL("@par_Valor_Anticipo_Planilla", entidad.ValorAnticipoPlanilla)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_comprobantes")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While


                    resultado.Close()
                    If entidad.Autorizacion > 0 Then
                        Dim Autorizacion = New Autorizaciones
                        Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                        Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18405}
                        Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                        Autorizacion.NumeroComprobante = entidad.Codigo
                        Autorizacion.Observaciones = "Autorización generada para el sobreanticipo " + entidad.Numero.ToString() + " por valor de " + entidad.ValorPagoTotal.ToString()
                        If Not IsNothing(entidad.Vehiculo.Placa) Then
                            Autorizacion.Observaciones += ", Placa: " + entidad.Vehiculo.Placa
                        End If
                        If Not IsNothing(entidad.Tercero.NombreCompleto) Then
                            Autorizacion.Observaciones += ", Conductor: " + entidad.Tercero.NombreCompleto
                        End If
                        If Not IsNothing(entidad.Ruta) Then
                            Autorizacion.Observaciones += ", Ruta: " + entidad.Ruta
                        End If
                        Autorizacion.Observaciones += ", Observaciones: " + entidad.Observaciones
                        Autorizacion.Valor = entidad.ValorPagoTotal
                        Dim repAutorizacion = New RepositorioAutorizaciones()
                        repAutorizacion.InsertarAutorizacion(Autorizacion, conexion)
                    End If

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.Numero = entidad.Codigo

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next
                    If Not IsNothing(entidad.Cuentas) Then
                        For Each cuenta In entidad.Cuentas
                            cuenta.UsuarioCrea = entidad.UsuarioCrea
                            If Not ActualizarCuentas(cuenta, conexion) Then
                                inserto = False
                                Exit For
                            End If
                        Next
                    End If
                    If Not IsNothing(entidad.DetalleCuentas) Then 'Definitivo
                        For Each DetalleCuentas In entidad.DetalleCuentas
                            DetalleCuentas.CodigoEmpresa = entidad.CodigoEmpresa
                            DetalleCuentas.CodigoDocumentoComprobante = entidad.Codigo
                            If Not InsertarDetalleCuentas(DetalleCuentas, conexion) Then
                                inserto = False
                                Exit For
                            End If
                        Next
                    End If
                    If entidad.Estado = 1 Then
                        Dim rep As New RepositorioEncabezadoComprobantesContables
                        Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Codigo, entidad.TipoDocumento, conexion, resultado)
                    End If
                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using

            Return entidad.Numero
        End Function
        Public Function InsertarDetalle(entidad As DetalleDocumentoComprobantes, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_EDCO_Codigo", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Valor_Base", entidad.ValorBase)
            contextoConexion.AgregarParametroSQL("@par_Valor_Debito", entidad.ValorDebito)
            contextoConexion.AgregarParametroSQL("@par_Valor_Credito", entidad.ValorCredito)
            contextoConexion.AgregarParametroSQL("@par_Genera_Cuenta", entidad.GeneraCuenta)
            contextoConexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            contextoConexion.AgregarParametroSQL("@par_CATA_TEPC_Codigo", entidad.TerceroParametrizacion.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_CCPC_Codigo", entidad.CentroCostoParametrizacion.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_DOCR_Codigo", entidad.DocumentoCruce.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_documento_comprobantes]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function
        Public Overrides Function Modificar(entidad As EncabezadoDocumentoComprobantes) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Titular", entidad.Tercero.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Beneficiario", entidad.Beneficiario.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_FPDC_Codigo", entidad.FormaPagoDocumento.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_DEIN_Codigo", entidad.DestinoIngreso.Codigo)
                    conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.NumeroDocumentoOrigen)
                    conexion.AgregarParametroSQL("@par_CUBA_Codigo", entidad.CuentaBancaria.Codigo)
                    conexion.AgregarParametroSQL("@par_CAJA_Codigo", entidad.Caja.Codigo)
                    conexion.AgregarParametroSQL("@par_Numero_Pago_Recaudo", entidad.NumeroPagoRecaudo)
                    conexion.AgregarParametroSQL("@par_Fecha_Pago_Recaudo", entidad.FechaPagoRecaudo, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo_Transferencia", entidad.ValorPagoTransferencia)
                    conexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo_Efectivo", entidad.ValorPagoEfectivo)
                    conexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo_Cheque", entidad.ValorPagocheque)
                    conexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo_Total", entidad.ValorPagoTotal)
                    conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Creacion", entidad.OficinaDestino.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Destino", entidad.OficinaDestino.Codigo)
                    conexion.AgregarParametroSQL("@par_Genero_Consignacion", entidad.GeneraConsignacion)
                    conexion.AgregarParametroSQL("@par_Valor_Alterno", entidad.ValorAlterno)
                    conexion.AgregarParametroSQL("@par_ECCO_Codigo", entidad.ConceptoContable)
                    If Not IsNothing(entidad.CentroCostos) Then
                        If entidad.CentroCostos.Codigo <> 21500 Then
                            conexion.AgregarParametroSQL("@par_Centro_Costos", entidad.CentroCostos.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Vehiculo) Then
                        conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    End If
                    If entidad.Autorizacion > 0 And entidad.Estado = 1 Then
                        conexion.AgregarParametroSQL("@par_Estado", 2)
                    Else
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                    If entidad.ValorAnticipoPlanilla > 0 Then
                        conexion.AgregarParametroSQL("@par_Valor_Anticipo_Planilla", entidad.ValorAnticipoPlanilla)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_documento_comprobantes")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.Numero = entidad.Codigo

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                    If entidad.Estado = 1 Then
                        Dim rep As New RepositorioEncabezadoComprobantesContables
                        Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Codigo, entidad.TipoDocumento, conexion, resultado)
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using

            Return entidad.Numero
        End Function
        Public Overrides Function Obtener(filtro As EncabezadoDocumentoComprobantes) As EncabezadoDocumentoComprobantes
            Dim item As New EncabezadoDocumentoComprobantes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("dbo.gsp_obtener_encabezado_documento_comprobantes")

                While resultado.Read
                    item = New EncabezadoDocumentoComprobantes(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As EncabezadoDocumentoComprobantes) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                conexion.AgregarParametroSQL("@par_USUA_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_OFIC_Anula", entidad.OficinaAnula.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_anular_encabezado_documento_comprobantes]")

                While resultado.Read
                    anulo = IIf(Convert.ToInt64(resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
        Public Function InsertarDetalleCuentas(entidad As DetalleDocumentoCuentaComprobantes, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim InsertoDetallCuentas As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ENDC_Codigo", entidad.CodigoDocumentoCuenta)
            contextoConexion.AgregarParametroSQL("@par_EDCO_Codigo", entidad.CodigoDocumentoComprobante)
            contextoConexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo", entidad.ValorPago)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_documento_cuenta_comprobantes]")

            While resultado.Read
                InsertoDetallCuentas = IIf((resultado.Item("Codigo")) > 0, True, False)
            End While
            resultado.Close()
            Return InsertoDetallCuentas
        End Function
        Public Function ActualizarCuentas(entidad As EncabezadoDocumentoCuentas, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim Actualizo As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Valor_Abono", entidad.ValorPagar)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Aprueba", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_actualizar_encabezado_documento_cuentas]")

            While resultado.Read
                Actualizo = IIf((resultado.Item("Codigo")) > 0, True, False)
            End While
            resultado.Close()

            Return Actualizo
        End Function


        Public Function ActualizarEgresosArchivoRespuesta(entidad As EncabezadoDocumentoComprobantes) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True
            Dim resultado As IDataReader

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    For Each comprobante In entidad.Comprobantes
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_Numero", comprobante.Numero)
                        conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", comprobante.DocumentoOrigen.Codigo)
                        conexion.AgregarParametroSQL("@par_Fecha_Pago_Recaudo", comprobante.FechaPagoRecaudo, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Valor_Pago_Recaudo_Transferencia", comprobante.ValorPagoTransferencia, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Numero_Pago_Recaudo_Transferencia", comprobante.NumeroPagoRecaudo)
                        conexion.AgregarParametroSQL("@par_Mensaje_Proceso_Transferencia", comprobante.MensajeProcesoTransferencia)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_documento_comprobantes_archivo_respuesta")

                        While resultado.Read
                            entidad.Numero += resultado.Item("Numero").ToString()
                        End While
                        resultado.Close()
                    Next




                End Using

                transaccion.Complete()

            End Using

            Return entidad.Numero
        End Function

        Public Function ValidarSecuenciaLotes(entidad As EncabezadoDocumentoComprobantes) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True
            Dim resultado As IDataReader

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_secuencia_lotes")

                    While resultado.Read
                        entidad.Numero += resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()

                End Using

                transaccion.Complete()

            End Using

            Return entidad.Numero
        End Function
    End Class

End Namespace