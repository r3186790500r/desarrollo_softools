﻿PRINT  'gsp_consultar_departamentos'
GO
DROP PROCEDURE gsp_consultar_departamentos
GO
CREATE PROCEDURE  gsp_consultar_departamentos
(
@par_EMPR_Codigo SMALLINT
)
AS
BEGIN
	SELECT
	EMPR_Codigo,
	Codigo,
	PAIS_Codigo,
	Codigo_Alterno,
	Nombre,
	Estado
	FROM
	Departamentos
	WHERE 
	EMPR_Codigo = @par_EMPR_Codigo
END
GO