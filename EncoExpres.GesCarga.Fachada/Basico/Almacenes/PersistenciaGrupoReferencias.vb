﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Repositorio.Basico.Almacen

Namespace Basico.Almacen
    Public NotInheritable Class PersistenciaGrupoReferencias
        Implements IPersistenciaBase(Of GrupoReferencias)

        Public Function Consultar(filtro As GrupoReferencias) As IEnumerable(Of GrupoReferencias) Implements IPersistenciaBase(Of GrupoReferencias).Consultar
            Return New RepositorioGrupoReferencias().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As GrupoReferencias) As Long Implements IPersistenciaBase(Of GrupoReferencias).Insertar
            Return New RepositorioGrupoReferencias().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As GrupoReferencias) As Long Implements IPersistenciaBase(Of GrupoReferencias).Modificar
            Return New RepositorioGrupoReferencias().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As GrupoReferencias) As GrupoReferencias Implements IPersistenciaBase(Of GrupoReferencias).Obtener
            Return New RepositorioGrupoReferencias().Obtener(filtro)
        End Function
        Public Function Anular(entidad As GrupoReferencias) As Boolean
            Return New RepositorioGrupoReferencias().Anular(entidad)
        End Function
    End Class

End Namespace

