﻿EncoExpresApp.controller("ConsultarBandejaSalidaCorreosCtrl", ['$scope', '$linq', '$timeout', 'BandejaSalidaCorreosFactory', 'blockUI', 'TipoDocumentosFactory', function ($scope, $linq,  $timeout, BandejaSalidaCorreosFactory, blockUI, TipoDocumentosFactory) {
        
    $scope.MapaSitio = [{ Nombre: 'Utilitarios' },{ Nombre: 'Consultar Bandeja Salida' }];
    $scope.Buscando = false;
    $scope.ResultadoSinRegistros = '';
    $scope.MensajesError = [];
    var filtros = {};

    $scope.ListadoTipoDocumentos = [];

    $scope.totalRegistros = 0;
    $scope.paginaActual = 1;
    $scope.cantidadRegistrosPorPagina = 10;
    $scope.totalPaginas = 0;

    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }        
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };
    
    $scope.ListadoEstados = [
        { Nombre: '(TODOS)', Codigo: -1 },
        { Nombre: 'ENVIADOS', Codigo: 1 },
        { Nombre: 'NO ENVIADOS', Codigo: 0 },
    ];
    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

    TipoDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
    then(function (response) {
        if (response.data.ProcesoExitoso === true) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoTipoDocumentos = [];

                response.data.Datos.push({Nombre: '(TODOS)', Codigo:0 })

                $scope.ListadoTipoDocumentos = response.data.Datos;

                $scope.TipoDocu = $scope.ListadoTipoDocumentos[$scope.ListadoTipoDocumentos.length - 1];

            }
            else {
                $scope.ListadoTipoDocumentos = [];
            }
        }
    }, function (response) {
    });


    $scope.Buscar = function () {
        Find();
    };

    function Find() {
        blockUI.start('Buscando registros ...');
       
        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);

        $scope.Buscando = true;
        $scope.MensajesError = [];
                
        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            NumeroInicial: $scope.NumeroInicial,
            NumeroFinal: $scope.NumeroFinal,
            FechaInicial: $scope.FechaInicial,
            FechaFinal: $scope.FechaFinal,
            Pagina: $scope.paginaActual,
            RegistrosPagina: $scope.cantidadRegistrosPorPagina,
            Estado: $scope.ModalEstado.Codigo,
            TipoDocumento: $scope.TipoDocu.Codigo,
        };



        DatosRequeridos();
                        
        if ($scope.MensajesError.length == 0) {
            blockUI.delay = 1000;
            BandejaSalidaCorreosFactory.Consultar(filtros).
               then(function (response) {
                   if (response.data.ProcesoExitoso === true) {
                       $scope.ListadoBandejaSalidaCorreos = response.data.Datos;
                       if (response.data.Datos.length > 0) {
                           $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                           $scope.totalPaginas = parseInt($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                           $scope.Buscando = false;
                       }
                       else {
                           $scope.totalRegistros = 0;
                           $scope.totalPaginas = 0;
                           $scope.paginaActual = 1;
                           $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                           $scope.Buscando = false;
                       }
                   }
               }, function (response) {
                   $scope.Buscando = false;
               });
        }
        else {
            $scope.Buscando = false;
        }
        blockUI.stop();
    };

    function DatosRequeridos()
    {
        if (filtros.NumeroInicial == undefined) {
            filtros.NumeroInicial = 0;
        }

        if (filtros.NumeroFinal == undefined) {
            filtros.NumeroFinal = 0;
        }

        if (filtros.NumeroInicial > 0 && filtros.NumeroFinal > 0) {
            if (filtros.NumeroInicial > filtros.NumeroFinal) {
                $scope.MensajesError.push('El número inicial debe ser menor que el número final');
            }
        }
        else if (filtros.NumeroInicial > 0 && filtros.NumeroFinal <= 0) {
            filtros.NumeroFinal = filtros.NumeroInicial;
        }
        else if (filtros.NumeroInicial <= 0 && filtros.NumeroFinal > 0) {
            $scope.MensajesError.push('El número inicial debe ser mayor a cero');
        }


        if (filtros.FechaInicial !== null && filtros.FechaFinal !== null) {
            if (filtros.FechaInicial > filtros.FechaFinal) {
                $scope.MensajesError.push('La fecha inicial debe ser menor que la fecha final');
            }
        }
        else if (filtros.FechaInicial !== null && filtros.FechaFinal == null) {
            filtros.FechaFinal = filtros.FechaInicial;
        }
        else if (filtros.FechaInicial == null && filtros.FechaFinal !== null) {
            $scope.MensajesError.push('Debe seleccionar la fecha inicial');
        }
    }

}]);