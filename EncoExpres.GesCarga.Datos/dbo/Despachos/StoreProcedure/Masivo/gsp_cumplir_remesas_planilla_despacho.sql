﻿PRINT 'gsp_cumplir_remesas_planilla_despacho'
GO

DROP PROCEDURE gsp_cumplir_remesas_planilla_despacho
GO

CREATE PROCEDURE gsp_cumplir_remesas_planilla_despacho
(
	@par_EMPR_Codigo NUMERIC,
	@par_NumeroPlanilla NUMERIC,
	@par_NumeroCumplido NUMERIC,
	@par_ENRE_Numero NUMERIC,
	@par_Fecha_Recibe DATETIME,
	@par_Nombre_Recibe VARCHAR(100),
	@par_Numero_Identificacion_Recibe VARCHAR(30) = NULL,
	@par_Telefonos_Recibe VARCHAR(50) = NULL,
	@par_Observaciones_Recibe VARCHAR(500) = NULL,
	@par_Cantidad_Recibe NUMERIC,
	@par_Peso_Recibe NUMERIC,
	@par_USUA_Codigo_Crea INT,
	@par_OFIC_Codigo INT
)
AS
BEGIN

	DECLARE @CantRegis INT

	SET @CantRegis = (SELECT COUNT(*) FROM Cumplido_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero)

	IF @CantRegis = 0 BEGIN
		INSERT INTO Cumplido_Remesas
		(
			EMPR_Codigo,
			ENRE_Numero,
			Fecha_Recibe,
			Nombre_Recibe,
			Numero_Identificacion_Recibe,
			Telefonos_Recibe,
			Observaciones_Recibe,
			Cantidad_Recibe,
			Peso_Recibe,
			Fecha_Crea,
			USUA_Codigo_Crea,
			OFIC_Codigo
		)
		VALUES
		(
			@par_EMPR_Codigo,
			@par_ENRE_Numero,
			@par_Fecha_Recibe,
			@par_Nombre_Recibe,
			@par_Numero_Identificacion_Recibe,
			@par_Telefonos_Recibe,
			@par_Observaciones_Recibe,
			@par_Cantidad_Recibe,
			@par_Peso_Recibe,
			GETDATE(),
			@par_USUA_Codigo_Crea,
			@par_OFIC_Codigo
		)
	END
	ELSE BEGIN
		UPDATE
			Cumplido_Remesas
		SET
			Numero_Identificacion_Recibe = @par_Numero_Identificacion_Recibe,
			Nombre_Recibe = @par_Nombre_Recibe,
			Telefonos_Recibe = @par_Telefonos_Recibe,
			Fecha_Recibe = @par_Fecha_Recibe,
			Cantidad_Recibe = @par_Cantidad_Recibe,
			Peso_Recibe = @par_Peso_Recibe,
			Observaciones_Recibe = @par_Observaciones_Recibe
		WHERE
			EMPR_Codigo = @par_EMPR_Codigo
			AND ENRE_Numero = @par_ENRE_Numero
	END

	UPDATE
		Encabezado_Planilla_Despachos
	SET
		ECPD_Numero = @par_NumeroCumplido
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Numero = @par_NumeroPlanilla

	UPDATE
		Encabezado_Remesas
	SET
		ECPD_Numero = @par_NumeroCumplido,
		Cumplido = 1
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Numero = @par_ENRE_Numero
		AND ENPD_Numero = @par_NumeroPlanilla

	UPDATE
		Encabezado_Manifiesto_Carga
	SET
		ECPD_Numero = @par_NumeroCumplido
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND ENPD_Numero = @par_NumeroPlanilla


	SELECT @par_ENRE_Numero AS ENRE_Numero
END
GO