﻿PRINT 'gsp_eliminar_cajas'
GO
DROP PROCEDURE gsp_eliminar_cajas
GO
CREATE PROCEDURE gsp_eliminar_cajas
(
  @par_EMPR_Codigo SMALLINT,
  @par_Codigo NUMERIC
  )
AS
BEGIN
  DELETE Cajas 
  WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND Codigo = @par_Codigo

    SELECT @@ROWCOUNT AS Numero
END
GO