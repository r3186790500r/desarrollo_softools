USE [ENCOEXPRES]
GO

/****** Object:  StoredProcedure [dbo].[gsp_modificar_encabezado_planilla_paqueteria]    Script Date: 17/02/2022 10:29:55 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER   PROCEDURE [dbo].[gsp_modificar_encabezado_planilla_paqueteria]
(                                  
 @par_EMPR_Codigo smallint                                   
 ,@par_Numero numeric(18,0)                                   
 ,@par_Fecha datetime  = NULL                                  
 ,@par_Fecha_Hora_Salida datetime  = NULL 
 ,@par_Fecha_Entrega datetime  = NULL           
 ,@par_RUTA_Codigo numeric(18,0) = NULL                                  
                                  
 ,@par_VEHI_Codigo numeric(18,0) = NULL                                  
 ,@par_SEMI_Codigo numeric(18,0) = NULL                                  
 ,@par_TERC_Codigo_Tenedor numeric(18,0) = NULL                                  
 ,@par_TERC_Codigo_Conductor numeric(18,0) = NULL                                  
 ,@par_Cantidad numeric(18,2) = NULL                                  
                                  
 ,@par_Peso numeric(18,2) = NULL                                  
 ,@par_TATC_Codigo smallint = NULL                                  
 ,@par_TTTC_Codigo smallint = NULL    
 ,@par_Valor_Flete_Transportador money = NULL                            
                                  
 ,@par_Valor_Anticipo money = NULL                                  
 ,@par_Valor_Impuestos money = NULL                                  
 ,@par_Valor_Pagar_Transportador money = NULL                                  
 ,@par_Valor_Flete_Cliente money = NULL                                  
      
 ,@par_LNTC_Codigo_Compra NUMERIC = NULL      
 ,@par_TLNC_Codigo_Compra NUMERIC = NULL      
 ,@par_ETCC_Numero NUMERIC = NULL      
 ,@par_Valor_Auxiliares money  = NULL        
 ,@par_Numeracion varchar(50) = NULL      
                        
 ,@par_Observaciones varchar(200) = NULL                                  
 ,@par_Estado smallint = NULL                                  
 ,@par_USUA_Codigo_Crea smallint    
 ,@par_ENRE_Numero_Documento_Masivo  numeric = null                            
 ,@par_ENRE_Numero_Masivo  numeric = null
 ,@par_Precinto NUMERIC = NULL       
 
 ,@par_OFIC_Destino smallint = null
)                                  
AS                                  
BEGIN      
 UPDATE Encabezado_Remesas SET ENPD_Numero = 0 where EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero    
 UPDATE Encabezado_Recolecciones SET ENPR_Numero = 0 where  EMPR_Codigo = @par_EMPR_Codigo AND ENPR_Numero = @par_Numero    
 DELETE Detalle_Planilla_Despachos where  EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero                                  
 DELETE Detalle_Planilla_Recolecciones where  EMPR_Codigo = @par_EMPR_Codigo AND ENPR_Numero = @par_Numero                                  
 DELETE Detalle_Auxiliares_Planilla_Despachos where  EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero                                  
 DELETE Detalle_Impuestos_Planilla_Despachos where  EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero                                  
 DELETE Detalle_Conductores_Planilla_Despachos where  EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero       
 
update Detalle_Asignacion_Precintos_Oficina SET ENPD_Numero = 0
where EMPR_Codigo = @par_EMPR_Codigo
and ENPD_Numero = @par_Numero
 
                        
 UPDATE Encabezado_Planilla_Despachos                               
 set Fecha_Hora_Salida = ISNULL(@par_Fecha_Hora_Salida ,'')                                  
 ,Fecha = ISNULL(@par_Fecha,'')                                  
 ,RUTA_Codigo = ISNULL(@par_RUTA_Codigo ,0)                                  
 ,VEHI_Codigo = ISNULL(@par_VEHI_Codigo ,0)          
 ,SEMI_Codigo = ISNULL(@par_SEMI_Codigo ,0)                                  
 ,TERC_Codigo_Tenedor =  ISNULL(@par_TERC_Codigo_Tenedor ,0)                                  
 ,TERC_Codigo_Conductor = ISNULL(@par_TERC_Codigo_Conductor ,0)                               
 ,Cantidad = ISNULL(@par_Cantidad ,0)                                  
 ,Peso = ISNULL(@par_Peso ,0)                                  
 ,Valor_Flete_Transportador =  ISNULL(@par_Valor_Flete_Transportador ,0)                                  
 ,Valor_Anticipo = ISNULL(@par_Valor_Anticipo ,0)                                  
 ,Valor_Impuestos  = ISNULL(@par_Valor_Impuestos ,0)                                  
                            
 ,Valor_Pagar_Transportador = ISNULL(@par_Valor_Pagar_Transportador ,0)  
 ,Valor_Flete_Cliente = ISNULL(@par_Valor_Flete_Cliente ,0)    
 ,Observaciones = ISNULL(@par_Observaciones,'')                                  
 ,Estado =   ISNULL(@par_Estado ,0)                                  
 ,Fecha_Modifica = GETDATE()                                   
 ,USUA_Codigo_Modifica = @par_USUA_Codigo_Crea    
 ,ENRE_Numero_Masivo =@par_ENRE_Numero_Masivo                    
 ,ENRE_Numero_Documento_Masivo = @par_ENRE_Numero_Documento_Masivo    
 ,TATC_Codigo_Compra = ISNULL(@par_TATC_Codigo ,TATC_Codigo_Compra)      
 ,TTTC_Codigo_Compra = ISNULL(@par_TTTC_Codigo ,TTTC_Codigo_Compra)      
 ,LNTC_Codigo_Compra = ISNULL(@par_LNTC_Codigo_Compra ,LNTC_Codigo_Compra)      
 ,TLNC_Codigo_Compra = ISNULL(@par_TLNC_Codigo_Compra ,TLNC_Codigo_Compra)      
 ,Valor_Auxiliares = ISNULL(@par_Valor_Auxiliares ,Valor_Auxiliares)      
 ,Numeracion = ISNULL(@par_Numeracion ,Numeracion)
 ,Precinto_Paqueteria = @par_Precinto
 ,OFIC_Destino = @par_OFIC_Destino
 ,Fecha_Recibe = @par_Fecha_Entrega
 where  EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_Numero
 
 IF @par_Precinto IS NOT NULL
 BEGIN
	update Detalle_Asignacion_Precintos_Oficina SET ENPD_Numero = @par_Numero
	where EMPR_Codigo = @par_EMPR_Codigo
	and Numero_Precinto = @par_Precinto
 END                              
     
 select Numero, Numero_Documento from Encabezado_Planilla_Despachos  where EMPR_Codigo = @par_EMPR_Codigo AND  Numero = @par_Numero                             
END
GO


