﻿PRINT 'gsp_listado_planillas_pendientes_asignar_tiempos'
GO
DROP PROCEDURE gsp_listado_planillas_pendientes_asignar_tiempos
GO
CREATE PROCEDURE gsp_listado_planillas_pendientes_asignar_tiempos
(
	@par_EMPR_Codigo SMALLINT,
	@par_Fecha_Inicial DATE = NULL,
	@par_Fecha_Final DATE = NULL,
	@OFIC_Codigo SMALLINT = NULL
)
AS
BEGIN
	Declare @Tiemposplanilla Int = 0;
	--Validacion cantidad tiempos para planillas
	SELECT @Tiemposplanilla = Count(Codigo) FROM Valor_Catalogos 
	WHERE CATA_Codigo = 82 AND campo2 is not null and EMPR_Codigo = @par_EMPR_Codigo

	SELECT 
	EMPR.Nombre_Razon_Social,
	@par_Fecha_Inicial AS FechaInicial,
	@par_Fecha_Final AS FechaFinal,
	ENPD.Numero, 
	ENPD.Numero_Documento,
	ENPD.Fecha,
	ISNULL(CIOR.Nombre, '') AS CIUD_Origen_Nombre,
	ISNULL(CIDE.Nombre, '') AS CIUD_Destino_Nombre,
	OFIC.Nombre AS Oficina,
	USUA.Codigo_Usuario

	from Encabezado_Planilla_Despachos as ENPD

	INNER JOIN Empresas EMPR ON
	ENPD.EMPR_Codigo = EMPR.Codigo

	LEFT JOIN Rutas RUTA ON
	ENPD.EMPR_Codigo = RUTA.EMPR_Codigo
	AND ENPD.RUTA_Codigo = RUTA.Codigo

	LEFT JOIN Ciudades CIOR ON
	RUTA.EMPR_Codigo = CIOR.EMPR_Codigo
	AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo

	LEFT JOIN Ciudades CIDE ON
	RUTA.EMPR_Codigo = CIDE.EMPR_Codigo
	AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo

	LEFT JOIN Oficinas OFIC ON
	ENPD.EMPR_Codigo = OFIC.EMPR_Codigo
	AND ENPD.OFIC_Codigo = OFIC.Codigo

	LEFT JOIN Usuarios USUA ON
	ENPD.USUA_Codigo_Crea = USUA.Codigo
	AND ENPD.USUA_Codigo_Crea = USUA.Codigo

	WHERE ENPD.EMPR_Codigo = @par_EMPR_Codigo
	AND
	(
		SELECT count(DTPD.ENPD_Numero) from Detalle_Tiempos_Planilla_Despachos DTPD 
		where DTPD.ENPD_Numero = ENPD.Numero
	) < @Tiemposplanilla
	AND ENPD.Fecha >= ISNULL(@par_Fecha_Inicial, ENPD.Fecha)
	AND ENPD.Fecha <= ISNULL(@par_Fecha_Final, ENPD.Fecha)
	AND ENPD.OFIC_Codigo = ISNULL(@OFIC_Codigo, ENPD.OFIC_Codigo)
	AND ENPD.Estado = 1--Estado Definitivo
END
GO