﻿PRINT 'Catalogo Tipo Documento PSL - 200 - DPSL'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 200
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 200
GO
DELETE Catalogos WHERE Codigo = 200
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos (EMPR_Codigo, Codigo, Nombre_Corto, Nombre, Actualizable, Numero_Columnas, Numero_Campos_Llave)
SELECT Codigo, 200, 'DPSL','Tipo Documento PS', 0, 2, 1 FROM Empresas
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 200, 20001, 'Solicitud Anticipo', '85', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 200, 20002, 'Egresos', '30', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 200, 20003, 'Liquidaciones', '160', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 200, 20004, 'Legalización Gastos', '230', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 200, 20005, 'Factura Venta', '170', '', '', 1, 1, GETDATE() FROM Empresas
GO