﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 13/08/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_consultar_paises'
GO
DROP PROCEDURE gsp_consultar_paises
GO
CREATE PROCEDURE gsp_consultar_paises 
 (       
  @par_EMPR_Codigo SMALLINT,    
  @par_Codigo NUMERIC = NULL,    
  @par_Nombre VARCHAR(50) = NULL,    
  @par_Codigo_Alterno VARCHAR(30) = NULL,    
  @par_Estado SMALLINT = NULL,    
  @par_NumeroPagina INT = NULL,    
  @par_RegistrosPagina INT = NULL    
 )    
 AS    
 BEGIN    
  SET NOCOUNT ON;    
   DECLARE    
    @CantidadRegistros INT    
   SELECT @CantidadRegistros = (    
     SELECT DISTINCT     
      COUNT(1)     
      FROM    
      Paises    
    
      WHERE    
      EMPR_Codigo = @par_EMPR_Codigo    
      AND Codigo > 0    
	  AND Codigo <> 202 
      AND Codigo = ISNULL(@par_Codigo, Codigo)    
      AND Estado = ISNULL(@par_Estado, Estado)    
      AND ((Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))    
      AND ((Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))    
      );    
                
     WITH Pagina AS    
     (    
    
      SELECT     
      EMPR_Codigo,    
      Codigo,    
      Codigo_Alterno,    
      Nombre,    
      MONE_Codigo,    
      Estado,    
      Nombre_Corto,    
      ROW_NUMBER() OVER(ORDER BY Nombre) AS RowNumber    
      FROM    
      Paises    
      WHERE    
      EMPR_Codigo = @par_EMPR_Codigo    
      AND Codigo > 0    
	  AND Codigo <> 202 
      AND Codigo = ISNULL(@par_Codigo, Codigo)    
      AND Estado = ISNULL(@par_Estado, Estado)    
      AND ((Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))    
      AND ((Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))    
    
   )    
   SELECT DISTINCT    
   0 AS Obtener,    
   EMPR_Codigo,    
   Codigo,    
   Codigo_Alterno,    
   Nombre,    
   MONE_Codigo,    
   Estado,    
   Nombre_Corto,    
   @CantidadRegistros AS TotalRegistros,    
   @par_NumeroPagina AS PaginaObtener,    
   @par_RegistrosPagina AS RegistrosPagina    
   FROM    
    Pagina    
   WHERE    
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
   order by Nombre    
   END       
GO