﻿PRINT 'gsp_consultar_vehiculos'
GO
DROP PROCEDURE gsp_consultar_vehiculos
GO
CREATE PROCEDURE gsp_consultar_vehiculos 
(          
@par_EMPR_Codigo SMALLINT,          
@par_Codigo NUMERIC = NULL,          
@par_Placa VARCHAR(15) = NULL,          
@par_Semiremolque VARCHAR(15) = NULL,          
@par_Codigo_Alterno VARCHAR(15) = NULL,          
@par_Tenedor VARCHAR(100) = NULL,          
@par_Conductor VARCHAR(100) = NULL,          
@par_Estado NUMERIC = NULL,          
@par_Tipo_Vehiculo NUMERIC = NULL,      
@par_NumeroPagina INT = null,           
@par_RegistrosPagina INT = NULL          
)          
AS          
BEGIN          
SET NOCOUNT ON;          
DECLARE          
@CantidadRegistros  INT          
SELECT @CantidadRegistros = (          
SELECT DISTINCT           
COUNT(1)           
FROM          
Vehiculos AS VEHI         
LEFT JOIN Semirremolques SEMI ON        
VEHI.EMPR_Codigo = SEMI.EMPR_Codigo          
AND VEHI.SEMI_Codigo = SEMI.Codigo  
  
LEFT JOIN Terceros AS COND ON  
COND.EMPR_Codigo = VEHI.EMPR_Codigo          
AND COND.Codigo = VEHI.TERC_Codigo_Conductor         
  
LEFT JOIN Terceros AS TENE ON  
TENE.EMPR_Codigo = VEHI.EMPR_Codigo          
AND TENE.Codigo = VEHI.TERC_Codigo_Tenedor        
  
LEFT JOIN Valor_Catalogos AS TIVE ON  
VEHI.EMPR_Codigo = TIVE.EMPR_Codigo          
AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo   
WHERE          
VEHI.Codigo != 0          
and VEHI.Codigo = isnull(@par_Codigo,VEHI.Codigo)          
AND VEHI.EMPR_Codigo = @par_EMPR_Codigo          
AND ((VEHI.Placa LIKE '%' + @par_Placa + '%') OR (@par_Placa IS NULL))          
AND VEHI.Codigo_Alterno = ISNULL(@par_Codigo_Alterno,VEHI.Codigo_Alterno)          
AND (ISNULL(TENE.Razon_Social,'') + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') LIKE '%'+@par_Tenedor+'%' or @par_Tenedor is null)               
AND (ISNULL(COND.Razon_Social,'') + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') LIKE '%'+@par_Conductor+'%' or @par_Conductor is null)               
AND VEHI.Estado = ISNULL(@par_Estado,VEHI.Estado)          
AND (SEMI.Placa = @par_Semiremolque OR @par_Semiremolque IS NULL)    
AND VEHI.CATA_TIVE_Codigo = ISNULL(@par_Tipo_Vehiculo,VEHI.CATA_TIVE_Codigo)      
);                 
WITH Pagina AS          
(          
          
SELECT          
COND.EMPR_Codigo,          
VEHI.Placa,          
VEHI.Codigo,          
VEHI.Codigo_Alterno,          
ISNULL(COND.Razon_Social,'') + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,          
COND.Numero_Identificacion As IdentificacionConductor,          
VEHI.TERC_Codigo_Conductor,          
ISNULL(TENE.Razon_Social,'') + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') As NombreTenedor,          
TENE.Numero_Identificacion As IdentificacionTenedor,          
VEHI.TERC_Codigo_Tenedor,          
VEHI.Estado,          
VEHI.Capacidad_Kilos AS Capacidad,          
CASE WHEN VEHI.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END AS EstadoVehiculo,          
VEHI.CATA_TIVE_Codigo,          
TIVE.Campo1 as TipoVehiculo,          
SEMI.Placa as PlacaSemirremolque,          
SEMI.Codigo as CodigoSemirremolque,          
ROW_NUMBER() OVER(ORDER BY VEHI.Codigo) AS RowNumber          
FROM          
Vehiculos AS VEHI         
LEFT JOIN Semirremolques SEMI ON        
VEHI.EMPR_Codigo = SEMI.EMPR_Codigo          
AND VEHI.SEMI_Codigo = SEMI.Codigo   
  
LEFT JOIN Terceros AS COND ON  
COND.EMPR_Codigo = VEHI.EMPR_Codigo          
AND COND.Codigo = VEHI.TERC_Codigo_Conductor         
  
LEFT JOIN Terceros AS TENE ON  
TENE.EMPR_Codigo = VEHI.EMPR_Codigo          
AND TENE.Codigo = VEHI.TERC_Codigo_Tenedor        
  
LEFT JOIN Valor_Catalogos AS TIVE ON  
VEHI.EMPR_Codigo = TIVE.EMPR_Codigo          
AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo         
WHERE          
VEHI.Codigo != 0          
and VEHI.Codigo = isnull(@par_Codigo,VEHI.Codigo)          
AND VEHI.EMPR_Codigo = @par_EMPR_Codigo          
AND ((VEHI.Placa LIKE '%' + @par_Placa + '%') OR (@par_Placa IS NULL))          
AND VEHI.Codigo_Alterno = ISNULL(@par_Codigo_Alterno,VEHI.Codigo_Alterno)          
AND (ISNULL(TENE.Razon_Social,'') + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') LIKE '%'+ltrim(rtrim(@par_Tenedor))+'%' or @par_Tenedor is null)               
AND (ISNULL(COND.Razon_Social,'') + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') LIKE '%'+@par_Conductor+'%' or @par_Conductor is null)             
AND VEHI.Estado = ISNULL(@par_Estado,VEHI.Estado)          
AND (SEMI.Placa = @par_Semiremolque OR @par_Semiremolque IS NULL)    
AND VEHI.CATA_TIVE_Codigo = ISNULL(@par_Tipo_Vehiculo,VEHI.CATA_TIVE_Codigo)      
)          
SELECT DISTINCT          
0 AS Obtener,          
EMPR_Codigo,          
Placa,          
Codigo,          
Codigo_Alterno,          
NombreConductor,          
IdentificacionConductor,          
TERC_Codigo_Conductor,          
NombreTenedor,          
IdentificacionTenedor,          
TERC_Codigo_Tenedor,          
Estado,          
Capacidad,          
EstadoVehiculo,          
CATA_TIVE_Codigo,          
TipoVehiculo,          
PlacaSemirremolque,          
CodigoSemirremolque AS SEMI_Codigo,          
@CantidadRegistros AS TotalRegistros,          
@par_NumeroPagina AS PaginaObtener,          
@par_RegistrosPagina AS RegistrosPagina          
FROM          
Pagina          
WHERE          
RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)            
AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
ORDER BY Codigo ASC          
          
END          
GO	