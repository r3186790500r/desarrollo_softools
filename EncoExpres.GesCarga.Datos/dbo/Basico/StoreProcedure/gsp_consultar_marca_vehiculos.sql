﻿PRINT 'gsp_consultar_marca_vehiculos'
GO
DROP PROCEDURE gsp_consultar_marca_vehiculos
GO
CREATE PROCEDURE gsp_consultar_marca_vehiculos
(
	@par_EMPR_Codigo SMALLINT,
	@par_Codigo NUMERIC = NULL,
	@par_Codigo_Alterno VARCHAR(20) = NULL,
	@par_Nombre VARCHAR(50) = NULL,
	@par_Estado INT = NULL,
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
		DECLARE
			@CantidadRegistros	INT
		SELECT @CantidadRegistros = (
				SELECT DISTINCT 
					COUNT(1) 
				FROM
					Marca_Vehiculos MAVE
				WHERE
					MAVE.Codigo <> 0
					AND MAVE.EMPR_Codigo = @par_EMPR_Codigo
					AND MAVE.Estado = ISNULL(@par_Estado, MAVE.Estado)
					AND MAVE.Codigo = ISNULL(@par_Codigo, MAVE.Codigo)
					AND ((MAVE.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))
					AND ((MAVE.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))
					);
					       
				WITH Pagina AS
				(

				SELECT
					0 AS Obtener,
					MAVE.EMPR_Codigo,
					MAVE.Codigo,
					MAVE.Codigo_Alterno,
					MAVE.Nombre,
					MAVE.Estado,
					ROW_NUMBER() OVER(ORDER BY MAVE.Nombre) AS RowNumber
				FROM
					Marca_Vehiculos MAVE
				WHERE
					MAVE.Codigo <> 0
					AND MAVE.EMPR_Codigo = @par_EMPR_Codigo
					AND MAVE.Estado = ISNULL(@par_Estado, MAVE.Estado)
					AND MAVE.Codigo = ISNULL(@par_Codigo, MAVE.Codigo)
					AND ((MAVE.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))
					AND ((MAVE.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))
		)
		SELECT DISTINCT
		0 AS Obtener,
		EMPR_Codigo,
		Codigo,
		Codigo_Alterno,
		Nombre,
		Estado,
		@CantidadRegistros AS TotalRegistros,
		@par_NumeroPagina AS PaginaObtener,
		@par_RegistrosPagina AS RegistrosPagina
		FROM
			Pagina
		WHERE
			RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
			AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
		order by Nombre
END
GO