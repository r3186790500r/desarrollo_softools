﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Entidades.Utilitarios

Namespace ControlViajes

    ''' <summary>
    ''' Clase <see cref="DetalleSeguimientoVehiculos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleSeguimientoVehiculos
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="DetalleSeguimientoVehiculos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleSeguimientoVehiculos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            TipoConsulta = Read(lector, "Tipo_Consulta")
            CodigoEvento = Read(lector, "CodigoEvento")
            If TipoConsulta = 3 Then
                NumeroRemesa = Read(lector, "ENRE_Numero")
                NumeroDocumentoRemesa = Read(lector, "Numero_Documento_Remesa")
                Cliente = New Terceros With {.NombreCompleto = Read(lector, "Nombre_Cliente")}
                Cantidad = Read(lector, "Cantidad_Cliente")
                Peso = Read(lector, "Peso_Cliente")
                SitioReporteSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_SRSV_Codigo")}
                NovedadSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NOSV_Codigo")}
                ReportarCliente = Read(lector, "Reportar_Cliente")
                TotalRegistros = Read(lector, "TotalRegistros")
            ElseIf TipoConsulta = 4 Then
                Codigo = Read(lector, "ID")
                NumeroRemesa = Read(lector, "ENRE_Numero")
                NumeroDocumentoRemesa = Read(lector, "Numero_Documento_Remesa")
                FechaReporte = Read(lector, "Fecha_Reporte")
                FechaRemesa = Read(lector, "Fecha_Remesa")
                Cliente = New Terceros With {.NombreCompleto = Read(lector, "Nombre_Cliente")}
                DocumentoCliente = Read(lector, "Documento_Cliente")
                Longitud = Read(lector, "Longitud")
                Latitud = Read(lector, "Latitud")
                TipoOrigenSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TOSV_Codigo"), .Nombre = Read(lector, "Tipo_Origen_Seguimiento")}
                SitioReporteSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_SRSV_Codigo"), .Nombre = Read(lector, "Sitio_Reporte_Seguimiento")}
                NovedadSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NOSV_Codigo"), .Nombre = Read(lector, "Novedad_Seguimiento")}
                Observaciones = Read(lector, "Observaciones")
                ReportarCliente = Read(lector, "Reportar_Cliente")
                Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Vehiculo")}
                Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Ruta")}
                FechaUltimoReporte = Read(lector, "Fecha_Ultimo_Reporte")
                Orden = Read(lector, "Orden")
                TotalRegistros = Read(lector, "TotalRegistros")
            ElseIf TipoConsulta = 5 Then
                Codigo = Read(lector, "CATA_NOSV_Codigo")
                Nombre = Read(lector, "Novedad_Seguimiento")
            ElseIf TipoConsulta = 6 Then
                CodigoEmpresa = Read(lector, "EMPR_Codigo")
                PuestoControl = New PuestoControles With {.Codigo = Read(lector, "PUCO_Codigo"), .Nombre = Read(lector, "Nombre_Puesto_Control")}
                Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Nombre_Ruta")}
                Orden = Read(lector, "Orden")
                HorasEstimadasArribo = Read(lector, "Horas_Estimadas_Arribo")
            ElseIf TipoConsulta = 7 Then 'Flota Propia
                Codigo = Read(lector, "ID")
                Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Vehiculo")}
                Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Nombre_Ruta")}
            Else
                CodigoEmpresa = Read(lector, "EMPR_Codigo")
                NumeroDocumentoPlanilla = Read(lector, "Numero_Documento_Planilla")
                Oficina = New Oficinas With {.Codigo = Read(lector, "ENPD_OFIC_Codigo")}
                Orden = Read(lector, "Orden")
                NumeroManifiesto = Read(lector, "ENMC_Numero")
                NumeroDocumentoManifiesto = Read(lector, "ENMC_Numero_Documento")
                Codigo = Read(lector, "ID")
                Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Vehiculo")}
                Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "Semirremolque")}
                Tenedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .NombreCompleto = Read(lector, "Nombre_Tenedor")}
                Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .NombreCompleto = Read(lector, "Nombre_Conductor")}
                ProveedorGPS = New Terceros With {.NombreCompleto = Read(lector, "Nombre_Proveedor_GPS")}
                Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Ruta")}
                Prioritario = Read(lector, "Prioritario")
                ReportarCliente = Read(lector, "Reportar_Cliente")
                Longitud = Read(lector, "Longitud")
                Latitud = Read(lector, "Latitud")
                Obtener = Read(lector, "Obtener")
                If Obtener = 0 Then
                    TipoOrigenSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TOSV_Codigo"), .Nombre = Read(lector, "Tipo_Origen_Seguimiento")}
                    SitioReporteSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_SRSV_Codigo"), .Nombre = Read(lector, "Sitio_Reporte_Seguimiento")}
                    NovedadSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NOSV_Codigo"), .Nombre = Read(lector, "Novedad_Seguimiento")}

                    PuestoControl = New PuestoControles With {.Codigo = Read(lector, "PUCO_Codigo"), .Nombre = Read(lector, "Puesto_Control"), .TiempoEstimado = Read(lector, "Tiempo_Estimado_Arribo")}
                    FechaReporte = Read(lector, "Fecha_Reporte")
                    FechaUltimoReporte = Read(lector, "Fecha_Ultimo_Reporte")
                    Anulado = Read(lector, "Anulado")
                    DesfaseSeguimiento = Read(lector, "Desfase_Seguimiento")
                    TotalRegistros = Read(lector, "TotalRegistros")
                ElseIf Obtener = 1 Then
                    Conductor = New Terceros With {.NombreCompleto = Read(lector, "Nombre_Conductor"), .NumeroIdentificacion = Read(lector, "Identificacion_Conductor"), .Celular = Read(lector, "Celular_Conductor")}
                    Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Vehiculo"), .TipoVehiculo = New ValorCatalogos With {.Nombre = Read(lector, "Tipo_Vehiculo")}}
                    URLGPS = Read(lector, "URL_GPS")
                    UsuarioGPS = Read(lector, "Usuario_GPS")
                    ClaveGPS = Read(lector, "Clave_GPS")
                    IdentificadorGPS = Read(lector, "IdentificadorGPS")
                    SitioReporteSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_SRSV_Codigo")}
                    NovedadSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NOSV_Codigo")}
                    PuestoControl = New PuestoControles With {.Codigo = Read(lector, "PUCO_Codigo"), .Nombre = Read(lector, "Nombre_Puesto_Control"), .TiempoEstimado = Read(lector, "Tiempo_Estimado_Arribo")}
                End If
            End If
            NumeroOrden = Read(lector, "ENOC_Numero")
            NumeroDocumentoOrden = Read(lector, "Numero_Documento_Orden")
            NumeroPlanilla = Read(lector, "ENPD_Numero")
            URLGPS = Read(lector, "URL_GPS")
            ReferenciasPersonales = Read(lector, "Referencias_Personales")
            NombreCliente = Read(lector, "Cliente")
            CodigoCliente = Read(lector, "CodigoCliente")
            strOrigen = Read(lector, "CiudadOrigen")
            strDestino = Read(lector, "CiudadDestino")
            strTipoDueno = Read(lector, "TipoDueno")
            strCarroceria = Read(lector, "Carroceria")
            strProducto = Read(lector, "Producto")
            UsuarioCrea = New Usuarios With {.Nombre = Read(lector, "UsuarioCrea")}
            Ubicacion = Read(lector, "Ubicacion")
            Observaciones = Read(lector, "Observaciones")
            FechaCrea = Read(lector, "Fecha_Crea")
            FechaReporte = Read(lector, "Fecha_Reporte")
            Empaque = Read(lector, "UnidadEmpaque")
            NumeroContenedor = Read(lector, "Numero_Contenedor")
            CiudadDevolucion = New Ciudades With {.Nombre = Read(lector, "CiudadDevolucion"), .Codigo = Read(lector, "CIUD_Codigo_Devolucion_Contenedor")}
            FechaDevolucion = Read(lector, "Fecha_Devolcuion")
            PatioDevolucion = Read(lector, "Patio_Devolcuion")
            CodigoTipoLineaNegocio = Read(lector, "TLNC_Codigo")
            CorreoConductor = Read(lector, "CorreoConductor")
            NumeroLegalizacion = Read(lector, "ELGC_Numero")
            NumeroDocumentoRemesa = Read(lector, "Numero_Documento_Remesa")
            Cliente = New Terceros With {.NombreCompleto = Read(lector, "Nombre_Cliente")}
            DocumentoCliente = Read(lector, "Documento_Cliente")
            FechaRemesa = Read(lector, "Fecha_Remesa")
        End Sub
        <JsonProperty>
        Public Property NombreCortoEmpresa As String

        <JsonProperty>
        Public Property IdentificadorGPS As String
        <JsonProperty>
        Public Property CodigoCliente As Double
        <JsonProperty>
        Public Property HorasEstimadasArribo As Integer
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property DocumentoCliente As String
        <JsonProperty>
        Public Property DocumentoDistribucionCliente As String
        <JsonProperty>
        Public Property Consulta As String
        <JsonProperty>
        Public Property FechaRemesa As DateTime
        <JsonProperty>
        Public Property editable As Boolean
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property NumeroDocumentoRemesa As Integer
        <JsonProperty>
        Public Property NumeroPlanilla As Integer
        <JsonProperty>
        Public Property NumeroDocumentoPlanilla As Integer
        <JsonProperty>
        Public Property Orden As Integer
        <JsonProperty>
        Public Property NumeroOrden As Integer
        <JsonProperty>
        Public Property NumeroDocumentoOrden As Integer
        <JsonProperty>
        Public Property NumeroManifiesto As Integer
        <JsonProperty>
        Public Property NumeroDocumentoManifiesto As Integer
        <JsonProperty>
        Public Property TipoOrigenSeguimiento As ValorCatalogos
        <JsonProperty>
        Public Property SitioReporteSeguimiento As ValorCatalogos
        <JsonProperty>
        Public Property NovedadSeguimiento As ValorCatalogos
        <JsonProperty>
        Public Property Ubicacion As String
        <JsonProperty>
        Public Property Longitud As Double
        <JsonProperty>
        Public Property Latitud As Double
        <JsonProperty>
        Public Property PuestoControl As PuestoControles
        <JsonProperty>
        Public Property TipoVehiculo As String
        <JsonProperty>
        Public Property FechaReporte As DateTime
        <JsonProperty>
        Public Property KilometrosVehiculo As Integer
        <JsonProperty>
        Public Property ReportarCliente As Integer
        <JsonProperty>
        Public Property Prioritario As Integer
        <JsonProperty>
        Public Property EnvioReporteCliente As Integer
        <JsonProperty>
        Public Property FechaReporteCliente As DateTime
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property Tenedor As Terceros
        <JsonProperty>
        Public Property ProveedorGPS As Terceros
        <JsonProperty>
        Public Property EmpresaTransportadora As Terceros
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property Mostrar As ValorCatalogos
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Semirremolque As Semirremolques
        <JsonProperty>
        Public Property OrdenarPor As ValorCatalogos
        <JsonProperty>
        Public Property PuestoControlUltimoReporte As PuestoControles
        <JsonProperty>
        Public Property PuestoControlSiguienteReporte As PuestoControles
        <JsonProperty>
        Public Property FechaUltimoReporte As DateTime
        <JsonProperty>
        Public Property TipoConsulta As Integer
        <JsonProperty>
        Public Property CodigoEvento As Integer
        <JsonProperty>
        Public Property ConsultarProgramacion As Integer
        <JsonProperty>
        Public Property EventoCorreo As EventoCorreos
        <JsonProperty>
        Public Property URLGPS As String
        <JsonProperty>
        Public Property UsuarioGPS As String
        <JsonProperty>
        Public Property ClaveGPS As String
        <JsonProperty>
        Public Property CodigosOficinas As String
        <JsonProperty>
        Public Property CodigosNivelRiesgo As String
        <JsonProperty>
        Public Property DesfaseSeguimiento As Integer
        <JsonProperty>
        Public Property FlotaPropia As Short
        <JsonProperty>
        Public Property ListaSeguimientosRemesas As IEnumerable(Of DetalleSeguimientoVehiculos)
        <JsonProperty>
        Public Property TipoDueno As ValorCatalogos
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property Origen As Ciudades
        <JsonProperty>
        Public Property Destino As Ciudades

        <JsonProperty>
        Public Property ReferenciasPersonales As String
        <JsonProperty>
        Public Property ConsultaGPS As Short

        <JsonProperty>
        Public Property NombreCliente As String
        <JsonProperty>
        Public Property strOrigen As String
        <JsonProperty>
        Public Property strDestino As String
        <JsonProperty>
        Public Property strTipoDueno As String
        <JsonProperty>
        Public Property strProducto As String
        <JsonProperty>
        Public Property strCarroceria As String
        <JsonProperty>
        Public Property ListaPrecintos As IEnumerable(Of DetallePrecintos)
        <JsonProperty>
        Public Property Documento As Byte()
        <JsonProperty>
        Public Property Listado As Short
        <JsonProperty>
        Public Property Historico As Short

        <JsonProperty>
        Public Property Empaque As String
        <JsonProperty>
        Public Property NumeroContenedor As String
        <JsonProperty>
        Public Property CiudadDevolucion As Ciudades

        <JsonProperty>
        Public Property FechaDevolucion As Date
        <JsonProperty>
        Public Property PatioDevolucion As String
        <JsonProperty>
        Public Property CodigoTipoLineaNegocio As Integer
        <JsonProperty>
        Public Property ModificaContenedor As Integer
        <JsonProperty>
        Public Property DocumentoManifiesto As Byte()
        <JsonProperty>
        Public Property DocumentoRemesa As Byte()
        <JsonProperty>
        Public Property DocumentoPlanilla As Byte()
        <JsonProperty>
        Public Property DocumentoOrdenCargue As Byte()
        <JsonProperty>
        Public Property CorreoConductor As String
        <JsonProperty>
        Public Property NumeroLegalizacion As Integer
    End Class

End Namespace
