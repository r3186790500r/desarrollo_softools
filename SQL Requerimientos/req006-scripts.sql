USE [ENCOE_GESCARGA50_PRUEBAS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
DROP TABLE IF EXISTS [dbo].[Tarifario_Carga_Tercero_Clientes_Paqueteria]

CREATE TABLE [dbo].[Tarifario_Carga_Tercero_Clientes_Paqueteria](
	[EMPR_Codigo] [smallint] NOT NULL,
	[Codigo] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ETCV_Numero] [numeric](18, 0) NOT NULL,
	[TERC_Codigo_Cliente] [numeric](18, 0) NOT NULL	
 CONSTRAINT [PK_Tarifario_Carga_Tercero_Clientes_Paqueteria] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Tarifario_Carga_Tercero_Clientes_Paqueteria]  WITH CHECK ADD  CONSTRAINT [FK_Tarifario_Carga_Tercero_Clientes_Paqueteria_ETCV_Numero] FOREIGN KEY([EMPR_Codigo], [ETCV_Numero])
REFERENCES [dbo].[Encabezado_Tarifario_Carga_Ventas] ([EMPR_Codigo], [Numero])
GO

ALTER TABLE [dbo].[Tarifario_Carga_Tercero_Clientes_Paqueteria] CHECK CONSTRAINT [FK_Tarifario_Carga_Tercero_Clientes_Paqueteria_ETCV_Numero]
GO

ALTER TABLE [dbo].[Tarifario_Carga_Tercero_Clientes_Paqueteria]  WITH CHECK ADD  CONSTRAINT [FK_Tarifario_Carga_Tercero_Clientes_Paqueteria_TERC_Codigo_Cliente] FOREIGN KEY([EMPR_Codigo], [TERC_Codigo_Cliente])
REFERENCES [dbo].[Tercero_Clientes] ([EMPR_Codigo], [TERC_Codigo])
GO

ALTER TABLE [dbo].[Tarifario_Carga_Tercero_Clientes_Paqueteria] CHECK CONSTRAINT [FK_Tarifario_Carga_Tercero_Clientes_Paqueteria_TERC_Codigo_Cliente]
GO


CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_cliente_tarifario_paqueteria]
	@par_EMPR_Codigo [smallint],
	@par_TERC_Codigo [numeric](18, 0)
WITH EXECUTE AS CALLER
AS
BEGIN   

SET NOCOUNT ON;    
	           
	WITH Pagina AS    
	(    
	SELECT    
		EMPR_Codigo,    
		Numero,    
		Nombre,    
		Estado,
		Fecha_Vigencia_Inicio,
		Fecha_Vigencia_Fin,
		Tarifario_Base,
		ROW_NUMBER() OVER(ORDER BY Numero) AS RowNumber    
	FROM    
		Encabezado_Tarifario_Carga_Ventas    
	WHERE    
		EMPR_Codigo = @par_EMPR_Codigo       
		AND Numero in (SELECT ETCV_Numero FROM [dbo].[Tarifario_Carga_Tercero_Clientes_Paqueteria]  
WHERE EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo_Cliente = @par_TERC_Codigo )    
		)   
	SELECT DISTINCT    
		0 As Obtener,
		EMPR_Codigo,    
		Numero,    
		Nombre,    
		Estado,
		Fecha_Vigencia_Inicio,   
		Fecha_Vigencia_Fin,
		Tarifario_Base
	FROM    
	Pagina    
	ORDER BY Numero asc    
END 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_cliente_tarifario_paqueteria]
	@par_EMPR_Codigo [smallint],
	@par_ETCV_Codigo [numeric](18, 0),
	@par_TERC_Codigo [numeric](18, 0)
WITH EXECUTE AS CALLER
AS
BEGIN                                                                
 INSERT INTO [dbo].[Tarifario_Carga_Tercero_Clientes_Paqueteria]                                                      
 (EMPR_Codigo                                                      
 ,ETCV_Numero                                                      
 ,TERC_Codigo_Cliente
 )                                                      
 VALUES                                                      
 (                             
	@par_EMPR_Codigo,
	@par_ETCV_Codigo,
	@par_TERC_Codigo
 )                                                      
END 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_eliminar_cliente_tarifario_paqueteria]
	@par_EMPR_Codigo [smallint],
	@par_TERC_Codigo [numeric](18, 0)
WITH EXECUTE AS CALLER
AS
BEGIN                                                                
 DELETE FROM [dbo].[Tarifario_Carga_Tercero_Clientes_Paqueteria] 
 WHERE
 EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo_Cliente = @par_TERC_Codigo                                                                                                         
END 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_tarifario_clientes]
	@par_EMPR_Codigo [smallint],
	@par_ETCV_Codigo [numeric](18, 0) = NULL,
	@par_TERC_Codigo [numeric](18, 0) = NULL,
	@par_LNTC_Codigo [numeric](18, 0) = NULL,
	@par_TLNC_Codigo [numeric](18, 0) = NULL,
	@par_CIUD_Codigo_Origen [numeric](18, 0) = NULL,
	@par_CIUD_Codigo_Destino [numeric](18, 0) = NULL,
	@par_CATA_FPVE_Codigo [numeric](18, 0) = NULL
WITH EXECUTE AS CALLER
AS
BEGIN      
 IF @par_LNTC_Codigo = 3 AND @par_TLNC_Codigo = 302      
 BEGIN      
  SET @par_CIUD_Codigo_Destino = 0      
 END      
                  
 IF ISNULL(@par_TERC_Codigo, 0) > 0               
 BEGIN              
  SELECT DTCV.EMPR_Codigo,                         
  DTCV.ETCV_Numero,                        
  DTCV.Codigo As DTCV_Codigo,                        
  DTCV.LNTC_Codigo,                         
  DTCV.TLNC_Codigo,                         
  DTCV.TATC_Codigo,                         
  DTCV.TTTC_Codigo,                        
  ISNULL(TECL.CATA_FPCL_Codigo,0) AS   CATA_FPCL_Codigo,                      
  DTCV.Valor_Flete,                        
  DTCV.Valor_Escolta,                        
  DTCV.Valor_Otros1,                         
  DTCV.Valor_Otros2,                        
  DTCV.Valor_Otros3,                        
  DTCV.Valor_Otros4,                        
  TTTC.Nombre As NombreTipoTarifaCarga,              
  TTTC.NombreTarifa As NombreTarifaCarga,          
  DTCV.CATA_FPVE_Codigo AS CodigoFormaPagoTarifa,          
  TTTC.CATA_Codigo AS Catalogo,              
  TTTC.VACA_Codigo,              
  TTTC.VACA_Campo2,              
  TTTC.VACA_Campo3,              
  ISNULL(TAPV.Valor_Manejo,0) AS Valor_Manejo,                      
  ISNULL(TAPV.Valor_Seguro,0) AS Valor_Seguro,              
  ISNULL(DTCV.Porcentaje_Seguro,0) AS Porcentaje_Seguro,              
  DTCV.Porcentaje_Afiliado,        
  DTCV.Reexpedicion,        
  DTCV.Porcentaje_Afiliado AS Porcentaje_Reexpedicion,        
  DTCV.Valor_Cargue,        
  DTCV.Valor_Descargue,  
  DTCV.CIUD_Codigo_Origen,  
  DTCV.CIUD_Codigo_Destino  
                        
  FROM Detalle_Tarifario_Carga_Ventas DTCV                        
                        
  LEFT JOIN                        
  V_Tipo_Tarifa_Transporte_Carga TTTC ON                        
  DTCV.EMPR_Codigo = TTTC.EMPR_Codigo                        
  AND DTCV.TATC_Codigo = TTTC.TATC_Codigo                        
  AND DTCV.TTTC_Codigo = TTTC.Codigo                        
                        
  AND TTTC.EstadoTarifa = 1 --> Tarifa Activa                        
  --AND TTTC.EstadoTipoTarifa = 1 --> Tipo Tarifa Activa                        
                        
  INNER JOIN                        
  Tercero_Clientes TECL ON                        
  DTCV.EMPR_Codigo = TECL.EMPR_Codigo                        
  AND DTCV.ETCV_Numero = ISNULL(@par_ETCV_Codigo,TECL.ETCV_Numero)                        
  AND TECL.TERC_Codigo = @par_TERC_Codigo                        
                        
  LEFT JOIN                       
  Tarifas_Paqueteria_Ventas AS TAPV                      
  ON TAPV.EMPR_Codigo = DTCV.EMPR_Codigo                        
  AND TAPV.ETCV_Numero = DTCV.ETCV_Numero      
                      
  WHERE                         
  DTCV.EMPR_Codigo = @par_EMPR_Codigo
  AND DTCV.ETCV_Numero =  ISNULL(@par_ETCV_Codigo,DTCV.ETCV_Numero)
  AND DTCV.Estado = 1--> Activo                        
  AND DTCV.LNTC_Codigo = ISNULL(@par_LNTC_Codigo,DTCV.LNTC_Codigo )                        
  AND DTCV.CIUD_Codigo_Origen = ISNULL(@par_CIUD_Codigo_Origen ,DTCV.CIUD_Codigo_Origen )                    
  AND DTCV.CIUD_Codigo_Destino = ISNULL(@par_CIUD_Codigo_Destino, DTCV.CIUD_Codigo_Destino)                
  AND (DTCV.CATA_FPVE_Codigo = @par_CATA_FPVE_Codigo  OR  @par_CATA_FPVE_Codigo IS NULL)                    
 END                        
 ELSE                            
 BEGIN               
  SELECT DTCV.EMPR_Codigo,                        
                        
  DTCV.ETCV_Numero,                        
  DTCV.Codigo As DTCV_Codigo,      
  DTCV.LNTC_Codigo,                         
  DTCV.TLNC_Codigo,                         
  DTCV.TATC_Codigo,                         
  DTCV.TTTC_Codigo,                        
  6102 AS CATA_FPCL_Codigo, --> Forma pago contado                        
  DTCV.Valor_Flete,                        
  DTCV.Valor_Escolta,                        
  DTCV.Valor_Otros1,                         
  DTCV.Valor_Otros2,                        
  DTCV.Valor_Otros3,                        
  DTCV.Valor_Otros4,                        
 TTTC.Nombre As NombreTipoTarifaCarga,                        
  TTTC.NombreTarifa As NombreTarifaCarga,          
  DTCV.CATA_FPVE_Codigo AS CodigoFormaPagoTarifa,              
  TTTC.CATA_Codigo AS Catalogo,              
  TTTC.VACA_Codigo,              
  TTTC.VACA_Campo2,              
  TTTC.VACA_Campo3,              
  ISNULL(TAPV.Valor_Manejo,0) AS Valor_Manejo,                      
  ISNULL(TAPV.Valor_Seguro,0) AS Valor_Seguro,              
  ISNULL(DTCV.Porcentaje_Seguro,0) AS Porcentaje_Seguro,              
  DTCV.Porcentaje_Afiliado,        
  DTCV.Reexpedicion,        
  DTCV.Porcentaje_Afiliado AS Porcentaje_Reexpedicion,        
  DTCV.Valor_Cargue,        
  DTCV.Valor_Descargue        ,    
  DTCV.CIUD_Codigo_Origen,  
  DTCV.CIUD_Codigo_Destino  
                        
  FROM Detalle_Tarifario_Carga_Ventas DTCV                        
                        
  INNER JOIN                         
  Encabezado_Tarifario_Carga_Ventas ETCV ON                        
  DTCV.EMPR_Codigo = ETCV.EMPR_Codigo                        
  AND DTCV.ETCV_Numero = ETCV.Numero                        
                        
  INNER JOIN                        
  V_Tipo_Tarifa_Transporte_Carga TTTC ON                      
  DTCV.EMPR_Codigo = TTTC.EMPR_Codigo                        
  AND DTCV.TATC_Codigo = TTTC.TATC_Codigo                        
  AND DTCV.TTTC_Codigo = TTTC.Codigo                        
                        
  AND TTTC.EstadoTarifa = 1 --> Tarifa Activa                        
  --AND TTTC.EstadoTipoTarifa = 1 --> Tipo Tarifa Activa                        
                      
  LEFT JOIN                       
  Tarifas_Paqueteria_Ventas AS TAPV                      
  ON TAPV.EMPR_Codigo = DTCV.EMPR_Codigo                        
  AND TAPV.ETCV_Numero = DTCV.ETCV_Numero        
    
                        
  WHERE                         
  DTCV.EMPR_Codigo = @par_EMPR_Codigo                        
  AND DTCV.Estado = 1--> Activo                        
  AND DTCV.LNTC_Codigo = @par_LNTC_Codigo                        
  AND DTCV.CIUD_Codigo_Origen = @par_CIUD_Codigo_Origen                        
  AND DTCV.CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino                        
  AND (DTCV.CATA_FPVE_Codigo = @par_CATA_FPVE_Codigo  OR  @par_CATA_FPVE_Codigo IS NULL)                    
  AND ETCV.Tarifario_Base = 1                           
 END                            
END
GO