﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Almacen
Imports EncoExpres.GesCarga.Repositorio.Almacen

Namespace Almacen
    Public NotInheritable Class PersistenciaOrdenCompra
        Implements IPersistenciaBase(Of OrdenCompra)
        Public Function Consultar(filtro As OrdenCompra) As IEnumerable(Of OrdenCompra) Implements IPersistenciaBase(Of OrdenCompra).Consultar
            Return New RepositorioOrdenCompra().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As OrdenCompra) As Long Implements IPersistenciaBase(Of OrdenCompra).Insertar
            Return New RepositorioOrdenCompra().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As OrdenCompra) As Long Implements IPersistenciaBase(Of OrdenCompra).Modificar
            Return New RepositorioOrdenCompra().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As OrdenCompra) As OrdenCompra Implements IPersistenciaBase(Of OrdenCompra).Obtener
            Return New RepositorioOrdenCompra().Obtener(filtro)
        End Function
        Public Function Anular(entidad As OrdenCompra) As Boolean
            Return New RepositorioOrdenCompra().Anular(entidad)
        End Function

    End Class
End Namespace
