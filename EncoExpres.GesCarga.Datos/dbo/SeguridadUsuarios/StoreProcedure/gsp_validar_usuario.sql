﻿PRINT 'gsp_validar_usuario'
GO
DROP PROCEDURE gsp_validar_usuario
GO
CREATE PROCEDURE gsp_validar_usuario      
(      
 @par_EMPR_Codigo SMALLINT,       
 @par_Codigo_Usuario VARCHAR(10)      
)      
AS      
BEGIN   
 DECLARE @NumeromaximoAplicativo smallint       
 DECLARE @NumeromaximoGesphone smallint       
 DECLARE @NumeromaximoPortal smallint      
      
 DECLARE @UsuariosLoginPortal smallint        
 DECLARE @UsuariosLoginGesphone smallint       
 DECLARE @UsuariosLoginAplicativo smallint       
      
 -- Consulta numero maximo de usuarios para el aplicativo      
 SELECT @NumeromaximoAplicativo = CONVERT(VARCHAR(3),DECRYPTBYPASSPHRASE('$$EncoExpres$$', ISNULL(Numero_Usuarios_Aplicativo,0)))       
 FROM Empresas  
 WHERE Codigo = @par_EMPR_Codigo  
 -- Consulta numero maximo de usuarios para Gesphone      
 SELECT @NumeromaximoGesphone = CONVERT(VARCHAR(3),DECRYPTBYPASSPHRASE('$$EncoExpres$$', ISNULL(Numero_Usuarios_Gesphone,0)))       
 FROM Empresas  
 WHERE Codigo = @par_EMPR_Codigo  
 -- Consulta numero maximo de usuarios para portal      
 SELECT @NumeromaximoPortal = CONVERT(VARCHAR(3),DECRYPTBYPASSPHRASE('$$EncoExpres$$', ISNULL(Numero_Usuarios_Portal,0)))       
 FROM Empresas  
 WHERE Codigo = @par_EMPR_Codigo  
 -- Consulta numero usuarios logeados en aplicativo      
 select @UsuariosLoginAplicativo = COUNT(Codigo) from usuarios       
 where       
 EMPR_Codigo = @par_EMPR_Codigo      
 AND CATA_APLI_Codigo = 201      
 AND Habilitado = 1      
 -- Consulta numero usuarios logeados en portal      
 select @UsuariosLoginPortal = COUNT(Codigo) from usuarios       
 where       
 EMPR_Codigo = @par_EMPR_Codigo      
 AND CATA_APLI_Codigo = 202      
 AND Habilitado = 1      
 -- Consulta numero usuarios logeados en gephone      
 select @UsuariosLoginGesphone = COUNT(Codigo) from usuarios       
 where       
 EMPR_Codigo = @par_EMPR_Codigo      
 AND CATA_APLI_Codigo = 203      
 AND Habilitado = 1      
 SELECT      
  Consultar = 0,      
  USUA.EMPR_Codigo,       
  USUA.Codigo,       
  USUA.Codigo_Usuario,       
  ISNULL(USUA.Codigo_Alterno,'') AS Codigo_Alterno,      
  USUA.Nombre,       
  USUA.Descripcion,       
  USUA.Clave,       
  USUA.Manager,             
  USUA.Habilitado,      
  USUA.Login,      
  ISNULL(USUA.Fecha_Ultimo_Ingreso,GETDATE()) AS Fecha_Ultimo_Ingreso,      
  USUA.Dias_Cambio_Clave,      
  ISNULL(USUA.Fecha_Ultimo_Cambio_Clave,GETDATE()) AS Fecha_Ultimo_Cambio_Clave,      
  ISNULL(USUA.TERC_Codigo_Conductor,0) AS TERC_Codigo_Conductor,      
  ISNULL(USUA.TERC_Codigo_Empleado,0) AS TERC_Codigo_Empleado,      
  ISNULL(USUA.TERC_Codigo_Cliente,0) AS TERC_Codigo_Cliente,     
  ISNULL(USUA.TERC_Codigo_Proveedor,0) AS TERC_Codigo_Proveedor,        
  ISNULL(USUA.Intentos_Ingreso,0) AS Intentos_Ingreso,      
  ISNULL(USUA.Mensaje_Banner,'') AS Mensaje_Banner,      
  USUA.CATA_APLI_Codigo,      
  APLI.Nombre AS AplicacionUsuario,      
 CONCAT(ISNULL(COND.Nombre,''),' ',ISNULL(COND.Apellido1,''),' ',ISNULL(COND.Apellido2,''),' ',ISNULL(COND.Razon_Social,'')) AS TerceroConductor,      
 CONCAT(ISNULL(CLIE.Nombre,''),' ',ISNULL(CLIE.Apellido1,''),' ',ISNULL(CLIE.Apellido2,''),' ',ISNULL(CLIE.Razon_Social,'')) AS TerceroCliente,      
 CONCAT(ISNULL(EMPL.Nombre,''),' ',ISNULL(EMPL.Apellido1,''),' ',ISNULL(EMPL.Apellido2,''),' ',ISNULL(EMPL.Razon_Social,'')) AS Empleado,      
 CONCAT(ISNULL(PROV.Nombre,''),' ',ISNULL(PROV.Apellido1,''),' ',ISNULL(PROV.Apellido2,''),' ',ISNULL(PROV.Razon_Social,'')) AS Proveedor,      
  ISNULL(@NumeromaximoAplicativo,0) AS NumeroMaximoAplicativo,      
  ISNULL(@NumeromaximoGesphone,0) AS NumeroMaximoGesphone ,      
  ISNULL(@NumeromaximoPortal,0) AS NumeroMaximoPortal,      
  ISNULL(@UsuariosLoginAplicativo,0) AS UsuariosLogeadosAplicativo,      
  ISNULL(@UsuariosLoginPortal,0) AS UsuariosLogeadosPortal,      
  ISNULL(@UsuariosLoginGesphone,0) AS UsuariosLogeadosGesphone      
    FROM      
  Usuarios USUA      
      
  LEFT OUTER JOIN      
  Empresas AS EMPR ON       
  USUA.EMPR_Codigo = EMPR.Codigo      
      
  LEFT JOIN      
  V_Aplicaciones_Gestrans AS APLI ON      
  USUA.EMPR_Codigo = APLI.EMPR_Codigo        
  AND USUA.CATA_APLI_Codigo = APLI.Codigo       
      
  LEFT JOIN      
  Terceros AS EMPL ON      
  USUA.EMPR_Codigo = EMPL.EMPR_Codigo      
  AND USUA.TERC_Codigo_Empleado = EMPL.Codigo       
      
  LEFT JOIN      
  Terceros AS CLIE ON      
  USUA.EMPR_Codigo = CLIE.EMPR_Codigo       
  AND USUA.TERC_Codigo_Cliente = CLIE.Codigo       
      
  LEFT JOIN      
  Terceros AS COND ON      
  USUA.EMPR_Codigo = COND.EMPR_Codigo       
  AND USUA.TERC_Codigo_Conductor = COND.Codigo       
    
 LEFT JOIN      
 Terceros AS PROV ON      
 USUA.EMPR_Codigo = PROV.EMPR_Codigo       
 AND USUA.TERC_Codigo_Proveedor = PROV.Codigo      
    
 WHERE      
  USUA.EMPR_Codigo = @par_EMPR_Codigo      
  AND USUA.Codigo_Usuario = @par_Codigo_Usuario       
END 
GO