﻿Print 'gsp_insertar_encabezado_remesa_paqueteria'
GO
DROP PROCEDURE gsp_insertar_encabezado_remesa_paqueteria
GO
CREATE PROCEDURE gsp_insertar_encabezado_remesa_paqueteria  
(  
 @par_EMPR_Codigo smallint,  
 @par_ENRE_Numero numeric,  
 @par_TERC_Codigo_Transportador_Externo numeric = NULL,  
 @par_Numero_Guia_Externa varchar(50) = NULL,  
 @par_CATA_TTRP_Codigo numeric = NULL,  
 @par_CATA_TDRP_Codigo numeric = NULL,  
 @par_CATA_TPRP_Codigo numeric = NULL,  
 @par_CATA_TSRP_Codigo numeric = NULL,  
 @par_CATA_TERP_Codigo numeric = NULL,  
 @par_CATA_TIRP_Codigo numeric = NULL,  
 @par_Fecha_Interfaz_Tracking datetime = NULL,  
 @par_Fecha_Interfaz_WMS datetime  = NULL,
  
 @par_CATA_ESRP_Codigo	numeric = NULL,
 @par_Observaciones_Remitente varchar(500) = NULL,
 @par_Observaciones_Destinatario varchar(500) = NULL,
 @par_CIUD_Codigo_Origen numeric = NULL,
 @par_CIUD_Codigo_Destino numeric = NULL,

 @par_OFIC_Codigo_Origen numeric = NULL,
 @par_OFIC_Codigo_Destino numeric = NULL,
 @par_OFIC_Codigo_Actual  numeric = NULL,
 @par_Descripcion_Mercancia varchar(500) = NULL,
 @par_Reexpedicion  smallint = NULL

)  
as  
begin  
  
  
INSERT INTO Remesas_Paqueteria  
           (EMPR_Codigo,  
   ENRE_Numero,  
   TERC_Codigo_Transportador_Externo,  
   Numero_Guia_Externa,  
   CATA_TTRP_Codigo,  
   CATA_TDRP_Codigo,  
   CATA_TPRP_Codigo,  
   CATA_TSRP_Codigo,  
   CATA_TERP_Codigo,  
   CATA_TIRP_Codigo,  
   Fecha_Interfaz_Cargue_Archivo,
   Fecha_Interfaz_Cargue_WMS,
   CATA_ESRP_Codigo,
   Observaciones_Remitente,
   Observaciones_Destinatario,
   CIUD_Codigo_Origen,
   CIUD_Codigo_Destino,

    OFIC_Codigo_Origen,
	OFIC_Codigo_Destino,
	OFIC_Codigo_Actual,
	Descripcion_Mercancia,
	Reexpedicion
   )  
     VALUES  
           (  
  
   @par_EMPR_Codigo,  
   ISNULL(@par_ENRE_Numero,0),  
   ISNULL(@par_TERC_Codigo_Transportador_Externo,0),  
   ISNULL(@par_Numero_Guia_Externa,''),  
   ISNULL(@par_CATA_TTRP_Codigo,0),  
   ISNULL(@par_CATA_TDRP_Codigo,0),  
   ISNULL(@par_CATA_TPRP_Codigo,0),  
   ISNULL(@par_CATA_TSRP_Codigo,0),  
   ISNULL(@par_CATA_TERP_Codigo,0),  
   ISNULL(@par_CATA_TIRP_Codigo,0),  
   ISNULL(@par_Fecha_Interfaz_Tracking,'01/01/1900'),  
   ISNULL(@par_Fecha_Interfaz_WMS,'01/01/1900'),
   ISNULL(@par_CATA_ESRP_Codigo,0),
   ISNULL(@par_Observaciones_Remitente,''),
   ISNULL(@par_Observaciones_Destinatario,''),
   @par_CIUD_Codigo_Origen,
   @par_CIUD_Codigo_Destino,

  ISNULL(@par_OFIC_Codigo_Origen,0),
  ISNULL(@par_OFIC_Codigo_Destino,0),
  ISNULL(@par_OFIC_Codigo_Actual,0),
  ISNULL(@par_Descripcion_Mercancia,''),
  ISNULL(@par_Reexpedicion,0)

     )  
  
     SELECT ENRE_Numero As Numero FROM Remesas_Paqueteria  
     WHERE EMPR_Codigo = @par_EMPR_Codigo  
     AND ENRE_Numero = @par_ENRE_Numero  
END  
GO