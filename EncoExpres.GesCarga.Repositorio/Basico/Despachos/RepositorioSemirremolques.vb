﻿Imports System.Data.OleDb
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioSemirremolques"/>
    ''' </summary>
    Public NotInheritable Class RepositorioSemirremolques
        Inherits RepositorioBase(Of Semirremolques)

        Public Overrides Function Consultar(filtro As Semirremolques) As IEnumerable(Of Semirremolques)
            Dim lista As New List(Of Semirremolques)
            Dim item As Semirremolques

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.ValorAutocomplete) Then 'Or filtro.Sync = True Then


                    conexion.AgregarParametroSQL("@par_Estado", 1)
                    If Not IsNothing(filtro.ValorAutocomplete) Then
                        conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)
                    End If
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_autocomplete_semirremolques]")

                    While resultado.Read
                        item = New Semirremolques(resultado)
                        lista.Add(item)
                    End While

                Else
                    If Not IsNothing(filtro.Placa) Then
                        conexion.AgregarParametroSQL("@par_Placa", filtro.Placa)
                    End If
                    If Not IsNothing(filtro.CodigoAlterno) Then
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                    End If
                    If Not IsNothing(filtro.Propietario) Then
                        If Not IsNothing(filtro.Propietario.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Propietario", filtro.Propietario.Nombre)
                        End If
                    End If
                    If Not IsNothing(filtro.Marca) Then
                        If Not IsNothing(filtro.Marca.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Marca", filtro.Marca.Nombre)
                        End If
                    End If
                    'If Not IsNothing(filtro.EstadoSemirremolques) Then
                    '    If filtro.EstadoSemirremolques.Codigo > 0 And filtro.EstadoSemirremolques.Codigo <> 3300 Then
                    '        conexion.AgregarParametroSQL("@par_CATA_ESSE", filtro.EstadoSemirremolques.Codigo)
                    '    End If
                    'End If
                    If Not IsNothing(filtro.Estado) Then
                        If filtro.Estado.Codigo > -1 Then 'NO APLICA
                            conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                        End If
                    End If
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_semirremolques]")

                    While resultado.Read
                        item = New Semirremolques(resultado)
                        lista.Add(item)
                    End While
                End If



            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Semirremolques) As Long
            Dim item = New Semirremolques
            Using conexionTemp = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexionTemp.CreateConnection()
                conexionTemp.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexionTemp.AgregarParametroSQL("@par_Filtro", entidad.Placa)
                Dim resultadoConsulta As IDataReader = conexionTemp.ExecuteReaderStoreProcedure("gsp_obtener_semirremolque_x_identificacion")
                While resultadoConsulta.Read
                    item = New Semirremolques(resultadoConsulta)
                End While
                resultadoConsulta.Close()
                conexionTemp.CleanParameters()
                conexionTemp.CloseConnection()
            End Using
            If item.Codigo > 0 Then
                entidad.Codigo = item.Codigo
                Return Modificar(entidad)
            Else
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@Codigo", entidad.Codigo) 'NUMERIC = 0,
                    conexion.AgregarParametroSQL("@par_Placa", entidad.Placa) 'varchar(20),
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno) 'varchar(20) = null,
                    If Not IsNothing(entidad.Propietario) Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Propietario", entidad.Propietario.Codigo) '	numeric = null,
                    End If
                    If Not IsNothing(entidad.Marca) Then
                        conexion.AgregarParametroSQL("@par_MASE_Codigo", entidad.Marca.Codigo) 'numeric = null,
                    End If
                    If Not IsNothing(entidad.TipoCarroceria) Then
                        conexion.AgregarParametroSQL("@par_CATA_TICA_Codigo", entidad.TipoCarroceria.Codigo) '	numeric = null,
                    End If
                    If Not IsNothing(entidad.TipoSemirremolque) Then
                        conexion.AgregarParametroSQL("@par_CATA_TISE_Codigo", entidad.TipoSemirremolque.Codigo) '	numeric = null,
                    End If
                    'If Not IsNothing(entidad.TipoEquipo) Then
                    '    conexion.AgregarParametroSQL("@par_CATA_TIEQ_Codigo", entidad.TipoEquipo.Codigo) '	numeric = null,
                    'End If
                    conexion.AgregarParametroSQL("@par_Equipo_Propio", entidad.EquipoPropio) 'smallint = null,
                    conexion.AgregarParametroSQL("@par_Modelo", entidad.Modelo) 'smallint = null,
                    conexion.AgregarParametroSQL("@par_Numero_Ejes", entidad.NumeroEjes) '	smallint = null,
                    conexion.AgregarParametroSQL("@par_Levanta_Ejes", entidad.LevantaEjes) '	smallint = null,
                    conexion.AgregarParametroSQL("@par_Ancho", entidad.Ancho, SqlDbType.Decimal) '	numeric = null,
                    conexion.AgregarParametroSQL("@par_Alto", entidad.Alto, SqlDbType.Decimal) '	numeric = null,
                    conexion.AgregarParametroSQL("@par_Largo", entidad.Largo, SqlDbType.Decimal) '	numeric = null,
                    conexion.AgregarParametroSQL("@par_Peso_Vacio", entidad.PesoVacio, SqlDbType.Decimal) '	numeric = null,
                    conexion.AgregarParametroSQL("@par_Capacidad_Galones", entidad.CapacidadGalones, SqlDbType.Decimal) '	numeric = null,
                    conexion.AgregarParametroSQL("@par_Capacidad_Kilos", entidad.Capacidad, SqlDbType.Decimal) '	numeric = null,
                    'If Not IsNothing(entidad.EstadoSemirremolques) Then
                    '    conexion.AgregarParametroSQL("@par_CATA_ESSE_Codigo", entidad.EstadoSemirremolques.Codigo) 'numeric = null,
                    'End If
                    conexion.AgregarParametroSQL("@par_Justificacion_Bloqueo", entidad.JustificacionBloqueo) 'varchar (150) = null
                    If Not IsNothing(entidad.Estado) Then
                        If entidad.Estado.Codigo > -1 Then 'NO APLICA
                            conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo) 'smallint = null,

                    If entidad.FechaCrea > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vinculacion", entidad.FechaCrea, SqlDbType.DateTime)
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_semirremolques")
                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()
                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    Else
                        conexion.CloseConnection()
                        Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                        If Not IsNothing(entidad.Documentos) Then

                            For Each detalle In entidad.Documentos
                                detalle.CodigoSemirremolques = entidad.Codigo
                                If Not Documento.InsertarDocumentoSemirremolque(detalle) Then
                                    Exit For
                                End If

                            Next
                        End If

                    End If

                End Using

                Return entidad.Codigo
            End If
        End Function

        Public Overrides Function Modificar(entidad As Semirremolques) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                conexion.AgregarParametroSQL("@Codigo", entidad.Codigo) 'NUMERIC = 0,
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno) 'varchar(20) = null,
                If Not IsNothing(entidad.Propietario) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Propietario", entidad.Propietario.Codigo) '	numeric = null,
                End If
                If Not IsNothing(entidad.Marca) Then
                    conexion.AgregarParametroSQL("@par_MASE_Codigo", entidad.Marca.Codigo) 'numeric = null,
                End If
                If Not IsNothing(entidad.TipoCarroceria) Then
                    conexion.AgregarParametroSQL("@par_CATA_TICA_Codigo", entidad.TipoCarroceria.Codigo) '	numeric = null,
                End If
                If Not IsNothing(entidad.TipoSemirremolque) Then
                    conexion.AgregarParametroSQL("@par_CATA_TISE_Codigo", entidad.TipoSemirremolque.Codigo) '	numeric = null,
                End If
                'If Not IsNothing(entidad.TipoEquipo) Then
                '    conexion.AgregarParametroSQL("@par_CATA_TIEQ_Codigo", entidad.TipoEquipo.Codigo) '	numeric = null,
                'End If
                conexion.AgregarParametroSQL("@par_Equipo_Propio", entidad.EquipoPropio) 'smallint = null,
                conexion.AgregarParametroSQL("@par_Modelo", entidad.Modelo) 'numeric = null,
                conexion.AgregarParametroSQL("@par_Numero_Ejes", entidad.NumeroEjes) '	smallint = null,
                conexion.AgregarParametroSQL("@par_Levanta_Ejes", entidad.LevantaEjes) '	smallint = null,
                conexion.AgregarParametroSQL("@par_Ancho", entidad.Ancho, SqlDbType.Decimal) '	numeric = null,
                conexion.AgregarParametroSQL("@par_Alto", entidad.Alto, SqlDbType.Decimal) '	numeric = null,
                conexion.AgregarParametroSQL("@par_Largo", entidad.Largo, SqlDbType.Decimal) '	numeric = null,
                conexion.AgregarParametroSQL("@par_Peso_Vacio", entidad.PesoVacio, SqlDbType.Decimal) '	numeric = null,
                conexion.AgregarParametroSQL("@par_Capacidad_Galones", entidad.CapacidadGalones, SqlDbType.Decimal) '	numeric = null,
                conexion.AgregarParametroSQL("@par_Capacidad_Kilos", entidad.Capacidad, SqlDbType.Decimal) '	numeric = null,
                'If Not IsNothing(entidad.EstadoSemirremolques) Then
                '    conexion.AgregarParametroSQL("@par_CATA_ESSE_Codigo", entidad.EstadoSemirremolques.Codigo) 'numeric = null,
                'End If
                conexion.AgregarParametroSQL("@par_Justificacion_Bloqueo", entidad.JustificacionBloqueo) 'varchar (150) = null
                If Not IsNothing(entidad.Estado) Then
                    If entidad.Estado.Codigo > -1 Then 'NO APLICA
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo) 'smallint = null,
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_semirremolques")
                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()
                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                Else
                    conexion.CloseConnection()
                    Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                    If Not IsNothing(entidad.Documentos) Then
                        For Each detalle In entidad.Documentos
                            detalle.CodigoSemirremolques = entidad.Codigo
                            If Not Documento.InsertarDocumentoSemirremolque(detalle) Then
                                Exit For
                            End If

                        Next
                    End If
                End If
            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As Semirremolques) As Semirremolques
            Dim item As New Semirremolques
            Dim Doc As New Documentos
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo = 0 And IsNothing(filtro.Placa) Then
                    Return item
                Else
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_Placa", filtro.Placa)
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_semirremolque]")

                    While resultado.Read
                        item = New Semirremolques(resultado)
                    End While
                    resultado.Close()
                    item.ListaNovedades = ConsultarNovedadesVehiculo(filtro.CodigoEmpresa, filtro.Codigo, conexion, resultado)
                    conexion.CloseConnection()
                    Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                    Doc.CodigoEmpresa = filtro.CodigoEmpresa
                    Doc.Semirremolques = True
                    Doc.CodigoSemirremolques = item.Codigo
                    item.Documentos = Documento.Consultar(Doc)
                End If

            End Using

            Return item
        End Function

        Public Function ConsultarNovedadesVehiculo(CodiEmpresa As Short, CodVehiculo As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of NovedadTercero)
            Dim lista As New List(Of NovedadTercero)
            Dim item As NovedadTercero
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_SEMI_Codigo", CodVehiculo)

            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_novedades_semirremolque")

            While resultado.Read
                item = New NovedadTercero(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function


        Public Function ConsultarDocumentos(entidad As SemirremolquesDocumentos) As IEnumerable(Of SemirremolquesDocumentos)
            Dim item As New SemirremolquesDocumentos
            Dim lista As New List(Of SemirremolquesDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                If Not IsNothing(entidad.Semirremolque) Then
                    If entidad.Semirremolque.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
                    End If
                End If



                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_documentos_semirremolque")

                While resultado.Read
                    item = New SemirremolquesDocumentos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
            End Using
            Return lista
        End Function

        Public Function GuardarNovedad(entidad As Semirremolques) As Long
            Dim inserto As Short
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_NOSE_Codigo", entidad.Novedad.Codigo)
                conexion.AgregarParametroSQL("@par_Justificacion", entidad.JustificacionNovedad)
                conexion.AgregarParametroSQL("@par_Responsable", entidad.UsuarioCrea.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_novedad_semirremolque")

                While resultado.Read
                    inserto = resultado.Item("Numero")
                End While
            End Using
            Return inserto
        End Function

        Public Function ConsultarSemirremolqueEstudioSeguridad(filtro As EstudioSeguridad) As EstudioSeguridad
            Dim item As New Semirremolques
            Dim objEstudio As New EstudioSeguridad
            Dim Reader As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero_Autorizacion", filtro.NumeroAutorizacion)

                Reader = conexion.ExecuteReaderStoreProcedure("[dbo].[Obtener_semirremolque_estudio_seguridad]")
                While Reader.Read
                    objEstudio = New EstudioSeguridad(Reader)
                End While
                Reader.Close()
            End Using

            Return objEstudio
        End Function

        Public Function Anular(entidad As Semirremolques) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_Semirremolques]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
        Public Function GenerarPlanitilla(filtro As Semirremolques) As Semirremolques

            Dim fullPath As String
            fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
            System.IO.File.Copy(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Semirremolques.xlsx", fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Semirremolques_Generado.xlsx", True)
            Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Semirremolques_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"

            Dim Catalogos As IEnumerable(Of ValorCatalogos)
            Dim Catalogo As New ValorCatalogos
            Dim ObjValorCatalogos As New RepositorioValorCatalogos
            Catalogo.CodigoEmpresa = filtro.CodigoEmpresa
            Catalogo.Catalogo = New Catalogos With {.Codigo = 23}
            Catalogos = ObjValorCatalogos.Consultar(Catalogo)
            Dim Marcas As IEnumerable(Of MarcaSemirremolques)
            Dim Marca As New MarcaSemirremolques
            Dim ObjValorMarcas As New RepositorioMarcaSemirremolques
            Marca.CodigoEmpresa = filtro.CodigoEmpresa
            Marca.Estado = 1
            Marcas = ObjValorMarcas.Consultar(Marca)

            Using cnn As New OleDbConnection(cn)
                cnn.Open()
                Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                For i = 1 To Catalogos.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        cmd.CommandText = "INSERT INTO [Tipo_Carroceria$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    End Using
                Next
                Catalogo.CodigoEmpresa = filtro.CodigoEmpresa
                Catalogo.Catalogo = New Catalogos With {.Codigo = 34}
                Catalogos = ObjValorCatalogos.Consultar(Catalogo)
                Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                For i = 1 To Catalogos.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        cmd.CommandText = "INSERT INTO [Tipo_Semirremolque$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    End Using
                Next
                Marcas = Marcas.OrderBy(Function(a) a.Nombre)
                For i = 1 To Marcas.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        cmd.CommandText = "INSERT INTO [Marca$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Marcas(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Marcas(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    End Using
                Next
                cnn.Close()
            End Using
            Dim RTA As New Semirremolques
            Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Semirremolques_Generado.xlsx")
            RTA.Planitlla = Archivo
            Return RTA
        End Function
    End Class

End Namespace