﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Paqueteria
    <JsonObject>
    Public NotInheritable Class GestionDocumentosRemesa
        Inherits BaseBasico
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroDocumentoGestion = Read(lector, "Numero_Documento_Gestion")
            TipoDocumento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TDGC_Codigo"), .Nombre = Read(lector, "CATA_TDGC_Nombre")}
            TipoGestion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TGCD_Codigo"), .Nombre = Read(lector, "CATA_TGCD_Nombre")}
            NumeroHojas = Read(lector, "Numero_Hojas")
            ValorRecaudar = Read(lector, "Valor_Recaudar")
            Recibido = Read(lector, "Recibido")
            Entregado = Read(lector, "Entregado")
            Legalizado = Read(lector, "Legalizo")
            Observaciones = Read(lector, "Observaciones_Legalizo")

        End Sub
        <JsonProperty>
        Public Property TipoDocumento As ValorCatalogos
        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property NumeroDocumentoGestion As Integer
        <JsonProperty>
        Public Property TipoGestion As ValorCatalogos
        <JsonProperty>
        Public Property NumeroHojas As Integer
        <JsonProperty>
        Public Property ValorRecaudar As Double
        <JsonProperty>
        Public Property Recibido As Integer
        <JsonProperty>
        Public Property Entregado As Integer
        <JsonProperty>
        Public Property Legalizado As Integer
        <JsonProperty>
        Public Property Observaciones As String
    End Class
End Namespace

