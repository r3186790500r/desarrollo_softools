USE [GESCARGA50_DESA]
GO

/****** Object:  StoredProcedure [dbo].[gsp_obtener_remesa_paqueteria_control_entregas]    Script Date: 12/01/2022 8:06:53 p.�m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER PROCEDURE [dbo].[gsp_obtener_remesa_paqueteria_control_entregas]    
(                            
 @par_EMPR_Codigo smallint,                            
 @par_Numero Numeric                           
)                            
AS                             
BEGIN      
 SELECT                      
 ENRE.EMPR_Codigo,                          
 ENRE.Fecha,                          
 ENRE.Numero,                          
 ENRE.Numero_Documento,      
 ENPR.Fecha_Recibe,      
 ENPR.Numero_Identificacion_Recibe,      
 ENPR.Nombre_Recibe,      
 ENPR.Telefonos_Recibe,      
 ENPR.Cantidad_Recibe,      
 ENPR.Peso_Recibe,      
 ENPR.CATA_NERP_Codigo,    
 NERP.Campo1 NovedadRecibe,    
 ENPR.Observaciones_Recibe,      
 ENPR.Firma_Recibe    ,  
 ENPR.Latitud,  
 ENPR.Longitud,
 /*modificacion 26/03/2021*/
 USUA.Nombre as Usuario_Entrega,
ENPR.OFIC_Codigo_Entrega as  OFIC_Codigo_Entrega,
 OFICIN.Nombre as OFIC_Nombre_Entrega,
 /*Fin modificacion 26/03/2021*/
 -- FP 2022 01 12 
 ENPR.Fecha_Entrega,
 CASE 
    WHEN ENPR.OFIC_Codigo_Entrega IS NOT NULL THEN EPCD.Numero_Documento
    WHEN ENPR.OFIC_Codigo_Entrega IS NULL THEN ENPR.ENPD_Numero_Ultima_Planilla
    ELSE NULL
  END AS PlanillaEntrega,
 USUA2.Nombre AS Usuario_Modifica,
 ENRE.VEHI_Codigo AS VEHI_Codigo,
 VEHI.Placa As VEHI_Placa,  
 VEHI.Codigo_Alterno As VEHI_CodigoAlterno 
 -- FP 2022 01 12 

 FROM                           
 Remesas_Paqueteria as ENPR                
      
 INNER JOIN Encabezado_Remesas AS ENRE                          
 ON ENPR.EMPR_Codigo = ENRE.EMPR_Codigo                    
 AND ENPR.ENRE_Numero = ENRE.Numero    
     
 LEFT JOIN Valor_Catalogos AS NERP                          
 ON ENPR.EMPR_Codigo = ENRE.EMPR_Codigo                    
 AND ENPR.CATA_NERP_Codigo = NERP.Codigo   

  /*modificacion 26/03/2021*/
  LEFT JOIN  Usuarios AS USUA
  ON ENPR.EMPR_Codigo = USUA.EMPR_Codigo
  and  ENPR.USUA_Codigo_Entrega = USUA.Codigo

    LEFT JOIN  Oficinas AS OFICIN
  ON ENPR.EMPR_Codigo = OFICIN.EMPR_Codigo
  and  ENPR.OFIC_Codigo_Entrega = OFICIN.Codigo
       /*Fin modificacion 26/03/2021*/ 

	-- FP 2022 01 12 
  LEFT JOIN  Usuarios AS USUA2
  ON ENPR.EMPR_Codigo = USUA.EMPR_Codigo
  AND  ENRE.USUA_Codigo_Modifica = USUA.Codigo

  LEFT JOIN Vehiculos AS VEHI                                    
  ON ENPR.EMPR_Codigo = VEHI.EMPR_Codigo 
  AND ENRE.VEHI_Codigo = VEHI.Codigo 

   LEFT JOIN Encabezado_Planilla_Cargue_Descargue AS EPCD                                    
  ON ENPR.EMPR_Codigo = EPCD.EMPR_Codigo 
  AND ENPR.ENPD_Numero_Ultima_Planilla = EPCD.Numero_Documento 
  	-- FP 2022 01 12 

      
 WHERE      
 ENRE.EMPR_Codigo = @par_EMPR_Codigo                        
 AND ENRE.Numero = @par_Numero                        
 AND ENRE.TIDO_Codigo = 110 --> Gu�a                        
END    
GO


