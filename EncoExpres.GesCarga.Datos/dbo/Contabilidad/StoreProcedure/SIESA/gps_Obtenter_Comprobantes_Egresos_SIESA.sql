﻿PRINT 'gps_Obtenter_Comprobantes_Egresos_SIESA'
GO
DROP PROCEDURE gps_Obtenter_Comprobantes_Egresos_SIESA
GO
CREATE PROCEDURE gps_Obtenter_Comprobantes_Egresos_SIESA
(
	@par_EMPR_Codigo smallint
)
AS
BEGIN
	SELECT TOP 10
	ENCC.EMPR_Codigo,
	ENCC.Numero, ENCC.Fecha, ENCC.Numero_Documento, ENCC.Fecha_Documento, ENCC.TERC_Documento,
	ENCC.OFIC_Documento, ENCC.CATA_DOOR_Codigo, ENCC.Observaciones, ENCC.Valor_Comprobante, ENCC.OFIC_Codigo,
	ISNULL(ENCC.Interfase_Contable, 0) AS Interfase_Contable, ENCC.Anulado,
	DECC.PLUC_Codigo, PLUC.Codigo_Cuenta, DECC.Valor_Base, DECC.Valor_Credito, DECC.Valor_Debito,
	DECC.Documento_Cruce, TERC.Numero_Identificacion AS Identificacion_Tercero
	FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Terceros AS TERC, Plan_Unico_Cuentas As PLUC
	WHERE ENCC.EMPR_Codigo = @par_EMPR_Codigo
	AND ENCC.EMPR_Codigo = DECC.EMPR_Codigo
	AND ENCC.Numero = DECC.ENCC_Numero
	AND ENCC.EMPR_Codigo = TERC.EMPR_Codigo
	AND ENCC.TERC_Documento = TERC.Codigo
	AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo
	AND DECC.PLUC_Codigo = PLUC.Codigo
	AND ISNULL(ENCC.Interfase_Contable, 0) = 0
	AND ISNULL(ENCC.Intentos_Interfase_Contable, 0) < 3
	AND ENCC.Anulado = 0
	ORDER BY ENCC.Numero
END
GO