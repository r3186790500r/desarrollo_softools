﻿PRINT ' gsp_modificar_plan_unico_cuentas'
GO
DROP PROCEDURE gsp_modificar_plan_unico_cuentas
GO
CREATE PROCEDURE gsp_modificar_plan_unico_cuentas
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC,
@par_CATA_CCPU_Codigo NUMERIC,
@par_Codigo_Cuenta VARCHAR(50),
@par_Codigo_Alterno VARCHAR(20),
@par_Nombre VARCHAR(100),
@par_Exige_Centro_Costo SMALLINT,
@par_Exige_Tercero SMALLINT,
@par_Exige_Valor_Base SMALLINT,
@par_Valor_Base MONEY,
@par_Exige_Documento_Cruce SMALLINT,
@par_Estado SMALLINT,
@par_USUA_Codigo_Modifica NUMERIC
)
AS
BEGIN

UPDATE Plan_Unico_Cuentas
SET
CATA_CCPU_Codigo = @par_CATA_CCPU_Codigo,
Codigo_Cuenta = @par_Codigo_Cuenta,
Codigo_Alterno = @par_Codigo_Alterno,
Nombre = @par_Nombre,
Exige_Centro_Costo = @par_Exige_Centro_Costo,
Exige_Tercero = @par_Exige_Tercero,
Exige_Valor_Base = @par_Exige_Valor_Base,
Valor_Base =  @par_Valor_Base,
Exige_Documento_Cruce = @par_Exige_Documento_Cruce,
Estado = @par_Estado,
USUA_Modifica = @par_USUA_Codigo_Modifica,
Fecha_Modifica = GETDATE()

WHERE
EMPR_Codigo = @par_EMPR_Codigo
AND Codigo = @par_Codigo

SELECT @par_Codigo AS Codigo

END
GO