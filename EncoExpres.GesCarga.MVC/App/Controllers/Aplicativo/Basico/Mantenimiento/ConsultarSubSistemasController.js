﻿EncoExpresApp.controller("ConsultarSubsistemasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'ValorCatalogosFactory', 'TipoSubsistemasFactory', 'SistemasFactory', 'LineaMantenimientoFactory', 'SubSistemasFactory', 'blockUI', function ($scope, $routeParams, $timeout, $linq, ValorCatalogosFactory, TipoSubsistemasFactory, SistemasFactory, LineaMantenimientoFactory, SubSistemasFactory, blockUI) {
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Mantenimineto' }, { Nombre: 'Subsistemas' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
    $scope.Buscando = false;
    $scope.ResultadoSinRegistros = ''; 
    $scope.MensajesError = [];
    var filtros = {};
    $scope.CodigoAlterno = '';
    $scope.ListadoEstados = [];
    $scope.totalRegistros = 0;
    $scope.paginaActual = 1;
    $scope.cantidadRegistrosPorPagina = 10;
    $scope.totalPaginas = 0;

    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SUBSISTEMAS);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    // -------------------------- Constantes ---------------------------------------------------------------------//

    $scope.CODIGO_CATALOGO_ESTADO_TERCEROS = 8656;

    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };
    /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
    /*Cargar el combo de estados*/ 
    $scope.ListadoEstados = [
        { Nombre: '(TODOS)', Codigo: -1 },
        { Nombre: 'ACTIVA', Codigo: 1 },
        { Nombre: 'INACTIVA', Codigo: 0 }
    ];
    $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

    /*Cargar el combo de estados*/
    SistemasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoSistemaMantenimiento = [];
                $scope.ListadoSistemaMantenimiento = response.data.Datos;
                $scope.ListadoSistemaMantenimiento.push({ Nombre: '(TODOS)', Codigo: 0 });
                $scope.ListadoSistemaMantenimiento.Asc = true
                OrderBy('Nombre', undefined, $scope.ListadoSistemaMantenimiento)
                $scope.SistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSistemaMantenimiento).First('$.Codigo == 0');
                $timeout(function () {
                    $('#sistemaMantenimiento').selectpicker()
                }, 100) 
            }
        }, function (response) {
            ShowError(response.statusText);
        });
    /*Cargar el combo de estados*/
    TipoSubsistemasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoTipoSistemaMantenimiento = [];
                $scope.ListadoTipoSistemaMantenimiento = response.data.Datos;
                $scope.ListadoTipoSistemaMantenimiento.push({ Nombre: '(TODOS)', Codigo: -1 });
                $scope.TipoSubSistema = $linq.Enumerable().From($scope.ListadoTipoSistemaMantenimiento).First('$.Codigo == -1');
            }
        }, function (response) {
            ShowError(response.statusText);
        });
     

    /*Metodo buscar*/
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            Find()
        }
    };
    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarSubsistemas';
        }
    };
    /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
    function Find() {
        blockUI.start('Buscando registros ...'); 
        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);

        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoSubSistemaMantenimiento = [];

        filtros = {
            Pagina: $scope.paginaActual,
            RegistrosPagina: $scope.cantidadRegistrosPorPagina,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Codigo,
            CodigoAlterno: $scope.CodigoAlterno,
            Nombre: $scope.Nombre,
            SistemaMantenimiento: $scope.SistemaMantenimiento,
            TipoSubSistema: $scope.TipoSubSistema,
            Estado: $scope.Estado,
        };

        DatosRequeridos();

        if ($scope.MensajesError.length == 0) {
            blockUI.delay = 1000;
            SubSistemasFactory.Consultar(filtros).
                then(function (response) { 
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoSubSistemaMantenimiento = response.data.Datos;
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        else {
            $scope.Buscando = false;
        }
        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
    function DatosRequeridos() {
        if (filtros.Codigo == undefined) {
            filtros.Codigo = 0;
        }

        if (filtros.Codigo == undefined) {
            filtros.Codigo = 0;
        }

        if ($scope.CodigoInicial != undefined && $scope.CodigoFinal == undefined) {
            $scope.CodigoFinal = $scope.CodigoInicial
        }

        if (($scope.CodigoFinal != undefined) && ($scope.CodigoInicial == undefined)) {
            $scope.MensajesError.push('Debe ingresar el código inicial para filtrar');
            continuar = false;
        }

        if (($scope.CodigoInicial != undefined) && ($scope.CodigoFinal != undefined)) {
            if ($scope.CodigoInicial > $scope.CodigoFinal) {
                $scope.MensajesError.push('El código inicial debe ser menor al código final');
                continuar = false;
            }
        }



    }
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    $scope.MaskNumero = function (option) {
        try { $scope.opCodigotion = MascaraNumero($scope.Codigo) } catch (e) { }
    };
    $scope.MaskMayus = function () {
        MascaraMayusGeneral($scope);
    }

    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Codigo = $routeParams.Codigo;
 
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.SistemaMantenimiento = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');
        $scope.TipoSubSistema = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        Find();
        $scope.Codigo = ''
    }

   
}]);

 