﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="TipoDocumentos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TipoDocumentos
        Inherits BaseDocumento
        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="TipoDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            Codigo = Read(lector, "Codigo")
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Nombre = Read(lector, "Nombre")
            Consecutivo = Read(lector, "Consecutivo")
            TipoNumeracion = Read(lector, "CATA_TIGN_Codigo")
            CierreContable = Read(lector, "Cierre_Contable")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
            Obtener = Read(lector, "Obtener")
            If Obtener = 1 Then
                ConsecutivoFin = Read(lector, "Consecutivo_Hasta")
                ControladorConsecutivo = Read(lector, "Controlar_Consecutivo_Hasta")
                DocumentosFaltantesAviso = Read(lector, "Numero_Documentos_Faltantes_Aviso")
                NotaInicio = Read(lector, "Notas_Inicio")
                NotaFin = Read(lector, "Notas_Fin")
                Estado = Read(lector, "Estado")
            End If
            GeneraComprobanteContable = Read(lector, "Genera_Comprobante_Contable")
        End Sub

        <JsonProperty>
        Public Property GeneraComprobanteContable As Short

        <JsonProperty>
        Public Property AplicaConceptoContables As Short

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Consecutivo As Double

        <JsonProperty>
        Public Property ControladorConsecutivo As Double

        <JsonProperty>
        Public Property DocumentosFaltantesAviso As Double
        <JsonProperty>
        Public Property CodigoImpuesto As Integer
        <JsonProperty>
        Public Property CierreContable As Integer
        <JsonProperty>
        Public Property TipoNumeracion As Integer

        <JsonProperty>
        Public Property ConsecutivoFin As Double
        <JsonProperty>
        Public Property NotaInicio As String
        <JsonProperty>
        Public Property NotaFin As String
        <JsonProperty>
        Public Property ListadoImpuestos As IEnumerable(Of ImpuestoTipoDocumentos)

        <JsonProperty>
        Public Property AplicaDetalle As Integer

    End Class

End Namespace
