﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Reportes.aspx.vb" Inherits="EncoExpres.GesCarga.ASP.Reportes" EnableEventValidation="false" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
            <asp:Image runat="server" ID="QRImage"/>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="686px">
        </rsweb:ReportViewer>
        <asp:Label ID="lblMensajeError" runat="server" EnableViewState="false" ForeColor="Red"></asp:Label>
        </div>
        <asp:HiddenField runat="server" ID="reFactByte" />
    </form>
</body>
</html>


