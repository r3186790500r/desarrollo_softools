﻿EncoExpresApp.controller("GenerarInterfazContableController", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'OficinasFactory', 'ValorCatalogosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, OficinasFactory, ValorCatalogosFactory) {
        $scope.Titulo = 'GESTIONAR ARCHIVO UIAF';
        $scope.MapaSitio = [{ Nombre: 'Contabilidad' }, { Nombre: 'Procesos' }, { Nombre: 'Generar Interfaz Contable' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + CODIGO_GENERAR_ARCHIVO_UIAF); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        //Obtiene la URL para llamar el proyecto ASP
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.Sistema = []
        $scope.Documento = []
        $scope.ListadoEstados = [

            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: '(TODOS)', Codigo: 2 },

        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');

        $scope.Generar = function () {
            blockUI.start('Generando Interfaz ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Generando = true;
            $scope.MensajesError = [];
            $scope.ListadoComprobantes = [];


            if (DatosRequeridos()) {
                var fechainicial = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaInicio);
                var fechafinal = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaFinal);
                var estadoDocumento = $scope.Estado.Codigo;
                var OficinasCodigo = $scope.Oficina.Codigo; 
                var CodigoUsuario = $scope.Sesion.UsuarioAutenticado.Codigo;
                window.open($scope.urlASP + '/InterfazContable/GenerarinterfazContable.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&FechInic=' + fechainicial + '&FechFina=' + fechafinal + '&CodigoUsuario=' + CodigoUsuario + '&SistemaContable=' + $scope.Sistema.Codigo + '&TipoDocumento=' + $scope.Documento.Codigo + '&Estado=' + estadoDocumento + '&Oficina=' + OficinasCodigo);

            }


            blockUI.stop();
        };
        /*Cargar el combo de oficinas*/
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                $scope.ListadoOficinas = [];
                $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' });
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item)
                    });
                    if ($scope.Sesion.UsuarioAutenticado.Prefijo == PREFIJO_INTEGRA) {
                        $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 1');
                    } else {
                        $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        function DatosRequeridos() {

            var continuar = true;

            if ($scope.ModeloFechaInicio == null) {
                $scope.MensajesError.push('Debe ingresar un rango de fechas para generar la interfaz');
                continuar = false;
            }

            if ($scope.ModeloFechaInicio !== null && $scope.ModeloFechaFinal !== null) {
                if ($scope.ModeloFechaInicio > $scope.ModeloFechaFinal) {
                    $scope.MensajesError.push('La fecha inicial debe ser menor que la fecha final');
                    continuar = false;
                }
            }
            if ($scope.ModeloFechaInicio !== null && $scope.ModeloFechaFinal == null) {
                $scope.ModeloFechaFinal = $scope.ModeloFechaInicio;
            }
            else if ($scope.ModeloFechaInicio == null && $scope.ModeloFechaFinal !== null) {
                $scope.MensajesError.push('Debe seleccionar la fecha inicial');
                continuar = false;
            }
            return continuar

        }
        /*Cargar el combo de Documenteos Origen*/
        ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: {
                Codigo: CODIGO_CATALOGO_SISTEMA_CONTABLE
            }
            ,
            Estado: 1
        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoContable = response.data.Datos;
                    $scope.Sistema = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de Documenteos Origen*/
        ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ARCHIVO_CONTABLE }
            ,
            Estado: 1
        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDocumento = response.data.Datos;
                    $scope.Documento = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });



    }]);