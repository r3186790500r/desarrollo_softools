﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Negocio.Basico.Tesoreria

''' <summary>
''' Controlador <see cref="EncabezadoConceptoContablesController"/>
''' </summary>
<Authorize>
Public Class EncabezadoConceptoContablesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoConceptoContables) As Respuesta(Of IEnumerable(Of EncabezadoConceptoContables))
        Return New LogicaEncabezadoConceptoContables(CapaPersistenciaEncabezadoConceptoContables, CapaPersistenciaDetalleConceptoContables).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoConceptoContables) As Respuesta(Of EncabezadoConceptoContables)
        Return New LogicaEncabezadoConceptoContables(CapaPersistenciaEncabezadoConceptoContables, CapaPersistenciaDetalleConceptoContables).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoConceptoContables) As Respuesta(Of Long)
        Return New LogicaEncabezadoConceptoContables(CapaPersistenciaEncabezadoConceptoContables, CapaPersistenciaDetalleConceptoContables).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EncabezadoConceptoContables) As Respuesta(Of Boolean)
        Return New LogicaEncabezadoConceptoContables(CapaPersistenciaEncabezadoConceptoContables, CapaPersistenciaDetalleConceptoContables).Anular(entidad)
    End Function
End Class
