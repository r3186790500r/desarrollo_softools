﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" impuestosCiudades"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class impuestosCiudades
        Inherits Ciudades

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="impuestosCiudades"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="impuestosCiudades"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            ValorTarifa = Read(lector, "Valor_Tarifa")
            ValorBase = Read(lector, "Valor_Base")
            CuentaPUC = New PlanUnicoCuentas With {.CodigoNombreCuenta = Read(lector, "NombreCuenta"), .Codigo = Read(lector, "CodigoPuc")}

            If lector("Obtener") = 1 Then

            Else

            End If

        End Sub

        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property ValorTarifa As Double
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property ValorBase As Double

        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas

    End Class
End Namespace
