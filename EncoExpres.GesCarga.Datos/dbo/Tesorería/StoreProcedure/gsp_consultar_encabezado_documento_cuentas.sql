﻿PRINT 'gsp_consultar_encabezado_documento_cuentas'
GO
DROP PROCEDURE gsp_consultar_encabezado_documento_cuentas
GO
CREATE PROCEDURE gsp_consultar_encabezado_documento_cuentas
(
 @par_EMPR_codigo SMALLINT,	
 @par_TIDO_Codigo NUMERIC,
 @par_TERC_Codigo NUMERIC,
 @par_DOOR_Codigo NUMERIC,
 @par_Numero_Documento NUMERIC = NULL,
 @par_NumeroPagina INT = NULL,  
 @par_RegistrosPagina INT = NULL  
)
AS
BEGIN  
  DECLARE @CantidadRegistros INT    
  DECLARE @intEstado INT 

  SELECT @CantidadRegistros = (    
    SELECT DISTINCT     
     COUNT(1)     
	FROM
	Encabezado_Documento_Cuentas ENDC,
	Terceros  TERC 
	WHERE 

	ENDC.EMPR_Codigo = TERC.EMPR_Codigo
	AND ENDC.TERC_Codigo = TERC.Codigo

	AND ENDC.EMPR_Codigo = @par_EMPR_Codigo
	AND ENDC.TIDO_Codigo = @par_TIDO_Codigo
	AND ENDC.TERC_Codigo = @par_TERC_Codigo
	AND ENDC.CATA_DOOR_Codigo = @par_DOOR_Codigo
	AND ENDC.Documento_Origen = ISNULL(@par_Numero_Documento,ENDC.Documento_Origen)
	AND ENDC.Estado > 0
	AND ENDC.Anulado = 0
	AND Saldo > 0
     );           
    WITH Pagina AS    
    (    
    
	SELECT 
	ENDC.EMPR_Codigo,
	ENDC.Codigo,
	ENDC.Numero,
	ENDC.TIDO_Codigo,
	ENDC.Codigo_Alterno,
	ENDC.Fecha,
	ENDC.TERC_Codigo,
	ENDC.CATA_DOOR_Codigo,
	ENDC.Codigo_Documento_Origen,
	ENDC.Documento_Origen,
	ENDC.Fecha_Documento_Origen,
	ENDC.Fecha_Vence_Documento_Origen,
	ENDC.Valor_Total,
	ENDC.Abono,
	ENDC.Saldo,
	ENDC.Anulado,
	ENDC.Estado,
	ISNULL(TERC.Razon_Social,'') + ' ' + ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1,'') + ' ' + ISNULL(TERC.Apellido2,'') As NombreTercero,
    ROW_NUMBER() OVER(ORDER BY Numero) AS RowNumber    
	FROM
	Encabezado_Documento_Cuentas ENDC,
	Terceros  TERC 
	WHERE 

	ENDC.EMPR_Codigo = TERC.EMPR_Codigo
	AND ENDC.TERC_Codigo = TERC.Codigo
	AND ENDC.EMPR_Codigo = 1
	AND ENDC.TIDO_Codigo = 90
	AND ENDC.TERC_Codigo = @par_TERC_Codigo
	AND ENDC.CATA_DOOR_Codigo = @par_DOOR_Codigo
	AND ENDC.Documento_Origen = ISNULL(@par_Numero_Documento,ENDC.Documento_Origen)
	AND ENDC.Anulado = 0
	AND Saldo > 0
  )     
  SELECT DISTINCT    
	EMPR_Codigo,
	Codigo,
	Numero,
	TIDO_Codigo,
	Codigo_Alterno,
	Fecha,
	TERC_Codigo,
	CATA_DOOR_Codigo,
	Codigo_Documento_Origen,
	Documento_Origen,
	Fecha_Documento_Origen,
	Fecha_Vence_Documento_Origen,
	Valor_Total,
	Abono,
	Saldo,
	Anulado,
	Estado,
	NombreTercero,  
   @CantidadRegistros AS TotalRegistros,    
   @par_NumeroPagina AS PaginaObtener,    
   @par_RegistrosPagina AS RegistrosPagina    
  FROM    
   Pagina    
  WHERE    
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, 1000)  
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, 1000)  
  ORDER BY Numero ASC    

END
GO