﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Repositorio.Basico

Namespace Basico
    Public NotInheritable Class PersistenciaCuentaBancariaOficinas
        Implements IPersistenciaBase(Of CuentaBancariaOficinas)
        Public Function Consultar(filtro As CuentaBancariaOficinas) As IEnumerable(Of CuentaBancariaOficinas) Implements IPersistenciaBase(Of CuentaBancariaOficinas).Consultar
            Return New RepositorioCuentaBancariaOficinas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As CuentaBancariaOficinas) As Long Implements IPersistenciaBase(Of CuentaBancariaOficinas).Insertar
            Return New RepositorioCuentaBancariaOficinas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As CuentaBancariaOficinas) As Long Implements IPersistenciaBase(Of CuentaBancariaOficinas).Modificar
            Return New RepositorioCuentaBancariaOficinas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As CuentaBancariaOficinas) As CuentaBancariaOficinas Implements IPersistenciaBase(Of CuentaBancariaOficinas).Obtener
            Return New RepositorioCuentaBancariaOficinas().Obtener(filtro)
        End Function

        Public Function ConsultarCuentaOficinas(codigoEmpresa As Short, codigoCuenta As Short) As IEnumerable(Of CuentaBancariaOficinas)
            Return New RepositorioCuentaBancariaOficinas().ConsultarCuentaOficinas(codigoEmpresa, codigoCuenta)
        End Function

        Public Function InsertarCuentaOficina(detalle As IEnumerable(Of CuentaBancariaOficinas))
            Return New RepositorioCuentaBancariaOficinas().InsertarCuentaOficinas(detalle)
        End Function
    End Class
End Namespace
