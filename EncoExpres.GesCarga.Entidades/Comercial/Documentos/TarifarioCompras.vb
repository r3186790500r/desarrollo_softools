﻿Imports Newtonsoft.Json

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref=" TarifarioCompras"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TarifarioCompras
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TarifarioCompras"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TarifarioCompras"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Numero")
            Nombre = Read(lector, "Nombre")
            FechaInicio = Read(lector, "Fecha_Vigencia_Inicio")
            FechaFin = Read(lector, "Fecha_Vigencia_Fin")
            Estado = New Estado With {.Codigo = Read(lector, "Estado")}
            Obtener = Read(lector, "Obtener")
            TarifarioBase = Read(lector, "Tarifario_Base")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            Else
            End If
        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property TarifarioBase As Integer
        <JsonProperty>
        Public Property FechaInicio As DateTime
        <JsonProperty>
        Public Property FechaFin As DateTime
        <JsonProperty>
        Public Property Estado As Estado
        <JsonProperty>
        Public Property Tarifas As IEnumerable(Of DetalleTarifarioCompras)
        <JsonProperty>
        Public Property Numero As Integer
        <JsonProperty>
        Public Property OpcionCargue As Integer
        <JsonProperty>
        Public Property UsuarioAnula As Integer



    End Class
End Namespace
