﻿EncoExpresApp.controller("ConsultarPeajesCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'ValorCatalogosFactory', 'PeajesFactory',
    'TercerosFactory',
    function ($scope, $timeout, $linq, blockUI, blockUIConfig, $routeParams, ValorCatalogosFactory, PeajesFactory,
        TercerosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Peajes' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.InputValor = true;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.ListadoTipoVehiculo = [];
        $scope.modalTarifasPeaje = {
            Categoria: '',
            Valor: '',
        }
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PEAJES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        //-- Catalogo Tipo Vehiculo
        $scope.ListadoTipoVehiculo = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != CODIGO_TIPO_VEHICULO_NO_APLICA) {
                                $scope.ListadoTipoVehiculo.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                            }
                        }
                        $scope.modalTarifasPeaje.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == ' + response.data.Datos[0].Codigo);
                       
                    }
                    else {
                        $scope.ListadoTipoVehiculo = [];
                    }
                }
            }, function (response) {
            });

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarPeajes';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        //-------------------------------------- CATALOGOS --------------------------------------//
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PEAJE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoPeaje = [];
                    $scope.ListadoTipoPeaje.push({ Codigo: -1, Nombre: '(TODOS)' });
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoTipoPeaje.push(response.data.Datos[i]);
                        }
                        $scope.TipoPeaje = $linq.Enumerable().From($scope.ListadoTipoPeaje).First('$.Codigo == -1');
                    }
                    else {
                        $scope.ListadoTipoPeaje = [];
                    }
                }
            }, function (response) {
            });
        //-------------------------------------- CATALOGOS --------------------------------------//
        //-------------------------------------- PROVEEDORES --------------------------------------//
        $scope.ListadoProveedores = [];
        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_PROVEEDOR, ValorAutocomplete: value, Sync: true
                    })

                    $scope.ListadoProveedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedores);
                    
                }
            }
            return $scope.ListadoProveedores;
        }
        //-------------------------------------- PROVEEDORES --------------------------------------//


        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoPeajes = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Nombre: $scope.Nombre,
                        Codigo: $scope.Codigo,
                        Estado: $scope.Estado.Codigo,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        TipoPeaje: { Codigo: $scope.TipoPeaje == undefined ? 0 : $scope.TipoPeaje.Codigo },
                        Proveedor: { Codigo: $scope.Proveedor == undefined ? 0 : $scope.Proveedor.Codigo }
                    }
                    blockUI.delay = 1000;
                    PeajesFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    response.data.Datos.forEach(function (item) {
                                        var registro = item;
                                        $scope.ListadoPeajes.push(registro);
                                    });

                                    $scope.ListadoPeajes.forEach(function (item) {
                                        $scope.codigo = item.Codigo;
                                    });
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                    console.log($scope.ListadoPeajes)
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        /*------------------------------------------------------------------------------------Eliminar Peajes-----------------------------------------------------------------------*/
        $scope.EliminarPeaje = function (codigo, nombre) {
            $scope.AuxiCodigo = codigo;
            $scope.AuxiNombre = nombre;
            $scope.ModalErrorCompleto = '';
            $scope.ModalError = '';
            $scope.MostrarMensajeError = false;
            showModal('modalEliminarPeaje');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.AuxiCodigo,
            };

            PeajesFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó el peaje ' + $scope.AuxiNombre);
                        closeModal('modalEliminarPeaje');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarPeaje');
                    $scope.ModalError = 'No se puede eliminar el peaje ' + $scope.AuxiNombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText;

                });

        };

        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true;
        }

        $scope.CerrarModal = function () {
            closeModal('modalEliminarPeaje');
            closeModal('modalMensajeEliminarPeaje');
        }
        //-------------------------------------------------TARIFAS PEAJE--------------------------------------------------------//
        $scope.isEditTarifasPeaje = false;
        $scope.ListaTarifasPeaje = [];
        $scope.TMPTarifa = {};
        $scope.modalTarifasPeaje = {
            Categoria: '',
            Valor: '',
        }
        //-- Catalogo Categorias Peajes
        $scope.ListadoCategorias = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CATEGORIAS_PEAJE }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoCategorias.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo, CampoAuxiliar2: response.data.Datos[i].CampoAuxiliar2 });
                        }
                        $scope.modalTarifasPeaje.Categoria = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo == ' + response.data.Datos[0].Codigo);
                    }
                    else {
                        $scope.ListadoCategorias = [];
                    }
                }
            }, function (response) {
            });
        //--Modal Tarifas Peaje
        $scope.AbrirTarifasPeaje = function (item) {
            $scope.TMPTarifa = item;
            
            showModal('modalTarifasPeaje');
            $scope.LimpiarmodalTarifasPeaje();
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: item.Codigo,
                ConsultarTarifasPeajes: true
            }
            blockUI.delay = 1000;
            PeajesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos !== undefined) {
                            $scope.ListaTarifasPeaje = response.data.Datos.TarifasPeajes;
                            for (var i = 0; i < $scope.ListaTarifasPeaje.length; i++) {
                                $scope.ListaTarifasPeaje[i].editable = false;
                            }
                            OrdenarIndicesTarifasPeaje();
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.AdicionarTarifasPeaje = function () {
            if (DatosRequeridosTarifasPeaje()) {
                $scope.modalTarifasPeaje.ValorRemolque1Eje = $scope.modalTarifasPeaje.ValorRemolque1Eje == undefined || $scope.modalTarifasPeaje.ValorRemolque1Eje == '' ? '' : $scope.modalTarifasPeaje.ValorRemolque1Eje;
                $scope.modalTarifasPeaje.ValorRemolque2Ejes = $scope.modalTarifasPeaje.ValorRemolque2Ejes == undefined || $scope.modalTarifasPeaje.ValorRemolque2Ejes == '' ? '' : $scope.modalTarifasPeaje.ValorRemolque2Ejes;
                $scope.modalTarifasPeaje.ValorRemolque3Ejes = $scope.modalTarifasPeaje.ValorRemolque3Ejes == undefined || $scope.modalTarifasPeaje.ValorRemolque3Ejes == '' ? '' : $scope.modalTarifasPeaje.ValorRemolque3Ejes;
                $scope.modalTarifasPeaje.ValorRemolque4Ejes = $scope.modalTarifasPeaje.ValorRemolque4Ejes == undefined || $scope.modalTarifasPeaje.ValorRemolque4Ejes == '' ? '' : $scope.modalTarifasPeaje.ValorRemolque4Ejes;
                if ($scope.isEditTarifasPeaje == false) {
                    $scope.ListaTarifasPeaje.push($scope.modalTarifasPeaje);
                    OrdenarIndicesTarifasPeaje();
                }
                else {
                    $scope.ListaTarifasPeaje[$scope.modalTarifasPeaje.indice] = $scope.modalTarifasPeaje;
                }
                $scope.LimpiarmodalTarifasPeaje();
            }
        }

        function DatosRequeridosTarifasPeaje() {
            $scope.MensajesErrorTarifas = [];
            var continuar = true;

            if ($scope.modalTarifasPeaje.Categoria == undefined || $scope.modalTarifasPeaje.Categoria == "" || $scope.modalTarifasPeaje.Categoria == null) {
                $scope.MensajesErrorTarifas.push('Debe seleccionar una categoría');
                continuar = false;
            }
            if ($scope.modalTarifasPeaje.Valor == undefined || $scope.modalTarifasPeaje.Valor == "" || $scope.modalTarifasPeaje.Valor == null) {
                $scope.MensajesErrorTarifas.push('Debe ingresar valor sin remolque');
                continuar = false;
            }

            var indice = $scope.modalTarifasPeaje.indice != undefined ? $scope.modalTarifasPeaje.indice : -1;

            if (continuar == true) {
                for (var i = 0; i < $scope.ListaTarifasPeaje.length; i++) {
                    var item = $scope.ListaTarifasPeaje[i];
                    if ($scope.isEditTarifasPeaje == true) {
                        if (indice != i) {
                            if (item.TipoVehiculo.Codigo == $scope.modalTarifasPeaje.TipoVehiculo.Codigo) {
                                $scope.MensajesErrorTarifas.push('El tipo de vehículo ya se encuentra agregado');
                                continuar = false;
                                break;
                            }
                        }
                    }
                    else {
                        if (item.TipoVehiculo.Codigo == $scope.modalTarifasPeaje.TipoVehiculo.Codigo) {
                            $scope.MensajesErrorTarifas.push('Esta tipo vehículo ya se ingresó');
                            continuar = false;
                            break;
                        }
                    }
                }
            }

            return continuar;
        }

        $scope.LimpiarmodalTarifasPeaje = function () {
            
            for (var i = 0; i < $scope.ListaTarifasPeaje.length; i++) {
                $scope.ListaTarifasPeaje[i].editable = false;
            }
            $scope.modalTarifasPeaje = {
                Categoria: '',
                Valor: '',
                TipoVehiculo: $scope.ListadoTipoVehiculo[0]
            };
            $scope.modalTarifasPeaje.Categoria = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo == ' + $scope.ListadoCategorias[0].Codigo);
            $scope.MensajesErrorTarifas = [];
            $scope.isEditTarifasPeaje = false;
        }

        function OrdenarIndicesTarifasPeaje() {
            for (var i = 0; i < $scope.ListaTarifasPeaje.length; i++) {
                $scope.ListaTarifasPeaje[i].indice = i;
            }
        }

        $scope.EditarTarifasPeaje = function (item) {
            $scope.InputValor = false;
            $scope.LimpiarmodalTarifasPeaje();
            $scope.modalTarifasPeaje = angular.copy(item);
            $scope.modalTarifasPeaje.Categoria = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo == ' + item.Categoria.Codigo);
            $scope.modalTarifasPeaje.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo==' + item.TipoVehiculo.Codigo);
            $scope.modalTarifasPeaje.Valor = item.Valor;
            $scope.isEditTarifasPeaje = true;
            $scope.MaskNumero(); $scope.MaskValores();
        }

        //--Eliminar
        $scope.TmpEliminarTarifasPeaje = {};
        $scope.ConfirmarEliminarTarifasPeaje = function (item) {
            $scope.TmpEliminarTarifasPeaje = { Indice: item.indice };
            showModal('modalEliminarTarifasPeaje');
        };

        $scope.EliminarTarifasPeajes = function (indice) {
            $scope.ListaTarifasPeaje.splice(indice, 1);
            OrdenarIndicesTarifasPeaje();
            closeModal('modalEliminarTarifasPeaje');
        };

        //--Almacena Informacion
        $scope.GuardarTarifasPeaje = function () {
            var tmpTarifasPeaje = [];
            if ($scope.ListaTarifasPeaje.length > 0) {
               
                for (var i = 0; i < $scope.ListaTarifasPeaje.length; i++) {
                    tmpTarifasPeaje.push({
                        Categoria: $scope.ListaTarifasPeaje[i].Categoria,
                        Valor: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.ListaTarifasPeaje[i].Valor)),
                        ValorRemolque1Eje: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.ListaTarifasPeaje[i].ValorRemolque1Eje)),
                        ValorRemolque2Ejes: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.ListaTarifasPeaje[i].ValorRemolque2Ejes)),
                        ValorRemolque3Ejes: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.ListaTarifasPeaje[i].ValorRemolque3Ejes)),
                        ValorRemolque4Ejes: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.ListaTarifasPeaje[i].ValorRemolque4Ejes)),
                        TipoVehiculo: { Codigo: $scope.ListaTarifasPeaje[i].TipoVehiculo.Codigo }
                    });
                }
            } else {

                //ShowError('Debe agregar al menos una Tarifa');
            }

            var TarifaPeaje = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.TMPTarifa.Codigo,
                TarifasPeajes: tmpTarifasPeaje,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            };

            PeajesFactory.InsertarTarifasPeajes(TarifaPeaje).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se guardaron los cambios satisfactoriamente')
                        closeModal('modalTarifasPeaje')
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
           
        }

        //-------------------------------------------------TARIFAS PEAJE--------------------------------------------------------//
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------//
        //------------------------------------------- Obtener parametros -------------------------------------------//
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }
        //------------------------------------------- Obtener parametros -------------------------------------------//
        //------------------------------------------- Mascaras -------------------------------------------//
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item);
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item);
        };

        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope);
        }

        //------------------------------------------- Mascaras -------------------------------------------//
    }]);