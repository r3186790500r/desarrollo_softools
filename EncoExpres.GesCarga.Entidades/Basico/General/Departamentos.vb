﻿Imports Newtonsoft.Json

Namespace Basico.General
    ''' <summary>
    ''' Clase <see cref="Departamentos"/>
    ''' </summary>
    <JsonObject>
    Public Class Departamentos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Departamentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Departamentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto DataReader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
                Pais = New Paises With {.Codigo = Read(lector, "PAIS_Codigo"), .Nombre = Read(lector, "Pais")}
            Else
                Pais = New Paises With {.Codigo = Read(lector, "PAIS_Codigo")}
            End If


        End Sub

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Pais As Paises

    End Class

End Namespace