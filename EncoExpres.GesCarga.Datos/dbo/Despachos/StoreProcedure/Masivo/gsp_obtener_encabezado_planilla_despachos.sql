﻿PRINT 'gsp_obtener_encabezado_planilla_despachos'
GO

DROP PROCEDURE gsp_obtener_encabezado_planilla_despachos
GO

CREATE PROCEDURE gsp_obtener_encabezado_planilla_despachos
(      
@par_EMPR_Codigo SMALLINT,      
@par_TIDO_Codigo SMALLINT,      
@par_Numero   NUMERIC,
@par_Numero_Documento NUMERIC = NULL,
@par_OFIC_Codigo SMALLINT = NULL
)
AS
BEGIN
	
	DECLARE @Numero NUMERIC

	IF @par_Numero_Documento > = 0 BEGIN
		SELECT
			@Numero = Numero
		FROM
			Encabezado_Planilla_Despachos
		WHERE
			EMPR_Codigo = @par_EMPR_Codigo
			AND Numero_Documento = @par_Numero_Documento
			AND TIDO_Codigo = @par_TIDO_Codigo
			AND OFIC_Codigo = @par_OFIC_Codigo
	END
	ELSE BEGIN
		SET @Numero = @par_Numero
	END

SELECT
	1 AS Obtener
   ,ENPD.EMPR_Codigo
   ,ENPD.Numero
   ,ENPD.TIDO_Codigo
   ,ENPD.Numero_Documento
   ,ENPD.ENMC_Numero
   ,ENPD.Fecha_Hora_Salida
   ,ENPD.Fecha
   ,ISNULL(ENPD.LNTC_Codigo_Compra, 0) AS LNTC_Codigo_Compra
   ,ISNULL(ENPD.TLNC_Codigo_Compra, 0) AS TLNC_Codigo_Compra
   ,ISNULL(ENPD.RUTA_Codigo, 0) AS RUTA_Codigo
   ,ENPD.TATC_Codigo_Compra
   ,ENPD.TTTC_Codigo_Compra
   ,ISNULL(ENPD.ETCC_Numero, 0) AS ETCC_Numero
   ,ENPD.VEHI_Codigo
   ,ISNULL(ENPD.SEMI_Codigo,'') AS SEMI_Codigo
   ,ISNULL(SEMI.Placa,'') AS SEMI_PLaca
   ,ENPD.TERC_Codigo_Tenedor
   ,ENPD.TERC_Codigo_Conductor
   ,ENPD.Cantidad
   ,ENPD.Peso
   ,
	--sub divicion    
	ENPD.Valor_Flete_Transportador
   , --quitar la palabra valor     
	ENPD.Valor_Anticipo
   ,ENPD.Valor_Impuestos
   ,ENPD.Valor_Pagar_Transportador
   ,ENPD.Valor_Flete_Cliente
   ,ENPD.Valor_Seguro_Mercancia
   ,ENPD.Valor_Otros_Cobros
   ,ENPD.Valor_Total_Credito
   ,ENPD.Valor_Total_Contado
   ,ENPD.Valor_Total_Alcobro
   ,ENPD.Valor_Auxiliares
   ,
	--fin division     
	ENPD.Observaciones
   ,ENPD.Anulado
   ,ENPD.Estado
   ,ENPD.Fecha_Crea
   ,ENPD.USUA_Codigo_Crea
   ,ISNULL(ENPD.Fecha_Modifica, '') AS Fecha_Modifica
   ,ISNULL(ENPD.USUA_Codigo_Modifica, 0) AS USUA_Codigo_Modifica
   ,ISNULL(ENPD.Fecha_Anula, '') AS Fecha_Anula
   ,ISNULL(ENPD.USUA_Codigo_Anula, 0)
   ,ISNULL(ENPD.Causa_Anula, '') AS Causa_Anula
   ,ENPD.OFIC_Codigo
   ,OFIC.Nombre AS NombreOficina
   ,ENPD.Numeracion
   ,
	--Datos para el obtener     
	LNTC.Nombre AS LineaNegocio
   ,TLNC.Nombre AS TipoLineaNegocio
   ,VTTC.Nombre AS TipoTarifa
   ,VTTC.NombreTarifa AS NombreTrarifa
   ,ENPD.TATC_Codigo_Compra AS NumeroTarifario
   ,VEHI.Placa AS PlacaVehiculo
   ,SEMI.Placa AS SemiRemolPlaca
   ,ISNULL(COND.Razon_Social, '') + ' ' + ISNULL(COND.Nombre, '')
	+ ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') AS NombreConductor
   ,COND.Numero_Identificacion AS IdentificacionConductor
   ,ISNULL(TENE.Razon_Social, '') + ' ' + ISNULL(TENE.Nombre, '')
	+ ' ' + ISNULL(TENE.Apellido1, '') + ' ' + ISNULL(TENE.Apellido2, '') AS NombreTenedor
   ,TENE.Numero_Identificacion AS IdentificacionTenedor
   ,RUTA.Nombre AS NombreRuta
   ,
	---- REMESA     
	ISNULL(ENRE.Numero_Documento, 0) AS NumeroRemesa
   ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '')
	+ ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente
   ,ISNULL(PRTR.Nombre, '') AS NombreProducto
   ,ISNULL(ENRE.Cantidad_Cliente, 0) AS CantidadRemesa
   ,ISNULL(ENRE.Peso_Cliente, 0) AS PesoRemesa
   ,ISNULL(DEST.Razon_Social, '') + ' ' + ISNULL(DEST.Nombre, '')
	+ ' ' + ISNULL(DEST.Apellido1, '') + ' ' + ISNULL(DEST.Apellido2, '') AS NombreDestinatario
   ,ISNULL(ENPD.ECPD_Numero, 0) AS ECPD_Numero
   ,ISNULL(ENPD.ECPD_Numero,0) AS ECPD_Numero
   ,ISNULL(ECPD.Numero_Documento,0) AS NumeroCumplido
   ,ISNULL(ENPD.ENMC_Numero,0) AS ENMC_Numero
   ,ISNULL(ENMC.Numero_Documento,0) AS NumeroManifiesto
   ,ISNULL(ENPD.ELPD_Numero,0) AS ELPD_Numero
   ,ISNULL(ELPD.Numero_Documento,0) AS NumeroLiquidacion

FROM Encabezado_Planilla_Despachos ENPD
LEFT JOIN Encabezado_Remesas ENRE
	ON ENPD.EMPR_Codigo = ENRE.EMPR_Codigo
		AND ENPD.Numero = ENRE.ENPD_Numero

---- PLACA VEHICULO    
INNER JOIN Vehiculos VEHI
	ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo
		AND ENPD.VEHI_Codigo = VEHI.Codigo
---- SEMIREMOLQUE    
INNER JOIN Semirremolques SEMI
	ON ENPD.EMPR_Codigo = SEMI.EMPR_Codigo
		AND ENPD.SEMI_Codigo = SEMI.Codigo
---- TENEDOR    
INNER JOIN Terceros TENE
	ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo
		AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo
---- CONDUCTOR    
INNER JOIN Terceros COND
	ON VEHI.EMPR_Codigo = COND.EMPR_Codigo
		AND VEHI.TERC_Codigo_Conductor = COND.Codigo
---- LINEA DE NEGOCIO    
INNER JOIN Linea_Negocio_Transporte_Carga LNTC
	ON ENPD.EMPR_Codigo = LNTC.EMPR_Codigo
		AND ENPD.LNTC_Codigo_Compra = LNTC.Codigo
---- TIPO DE LINEA DE NEGOCIO    
INNER JOIN Tipo_Linea_Negocio_Carga TLNC
	ON ENPD.EMPR_Codigo = TLNC.EMPR_Codigo
		AND ENPD.TLNC_Codigo_Compra = TLNC.Codigo
		AND LNTC.Codigo = TLNC.LNTC_Codigo
---- RUTA     
INNER JOIN Rutas RUTA
	ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo
		AND ENPD.RUTA_Codigo = RUTA.Codigo
---- TARIFARIO     
INNER JOIN V_Tipo_Tarifa_Transporte_Carga VTTC
	ON ENPD.EMPR_Codigo = VTTC.EMPR_Codigo
		AND ENPD.TATC_Codigo_Compra = VTTC.TATC_Codigo
		AND ENPD.TTTC_Codigo_Compra = VTTC.Codigo
---- OFICINA     
INNER JOIN Oficinas OFIC
	ON ENPD.EMPR_Codigo = OFIC.EMPR_Codigo
		AND ENPD.OFIC_Codigo = OFIC.Codigo
---- PRODUCTO     
LEFT JOIN Producto_Transportados PRTR
	ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
		AND ENRE.PRTR_Codigo = PRTR.Codigo
---- CLIENTE     
LEFT JOIN Terceros CLIE
	ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo

---- DESTINATARIO     
LEFT JOIN Terceros DEST
	ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo
		AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo

LEFT JOIN Encabezado_Cumplido_Planilla_Despachos ECPD
	ON ENPD.EMPR_Codigo = ENPD.EMPR_Codigo
		AND ENPD.Numero = ECPD.ENPD_Numero

LEFT JOIN Encabezado_Manifiesto_Carga ENMC
	ON ENPD.EMPR_Codigo = ENMC.EMPR_Codigo
		AND ENPD.ENMC_Numero = ENMC.Numero

LEFT JOIN Encabezado_Liquidacion_Planilla_Despachos ELPD
	ON ENPD.EMPR_Codigo = ELPD.EMPR_Codigo
		AND ENPD.ELPD_Numero = ELPD.Numero

WHERE ENPD.EMPR_Codigo = @par_EMPR_Codigo
AND ENPD.TIDO_Codigo = @par_TIDO_Codigo
AND ENPD.Numero = @Numero
END

SELECT @Numero AS Numero
GO