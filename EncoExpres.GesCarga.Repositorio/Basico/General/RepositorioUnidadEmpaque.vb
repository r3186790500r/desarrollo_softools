﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioUnidadEmpaque"/>
    ''' </summary>
    Public NotInheritable Class RepositorioUnidadEmpaque
        Inherits RepositorioBase(Of UnidadEmpaque)

        Public Overrides Function Consultar(filtro As UnidadEmpaque) As IEnumerable(Of UnidadEmpaque)
            Dim lista As New List(Of UnidadEmpaque)
            Dim item As UnidadEmpaque

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_Unidad_Empaque]")

                While resultado.Read
                    item = New UnidadEmpaque(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As UnidadEmpaque) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As UnidadEmpaque) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As UnidadEmpaque) As UnidadEmpaque
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace