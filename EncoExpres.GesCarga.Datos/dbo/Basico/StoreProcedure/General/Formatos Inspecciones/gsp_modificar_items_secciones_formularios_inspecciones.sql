﻿PRINT 'gsp_modificar_items_secciones_formularios_inspecciones'
GO
DROP PROCEDURE gsp_modificar_items_secciones_formularios_inspecciones
GO
CREATE PROCEDURE gsp_modificar_items_secciones_formularios_inspecciones 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC,  
@par_ENFI_Codigo NUMERIC,  
@par_Nombre VARCHAR (50),  
@par_Orden_Formulario SMALLINT,  
@par_SFIN_Codigo NUMERIC,  
@par_CATA_TCFI_Codigo NUMERIC,  
@par_Relevante SMALLINT,  
@par_Tamaño SMALLINT,  
@par_Estado SMALLINT,  
@par_USUA_Codigo_Modifica SMALLINT  
)  
AS  
BEGIN  
  
IF  @par_Codigo > 0  
BEGIN  
  
 UPDATE Detalle_Items_Formularios_Inspecciones  
 SET  
 ENFI_Codigo = @par_ENFI_Codigo,  
 Nombre = @par_Nombre,  
 Orden_Formulario = @par_Orden_Formulario,  
 SFIN_Codigo = @par_SFIN_Codigo,  
 CATA_TCFI_Codigo = @par_CATA_TCFI_Codigo,  
 Tamaño = @par_Tamaño,  
 Relevante = @par_Relevante,  
 Estado = @par_Estado,  
 USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,  
 Fecha_Modifica = GETDATE()  
 WHERE   
 EMPR_Codigo = @par_EMPR_Codigo  
 AND Codigo = @par_Codigo  
  
 SELECT @par_Codigo  AS Codigo  
  
END  
ELSE   
BEGIN  
  
 INSERT INTO Detalle_Items_Formularios_Inspecciones  
 (  
 EMPR_Codigo,  
 ENFI_Codigo,  
 Nombre,  
 Orden_Formulario,  
 SFIN_Codigo,  
 CATA_TCFI_Codigo,  
 Relevante,  
 Tamaño,  
 Estado,  
 Fecha_Crea,  
 USUA_Codigo_Crea  
 )  
 VALUES   
 (  
 @par_EMPR_Codigo,  
 @par_ENFI_Codigo,  
 @par_Nombre,  
 @par_Orden_Formulario,  
 @par_SFIN_Codigo,  
 @par_CATA_TCFI_Codigo,  
 @par_Relevante,  
 @par_Tamaño,  
 @par_Estado,  
 GETDATE(),  
 @par_USUA_Codigo_Modifica  
 )  
  
 SELECT @@IDENTITY AS Codigo  
  
END    
  
END  
GO