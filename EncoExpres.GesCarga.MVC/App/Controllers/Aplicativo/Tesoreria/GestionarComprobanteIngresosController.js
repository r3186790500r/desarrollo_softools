﻿EncoExpresApp.controller("GestionarComprobanteIngresosCtrl", ['$scope', '$routeParams', '$linq', 'blockUI', '$timeout', 'TercerosFactory', 'ValorCatalogosFactory', 'OficinasFactory', 'DocumentoComprobantesFactory', 'CajasFactory', 'CuentaBancariasFactory', 'PlanUnicoCuentasFactory', 'VehiculosFactory', 'ConceptoContablesFactory', 'DetalleConceptoContablesFactory', 'CierreContableDocumentosFactory',

    function ($scope, $routeParams, $linq, blockUI, $timeout, TercerosFactory, ValorCatalogosFactory, OficinasFactory, DocumentoComprobantesFactory, CajasFactory, CuentaBancariasFactory, PlanUnicoCuentasFactory, VehiculosFactory, ConceptoContablesFactory, DetalleConceptoContablesFactory, CierreContableDocumentosFactory) {
        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */
        $scope.Titulo = 'GESTIONAR COMPROBANTES INGRESO';
        $scope.MapaSitio = [{ Nombre: 'Tesorería' }, { Nombre: 'Documentos' }, { Nombre: 'Comprobante de Ingreso' }, { Nombre: 'Gestionar' }];


        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_COMPROBANTE_INGRESOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 }
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');

        $scope.ListadoCuentasPUC = [];
        $scope.ListadoOficinas = [];
        $scope.ListadoDocumentosOrigen = [];
        $scope.ListadoCuentaBancariaOficinas = [];
        $scope.ListadoConceptoContable = [];
        $scope.ListadoVehiculos = [];
        $scope.ListaFormaPago = [];
        $scope.MensajesError = [];
        $scope.ListadoCuentaBancarias = [];
        $scope.ListadoMovimiento = [];
        $scope.ListadoTodaCajaOficinas = [];
        $scope.ListadoCajaOficinas = [];

        $scope.ActivarEfectivo = false
        $scope.ActivarCheque = false
        $scope.ActivarConsignacion = false
        $scope.PlacaValida = true;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Numero: 0,
            Fecha: new Date(),
            Debito: 0,
            Credito: 0,
            Diferencia: 0,
            ConceptoContable: { Codigo: 0 },
            OficinaCrea: $scope.Sesion.UsuarioAutenticado.Oficinas[0],
        }
        $scope.Modal = {

        }

        /* Obtener parametros del enrutador*/
        if ($routeParams.Numero !== undefined) {
            $scope.Modelo.Numero = $routeParams.Numero;
        }
        else {
            $scope.Modelo.Numero = 0;
            FindCierre()
        }

        function FindCierre() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentos: { Codigo: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_INGRESO },
                Sync: true
            };
            var RCierreContable = CierreContableDocumentosFactory.Consultar(filtros)
            if (RCierreContable.ProcesoExitoso === true) {
                if (RCierreContable.Datos.length > 0) {
                    $scope.ListadoTipoDocumentos = RCierreContable.Datos;
                    $scope.ValidarCierre();
                }
            }
        }

        $scope.ValidarCierre = function () {
            if ($scope.ListadoTipoDocumentos.length > 0) {
                var anoFecha = $scope.Modelo.Fecha.getFullYear();
                var mesFecha = $scope.Modelo.Fecha.getMonth();
                for (var k = 0; k < $scope.ListadoTipoDocumentos.length; k++) {
                    var item = $scope.ListadoTipoDocumentos[k];
                    var mes = MascaraNumero(item.Mes.CampoAuxiliar2);
                    if (item.Ano === anoFecha && mes === (mesFecha + 1) && item.EstadoCierre.Codigo === 18002) {
                        ShowError('La fecha se encuentra en un mes y año con cierre contable');
                        $scope.Modelo.Fecha = '';
                        $scope.PeriodoValido = false
                        break;
                    }
                }
            }
        };
        /*Cargar el autocomplete de cuentas PUC*/
        PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCuentasPUC = response.data.Datos

                        if ($scope.CodigoCuentaPUC !== undefined && $scope.CodigoCuentaPUC !== '' && $scope.CodigoCuentaPUC !== null) {
                            $scope.Modal.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                        }
                    }
                    else {
                        $scope.ListadoCuentasPUC = [];
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Oficinas*/
        //OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        //    then(function (response) {
        //        if (response.data.ProcesoExitoso === true) {
        //            if (response.data.ProcesoExitoso === true) {
        // $scope.ListadoOficinas = response.data.Datos
        $scope.ListadoOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos;
        if ($scope.CodigoOficinaDestino !== undefined && $scope.CodigoOficinaDestino !== '' && $scope.CodigoOficinaDestino !== null) {
            $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficinaDestino)
        }
        else {
            $scope.Modelo.OficinaDestino = $scope.ListadoOficinas[0];
        }
        //}
        //else {
        //    $scope.ListadoOficinas = [];
        //    }
        //}
        //}, function (response) {
        //});
        /*Cargar el combo de Documenteos Origen*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        if (item.CampoAuxiliar2.match(/IGR/)) {
                            $scope.ListadoDocumentosOrigen.push(item);
                        }
                    });
                    $scope.ListadoDocumentosOrigen.push({ Codigo: 2600, Nombre: "Seleccione un documento origen" });

                    if ($scope.CodigoDocumentoOrigen !== undefined && $scope.CodigoDocumentoOrigen !== '' && $scope.CodigoDocumentoOrigen !== null) {
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + $scope.CodigoDocumentoOrigen);
                    }
                    else {
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == 2600');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de Forma de pago*/
        //ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_DOCUMENTO_COMPROBANTES } }).
        //    then(function (response) {
        //        if (response.data.ProcesoExitoso === true) {
        //            $scope.ListaFormaPago = response.data.Datos

        //            if ($scope.CodigoFormaPago !== undefined && $scope.CodigoFormaPago !== '' && $scope.CodigoFormaPago !== null) {
        //                $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo ==' + $scope.CodigoFormaPago);
        //            }
        //            else {
        //                $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo == 4700');
        //            }
        //        }
        //    }, function (response) {
        //        ShowError(response.statusText);
        //    });
        $scope.ListaFormaPago = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_DOCUMENTO_COMPROBANTES }, Sync: true }).Datos;
        if ($scope.CodigoFormaPago !== undefined && $scope.CodigoFormaPago !== '' && $scope.CodigoFormaPago !== null) {
            $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo ==' + $scope.CodigoFormaPago);
        }
        else {
            $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo == 4700');
        }
        function CargarCuentasBancarias() {
            /*Cargar el combo Cuentas Bancarias por oficina*/
            CuentaBancariasFactory.CuentaBancariaOficinas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.OficinaDestino.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCuentaBancariaOficinas = [];
                        $scope.ListadoCuentaBancariaOficinas = response.data.Datos;

                        if ($scope.CodigoCuentaBancaria !== undefined && $scope.CodigoCuentaBancaria !== null && $scope.CodigoCuentaBancaria !== '' && $scope.CodigoCuentaBancaria !== 0) {
                            $scope.ListadoCuentaBancariaOficinas.forEach(function (item) {
                                if (item.CodigoCuenta > 0) {
                                    if (item.CodigoOficina == $scope.CodigoOficinaDestino) {
                                        $scope.ListadoCuentaBancarias.push(item);
                                    }
                                }
                            });

                            $scope.Modelo.CuentaBancariaCheque = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + $scope.CodigoCuentaBancaria);
                            $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + $scope.CodigoCuentaBancaria);
                        }
                        else {
                            $scope.ListadoCuentaBancarias = [];
                            $scope.ListadoCuentaBancariaOficinas.forEach(function (item) {
                                if (item.Oficina.Codigo == $scope.Modelo.OficinaDestino.Codigo) {
                                    $scope.ListadoCuentaBancarias.push(item);
                                }
                            });
                            if ($scope.ListadoCuentaBancarias.length > 0) {
                                $scope.Modelo.CuentaBancariaConsignacion = $scope.ListadoCuentaBancarias[0];
                                $scope.Modelo.CuentaBancariaCheque = $scope.ListadoCuentaBancarias[0];
                            }
                            else {
                                $scope.MensajesError.push('La oficina seleccionada no tiene ninguna cuenta bancaria asociada');
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        function CargarCajas() {
            $scope.ListadoCajaOficinas = [];
            $scope.MensajesError = [];
            CajasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTodaCajaOficinas = [];
                        $scope.ListadoTodaCajaOficinas = response.data.Datos;

                        if ($scope.CodigoCaja !== undefined && $scope.CodigoCaja !== null && $scope.CodigoCaja !== '' && $scope.CodigoCaja !== 0) {
                            $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                                if (itmOfic.Codigo > 0) {
                                    if (itmOfic.Oficina.Codigo == $scope.CodigoOficinaDestino) {
                                        $scope.ListadoCajaOficinas.push(itmOfic);
                                    }
                                }
                            });

                            $scope.Modelo.Caja = $linq.Enumerable().From($scope.ListadoCajaOficinas).First('$.Codigo == ' + $scope.CodigoCaja);

                        }
                        else {
                            $scope.ListadoCajaOficinas = [];
                            $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                                if (itmOfic.Codigo > 0) {
                                    if (itmOfic.Oficina.Codigo == $scope.Modelo.OficinaDestino.Codigo) {
                                        $scope.ListadoCajaOficinas.push(itmOfic);
                                    }
                                }
                            });
                            if ($scope.ListadoCajaOficinas.length > 0) {
                                $scope.Modelo.Caja = $scope.ListadoCajaOficinas[0];
                            }
                            else {
                                $scope.MensajesError.push('La oficina seleccionada no tiene ninguna caja asociada');
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        /*Cargar Combo Conceptos Contables*/
        ConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoConceptoContable = response.data.Datos;

                        if ($scope.CodigoConceptoContable !== undefined && $scope.CodigoConceptoContable !== '' && $scope.CodigoConceptoContable !== null) {
                            if ($scope.CodigoConceptoContable > 0) {
                                $scope.Modelo.ConceptoContable = $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo ==' + $scope.CodigoConceptoContable);
                            }
                        }
                    }
                    else {
                        $scope.ListadoConceptoContable = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.AsignarPlaca = function (Placa) {
            if (Placa != undefined || Placa != null) {
                if (angular.isObject(Placa)) {
                    $scope.LongitudPlaca = Placa.length;
                    $scope.PlacaValida = true;
                    $scope.ValidarTercero(Placa.Tenedor.Identificacion)
                    $scope.ValidarBeneficiario(Placa.Tenedor.Identificacion)
                }
                else if (angular.isString(Placa)) {
                    $scope.LongitudPlaca = Placa.length;
                    $scope.PlacaValida = false;
                }
            }
        };

        $scope.AsignarCuentaPUCNumero = function (Cuenta) {
            if (Cuenta !== '' && Cuenta !== undefined && Cuenta !== null) {
                PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoCuenta: Cuenta, Estado: 1 }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoCuentasPUC = response.data.Datos;
                                $scope.Modal.CuentaPUC = response.data.Datos[0];
                            }
                            else {
                                $scope.ListadoCuentasPUC = [];
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.ListadoCuentasPUC = [];
            }
        }

        //Autocomplete Terceros:
        $scope.AutocompleteTerceros = function (value) {
            return TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true }).Datos
        }
        //Fin Autocomplete Terceros

        /*cargar AutoComplete de la placa */
        VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoVehiculos = response.data.Datos;

                        if ($scope.CodigoPlaca !== undefined && $scope.CodigoPlaca !== '' && $scope.CodigoPlaca !== null) {
                            if ($scope.CodigoPlaca > 0) {
                                $scope.Modelo.Placa = $linq.Enumerable().From($scope.ListadoVehiculos).First('$.Codigo == ' + $scope.CodigoPlaca);
                            }
                        }

                    }
                    else {
                        $scope.ListadoVehiculos = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.ValidarTercero = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Tercero = response.data.Datos
                            }
                            else {
                                $scope.Modelo.Tercero = undefined
                                ShowError('La identificación Ingresada no es válida o el tercero no se encuentra registrado')
                            }
                        }
                    });
            }
        };
        $scope.ValidarTerceroModal = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modal.TerceroModal = response.data.Datos
                            }
                            else {
                                $scope.Modal.TerceroModal = undefined
                                ShowError('La identificación Ingresada no es válida o el tercero no se encuentra registrado')
                            }
                        }
                    });
            }
        };
        $scope.ValidarBeneficiario = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Beneficiario = response.data.Datos
                            }
                            else {
                                $scope.Modelo.Beneficiario = undefined
                                ShowError('La identificación Ingresada no es válida o el beneficiario no se encuentra registrado')
                            }
                        }
                    });
            }
        };

        $scope.validarFormaPago = function () {
            if ($scope.Modelo.FormaPagoRecaudo != null) {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                    $scope.ActivarEfectivo = false
                    $scope.ActivarCheque = false
                    $scope.ActivarConsignacion = true

                    $scope.Modelo.CuentaBancariaCheque = ''
                    $scope.Modelo.FechaCheque = null
                    $scope.Modelo.ValorPagoCheque = ''


                    $scope.Modelo.Caja = ''
                    $scope.Modelo.ValorPagoEfectivo = ''


                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                    $scope.ActivarEfectivo = false
                    $scope.ActivarCheque = true
                    $scope.ActivarConsignacion = false

                    $scope.Modelo.Caja = ''
                    $scope.Modelo.ValorPagoEfectivo = ''

                    $scope.Modelo.CuentaBancariaConsignacion = ''
                    $scope.Modelo.FechaConsignacion = null
                    $scope.Modelo.ValorPagoTransferencia = ''
                    $scope.Modelo.NumeroConsignacion = ''

                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                    $scope.ActivarEfectivo = true
                    $scope.ActivarCheque = false
                    $scope.ActivarConsignacion = false

                    $scope.Modelo.CuentaBancariaConsignacion = ''
                    $scope.Modelo.FechaConsignacion = null
                    $scope.Modelo.ValorPagoTransferencia = ''
                    $scope.Modelo.NumeroConsignacion = ''


                    $scope.Modelo.CuentaBancariaCheque = ''
                    $scope.Modelo.FechaCheque = null
                    $scope.Modelo.ValorPagoCheque = ''
                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_NO_APLICA) {
                    $scope.ActivarEfectivo = false
                    $scope.ActivarCheque = false
                    $scope.ActivarConsignacion = false

                    $scope.Modelo.CuentaBancariaConsignacion = ''
                    $scope.Modelo.FechaConsignacion = null
                    $scope.Modelo.ValorPagoTransferencia = ''
                    $scope.Modelo.NumeroConsignacion = ''


                    $scope.Modelo.CuentaBancariaCheque = ''
                    $scope.Modelo.FechaCheque = null
                    $scope.Modelo.ValorPagoCheque = ''


                    $scope.Modelo.Caja = ''
                    $scope.Modelo.ValorPagoEfectivo = ''
                }
            }
        }

        $scope.AsignarOficinaDestino = function (Oficina) {
            $scope.MensajesError = [];
            if (Oficina.Codigo !== 0 && Oficina.Codigo !== undefined && Oficina.Codigo !== null) {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                    CargarCajas()
                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE || $scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                    CargarCuentasBancarias()
                }
            }
        }
        $scope.ValidarFormaPago = function (Oficina) {
            if ($scope.Modelo.FormaPagoRecaudo.Codigo !== CODIGO_FORMA_PAGO_NO_APLICA) {
                $scope.AsignarOficinaDestino(Oficina)
            }
        }
        /*Funcion Limpiar Modal Movimiento*/
        function limpiarModalMovimiento() {
            $scope.Modal.TerceroModal = {};
            $scope.ListadoCuentasPUC = [];
            $scope.Modal.ValorBase = '';
            $scope.Modal.ValorDebito = '';
            $scope.Modal.ValorCredito = '';
            $scope.Modal.CuentaPUC = '';
            $scope.Modal.CodigoCuentaPUC = '';
        }

        $scope.ModalMovimiento = function () {
            if ($scope.Modelo.FormaPagoRecaudo.Codigo !== CODIGO_FORMA_PAGO_NO_APLICA) {
                $scope.MensajesError = [];
                $scope.MensajesErrorMovimiento = [];
                if (($scope.Modelo.ValorPagoCheque !== undefined && $scope.Modelo.ValorPagoCheque !== '' && $scope.Modelo.ValorPagoCheque !== null) || ($scope.Modelo.ValorPagoEfectivo !== undefined && $scope.Modelo.ValorPagoEfectivo !== '' && $scope.Modelo.ValorPagoEfectivo !== null) || ($scope.Modelo.ValorPagoTransferencia !== undefined && $scope.Modelo.ValorPagoTransferencia !== '' && $scope.Modelo.ValorPagoTransferencia !== null)) {
                    $scope.MensajesError = [];
                    limpiarModalMovimiento();
                    showModal('modalMovimiento');
                    if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                        $scope.Modal.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                    }
                    if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                        $scope.Modal.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                    }
                    if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                        $scope.Modal.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                    }
                }
                else {
                    $scope.MensajesError.push('Debe Ingresar el valor de la forma de pago seleccionada ');
                    Ventana.scrollTop = 0
                }
            }
            else {
                $scope.MensajesError.push('Debe seleccionar una forma de pago');
                Ventana.scrollTop = 0
            }
        };
        $scope.GuardarMovimiento = function () {
            $scope.MensajesErrorMovimiento = [];
            if (DatosRequeridosMovimiento()) {
                CargarlistadoMovimiento()
            }
        }
        function CargarlistadoMovimiento() {
            closeModal('modalMovimiento');
            $scope.MaskNumero();
            $scope.ListadoMovimiento.push({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CuentaPUC: $scope.Modal.CuentaPUC,
                //CentroCosto: $scope.Modelo.CentroCosto,
                ValorBase: MascaraNumero($scope.Modal.ValorBase),
                ValorDebito: MascaraNumero($scope.Modal.ValorDebito),
                ValorCredito: MascaraNumero($scope.Modal.ValorCredito),
                Tercero: $scope.Modal.TerceroModal,
                GeneraCuenta: 0,
                Observaciones: '',
                Prefijo: '',
                CodigoAnexo: '',
                SufijoCodigoAnexo: '',
                CampoAuxiliar: '',
                TerceroParametrizableContable: { Codigo: 3100 },
                DocumentoCruce: { Codigo: 2800 },
                CentroCostoParametrizableContable: { Codigo: 3200 },
            });
            CalcularMovimiento()
            $scope.MaskValores();
        }
        /*Funcion Calcular Totales*/
        function CalcularMovimiento() {
            $scope.Modelo.Debito = 0
            $scope.Modelo.Credito = 0
            $scope.Modelo.Diferencia = 0
            /*realiza el recorrido para  obtener  los totales */
            if ($scope.ListadoMovimiento.length > 0) {
                $scope.ListadoMovimiento.forEach(function (item) {
                    $scope.Modelo.Debito = Math.ceil($scope.Modelo.Debito) + Math.ceil(item.ValorDebito)
                    $scope.Modelo.Credito = Math.ceil($scope.Modelo.Credito) + Math.ceil(item.ValorCredito)
                })
            }
            $scope.Modelo.Diferencia = Math.ceil(($scope.Modelo.Debito) - parseInt($scope.Modelo.Credito))
        }

        function DatosRequeridosMovimiento() {
            $scope.MensajesErrorMovimiento = [];
            var continuar = true;
            if ($scope.Modal.CuentaPUC == undefined || $scope.Modal.CuentaPUC == null || $scope.Modal.CuentaPUC == "") {
                $scope.MensajesErrorMovimiento.push('Debe Seleccionar una Cuenta Puc');
                continuar = false;
            }
            if ($scope.Modal.CuentaPUC.ExigeValorBase == 1) {
                if ($scope.Modal.ValorBase == 0 || $scope.Modal.ValorBase == "") {
                    $scope.MensajesErrorMovimiento.push('Debe ingresar el  valor base ');
                    continuar = false;
                }
            }
            if ($scope.Modal.CuentaPUC.ExigeTercero == 1) {
                if ($scope.Modal.TerceroModal == undefined || $scope.Modal.TerceroModal == "") {
                    $scope.MensajesErrorMovimiento.push('Debe ingresar el tercero');
                    continuar = false;
                }
            }
            else {
                $scope.Modal.TerceroModal = { Codigo: 0 };
            }
            if ($scope.Modal.ValorDebito > 0 && $scope.Modal.ValorCredito > 0) {
                $scope.MensajesErrorMovimiento.push('Debe ingresar solo un valor ya sea Crédito o Débito');
                continuar = false;
            }
            if ($scope.Modal.ValorDebito == undefined || $scope.Modal.ValorDebito == null || $scope.Modal.ValorDebito == "") {
                $scope.Modal.ValorDebito = 0;
            }
            if ($scope.Modal.ValorCredito == undefined || $scope.Modal.ValorCredito == null || $scope.Modal.ValorCredito == "") {
                $scope.Modal.ValorCredito = 0;
            }
            if ($scope.Modal.ValorBase == undefined || $scope.Modal.ValorBase == null || $scope.Modal.ValorBase == "") {
                $scope.Modal.ValorBase = 0
            }

            if (continuar == false) {
                Ventana.scrollTop = 0
            }

            return continuar;
        }
        $scope.LimpiarMovimiento = function () {
            $scope.ListadoMovimiento = [];
            $scope.Modelo.Debito = 0;
            $scope.Modelo.Credito = 0;
            $scope.Modelo.Diferencia = 0;
        };

        /*Eliminar Movimiento*/
        $scope.ConfirmacionEliminarMovimiento = function (indice) {
            $scope.MensajeEliminar = { indice };
            $scope.VarAuxi = indice
            showModal('modalEliminarMovimiento');
        };

        $scope.EliminarMovimiento = function (indice) {
            $scope.ListadoMovimiento.splice($scope.VarAuxi, 1);
            closeModal('modalEliminarMovimiento');
            CalcularMovimiento()
        };

        $scope.ConsultarMovimiento = function () {
            if (DatosRequeridosConsutarMovimiento()) {
                if ($scope.Modelo.ConceptoContable !== undefined && $scope.Modelo.ConceptoContable !== null && $scope.Modelo.ConceptoContable !== '') {
                    if ($scope.Modelo.ConceptoContable.Codigo == 0) {
                        //Crédito
                        var item = {};
                        if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                            item.CuentaPUC = $scope.Caja.PlanUnicoCuentas;

                        } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                            item.CuentaPUC = $scope.CuentaBancariaCheque.PlanUnicoCuentas;

                        } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                            item.CuentaPUC = $scope.CuentaBancariaConsignacion.PlanUnicoCuentas;
                        }
                        item.ValorDebito = 0;
                        item.GeneraCuenta = 0;
                        item.Observaciones = '';
                        item.Prefijo = 0;
                        item.CodigoAnexo = 0;
                        item.CampoAuxiliar = '';

                        item.Tercero = $scope.Modelo.Beneficiario;
                        item.TerceroParametrizableContable = { Codigo: 3100 };
                        item.DocumentoCruce = { Codigo: 2800 };
                        item.CentroCostoParametrizableContable = { Codigo: 3200 };
                        $scope.ListadoMovimiento.push(item);

                        CalcularMovimiento();
                    } else {
                        DetalleConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.ConceptoContable.Codigo }).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) {
                                        response.data.Datos.forEach(function (item) {
                                            var Concepto = {};
                                            if (item.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_DEBITO) {
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                                                    item.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                    item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                                                    item.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                    item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                                                    item.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                    item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia)
                                                }
                                                item.ValorCredito = 0;
                                                if (item.CuentaPUC.AplicarMedioPago > 0) {
                                                    if ($scope.ActivarCambista) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarCheque) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaCheque.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarEfectivo) {
                                                        Concepto.CuentaPUC = $scope.Modelo.Caja.CuentaPUC
                                                    }
                                                    if ($scope.ActivarConsignacion) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }

                                                } else {
                                                    Concepto.CuentaPUC = item.CuentaPUC;
                                                }
                                            }
                                            if (item.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_CREDITO) {
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                                                    item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                    item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                                                    item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                    item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                                                    item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                    item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                }
                                                item.ValorDebito = 0;
                                                if (item.CuentaPUC.AplicarMedioPago > 0) {
                                                    if ($scope.ActivarCambista) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarCheque) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaCheque.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarEfectivo) {
                                                        Concepto.CuentaPUC = $scope.Modelo.Caja.CuentaPUC
                                                    }
                                                    if ($scope.ActivarConsignacion) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }

                                                } else {
                                                    Concepto.CuentaPUC = item.CuentaPUC;
                                                }
                                            }

                                            //item.CuentaPUC = item.CuentaPUC;
                                            item.Tercero = $scope.Modelo.Beneficiario;
                                            item.TerceroParametrizableContable = { Codigo: 3100 }
                                            item.DocumentoCruce = { Codigo: 2800 }
                                            item.CentroCostoParametrizableContable = { Codigo: 3200 }
                                            $scope.ListadoMovimiento.push(item)
                                            CalcularMovimiento()
                                        });
                                    } else {
                                        ShowError('El Concepto contable no se encuentra parametrizado');
                                    }
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }

            }
        };

        function DatosRequeridosConsutarMovimiento() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.ConceptoContable == undefined || $scope.Modelo.ConceptoContable == null || $scope.Modelo.ConceptoContable == "" || $scope.Modelo.ConceptoContable.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar un concepto contable');
                continuar = false;
            }

            if ($scope.Modelo.Beneficiario == undefined || $scope.Modelo.Beneficiario == null || $scope.Modelo.Beneficiario == "") {
                $scope.MensajesError.push('Debe ingresar un beneficiario');
                continuar = false;
            }
            if ($scope.Modelo.FormaPagoRecaudo.Codigo == 4700) {
                $scope.MensajesError.push('Debe seleccionar una forma de pago');
                continuar = false;
            }
            else {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                    if ($scope.Modelo.ValorPagoEfectivo == 0) {
                        $scope.MensajesError.push('El valor ingresado debe ser mayor a 0');
                        continuar = false;
                    }
                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {

                    if ($scope.Modelo.ValorPagoCheque == 0) {
                        $scope.MensajesError.push('El valor ingresado debe ser mayor a 0');
                        continuar = false;
                    }
                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                    if ($scope.Modelo.ValorPagoTransferencia == 0) {
                        $scope.MensajesError.push('El valor ingresado debe ser mayor a 0');
                        continuar = false;
                    }
                }
                else if (($scope.Modelo.ValorPagoCheque !== undefined && $scope.Modelo.ValorPagoCheque !== '' && $scope.Modelo.ValorPagoCheque !== null && $scope.Modelo.ValorPagoEfectivo !== 0) ||
                    ($scope.Modelo.ValorPagoEfectivo !== undefined && $scope.Modelo.ValorPagoEfectivo !== '' && $scope.Modelo.ValorPagoEfectivo !== null && $scope.Modelo.ValorPagoCheque !== 0) ||
                    ($scope.Modelo.ValorPagoTransferencia !== undefined && $scope.Modelo.ValorPagoTransferencia !== '' && $scope.Modelo.ValorPagoTransferencia !== null && $scope.Modelo.ValorPagoTransferencia !== 0)) {
                }
                else {
                    $scope.MensajesError.push('Debe Ingresar el valor de la forma de pago seleccionada');
                    continuar = false;
                }
            }

            if (continuar == false) {
                Ventana.scrollTop = 0
            }


            return continuar;
        }
        /*----------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                $scope.PeriodoValido = true
                FindCierre()
                if ($scope.PeriodoValido) {
                    showModal('modalConfirmacionGuardar');
                }
            }
            else {
                Ventana.scrollTop = 0
            }
        };


        /*Guardar/Modificar Comprobante De ingreso*/
        $scope.Guardar = function () {
            if (DatosRequeridos()) {
                $scope.MaskNumero();
                $scope.Modelo.CodigoAlterno = 0;
                $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_COMPROBANTE_INGRESO;
                $scope.Modelo.Vehiculo = $scope.Modelo.Placa;
                $scope.Modelo.FormaPagoDocumento = $scope.Modelo.FormaPagoRecaudo;
                $scope.Modelo.Estado = $scope.Estado.Codigo;
                $scope.Modelo.ValorAlterno = 0;
                $scope.Modelo.ConceptoContable = $scope.Modelo.ConceptoContable.Codigo

                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                    $scope.Modelo.Caja = $scope.Modelo.Caja;
                    $scope.Modelo.ValorPagoEfectivo = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                    $scope.Modelo.CuentaBancaria = { Codigo: 0 };
                    $scope.Modelo.FechaPagoRecaudo = new Date();
                    $scope.Modelo.DestinoIngreso = { Codigo: 4801 }; //Codigo Destino ingreso Caja
                    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                    $scope.Modelo.CuentaBancaria = { Codigo: $scope.Modelo.CuentaBancariaCheque.CodigoCuenta };
                    $scope.Modelo.FechaPagoRecaudo = $scope.Modelo.FechaCheque;
                    $scope.Modelo.ValorPagoCheque = MascaraNumero($scope.Modelo.ValorPagoCheque);
                    $scope.Modelo.Numeracion = $scope.Modelo.NumeroCheque
                    $scope.Modelo.Caja = { Codigo: 0 };
                    $scope.Modelo.DestinoIngreso = { Codigo: 4802 };//Codigo Destino ingreso Cuenta Bancaria
                    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoCheque);
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                    $scope.Modelo.CuentaBancaria = { Codigo: $scope.Modelo.CuentaBancariaConsignacion.CodigoCuenta };
                    $scope.Modelo.FechaPagoRecaudo = $scope.Modelo.FechaConsignacion;
                    $scope.Modelo.NumeroPagoRecaudo = $scope.Modelo.NumeroConsignacion;
                    $scope.Modelo.ValorPagoTransferencia = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                    $scope.Modelo.Caja = { Codigo: 0 };
                    $scope.Modelo.DestinoIngreso = { Codigo: 4802 };//Codigo Destino ingreso Cuenta Bancaria
                    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                }

                $scope.Modelo.GeneraConsignacion = "";
                $scope.Modelo.Detalle = $scope.ListadoMovimiento;

                //$scope.Modelo.Tercero = {Codigo: $scope.Modelo.Tercero.Codigo}
                //$scope.Modelo.Beneficiario = { Codigo: $scope.Modelo.Beneficiario.Codigo }

                var DetalleEntidad = [];
                $scope.Modelo.Detalle.forEach(item => {
                    DetalleEntidad.push({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CuentaPUC: { Codigo: item.CuentaPUC.Codigo },
                        Tercero: { Codigo: item.Tercero.Codigo },
                        ValorBase: item.ValorBase,
                        ValorDebito: item.ValorDebito,
                        ValorCredito: item.ValorCredito,
                        GeneraCuenta: item.GeneraCuenta,
                        Observaciones: item.Observaciones,
                        TerceroParametrizacion: { Codigo: item.TerceroParametrizacion == undefined ? 0 : item.TerceroParametrizacion.Codigo },
                        CentroCostoParametrizacion: { Codigo: item.CentroCostoParametrizacion == undefined ? 0 : item.CentroCostoParametrizacion.Codigo },
                        DocumentoCruce: { Codigo: item.DocumentoCruce == undefined ? 0 : item.DocumentoCruce.Codigo }

                    });

                });

                var CuentasEntidad = [];

                if ($scope.Modelo.Cuentas != undefined) {
                    $scope.Modelo.Cuentas.forEach(itemCuenta => {
                        CuentasEntidad.push({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: itemCuenta.Codigo,
                            ValorPagar: itemCuenta.ValorPagar
                        });


                    });
                }
                var DetalleCuentasEntidad = [];


                if ($scope.Modelo.DetalleCuentas != undefined) {
                    $scope.Modelo.DetalleCuentas.forEach(itemDetalleCuenta => {
                        DetalleCuentasEntidad.push({
                            CodigoDocumentoCuenta: itemDetalleCuenta.CodigoDocumentoCuenta,
                            ValorPago: itemDetalleCuenta.ValorPago
                        });
                    });
                }

                var Entidad = {
                    CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                    Numero: $scope.Modelo.Numero,
                    Codigo: $scope.Modelo.Codigo,
                    CodigoAlterno: $scope.Modelo.CodigoAlterno,
                    TipoDocumento: $scope.Modelo.TipoDocumento,
                    Fecha: $scope.Modelo.Fecha,
                    Tercero: { Codigo: $scope.Modelo.Tercero.Codigo },
                    Beneficiario: { Codigo: $scope.Modelo.Beneficiario.Codigo },
                    Observaciones: $scope.Modelo.Observaciones,
                    DocumentoOrigen: { Codigo: $scope.Modelo.DocumentoOrigen.Codigo },
                    FormaPagoDocumento: { Codigo: $scope.Modelo.FormaPagoDocumento.Codigo },
                    DestinoIngreso: { Codigo: $scope.Modelo.DestinoIngreso.Codigo },
                    NumeroDocumentoOrigen: $scope.Modelo.NumeroDocumentoOrigen,
                    CuentaBancaria: { Codigo: $scope.Modelo.CuentaBancaria.Codigo },
                    Caja: { Codigo: $scope.Modelo.Caja.Codigo },
                    NumeroPagoRecaudo: $scope.Modelo.NumeroPagoRecaudo,
                    FechaPagoRecaudo: $scope.Modelo.FechaPagoRecaudo,
                    ValorPagoTransferencia: $scope.Modelo.ValorPagoTransferencia,
                    ValorPagoEfectivo: $scope.Modelo.ValorPagoEfectivo,
                    ValorPagoCheque: $scope.Modelo.ValorPagocheque,
                    ValorPagoTotal: $scope.Modelo.ValorPagoTotal,
                    Numeracion: $scope.Modelo.Numeracion,
                    OficinaDestino: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                    GeneraConsignacion: $scope.Modelo.GeneraConsignacion,
                    ValorAlterno: $scope.Modelo.ValorAlterno,
                    ConceptoContable: $scope.Modelo.ConceptoContable,
                    CentroCostos: { Codigo: $scope.Modelo.CentroCostos == undefined ? 0 : $scope.Modelo.CentroCostos.Codigo },
                    Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo },
                    Estado: $scope.Modelo.Estado,
                    UsuarioCrea: { Codigo: $scope.Modelo.UsuarioCrea.Codigo },
                    UsuarioModifica: { Codigo: $scope.Modelo.UsuarioCrea.Codigo },
                    Autorizacion: $scope.Modelo.Autorizacion,
                    Detalle: DetalleEntidad,
                    Cuentas: CuentasEntidad,
                    DetalleCuentas: DetalleCuentasEntidad

                }

                DocumentoComprobantesFactory.Guardar(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if ($scope.Codigo == 0) {
                                ShowSuccess('Se guardó el comprobante de ingreso N.' + response.data.Datos);
                            }
                            else {
                                ShowSuccess('Se modificó el comprobante de ingreso N.' + response.data.Datos);
                            }
                            closeModal('modalConfirmacionGuardar');
                            document.location.href = '#!ConsultarComprobanteIngresos/' + response.data.Datos;
                            $scope.MaskValores();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                Ventana.scrollTop = 0
                closeModal('modalConfirmacionGuardarComprobanteIngresos');
                $scope.MaskValores();
            }
        }
        /*Datos Requeridos Guardar o Modificar Comprobante*/
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == null || $scope.Modelo.Fecha == "") {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }

            if ($scope.Modelo.Tercero == undefined || $scope.Modelo.Tercero == null || $scope.Modelo.Tercero == "") {
                $scope.MensajesError.push('Debe ingresar un tercero ');
                continuar = false;
            }
            if ($scope.Modelo.Placa == undefined || $scope.Modelo.Placa == null || $scope.Modelo.Placa == "") {
                $scope.Modelo.Placa = { Codigo: 0, Placa: '' };
            }
            else if ($scope.PlacaValida == false) {
                $scope.MensajesError.push('La placa ingresada no es valida');
                continuar = false;
            }
            if ($scope.Modelo.Beneficiario == undefined || $scope.Modelo.Beneficiario == null || $scope.Modelo.Beneficiario == "") {
                $scope.MensajesError.push('Debe ingresar un beneficiario');
                continuar = false;
            }
            if ($scope.Modelo.DocumentoOrigen !== undefined && $scope.Modelo.DocumentoOrigen !== null && $scope.Modelo.DocumentoOrigen.Codigo !== 2600) {
                if ($scope.Modelo.NumeroDocumentoOrigen == "" || $scope.Modelo.NumeroDocumentoOrigen == null || $scope.Modelo.NumeroDocumentoOrigen == undefined) {
                    $scope.MensajesError.push('Debe ingresar el número origen');
                    continuar = false;
                }
            }
            if ($scope.Modelo.FormaPagoRecaudo.Codigo == 4700) {
                $scope.MensajesError.push('Debe seleccionar una forma de pago');
                continuar = false;
            }
            if ($scope.Modelo.OficinaDestino.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar una oficina');
                continuar = false;
            }
            else {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                    if ($scope.Modelo.FechaConsignacion == undefined || $scope.Modelo.FechaConsignacion == null || $scope.Modelo.FechaConsignacion == "") {
                        $scope.MensajesError.push('Debe seleccionar la fecha de la Transferencia ');
                        continuar = false;
                    }
                    if ($scope.Modelo.CuentaBancariaConsignacion == undefined || $scope.Modelo.CuentaBancariaConsignacion == null || $scope.Modelo.CuentaBancariaConsignacion == "") {
                        $scope.MensajesError.push('Debe seleccionar la cuenta bancaria de la Transferencia ');
                        continuar = false;
                    }
                    if ($scope.Modelo.ValorPagoTransferencia == undefined || $scope.Modelo.ValorPagoTransferencia == null || $scope.Modelo.ValorPagoTransferencia == "") {
                        $scope.MensajesError.push('Debe ingresar el valor de la Transferencia ');
                        continuar = false;
                    }
                    if ($scope.Modelo.NumeroConsignacion == undefined || $scope.Modelo.NumeroConsignacion == null || $scope.Modelo.NumeroConsignacion == "") {
                        $scope.Modelo.NumeroConsignacion = 0
                    }
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                    if ($scope.Modelo.FechaCheque == undefined || $scope.Modelo.FechaCheque == null || $scope.Modelo.FechaCheque == "") {
                        $scope.MensajesError.push('Debe seleccionar la fecha del cheque ');
                        continuar = false;
                    }
                    if ($scope.Modelo.NumeroCheque == undefined || $scope.Modelo.NumeroCheque == null || $scope.Modelo.NumeroCheque == "") {
                        $scope.MensajesError.push('Debe ingresar el cheque ');
                        continuar = false;
                    }
                    if ($scope.Modelo.CuentaBancariaCheque == undefined || $scope.Modelo.CuentaBancariaCheque == null || $scope.Modelo.CuentaBancariaCheque == "") {
                        $scope.MensajesError.push('Debe sngresar  la cuenta bancaria del cheque ');
                        continuar = false;
                    }
                    if ($scope.Modelo.ValorPagoCheque == undefined || $scope.Modelo.ValorPagoCheque == null || $scope.Modelo.ValorPagoCheque == "") {
                        $scope.MensajesError.push('Debe ingresar el valor del Cheque  ');
                        continuar = false;
                    }
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                    if ($scope.Modelo.Caja == undefined || $scope.Modelo.Caja == null || $scope.Modelo.Caja == "") {
                        $scope.MensajesError.push('Debe ingresar la caja del efectivo');
                        continuar = false;
                    }
                    if ($scope.Modelo.ValorPagoEfectivo == undefined || $scope.Modelo.ValorPagoEfectivo == null || $scope.Modelo.ValorPagoEfectivo == "") {
                        $scope.MensajesError.push('Debe ingresar el valor del efectivo');
                        continuar = false;
                    }

                }
            }

            if ($scope.Modelo.Debito == 0 && $scope.Modelo.Credito == 0 && $scope.Modelo.Diferencia == 0) {
                $scope.MensajesError.push('Debe ingresar minimo un movimiento en el comprobante de ingreso');
                continuar = false;
            }
            if ($scope.Modelo.Diferencia !== 0) {
                $scope.MensajesError.push('El comprobante de ingreso se encuentra desbalancedado');
                continuar = false;
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar;
        }

        // Metodo Obtener Comprobantes de ingreso  

        if ($scope.Modelo.Numero > 0) {
            $scope.Titulo = 'EDITAR COMPROBANTE INGRESO';
            $scope.Deshabilitar = true;
            $scope.Bloquear = true;
            Obtener();
        }

        function Obtener() {
            blockUI.start('Cargando Comprobante de ingreso Número ' + $scope.Modelo.Numero);

            $timeout(function () {
                blockUI.message('Cargando Comprobante de ingreso  Número ' + $scope.Modelo.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Numero,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_INGRESO,
            };

            blockUI.delay = 1000;
            DocumentoComprobantesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = response.data.Datos.Codigo
                        $scope.Modelo.Numero = response.data.Datos.Numero;
                        $scope.ListadoMovimiento = response.data.Datos.Detalle;
                        var fecha = new Date(response.data.Datos.Fecha);
                        fecha.setHours(0);
                        fecha.setMinutes(0);
                        fecha.setMilliseconds(0);
                        $scope.Modelo.Fecha = fecha;
                        $scope.Modelo.Tercero = response.data.Datos.Tercero;
                        $scope.Modelo.Observaciones = response.data.Datos.Observaciones;
                        $scope.Modelo.Beneficiario = response.data.Datos.Beneficiario;
                        $scope.CodigoPlaca = response.data.Datos.Vehiculo.Codigo
                        if ($scope.ListadoVehiculos.length > 0 && $scope.CodigoPlaca > 0) {
                            $scope.Modelo.Placa = $linq.Enumerable().From($scope.ListadoVehiculos).First('$.Codigo == ' + response.data.Datos.Vehiculo.Codigo);
                        }
                        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);

                        if ($scope.Estado.Codigo == 0) {
                            $scope.Deshabilitar = false;
                        }
                        else if ($scope.Estado.Codigo == 1) {
                            $scope.Deshabilitar = true;
                        }
                        else if (response.data.Datos.Anulado == 1) {
                            $scope.Deshabilitar = true;
                            $scope.ListadoEstados.push({ Nombre: "ANULADO", Codigo: -1 })
                            $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
                        }

                        $scope.CodigoDocumentoOrigen = response.data.Datos.DocumentoOrigen.Codigo
                        if ($scope.ListadoDocumentosOrigen.length > 0) {
                            $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + response.data.Datos.DocumentoOrigen.Codigo);
                        }
                        $scope.Modelo.NumeroDocumentoOrigen = response.data.Datos.NumeroDocumentoOrigen;
                        $scope.CodigoOficinaDestino = response.data.Datos.OficinaDestino.Codigo;
                        if ($scope.ListadoOficinas.length > 0) {
                            $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + response.data.Datos.OficinaDestino.Codigo);
                        }

                        $scope.valorAlterno = response.data.Datos.ValorAlterno;
                        $scope.Observaciones = response.data.Datos.Observaciones;
                        $scope.CodigoFormaPago = response.data.Datos.FormaPagoDocumento.Codigo;
                        $scope.ListaFormaPago = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_DOCUMENTO_COMPROBANTES }, Sync: true }).Datos;
                        if ($scope.ListaFormaPago.length > 0) {
                            $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo == ' + response.data.Datos.FormaPagoDocumento.Codigo);
                        }
                        $scope.CodigoConceptoContable = response.data.Datos.ConceptoContable
                        if ($scope.ListadoConceptoContable.length > 0) {
                            if ($scope.CodigoConceptoContable > 0) {
                                $scope.Modelo.ConceptoContable = $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo == ' + response.data.Datos.ConceptoContable);
                            }
                        }
                        CargarCajas()
                        //Forma Pago Efectivo
                        if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                            $scope.ActivarEfectivo = true
                            $scope.ActivarCheque = false
                            $scope.ActivarConsignacion = false
                            $scope.Modelo.ValorPagoEfectivo = response.data.Datos.ValorPagoEfectivo
                            $scope.CodigoCaja = response.data.Datos.Caja.Codigo;
                            if ($scope.ListadoTodaCajaOficinas.length > 0) {
                                $scope.ListadoCajaOficinas = [];
                                $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                                    if (itmOfic.CodigoOficina == $scope.CodigoOficinaDestino) {
                                        $scope.ListadoCajaOficinas.push(itmOfic);
                                    }
                                });
                                $scope.Caja = $linq.Enumerable().From($scope.ListadoCajaOficinas).First('$.Codigo == ' + response.data.Datos.Caja.Codigo);
                            }
                        }

                        //Forma Pago Cheque
                        if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                            CargarCuentasBancarias()
                            $scope.ActivarEfectivo = false
                            $scope.ActivarCheque = true
                            $scope.ActivarConsignacion = false
                            $scope.Modelo.ValorPagoCheque = response.data.Datos.ValorPagocheque
                            $scope.CodigoCuentaBancaria = response.data.Datos.CuentaBancaria.Codigo
                            if ($scope.ListadoCuentaBancarias.length > 0) {
                                $scope.Modelo.CuentaBancariaCheque = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + response.data.Datos.CuentaBancaria.Codigo);
                            }
                            var fechaCheque = new Date(response.data.Datos.FechaPagoRecaudo);
                            fechaCheque.setHours(0);
                            fechaCheque.setMinutes(0);
                            fechaCheque.setMilliseconds(0);

                            $scope.Modelo.FechaCheque = fechaCheque;
                            $scope.Modelo.NumeroCheque = response.data.Datos.Numeracion
                        }
                        //Forma Pago Cosnignacion
                        if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                            CargarCuentasBancarias()
                            $scope.ActivarEfectivo = false
                            $scope.ActivarCheque = false
                            $scope.ActivarConsignacion = true
                            $scope.CodigoCuentaBancaria = response.data.Datos.CuentaBancaria.Codigo
                            $scope.Modelo.NumeroConsignacion = response.data.Datos.NumeroPagoRecaudo
                            var fechaConsignacion = new Date(response.data.Datos.FechaPagoRecaudo);
                            fechaConsignacion.setHours(0);
                            fechaConsignacion.setMinutes(0);
                            fechaConsignacion.setMilliseconds(0);
                            $scope.Modelo.FechaConsignacion = fechaConsignacion
                            $scope.Modelo.ValorPagoTransferencia = response.data.Datos.ValorPagoTransferencia
                            if ($scope.ListadoCuentaBancarias.length > 0) {
                                $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + response.data.Datos.CuentaBancaria.Codigo);
                            }
                        }
                        $scope.ValidarBeneficiario($scope.Modelo.Beneficiario.NumeroIdentificacion)
                        $scope.ValidarTercero($scope.Modelo.Tercero.NumeroIdentificacion)
                        CalcularMovimiento()
                        $scope.MaskValores()
                    }
                    else {
                        ShowError('No se logro consultar el Comprobante de ingreso No.' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarComprobanteIngresos';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarComprobanteIngresos';
                });

            blockUI.stop();
        };

        $scope.VolverMaster = function () {
            if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarComprobanteIngresos/' + $scope.Modelo.Numero;
            } else {
                document.location.href = '#!ConsultarComprobanteIngresos';
            }
        };

        $scope.MaskMayus = function () {
            try { $scope.Descripcion = $scope.Descripcion.toUpperCase() } catch (e) { };
        };
        $scope.MaskNumero = function () {
            try { $scope.Modelo.Tercero.NumeroIdentificacion = MascaraNumero($scope.Modelo.Tercero.NumeroIdentificacion) } catch (e) { };
            try { $scope.Modelo.Beneficiario.NumeroIdentificacion = MascaraNumero($scope.Modelo.Beneficiario.NumeroIdentificacion) } catch (e) { };
            try { $scope.Modelo.NumeroDocumentoOrigen = MascaraNumero($scope.Modelo.NumeroDocumentoOrigen) } catch (e) { };
            try { $scope.Modelo.NumeroConsignacion = MascaraNumero($scope.Modelo.NumeroConsignacion) } catch (e) { };
            try { $scope.Modelo.NumeroCheque = MascaraNumero($scope.Modelo.NumeroCheque) } catch (e) { };
            try { $scope.Modelo.CodigoCuentaPUC = MascaraNumero($scope.Modelo.CodigoCuentaPUC) } catch (e) { };
            try { $scope.Modelo.TerceroModal.NumeroIdentificacion = MascaraNumero($scope.Modelo.TerceroModal.NumeroIdentificacion) } catch (e) { };
        };
        $scope.MaskPlaca = function () {
            try { $scope.Vehiculo.Placa = MascaraPlaca($scope.Vehiculo.Placa) } catch (e) { };
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
    }]);