﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	print 'gsp_liberar_guias'
DROP PROCEDURE gsp_liberar_guias
GO
CREATE PROCEDURE gsp_liberar_guias (
@par_EMPR_Codigo NUMERIC, 
@par_ENRE_Numero NUMERIC,
@par_ENPE_Numero NUMERIC,
@par_OFIC_Codigo NUMERIC
)
AS BEGIN 
UPDATE Encabezado_Remesas SET ENPE_Numero = 0 
WHERE TIDO_Codigo = 110 and Numero = @par_ENRE_Numero AND ENPE_Numero = @par_ENPE_Numero

 SELECT @@ROWCOUNT As CantidadRegistros
END 
GO