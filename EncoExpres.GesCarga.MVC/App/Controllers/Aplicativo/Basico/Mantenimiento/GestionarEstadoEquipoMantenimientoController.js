﻿EncoExpresApp.controller("GestionarEstadoEquipoMantenimientoCtrl", ['$scope', '$routeParams', 'blockUI', '$timeout', 'ValorCatalogosFactory', '$linq', 'EstadoEquipoMantenimientoFactory',
    function ($scope, $routeParams, blockUI, $timeout, ValorCatalogosFactory, $linq, EstadoEquipoMantenimientoFactory) {
        $scope.MensajesError = [];
        $scope.MensajesErrorDetalle = [];
        $scope.ListadoEstados = [];
        $scope.ModificarDetalle = 0;
        $scope.Operacion = 'Adicionar';


        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ESTADO_EQUIPO_MANTENIMIENTO);
        $scope.ListadoDocumentos = [{
            idRow: 1,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Referencia: '',
            Documento: '',
            Extencion: ''
        }]
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Mantenimiento' }, { Nombre: 'Estado Equipos' }];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Estado: ''
        };
        //$scope.Modelo.ID_Documentos_Eliminar = [];

        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Codigo = 0;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar el combo de estados*/

        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0];

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR EQUIPO';
            Obtener();
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando equipo ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando equipo  ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            EstadoEquipoMantenimientoFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Nombre = response.data.Datos.Nombre
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado.Codigo);
                    }
                    else {
                        ShowError('No se logro consultar el equipo ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarEstadoEquiposMantenimiento';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar el plan de mantenimiento No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarEstadoEquiposMantenimiento';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if (parseInt($scope.Modelo.Codigo) > 0) {
                document.location.href = '#!ConsultarEstadoEquiposMantenimiento/' + $scope.Modelo.Codigo;
            }
            else {
                document.location.href = '#!ConsultarEstadoEquiposMantenimiento';
            }
        };

        // Metodo para guardar /modificar
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.Guardar = function () {
            if (DatosRequeridos()) {
                EstadoEquipoMantenimientoFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    blockUI.stop();
                                    ShowSuccess('Se guardó el estado de equipo ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarEstadoEquiposMantenimiento/' + response.data.Datos;
                                }
                                else {
                                    blockUI.stop();
                                    ShowSuccess('Se modificó el estado de equipo ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarEstadoEquiposMantenimiento/' + $scope.Modelo.Codigo;
                                }
                                closeModal('modalConfirmacionGuardar');
                                //location.href = '#!ConsultarEstadoEquiposMantenimiento';
                            }
                            else {
                                blockUI.stop();
                                ShowError(response.data.MensajeOperacion);
                                location.href = '#!ConsultarEstadoEquiposMantenimiento';
                            }
                        }
                        else {
                            blockUI.stop();
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                blockUI.stop();

            }
            else {
                closeModal('modalConfirmacionGuardar', 1)
            }
        }
        // Valida los datos requridos 
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == '' || $scope.Modelo.Nombre == undefined) {
                $scope.MensajesError.push('Debe ingresar el nombre del estado');
                continuar = false;
            }
            return continuar

        }

        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        };
    }]);