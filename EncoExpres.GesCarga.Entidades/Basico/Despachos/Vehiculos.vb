﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" Vehiculos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Vehiculos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Vehiculos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Vehiculos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param> 
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Placa = Read(lector, "Placa")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            'EstadoVehiculos = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESVE_Codigo"), .Nombre = Read(lector, "EstadoVehiculo")}
            Estado = New Estado With {.Codigo = Read(lector, "Estado"), .Nombre = Read(lector, "EstadoVehiculo")}
            TipoVehiculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIVE_Codigo"), .Nombre = Read(lector, "TipoVehiculo"), .CampoAuxiliar3 = Read(lector, "AplicaSemirremolque")}
            Obtener = Read(lector, "Obtener")
            Conductor = New Tercero With {.Nombre = Read(lector, "NombreConductor"), .Identificacion = Read(lector, "IdentificacionConductor"), .Codigo = Read(lector, "TERC_Codigo_Conductor"), .NombreCompleto = Read(lector, "NombreConductor"), .NumeroIdentificacion = Read(lector, "IdentificacionConductor"), .Celulares = Read(lector, "Celulares")}
            Tenedor = New Tercero With {.Nombre = Read(lector, "NombreTenedor"), .Identificacion = Read(lector, "IdentificacionTenedor"), .Codigo = Read(lector, "TERC_Codigo_Tenedor"), .NombreCompleto = Read(lector, "NombreTenedor")}
            Semirremolque = New Semirremolques With {.Placa = Read(lector, "PlacaSemirremolque"), .Codigo = Read(lector, "SEMI_Codigo")}
            TotalRegistros = Read(lector, "TotalRegistros")
            Afiliador = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Afiliador")}
            TipoDueno = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIDV_Codigo")}
            TarjetaPropiedad = Read(lector, "Tarjeta_Propiedad")
            TipoCarroceria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICA_Codigo")}
            NumeroMotor = Read(lector, "Numero_Motor")
            Modelo = Read(lector, "Modelo")
            ModeloRepotenciado = Read(lector, "Modelo_Repotenciado")
            Chasis = Read(lector, "Numero_Serie")
            Color = New ColorVehiculos With {.Codigo = Read(lector, "COVE_Codigo")}
            Marca = New MarcaVehiculos With {.Codigo = Read(lector, "MAVE_Codigo")}
            Linea = New LineaVehiculos With {.Codigo = Read(lector, "LIVE_Codigo")}
            PesoBruto = Read(lector, "Peso_Bruto")
            Capacidad = Read(lector, "Capacidad")
            CapacidadGalones = Read(lector, "CapacidadGalones")
            PesoTara = Read(lector, "Peso_Tara")
            NumeroEjes = Read(lector, "Numero_Ejes")
            Cilindraje = Read(lector, "Cilindraje")
            Kilometraje = Read(lector, "Kilometraje")
            FechaActualizacionKilometraje = Read(lector, "Fecha_Actuliza_Kilometraje")
            FechaUltimoCargue = Read(lector, "Fecha_Ultimo_Cargue")
            ProveedorGPS = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Proveedor_GPS")}
            UsuarioGPS = Read(lector, "Usuario_GPS")
            ClaveGPS = Read(lector, "Clave_GPS")
            TelefonoGPS = Read(lector, "TelefonoGPS")
            CausaInactividad = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CAIV_Codigo"), .Nombre = Read(lector, "CATA_CAIV_Nombre")}
            JustificacionBloqueo = Read(lector, "Justificacion_Bloqueo")
            EventoAuditoria = Read(lector, "Evento_Auditoria")
            AseguradoraSOAT = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Aseguradora_SOAT")}
            FechaVencimientoSOAT = Read(lector, "Fecha_Vencimiento_SOAT")
            ReferenciaSOAT = Read(lector, "Referencia_SOAT")
            URLGPS = Read(lector, "URL_GPS")
            Puntos = Read(lector, "Puntos")
            IdentificadorGPS = Read(lector, "Identificador_GPS")
            Causa = Read(lector, "Causa")
            OrganismoTransito = Read(lector, "Organismo_Transito")
            EstadoListaNegra = Read(lector, "EstadoListaNegra")
            CodigoListaNegra = Read(lector, "CodigoListaNegra")
            Propietario = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Propietario"), .NombreCompleto = Read(lector, "NombrePropietario"), .Identificacion = Read(lector, "Identificacion_propietario")}
            CapacidadVolumen = Read(lector, "Capacidad_Volumen")
            FechaModifica = Read(lector, "Fecha_Modifica")
            UsuarioModifica = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Modifica"), .Nombre = Read(lector, "UsuarioModifica")}
            JustificacionNovedad = Read(lector, "Justificacion_Novedad")
            Novedad = New ValorCatalogos With {.Nombre = Read(lector, "Novedad"), .Codigo = Read(lector, "CATA_NOVE_Codigo")}
            TipoCombustible = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICO_Codigo")}
            Sucursal = Read(lector, "Sucursal")
        End Sub




        <JsonProperty>
        Public Property TipoCombustible As ValorCatalogos
        <JsonProperty>
        Public Property ListaNovedades As IEnumerable(Of NovedadTercero)
        <JsonProperty>
        Public Property Novedad As ValorCatalogos
        <JsonProperty>
        Public Property JustificacionNovedad As String
        <JsonProperty>
        Public Property Propietarios As IEnumerable(Of PropietarioVehiculo)
        <JsonProperty>
        Public Property Placa As String
        <JsonProperty>
        Public Property Propietario As Tercero
        <JsonProperty>
        Public Property Tenedor As Tercero
        <JsonProperty>
        Public Property Sucursal As String
        <JsonProperty>
        Public Property Conductor As Tercero
        <JsonProperty>
        Public Property Afiliador As Tercero
        <JsonProperty>
        Public Property TipoDueno As ValorCatalogos
        <JsonProperty>
        Public Property TipoVehiculo As ValorCatalogos
        <JsonProperty>
        Public Property TarjetaPropiedad As String
        <JsonProperty>
        Public Property TipoCarroceria As ValorCatalogos
        <JsonProperty>
        Public Property NumeroMotor As String
        <JsonProperty>
        Public Property Modelo As Double
        <JsonProperty>
        Public Property ModeloRepotenciado As String
        <JsonProperty>
        Public Property Chasis As String
        <JsonProperty>
        Public Property Color As ColorVehiculos
        <JsonProperty>
        Public Property Marca As MarcaVehiculos
        <JsonProperty>
        Public Property Linea As LineaVehiculos
        <JsonProperty>
        Public Property PesoBruto As Double
        <JsonProperty>
        Public Property Capacidad As Double
        <JsonProperty>
        Public Property CapacidadGalones As Double
        <JsonProperty>
        Public Property PesoTara As Double
        <JsonProperty>
        Public Property NumeroEjes As Double
        <JsonProperty>
        Public Property Cilindraje As Double
        <JsonProperty>
        Public Property ProveedorGPS As Tercero
        <JsonProperty>
        Public Property UsuarioGPS As String
        <JsonProperty>
        Public Property ClaveGPS As String
        <JsonProperty>
        Public Property TelefonoGPS As Double
        <JsonProperty>
        Public Property NumeroEstudioSeguridad As Double

        <JsonProperty>
        Public Overloads Property Estado As Estado
        <JsonProperty>
        Public Property CausaInactividad As ValorCatalogos
        <JsonProperty>
        Public Property JustificacionBloqueo As String
        <JsonProperty>
        Public Property EventoAuditoria As String
        <JsonProperty>
        Public Property Kilometraje As Double
        <JsonProperty>
        Public Property FechaActualizacionKilometraje As Date
        <JsonProperty>
        Public Property FechaUltimoCargue As Date
        <JsonProperty>
        Public Property Semirremolque As Semirremolques
        <JsonProperty>
        Public Property Documento As Documentos
        <JsonProperty>
        Public Property Documentos As IEnumerable(Of Documentos)
        <JsonProperty>
        Public Property DetalleFotos As IEnumerable(Of FotosVehiculos)
        <JsonProperty>
        Public Property AseguradoraSOAT As Tercero
        <JsonProperty>
        Public Property ReferenciaSOAT As String
        <JsonProperty>
        Public Property FechaVencimientoSOAT As Date
        <JsonProperty>
        Public Property URLGPS As String
        <JsonProperty>
        Public Property Puntos As Integer
        <JsonProperty>
        Public Property IdentificadorGPS As String
        <JsonProperty>
        Public Property PlantillaDocumento As Short
        <JsonProperty>
        Public Property Planitlla As Byte()
        <JsonProperty>
        Public Property Causa As String
        <JsonProperty>
        Public Property OrganismoTransito As String
        <JsonProperty>
        Public Property EstadoListaNegra As Integer
        <JsonProperty>
        Public Property CodigoListaNegra As Integer
        <JsonProperty>
        Public Property CapacidadVolumen As Double
        <JsonProperty>
        Public Property FechaFinal As Date
        <JsonProperty>
        Public Property FechaInicial As Date


    End Class

    ''' <summary>
    ''' Clase <see cref="VehiculosDocumentos"/>
    ''' </summary>
    <JsonObject>
    Public Class VehiculosDocumentos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="VehiculosDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="VehiculosDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader con resultado de consulta</param>
        Sub New(lector As IDataReader)
            Placa = Read(lector, "Placa")
            Tercero = New Terceros With {
                .Codigo = Read(lector, "TERC_Codigo"),
                .NombreCompleto = Read(lector, "TERC_Nombre"),
                .NumeroIdentificacion = Read(lector, "TERC_Identificacion"),
                .Celular = Read(lector, "TERC_Celular"),
                .Correo = Read(lector, "TERC_Emails")
            }
            TipoPropietario = Read(lector, "Tipo_Propietario")
            Documento = Read(lector, "Documento")
            FechaVenceDocumento = Read(lector, "Fecha_Vence")
            CantidadDiasFaltantes = Read(lector, "Dias_por_Vencer")
            FechaEmision = Read(lector, "Fecha_Emision")
        End Sub

        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Tercero As Terceros
        <JsonProperty>
        Public Property Placa As String
        <JsonProperty>
        Public Property TipoPropietario As String
        <JsonProperty>
        Public Property Documento As String
        <JsonProperty>
        Public Property FechaEmision As DateTime
        <JsonProperty>
        Public Property FechaVenceDocumento As DateTime
        <JsonProperty>
        Public Property Fecha As DateTime
        <JsonProperty>
        Public Property CantidadDiasFaltantes As String
        <JsonProperty>
        Public Property CodigoDetalleEpos As Integer
        <JsonProperty>
        Public Property TipoAplicativo As Integer

    End Class

End Namespace
