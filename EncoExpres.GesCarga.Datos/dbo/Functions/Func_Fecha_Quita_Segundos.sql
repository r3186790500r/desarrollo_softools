﻿Print 'Func_Fecha_Quita_Segundos'
GO
DROP FUNCTION Func_Fecha_Quita_Segundos
GO
CREATE FUNCTION Func_Fecha_Quita_Segundos
(
	@par_Fecha DATETIME
	
)
RETURNS DATETIME
AS
BEGIN
	declare @fechita datetime = getdate()
	set @fechita = DATEADD(MILLISECOND, -(DATEPART(MILLISECOND,@par_Fecha)),@par_Fecha)
	set @fechita = DATEADD(SECOND, -(DATEPART(SECOND,@fechita)),@par_Fecha)
	RETURN @fechita
END
GO