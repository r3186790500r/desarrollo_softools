﻿PRINT 'gsp_consultar_detalle_documento_comprobantes'
GO
DROP PROCEDURE gsp_consultar_detalle_documento_comprobantes
GO
CREATE PROCEDURE gsp_consultar_detalle_documento_comprobantes
(
@par_EMPR_Codigo NUMERIC,
@par_EDCO_Codigo NUMERIC
)
AS
BEGIN

SELECT 
	DDCO.EMPR_Codigo,
	DDCO.Codigo,
	DDCO.EDCO_Codigo,
	DDCO.PLUC_Codigo,
	DDCO.TERC_Codigo,
	DDCO.Valor_Base,
	DDCO.Valor_Debito,
	DDCO.Valor_Credito,
	DDCO.Genera_Cuenta,
	DDCO.Observaciones,
	DDCO.CATA_TEPC_Codigo,
	DDCO.CATA_CCPC_Codigo,
	DDCO.CATA_DOCR_Codigo,
	PLUC.Nombre AS NombreCuenta,
	PLUC.Codigo_Cuenta AS CuentaPUC,
	CASE WHEN TERC.CATA_TINT_Codigo = 1202 THEN TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 ELSE TERC.Razon_Social END As NombreTercero,
	TEPC.Nombre AS TerceroParametrizacion,
	CCPC.Nombre AS CentroCostoParametrizacion,
	DOCR.Nombre AS DocumentoCruze
FROM
	Detalle_Documento_Comprobantes DDCO INNER JOIN Plan_Unico_Cuentas PLUC ON
	DDCO.EMPR_Codigo = PLUC.EMPR_Codigo
	AND DDCO.PLUC_Codigo = PLUC.Codigo

	LEFT JOIN  Terceros TERC ON
	DDCO.EMPR_Codigo = TERC.EMPR_Codigo
	AND DDCO.TERC_Codigo = TERC.Codigo

	LEFT JOIN V_Tercero_Parametrizacion_Contable TEPC ON 
	DDCO.EMPR_Codigo = TEPC.EMPR_Codigo
	AND DDCO.CATA_TEPC_Codigo = TEPC.Codigo

	LEFT JOIN V_Centro_Costo_Parametrizacion_Contable CCPC ON
	DDCO.EMPR_Codigo = CCPC.EMPR_Codigo
	AND DDCO.CATA_CCPC_Codigo = CCPC.Codigo

	LEFT JOIN V_Tipo_Documento_Cruce DOCR ON
	DDCO.EMPR_Codigo = DOCR.EMPR_Codigo
	AND DDCO.CATA_DOCR_Codigo = DOCR.Codigo

WHERE
DDCO.EMPR_Codigo = @par_EMPR_Codigo
AND DDCO.EDCO_Codigo  = @par_EDCO_Codigo

END
GO