﻿EncoExpresApp.controller("ConsultarSolicitudAutorizacionesCtrl", ['$scope', '$timeout', '$linq', 'AutorizacionesFactory', 'blockUI', 'ValorCatalogosFactory', '$routeParams',
    function ($scope, $timeout, $linq, AutorizacionesFactory, blockUI, ValorCatalogosFactory, $routeParams) {
        $scope.MapaSitio = [{ Nombre: 'Control Trafico' }, { Nombre: 'Procesos' }, { Nombre: 'Autorizaciones' }];
        console.clear();

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
        }
        $scope.Documento = {}
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_AUTORIZACIONES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoEstados = [
            { Codigo: -1, Nombre: "(NO APLICA)" },
            { Codigo: 0, Nombre: "INACTIVO" },
            { Codigo: 1, Nombre: "ACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0]

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 184 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoSolicitudAutorizacionOperaciones = []
                        $scope.ListadoTipoSolicitudAutorizacionOperaciones.push({ Nombre: '(NO APLICA)', Codigo: 0 })
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoTipoSolicitudAutorizacionOperaciones.push(response.data.Datos[i])
                        }
                        $scope.Modelo.TipoSolicitudAutorizacionOperaciones = $scope.ListadoTipoSolicitudAutorizacionOperaciones[0]
                    }
                    else {
                        $scope.ListadoTipoSolicitudAutorizacionOperaciones = []
                    }
                }
            }, function (response) {
            });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 183 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoEstadoSolicitudAutorizacionOperaciones = []
                        $scope.ListadoEstadoSolicitudAutorizacionOperaciones = response.data.Datos
                        $scope.Modelo.EstadoSolicitudAutorizacionOperaciones = $scope.ListadoEstadoSolicitudAutorizacionOperaciones[0]
                    }
                    else {
                        $scope.ListadoTipoSolicitudAutorizacionOperaciones = []
                    }
                }
            }, function (response) {
            });
        $scope.LidatosAcciones = [
            { Nombre: 'AUTORIZAR', Codigo: 18302 },
            { Nombre: 'RECHAZAR', Codigo: 18303 }
        ]

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

        /*Metodo buscar*/
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                Find()
            }
        };
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarTerceros';
            }
        };
        if ($routeParams.Numero !== undefined) {
            $scope.Modelo.Numero = $routeParams.Numero;
            $scope.Buscar()
        } else {
            $scope.Modelo.Numero = undefined;
        }

        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {

            $scope.ListadoSolicitudes = [];
            if (DatosRequeridos()) {
                BloqueoPantalla.start('Buscando registros ...');

                var Filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Modelo.Numero,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                    TipoAutorizacion: $scope.Modelo.TipoSolicitudAutorizacionOperaciones,
                    EstadoAutorizacion: $scope.Modelo.EstadoSolicitudAutorizacionOperaciones,
                    FechaInicioAutoriza: $scope.Modelo.FechaInicioAutoriza,
                    FechaFinAutoriza: $scope.Modelo.FechaFinAutoriza,
                    NombreUsuarioSolicita: $scope.Modelo.UsuarioSolicita,
                    NombreUsuarioAutoriza: $scope.Modelo.UsuarioAutoriza
                }

                $scope.Modelo.Numero = 0
                $scope.Modelo.Pagina = $scope.paginaActual
                $scope.Modelo.RegistrosPagina = $scope.cantidadRegistrosPorPagina
                if ($scope.MensajesError.length === 0) {
                    AutorizacionesFactory.Consultar(Filtro).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {

                                    $scope.ListadoSolicitudes = response.data.Datos
                                    for (var i = 0; i < $scope.ListadoSolicitudes.length; i++) {
                                        if ($scope.ListadoSolicitudes[i].FechaAutoriza < MIN_DATE) {
                                            $scope.ListadoSolicitudes[i].FechaAutoriza = ''
                                        }
                                    }
                                    //$scope.Modelo.Codigo = undefined;
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.ListadoSolicitudes = "";
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                }
                            }
                            BloqueoPantalla.stop();
                        }, function (response) {
                            BloqueoPantalla.stop();
                        });
                }
                else {
                    BloqueoPantalla.stop();
                }
            }
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var modelo = $scope.Modelo
            var continuar = true;

            if (modelo.NumeroViaje == null || modelo.NumeroViaje == undefined || modelo.NumeroViaje == 0) {

                if ((modelo.FechaInicioAutoriza == null || modelo.FechaInicioAutoriza == undefined || modelo.FechaInicioAutoriza == '')
                    && (modelo.FechaFinAutoriza == null || modelo.FechaFinAutoriza == undefined || modelo.FechaFinAutoriza == '')
                ) {
                    $scope.MensajesError.push('Debe ingresar los filtros de fechas');
                    continuar = false
                } else if ((modelo.FechaInicioAutoriza !== null && modelo.FechaInicioAutoriza !== undefined && modelo.FechaInicioAutoriza !== '')
                    && (modelo.FechaFinAutoriza !== null && modelo.FechaFinAutoriza !== undefined && modelo.FechaFinAutoriza !== '')) {
                    if (modelo.FechaFinAutoriza < modelo.FechaInicioAutoriza) {
                        $scope.MensajesError.push('La fecha inicio debe ser menor a la fecha fin');
                        continuar = false
                    }
                    if ($scope.FechaInicioAutoriza !== null && $scope.FechaFinAutoriza == null) {
                        $scope.FechaFinAutoriza = $scope.FechaInicioAutoriza;
                    }
                    else if ($scope.FechaInicioAutoriza == null && ($scope.FechaFinAutoriza !== null && $scope.FechaFinAutoriza !== undefined)) {
                        $scope.MensajesError.push('Debe seleccionar la fecha inicio');
                        continuar = false
                    } else {
                        if (((modelo.FechaFinAutoriza - modelo.FechaInicioAutoriza) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                            $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                            continuar = false
                        }
                    }
                } else {
                    if ((modelo.FechaInicioAutoriza !== null && modelo.FechaInicioAutoriza !== undefined && modelo.FechaInicioAutoriza !== '')) {
                        $scope.Modelo.FechaFinAutoriza = modelo.FechaInicioAutoriza
                    } else {
                        $scope.Modelo.FechaInicioAutoriza = modelo.FechaFinAutoriza
                    }
                }
            } else {
                $scope.Modelo.NumeroViaje = modelo.NumeroViaje
            }

            return continuar
        }


        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.AbrirViaje = function (item) {
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
            }
            ViajesFactory.Consultar(filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos.length > 0) {
                            showModal('ActualizarViaje')
                            $scope.ActualizarViaje = response.data.Datos[0]

                            item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                            item.PuntoGestion = $scope.Modelo.PuntoGestion
                            item.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                            ViajesFactory.Obtener(item).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso == true) {
                                        if (response.data.Datos.Numero > 0) {
                                            showModal('ActualizarViaje')
                                            //$scope.ActualizarViaje = response.data.Datos
                                            if (new Date(response.data.Datos.FechaVigenciaGuia) > MIN_DATE) {
                                                $scope.ActualizarViaje.FechaVigenciaGuia = new Date(response.data.Datos.FechaVigenciaGuia)
                                                $scope.ActualizarViaje.HoraGuia = RetornarHoras($scope.ActualizarViaje.FechaVigenciaGuia);
                                            } else {
                                                $scope.ActualizarViaje.FechaVigenciaGuia = ''
                                            }
                                            if (new Date(response.data.Datos.FechaCargue) > MIN_DATE) {
                                                $scope.ActualizarViaje.FechaCargue = new Date(response.data.Datos.FechaCargue)
                                                $scope.ActualizarViaje.HoraCargue = RetornarHoras($scope.ActualizarViaje.FechaCargue);
                                            }
                                            else {
                                                $scope.ActualizarViaje.FechaCargue = ''
                                            }
                                            $scope.ActualizarViaje.Prescintos = response.data.Datos.Prescintos
                                            $scope.ActualizarViaje.VOLBSW = response.data.Datos.VOLBSW
                                            $scope.ActualizarViaje.VOLGOV = response.data.Datos.VOLGOV
                                            $scope.ActualizarViaje.VOLGSV = response.data.Datos.VOLGSV
                                            $scope.ActualizarViaje.VOLNSV = response.data.Datos.VOLNSV
                                            $scope.ActualizarViaje.VOLRVP = response.data.Datos.VOLRVP
                                            $scope.ActualizarViaje.URLGPS = response.data.Datos.URLGPS
                                            $scope.ActualizarViaje.UsuarioGPS = response.data.Datos.UsuarioGPS
                                            $scope.ActualizarViaje.ClaveGPS = response.data.Datos.ClaveGPS
                                            $scope.ActualizarViaje.Producto = response.data.Datos.Producto
                                        }
                                    }
                                }, function (response) {
                                });

                        }
                    }
                }, function (response) {
                });


        }
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.GestionarSolicitud = function (item) {
            $scope.TempItem = item
            $scope.Modelo.EstadoAcccion = $scope.LidatosAcciones[0]
            $scope.Modelo.Observaciones = ''
            $('#NuevoDocumento').hide()
            $('#BotonNuevo').show()
            $scope.Modelo.ListadoDocumentos = []
            showModal('ModalGestionarSolicitud')
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.TempItem.Codigo,
            }
            AutorizacionesFactory.Obtener(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Solicitud = response.data.Datos;
                        try {
                            $scope.Solicitud.Valor = MascaraValores(parseInt($scope.Solicitud.Valor))
                        } catch (e) {

                        }
                    }
                }, function (response) {
                    $scope.Buscando = false;
                });
        }
        $scope.ActualizarSolicitud = function () {
            if ($scope.Solicitud.TipoAutorizacion.Codigo == 18402 && $scope.EstadoAcccion.Codigo == 18302 && ($scope.Solicitud.Valor == 0 || $scope.Solicitud.Valor == undefined)) {
                ShowError('Por favor ingrese el anticipo')
            } else {
                var entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.TempItem.Codigo,
                    EstadoAutorizacion: $scope.EstadoAcccion,
                    Observaciones: $scope.Observaciones,
                    UsuarioGestion: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Valor: $scope.Solicitud.Valor
                }
                AutorizacionesFactory.Guardar(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('La solicitud se gestionó correctamente')
                                closeModal('ModalGestionarSolicitud')
                            }
                            else {
                                ShowError('No se logro gestionar la solicitud')
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
        }
        $scope.MostrarConfirmacionEliminar = function (item) {
            showModal('modalConfirmacionEliminar')
            $scope.TempItemeElimina = item

        }
        $scope.EliminarSolicitud = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.TempItemeElimina.Numero,
            }
            AutorizacionesFactory.Eliminar(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('La solicitud se elimino correctamente')
                        closeModal('modalConfirmacionEliminar')
                    }
                    else {
                        ShowError('No se logro eliminar la solicitud')
                    }
                    Find()
                }, function (response) {
                    $scope.Buscando = false;
                });
        }

        $scope.AdicionarDocumento = function () {
            if ($scope.Documento.Descripcion == '' || $scope.Documento.Descripcion == undefined) {
                ShowWarning('Por favor ingrese la descripcion del documento')
            } else {
                if ($scope.Documento.Archivo == '' || $scope.Documento.Archivo == undefined) {
                    ShowWarning('Por favor ingrese un documento')
                }
                else {
                    if ($scope.Modelo.ListadoDocumentos.length == LIMITE_DOCUMENTOS_AUTORIZACIONES) {
                        ShowInfo('El numero maximo de documentos permitidos es ' + LIMITE_DOCUMENTOS_AUTORIZACIONES)
                    } else {
                        $scope.Modelo.ListadoDocumentos.push(JSON.parse(JSON.stringify($scope.Documento)))
                        $scope.Documento.Descripcion = ''
                        $scope.Documento.Archivo = undefined
                    }
                }
            }
        }
        $scope.ElimiarDocumento = function (index) {
            $scope.Modelo.ListadoDocumentos.splice(index, 1)
        }
        $scope.CargarArchivo = function (element) {
            var continuar = true;
            if (element.files.length > 0) {
                $scope.PDF = element
                $scope.TipoDocumento = element
                blockUI.start();
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento.Archivo = angular.element(this.ItemFoto)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, poar favor verifique el formato del archivo  /nFormatos permitidos: .BMP, .GIF, .JPG, .JPEG, .TIF, .TIFF, .PNG, .doc, .docx, .docm, .dotx, .dotm, .dot, .xps, .txt, .xml, .odt, .xlsx, .xlsm, .xlsb, .xltx, .xltm, .xls, .xlt, .xml, .xlam, .xla, .xlw, .xlr");
                    continuar = false;
                }
                if (continuar == true) {
                    var Formatos = ('.BMP, .GIF, .JPG, .JPEG, .TIF, .TIFF, .PNG, .doc, .docx, .docm, .dotx, .dotm, .dot, .xps, .txt, .xml, .odt, .xlsx, .xlsm, .xlsb, .xltx, .xltm, .xls, .xlt, .xml, .xlam, .xla, .xlw, .xlr').split(',')
                    var cont = 0
                    for (var i = 0; i < Formatos.length; i++) {
                        Formatos[i] = Formatos[i].replace(' ', '')
                        var Extencion = element.files[0].name.split('.')
                        //if (element.files[0].type == Formatos[i]) {
                        if ('.' + (Extencion[1].toUpperCase()) == Formatos[i].toUpperCase()) {
                            cont++
                        }
                    }
                    if (cont == 0) {
                        ShowError("Archivo invalido, poar favor verifique el formato del archivo  Formatos permitidos: '.BMP, .GIF, .JPG, .JPEG, .TIF, .TIFF, .PNG, .doc, .docx, .docm, .dotx, .dotm, .dot, .xps, .txt, .xml, .odt, .xlsx, .xlsm, .xlsb, .xltx, .xltm, .xls, .xlt, .xml, .xlam, .xla, .xlw, .xlr");
                        continuar = false;
                    }
                    else if (element.files[0].size >= 3000000) //3 MB
                    {
                        ShowError("El archivo " + element.files[0].name + " supera los 3 MB de tamaño, intente con otro archivo", false);
                        continuar = false;
                    }
                }
                if (continuar == true) {
                    var reader = new FileReader();
                    $scope.Documento.ArchivoCargado = element.files[0]
                    reader.onload = $scope.AsignarArchivo;
                    reader.readAsDataURL(element.files[0]);
                }

                blockUI.stop();
            }
        }
        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                $scope.Documento.Nombre = Extencion[0]
                $scope.Documento.Extension = Extencion[1]
            });
        }
        $scope.MostarDocumentos = function (item) {
            $scope.Numero = item.Numero.toString()
            AutorizacionesFactory.Obtener(item).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        try {
                            if (response.data.Datos.Documentos.length > 0) {
                                $scope.Modelo.ListadoDocumentos = response.data.Datos.Documentos
                                showModal('ModalDocumentos')
                            } else {
                                ShowWarning('Esta solicitud no tiene documentos asociados')
                            }

                        } catch (e) {
                            ShowWarning('Esta solicitud no tiene documentos asociados')
                        }

                    }
                }, function (response) {
                    $scope.Buscando = false;
                });
        }
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {

            window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Autorizaciones&Codigo=' + item.Codigo + '&ESAO_Numero=' + $scope.Numero + '');


        };
    }]);
