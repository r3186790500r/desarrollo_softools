﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Entidades.Utilitarios
Imports EncoExpres.GesCarga.Repositorio.SeguridadUsuarios

Namespace Utilitarios

    ''' <summary>
    ''' Clase <see cref="RepositorioCorreos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioCorreos
        Inherits Correos
        Dim Correo As New Correos()

        ''Evento de correos para solicitudes y ordenes de servicio

        Public Sub GenerarCorreo(Entidad As EncabezadoSolicitudOrdenServicios)
            Dim lista As New List(Of EventoCorreos)
            Dim listaCorreos As New List(Of String)
            Dim item As EventoCorreos
            Dim ContenidoCorreo As String = ""
            Dim AsuntoCorreo As String = ""
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                'Consulta los enventos para el documento teniendo como parametro el tipo de documento
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", Entidad.TipoDocumento)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_evento_correos_tipo_documento]")

                While resultado.Read
                    item = New EventoCorreos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                If lista.Count() > 0 Then
                    For Each Evento In lista
                        'Se arma el mensaje
                        ContenidoCorreo = ""
                        Correo.CamposMensaje = New List(Of BaseCorreo)
                        AdicionarCampo(Correo.CamposMensaje, CampoOrdenServicio, Entidad.NumeroDocumento.ToString())
                        AdicionarCampo(Correo.CamposMensaje, CampoSolicitudServicio, Entidad.NumeroDocumento.ToString())
                        AdicionarCampo(Correo.CamposMensaje, CampoCliente, Entidad.Cliente.NombreCompleto)
                        AdicionarCampo(Correo.CamposMensaje, CampoLineaNegocio, Entidad.LineaNegocioCarga.Nombre)
                        AdicionarCampo(Correo.CamposMensaje, CampoTipoLineaNegocio, Entidad.TipoLineaNegocioCarga.Nombre)
                        AdicionarCampo(Correo.CamposMensaje, CampoOrigen, Entidad.CiudadCargue.Nombre)
                        Dim Destino As String = ""
                        Dim Unidades As Integer = 0
                        Dim Peso As Integer = 0
                        Dim Producto As String = ""
                        If Entidad.Detalle.Count() > 0 Then
                            For Each Detalle In Entidad.Detalle
                                Destino += Detalle.CiudadDestino.Nombre
                                Unidades += Detalle.Cantidad
                                Peso += Detalle.Peso
                                Producto = Detalle.Producto.Nombre
                            Next
                        Else
                            If Not IsNothing(Entidad.CiudadDescargue) Then
                                Destino = Entidad.CiudadDescargue.Nombre
                            End If
                            If Not IsNothing(Entidad.Producto) Then
                                Producto = Entidad.Producto.Nombre
                            End If
                        End If
                        AdicionarCampo(Correo.CamposMensaje, CampoDestino, Destino)
                        AdicionarCampo(Correo.CamposMensaje, CampoFechaInicio, Entidad.FechaCargue.ToShortDateString())
                        AdicionarCampo(Correo.CamposMensaje, CampoHoraInicio, Entidad.FechaCargue.ToShortTimeString())
                        AdicionarCampo(Correo.CamposMensaje, CampoDireccionOrigen, Entidad.DireccionCargue)
                        AdicionarCampo(Correo.CamposMensaje, CampoUnidades, Unidades.ToString())
                        AdicionarCampo(Correo.CamposMensaje, CampoPeso, Peso.ToString())
                        AdicionarCampo(Correo.CamposMensaje, CampoProducto, Producto)
                        ContenidoCorreo = GenerarCuerpo(Evento.EncabezadoCorreo, GenerarMensaje(Correo, Evento.MensajeCorreo), Evento.MensajeFirma)
                        AsuntoCorreo = GenerarMensaje(Correo, Evento.AsuntoCorreo)
                        'Consulta los correos  a los cuales aplica el documento teniendo en cuenta las distribuciones 
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EVCO_Codigo", Evento.Codigo)
                        If Not IsNothing(Entidad.Cliente) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", Entidad.Cliente.Codigo)
                        Else
                            If Entidad.CodigoCliente > 0 Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", Entidad.CodigoCliente)
                            End If
                        End If

                        If Not IsNothing(Entidad.SedeFacturacion) Then
                            If Entidad.SedeFacturacion.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_TEDI_Codigo", Entidad.SedeFacturacion.Codigo)
                            End If

                        End If

                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_distribucion_evento_correos]")
                        While resultado.Read
                            listaCorreos.Add(Read(resultado, "Correo"))
                        End While
                        resultado.Close()
                        If listaCorreos.Count() > 0 Then
                            For Each Email In listaCorreos
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                                conexion.AgregarParametroSQL("@par_TIDO_Codigo", Entidad.TipoDocumento)
                                conexion.AgregarParametroSQL("@par_Numero_Documento", Entidad.NumeroDocumento)
                                conexion.AgregarParametroSQL("@par_Cuenta_Correo_Para", Email)
                                conexion.AgregarParametroSQL("@par_Texto_Correo", ContenidoCorreo)
                                conexion.AgregarParametroSQL("@par_Asunto_Correo", AsuntoCorreo)
                                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", Entidad.UsuarioCrea.Codigo)
                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_bandeja_salida_correos]")
                                resultado.Close()
                            Next
                        End If
                    Next
                End If
            End Using
        End Sub

        ''Evento de correos para Estudio seguridad

        Public Sub GenerarCorreo(Entidad As EstudioSeguridad)
            Dim lista As New List(Of EventoCorreos)
            Dim listaCorreos As New List(Of String)
            Dim item As EventoCorreos
            Dim ContenidoCorreo As String = ""
            Dim AsuntoCorreo As String = ""
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                'Consulta los enventos para el documento teniendo como parametro el tipo de documento
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", Entidad.TipoDocumento)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_evento_correos_tipo_documento]")

                While resultado.Read
                    item = New EventoCorreos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                If Entidad.TipoDocumento <> 220 Then
                    Entidad.TipoDocumento = 220
                End If
                If lista.Count() > 0 Then
                    For Each Evento In lista
                        'Se arma el mensaje
                        ContenidoCorreo = ""
                        Correo.CamposMensaje = New List(Of BaseCorreo)

                        AdicionarCampo(Correo.CamposMensaje, CampoEstudioSeguridad, Entidad.NumeroDocumento.ToString())
                        AdicionarCampo(Correo.CamposMensaje, CampoPlaca, Entidad.Placa)
                        AdicionarCampo(Correo.CamposMensaje, CampoConductor, Entidad.Conductor.NombreCompleto)

                        ContenidoCorreo = GenerarCuerpo(Evento.EncabezadoCorreo, GenerarMensaje(Correo, Evento.MensajeCorreo), Evento.MensajeFirma)

                        AsuntoCorreo = GenerarMensaje(Correo, Evento.AsuntoCorreo)
                        'Consulta los correos  a los cuales aplica el documento teniendo en cuenta las distribuciones 
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EVCO_Codigo", Evento.Codigo)
                        'If Not IsNothing(Entidad.Cliente) Then
                        '    conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", Entidad.Cliente.Codigo)
                        'End If
                        If Not IsNothing(Entidad.Conductor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", Entidad.Conductor.Codigo)
                        End If
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_distribucion_evento_correos]")
                        While resultado.Read
                            listaCorreos.Add(Read(resultado, "Correo"))
                        End While
                        resultado.Close()
                        If listaCorreos.Count() > 0 Then
                            For Each Email In listaCorreos
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                                conexion.AgregarParametroSQL("@par_TIDO_Codigo", Evento.TipoDocumento)
                                conexion.AgregarParametroSQL("@par_Numero_Documento", Evento.NumeroDocumento)
                                conexion.AgregarParametroSQL("@par_Cuenta_Correo_Para", Email)
                                conexion.AgregarParametroSQL("@par_Texto_Correo", ContenidoCorreo)
                                conexion.AgregarParametroSQL("@par_Asunto_Correo", AsuntoCorreo)
                                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", Entidad.UsuarioCrea.Codigo)
                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_bandeja_salida_correos]")
                                resultado.Close()
                            Next
                        End If
                    Next
                End If
            End Using

        End Sub

        ''Evento de correos para terceros

        Public Sub GenerarCorreo(Entidad As Terceros)
            Dim lista As New List(Of EventoCorreos)
            Dim listaCorreos As New List(Of String)
            Dim item As EventoCorreos
            Dim ContenidoCorreo As String = ""
            Dim AsuntoCorreo As String = ""
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                'Consulta los enventos para el documento teniendo como parametro el tipo de documento
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 10)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_evento_correos_tipo_documento]")

                While resultado.Read
                    item = New EventoCorreos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                If lista.Count() > 0 Then
                    For Each Evento In lista
                        'Se arma el mensaje
                        ContenidoCorreo = ""
                        Correo.CamposMensaje = New List(Of BaseCorreo)

                        AdicionarCampo(Correo.CamposMensaje, CampoTercero, Entidad.Nombre + Entidad.RazonSocial)
                        AdicionarCampo(Correo.CamposMensaje, CampoIdentificacionTercero, Entidad.NumeroIdentificacion)

                        ContenidoCorreo = GenerarCuerpo(Evento.EncabezadoCorreo, GenerarMensaje(Correo, Evento.MensajeCorreo), Evento.MensajeFirma)
                        AsuntoCorreo = GenerarMensaje(Correo, Evento.AsuntoCorreo)
                        'Consulta los correos  a los cuales aplica el documento teniendo en cuenta las distribuciones 
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EVCO_Codigo", Evento.Codigo)
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_distribucion_evento_correos]")
                        While resultado.Read
                            listaCorreos.Add(Read(resultado, "Correo"))
                        End While
                        resultado.Close()
                        If listaCorreos.Count() > 0 Then
                            For Each Email In listaCorreos
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                                conexion.AgregarParametroSQL("@par_TIDO_Codigo", Evento.TipoDocumento)
                                conexion.AgregarParametroSQL("@par_Numero_Documento", Evento.Codigo)
                                conexion.AgregarParametroSQL("@par_Cuenta_Correo_Para", Email)
                                conexion.AgregarParametroSQL("@par_Texto_Correo", ContenidoCorreo)
                                conexion.AgregarParametroSQL("@par_Asunto_Correo", AsuntoCorreo)
                                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", Entidad.UsuaCodigoCrea)
                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_bandeja_salida_correos]")
                                resultado.Close()
                            Next
                        End If
                    Next
                End If
            End Using

        End Sub

        ''Evento de correos para vehículos


        Public Sub GenerarCorreo(Entidad As Vehiculos)
            Dim lista As New List(Of EventoCorreos)
            Dim listaCorreos As New List(Of String)
            Dim item As EventoCorreos
            Dim ContenidoCorreo As String = ""
            Dim AsuntoCorreo As String = ""
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                'Consulta los enventos para el documento teniendo como parametro el tipo de documento
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 20)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_evento_correos_tipo_documento]")

                While resultado.Read
                    item = New EventoCorreos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                If lista.Count() > 0 Then
                    For Each Evento In lista
                        'Se arma el mensaje
                        ContenidoCorreo = ""
                        Correo.CamposMensaje = New List(Of BaseCorreo)

                        AdicionarCampo(Correo.CamposMensaje, CampoPlaca, Entidad.Placa)
                        AdicionarCampo(Correo.CamposMensaje, CampoConductor, Entidad.Conductor.Nombre)

                        ContenidoCorreo = GenerarCuerpo(Evento.EncabezadoCorreo, GenerarMensaje(Correo, Evento.MensajeCorreo), Evento.MensajeFirma)
                        AsuntoCorreo = GenerarMensaje(Correo, Evento.AsuntoCorreo)
                        'Consulta los correos  a los cuales aplica el documento teniendo en cuenta las distribuciones 
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EVCO_Codigo", Evento.Codigo)
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_distribucion_evento_correos]")
                        While resultado.Read
                            listaCorreos.Add(Read(resultado, "Correo"))
                        End While
                        resultado.Close()
                        If listaCorreos.Count() > 0 Then
                            For Each Email In listaCorreos
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                                conexion.AgregarParametroSQL("@par_TIDO_Codigo", Evento.TipoDocumento)
                                conexion.AgregarParametroSQL("@par_Numero_Documento", Evento.Codigo)
                                conexion.AgregarParametroSQL("@par_Cuenta_Correo_Para", Email)
                                conexion.AgregarParametroSQL("@par_Texto_Correo", ContenidoCorreo)
                                conexion.AgregarParametroSQL("@par_Asunto_Correo", AsuntoCorreo)
                                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", Entidad.UsuarioCrea.Codigo)
                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_bandeja_salida_correos]")
                                resultado.Close()
                            Next
                        End If
                    Next
                End If
            End Using

        End Sub


        'Evento de correos para Seguimiento Vehícular

        Public Sub GenerarCorreo(Entidad As DetalleSeguimientoVehiculos)
            Dim lista As New List(Of EventoCorreos)
            Dim listaCorreos As New List(Of String)
            Dim item As EventoCorreos
            Dim ContenidoCorreo As String = ""
            Dim AsuntoCorreo As String = ""
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                'Consulta los enventos para el documento teniendo como parametro el tipo de documento
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", Entidad.TipoDocumento)

                If Entidad.CodigoEvento = 221 Then
                    conexion.AgregarParametroSQL("@Codigo", Entidad.CodigoEvento)
                Else
                    conexion.AgregarParametroSQL("@Codigo", 159)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_evento_correos_tipo_documento]")
                While resultado.Read
                    item = New EventoCorreos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                If lista.Count() > 0 Then
                    For Each Evento In lista
                        'Se arma el mensaje
                        ContenidoCorreo = ""
                        Correo.CamposMensaje = New List(Of BaseCorreo)
                        If Entidad.TipoDocumento = 120 Then
                            AdicionarCampo(Correo.CamposMensaje, CampoPlaca, Entidad.Vehiculo.Placa)
                            AdicionarCampo(Correo.CamposMensaje, Planilla, Entidad.NumeroPlanilla)
                            AdicionarCampo(Correo.CamposMensaje, CampoFechaFin, Entidad.FechaCrea)

                        End If
                        ContenidoCorreo = GenerarCuerpo(Evento.EncabezadoCorreo, GenerarMensaje(Correo, Evento.MensajeCorreo), Evento.MensajeFirma)
                        AsuntoCorreo = GenerarMensaje(Correo, Evento.AsuntoCorreo)
                        'Consulta los correos  a los cuales aplica el documento teniendo en cuenta las distribuciones 
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EVCO_Codigo", Evento.Codigo)
                        If Entidad.CodigoCliente > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", Entidad.CodigoCliente)
                        End If
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_distribucion_evento_correos]")
                        While resultado.Read
                            listaCorreos.Add(Read(resultado, "Correo"))
                        End While
                        resultado.Close()
                        If listaCorreos.Count() > 0 Then
                            For Each Email In listaCorreos
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 120)
                                If Entidad.CodigoEvento = 221 Then
                                    conexion.AgregarParametroSQL("@par_Numero_Documento", Entidad.NumeroPlanilla)
                                Else
                                    conexion.AgregarParametroSQL("@par_Numero_Documento", Entidad.Codigo)
                                End If
                                conexion.AgregarParametroSQL("@par_Cuenta_Correo_Para", Email)
                                conexion.AgregarParametroSQL("@par_Texto_Correo", ContenidoCorreo)
                                conexion.AgregarParametroSQL("@par_Asunto_Correo", AsuntoCorreo)
                                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", Entidad.UsuarioCrea.Codigo)
                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_bandeja_salida_correos]")
                                resultado.Close()
                            Next
                        End If
                    Next
                End If
            End Using

        End Sub

        'Evento de correos para Inspecciones no cumplidas:
        Public Sub GenerarCorreo(Entidad As InspeccionPreoperacional, ByVal conexion As DataBaseFactory)
            Dim lista As New List(Of EventoCorreos)
            Dim listaCorreos As New List(Of String)
            Dim item As EventoCorreos
            Dim ContenidoCorreo As String = ""
            Dim AsuntoCorreo As String = ""

            'Consulta los enventos para el documento teniendo como parametro el tipo de documento
            conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 240)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_evento_correos_tipo_documento]")

                While resultado.Read
                    item = New EventoCorreos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                If lista.Count() > 0 Then
                    For Each Evento In lista
                        'Se arma el mensaje
                        ContenidoCorreo = ""
                        Correo.CamposMensaje = New List(Of BaseCorreo)
                        If Entidad.TipoDocumento = 240 Then
                            AdicionarCampo(Correo.CamposMensaje, CampoProgramacion, Entidad.Numero)
                        End If
                        ContenidoCorreo = GenerarCuerpo(Evento.EncabezadoCorreo, GenerarMensaje(Correo, Evento.MensajeCorreo), Evento.MensajeFirma)
                        AsuntoCorreo = GenerarMensaje(Correo, Evento.AsuntoCorreo)
                        'Consulta los correos  a los cuales aplica el documento teniendo en cuenta las distribuciones 
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EVCO_Codigo", Evento.Codigo)

                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_distribucion_evento_correos]")
                        While resultado.Read
                            listaCorreos.Add(Read(resultado, "Correo"))
                        End While
                        resultado.Close()

                        Dim RepoUsuarios As RepositorioUsuarios = New RepositorioUsuarios
                        Dim ListaCorreosPorOficina As List(Of String)
                        ListaCorreosPorOficina = RepoUsuarios.ConsultarCorreosPorOficina(Entidad, conexion)

                        If ListaCorreosPorOficina.Count > 0 Then
                            For Each strCorreo In ListaCorreosPorOficina
                                listaCorreos.Add(strCorreo)
                            Next
                        End If
                        resultado.Close()
                        If listaCorreos.Count() > 0 Then
                            For Each Email In listaCorreos
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 240)
                                conexion.AgregarParametroSQL("@par_Numero_Documento", Entidad.Codigo)
                                conexion.AgregarParametroSQL("@par_Cuenta_Correo_Para", Email)
                                conexion.AgregarParametroSQL("@par_Texto_Correo", ContenidoCorreo)
                                conexion.AgregarParametroSQL("@par_Asunto_Correo", AsuntoCorreo)
                                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", Entidad.UsuarioCrea.Codigo)

                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_bandeja_salida_correos]")
                                resultado.Close()
                            Next

                        End If
                    Next
                End If

        End Sub


        'Evento de correos para Programaciones Orden Servicio

        Public Sub GenerarCorreo(Entidad As DetalleProgramacionOrdenServicios)
            Dim lista As New List(Of EventoCorreos)
            Dim listaCorreos As New List(Of String)
            Dim item As EventoCorreos
            Dim ContenidoCorreo As String = ""
            Dim AsuntoCorreo As String = ""
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                'Consulta los enventos para el documento teniendo como parametro el tipo de documento
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 310)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_evento_correos_tipo_documento]")

                While resultado.Read
                    item = New EventoCorreos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                If lista.Count() > 0 Then
                    For Each Evento In lista
                        'Se arma el mensaje
                        ContenidoCorreo = ""
                        Correo.CamposMensaje = New List(Of BaseCorreo)
                        If Entidad.TipoDocumento = 310 Then
                            AdicionarCampo(Correo.CamposMensaje, CampoProgramacion, Entidad.Numero)
                        End If
                        ContenidoCorreo = GenerarCuerpo(Evento.EncabezadoCorreo, GenerarMensaje(Correo, Evento.MensajeCorreo), Evento.MensajeFirma)
                        AsuntoCorreo = GenerarMensaje(Correo, Evento.AsuntoCorreo)
                        'Consulta los correos  a los cuales aplica el documento teniendo en cuenta las distribuciones 
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EVCO_Codigo", Evento.Codigo)

                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_distribucion_evento_correos]")
                        While resultado.Read
                            listaCorreos.Add(Read(resultado, "Correo"))
                        End While
                        resultado.Close()

                        If listaCorreos.Count() > 0 Then
                            For Each Email In listaCorreos
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Entidad.CodigoEmpresa)
                                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 310)
                                conexion.AgregarParametroSQL("@par_Numero_Documento", Entidad.Codigo)
                                conexion.AgregarParametroSQL("@par_Cuenta_Correo_Para", Email)
                                conexion.AgregarParametroSQL("@par_Texto_Correo", ContenidoCorreo)
                                conexion.AgregarParametroSQL("@par_Asunto_Correo", AsuntoCorreo)
                                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", Entidad.UsuarioCrea.Codigo)
                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_bandeja_salida_correos]")
                                resultado.Close()
                            Next
                        End If
                    Next
                End If
            End Using

        End Sub

        Public Function GenerarMensaje(Correo As Correos, Mensaje As String) As String
            For Each Campo In Correo.CamposMensaje
                Mensaje = Mensaje.Replace(Campo.NombreCampo, Campo.Valor.ToUpper())
            Next
            Return Mensaje
        End Function

        Public Function GenerarCuerpo(Encabezado As String, Mensaje As String, Firma As String) As String
            Dim ContenidoCorreo As String = ""
            ContenidoCorreo += vbNewLine
            ContenidoCorreo += vbNewLine
            ContenidoCorreo = Encabezado
            ContenidoCorreo += vbNewLine
            ContenidoCorreo += vbNewLine
            ContenidoCorreo += Mensaje
            ContenidoCorreo += vbNewLine
            ContenidoCorreo += vbNewLine

            ContenidoCorreo += vbNewLine
            ContenidoCorreo += vbNewLine
            ContenidoCorreo += Firma
            ContenidoCorreo += vbNewLine
            ContenidoCorreo += vbNewLine
            Return ContenidoCorreo
        End Function

        Public Sub AdicionarCampo(ByRef Listado As List(Of BaseCorreo), Campo As String, Valor As String)
            Listado.Add(New BaseCorreo With {.NombreCampo = Campo, .Valor = Valor})
        End Sub

    End Class


End Namespace
