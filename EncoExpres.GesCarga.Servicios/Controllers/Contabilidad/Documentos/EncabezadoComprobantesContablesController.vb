﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Contabilidad.Documentos
Imports EncoExpres.GesCarga.Negocio.Contabilidad

''' <summary>
''' Controlador <see cref="EncabezadoComprobantesContablesController"/>
''' </summary>
<Authorize>
Public Class EncabezadoComprobantesContablesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoComprobantesContables) As Respuesta(Of IEnumerable(Of EncabezadoComprobantesContables))
        Return New LogicaEncabezadoComprobantesContables(CapaPersistenciaEncabezadoComprobantesContables).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoComprobantesContables) As Respuesta(Of EncabezadoComprobantesContables)
        Return New LogicaEncabezadoComprobantesContables(CapaPersistenciaEncabezadoComprobantesContables).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoComprobantesContables) As Respuesta(Of Long)
        Return New LogicaEncabezadoComprobantesContables(CapaPersistenciaEncabezadoComprobantesContables).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EncabezadoComprobantesContables) As Respuesta(Of Boolean)
        Return New LogicaEncabezadoComprobantesContables(CapaPersistenciaDetalleSeguimientoVehiculos).Anular(entidad)
    End Function
End Class
