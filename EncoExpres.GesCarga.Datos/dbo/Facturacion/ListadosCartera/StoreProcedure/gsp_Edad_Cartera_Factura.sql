﻿PRINT 'gsp_Edad_Cartera_Factura'
GO
DROP PROCEDURE gsp_Edad_Cartera_Factura
GO
CREATE PROCEDURE gsp_Edad_Cartera_Factura
(
	@par_EMPR_Codigo AS Smallint,
	@par_FechaInicial AS DATE = NULL,
	@par_FechaFinal AS DATE = NULL,
	@par_FechaCorte AS DATE = NULL,
	@par_TERC_Codigo AS NUMERIC = NULL
)
AS
BEGIN
	IF(@par_FechaCorte is NULL)
	BEGIN
		SET @par_FechaCorte = GETDATE()
	END

	SELECT
	ENFA.EMPR_Codigo,
	ENFA.Numero,
	ENFA.Numero_Documento,
	ENFA.Fecha,
	ENFA.Dias_Plazo,
	DATEADD(DAY, ISNULL(ENFA.Dias_Plazo, 0),CONVERT(DATE, ENFA.Fecha)) AS FechaVence,
	ENFA.Valor_Factura,
	ENFA.TERC_Cliente,                      
	ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'') As NombreCliente,
	CATA_TIID.Campo1 AS TipoIdentificacionCliente,
	CLIE.Numero_Identificacion AS IdentificacionCliente,
	CLIE.Telefonos AS TelefonosCliente,
	ENFA.OFIC_Factura,
	OFIC.Nombre AS NombreOficina,
	ISNULL(NFCR.Valor_Nota, 0) AS ValorNotaCredito,
	ISNULL(NFDE.Valor_Nota, 0) AS ValorNotaDebito,
	(ENFA.Valor_Factura + ISNULL(NFDE.Valor_Nota, 0) - ISNULL(NFCR.Valor_Nota, 0)) AS TotalFactura,
	ISNULL(DDCO.Fecha, '') AS ComprobanteFecha,
	ISNULL(EDCU.Codigo, 0) AS CuentaCodigo,
	ISNULL(DDCO.EDCO_Codigo, 0)  AS ComprobanteCodigo,
	ISNULL(DDCO.Valor_Pago_Recaudo, 0) AS CuentaValorRecaudado,
	DATEDIFF(dd, DATEADD(DAY, ISNULL(ENFA.Dias_Plazo, 0),CONVERT(DATE, ENFA.Fecha)), @par_FechaCorte) AS DiasMora,
	CEILING(CONVERT(NUMERIC, DATEDIFF(dd, DATEADD(DAY, ISNULL(ENFA.Dias_Plazo, 0),CONVERT(DATE, ENFA.Fecha)), @par_FechaCorte)) / 30) AS EdadCartera,
	(ENFA.Valor_Factura + ISNULL(NFDE.Valor_Nota, 0) - ISNULL(NFCR.Valor_Nota, 0)) - ISNULL(DDCO.Valor_Pago_Recaudo, 0) AS ValorCarteraFactura,
	@par_FechaCorte AS FechaCorte,
	IIF(
		DATEDIFF(dd, DATEADD(DAY, ISNULL(ENFA.Dias_Plazo, 0),CONVERT(DATE, ENFA.Fecha)), @par_FechaCorte) >= 1
		And DATEDIFF(dd, DATEADD(DAY, ISNULL(ENFA.Dias_Plazo, 0),CONVERT(DATE, ENFA.Fecha)), @par_FechaCorte) <= 30,
		(ENFA.Valor_Factura + ISNULL(NFDE.Valor_Nota, 0) - ISNULL(NFCR.Valor_Nota, 0)) - ISNULL(DDCO.Valor_Pago_Recaudo, 0), 0
	) AS UnoTreinta,
	IIF(
		DATEDIFF(dd, DATEADD(DAY, ISNULL(ENFA.Dias_Plazo, 0),CONVERT(DATE, ENFA.Fecha)), @par_FechaCorte) > 30
		And DATEDIFF(dd, DATEADD(DAY, ISNULL(ENFA.Dias_Plazo, 0),CONVERT(DATE, ENFA.Fecha)), @par_FechaCorte) <= 60,
		(ENFA.Valor_Factura + ISNULL(NFDE.Valor_Nota, 0) - ISNULL(NFCR.Valor_Nota, 0)) - ISNULL(DDCO.Valor_Pago_Recaudo, 0), 0
	) AS TreintaSesenta,
	IIF(
		DATEDIFF(dd, DATEADD(DAY, ISNULL(ENFA.Dias_Plazo, 0),CONVERT(DATE, ENFA.Fecha)), @par_FechaCorte) > 60
		And DATEDIFF(dd, DATEADD(DAY, ISNULL(ENFA.Dias_Plazo, 0),CONVERT(DATE, ENFA.Fecha)), @par_FechaCorte) <= 90,
		(ENFA.Valor_Factura + ISNULL(NFDE.Valor_Nota, 0) - ISNULL(NFCR.Valor_Nota, 0)) - ISNULL(DDCO.Valor_Pago_Recaudo, 0), 0
	) AS SesentaNoventa,
	IIF(
		DATEDIFF(dd, DATEADD(DAY, ISNULL(ENFA.Dias_Plazo, 0),CONVERT(DATE, ENFA.Fecha)), @par_FechaCorte) > 90,
		(ENFA.Valor_Factura + ISNULL(NFDE.Valor_Nota, 0) - ISNULL(NFCR.Valor_Nota, 0)) - ISNULL(DDCO.Valor_Pago_Recaudo, 0), 0
	) AS Noventa

	FROM Encabezado_Facturas ENFA

	--Informacion Tercero
	LEFT JOIN Terceros AS CLIE ON
	ENFA.EMPR_Codigo = CLIE.EMPR_Codigo
	AND ENFA.TERC_Cliente = CLIE.Codigo
	--Informacion Tercero

	--Informacion Tipo Identificacion
	LEFT JOIN Valor_Catalogos AS CATA_TIID ON
	CLIE.EMPR_Codigo = CATA_TIID.EMPR_Codigo
	AND CLIE.CATA_TIID_Codigo = CATA_TIID.Codigo
	--Informacion Tipo Identificacion

	--Informacion Oficina
	LEFT JOIN Oficinas AS OFIC ON
	ENFA.EMPR_Codigo = OFIC.EMPR_Codigo
	AND ENFA.OFIC_Factura = OFIC.Codigo
	--Informacion Oficina

	--Informacion Cuenta X Cobrar
	LEFT JOIN Encabezado_Documento_Cuentas EDCU ON
	ENFA.EMPR_Codigo = EDCU.EMPR_Codigo
	AND ENFA.Numero = EDCU.Codigo_Documento_Origen
	AND EDCU.tido_codigo = 80--Cuenta x cobrar
	--Informacion Cuenta X Cobrar

	--Detalle Informacion Cuenta X Cobrar
	LEFT JOIN (
		--SELECT EMPR_Codigo, ENDC_Codigo, SUM(Valor_Pago_Recaudo) AS Valor_Pago_Recaudo, MAX(EDCO_Codigo) AS EDCO_Codigo
		--FROM Detalle_Documento_Cuenta_Comprobantes
		--WHERE EMPR_Codigo = @par_EMPR_Codigo
		--GROUP BY EMPR_Codigo, ENDC_Codigo
		SELECT EMPR_Codigo, MAX(Fecha) Fecha, MAX(EDCO_Codigo) AS EDCO_Codigo, ENDC_Codigo, SUM(Valor_Pago_Recaudo) AS Valor_Pago_Recaudo
		FROM V_Detalle_Documento_Cuenta_Comprobantes_Facturas
		WHERE EMPR_Codigo = @par_EMPR_Codigo and Fecha <= @par_FechaCorte

		GROUP BY EMPR_Codigo, ENDC_Codigo

	) DDCO ON
		EDCU.EMPR_Codigo = DDCO.EMPR_Codigo
		AND EDCU.Codigo = DDCO.ENDC_Codigo
	--Detalle Informacion Cuenta X Cobrar

	--Informacion Nota Credito
	LEFT JOIN (
		SELECT EMPR_Codigo, ENFA_Numero, TIDO_Codigo, SUM(Valor_Nota) AS Valor_Nota
		FROM Encabezado_Notas_Facturas
		WHERE EMPR_Codigo = @par_EMPR_Codigo
		GROUP BY TIDO_Codigo, ENFA_Numero, EMPR_Codigo
	) AS NFCR ON
	ENFA.EMPR_Codigo = NFCR.EMPR_Codigo
	AND ENFA.Numero = NFCR.ENFA_Numero
	AND NFCR.TIDO_Codigo = 190--Nota Credito
	--Informacion Nota Credito

	--Informacion Nota Debito
	LEFT JOIN (
		SELECT EMPR_Codigo, ENFA_Numero, TIDO_Codigo, SUM(Valor_Nota) AS Valor_Nota
		FROM Encabezado_Notas_Facturas
		WHERE EMPR_Codigo = @par_EMPR_Codigo
		GROUP BY TIDO_Codigo, ENFA_Numero, EMPR_Codigo
	) AS NFDE ON
	ENFA.EMPR_Codigo = NFDE.EMPR_Codigo
	AND ENFA.Numero = NFDE.ENFA_Numero
	AND NFDE.TIDO_Codigo = 195--Nota Debito
	--Informacion Nota Debito

	--Comprobante Ingreso
	--LEFT JOIN Encabezado_Documento_Comprobantes ENDC ON
	--DDCO.EMPR_Codigo = ENDC.EMPR_Codigo
	--AND DDCO.EDCO_Codigo = ENDC.Codigo

	--AND ENDC.CATA_DOOR_Codigo = 2601 --Documento Origen Factura
	--AND ENDC.TIDO_Codigo = 40 --Comprobante Ingreso
	--AND ENDC.Estado = 1
	--AND ENDC.Anulado = 0
	--Comprobante Ingreso

	WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo
	AND (ENFA.Fecha >= @par_FechaInicial OR @par_FechaInicial IS NULL)                      
	AND (ENFA.Fecha <= @par_FechaFinal OR @par_FechaFinal IS NULL)
	AND (ENFA.TERC_Cliente = @par_TERC_Codigo OR @par_TERC_Codigo IS NULL)  
	AND ENFA.Estado = 1
	AND ENFA.Anulado = 0                    

END
GO