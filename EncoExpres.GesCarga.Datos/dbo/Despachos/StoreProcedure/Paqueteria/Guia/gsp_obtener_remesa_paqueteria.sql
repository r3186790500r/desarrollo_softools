﻿PRINT 'gsp_obtener_remesa_paqueteria'
GO
DROP PROCEDURE gsp_obtener_remesa_paqueteria
GO
CREATE PROCEDURE gsp_obtener_remesa_paqueteria 
(          
@par_EMPR_Codigo smallint,          
@par_Numero Numeric         
)          
AS           
BEGIN          
select         
1 as Obtener,      
ENRE.EMPR_Codigo,        
ENRE.Fecha,        
ENRE.Numero,        
ENRE.Numero_Documento,        
ENRE.Documento_Cliente,        
ENRE.Observaciones,        
ENRE.Fecha_Documento_Cliente,        
ENRE.CATA_FOPR_Codigo,        
ENRE.TERC_Codigo_Cliente,        
ENRE.DTCV_Codigo,        
ENRE.PRTR_Codigo,        
ENRE.Peso_Cliente,        
ENRE.Cantidad_Cliente,        
ENRE.Valor_Comercial_Cliente,        
ENRE.Peso_Volumetrico_Cliente,        
ENPR.CATA_TERP_Codigo,        
ENPR.CATA_TTRP_Codigo,        
ENPR.CATA_TDRP_Codigo,        
ENPR.CATA_TIRP_Codigo,        
ENPR.CATA_TPRP_Codigo,        
ENPR.CATA_TSRP_Codigo,        
      
ENRE.TERC_Codigo_Remitente,        
ENRE.CIUD_Codigo_Remitente,        
ENRE.Direccion_Remitente,        
ENRE.Telefonos_Remitente,        
ENRE.TERC_Codigo_Destinatario,        
ENRE.CIUD_Codigo_Destinatario,        
ENRE.Direccion_Destinatario,        
ENRE.Telefonos_Destinatario,        
ENRE.Valor_Flete_Cliente,        
ENRE.Valor_Manejo_Cliente,        
ENRE.Valor_Seguro_Cliente,        
ENRE.Valor_Descuento_Cliente,        
ENRE.Total_Flete_Cliente,        
ENRE.Total_Flete_Transportador,        
ENRE.Estado,      
ISNULL(ENPR.OFIC_Codigo_Origen,0) AS OFIC_Codigo_Origen, 
ISNULL(OFOR.Nombre, '') AS NombreOficinaOrigen,  
ISNULL(DTCV.ETCV_Numero,0) As ETCV_Numero,      
ISNULL(DTCV.LNTC_Codigo,0) As LNTC_Codigo,        
ISNULL(DTCV.TLNC_Codigo,0) As TLNC_Codigo,      
ISNULL(DTCV.RUTA_Codigo,0) As RUTA_Codigo,      
ISNULL(DTCV.TATC_Codigo,0) As TATC_Codigo,      
ISNULL(TTTC.NombreTarifa,'') As NombreTarifa,  
ISNULL(DTCV.TTTC_Codigo,0) As TTTC_Codigo,      
ISNULL(TTTC.Nombre,'') As NombreTipoTarifa,  
  
ENPR.Fecha_Interfaz_Cargue_Archivo,    
ENPR.Fecha_Interfaz_Cargue_WMS,      
isnull(ENRE.VEHI_Codigo,0) as VEHI_Codigo,      
ISNULL(ENRE.SEMI_Codigo,0) As SEMI_Codigo,    
  
ENRE.CATA_TIRE_Codigo,   
ISNULL(ENPR.Reexpedicion,0) As Reexpedicion,  
ENPR.CATA_ESRP_Codigo,  
REMI.CATA_TIID_Codigo As TIID_Remitente,  
REMI.Numero_Identificacion As Identificacion_Remitente,  
RTRIM(LTRIM(ISNULL(REMI.Razon_Social,'') + ISNULL(REMI.Nombre,'') + ' ' + ISNULL(REMI.Apellido1, '') + ' ' + ISNULL(REMI.Apellido2,''))) As NombreRemitente,  
ISNULL(ENRE.Barrio_Remitente,'') As Barrio_Remitente,  
ISNULL(ENRE.Codigo_Postal_Remitente,'') As Codigo_Postal_Remitente,  
ISNULL(ENPR.Observaciones_Remitente,'') As Observaciones_Remitente,  
  
DEST.CATA_TIID_Codigo As TIID_Destinatario,  
DEST.Numero_Identificacion As Identificacion_Destinatario,  
LTRIM(RTRIM(ISNULL(DEST.Razon_Social,'') + ISNULL(DEST.Nombre,'') + ' ' + ISNULL(DEST.Apellido1, '') + ' ' + ISNULL(DEST.Apellido2,''))) As NombreDestinatario,  
ISNULL(ENRE.Barrio_Destinatario,'') As Barrio_Destinatario,  
ISNULL(ENRE.Codigo_Postal_Destinatario, '') As Codigo_Postal_Destinatario,  
ISNULL(ENPR.Observaciones_Destinatario,'') As Observaciones_Destinatario,  
ISNULL(ENPR.Descripcion_Mercancia,'') As Descripcion_Mercancia,  
ISNULL(ENPR.OFIC_Codigo_Origen,0) As OFIC_Codigo_Origen,  
ISNULL(ENPR.OFIC_Codigo_Destino,0) As OFIC_Codigo_Destino,  
ISNULL(ENPR.OFIC_Codigo_Actual,0) As OFIC_Codigo_Actual,  
ISNULL(OFOR.Nombre,'') As NombreOficinaOrigen,  
ISNULL(OFDE.Nombre,'') As NombreOficinaDestino,  
ENPR.CATA_ESRP_Codigo,  
ESRP.Nombre As NombreEstadoRemesa,  
ENRE.ENPD_Numero,  
ISNULL(VEHI.Placa,'') As VEHI_Placa,  
ISNULL(COND.Razon_Social,'') + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,  
ENRE.OFIC_Codigo,  
OFIC.Nombre As NombreOficina  
  
from         
Remesas_Paqueteria as ENPR        
        
INNER JOIN Encabezado_Remesas AS ENRE        
ON ENPR.EMPR_Codigo = ENRE.EMPR_Codigo        
AND ENPR.ENRE_Numero = ENRE.Numero        
    
  INNER JOIN Terceros As REMI ON  
 ENRE.EMPR_Codigo = REMI.EMPR_Codigo  
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo  
   
 INNER JOIN Terceros As DEST ON  
 ENRE.EMPR_Codigo = DEST.EMPR_Codigo  
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo     
 INNER JOIN V_Estado_Remesa_Paqueteria AS ESRP ON  
 ENPR.EMPR_Codigo = ESRP.EMPR_Codigo  
 AND ENPR.CATA_ESRP_Codigo = ESRP.Codigo  
  
 INNER JOIn Oficinas AS OFIC ON  
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo  
 AND ENRE.OFIC_Codigo = OFIC.Codigo  
  
 LEFT JOIN Oficinas As OFOR ON  
 ENPR.EMPR_Codigo = OFOR.EMPR_Codigo  
 AND ENPR.OFIC_Codigo_Origen = OFOR.Codigo  
  
 LEFT JOIN Oficinas As OFDE ON  
 ENPR.EMPR_Codigo = OFDE.EMPR_Codigo  
 AND ENPR.OFIC_Codigo_Destino = OFDE.Codigo  
  
       
LEFT JOIN Detalle_Tarifario_Carga_Ventas AS DTCV        
ON ENPR.EMPR_Codigo = DTCV.EMPR_Codigo        
AND ENRE.DTCV_Codigo = DTCV.Codigo        
AND ENRE.EMPR_Codigo = DTCV.EMPR_Codigo        
--AND ENRE.ETCV_Numero = DTCV.ETCV_Numero        
   
 INNER JOIN V_Tipo_Tarifa_Transporte_Carga As TTTC ON  
 DTCV.EMPR_Codigo = TTTC.EMPR_Codigo  
 AND DTCV.TATC_Codigo = TTTC.TATC_Codigo  
 AND DTCV.TTTC_Codigo = TTTC.Codigo  
  
 LEFT JOIN Vehiculos AS VEHI ON  
 ENRE.EMPR_Codigo = VEHI.EMPR_Codigo  
 AND ENRE.VEHI_Codigo = VEHI.Codigo  
  
 LEFT JOIN Terceros As COND ON  
 ENRE.EMPR_Codigo = COND.EMPR_Codigo  
 AND ENRE.TERC_Codigo_Conductor = COND.Codigo  
   
WHERE      
      
 ENRE.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENRE.Numero = @par_Numero      
 AND ENRE.TIDO_Codigo = 110 --> Guía      
END  
GO 