﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Seguridad
Imports EncoExpres.GesCarga.Repositorio.Seguridad

Namespace Seguridad
    Public NotInheritable Class PersistenciaEntidadOrigenEstudioSeguridad
        Implements IPersistenciaBase(Of EntidadOrigenEstudioSeguridad)
        Public Function Consultar(filtro As EntidadOrigenEstudioSeguridad) As IEnumerable(Of EntidadOrigenEstudioSeguridad) Implements IPersistenciaBase(Of EntidadOrigenEstudioSeguridad).Consultar
            Return New RepositorioEntidadOrigenEstudioSeguridad().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EntidadOrigenEstudioSeguridad) As Long Implements IPersistenciaBase(Of EntidadOrigenEstudioSeguridad).Insertar
            Return New RepositorioEntidadOrigenEstudioSeguridad().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EntidadOrigenEstudioSeguridad) As Long Implements IPersistenciaBase(Of EntidadOrigenEstudioSeguridad).Modificar
            Return New RepositorioEntidadOrigenEstudioSeguridad().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EntidadOrigenEstudioSeguridad) As EntidadOrigenEstudioSeguridad Implements IPersistenciaBase(Of EntidadOrigenEstudioSeguridad).Obtener
            Return New RepositorioEntidadOrigenEstudioSeguridad().Obtener(filtro)
        End Function

    End Class
End Namespace
