﻿PRINT 'gsp_insertar_puesto_controles'
GO
DROP PROCEDURE gsp_insertar_puesto_controles
GO
CREATE PROCEDURE gsp_insertar_puesto_controles
(
@par_EMPR_Codigo	smallint,
@par_Nombre	varchar(100) =NULL,
@par_Codigo_Alterno	varchar(20)  =NULL,
@par_TERC_Codigo_Proveedor	numeric  =NULL,
@par_Contacto	varchar(100) =NULL,
@par_Telefono	varchar(30) =NULL,
@par_Celular	varchar(30) =NULL,
@par_Ubicacion	varchar(300) =NULL,
@par_Estado	smallint,
@par_USUA_Codigo_Crea	smallint
)
AS BEGIN
declare @codigo as numeric =(select ISNULL(max(Codigo),1) from Puesto_Controles WHERE EMPR_Codigo = @par_EMPR_Codigo)
INSERT INTO Puesto_Controles
           (EMPR_Codigo
           ,Codigo
           ,Nombre
           ,Codigo_Alterno
           ,TERC_Codigo_Proveedor
           ,Contacto
           ,Telefono
           ,Celular
           ,Ubicacion
		   ,Estado
           ,Fecha_Crea
           ,USUA_Codigo_Crea
           )
     VALUES
           (@par_EMPR_Codigo
           ,@codigo
           ,ISNULL(@par_Nombre,'')
           ,@par_Codigo_Alterno
           ,ISNULL(@par_TERC_Codigo_Proveedor,0)
           ,ISNULL(@par_Contacto,'')
           ,ISNULL(@par_Telefono,'')
           ,ISNULL(@par_Celular,'')
           ,ISNULL(@par_Ubicacion,'')
		   ,@par_Estado
           ,GETDATE()
           ,@par_USUA_Codigo_Crea)

		   SELECT @codigo as Codigo
END
GO
