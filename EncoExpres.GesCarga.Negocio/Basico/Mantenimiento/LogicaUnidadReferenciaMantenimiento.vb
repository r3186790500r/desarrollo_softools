﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Mantenimiento


    Public NotInheritable Class LogicaUnidadReferenciaMantenimiento
        Inherits LogicaBase(Of UnidadReferenciaMantenimiento)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of UnidadReferenciaMantenimiento)

        Sub New(persistencia As IPersistenciaBase(Of UnidadReferenciaMantenimiento))
            _persistencia = persistencia

        End Sub

        Public Overrides Function Consultar(filtro As UnidadReferenciaMantenimiento) As Respuesta(Of IEnumerable(Of UnidadReferenciaMantenimiento))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of UnidadReferenciaMantenimiento))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of UnidadReferenciaMantenimiento))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As UnidadReferenciaMantenimiento) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If


            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As UnidadReferenciaMantenimiento) As Respuesta(Of UnidadReferenciaMantenimiento)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of UnidadReferenciaMantenimiento)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of UnidadReferenciaMantenimiento)(consulta)
        End Function
        Private Function DatosRequeridos(entidad As UnidadReferenciaMantenimiento) As Respuesta(Of UnidadReferenciaMantenimiento)
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
