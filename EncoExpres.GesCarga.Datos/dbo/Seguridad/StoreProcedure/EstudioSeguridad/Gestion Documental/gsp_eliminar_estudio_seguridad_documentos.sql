﻿PRINT 'gsp_eliminar_estudio_seguridad_documentos'
GO
DROP PROCEDURE gsp_eliminar_estudio_seguridad_documentos
GO
CREATE PROCEDURE gsp_eliminar_estudio_seguridad_documentos 
(      
@par_EMPR_Codigo SMALLINT,      
@par_TDES_Codigo NUMERIC,  
@par_Numero NUMERIC    
)      
AS      
 BEGIN      
   DELETE Estudio_Seguridad_Documentos       
   WHERE       
   EMPR_Codigo =  @par_EMPR_Codigo      
   AND TDES_Codigo = @par_TDES_Codigo      
   AND ENES_Numero = @par_Numero     
       
 SELECT Codigo = @par_TDES_Codigo      
END      
GO