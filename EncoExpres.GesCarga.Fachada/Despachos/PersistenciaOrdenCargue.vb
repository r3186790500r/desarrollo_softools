﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Despachos.OrdenCargue
Imports EncoExpres.GesCarga.Repositorio.Despachos.Masivo

Namespace Despachos
    Public NotInheritable Class PersistenciaOrdenCargue
        Implements IPersistenciaBase(Of OrdenCargue)

        Public Function Consultar(filtro As OrdenCargue) As IEnumerable(Of OrdenCargue) Implements IPersistenciaBase(Of OrdenCargue).Consultar
            Return New RepositorioOrdenCargue().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As OrdenCargue) As Long Implements IPersistenciaBase(Of OrdenCargue).Insertar
            Return New RepositorioOrdenCargue().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As OrdenCargue) As Long Implements IPersistenciaBase(Of OrdenCargue).Modificar
            Return New RepositorioOrdenCargue().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As OrdenCargue) As OrdenCargue Implements IPersistenciaBase(Of OrdenCargue).Obtener
            Return New RepositorioOrdenCargue().Obtener(filtro)
        End Function
        Public Function Anular(entidad As OrdenCargue) As Boolean
            Return New RepositorioOrdenCargue().Anular(entidad)
        End Function
        Public Function ActualizarCargueDescargue(entidad As OrdenCargue) As Long
            Return New RepositorioOrdenCargue().ActualizarCargueDescargue(entidad)
        End Function
    End Class
End Namespace