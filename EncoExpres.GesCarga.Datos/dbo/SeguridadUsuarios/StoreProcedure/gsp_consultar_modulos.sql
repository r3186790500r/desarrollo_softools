﻿PRINT 'gsp_consultar_modulos'
GO
DROP PROCEDURE gsp_consultar_modulos
GO
CREATE PROCEDURE  gsp_consultar_modulos
(
@par_EMPR_Codigo SMALLINT 
)
AS 
BEGIN
SELECT 
	EMPR_Codigo,
	Codigo,
	Nombre,
	Estado,
	CATA_APLI_Codigo,
	Orden_Menu As Orden,
	Mostrar_Menu,
	ISNULL(Imagen,'') Imagen

FROM Modulo_Aplicaciones 

WHERE EMPR_Codigo = @par_EMPR_Codigo
AND Estado = 1 

ORDER BY Orden
END
GO