﻿Imports Newtonsoft.Json
Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="EmpresaControles"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EmpresaControles
        Inherits BaseDocumento
        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="EmpresaControles"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EmpresaControles"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "Codigo_Empresa")
            Codigo = Read(lector, "Codigo")
            CodigoMenu = Read(lector, "MEAP_Codigo")
            Control = Read(lector, "Control")
            Visible = Read(lector, "Visible")
            Obligatorio = Read(lector, "Obligatorio")
            TextoLabel = Read(lector, "Texto_Label")
            LongitudTexto = Read(lector, "Logitud_Texto")

        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Codigo As Integer
        <JsonProperty>
        Public Property CodigoMenu As Integer
        <JsonProperty>
        Public Property Control As String
        <JsonProperty>
        Public Property Visible As Integer
        <JsonProperty>
        Public Property Obligatorio As Integer
        <JsonProperty>
        Public Property TextoLabel As String
        <JsonProperty>
        Public Property LongitudTexto As String

    End Class
End Namespace
