﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="MarcaVehiculosController"/>
''' </summary>
<Authorize>
Public Class MarcaVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As MarcaVehiculos) As Respuesta(Of IEnumerable(Of MarcaVehiculos))
        Return New LogicaMarcaVehiculos(CapaPersistenciaMarcaVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As MarcaVehiculos) As Respuesta(Of MarcaVehiculos)
        Return New LogicaMarcaVehiculos(CapaPersistenciaMarcaVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As MarcaVehiculos) As Respuesta(Of Long)
        Return New LogicaMarcaVehiculos(CapaPersistenciaMarcaVehiculos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As MarcaVehiculos) As Respuesta(Of Boolean)
        Return New LogicaMarcaVehiculos(CapaPersistenciaMarcaVehiculos).Anular(entidad)
    End Function
End Class
