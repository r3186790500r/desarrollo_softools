﻿Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Basico.ServicioCliente
    Public NotInheritable Class LogicaLineaNegocioTransporteCarga
        Inherits LogicaBase(Of LineaNegocioTransporteCarga)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of LineaNegocioTransporteCarga)

        Sub New(persistencia As IPersistenciaBase(Of LineaNegocioTransporteCarga))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As LineaNegocioTransporteCarga) As Respuesta(Of IEnumerable(Of LineaNegocioTransporteCarga))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of LineaNegocioTransporteCarga))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of LineaNegocioTransporteCarga))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As LineaNegocioTransporteCarga) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As LineaNegocioTransporteCarga) As Respuesta(Of LineaNegocioTransporteCarga)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of LineaNegocioTransporteCarga)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of LineaNegocioTransporteCarga)(consulta)
        End Function

        Private Function DatosRequeridos(entidad As LineaNegocioTransporteCarga) As Respuesta(Of LineaNegocioTransporteCarga)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of LineaNegocioTransporteCarga) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of LineaNegocioTransporteCarga) With {.ProcesoExitoso = True}

        End Function
    End Class

End Namespace