﻿EncoExpresApp.controller("GestionarSistemasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'SistemasFactory', 'LineaMantenimientoFactory', 'ValorCatalogosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, SistemasFactory, LineaMantenimientoFactory, ValorCatalogosFactory) {

        $scope.Titulo = 'GESTIONAR SISTEMAS DE MANTENIMIENTO';
        $scope.ListadoLineaMantenimiento = [];
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SISTEMAS);

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Manteniminento' }, { Nombre: 'Sistemas' }];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            Estado:0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoAlterno : ''
        };
        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            
            $scope.Modelo.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Codigo = 0;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if (parseInt($scope.Modelo.Codigo) > 0) { 
                document.location.href = '#!ConsultarSistemas/' + $scope.Modelo.Codigo; 
            }
            else {
                document.location.href = '#!ConsultarSistemas';
            }
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar el combo de estados*/ 
        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0];

        /*Cargar el combo Linea Mantenimiento*/
        LineaMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoLineaMantenimiento = response.data.Datos
                    $scope.ListadoLineaMantenimiento.push({ Nombre: '(TODOS)', Codigo: -1 })
                    $scope.LineaMantenimiento = $linq.Enumerable().From($scope.ListadoLineaMantenimiento).First('$.Codigo == -1');
                } else {

                    $scope.ListadoLineaMantenimiento = [];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR SISTEMA DE MANTENIMIENTO';
            $scope.Deshabilitar = true;
            Obtener();
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando Sistema de mantenimiento ' + $scope.Modelo.Codigo); 
            $timeout(function () {
                blockUI.message('Cargando Sistema de mantenimiento ' + $scope.Modelo.Codigo);
            }, 200); 
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            }; 
            blockUI.delay = 1000;
            SistemasFactory.Obtener(filtros).
                then(function (response) { 
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Nombre = response.data.Datos.Nombre
                        $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno
                        $scope.LineaMantenimiento = $linq.Enumerable().From($scope.ListadoLineaMantenimiento).First('$.Codigo ==' + response.data.Datos.LineaMantenimiento.Codigo);
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado.Codigo);  
                    }
                    else {
                        ShowError('No se logro consultar el Sistema de mantenimiento  No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarSistemas';
                    }

        

                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar el sistema de mantenimineto No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarSistemas';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardarSistema = function () {
            showModal('modalConfirmacionGuardarSistema');
        };
        // Metodo para guardar /modificar
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarSistema'); 
            $scope.Modelo.Nombre = $scope.Modelo.Nombre;
            $scope.Modelo.LineaMantenimiento = $scope.LineaMantenimiento;
            if (DatosRequeridos()) {
                SistemasFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó la línea ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarSistemas/' + response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó la línea ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarSistemas/' + response.data.Datos;
                                }

                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };



        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == "") {
                $scope.MensajesError.push('Debe ingresar el nombre del sistema de mantenimiento');
                continuar = false;
            }

           

            if ($scope.Modelo.LineaMantenimiento == undefined || $scope.Modelo.LineaMantenimiento.Codigo == -1 || $scope.Modelo.LineaMantenimiento == null) {
                $scope.MensajesError.push('Debe ingresar la Línea  de Mantenimiento');
                continuar = false;
            }

            if ($scope.LineaMantenimiento == undefined || $scope.LineaMantenimiento == "" || $scope.LineaMantenimiento == null) {
                $scope.MensajesError.push('Debe ingresar la línea de mantenimiento ');
                continuar = false;
            } 
          

            return continuar;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.MaskMayus = function (option) {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        };


    }]);