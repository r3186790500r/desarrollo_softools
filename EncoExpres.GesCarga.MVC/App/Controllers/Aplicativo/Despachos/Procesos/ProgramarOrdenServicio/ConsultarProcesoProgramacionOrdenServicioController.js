﻿EncoExpresApp.controller("ConsultarProcesoProgramacionOrdenServicioCtrl", ['$scope', '$timeout', 'EncabezadoSolicitudOrdenServiciosFactory', '$linq', 'blockUI', 'blockUIConfig',
    '$routeParams', 'TercerosFactory', 'ValorCatalogosFactory', 'EncabezadoProgramacionOrdenServiciosFactory', 'EmpresasFactory', 'OficinasFactory', 'ManifiestoFactory','AutorizacionesFactory',
    function ($scope, $timeout, EncabezadoSolicitudOrdenServiciosFactory, $linq, blockUI, blockUIConfig,
        $routeParams, TercerosFactory, ValorCatalogosFactory, EncabezadoProgramacionOrdenServiciosFactory, EmpresasFactory, OficinasFactory, ManifiestoFactory, AutorizacionesFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Procesos' }, { Nombre: 'Despachar Programación' }];
        $scope.Titulo = "DESPACHAR PROGRAMACIÓN";
        // ----------------------------- Decalaracion Variables -----------------------------//
        var filtros = {};
        var GestionarSolicitudServicio = 1;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.ListadoOrdenServicios = [];
        $scope.MostrarMensajeError = false
        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        $scope.fechaBloqueo = new Date(new Date().setSeconds(172800));
        $scope.ListadoEstadoSolicitud = [
            { Codigo: 0, Nombre: '(TODAS)' },
            { Codigo: 2, Nombre: 'BACKORDER' },
            { Codigo: 3, Nombre: 'PENDIENTE' },
            { Codigo: 1, Nombre: 'PENDIENTE HOY' },
            { Codigo: 6, Nombre: 'ASIGNADO' },
            { Codigo: 4, Nombre: 'MAÑANA' },
            { Codigo: 7, Nombre: 'DESPACHADO' },
            { Codigo: 5, Nombre: 'PROYECTADO' }
        ]
        $scope.ModalEstadoSolicitus = $linq.Enumerable().From($scope.ListadoEstadoSolicitud).First('$.Codigo == ' + CERO);
        //$scope.ListadoEstadoSolicitud.push({ Codigo: 0, Nombre: '(TODAS)' });

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_DESPCHAR_PROGRAMACION_ORDEN_SERVICIOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        //$scope.ModeloOficina = { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo }

        $scope.ListadoPermisosEspecificos = []
        $scope.ListadoPermisos.forEach(function (item) {
            if (item.Padre == OPCION_MENU_DESPCHAR_PROGRAMACION_ORDEN_SERVICIOS) {
                $scope.ListadoPermisosEspecificos.push(item)
            }
        });
        $scope.PermisoVisualizarOficinas = false

        $scope.ListadoPermisosEspecificos.forEach(function (item) {
            if (item.Codigo == PERMISO_VISUALIZAR_OFICINAS_PROGRAMACION) {
                $scope.PermisoVisualizarOficinas = true
            }

        })
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.OpcionFormatoGastos = true;
        if ($scope.Sesion.UsuarioAutenticado.CodigoEmpresa != 6) {
            $scope.OpcionFormatoGastos = false;
        }
        // ----------------------------- Decalaracion Variables -----------------------------//
        //------Funcion Paginacion E Imprimir
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        //------Funcion Paginacion E Imprimir
        //-- Consulta OFICINAS
        $scope.ListadoOficinas = []
        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.ModeloOficina = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }
        //estados 
        //ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 186 } }).
        //    then(function (response) {
        //        if (response.data.ProcesoExitoso === true) {
        //            $scope.ListadoEstadoSolicitud = [];
        //            if (response.data.Datos.length > 0) {
        //                $scope.ListadoEstadoSolicitud.push({ Codigo: 0, Nombre: '(TODAS)' });
        //                for (var i = 0; i < response.data.Datos.length; i++) {
        //                    $scope.ListadoEstadoSolicitud.push(response.data.Datos[i]);
        //                }
        //                $scope.ModalEstadoSolicitus = $linq.Enumerable().From($scope.ListadoEstadoSolicitud).First('$.Codigo == ' + CERO);
        //            }
        //            else {
        //                $scope.ListadoEstadoSolicitud = []
        //            }
        //        }
        //    }, function (response) {
        //    });

        //------Funciones
        $scope.Buscar = function () {
            $scope.ID_Detalle = 0
            $scope.ListadoOrdenServicios = [];
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    $scope.ConsultaInicial = 0
                    $scope.Codigo = 0
                    Find();
                }
            }
        };
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarProcesoPorgrmacionOrdenServicio';
            }
        };
        if ($scope.Sesion.UsuarioAutenticado.ManejoValorDeclarado) {
            var ResponseAutorizaciones = AutorizacionesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoAutorizacion: { Codigo: 18407 /*Autorizar Mayor Valor Declarado*/ }, Sync: true }).Datos;
        }
        function Find(ConsultaInicial) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Buscando registros..."); }, 100);

            $scope.Buscando = true;
            $scope.ListadoOrdenServicios = [];
            $scope.MensajesError = [];
            if ($scope.Buscando) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.ModeloNumero,
                    NumeroOrden: $scope.ModeloNumeroOrdenServicio,
                    FechaInicial: $scope.ID_Detalle > 0 ? null : $scope.ModeloFechaInicial,
                    FechaFinal: $scope.ID_Detalle > 0 ? null : $scope.ModeloFechaFinal,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_SOLICITUD_ORDEN_SERVICIO,
                    Estado: $scope.Estado.Codigo,
                    Oficina: ($scope.ID_Detalle > 0 ? {} : $scope.ModeloOficina),
                    Cliente: $scope.Cliente,
                    Pagina: $scope.paginaActual,
                    EstadoProgramacion: { Codigo: $scope.ModalEstadoSolicitus.Codigo },
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    PendienteGenerar: 1,
                    ConsultaDetalle: 1,
                    ID_Detalle: $scope.ID_Detalle,
                    ConsultaInicial: $scope.ConsultaInicial,
                    UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
                }
                EncabezadoProgramacionOrdenServiciosFactory.Consultar(filtros).
                    then(function (response) {
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                if (response.data.Datos[0].Detalle.length > 0) {

                                    

                                    $scope.ListadoOrdenServicios = response.data.Datos[0].Detalle
                                    for (var i = 0; i < $scope.ListadoOrdenServicios.length; i++) {
                                        $scope.ListadoOrdenServicios[i].FechaCompara = new Date($scope.ListadoOrdenServicios[i].FechaCargue)
                                        if ($scope.Sesion.UsuarioAutenticado.ManejoValorDeclarado) {
                                            ResponseAutorizaciones.forEach(itemAutorizacion => {
                                                if (itemAutorizacion.NumeroDocumento == $scope.ListadoOrdenServicios[i].Codigo) {
                                                    $scope.ListadoOrdenServicios[i].VerAutorizacion = 1;
                                                }
                                            });
                                        }
                                    }
                                    $scope.totalRegistros = $scope.ListadoOrdenServicios[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                } else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        ShowError(response.stuatusText);
                    });
            }
        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '') &&
                ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '') &&
                ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true) &&
                ($scope.ModeloNumeroOrdenServicio === null || $scope.ModeloNumeroOrdenServicio === undefined || $scope.ModeloNumeroOrdenServicio === '' || $scope.ModeloNumeroOrdenServicio === 0 || isNaN($scope.ModeloNumeroOrdenServicio) === true)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;
            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0) ||
                ($scope.ModeloNumeroOrdenServicio !== null && $scope.ModeloNumeroOrdenServicio !== undefined && $scope.ModeloNumeroOrdenServicio !== '' && $scope.ModeloNumeroOrdenServicio !== 0) ||
                ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '') ||
                ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '') &&
                    ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial;
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal;
                    }
                }
            }
            return continuar;
        }

        $scope.MaskMayus = function () {
            try { $scope.Cliente = $scope.Cliente.toUpperCase() } catch (e) { }
        };

        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            $scope.ID_Detalle = $routeParams.Numero;
            $scope.Cliente = { NombreCompleto: '', Codigo: 0 }
            //Find();
        }
        $scope.ConsultaInicial = 1
        Find();

        $scope.DesplegarDespacharViaje = function (NumeroProgramacion, IDCodigo, CodigoDetalle, NumeroOrden) {
            $scope.NumeroOrden = NumeroOrden
            $scope.NumeroProgramacion = NumeroProgramacion
            $scope.IDCodigo = IDCodigo
            $scope.CodigoDetalle = CodigoDetalle
            if (IDCodigo > 0) {
                document.location.href = '#!GestionarDespacharProgramacionOrdenServicios/' + $scope.NumeroOrden + '/' + IDCodigo + '/' + $scope.NumeroProgramacion + '/' + CodigoDetalle;;
            } else {
                EncabezadoProgramacionOrdenServiciosFactory.Guardar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: NumeroProgramacion,
                    IDdetalle: CodigoDetalle
                }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                document.location.href = '#!GestionarDespacharProgramacionOrdenServicios/' + $scope.NumeroOrden + '/' + response.data.Datos + '/' + $scope.NumeroProgramacion + '/' + $scope.CodigoDetalle;;
                                //document.location.href = '#!GestionarDespacharProgramacionOrdenServicios/' + $scope.Modelo.OrdenServicio.Numero + '/' + response.data.Datos;
                            }
                            else {
                                ShowError('Este detalle ya se encuentra en Proceso de despacho');
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }

        }
        $scope.DesplegarInformeOrdenServicio = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_ORDEN_SERVICIO;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref + '&TipoDocumento=' + CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref + '&TipoDocumento=' + CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.DesplegarInformeOrdenCargue = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_ORCA;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.DesplegarInformeRemesa = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_REME;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.DesplegarInformePlanillaDespacho = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_PLDC;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.DesplegarInformeManifiesto = function (Numero, OpcionPDf, OpcionEXCEL, NumeroDocumento) {

            $scope.urlASP = '';
            $scope.Numero = Numero;
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = NumeroDocumento;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Planilla: {TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO},
                UsuarioCrea: $scope.Sesion.UsuarioAutenticado.Codigo,
                UsuarioModifica: $scope.Sesion.UsuarioAutenticado.Codigo,
                NumeroDocumento: $scope.NumeroDocumento,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas
            };
            var imgQRCode;
            ManifiestoFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        if (response.data.Datos.length > 0) {

                            imgQRCode = qrcodeGenerator(JsObjToString({
                                NumeroDocumento: response.data.Datos[0].NumeroDocumento,
                                Placa: response.data.Datos[0].Vehiculo.Placa,
                                Remolque: response.data.Datos[0].Semirremolque.Placa,
                                Orig: response.data.Datos[0].NombreCiudadOrigen,
                                Dest: response.data.Datos[0].NombreCiudadDestino,
                                Mercancia: response.data.Datos[0].NombreProductotrans,
                                Conductor: response.data.Datos[0].Conductor.NombreCompleto,
                                Empresa: response.data.Datos[0].NombreEmpresa,
                                Observaciones: response.data.Datos[0].Observaciones

                            }));
                            var objEnviar = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Numero: $scope.NumeroDocumento,
                                QR_Manifiesto: imgQRCode.bytes
                            }
                            ManifiestoFactory.Guardar(objEnviar).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso == true) {
                                        if (response.data.Datos > CERO) {
                                            $scope.NombreReporte = NOMBRE_REPORTE_MANI;
                                            //Arma el filtro
                                            $scope.ArmarFiltroManifiesto();
                                            //Llama a la pagina ASP con el filtro por GET
                                            if (OpcionPDf == 1) {
                                                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
                                            }
                                            if (OpcionEXCEL == 1) {
                                                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
                                            }

                                            //Se limpia el filtro para poder hacer mas consultas
                                            $scope.FiltroArmado = '';

                                        }
                                        else {
                                            ShowError(response.statusText);
                                        }
                                    }
                                    else {
                                        ShowError(response.statusText);
                                    }
                                }, function (response) {
                                    ShowError(response.statusText);
                                });
                        }
                        else {

                            $scope.Buscando = false;
                        }

                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };

        $scope.DesplegarInformePoliza = function (Numero, OpcionPDf, OpcionEXCEL) {

            $scope.urlASP = '';
            $scope.Numero = Numero;
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

            $scope.NombreReporte = NOMBRE_REPORTE_PLANILLA_POLIZA;
            //Arma el filtro
            $scope.ArmarFiltroPlanillaPoliza();
            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.DesplegarFormatoGastos = function (Numero, OpcionPDF, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.Numero = Numero;
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NombreFormato = NOMBRE_FORMATO_GASTOS_CONDUCTOR;

            if (OpcionPDF == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreFormato + '&Numero=' + Numero + '&OpcionExportarPdf=' + OpcionPDF + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreFormato + '&Numero=' + Numero + '&OpcionExportarExcel=' + OpcionPDF + '&Prefijo=' + $scope.pref);
            }

        }

        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento != undefined && $scope.NumeroDocumento != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }

        }
        $scope.ArmarFiltroManifiesto = function () {
            if ($scope.Numero != undefined && $scope.Numero != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.Numero;
            }

        }
        $scope.ArmarFiltroPlanillaPoliza = function () {
            if ($scope.Numero != undefined && $scope.Numero != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.Numero;
            }

        }
        $scope.DesplegarPlan = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NombreReporte = NOMBRE_REPORTE_PLAN_RUTA;

            //Arma el filtro
            $scope.FiltroArmado = '&Numero=' + Numero;

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.DesplegarAutorizacion = function (item) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

            var NumeroAutorizacion = 0;
            ResponseAutorizaciones.forEach(itemAutorizacion => {
                if (itemAutorizacion.NumeroDocumento == item.Codigo) {
                    NumeroAutorizacion = itemAutorizacion.Codigo

                }
            });


            window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=AutorizacionValorDeclarado&ID=' + NumeroAutorizacion);


        }
        //------Funciones
    }]);