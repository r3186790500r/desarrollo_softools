﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Despachos
    <JsonObject>
    Public NotInheritable Class CumplidoRemesa
        Inherits Base
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CumplidoRemesa"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            Me.NumeroRemesa = Read(lector, "NumeroDocumentoRemesa")
            Me.FechaRemesa = Read(lector, "FechaRemesa")
            Me.NombreRuta = Read(lector, "NombreRuta")
            Me.NombreCliente = Read(lector, "NombreCliente")
            Me.DocumentoCliente = Read(lector, "Documento_Cliente")
            Me.FleteCliente = Read(lector, "Total_Flete_Cliente")
            Me.NombreProducto = Read(lector, "NombreProducto")
            Me.Cantidad = Read(lector, "CantidadRemesa")
            Me.Peso = Read(lector, "PesoRemesa")
            Me.IdentificacionRecibe = Read(lector, "Numero_Identificacion_Recibe")
            Me.NombreRecibe = Read(lector, "Nombre_Recibe")
            Me.TelefonoRecibe = Read(lector, "Telefonos_Recibe")
            Me.FechaRecibe = Read(lector, "Fecha_Recibe")
            Me.CantidadRecibida = Read(lector, "Cantidad_Recibe")
            Me.PesoRecibido = Read(lector, "Peso_Recibe")

            Me.CantidadFaltante = Read(lector, "Cantidad_Faltante")
            Me.PesoFaltante = Read(lector, "Peso_Faltante")
            Me.ValorFlatante = Read(lector, "Valor_Faltante")
            Me.PesoCargue = Read(lector, "Peso_Cargue")
            Me.PesoDescargue = Read(lector, "Peso_Descargue")

            Me.ObservacionesRecibe = Read(lector, "Observaciones_Recibe")
            Me.SoporteCliente = Read(lector, "Soporte_Cliente")


            Me.Cumplido = Read(lector, "Cumplido")
        End Sub

#Region "Propiedades"

        <JsonProperty>
        Public Property NumeroRemesa As Integer

        <JsonProperty>
        Public Property FechaRemesa As Date

        <JsonProperty>
        Public Property NombreRuta As String

        <JsonProperty>
        Public Property NombreCliente As String

        <JsonProperty>
        Public Property DocumentoCliente As String

        <JsonProperty>
        Public Property FleteCliente As Double

        <JsonProperty>
        Public Property NombreProducto As String

        <JsonProperty>
        Public Property Cantidad As Double

        <JsonProperty>
        Public Property Peso As Double

        <JsonProperty>
        Public Property FechaRecibe As Date

        <JsonProperty>
        Public Property NombreRecibe As String

        <JsonProperty>
        Public Property IdentificacionRecibe As String

        <JsonProperty>
        Public Property TelefonoRecibe As String

        <JsonProperty>
        Public Property SoporteCliente As String
        <JsonProperty>
        Public Property ObservacionesRecibe As String

        <JsonProperty>
        Public Property CantidadRecibida As Integer

        <JsonProperty>
        Public Property PesoRecibido As Integer

        <JsonProperty>
        Public Property FirmaRecibe As String

        <JsonProperty>
        Public Property FechaCrea As Date

        <JsonProperty>
        Public Property UsuarioCrea As Usuarios

        <JsonProperty>
        Public Property FechaModifica As Date

        <JsonProperty>
        Public Property UsuarioModifica As Usuarios

        <JsonProperty>
        Public Property Oficina As Oficinas

        <JsonProperty>
        Public Property Cumplido As Integer


        <JsonProperty>
        Public Property CantidadFaltante As Double

        <JsonProperty>
        Public Property PesoFaltante As Double

        <JsonProperty>
        Public Property ValorFlatante As Double

        <JsonProperty>
        Public Property PesoCargue As Double

        <JsonProperty>
        Public Property PesoDescargue As Double
#End Region

    End Class
End Namespace
