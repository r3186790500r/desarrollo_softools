﻿PRINT 'gsp_insertar_chequera_cuenta_bancarias'
GO
DROP PROCEDURE gsp_insertar_chequera_cuenta_bancarias
GO
CREATE PROCEDURE gsp_insertar_chequera_cuenta_bancarias
(
@par_EMPR_Codigo SMALLINT,
@par_CUBA_Codigo SMALLINT,
@par_Cheque_Inicial NUMERIC,
@par_Cheque_Final NUMERIC,
@par_Cheque_Actual NUMERIC,
@par_Estado SMALLINT,
@par_USUA_Codigo_Crea SMALLINT
)
AS
BEGIN

INSERT INTO Chequera_Cuenta_Bancarias
(
EMPR_Codigo,
CUBA_Codigo,
Cheque_Inicial,
Cheque_Final,
Cheque_Actual,
Estado,
USUA_Codigo_Crea,
Fecha_Crea
)
VALUES 
(
@par_EMPR_Codigo,
@par_CUBA_Codigo,
@par_Cheque_Inicial,
@par_Cheque_Final,
@par_Cheque_Actual,
@par_Estado,
@par_USUA_Codigo_Crea,
GETDATE()
)
SELECT Codigo = @@identity

END 
GO