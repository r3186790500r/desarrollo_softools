﻿PRINT 'gsp_modificar_semirremolques'
go
DROP PROCEDURE gsp_modificar_semirremolques
GO
CREATE PROCEDURE gsp_modificar_semirremolques
(
@par_EMPR_Codigo SMALLINT
,@Codigo NUMERIC
,@par_Codigo_AlternO varchar(20) = null
,@par_TERC_Codigo_Propietario NUMERIC = NULL
,@par_Equipo_Propio SMALLINT = NULL
,@par_MASE_Codigo NUMERIC = NULL
,@par_CATA_TISE_Codigo NUMERIC = NULL
,@par_CATA_TICA_Codigo NUMERIC = NULL
,@par_CATA_TIEQ_Codigo NUMERIC = NULL
,@par_Modelo NUMERIC = NULL
,@par_Numero_Ejes SMALLINT = NULL
,@par_Levanta_Ejes SMALLINT = NULL
,@par_Ancho NUMERIC(18,2) = NULL
,@par_Alto NUMERIC(18,2)  = NULL
,@par_Largo NUMERIC(18,2)  = NULL
,@par_Peso_Vacio NUMERIC(18,2)  = NULL
,@par_Capacidad_Galones NUMERIC(18,2)  = NULL
,@par_Capacidad_Kilos NUMERIC(18,2)  = NULL

,@par_Justificacion_Bloqueo varchar(150) = NULL
,@par_Estado SMALLINT = NULL
,@par_USUA_Codigo_Modifica SMALLINT = NULL

)
AS
BEGIN

update Semirremolques
           set 
		   Codigo_Alterno = @par_Codigo_Alterno
           ,TERC_Codigo_Propietario = ISNULL(@par_TERC_Codigo_Propietario,0)
           ,Equipo_Propio = ISNULL(@par_Equipo_Propio,0)
           ,MASE_Codigo = ISNULL(@par_MASE_Codigo,0)
           ,CATA_TISE_Codigo =ISNULL(@par_CATA_TISE_Codigo,3400)
           ,CATA_TICA_Codigo = ISNULL(@par_CATA_TICA_Codigo,2300)
           ,CATA_TIEQ_Codigo = ISNULL(@par_CATA_TIEQ_Codigo,3500)
           ,Modelo = @par_Modelo
           ,Numero_Ejes = @par_Numero_Ejes
           ,Levanta_Ejes = @par_Levanta_Ejes
           ,Ancho = @par_Ancho
           ,Alto = @par_Alto
           ,Largo = @par_Largo
           ,Peso_Vacio = @par_Peso_Vacio
           ,Capacidad_Galones = @par_Capacidad_Galones
           ,Capacidad_Kilos = @par_Capacidad_Kilos
         
           ,Justificacion_Bloqueo = @par_Justificacion_Bloqueo
           ,Estado = ISNULL(@par_Estado,0)
           ,USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica
           ,Fecha_Modifica = GETDATE()
		   where EMPR_Codigo = @par_EMPR_Codigo
		   and Codigo = @Codigo
select @Codigo as Codigo
END
GO
