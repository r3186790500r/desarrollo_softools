﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico
Imports EncoExpres.GesCarga.Repositorio.Basico

Namespace Basico
    Public NotInheritable Class PersistenciaTipoSubsistemas
        Implements IPersistenciaBase(Of TipoSubsistemas)
        Public Function Consultar(filtro As TipoSubsistemas) As IEnumerable(Of TipoSubsistemas) Implements IPersistenciaBase(Of TipoSubsistemas).Consultar
            Return New RepositorioTipoSubsistemas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TipoSubsistemas) As Long Implements IPersistenciaBase(Of TipoSubsistemas).Insertar
            Return New RepositorioTipoSubsistemas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TipoSubsistemas) As Long Implements IPersistenciaBase(Of TipoSubsistemas).Modificar
            Return New RepositorioTipoSubsistemas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TipoSubsistemas) As TipoSubsistemas Implements IPersistenciaBase(Of TipoSubsistemas).Obtener
            Return New RepositorioTipoSubsistemas().Obtener(filtro)
        End Function
    End Class
End Namespace
