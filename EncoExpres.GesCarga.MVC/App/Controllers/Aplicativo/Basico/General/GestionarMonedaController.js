﻿EncoExpresApp.controller("GestionarMonedaCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'MonedasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, MonedasFactory) {
        $scope.Titulo = 'GESTIONAR MONEDA';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Moneda' }, { Nombre: 'Gestionar' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_MONEDA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.Codigo = 0

        $scope.ListadoEstados = [
            { Nombre: 'Activa', Codigo: 1 },
            { Nombre: 'Inactiva', Codigo: 0 }
        ];


        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Estado: $scope.ListadoEstados[0],
        }
        /* Obtener parametros del enrutador*/

        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            $scope.local = $routeParams.Local;

            if ($scope.Modelo.Codigo > CERO) {
                ObtenerMoneda()

            } else {

            }
        }
        else {
            $scope.Modelo.Codigo = 0;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
                Local: $scope.local

            };
            MonedasFactory.MonedaLocal(filtros).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.DesabilitarLocal = true
                } else {
                    $scope.DesabilitarLocal = false;
                }

            });
        }





        $scope.LongitudCiudad = 0;// verifica la cantidad de datos ingresados  de la ciudad
        $scope.IngresoValidaCiudad = true
        $scope.CodigoCiudad = 0;

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/



        function ObtenerMoneda() {
            blockUI.start('Cargando Moneda Código ' + $scope.Modelo.Nombre);

            $timeout(function () {
                blockUI.message('Cargando Moneda Código ' + $scope.Modelo.Nombre);
            }, 200);



            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
                Local: $scope.local

            };

            blockUI.delay = 1000;
            MonedasFactory.Obtener(filtros).then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.Modelo.Nombre = response.data.Datos.Nombre;
                    $scope.Modelo.NombreCorto = response.data.Datos.NombreCorto;
                    $scope.Modelo.Simbolo = response.data.Datos.Simbolo;


                    if (response.data.Datos.Local == 1) {
                        $scope.Modelo.Local = true;
                        $scope.DesabilitarLocal = false;
                    } else {
                        MonedasFactory.MonedaLocal(filtros).then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.DesabilitarLocal = true;
                            } else {
                                $scope.DesabilitarLocal = false;
                            }

                        });
                    }


                    $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);

                }
                else {
                    ShowError('No se logro consultar la Moneda No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarMoneda';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarMoneda';
            });
            blockUI.stop();
        };

        /*---------------------------------------------------------------------------- GUARDAR ----------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardarMoneda = function () {
            showModal('modalConfirmacionGuardarMoneda');

        };

        MonedasFactory.MonedaLocal({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.DesabilitarLocal = true
            } else {
                $scope.DesabilitarLocal = false;
            }

        });

        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarMoneda');
            if (DatosRequeridosMoneda()) {
                $scope.Modelo.Estado = $scope.Modelo.Estado.Codigo
                if ($scope.Modelo.Local == true) {
                    $scope.Modelo.Local = 1

                } else {
                    $scope.Modelo.Local = 0

                }

                MonedasFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó la Moneda: ' + $scope.Modelo.Nombre);

                                }
                                else {
                                    ShowSuccess('Se modificó la Moneda: ' + $scope.Modelo.Nombre);

                                }
                                location.href = '#!ConsultarMonedas/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridosMoneda() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == "") {
                $scope.MensajesError.push('Debe ingresar el nombre de la Moneda');
                continuar = false;
            }

            if ($scope.Modelo.NombreCorto == undefined || $scope.Modelo.NombreCorto == "" || $scope.NombreCorto == false) {
                $scope.MensajesError.push('Debe ingresar el NombreCorto de la Moneda');
                continuar = false;
            }

            if ($scope.Modelo.Simbolo == undefined || $scope.Modelo.Simbolo == "" || $scope.NombreCorto == false) {
                $scope.MensajesError.push('Debe ingresar el simbolo de la Moneda');
                continuar = false;
            }

            if ($scope.Modelo.Estado.Codigo == -1) {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar el estado de la Moneda');
                continuar = false;
            }



            return continuar;
        }
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function () {
            $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno)
        };
        $scope.MaskTelefono = function () {
            $scope.Modelo.Telefono = MascaraTelefono($scope.Modelo.Telefono)
        };
        $scope.MaskMinus = function () {
            try { $scope.Modelo.Email = $scope.Modelo.Email.toLowerCase() } catch (e) { }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta

    }]);