﻿PRINT 'gsp_obtener_proveedor'
GO
DROP PROCEDURE gsp_obtener_proveedor
GO
CREATE PROCEDURE gsp_obtener_proveedor  
(  
@par_EMPR_Codigo smallint,  
@par_Codigo Numeric  = null,
@par_Identificacion VARCHAR(20) = NULL
)  
AS   
BEGIN  
SELECT   
TEEM.EMPR_Codigo,
ISNULL(ETCC_Numero,'') AS ETCC_Numero,
ISNULL(TEEM.CATA_FOCO_Codigo,8800) AS CATA_FOCO_Codigo,
ISNULL(TEEM.Dias_Plazo_Cobro,0) AS Dias_Plazo_Cobro
FROM Tercero_Proveedores AS TEEM LEFT JOIN Terceros AS TERC ON 
TERC.EMPR_Codigo = TEEM.EMPR_Codigo
AND TERC.Codigo = TEEM.TERC_Codigo
WHERE TEEM.EMPR_Codigo = @par_EMPR_Codigo  
AND TERC.Codigo = isnull(@par_Codigo  ,TERC.Codigo)
AND TERC.Numero_Identificacion =  isnull(@par_Identificacion  ,TERC.Numero_Identificacion) 
  
END  
GO