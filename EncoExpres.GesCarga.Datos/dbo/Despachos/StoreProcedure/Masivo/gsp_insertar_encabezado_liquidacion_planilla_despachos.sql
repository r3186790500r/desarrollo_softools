﻿PRINT 'gsp_insertar_encabezado_liquidacion_planilla_despachos'
GO

DROP PROCEDURE gsp_insertar_encabezado_liquidacion_planilla_despachos
GO

CREATE PROCEDURE gsp_insertar_encabezado_liquidacion_planilla_despachos
(
	@par_EMPR_Codigo NUMERIC,
	@par_TIDO_Codigo NUMERIC,
	@par_Fecha DATE,
	@par_Fecha_Entrega DATE,
	@par_ENPD_Numero NUMERIC,
	@par_ENMC_Numero NUMERIC,
	@par_Observaciones VARCHAR(500),
	@par_Valor_Flete_Transportador MONEY,
	@par_Valor_Conceptos_Liquidacion MONEY,
	@par_Valor_Base_Impuestos MONEY,
	@par_Valor_Impuestos MONEY,
	@par_Valor_Pagar MONEY,
	@par_Estado SMALLINT,
	@par_USUA_Codigo_Crea SMALLINT,
	@par_OFIC_Codigo SMALLINT
)
AS
BEGIN
	DECLARE @NumeroIdentity NUMERIC
	DECLARE @NumeroDocumentoGenerado NUMERIC
	DECLARE @NumeroLiquidacion NUMERIC

	SELECT
		@NumeroLiquidacion = ELPD_Numero
	FROM
		Encabezado_Planilla_Despachos
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Numero = @par_ENPD_Numero

	IF ISNULL(@NumeroLiquidacion,0) = 0 BEGIN
	
		EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, @par_OFIC_Codigo, @NumeroDocumentoGenerado OUTPUT

		INSERT INTO
			Encabezado_Liquidacion_Planilla_Despachos
			(
				EMPR_Codigo,
				TIDO_Codigo,
				Numero_Documento,
				Fecha,
				Fecha_Entrega,
				ENPD_Numero,
				ENMC_Numero,
				Observaciones,
				Valor_Flete_Transportador,
				Valor_Conceptos_Liquidacion,
				Valor_Base_Impuestos,
				Valor_Impuestos,
				Valor_Pagar,
				Anulado,
				Estado,
				Fecha_Crea,
				USUA_Codigo_Crea,
				OFIC_Codigo,
				Numeracion
			)
		VALUES
		(
			@par_EMPR_Codigo,
			@par_TIDO_Codigo,
			@NumeroDocumentoGenerado,
			@par_Fecha,
			@par_Fecha_Entrega,
			@par_ENPD_Numero,
			@par_ENMC_Numero,
			@par_Observaciones,
			@par_Valor_Flete_Transportador,
			@par_Valor_Conceptos_Liquidacion,
			@par_Valor_Base_Impuestos,
			@par_Valor_Impuestos,
			@par_Valor_Pagar,
			0,
			@par_Estado,
			GETDATE(),
			@par_USUA_Codigo_Crea,
			@par_OFIC_Codigo,
			''
		)

		SET @NumeroIdentity = @@identity

		SELECT
			Numero = @NumeroIdentity,
			@NumeroDocumentoGenerado AS NumeroDocumento


		UPDATE
			Encabezado_Planilla_Despachos
		SET
			ELPD_Numero = @NumeroIdentity
		WHERE
			EMPR_Codigo = @par_EMPR_Codigo
			AND Numero = @par_ENPD_Numero

	END
	ELSE BEGIN
		SELECT
			Numero = -1,
			NumeroDocumento = -1
	END
END
GO