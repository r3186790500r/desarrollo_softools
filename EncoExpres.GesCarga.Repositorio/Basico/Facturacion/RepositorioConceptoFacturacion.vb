﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Facturacion
Imports EncoExpres.GesCarga.Entidades.Basico.General

Imports EncoExpres.GesCarga.Repositorio.Basico.General


Namespace Basico.Facturacion

    ''' <summary>
    ''' Clase <see cref="RepositorioConceptoFacturacion"/>
    ''' </summary>

    Public NotInheritable Class RepositorioConceptoFacturacion
        Inherits RepositorioBase(Of ConceptoFacturacion)
        Dim inserto As Boolean = True
        Public Overrides Function Consultar(filtro As ConceptoFacturacion) As IEnumerable(Of ConceptoFacturacion)
            Dim lista As New List(Of ConceptoFacturacion)
            Dim item As ConceptoFacturacion

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Codigo) Then
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If
                If Not IsNothing(filtro.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If

                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_conceptos_facturacion]")

                While resultado.Read
                    item = New ConceptoFacturacion(resultado)
                    item.CodigoEmpresa = filtro.CodigoEmpresa
                    If Not IsNothing(filtro.Ciudad) Then
                        item.Ciudad = New Ciudades With {.Codigo = filtro.Ciudad.Codigo}
                    End If
                    item.ListadoImpuestos = Consulta_Impuestos_Asociados(item)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ConceptoFacturacion) As Long



            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Operacion ", entidad.Operacion)
                conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)

                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_conceptos_facturacion")



                While resultado.Read
                    entidad.Codigo = resultado.Item("Numero").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(0) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Function LimpiarImpuestosAsociados(entidad As ConceptoFacturacion) As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_limpiar_facturacion_conceptos")


            End Using
            Return entidad.Codigo
        End Function

        Public Function AsignarImpuestos(entidad As ImpuestoConceptoFacturacion, Codigo As Integer) As Boolean

            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Impuesto", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_valor_tarifa", entidad.Valor_tarifa)
                conexion.AgregarParametroSQL("@par_valor_base", entidad.valor_base)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asignar_facturacion_conceptos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Numero").ToString()
                End While

                If entidad.Codigo > Cero Then
                    inserto = True
                Else
                    inserto = False
                End If

            End Using
            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As ConceptoFacturacion) As Long

            Dim resultado As IDataReader
            If entidad.AplicaImpuesto = Cero Then
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Operacion", entidad.Operacion)

                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_conceptos_facturacion")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()
                End Using
            Else
                LimpiarImpuestosAsociados(entidad)
                If Not IsNothing(entidad.ListadoImpuestos) Then
                    For Each listaguiasnoselecc In entidad.ListadoImpuestos
                        inserto = AsignarImpuestos(listaguiasnoselecc, entidad.Codigo)
                    Next
                End If
                If inserto = True Then
                    entidad.Codigo = 1
                End If
            End If


            If entidad.Codigo.Equals(Cero) Then
                Return Cero
            End If



            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As ConceptoFacturacion) As ConceptoFacturacion
            Dim item As New ConceptoFacturacion
            Dim RepoImpuesto As New RepositorioImpuestos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_conceptos_facturacion]")

                While resultado.Read
                    item = New ConceptoFacturacion(resultado)
                End While

                If filtro.Codigo > 0 Then
                    item.ListadoImpuestos = Obtener_Detalle(filtro)
                End If



            End Using


            Return item
        End Function

        Public Function Consulta_Impuestos_Asociados(filtro As ConceptoFacturacion) As IEnumerable(Of ImpuestoConceptoFacturacion)
            Dim lista As New List(Of ImpuestoConceptoFacturacion)
            Dim item As ImpuestoConceptoFacturacion

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                If Not IsNothing(filtro.Ciudad) Then
                    If filtro.Ciudad.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.Ciudad.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_impuestos_conceptos_facturacion")

                While resultado.Read
                    item = New ImpuestoConceptoFacturacion(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function

        Public Function Obtener_Detalle(filtro As ConceptoFacturacion) As IEnumerable(Of ImpuestoConceptoFacturacion)
            Dim lista As New List(Of ImpuestoConceptoFacturacion)
            Dim item As ImpuestoConceptoFacturacion

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_impuestos_conceptos_facturacion")

                While resultado.Read
                    item = New ImpuestoConceptoFacturacion(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function
        Public Function Anular(entidad As ConceptoFacturacion) As Boolean
            Dim anulo As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                If entidad.AplicaImpuesto = Cero Then
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                    LimpiarImpuestosAsociados(entidad)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_concepto_facturacion]")
                Else
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Impuesto", entidad.CodigoImpuesto)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_impuestos_conceptos_facturacion]")

                End If


                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class

End Namespace