﻿EncoExpresApp.controller("GestionarInspeccionPreoperacionalCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'blockUIConfig',
    'SitiosCargueDescargueFactory', 'VehiculosFactory', 'SemirremolquesFactory', 'DocumentoInspeccionPreoperacionalFactory', 'InspeccionPreoperacionalFactory',
    'FormularioInspeccionesFactory', 'TercerosFactory','OrdenCargueFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, blockUIConfig,
        SitiosCargueDescargueFactory, VehiculosFactory, SemirremolquesFactory, DocumentoInspeccionPreoperacionalFactory, InspeccionPreoperacionalFactory,
        FormularioInspeccionesFactory, TercerosFactory, OrdenCargueFactory) {

        $scope.Titulo = 'GESTIONAR INSPECCIÓN';

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO; 

        $scope.MapaSitio = [{ Nombre: 'Control Tráfico' }, { Nombre: 'Documentos' }, { Nombre: 'Inspecciones' }, { Nombre: 'Gestionar' }];

        if ($routeParams.MeapCodigo !== undefined && $routeParams.MeapCodigo !== null && $routeParams.MeapCodigo !== '' && $routeParams.MeapCodigo !== 0) {
            MeapCodigo = parseInt($routeParams.MeapCodigo);
            if (MeapCodigo > 0) {
                switch (MeapCodigo) {
                    case OPCION_MENU_INSPECCIONES_PREOPERACIONALES:
                        $scope.OPCION_MENU = OPCION_MENU_INSPECCIONES_PREOPERACIONALES;
                        break;
                    case INSPECIONES_GESPHONE:
                        $scope.OPCION_MENU = INSPECIONES_GESPHONE;
                        $scope.MapaSitio = [{ Nombre: 'Preoperacional ' }, { Nombre: 'Gestionar' }]; 
                        break;
                }
            }
        }
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + $scope.OPCION_MENU);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoFormatosInspeccion = [];
        $scope.ListaPuntosGestion = [];
        $scope.ListaVehiculos = [];
        $scope.ListadoResponsables = [];
        $scope.ListaSitioCargueDescargue = [];
        $scope.CorreosClientes = [];
        $scope.CorreosTransportador = []
        $scope.CorreosConductor = []
        $scope.ListadoSecciones = [];
        $scope.ListaVehiculo = [];
        $scope.ListaSemirremolque = [];
        $scope.ListaConductor = [];
        $scope.ListaTransportador = [];
        $scope.ListaCliente = [];
        $scope.ListaAutoriza = [];
        $scope.ListaSitiosCargueDescargue = [];
        $scope.DeshabilitarActualizar = false;
        $scope.ValidarDatosDetalle = false;
        $scope.Deshabilitar = false;
        $scope.MostrarCanvasFirma = true;
        $scope.TipoInspeccionPreoperacional = false;
        $scope.OrdenCargue = '';
        $scope.PlanillaDespacho = '';
        
        $scope.list = {
            Fecha: new Date(),
            Fotos: [],
        };

        // Limpia la lista temporal de las fotos por usuario
        //DocumentoInspeccionPreoperacionalFactory.LimpiarTemporalUsuario({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo } }).
        //    then(function (response) {
        //        if (response.data.ProcesoExitoso == true) {

        //        } else {

        //        }
        //    }, function (response) {
        //        ShowError(response.statusText);
        //    })

        $scope.FechaActual = new Date();
        $scope.HoraActual = RetornarHoras($scope.FechaActual);
        $scope.FechaActual = RetornarFechaSinHora($scope.FechaActual)

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Numero: 0,
            CodigoAlterno: '',
            Nombre: '',
            FechaInspeccion: new Date(),
            FechaInicioHora: new Date(),
            FechaFinHora: new Date(),
            Estado: 0,
            HoraInicio: RetornarHoras($scope.FechaActual),
        }

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "BORRADOR" },
            { Codigo: 1, Nombre: "DEFINITIVO" },
        ]

 
        /* Obtener parametros*/
        if ($routeParams.Numero > 0) {
            $scope.Modelo.Numero = $routeParams.Numero;
            $scope.Titulo = 'CONSULTAR INSPECCIÓN PRE-OPERACIONAL';
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
            Obtener();
        } else {
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }
        //else if ($routeParams.Numero > 0 && $routeParams.Autorizacion > 0) {
        //    $scope.Modelo.Numero = $routeParams.Numero;
        //    $scope.Autorizacion = parseInt($routeParams.Autorizacion);
        //    $scope.Titulo = 'CONSULTAR INSPECCIÓN PRE-OPERACIONAL';
        //    Obtener();
        //} else {
        //    $scope.Modelo.Numero = 0;
        //    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        //}

        /*-----------------------------------------------------------------Cargar Imagenes --------------------------------------------------------------------------------- */
        //$scope.EliminarFoto = function (index, Codigo) {
        //    var EliminarTemporal = 1
        //    if ($scope.Modelo.Numero > 0) {
        //        EliminarTemporal = 0
        //    }
        //    else {
        //        EliminarTemporal = 1
        //    }
        //    var fotoEliminar = {
        //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //        Codigo: Codigo,
        //        EliminarTemporal: EliminarTemporal,
        //    }
        //    fotoEliminar.Foto = null;
        //    DocumentoInspeccionPreoperacionalFactory.EliminarFoto(fotoEliminar).
        //        then(function (response) {
        //            if (response.data.ProcesoExitoso == true) {
        //                $scope.list.Fotos.splice(index, 1);
        //            } else {
        //                ShowError(response.data.MensajeError);
        //            }
        //        }, function (response) {
        //            ShowError(response.statusText);
        //        })
        //};

        //Se realiza la consulta de las inspecciones asociadas al viaje

        //$scope.CargarImagen = function (element) {
        //    var continuar = true;
        //    $scope.TipoImagen = element
        //    blockUI.start();
        //    $scope.NombreDocumento = element.files[0].name
        //    if ($scope.list.Fotos.length < 10) {
        //        if (element === undefined) {
        //            ShowError("Solo se pueden cargar archivos en formato jpeg, png  o pdf", false);
        //            continuar = false;
        //        }
        //        if (continuar == true) {
        //            if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png' && element.files[0].type !== 'application/pdf') {
        //                ShowError("Solo se pueden cargar archivos en formato jpeg, png  o pdf", false);
        //                continuar = false;
        //            }
        //            if (element.files[0].type == 'application/pdf') {
        //                if (element.files[0].size >= 4000000) { //4 MB
        //                    ShowError("El documento " + element.files[0].name + " supera los 4 MB, intente con otro documento", false);
        //                    continuar = false;
        //                }
        //            }
        //        }
        //        if (continuar == true) {
        //            var reader = new FileReader();
        //            reader.onload = $scope.AsignarImagenListado;
        //            reader.readAsDataURL(element.files[0]);
        //        }
        //    }
        //    else {
        //        ShowError("Ha superado el maximo de imagenes permitidas", false);
        //    }
        //    blockUI.stop();
        //}

        //$scope.AsignarImagenListado = function (e) {
        //    $scope.$apply(function () {
        //        $scope.Convertirfoto = null
        //        var string = ''
        //        var TipoImagen = ''
        //        if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
        //            var img = new Image()
        //            img.src = e.target.result
        //            //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
        //            //la imagen es vertical
        //            $timeout(function () {
        //                if (img.height > img.width) {
        //                    RedimVertical(img)
        //                    $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
        //                }
        //                //la imagen es horizontal
        //                else {
        //                    RedimHorizontal(img)
        //                    $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
        //                }
        //                TipoImagen = 'JPEG'
        //                $scope.TipoImagen = TipoImagen
        //                AsignarFotoListado()
        //            }, 100)
        //        }
        //        else if ($scope.TipoImagen.files[0].type == 'image/png') {
        //            var img = new Image()
        //            img.src = e.target.result
        //            //la imagen es vertical
        //            $timeout(function () {
        //                if (img.height > img.width) {
        //                    RedimVertical(img)
        //                    $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
        //                }
        //                //la imagen es horizontal
        //                else {
        //                    RedimHorizontal(img)
        //                    $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
        //                }
        //                TipoImagen = 'PNG'
        //                $scope.TipoImagen = TipoImagen
        //                AsignarFotoListado()
        //            }, 400)
        //        }
        //        else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
        //            $scope.Convertirfoto = e.target.result.replace(/data:application\/pdf;base64,/g, '')
        //            TipoImagen = 'PDF'
        //            $scope.TipoImagen = TipoImagen
        //            AsignarFotoListado()
        //        }
        //    });
        //}

        //function AsignarFotoListado() {
        //    var foto = {
        //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //        NumeroInspeccion: $scope.Modelo.Numero,
        //        NombreDocumento: $scope.NombreDocumento,
        //        ExtencionDocumento: $scope.TipoImagen,
        //        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        //        Documento: $scope.Convertirfoto,
        //    }
        //    // Guardar Foto temporal
        //    DocumentoInspeccionPreoperacionalFactory.InsertarTemporal(foto).
        //        then(function (response) {
        //            if (response.data.Datos > 0) {
        //                // Se guardó correctamente la foto
        //                foto.Codigo = response.data.Datos
        //                $scope.list.Fotos.push(foto);
        //            } else {
        //                ShowError(response.data.MensajeError);
        //            }
        //        }, function (response) {
        //            ShowError(response.statusText);
        //        });
        //}

        /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        //-------------AutoCompletes
        //---- Condcutor
        $scope.ListaConductor = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CONDUCTOR,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaConductor = ValidarListadoAutocomplete(Response.Datos, $scope.ListaConductor);
                }
            }
            return $scope.ListaConductor;
        };
        //---- Perfil Empresa Transportadora
        $scope.ListaTransportador = [];
        $scope.AutocompleteTransportador = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_EMPRESA_TRANSPORTADORA,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaTransportador = ValidarListadoAutocomplete(Response.Datos, $scope.ListaTransportador);
                }
            }
            return $scope.ListaTransportador;
        };
        //---- Cliente
        $scope.ListaCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCliente);
                }
            }
            return $scope.ListaCliente;
        };
        //---- Autoriza - Responsable
        $scope.ListaAutoriza = [];
        $scope.AutocompleteAutoriza = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_EMPLEADO,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaAutoriza = ValidarListadoAutocomplete(Response.Datos, $scope.ListaAutoriza);
                }
            }
            return $scope.ListaAutoriza;
        };
        //---- Vehiculos
        $scope.ListaVehiculo = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaVehiculo = ValidarListadoAutocomplete(Response.Datos, $scope.ListaVehiculo);
                }
            }
            return $scope.ListaVehiculo;
        };
        //---- Sitios Cargue Descargue
        SitiosCargueDescargueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListaSitiosCargueDescargue = response.data.Datos;
                        if ($scope.CodigoSitioCargue != undefined && $scope.CodigoSitioCargue != '' && $scope.CodigoSitioCargue != 0 && $scope.CodigoSitioCargue != null) {
                            if ($scope.CodigoSitioCargue > 0) {
                                $scope.Modelo.SitioCargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==' + $scope.CodigoSitioCargue);
                            } else {
                                $scope.Modelo.SitioCargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.Modelo.SitioCargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==0');
                        }
                        if ($scope.CodigoSitioDescargue != undefined && $scope.CodigoSitioDescargue != '' && $scope.CodigoSitioDescargue != 0 && $scope.CodigoSitioDescargue != null) {
                            if ($scope.CodigoSitioDescargue > 0) {
                                $scope.Modelo.SitioDescargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==' + $scope.CodigoSitioDescargue);
                            } else {
                                $scope.Modelo.SitioDescargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.Modelo.SitioDescargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==0');
                        }
                    }
                    else {
                        $scope.SitiosCargueDescargue = [];
                        $scope.Modelo.SitioCargue = null;
                        $scope.Modelo.SitioDescargue = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        // Semirremolques
        SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Placa: '', Codigo: 0 })
                        $scope.ListaSemirremolque = response.data.Datos;
                        if ($scope.CodigoSemirremolque != undefined && $scope.CodigoSemirremolque != '' && $scope.CodigoSemirremolque != 0 && $scope.CodigoSemirremolque != null) {
                            if ($scope.CodigoSemirremolque > 0) {
                                $scope.ModeloSemirremolque = $linq.Enumerable().From($scope.ListaSemirremolque).First('$.Codigo ==' + $scope.CodigoSemirremolque);
                            } else {
                                $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListaSemirremolque).First('$.Codigo == 0');
                            }
                        } else {
                            $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListaSemirremolque).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListaSemirremolque = [];
                        $scope.Modelo.Semirremolque = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });



        //Cargar semirremolque de la placa vehiculo
        $scope.CargarSemirremolque = function (modelovehiculo) {
            // Vehiculo
            if (modelovehiculo != undefined && modelovehiculo != null && modelovehiculo != "") {
                VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: modelovehiculo.Codigo }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.CodigoRemolqueVehiculo = response.data.Datos.Semirremolque.Codigo;
                            var ExisteSemi = false;
                            for (var i = 0; i < $scope.ListaSemirremolque.length; i++) {
                                if ($scope.ListaSemirremolque[i].Codigo == $scope.CodigoRemolqueVehiculo) {
                                    ExisteSemi = true;
                                    break;
                                }
                            }
                            if (ExisteSemi) {
                                $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListaSemirremolque).First('$.Codigo == ' + $scope.CodigoRemolqueVehiculo);
                            }
                            $scope.CodigoConductorVehiculo = response.data.Datos.Conductor.Codigo;
                            $scope.Modelo.Conductor = $scope.CargarTercero(response.data.Datos.Conductor.Codigo);
                        } else {
                            $scope.ListaSemirremolque = [];
                            $scope.Modelo.Semirremolque = null;
                            $scope.ListaConductor = [];
                            $scope.Modelo.Conductor = null;
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        //--Formularios Inspecciones
        $scope.ListadoFormatosInspeccion = FormularioInspeccionesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos;

        $scope.ListTitem = function (item) {
            if (item.items) {
                item.items = false
            } else {
                item.items = true
            }
        }

        $scope.ConsultarFormatoInspeccion = function (CodigoFormato) {
         
            if (CodigoFormato !== undefined && CodigoFormato !== null && CodigoFormato !== '') {
                FormularioInspeccionesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CodigoFormato, ConsularDetalle: 1 }).
                    then(function (response) {
                        $scope.ListadoSecciones = [];
                        if (response.data.ProcesoExitoso === true) {
                            $scope.Modelo.TiempoVigencia = response.data.Datos.TiempoVigencia;
                            $scope.HabilitarFirma = response.data.Datos.Firma == 1 ? true : false;
                            response.data.Datos.SeccionFormulario.forEach(function (item) {
                                if (item.EstadoDocumento.Codigo == 1) {

                                    item.items = true,
                                        item.Relevante = 0;
                                    item.Cumple = 0;
                                    item.NoAplica = 0;
                                    item.ListadoItemSeccion = [];
                                    response.data.Datos.itemFormulario.forEach(function (itemSeccion) {

                                        itemSeccion.DeshabilitarCumple = false;
                                        itemSeccion.DeshabilitarNoAplica = false;

                                        if (itemSeccion.EstadoDocumento.Codigo == 1) {
                                            if (itemSeccion.TipoControl.Codigo == CODIGO_TIPO_CONTROL_CHECK) {
                                                itemSeccion.Valor = true
                                            }
                                            if (itemSeccion.Seccion == item.Codigo) {
                                                if (itemSeccion.Relevante == 1) {
                                                    itemSeccion.Relevante = true;
                                                }
                                                else {
                                                    itemSeccion.Relevante = false;
                                                }
                                                itemSeccion.Cumple = false;
                                                itemSeccion.NoAplica = false;
                                                item.ListadoItemSeccion.push(itemSeccion)
                                            }
                                        }
                                    })
                                    item.ListadoItemSeccion.sort((a, b) => {
                                        return (MascaraNumero(a.Orden) - MascaraNumero(b.Orden));
                                    });
                                    $scope.ListadoSecciones.push(item)
                                }

                          
                               
                            })
                            if (response.data.Datos.TipoInspeccion.Codigo == 23101/*Preoperacional*/) {
                                $scope.TipoInspeccionPreoperacional = true;
                            } else {
                                $scope.TipoInspeccionPreoperacional = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.HabilitarFirma = false;
            }
        }


        $scope.validaCumple = function (Detalle) {

            if (Detalle.Cumple) {
                //Detalle.DeshabilitarNoAplica = true;
                Detalle.NoAplica = false;
            }
          
                
        };
        $scope.validaNoAplica = function (Detalle) {
 
            if (Detalle.NoAplica ) {
                //Detalle.DeshabilitarCumple = true;
                Detalle.Cumple = false;
              
            }
                
        };
       

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando inspección No. ' + $scope.Modelo.Numero);

            $timeout(function () {
                blockUI.message('Cargando inspección No.' + $scope.Modelo.Numero);
            }, 1000);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                ConsularDetalle: 1,
            };

            blockUI.delay = 1000;
            InspeccionPreoperacionalFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.FormatoInspeccion.TipoInspeccion.Codigo == 23101/*Preoperacional*/) {
                            $scope.TipoInspeccionPreoperacional = true;
                        }
                        $scope.DeshabilitarActualizar = true;

                        $scope.Modelo = JSON.parse(JSON.stringify(response.data.Datos));

                        //var image = new Image();
                        //image.src = "data:image/png;base64," + response.data.Datos.Firma
                        //var img = cargaContextoCanvas('Firma')
                        //img.drawImage(image, 0, 0)

                        $scope.Modelo.FechaInspeccion = new Date(response.data.Datos.FechaInspeccion);

                        if (response.data.Datos.FechaAutoriza == '1900-01-01T00:00:00') {
                            $scope.Modelo.FechaAutoriza = new Date();
                        } else {
                            $scope.Modelo.FechaAutoriza = new Date(response.data.Datos.FechaAutoriza);
                        }

                        var FechaMinima = RetornaFechaSinHoras(FORMATO_FECHA_MINIMA)

                        /*valida el formato de fecha en dd/mm/yyyy*/
                        var fechaInicio = new Date(response.data.Datos.FechaInicioHora);
                        fechaInicio.setHours(0);
                        fechaInicio.setMinutes(0);
                        fechaInicio.setSeconds(0);
                        fechaInicio.setMilliseconds(0);

                        $scope.Modelo.FechaInicioHora = fechaInicio;

                        /*valida el formato de fecha en dd/mm/yyyy*/
                        var fechaFin = new Date(response.data.Datos.FechaFinHora);
                        fechaFin.setHours(0);
                        fechaFin.setMinutes(0);
                        fechaFin.setSeconds(0);
                        fechaFin.setMilliseconds(0);

                        $scope.Modelo.FechaFinHora = fechaFin;

                        $scope.Modelo.FechaInicioHora = Formatear_Fecha($scope.Modelo.FechaInicioHora, FORMATO_FECHA_s)
                        $scope.Modelo.FechaFinHora = Formatear_Fecha($scope.Modelo.FechaFinHora, FORMATO_FECHA_s)
                        FechaMinima = Formatear_Fecha(FechaMinima, FORMATO_FECHA_s)

                        if (new Date(response.data.Datos.FechaInicioHora) < MIN_DATE) {
                            $scope.Modelo.FechaInicioHora = new Date()
                            $scope.Modelo.HoraInicio = RetornarHoras($scope.Modelo.FechaInicioHora)
                        }
                        else {
                            $scope.Modelo.FechaInicioHora = fechaInicio
                            $scope.Modelo.HoraInicio = RetornarHoras(response.data.Datos.FechaInicioHora);
                        }
                        if (new Date(response.data.Datos.FechaFinHora) < MIN_DATE) {
                            $scope.Modelo.FechaFinHora = new Date()
                            $scope.Modelo.HoraFin = '00:00'
                        }
                        else {
                            $scope.Modelo.FechaFinHora = fechaFin
                            $scope.Modelo.HoraFin = RetornarHoras(response.data.Datos.FechaFinHora);
                        }
                        $scope.ListadoSecciones = [];

                        try {
                            if (response.data.Datos.Detalle.length > 0) {
                                var DetalleForm = response.data.Datos.Detalle;
                                for (var i = 0; i < DetalleForm.length; i++) {
                                    if (DetalleForm[i].SeccionFormulario > CERO && DetalleForm[i].ItemFormulario == 0) {
                                        $scope.ListadoSecciones.push({
                                            CodigoEmpresa: DetalleForm[i].CodigoEmpresa,
                                            Codigo: DetalleForm[i].SeccionFormulario,
                                            Nombre: DetalleForm[i].Nombre,
                                            Orden: DetalleForm[i].OrdenSeccion,
                                            Relevante: 0,
                                            Cumple: 0,
                                            ListadoItemSeccion: [],
                                            CodigoDetalle: DetalleForm[i].Codigo
                                        });
                                    }
                                }

                                for (var i = 0; i < DetalleForm.length; i++) {
                                    for (var j = 0; j < $scope.ListadoSecciones.length; j++) {
                                        if ($scope.ListadoSecciones[j].Codigo == DetalleForm[i].SeccionFormulario && DetalleForm[i].ItemFormulario > 0) {
                                            if (DetalleForm[i].TipoControl == CODIGO_TIPO_CONTROL_DATE) {
                                                DetalleForm[i].Valor = new Date(DetalleForm[i].Valor);
                                            }
                                            else if (DetalleForm[i].TipoControl == CODIGO_TIPO_CONTROL_NUMERO) {
                                                DetalleForm[i].Valor = Math.ceil(DetalleForm[i].Valor)
                                            }
                                            else if (DetalleForm[i].TipoControl == CODIGO_TIPO_CONTROL_CHECK) {
                                                if (DetalleForm[i].Valor == "1") {
                                                    DetalleForm[i].Valor = true
                                                }
                                                else {
                                                    DetalleForm[i].Valor = false
                                                }
                                            }
                                            if (DetalleForm[i].Relevante == 1) {
                                                DetalleForm[i].Relevante = true
                                            }
                                            else {
                                                DetalleForm[i].Relevante = false
                                            }
                                            if (DetalleForm[i].Cumple == 1) {
                                                DetalleForm[i].Cumple = true
                                            }
                                            else {
                                                DetalleForm[i].Cumple = false
                                            }
                                            $scope.ListadoSecciones[j].ListadoItemSeccion.push({
                                                CodigoEmpresa: DetalleForm[i].CodigoEmpresa,
                                                Codigo: DetalleForm[i].ItemFormulario,
                                                Nombre: DetalleForm[i].Nombre,
                                                Orden: DetalleForm[i].OrdenItem,
                                                TipoControl: { Codigo: DetalleForm[i].TipoControl },
                                                Valor: DetalleForm[i].Valor,
                                                Relevante: DetalleForm[i].Relevante,
                                                Cumple: DetalleForm[i].Cumple,
                                                Tamano: DetalleForm[i].Tamano,
                                                CodigoDetalle: DetalleForm[i].Codigo
                                            });

                                        }
                                        if ($scope.ListadoSecciones.length -1 == j) {
                                            $scope.ListadoSecciones[j].ListadoItemSeccion.sort((a, b) => {
                                                return (MascaraNumero(a.Orden) - MascaraNumero(b.Orden));
                                            });
                                        }
                                    }
                                }
                               
                            } else {
                                $scope.ConsultarFormatoInspeccion($scope.Modelo.FormatoInspeccion.Codigo)
                            }
                        } catch (e) {
                            $scope.ConsultarFormatoInspeccion($scope.Modelo.FormatoInspeccion.Codigo)
                        }

                        $scope.CodigoFormato = response.data.Datos.FormatoInspeccion.Codigo
                        if ($scope.ListadoFormatosInspeccion.length > 0 && $scope.CodigoFormato > 0) {
                            $scope.Modelo.FormatoInspeccion = $linq.Enumerable().From($scope.ListadoFormatosInspeccion).First('$.Codigo == ' + response.data.Datos.FormatoInspeccion.Codigo);
                            $scope.HabilitarFirma = $scope.Modelo.FormatoInspeccion.Firma == 1 ? true : false;
                        }

                        if ($scope.HabilitarFirma == true && response.data.Datos.Firma != null) {
                            var image = new Image();
                            var srcImage = "data:image/png;base64," + response.data.Datos.Firma;
                            image.src = srcImage;
                            $scope.ImgFirma = srcImage;
                            var img = cargaContextoCanvas('Firma');
                            img.drawImage(image, 0, 0);
                        }

                        $scope.CodigoConductor = response.data.Datos.Conductor.Codigo;
                        $scope.Modelo.Conductor = $scope.CargarTercero(response.data.Datos.Conductor.Codigo);

                        $scope.CodigoVehiculo = response.data.Datos.Vehiculo.Codigo;
                        $scope.Modelo.Vehiculo = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo);

                        $scope.CodigoSemirremolque = response.data.Datos.Semirremolque.Codigo;
                        if ($scope.ListaSemirremolque !== null && $scope.ListaSemirremolque !== undefined) {
                            if ($scope.ListaSemirremolque.length > 0) {
                                $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListaSemirremolque).First('$.Codigo == ' + response.data.Datos.Semirremolque.Codigo);
                            }
                        };

                        $scope.CodigoTransportador = response.data.Datos.Transportador.Codigo;
                        $scope.Modelo.Transportador = $scope.CargarTercero(response.data.Datos.Transportador.Codigo);

                        $scope.CodigoCliente = response.data.Datos.Cliente.Codigo;
                        $scope.Modelo.Cliente = $scope.CargarTercero(response.data.Datos.Cliente.Codigo);

                        $scope.CodigoSitioCargue = response.data.Datos.SitioCargue.Codigo;
                        if ($scope.CodigoSitioCargue !== null && $scope.CodigoSitioCargue !== undefined && $scope.CodigoSitioCargue !== 0) {
                            if ($scope.ListaSitiosCargueDescargue.length > 0) {
                                $scope.Modelo.SitioCargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo == ' + response.data.Datos.SitioCargue.Codigo);
                            }
                        };

                        $scope.CodigoSitioDescargue = response.data.Datos.SitioDescargue.Codigo;
                        if ($scope.CodigoSitioDescargue !== null && $scope.CodigoSitioDescargue !== undefined && $scope.CodigoSitioDescargue !== 0) {
                            if ($scope.ListaSitiosCargueDescargue.length > 0) {
                                $scope.Modelo.SitioDescargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo == ' + response.data.Datos.SitioDescargue.Codigo);
                            }
                        };

                        $scope.CodigoAutoriza = response.data.Datos.Autoriza.Codigo;
                        $scope.Modelo.Autoriza = $scope.CargarTercero(response.data.Datos.Autoriza.Codigo);

                        $scope.CodigoResponsable = response.data.Datos.Responsable.Codigo;
                        if ($scope.CodigoResponsable == 0) {
                            $scope.CodigoResponsable = $scope.Sesion.UsuarioAutenticado.Empleado.Codigo;
                        }
                        $scope.Modelo.Responsable = $scope.CargarTercero($scope.CodigoResponsable);

                        if (response.data.Datos.Anulado == ESTADO_ANULADO) {
                            $scope.MostrarCanvasFirma = false;
                            $scope.Deshabilitar = true;
                            $scope.ListadoEstados.push({ Nombre: "ANULADO", Codigo: -1 })
                            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
                        }
                        else {
                            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.MostrarCanvasFirma = false;
                                $scope.Deshabilitar = true;
                            }
                            else {
                                $scope.MostrarCanvasFirma = true;
                                $scope.Deshabilitar = false;
                            }
                        }
                    }
                    else {
                        ShowError('No se logro consultar la inspección No. ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarInspeccionPreoperacional';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarInspeccionPreoperacional';
                });

            blockUI.stop();
            //$timeout(function () {
            //    var consultarFotos = {
            //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //        Numero: $scope.Modelo.Numero,
            //    }
            //    DocumentoInspeccionPreoperacionalFactory.Consultar(consultarFotos).
            //        then(function (response) {
            //            if (response.data.Datos.length > 0) {
            //                $scope.list.Fotos = response.data.Datos
            //            }
            //        }, function (response) {
            //            ShowError(response.statusText);
            //        })
            //}, 400)
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            $scope.ValidarDatosDetalle = false

            //if ($scope.Modelo.Cliente.Codigo > 0) {
            //    var FiltroTercero = {
            //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //        Codigo: $scope.Modelo.Cliente.Codigo,
            //        EventoCorreo: 2,
            //    }
            //    TercerosFactory.ConsultarEmailTercero(FiltroTercero).then(function (response) {
            //        if (response.data.ProcesoExitoso == true) {
            //            if (response.data.Datos.length > 0) {
            //                $scope.CorreosClientes = response.data.Datos
            //            }
            //        }
            //    }, function (response) {
            //    })
            //}

            //if ($scope.Modelo.Conductor.Codigo > 0) {
            //    var FiltroTercero = {
            //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //        Codigo: $scope.Modelo.Conductor.Codigo,
            //        EventoCorreo: 2,
            //    }
            //    TercerosFactory.ConsultarEmailTercero(FiltroTercero).then(function (response) {
            //        if (response.data.ProcesoExitoso == true) {
            //            if (response.data.Datos.length > 0) {
            //                $scope.CorreosConductor = response.data.Datos
            //            }
            //        }
            //    }, function (response) {
            //    })
            //}

            //if ($scope.Modelo.Transportador.Codigo > 0) {
            //    var FiltroTercero = {
            //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //        Codigo: $scope.Modelo.Transportador.Codigo,
            //        EventoCorreo: 2,
            //    }
            //    TercerosFactory.ConsultarEmailTercero(FiltroTercero).then(function (response) {
            //        if (response.data.ProcesoExitoso == true) {
            //            if (response.data.Datos.length > 0) {
            //                $scope.CorreosTransportador = response.data.Datos
            //            }
            //        }
            //    }, function (response) {
            //    })
            //}

            if (DatosRequeridos()) {
                if ($scope.ValidarDatosDetalle == true) {
                    showModal('modalConfirmacionDatosDetalle');
                }
                else {
                    showModal('modalConfirmacionGuardar');
                }
            }
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            closeModal('modalConfirmacionDatosDetalle');
            $scope.ListaDetalle = [];

            $scope.ListadoAuxiSecciones = [];

            $scope.ListadoAuxiSecciones = JSON.parse(JSON.stringify($scope.ListadoSecciones));


            $scope.ListadoSecciones.forEach(function (item) {
                $scope.ListaDetalle.push({
                    CodigoEmpresa: item.CodigoEmpresa,
                    SeccionFormulario: item.Codigo,
                    ItemFormulario: 0,
                    Nombre: item.Nombre,
                    OrdenSeccion: item.Orden,
                    OrdenItem: 0,
                    TipoControl: CODIGO_TIPO_CONTROL_NO_APLICA,
                    Valor: '',
                    Relevante: 0,
                    Cumple: item.Cumple == true || item.Cumple == 1 ? 1 : 0,
                    NoAplica: item.NoAplica == true || item.NoAplica == 1 ? 1 : 0,
                    Codigo: item.CodigoDetalle
                })

                item.ListadoItemSeccion.forEach(function (itemDetalle) {
                    if (itemDetalle.TipoControl.Codigo == CODIGO_TIPO_CONTROL_CHECK) {
                        if (itemDetalle.Valor == true) {
                            itemDetalle.Valor = 1
                        } else {
                            itemDetalle.Valor = 0
                        }
                    }
                    if (itemDetalle.Relevante == true) {
                        itemDetalle.Relevante = 1
                    } else {
                        itemDetalle.Relevante = 0
                    }
                    if (itemDetalle.Cumple == true) {
                        itemDetalle.Cumple = 1
                    } else {
                        itemDetalle.Cumple = 0                        
                    }

                    if (itemDetalle.NoAplica == true) {
                        itemDetalle.NoAplica = 1
                    } else {
                        itemDetalle.NoAplica = 0
                        if (itemDetalle.Cumple == 0) {
                            if (itemDetalle.ExigeCumplimiento == 1) {
                                $scope.Modelo.Cumple = 0;
                            }
                        }
                    }

                    $scope.ListaDetalle.push({
                        CodigoEmpresa: itemDetalle.CodigoEmpresa,
                        Codigo: itemDetalle.CodigoDetalle,
                        SeccionFormulario: item.Codigo,
                        ItemFormulario: itemDetalle.Codigo,
                        Nombre: itemDetalle.Nombre,
                        OrdenSeccion: item.Orden,
                        OrdenItem: itemDetalle.Orden,
                        TipoControl: itemDetalle.TipoControl.Codigo,
                        Valor: itemDetalle.Valor,
                        Relevante: itemDetalle.Relevante,
                        Cumple: itemDetalle.Cumple == true || itemDetalle.Cumple == 1 ? 1 : 0,
                        NoAplica: itemDetalle.NoAplica == true || itemDetalle.NoAplica == 1 ? 1 : 0,
                        Tamano: itemDetalle.Tamano,
                    })
                })

            });
            $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_INSPECCIONES

            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
            $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            if ($scope.Modelo.FormatoInspeccion.TipoInspeccion.Codigo != 23101/*Preoperacional*/) {
                $scope.Modelo.FechaInicioHora = RetornarHoraFecha($scope.Modelo.FechaInicioHora, $scope.Modelo.HoraInicio)
                $scope.Modelo.FechaFinHora = RetornarHoraFecha($scope.Modelo.FechaFinHora, $scope.Modelo.HoraFin)
            }
           
            //$scope.Modelo.FechaInspeccion = RetornarFechaEspecificaSinTimeZone($scope.Modelo.FechaInspeccion)

            //$scope.Modelo.FechaAutoriza = RetornarFechaEspecificaSinTimeZone($scope.Modelo.FechaAutoriza)

            $scope.Modelo.Detalle = $scope.ListaDetalle;

            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            if ($scope.HabilitarFirma == true) {
                $scope.Modelo.Firma = image.src.replace(/data:image\/png;base64,/g, '');
            }
            else {
                $scope.Modelo.Firma = undefined;
            }
            //$scope.ListadoAuxiliarFotos = JSON.parse(JSON.stringify($scope.list.Fotos))
            //$scope.list.Fotos.forEach(function (foto) {
            //    foto.Documento = ''
            //})

            //if ($scope.Modelo.EventoCorreo !== undefined && $scope.Modelo.EventoCorreo !== null && $scope.Modelo.EventoCorreo !== '') {
            //    if ($scope.Modelo.EventoCorreo.ListaDistrubicionCorreos !== undefined && $scope.Modelo.EventoCorreo.ListaDistrubicionCorreos !== null && $scope.Modelo.EventoCorreo.ListaDistrubicionCorreos !== '') {
            //        $scope.Modelo.EventoCorreo.ListaDistrubicionCorreos.forEach(function (itemEvento) {
            //            if (itemEvento.Email == CORREO_CLIENTE) {
            //                if ($scope.CorreosClientes.length == 0) {
            //                    itemEvento.Email = ''
            //                }
            //                else {
            //                    $scope.CorreosClientes.forEach(function (itemCond) {
            //                        $scope.Modelo.EventoCorreo.ListaDistrubicionCorreos.push({ Email: itemCond.Correo })
            //                        itemEvento.Email = ''
            //                    })
            //                }
            //            }
            //            else if (itemEvento.Email == CORREO_CONDUCTOR) {
            //                if ($scope.CorreosConductor.length == 0) {
            //                    itemEvento.Email = ''
            //                }
            //                else {
            //                    $scope.CorreosConductor.forEach(function (itemCond) {
            //                        $scope.Modelo.EventoCorreo.ListaDistrubicionCorreos.push({ Email: itemCond.Correo })
            //                        itemEvento.Email = ''
            //                    })
            //                }
            //            }
            //            else if (itemEvento.Email == CORREO_TRANSPORTADOR) {
            //                if ($scope.CorreosTransportador.length == 0) {
            //                    itemEvento.Email = ''
            //                }
            //                else {
            //                    $scope.CorreosTransportador.forEach(function (itemTrans) {
            //                        $scope.Modelo.EventoCorreo.ListaDistrubicionCorreos.push({ Email: itemTrans.Correo })
            //                        itemEvento.Email = ''
            //                    })
            //                }
            //            }
            //        });
            //        $scope.urlASP = '';
            //        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            //        $scope.NombreReporte = 'RepInspeccionPuntoGestion'
            //        $scope.Modelo.EventoCorreo.MensajeCorreo += '  <br />  <br /><a href ="' + $scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&Numero=' + $scope.Modelo.Numero + '&OpcionExportarPdf=' + 1 + '"> Ver Reporte</a>   <br />  <br />  <br />'
            //    }
            //}

            //$scope.Modelo.DetalleFotos = $scope.list.Fotos
            $scope.Modelo.OrdenCargue = {
                Numero: $scope.OrdenCargue.Numero
            }
            $scope.Modelo.Planilla = {
                Numero: $scope.PlanillaDespacho.Numero,
                Manifiesto: { Numero: $scope.PlanillaDespacho.Manifiesto == undefined? 0 : $scope.PlanillaDespacho.Manifiesto.Numero }
            }
            if ($scope.Sesion.UsuarioAutenticado.ManejoValidacionInspecciones) {
                $scope.Modelo.Cumple = $scope.Modelo.Cumple == undefined ? 1 : $scope.Modelo.Cumple;
            } else {
                $scope.Modelo.Cumple = 1;
            }
            $scope.Modelo.Oficina = { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo }

            console.log('Enviando',$scope.Modelo)

            InspeccionPreoperacionalFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Numero === 0) {
                                ShowSuccess('Se guardó la inspección No. ' + response.data.Datos);
                                location.href = '#!ConsultarInspeccionPreoperacional/' + response.data.Datos + '/' +$scope.OPCION_MENU;
                            } else {
                                ShowSuccess('Se modificó la inspección No. ' + $scope.Modelo.Numero);
                                location.href = '#!ConsultarInspeccionPreoperacional/' + $scope.Modelo.Numero + '/'+$scope.OPCION_MENU;
                            }
                        }
                        else {
                            ShowError(response.statusText);
                            $scope.list.Fotos = $scope.ListadoAuxiliarFotos
                            $scope.ListadoSecciones = JSON.parse(JSON.stringify($scope.ListadoAuxiSecciones))
                        }
                    }
                    else {
                        ShowError(response.statusText);
                        $scope.list.Fotos = $scope.ListadoAuxiliarFotos
                        $scope.ListadoSecciones = JSON.parse(JSON.stringify($scope.ListadoAuxiSecciones))
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $scope.ListadoSecciones = JSON.parse(JSON.stringify($scope.ListadoAuxiSecciones))
                    $scope.list.Fotos = $scope.ListadoAuxiliarFotos
                });
        };


        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            $scope.ValidarDatosDetalle = false
            if ($scope.Modelo.CodigoAlterno == undefined || $scope.Modelo.CodigoAlterno == null) {
                $scope.Modelo.CodigoAlterno = ''
            }
            if ($scope.Modelo.FormatoInspeccion == undefined || $scope.Modelo.FormatoInspeccion == '' || $scope.Modelo.FormatoInspeccion == null || $scope.Modelo.FormatoInspeccion.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar el formato inspección');
                continuar = false;
            }
            if ($scope.Modelo.FechaInspeccion == undefined || $scope.Modelo.FechaInspeccion == '' || $scope.Modelo.FechaInspeccion == null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }
            else {
                var FechaInspeccion = RetornarFechaSinHora($scope.Modelo.FechaInspeccion)
                if (FechaInspeccion < $scope.FechaActual) {
                    $scope.MensajesError.push('La fecha debe ser igual o superior a la fecha actual ');
                    continuar = false;
                }
            }
            if ($scope.Modelo.TiempoVigencia == undefined || $scope.Modelo.TiempoVigencia == '' || $scope.Modelo.TiempoVigencia == null) {
                if ($scope.Modelo.FormatoInspeccion.TipoInspeccion.Codigo != 23101/*Preoperacional*/) {
                    $scope.MensajesError.push('Debe ingresar el tiempo vigencia');
                    continuar = false;
                }
            }
            if ($scope.Modelo.Vehiculo == undefined || $scope.Modelo.Vehiculo == '' || $scope.Modelo.Vehiculo == null || $scope.Modelo.Vehiculo.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar placa');
                continuar = false;
            }
            if ($scope.Modelo.Responsable == undefined || $scope.Modelo.Responsable == '' || $scope.Modelo.Responsable == null || $scope.Modelo.Responsable.Codigo == 0) {
                if ($scope.Modelo.FormatoInspeccion.TipoInspeccion.Codigo != 23101/*Preoperacional*/) {
                    $scope.MensajesError.push('Debe  ingresar un responsable');
                    continuar = false;
                }
            }
            if ($scope.Modelo.FechaInicioHora == undefined || $scope.Modelo.FechaInicioHora == '' || $scope.Modelo.FechaInicioHora == null) {
                $scope.MensajesError.push('Debe ingresar la fecha inicio');
                continuar = false;
            }
            if ($scope.Modelo.HoraInicio == undefined || $scope.Modelo.HoraInicio == '' || $scope.Modelo.HoraInicio == null) {
                $scope.MensajesError.push('Debe ingresar la hora inicio');
                continuar = false;
            }
            if ($scope.Modelo.HoraFin == undefined || $scope.Modelo.HoraFin == '' || $scope.Modelo.HoraFin == null) {
                if ($scope.Modelo.FormatoInspeccion.TipoInspeccion.Codigo != 23101/*Preoperacional*/) {

                    $scope.MensajesError.push('Debe ingresar la hora fin');
                    continuar = false;
                }
            }
            if ($scope.Modelo.HoraInicio !== undefined && $scope.Modelo.HoraInicio !== "" && $scope.Modelo.HoraInicio !== null) {
                var HoraInicio = $scope.Modelo.HoraInicio;
                try {
                    var arrHora = HoraInicio.split(":");
                    var hora = arrHora[0];
                    var minutos = arrHora[1];
                    if (parseInt(hora) < 0 || parseInt(hora) > 23) {
                        $scope.MensajesError.push('La hora inicio  no tiene el formato correcto');
                        continuar = false;
                    }
                    if (continuar == true) {
                        if (parseInt(minutos) < 0 || parseInt(minutos) > 59) {
                            $scope.MensajesError.push('La hora inicio no tiene el formato correcto');
                            continuar = false;
                        }
                    }
                }
                catch (err) {
                    if (continuar == false) {
                        $scope.MensajesError.push('La hora inicio no tiene el formato correcto');
                        continuar = false;
                    }
                }
            }
            if ($scope.Modelo.HoraFin !== undefined && $scope.Modelo.HoraFin !== "" && $scope.Modelo.HoraFin !== null) {
                var HoraFin = $scope.Modelo.HoraFin;
                try {
                    var arrHora = HoraFin.split(":");
                    var hora = arrHora[0];
                    var minutos = arrHora[1];
                    if (parseInt(hora) < 0 || parseInt(hora) > 23) {
                        $scope.MensajesError.push('La hora fin  no tiene el formato correcto');
                        continuar = false;
                    }
                    if (continuar == true) {
                        if (parseInt(minutos) < 0 || parseInt(minutos) > 59) {
                            $scope.MensajesError.push('La hora fin no tiene el formato correcto');
                            continuar = false;
                        }
                    }
                }
                catch (err) {
                    if (continuar == false) {
                        $scope.MensajesError.push('La hora fin no tiene el formato correcto');
                        continuar = false;
                    }
                }
            }
            if ($scope.ListadoSecciones.length > 0) {
                $scope.ListadoSecciones.forEach(function (item) {
                    item.ListadoItemSeccion.forEach(function (itemseccion) {
                        if (!itemseccion.Cumple) {
                            $scope.MensajesError.push('Debe Cumplir con el  item ' + item.Nombre + ' en el campo ' + itemseccion.Nombre);
                            continuar = false;
                        }
                        if (itemseccion.TipoControl.Codigo !== CODIGO_TIPO_CONTROL_CHECK) {
                            if (itemseccion.Valor == undefined || itemseccion.Valor == null || itemseccion.Valor == '') {
                                if ($scope.Modelo.FormatoInspeccion.TipoInspeccion.Codigo != 23101/*Preoperacional*/) {
                                    $scope.MensajesError.push('Debe ingresar la información del item ' + item.Nombre + ' en el campo ' + itemseccion.Nombre);
                                    continuar = false;
                                }
                            }
                        }
                        else {
                            if (itemseccion.Valor == false) {
                                itemseccion.Cumple = false
                            }
                        }
                    })
                })
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar;
        } 

        $scope.MarcarCumple = function (detalle) {
            $scope.ListadoSecciones.forEach(function (item) {
                item.ListadoItemSeccion.forEach(function (itemseccion) {
                    if (detalle.$$hashKey == itemseccion.$$hashKey) {
                        if (detalle.Valor == true) {
                            itemseccion.Cumple = true
                        }
                    }
                })
            })
        }

        $scope.LimpiarFirma = function () {
            try {
                var canvas = document.getElementById('Firma');
                var context = canvas.getContext('2d');
                context.clearRect(0, 0, canvas.width, canvas.height);
            } catch (e) {

            }
        };


        $scope.ValidarDocumentos = function (Modelo) {
            if ($scope.Modelo.Vehiculo != undefined) {
                if ($scope.Modelo.Vehiculo.Codigo > 0) {
                    var ResponseOrdenCargue = InspeccionPreoperacionalFactory.ObtenerDocumentosDespachoActivosVehiculo({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo },
                        Sync : true
                    }).Datos;

                    if (ResponseOrdenCargue != undefined) {
                        if (ResponseOrdenCargue.OrdenCargue != null) {
                            $scope.OrdenCargue = ResponseOrdenCargue.OrdenCargue;
                        } else {
                            $scope.OrdenCargue = { Numero: 0 };                            
                        }
                        if (ResponseOrdenCargue.Planilla != null) {
                            $scope.PlanillaDespacho = ResponseOrdenCargue.Planilla;
                        } else {
                            $scope.PlanillaDespacho = {
                                Numero: 0,
                                Manifiesto: { Numero: 0 }
                            };
                        }
                    } else {
                        $scope.OrdenCargue = { Numero: 0 };
                        $scope.PlanillaDespacho = {
                            Numero: 0,
                            Manifiesto: { Numero: 0 }
                        };

                    }
                }
            }
        };

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarInspeccionPreoperacional/' +  $scope.Modelo.Numero + '/' + $scope.OPCION_MENU;
        };

        if (MeapCodigo == INSPECIONES_GESPHONE) {  

            //Buscar valores del conductor logueado
            var Response = VehiculosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Conductor: $scope.Sesion.UsuarioAutenticado.Conductor,
                Estado: ESTADO_ACTIVO,
                Sync: true
            });


            //ASIGNAR VALORES 
            $scope.Modelo.Vehiculo = Response.Datos[0]
            $scope.Modelo.Conductor = $scope.Sesion.UsuarioAutenticado.Nombre 
            //LLAMAR FUNCIONES
            $scope.CargarSemirremolque($scope.Modelo.Vehiculo);
            $scope.ValidarDocumentos($scope.Modelo);

            for (var i = 0; i < $scope.ListadoFormatosInspeccion.length; i++) {

                if ($scope.ListadoFormatosInspeccion[i].Codigo == 22) {
                    $scope.Modelo.FormatoInspeccion = $scope.ListadoFormatosInspeccion[i]
                }

            }

            $scope.ConsultarFormatoInspeccion($scope.Modelo.FormatoInspeccion.Codigo);

        }

        //$scope.LimpiarFirma = function () {
        //    var canvas = document.getElementById('Firma');
        //    var context = canvas.getContext("2d");
        //    context.clearRect(0, 0, canvas.width, canvas.height);
        //}

        //var canvas = document.getElementById('Firma');
        //var image = new Image();
        //image.src = canvas.toDataURL("image/png");

        //function RedimVertical(img) {
        //    Se carga el contexto vans donde se redimencionara la imagen
        //    var ctx = cargaContextoCanvas('CanvasVertical');
        //    if (ctx) {
        //         dentro del canvaz se dibija la imagen que se recive por parametro
        //        ctx.drawImage(img, 0, 0, 499, 699);
        //    }
        //}
        //function RedimHorizontal(img) {
        //    Se carga el contexto vans donde se redimencionara la imagen
        //    var ctx = cargaContextoCanvas('CanvasHorizontal');
        //    if (ctx) {
        //         dentro del canvaz se dibija la imagen que se recive por parametro
        //        ctx.drawImage(img, 0, 0, 699, 499);
        //    }
        //}

        //$scope.MostraPDF = function (PDF) {
        //    if ($scope.EncabezadoCheckList.Numero == 0) {
        //        if ($scope.TipoImagen == 'PDF') {
        //            window.open("data:application/pdf;base64, " + $scope.Convertirfoto);
        //            $scope.GuardarDocumento = 1
        //        }
        //    } else {
        //        if ($scope.EncabezadoCheckList.Numero > 0) {
        //            window.open("data:application/pdf;base64, " + PDF);
        //            $scope.GuardarDocumento = 1
        //        }
        //    }
        //};

        $scope.MaskPlaca = function () {
            try { $scope.Modelo.Vehiculo = MascaraPlaca($scope.Modelo.Vehiculo) } finally { }
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.FormatoInspeccion = $scope.Modelo.FormatoInspeccion.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.TiempoVigencia = MascaraNumero($scope.Modelo.TiempoVigencia) } catch (e) { }
        };
        $scope.MaskHora = function () {
            try { $scope.Modelo.HoraInicio = MascaraHora($scope.Modelo.HoraInicio) } catch (e) { }
            try { $scope.Modelo.HoraFin = MascaraHora($scope.Modelo.HoraFin) } catch (e) { }
        };
        $scope.MaskHoraGrid = function (value) {
            return MascaraHora(value)
        };
        $scope.MaskNumeroGird = function (value) {
            return MascaraNumero(value)
        }

    }]);