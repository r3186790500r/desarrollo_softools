﻿PRINT 'gsp_consultar_producto_transportados'
GO
DROP PROCEDURE gsp_consultar_producto_transportados
GO
CREATE PROCEDURE gsp_consultar_producto_transportados
(@par_EMPR_Codigo smallint,
@par_Estado smallint)
AS
BEGIN
select
EMPR_Codigo,
Codigo,
ISNULL(Nombre,'') as Nombre,
Estado,
ISNULL(Codigo_Alterno,'') as Codigo_Alterno,
Descripcion

from Producto_Transportados
where EMPR_Codigo = @par_EMPR_Codigo
and Estado = @par_Estado
END
GO
