﻿PRINT 'gsp_obtener_encabezado_Concepto_contables'
GO
DROP PROCEDURE gsp_obtener_encabezado_Concepto_contables
GO
CREATE PROCEDURE gsp_obtener_encabezado_Concepto_contables
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC
)
AS
BEGIN

	SELECT 
		1 AS Obtener,
		ENCC.EMPR_Codigo,
		ENCC.Codigo,
		ENCC.Codigo_Alterno,
		ENCC.Nombre,
		ENCC.TIDO_Codigo,
		ENCC.CATA_DOOR_Codigo,
		ENCC.CATA_TICC_Codigo,
		ENCC.CATA_TINA_Codigo,
		ENCC.OFIC_Codigo_Aplica,
		ENCC.Observaciones,
		ENCC.OFIC_Codigo,
		ENCC.Fuente,
		ENCC.Fuente_Anulacion,
		ENCC.Estado,
		DOOR.Nombre AS DocumentoOrigen ,
		TICC.Nombre AS TipoConceptoContable,
		OFAP.Nombre AS OficinaAPlica
	FROM
		Encabezado_Concepto_Contables ENCC INNER JOIN V_Documentos_Origen DOOR ON 
		ENCC.EMPR_Codigo = DOOR.EMPR_Codigo
		AND ENCC.CATA_DOOR_Codigo= DOOR.Codigo

		LEFT JOIN 
		V_Tipo_Concepto_Contable TICC ON 
		ENCC.EMPR_Codigo = TICC.EMPR_Codigo
		AND ENCC.CATA_TICC_Codigo = TICC.Codigo

		LEFT JOIN 
		Valor_Catalogos TINA ON
		ENCC.EMPR_Codigo = TINA.EMPR_Codigo
		AND ENCC.CATA_TINA_Codigo = TINA.Codigo

		LEFT JOIN 
		Oficinas OFAP ON 
		ENCC.EMPR_Codigo = OFAP.EMPR_Codigo
		AND ENCC.OFIC_Codigo_Aplica = OFAP.Codigo	

	WHERE 
		ENCC.EMPR_Codigo = @par_EMPR_Codigo
		AND ENCC.Codigo = @par_Codigo

END
GO