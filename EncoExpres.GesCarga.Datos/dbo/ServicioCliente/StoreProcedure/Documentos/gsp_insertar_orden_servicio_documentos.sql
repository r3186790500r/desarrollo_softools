﻿PRINT 'gsp_insertar_orden_servicio_documentos' 
GO
DROP PROCEDURE gsp_insertar_orden_servicio_documentos
GO
CREATE PROCEDURE gsp_insertar_orden_servicio_documentos 
(      
  @par_USUA_Codigo SMALLINT,      
  @par_EMPR_Codigo SMALLINT,            
  @par_Numero NUMERIC = NULL,     
  @par_TIDO_Codigo NUMERIC = NULL,      
  @par_CDGD_Codigo NUMERIC = NULL,                
  @par_Referencia VARCHAR(50) = NULL,     
  @par_Emisor VARCHAR(50) = NULL,      
  @par_Fecha_Emision DATE = NULL,     
  @par_Fecha_Vence DATE = NULL,     
  @par_Documento VARBINARY(MAX),            
  @par_Nombre_Documento VARCHAR(50) = NULL,          
  @par_Extension_Documento CHAR(5) = NULL  
  
)        
AS        
BEGIN   

DECLARE @ValorCondicionDefinitivo INT  
 SELECT @ValorCondicionDefinitivo = (  
  
 SELECT DISTINCT   
 COUNT(*)   
 FROM  
   
 Gestion_Documental_Documentos AS GEDD 
  
 WHERE  
  
 GEDD.EMPR_Codigo = @par_EMPR_Codigo                  
 AND GEDD.Numero = @par_Numero  
 AND GEDD.CDGD_Codigo = @par_CDGD_Codigo ) 

DECLARE @ValorCondicion INT  
 SELECT @ValorCondicion = (  
  
 SELECT DISTINCT   
 COUNT(*)   
 FROM  
   
 T_Gestion_Documental_Documentos AS TGDD
  
 WHERE  
  
 TGDD.EMPR_Codigo = @par_EMPR_Codigo                  
 AND TGDD.Numero = @par_Numero  
 AND TGDD.USUA_Codigo = @par_USUA_Codigo  
 AND TGDD.TIDO_Codigo = @par_TIDO_Codigo  
 AND TGDD.CDGD_Codigo = @par_CDGD_Codigo ) 
    
IF (@ValorCondicionDefinitivo > 0) 

     BEGIN  
  
 DELETE Gestion_Documental_Documentos  WHERE  
  
 EMPR_Codigo = @par_EMPR_Codigo                  
 AND Numero = @par_Numero  
 AND CDGD_Codigo = @par_CDGD_Codigo  
  
  INSERT INTO        
  T_Gestion_Documental_Documentos     
  (    
 USUA_Codigo,      
EMPR_Codigo,   
Numero,   
TIDO_Codigo,   
CDGD_Codigo,  
Referencia,
Emisor,
Fecha_Emision,
Fecha_Vence,     
Archivo,    
Nombre_Documento,    
Extension_Documento,
Fecha_Crea 
  )        
 VALUES        
 (    
  @par_USUA_Codigo,     
  @par_EMPR_Codigo,          
  @par_Numero,  
  @par_TIDO_Codigo,        
  @par_CDGD_Codigo,
  ISNULL(@par_Referencia, 0),   
  ISNULL(@par_Emisor, 0),     
  ISNULL(@par_Fecha_Emision, ''),   
  ISNULL(@par_Fecha_Vence, ''),                
  @par_Documento,          
  ISNULL(@par_Nombre_Documento, ''),          
  ISNULL(@par_Extension_Documento, ''),
  GETDATE()  
 )              
        
  SELECT @@ROWCOUNT AS Codigo    
 END  

ELSE IF (@ValorCondicion > 0)   

     BEGIN  
  
 DELETE T_Gestion_Documental_Documentos  WHERE  
  
 EMPR_Codigo = @par_EMPR_Codigo                  
 AND Numero = @par_Numero  
 AND USUA_Codigo = @par_USUA_Codigo  
 AND TIDO_Codigo = @par_TIDO_Codigo  
 AND CDGD_Codigo = @par_CDGD_Codigo  
  
  INSERT INTO        
  T_Gestion_Documental_Documentos     
  (    
 USUA_Codigo,      
EMPR_Codigo,   
Numero,   
TIDO_Codigo,   
CDGD_Codigo,  
Referencia,
Emisor,
Fecha_Emision,
Fecha_Vence,     
Archivo,    
Nombre_Documento,    
Extension_Documento,
Fecha_Crea 
  )        
 VALUES        
 (    
  @par_USUA_Codigo,     
  @par_EMPR_Codigo,          
  @par_Numero,  
  @par_TIDO_Codigo,        
  @par_CDGD_Codigo,
  ISNULL(@par_Referencia, 0),   
  ISNULL(@par_Emisor, 0),     
  ISNULL(@par_Fecha_Emision, ''),   
  ISNULL(@par_Fecha_Vence, ''),                
  @par_Documento,          
  ISNULL(@par_Nombre_Documento, ''),          
  ISNULL(@par_Extension_Documento, ''),
  GETDATE()  
 )        
        
  SELECT @@ROWCOUNT AS Codigo    
 END  
  
ELSE IF (@ValorCondicion = 0)  

     BEGIN  
 INSERT INTO        
  T_Gestion_Documental_Documentos     
  (    
 USUA_Codigo,      
EMPR_Codigo,   
Numero,
TIDO_Codigo,        
CDGD_Codigo,   
Referencia,
Emisor,
Fecha_Emision,
Fecha_Vence,    
Archivo,    
Nombre_Documento,    
Extension_Documento,
Fecha_Crea 
  )        
 VALUES        
 (    
  @par_USUA_Codigo,     
  @par_EMPR_Codigo,          
  @par_Numero,
  @par_TIDO_Codigo,            
  @par_CDGD_Codigo, 
  ISNULL(@par_Referencia, 0),   
  ISNULL(@par_Emisor, 0),     
  ISNULL(@par_Fecha_Emision, ''),   
  ISNULL(@par_Fecha_Vence, ''),                  
  @par_Documento,          
  ISNULL(@par_Nombre_Documento, ''),          
  ISNULL(@par_Extension_Documento, ''),
  GETDATE()    
 )        
        
  SELECT @@ROWCOUNT AS Codigo    
   END  
     
END        
GO