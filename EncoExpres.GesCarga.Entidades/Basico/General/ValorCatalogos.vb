﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="ValorCatalogos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ValorCatalogos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ValorCatalogos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ValorCatalogos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Catalogo = New Catalogos With {.Codigo = Read(lector, "CATA_Codigo"), .Nombre = Read(lector, "Nombre")}
            Nombre = Read(lector, "Campo1")
            CampoAuxiliar2 = Read(lector, "Campo2")
            CampoAuxiliar3 = Read(lector, "Campo3")
            CampoAuxiliar4 = Read(lector, "Campo4")
            CampoAuxiliar5 = Read(lector, "Campo5")

        End Sub
        ''' <summary>
        ''' Obtiene o establece el catalogo asociado al item
        ''' </summary>
        <JsonProperty>
        Public Property Catalogo As Catalogos
        ''' <summary>
        ''' Obtiene o establece el id del registro para el item del catalogo
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty>
        Public Property CampoAuxiliar2 As String
        ''' <summary>
        ''' Obtiene o establece el nombre del registro para el item del catalogo
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty>
        Public Property Nombre As String
        ''' <summary>
        ''' Obtiene o establece un valor auxiliar para el item del catalogo
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty>
        Public Property CampoAuxiliar3 As String
        ''' <summary>
        ''' Obtiene o establece un valor auxiliar para el item del catalogo
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty>
        Public Property CampoAuxiliar4 As String
        ''' <summary>
        ''' Obtiene o establece un valor auxiliar para el item del catalogo
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty>
        Public Property CampoAuxiliar5 As String
        <JsonProperty>
        Public Property CodigoAlteno As Integer


    End Class
End Namespace
