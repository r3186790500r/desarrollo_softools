﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_Reporte_Encabezado_Manifiesto'

DROP PROCEDURE gsp_Reporte_Encabezado_Manifiesto
GO

CREATE PROCEDURE gsp_Reporte_Encabezado_Manifiesto(
@par_EMPR_Numero NUMERIC,
@par_Numero NUMERIC
)
AS BEGIN 
SELECT 
EMPR.Nombre_Razon_Social AS NombreEmpresa
,EMPR.Numero_Identificacion AS Nit_Empresa
,EMPR.Direccion AS DireccionEmpresa 
,CIEM.Nombre AS CiudadEmpresa
,EMPR.Telefonos AS TelefonoEmpresa 
,EMPR.Codigo_Alterno AS CodigoInterno
,ENMA.Numero_Manifiesto_Electronico 
,ENMA.Numero AS NumeroInternoManifiesto 
,ENMA.Fecha_Crea AS FechaExpedicion
,CIOR.Nombre AS CiudadOrigen
,CIDE.Nombre AS CiudadDestino
,DATEADD(DAY,15,ENMA.Fecha_Crea) AS FechaLimite 
,ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'')  
+' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS TitutlarManifiesto
,TENE.Numero_Identificacion AS IdentificacionTitular 
,TENE.Direccion AS DireccionTitular  
,TENE.Telefonos AS TelefonoTitular
,CITE.Nombre AS CiudadTitular
,RUTA.Nombre AS NombreRuta
,VEHI.Placa AS PlacaVehiculo
,MAVE.Nombre AS MarcaVehiculo
,SEMI.Placa AS PlacaSemiremolque 
,ISNULL(TEPR.Razon_Social,'')+' '+ISNULL(TEPR.Nombre,'')  
+' '+ISNULL(TEPR.Apellido1,'')+' '+ISNULL(TEPR.Apellido2,'') AS TitutlarPropietario
,TEPR.Numero_Identificacion AS IdentificacionPropietario 
,TEPR.Direccion AS DireccionPropietario
,TEPR.Telefonos AS TelefonoPropietario
,CIPR.Nombre AS CiudadPropietario
,ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'')  
+' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS TitutlarTenedor
,TENE.Numero_Identificacion AS IdentificacionTenedor
,TENE.Direccion AS DireccionTenedor 
,TENE.Telefonos AS TelefonoTitular
,CITE.Nombre AS CiudadTenedor
,ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')  
+' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS TitutlarConductor
,TECO.Numero_Identificacion AS IdentificacionConductor
,TECO.Direccion AS DireccionConductor
,TECO.Telefonos AS TelefonoConductor
,CICO.Nombre AS CiudadConductor
,ENMA.Valor_Flete AS ValoPagarPactado
,dbo.funcCantidadConLetra(ENMA.Valor_Flete) AS ValoPagarPactadoletra
,ENMA.Valor_Flete_Neto AS ValorNetoPagar
,ENMA.Valor_Anticipo AS ValorAnticipo
,ENMA.Valor_Pagar AS SaldoPagar
,ENMA.Fecha_Cumplimiento AS FechaPagoSaldo
,CIDE.Nombre AS LugarSaldoPaga
,ISNULL(POLI.Razon_Social,'')+' '+ISNULL(POLI.Nombre,'')  
+' '+ISNULL(POLI.Apellido1,'')+' '+ISNULL(POLI.Apellido2,'') AS CompañiaSeguro
,ENMA.Fecha_Vigencia_Poliza AS VencimientoPoliza 
,ENMA.Numero_Poliza AS NumeroPoliza
,ENMA.Valor_ICA AS ICA
,ENMA.Valor_Retencion_Fuente AS ReteFuente
,EMPR.Prefijo PreEmpresa
FROM 
Encabezado_Manifiesto_Carga ENMA
INNER JOIN Empresas EMPR ON 
ENMA.EMPR_Codigo = EMPR.Codigo 

INNER JOIN Oficinas OFIC ON
ENMA.EMPR_Codigo = OFIC.EMPR_Codigo 
AND ENMA.OFIC_Codigo = OFIC.Codigo

INNER JOIN Terceros TEPR ON 
ENMA.EMPR_Codigo = TEPR.EMPR_Codigo 
AND ENMA.TERC_Codigo_Propietario = TEPR.Codigo

INNER JOIN Terceros TENE ON 
ENMA.EMPR_Codigo = TENE.EMPR_Codigo 
AND ENMA.TERC_Codigo_Tenedor = TENE.Codigo

INNER JOIN Terceros TECO ON 
ENMA.EMPR_Codigo = TECO.EMPR_Codigo 
AND ENMA.TERC_Codigo_Conductor = TECO.Codigo

INNER JOIN Terceros POLI ON 
ENMA.EMPR_Codigo = POLI.EMPR_Codigo 
AND ENMA.TERC_Codigo_Aseguradora = POLI.Codigo

INNER JOIN Rutas RUTA ON 
ENMA.EMPR_Codigo = RUTA.EMPR_Codigo 
AND ENMA.RUTA_Codigo = RUTA.Codigo 

INNER JOIN  Ciudades CIEM ON 
EMPR.Codigo = CIEM.EMPR_Codigo 
AND EMPR.CIUD_Codigo = CIEM.Codigo

INNER JOIN Ciudades CIOR ON 
RUTA.EMPR_Codigo = CIOR.EMPR_Codigo
AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo

INNER JOIN Ciudades CIDE ON 
RUTA.EMPR_Codigo = CIDE.EMPR_Codigo
AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo

INNER JOIN Ciudades CIPR ON 
TEPR.EMPR_Codigo = CIPR.EMPR_Codigo 
AND TEPR.CIUD_Codigo = CIPR.Codigo

INNER JOIN Ciudades CITE ON 
TENE.EMPR_Codigo = CITE.EMPR_Codigo 
AND TENE.CIUD_Codigo = CITE.Codigo

INNER JOIN Ciudades CICO ON 
TECO.EMPR_Codigo = CICO.EMPR_Codigo
AND TECO.CIUD_Codigo = CICO.Codigo

INNER JOIN Vehiculos VEHI ON 
ENMA.EMPR_Codigo = VEHI.EMPR_Codigo
AND ENMA.VEHI_Codigo = VEHI.Codigo

INNER JOIN Marca_Vehiculos MAVE ON 
VEHI.EMPR_Codigo = MAVE.EMPR_Codigo
AND VEHI.MAVE_Codigo = MAVE.Codigo

LEFT JOIN  Semirremolques SEMI
ON VEHI.EMPR_Codigo = SEMI.EMPR_Codigo 
AND VEHI.SEMI_Codigo = SEMI.Codigo

WHERE 
ENMA.EMPR_Codigo = @par_EMPR_Numero 
AND ENMA.Numero = @par_Numero
END
GO