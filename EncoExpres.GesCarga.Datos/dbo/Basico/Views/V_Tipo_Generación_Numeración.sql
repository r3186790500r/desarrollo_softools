﻿Print 'V_Tipo_Generación_Numeración'
GO
DROP VIEW V_Tipo_Generación_Numeración
GO
CREATE VIEW V_Tipo_Generación_Numeración 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 11
GO