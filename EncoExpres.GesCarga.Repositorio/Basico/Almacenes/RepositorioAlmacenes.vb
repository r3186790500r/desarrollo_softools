﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen

Namespace Basico.Almacen
    ''' <summary>
    ''' Clase <see cref="RepositorioAlmacenes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioAlmacenes
        Inherits RepositorioBase(Of Almacenes)

        Public Overrides Function Consultar(filtro As Almacenes) As IEnumerable(Of Almacenes)
            Dim lista As New List(Of Almacenes)
            Dim item As Almacenes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.NombreCiudad) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Ciudad", filtro.NombreCiudad)
                End If
                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Oficina", filtro.Oficina.Codigo)
                    End If
                End If
                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_almacenes]")

                While resultado.Read
                    item = New Almacenes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Almacenes) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
                conexion.AgregarParametroSQL("@par_Contacto", entidad.Contacto)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo", entidad.Ciudad.Codigo)
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Celular", entidad.Celular)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_almacenes")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As Almacenes) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
                conexion.AgregarParametroSQL("@par_Contacto", entidad.Contacto)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo", entidad.Ciudad.Codigo)
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Celular", entidad.Celular)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_almacenes")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As Almacenes) As Almacenes
            Dim item As New Almacenes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_almacenes]")

                While resultado.Read
                    item = New Almacenes(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As Almacenes) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_almacenes]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class

End Namespace

