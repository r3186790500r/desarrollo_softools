﻿PRINT 'gsp_cumplido_remesas'
GO

DROP PROCEDURE gsp_cumplido_remesas
GO

CREATE PROCEDURE gsp_cumplido_remesas
(
	@par_EMPR_Codigo NUMERIC ,   
	@par_ECPD_Numero NUMERIC   
)
AS
BEGIN
	SELECT
		DEPD.EMPR_Codigo
		,DEPD.ENPD_Numero AS NumeroPlanilla
		,ENRE.Numero AS NumeroRemesa
		,ENRE.Numero_Documento AS NumeroDocumentoRemesa
		,ENRE.Fecha AS FechaRemesa
		,RUTA.Nombre AS NombreRuta
		,LTRIM(RTRIM(CONCAT(CLIE.Nombre,' ',CLIE.Apellido1,' ',CLIE.Apellido2,' ',CLIE.Razon_Social))) AS NombreCliente
		,ISNULL(ENRE.Documento_Cliente,'') AS Documento_Cliente
		,ENRE.Total_Flete_Cliente
		,PRTR.Nombre AS NombreProducto
		,ENRE.Cantidad_Cliente AS CantidadRemesa
		,ENRE.Peso_Cliente AS PesoRemesa		
		,ISNULL(CURE.Numero_Identificacion_Recibe,'') AS Numero_Identificacion_Recibe
		,ISNULL(CURE.Nombre_Recibe,'') AS Nombre_Recibe
		,ISNULL(CURE.Telefonos_Recibe,'') AS Telefonos_Recibe
		,ISNULL(CURE.Fecha_Recibe,'') AS Fecha_Recibe
		,ISNULL(CURE.Cantidad_Recibe,0) AS Cantidad_Recibe
		,ISNULL(CURE.Peso_Recibe,0) AS Peso_Recibe
		,ISNULL(CURE.Observaciones_Recibe,'') AS Observaciones_Recibe
		,ISNULL(ENRE.Cumplido,0) AS Cumplido
	FROM
		Detalle_Planilla_Despachos DEPD
	
		INNER JOIN Encabezado_Remesas ENRE
		ON DEPD.EMPR_Codigo = ENRE.EMPR_Codigo AND DEPD.ENRE_Numero = ENRE.Numero
	 
		INNER JOIN Producto_Transportados PRTR
		ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo AND ENRE.PRTR_Codigo = PRTR.Codigo

		INNER JOIN Terceros CLIE
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo
	
		INNER JOIN Terceros DEST
		ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo

		INNER JOIN Rutas RUTA
		ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo AND ENRE.RUTA_Codigo = RUTA.Codigo

		LEFT JOIN Cumplido_Remesas CURE
		ON ENRE.EMPR_Codigo = CURE.EMPR_Codigo AND ENRE.Numero = CURE.ENRE_Numero

		LEFT JOIN Encabezado_Cumplido_Planilla_Despachos ECPD
		ON DEPD.EMPR_Codigo = ECPD.EMPR_Codigo AND DEPD.ENPD_Numero = ECPD.ENPD_Numero

	WHERE
		DEPD.EMPR_Codigo = @par_EMPR_Codigo
		AND ECPD.Numero = @par_ECPD_Numero
END
GO