﻿PRINT 'gsp_consultar_detalle_distribucion_remesas'
GO
DROP PROCEDURE gsp_consultar_detalle_distribucion_remesas
GO 
CREATE PROCEDURE gsp_consultar_detalle_distribucion_remesas 
(            
 @par_EMPR_Codigo SMALLINT,       
 @par_ENRE_Numero NUMERIC = NULL         
)            
AS            
BEGIN                
 SELECT DISTINCT           
  DEDR.EMPR_Codigo,            
  DEDR.ID,            
  DEDR.ENRE_Numero,       
  ISNULL(DEDR.PRTR_Codigo, 0) AS PRTR_Codigo,    
  ISNULL(PRTR.Nombre, '') AS Nombre_Producto,    
  ISNULL(DEDR.UEPT_Codigo, 0) AS UEPT_Codigo,    
  ISNULL(UEPT.Descripcion, '') AS Nombre_Unidad_Empaque,   
  ISNULL(DEDR.Cantidad, 0) AS Cantidad,    
  ISNULL(DEDR.Peso, 0) AS Peso,     
  ISNULL(DEDR.Documento_Cliente, 0) AS Documento_Cliente,     
  ISNULL(DEDR.Fecha_Documento_Cliente, '') AS Fecha_Documento_Cliente,    
  ISNULL(DEDR.TERC_Codigo_Destinatario, 0) AS TERC_Codigo_Destinatario,     
  ISNULL(DEDR.CATA_TIID_Codigo, 0) AS CATA_TIID_Codigo,
  ISNULL(DEDR.Numero_Identificacion_Destinatario, '') AS Numero_Identificacion_Destinatario,     
  ISNULL(DEDR.Nombre_Destinatario, '') AS Nombre_Destinatario,     
  ISNULL(DEDR.CIUD_Codigo_Destinatario, 0) AS CIUD_Codigo_Destinatario,    
  ISNULL(CIUD.Nombre, '') AS Nombre_Ciudad,    
  ISNULL(DEDR.ZOCI_Codigo_Destinatario, 0) AS ZOCI_Codigo_Destinatario,    
  ISNULL(ZOCI.Nombre, '') AS Nombre_Zona,   
  ISNULL(DEDR.Barrio_Destinatario, '') AS Barrio_Destinatario,    
  ISNULL(DEDR.Direccion_Destinatario, 0) AS Direccion_Destinatario,     
  CASE WHEN DEDR.Codigo_Postal_Destinatario = '' THEN 0 ELSE ISNULL(DEDR.Codigo_Postal_Destinatario, 0) END AS Codigo_Postal_Destinatario,     
  ISNULL(DEDR.Telefonos_Destinatario, '') AS Telefonos_Destinatario,     
  ISNULL(DEDR.Observaciones_Entrega, '') AS Observaciones_Entrega,     
  ISNULL(DEDR.CATA_ESDR_Codigo, 0) AS CATA_ESDR_Codigo,   
  ISNULL(VACA.Campo1,'') As Nombre_Estado,    
  ISNULL(DEDR.USUA_Codigo_Crea, 0) AS USUA_Codigo_Crea,     
  ISNULL(USUA.Codigo_Usuario,'') As Nombre_Usuario_Crea,    
  ISNULL(DEDR.Fecha_Crea,'') As Fecha_Crea   
       
 FROM            
 Detalle_Distribucion_Remesas as DEDR      
   
 LEFT JOIN Producto_Transportados AS PRTR      
 ON DEDR.EMPR_Codigo = PRTR.EMPR_Codigo      
 AND DEDR.PRTR_Codigo = PRTR.Codigo      
    
 LEFT JOIN Unidad_Empaque_Producto_Transportados AS UEPT      
 ON DEDR.EMPR_Codigo = UEPT.EMPR_Codigo      
 AND DEDR.UEPT_Codigo = UEPT.Codigo      
      
 LEFT JOIN Ciudades AS CIUD      
 ON DEDR.EMPR_Codigo = CIUD.EMPR_Codigo      
 AND DEDR.CIUD_Codigo_Destinatario = CIUD.Codigo      
  
 LEFT JOIN Zona_Ciudades AS ZOCI      
 ON DEDR.EMPR_Codigo = ZOCI.EMPR_Codigo      
 AND DEDR.CIUD_Codigo_Destinatario = ZOCI.Codigo     
  
 LEFT JOIN Valor_Catalogos AS VACA      
 ON DEDR.EMPR_Codigo = VACA.EMPR_Codigo      
 AND DEDR.CATA_ESDR_Codigo = VACA.Codigo   
    
 LEFT JOIN Usuarios AS USUA      
 ON DEDR.EMPR_Codigo = USUA.EMPR_Codigo      
 AND DEDR.USUA_Codigo_Crea = USUA.Codigo      
      
  WHERE            
   DEDR.EMPR_Codigo = @par_EMPR_Codigo            
   AND DEDR.ENRE_Numero = ISNULL(@par_ENRE_Numero, DEDR.ENRE_Numero)       
END      
GO