﻿Print 'gsp_consultar_terceros_SIPLAFT'
GO
DROP PROCEDURE gsp_consultar_terceros_SIPLAFT
GO
CREATE PROCEDURE gsp_consultar_terceros_SIPLAFT (

	@par_EMPR_Codigo	smallint,
	@par_Fecha_Inicial	date,
	@par_Fecha_Final	date
)

 AS
 BEGIN


 -- Consulta el valor de transacciones limite total para reportar los terceros
 DECLARE @ValorTransaccion MONEY = 0
 SELECT @ValorTransaccion = ISNULL(Valor_Transaccion_SIPLAFT, 0) FROM Empresas WHERE Codigo = @par_EMPR_Codigo


	 DECLARE  @tblTerceros_SIPLAFT TABLE (
		EMPR_Codigo			SMALLINT,
		TERC_Codigo			NUMERIC,
		Iden_Tercero		VARCHAR(20),
		CATA_TIID_Codigo	INT,
		Valor_Flete_Cliente	MONEY
	 )


	 --- 1. Clientes
	 INSERT INTO @tblTerceros_SIPLAFT
	 SELECT      
	  ENRE.EMPR_Codigo, ENRE.TERC_Codigo_Cliente, CLIE.Numero_Identificacion, CLIE.CATA_TIID_Codigo, SUM(ENRE.Valor_Flete_Cliente)      
	 FROM      
	  Encabezado_Remesas ENRE, Terceros CLIE      
	 WHERE      
	  ENRE.EMPR_Codigo = CLIE.EMPR_Codigo      
	  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo      
	  AND ENRE.EMPR_Codigo = @par_EMPR_Codigo
	  AND ENRE.Estado = 1      
	  AND ENRE.Anulado = 0    
	  AND ENRE.TIDO_Codigo IN (100, 110)
	  AND ENRE.Fecha >= CONVERT(DATE,@par_Fecha_Inicial,101)      
	  AND ENRE.Fecha <= CONVERT(DATE,@par_Fecha_Final,101)      
	 GROUP BY      
	  ENRE.EMPR_Codigo, ENRE.TERC_Codigo_Cliente, CLIE.Numero_Identificacion, CLIE.CATA_TIID_Codigo      


	  ---- 2. Tenedores
	 INSERT INTO @tblTerceros_SIPLAFT   
	 SELECT      
	  ENRE.EMPR_Codigo, VEHI.TERC_Codigo_Tenedor, TERC.Numero_Identificacion, TERC.CATA_TIID_Codigo, SUM(ENRE.Valor_Flete_Transportador)      
	 FROM      
	  Encabezado_Remesas ENRE, Terceros TERC, Vehiculos VEHI      
	 WHERE      
	  ENRE.EMPR_Codigo = VEHI.EMPR_Codigo      
	  AND ENRE.VEHI_Codigo = VEHI.Codigo
	  AND VEHI.EMPR_Codigo = TERC.EMPR_Codigo      
	  AND VEHI.TERC_Codigo_Tenedor = TERC.Codigo      
	  AND ENRE.EMPR_Codigo = @par_EMPR_Codigo
	  AND ENRE.Estado = 1      
	  AND ENRE.Anulado = 0      
	  AND VEHI.CATA_TIDV_Codigo = 2101 --Sólo tenedores de vehículos que no sean propios      
	  AND ENRE.TIDO_Codigo IN (100, 110)
	  AND VEHI.Codigo > 0
	  AND ENRE.Fecha >= CONVERT(DATETIME,@par_Fecha_Inicial,101)      
	  AND ENRE.Fecha <= CONVERT(DATETIME,@par_Fecha_Final,101)      
	 GROUP BY      
	  ENRE.EMPR_Codigo, VEHI.TERC_Codigo_Tenedor, TERC.Numero_Identificacion, TERC.CATA_TIID_Codigo  


	  -- 3. Remitente/Cliente
	  INSERT INTO @tblTerceros_SIPLAFT
	  SELECT      
	  ENRE.EMPR_Codigo, ENRE.TERC_Codigo_Cliente, TERC.Numero_Identificacion, TERC.CATA_TIID_Codigo, SUM(ENRE.Valor_Flete_Cliente)      
	 FROM      
	  Encabezado_Remesas ENRE, Terceros TERC
	 WHERE      
	  ENRE.EMPR_Codigo = TERC.EMPR_Codigo
	  AND ENRE.TERC_Codigo_Cliente = TERC.Codigo
	  AND ENRE.EMPR_Codigo = @par_EMPR_Codigo
	  AND ENRE.Estado = 1      
	  AND ENRE.Anulado = 0      
	  AND ENRE.TIDO_Codigo IN (100, 110)
   
	  AND ENRE.Fecha >= CONVERT(DATETIME,@par_Fecha_Inicial,101)      
	  AND ENRE.Fecha <= CONVERT(DATETIME,@par_Fecha_Final,101)      
	 GROUP BY      
	  ENRE.EMPR_Codigo, ENRE.TERC_Codigo_Cliente, TERC.Numero_Identificacion, TERC.CATA_TIID_Codigo  


	  -- 4. Destinatario
	 INSERT INTO @tblTerceros_SIPLAFT
	 SELECT      
	  ENRE.EMPR_Codigo, ENRE.TERC_Codigo_Destinatario, TERC.Numero_Identificacion, TERC.CATA_TIID_Codigo, SUM(ENRE.Valor_Flete_Cliente)      
	 FROM      
	  Encabezado_Remesas ENRE, Terceros TERC
	 WHERE      
	  ENRE.EMPR_Codigo = TERC.EMPR_Codigo
	  AND ENRE.TERC_Codigo_Destinatario = TERC.Codigo
	  AND ENRE.EMPR_Codigo = @par_EMPR_Codigo
	  AND ENRE.Estado = 1      
	  AND ENRE.Anulado = 0      
	  AND ENRE.TIDO_Codigo IN (100, 110)
   
	  AND ENRE.Fecha >= CONVERT(DATETIME,@par_Fecha_Inicial,101)      
	  AND ENRE.Fecha <= CONVERT(DATETIME,@par_Fecha_Final,101)      
	 GROUP BY      
	  ENRE.EMPR_Codigo, ENRE.TERC_Codigo_Destinatario, TERC.Numero_Identificacion, TERC.CATA_TIID_Codigo  


	  --- 4.1 . Destinatarios que estan en remesas con distribucion
	  INSERT INTO @tblTerceros_SIPLAFT
	  SELECT      
	  DDRE.EMPR_Codigo, DDRE.TERC_Codigo_Destinatario, TERC.Numero_Identificacion, TERC.CATA_TIID_Codigo, SUM(ENRE.Valor_Flete_Cliente)      
	 FROM      
	  Detalle_Distribucion_Remesas DDRE, Terceros TERC, Encabezado_Remesas ENRE 
	 WHERE      
	  DDRE.EMPR_Codigo = TERC.EMPR_Codigo
	  AND DDRE.TERC_Codigo_Destinatario = TERC.Codigo
	  AND DDRE.EMPR_Codigo = ENRE.EMPR_Codigo
	  AND DDRE.ENRE_Numero = ENRE.Numero

	  AND DDRE.EMPR_Codigo = @par_EMPR_Codigo
	  AND ENRE.Estado = 1      
	  AND ENRE.Anulado = 0      
	  AND ENRE.TIDO_Codigo IN (100, 110)
   
	  AND ENRE.Fecha >= CONVERT(DATETIME,@par_Fecha_Inicial,101)      
	  AND ENRE.Fecha <= CONVERT(DATETIME,@par_Fecha_Final,101)      
	 GROUP BY      
	  DDRE.EMPR_Codigo, DDRE.TERC_Codigo_Destinatario, TERC.Numero_Identificacion, TERC.CATA_TIID_Codigo  

	  SELECT TEMP.* FROM 
		  (SELECT EMPR_Codigo, TERC_Codigo, Iden_Tercero, CATA_TIID_Codigo, SUM(Valor_Flete_Cliente) As Valor 
		  FROM @tblTerceros_SIPLAFT
		  WHERE EMPR_Codigo = @par_EMPR_Codigo
		  GROUP BY EMPR_Codigo, TERC_Codigo, Iden_Tercero, CATA_TIID_Codigo) As TEMP

	  WHERE TEMP.Valor >= @ValorTransaccion

END
GO