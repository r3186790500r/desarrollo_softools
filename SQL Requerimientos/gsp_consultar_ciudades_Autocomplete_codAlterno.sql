USE [GESCARGA50_DESA]
GO

/****** Object:  StoredProcedure [dbo].[gsp_consultar_ciudades_Autocomplete_codAlterno]    Script Date: 20/12/2021 2:03:36 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[gsp_consultar_ciudades_Autocomplete_codAlterno]    
(                
@par_EMPR_Codigo SMALLINT,                
@par_Codigo INT = NULL,                
@par_Filtro VARCHAR(40) = NULL,                
@par_Estado SMALLINT = NULl,        
@par_Ciudad_Oficinas SMALLINT = NULl ,      
@par_Ciudad_Regiones SMALLINT = NULl,      
@par_REPA_Codigo NUMERIC = NULL      
)                
AS                
BEGIN               
IF @par_Ciudad_Oficinas > 0        
BEGIN        
 SELECT DISTINCT          
    0 AS Obtener,                
    CIUD.EMPR_Codigo,                
    CIUD.Codigo,                
    CIUD.Nombre AS Ciudad,                
    CIUD.Codigo_Alterno,                
 CIUD.Codigo_Postal,    
    --CIUD.Tarifa_Impuesto1,                
    --CIUD.Tarifa_Impuesto2,                
    CIUD.Estado,                
    CIUD.DEPA_Codigo,                
    DEPA.Nombre AS Departamento,                
    DEPA.PAIS_Codigo,                
    PAIS.Nombre AS Pais,                
    CONCAT(CIUD.Nombre,' (',DEPA.Nombre,')') AS CiudadDepartamento        
   FROM            
    Ciudades_Oficina_Enturnamiento_Despachos AS COED        
    inner join Ciudades AS CIUD on        
 CIUD.EMPR_Codigo = COED.EMPR_Codigo        
 and COED.CIUD_Codigo = CIUD.Codigo        
              
  LEFT JOIN Departamentos AS DEPA ON                
     CIUD.EMPR_Codigo = DEPA.EMPR_Codigo                
     AND CIUD.DEPA_Codigo = DEPA.Codigo                
              
  LEFT JOIN Paises AS PAIS ON                
     DEPA.EMPR_Codigo = PAIS.EMPR_Codigo                
     AND DEPA.PAIS_Codigo = PAIS.Codigo                
                 
   WHERE                
    CIUD.EMPR_Codigo = DEPA.EMPR_Codigo                
    AND CIUD.DEPA_Codigo = DEPA.Codigo                
    AND DEPA.EMPR_Codigo = PAIS.EMPR_Codigo                
    AND DEPA.PAIS_Codigo = PAIS.Codigo                
    AND CIUD.Codigo <> 0                
    AND CIUD.Estado = ISNULL(@par_Estado, CIUD.Estado)                
    AND CIUD.Codigo_Alterno = ISNULL(@par_Codigo, CIUD.Codigo_Alterno)                
    AND CIUD.EMPR_Codigo = @par_EMPR_Codigo                
                   
    AND ((CIUD.Nombre LIKE @par_Filtro + '%') OR (@par_Filtro IS NULL))                
                
             
 ORDER BY CIUD.Nombre            
END        
ELSE IF @par_Ciudad_Regiones > 0        
BEGIN        
 SELECT DISTINCT             
    0 AS Obtener,                
    CIUD.EMPR_Codigo,                
    CIUD.Codigo,                
    CIUD.Nombre AS Ciudad,                
    CIUD.Codigo_Alterno,     
 CIUD.Codigo_Postal,               
    --CIUD.Tarifa_Impuesto1,                
    --CIUD.Tarifa_Impuesto2,                
    CIUD.Estado,                
    CIUD.DEPA_Codigo,                
    DEPA.Nombre AS Departamento,                
    DEPA.PAIS_Codigo,                
    PAIS.Nombre AS Pais,                
    CONCAT(CIUD.Nombre,' (',DEPA.Nombre,')') AS CiudadDepartamento        
   FROM            
    Ciudades_Regiones_Paises AS CIRP        
    inner join Ciudades AS CIUD on        
 CIUD.EMPR_Codigo = CIRP.EMPR_Codigo        
 and CIRP.CIUD_Codigo = CIUD.Codigo        
              
  LEFT JOIN Departamentos AS DEPA ON                
     CIUD.EMPR_Codigo = DEPA.EMPR_Codigo                
     AND CIUD.DEPA_Codigo = DEPA.Codigo                
              
  LEFT JOIN Paises AS PAIS ON                
     DEPA.EMPR_Codigo = PAIS.EMPR_Codigo                
     AND DEPA.PAIS_Codigo = PAIS.Codigo                
                 
   WHERE                
    CIUD.EMPR_Codigo = DEPA.EMPR_Codigo                
    AND CIUD.DEPA_Codigo = DEPA.Codigo                
    AND DEPA.EMPR_Codigo = PAIS.EMPR_Codigo                
    AND DEPA.PAIS_Codigo = PAIS.Codigo                
    AND CIUD.Codigo <> 0                
    AND CIUD.Estado = ISNULL(@par_Estado, CIUD.Estado)                
    AND CIUD.Codigo_Alterno = ISNULL(@par_Codigo, CIUD.Codigo_Alterno)                
    AND CIUD.EMPR_Codigo = @par_EMPR_Codigo                
            
    AND ((CIUD.Nombre LIKE @par_Filtro + '%') OR (@par_Filtro IS NULL))                
                
             
 ORDER BY CIUD.Nombre            
END       
ELSE IF @par_REPA_Codigo > 0        
BEGIN        
 SELECT DISTINCT             
    0 AS Obtener,                
    CIUD.EMPR_Codigo,                
    CIUD.Codigo,                
    CIUD.Nombre AS Ciudad,                
    CIUD.Codigo_Alterno,    
 CIUD.Codigo_Postal,    
    --CIUD.Tarifa_Impuesto1,                
    --CIUD.Tarifa_Impuesto2,                
    CIUD.Estado,                
    CIUD.DEPA_Codigo,                
    DEPA.Nombre AS Departamento,                
    DEPA.PAIS_Codigo,                
    PAIS.Nombre AS Pais,                
    CONCAT(CIUD.Nombre,' (',DEPA.Nombre,')') AS CiudadDepartamento        
   FROM            
    Ciudades_Regiones_Paises AS CIRP        
    inner join Ciudades AS CIUD on        
 CIUD.EMPR_Codigo = CIRP.EMPR_Codigo        
 and CIRP.CIUD_Codigo = CIUD.Codigo        
              
  LEFT JOIN Departamentos AS DEPA ON                
     CIUD.EMPR_Codigo = DEPA.EMPR_Codigo                
     AND CIUD.DEPA_Codigo = DEPA.Codigo                
              
  LEFT JOIN Paises AS PAIS ON                
     DEPA.EMPR_Codigo = PAIS.EMPR_Codigo                
     AND DEPA.PAIS_Codigo = PAIS.Codigo                
                 
   WHERE                
    CIUD.EMPR_Codigo = DEPA.EMPR_Codigo                
    AND CIUD.DEPA_Codigo = DEPA.Codigo                
    AND DEPA.EMPR_Codigo = PAIS.EMPR_Codigo                
    AND DEPA.PAIS_Codigo = PAIS.Codigo                
    AND CIUD.Codigo <> 0                
    AND CIUD.Estado = ISNULL(@par_Estado, CIUD.Estado)                
    AND CIUD.Codigo_Alterno = ISNULL(@par_Codigo, CIUD.Codigo_Alterno)                
    AND CIUD.EMPR_Codigo = @par_EMPR_Codigo                
                   
    AND ((CIUD.Nombre LIKE @par_Filtro + '%') OR (@par_Filtro IS NULL))                
    AND CIRP.REPA_Codigo = @par_REPA_Codigo            
             
 ORDER BY CIUD.Nombre            
END       
ELSE        
BEGIN        
  IF @par_Codigo > 0 AND @par_Codigo  IS NOT NULL          
  BEGIN          
  SELECT top(1)               
    0 AS Obtener,                
    CIUD.EMPR_Codigo,                
    CIUD.Codigo,                
    CIUD.Nombre AS Ciudad,                
    CIUD.Codigo_Alterno,    
 CIUD.Codigo_Postal,               
    --CIUD.Tarifa_Impuesto1,                
    --CIUD.Tarifa_Impuesto2,                
    CIUD.Estado,                
    CIUD.DEPA_Codigo,                
    DEPA.Nombre AS Departamento,                
    DEPA.PAIS_Codigo,                
    PAIS.Nombre AS Pais,                
    CONCAT(CIUD.Nombre,' (',DEPA.Nombre,')') AS CiudadDepartamento,                
    ROW_NUMBER() OVER(ORDER BY CIUD.Nombre) AS RowNumber                
   FROM                
    Ciudades AS CIUD              
              
  LEFT JOIN Departamentos AS DEPA ON                
     CIUD.EMPR_Codigo = DEPA.EMPR_Codigo                
     AND CIUD.DEPA_Codigo = DEPA.Codigo                
              
  LEFT JOIN Paises AS PAIS ON                
     DEPA.EMPR_Codigo = PAIS.EMPR_Codigo                
     AND DEPA.PAIS_Codigo = PAIS.Codigo                
                 
   WHERE                
    CIUD.EMPR_Codigo = DEPA.EMPR_Codigo                
    AND CIUD.DEPA_Codigo = DEPA.Codigo                
    AND DEPA.EMPR_Codigo = PAIS.EMPR_Codigo                
    AND DEPA.PAIS_Codigo = PAIS.Codigo                
    AND CIUD.Codigo <> 0                
    AND CIUD.Codigo_Alterno = @par_Codigo          
    AND CIUD.EMPR_Codigo = @par_EMPR_Codigo                
                
             
 ORDER BY CIUD.Nombre            
  END          
  ELSE          
  BEGIN          
   SELECT              
    0 AS Obtener,                
    CIUD.EMPR_Codigo,                
    CIUD.Codigo,                
    CIUD.Nombre AS Ciudad,                
    CIUD.Codigo_Alterno,     
 CIUD.Codigo_Postal,       
    --CIUD.Tarifa_Impuesto1,                
    --CIUD.Tarifa_Impuesto2,                
    CIUD.Estado,                
    CIUD.DEPA_Codigo,                
    DEPA.Nombre AS Departamento,                
    DEPA.PAIS_Codigo,                
    PAIS.Nombre AS Pais,                
    CONCAT(CIUD.Nombre,' (',DEPA.Nombre,')') AS CiudadDepartamento,                
    ROW_NUMBER() OVER(ORDER BY CIUD.Nombre) AS RowNumber                
   FROM                
    Ciudades AS CIUD              
              
  LEFT JOIN Departamentos AS DEPA ON                
     CIUD.EMPR_Codigo = DEPA.EMPR_Codigo              
     AND CIUD.DEPA_Codigo = DEPA.Codigo                
              
  LEFT JOIN Paises AS PAIS ON                
     DEPA.EMPR_Codigo = PAIS.EMPR_Codigo                
     AND DEPA.PAIS_Codigo = PAIS.Codigo                
                 
   WHERE                
    CIUD.EMPR_Codigo = DEPA.EMPR_Codigo                
    AND CIUD.DEPA_Codigo = DEPA.Codigo                
    AND DEPA.EMPR_Codigo = PAIS.EMPR_Codigo                
    AND DEPA.PAIS_Codigo = PAIS.Codigo                
    AND CIUD.Codigo <> 0                
    AND CIUD.Estado = ISNULL(@par_Estado, CIUD.Estado)                
    AND CIUD.Codigo_Alterno = ISNULL(@par_Codigo, CIUD.Codigo_Alterno)                
    AND CIUD.EMPR_Codigo = @par_EMPR_Codigo                
                   
    AND ((CIUD.Nombre LIKE @par_Filtro + '%') OR (@par_Filtro IS NULL))                
                
             
 ORDER BY CIUD.Nombre            
END                
END                
END    

GO


