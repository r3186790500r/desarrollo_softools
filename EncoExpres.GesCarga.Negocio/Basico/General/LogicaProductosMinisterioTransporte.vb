﻿Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Fachada.Basico.General
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Basico.General
    Public NotInheritable Class LogicaProductosMinisterioTransporte
        Inherits LogicaBase(Of ProductosMinisterioTransporte)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of ProductosMinisterioTransporte)

        Sub New(persistencia As IPersistenciaBase(Of ProductosMinisterioTransporte))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As ProductosMinisterioTransporte) As Respuesta(Of IEnumerable(Of ProductosMinisterioTransporte))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of ProductosMinisterioTransporte))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of ProductosMinisterioTransporte))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As ProductosMinisterioTransporte) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As ProductosMinisterioTransporte) As Respuesta(Of ProductosMinisterioTransporte)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of ProductosMinisterioTransporte)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of ProductosMinisterioTransporte)(consulta)
        End Function

        Private Function DatosRequeridos(entidad As ProductosMinisterioTransporte) As Respuesta(Of ProductosMinisterioTransporte)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of ProductosMinisterioTransporte) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of ProductosMinisterioTransporte) With {.ProcesoExitoso = True}

        End Function

        Public Function Anular(entidad As ProductosMinisterioTransporte) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaProductosMinisterioTransporte).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

    End Class

End Namespace