﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" Catalogos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Catalogos
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Catalogos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Catalogos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            NombreCorto = Read(lector, "Nombre_Corto")
            Nombre = Read(lector, "Nombre")
            Actualizable = Read(lector, "Actualizable")
            NumeroColumnas = Read(lector, "Numero_Columnas")
            NumeroCamposLLave = lector.Item("Numero_Campos_Llave")
        End Sub

        ''' <summary>
        ''' Obtiene o establece el codigo del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Codigo As Integer

        ''' <summary>
        ''' Obtiene o establece las siglas del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property NombreCorto As String

        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String

        ''' <summary>
        ''' Obtiene o establece si el catalogo es actualizable o no
        ''' </summary>
        <JsonProperty>
        Public Property Actualizable As Short

        ''' <summary>
        ''' Obtiene o establece el numero de columnas del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property NumeroColumnas As Short

        ''' <summary>
        ''' Obtiene o establece la cantidad de campos llave del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property NumeroCamposLLave As Short


        <JsonProperty>
        Public Property CodigoAlteno As Short

    End Class
End Namespace
