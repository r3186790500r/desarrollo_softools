﻿EncoExpresApp.factory('DocumentoComprobantesFactory', ['$http', function ($http) {

    var urlService = GetUrl('EncabezadoDocumentoComprobantes');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarCuentasconEgresos = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarCuentasconEgresos';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ActualizarEgresosArchivoRespuesta = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ActualizarEgresosArchivoRespuesta';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    
    factory.ValidarSecuenciaLotes = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ValidarSecuenciaLotes';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    
    factory.ConsultarCodigosRespuesaBancolombia = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarCodigosRespuesaBancolombia';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    
    factory.ConsultarCadenaEgresos = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarCadenaEgresos';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    return factory;
}]);