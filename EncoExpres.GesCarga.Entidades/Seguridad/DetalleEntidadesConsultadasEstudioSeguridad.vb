﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Seguridad

    ''' <summary>
    ''' Clase <see cref="DetalleEntidadesConsultadasEstudioSeguridad"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleEntidadesConsultadasEstudioSeguridad
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleEntidadesConsultadasEstudioSeguridad"/>
        ''' </summary>
        Sub New()
        End Sub
        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleEntidadesConsultadasEstudioSeguridad"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroEstudio = New EstudioSeguridad With {.Numero = Read(lector, "ENES_Numero")}
            Codigo = Read(lector, "CATA_ECES_Codigo")
            Nombre = Read(lector, "Nombre_Entidad_Consultada")
            Estado = Read(lector, "Estado")
            ObservacionEntidades = Read(lector, "Observaciones")

        End Sub

        <JsonProperty>
        Public Property NumeroEstudio As EstudioSeguridad
        <JsonProperty>
        Public Property ObservacionEntidades As String
        <JsonProperty>
        Public Property Nombre As String

    End Class
End Namespace
