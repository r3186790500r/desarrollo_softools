﻿PRINT 'gsp_consultar_encabezado_liquidacion_planilla_despachos'
GO
DROP PROCEDURE gsp_consultar_encabezado_liquidacion_planilla_despachos
GO 
CREATE PROCEDURE gsp_consultar_encabezado_liquidacion_planilla_despachos 
(      
 @par_EMPR_Codigo SMALLINT,      
 @par_Numero NUMERIC = NULL,      
 @par_Fecha_Inicial DATETIME = NULL,      
 @par_Fecha_Final DATETIME = NULL,      
 @par_ENPD_Numero NUMERIC = NULL,      
 @par_ENMC_Numero NUMERIC = NULL,      
 @par_VEHI_Placa VARCHAR(20) = NULL,      
 @par_TENE_Codigo NUMERIC = NULL,      
 @par_COND_Codigo NUMERIC = NULL,      
 @par_Estado INT = NULL,    
 @par_Anulado INT = NULL,    
 @par_Aprobado INT = NULL, 
 @par_OFIC_Codigo INT = NULL,      
 @par_NumeroPagina INT = NULL,      
 @par_RegistrosPagina INT = NULL      
)      
AS      
BEGIN      
SET NOCOUNT ON;       
 DECLARE @CantidadRegistros INT      
 SELECT @CantidadRegistros =      
 (      
  SELECT DISTINCT      
   COUNT(1)      
  FROM      
   Encabezado_Liquidacion_Planilla_Despachos ELPD      
      
   LEFT JOIN Encabezado_Planilla_Despachos ENPD      
   ON ELPD.EMPR_Codigo = ENPD.EMPR_Codigo AND ELPD.ENPD_Numero = ENPD.Numero      
   AND ENPD.Numero_Documento = ISNULL(@par_ENPD_Numero,ENPD.Numero_Documento)      
         
   LEFT JOIN Vehiculos VEHI      
   ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo AND ENPD.VEHI_Codigo = VEHI.Codigo      
   AND VEHI.Placa = ISNULL(@par_VEHI_Placa, VEHI.Placa)      
      
   LEFT JOIN Terceros TENE ON      
   ENPD.EMPR_Codigo = TENE.EMPR_Codigo AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo      
   AND TENE.Codigo = ISNULL(@par_TENE_Codigo,TENE.Codigo)      
      
   LEFT JOIN Terceros COND ON      
   ENPD.EMPR_Codigo = COND.EMPR_Codigo AND ENPD.TERC_Codigo_Conductor = COND.Codigo      
   AND COND.Codigo = ISNULL(@par_COND_Codigo,COND.Codigo)      
      
   INNER JOIN Oficinas OFIC ON      
   ELPD.EMPR_Codigo = OFIC.EMPR_Codigo AND ELPD.OFIC_Codigo = OFIC.Codigo      
      
   LEFT JOIN Rutas RUTA      
   ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo AND ENPD.RUTA_Codigo = RUTA.Codigo      
      
   LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON      
   ENPD.EMPR_Codigo = ENMC.EMPR_Codigo AND ENPD.ENMC_Numero = ENMC.Numero      
      
  WHERE      
   ELPD.EMPR_Codigo = @par_EMPR_Codigo      
   AND ELPD.Numero_Documento = ISNULL(@par_Numero,ELPD.Numero_Documento)      
   AND ELPD.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ELPD.Fecha) AND ISNULL(@par_Fecha_Final, ELPD.Fecha)      
   AND ELPD.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ELPD.OFIC_Codigo)      
   AND ELPD.Estado = ISNULL(@par_Estado, ELPD.Estado)   
   AND ELPD.Anulado = ISNULL(@par_Anulado, ELPD.Anulado)   
   AND ELPD.Aprobar = ISNULL(@par_Aprobado, ELPD.Aprobar)  
   AND (ENMC.Numero_Documento = @par_ENMC_Numero or  @par_ENMC_Numero is null)    
 );      
 WITH Pagina      
 AS      
 (SELECT      
   ELPD.EMPR_Codigo,      
   ELPD.Numero,      
   ELPD.TIDO_Codigo,      
   ELPD.Numero_Documento,      
   ELPD.Fecha,      
   ELPD.ENPD_Numero,      
   ISNULL(ENPD.Numero_Documento,0) AS NumeroPlanilla,      
   ISNULL(ENMC.Numero_Documento,0) AS NumeroManifiesto,      
   ELPD.Estado,      
   ELPD.Anulado,      
   ISNULL(ENPD.TERC_Codigo_Tenedor,0) AS TERC_Codigo_Tenedor,      
   ISNULL(CONCAT(TENE.Nombre,' ',TENE.Apellido1,' ',TENE.Apellido2),'') AS NombreTenedor,      
   ISNULL(ENPD.TERC_Codigo_Conductor,0) AS TERC_Codigo_Conductor,      
   CONCAT(COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2) AS NombreConductor,      
   ELPD.OFIC_Codigo,      
   OFIC.Nombre AS NombreOficina,      
   ISNULL(VEHI.Placa,'') AS PlacaVehiculo,      
   ISNULL(RUTA.Nombre,'') AS NombreRuta,      
   ELPD.Valor_Pagar,    
      ROW_NUMBER() OVER (ORDER BY ELPD.Numero DESC) AS RowNumber      
  FROM      
   Encabezado_Liquidacion_Planilla_Despachos ELPD      
      
   LEFT JOIN Encabezado_Planilla_Despachos ENPD      
   ON ELPD.EMPR_Codigo = ENPD.EMPR_Codigo AND ELPD.ENPD_Numero = ENPD.Numero      
   AND ENPD.Numero_Documento = ISNULL(@par_ENPD_Numero,ENPD.Numero_Documento)      
         
   LEFT JOIN Vehiculos VEHI      
   ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo AND ENPD.VEHI_Codigo = VEHI.Codigo      
   AND VEHI.Placa = ISNULL(@par_VEHI_Placa, VEHI.Placa)      
      
   LEFT JOIN Terceros TENE ON      
   ENPD.EMPR_Codigo = TENE.EMPR_Codigo AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo      
   AND TENE.Codigo = ISNULL(@par_TENE_Codigo,TENE.Codigo)      
      
   LEFT JOIN Terceros COND ON      
   ENPD.EMPR_Codigo = COND.EMPR_Codigo AND ENPD.TERC_Codigo_Conductor = COND.Codigo      
   AND COND.Codigo = ISNULL(@par_COND_Codigo,COND.Codigo)      
      
   INNER JOIN Oficinas OFIC ON      
   ELPD.EMPR_Codigo = OFIC.EMPR_Codigo AND ELPD.OFIC_Codigo = OFIC.Codigo      
      
   LEFT JOIN Rutas RUTA      
   ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo AND ENPD.RUTA_Codigo = RUTA.Codigo      
      
   LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON      
   ENPD.EMPR_Codigo = ENMC.EMPR_Codigo AND ENPD.ENMC_Numero = ENMC.Numero      
      
  WHERE      
   ELPD.EMPR_Codigo = @par_EMPR_Codigo      
   AND ELPD.Numero_Documento = ISNULL(@par_Numero,ELPD.Numero_Documento)      
   AND ELPD.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ELPD.Fecha) AND ISNULL(@par_Fecha_Final, ELPD.Fecha)      
   AND ELPD.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ELPD.OFIC_Codigo)      
   AND ELPD.Estado = ISNULL(@par_Estado, ELPD.Estado)    
   AND ELPD.Anulado = ISNULL(@par_Anulado, ELPD.Anulado) 
   AND ELPD.Aprobar = ISNULL(@par_Aprobado, ELPD.Aprobar)     
   AND (ENMC.Numero_Documento = @par_ENMC_Numero or  @par_ENMC_Numero is null)    
  )      
 SELECT DISTINCT      
  0 AS Obtener,      
  EMPR_Codigo,      
  Numero,      
  TIDO_Codigo,      
  Numero_Documento,      
  Fecha,      
  ENPD_Numero,      
  NumeroPlanilla,      
  NumeroManifiesto,      
  Estado,      
  Anulado,      
  TERC_Codigo_Tenedor,      
  NombreTenedor,      
  TERC_Codigo_Conductor,      
  NombreConductor,      
  OFIC_Codigo,      
  NombreOficina,      
  PlacaVehiculo,      
  NombreRuta,      
  Valor_Pagar,    
     @CantidadRegistros AS TotalRegistros,      
     @par_NumeroPagina AS PaginaObtener,      
     @par_RegistrosPagina AS RegistrosPagina      
 FROM Pagina      
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)      
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)      
 ORDER BY Numero      
END      
GO