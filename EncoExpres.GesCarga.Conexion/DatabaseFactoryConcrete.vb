﻿''' <summary>
''' Clase <see cref="DatabaseFactoryConcrete"/>
''' </summary>
Public Class DatabaseFactoryConcrete

    ''' <summary>
    ''' Cadena de conexión de la base de datos
    ''' </summary>
    Private _connectionString As String

    ''' <summary>
    ''' Inicializa una instacia de la clase <see cref="DatabaseFactoryConcrete"/> e inicializa la cadena de conexión
    ''' </summary>
    ''' <param name="connectionString"></param>
    Public Sub New(ByVal connectionString As String)
        _connectionString = connectionString
    End Sub

    ''' <summary>
    ''' Inicializa una instancia de la clase <see cref="DataBaseSqlServer"/>, pasando como parametro al contructor la cadena de conexión
    ''' </summary>
    ''' <returns> <see cref="DataBaseFactory"/> </returns>
    Public Function CreateDataBaseFactory() As DataBaseFactory
        Return New DataBaseSqlServer(_connectionString)
    End Function
End Class
