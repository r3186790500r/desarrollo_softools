﻿PRINT 'gsp_consultar_Unidad_Empaque'
GO
DROP PROCEDURE gsp_consultar_Unidad_Empaque
GO
CREATE PROCEDURE gsp_consultar_Unidad_Empaque  
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo SMALLINT = NULL
)  
AS  
BEGIN  
 SELECT  
  UEPT.EMPR_Codigo,  
  UEPT.Codigo,  
  ISNULL(UEPT.Nombre_Corto,'') AS Nombre_Corto,  
  ISNULL(UEPT.Descripcion,'') AS Descripcion,  
  ISNULL(UEPT.Estado,'') AS Estado
 FROM  
  Unidad_Empaque_Producto_Transportados AS UEPT

 WHERE  
  UEPT.EMPR_Codigo = @par_EMPR_Codigo
  AND UEPT.Codigo = ISNULL(@par_Codigo, UEPT.Codigo)  
END  
GO