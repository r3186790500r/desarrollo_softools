﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.ControlTrafico
Imports System.Transactions

Namespace ControlTrafico

    ''' <summary>
    ''' Clase <see cref="RepositorioSeguimientoTransportadoraGPS"/>
    ''' </summary>
    Public NotInheritable Class RepositorioSeguimientoTransportadoraGPS
        Inherits RepositorioBase(Of SeguimientoTransportadoraGPS)

        Public Overrides Function Consultar(filtro As SeguimientoTransportadoraGPS) As IEnumerable(Of SeguimientoTransportadoraGPS)
            Dim lista As New List(Of SeguimientoTransportadoraGPS)
            Dim item As SeguimientoTransportadoraGPS
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo", filtro.CodigoUsuarioCrea)

                conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial)

                conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal)


                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_seguimiento_transportadora_gps]")

                While resultado.Read
                    item = New SeguimientoTransportadoraGPS(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As SeguimientoTransportadoraGPS) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)


                    conexion.AgregarParametroSQL("@par_NIT_Transportadora", entidad.NITTransportadora)
                    conexion.AgregarParametroSQL("@par_Nombre_Transportadora", entidad.NombreTransportadora)
                    conexion.AgregarParametroSQL("@par_VEHI_Placa", entidad.VEHIPlaca)
                    conexion.AgregarParametroSQL("@par_Cedula_Conductor", entidad.CedulaConductor)
                    conexion.AgregarParametroSQL("@par_Fecha_Reporte", entidad.FechaReporte, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Nombre_Evento", entidad.NombreEvento)
                    conexion.AgregarParametroSQL("@par_Numero_Secuencia", entidad.NumeroSecuencia)
                    conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Float)
                    conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Float)
                    conexion.AgregarParametroSQL("@par_Altitud", entidad.Altitud)
                    conexion.AgregarParametroSQL("@par_Velocidad_Instantanea", entidad.VelocidadInstantanea)
                    conexion.AgregarParametroSQL("@par_Heading", entidad.Heading)
                    conexion.AgregarParametroSQL("@par_Satelites_Usados", entidad.SatelitesUsados)
                    conexion.AgregarParametroSQL("@par_HDOP", entidad.HDOP)
                    conexion.AgregarParametroSQL("@par_Tipo_Posicion", entidad.TipoPosicion)
                    conexion.AgregarParametroSQL("@par_Inputs", entidad.Inputs)
                    conexion.AgregarParametroSQL("@par_Codigo_Evento", entidad.CodigoEvento)
                    conexion.AgregarParametroSQL("@par_Mensaje_Evento", entidad.MensajeEvento)
                    conexion.AgregarParametroSQL("@par_Distancia_Recorrida", entidad.DistanciaRecorrida)
                    conexion.AgregarParametroSQL("@par_Tiempo_Trabajo", entidad.TiempoTrabajo)
                    conexion.AgregarParametroSQL("@par_Tiempo_Falta", entidad.TiempoFalta)
                    conexion.AgregarParametroSQL("@par_Magnitud_Falta", entidad.MagnitudFalta)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.CodigoUsuarioCrea)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_seguimiento_transportadora_gps]")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While


                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        inserto = False
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As SeguimientoTransportadoraGPS) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As SeguimientoTransportadoraGPS) As SeguimientoTransportadoraGPS
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As SeguimientoTransportadoraGPS) As Boolean
            Throw New NotImplementedException()
        End Function

    End Class

End Namespace