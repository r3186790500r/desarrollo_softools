ALTER PROCEDURE [dbo].[gsp_Consultar_Encabezado_Liquidacion_Planilla_Despachos_SIESA_ENCOE]              
(                
 @par_EMPR_Codigo smallint,             
 @par_ELPD_Numero numeric = NULL            
)                
AS                
BEGIN              
              
	SELECT TOP 10               
	ELPD.EMPR_Codigo,
	ELPD.TIDO_Codigo,
	ELPD.Numero,                
	ELPD.Numero_Documento,                
	ELPD.Fecha,                
	ELPD.OFIC_Codigo,              
	ELPD.Observaciones,   
	ELPD.Valor_Pagar,
	ELPD.Valor_Flete_Transportador,
	ELPD.Valor_Impuestos,
	ENPD.Numero AS Numero_Planilla,
	ENPD.Numero_Documento AS Numero_Documento_Planilla,
	TENE.Numero_Identificacion AS Identificacion_Tenedor,    
	COND.Numero_Identificacion AS Identificacion_Conductor,
	COND.Codigo AS Codigo_Conductor,
	VEHI.Placa AS Placa_Vehiculo,
	VEHI.Codigo_Alterno AS Codigo_Vehiculo
                 
	FROM Encabezado_Liquidacion_Planilla_Despachos AS ELPD              
    
	INNER JOIN Encabezado_Planilla_Despachos AS ENPD            
	ON ELPD.EMPR_Codigo = ENPD.EMPR_Codigo            
	AND ELPD.ENPD_Numero = ENPD.Numero            
            
	INNER JOIN Vehiculos AS VEHI             
	ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo            
	AND ENPD.VEHI_Codigo = VEHI.Codigo    
            
	INNER JOIN Terceros AS TENE     
	ON ENPD.EMPR_Codigo = TENE.EMPR_Codigo              
	AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo 

	INNER JOIN Terceros AS COND     
	ON ENPD.EMPR_Codigo = COND.EMPR_Codigo              
	AND ENPD.TERC_Codigo_Conductor = COND.Codigo 
 
	WHERE ELPD.EMPR_Codigo = @par_EMPR_Codigo        
	AND (ELPD.Numero = @par_ELPD_Numero OR @par_ELPD_Numero IS NULL)    
	AND ISNULL(ELPD.Interfaz_Contable, 0 ) = 0                
	AND ISNULL(ELPD.Intentos_Interfase_Contable, 0 ) < 5                
	AND ELPD.Anulado = 0            
	AND ELPD.Estado = 1            
            
END 