﻿EncoExpresApp.controller("GestionarCuentaCambistaCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'CuentaBancariasFactory', 'ValorCatalogosFactory', 'PlanUnicoCuentasFactory', 'BancosFactory', 'TercerosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, CuentaBancariasFactory, ValorCatalogosFactory, PlanUnicoCuentasFactory, BancosFactory, TercerosFactory) {

    $scope.Titulo = 'GESTIONAR CUENTAS CAMBISTAS';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Cuentas Cambistas' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + 100308);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.ListadoBancos = [];
    $scope.ListadoCuentasPUC = [];
    $scope.ListadoTipoCuentaBancaria = [];
    $scope.ListadoProveedores = []
    $scope.ListadoConsulta = [];
    $scope.CuentaPUCValida = true
    $scope.ValidarValorBase = false
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0
    }

    $scope.AsignarCuentaPUC = function (CuentaPUC) {
        if (CuentaPUC !== undefined || CuentaPUC !== null) {
            if (angular.isObject(CuentaPUC)) {
                $scope.LongitudCuentaPUC = CuentaPUC.length;
                $scope.CuentaPUCValida = true;
            }
            else if (angular.isString(CuentaPUC)) {
                $scope.LongitudCuentaPUC = CuentaPUC.length;
                $scope.CuentaPUCValida = false;
            }
        }
    };
    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVO" },
        { Codigo: 1, Nombre: "ACTIVO" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    $scope.Modelo.TipoCuentaBancaria = { Codigo: 403 }

    TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoProveedores = []
                if (response.data.Datos.length > 0) {
                    $scope.ListadoProveedores = response.data.Datos;
                    $scope.ListadoProveedores.push({ NombreCompleto: "", Codigo: 0 })

                    if ($scope.Tercero > 0) {
                        $scope.Modelo.Tercero = $linq.Enumerable().From($scope.ListadoProveedores).First('$.Codigo ==' + $scope.Tercero);
                    } else {
                        $scope.Modelo.Tercero = $scope.ListadoProveedores[$scope.ListadoProveedores.length - 1];
                    }
                }
                else {
                    $scope.ListadoProveedores = [];
                }
            }
        }, function (response) {
            ShowError(response.statusText);
        });

    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando cuenta bancaria código No. ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando  cuenta bancaria código No.' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        CuentaBancariasFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.Modelo.Codigo = response.data.Datos.Codigo;
                    $scope.Modelo.Nombre = response.data.Datos.Nombre
                    $scope.Tercero = response.data.Datos.Tercero.Codigo
                    $scope.MaskValores();
                    if ($scope.ListadoProveedores.length > 0 && $scope.Tercero > 0) {
                        $scope.Modelo.Tercero = $linq.Enumerable().From($scope.ListadoProveedores).First('$.Codigo ==' + $scope.Tercero)
                    }

                    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                }
                else {
                    ShowError('No se logro consultar la cuenta bancaria código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarCuentasCambista';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarCuentasCambista';
            });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    $scope.ValidarValor = function () {
        if ($scope.Modelo.ExigeValorBase === true) {
            $scope.ValidarValorBase = true
        }
        else {
            $scope.ValidarValorBase = false
        }
    }

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {
            $scope.MaskNumero();
            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

            CuentaBancariasFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo === 0) {
                                ShowSuccess('Se guardó la cuenta bancaria "' + $scope.Modelo.Nombre + '"');
                            }
                            else {
                                ShowSuccess('Se modificó  la cuenta bancaria "' + $scope.Modelo.Nombre + '"');
                            }
                            location.href = '#!ConsultarCuentasCambista/' + $scope.Modelo.Codigo;
                        }
                    }
                    $scope.MaskValores();
                }, function (response) {
                    if (response.statusText.includes('IX_Cuenta_Bancarias_Nombre')) {
                        ShowError('No se pudo guardar, el nombre ingresado ya está siendo utilizado')
                    } else if (response.statusText.includes('IX_Cuenta_Bancarias_Numero_Cuenta')) {
                        ShowError('No se pudo guardar, el número de cuenta ingresado ya está siendo utilizado')
                    } else {
                        ShowError(response.statusText);
                    }
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.Nombre === undefined || $scope.Modelo.Nombre === '') {
            $scope.MensajesError.push('Debe ingresar el nombre de la cuenta bancaria');
            continuar = false;
        }
        if ($scope.Modelo.Tercero === undefined || $scope.Modelo.Tercero === '' || $scope.Modelo.Tercero === null || $scope.Modelo.Tercero.Codigo === 0) {
            $scope.MensajesError.push('Debe ingresar un tercero');
            continuar = false;
        }

        return continuar;
    }

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarCuentasCambista/' + $scope.Modelo.Codigo;
    };


    $scope.MaskMayus = function () {
        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Modelo.NumeroCuenta = MascaraNumero($scope.Modelo.NumeroCuenta) } catch (e) { }
    };
    $scope.MaskValores = function () {
        try { $scope.Modelo.SobregiroAutorizado = MascaraValores($scope.Modelo.SobregiroAutorizado) } catch (e) { }
        try { $scope.Modelo.SaldoActual = MascaraValores($scope.Modelo.SaldoActual) } catch (e) { }
    };
}]);