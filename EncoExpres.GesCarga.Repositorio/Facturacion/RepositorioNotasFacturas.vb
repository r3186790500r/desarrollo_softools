﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Entidades.Basico.Facturacion
Imports EncoExpres.GesCarga.Repositorio.Basico.Facturacion

Namespace Facturacion
    Public NotInheritable Class RepositorioNotasFacturas
        Inherits RepositorioBase(Of NotasFacturas)

        Public Overrides Function Consultar(filtro As NotasFacturas) As IEnumerable(Of NotasFacturas)
            Dim lista As New List(Of NotasFacturas)
            Dim item As NotasFacturas
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Numero > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.TipoDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                End If

                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicial <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Clientes) Then
                    If filtro.Clientes.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TERC_Cliente", filtro.Clientes.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.FacturarA) Then
                    If filtro.FacturarA.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TERC_Facturar", filtro.FacturarA.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.OficinaNota) Then
                    If filtro.OficinaNota.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_OFIC_Nota", filtro.OficinaNota.Codigo)
                    End If
                End If

                If filtro.Estado > -1 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If filtro.Anulado > Cero Then
                    conexion.AgregarParametroSQL("@par_Anulado", filtro.Anulado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_Nota_Facturas")

                While resultado.Read
                    item = New NotasFacturas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As NotasFacturas) As Long
            Dim inserto As Boolean = False

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.CleanParameters()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Factura.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_OFIC_Nota", entidad.OficinaNota.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Cliente", entidad.Clientes.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Facturar", entidad.FacturarA.Codigo)
                    conexion.AgregarParametroSQL("@par_CONF_Codigo", entidad.ConceptoNotas.Codigo)


                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    Else
                        conexion.AgregarParametroSQL("@par_Observaciones", VACIO)
                    End If

                    conexion.AgregarParametroSQL("@par_Valor_Nota", entidad.ValorNota)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Anulado", entidad.Anulado)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_notas_facturas")


                    While resultado.Read
                        If (resultado.Item("Numero") > Cero) Then
                            entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                            entidad.Numero = resultado.Item("Numero").ToString()
                            inserto = True
                        Else
                            entidad.MensajeSQL = resultado.Item("MensajeSQL").ToString()
                        End If
                    End While
                    resultado.Close()
                End Using

                If inserto = True And entidad.Estado = 1 Then
                    Dim Factura As Facturas = entidad.Factura
                    Factura.CodigoEmpresa = entidad.CodigoEmpresa
                    Factura.UsuarioAnula = New Entidades.SeguridadUsuarios.Usuarios With {.Codigo = entidad.UsuarioCrea.Codigo}
                    Factura.CausaAnulacion = "Anulación Por Nota Crédito No. " & entidad.NumeroDocumento

                    Dim ConceptoAnula = ObtenerConceptoAnula(entidad)

                    If entidad.ConceptoNotas.Codigo = ConceptoAnula Then 'cuando el concepto es anulación factura'
                        Select Case entidad.Factura.CataTipoFactura
                            Case 5201 'Tipo Factura ventas
                                Dim FactRepo = New RepositorioFacturas()
                                Dim ResultFactRepo = FactRepo.Anular(Factura)
                                If ResultFactRepo = False Then
                                    entidad.MensajeSQL = "No se pudo anular la factura ventas"
                                    inserto = False
                                End If
                            Case 5202 ' Tipo factura otros conceptos
                                Dim FactOtrosRepo = New RepositorioFacturasOtrosConceptos()
                                Dim ResultFacOtrostRepo = FactOtrosRepo.Anular(Factura)
                                If ResultFacOtrostRepo = False Then
                                    entidad.MensajeSQL = "No se pudo anular la factura otros conceptos"
                                    inserto = False
                                End If
                            Case Else
                                Dim FactRepo = New RepositorioFacturas()
                                Dim ResultFactRepo = FactRepo.Anular(Factura)
                                If ResultFactRepo = False Then
                                    entidad.MensajeSQL = "No se pudo anular la factura ventas"
                                    inserto = False
                                End If
                        End Select
                    End If
                End If

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.NumeroDocumento = Cero
                End If

            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As NotasFacturas) As Long
            Dim modifico As Boolean = False


            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.CleanParameters()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENNF_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Factura.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_OFIC_Nota", entidad.OficinaNota.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Cliente", entidad.Clientes.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Facturar", entidad.FacturarA.Codigo)
                    conexion.AgregarParametroSQL("@par_CONF_Codigo", entidad.ConceptoNotas.Codigo)

                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    Else
                        conexion.AgregarParametroSQL("@par_Observaciones", VACIO)
                    End If

                    conexion.AgregarParametroSQL("@par_Valor_Nota", entidad.ValorNota)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_notas_facturas")

                    While resultado.Read
                        If (resultado.Item("Numero") = entidad.Numero) Then
                            entidad.NumeroDocumento = resultado.Item("Numero_Documento")
                            entidad.Numero = resultado.Item("Numero").ToString()
                            modifico = True
                        End If

                    End While
                    resultado.Close()
                End Using

                If modifico = True And entidad.Estado = 1 And entidad.TipoDocumento = 190 Then
                    Dim Factura As Facturas = entidad.Factura
                    Factura.CodigoEmpresa = entidad.CodigoEmpresa
                    Factura.UsuarioAnula = New Entidades.SeguridadUsuarios.Usuarios With {.Codigo = entidad.UsuarioCrea.Codigo}
                    Factura.CausaAnulacion = "Anulación Por Nota Crédito No. " & entidad.NumeroDocumento

                    Dim ConceptoAnula = ObtenerConceptoAnula(entidad)
                    If entidad.ConceptoNotas.Codigo = ConceptoAnula Then 'cuando el concepto es anulación factura'
                        Select Case entidad.Factura.CataTipoFactura
                            Case 5201 'Tipo Factura ventas
                                Dim FactRepo = New RepositorioFacturas()
                                Dim ResultFactRepo = FactRepo.Anular(Factura)
                                If ResultFactRepo = False Then
                                    entidad.MensajeSQL = "No se pudo anular la factura ventas"
                                    modifico = False
                                End If
                            Case 5202 ' Tipo factura otros conceptos
                                Dim FactOtrosRepo = New RepositorioFacturasOtrosConceptos()
                                Dim ResultFacOtrostRepo = FactOtrosRepo.Anular(Factura)
                                If ResultFacOtrostRepo = False Then
                                    entidad.MensajeSQL = "No se pudo anular la factura otros conceptos"
                                    modifico = False
                                End If
                            Case Else
                                Dim FactRepo = New RepositorioFacturas()
                                Dim ResultFactRepo = FactRepo.Anular(Factura)
                                If ResultFactRepo = False Then
                                    entidad.MensajeSQL = "No se pudo anular la factura ventas"
                                    modifico = False
                                End If
                        End Select
                    End If
                End If

                If modifico Then
                    transaccion.Complete()
                Else
                    entidad.NumeroDocumento = Cero
                End If
            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As NotasFacturas) As NotasFacturas
            Dim item As New NotasFacturas
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroDocumento", filtro.NumeroDocumento)
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_notas_facturas")

                While resultado.Read
                    item = New NotasFacturas(resultado)
                End While
            End Using

            Return item
        End Function

        Public Function Anular(entidad As NotasFacturas) As Boolean
            Dim anulo As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_notas_facturas")

                While resultado.Read
                    anulo = IIf(Convert.ToInt32(resultado.Item("Numero")) > 0, True, False)
                End While

            End Using
            Return anulo
        End Function

        Public Function ObtenerDatosNotaElectronicaSaphety(filtro As NotasFacturas) As NotasFacturas
            Dim item As New NotasFacturas
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_nota_electronica_Saphety")

                While resultado.Read
                    item = New NotasFacturas(resultado)
                End While

            End Using
            Return item
        End Function

        Public Function GuardarNotaElectronica(entidad As NotasFacturas) As Long
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    If Len(entidad.CUDE) > 0 Then
                        conexion.AgregarParametroSQL("@par_CUDE", entidad.CUDE)
                    End If
                    If entidad.FechaEnvioElectronico <> Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Envio", entidad.FechaEnvioElectronico, SqlDbType.Date)
                    End If
                    If Not IsNothing(entidad.ID_Pruebas) Then
                        conexion.AgregarParametroSQL("@par_ID_Pruebas", entidad.ID_Pruebas)
                    End If
                    If Not IsNothing(entidad.ID_Habilitacion) Then
                        conexion.AgregarParametroSQL("@par_ID_Habilitacion", entidad.ID_Habilitacion)
                    End If
                    If Not IsNothing(entidad.ID_Emision) Then
                        conexion.AgregarParametroSQL("@par_ID_Emision", entidad.ID_Emision)
                    End If
                    If entidad.NotaElectronica > 0 Then
                        conexion.AgregarParametroSQL("@par_Estado_Noel", SI_APLICA)
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Guardar_Nota_Electronica_Saphety")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero")
                    End While
                    resultado.Close()
                End Using

                If entidad.Numero <> 0 Then
                    transaccion.Complete()
                End If
            End Using

            If Len(entidad.Archivo) > 0 Then
                Dim RespuestaGuardado As Boolean = GuardarArhivoRespuestaSaphety(entidad)
            End If


            Return entidad.Numero
        End Function

        Public Function ObtenerDocumentoReporteSaphety(filtro As NotasFacturas) As NotasFacturas
            Dim item As New NotasFacturas
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_documento_nota_saphety")

                While resultado.Read
                    item = New NotasFacturas(resultado)
                End While

            End Using
            Return item
        End Function

        Public Function GuardarArhivoRespuestaSaphety(entidad As NotasFacturas) As Boolean
            Dim guardar As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENNF_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_Tipo_Docu", entidad.TipoArchivo)
                conexion.AgregarParametroSQL("@par_Archivo", entidad.Archivo)
                conexion.AgregarParametroSQL("@par_Descrip", entidad.DecripcionArchivo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Documento_Nota_Factura")
                While resultado.Read
                    guardar = True
                End While
                resultado.Close()

            End Using
            Return guardar
        End Function

        Public Function ObtenerConceptoAnula(entidad As NotasFacturas) As Integer
            'Obtiene el Concepto que Anula Facturas
            Dim ConceptoAnula As Integer = 0
            Dim ConceptoNota As ConceptoNotasFacturas = New ConceptoNotasFacturas With {.CodigoEmpresa = entidad.CodigoEmpresa, .Estado = 1, .TipoDocumento = -1}
            Dim RepConceptosNotas = New RepositorioConceptoNotasFacturas()
            Dim ResultConcep = RepConceptosNotas.Consultar(ConceptoNota)
            For index = 0 To ResultConcep.Count - 1
                If ResultConcep(index).Nombre.IndexOf("ANULA") > -1 And ResultConcep(index).TipoDocumento = 190 Then
                    ConceptoAnula = ResultConcep(index).Codigo
                    Exit For
                End If
            Next
            Return ConceptoAnula
        End Function
    End Class
End Namespace

