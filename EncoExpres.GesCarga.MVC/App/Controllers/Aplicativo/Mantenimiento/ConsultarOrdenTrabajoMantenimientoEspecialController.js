﻿EncoExpresApp.controller("ConsultarOrdenTrabajoMantenimientoEspecialCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', '$linq', 'EquipoMantenimientoFactory', 'OrdenTrabajoMantenimientoFactory','TercerosFactory', function ($scope, $timeout, $routeParams, blockUI, $linq, EquipoMantenimientoFactory, OrdenTrabajoMantenimientoFactory, TercerosFactory) {
    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
    $scope.Buscando = false;
    $scope.ResultadoSinRegistros = '';
    $scope.MensajesError = [];
    var filtros = {};
    $scope.ListadoEstados = [];
    $scope.totalRegistros = 0;
    $scope.paginaActual = 1;
    $scope.cantidadRegistrosPorPagina = 10;
    $scope.totalPaginas = 0;
    $scope.MapaSitio = [{ Nombre: 'Mantenimiento' }, { Nombre: 'Documentos' }, { Nombre: 'Ordenes Mantenimiento' }];
    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ORDENES_TRABAJO);
    $scope.Modelo = {}
    $scope.Modelo.FechaFinal = new Date();
    $scope.Modelo.FechaInicial = new Date();
    $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    // -------------------------- Constantes ---------------------------------------------------------------------//

    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };

    /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

    /*Cargar el combo de estados*/
    $scope.ListadoEstados = [
        { Codigo: -1, Nombre: '(TODOS)' },
        { Codigo: 0, Nombre: 'BORRADOR' },
        { Codigo: 1, Nombre: 'DEFINITIVO' },
        { Codigo: 2, Nombre: 'ANULADO' }
    ];

    $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
    /*Cargar el combo de Equipo Mantenimiento*/
    EquipoMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoEquipoMantenimiento = [];
                $scope.ListadoEquipoMantenimiento = response.data.Datos;
                $scope.ListadoEquipoMantenimiento.push({ Nombre: '(TODOS)', Codigo: 0 });
                $scope.Modelo.Equipo = $linq.Enumerable().From($scope.ListadoEquipoMantenimiento).First('$.Codigo == 0');
            }
        }, function (response) {
            ShowError(response.statusText);
        });

    /*---------------------------------------------------------------------Funcion Buscar Tareas Mantenimiento-----------------------------------------------------------------------------*/
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            Find()
        }
    };
    //Cargar autocomplete conductor 
    $scope.AutocompleteProveedores = function (value) {
        $scope.ListaProveedores = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, CadenaPerfiles: PERFIL_PROVEEDOR, Sync: true }).Datos;
        
            
        return $scope.ListaProveedores;
    }
    function DatosRequeridos() {
        var continuar = true

        if ($scope.Modelo.FechaInicial == undefined || $scope.Modelo.FechaFinal == undefined) {
            if (($scope.Modelo.NumeroInicial == undefined || $scope.Modelo.NumeroInicial == null || $scope.Modelo.NumeroInicial == '' || isNaN($scope.Modelo.NumeroInicial)) || ($scope.Modelo.NumeroFinal == undefined || $scope.Modelo.NumeroFinal == null || $scope.Modelo.NumeroFinal == '' || isNaN($scope.Modelo.NumeroFinal)) ) {
                $scope.MensajesError.push('debe ingresar filtros de fecha o Número')
                continuar = false
            }
        }
        if ($scope.Modelo.NumeroInicial !== undefined && $scope.Modelo.NumeroInicial !== null && $scope.Modelo.NumeroFinal !== undefined && $scope.Modelo.NumeroFinal !== null) {
            if ($scope.Modelo.NumeroInicial > $scope.Modelo.NumeroFinal) {
                $scope.MensajesError.push('El número inicial no puede ser mayor al número final')
                continuar = false
            }
        }
        if ($scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== null) {
            if ($scope.Modelo.FechaInicial > $scope.Modelo.FechaFinal) {
                $scope.MensajesError.push('La fecha inicial no puede ser mayor al la fecha final')
                continuar = false
            }
        }
        if ($scope.Modelo.Proveedor !== undefined && $scope.Modelo.Proveedor !== '' && $scope.Modelo.Proveedor !== null) {
            if ($scope.Modelo.Proveedor.Codigo == 0 || $scope.Modelo.Proveedor.Codigo == undefined) {
                $scope.MensajesError.push('Debe seleccionar un proveedor válido')
                continuar = false
            }
        }
        return continuar
    }

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);

        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoOrdenTrabajoMantenimiento = [];
        $scope.Modelo.Pagina = $scope.paginaActual
        $scope.Modelo.RegistrosPagina = $scope.cantidadRegistrosPorPagina
        $scope.Modelo.Estado = $scope.Estado.Codigo
        if (DatosRequeridos()) {
            blockUI.delay = 1000;

            OrdenTrabajoMantenimientoFactory.Consultar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoOrdenTrabajoMantenimiento = response.data.Datos
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        else {
            $scope.Buscando = false;
        }
        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------Anular Contrato-----------------------------------------------------------*/

    /*Validacion boton anular */
    $scope.ConfirmacionAnularContrato = function (numero, EstadoDocumento) {
        $scope.estadoanulado = EstadoDocumento;
        $scope.NumeroAnulacion = numero
        if ($scope.ValidarPermisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
            if ($scope.estadoanulado.Codigo !== CODIGO_ESTADO_ANULADO) {
                showModal('modalConfirmacionAnularTarea');
            }
            else {
                closeModal('modalConfirmacionAnularTarea');
            }
        }
    };
    /*Funcion que solicita los datos de la anulación */
    $scope.SolicitarDatosAnulacion = function () {
        $scope.FechaAnulacion = new Date();
        closeModal('modalConfirmacionAnularTarea');
        showModal('modalDatosAnularTarea');
    };
    /*---------------------------------------------------------------------Funcion Anular Tarea Mantenimiento ----------------------------------------------------------------------------*/
    $scope.Anular = function () {

        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: $scope.NumeroAnulacion,
            UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CausaAnulacion: $scope.CausaAnulacion,
        };

        OrdenTrabajoMantenimientoFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se anuló la tarea de mantenimiento  No.' + entidad.Numero);
                    closeModal('modalDatosAnularTarea');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                ShowError(response.statusText);
            });
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    $scope.MaskNumero = function () {
        MascaraNumeroGeneral($scope)
    };


    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarOrdenTrabajoMantenimiento';
        }
    };
   
    $scope.urlASP = '';
    $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
    $scope.DesplegarInforme = function (Numero) {
        if ($scope.ValidarPermisos.AplicaImprimir == PERMISO_ACTIVO) {
            $scope.FiltroArmado = '';

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.Codigo = Numero;
            $scope.NombreReporte = 'RepOrdenTrabajo'
            //Arma el filtro
            $scope.ArmarFiltro();
            //Llama a la pagina ASP con el filtro por GET

            window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + 1);

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';
        };
    };

    // funcion enviar parametros al proyecto ASP armando el filtro
    $scope.ArmarFiltro = function () {
        $scope.FiltroArmado = '';
        if ($scope.Codigo !== undefined && $scope.Codigo !== '' && $scope.Codigo !== null) {
            $scope.FiltroArmado += '&Numero=' + $scope.Codigo;
        }
    }


    if ($routeParams.Codigo > 0) {
        $scope.Modelo.NumeroInicial = $routeParams.Codigo;
        $scope.Modelo.NumeroFinal = $routeParams.Codigo;
        $scope.Modelo.FechaFinal = '';
        $scope.Modelo.FechaInicial = '';
        Find();
    }

    $scope.MaskMayus = function () {
        MascaraMayusGeneral($scope);
    }
}]);