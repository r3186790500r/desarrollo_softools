﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Entidades.Despachos.Procesos
Imports EncoExpres.GesCarga.Entidades.Utilitarios
Imports EncoExpres.GesCarga.Repositorio.Utilitarios
Imports System.IO
Imports System.Net.Mail
Imports System.Transactions

Namespace ControlViajes

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleSeguimientoVehiculos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleSeguimientoVehiculos
        Inherits RepositorioBase(Of DetalleSeguimientoVehiculos)

        Public Overrides Function Consultar(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Dim lista As New List(Of DetalleSeguimientoVehiculos)
            Dim item As DetalleSeguimientoVehiculos
            Dim resultado As IDataReader
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    If filtro.NumeroDocumentoPlanilla > Cero Then
                        conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroDocumentoPlanilla)
                    End If
                    If filtro.NumeroDocumentoOrden > Cero Then
                        conexion.AgregarParametroSQL("@par_ENOC_Numero", filtro.NumeroDocumentoOrden)
                    End If
                    If filtro.NumeroManifiesto > Cero Then
                        conexion.AgregarParametroSQL("@par_Numero_Manifiesto", filtro.NumeroManifiesto)
                    End If
                    If Not IsNothing(filtro.PuestoControl) Then
                        If filtro.PuestoControl.Codigo > Cero Then
                            conexion.AgregarParametroSQL("@par_Puesto_Control", filtro.PuestoControl.Codigo)
                        End If
                    End If
                    If Not IsNothing(filtro.Vehiculo) Then
                        If Not IsNothing(filtro.Vehiculo.Placa) And filtro.Vehiculo.Placa <> "" Then
                            conexion.AgregarParametroSQL("@par_Vehiculo", filtro.Vehiculo.Placa)
                        End If
                    End If
                    If Not IsNothing(filtro.Conductor) Then
                        If filtro.Conductor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", filtro.Conductor.Codigo) '''''''''''''''''''''''''''''''''''''''''
                        ElseIf Not IsNothing(filtro.Conductor.NombreCompleto) And filtro.Conductor.NombreCompleto <> "" Then
                            conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.NombreCompleto)
                        End If
                    End If
                    If Not IsNothing(filtro.Ruta) Then
                        If filtro.Ruta.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Ruta", filtro.Ruta.Codigo)
                        End If
                    End If
                    If Not IsNothing(filtro.SitioReporteSeguimiento) Then
                        If filtro.SitioReporteSeguimiento.Codigo > CODIGO_CATALOGO_SITIO_SEGUIMIENTO_NO_APLICA Then
                            conexion.AgregarParametroSQL("@par_CATA_SRSV_Codigo", filtro.SitioReporteSeguimiento.Codigo)
                        End If
                    End If
                    If Not IsNothing(filtro.TipoOrigenSeguimiento) Then
                        If filtro.TipoOrigenSeguimiento.Codigo <> Cero And filtro.TipoOrigenSeguimiento.Codigo <> CODIGO_CATALOGO_TIPO_ORIGEN_SEGUIMIENTO_NO_APLICA Then
                            conexion.AgregarParametroSQL("@par_CATA_TOSV_Codigo", filtro.TipoOrigenSeguimiento.Codigo)
                        End If
                    End If
                    If Not IsNothing(filtro.Mostrar) Then
                        If filtro.Mostrar.CampoAuxiliar2 <> "" Then
                            If filtro.Mostrar.CampoAuxiliar2 > Cero Then
                                If filtro.Anulado = Cero Then
                                    conexion.AgregarParametroSQL("@par_Mostrar", filtro.Mostrar.CampoAuxiliar2)
                                End If
                            End If
                        End If
                    End If
                    If Not IsNothing(filtro.Anulado) Then
                        conexion.AgregarParametroSQL("@par_Anulado", filtro.Anulado)
                    End If

                    If Not IsNothing(filtro.CodigosOficinas) Then
                        conexion.AgregarParametroSQL("@par_Codigos_Oficinas", filtro.CodigosOficinas)
                    End If

                    If Not IsNothing(filtro.CodigosNivelRiesgo) Then
                        If filtro.CodigosNivelRiesgo <> "" Then
                            conexion.AgregarParametroSQL("@par_Codigos_NivelRiesgo", filtro.CodigosNivelRiesgo)
                        End If
                    End If

                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > Cero Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > Cero Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If
                    If Not IsNothing(filtro.TipoDueno) Then
                        If filtro.TipoDueno.Codigo <> 2100 Then ''NO APLICA
                            conexion.AgregarParametroSQL("@par_CATA_TIDU_Codigo", filtro.TipoDueno.Codigo)

                        End If
                    End If
                    If Not IsNothing(filtro.Cliente) Then
                        If filtro.Cliente.Codigo > 0 Then ''NO APLICA
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.Cliente.Codigo)

                        End If
                    End If
                    If Not IsNothing(filtro.Origen) Then
                        If filtro.Origen.Codigo > 0 Then ''NO APLICA
                            conexion.AgregarParametroSQL("@par_CIUD_Codigo_Origen", filtro.Origen.Codigo)

                        End If
                    End If
                    If Not IsNothing(filtro.Destino) Then
                        If filtro.Destino.Codigo > 0 Then ''NO APLICA
                            conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", filtro.Destino.Codigo)

                        End If
                    End If
                    If Not IsNothing(filtro.Producto) Then
                        If filtro.Producto.Codigo > 0 Then ''NO APLICA
                            conexion.AgregarParametroSQL("@par_PRTR_Codigo", filtro.Producto.Codigo)

                        End If
                    End If

                    If filtro.Historico = 0 Then
                        If filtro.FlotaPropia = 1 Then
                            'Consulta seguimiento Flota Propia
                            If filtro.FechaInicial > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Inicio", filtro.FechaInicial, SqlDbType.Date)
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_flota_propia]")
                        ElseIf filtro.TipoConsulta = Cero Then
                            'Consulta seguimiento Orden de Cargue
                            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_vehiculos_orden_cargue]")
                        ElseIf filtro.TipoConsulta = 1 Then
                            'Consulta seguimiento Planilla Despachos
                            If filtro.ConsultaGPS > 0 Then
                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_vehiculos_planilla_despachos_gps]")
                            Else
                                If filtro.ReportarCliente > 0 Then
                                    conexion.AgregarParametroSQL("@par_Reportar_Cliente", 1)
                                End If
                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_vehiculos_planilla_despachos]")
                            End If
                        Else
                            'Consulta seguimiento Consultas
                            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultas_detalle_seguimiento_vehiculos]")
                        End If
                    Else
                        If filtro.FechaInicial > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                        End If
                        If filtro.FechaFinal > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                        End If
                        If filtro.FlotaPropia = 1 Then
                            'Consulta seguimiento Flota Propia
                            If filtro.FechaInicial > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Inicio", filtro.FechaInicial, SqlDbType.Date)
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_flota_propia_historico]")
                        ElseIf filtro.TipoConsulta = Cero Then
                            'Consulta seguimiento Orden de Cargue
                            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_vehiculos_orden_cargue_historico]")
                        ElseIf filtro.TipoConsulta = 1 Then
                            'Consulta seguimiento Planilla Despachos
                            If filtro.ConsultaGPS > 0 Then
                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_vehiculos_planilla_despachos_gps]")
                            Else
                                If filtro.ReportarCliente > 0 Then
                                    conexion.AgregarParametroSQL("@par_Reportar_Cliente", 1)
                                End If
                                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_vehiculos_planilla_despachos_historico]")
                            End If
                        ElseIf filtro.TipoConsulta = 2 Then
                            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultas_detalle_seguimiento_vehiculos_despachos]")
                        Else
                            'Consulta seguimiento Consultas
                            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultas_detalle_seguimiento_vehiculos]")
                        End If
                    End If

                    While resultado.Read
                        item = New DetalleSeguimientoVehiculos(resultado)
                        lista.Add(item)
                    End While
                    transaccion.Complete()
                End Using
            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleSeguimientoVehiculos) As Long
            Dim inserto As Boolean = True
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        If entidad.ModificaContenedor > 0 Then
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
                            conexion.AgregarParametroSQL("@par_Numero_Contenedor", entidad.NumeroContenedor)
                            If Not IsNothing(entidad.CiudadDevolucion) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Devolucion_Contenedor", entidad.CiudadDevolucion.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Patio_Devolcuion", entidad.PatioDevolucion)
                            If entidad.FechaDevolucion > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Devolcuion", entidad.FechaDevolucion, SqlDbType.Date)
                            End If
                            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_modificar_detalle_contenedor]")
                            While resultado.Read
                                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                            End While
                            resultado.Close()
                        Else
                            If entidad.FlotaPropia <> 1 Then
                                conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.NumeroPlanilla)
                                conexion.AgregarParametroSQL("@par_ENOC_Numero", entidad.NumeroOrden)
                                conexion.AgregarParametroSQL("@par_Oficina", entidad.Oficina.Codigo)
                            Else
                                conexion.AgregarParametroSQL("@par_Oficina", entidad.Oficina.Codigo)
                            End If
                            If entidad.NumeroManifiesto > 0 Then
                                conexion.AgregarParametroSQL("@par_Numero_Manifiesto", entidad.NumeroManifiesto)
                            End If
                            If Not IsNothing(entidad.Ubicacion) Then
                                conexion.AgregarParametroSQL("@par_Ubicacion", entidad.Ubicacion)
                            End If
                            If Not IsNothing(entidad.Longitud) Then
                                conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Float)
                            End If
                            If Not IsNothing(entidad.Latitud) Then
                                conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Float)
                            End If
                            If Not IsNothing(entidad.PuestoControl) Then
                                conexion.AgregarParametroSQL("@par_PUCO_Codigo", entidad.PuestoControl.Codigo)
                            End If
                            If Not IsNothing(entidad.TipoOrigenSeguimiento) Then
                                conexion.AgregarParametroSQL("@par_CATA_TOSV_Codigo", entidad.TipoOrigenSeguimiento.Codigo)
                            End If
                            If Not IsNothing(entidad.SitioReporteSeguimiento) Then
                                conexion.AgregarParametroSQL("@par_CATA_SRSV_Codigo", entidad.SitioReporteSeguimiento.Codigo)
                            End If

                            If Not IsNothing(entidad.NovedadSeguimiento) Then
                                conexion.AgregarParametroSQL("@par_CATA_NOSV_Codigo", entidad.NovedadSeguimiento.Codigo)
                            End If
                            If Not IsNothing(entidad.Observaciones) Then
                                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                            End If
                            If Not IsNothing(entidad.KilometrosVehiculo) Then
                                conexion.AgregarParametroSQL("@par_Kilometros_Vehiculo", entidad.KilometrosVehiculo)
                            End If
                            If Not IsNothing(entidad.ReportarCliente) Then
                                conexion.AgregarParametroSQL("@par_Reportar_Cliente", entidad.ReportarCliente)
                            End If
                            If Not IsNothing(entidad.Vehiculo) Then
                                conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                            End If
                            If Not IsNothing(entidad.Ruta) Then
                                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                            End If
                            If Not IsNothing(entidad.Prioritario) Then
                                conexion.AgregarParametroSQL("@par_Prioritario", entidad.Prioritario)
                            End If
                            If Not IsNothing(entidad.EnvioReporteCliente) Then
                                conexion.AgregarParametroSQL("@par_Envio_Reporte_Cliente", entidad.EnvioReporteCliente)
                            End If
                            If entidad.FechaReporteCliente > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Reporte_Cliente", entidad.FechaReporteCliente)
                            End If
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                            If entidad.FechaReporte > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Reporte", entidad.FechaReporte, SqlDbType.DateTime)
                            End If

                            If entidad.FlotaPropia = 1 Then
                                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_seguimiento_flota_propia]")
                                While resultado.Read
                                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                                End While
                                resultado.Close()
                            Else
                                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_seguimiento_vehiculos]")
                                While resultado.Read
                                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                                End While
                                resultado.Close()
                            End If

                        End If
                        If entidad.Codigo.Equals(Cero) Then
                            inserto = False
                            Return Cero
                        End If

                        If Not IsNothing(entidad.ListaSeguimientosRemesas) Then
                            For Each seguimiento In entidad.ListaSeguimientosRemesas
                                If Not InsertarSeguimientosRemesas(seguimiento, conexion) Then
                                    inserto = False
                                    Exit For
                                End If
                            Next
                        End If

                    End Using

                    If inserto Then

                        transaccion.Complete()
                    Else
                        entidad.Codigo = Cero
                    End If

                End Using
            End Using
            Return entidad.Codigo
        End Function
        Public Function InsertarDocumentoSeguimientopdf(entidad As DetalleSeguimientoVehiculos, ByRef contextoConexion As DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Numero_Documento", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TIDO_Codigo", 120)
            contextoConexion.AgregarParametroSQL("@par_Documento", entidad.Documento, SqlDbType.VarBinary)
            contextoConexion.AgregarParametroSQL("@par_Extension_Documento", ".pdf")
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_evento_correo_documentos]")

            While resultado.Read
                inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
            End While
            resultado.Close()

            Return inserto
        End Function
        Public Function InsertarSeguimientosRemesas(entidad As DetalleSeguimientoVehiculos, ByRef Conexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            Conexion.CleanParameters()
            Conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            Conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.NumeroPlanilla)
            If entidad.NumeroRemesa > 0 Then
                Conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
            End If
            If Not IsNothing(entidad.TipoOrigenSeguimiento) Then
                Conexion.AgregarParametroSQL("@par_CATA_TOSV_Codigo", entidad.TipoOrigenSeguimiento.Codigo)
            End If
            If Not IsNothing(entidad.Ubicacion) Then
                Conexion.AgregarParametroSQL("@par_Ubicacion", entidad.Ubicacion)
            End If
            If Not IsNothing(entidad.Longitud) Then
                Conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Float)
            End If
            If Not IsNothing(entidad.Latitud) Then
                Conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Float)
            End If
            If Not IsNothing(entidad.PuestoControl) Then
                Conexion.AgregarParametroSQL("@par_PUCO_Codigo", entidad.PuestoControl.Codigo)
            End If
            If Not IsNothing(entidad.SitioReporteSeguimiento) Then
                Conexion.AgregarParametroSQL("@par_CATA_SRSV_Codigo", entidad.SitioReporteSeguimiento.Codigo)
            End If
            If Not IsNothing(entidad.NovedadSeguimiento) Then
                Conexion.AgregarParametroSQL("@par_CATA_NOSV_Codigo", entidad.NovedadSeguimiento.Codigo)
            End If
            If Not IsNothing(entidad.Observaciones) Then
                Conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            End If
            If Not IsNothing(entidad.ReportarCliente) Then
                Conexion.AgregarParametroSQL("@par_Reportar_Cliente", entidad.ReportarCliente)
            End If
            If Not IsNothing(entidad.EnvioReporteCliente) Then
                Conexion.AgregarParametroSQL("@par_Envio_Reporte_Cliente", entidad.EnvioReporteCliente)
            End If
            If entidad.FechaReporteCliente > Date.MinValue Then
                Conexion.AgregarParametroSQL("@par_Fecha_Reporte_Cliente", entidad.FechaReporteCliente)
            End If
            Conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = Conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_seguimiento_vehiculos_remesas]")
            While resultado.Read
                inserto = IIf(resultado.Item("Codigo") > Cero, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As DetalleSeguimientoVehiculos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleSeguimientoVehiculos) As DetalleSeguimientoVehiculos
            Dim item As New DetalleSeguimientoVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Tipo_Consulta", filtro.TipoConsulta)

                If filtro.FlotaPropia = 1 Then
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_detalle_seguimiento_flota_propia]")

                    While resultado.Read
                        item = New DetalleSeguimientoVehiculos(resultado)
                    End While
                Else
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_detalle_seguimiento_vehiculos]")

                    While resultado.Read
                        item = New DetalleSeguimientoVehiculos(resultado)
                    End While
                    resultado.Close()
                    item.ListaPrecintos = ObtenerPrecintos(item, conexion)
                End If

            End Using
            Return item
        End Function
        Public Function ObtenerPrecintos(entidad As DetalleSeguimientoVehiculos, conexion As Conexion.DataBaseFactory) As IEnumerable(Of DetallePrecintos)
            Dim item As New DetallePrecintos
            Dim lista As New List(Of DetallePrecintos)

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENPD_Codigo", entidad.NumeroPlanilla)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_precintos_remesas_planilla")

            While resultado.Read
                item = New DetallePrecintos(resultado)
                lista.Add(item)
            End While
            resultado.Close()

            Return lista
        End Function
        Public Function Anular(entidad As DetalleSeguimientoVehiculos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", entidad.NumeroDocumentoRemesa)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)

                If entidad.FlotaPropia = 1 Then
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_anular_detalle_seguimiento_flota_propia]")

                    While resultado.Read
                        anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                    End While
                Else
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_anular_detalle_seguimiento_vehiculos]")

                    While resultado.Read
                        anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                    End While
                End If

            End Using
            Return anulo
        End Function

        Public Function EnviarCorreoCliente(entidad As DetalleSeguimientoVehiculos) As Boolean

            Dim inserto As Boolean = True
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                If entidad.Documento.Length > 0 Then
                    Call InsertarDocumentoSeguimientopdf(entidad, conexionDocumentos)
                End If
            End Using
            If inserto Then
                entidad.TipoDocumento = 120
                Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
                Correos.GenerarCorreo(entidad)
            End If
            Return inserto
        End Function

        'Enviar Email Seguimiento Vehicular'

        Public Function EnviarEmailSeguimientoVehicular(entidad As DetalleSeguimientoVehiculos) As Boolean

            Dim inserto As Boolean = True
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                If entidad.Documento.Length > 0 Then
                    Call InsertarDocumentoSeguimientopdf(entidad, conexionDocumentos)
                End If
            End Using
            If inserto Then
                entidad.TipoDocumento = 120
                Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
                Correos.GenerarCorreo(entidad)
            End If
            Return inserto
        End Function

        Public Function CambiarRutaSeguimiento(entidad As DetalleSeguimientoVehiculos) As Boolean
            Dim cambio As Boolean = False

            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_Ruta", entidad.Ruta.Codigo)
                conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", entidad.NumeroPlanilla)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gps_cambiar_Ruta_Seguimiento_Vehicular]")

                While resultado.Read
                    cambio = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While
                resultado.Close()

            End Using
            Return cambio
        End Function

        Public Function EnviarCorreoAvanceVehiculosCliente(entidad As DetalleSeguimientoVehiculos) As Boolean

            Dim inserto As Boolean = True
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                If entidad.Documento.Length > 0 Then
                    Call InsertarDocumentoSeguimientopdf(entidad, conexionDocumentos)
                End If
            End Using
            If inserto Then
                entidad.TipoDocumento = 12001
                Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
                Correos.GenerarCorreo(entidad)
            End If
            Return inserto
        End Function

        Public Function EnviarDocumentosConductor(entidad As DetalleSeguimientoVehiculos) As Boolean
            Try
                Dim repoconf As New RepositorioConfiguracionServidorCorreos
                Dim Configuracion As New ConfiguracionServidorCorreos
                Configuracion.CodigoEmpresa = entidad.CodigoEmpresa
                Configuracion = repoconf.Obtener(Configuracion)

                Dim Mail As New MailMessage
                Dim Conexion As New SmtpClient
                Dim Remitente As New MailAddress(Configuracion.Usuario, Configuracion.Remitente)
                Mail.From = Remitente
                Mail.To.Add(entidad.CorreoConductor)
                'Mail.To.Add(Configuracion.Receptor)
                Mail.Subject = "Documentos despacho " + entidad.NumeroDocumentoManifiesto.ToString()
                Mail.Body = "Adjunto a este correo se enceuntran los documentos correspondientes al despacho de la planilla n°" + entidad.NumeroDocumentoPlanilla.ToString()
                Conexion.EnableSsl = Configuracion.AutenticacionSSL
                If Configuracion.AutenticacionRequerida > 0 Then
                    Conexion.Credentials = New Net.NetworkCredential(Configuracion.Usuario, Configuracion.Contraseña)
                End If
                Conexion.Port = Configuracion.SmtpPuerto
                Conexion.Host = Configuracion.Smtp
                If Not IsNothing(entidad.DocumentoManifiesto) Then
                    Mail.Attachments.Add(New Attachment(New MemoryStream(entidad.DocumentoManifiesto), "Manifiesto " + entidad.NumeroDocumentoManifiesto.ToString() + ".pdf"))
                End If
                If Not IsNothing(entidad.DocumentoManifiesto) Then
                    Mail.Attachments.Add(New Attachment(New MemoryStream(entidad.DocumentoPlanilla), "Planilla " + entidad.NumeroDocumentoPlanilla.ToString() + ".pdf"))
                End If
                If Not IsNothing(entidad.DocumentoOrdenCargue) Then
                    Mail.Attachments.Add(New Attachment(New MemoryStream(entidad.DocumentoOrdenCargue), "Orden Cargue " + entidad.NumeroDocumentoOrden.ToString() + ".pdf"))
                End If
                If Not IsNothing(entidad.DocumentoRemesa) Then
                    Mail.Attachments.Add(New Attachment(New MemoryStream(entidad.DocumentoRemesa), "Remesas.pdf"))
                End If
                Conexion.Send(Mail)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function ConsultarMasterRemesa(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Dim lista As New List(Of DetalleSeguimientoVehiculos)
            Dim item As DetalleSeguimientoVehiculos
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                '   If filtro.Consulta = 1 Then
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If
                ' End If
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Vehiculo) Then
                    If Not IsNothing(filtro.Vehiculo.Placa) And filtro.Vehiculo.Placa <> "" Then
                        conexion.AgregarParametroSQL("@par_Vehiculo", filtro.Vehiculo.Placa)
                    End If
                End If
                If Not IsNothing(filtro.Ruta) Then
                    If filtro.Ruta.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Ruta", filtro.Ruta.Codigo)
                    End If
                End If
                If filtro.NumeroDocumentoRemesa > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", filtro.NumeroDocumentoRemesa)
                End If

                If filtro.FechaRemesa > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Remesa", filtro.FechaRemesa, SqlDbType.DateTime)
                End If
                If Not IsNothing(filtro.Cliente) Then
                    If Not IsNothing(filtro.Cliente.NombreCompleto) And filtro.Cliente.NombreCompleto <> "" Then
                        conexion.AgregarParametroSQL("@par_Cliente", filtro.Cliente.NombreCompleto)
                    End If
                End If
                If Len(filtro.DocumentoCliente) > 0 Then
                    conexion.AgregarParametroSQL("@par_Documento_Cliente", filtro.DocumentoCliente)
                End If
                If Len(filtro.DocumentoDistribucionCliente) > 0 Then
                    conexion.AgregarParametroSQL("@par_Documento_Distribucion_Cliente", filtro.DocumentoDistribucionCliente)
                End If
                If Not IsNothing(filtro.TipoOrigenSeguimiento) Then
                    If filtro.TipoOrigenSeguimiento.Codigo <> Cero And filtro.TipoOrigenSeguimiento.Codigo <> CODIGO_CATALOGO_TIPO_ORIGEN_SEGUIMIENTO_NO_APLICA Then
                        conexion.AgregarParametroSQL("@par_CATA_TOSV_Codigo", filtro.TipoOrigenSeguimiento.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.SitioReporteSeguimiento) Then
                    If filtro.SitioReporteSeguimiento.Codigo > CODIGO_CATALOGO_SITIO_SEGUIMIENTO_NO_APLICA Then
                        conexion.AgregarParametroSQL("@par_CATA_SRSV_Codigo", filtro.SitioReporteSeguimiento.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Mostrar) Then
                    If filtro.Mostrar.CampoAuxiliar2 <> "" Then
                        If filtro.Mostrar.CampoAuxiliar2 > Cero Then
                            If filtro.Anulado = Cero Then
                                If filtro.Consulta = 1 Then
                                    conexion.AgregarParametroSQL("@par_Mostrar", filtro.Mostrar.CampoAuxiliar2)
                                End If
                            End If
                        End If
                    End If
                End If
                If Not IsNothing(filtro.Anulado) Then
                    If filtro.Consulta = 1 Then
                        conexion.AgregarParametroSQL("@par_Anulado", filtro.Anulado)
                    End If
                End If
                If Not IsNothing(filtro.CodigosOficinas) Then
                    If filtro.Consulta = 1 Then
                        conexion.AgregarParametroSQL("@par_Codigos_Oficinas", filtro.CodigosOficinas)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If filtro.ReportarCliente > 0 Then
                    conexion.AgregarParametroSQL("@par_Reportar_Cliente", 1)
                End If
                If filtro.Consulta = 1 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultas_detalle_seguimiento_remesas]")
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_remesas_historico]")
                End If

                While resultado.Read
                    item = New DetalleSeguimientoVehiculos(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function


        Public Function ConsultarPuntosControl(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Dim lista As New List(Of DetalleSeguimientoVehiculos)
            Dim item As DetalleSeguimientoVehiculos
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENMC_Numero", filtro.NumeroDocumentoPlanilla)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroManifiesto)


                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_puestos_control_seguimiento_vehicular]")

                While resultado.Read
                    item = New DetalleSeguimientoVehiculos(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Function ConsultarRemesas(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Dim lista As New List(Of DetalleSeguimientoVehiculos)
            Dim item As DetalleSeguimientoVehiculos
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                If filtro.Orden > 0 Then
                    conexion.AgregarParametroSQL("@par_Orden", filtro.Orden)
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_vehiculos_remesas]")

                While resultado.Read
                    item = New DetalleSeguimientoVehiculos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()

            End Using
            Return lista
        End Function

        Public Function ConsultarNovedadesSitiosReporte(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Dim lista As New List(Of DetalleSeguimientoVehiculos)
            Dim item As DetalleSeguimientoVehiculos
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CATA_SRSV_Codigo", filtro.SitioReporteSeguimiento.Codigo)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_novedades_sitio_reporte]")

                While resultado.Read
                    item = New DetalleSeguimientoVehiculos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()

            End Using
            Return lista
        End Function

        Public Function ConsultarPuestoControlRutas(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Dim lista As New List(Of DetalleSeguimientoVehiculos)
            Dim item As DetalleSeguimientoVehiculos
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", filtro.Ruta.Codigo)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_puesto_control_rutas]")

                While resultado.Read
                    item = New DetalleSeguimientoVehiculos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()

            End Using
            Return lista
        End Function

        Public Function ConsultarRemesasSinManifiesto(filtro As ReporteMinisterio) As IEnumerable(Of ReporteMinisterio)
            Dim lista As New List(Of ReporteMinisterio)
            Dim item As ReporteMinisterio

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                Dim resultado As IDataReader

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.CodigosOficinas) Then
                    conexion.AgregarParametroSQL("@par_Codigos_Oficinas", filtro.CodigosOficinas)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_seguimiento_manifiesto_reporte_rndc")

                While resultado.Read
                    item = New ReporteMinisterio(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function


    End Class

End Namespace