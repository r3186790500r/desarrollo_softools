﻿EncoExpresApp.controller("GestionarRecoleccionesPaqueteriaCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'blockUIConfig', 'RutasFactory', 'CiudadesFactory', 'OficinasFactory', 'ZonasFactory',
    'RecoleccionesFactory', 'UnidadEmpaqueFactory', 'TercerosFactory', 'ValorCatalogosFactory', 'ProductoTransportadosFactory', 'TarifarioVentasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, blockUIConfig, RutasFactory, CiudadesFactory, OficinasFactory, ZonasFactory,
        RecoleccionesFactory, UnidadEmpaqueFactory, TercerosFactory, ValorCatalogosFactory, ProductoTransportadosFactory, TarifarioVentasFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'GESTIONAR RECOLECCIONES';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Recolecciones' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ModeloOficinaGestiona = '';

        try {
            try {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.RECOLECCIONES);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

            } catch (e) {

                //$scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_RECOLECCIONES);
                //$scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                //$scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                //$scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                //$scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                //$scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
            }
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        $scope.ManejoPesoVolumetrico = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria;
        //--------------------------Validaciones Proceso-------------------------//
        //--------------------------Variables-------------------------//
        $scope.Deshabilitar = false;
        $scope.DestinatarioNoRegistrado = false;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            Fecha: new Date(),
            FormaPago: {},
            Zona: {},
            Remitente: {
                TipoIdentificacion: {}
            },
            Destinatario: {
                TipoIdentificacion: {}
            }
        };
        $scope.DeshabilitarNumeroDocumento = false;
        $scope.DeshabilitarCiudad = false;
        $scope.Numero = 0;
        $scope.ModeloFecha = new Date();
        $scope.Latitud = '';
        $scope.Longitud = '';
        $scope.ListadoCliente = [];
        $scope.ListadoTipoIdentificacion = [];
        $scope.ListadoFormaPago = [];
        $scope.ListadoZonas = [];
        $scope.ListadoUnidadEmpaque = [];
        $scope.ListadoHorariosEntrega = [];
        $scope.ListadoCiudades = [];
        $scope.ListadoProductoTransportados = [];

        $scope.ListadoTipoLineaNegocioTransportes = [];
        $scope.ListadoRutas = [];
        $scope.ListadoTarifaTransportes = [];

        $scope.ListaAuxTipoTarifas = [];
        $scope.ListaTarifaCarga = [];
        $scope.ListaTipoTarifaCargaVenta = [];
        //--Receptoria
        $scope.Comision = 0;
        $scope.Agencista = 0;
        var TMPValorFleteCliente = 0;
        var TMPPorcentajeAfiliado = 0;
        var TMPPorcentajeSeguro = 0;
        var OficinaObj;
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_BORRADOR);

        var AplicaTarifaProducto = -1;
        $scope.ListaCondicionesComerciales = [];
        var TMPRemitente;
        $scope.ListadoRemitente = [];
        var TMPDestinatario;
        $scope.ListadoDestinatario = [];
        $scope.MensajesErrorTarifas = [];
        //--Receptoria
        $scope.ListadoLineaNegocioPaqueteria = [];
        var tarifaConReexpediccion = false;
        var FormasPagoCliente = [];
        $scope.PesoNormal = true;

        $scope.MostrarApellidoRemitente = true;
        $scope.MostrarApellidoDestinatario = true;

        var dataRemitente = 0;
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete
        //--Clientes
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //--Clientes
        //--Remitente
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_REMITENTE,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoRemitente);
                }
            }
            return $scope.ListadoRemitente;
        };
        //--Remitente
        //--Producto Transportado
        $scope.AutocompleteProductoTransportado = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoProductoTransportados = [];
                    blockUIConfig.autoBlock = false;
                    var Response = ProductoTransportadosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        AplicaTarifario: AplicaTarifaProducto,
                        Sync: true
                    });
                    $scope.ListadoProductoTransportados = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProductoTransportados);
                }
            }
            return $scope.ListadoProductoTransportados;
        };
        //--Producto Transportado
        //--Ciudades
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        };
        //--Ciudades
        //--Destinatario
        $scope.AutocompleteDestinatario = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_DESTINATARIO,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoDestinatario = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoDestinatario);
                }
            }
            return $scope.ListadoDestinatario;
        };
        //--Destinatario
        //----------AutoComplete
        //--------------------------Init-------------------------//
        $scope.InitLoad = function () {
            //-- Oficina Receptoria
            OficinaObj = OficinasFactory.Obtener({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                Sync: true
            }).Datos;
            //-- Oficina Receptoria
            $scope.ListadoOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Pagina: 1, Codigo: -1, RegistrosPagina: -1, Sync: true }).Datos;
            $scope.Modelo.OficinaGestiona = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
            //--Forma de pago
            var ListaFormaPago = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS },
                Sync: true
            }).Datos;
            ListaFormaPago.splice(ListaFormaPago.findIndex(item => item.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA), 1);
            $scope.ListadoFormaPago.push({ Codigo: -1, Nombre: "(SELECCIONAR)" });
            $scope.ListadoFormaPago = $scope.ListadoFormaPago.concat(ListaFormaPago);
            $scope.Modelo.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==-1');
            //--tipo identificaciones Tercero
            $scope.ListadoTipoIdentificacion = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO }, Sync: true }).Datos;
            $scope.ListadoTipoIdentificacion.splice($scope.ListadoTipoIdentificacion.findIndex(item => item.Codigo === 100), 1);
            $scope.Modelo.Remitente.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0];
            $scope.Modelo.Destinatario.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0];
            //--Combo de zonas
            $scope.ListadoZonas = ZonasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: ESTADO_DEFINITIVO,
                Ciudad: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo },
                Sync: true
            }).Datos;
            $scope.ListadoZonas.push({ Codigo: -1, Nombre: "(NO APLICA)" });
            $scope.Modelo.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==' + -1);
            //--Unidad empaque
            $scope.ListadoUnidadEmpaque = UnidadEmpaqueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos;
            //--Catalogo Linea Negocio Paqueteria
            $scope.ListadoLineaNegocioPaqueteria = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CATALOGO_LINEA_NEGOCIO_PAQUETERIA.CODIGO },
                Sync: true
            }).Datos;
            $scope.Modelo.LineaNegocioPaqueteria = $linq.Enumerable().From($scope.ListadoLineaNegocioPaqueteria).First('$.Codigo ==' + CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA);
            //--Catalogo Linea Negocio Paqueteria
            //--Catalogo Horarios
            $scope.ListadoHorariosEntrega = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_HORARIO_ENTREGA_REMESAS_PAQUETERIA },
                Sync: true
            }).Datos;
            $scope.Modelo.HorarioEntrega = $scope.ListadoHorariosEntrega[0];
            //--Catalogo Horarios
            //---Obtener
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== '0') {
                $scope.Numero = $routeParams.Numero;
                if ($scope.Numero > 0) {
                    $scope.Titulo = 'CONSULTAR RECOLECCIÓN';
                    Obtener();
                }
            }
        };
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //--------------------------Init-------------------------//      
        //$scope.ModeloOficinaGestiona = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
        //-------------------------Funciones Generales-------------------------------//
        //--Validar Tipo Identificacion remitente y destinatario
        $scope.ValidarTIIDRemitente = function () {
            if ($scope.Modelo.Remitente.TipoIdentificacion.Codigo == 102) {
                //--NIT
                $scope.Modelo.Remitente.TipoNaturaleza = { Codigo: 502 };
                $scope.MostrarApellidoRemitente = false;
            }
            else {
                //--CC, CE, PASSPORT
                $scope.Modelo.Remitente.TipoNaturaleza = { Codigo: 501 };
                if ($scope.Modelo.Remitente.Codigo <= 0 || $scope.Modelo.Remitente.Codigo == undefined) {
                    $scope.MostrarApellidoRemitente = true;
                }
                else {
                    $scope.MostrarApellidoRemitente = false;
                }
            }
        };
        $scope.ValidarTIIDDestinatario = function () {
            if ($scope.Modelo.Destinatario.TipoIdentificacion.Codigo == 102) {
                //--NIT
                $scope.Modelo.Destinatario.TipoNaturaleza = { Codigo: 502 };
                $scope.MostrarApellidoDestinatario = false;
            }
            else {
                //--CC, CE, PASSPORT
                $scope.Modelo.Destinatario.TipoNaturaleza = { Codigo: 501 };
                if ($scope.Modelo.Destinatario.Codigo <= 0 || $scope.Modelo.Destinatario.Codigo == undefined) {
                    $scope.MostrarApellidoDestinatario = true;
                }
                else {
                    $scope.MostrarApellidoDestinatario = false;
                }
            }
        };
        //--Validar Tipo Identificacion remitente y destinatario

        //--Limpiar AutoCompletes
        $scope.limpiarControl = function (Control) {
            switch (Control) {
                case "Cliente":
                    $scope.Modelo.Cliente = "";
                    $scope.ValidarTarifarioCliente();
                    break;
                case "Remitente":
                    $scope.Modelo.Remitente.NumeroIdentificacion = "";
                    LimpiaCamposRemitente();
                    break;
                case "Destinatario":
                    $scope.Modelo.Destinatario.NumeroIdentificacion = "";
                    LimpiaCamposDestinatario();
                    break;
                case "Producto":
                    $scope.Modelo.ProductoTransportado = "";
                    break;
            }
        };
        //--Limpiar Autocompletes
        //--Validacion Cliente o Remitente
        $scope.ObtenerCliente = function () {
            var response = null;
            if ($scope.Modelo.Cliente != undefined && $scope.Modelo.Cliente != '' && $scope.Modelo.Cliente != null) {
                if ($scope.Modelo.Cliente.Codigo > 0) {
                    dataRemitente = PERFIL_CLIENTE;
                    response = TercerosFactory.Obtener({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Modelo.Cliente.Codigo,
                        Sync: true
                    });
                    $scope.Modelo.Remitente.NumeroIdentificacion = '';
                    LimpiaCamposRemitente();
                    ObtenerInformacionRemitente(response, PERFIL_CLIENTE);
                }
            }
            else {
                if (dataRemitente == PERFIL_CLIENTE) {
                    $scope.Modelo.Remitente.NumeroIdentificacion = '';
                    LimpiaCamposRemitente();
                }
            }
        };
        $scope.ObtenerRemitente = function () {
            var response = null;
            if ($scope.Modelo.Remitente.NumeroIdentificacion != undefined && $scope.Modelo.Remitente.NumeroIdentificacion != '' && $scope.Modelo.Remitente.NumeroIdentificacion != null) {
                $scope.RemitenteNoRegistrado = true;
                dataRemitente = PERFIL_REMITENTE;
                $scope.RemitenteNoRegistrado = true;
                response = TercerosFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.Modelo.Remitente.NumeroIdentificacion,
                    Sync: true
                });
                LimpiaCamposRemitente();
                ObtenerInformacionRemitente(response, PERFIL_REMITENTE);
            }
            else {
                if (dataRemitente == PERFIL_REMITENTE) {
                    $scope.RemitenteNoRegistrado = false;
                    LimpiaCamposRemitente();
                }
            }
        };
        $scope.ObtenerRemitenteNombre = function (Remitente) {
            var response = null;
            if (angular.isObject(Remitente)) {
                if (Remitente.Codigo > 0) {
                    $scope.RemitenteNoRegistrado = true;
                    dataRemitente = PERFIL_REMITENTE;
                    response = TercerosFactory.Obtener({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: Remitente.Codigo,
                        //CadenaPerfiles: PERFIL_REMITENTE,
                        Sync: true
                    });
                    $scope.Modelo.Remitente.NumeroIdentificacion = '';
                    LimpiaCamposRemitente();
                    ObtenerInformacionRemitente(response, PERFIL_REMITENTE);
                }
            }
        };
        function ObtenerInformacionRemitente(response, tipo) {
            //--Obtiene Informacion ya sea de cliente o remitente
            if (response != null) {
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.Codigo > 0) {
                        $scope.RemitenteNoRegistrado = false;
                        //--Carga Informacion Remitente en el Modelo
                        $scope.Modelo.Remitente = {
                            Codigo: response.Datos.Codigo,
                            Direccion: response.Datos.Direccion,
                            CentroCosto: response.Datos.CentroCosto,
                            LineaSerivicioCliente: response.Datos.LineaSerivicioCliente,
                            TipoIdentificacion: $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.Datos.TipoIdentificacion.Codigo),
                            NumeroIdentificacion: response.Datos.NumeroIdentificacion,
                            //Nombre: response.Datos.NombreCompleto,
                            Ciudad: $scope.CargarCiudad(response.Datos.Ciudad.Codigo),
                            //SitioCargue: response.Datos.SitioCargue,
                            Telefonos: response.Datos.Telefonos.replace(';', ' - '),
                            Correo: response.Datos.CorreoFacturacion
                            //Cliente: response.Datos.Cliente
                        };
                        $scope.NombreRemitente = response.Datos.NombreCompleto;
                        $scope.ValidarTIIDRemitente();

                        if (tipo == PERFIL_CLIENTE) {
                            $scope.NombreRemitente = response.Datos.NombreCompleto;
                            FormasPagoCliente = response.Datos.FormasPago;
                            //--Información Cliente
                            if (response.Datos.Cliente != undefined && response.Datos.Cliente != '' && response.Datos.Cliente != null) {
                                //--Condiciones Comerciales
                                if (response.Datos.Cliente.CondicionesComercialesTarifas == ESTADO_ACTIVO) {
                                    $scope.ListaCondicionesComerciales = response.Datos.Cliente.CondicionesComerciales;
                                }
                                //--Condiciones Comerciales
                            }
                            //--Información Cliente
                        }
                        //--Verifica si es cliente o remitente para bloqueo de campos
                        //if (dataRemitente == PERFIL_CLIENTE) {
                        //    $scope.BloqueoRemitenteNombre = true;
                        //    $scope.BloqueoRemitenteIdentificacion = true;
                        //    $scope.Modelo.Remitente.Nombre = response.Datos.NombreCompleto;
                        //}
                        //else {
                        //    $scope.BloqueoRemitenteNombre = false;
                        //    $scope.BloqueoRemitenteIdentificacion = false;
                        //}
                        TMPRemitente = angular.copy($scope.Modelo.Remitente);
                    }
                }
            }
        }
        function LimpiaCamposRemitente() {
            $scope.Modelo.Remitente.Codigo = '';
            $scope.NombreRemitente = '';
            $scope.Modelo.Remitente.PrimeroApellido = "";
            //$scope.ListadoDireccionesRemitente = [];
            $scope.Modelo.Remitente.Direccion = '';
            $scope.Modelo.Remitente.Ciudad = '';
            $scope.Modelo.Remitente.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0];
            $scope.Modelo.Remitente.Telefonos = '';
            $scope.Modelo.Remitente.Correo = '';
            $scope.ListaCondicionesComerciales = [];
            $scope.BloqueoRemitente = false;
            $scope.BloqueoRemitenteNombre = false;
            $scope.BloqueoRemitenteIdentificacion = false;
            $scope.MostrarApellidoRemitente = true;
            TMPRemitente = "";
        }
        //--Validacion Cliente o Remitente
        //--Validacion Destinatario
        $scope.ObtenerDestinatario = function () {
            var response = null;
            if ($scope.Modelo.Destinatario.NumeroIdentificacion != undefined && $scope.Modelo.Destinatario.NumeroIdentificacion != '' && $scope.Modelo.Destinatario.NumeroIdentificacion != null) {
                $scope.DestinatarioNoRegistrado = true;
                response = TercerosFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.Modelo.Destinatario.NumeroIdentificacion,
                    //CadenaPerfiles: PERFIL_DESTINATARIO,
                    Sync: true
                });
                LimpiaCamposDestinatario();
                ObtenerInformacionDestinatario(response);
            }
            else {
                $scope.DestinatarioNoRegistrado = false;
                LimpiaCamposDestinatario();
            }
        };

        $scope.ObtenerDestinatarioNombre = function (Destinatario) {
            var response = null;
            if (angular.isObject(Destinatario)) {
                if (Destinatario.Codigo > 0) {
                    $scope.DestinatarioNoRegistrado = true;
                    response = TercerosFactory.Obtener({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: Destinatario.Codigo,
                        //CadenaPerfiles: PERFIL_DESTINATARIO,
                        Sync: true
                    });
                    LimpiaCamposDestinatario();
                    ObtenerInformacionDestinatario(response);
                }
            }
        };

        function ObtenerInformacionDestinatario(response) {
            //--Obtiene Informacion del destinatario
            if (response != null) {
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.Codigo > 0) {
                        $scope.DestinatarioNoRegistrado = false;
                        $scope.Modelo.Destinatario = response.Datos;
                        $scope.Modelo.Destinatario.NumeroIdentificacion = response.Datos.NumeroIdentificacion;
                        $scope.Modelo.Destinatario.Nombre = response.Datos.NombreCompleto;
                        $scope.Modelo.Destinatario.Ciudad = '';
                        $scope.Modelo.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.Datos.TipoIdentificacion.Codigo);
                        //if ($scope.Modelo.Remesa.Destinatario.Direcciones.length > 0) {
                        //    $scope.ListadoDireccionesDestinatario = $scope.Modelo.Remesa.Destinatario.Direcciones;
                        //} else {
                        //    $scope.ListadoDireccionesDestinatario = [];
                        //}
                        //if ($scope.Modelo.Remesa.Destinatario.Direccion !== '' && $scope.Modelo.Remesa.Destinatario.Direccion !== undefined && $scope.Modelo.Remesa.Destinatario.Direccion !== null) {
                        //    $scope.ListadoDireccionesDestinatario.push({ Direccion: $scope.Modelo.Remesa.Destinatario.Direccion });
                        //    $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[0].Direccion;
                        //}
                        $scope.NombreDestinatario = response.Datos.NombreCompleto;
                        $scope.Modelo.Destinatario.Telefonos = $scope.Modelo.Destinatario.Telefonos.replace(';', ' - ');
                        $scope.Modelo.Destinatario.CorreoFacturacion = response.Datos.CorreoFacturacion;
                        $scope.Modelo.Barrio = response.Datos.Barrio;
                        $scope.Modelo.CodigoPostal = response.Datos.CodigoPostal;
                        $scope.ValidarTIIDDestinatario();
                        TMPDestinatario = angular.copy($scope.Modelo.Destinatario);
                        //$scope.BloqueoDestinatario = true;
                    }
                }
            }
        }

        function LimpiaCamposDestinatario() {
            $scope.Modelo.Destinatario.Codigo = '';
            //$scope.Modelo.Destinatario.Nombre = '';
            $scope.NombreDestinatario = '';
            $scope.Modelo.Destinatario.PrimeroApellido = "";
            $scope.Modelo.Destinatario.CodigoPostal = '';
            //$scope.ListadoDireccionesDestinatario = [];
            $scope.Modelo.Destinatario.Direccion = '';
            $scope.Modelo.Destinatario.Ciudad = '';
            $scope.Modelo.Destinatario.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0];
            $scope.Modelo.Destinatario.Telefonos = '';
            $scope.Modelo.Barrio = '';
            TMPDestinatario = "";
            $scope.BloqueoDestinatario = false;
            $scope.MostrarApellidoDestinatario = true;
        }
        //--Validacion Destinatario
        //-- Tarifas
        function DatosRequeridosTarifas() {
            $scope.MensajesError = [];
            var Continuar = true;

            if ($scope.Modelo.Remitente.Ciudad !== undefined && $scope.Modelo.Remitente.Ciudad !== null) {
                if (!($scope.Modelo.Remitente.Ciudad.Codigo > 0)) {
                    Continuar = false;
                }
            } else {
                Continuar = false;
            }

            if ($scope.Modelo.Destinatario.Ciudad !== undefined && $scope.Modelo.Destinatario.Ciudad !== null) {
                if (!($scope.Modelo.Destinatario.Ciudad.Codigo > 0)) {
                    Continuar = false;
                }
            } else {
                Continuar = false;
            }
            return Continuar;
        }
        $scope.ValidarTarifarioCliente = function () {
            if (DatosRequeridosTarifas() == true) {
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Tarifas...");
                $timeout(function () { blockUI.message("Cargando Tarifas..."); ObtenerTarifarioCliente(); }, 100);
            }
        };
        function ObtenerTarifarioCliente() {
            $scope.ListaAuxTipoTarifas = [];
            $scope.ListaTarifaCarga = [];
            $scope.ListaTipoTarifaCargaVenta = [];
            var TipoLineaNegocioAux = 0;

            if ($scope.Modelo.Remitente.Ciudad.Codigo == $scope.Modelo.Destinatario.Ciudad.Codigo) {
                TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA;
                CodigoTipoRemesa = CODIGO_TIPO_REMESA_URBANA;
            }
            else {
                TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL;
                CodigoTipoRemesa = CODIGO_TIPO_REMESA_NACIONAL;
            }
            $scope.CodigoCliente = 0;
            if ($scope.Modelo.Cliente !== undefined && $scope.Modelo.Cliente !== null && $scope.Modelo.Cliente !== '') {
                $scope.CodigoCliente = $scope.Modelo.Cliente.Codigo;
            }

            if ($scope.Modelo.Remitente.Ciudad != undefined && $scope.Modelo.Remitente.Ciudad != null && $scope.Modelo.Remitente.Ciudad != '' &&
                $scope.Modelo.Destinatario.Ciudad != undefined && $scope.Modelo.Destinatario.Ciudad != null && $scope.Modelo.Destinatario.Ciudad != '') {
                $scope.TarifaCliente = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.CodigoCliente,
                    CodigoCiudadOrigen: $scope.Modelo.Remitente.Ciudad.Codigo,
                    CodigoCiudadDestino: $scope.Modelo.Destinatario.Ciudad.Codigo,
                    CodigoLineaNegocio: CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA,
                    CodigoTipoLineaNegocio: TipoLineaNegocioAux,
                    TarifarioProveedores: false,
                    TarifarioClientes: true,
                    Sync: true
                };
                var ResponsTercero = TercerosFactory.Consultar($scope.TarifaCliente);
                if (ResponsTercero.ProcesoExitoso == true) {
                    if (ResponsTercero.Datos.length > 0) {
                        $scope.TarifarioInvalido = false;
                        for (var i = 0; i < ResponsTercero.Datos.length; i++) {
                            $scope.NumeroTarifario = ResponsTercero.Datos[i].NumeroTarifarioVenta;
                            var InsertaTarifa = true;
                            for (var j = 0; j < $scope.ListaTarifaCarga.length; j++) {
                                if ($scope.ListaTarifaCarga[j].Codigo == ResponsTercero.Datos[i].TarifaCarga.Codigo) {
                                    InsertaTarifa = false;
                                }
                            }
                            if (InsertaTarifa == true) {
                                $scope.ListaTarifaCarga.push(ResponsTercero.Datos[i].TarifaCarga);
                            }
                            var AuxTipoTarifa = {
                                CodigoDetalleTarifa: ResponsTercero.Datos[i].CodigoDetalleTarifa,
                                Codigo: ResponsTercero.Datos[i].TipoTarifaCarga.Codigo,
                                CodigoTarifa: ResponsTercero.Datos[i].TarifaCarga.Codigo,
                                Nombre: ResponsTercero.Datos[i].TipoTarifaCarga.Nombre,
                                ValorFlete: ResponsTercero.Datos[i].ValorFlete,
                                ValorManejo: ResponsTercero.Datos[i].ValorManejo,
                                ValorSeguro: ResponsTercero.Datos[i].ValorSeguro,
                                PorcentajeAfiliado: ResponsTercero.Datos[i].PorcentajeAfiliado,
                                PorcentajeSeguro: ResponsTercero.Datos[i].PorcentajeSeguro,
                                PesoMinimo: ResponsTercero.Datos[i].ValorCatalogoTipoTarifa.CampoAuxiliar2,
                                PesoMaximo: ResponsTercero.Datos[i].ValorCatalogoTipoTarifa.CampoAuxiliar3,
                                Reexpedicion: ResponsTercero.Datos[i].Reexpedicion,
                                PorcentajeReexpedicion: ResponsTercero.Datos[i].PorcentajeReexpedicion,
                                CondicionesComerciales: false,
                                FleteMinimo: 0,
                                ManejoMinimo: 0,
                                PorcentajeValorDeclarado: 0,
                                PorcentajeFlete: 0,
                                PorcentajeVolumen: 0
                            };
                            //--Valida Condiciones Comerciales con Tarifario
                            for (var j = 0; j < $scope.ListaCondicionesComerciales.length; j++) {
                                if ($scope.ListaCondicionesComerciales[j].LineaNegocio.Codigo == CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA &&
                                    $scope.ListaCondicionesComerciales[j].Tarifa.Codigo == ResponsTercero.Datos[i].TarifaCarga.Codigo &&
                                    $scope.ListaCondicionesComerciales[j].TipoTarifa.Codigo == ResponsTercero.Datos[i].TipoTarifaCarga.Codigo) {
                                    AuxTipoTarifa.FleteMinimo = $scope.ListaCondicionesComerciales[j].FleteMinimo;
                                    AuxTipoTarifa.ManejoMinimo = $scope.ListaCondicionesComerciales[j].ManejoMinimo;
                                    AuxTipoTarifa.PorcentajeValorDeclarado = $scope.ListaCondicionesComerciales[j].PorcentajeValorDeclarado;
                                    AuxTipoTarifa.PorcentajeFlete = $scope.ListaCondicionesComerciales[j].PorcentajeFlete;
                                    AuxTipoTarifa.PorcentajeVolumen = $scope.ListaCondicionesComerciales[j].PorcentajeVolumen;
                                    AuxTipoTarifa.CondicionesComerciales = true;
                                }
                            }
                            //--Valida Condiciones Comerciales con Tarifario
                            $scope.ListaAuxTipoTarifas.push(AuxTipoTarifa);

                            if (i == 0) {
                                $scope.Modelo.NumeroTarifarioVenta = ResponsTercero.Datos[i].NumeroTarifarioVenta;
                            }
                        }
                        if ($scope.CodigoTarifaCarga !== undefined && $scope.CodigoTarifaCarga !== null) {
                            if ($scope.CodigoTarifaCarga > 0) {
                                $scope.Modelo.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + $scope.CodigoTarifaCarga);
                            }
                            else {
                                try {
                                    $scope.Modelo.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + TARIFA_RANGO_PESO_VALOR_KILO);
                                } catch (e) {
                                    $scope.Modelo.TarifaCarga = $scope.ListaTarifaCarga[0];
                                }
                            }
                        }
                        else {
                            try {
                                $scope.Modelo.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + TARIFA_RANGO_PESO_VALOR_KILO);
                            } catch (e) {
                                $scope.Modelo.TarifaCarga = $scope.ListaTarifaCarga[0];
                            }
                        }
                        try {
                            $scope.ObtenerValoresPaqueteria();

                        } catch (e) {

                        }
                        $scope.CargarTipoTarifa($scope.Modelo.TarifaCarga.Codigo);
                    }
                    else {
                        $scope.TarifarioInvalido = true;
                    }
                }
                else {
                    $scope.TarifarioInvalido = true;
                }
            }
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
        }
        $scope.CargarTipoTarifa = function (CodigoTarifaCarga) {
            $scope.ListaTipoTarifaCargaVenta = [];
            $scope.ListaAuxTipoTarifas.forEach(function (item) {
                if (CodigoTarifaCarga == item.CodigoTarifa) {
                    $scope.ListaTipoTarifaCargaVenta.push(item);
                }
            });
            $scope.Modelo.TipoTarifaCarga = $scope.ListaTipoTarifaCargaVenta[0];
            if ($scope.CodigoTipoTarifaTransporte !== undefined && $scope.CodigoTipoTarifaTransporte !== null) {
                if ($scope.CodigoTipoTarifaTransporte > 0) {
                    $scope.Modelo.TipoTarifaCarga = $linq.Enumerable().From($scope.ListaTipoTarifaCargaVenta).First('$.Codigo ==' + $scope.CodigoTipoTarifaTransporte);
                }
            }
            $scope.Calcular();
        };
        $scope.ObtenerValoresPaqueteria = function () {
            $scope.TarifasPaqueteria = false;
            try {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.NumeroTarifario,
                    Sync: true
                };
                var response = TarifarioVentasFactory.ObtenerDetalleTarifarioPaqueteria(filtros);
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.Paqueteria !== undefined && response.Datos.Paqueteria !== '' && response.Datos.Paqueteria !== null) {
                        $scope.TarifasPaqueteria = true;
                        $scope.PaqueteriaMinimoValorSeguro = response.Datos.Paqueteria.MinimoValorSeguro;
                        $scope.PaqueteriaPorcentajeSeguro = response.Datos.Paqueteria.PorcentajeSeguro;
                        $scope.PaqueteriaMinimoValorManejo = response.Datos.Paqueteria.MinimoValorManejo;
                    }
                }
            } catch (e) {

            }
        };
        //---------------------Gestion Tarifas------------------------------//
        //--Valida Producto
        $scope.ValidarProducto = function () {
            if ($scope.Modelo.ProductoTransportado !== undefined) {
                if ($scope.Modelo.ProductoTransportado.Codigo > 0) {
                    $scope.Modelo.DescripcionProductoTransportado = '';
                } else {
                    $scope.Modelo.ProductoTransportado.inserta = true;
                    $scope.Modelo.DescripcionProductoTransportado = '';

                }
            }
        };
        //--Valida Producto
        //--Validar Peso
        $scope.ValidarPeso = function () {
            var peso = parseFloat(RevertirMV($scope.Modelo.PesoCobrar));
            if (peso > 0) {
                if ($scope.Modelo.LineaNegocioPaqueteria.Codigo == CATALOGO_LINEA_NEGOCIO_PAQUETERIA.COURIER) {
                    if (peso > 5) {
                        ShowError("El peso debe estar entre 1 y 5 kg cuando la línea de negocio es " + $scope.Modelo.LineaNegocioPaqueteria.Nombre);
                        if ($scope.PesoNormal) {
                            $scope.Modelo.PesoCliente = 0;
                        }
                        else {
                            $scope.Modelo.Largo = 0;
                            $scope.Modelo.Alto = 0;
                            $scope.Modelo.Ancho = 0;
                            $scope.PesoVolumetrico = 0;
                        }
                        $scope.Modelo.PesoCobrar = 0;
                    }
                }
                if ($scope.Modelo.LineaNegocioPaqueteria.Codigo == CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA) {
                    if (peso <= 5) {
                        ShowError("El peso debe ser mayor a 5 kg cuando la línea de negocio es " + $scope.Modelo.LineaNegocioPaqueteria.Nombre);
                        if ($scope.PesoNormal) {
                            $scope.Modelo.PesoCliente = 0;
                        }
                        else {
                            $scope.Modelo.Largo = 0;
                            $scope.Modelo.Alto = 0;
                            $scope.Modelo.Ancho = 0;
                            $scope.PesoVolumetrico = 0;
                        }
                        $scope.Modelo.PesoCobrar = 0;
                    }
                }
            }
        };
        //--Validar Peso
        //--Validar Peso Volumetrico
        $scope.ValidarPesoVolumetrico = function () {
            var tmpPesoVol = 0;
            if ($scope.ManejoPesoVolumetrico) {
                if ($scope.Modelo.Alto !== undefined && $scope.Modelo.Alto !== '' && $scope.Modelo.Alto !== '' &&
                    $scope.Modelo.Ancho !== undefined && $scope.Modelo.Ancho !== '' && $scope.Modelo.Ancho !== '' &&
                    $scope.Modelo.Largo !== undefined && $scope.Modelo.Largo !== '' && $scope.Modelo.Largo !== '') {
                    if ($scope.Modelo.Alto > 0 && $scope.Modelo.Ancho > 0 && $scope.Modelo.Largo > 0) {
                        var resVal
                        resVal = RecoleccionesFactory.ValidarPesoVolumetrico({
                            Alto: RevertirMV($scope.Modelo.Alto),
                            Ancho: RevertirMV($scope.Modelo.Ancho),
                            Largo: RevertirMV($scope.Modelo.Largo),
                            Sync: true
                        });
                        
                        if (resVal.ProcesoExitoso) {
                            tmpPesoVol = resVal.Datos.PesoVolumetrico;
                            if (tmpPesoVol != $scope.Modelo.PesoVolumetrico) {
                                $scope.Modelo.Alto = 0;
                                $scope.Modelo.Ancho = 0;
                                $scope.Modelo.Largo = 0;
                            }
                        }
                    }
                }
            }
        };
        //--Validar Peso Volumetrico
        //----Asignar Peso Producto
        $scope.AsignarPesoProducto = function () {
            if ($scope.Modelo.ProductoTransportado.Peso > 0) {
                $scope.Modelo.PesoCliente = $scope.Modelo.ProductoTransportado.Peso;
            }
        };
        //----Asignar Peso Producto
        //-----------Calcular----------//
        $scope.Calcular = function () {
            if ($scope.Modelo.CantidadCliente == undefined || $scope.Modelo.CantidadCliente == 0 || $scope.Modelo.CantidadCliente == "") {
                $scope.Modelo.CantidadCliente = 1;
            }
            tarifaConReexpediccion = false;
            var detalleTarifa = {};
            $scope.MensajesErrorTarifas = [];

            $scope.ValorReexpedicion = 0;
            $scope.Modelo.ValorFleteCliente = 0;
            $scope.Modelo.ValorSeguroCliente = 0;
            $scope.Modelo.ValorFleteTransportador = 0;
            $scope.Modelo.TotalFleteCliente = 0;
            var Calculo = false;
            $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.PesoCliente);
            if ($scope.ManejoPesoVolumetrico) {
                if ($scope.Modelo.Alto !== undefined && $scope.Modelo.Alto !== '' && $scope.Modelo.Alto !== '' &&
                    $scope.Modelo.Ancho !== undefined && $scope.Modelo.Ancho !== '' && $scope.Modelo.Ancho !== '' &&
                    $scope.Modelo.Largo !== undefined && $scope.Modelo.Largo !== '' && $scope.Modelo.Largo !== '') {
                    if ($scope.Modelo.Alto > 0 && $scope.Modelo.Ancho > 0 && $scope.Modelo.Largo > 0) {
                        var resVal
                        resVal = RecoleccionesFactory.ValidarPesoVolumetrico({
                            Alto: RevertirMV($scope.Modelo.Alto),
                            Ancho: RevertirMV($scope.Modelo.Ancho),
                            Largo: RevertirMV($scope.Modelo.Largo),
                            Sync: true
                        });

                        if (resVal.ProcesoExitoso) {
                            $scope.Modelo.PesoVolumetrico = resVal.Datos.PesoVolumetrico;
                        }
                    }
                }
                //--Compara Peso volumetrico con peso normal
                if (RevertirMV($scope.Modelo.PesoCliente) > 0 || $scope.Modelo.PesoVolumetrico > 0) {
                    if (RevertirMV($scope.Modelo.PesoCliente) > $scope.Modelo.PesoVolumetrico) {
                        $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.PesoCliente);
                        $scope.PesoNormal = true;
                    }
                    if ($scope.Modelo.PesoVolumetrico > RevertirMV($scope.Modelo.PesoCliente)) {
                        $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.PesoVolumetrico);
                        $scope.PesoNormal = false;
                    }
                } else {
                    $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.PesoCliente);
                }
            } else {
                $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.PesoCliente);
            }
            $scope.ValidarPeso();
            if ($scope.Modelo.TarifaCarga != null && $scope.Modelo.TarifaCarga != undefined &&
                $scope.Modelo.PesoCobrar > 0 && $scope.Modelo.ProductoTransportado != undefined && $scope.Modelo.ProductoTransportado != "" &&
                $scope.Modelo.ProductoTransportado != null) {
                var resVal
                resVal = RecoleccionesFactory.CalcularValores({
                    CodigoTarifaTransporte: $scope.Modelo.TarifaCarga.Codigo,
                    ValorComercial: $scope.Modelo.ValorComercialCliente,
                    LineaNegocioPaqueteria: { Codigo: $scope.Modelo.LineaNegocioPaqueteria ? $scope.Modelo.LineaNegocioPaqueteria.Codigo : 0 },
                    ProductoTransportado: {
                        Codigo: $scope.Modelo.ProductoTransportado ? $scope.Modelo.ProductoTransportado.Codigo : 0,
                        Peso: $scope.Modelo.ProductoTransportado ? $scope.Modelo.ProductoTransportado.Peso : 0
                    },
                    Cantidad: $scope.Modelo.CantidadCliente,
                    PesoCobrar: $scope.Modelo.PesoCobrar,
                    Peso: $scope.Modelo.PesoCliente,
                    ListaTarifaCargaVenta: $scope.ListaTipoTarifaCargaVenta.map(a => {
                        return {
                            CodigoTarifa: a.CodigoTarifa,
                            Codigo: a.Codigo,
                            ValorFlete: a.ValorFlete,
                            PesoMinimo: a.PesoMinimo,
                            PesoMaximo: a.PesoMaximo,
                            CondicionesComerciales: a.CondicionesComerciales,
                            PorcentajeFlete: a.PorcentajeFlete,
                            PorcentajeVolumen: a.PorcentajeVolumen,
                            Reexpedicion: a.Reexpedicion,
                            FleteMinimo: a.FleteMinimo,
                            PorcentajeReexpedicion: a.PorcentajeReexpedicion,
                            PorcentajeSeguro: a.PorcentajeSeguro,
                            PorcentajeValorDeclarado: a.PorcentajeValorDeclarado,
                            ManejoMinimo: a.ManejoMinimo
                        };
                    }),
                    parametrosCalculos: {
                        PesoNormal: $scope.PesoNormal,
                        OficinaObjPorcentajeSeguroPaqueteria: OficinaObj.PorcentajeSeguroPaqueteria ? OficinaObj.PorcentajeSeguroPaqueteria : 0,
                        PaqueteriaMinimoValorManejo: $scope.PaqueteriaMinimoValorManejo,
                        PaqueteriaMinimoValorSeguro: $scope.PaqueteriaMinimoValorSeguro,
                        calculo : false
                    },
                    Sync: true
                });

                if (resVal.ProcesoExitoso) {
                    ValorFleteCliente = resVal.Datos.ValorFleteCliente;
                    Calculo = resVal.Datos.parametrosCalculos.calculo
                    $scope.Modelo.PesoCliente = resVal.Datos.Peso
                    detalleTarifa = resVal.Datos.detalleTarifa
                    $scope.Modelo.ValorFleteCliente = resVal.Datos.ValorFleteCliente
                    $scope.Modelo.ValorSeguroCliente = resVal.Datos.ValorSeguroCliente
                    $scope.Modelo.TotalFleteCliente = resVal.Datos.TotalFleteCliente
                }
                if ($scope.Modelo.TarifaCarga.Codigo == 302) {
                    AplicaTarifaProducto = 1;
                }
                if ($scope.Modelo.PesoCobrar > 0) {
                    if ($scope.Modelo.TarifaCarga.Codigo == TARIFA_RANGO_PESO_VALOR_KILO || $scope.Modelo.TarifaCarga.Codigo == TARIFA_RANGO_PESO_VALOR_FIJO) {
                        AplicaTarifaProducto = -1;
                    }
                }
                if (detalleTarifa.Reexpedicion == CODIGO_UNO) {
                    tarifaConReexpediccion = true;
                }
                if (!Calculo) {
                    $scope.TarifarioInvalido = true;
                    switch ($scope.Modelo.TarifaCarga.Codigo) {
                        case 302: //Producto
                            $scope.MensajesErrorTarifas.push("Debe seleccionar un producto que coincida con la tarifa");
                            break;
                        case TARIFA_RANGO_PESO_VALOR_FIJO:
                            $scope.MensajesErrorTarifas.push("Debe ingresar un peso que coincida con las tarifas");
                            break;
                        case TARIFA_RANGO_PESO_VALOR_KILO:
                            $scope.MensajesErrorTarifas.push("Debe ingresar un peso que coincida con las tarifas");
                            break;
                    }
                }
                else if (Calculo){
                    $scope.TarifarioInvalido = false;
                }
            }
            $scope.MaskValores();
        };
        //--Calcular
        //--Obtener
        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
            };

            blockUI.delay = 1000;
            RecoleccionesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.NumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.Modelo.Fecha = new Date(response.data.Datos.Fecha);
                        //--Remitente
                        var CargaCliente = false;
                        if (response.data.Datos.Cliente.Codigo > 0) {
                            $scope.Modelo.Cliente = $scope.CargarTercero(response.data.Datos.Cliente.Codigo);
                            $scope.Modelo.Remitente.Codigo = $scope.Modelo.Cliente.Codigo;
                            $scope.BloqueoRemitenteNombre = true;
                            $scope.BloqueoRemitenteIdentificacion = true;
                            $scope.BloqueoDestinatario = true;
                            CargaCliente = true;
                        }
                        if (response.data.Datos.Remitente.Codigo > 0 && CargaCliente == false) {
                            $scope.Modelo.Remitente = $scope.CargarTercero(response.data.Datos.Remitente.Codigo);
                        }

                        //--Obtiente tercero e informacion
                        var resDocu;
                        var remitente = response.data.Datos.Cliente.Codigo > 0 ? response.data.Datos.Cliente.Codigo : response.data.Datos.Remitente.Codigo;
                        resDocu = TercerosFactory.Obtener({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: remitente,
                            Sync: true
                        });
                        if (resDocu.ProcesoExitoso) {
                            FormasPagoCliente = resDocu.Datos.FormasPago;
                        }
                        //--Obtiente tercero e informacion

                        if (response.data.Datos.Remitente.TipoIdentificacion.Codigo > 0 && response.data.Datos.Remitente.TipoIdentificacion.Codigo != 100) {
                            $scope.Modelo.Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.Remitente.TipoIdentificacion.Codigo);
                        }
                        if (response.data.Datos.Remitente.Codigo > 0) {
                            $scope.Modelo.Remitente.NumeroIdentificacion = response.data.Datos.Remitente.NumeroIdentificacion;
                            $scope.NombreRemitente = response.data.Datos.Remitente.Nombre;
                        }
                        $scope.Modelo.Remitente.Ciudad = $scope.CargarCiudad(response.data.Datos.Remitente.Ciudad.Codigo);
                        $scope.Modelo.Remitente.Telefonos = response.data.Datos.Remitente.Telefonos;
                        $scope.Modelo.Remitente.Direccion = response.data.Datos.Remitente.Direccion;
                        $scope.Modelo.Remitente.Correo = response.data.Datos.Remitente.CorreoFacturacion;
                        $scope.ValidarTIIDRemitente();
                        //--Remitente
                        //--Destinatario
                        if (response.data.Datos.Destinatario.Codigo > 0) {
                            $scope.Modelo.Destinatario = $scope.CargarTercero(response.data.Datos.Destinatario.Codigo);
                            $scope.Modelo.Destinatario.NumeroIdentificacion = response.data.Datos.Destinatario.NumeroIdentificacion;
                            $scope.NombreDestinatario = response.data.Datos.Destinatario.Nombre;
                        }
                        if (response.data.Datos.Destinatario.TipoIdentificacion.Codigo > 0 && response.data.Datos.Destinatario.TipoIdentificacion.Codigo != 100) {
                            $scope.Modelo.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.Destinatario.TipoIdentificacion.Codigo);
                        }
                        $scope.Modelo.Destinatario.Ciudad = $scope.CargarCiudad(response.data.Datos.Destinatario.Ciudad.Codigo);
                        $scope.Modelo.Destinatario.Telefonos = response.data.Datos.Destinatario.Telefonos;
                        $scope.Modelo.Destinatario.Barrio = response.data.Datos.Destinatario.Barrio;
                        $scope.Modelo.Destinatario.CodigoPostal = response.data.Datos.Destinatario.CodigoPostal;
                        $scope.Modelo.Destinatario.Direccion = response.data.Datos.Destinatario.Direccion;
                        //$scope.Modelo.FechaEntrega = new Date(response.data.Datos.FechaEntrega);
                        $scope.ValidarTIIDDestinatario();
                        if ($scope.ListadoHorariosEntrega.length > 0 && response.data.Datos.HorarioEntrega.Codigo > 0) {
                            $scope.Modelo.HorarioEntrega = $linq.Enumerable().From($scope.ListadoHorariosEntrega).First('$.Codigo ==' + response.data.Datos.HorarioEntrega.Codigo);
                        }
                        //--Destinatario
                        //--Informacion Recoleccion
                        if (new Date(response.data.Datos.FechaRecoleccion) > MIN_DATE) {
                            $scope.Modelo.FechaRecoleccion = new Date(response.data.Datos.FechaRecoleccion);
                        }
                        if (new Date(response.data.Datos.FechaEntrega) > MIN_DATE) {
                            $scope.Modelo.FechaEntrega = new Date(response.data.Datos.FechaEntrega);
                        }

                        if ($scope.ListadoZonas.length > 0 && response.data.Datos.Zonas.Codigo > 0) {
                            $scope.Modelo.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo == ' + response.data.Datos.Zonas.Codigo);
                        }
                        $scope.Modelo.Contacto = response.data.Datos.NombreContacto;
                        $scope.Modelo.Barrio = response.data.Datos.Barrio;
                        $scope.Modelo.Direccion = response.data.Datos.Direccion;
                        $scope.Modelo.Telefonos = response.data.Datos.Telefonos;
                        $scope.Modelo.Latitud = response.data.Datos.Latitud;
                        $scope.Modelo.Longitud = response.data.Datos.Longitud;
                        $scope.Modelo.OficinaGestiona = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo==' + response.data.Datos.OficinaGestiona.Codigo);
                        $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.Ciudad.Codigo);
                        //--Informacion Recoleccion
                        //--Tarifas
                        $scope.Modelo.Mercancia = response.data.Datos.Mercancia;
                        $scope.Modelo.CantidadCliente = response.data.Datos.Cantidad;
                        $scope.Modelo.PesoCliente = response.data.Datos.Peso;
                        $scope.Modelo.Largo = response.data.Datos.Largo;
                        $scope.Modelo.Alto = response.data.Datos.Alto;
                        $scope.Modelo.Ancho = response.data.Datos.Ancho;
                        $scope.Modelo.PesoVolumetrico = response.data.Datos.PesoVolumetrico;
                        $scope.Modelo.PesoCobrar = response.data.Datos.PesoCobrar;
                        $scope.Modelo.ValorComercialCliente = response.data.Datos.ValorComercial;

                        $scope.Modelo.ValorFleteCliente = response.data.Datos.ValorFleteCliente;
                        $scope.Modelo.ValorManejoCliente = response.data.Datos.ValorManejoCliente;
                        $scope.Modelo.ValorSeguroCliente = response.data.Datos.ValorSeguroCliente;
                        $scope.Modelo.TotalFleteCliente = response.data.Datos.TotalFleteCliente;
                        $scope.Modelo.Observaciones = response.data.Datos.Observaciones;
                        if (response.data.Datos.CodigoUnidadEmpaque > 0) {
                            $scope.Modelo.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo == ' + response.data.Datos.CodigoUnidadEmpaque);
                        }
                        $scope.Modelo.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + response.data.Datos.FormaPago.Codigo);
                        $scope.Modelo.ProductoTransportado = $scope.CargarProducto(response.data.Datos.ProductoTransportado.Codigo);
                        $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + response.data.Datos.Oficinas.Codigo);

                        $scope.CodigoTarifaCarga = response.data.Datos.CodigoTarifaTransporte;
                        $scope.CodigoTipoTarifaTransporte = response.data.Datos.CodigoTipoTarifaTransporte;
                        var CodigoTarifaCarga = response.data.Datos.CodigoTarifaTransporte;
                        var CodigoTipoTarifaTransporte = response.data.Datos.CodigoTipoTarifaTransporte;
                        //--Arma Tarifa consultada
                        if ($scope.ListaTarifaCarga.length == 0) {
                            $scope.ListaTarifaCarga = [
                                { Codigo: response.data.Datos.CodigoTarifaTransporte, Nombre: response.data.Datos.NombreTarifa }
                            ];
                        }
                        $scope.Modelo.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + CodigoTarifaCarga);
                        //--Arma Tipo de tarifa consultada
                        if ($scope.ListaTipoTarifaCargaVenta.length == 0) {
                            $scope.ListaTipoTarifaCargaVenta = [
                                { CodigoDetalleTarifa: response.data.Datos.DetalleTarifaVenta.Codigo, Codigo: CodigoTipoTarifaTransporte, CodigoTarifa: CodigoTarifaCarga, Nombre: response.data.Datos.NombreTipoTarifa }
                            ];
                        }
                        $scope.Modelo.TipoTarifaCarga = $linq.Enumerable().From($scope.ListaTipoTarifaCargaVenta).First('$.Codigo ==' + CodigoTipoTarifaTransporte);
                        //--Tarifas

                        //--Condiciones Comerciales
                        if (resDocu.ProcesoExitoso) {
                            if (resDocu.Datos.Codigo > 0) {
                                if (resDocu.Datos.Cliente != undefined && resDocu.Datos.Cliente != '' && resDocu.Datos.Cliente != null) {
                                    if (resDocu.Datos.Cliente.CondicionesComercialesTarifas == ESTADO_ACTIVO) {
                                        $scope.ListaCondicionesComerciales = resDocu.Datos.Cliente.CondicionesComerciales;
                                    }
                                }
                            }
                        }
                        if (response.data.Datos.Estado == ESTADO_BORRADOR) {
                            ObtenerTarifarioCliente();
                        }
                        //--Condiciones Comerciales

                        if (response.data.Datos.Anulado == ESTADO_ANULADO) {
                            $scope.ListadoEstados.push({ Nombre: 'ANULADO', Codigo: 2 });
                            $scope.Modelo.Estado = $scope.ListadoEstados[2];
                            $scope.Deshabilitar = true;
                        }
                        else {
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.Deshabilitar = true;
                            }
                            else {
                                ObtenerTarifarioCliente();
                            }
                        }

                        $scope.CodigoTarifaCarga = undefined;
                        $scope.CodigoTipoTarifaTransporte = undefined;
                        $scope.MaskValores();
                    }
                    else {
                        ShowError('No se logro consultar la Recoleccion número ' + $scope.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarRecoleccionesPaqueteria';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarRecoleccionesPaqueteria';
                });

            blockUI.stop();
        }
        //--Obtener
        //--Guardar
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            //--Remitente Nombre
            $scope.Modelo.Remitente.Nombre = $scope.NombreRemitente;
            //--Remitente Nombre
            //--Destinatario Nombre
            $scope.Modelo.Destinatario.Nombre = $scope.NombreDestinatario;
            //--Destinatario Nombre
            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                NumeroDocumento: $scope.Modelo.NumeroDocumento,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_RECOLECCION,
                Fecha: $scope.Modelo.Fecha,
                FechaEntrega: $scope.Modelo.FechaEntrega,
                Cliente: $scope.Modelo.Cliente,
                Remitente: $scope.Modelo.Remitente,
                Destinatario: $scope.Modelo.Destinatario,
                NombreContacto: $scope.Modelo.Contacto,
                Oficinas: $scope.Modelo.Oficina,
                Zonas: $scope.Modelo.Zona,
                Ciudad: $scope.Modelo.Ciudad,
                Barrio: $scope.Modelo.Barrio,
                Direccion: $scope.Modelo.Direccion,
                Latitud: $scope.Modelo.Latitud,
                Longitud: $scope.Modelo.Longitud,
                Telefonos: $scope.Modelo.Telefonos,
                Mercancia: $scope.Modelo.Mercancia,
                CodigoUnidadEmpaque: $scope.Modelo.UnidadEmpaque == undefined ? 0 : $scope.Modelo.UnidadEmpaque.Codigo,
                Cantidad: $scope.Modelo.CantidadCliente,
                Peso: $scope.Modelo.PesoCliente,
                Largo: $scope.Modelo.Largo,
                Alto: $scope.Modelo.Alto,
                Ancho: $scope.Modelo.Ancho,
                PesoVolumetrico: $scope.Modelo.PesoVolumetrico,
                PesoCobrar: $scope.Modelo.PesoCobrar,
                HorarioEntrega: $scope.Modelo.HorarioEntrega,
                ValorComercial: $scope.Modelo.ValorComercialCliente,
                ValorFleteCliente: $scope.Modelo.ValorFleteCliente,
                ValorManejoCliente: $scope.Modelo.ValorManejoCliente,
                ValorSeguroCliente: $scope.Modelo.ValorSeguroCliente,
                TotalFleteCliente: $scope.Modelo.TotalFleteCliente,
                Observaciones: $scope.Modelo.Observaciones,
                FormaPago: $scope.Modelo.FormaPago,
                DetalleTarifaVenta: { Codigo: $scope.Modelo.TipoTarifaCarga.CodigoDetalleTarifa },
                ProductoTransportado: $scope.Modelo.ProductoTransportado,
                Estado: $scope.Modelo.Estado.Codigo,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                FechaRecoleccion: $scope.Modelo.FechaRecoleccion,
                OficinaGestiona: { Codigo: $scope.Modelo.OficinaGestiona.Codigo },
                LineaNegocioPaqueteria: $scope.Modelo.LineaNegocioPaqueteria,

                CodigoTarifaTransporte: $scope.Modelo.TarifaCarga.Codigo,
                ListaTarifaCargaVenta: $scope.ListaTipoTarifaCargaVenta.map(a => {
                    return {
                        CodigoTarifa: a.CodigoTarifa,
                        Codigo: a.Codigo,
                        ValorFlete: a.ValorFlete,
                        PesoMinimo: a.PesoMinimo,
                        PesoMaximo: a.PesoMaximo,
                        CondicionesComerciales: a.CondicionesComerciales,
                        PorcentajeFlete: a.PorcentajeFlete,
                        PorcentajeVolumen: a.PorcentajeVolumen,
                        Reexpedicion: a.Reexpedicion,
                        FleteMinimo: a.FleteMinimo,
                        PorcentajeReexpedicion: a.PorcentajeReexpedicion,
                        PorcentajeSeguro: a.PorcentajeSeguro,
                        PorcentajeValorDeclarado: a.PorcentajeValorDeclarado,
                        ManejoMinimo: a.ManejoMinimo
                    };
                }),
                parametrosCalculos: {
                    PesoNormal: $scope.PesoNormal,
                    OficinaObjPorcentajeSeguroPaqueteria: OficinaObj.PorcentajeSeguroPaqueteria ? OficinaObj.PorcentajeSeguroPaqueteria : 0,
                    PaqueteriaMinimoValorManejo: $scope.PaqueteriaMinimoValorManejo,
                    PaqueteriaMinimoValorSeguro: $scope.PaqueteriaMinimoValorSeguro,
                    calculo: false
                },
            };

            RecoleccionesFactory.Guardar($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Numero === 0) {
                                ShowSuccess('Se guardó la recolección ' + response.data.Datos);
                            }
                            else {
                                ShowSuccess('Se modificó la recolección ' + response.data.Datos);
                            }
                            location.href = '#!ConsultarRecoleccionesPaqueteria/' + response.data.Datos;
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo;
            var dateEntMin = new Date();
            dateEntMin.setDate(dateEntMin.getDate() - 1);

            if (Modelo.FormaPago == undefined || Modelo.FormaPago == null || Modelo.FormaPago == '') {
                $scope.MensajesError.push('Debe ingresar la forma de pago');
                continuar = false;
            }
            else if (Modelo.FormaPago.Codigo == 6100 || Modelo.FormaPago.Codigo == -1) {
                $scope.MensajesError.push('Debe ingresar la forma de pago');
                continuar = false;
            }
            else {
                //--Forma Pago
                if (Modelo.Cliente != undefined && Modelo.Cliente != null && Modelo.Cliente != "") {
                    var existe = false;
                    if (FormasPagoCliente != undefined && FormasPagoCliente != null && FormasPagoCliente != "") {
                        for (var i = 0; i < FormasPagoCliente.length; i++) {
                            if (FormasPagoCliente[i].Codigo == Modelo.FormaPago.Codigo) {
                                existe = true;
                                break;
                            }
                        }
                        if (existe == false) {
                            $scope.MensajesError.push('La forma de pago no esta asignada al cliente');
                            continuar = false;
                        }
                    }
                }
                else {
                    if (Modelo.Remitente !== undefined || Modelo.Remitente !== null || Modelo.Remitente !== "") {
                        if (Modelo.Remitente.Codigo == 0) {
                            if (Modelo.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                                $scope.MensajesError.push('No se puede asignar forma de pago crédito para un remitente nuevo');
                                continuar = false;
                            }
                        }
                        else {
                            var existe = false;
                            if (FormasPagoCliente != undefined && FormasPagoCliente != null && FormasPagoCliente != "") {
                                for (var i = 0; i < FormasPagoCliente.length; i++) {
                                    if (FormasPagoCliente[i].Codigo == Modelo.FormaPago.Codigo) {
                                        existe = true;
                                        break;
                                    }
                                }
                                if (existe == false) {
                                    $scope.MensajesError.push('La forma de pago no esta asignada al remitente');
                                    continuar = false;
                                }
                            }
                            else {
                                if (Modelo.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                                    $scope.MensajesError.push('No se puede asignar forma de pago crédito para un remitente');
                                    continuar = false;
                                }
                            }
                        }
                    }
                }

                //--Forma Pago
            }
            if (Modelo.Fecha === undefined || Modelo.Fecha === '' || Modelo.Fecha === null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }

            //--Validacion Remitente
            if ((Modelo.Remitente.NumeroIdentificacion != undefined && Modelo.Remitente.NumeroIdentificacion != "") ||
                ($scope.NombreRemitente != undefined && $scope.NombreRemitente != "")) {
                if (ValidarCampo(Modelo.Remitente.NumeroIdentificacion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el número de identificación del remitente');
                    continuar = false;
                }
                if (Modelo.Remitente.TipoIdentificacion.Codigo == 100) {
                    $scope.MensajesError.push('Debe seleccionar el tipo de identificación del remitente');
                    continuar = false;
                }
                if (ValidarCampo($scope.NombreRemitente) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el nombre del remitente');
                    continuar = false;
                }
                if (ValidarCampo(Modelo.Remitente.Direccion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe seleccionar la dirección del remitente');
                    continuar = false;
                }
                if (ValidarCampo(Modelo.Remitente.Telefonos) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el teléfono del remitente');
                    continuar = false;
                }
            }

            if (ValidarCampo(Modelo.Remitente.Ciudad) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe seleccionar la ciudad del remitente');
                continuar = false;
            }
            //--Validacion Remitente
            //--Validacion Destinatario
            if ((Modelo.Destinatario.NumeroIdentificacion != undefined && Modelo.Destinatario.NumeroIdentificacion != "") ||
                ($scope.NombreDestinatario != undefined && $scope.NombreDestinatario != "")) {
                if (ValidarCampo(Modelo.Destinatario.NumeroIdentificacion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el número de identificación del destinatario');
                    continuar = false;
                }
                if (Modelo.Remitente.TipoIdentificacion.Codigo == 100) {
                    $scope.MensajesError.push('Debe seleccionar el tipo de identificación del destinatario');
                    continuar = false;
                }
                if (ValidarCampo($scope.NombreDestinatario) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el nombre del destinatario');
                    continuar = false;
                }
            }
            if (ValidarCampo(Modelo.Destinatario.Ciudad) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe seleccionar la ciudad del destinatario');
                continuar = false;
            }
            if (Modelo.FechaEntrega !== undefined || Modelo.FechaEntrega !== '' || Modelo.FechaEntrega !== null) {
                if (Modelo.FechaEntrega < dateEntMin) {
                    $scope.MensajesError.push('la fecha entrega debe ser posterior a la fecha actual');
                    continuar = false;
                }
            }
            //--Validacion Destinatario

            if (Modelo.FechaRecoleccion === undefined || Modelo.FechaRecoleccion === '' || Modelo.FechaRecoleccion === null) {
                $scope.MensajesError.push('Debe ingresar la fecha de recolección');
                continuar = false;
            } else {
                if (Modelo.FechaRecoleccion < new Date()) {
                    $scope.MensajesError.push('La fecha de recoleccion no puede ser menor a la actual');
                    continuar = false;
                }
            }

            if (Modelo.Ciudad === undefined || Modelo.Ciudad === 0 || Modelo.Ciudad === null) {
                $scope.MensajesError.push('Debe ingresar la ciudad recolección');
                continuar = false;
            }
            if (Modelo.Direccion === undefined || Modelo.Direccion === '' || Modelo.Direccion === null) {
                $scope.MensajesError.push('Debe ingresar la dirección recolección');
                continuar = false;
            }
            if (Modelo.Telefonos === undefined || Modelo.Telefonos === '' || Modelo.Telefonos === null) {
                $scope.MensajesError.push('Debe ingresar los teléfonos recolección');
                continuar = false;
            }

            //if (Modelo.Mercancia === undefined || Modelo.Mercancia === '' || Modelo.Mercancia === null) {
            //    $scope.MensajesError.push('Debe ingresar la mercancía');
            //    continuar = false;
            //}

            //if (Modelo.UnidadEmpaque === undefined || Modelo.UnidadEmpaque === '' || Modelo.UnidadEmpaque === null) {
            //    $scope.MensajesError.push('Debe seleccionar la unidad de empaque');
            //    continuar = false;
            //}

            if (tarifaConReexpediccion == true && Modelo.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA) {
                $scope.MensajesError.push("La forma de pago no puede ser contra entrega cuando la tarifa tiene reexpedición");
                continuar = false;
            }
            //if (Modelo.CantidadCliente === undefined || Modelo.CantidadCliente === '' || Modelo.CantidadCliente === null) {
            //    $scope.MensajesError.push('Debe ingresar la cantidad');
            //    continuar = false;
            //}

            //if (Modelo.ProductoTransportado === undefined || Modelo.ProductoTransportado === '' || Modelo.ProductoTransportado === null) {
            //    $scope.MensajesError.push('Debe ingresar el producto');
            //    continuar = false;
            //}

            //if (Modelo.ValorFleteCliente == '' || Modelo.ValorFleteCliente == undefined || Modelo.ValorFleteCliente <= 0) {
            //    $scope.MensajesError.push('No hay un flete asignado con las condiciones especificadas');
            //    continuar = false;
            //}

            return continuar;
        }
        //--Guardar
        $scope.VolverMaster = function () {
            if ($scope.ModeloNumeroDocumento != undefined)
                document.location.href = '#!ConsultarRecoleccionesPaqueteria/' + $scope.ModeloNumeroDocumento;
            else
                document.location.href = '#!ConsultarRecoleccionesPaqueteria';
        };
        //-----------------------Mapa Google------------------------------//
        $scope.AbrirMapa = function (lat, lng) {
            showModal('modalMostrarMapa');
            iniciarMapaGoogle(lat, lng);
        };
        function iniciarMapaGoogle(lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.MapaLatitud = lat != '' ? lat : 0;
            $scope.MapaLongitud = lng != '' ? lng : 0;
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    };
                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };
                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.MapaLatitud = this.getPosition().lat();
                    $scope.MapaLongitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.MapaLatitud != undefined && $scope.MapaLongitud != undefined && $scope.MapaLatitud != 0 && $scope.MapaLongitud != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.MapaLatitud, $scope.MapaLongitud), 'Posición del usuario', 'Destino');
                } else {
                    $scope.MapaLatitud = posicion.coords.latitude;
                    $scope.MapaLongitud = posicion.coords.longitude;
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }
        $scope.AsignarPosicion = function () {
            $scope.Modelo.Latitud = $scope.MapaLatitud;
            $scope.Modelo.Longitud = $scope.MapaLongitud;
            closeModal('modalMostrarMapa');
        };
        //-----------------------Mapa Google------------------------------//

        //--Mascaras
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresInt = function (Value) {
            return MascaraValoresInt(Value)
        };
        $scope.MaskintValores = function (option) {
            MascaraValoresIntGeneral($scope)
        };
        $scope.MaskMayus = function (option) {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskDireccion = function (option) {
            try { $scope.ModeloDireccion = MascaraDireccion($scope.ModeloDireccion) } catch (e) { }
        };
        $scope.MaskTelefono = function (option) {
            try { $scope.ModeloTelefonos = MascaraTelefono($scope.ModeloTelefonos) } catch (e) { }
        };
        //--Mascaras
    }]);