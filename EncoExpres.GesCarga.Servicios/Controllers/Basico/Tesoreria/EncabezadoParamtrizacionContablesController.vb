﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Negocio.Basico.Tesoreria

''' <summary>
''' Controlador <see cref="EncabezadoParametrizacionContablesController"/>
''' </summary>
<Authorize>
Public Class EncabezadoParametrizacionContablesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoParametrizacionContables) As Respuesta(Of IEnumerable(Of EncabezadoParametrizacionContables))
        Return New LogicaEncabezadoParametrizacionContables(CapaPersistenciaEncabezadoParametrizacionContables, CapaPersistenciaDetalleParametrizacionContables).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoParametrizacionContables) As Respuesta(Of EncabezadoParametrizacionContables)
        Return New LogicaEncabezadoParametrizacionContables(CapaPersistenciaEncabezadoParametrizacionContables, CapaPersistenciaDetalleParametrizacionContables).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoParametrizacionContables) As Respuesta(Of Long)
        Return New LogicaEncabezadoParametrizacionContables(CapaPersistenciaEncabezadoParametrizacionContables, CapaPersistenciaDetalleParametrizacionContables).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EncabezadoParametrizacionContables) As Respuesta(Of Boolean)
        Return New LogicaEncabezadoParametrizacionContables(CapaPersistenciaEncabezadoParametrizacionContables, CapaPersistenciaDetalleParametrizacionContables).Anular(entidad)
    End Function
End Class
