﻿PRINT 'Opción Menú Consulta Procesos PSL'
GO
DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 1000203		
GO
DELETE Menu_Aplicaciones WHERE Codigo = 1000203
GO
INSERT INTO Menu_Aplicaciones( 
EMPR_Codigo,
Codigo,
MOAP_Codigo,
Nombre,
Padre_Menu,
Orden_Menu,
Mostrar_Menu,
Aplica_Habilitar,
Aplica_Consultar,
Aplica_Actualizar,
Aplica_Eliminar_Anular,
Aplica_Imprimir,
Aplica_Contabilidad,
Opcion_Permiso,
Opcion_Listado,
Aplica_Ayuda,
Url_Pagina,
Url_Ayuda) 
VALUES(
4
,1000203
,100
,'Consulta Procesos PSL'
,10002
,30
,1
,1
,1
,1
,1
,1
,1
,0
,0
,0
,'#!ConsultarInterfazContablePsl'
,'#!'
)
GO
INSERT INTO Permiso_Grupo_Usuarios(
EMPR_Codigo,
MEAP_Codigo,
GRUS_Codigo,
Habilitar,
Consultar,
Actualizar,
Eliminar_Anular,
Imprimir,
Permiso,
Contabilidad
) 
VALUES (
4,
1000203,
3,
1,
1,
1,
1,
1,
0,
0
) 