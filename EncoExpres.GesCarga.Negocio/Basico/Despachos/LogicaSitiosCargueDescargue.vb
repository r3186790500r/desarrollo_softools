﻿Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Fachada.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Basico.Despachos
    Public NotInheritable Class LogicaSitiosCargueDescargue
        Inherits LogicaBase(Of SitiosCargueDescargue)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of SitiosCargueDescargue)

        Sub New(persistencia As IPersistenciaBase(Of SitiosCargueDescargue))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As SitiosCargueDescargue) As Respuesta(Of IEnumerable(Of SitiosCargueDescargue))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of SitiosCargueDescargue))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of SitiosCargueDescargue))(consulta)
        End Function

        Public Function ConsultarSitiosRutas(filtro As Rutas) As Respuesta(Of IEnumerable(Of SitiosCargueDescargueRutas))
            Dim consulta = CType(_persistencia, PersistenciaSitiosCargueDescargue).ConsultarSitiosRutas(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of SitiosCargueDescargueRutas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of SitiosCargueDescargueRutas))(consulta)
        End Function


        Public Overrides Function Guardar(entidad As SitiosCargueDescargue) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function GuardarSitiosRuta(entidad As IEnumerable(Of SitiosCargueDescargueRutas)) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaSitiosCargueDescargue).GuardarSitiosRuta(entidad)


            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, valor, Recursos.OperacionInserto)}

        End Function

        Public Overrides Function Obtener(filtro As SitiosCargueDescargue) As Respuesta(Of SitiosCargueDescargue)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of SitiosCargueDescargue)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of SitiosCargueDescargue)(consulta)
        End Function

        Private Function DatosRequeridos(entidad As SitiosCargueDescargue) As Respuesta(Of SitiosCargueDescargue)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of SitiosCargueDescargue) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of SitiosCargueDescargue) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As SitiosCargueDescargue) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaSitiosCargueDescargue).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function GenerarPlanitilla(entidad As SitiosCargueDescargue) As Respuesta(Of SitiosCargueDescargue)
            Dim mensajeGuardo = "Genero Archivo"
            Dim url As New SitiosCargueDescargue
            url = CType(_persistencia, PersistenciaSitiosCargueDescargue).GenerarPlanitilla(entidad)
            Return New Respuesta(Of SitiosCargueDescargue)(url)
        End Function
    End Class

End Namespace