﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Public Class OficinaSIESA

    <JsonProperty>
    Public Property CodigoSiesa As String
    <JsonProperty>
    Public Property Nombre As String
    <JsonProperty>
    Public Property Regional As String
    <JsonProperty>
    Public Property Contacto As String
    <JsonProperty>
    Public Property Ciudad As String
    <JsonProperty>
    Public Property Telefono As String
    <JsonProperty>
    Public Property Direccion As String
    <JsonProperty>
    Public Property Email As String

End Class
