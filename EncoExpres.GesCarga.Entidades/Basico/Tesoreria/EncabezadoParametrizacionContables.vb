﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="EncabezadoParametrizacionContables"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoParametrizacionContables
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="EncabezadoParametrizacionContables"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoParametrizacionContables"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            TipoDocumentoGenera = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIDG_Codigo"), .Nombre = Read(lector, "TipoDocumentoGenera")}
            TipoGeneraDetalle = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TGDC_Codigo"), .Nombre = Read(lector, "TipoGeneraDetalleContable")}
            Observaciones = Read(lector, "Observaciones")
            Fuente = Read(lector, "Fuente")
            FuenteAnula = Read(lector, "Fuente_Anulacion")
            Estado = Read(lector, "Estado")
            Provision = Read(lector, "Provision")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            Else

            End If
        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property TipoDocumentoGenera As ValorCatalogos
        <JsonProperty>
        Public Property TipoGeneraDetalle As ValorCatalogos
        <JsonProperty>
        Public Property Fuente As String
        <JsonProperty>
        Public Property FuenteAnula As String
        <JsonProperty>
        Public Property Provision As Integer

        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleParametrizacionContables)
    End Class

End Namespace
