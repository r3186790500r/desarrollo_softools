﻿Print 'V_Tipo_Reporte_Seguimiento'
GO
DROP VIEW V_Tipo_Reporte_Seguimiento
GO
CREATE VIEW V_Tipo_Reporte_Seguimiento 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 83
GO