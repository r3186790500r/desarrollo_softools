﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaConceptoLiquidacion
        Implements IPersistenciaBase(Of ConceptoLiquidacionPlanillaDespacho)

        Public Function Consultar(filtro As ConceptoLiquidacionPlanillaDespacho) As IEnumerable(Of ConceptoLiquidacionPlanillaDespacho) Implements IPersistenciaBase(Of ConceptoLiquidacionPlanillaDespacho).Consultar
            Return New RepositorioConceptoLiquidacion().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ConceptoLiquidacionPlanillaDespacho) As Long Implements IPersistenciaBase(Of ConceptoLiquidacionPlanillaDespacho).Insertar
            Return New RepositorioConceptoLiquidacion().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ConceptoLiquidacionPlanillaDespacho) As Long Implements IPersistenciaBase(Of ConceptoLiquidacionPlanillaDespacho).Modificar
            Return New RepositorioConceptoLiquidacion().Modificar(entidad)
        End Function

        Public Function GuardarImpuestos(entidad As ConceptoLiquidacionPlanillaDespacho) As Long
            Return New RepositorioConceptoLiquidacion().GuardarImpuestos(entidad)
        End Function

        Public Function Obtener(filtro As ConceptoLiquidacionPlanillaDespacho) As ConceptoLiquidacionPlanillaDespacho Implements IPersistenciaBase(Of ConceptoLiquidacionPlanillaDespacho).Obtener
            Return New RepositorioConceptoLiquidacion().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ConceptoLiquidacionPlanillaDespacho) As Boolean
            Return New RepositorioConceptoLiquidacion().Anular(entidad)
        End Function

    End Class

End Namespace

