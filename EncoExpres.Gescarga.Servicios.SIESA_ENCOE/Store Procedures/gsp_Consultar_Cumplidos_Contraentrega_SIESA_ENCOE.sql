DROP PROCEDURE [dbo].[gsp_Consultar_Cumplidos_Contraentrega_SIESA_ENCOE]  
GO

CREATE PROCEDURE [dbo].[gsp_Consultar_Cumplidos_Contraentrega_SIESA_ENCOE]  
(    
 @par_EMPR_Codigo smallint  
)    
AS    
BEGIN    
	SELECT TOP 10    
    
	ENRE.Numero,   
	ENRE.Numero_Documento AS Numero_Guia,  
	ENRE.Numeracion AS Numero_Preimpreso,  
	ENRE.Fecha AS Fecha_Guia,  
	ENRE.OFIC_Codigo AS Oficina_Crea,  
	ENRE.Total_Flete_Cliente AS Valor_Total_Guia,  
	ENRE.TERC_Codigo_Cliente AS Codigo_Cliente,   
	CLIE.Numero_Identificacion AS Identificacion_Cliente,  
	ENRE.TERC_Codigo_Destinatario AS Codigo_Destinatario,  
	DEST.Numero_Identificacion AS Identificacion_Destinatario,  
	REPA.OFIC_Codigo_Origen AS Oficina_Origen,  
	ENPA.Numero_Documento AS Numero_Ultima_Planilla,  
	CURE.Fecha_Recibe AS Fecha_Entrega,  
	CURE.USUA_Codigo_Crea AS Usuario_Entrega,  
	TEEN.Numero_Identificacion AS Identificacion_Empleado_Entrega,  
	TECO.Numero_Identificacion AS Identificacion_Conductor_Entrega,  
	CURE.OFIC_Codigo AS Oficina_Cumple,  
    OFIC.Codigo_Alterno AS Codigo_Alterno_Oficina -- Oficina Factura
	
	FROM Encabezado_Remesas ENRE  
    
	INNER JOIN Remesas_Paqueteria AS REPA     
	ON ENRE.EMPR_Codigo = REPA.EMPR_Codigo    
	AND ENRE.Numero = REPA.ENRE_Numero  
	AND REPA.CATA_ESRP_Codigo = 6030 -- Entregada o Cumplida  
      
	LEFT JOIN Encabezado_Planilla_Despachos AS ENPA -- Ultima Planilla Gu�a  
	ON REPA.EMPR_Codigo = ENPA.EMPR_Codigo    
	AND REPA.ENPD_Numero_Ultima_Planilla = ENPA.Numero  
  
	INNER JOIN Cumplido_Remesas AS CURE -- Cumplido Remesas  
	ON ENRE.EMPR_Codigo = CURE.EMPR_Codigo    
	AND ENRE.Numero = CURE.ENRE_Numero   
	
	LEFT JOIN Oficinas AS OFIC -- Oficina Factura Remesas  
	ON REPA.EMPR_Codigo = OFIC.EMPR_Codigo    
	AND REPA.OFIC_Codigo_Factura_Venta = OFIC.Codigo   
	
	INNER JOIN Terceros AS CLIE -- Cliente  
	ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo    
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo  
  
	INNER JOIN Terceros AS DEST -- Destinatario  
	ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo    
	AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo  
   
	INNER JOIN Usuarios AS USEN -- Usuario Entrego  
	ON CURE.EMPR_Codigo = USEN.EMPR_Codigo  
	AND CURE.USUA_Codigo_Crea = USEN.Codigo  
  
	LEFT JOIN Terceros AS TEEN -- Tercero Empleado Entrego  
	ON USEN.EMPR_Codigo = TEEN.EMPR_Codigo  
	AND USEN.TERC_Codigo_Empleado = TEEN.Codigo  
  
	LEFT JOIN Terceros AS TECO -- Tercero Conductor Entrego  
	ON USEN.EMPR_Codigo = TECO.EMPR_Codigo  
	AND USEN.TERC_Codigo_Conductor = TECO.Codigo  
		
	WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo     
	AND ENRE.CATA_FOPR_Codigo = 4903 -- Contra Entrega  
	AND ENRE.Estado = 1  
	AND ENRE.Anulado = 0  
	AND ISNULL(CURE.Interfaz_Contable_Cumplido, 0 ) = 0    
	AND ISNULL(CURE.Intentos_Interfase_Contable, 0 ) < 5  
  
	ORDER BY ENRE.Numero DESC -- Utilizado Pruebas  
     
END