﻿PRINT '[Encabezado_Planilla_Despachos]'
GO
CREATE TABLE [dbo].[Encabezado_Planilla_Despachos](
	[EMPR_Codigo] [smallint] NOT NULL,
	[Numero] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TIDO_Codigo] [numeric](18, 0) NOT NULL,
	[Numero_Documento] [numeric](18, 0) NOT NULL,
	[ENMC_Numero] [numeric](18, 0) NOT NULL,
	[Fecha_Hora_Salida] [datetime] NULL,
	[Fecha] [date] NOT NULL,
	[LNTC_Codigo_Compra] [smallint] NULL,
	[TLNC_Codigo_Compra] [smallint] NULL,
	[RUTA_Codigo] [numeric](18, 0) NOT NULL,
	[TATC_Codigo_Compra] [smallint] NULL,
	[TTTC_Codigo_Compra] [smallint] NULL,
	[ETCC_Numero] [numeric](18, 0) NULL,
	[VEHI_Codigo] [numeric](18, 0) NOT NULL,
	[SEMI_Codigo] [numeric](18, 0) NOT NULL,
	[TERC_Codigo_Tenedor] [numeric](18, 0) NOT NULL,
	[TERC_Codigo_Conductor] [numeric](18, 0) NOT NULL,
	[Cantidad] [numeric](18, 2) NOT NULL,
	[Peso] [numeric](18, 2) NOT NULL,
	[Valor_Flete_Transportador] [money] NOT NULL,
	[Valor_Anticipo] [money] NOT NULL,
	[Valor_Impuestos] [money] NOT NULL,
	[Valor_Pagar_Transportador] [money] NOT NULL,
	[Valor_Flete_Cliente] [money] NOT NULL,
	[Valor_Seguro_Mercancia] [money] NOT NULL,
	[Valor_Otros_Cobros] [money] NOT NULL,
	[Valor_Total_Credito] [money] NOT NULL,
	[Valor_Total_Contado] [money] NOT NULL,
	[Valor_Total_Alcobro] [money] NOT NULL,
	[Valor_Auxiliares] [money] NULL,
	[Observaciones] [varchar](200) NOT NULL,
	[Anulado] [smallint] NOT NULL,
	[Estado] [smallint] NOT NULL,
	[Fecha_Crea] [datetime] NOT NULL,
	[USUA_Codigo_Crea] [smallint] NOT NULL,
	[Fecha_Modifica] [datetime] NULL,
	[USUA_Codigo_Modifica] [smallint] NULL,
	[Fecha_Anula] [datetime] NULL,
	[USUA_Codigo_Anula] [smallint] NULL,
	[Causa_Anula] [varchar](150) NULL,
	[OFIC_Codigo] [numeric](18, 0) NULL,
	[Numeracion] [varchar](50) NULL,
	[ECPD_Numero] [numeric](18, 0) NULL,
	[ELPD_Numero] [numeric](18, 0) NULL,
	[ELGC_Numero] [numeric](18, 0) NULL,
	[Ultimo_Seguimiento_Vehicular] [datetime] NULL,
	[Valor_Seguro_Poliza] [numeric](18, 0) NULL,
	[Anticipo_Pagado_A] [varchar](50) NULL,
	[ENRE_Numero_Masivo] [numeric](18, 0) NULL,
	[ENRE_Numero_Documento_Masivo] [numeric](18, 0) NULL,
	[Valor_Remesas_Contra_Entrega] [numeric](18, 0) NULL,
	[Valor_Bruto] [numeric](18, 0) NULL,
	[Porcentaje] [numeric](18, 0) NULL,
	[Valor_Descuentos] [numeric](18, 0) NULL,
	[Flete_Sugerido] [numeric](18, 0) NULL,
	[Valor_Fondo_Ayuda_Mutua] [numeric](18, 0) NULL,
	[Valor_Neto_Pagar] [numeric](18, 0) NULL,
	[Valor_Utilidad] [numeric](18, 0) NULL,
	[Valor_Estampilla] [numeric](18, 0) NULL,
	[Gastos_Agencia] [numeric](18, 0) NULL,
	[Valor_Reexpedicion] [numeric](18, 0) NULL,
	[Valor_Planilla_Adicional] [numeric](18, 0) NULL,
	[Calcular_Contra_Entrega] [smallint] NULL,
	[ENPD_Numero_Transbordo] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Encabezado_Planilla_Despachos] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]  WITH CHECK ADD  CONSTRAINT [FK_Encabezado_Planilla_Despachos_Empresas] FOREIGN KEY([EMPR_Codigo])
REFERENCES [dbo].[Empresas] ([Codigo])
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos] CHECK CONSTRAINT [FK_Encabezado_Planilla_Despachos_Empresas]
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]  WITH CHECK ADD  CONSTRAINT [FK_Encabezado_Planilla_Despachos_Rutas] FOREIGN KEY([EMPR_Codigo], [RUTA_Codigo])
REFERENCES [dbo].[Rutas] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos] CHECK CONSTRAINT [FK_Encabezado_Planilla_Despachos_Rutas]
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]  WITH CHECK ADD  CONSTRAINT [FK_Encabezado_Planilla_Despachos_Terceros] FOREIGN KEY([EMPR_Codigo], [TERC_Codigo_Conductor])
REFERENCES [dbo].[Terceros] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos] CHECK CONSTRAINT [FK_Encabezado_Planilla_Despachos_Terceros]
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]  WITH CHECK ADD  CONSTRAINT [FK_Encabezado_Planilla_Despachos_Terceros1] FOREIGN KEY([EMPR_Codigo], [TERC_Codigo_Tenedor])
REFERENCES [dbo].[Terceros] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos] CHECK CONSTRAINT [FK_Encabezado_Planilla_Despachos_Terceros1]
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]  WITH CHECK ADD  CONSTRAINT [FK_Encabezado_Planilla_Despachos_Tipo_Documentos] FOREIGN KEY([EMPR_Codigo], [TIDO_Codigo])
REFERENCES [dbo].[Tipo_Documentos] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos] CHECK CONSTRAINT [FK_Encabezado_Planilla_Despachos_Tipo_Documentos]
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]  WITH CHECK ADD  CONSTRAINT [FK_Encabezado_Planilla_Despachos_Usuarios] FOREIGN KEY([EMPR_Codigo], [USUA_Codigo_Crea])
REFERENCES [dbo].[Usuarios] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos] CHECK CONSTRAINT [FK_Encabezado_Planilla_Despachos_Usuarios]
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]  WITH CHECK ADD  CONSTRAINT [FK_Encabezado_Planilla_Despachos_Usuarios1] FOREIGN KEY([EMPR_Codigo], [USUA_Codigo_Crea])
REFERENCES [dbo].[Usuarios] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos] CHECK CONSTRAINT [FK_Encabezado_Planilla_Despachos_Usuarios1]
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos]  WITH CHECK ADD  CONSTRAINT [FK_Encabezado_Planilla_Despachos_Vehiculos] FOREIGN KEY([EMPR_Codigo], [VEHI_Codigo])
REFERENCES [dbo].[Vehiculos] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Encabezado_Planilla_Despachos] CHECK CONSTRAINT [FK_Encabezado_Planilla_Despachos_Vehiculos]
GO

