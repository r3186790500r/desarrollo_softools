﻿PRINT 'TABLA Encabezado_Seguimiento_Vehiculos'
GO
DROP TABLE [dbo].[Encabezado_Seguimiento_Vehiculos]
GO
CREATE TABLE [dbo].[Encabezado_Seguimiento_Vehiculos](
	[EMPR_Codigo] [smallint] NOT NULL,
	[Codigo] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Numero_Documento] [numeric](18, 0) NOT NULL,
	[TIDO_Codigo] [numeric](18, 0) NOT NULL,
	[ENRE_Numero] [numeric](18, 0) NOT NULL,
	[ENMA_Numero] [numeric](18, 0) NOT NULL,
	[Observaciones] [varchar](200) NOT NULL,
	[Estado] [smallint] NOT NULL,
	[USUA_Codigo_Crea] [numeric](18, 0) NOT NULL,
	[Fecha_Crea] [datetime] NOT NULL,
	[USUA_Codigo_Modifica] [numeric](18, 0) NULL,
	[Fecha_Modifica] [datetime] NULL,
	[Anulado] [smallint] NOT NULL,
	[USUA_Codigo_Anula] [numeric](18, 0) NULL,
	[Fecha_Anula] [datetime] NULL,
	[Causa_Anula] [varchar](200) NULL
 CONSTRAINT [PK_Encabezado_Seguimiento_Vehiculos] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO