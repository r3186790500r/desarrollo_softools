﻿print 'gsp_consultar_gestion_documentos'
GO
DROP PROCEDURE gsp_consultar_gestion_documentos
GO
CREATE PROCEDURE gsp_consultar_gestion_documentos  
(
 @par_EMPR_Codigo SMALLINT,  
 @par_EDGD_Codigo NUMERIC = NULL,  
 @par_DOGD_Codigo NUMERIC = NULL,
 @par_Estado NUMERIC = NULL,
 @par_NumeroPagina INT = NULL,  
 @par_RegistrosPagina INT = NULL  
)
AS
BEGIN
 SET NOCOUNT ON;  
  DECLARE  
   @CantidadRegistros INT  
  SELECT @CantidadRegistros = (  
    SELECT DISTINCT   
     COUNT(1)   
    FROM  
     V_Configuracion_Documentos CODO  
    WHERE  
     CODO.EMPR_Codigo = @par_EMPR_Codigo  
     AND CODO.Estado = ISNULL(@par_Estado, CODO.Estado)  
     AND CODO.EDGD_Codigo = ISNULL(@par_EDGD_Codigo, CODO.EDGD_Codigo)  
     AND CODO.DOGD_Codigo = ISNULL(@par_DOGD_Codigo, CODO.DOGD_Codigo)  
     );  
              
    WITH Pagina AS  
    (  
  
    SELECT  
    EMPR_Codigo,
	Codigo,
	EDGD_Codigo,
	DOGD_Codigo,
	Nombre_Entidad,
	Nombre_Documento,
	Aplica_Referencia,
	Aplica_Emisor,
	Aplica_Fecha_Emision,
	Aplica_Fecha_Vencimiento,
	Aplica_Archivo,
	Aplica_Eliminar,
	Aplica_Descargar,
	Tamano,
	Nombre_Tamano,
	Habilitado,
	CATA_TIAD_Codigo,
	Tipo_Archivo,
	Formato_Aplicacion,
	Estado  ,
     ROW_NUMBER() OVER(ORDER BY CODO.Nombre_Documento) AS RowNumber  
    FROM  
     V_Configuracion_Documentos CODO  
    WHERE  
     CODO.EMPR_Codigo = @par_EMPR_Codigo  
     AND CODO.Estado = ISNULL(@par_Estado, CODO.Estado)  
     AND CODO.EDGD_Codigo = ISNULL(@par_EDGD_Codigo, CODO.EDGD_Codigo)  
     AND CODO.DOGD_Codigo = ISNULL(@par_DOGD_Codigo, CODO.DOGD_Codigo)  
  )  
  SELECT DISTINCT  
	0 AS Obtener,  
	EMPR_Codigo,
	Codigo,
	EDGD_Codigo,
	DOGD_Codigo,
	Nombre_Entidad,
	Nombre_Documento,
	Aplica_Referencia,
	Aplica_Emisor,
	Aplica_Fecha_Emision,
	Aplica_Fecha_Vencimiento,
	Aplica_Archivo,
	Aplica_Eliminar,
	Aplica_Descargar,
	Tamano,
	Nombre_Tamano,
	Habilitado,
	CATA_TIAD_Codigo,
	Tipo_Archivo,
	Formato_Aplicacion,
	Estado  ,
	@CantidadRegistros AS TotalRegistros,  
	@par_NumeroPagina AS PaginaObtener,  
	@par_RegistrosPagina AS RegistrosPagina  
	FROM  
	Pagina  
	WHERE  
	RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
	order by Nombre_Documento  
END
GO