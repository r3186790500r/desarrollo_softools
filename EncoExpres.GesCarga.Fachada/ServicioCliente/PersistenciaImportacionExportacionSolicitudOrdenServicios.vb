﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Repositorio.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class PersistenciaImportacionExportacionSolicitudOrdenServicios
        Implements IPersistenciaBase(Of ImportacionExportacionSolicitudOrdenServicios)

        Public Function Consultar(filtro As ImportacionExportacionSolicitudOrdenServicios) As IEnumerable(Of ImportacionExportacionSolicitudOrdenServicios) Implements IPersistenciaBase(Of ImportacionExportacionSolicitudOrdenServicios).Consultar
            Return New RepositorioImportacionExportacionSolicitudOrdenServicios().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ImportacionExportacionSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of ImportacionExportacionSolicitudOrdenServicios).Insertar
            Return New RepositorioImportacionExportacionSolicitudOrdenServicios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ImportacionExportacionSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of ImportacionExportacionSolicitudOrdenServicios).Modificar
            Return New RepositorioImportacionExportacionSolicitudOrdenServicios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ImportacionExportacionSolicitudOrdenServicios) As ImportacionExportacionSolicitudOrdenServicios Implements IPersistenciaBase(Of ImportacionExportacionSolicitudOrdenServicios).Obtener
            Return New RepositorioImportacionExportacionSolicitudOrdenServicios().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ImportacionExportacionSolicitudOrdenServicios) As Boolean
            Return New RepositorioImportacionExportacionSolicitudOrdenServicios().Anular(entidad)
        End Function
    End Class

End Namespace

