﻿Imports Newtonsoft.Json
Namespace Basico.Facturacion

    ''' <summary>
    ''' Clase <see cref=" FacturaElectronica"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class FacturaElectronica
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="FacturaElectronica"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="FacturaElectronica"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

        End Sub
        <JsonProperty>
        Public Property CUFE As String
        <JsonProperty>
        Public Property FechaEnvio As Date
        <JsonProperty>
        Public Property CodigoError As String
        <JsonProperty>
        Public Property MensajeError As String
        <JsonProperty>
        Public Property ID_Pruebas As String
        <JsonProperty>
        Public Property ID_Habilitacion As String
        <JsonProperty>
        Public Property ID_Emision As String
        <JsonProperty>
        Public Property DocumentoUBL As String
        <JsonProperty>
        Public Property TipoDocumentoUBL As String
        <JsonProperty>
        Public Property QRcode As Byte()
        <JsonProperty>
        Public Property Archivo As String
        <JsonProperty>
        Public Property TipoArchivo As String
        <JsonProperty>
        Public Property DecripcionArchivo As String
#Region "Informacion Factura Electronica"
        <JsonProperty>
        Public Property FechaCreacion As String
#End Region
    End Class
End Namespace
