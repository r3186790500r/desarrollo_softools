﻿PRINT 'gsp_actualizar_intentos_ingreso_usuario'
GO
DROP PROCEDURE gsp_actualizar_intentos_ingreso_usuario
GO
CREATE PROCEDURE gsp_actualizar_intentos_ingreso_usuario(
  @par_EMPR_Codigo SMALLINT,
  @par_USUA_Codigo SMALLINT 
  )
AS
BEGIN
  
    UPDATE Usuarios SET Intentos_Ingreso = (Intentos_Ingreso + 1)
    WHERE EMPR_Codigo = @par_EMPR_Codigo
    AND Codigo = @par_USUA_Codigo

    SELECT Intentos_Ingreso AS Intentos FROM Usuarios WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_USUA_Codigo
END
GO