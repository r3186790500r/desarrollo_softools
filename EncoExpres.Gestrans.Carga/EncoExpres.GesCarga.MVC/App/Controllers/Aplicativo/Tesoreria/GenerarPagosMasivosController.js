﻿SofttoolsApp.controller("GenerarPagosMasivosCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'OficinasFactory', 'DocumentoCuentasFactory', 'blockUIConfig', 'TercerosFactory', 'BancosFactory', 'CajasFactory', 'CuentaBancariasFactory', 'ConceptoContablesFactory', 'DetalleConceptoContablesFactory', 'DocumentoComprobantesFactory', 'LiquidacionesFactory', 'PlanillaDespachosFactory',
    function ($scope, $timeout, $linq, blockUI, ValorCatalogosFactory, OficinasFactory, DocumentoCuentasFactory, blockUIConfig, TercerosFactory, BancosFactory, CajasFactory, CuentaBancariasFactory, ConceptoContablesFactory, DetalleConceptoContablesFactory, DocumentoComprobantesFactory, LiquidacionesFactory, PlanillaDespachosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Tesoreria' }, { Nombre: 'Procesos' }, { Nombre: 'Generar Pagos Masivos' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + CODIGO_MENU_GENERAR_PAGOS_MASIVOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }


        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoOficinas = [];
        $scope.ListaCXP = [];
        $scope.ListadoTodaCajaOficinas = [];
        $scope.ListadoConceptoContable = [];
        $scope.cantidadRegistrosPorPagina = 1000
        $scope.paginaActual = 1
        $scope.Modelo = {
            DocumentoOrigen: ''
        }
        $scope.MensajesErrorcxp = [];
        /*Cargar el combo de Documenteos Origen*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDocumentosOrigen = []
                    $scope.ListadoDocumentosOrigen.push({ Codigo: 2600, Nombre: "(TODOS)" });
                    response.data.Datos.forEach(function (item) {
                        if (item.CampoAuxiliar2.match(/EGR/) && item.CampoAuxiliar2.match(/GEN/)) {
                            // se filtra para que solo salgan documentos de anticipos y liquidaciones:
                            if (item.Codigo == 2602 || item.Codigo == 2604) {
                                $scope.ListadoDocumentosOrigen.push(item);
                            }
                        }
                    });
                    if ($scope.CodigoDocumentoOrigen !== undefined && $scope.CodigoDocumentoOrigen !== '' && $scope.CodigoDocumentoOrigen !== null) {
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + $scope.CodigoDocumentoOrigen);
                    }
                    else {
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == 2600');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        /*Cargar el combo de Oficinas*/
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                $scope.ListadoOficinas = [];
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoOficinas = response.data.Datos
                        $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' })
                        if ($scope.CodigoOficinaDestino !== undefined && $scope.CodigoOficinaDestino !== '' && $scope.CodigoOficinaDestino !== null) {
                            $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficinaDestino)
                        }
                        else {
                            $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==-1 ');
                        }

                        try {
                            $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                        } catch (e) {
                            $scope.Oficina = $scope.ListadoOficina[$scope.ListadoOficinas.length - 1];
                        }
                    }
                    else {
                        $scope.ListadoOficinas = [];
                    }
                }
            }, function (response) {
            });

        $scope.ListadoTerceros = []
        $scope.AutocompleteTerceros = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            //CadenaPerfiles: PERFIL_TENEDOR,
                            ValorAutocomplete: value, Sync: true
                        })
                    $scope.ListadoTerceros = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTerceros)
                }
            }
            return $scope.ListadoTerceros
        }

        /*Cargar el listado de Bancos*/
        BancosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoBancos = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoBancos.push(item);
                    });
                    try {
                        $scope.Modelo.Cliente.Impuestos
                        $scope.Modelo.Banco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + $scope.Modelo.Banco.Codigo);
                    } catch (e) {
                        $scope.Modelo.Banco = $scope.ListadoBancos[0]
                    }
                }
            }, function (response) {
            });

        //Cargar Listado Tipos Cuentas Bancarias:
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CUENTA_BANCARIA }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoBancos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoBancos = response.data.Datos;
                        try {
                            $scope.Modelo.TipoBanco = $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo ==' + $scope.Modelo.TipoBanco.Codigo);
                        } catch (e) {
                            $scope.Modelo.TipoBanco = $scope.ListadoTipoBancos[0]
                        }
                    }
                    else {
                        $scope.ListadoTipoBancos = []
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de Forma de pago*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MEDIO_PAGO } }).
            then(function (response) {
                $scope.ListaFormaPago = []
                if (response.data.ProcesoExitoso === true) {

                    for (var i = 0; i < response.data.Datos.length; i++) {
                        if (response.data.Datos[i].Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA || response.data.Datos[i].Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                            $scope.ListaFormaPago.push(response.data.Datos[i])
                        }
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //Cargar Cajas:
        CajasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTodaCajaOficinas = [];
                    $scope.ListadoTodaCajaOficinas = response.data.Datos;

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar Combo Conceptos Contables*/
        ConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoConceptoContable = response.data.Datos;
                    }
                    else {
                        $scope.ListadoConceptoContable = [];
                    }
                }
            }, function (response) {
                ShowError("Error Listado Conceptos Contables : " + response.statusText);
            });

        //Cargar Listados Conceptos Contables : 
        $scope.ListadoMovimientosAnticipo = []
        DetalleConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 13 /*Anticipo*/ }).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    $scope.ListadoMovimientosAnticipo = response.data.Datos;
                }
            });

        $scope.ListadoMovimientosLiquidacion = []
        DetalleConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 14 /*Liquidación*/ }).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    $scope.ListadoMovimientosLiquidacion = response.data.Datos;
                }
            });


        function DatosRequeridosCXP() {
            $scope.MensajesErrorcxp = [];
            var continuar = true

            //if ($scope.Modelo.Tercero == undefined || $scope.Modelo.Tercero == null || $scope.Modelo.Tercero == "" || $scope.Modelo.Tercero.Codigo == 0) {
            //    $scope.MensajesErrorcxp.push('Debe ingresar un tercero');
            //    continuar = false;
            //}
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')

            ) {

            } else if (
                ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesErrorcxp.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorcxp.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar
        }

        /*Consultar Cuentas Pendientes por pagar*/

        $scope.ConsultarCXP = function () {
            $scope.ListaCXP = [];
            $scope.totalRegistros = 0
            if (DatosRequeridosCXP()) {

                if ($scope.Modelo.DocumentoOrigen.Codigo != 2600) {
                    var Filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR, //CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR
                        Tercero: { Codigo: $scope.Modelo.Tercero == undefined ? 0 : $scope.Modelo.Tercero.Codigo },
                        DocumentoOrigen: { Codigo: $scope.Modelo.DocumentoOrigen.Codigo },
                        Oficina: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                        FechaInicial: $scope.FechaInicio,
                        FechaFinal: $scope.FechaFin,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                    }
                    //Consulta Cuentas pendientes:

                    DocumentoCuentasFactory.Consultar(Filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) {
                                        $scope.ListaCXP = response.data.Datos
                                        $scope.ListaCXP.forEach(function (item) {
                                            item.ValorPagar = MascaraValores(item.Saldo)
                                            item.NumeroDocumentoGrid = $scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2613 ? 'A - ' + item.NumeroDocumento : 'L - ' + item.NumeroDocumento
                                            item.NombreDocumentoOrigen = $scope.Modelo.DocumentoOrigen.Nombre
                                            item.Tercero = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Tercero.Codigo, Sync: true }).Datos
                                            item.NombreBanco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + item.Tercero.Banco.Codigo).Nombre
                                            item.NombreTipoCuenta = $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo==' + item.Tercero.TipoBanco.Codigo)
                                            item.NumeroCuentaBancaria = item.Tercero.CuentaBancaria
                                            item.FechaPago = new Date()
                                            item.MedioPago = $scope.ListaFormaPago[1]
                                            item.ConceptoContable = item.ConceptoContable == undefined ? $scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2613 ? $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo==13') : $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo==14') : item2.ConceptoContable
                                            item.MovimientosConcepto = item.MovimientosConcepto == undefined ? $scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2613 ? $scope.ListadoMovimientosAnticipo : $scope.ListadoMovimientosLiquidacion : item.MovimientosConcepto
                                            item.GeneraEgreso = true
                                            item.Vehiculo = $scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2613 ? PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Numero: item.NumeroDocumento, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true }).Datos[0].Vehiculo : $scope.CargarVehiculosPlaca(LiquidacionesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Aprobado: -1, NumeroDocumento: item.NumeroDocumento, Estado: -1, TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO }, Sync: true }).Datos[0].PlacaVehiculo)
                                            $scope.CargarCuentasOrigen(item)
                                            item.CuentaOrigen = item.ListadoCuentasOrigen[0]
                                        });
                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                        //$scope.MaskValores()
                                    } else {
                                        $scope.ListaCXP = [];
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                }
                                else {
                                    $scope.ListaCXP = [];
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                            }
                            $scope.CalcularTotales();
                        }, function (response) {
                        });
                } else {
                    $scope.ListadoDocumentosOrigen.forEach(item => {
                        var Filtros = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR, //CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR
                            Tercero: { Codigo: $scope.Modelo.Tercero == undefined ? 0 : $scope.Modelo.Tercero.Codigo },
                            DocumentoOrigen: { Codigo: item.Codigo },
                            Oficina: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                            FechaInicial: $scope.FechaInicio,
                            FechaFinal: $scope.FechaFin,
                            Pagina: $scope.paginaActual,
                            RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                        }

                        DocumentoCuentasFactory.Consultar(Filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {

                                            if ($scope.ListaCXP.length > 0) {
                                                response.data.Datos.forEach(data => {
                                                    $scope.ListaCXP.push(data)
                                                });
                                            } else {
                                                $scope.ListaCXP = response.data.Datos
                                            }
                                            $scope.ListaCXP.forEach(function (item2) {

                                                item2.ValorPagar = MascaraValores(item2.Saldo)
                                                item2.NumeroDocumentoGrid = item2.NumeroDocumentoGrid == undefined ? item.Codigo == 2604 || item.Codigo == 2613 ? 'A - ' + item2.NumeroDocumento : 'L - ' + item2.NumeroDocumento : item2.NumeroDocumentoGrid
                                                item2.NombreDocumentoOrigen = item2.NombreDocumentoOrigen == undefined ? item.Nombre : item2.NombreDocumentoOrigen
                                                item2.Tercero = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item2.Tercero.Codigo, Sync: true }).Datos
                                                item2.NombreBanco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + item2.Tercero.Banco.Codigo).Nombre
                                                item2.NombreTipoCuenta = $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo==' + item2.Tercero.TipoBanco.Codigo).Nombre
                                                item2.NumeroCuentaBancaria = item2.Tercero.CuentaBancaria
                                                item2.FechaPago = new Date()
                                                item2.MedioPago = $scope.ListaFormaPago[1]
                                                item2.ConceptoContable = item2.ConceptoContable == undefined ? item.Codigo == 2604 || item.Codigo == 2613 ? $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo==13') : $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo==14') : item2.ConceptoContable
                                                item2.MovimientosConcepto = item2.MovimientosConcepto == undefined ? item.Codigo == 2604 || item.Codigo == 2613 ? $scope.ListadoMovimientosAnticipo : $scope.ListadoMovimientosLiquidacion : item2.MovimientosConcepto
                                                item2.GeneraEgreso = true
                                                item2.Vehiculo = item.Codigo == 2604 || item.Codigo == 2613 ? PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Numero: item2.NumeroDocumento, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true }).Datos[0].Vehiculo : $scope.CargarVehiculosPlaca(LiquidacionesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Aprobado: -1, NumeroDocumento: item2.NumeroDocumento, Estado: -1, TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO }, Sync: true }).Datos[0].PlacaVehiculo)
                                                $scope.CargarCuentasOrigen(item2)
                                                item2.CuentaOrigen = item2.ListadoCuentasOrigen[0]

                                            });
                                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                            $scope.Buscando = false;
                                            $scope.ResultadoSinRegistros = '';
                                            
                                        } else {
                                            if ($scope.ListaCXP.length == 0) {
                                                $scope.ListaCXP = [];
                                                $scope.totalRegistros = 0;
                                                $scope.totalPaginas = 0;
                                                $scope.paginaActual = 1;
                                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                                $scope.Buscando = false;
                                            }
                                        }
                                    }
                                    else {
                                        if ($scope.ListaCXP.length == 0) {
                                            $scope.ListaCXP = [];
                                            $scope.totalRegistros = 0;
                                            $scope.totalPaginas = 0;
                                            $scope.paginaActual = 1;
                                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                            $scope.Buscando = false;
                                        }
                                    }
                                }
                                $scope.CalcularTotales();
                            }, function (response) {
                            });


                    });


                }
               
            }
        }

        $scope.ObtenerTercero = function () {
            if ($scope.Modelo.Tercero != undefined) {
                if ($scope.Modelo.Tercero.Codigo > 0) {
                    $scope.Modelo.Tercero = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.Tercero.Codigo, Sync: true }).Datos;
                }
            }
        }

        $scope.CargarCuentasOrigen = function (item) {
            if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                item.ListadoCuentasOrigen = $scope.CargarCajas()
                //Calcular()
            }
            else if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                item.ListadoCuentasOrigen = CargarCuentasBancarias()
                //Calcular()
            }
        }

        $scope.CargarCajas = function () {
            $scope.ListadoCajasOficina = [];
            $scope.MensajesErrorcxp = [];


            $scope.ListadoCajasOficina = $linq.Enumerable().From($scope.ListadoTodaCajaOficinas).Where('$.Oficina.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo).ToArray();
            $scope.ListadoCajasOficina = $linq.Enumerable().From($scope.ListadoTodaCajaOficinas).Select(x => { x.NombreCuenta = x.Nombre; return x }).ToArray()
            if ($scope.ListadoCajasOficina == [] || $scope.ListadoCajasOficina == undefined || $scope.ListadoCajasOficina == null || $scope.ListadoCajasOficina.length == 0) {
                $scope.MensajesErrorcxp.push('La oficina actual no tiene ninguna caja asociada');
                return [];
            } else {
                return $scope.ListadoCajasOficina
            }


        }


        function CargarCuentasBancarias() {
            $scope.MensajesErrorcxp = [];
            /*Cargar el combo Cuentas Bancarias por oficina*/
            var ListaCuentas = CuentaBancariasFactory.CuentaBancariaOficinas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo, Sync: true }).Datos;
            if (ListaCuentas != undefined) {
                if (ListaCuentas.length != 0) {

                    $scope.ListadoCuentaBancariaOficinas = [];
                    $scope.ListadoCuentaBancariaOficinas = ListaCuentas;
                    $scope.ListadoCuentaBancarias = $scope.ListadoCuentaBancariaOficinas
                    return $scope.ListadoCuentaBancarias

                } else {
                    $scope.MensajesErrorcxp.push('La oficina actual no tiene ninguna cuenta bancaria asociada');
                    return [];
                }
            } else {
                $scope.MensajesErrorcxp.push('La oficina actual no tiene ninguna cuenta bancaria asociada');
                return [];
            }

        }

        $scope.GenerarEgresos = function () {
            var ListaSeleccionados = [];

            ListaSeleccionados = $linq.Enumerable().From($scope.ListaCXP).Where('$.GeneraEgreso==true').ToArray();
            if (ListaSeleccionados != undefined) {
                if (ListaSeleccionados.length > 0) {
                    if (DatosRequeridos(ListaSeleccionados)) {
                        ListaSeleccionados.forEach(item => {
                            item.MovimientosConcepto.forEach(itemDetalle => {
                                if (itemDetalle.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_DEBITO) {
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                                        itemDetalle.ValorDebito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar);
                                    }
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                                        itemDetalle.ValorDebito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar)
                                    }
                                    itemDetalle.ValorCredito = 0;
                                    if (itemDetalle.CuentaPUC.AplicarMedioPago > 0) {

                                        if (item.ActivarEfectivo) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaPUC
                                        }
                                        if (item.ActivarConsignacion) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaBancaria.CuentaPUC
                                        }

                                    } else {
                                        itemDetalle.CuentaPUC = itemDetalle.CuentaPUC;
                                    }
                                } else if (itemDetalle.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_CREDITO) {
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                                        itemDetalle.ValorCredito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar);
                                    }
                                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                                        itemDetalle.ValorCredito = MascaraNumero(item.ValorPagar);
                                        itemDetalle.ValorBase = MascaraNumero(item.ValorPagar)
                                    }
                                    itemDetalle.ValorDebito = 0;
                                    if (itemDetalle.CuentaPUC.AplicarMedioPago > 0) {

                                        if (item.ActivarEfectivo) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaPUC
                                        }
                                        if (item.ActivarConsignacion) {
                                            itemDetalle.CuentaPUC = item.CuentaOrigen.CuentaBancaria.CuentaPUC
                                        }

                                    } else {
                                        itemDetalle.CuentaPUC = itemDetalle.CuentaPUC;
                                    }
                                }
                                itemDetalle.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                                itemDetalle.Tercero = { Codigo: item.Tercero.Codigo };
                                itemDetalle.TerceroParametrizacion = { Codigo: 3100 }
                                itemDetalle.DocumentoCruce = { Codigo: 2800 }
                                itemDetalle.CentroCostoParametrizacion = { Codigo: 3200 }
                                itemDetalle.Diferencia = 0
                            });



                            var DetallesMovimientoGuardar = []
                            item.MovimientosConcepto.forEach(itemMovimiento => {
                                DetallesMovimientoGuardar.push({
                                    CodigoEmpresa: itemMovimiento.CodigoEmpresa,
                                    Numero: itemMovimiento.Numero,
                                    CuentaPUC: { Codigo: itemMovimiento.CuentaPUC.Codigo },
                                    Tercero: { Codigo: itemMovimiento.Tercero.Codigo },
                                    ValorBase: itemMovimiento.ValorBase,
                                    ValorDebito: itemMovimiento.ValorDebito,
                                    ValorCredito: itemMovimiento.ValorCredito,
                                    GeneraCuenta: MascaraNumero(itemMovimiento.GeneraCuenta),
                                    Observaciones: itemMovimiento.Observaciones,
                                    TerceroParametrizacion: { Codigo: itemMovimiento.TerceroParametrizacion.Codigo },
                                    CentroCostoParametrizacion: { Codigo: itemMovimiento.CentroCostoParametrizacion.Codigo },
                                    DocumentoCruce: { Codigo: itemMovimiento.DocumentoCruce.Codigo }
                                });
                            });

                            $scope.Modelo.Cuentas = [];

                            $scope.Modelo.Cuentas.push(
                                {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Codigo: item.Codigo,
                                    ValorPagar: MascaraNumero(item.ValorPagar),
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                }
                            )


                            $scope.ListaDetalleCuentas = [];
                            $scope.ListaDetalleCuentas.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoDocumentoCuenta: item.Codigo, ValorPago: MascaraNumero(item.ValorPagar) })


                            //Preparar Objeto Guardar:
                            var Entidad = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                CodigoAlterno: 0,
                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                                FormaPagoDocumento: { Codigo: item.MedioPago.Codigo },
                                DocumentoOrigen: { Codigo: item.DocumentoOrigen.Codigo },
                                NumeroDocumentoOrigen: item.NumeroDocumento,
                                Fecha: item.FechaPago,
                                Estado: ESTADO_ACTIVO,
                                ValorAlterno: 0,
                                ConceptoContable: item.ConceptoContable.Codigo,
                                Beneficiario: { Codigo: item.Tercero.Codigo },
                                Tercero: { Codigo: item.Tercero.Codigo },
                                Caja: { Codigo: item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO ? item.CuentaOrigen.Codigo : 0 },
                                ValorPagoEfectivo: item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO ? MascaraNumero(item.ValorPagar) : 0,
                                CuentaBancaria: { Codigo: item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO ? 0 : item.CuentaOrigen.CuentaBancaria.Codigo },
                                Vehiculo: { Codigo: item.Vehiculo.Codigo },
                                Observaciones: item.Observaciones,
                                FechaPagoRecaudo: item.FechaPago,
                                DestinoIngreso: { Codigo: item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO ? 4801 : 4802 }, //Codigo Destino ingreso Caja\Cuenta Bancaria
                                ValorPagoTotal: MascaraNumero(item.ValorPagar),
                                NumeroPagoRecaudo: 0,
                                ValorPagoTransferencia: item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO ? 0 : MascaraNumero(item.ValorPagar),
                                CuentaBancariaCheque: { Codigo: item.CuentaOrigen.CuentaBancaria.Codigo },
                                CuentaBancariaConsignacion: { Codigo: item.CuentaOrigen.CuentaBancaria.Codigo },
                                GeneraConsignacion: "",
                                Detalle: DetallesMovimientoGuardar,
                                DetalleCuentas: $scope.ListaDetalleCuentas,
                                Cuentas: $scope.Modelo.Cuentas,
                                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                OficinaDestino: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                Sync: true
                            }
                            //Fin Objeto Guardar

                            var Errores = 0
                            var ResponseGuardar = DocumentoComprobantesFactory.Guardar(Entidad);

                            if (ResponseGuardar != undefined) {
                                if (ResponseGuardar.ProcesoExitoso === true) {
                                    console.log('Se guardó el comprobante de egreso N.' + ResponseGuardar.Datos)
                                } else {
                                    Errores++;
                                    console.log('No se pudo guardar el comprobante, consulte con el administrador del sistema :' + ResponseGuardar.MensajeOperacion);
                                }
                            } else {
                                Errores++;
                                console.log('Error al guardar, consulte con el administrador del sistema');
                            }
                            if (Errores > 0) {
                                ShowError('Algunos detalles no se guardaron correctamente, verifique la información de los detalles')
                            } else {
                                $scope.ConsultarCXP()
                                ShowSuccess('Se generaron ' + ListaSeleccionados.length + ' comprobantes de egreso')
                            }

                        });
                    }
                } else {
                    ShowError('Debe seleccionar al menos una cuenta por pagar')
                }
            } else {
                ShowError('Debe seleccionar al menos una cuenta por pagar')
            }
        }

        function DatosRequeridos(ListaCuentas) {
            var continuar = true
            $scope.MensajesErrorcxp = [];
            ListaCuentas.forEach(item => {
                if (MascaraNumero(item.ValorPagar) == 0 || MascaraNumero(item.ValorPagar) == undefined || MascaraNumero(item.ValorPagar) == null) {
                    continuar = false
                }
                if (item.MedioPago == undefined || item.MedioPago == null || item.MedioPago == '') {
                    continuar = false
                } else if (item.MedioPago.Codigo == undefined || item.MedioPago.Codigo == null || item.MedioPago.Codigo == '' || item.MedioPago.Codigo == 0) {
                    continuar = false
                }
                if (item.CuentaOrigen == undefined || item.CuentaOrigen == null || item.CuentaOrigen == '') {
                    continuar = false
                } else if (item.CuentaOrigen.Codigo == undefined || item.CuentaOrigen.Codigo == null || item.CuentaOrigen.Codigo == '' || item.CuentaOrigen.Codigo == 0) {

                    if (item.MedioPago.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                        if (item.CuentaOrigen.CuentaBancaria.Codigo == undefined || item.CuentaOrigen.CuentaBancaria.Codigo == null || item.CuentaOrigen.CuentaBancaria.Codigo == '' || item.CuentaOrigen.CuentaBancaria.Codigo == 0) {
                            continuar = false
                        }
                    } else {
                        if (item.CuentaOrigen.Codigo == undefined || item.CuentaOrigen.Codigo == null || item.CuentaOrigen.Codigo == '' || item.CuentaOrigen.Codigo == 0) {
                            continuar = false
                        }
                    }
                }
            });

            if (!continuar) {
                $scope.MensajesErrorcxp.push('Debe llenar toda la información de los items seleccionados')
            }

            return continuar
        }

        $scope.MarcarEgresos = function () {
            $scope.ListaCXP = $linq.Enumerable().From($scope.ListaCXP).Select(function (x) {
                x.GeneraEgreso = $scope.CuentasSeleccionados
                return x
            }).ToArray()

            //$scope.CalcularTotales();
        }

        $scope.CalcularTotales = function () {
            var TotalPendiente = 0
            var TotalTraslado = 0

            $scope.ListaCXP.forEach(item => {
                if (item.GeneraEgreso) {
                    TotalTraslado = TotalTraslado + MascaraNumero(item.ValorPagar)
                } else {
                    TotalPendiente = TotalPendiente + MascaraNumero(item.ValorPagar)
                }
                $scope.Modelo.TotalPendiente = TotalPendiente
                $scope.Modelo.TotalTraslado = TotalTraslado
            });
        }


        //Máscaras:
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        }
    }
]);