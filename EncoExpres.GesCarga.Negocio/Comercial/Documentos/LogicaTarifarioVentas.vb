﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Fachada.Comercial.Documentos
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Comercial.Documentos
    Public NotInheritable Class LogicaTarifarioVentas
        Inherits LogicaBase(Of TarifarioVentas)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of TarifarioVentas)

        Sub New(persistencia As IPersistenciaBase(Of TarifarioVentas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As TarifarioVentas) As Respuesta(Of IEnumerable(Of TarifarioVentas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of TarifarioVentas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of TarifarioVentas))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As TarifarioVentas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As TarifarioVentas) As Respuesta(Of TarifarioVentas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of TarifarioVentas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of TarifarioVentas)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As TarifarioVentas) As Respuesta(Of TarifarioVentas)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of TarifarioVentas) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of TarifarioVentas) With {.ProcesoExitoso = True}

        End Function
        Public Function GenerarPlanitilla(entidad As TarifarioVentas) As Respuesta(Of TarifarioVentas)
            Dim mensajeGuardo = "Genero Archivo"
            Dim url As New TarifarioVentas

            url = CType(_persistencia, PersistenciaTarifarioVentas).GenerarPlanitilla(entidad)

            Return New Respuesta(Of TarifarioVentas)(url)
        End Function
        Public Function Anular(entidad As TarifarioVentas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaTarifarioVentas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function ConsultarDetalleTarifarioVentas(entidad As DetalleTarifarioVentas) As Respuesta(Of IEnumerable(Of DetalleTarifarioVentas))
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Lista As IEnumerable(Of DetalleTarifarioVentas)

            Lista = CType(_persistencia, PersistenciaTarifarioVentas).ConsultarDetalleTarifarioVentas(entidad)

            If IsNothing(Lista) Then
                Return New Respuesta(Of IEnumerable(Of DetalleTarifarioVentas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleTarifarioVentas))(Lista)
        End Function

        Public Function ObtenerDetalleTarifarioPaqueteria(filtro As TarifarioVentas) As Respuesta(Of TarifarioVentas)
            Dim Obtener As TarifarioVentas
            Obtener = CType(_persistencia, PersistenciaTarifarioVentas).ObtenerDetalleTarifarioPaqueteria(filtro)
            If IsNothing(Obtener) Then
                Return New Respuesta(Of TarifarioVentas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If
            Return New Respuesta(Of TarifarioVentas)(Obtener)
        End Function
    End Class

End Namespace