﻿Imports Newtonsoft.Json

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="UsuarioGrupoUsuarios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class UsuarioGrupoUsuarios
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="UsuarioGrupoUsuarios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="UsuarioGrupoUsuarios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            CodigoUsuario = Read(lector, "USUA_Codigo")
            GrupoUsuarios = New GrupoUsuarios With {.Codigo = Read(lector, "GRUS_Codigo"), .Nombre = Read(lector, "NombreGrupo")}
        End Sub

        ''' <summary>
        ''' Obtiene o establece el grupo usuarios
        ''' </summary>
        <JsonProperty>
        Public Property GrupoUsuarios As GrupoUsuarios

        ''' <summary>
        ''' Obtiene o establece el codigo usuario
        ''' </summary>
        <JsonProperty>
        Public Property CodigoUsuario As Short

        ''' <summary>
        ''' Obtiene o establece el grupo de seguridad
        ''' </summary>
        <JsonProperty>
        Public Property CodigoGrupoUsuarios As Short
        <JsonProperty>
        Public Property RegistrosAfectados As Short
    End Class

End Namespace
