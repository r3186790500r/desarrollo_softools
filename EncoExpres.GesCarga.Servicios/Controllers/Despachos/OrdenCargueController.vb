﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Despachos.OrdenCargue
Imports EncoExpres.GesCarga.Negocio.Despachos

''' <summary>
''' Controlador <see cref="OrdenCargueController"/>
''' </summary>
<Authorize>
Public Class OrdenCargueController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As OrdenCargue) As Respuesta(Of IEnumerable(Of OrdenCargue))
        Return New LogicaOrdenCargue(CapaPersistenciaOrdenCargue).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As OrdenCargue) As Respuesta(Of OrdenCargue)
        Return New LogicaOrdenCargue(CapaPersistenciaOrdenCargue).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As OrdenCargue) As Respuesta(Of Long)
        Return New LogicaOrdenCargue(CapaPersistenciaOrdenCargue).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As OrdenCargue) As Respuesta(Of Boolean)
        Return New LogicaOrdenCargue(CapaPersistenciaOrdenCargue).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("ActualizarCargueDescargue")>
    Public Function ActualizarCargueDescargue(ByVal entidad As OrdenCargue) As Respuesta(Of Long)
        Return New LogicaOrdenCargue(CapaPersistenciaOrdenCargue).ActualizarCargueDescargue(entidad)
    End Function
End Class
