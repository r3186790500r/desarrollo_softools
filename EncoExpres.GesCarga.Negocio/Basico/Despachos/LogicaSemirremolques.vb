﻿Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Fachada.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Basico.Despachos
    Public NotInheritable Class LogicaSemirremolques
        Inherits LogicaBase(Of Semirremolques)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Semirremolques)

        Sub New(persistencia As IPersistenciaBase(Of Semirremolques))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Semirremolques) As Respuesta(Of IEnumerable(Of Semirremolques))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Semirremolques))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Semirremolques))(consulta)
        End Function

        Public Function ConsultarDocumentos(filtro As SemirremolquesDocumentos) As Respuesta(Of IEnumerable(Of SemirremolquesDocumentos))
            Dim consulta = CType(_persistencia, PersistenciaSemirremolques).ConsultarDocumentos(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of SemirremolquesDocumentos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of SemirremolquesDocumentos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Semirremolques) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function GuardarNovedad(entidad As Semirremolques) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            Dim consulta = CType(_persistencia, PersistenciaSemirremolques).GuardarNovedad(entidad)

            If consulta.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Semirremolques) As Respuesta(Of Semirremolques)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Semirremolques)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Semirremolques)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As Semirremolques) As Respuesta(Of Semirremolques)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Semirremolques) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Semirremolques) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As Semirremolques) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaSemirremolques).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function ObtenerSemirremolqueEstudioSeguridad(filtro As EstudioSeguridad) As Respuesta(Of EstudioSeguridad)

            Dim consulta = CType(_persistencia, PersistenciaSemirremolques).ConsultarSemirremolqueEstudioSeguridad(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EstudioSeguridad)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EstudioSeguridad)(consulta)
        End Function
        Public Function GenerarPlanitilla(entidad As Semirremolques) As Respuesta(Of Semirremolques)
            Dim mensajeGuardo = "Genero Archivo"
            Dim url As New Semirremolques
            url = CType(_persistencia, PersistenciaSemirremolques).GenerarPlanitilla(entidad)
            Return New Respuesta(Of Semirremolques)(url)
        End Function
    End Class

End Namespace