﻿Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Fachada.Tesoreria
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Tesoreria
    Public NotInheritable Class LogicaCuentasPorCobrar
        Inherits LogicaBase(Of CuentasPorCobrar)

        ReadOnly _persistencia As IPersistenciaBase(Of CuentasPorCobrar)

        Sub New(persistencia As IPersistenciaBase(Of CuentasPorCobrar))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As CuentasPorCobrar) As Respuesta(Of IEnumerable(Of CuentasPorCobrar))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of CuentasPorCobrar))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of CuentasPorCobrar))(consulta)
        End Function


        Public Overrides Function Guardar(entidad As CuentasPorCobrar) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Codigo, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Private Function DatosRequeridos(entidad As CuentasPorCobrar) As Respuesta(Of CuentasPorCobrar)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of CuentasPorCobrar) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of CuentasPorCobrar) With {.ProcesoExitoso = True}

        End Function

        Public Overrides Function Obtener(filtro As CuentasPorCobrar) As Respuesta(Of CuentasPorCobrar)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of CuentasPorCobrar)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of CuentasPorCobrar)(consulta)
        End Function

        Public Function Anular(entidad As CuentasPorCobrar) As Respuesta(Of CuentasPorCobrar)
            Return New Respuesta(Of CuentasPorCobrar)(CType(_persistencia, PersistenciaCuentasPorCobrar).Anular(entidad))
        End Function

        Public Function ConsultarDetalleObservaciones(entidad As CuentasPorCobrar) As Respuesta(Of CuentasPorCobrar)
            Dim Cuenta As New CuentasPorCobrar
            Cuenta = CType(_persistencia, PersistenciaCuentasPorCobrar).ConsultarDetalleObservaciones(entidad)

            If IsNothing(Cuenta) Then
                Return New Respuesta(Of CuentasPorCobrar)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of CuentasPorCobrar)(Cuenta)
        End Function

        Public Function GuardarDetalleObservaciones(entidad As CuentasPorCobrar) As Respuesta(Of Long)
            Dim Cuenta As Long = 0
            Cuenta = CType(_persistencia, PersistenciaCuentasPorCobrar).GuardarDetalleObservaciones(entidad)

            If Cuenta = 0 Then
                Return New Respuesta(Of Long)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Long)(Cuenta)
        End Function
    End Class
End Namespace