﻿PRINT 'gsp_consultar_auditoria_documentos'
GO
DROP PROCEDURE gsp_consultar_auditoria_documentos
GO
CREATE PROCEDURE gsp_consultar_auditoria_documentos    
(    
@par_EMPR_Codigo SMALLINT,     
@par_Tipo_Consulta NUMERIC,    
@par_Numero_Inicio NUMERIC = NULL,    
@par_Numero_Fin NUMERIC = NULL,    
@par_Fecha_Inicial DATETIME = NULL,    
@par_Fecha_Final DATETIME = NULL,    
@par_Usuario VARCHAR(100) = NULL,    
@par_Estado NUMERIC = NULL,    
@par_Anulado NUMERIC = NULL,    
@par_NumeroPagina INT = NULL,    
@par_RegistrosPagina INT = NULL    
)    
AS    
BEGIN     
    
 set @par_Fecha_Inicial = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Inicial)               
 set @par_Fecha_Inicial = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Inicial)),@par_Fecha_Inicial)                      
 set @par_Fecha_Inicial = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Inicial)),@par_Fecha_Inicial)                        
 set @par_Fecha_Final = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Final)                    
 set @par_Fecha_Final = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Final)),@par_Fecha_Final)                      
 set @par_Fecha_Final = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Final)),@par_Fecha_Final)                      
 set @par_Fecha_Final = DATEADD(MILLISECOND, 998,@par_Fecha_Final)                      
 set @par_Fecha_Final = DATEADD(SECOND,59,@par_Fecha_Final)                      
 set @par_Fecha_Final = DATEADD(MINUTE,59,@par_Fecha_Final)                      
 set @par_Fecha_Final = DATEADD(HOUR,23,@par_Fecha_Final)          
     
 --Seleccion tipo de consulta     
             
 IF @par_Tipo_Consulta = 8 --Tarifario Compras    
     
 BEGIN    
    SET NOCOUNT ON; 
	
    UPDATE Encabezado_Tarifario_Carga_Compras SET Anulado = 0 WHERE Anulado IS NULL    
 DECLARE @CantidadRegistrosTarifarioCompras INT    
 SELECT @CantidadRegistrosTarifarioCompras = (    
 SELECT DISTINCT     
    
 COUNT(1)     
    
 FROM     
    
 Encabezado_Tarifario_Carga_Compras AS ETCC LEFT JOIN Usuarios AS USCC  ON    
 ETCC.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ETCC.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ETCC.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ETCC.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ETCC.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ETCC.USUA_Codigo_Anula = USCA.Codigo    
     
 WHERE     
    
 ETCC.EMPR_Codigo = @par_EMPR_Codigo      
 AND ETCC.Numero >= ISNULL(@par_Numero_Inicio,ETCC.Numero)                                
 AND ETCC.Numero <= ISNULL(@par_Numero_Fin,ETCC.Numero)     
 AND ETCC.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ETCC.Fecha_Crea)                                
 AND ETCC.Fecha_Crea <= ISNULL(@par_Fecha_Final,ETCC.Fecha_Crea)    
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))     
 AND ETCC.Estado = ISNULL(@par_Estado, ETCC.Estado)     
 AND ETCC.Anulado = ISNULL(@par_Anulado, ETCC.Anulado)     
 );    
                
 WITH PaginaTarifarioCompras AS    
 (    
    
   SELECT     
 0 AS Obtener,     
 ETCC.EMPR_Codigo,    
 ETCC.Numero,    
 USCC.Nombre AS Usuario_Crea,    
 ETCC.Fecha_Crea,    
 USCM.Nombre AS Usuario_Modifica,    
 ETCC.Fecha_Modifica,    
 USCA.Nombre AS Usuario_Anula,    
 ETCC.Fecha_Anula,    
 ETCC.Causa_Anula,   
 CASE WHEN (ETCC.Estado = 0 AND ETCC.Estado = @par_Estado) OR ETCC.Estado = 0 THEN 'BORRADOR'     
 WHEN (ETCC.Estado = 1 AND ETCC.Anulado = 0 AND ETCC.Estado = @par_Estado ) OR (ETCC.Estado = 1 AND ETCC.Anulado = 0) THEN 'DEFINITIVO'    
 WHEN (@par_Anulado = 1 AND ETCC.Anulado = 1) OR ETCC.Anulado = 1 THEN 'ANULADO' END AS Nombre_Estado,   
 ROW_NUMBER() OVER(ORDER BY ETCC.Numero) AS RowNumber    
     
 FROM     
 --SELECT * FROM
 Encabezado_Tarifario_Carga_Compras AS ETCC LEFT JOIN Usuarios AS USCC  ON    
 ETCC.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ETCC.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ETCC.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ETCC.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ETCC.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ETCC.USUA_Codigo_Anula = USCA.Codigo    
     
 WHERE     
    
 ETCC.EMPR_Codigo = @par_EMPR_Codigo      
 AND ETCC.Numero >= ISNULL(@par_Numero_Inicio,ETCC.Numero)                                
 AND ETCC.Numero <= ISNULL(@par_Numero_Fin,ETCC.Numero)     
 AND ETCC.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ETCC.Fecha_Crea)                                
 AND ETCC.Fecha_Crea <= ISNULL(@par_Fecha_Final,ETCC.Fecha_Crea)    
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))     
 AND ETCC.Estado = ISNULL(@par_Estado, ETCC.Estado)     
 AND ETCC.Anulado = ISNULL(@par_Anulado, ETCC.Anulado)       
 )    
 SELECT    
 0 AS Obtener,    
 EMPR_Codigo,    
 ISNULL(Numero, 0) AS Numero,    
 ISNULL(Usuario_Crea, '') AS Usuario_Crea,    
 ISNULL(Fecha_Crea, '') AS Fecha_Crea,    
 ISNULL(Usuario_Modifica,'')  AS Usuario_Modifica,    
 ISNULL(Fecha_Modifica, '') AS Fecha_Modifica,    
 ISNULL(Usuario_Anula, '') AS Usuario_Anula,    
 ISNULL(Fecha_Anula, '') AS Fecha_Anula,    
 ISNULL(Causa_Anula, '') AS Causa_Anulacion,    
 ISNULL(Nombre_Estado, '') AS Nombre_Estado,    
 @CantidadRegistrosTarifarioCompras AS TotalRegistros,    
 @par_NumeroPagina AS PaginaObtener,    
 @par_RegistrosPagina AS RegistrosPagina    
 FROM    
  PaginaTarifarioCompras    
 WHERE    
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosTarifarioCompras)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosTarifarioCompras)    
 ORDER BY Numero    
    
END    
     
 ELSE IF @par_Tipo_Consulta = 9 --Tarifario Ventas    
    
 BEGIN    
    SET NOCOUNT ON; 	
	
    UPDATE Encabezado_Tarifario_Carga_Ventas SET Anulado = 0 WHERE Anulado IS NULL    
 DECLARE @CantidadRegistrosTarifarioVentas INT    
 SELECT @CantidadRegistrosTarifarioVentas = (    
 SELECT DISTINCT     
    
 COUNT(1)     
    
 FROM     
    
 Encabezado_Tarifario_Carga_Ventas AS ETCV LEFT JOIN Usuarios AS USCC  ON    
 ETCV.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ETCV.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ETCV.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ETCV.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ETCV.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ETCV.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ETCV.EMPR_Codigo = @par_EMPR_Codigo      
 AND ETCV.Numero >= ISNULL(@par_Numero_Inicio, ETCV.Numero)                                
 AND ETCV.Numero <= ISNULL(@par_Numero_Fin, ETCV.Numero)     
 AND ETCV.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ETCV.Fecha_Crea)                                
 AND ETCV.Fecha_Crea <= ISNULL(@par_Fecha_Final,ETCV.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ETCV.Estado = ISNULL(@par_Estado, ETCV.Estado)     
 AND ETCV.Anulado = ISNULL(@par_Anulado, ETCV.Anulado)     
 );    
                
 WITH PaginaTarifarioVentas AS    
 (    
    
 SELECT     
 0 AS Obtener,    
 ETCV.EMPR_Codigo,    
 ETCV.Numero,     
 USCC.Nombre AS Usuario_Crea,    
 ETCV.Fecha_Crea,    
 USCM.Nombre AS Usuario_Modifica,    
 ETCV.Fecha_Modifica,    
 USCA.Nombre AS Usuario_Anula,    
 ETCV.Fecha_Anula,    
 ETCV.Causa_Anula AS Causa_Anulacion,    
 CASE WHEN (ETCV.Estado = 0 AND ETCV.Estado = @par_Estado) OR ETCV.Estado = 0 THEN 'BORRADOR'     
 WHEN (ETCV.Estado = 1 AND ETCV.Anulado = 0 AND ETCV.Estado = @par_Estado ) OR (ETCV.Estado = 1 AND ETCV.Anulado = 0) THEN 'DEFINITIVO'    
 WHEN (@par_Anulado = 1 AND ETCV.Anulado = 1) OR ETCV.Anulado = 1 THEN 'ANULADO' END AS Nombre_Estado,    
 ROW_NUMBER() OVER(ORDER BY ETCV.Numero) AS RowNumber    
  
 FROM     
    
 Encabezado_Tarifario_Carga_Ventas AS ETCV LEFT JOIN Usuarios AS USCC  ON    
 ETCV.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ETCV.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ETCV.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ETCV.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ETCV.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ETCV.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ETCV.EMPR_Codigo = @par_EMPR_Codigo      
 AND ETCV.Numero >= ISNULL(@par_Numero_Inicio, ETCV.Numero)                                
 AND ETCV.Numero <= ISNULL(@par_Numero_Fin, ETCV.Numero)     
 AND ETCV.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ETCV.Fecha_Crea)                                
 AND ETCV.Fecha_Crea <= ISNULL(@par_Fecha_Final,ETCV.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ETCV.Estado = ISNULL(@par_Estado, ETCV.Estado)     
 AND ETCV.Anulado = ISNULL(@par_Anulado, ETCV.Anulado)     
 )    
 SELECT    
 0 AS Obtener,    
 EMPR_Codigo,     
 ISNULL(Numero, 0) AS Numero,    
 ISNULL(Usuario_Crea, '') AS Usuario_Crea,    
 ISNULL(Fecha_Crea, '') AS Fecha_Crea,    
 ISNULL(Usuario_Modifica,'')  AS Usuario_Modifica,    
 ISNULL(Fecha_Modifica, '') AS Fecha_Modifica,    
 ISNULL(Usuario_Anula, '') AS Usuario_Anula,    
 ISNULL(Fecha_Anula, '') AS Fecha_Anula,    
 ISNULL(Causa_Anulacion, '') AS Causa_Anulacion,    
 ISNULL(Nombre_Estado, '') AS Nombre_Estado,    
 @CantidadRegistrosTarifarioVentas AS TotalRegistros,    
 @par_NumeroPagina AS PaginaObtener,    
 @par_RegistrosPagina AS RegistrosPagina    
 FROM    
  PaginaTarifarioVentas    
 WHERE    
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosTarifarioVentas)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosTarifarioVentas)    
 ORDER BY Numero    
    
END  

 ELSE IF @par_Tipo_Consulta = 4 --Manifiesto Carga    
    
 BEGIN    
    SET NOCOUNT ON;   

	UPDATE Encabezado_Manifiesto_Carga SET Anulado = 0 WHERE Anulado IS NULL  
 DECLARE @CantidadRegistrosManifiestoCarga INT    
 SELECT @CantidadRegistrosManifiestoCarga = (    
 SELECT DISTINCT     
    
 COUNT(1)     
    
 FROM     
    
 Encabezado_Manifiesto_Carga AS ENMC LEFT JOIN Usuarios AS USCC  ON    
 ENMC.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENMC.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENMC.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENMC.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENMC.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENMC.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENMC.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENMC.Numero >= ISNULL(@par_Numero_Inicio, ENMC.Numero)                                
 AND ENMC.Numero <= ISNULL(@par_Numero_Fin, ENMC.Numero)     
 AND ENMC.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENMC.Fecha_Crea)                                
 AND ENMC.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENMC.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENMC.Estado = ISNULL(@par_Estado, ENMC.Estado)     
 AND ENMC.Anulado = ISNULL(@par_Anulado, ENMC.Anulado)     
 );    
                
 WITH PaginaManifiestoCarga AS    
 (    
    
 SELECT     
 0 AS Obtener,    
 ENMC.EMPR_Codigo,    
 ENMC.Numero,     
 USCC.Nombre AS Usuario_Crea,    
 ENMC.Fecha_Crea,    
 USCM.Nombre AS Usuario_Modifica,    
 ENMC.Fecha_Modifica,    
 USCA.Nombre AS Usuario_Anula,    
 ENMC.Fecha_Anula,    
 ENMC.Causa_Anula AS Causa_Anulacion,    
 CASE WHEN (ENMC.Estado = 0 AND ENMC.Estado = @par_Estado) OR ENMC.Estado = 0 THEN 'BORRADOR'     
 WHEN (ENMC.Estado = 1 AND ENMC.Anulado = 0 AND ENMC.Estado = @par_Estado ) OR (ENMC.Estado = 1 AND ENMC.Anulado = 0) THEN 'DEFINITIVO'    
 WHEN (@par_Anulado = 1 AND ENMC.Anulado = 1) OR ENMC.Anulado = 1 THEN 'ANULADO' END AS Nombre_Estado,    
 ROW_NUMBER() OVER(ORDER BY ENMC.Numero) AS RowNumber    
  
 FROM     
    
 Encabezado_Manifiesto_Carga AS ENMC LEFT JOIN Usuarios AS USCC  ON    
 ENMC.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENMC.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENMC.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENMC.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENMC.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENMC.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENMC.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENMC.Numero >= ISNULL(@par_Numero_Inicio, ENMC.Numero)                                
 AND ENMC.Numero <= ISNULL(@par_Numero_Fin, ENMC.Numero)     
 AND ENMC.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENMC.Fecha_Crea)                                
 AND ENMC.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENMC.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENMC.Estado = ISNULL(@par_Estado, ENMC.Estado)     
 AND ENMC.Anulado = ISNULL(@par_Anulado, ENMC.Anulado)     
 )    
 SELECT    
 0 AS Obtener,    
 EMPR_Codigo,     
 ISNULL(Numero, 0) AS Numero,    
 ISNULL(Usuario_Crea, '') AS Usuario_Crea,    
 ISNULL(Fecha_Crea, '') AS Fecha_Crea,    
 ISNULL(Usuario_Modifica,'')  AS Usuario_Modifica,    
 ISNULL(Fecha_Modifica, '') AS Fecha_Modifica,    
 ISNULL(Usuario_Anula, '') AS Usuario_Anula,    
 ISNULL(Fecha_Anula, '') AS Fecha_Anula,    
 ISNULL(Causa_Anulacion, '') AS Causa_Anulacion,    
 ISNULL(Nombre_Estado, '') AS Nombre_Estado,    
 @CantidadRegistrosManifiestoCarga AS TotalRegistros,    
 @par_NumeroPagina AS PaginaObtener,    
 @par_RegistrosPagina AS RegistrosPagina    
 FROM    
  PaginaManifiestoCarga    
 WHERE    
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosManifiestoCarga)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosManifiestoCarga)    
 ORDER BY Numero    
    
END  

 ELSE IF @par_Tipo_Consulta = 1 --Comprobante de Ingreso   
    
 BEGIN    
    SET NOCOUNT ON; 
	
	UPDATE Encabezado_Documento_Comprobantes SET Anulado = 0 WHERE Anulado IS NULL    
 DECLARE @CantidadRegistrosComprobanteIngreso INT    
 SELECT @CantidadRegistrosComprobanteIngreso = (    
 SELECT DISTINCT     
    
 COUNT(1)     
    
 FROM     
    
 Encabezado_Documento_Comprobantes AS ENDC LEFT JOIN Usuarios AS USCC  ON    
 ENDC.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENDC.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENDC.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENDC.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENDC.Numero >= ISNULL(@par_Numero_Inicio, ENDC.Numero)                                
 AND ENDC.Numero <= ISNULL(@par_Numero_Fin, ENDC.Numero)     
 AND ENDC.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENDC.Fecha_Crea)                                
 AND ENDC.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENDC.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENDC.Estado = ISNULL(@par_Estado, ENDC.Estado)     
 AND ENDC.Anulado = ISNULL(@par_Anulado, ENDC.Anulado) 
 AND ENDC.CATA_DOOR_Codigo = 2607 --Comprobante Ingreso
 );    
                
 WITH PaginaComprobanteIngreso AS    
 (    
    
 SELECT     
 0 AS Obtener,    
 ENDC.EMPR_Codigo,    
 ENDC.Numero,     
 USCC.Nombre AS Usuario_Crea,    
 ENDC.Fecha_Crea,    
 USCM.Nombre AS Usuario_Modifica,    
 ENDC.Fecha_Modifica,    
 USCA.Nombre AS Usuario_Anula,    
 ENDC.Fecha_Anula,    
 ENDC.Causa_Anulacion AS Causa_Anulacion,    
 CASE WHEN (ENDC.Estado = 0 AND ENDC.Estado = @par_Estado) OR ENDC.Estado = 0 THEN 'BORRADOR'     
 WHEN (ENDC.Estado = 1 AND ENDC.Anulado = 0 AND ENDC.Estado = @par_Estado ) OR (ENDC.Estado = 1 AND ENDC.Anulado = 0) THEN 'DEFINITIVO'    
 WHEN (@par_Anulado = 1 AND ENDC.Anulado = 1) OR ENDC.Anulado = 1 THEN 'ANULADO' END AS Nombre_Estado,    
 ROW_NUMBER() OVER(ORDER BY ENDC.Numero) AS RowNumber    
  
 FROM     
    
 Encabezado_Documento_Comprobantes AS ENDC LEFT JOIN Usuarios AS USCC  ON    
 ENDC.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENDC.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENDC.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENDC.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENDC.Numero >= ISNULL(@par_Numero_Inicio, ENDC.Numero)                                
 AND ENDC.Numero <= ISNULL(@par_Numero_Fin, ENDC.Numero)     
 AND ENDC.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENDC.Fecha_Crea)                                
 AND ENDC.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENDC.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENDC.Estado = ISNULL(@par_Estado, ENDC.Estado)     
 AND ENDC.Anulado = ISNULL(@par_Anulado, ENDC.Anulado) 
 AND ENDC.CATA_DOOR_Codigo = 2607 --Comprobante Ingreso   
 )    
 SELECT    
 0 AS Obtener,    
 EMPR_Codigo,     
 ISNULL(Numero, 0) AS Numero,    
 ISNULL(Usuario_Crea, '') AS Usuario_Crea,    
 ISNULL(Fecha_Crea, '') AS Fecha_Crea,    
 ISNULL(Usuario_Modifica,'')  AS Usuario_Modifica,    
 ISNULL(Fecha_Modifica, '') AS Fecha_Modifica,    
 ISNULL(Usuario_Anula, '') AS Usuario_Anula,    
 ISNULL(Fecha_Anula, '') AS Fecha_Anula,    
 ISNULL(Causa_Anulacion, '') AS Causa_Anulacion,    
 ISNULL(Nombre_Estado, '') AS Nombre_Estado,    
 @CantidadRegistrosComprobanteIngreso AS TotalRegistros,    
 @par_NumeroPagina AS PaginaObtener,    
 @par_RegistrosPagina AS RegistrosPagina    
 FROM    
  PaginaComprobanteIngreso    
 WHERE    
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosComprobanteIngreso)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosComprobanteIngreso)    
 ORDER BY Numero    
    
END 

ELSE IF @par_Tipo_Consulta = 0 --Comprobante de Egresos   
    
 BEGIN    
    SET NOCOUNT ON;    

	UPDATE Encabezado_Documento_Comprobantes SET Anulado = 0 WHERE Anulado IS NULL
 DECLARE @CantidadRegistrosComprobanteEgresos INT    
 SELECT @CantidadRegistrosComprobanteEgresos = (    
 SELECT DISTINCT     
    
 COUNT(1)     
    
 FROM     
    
 Encabezado_Documento_Comprobantes AS ENDC LEFT JOIN Usuarios AS USCC  ON    
 ENDC.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENDC.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENDC.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENDC.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENDC.Numero >= ISNULL(@par_Numero_Inicio, ENDC.Numero)                                
 AND ENDC.Numero <= ISNULL(@par_Numero_Fin, ENDC.Numero)     
 AND ENDC.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENDC.Fecha_Crea)                                
 AND ENDC.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENDC.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENDC.Estado = ISNULL(@par_Estado, ENDC.Estado)     
 AND ENDC.Anulado = ISNULL(@par_Anulado, ENDC.Anulado) 
 AND ENDC.CATA_DOOR_Codigo = 2606 --Comprobante Egreso
 );    
                
 WITH PaginaComprobanteEgreso AS    
 (    
    
 SELECT     
 0 AS Obtener,    
 ENDC.EMPR_Codigo,    
 ENDC.Numero,     
 USCC.Nombre AS Usuario_Crea,    
 ENDC.Fecha_Crea,    
 USCM.Nombre AS Usuario_Modifica,    
 ENDC.Fecha_Modifica,    
 USCA.Nombre AS Usuario_Anula,    
 ENDC.Fecha_Anula,    
 ENDC.Causa_Anulacion AS Causa_Anulacion,    
 CASE WHEN (ENDC.Estado = 0 AND ENDC.Estado = @par_Estado) OR ENDC.Estado = 0 THEN 'BORRADOR'     
 WHEN (ENDC.Estado = 1 AND ENDC.Anulado = 0 AND ENDC.Estado = @par_Estado ) OR (ENDC.Estado = 1 AND ENDC.Anulado = 0) THEN 'DEFINITIVO'    
 WHEN (@par_Anulado = 1 AND ENDC.Anulado = 1) OR ENDC.Anulado = 1 THEN 'ANULADO' END AS Nombre_Estado,    
 ROW_NUMBER() OVER(ORDER BY ENDC.Numero) AS RowNumber    
  
 FROM     
    
 Encabezado_Documento_Comprobantes AS ENDC LEFT JOIN Usuarios AS USCC  ON    
 ENDC.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENDC.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENDC.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENDC.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENDC.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENDC.Numero >= ISNULL(@par_Numero_Inicio, ENDC.Numero)                                
 AND ENDC.Numero <= ISNULL(@par_Numero_Fin, ENDC.Numero)     
 AND ENDC.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENDC.Fecha_Crea)                                
 AND ENDC.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENDC.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENDC.Estado = ISNULL(@par_Estado, ENDC.Estado)     
 AND ENDC.Anulado = ISNULL(@par_Anulado, ENDC.Anulado)    
 AND ENDC.CATA_DOOR_Codigo = 2606 --Comprobante Egreso 
 )    
 SELECT    
 0 AS Obtener,    
 EMPR_Codigo,     
 ISNULL(Numero, 0) AS Numero,    
 ISNULL(Usuario_Crea, '') AS Usuario_Crea,    
 ISNULL(Fecha_Crea, '') AS Fecha_Crea,    
 ISNULL(Usuario_Modifica,'')  AS Usuario_Modifica,    
 ISNULL(Fecha_Modifica, '') AS Fecha_Modifica,    
 ISNULL(Usuario_Anula, '') AS Usuario_Anula,    
 ISNULL(Fecha_Anula, '') AS Fecha_Anula,    
 ISNULL(Causa_Anulacion, '') AS Causa_Anulacion,    
 ISNULL(Nombre_Estado, '') AS Nombre_Estado,    
 @CantidadRegistrosComprobanteEgresos AS TotalRegistros,    
 @par_NumeroPagina AS PaginaObtener,    
 @par_RegistrosPagina AS RegistrosPagina    
 FROM    
  PaginaComprobanteEgreso    
 WHERE    
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosComprobanteEgresos)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosComprobanteEgresos)    
 ORDER BY Numero    
    
END 

ELSE IF @par_Tipo_Consulta = 2 --Factura Ventas   
    
 BEGIN    
    SET NOCOUNT ON;
	
	UPDATE Encabezado_Facturas SET Anulado = 0 WHERE Anulado IS NULL    
 DECLARE @CantidadRegistrosFacturaVentas INT    
 SELECT @CantidadRegistrosFacturaVentas = (    
 SELECT DISTINCT     
    
 COUNT(1)     
    
 FROM     
    
 Encabezado_Facturas AS ENFA LEFT JOIN Usuarios AS USCC  ON    
 ENFA.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENFA.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENFA.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENFA.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENFA.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENFA.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENFA.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENFA.Numero >= ISNULL(@par_Numero_Inicio, ENFA.Numero)                                
 AND ENFA.Numero <= ISNULL(@par_Numero_Fin, ENFA.Numero)     
 AND ENFA.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENFA.Fecha_Crea)                                
 AND ENFA.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENFA.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENFA.Estado = ISNULL(@par_Estado, ENFA.Estado)     
 AND ENFA.Anulado = ISNULL(@par_Anulado, ENFA.Anulado) 
 );    
                
 WITH PaginaFacturaVentas AS    
 (    
    
 SELECT     
 0 AS Obtener,    
 ENFA.EMPR_Codigo,    
 ENFA.Numero,     
 USCC.Nombre AS Usuario_Crea,    
 ENFA.Fecha_Crea,    
 USCM.Nombre AS Usuario_Modifica,    
 ENFA.Fecha_Modifica,    
 USCA.Nombre AS Usuario_Anula,    
 ENFA.Fecha_Anula,    
 ENFA.Causa_Anula AS Causa_Anulacion,    
 CASE WHEN (ENFA.Estado = 0 AND ENFA.Estado = @par_Estado) OR ENFA.Estado = 0 THEN 'BORRADOR'     
 WHEN (ENFA.Estado = 1 AND ENFA.Anulado = 0 AND ENFA.Estado = @par_Estado ) OR (ENFA.Estado = 1 AND ENFA.Anulado = 0) THEN 'DEFINITIVO'    
 WHEN (@par_Anulado = 1 AND ENFA.Anulado = 1) OR ENFA.Anulado = 1 THEN 'ANULADO' END AS Nombre_Estado,    
 ROW_NUMBER() OVER(ORDER BY ENFA.Numero) AS RowNumber    
  
 FROM     
    
 Encabezado_Facturas AS ENFA LEFT JOIN Usuarios AS USCC  ON    
 ENFA.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENFA.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENFA.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENFA.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENFA.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENFA.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENFA.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENFA.Numero >= ISNULL(@par_Numero_Inicio, ENFA.Numero)                                
 AND ENFA.Numero <= ISNULL(@par_Numero_Fin, ENFA.Numero)     
 AND ENFA.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENFA.Fecha_Crea)                                
 AND ENFA.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENFA.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENFA.Estado = ISNULL(@par_Estado, ENFA.Estado)     
 AND ENFA.Anulado = ISNULL(@par_Anulado, ENFA.Anulado)   
 )    
 SELECT    
 0 AS Obtener,    
 EMPR_Codigo,     
 ISNULL(Numero, 0) AS Numero,    
 ISNULL(Usuario_Crea, '') AS Usuario_Crea,    
 ISNULL(Fecha_Crea, '') AS Fecha_Crea,    
 ISNULL(Usuario_Modifica,'')  AS Usuario_Modifica,    
 ISNULL(Fecha_Modifica, '') AS Fecha_Modifica,    
 ISNULL(Usuario_Anula, '') AS Usuario_Anula,    
 ISNULL(Fecha_Anula, '') AS Fecha_Anula,    
 ISNULL(Causa_Anulacion, '') AS Causa_Anulacion,    
 ISNULL(Nombre_Estado, '') AS Nombre_Estado,    
 @CantidadRegistrosFacturaVentas AS TotalRegistros,    
 @par_NumeroPagina AS PaginaObtener,    
 @par_RegistrosPagina AS RegistrosPagina    
 FROM    
  PaginaFacturaVentas    
 WHERE    
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosFacturaVentas)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosFacturaVentas)    
 ORDER BY Numero    
    
END 

ELSE IF @par_Tipo_Consulta = 5 --Orden de Cargue   
    
 BEGIN    
    SET NOCOUNT ON; 
	
	UPDATE Encabezado_Orden_Cargues SET Anulado = 0 WHERE Anulado IS NULL    
 DECLARE @CantidadRegistrosOrdenCargue INT    
 SELECT @CantidadRegistrosOrdenCargue = (    
 SELECT DISTINCT     
    
 COUNT(1)     
    
 FROM     
    
 Encabezado_Orden_Cargues AS ENOC LEFT JOIN Usuarios AS USCC  ON    
 ENOC.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENOC.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENOC.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENOC.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENOC.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENOC.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENOC.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENOC.Numero >= ISNULL(@par_Numero_Inicio, ENOC.Numero)                                
 AND ENOC.Numero <= ISNULL(@par_Numero_Fin, ENOC.Numero)     
 AND ENOC.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENOC.Fecha_Crea)                                
 AND ENOC.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENOC.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENOC.Estado = ISNULL(@par_Estado, ENOC.Estado)     
 AND ENOC.Anulado = ISNULL(@par_Anulado, ENOC.Anulado) 
 );    
                
 WITH PaginaOrdenCargue AS    
 (    
    
 SELECT     
 0 AS Obtener,    
 ENOC.EMPR_Codigo,    
 ENOC.Numero,     
 USCC.Nombre AS Usuario_Crea,    
 ENOC.Fecha_Crea,    
 USCM.Nombre AS Usuario_Modifica,    
 ENOC.Fecha_Modifica,    
 USCA.Nombre AS Usuario_Anula,    
 ENOC.Fecha_Anula,    
 ENOC.Causa_Anula AS Causa_Anulacion,    
 CASE WHEN (ENOC.Estado = 0 AND ENOC.Estado = @par_Estado) OR ENOC.Estado = 0 THEN 'BORRADOR'     
 WHEN (ENOC.Estado = 1 AND ENOC.Anulado = 0 AND ENOC.Estado = @par_Estado ) OR (ENOC.Estado = 1 AND ENOC.Anulado = 0) THEN 'DEFINITIVO'    
 WHEN (@par_Anulado = 1 AND ENOC.Anulado = 1) OR ENOC.Anulado = 1 THEN 'ANULADO' END AS Nombre_Estado,    
 ROW_NUMBER() OVER(ORDER BY ENOC.Numero) AS RowNumber    
  
 FROM     
    
 Encabezado_Orden_Cargues AS ENOC LEFT JOIN Usuarios AS USCC  ON    
 ENOC.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENOC.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENOC.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENOC.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENOC.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENOC.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENOC.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENOC.Numero >= ISNULL(@par_Numero_Inicio, ENOC.Numero)                                
 AND ENOC.Numero <= ISNULL(@par_Numero_Fin, ENOC.Numero)     
 AND ENOC.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENOC.Fecha_Crea)                                
 AND ENOC.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENOC.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENOC.Estado = ISNULL(@par_Estado, ENOC.Estado)     
 AND ENOC.Anulado = ISNULL(@par_Anulado, ENOC.Anulado)   
 )    
 SELECT    
 0 AS Obtener,    
 EMPR_Codigo,     
 ISNULL(Numero, 0) AS Numero,    
 ISNULL(Usuario_Crea, '') AS Usuario_Crea,    
 ISNULL(Fecha_Crea, '') AS Fecha_Crea,    
 ISNULL(Usuario_Modifica,'')  AS Usuario_Modifica,    
 ISNULL(Fecha_Modifica, '') AS Fecha_Modifica,    
 ISNULL(Usuario_Anula, '') AS Usuario_Anula,    
 ISNULL(Fecha_Anula, '') AS Fecha_Anula,    
 ISNULL(Causa_Anulacion, '') AS Causa_Anulacion,    
 ISNULL(Nombre_Estado, '') AS Nombre_Estado,    
 @CantidadRegistrosOrdenCargue AS TotalRegistros,    
 @par_NumeroPagina AS PaginaObtener,    
 @par_RegistrosPagina AS RegistrosPagina    
 FROM    
  PaginaOrdenCargue    
 WHERE    
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosOrdenCargue)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosOrdenCargue)    
 ORDER BY Numero    
    
END 

ELSE IF @par_Tipo_Consulta = 6 --Orden de Servicio   
    
 BEGIN    
    SET NOCOUNT ON; 	
	   
    UPDATE Encabezado_Solicitud_Orden_Servicios SET Anulado = 0 WHERE Anulado IS NULL  
 DECLARE @CantidadRegistrosOrdenServicio INT    
 SELECT @CantidadRegistrosOrdenServicio = (    
 SELECT DISTINCT     
    
 COUNT(1)     
    
 FROM     
    
 Encabezado_Solicitud_Orden_Servicios AS ESOS LEFT JOIN Usuarios AS USCC  ON    
 ESOS.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ESOS.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ESOS.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ESOS.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ESOS.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ESOS.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ESOS.EMPR_Codigo = @par_EMPR_Codigo      
 AND ESOS.Numero >= ISNULL(@par_Numero_Inicio, ESOS.Numero)                                
 AND ESOS.Numero <= ISNULL(@par_Numero_Fin, ESOS.Numero)     
 AND ESOS.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ESOS.Fecha_Crea)                                
 AND ESOS.Fecha_Crea <= ISNULL(@par_Fecha_Final,ESOS.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ESOS.Estado = ISNULL(@par_Estado, ESOS.Estado)     
 AND ESOS.Anulado = ISNULL(@par_Anulado, ESOS.Anulado) 
 );    
                
 WITH PaginaOrdenServicio AS    
 (    
    
 SELECT     
 0 AS Obtener,    
 ESOS.EMPR_Codigo,    
 ESOS.Numero,     
 USCC.Nombre AS Usuario_Crea,    
 ESOS.Fecha_Crea,    
 USCM.Nombre AS Usuario_Modifica,    
 ESOS.Fecha_Modifica,    
 USCA.Nombre AS Usuario_Anula,    
 ESOS.Fecha_Anula,    
 ESOS.Causa_Anula AS Causa_Anulacion,    
 CASE WHEN (ESOS.Estado = 0 AND ESOS.Estado = @par_Estado) OR ESOS.Estado = 0 THEN 'BORRADOR'     
 WHEN (ESOS.Estado = 1 AND ESOS.Anulado = 0 AND ESOS.Estado = @par_Estado ) OR (ESOS.Estado = 1 AND ESOS.Anulado = 0) THEN 'DEFINITIVO'    
 WHEN (@par_Anulado = 1 AND ESOS.Anulado = 1) OR ESOS.Anulado = 1 THEN 'ANULADO' END AS Nombre_Estado,    
 ROW_NUMBER() OVER(ORDER BY ESOS.Numero) AS RowNumber    
  
 FROM     
    
 Encabezado_Solicitud_Orden_Servicios AS ESOS LEFT JOIN Usuarios AS USCC  ON    
 ESOS.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ESOS.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ESOS.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ESOS.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ESOS.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ESOS.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ESOS.EMPR_Codigo = @par_EMPR_Codigo      
 AND ESOS.Numero >= ISNULL(@par_Numero_Inicio, ESOS.Numero)                                
 AND ESOS.Numero <= ISNULL(@par_Numero_Fin, ESOS.Numero)     
 AND ESOS.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ESOS.Fecha_Crea)                                
 AND ESOS.Fecha_Crea <= ISNULL(@par_Fecha_Final,ESOS.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ESOS.Estado = ISNULL(@par_Estado, ESOS.Estado)     
 AND ESOS.Anulado = ISNULL(@par_Anulado, ESOS.Anulado)   
 )    
 SELECT    
 0 AS Obtener,    
 EMPR_Codigo,     
 ISNULL(Numero, 0) AS Numero,    
 ISNULL(Usuario_Crea, '') AS Usuario_Crea,    
 ISNULL(Fecha_Crea, '') AS Fecha_Crea,    
 ISNULL(Usuario_Modifica,'')  AS Usuario_Modifica,    
 ISNULL(Fecha_Modifica, '') AS Fecha_Modifica,    
 ISNULL(Usuario_Anula, '') AS Usuario_Anula,    
 ISNULL(Fecha_Anula, '') AS Fecha_Anula,    
 ISNULL(Causa_Anulacion, '') AS Causa_Anulacion,    
 ISNULL(Nombre_Estado, '') AS Nombre_Estado,    
 @CantidadRegistrosOrdenServicio AS TotalRegistros,    
 @par_NumeroPagina AS PaginaObtener,    
 @par_RegistrosPagina AS RegistrosPagina    
 FROM    
  PaginaOrdenServicio    
 WHERE    
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosOrdenServicio)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosOrdenServicio)    
 ORDER BY Numero    
    
END 

ELSE IF @par_Tipo_Consulta = 3 --Guías Paquetería   
    
 BEGIN    
    SET NOCOUNT ON;    

	UPDATE Encabezado_Remesas SET Anulado = 0 WHERE Anulado IS NULL 
 DECLARE @CantidadRegistrosGuiasPaqueteria INT    
 SELECT @CantidadRegistrosGuiasPaqueteria = (    
 SELECT DISTINCT     
    
 COUNT(1)     
    
 FROM     
    
 Encabezado_Remesas AS ENRE LEFT JOIN Usuarios AS USCC  ON    
 ENRE.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENRE.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENRE.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENRE.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENRE.Numero >= ISNULL(@par_Numero_Inicio, ENRE.Numero)                                
 AND ENRE.Numero <= ISNULL(@par_Numero_Fin, ENRE.Numero)     
 AND ENRE.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENRE.Fecha_Crea)                                
 AND ENRE.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENRE.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)     
 AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado) 
 AND ENRE.TIDO_Codigo = 110 --Paquetería
 );    
                
 WITH PaginaGuiasPaqueteria AS    
 (    
    
 SELECT     
 0 AS Obtener,    
 ENRE.EMPR_Codigo,    
 ENRE.Numero,     
 USCC.Nombre AS Usuario_Crea,    
 ENRE.Fecha_Crea,    
 USCM.Nombre AS Usuario_Modifica,    
 ENRE.Fecha_Modifica,    
 USCA.Nombre AS Usuario_Anula,    
 ENRE.Fecha_Anula,    
 ENRE.Causa_Anula AS Causa_Anulacion,    
 CASE WHEN (ENRE.Estado = 0 AND ENRE.Estado = @par_Estado) OR ENRE.Estado = 0 THEN 'BORRADOR'     
 WHEN (ENRE.Estado = 1 AND ENRE.Anulado = 0 AND ENRE.Estado = @par_Estado ) OR (ENRE.Estado = 1 AND ENRE.Anulado = 0) THEN 'DEFINITIVO'    
 WHEN (@par_Anulado = 1 AND ENRE.Anulado = 1) OR ENRE.Anulado = 1 THEN 'ANULADO' END AS Nombre_Estado,    
 ROW_NUMBER() OVER(ORDER BY ENRE.Numero) AS RowNumber    
  
 FROM     
    
 Encabezado_Remesas AS ENRE LEFT JOIN Usuarios AS USCC  ON    
 ENRE.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENRE.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENRE.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENRE.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENRE.Numero >= ISNULL(@par_Numero_Inicio, ENRE.Numero)                                
 AND ENRE.Numero <= ISNULL(@par_Numero_Fin, ENRE.Numero)     
 AND ENRE.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENRE.Fecha_Crea)                                
 AND ENRE.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENRE.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)     
 AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)   
 AND ENRE.TIDO_Codigo = 110 --Paquetería
 )    
 SELECT    
 0 AS Obtener,    
 EMPR_Codigo,     
 ISNULL(Numero, 0) AS Numero,    
 ISNULL(Usuario_Crea, '') AS Usuario_Crea,    
 ISNULL(Fecha_Crea, '') AS Fecha_Crea,    
 ISNULL(Usuario_Modifica,'')  AS Usuario_Modifica,    
 ISNULL(Fecha_Modifica, '') AS Fecha_Modifica,    
 ISNULL(Usuario_Anula, '') AS Usuario_Anula,    
 ISNULL(Fecha_Anula, '') AS Fecha_Anula,    
 ISNULL(Causa_Anulacion, '') AS Causa_Anulacion,    
 ISNULL(Nombre_Estado, '') AS Nombre_Estado,    
 @CantidadRegistrosGuiasPaqueteria AS TotalRegistros,    
 @par_NumeroPagina AS PaginaObtener,    
 @par_RegistrosPagina AS RegistrosPagina    
 FROM    
  PaginaGuiasPaqueteria    
 WHERE    
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosGuiasPaqueteria)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosGuiasPaqueteria)    
 ORDER BY Numero    
    
END 

ELSE IF @par_Tipo_Consulta = 7 --Remesas   
    
 BEGIN    
    SET NOCOUNT ON;  
	
	UPDATE Encabezado_Remesas SET Anulado = 0 WHERE Anulado IS NULL   
 DECLARE @CantidadRegistrosRemesas INT    
 SELECT @CantidadRegistrosRemesas = (    
 SELECT DISTINCT     
    
 COUNT(1)     
    
 FROM     
    
 Encabezado_Remesas AS ENRE LEFT JOIN Usuarios AS USCC  ON    
 ENRE.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENRE.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENRE.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENRE.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENRE.Numero >= ISNULL(@par_Numero_Inicio, ENRE.Numero)                                
 AND ENRE.Numero <= ISNULL(@par_Numero_Fin, ENRE.Numero)     
 AND ENRE.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENRE.Fecha_Crea)                                
 AND ENRE.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENRE.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)     
 AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado) 
 AND ENRE.TIDO_Codigo = 100 --Remesas
 );    
                
 WITH PaginaRemesas AS    
 (    
    
 SELECT     
 0 AS Obtener,    
 ENRE.EMPR_Codigo,    
 ENRE.Numero,     
 USCC.Nombre AS Usuario_Crea,    
 ENRE.Fecha_Crea,    
 USCM.Nombre AS Usuario_Modifica,    
 ENRE.Fecha_Modifica,    
 USCA.Nombre AS Usuario_Anula,    
 ENRE.Fecha_Anula,    
 ENRE.Causa_Anula AS Causa_Anulacion,    
 CASE WHEN (ENRE.Estado = 0 AND ENRE.Estado = @par_Estado) OR ENRE.Estado = 0 THEN 'BORRADOR'     
 WHEN (ENRE.Estado = 1 AND ENRE.Anulado = 0 AND ENRE.Estado = @par_Estado ) OR (ENRE.Estado = 1 AND ENRE.Anulado = 0) THEN 'DEFINITIVO'    
 WHEN (@par_Anulado = 1 AND ENRE.Anulado = 1) OR ENRE.Anulado = 1 THEN 'ANULADO' END AS Nombre_Estado,    
 ROW_NUMBER() OVER(ORDER BY ENRE.Numero) AS RowNumber    
  
 FROM     
    
 Encabezado_Remesas AS ENRE LEFT JOIN Usuarios AS USCC  ON    
 ENRE.EMPR_Codigo = USCC.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Crea = USCC.Codigo    
    
 LEFT JOIN Usuarios AS USCM  ON    
 ENRE.EMPR_Codigo = USCM.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Modifica = USCM.Codigo    
    
 LEFT JOIN Usuarios AS USCA  ON    
 ENRE.EMPR_Codigo = USCA.EMPR_Codigo    
 AND ENRE.USUA_Codigo_Anula = USCA.Codigo    
    
 WHERE     
          
 ENRE.EMPR_Codigo = @par_EMPR_Codigo      
 AND ENRE.Numero >= ISNULL(@par_Numero_Inicio, ENRE.Numero)                                
 AND ENRE.Numero <= ISNULL(@par_Numero_Fin, ENRE.Numero)     
 AND ENRE.Fecha_Crea >= ISNULL(@par_Fecha_Inicial,ENRE.Fecha_Crea)                                
 AND ENRE.Fecha_Crea <= ISNULL(@par_Fecha_Final,ENRE.Fecha_Crea)       
 AND (USCC.Nombre = ISNULL(@par_Usuario, USCC.Nombre) OR USCM.Nombre = ISNULL(@par_Usuario, USCM.Nombre) OR USCA.Nombre = ISNULL(@par_Usuario, USCA.Nombre))  
 AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)     
 AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)  
 AND ENRE.TIDO_Codigo = 100 --Remesas 
 )    
 SELECT    
 0 AS Obtener,    
 EMPR_Codigo,     
 ISNULL(Numero, 0) AS Numero,    
 ISNULL(Usuario_Crea, '') AS Usuario_Crea,    
 ISNULL(Fecha_Crea, '') AS Fecha_Crea,    
 ISNULL(Usuario_Modifica,'')  AS Usuario_Modifica,    
 ISNULL(Fecha_Modifica, '') AS Fecha_Modifica,    
 ISNULL(Usuario_Anula, '') AS Usuario_Anula,    
 ISNULL(Fecha_Anula, '') AS Fecha_Anula,    
 ISNULL(Causa_Anulacion, '') AS Causa_Anulacion,    
 ISNULL(Nombre_Estado, '') AS Nombre_Estado,    
 @CantidadRegistrosRemesas AS TotalRegistros,    
 @par_NumeroPagina AS PaginaObtener,    
 @par_RegistrosPagina AS RegistrosPagina    
 FROM    
  PaginaRemesas    
 WHERE    
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosRemesas)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistrosRemesas)    
 ORDER BY Numero    
    
END 

END
GO