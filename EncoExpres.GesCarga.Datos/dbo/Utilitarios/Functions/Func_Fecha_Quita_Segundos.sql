﻿PRINT 'Func_Fecha_Quita_Segundos'
GO
DROP FUNCTION Func_Fecha_Quita_Segundos
GO
CREATE FUNCTION Func_Fecha_Quita_Segundos    
(    
 @par_Fecha DATETIME      
)    
RETURNS DATETIME    
AS    
BEGIN    
 --declare @fechita datetime = NULL   
 IF @par_Fecha  is null  
  BEGIN  
   SET @par_Fecha = null  
  END  
 ELSE  
  BEGIN  
   SET @par_Fecha = DATEADD(MILLISECOND, -(DATEPART(MILLISECOND,@par_Fecha)),@par_Fecha)    
   SET @par_Fecha = DATEADD(SECOND, -(DATEPART(SECOND,@par_Fecha)),@par_Fecha)    
  END    
 RETURN @par_Fecha    
END  
GO