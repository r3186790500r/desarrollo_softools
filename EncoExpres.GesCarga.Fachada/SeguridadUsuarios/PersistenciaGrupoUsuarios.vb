﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Repositorio.SeguridadUsuarios

Namespace SeguridadUsuarios
    Public NotInheritable Class PersistenciaGrupoUsuarios
        Implements IPersistenciaBase(Of GrupoUsuarios)
        Public Function Consultar(filtro As GrupoUsuarios) As IEnumerable(Of GrupoUsuarios) Implements IPersistenciaBase(Of GrupoUsuarios).Consultar
            Return New RepositorioGrupoUsuarios().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As GrupoUsuarios) As Long Implements IPersistenciaBase(Of GrupoUsuarios).Insertar
            Return New RepositorioGrupoUsuarios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As GrupoUsuarios) As Long Implements IPersistenciaBase(Of GrupoUsuarios).Modificar
            Return New RepositorioGrupoUsuarios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As GrupoUsuarios) As GrupoUsuarios Implements IPersistenciaBase(Of GrupoUsuarios).Obtener
            Return New RepositorioGrupoUsuarios().Obtener(filtro)
        End Function
        Public Function Anular(entidad As GrupoUsuarios) As Boolean
            Return New RepositorioGrupoUsuarios().Anular(entidad)
        End Function

    End Class
End Namespace
