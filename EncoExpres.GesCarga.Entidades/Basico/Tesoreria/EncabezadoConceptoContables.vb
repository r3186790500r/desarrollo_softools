﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="EncabezadoConceptoContables"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoConceptoContables
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="EncabezadoConceptoContables"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoConceptoContables"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            DocumentoAplica = Read(lector, "TIDO_Codigo")
            DocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOOR_Codigo"), .Nombre = Read(lector, "DocumentoOrigen")}
            TipoConceptoContable = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICC_Codigo"), .Nombre = Read(lector, "TipoConceptoContable")}
            TipoNaturaleza = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TINA_Codigo")}
            OficinaAplica = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Aplica"), .Nombre = Read(lector, "OficinaAPlica")}
            Observaciones = Read(lector, "Observaciones")
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo")}
            Fuente = Read(lector, "Fuente")
            FuenteAnula = Read(lector, "Fuente_Anulacion")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            Else

            End If
        End Sub

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property OficinaAplica As Oficinas
        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property DocumentoOrigen As ValorCatalogos
        <JsonProperty>
        Public Property TipoConceptoContable As ValorCatalogos
        <JsonProperty>
        Public Property TipoNaturaleza As ValorCatalogos
        <JsonProperty>
        Public Property Fuente As String
        <JsonProperty>
        Public Property FuenteAnula As String
        <JsonProperty>
        Public Property DocumentoAplica As Integer

        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleConceptoContables)
    End Class

End Namespace
