﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioRendimientoGalonCombustible"/>
    ''' </summary>
    Public NotInheritable Class RepositorioRendimientoGalonCombustible
        Inherits RepositorioBase(Of RendimientoGalonCombustible)

        Public Overrides Function Consultar(filtro As RendimientoGalonCombustible) As IEnumerable(Of RendimientoGalonCombustible)
            Dim lista As New List(Of RendimientoGalonCombustible)
            Dim item As RendimientoGalonCombustible

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If Not IsNothing(filtro.TipoVehiculo) Then
                    If filtro.TipoVehiculo.Codigo <> 2200 Then
                        conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", filtro.TipoVehiculo.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Ruta) Then
                    conexion.AgregarParametroSQL("@par_RUTA_Codigo", filtro.Ruta.Codigo)
                End If
                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_Rendimiento_Galon_Combustible]")

                While resultado.Read
                    item = New RendimientoGalonCombustible(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As RendimientoGalonCombustible) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                conexion.AgregarParametroSQL("@par_Peso_Desde", entidad.PesoDesde, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Peso_Hasta", entidad.PesoHasta, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Rendimiento_Galon", entidad.RendimientoGalon, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Rendimiento_Galon_Combustible")

                While resultado.Read
                    If resultado.Item("Codigo").ToString() = "" Then
                        entidad.Codigo = -1
                    Else
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End If

                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As RendimientoGalonCombustible) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                conexion.AgregarParametroSQL("@par_Peso_Desde", entidad.PesoDesde, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Peso_Hasta", entidad.PesoHasta, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Rendimiento_Galon", entidad.RendimientoGalon, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_Rendimiento_Galon_Combustible")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function


        Public Function Actualizar(entidad As RendimientoGalonCombustible) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                If Not IsNothing(entidad.TipoVehiculo) Then
                    If (entidad.TipoVehiculo.Codigo <> 2200) Then
                        conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.Ruta) Then
                    conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Rendimiento_Galon", entidad.RendimientoGalon, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Rendimiento_Galon_Combustible")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As RendimientoGalonCombustible) As RendimientoGalonCombustible
            Dim item As New RendimientoGalonCombustible

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_Rendimiento_Galon_Combustible]")

                While resultado.Read
                    item = New RendimientoGalonCombustible(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As RendimientoGalonCombustible) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_Rendimiento_Galon_Combustible]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace