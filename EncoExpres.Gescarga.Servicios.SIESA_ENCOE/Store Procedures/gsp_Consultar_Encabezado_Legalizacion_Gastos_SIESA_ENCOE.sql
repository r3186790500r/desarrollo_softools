DROP PROCEDURE [dbo].[gsp_Consultar_Encabezado_Legalizacion_Gastos_SIESA_ENCOE]  
GO

CREATE PROCEDURE [dbo].[gsp_Consultar_Encabezado_Legalizacion_Gastos_SIESA_ENCOE]              
(                
 @par_EMPR_Codigo smallint,             
 @par_ELGC_Numero numeric = NULL            
)                
AS                
BEGIN              
              
SELECT TOP 10               
ELGC.EMPR_Codigo,                
ELGC.Numero,                
ELGC.Numero_Documento,                
ELGC.Fecha,                
ELGC.TERC_Codigo_Conductor,    
COND.Numero_Identificacion AS Identificacion_Conductor,    
ELGC.OFIC_Codigo,              
ELGC.Observaciones,   
ELGC.Saldo_Favor_Conductor,  
ELGC.Saldo_Favor_Empresa,
ELGC.Valor_Gastos,
OFIC.Codigo_Alterno AS Codigo_Alterno_Oficina
                 
FROM Encabezado_Legalizacion_Gastos_Conductor ELGC              
    
INNER JOIN Terceros AS COND    
ON ELGC.EMPR_Codigo = COND.EMPR_Codigo    
AND ELGC.TERC_Codigo_Conductor = COND.Codigo    

INNER JOIN Oficinas AS OFIC    
ON ELGC.EMPR_Codigo = OFIC.EMPR_Codigo    
AND ELGC.OFIC_Codigo = OFIC.Codigo

WHERE ELGC.EMPR_Codigo = @par_EMPR_Codigo        
AND (ELGC.Numero = @par_ELGC_Numero OR @par_ELGC_Numero IS NULL)    
AND ISNULL(ELGC.Interfaz_Contable_Legalizacion, 0 ) = 0                
AND ISNULL(ELGC.Intentos_Interfase_Contable, 0 ) < 5                
AND ELGC.Anulado = 0            
AND ELGC.Estado = 1            
           
END 