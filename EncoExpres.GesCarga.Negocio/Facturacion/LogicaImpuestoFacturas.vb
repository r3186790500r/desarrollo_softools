﻿Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Facturacion
    Public NotInheritable Class LogicaImpuestoFacturas
        Inherits LogicaBase(Of ImpuestoFacturas)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of ImpuestoFacturas)

        Sub New(persistencia As IPersistenciaBase(Of ImpuestoFacturas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As ImpuestoFacturas) As Respuesta(Of IEnumerable(Of ImpuestoFacturas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of ImpuestoFacturas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of ImpuestoFacturas))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As ImpuestoFacturas) As Respuesta(Of ImpuestoFacturas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of ImpuestoFacturas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of ImpuestoFacturas)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As ImpuestoFacturas) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
