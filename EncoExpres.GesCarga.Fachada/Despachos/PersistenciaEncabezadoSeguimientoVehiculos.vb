﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Repositorio.Despachos

Namespace Despachos
    Public NotInheritable Class PersistenciaEncabezadoSeguimientoVehiculos
        Implements IPersistenciaBase(Of EncabezadoSeguimientoVehiculos)

        Public Function Consultar(filtro As EncabezadoSeguimientoVehiculos) As IEnumerable(Of EncabezadoSeguimientoVehiculos) Implements IPersistenciaBase(Of EncabezadoSeguimientoVehiculos).Consultar
            Return New RepositorioEncabezadoSeguimientoVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoSeguimientoVehiculos) As Long Implements IPersistenciaBase(Of EncabezadoSeguimientoVehiculos).Insertar
            Return New RepositorioEncabezadoSeguimientoVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoSeguimientoVehiculos) As Long Implements IPersistenciaBase(Of EncabezadoSeguimientoVehiculos).Modificar
            Return New RepositorioEncabezadoSeguimientoVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoSeguimientoVehiculos) As EncabezadoSeguimientoVehiculos Implements IPersistenciaBase(Of EncabezadoSeguimientoVehiculos).Obtener
            Return New RepositorioEncabezadoSeguimientoVehiculos().Obtener(filtro)
        End Function

    End Class

End Namespace

