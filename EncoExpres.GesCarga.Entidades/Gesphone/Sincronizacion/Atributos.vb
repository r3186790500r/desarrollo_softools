﻿Namespace Gesphone

    ''' <summary>
    ''' Clase para la definir por medio de un decorado que campos no se deben mapear desde la base de datos.
    ''' </summary>
    <AttributeUsage(AttributeTargets.Property)>
    Public Class NotMapped
        Inherits Attribute

    End Class

    ''' <summary>
    ''' Clase para definir por medio de un decorado la configuración de campos en la base de datos OffLine
    ''' </summary>
    <AttributeUsage(AttributeTargets.Property)>
    Public Class SchemaOffLine
        Inherits Attribute

        Public Column As String

        Public Value As String

        Public Foreignkey As String

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SchemaOffLine"/>
        ''' </summary>
        ''' <param name="column">Nombre de la columna</param>
        ''' <param name="value">Tipo de dato</param>
        Sub New(column As String, value As String)
            Me.Column = column
            Me.Value = value
            Me.Foreignkey = String.Empty
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SchemaOffLine"/>
        ''' </summary>
        ''' <param name="isforeingKey">Es campo llave Foranea</param>
        ''' <param name="foreingKey">Nombre llave foranea</param>
        ''' <param name="value">Valor llave foranea</param>
        Sub New(isforeingKey As Boolean, foreingKey As String, value As String)
            If isforeingKey = True Then
                Me.Value = value
                Me.Foreignkey = foreingKey
            End If

            Me.Column = String.Empty
        End Sub

    End Class

End Namespace
