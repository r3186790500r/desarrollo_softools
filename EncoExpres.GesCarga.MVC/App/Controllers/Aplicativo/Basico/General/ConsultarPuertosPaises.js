﻿EncoExpresApp.controller("ConsultarPuertosPaisesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'PuertosPaisesFactory', 'blockUI',
    function ($scope, $routeParams, $timeout, $linq, PuertosPaisesFactory, blockUI) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Puertos País' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        var filtros = {};
        $scope.ListadoEstados = [];
        $scope.MostrarMensajeError = false
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PUERTOS_PAISES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };
        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        /*Metodo buscar*/
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarPuertosPaises';
            }
        };
        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoPuertosPaises = [];

            DatosRequeridos();
            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Nombre: $scope.Nombre,
                Pais: { Nombre: $scope.Pais },
                Estado: $scope.ModalEstado.Codigo,
            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                PuertosPaisesFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                response.data.Datos.forEach(function (item) {
                                    if (item.Codigo > 0) {
                                        $scope.ListadoPuertosPaises.push(item);
                                    }
                                });

                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoPuertosPaises = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        function DatosRequeridos() {
            if (filtros.Codigo == undefined) {
                filtros.Codigo = 0;
            }
        }
        /*------------------------------------------------------------------------------------Eliminar Oficinas-----------------------------------------------------------------------*/
        $scope.EliminarPuertosPaises = function (codigo, Nombre) {
            $scope.Codigo = codigo
            $scope.NombrePuertosPaisesEliminada = Nombre
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarPuertosPaises');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            PuertosPaisesFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó el puerto ' + $scope.NombrePuertosPaisesEliminada);
                        closeModal('modalEliminarPuertosPaises');
                        $scope.Codigo = 0;
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeEliminarPuertosPaises');
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'No se puede eliminar el puerto ' + $scope.NombrePuertosPaisesEliminada + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }

        $scope.CerrarModal = function () {
            closeModal('modalEliminarPuertosPaises');
            closeModal('modalMensajeEliminarPuertosPaises');
        }

    }]);