﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Facturacion
Imports EncoExpres.GesCarga.Negocio.Basico.Facturacion

''' <summary>
''' Controlador <see cref="ConceptosFacturacionController"/>
''' </summary>
<Authorize>
Public Class ConceptosFacturacionController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ConceptoFacturacion) As Respuesta(Of IEnumerable(Of ConceptoFacturacion))
        Return New LogicaConceptoFacturacion(CapaPersistenciaConceptosFacturacion).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ConceptoFacturacion) As Respuesta(Of ConceptoFacturacion)
        Return New LogicaConceptoFacturacion(CapaPersistenciaConceptosFacturacion).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ConceptoFacturacion) As Respuesta(Of Long)
        Return New LogicaConceptoFacturacion(CapaPersistenciaConceptosFacturacion).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ConceptoFacturacion) As Respuesta(Of Boolean)
        Return New LogicaConceptoFacturacion(CapaPersistenciaConceptosFacturacion).Anular(entidad)
    End Function

End Class
