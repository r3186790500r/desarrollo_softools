﻿PRINT 'gsp_consultar_terceros'
GO
DROP PROCEDURE gsp_consultar_terceros
GO
CREATE PROCEDURE gsp_consultar_terceros 
  (         
  @par_EMPR_Codigo SMALLINT,                        
  @par_Codigo NUMERIC = NULL,                        
  @par_Numero_Identificacion VARCHAR(15) = NULL ,                        
  @par_Codigo_Alterno VARCHAR(10) = NULL,                        
  @par_Nombre_Tercero VARCHAR(100) = NULL,                        
  @par_Nombre_Ciudad VARCHAR(100) = NULL,                        
  @par_Estado SMALLINT = NULL,                        
  @par_Perfil_Tercero SMALLINT = NULL,                        
  @par_NumeroPagina INT = NULL,                         
  @par_RegistrosPagina INT = NULL                         
  )                        
AS                        
BEGIN                        
  SET NOCOUNT ON;                        
  DECLARE @CantidadRegistros INT                        
                        
  IF @par_Perfil_Tercero IS NULL                        
  BEGIN                        
   SELECT @CantidadRegistros = (                        
    SELECT DISTINCT                         
    COUNT(TERC.Codigo)                         
   FROM                         
     Terceros TERC LEFT JOIN Ciudades CIEX ON                      
     TERC.EMPR_Codigo = CIEX.EMPR_Codigo                      
     AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                      
     ,Ciudades CIUD    
  LEFT JOIN Departamentos DEPA ON  
  CIUD.EMPR_Codigo = DEPA.EMPR_Codigo  
  AND CIUD.DEPA_Codigo = DEPA.Codigo                        
   WHERE                         
      TERC.EMPR_Codigo = CIUD.EMPR_Codigo                        
     AND TERC.CIUD_Codigo = CIUD.Codigo                               
     AND TERC.Codigo > 0                        
     AND TERC.EMPR_Codigo = @par_EMPR_Codigo                        
     AND (TERC.Codigo = @par_Codigo OR  @par_Codigo IS NULL)          
     AND (TERC.Numero_Identificacion = @par_Numero_Identificacion OR @par_Numero_Identificacion IS NULL)                      
     AND ((TERC.Codigo_Alterno = @par_Codigo_Alterno) OR (@par_Codigo_Alterno IS NULL))                        
     AND ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%') OR (@par_Nombre_Tercero IS NULL))                        
     AND ((CIUD.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Ciudad)) + '%') OR (@par_Nombre_Ciudad IS NULL))                        
     AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)               
   );                               
  WITH Pagina AS                        
  (                        
  SELECT 0 As Obtener                        
     ,TERC.EMPR_Codigo                        
     ,TERC.Codigo                        
     ,TERC.Numero_Identificacion                        
     ,TERC.Digito_Chequeo                   
  ,TERC.CATA_TINT_Codigo                
  ,TERC.CATA_TIID_Codigo                
  ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                      
     ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                      
     ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                        
     ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                        
     ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                        
     ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2                        
     ,ISNULL(TERC.Emails,'')AS Emails                      
     ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                        
     ,CIUD.Nombre AS NombreCuidadDireccion    
  ,CONCAT(CIUD.Nombre,' (',DEPA.Nombre,')') AS CiudadDepartamento               
  ,ISNULL(TERC.Barrio,'') AS Barrio                      
     ,ISNULL(TERC.Direccion,'')AS Direccion         
  ,ISNULL(TERC.Codigo_Postal,0) AS Codigo_Postal                            
     ,ISNULL(TERC.Telefonos,'')AS Telefonos                        
     ,ISNULL(TERC.Celulares,'')AS Celulares                   
     ,TERC.Estado                     
  ,TERC.TERC_Codigo_Beneficiario              
     ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                        
     ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno            
     ,ROW_NUMBER() OVER(ORDER BY TERC.Codigo) AS RowNumber                        
                               
   FROM                         
     Terceros TERC LEFT JOIN Ciudades CIEX ON                        
     TERC.EMPR_Codigo = CIEX.EMPR_Codigo                        
     AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo     
  ,Ciudades CIUD   
     LEFT JOIN Departamentos DEPA ON  
  CIUD.EMPR_Codigo = DEPA.EMPR_Codigo  
  AND CIUD.DEPA_Codigo = DEPA.Codigo          
                        
     WHERE    
  TERC.EMPR_Codigo = CIUD.EMPR_Codigo                        
     AND TERC.CIUD_Codigo = CIUD.Codigo                               
     AND TERC.Codigo > 0                        
     AND TERC.EMPR_Codigo = @par_EMPR_Codigo                        
     AND (TERC.Codigo = @par_Codigo OR  @par_Codigo IS NULL)          
     AND (TERC.Numero_Identificacion = @par_Numero_Identificacion OR @par_Numero_Identificacion IS NULL)                      
     AND ((TERC.Codigo_Alterno = @par_Codigo_Alterno) OR (@par_Codigo_Alterno IS NULL))                        
     AND ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%') OR (@par_Nombre_Tercero IS NULL))                        
     AND ((CIUD.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Ciudad)) + '%') OR (@par_Nombre_Ciudad IS NULL))                        
     AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)                        
    )                        
                        
   SELECT DISTINCT                        
    0 As Obtener                        
     ,EMPR_Codigo                        
     ,Codigo                        
     ,Numero_Identificacion                        
     ,Digito_Chequeo                 
  ,CATA_TINT_Codigo                
  ,CATA_TIID_Codigo                
  ,CIUD_Codigo_Expedicion                   
  ,Ciudad_Expedicion                       
     ,Razon_Social                        
     ,Nombre                        
     ,Apellido1                        
     ,Apellido2                        
     ,Emails                        
     ,CIUD_Codigo_Direccion                        
     ,NombreCuidadDireccion        
  ,CiudadDepartamento     
     ,Barrio           
  ,Direccion         
  ,Codigo_Postal                       
     ,Telefonos                      
     ,Celulares                      
     ,Estado                        
     ,NombreEstado                        
     ,Codigo_Alterno                 
  ,ISNULL(TERC_Codigo_Beneficiario,0) AS TERC_Codigo_Beneficiario              
  ,0 AS PAIS_Codigo                   
     ,@CantidadRegistros AS TotalRegistros                        
     ,@par_NumeroPagina AS PaginaObtener                        
     ,@par_RegistrosPagina AS RegistrosPagina                        
   FROM                        
    Pagina                        
   WHERE                        
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)                        
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)                        
   ORDER BY Codigo ASC                        
  END                        
  ELSE                        
  BEGIN                        
   SELECT @CantidadRegistros = (                        
    SELECT DISTINCT                         
    COUNT(TERC.Codigo)                         
    FROM                         
                             
     Terceros TERC LEFT JOIN Ciudades CIEX ON                      
     TERC.EMPR_Codigo = CIEX.EMPR_Codigo                      
     AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                         
     ,Ciudades CIUD                        
  LEFT JOIN Departamentos DEPA ON  
  CIUD.EMPR_Codigo = DEPA.EMPR_Codigo  
  AND CIUD.DEPA_Codigo = DEPA.Codigo    
  ,Perfil_Terceros PETE                
                        
     WHERE    
  TERC.EMPR_Codigo = CIUD.EMPR_Codigo                        
     AND TERC.CIUD_Codigo = CIUD.Codigo                                 
     AND TERC.Codigo > 0                        
     AND TERC.EMPR_Codigo = @par_EMPR_Codigo                        
     AND (TERC.Codigo = @par_Codigo OR  @par_Codigo IS NULL)          
     AND (TERC.Numero_Identificacion = @par_Numero_Identificacion OR @par_Numero_Identificacion IS NULL)              
     AND ((TERC.Codigo_Alterno = @par_Codigo_Alterno) OR (@par_Codigo_Alterno IS NULL))                        
     AND ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%') OR (@par_Nombre_Tercero IS NULL))                        
     AND ((CIUD.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Ciudad)) + '%') OR (@par_Nombre_Ciudad IS NULL))                        
     AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)                        
     AND TERC.EMPR_Codigo = PETE.EMPR_Codigo                        
     AND TERC.Codigo = PETE.TERC_Codigo                        
     AND PETE.Codigo = @par_Perfil_Tercero                        
   );                               
  WITH Pagina AS                        
  (                        
  SELECT 0 As Obtener                        
     ,TERC.EMPR_Codigo                        
     ,TERC.Codigo                        
     ,TERC.Numero_Identificacion                        
     ,TERC.Digito_Chequeo                  
  ,TERC.CATA_TINT_Codigo                
  ,TERC.CATA_TIID_Codigo                 
  ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                     
     ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                        
     ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                        
     ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                        
     ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                        
     ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2                        
     ,ISNULL(TERC.Emails,'') AS Emails                        
     ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                        
     ,CIUD.Nombre AS NombreCuidadDireccion       
  ,CONCAT(CIUD.Nombre,' (',DEPA.Nombre,')') AS CiudadDepartamento                   
     ,ISNULL(TERC.Barrio,'') AS Barrio                      
     ,ISNULL(TERC.Direccion,'')AS Direccion         
  ,ISNULL(TERC.Codigo_Postal, 0) AS Codigo_Postal           
     ,ISNULL(TERC.Telefonos,'') AS Telefonos                 
     ,ISNULL(TERC.Celulares ,'') AS Celulares                     
     ,TERC.Estado                 
 ,TERC.TERC_Codigo_Beneficiario                     
     ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                        
     ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno            
     ,ROW_NUMBER() OVER(ORDER BY TERC.Codigo) AS RowNumber                        
   FROM                         
                             
     Terceros TERC LEFT JOIN Ciudades CIEX ON                      
     TERC.EMPR_Codigo = CIEX.EMPR_Codigo                      
     AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                         
     ,Ciudades CIUD  
  LEFT JOIN Departamentos DEPA ON  
  CIUD.EMPR_Codigo = DEPA.EMPR_Codigo  
  AND CIUD.DEPA_Codigo = DEPA.Codigo                                  
     ,Perfil_Terceros PETE            
              
                        
   WHERE  TERC.EMPR_Codigo = CIUD.EMPR_Codigo                        
     AND TERC.CIUD_Codigo = CIUD.Codigo                              
     AND TERC.Codigo > 0                        
     AND TERC.EMPR_Codigo = @par_EMPR_Codigo                        
     AND (TERC.Codigo = @par_Codigo OR  @par_Codigo IS NULL)          
     AND (TERC.Numero_Identificacion = @par_Numero_Identificacion OR @par_Numero_Identificacion IS NULL)                      
     AND ((TERC.Codigo_Alterno = @par_Codigo_Alterno) OR (@par_Codigo_Alterno IS NULL))                        
     AND ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%') OR (@par_Nombre_Tercero IS NULL))                        
     AND ((CIUD.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Ciudad)) + '%') OR (@par_Nombre_Ciudad IS NULL))                        
     AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)                        
     AND TERC.EMPR_Codigo = PETE.EMPR_Codigo                        
     AND TERC.Codigo = PETE.TERC_Codigo                        
     AND PETE.Codigo = @par_Perfil_Tercero                         
 )                        
                        
   SELECT DISTINCT                        
    0 As Obtener                        
     ,EMPR_Codigo                        
     ,Codigo                        
     ,Numero_Identificacion                        
     ,Digito_Chequeo                  
  ,CATA_TINT_Codigo                
  ,CATA_TIID_Codigo                
  ,CIUD_Codigo_Expedicion                      
  ,Ciudad_Expedicion                     
     ,Razon_Social                        
     ,Nombre                        
     ,Apellido1                        
     ,Apellido2                        
     ,Emails                        
     ,CIUD_Codigo_Direccion                        
     ,NombreCuidadDireccion     
  ,CiudadDepartamento                       
     ,Barrio           
  ,Direccion         
  ,Codigo_Postal                    
     ,Telefonos                       
     ,Celulares                       
     ,Estado                     
  ,ISNULL(TERC_Codigo_Beneficiario,0) AS TERC_Codigo_Beneficiario              
     ,NombreEstado                        
     ,Codigo_Alterno          
  ,0 AS PAIS_Codigo                   
     ,@CantidadRegistros AS TotalRegistros                        
     ,@par_NumeroPagina AS PaginaObtener                        
     ,@par_RegistrosPagina AS RegistrosPagina                        
   FROM                        
    Pagina                        
   WHERE                        
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)                        
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)                        
   ORDER BY Codigo ASC                        
  END                        
END                  
GO