﻿Imports System.Security.Claims
Imports System.Threading.Tasks
Imports Microsoft.Owin.Security.OAuth
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Negocio.Aplicativo.SeguridadUsuarios
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Fachada.SeguridadUsuarios

Namespace EncoExpres.GesCarga.Servicios

    ''' <summary>
    ''' Clase que recibe el login y el password del usuario, los valida y crea el token oAuth
    ''' </summary>
    Public Class ProveedorTokenAutorizacion
        Inherits OAuthAuthorizationServerProvider

        ''' <summary>
        ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para validación de usuario.
        ''' </summary>
        Dim persistenciaUsuario As IPersistenciaBase(Of Usuarios)

        ''' <summary>
        ''' Método para validar que la solicitud tiene un client_id registrado.
        ''' </summary>
        ''' <param name="context">Contexto de la petición</param>
        ''' <returns>Tarea para permitir la ejecución asíncrona</returns>
        Public Overrides Async Function ValidateClientAuthentication(context As OAuthValidateClientAuthenticationContext) As Task
            Await Task.FromResult(context.Validated())
        End Function

        ''' <summary>
        ''' Método que valida el usuario y la contraseña que llegan en la solicitud para generar el token de acceso
        ''' </summary>
        ''' <param name="context">Contexto de la petición</param>
        ''' <returns>Tarea para permitir la ejecución asíncrona</returns>
        Public Overrides Async Function GrantResourceOwnerCredentials(context As OAuthGrantResourceOwnerCredentialsContext) As Task

            '' Habilitar el CORS para que la aplicación resuelva las peticines de acceso de todos los dominios
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", {"*"})

            '' Validación de usuario
            Dim mensajeError As String = String.Empty
            persistenciaUsuario = New PersistenciaUsuarios()

            Dim separadores() As Char = {"|"}
            Dim arrDatos = context.UserName.Split(separadores)
            Dim VarToken = 0

            Dim result = New LogicaUsuarios(persistenciaUsuario).Validar(arrDatos(1), arrDatos(0), context.Password, VarToken)

            If Not (result.ProcesoExitoso) Then
                context.SetError("invalid_grant", result.MensajeOperacion)
            Else
                Dim identity = New ClaimsIdentity(context.Options.AuthenticationType)
                identity.AddClaim(New Claim(ClaimTypes.Name, result.Datos.Nombre))
                identity.AddClaim(New Claim(ClaimTypes.Role, result.Datos.Descripcion))
                identity.AddClaim(New Claim(ClaimTypes.NameIdentifier, result.Datos.CodigoUsuario))
                identity.AddClaim(New Claim(ClaimTypes.Role, "user"))
                context.Validated(identity)
            End If

            Await Task.FromResult(0)

        End Function

    End Class

End Namespace
