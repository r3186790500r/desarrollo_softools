﻿PRINT 'gsp_generar_manifiesto'
GO

DROP PROCEDURE gsp_generar_manifiesto
GO

CREATE PROCEDURE gsp_generar_manifiesto
(
	@par_EMPR_Codigo smallint,
	@par_Numero_Documento_Planilla int,
	--@par_TIDO_Codigo numeric,
	@par_USUA_Codigo_Crea smallint,
	@par_OFIC_Codigo numeric,
	@par_CATA_TIMA_Codigo smallint,
	@par_ESOS_Numero int
)
AS
BEGIN
	DECLARE @NumeroDocumentoManifiesto INT
	DECLARE @NumeroDocumentoRemesa INT
	DECLARE @TipoRemesa INT
	DECLARE @CiudadOrigen INT
	DECLARE @CiudadDestino INT
	DECLARE @NumeroPlanilla INT
	DECLARE @NumeroRemesa INT
	DECLARE @NumeroManifiesto INT


	--Se consulta el número interno de la planilla
	SET @NumeroPlanilla = (
	SELECT
		Numero
	FROM
		Encabezado_Planilla_Despachos
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Numero_Documento = @par_Numero_Documento_Planilla)
	

	--Se consulta el número interno del manifiesto
	SET @NumeroManifiesto = (
	SELECT
		ISNULL(ENMC_Numero,0)
	FROM
		Encabezado_Planilla_Despachos
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Numero = @NumeroPlanilla)


	IF @NumeroManifiesto = 0 BEGIN
		EXEC gsp_generar_consecutivo @par_EMPR_Codigo, 140, @par_OFIC_Codigo, @NumeroDocumentoManifiesto OUTPUT
		INSERT INTO Encabezado_Manifiesto_Carga
		(
			EMPR_Codigo
		   ,TIDO_Codigo
		   ,Numero_Documento
		   ,Fecha
		   ,VEHI_Codigo
		   ,TERC_Codigo_Propietario
		   ,TERC_Codigo_Tenedor
		   ,TERC_Codigo_Conductor
		   ,TERC_Codigo_Segundo_Conductor
		   ,TERC_Codigo_Afiliador
		   ,SEMI_Codigo
		   ,TIVE_Codigo
		   ,RUTA_Codigo
		   ,Observaciones
		   ,Fecha_Cumplimiento
		   ,Cantidad_Total
		   ,Peso_Total
		   ,Valor_Flete
		   ,Valor_Retencion_Fuente
		   ,Valor_ICA
		   ,Valor_Otros_Descuentos
		   ,Valor_Flete_Neto
		   ,Valor_Anticipo
		   ,Valor_Pagar
		   ,TERC_Codigo_Aseguradora
		   ,Numero_Poliza
		   ,Fecha_Vigencia_Poliza
		   ,ELPD_Numero
		   ,ECPD_Numero
		   ,Finalizo_Viaje
		   ,Anulado
		   ,Siniestro
		   ,Estado
		   ,Fecha_Crea
		   ,USUA_Codigo_Crea
		   ,OFIC_Codigo
		   ,CATA_TIMA_Codigo
		   ,Numero_Manifiesto_Electronico
		)
		SELECT
			@par_EMPR_Codigo,
			140,--Manifiesto Carga
			@NumeroDocumentoManifiesto,
			ENPD.Fecha,
			ENPD.VEHI_Codigo,
			VEHI.TERC_Codigo_Propietario,
			ENPD.TERC_Codigo_Tenedor,
			ENPD.TERC_Codigo_Conductor,
			0,--TERC_Codigo_Afiliador,
			VEHI.TERC_Codigo_Afiliador,
			ENPD.SEMI_Codigo,
			VEHI.CATA_TIVE_Codigo,
			ENPD.RUTA_Codigo,
			ENPD.Observaciones,
			DATEADD(DAY,4,ENPD.Fecha),
			ENPD.Cantidad,
			ENPD.Peso,
			ENPD.Valor_Pagar_Transportador,
			0,--Valor_Retencion_Fuente,
			0,--Valor_ICA,
			0,--Valor_Otros_Descuentos,
			ENPD.Valor_Flete_Cliente,
			ENPD.Valor_Anticipo,
			ENPD.Valor_Pagar_Transportador,
			0,--TERC_Codigo_Aseguradora
			0,--Numero_Poliza
			0,--Fecha_Vigencia_Poliza
			0,--ELPD_Numero,
			0,--ECPD_Numero,
			0,--Finalizo_Viaje,
			0,--Anulado
			0,--Siniestro,
			1,--Definitivo,
			GETDATE(),
			@par_USUA_Codigo_Crea,
			@par_OFIC_Codigo,
			@par_CATA_TIMA_Codigo,
			0
		FROM
			Encabezado_Planilla_Despachos ENPD,
			Vehiculos VEHI
		WHERE
			ENPD.EMPR_Codigo = VEHI.EMPR_Codigo
			AND ENPD.VEHI_Codigo = VEHI.Codigo

			AND ENPD.EMPR_Codigo = @par_EMPR_Codigo
			AND ENPD.Numero = @NumeroPlanilla

		----------------------------------------------------------------
		-----------------INSERTAR DETALLE DEL MANIFIESTO----------------
		----------------------------------------------------------------
		SET @NumeroManifiesto = (
		SELECT
			Numero
		FROM
			Encabezado_Manifiesto_Carga
		WHERE
			EMPR_Codigo = @par_EMPR_Codigo
			AND Numero_Documento = @NumeroDocumentoManifiesto)


		INSERT INTO Detalle_Manifiesto_Carga
		(
			EMPR_Codigo,
			ENMC_Numero,
			ENRE_Numero
		)
		SELECT
			EMPR_Codigo,@NumeroManifiesto,Numero
		FROM
			Encabezado_Remesas
		WHERE
			EMPR_Codigo = @par_EMPR_Codigo
			AND ENPD_Numero = @NumeroPlanilla

		----------------------------------------------------------------
		--------------ASOCIAR MANIFIESTO A LA PLANILLA------------------
		----------------------------------------------------------------
		UPDATE
			Encabezado_Planilla_Despachos
		SET
			ENMC_Numero = @NumeroManifiesto
		WHERE
			EMPR_Codigo = @par_EMPR_Codigo
			AND Numero = @NumeroPlanilla

		----------------------------------------------------------------
		--------ASOCIAR MANIFIESTO A LAS REMESAS DE LA PLANILLA---------
		----------------------------------------------------------------
		UPDATE
			Encabezado_Remesas
		SET
			ENMC_Numero = @NumeroManifiesto
		WHERE
			EMPR_Codigo = @par_EMPR_Codigo
			AND ENPD_Numero = @NumeroPlanilla
			AND ENMC_Numero = 0
			AND Anulado = 0
			AND Estado = 1


		--Si @par_ESOS_Numero es mayor que cero es porque se generó el manifiesto a partir de la planilla de la solicitud de servicio,
		--de lo contrario, fue generada desde la planilla de paquetería.

		IF @par_ESOS_Numero > 0 BEGIN
			----------------------------------------------------------------
			--------------ACTUALIZAR DETALLE DEL DESPACHO-------------------
			----------------------------------------------------------------
			UPDATE
				Detalle_Despacho_Orden_Servicios
			SET
				ENMC_Numero = @NumeroManifiesto,
				ENMC_Numero_Documento = @NumeroDocumentoManifiesto
			WHERE
				EMPR_Codigo = @par_EMPR_Codigo
				AND ESOS_Numero = @par_ESOS_Numero
		END
		ELSE BEGIN
			----------------------------------------------------------------
			----------------GENERAR REMESA DE PAQUETERÍA--------------------
			----------------------------------------------------------------
			
			--Se consulta el tipo de ruta para con ello insertar el tipo de remesa 
			SELECT
				@CiudadOrigen = ORIG.Codigo,
				@CiudadDestino = DEST.Codigo
			FROM
				Encabezado_Planilla_Despachos ENPD,
				Rutas RUTA,
				Ciudades ORIG,
				Ciudades DEST
			WHERE
				ENPD.EMPR_Codigo = RUTA.EMPR_Codigo
				AND ENPD.RUTA_Codigo = RUTA.Codigo
				AND RUTA.EMPR_Codigo = ORIG.EMPR_Codigo
				AND RUTA.CIUD_Codigo_Origen = ORIG.Codigo
				AND RUTA.EMPR_Codigo = DEST.EMPR_Codigo
				AND RUTA.CIUD_Codigo_Destino = DEST.Codigo
				AND ENPD.EMPR_Codigo = @par_EMPR_Codigo
				AND ENPD.Numero = @NumeroPlanilla

			IF @CiudadOrigen = @CiudadDestino BEGIN
				SET @TipoRemesa = 8812
			END
			ELSE BEGIN
				SET @TipoRemesa = 8811
			END
			
			EXEC gsp_generar_consecutivo @par_EMPR_Codigo, 100, @par_OFIC_Codigo, @NumeroDocumentoRemesa OUTPUT
			INSERT INTO Encabezado_Remesas
			(
				EMPR_Codigo,
				TIDO_Codigo,
				Numero_Documento,
				CATA_TIRE_Codigo,
				Fecha,
				RUTA_Codigo,
				PRTR_Codigo,
				CATA_FOPR_Codigo,
				TERC_Codigo_Cliente,
				TERC_Codigo_Remitente,
				CIUD_Codigo_Remitente,
				Direccion_Remitente,
				Telefonos_Remitente,
				Observaciones,
				Cantidad_Cliente,
				Peso_Cliente,
				Peso_Volumetrico_Cliente,
				DTCV_Codigo,
				Valor_Flete_Cliente,
				Valor_Manejo_Cliente,
				Valor_Seguro_Cliente,
				Valor_Descuento_Cliente,
				Total_Flete_Cliente,
				Valor_Comercial_Cliente,
				Cantidad_Transportador,
				Peso_Transportador,
				Valor_Flete_Transportador,
				Total_Flete_Transportador,
				TERC_Codigo_Destinatario,
				CIUD_Codigo_Destinatario,
				Direccion_Destinatario,
				Telefonos_Destinatario,
				Anulado,
				Estado,
				Fecha_Crea,
				USUA_Codigo_Crea,
				OFIC_Codigo,
				ETCC_Numero,
				ETCV_Numero,
				ESOS_Numero,
				ENOC_Numero,
				ENPR_Numero,
				ENPD_Numero,
				ENPE_Numero,
				ENMC_Numero,
				ECPD_Numero,
				ENFA_Numero,
				VEHI_Codigo,
				TERC_Codigo_Conductor,
				SEMI_Codigo
			)
			SELECT
				ENPD.EMPR_Codigo,
				100,--Remesa
				@NumeroDocumentoRemesa,
				@TipoRemesa,
				ENPD.Fecha,
				ENPD.RUTA_Codigo,
				12,--Producto
				6101,
				ENRE.TERC_Codigo_Cliente,
				ENRE.TERC_Codigo_Remitente,
				ENRE.CIUD_Codigo_Remitente,
				ENRE.Direccion_Remitente,
				ENRE.Telefonos_Remitente,
				ENPD.Observaciones,
				SUM(ENPD.Cantidad),
				SUM(ENPD.Peso),
				0,
				0,
				SUM(ENPD.Valor_Flete_Cliente),
				0,
				0,
				0,
				SUM(ENPD.Valor_Flete_Cliente),
				0,
				0,
				0,
				SUM(ENPD.Valor_Flete_Transportador),
				SUM(ENPD.Valor_Flete_Transportador),
				ENRE.TERC_Codigo_Destinatario,
				ENRE.CIUD_Codigo_Destinatario,
				ENRE.Direccion_Destinatario,
				ENRE.Telefonos_Destinatario,
				0,
				1,
				GETDATE(),
				@par_USUA_Codigo_Crea,
				@par_OFIC_Codigo,
				ISNULL(ENRE.ETCC_Numero,0),
				ISNULL(ENRE.ETCV_Numero,0),
				ISNULL(ENRE.ESOS_Numero,0),
				ISNULL(ENRE.ENOC_Numero,0),
				ISNULL(ENRE.ENPR_Numero,0),
				@NumeroPlanilla,
				ISNULL(ENRE.ENPE_Numero,0),
				@NumeroManifiesto,
				ISNULL(ENRE.ECPD_Numero,0),
				ISNULL(ENRE.ENFA_Numero,0),
				ENPD.VEHI_Codigo,
				ENPD.TERC_Codigo_Conductor,
				ENPD.SEMI_Codigo
			FROM
				Encabezado_Planilla_Despachos ENPD,
				Encabezado_Remesas ENRE
			WHERE
				ENPD.EMPR_Codigo = ENRE.EMPR_Codigo
				AND ENPD.Numero = ENRE.ENPD_Numero
				
				AND ENPD.EMPR_Codigo = @par_EMPR_Codigo
				AND ENPD.Numero = @NumeroPlanilla
				AND ENRE.Estado = 1
				AND ENRE.Anulado = 0
			GROUP BY
				ENPD.EMPR_Codigo,
				ENPD.Fecha,
				ENPD.RUTA_Codigo,
				ENRE.TERC_Codigo_Cliente,
				ENRE.TERC_Codigo_Remitente,
				ENRE.CIUD_Codigo_Remitente,
				ENRE.Direccion_Remitente,
				ENRE.Telefonos_Remitente,
				ENPD.Observaciones,
				ENRE.TERC_Codigo_Destinatario,
				ENRE.CIUD_Codigo_Destinatario,
				ENRE.Direccion_Destinatario,
				ENRE.Telefonos_Destinatario,
				ENPD.VEHI_Codigo,
				ENRE.ETCC_Numero,
				ENRE.ETCV_Numero,
				ENRE.ESOS_Numero,
				ENRE.ENOC_Numero,
				ENRE.ENPR_Numero,
				ENRE.ENPE_Numero,
				ENRE.ECPD_Numero,
				ENRE.ENFA_Numero,
				ENPD.VEHI_Codigo,
				ENPD.TERC_Codigo_Conductor,
				ENPD.SEMI_Codigo
		END
		
		----------------------------------------------------------------
		--------SE RETORNA EL CONSECUTIVO GENERADO DEL MANIFIESTO-------
		----------------------------------------------------------------
		SELECT  @NumeroDocumentoManifiesto AS Manifiesto

	END
	ELSE
	BEGIN
		SELECT -1 AS Manifiesto
	END
END
GO