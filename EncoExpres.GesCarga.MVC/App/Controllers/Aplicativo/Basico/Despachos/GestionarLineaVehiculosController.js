﻿EncoExpresApp.controller("GestionarLineaVehiculosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'LineaVehiculosFactory', 'MarcaVehiculosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, LineaVehiculosFactory, MarcaVehiculosFactory) {

    $scope.Titulo = 'GESTIONAR LINEAS VEHÍCULOS';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Lineas Vechículos' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LINEA_VEHICULOS);

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LINEA_VEHICULOS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.ListadoMarcas = [];
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0
    }

    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVA" },
        { Codigo: 1, Nombre: "ACTIVA" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }
    $scope.LongitudMarca = 0;// verifica la cantidad de datos ingresados  de la Marca
    $scope.IngresoValidaMarca = true
    $scope.CodigoMarca = 0;

    $scope.AsignarMarca = function (Marca) {
        if (Marca !== undefined || Marca !== null) {
            if (angular.isObject(Marca)) {
                $scope.LongitudMarca = Marca.length;
                $scope.IngresoValidaMarca = true;
            }
            else if (angular.isString(Marca)) {
                $scope.LongitudMarca = Marca.length;
                $scope.IngresoValidaMarca = false;
            }
        }
    };

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*Cargar Autocomplete de Marcas*/
    MarcaVehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    response.data.Datos.push({ Nombre: '', Codigo: 0 })
                    $scope.ListadoMarcas = response.data.Datos;

                    if ($scope.Marca > 0) {
                        $scope.Modelo.Marca = $linq.Enumerable().From($scope.ListadoMarcas).First('$.Codigo ==' + $scope.Marca);
                    } else {
                        $scope.Modelo.Marca = $scope.ListadoMarcas[$scope.ListadoMarcas.length - 1];
                    }
                }
                else {
                    $scope.ListadoMarcas = [];
                    $scope.Modelo.Marca = null;
                }
            }
        }, function (response) {
            ShowError(response.statusText);
        });

    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando linea vehículo código ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando linea vehículo Código ' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        LineaVehiculosFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.Modelo.Codigo = response.data.Datos.Codigo;
                    $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                    $scope.Modelo.Nombre = response.data.Datos.Nombre;
                    $scope.Modelo.Marca = response.data.Datos.Nombre;

                    $scope.Marca = response.data.Datos.Marca.Codigo;
                    if ($scope.ListadoMarcas.length > 0 && $scope.Marca > 0) {
                        $scope.Modelo.Marca = $linq.Enumerable().From($scope.ListadoMarcas).First('$.Codigo ==' + response.data.Datos.Marca.Codigo);
                    }


                    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                }
                else {
                    ShowError('No se logro consultar la linea vehículo código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarLineaVehiculos';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarLineaVehiculos';
            });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {

            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
            LineaVehiculosFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo === 0) {
                                ShowSuccess('Se guardó la linea vehículo "' + $scope.Modelo.Nombre + '"');
                                location.href = '#!ConsultarLineaVehiculos/' + response.data.Datos;
                            }
                            else {
                                ShowSuccess('Se modificó la linea vehículo "' + $scope.Modelo.Nombre + '"');
                                location.href = '#!ConsultarLineaVehiculos/' + $scope.Modelo.Codigo;
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    if (response.statusText.includes('IX_Linea_Vehiculos_Nombre')) {
                        ShowError('No se pudo guardar, el nombre ingresado ya está siendo utilizado')
                    }
                    else if (response.statusText.includes('IX_Linea_Vehiculos_Codigo_Alterno')) {
                        ShowError('No se pudo guardar, el código alterno ingresado ya está siendo utilizado')
                    } else {
                        ShowError(response.statusText);
                    }
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.Nombre === undefined || $scope.Modelo.Nombre === '') {
            $scope.MensajesError.push('Debe ingresar el nombre de la linea vehículo');
            continuar = false;
        }
        if ($scope.Modelo.Marca === undefined || $scope.Modelo.Marca.Nombre === '' || $scope.IngresoValidaMarca === false) {
            $scope.MensajesError.push('Debe ingresar la marca de la linea vehículo');
            continuar = false;
        }
        return continuar;
    }

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarLineaVehiculos/' + $scope.Modelo.Codigo;
    };
    $scope.MaskMayus = function () {
        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskNumero = function (option) {
        MascaraNumeroGeneral($scope)
    };
}]);