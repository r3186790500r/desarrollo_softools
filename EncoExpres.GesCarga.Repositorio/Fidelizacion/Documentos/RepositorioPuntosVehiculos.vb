﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Fidelizacion.Documentos

Namespace Fidelizacion.Documentos
    Public NotInheritable Class RepositorioPuntosVehiculos
        Inherits RepositorioBase(Of PuntosVehiculos)

        Public Overrides Function Consultar(filtro As PuntosVehiculos) As IEnumerable(Of PuntosVehiculos)
            Dim lista As New List(Of PuntosVehiculos)
            Dim item As PuntosVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Vehiculo) Then
                    If filtro.Vehiculo.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Vehi_Codigo", filtro.Vehiculo.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Tenedor) Then
                    If filtro.Tenedor.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TENE_Codigo", filtro.Tenedor.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Conductor) Then
                    If filtro.Conductor.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_COND_Codigo", filtro.Conductor.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CATA_CFTR) Then
                    If filtro.CATA_CFTR.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Cata_CFTR", filtro.CATA_CFTR.Codigo)
                    End If
                End If

                'If filtro.Fecha <> Date.MinValue Then
                '    conexion.AgregarParametroSQL("@par_Fecha", filtro.Fecha, SqlDbType.Date)
                'End If

                If filtro.Estado >= Cero Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_fidelizacion_puntos_vehiculos")

                While resultado.Read
                    item = New PuntosVehiculos(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As PuntosVehiculos) As Long
            Dim inserto As Boolean = False

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.CleanParameters()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Vehi_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_Cata_CFTR", entidad.CATA_CFTR.Codigo)
                    conexion.AgregarParametroSQL("@par_TotalPuntos", entidad.TotalPuntos)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Fidelizacion_Plan_Puntos_Vehiculos")

                    While resultado.Read
                        entidad.Vehiculo.Codigo = resultado.Item("Vehi_Codigo").ToString()
                        inserto = True
                    End While
                    resultado.Close()

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Vehiculo.Codigo = Cero
                End If

            End Using

            Return entidad.Vehiculo.Codigo
        End Function

        Public Overrides Function Modificar(entidad As PuntosVehiculos) As Long
            Dim Modifico As Boolean = False
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Vehi_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_Cata_CFTR", entidad.CATA_CFTR.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modifica_Fidelizacion_Plan_Puntos_Vehiculos")

                    While resultado.Read
                        entidad.Vehiculo.Codigo = resultado.Item("Vehi_Codigo").ToString()
                        Modifico = True
                    End While
                    resultado.Close()
                End Using

                If Modifico Then
                    transaccion.Complete()
                Else
                    entidad.Vehiculo.Codigo = Cero
                End If

            End Using

            Return entidad.Vehiculo.Codigo
        End Function

        Public Overrides Function Obtener(filtro As PuntosVehiculos) As PuntosVehiculos
            Dim item As New PuntosVehiculos
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Vehi_Codigo", filtro.Vehiculo.Codigo)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_fidelizacion_puntos_vehiculos")

                While resultado.Read
                    item = New PuntosVehiculos(resultado)
                End While

            End Using
            Return item
        End Function
    End Class
End Namespace

