﻿PRINT 'gsp_eliminar_tercero_cliente_condiciones_peso'
GO
DROP PROCEDURE gsp_eliminar_tercero_cliente_condiciones_peso
GO
CREATE PROCEDURE gsp_eliminar_tercero_cliente_condiciones_peso
(  
@par_EMPR_Codigo SMALLINT,      
@par_TERC_Codigo NUMERIC  
)      
AS      
BEGIN    
DELETE Tercero_Clientes_Condiciones_Peso_Cumplido WHERE EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo = @par_TERC_Codigo    
END