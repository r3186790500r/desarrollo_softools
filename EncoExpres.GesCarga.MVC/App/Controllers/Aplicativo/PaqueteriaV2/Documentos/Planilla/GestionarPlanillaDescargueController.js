﻿EncoExpresApp.controller("GestionarPlanillaDescargueCtrl", ['$scope', '$linq', '$timeout', 'blockUI', 'blockUIConfig', 'ValidacionPlanillaDespachosFactory', '$routeParams', 'PlanillaDespachosFactory', 'PlanillaGuiasFactory', 'RemesaGuiasFactory', 'PlanillaPaqueteriaFactory', 'VehiculosFactory', 'OficinasFactory', 'TercerosFactory', 'SemirremolquesFactory', 'CiudadesFactory',
    function ($scope, $linq, $timeout, blockUI, blockUIConfig, ValidacionPlanillaDespachosFactory, $routeParams, PlanillaDespachosFactory, PlanillaGuiasFactory, RemesaGuiasFactory, PlanillaPaqueteriaFactory, VehiculosFactory, OficinasFactory, TercerosFactory, SemirremolquesFactory, CiudadesFactory) {
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Planilla  Descargue' }, { Nombre: 'Gestionar' }];

        //Declaración Variables:

        $scope.PlacaVehiculo = '';
        $scope.NombreConductor = '';
        $scope.NombreTenedor = '';
        $scope.NombreRuta = '';
        $scope.PlacaSemirremolque = '';
        $scope.MostrarSeccionGuias = false;
        $scope.Planilla = [];
        $scope.ListadoGuias = [];
        $scope.ListadoGuiasPaqueteria = [];
        $scope.ListadoGuiasDescargadas = [];
        $scope.ListadoGuiasRechazadas = [];
        $scope.ListadoGuiasActuales = [];
        $scope.ListadoPlanillasDespacho = [];
        $scope.MensajesError = [];
        $scope.MensajesConfirmacion = [];
        $scope.NumeroPlanilla = '';
        $scope.NumeroDocumentoPlanilla = '';
        $scope.Observaciones = '';
        $scope.Cantidad = '';
        $scope.Peso = 0;
        $scope.CantidadGuias = 0
        $scope.CodigoPlanilla = '';
        $scope.DeshabilitarActualizar = false
        $scope.modalUnidadesRemesas = []
        var tmparr = [];
        $scope.totalUnidades = 0
        $scope.IngresadaActual = 0
        $scope.Deshabilitar = false;
        $scope.Fecha = new Date();
        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: 'BORRADOR' },
            { Codigo: 1, Nombre: 'DEFINITIVO' }
        ];
        $scope.Planilla = '';
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==0');
        $scope.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas;
        //Métodos:

        $scope.AutocompleteVehiculos = function (value) {
            $scope.ListaVehiculos = [];
            var Lista = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true, Estado: ESTADO_ACTIVO }).Datos;
            return Lista;
        }

        $scope.AutocompleteDespachos = function (value) {
            $scope.ListaDespachos = [];
            var existe = false
            $scope.ListadoPlanillasDespacho.forEach(itemDespacho => {
                if (itemDespacho.NumeroPrecintoPaqueteria = 6010) {
                    existe = false
                    var numero = ''
                    numero = itemDespacho.Planilla.NumeroDocumento
                    if (numero.toString().includes(value)) {
                        var filtro = {
                            Numero: numero,
                            Planilla: itemDespacho.Planilla.Numero
                        }
                        $scope.ListaDespachos.forEach(despacho => {
                            if (despacho.Numero == numero) {
                                existe = true
                            }

                        });

                        if (!existe) {
                            $scope.ListaDespachos.push(filtro)
                        }

                    }
                }

            });
            var Lista = $scope.ListaDespachos
            return Lista
        }

        $scope.ObtenerVehiculo = function () {
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            $scope.MostrarSeccionGuias = false;
            if ($scope.PlacaVehiculo.Placa.length > 3) {
                if ($scope.PlacaVehiculo != undefined && $scope.PlacaVehiculo != null && $scope.PlacaVehiculo != '') {

                    var ResponseVehiculo = '';

                    ResponseVehiculo = $scope.CargarVehiculosPlaca($scope.PlacaVehiculo.Placa);

                    if (ResponseVehiculo != undefined && ResponseVehiculo != '') {

                        var ResponsListaNegra = VehiculosFactory.ConsultarEstadoListaNegra({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: ResponseVehiculo.Codigo,
                            Sync: true
                        });

                        if (ResponsListaNegra.ProcesoExitoso == false) {
                            if (VehiculosFactory.ConsultarDocumentosSoatRTM({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Codigo: ResponseVehiculo.Codigo,
                                Sync: true
                            }).ProcesoExitoso) {
                                if (ResponseVehiculo.Estado.Codigo == 0) {
                                    ShowError("El vehículo " + ResponseVehiculo.Placa + " esta inactivo por motivo '" + ResponseVehiculo.JustificacionBloqueo + "'");
                                }

                            }
                            else {
                                ShowError("El vehículo tiene documentos proximos a vencerse");
                                ResponseVehiculo = '';
                            }
                        }
                        else {
                            ShowError("El vehículo se encuentra mal matriculado ante el RNDC");
                            ResponseVehiculo = '';
                        }


                        $scope.Semiremolque = ResponseVehiculo.Semirremolque.Placa
                        $scope.NombreConductor = $scope.CargarTercero(ResponseVehiculo.Conductor.Codigo);

                        $scope.DocumentoConductor = ResponseVehiculo.Conductor.Identificacion;

                        $scope.NombreTenedor = $scope.CargarTercero(ResponseVehiculo.Tenedor.Codigo).NombreCompleto;

                        $scope.MostrarSeccionGuias = true;
                    } else {

                        $scope.MensajesError.push('El vehículo ingresado no es válido');
                        $("#window").animate({
                            scrollTop: $('.breadcrumb').offset().top
                        }, 200);
                    }

                }
            }
        }

        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)

                }
            }
            return $scope.ListadoConductores
        }

        SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSemirremolques = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoSemirremolques = response.data.Datos;
                        try {
                            if ($scope.Modelo.Semirremolque.Codigo > 0) {
                                $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListadoSemirremolques).First('$.Codigo ==' + $scope.Semirremolque.Codigo);
                            }
                        } catch (e) {
                            $scope.Semirremolque = ''
                        }
                    }
                    else {
                        $scope.ListadoSemirremolques = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ObternerVehiculo = function (CodigoAlternoVehiculo) {
            if (CodigoAlternoVehiculo != "" && CodigoAlternoVehiculo != undefined && CodigoAlternoVehiculo != null) {
                VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, CodigoAlterno: CodigoAlternoVehiculo }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                //$scope.Modelo.Planilla.Vehiculo = response.data.Datos[0];
                                $scope.PlacaVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos[0].Codigo, Sync: true }).Datos;

                            }
                            else {
                                $scope.CodigoAlternoVehiculo = "";
                            }
                        }
                        else {
                            $scope.CodigoAlternoVehiculo = "";
                        }
                    }, function (response) {
                    });
            }
        };

        $scope.verificarDescargas = function (Guia) {
            var descargadas = Guia.listaUnidades.length
            Guia.Pendientes = Guia.NumeroUnidad - descargadas

            if (Guia.Pendientes == 0) {
                var indice = $scope.ListadoGuiasPaqueteria.indexOf(Guia);
                $scope.ListadoGuiasPaqueteria.splice(indice, 1);
                $scope.ListadoGuiasDescargadas.push(Guia)
            }

        }

        $scope.ValidarUnidad = function () {
            $scope.MensajesError = [];
            if (($scope.UnidadGuia != undefined && $scope.UnidadGuia != null && $scope.UnidadGuia != '' && !isNaN($scope.UnidadGuia)) &&
                ($scope.NumeroGuia != undefined && $scope.NumeroGuia != null && $scope.NumeroGuia != '' && !isNaN($scope.NumeroGuia))) {
                var Etiqueta = $scope.NumeroGuia
                var Existe = false
                var inserto = false

                var response = RemesaGuiasFactory.ConsultarDetalleUnidadesDescargue({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroInicial: $scope.UnidadGuia, NumeroDocumento: $scope.NumeroGuia, Estado: ESTADO_ACTIVO, Sync: true, NumeroPlanilla: -1 });
                console.log('Unidad', response)
                if (response.ProcesoExitoso === true) {
                    var guia = response.Datos[0]
                    if (guia != undefined && guia.ENRE_Numero != '') {
                        $scope.ListadoGuiasPaqueteria.forEach(itemGuia => {
                            if (itemGuia.NumeroDocumentoRemesa == Etiqueta) {

                                itemGuia.listaUnidades.forEach(itemUnidad => {
                                    if (itemUnidad.NumeroUnidad == $scope.UnidadGuia) {
                                        Existe = true
                                        inserto = true
                                        $scope.MensajesError.push('La unidad ingresada ya se encuentra en la planilla')
                                        $("#window").animate({
                                            scrollTop: $('.breadcrumb').offset().top
                                        }, 200);
                                    }

                                });

                                if (!Existe) {
                                    itemGuia.listaUnidades.push({
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        NumeroRemesa: itemGuia.NumeroRemesa,
                                        NumeroDocumentoRemesa: itemGuia.NumeroDocumentoRemesa,
                                        NumeroUnidad: $scope.UnidadGuia,
                                        Peso: itemGuia.Peso,
                                        PesoVolumetrico: itemGuia.PesoVolumetrico,
                                        UnidadesPendientes: itemGuia.UnidadesPendientes,
                                        UnidadesActuales: itemGuia.UnidadesActuales,
                                        ObservacionesVerificacion: '',
                                        CiudadDestino: itemGuia.CiudadDestino,
                                        faltaUnidad: false,
                                        st: ''
                                    });
                                    itemGuia.verUnidaes = true
                                    $scope.Peso = itemGuia.Peso
                                    inserto = true
                                    $scope.IngresadaActual = $scope.NumeroGuia;
                                    $scope.NumeroEtiqueta = ''
                                    $scope.UnidadGuia = '';
                                    $scope.CalcularTotales();
                                    $scope.verificarDescargas(itemGuia)
                                }

                            }

                        });

                        if (!inserto) {
                            $scope.MensajesError.push('La unidad ingresada no existe en ninguna de las guias de la planilla')

                            if ($scope.ListadoGuiasRechazadas.length > 0) {
                                inserto = false
                                $scope.ListadoGuiasRechazadas.forEach(itemGuia => {

                                    if (itemGuia.NumeroDocumentoRemesa == Etiqueta) {

                                        itemGuia.listaUnidades.forEach(itemUnidad => {
                                            if (itemUnidad.NumeroUnidad == $scope.NumeroEtiqueta) {
                                                Existe = true
                                                inserto = true
                                                $scope.MensajesError.push('La Etiqueta ingresada ya se encuentra en la planilla')
                                                $("#window").animate({
                                                    scrollTop: $('.breadcrumb').offset().top
                                                }, 200);
                                            }

                                        });

                                        if (!Existe) {
                                            itemGuia.NumeroUnidad = itemGuia.NumeroUnidad + 1
                                            itemGuia.listaUnidades.push({
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                NumeroRemesa: guia.ENRE_Numero,
                                                NumeroDocumentoRemesa: Etiqueta,
                                                NumeroUnidad: guia.NumeroUnidad,
                                                Peso: 0,
                                                PesoVolumetrico: 0,
                                                UnidadesPendientes: 0,
                                                UnidadesActuales: 1,
                                                ObservacionesVerificacion: 'Unidad Rechazada',
                                                CiudadDestino: guia.CiudadDestinatario,
                                                faltaUnidad: false,
                                                st: ''
                                            });
                                            itemGuia.verUnidaes = true
                                            inserto = true
                                            $scope.IngresadaActual = $scope.NumeroGuia;
                                            $scope.NumeroEtiqueta = ''
                                            $scope.NumeroGuia = '';
                                        }

                                    }
                                });

                                if (!inserto) {
                                    $scope.ListadoGuiasRechazadas.push({
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        NumeroRemesa: guia.ENRE_Numero,
                                        NumeroDocumentoRemesa: Etiqueta,
                                        Peso: 0,
                                        ObservacionesVerificacion: 'RECHAZADA: Se rechazo en la planilla de descargue',
                                        CiudadDestino: guia.CiudadDestinatario,
                                        NumeroUnidad: guia.NumeroUnidad,
                                        verUnidaes: true,
                                        faltaUnidad: false,
                                        st: '',
                                        listaUnidades: []
                                    });

                                    $scope.ListadoGuiasRechazadas.forEach(itemGuia => {

                                        if (itemGuia.NumeroDocumentoRemesa == Etiqueta) {

                                            itemGuia.listaUnidades.push({
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                NumeroRemesa: guia.ENRE_Numero,
                                                NumeroDocumentoRemesa: Etiqueta,
                                                NumeroUnidad: guia.NumeroUnidad,
                                                Peso: 0,
                                                PesoVolumetrico: 0,
                                                UnidadesPendientes: 0,
                                                UnidadesActuales: 0,
                                                ObservacionesVerificacion: 'Unidad Rechazada',
                                                CiudadDestino: guia.CiudadDestinatario,
                                                faltaUnidad: false,
                                                st: ''
                                            });
                                        }
                                    });
                                }

                            } else {

                                $scope.ListadoGuiasRechazadas.push({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    NumeroRemesa: guia.ENRE_Numero,
                                    NumeroDocumentoRemesa: Etiqueta,
                                    Peso: 0,
                                    ObservacionesVerificacion: 'RECHAZADA: Se rechazo en la planilla de descargue',
                                    CiudadDestino: guia.CiudadDestinatario,
                                    NumeroUnidad: 0,
                                    verUnidaes: true,
                                    faltaUnidad: false,
                                    st: '',
                                    listaUnidades: []
                                });

                                $scope.ListadoGuiasRechazadas.forEach(itemGuia => {

                                    itemGuia.listaUnidades.push({
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        NumeroRemesa: guia.ENRE_Numero,
                                        NumeroDocumentoRemesa: Etiqueta,
                                        NumeroUnidad: guia.NumeroUnidad,
                                        Peso: 0,
                                        PesoVolumetrico: 0,
                                        UnidadesPendientes: 0,
                                        UnidadesActuales: 0,
                                        ObservacionesVerificacion: 'Unidad Rechazada',
                                        CiudadDestino: guia.CiudadDestinatario,
                                        faltaUnidad: false,
                                        st: ''
                                    });

                                });
                            }

                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                        }
                    } else {
                        $scope.MensajesError.push('La unidad ingresada no existe')
                        $("#window").animate({
                            scrollTop: $('.breadcrumb').offset().top
                        }, 200);
                    }

                    
                } else {
                    $scope.MensajesError.push('La unidad ingresada no existe')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }
            } else {

            }
            $scope.CalcularTotales();
            $scope.MostrarPendientes()
        }

        $scope.ValidarEtiqueta = function () {
            $scope.MensajesError = [];
            if ($scope.NumeroEtiqueta != undefined && $scope.NumeroEtiqueta != null &&
                $scope.NumeroEtiqueta != '' && !isNaN($scope.NumeroEtiqueta)) {
                var Etiqueta = ''
                var Existe = false
                var inserto = false
                Etiqueta = RemesaGuiasFactory.ConsultarGuiasEtiquetasPreimpresas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroInicial: $scope.NumeroEtiqueta, Estado: ESTADO_ACTIVO, Sync: true, NumeroPlanilla: -1 }).Datos[0];
                console.log('Etiqueta', Etiqueta)
                if (Etiqueta != '' && Etiqueta != undefined) {
                    $scope.ListadoGuiasPaqueteria.forEach(itemGuia => {
                        if (itemGuia.NumeroRemesa == Etiqueta.Remesa.Numero) {

                            itemGuia.listaUnidades.forEach(itemUnidad => {
                                if (itemUnidad.NumeroUnidad == $scope.NumeroEtiqueta) {
                                    Existe = true
                                    inserto = true
                                    $scope.MensajesError.push('La Etiqueta ingresada ya se encuentra en la planilla')
                                    $("#window").animate({
                                        scrollTop: $('.breadcrumb').offset().top
                                    }, 200);
                                }

                            });

                            if (!Existe) {
                                itemGuia.listaUnidades.push({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    NumeroRemesa: Etiqueta.Remesa.Numero,
                                    NumeroDocumentoRemesa: Etiqueta.Remesa.NumeroDocumento,
                                    NumeroUnidad: $scope.NumeroEtiqueta,
                                    Peso: Etiqueta.Remesa.PesoCliente / Etiqueta.Remesa.CantidadCliente,
                                    PesoVolumetrico: Etiqueta.Remesa.PesoVolumetricoCliente / Etiqueta.Remesa.CantidadCliente,
                                    UnidadesPendientes: Etiqueta.Remesa.CantidadCliente,
                                    UnidadesActuales: Etiqueta.Remesa.CantidadCliente,
                                    ObservacionesVerificacion: '',
                                    CiudadDestino: Etiqueta.Remesa.CiudadDestinatario.Nombre,
                                    faltaUnidad: false,
                                    st: ''
                                });
                                itemGuia.verUnidaes = true
                                $scope.Peso = Etiqueta.Remesa.PesoCliente
                                inserto = true
                                $scope.IngresadaActual = $scope.NumeroGuia;
                                $scope.NumeroEtiqueta = ''
                                $scope.NumeroGuia = '';
                                $scope.CalcularTotales();
                                $scope.verificarDescargas(itemGuia)
                            }

                        }

                    });
                    
                    if (!inserto) {
                        $scope.MensajesError.push('La etiqueta ingresada no existe en ninguna de las guias de la planilla')

                        if ($scope.ListadoGuiasRechazadas.length > 0) {
                            inserto = false
                            $scope.ListadoGuiasRechazadas.forEach(itemGuia => {
                                
                                if (itemGuia.NumeroRemesa == Etiqueta.Remesa.Numero) {

                                    itemGuia.listaUnidades.forEach(itemUnidad => {
                                        if (itemUnidad.NumeroUnidad == $scope.NumeroEtiqueta) {
                                            Existe = true
                                            inserto = true
                                            $scope.MensajesError.push('La Etiqueta ingresada ya se encuentra en la planilla')
                                            $("#window").animate({
                                                scrollTop: $('.breadcrumb').offset().top
                                            }, 200);
                                        }

                                    });

                                    if (!Existe) {
                                        itemGuia.NumeroUnidad = itemGuia.NumeroUnidad + 1
                                        itemGuia.listaUnidades.push({
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            NumeroRemesa: Etiqueta.Remesa.Numero,
                                            NumeroDocumentoRemesa: Etiqueta.Remesa.NumeroDocumento,
                                            NumeroUnidad: $scope.NumeroEtiqueta,
                                            Peso: Etiqueta.Remesa.PesoCliente / Etiqueta.Remesa.CantidadCliente,
                                            PesoVolumetrico: Etiqueta.Remesa.PesoVolumetricoCliente / Etiqueta.Remesa.CantidadCliente,
                                            UnidadesPendientes: Etiqueta.Remesa.CantidadCliente,
                                            UnidadesActuales: Etiqueta.Remesa.CantidadCliente,
                                            ObservacionesVerificacion: 'Unidad Rechazada',
                                            CiudadDestino: Etiqueta.Remesa.CiudadDestinatario.Nombre,
                                            faltaUnidad: false,
                                            st: ''
                                        });
                                        itemGuia.verUnidaes = true
                                        inserto = true
                                        $scope.IngresadaActual = $scope.NumeroGuia;
                                        $scope.NumeroEtiqueta = ''
                                        $scope.NumeroGuia = '';
                                    }

                                }
                            });

                            if (!inserto) {
                                $scope.ListadoGuiasRechazadas.push({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    NumeroRemesa: Etiqueta.Remesa.Numero,
                                    NumeroDocumentoRemesa: Etiqueta.Remesa.NumeroDocumento,
                                    Peso: Etiqueta.Remesa.PesoCliente / Etiqueta.Remesa.CantidadCliente,
                                    ObservacionesVerificacion: 'RECHAZADA: Se rechazo en la planilla de descargue',
                                    CiudadDestino: Etiqueta.Remesa.CiudadDestinatario.Nombre,
                                    NumeroUnidad: 1,
                                    verUnidaes: true,
                                    faltaUnidad: false,
                                    st: '',
                                    listaUnidades: []
                                });

                                $scope.ListadoGuiasRechazadas.forEach(itemGuia => {

                                    if (itemGuia.NumeroRemesa == Etiqueta.Remesa.Numero) {

                                        itemGuia.listaUnidades.push({
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            NumeroRemesa: Etiqueta.Remesa.Numero,
                                            NumeroDocumentoRemesa: Etiqueta.Remesa.NumeroDocumento,
                                            NumeroUnidad: $scope.NumeroEtiqueta,
                                            Peso: Etiqueta.Remesa.PesoCliente / Etiqueta.Remesa.CantidadCliente,
                                            PesoVolumetrico: Etiqueta.Remesa.PesoVolumetricoCliente / Etiqueta.Remesa.CantidadCliente,
                                            UnidadesPendientes: Etiqueta.Remesa.CantidadCliente,
                                            UnidadesActuales: Etiqueta.Remesa.CantidadCliente,
                                            ObservacionesVerificacion: 'Unidad Rechazada',
                                            CiudadDestino: Etiqueta.Remesa.CiudadDestinatario.Nombre,
                                            faltaUnidad: false,
                                            st: ''
                                        });
                                    }
                                });
                            }

                        } else {

                            $scope.ListadoGuiasRechazadas.push({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroRemesa: Etiqueta.Remesa.Numero,
                                NumeroDocumentoRemesa: Etiqueta.Remesa.NumeroDocumento,
                                Peso: Etiqueta.Remesa.PesoCliente / Etiqueta.Remesa.CantidadCliente,
                                ObservacionesVerificacion: 'RECHAZADA: Se rechazo en la planilla de descargue',
                                CiudadDestino: Etiqueta.Remesa.CiudadDestinatario.Nombre,
                                NumeroUnidad: 1,
                                verUnidaes: true,
                                faltaUnidad: false,
                                st: '',
                                listaUnidades: []
                            });

                            $scope.ListadoGuiasRechazadas.forEach(itemGuia => {

                                itemGuia.listaUnidades.push({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    NumeroRemesa: Etiqueta.Remesa.Numero,
                                    NumeroDocumentoRemesa: Etiqueta.Remesa.NumeroDocumento,
                                    NumeroUnidad: $scope.NumeroEtiqueta,
                                    Peso: Etiqueta.Remesa.PesoCliente / Etiqueta.Remesa.CantidadCliente,
                                    PesoVolumetrico: Etiqueta.Remesa.PesoVolumetricoCliente / Etiqueta.Remesa.CantidadCliente,
                                    UnidadesPendientes: Etiqueta.Remesa.CantidadCliente,
                                    UnidadesActuales: Etiqueta.Remesa.CantidadCliente,
                                    ObservacionesVerificacion: 'Unidad Rechazada',
                                    CiudadDestino: Etiqueta.Remesa.CiudadDestinatario.Nombre,
                                    faltaUnidad: false,
                                    st: ''
                                });

                            });                            

                        }


                        $("#window").animate({
                            scrollTop: $('.breadcrumb').offset().top
                        }, 200);

                    }

                } else {
                    $scope.MensajesError.push('La etiqueta ingresada no existe')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }
            } else {

            }
            $scope.CalcularTotales();
            $scope.MostrarPendientes()
        }

        $scope.ObtenerPlanilla = function () {
            if ($scope.PlacaVehiculo.Codigo != undefined || $scope.CodigoAlternoVehiculo != undefined || $scope.NumeroPlanilla) {
                var filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                    Planilla: {
                        Numero: ($scope.NumeroPlanilla.Numero != undefined) ? $scope.NumeroPlanilla.Numero : $scope.NumeroPlanilla,
                        Vehiculo: {
                            Codigo: $scope.PlacaVehiculo.Codigo,
                            CodigoAlterno: $scope.CodigoAlternoVehiculo,
                            Semirremolque: {
                                Codigo: $scope.Semirremolque
                            }
                        }
                    },
                    Sync: true
                }
                var response = PlanillaPaqueteriaFactory.ObtenerPlanilla(filtro);

                if (response.ProcesoExitoso === true) {
                    if (response.Datos.length > 0) {
                        
                        $scope.ListadoPlanillasDespacho = response.Datos
                        for (var i = 0; i < 1; i++) {

                            if (filtro.Planilla.Numero == response.Datos[i].Planilla.NumeroDocumento) {

                                $scope.CodigoAlternoVehiculo = response.Datos[i].Planilla.Vehiculo.CodigoAlterno
                                $scope.PlacaVehiculo = response.Datos[i].Planilla.Vehiculo
                                $scope.Semirremolque = response.Datos[i].Planilla.Semirremolque.Placa
                                $scope.NumeroIdentificacion = response.Datos[i].Planilla.Conductor.Identificacion
                                $scope.NombreConductor = response.Datos[i].Planilla.Conductor
                                $scope.NombreTenedor = response.Datos[i].Planilla.Tenedor.NombreCompleto
                                $scope.NumeroPlanilla = {
                                    Numero: response.Datos[i].Planilla.NumeroDocumento,
                                    Planilla: response.Datos[i].Planilla.Numero
                                }

                                $scope.ObtenerVehiculo()
                                if ($scope.NumeroPlanilla.Numero > 0) {
                                    $scope.ValidarGuia()
                                } else {
                                    $scope.NumeroPlanilla = ''
                                }

                            } else if ($scope.CodigoAlternoVehiculo == response.Datos[i].Planilla.Vehiculo.CodigoAlterno
                                || $scope.PlacaVehiculo.Codigo == response.Datos[i].Planilla.Vehiculo.Codigo) {

                                $scope.CodigoAlternoVehiculo = response.Datos[i].Planilla.Vehiculo.CodigoAlterno
                                $scope.PlacaVehiculo = response.Datos[i].Planilla.Vehiculo
                                $scope.Semirremolque = response.Datos[i].Planilla.Semirremolque.Placa
                                $scope.NumeroIdentificacion = response.Datos[i].Planilla.Conductor.Identificacion
                                $scope.NombreConductor = response.Datos[i].Planilla.Conductor
                                $scope.NombreTenedor = response.Datos[i].Planilla.Tenedor.NombreCompleto

                                $scope.ObtenerVehiculo()
                                if ($scope.NumeroPlanilla.Numero > 0) {
                                    $scope.ValidarGuia()
                                } else {
                                    $scope.NumeroPlanilla = ''
                                }

                            }
                            
                        }
                                                     
                        
                    }
                }
            }
            
        }
 
        $scope.verUnidades = function (item) {
            $scope.modalUnidadesRemesas = item;
            showModal('modalUnidadesRemesas');
        };

        $scope.ValidarGuia = function () {
            var cont = 0;
            $scope.MensajesError = [];
            var Guia = ''
            if ($scope.NumeroPlanilla.Planilla > 0) {
                Guia = RemesaGuiasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true, NumeroPlanilla: $scope.NumeroPlanilla.Planilla });
                if (Guia != '' && Guia != undefined) {

                    for (var i = 0; i < Guia.Datos.length; i++) {

                        $scope.ListadoGuiasPaqueteria.push({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            NumeroRemesa: Guia.Datos[i].Remesa.Numero,
                            NumeroDocumentoRemesa: Guia.Datos[i].Remesa.NumeroDocumento,
                            Peso: Guia.Datos[i].Remesa.PesoCliente,
                            ObservacionesVerificacion: Guia.Datos[i].Remesa.Observaciones,
                            CiudadDestino: Guia.Datos[i].Remesa.CiudadDestinatario.Nombre,
                            NumeroUnidad: Guia.Datos[i].Remesa.CantidadCliente,
                            Pendientes: Guia.Datos[i].Remesa.CantidadCliente,
                            EstadoGuia: Guia.Datos[i].EstadoGuia.Codigo,
                            verUnidaes: false,
                            faltaUnidad: false,
                            st: '',
                            listaUnidades: []
                        });
                        $scope.DeshabilitarActualizar = true

                    }

                    $scope.CalcularTotales();

                } else {
                    $scope.MensajesError.push('La guía ingresada no es válida')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }
            }
            
            
        }

        $scope.CalcularTotales = function () {

            $scope.Cantidad = 0;
            $scope.Peso = 0
            $scope.PesoVolumetrico = 0
            $scope.ListadoGuias = []
            $scope.CantidadGuias = 0
            $scope.ListadoGuiasPaqueteria.forEach(itemGuia => {
                $scope.Cantidad = $scope.Cantidad + itemGuia.NumeroUnidad
                $scope.Peso = $scope.Peso + itemGuia.Peso

                itemGuia.listaUnidades.forEach(itemUnidad => {
                    if (!itemUnidad.NumeroUnidad == 0) {
                        itemUnidad.EstadoGuia = itemGuia.EstadoGuia
                        $scope.ListadoGuias.push(itemUnidad)
                    }
                });
            });                        

            $scope.ListadoGuiasDescargadas.forEach(itemGuia => {
                $scope.Cantidad = $scope.Cantidad + itemGuia.NumeroUnidad
                $scope.Peso = $scope.Peso + itemGuia.Peso

                itemGuia.listaUnidades.forEach(itemUnidad => {
                    if (!itemUnidad.NumeroUnidad == 0) {
                        itemUnidad.EstadoGuia = itemGuia.EstadoGuia
                        $scope.ListadoGuias.push(itemUnidad)
                    }
                });
            });

            $scope.ListadoGuiasRechazadas.forEach(itemGuia => {
                itemGuia.NumeroUnidad = itemGuia.listaUnidades.length
                itemGuia.listaUnidades.forEach(itemUnidad => {
                    if (!itemUnidad.NumeroUnidad == 0) {                        
                        $scope.ListadoGuias.push(itemUnidad)
                    }
                });
            });

            console.log('lista', $scope.ListadoGuias)
            $scope.CantidadGuias = $scope.CantidadGuias + $scope.ListadoGuiasPaqueteria.length
            $scope.CantidadGuias = $scope.CantidadGuias + $scope.ListadoGuiasDescargadas.length
            $scope.UnidadesPendientes = $scope.Cantidad - $scope.ListadoGuias.length
            $scope.UnidadesCargadas = $scope.ListadoGuias.length
            $scope.MostrarSeccionGuias = true

        }

        $scope.ConfirmarEliminarGuia = function (item) {
            $scope.TmpEliminarGuia = item.NumeroUnidad ;
            $scope.TmpNumeroDocumentoRemesa = item.NumeroDocumentoRemesa;
            showModal('modalEliminarGuia');
        };

        $scope.ConfirmarEliminar = function (item) {
            $scope.TmpEliminarGuia = item;
            showModal('modalEliminar');
        };

        $scope.Eliminar = function () {
            $scope.ListadoGuiasDescargadas.forEach(itemGuia => {
                if (itemGuia.NumeroDocumentoRemesa == $scope.TmpEliminarGuia.NumeroDocumentoRemesa) {
                   itemGuia.listaUnidades.splice(0, itemGuia.listaUnidades.length)
                    itemGuia.verUnidades = false
                    var indice = $scope.ListadoGuiasDescargadas.indexOf(itemGuia);
                    $scope.ListadoGuiasDescargadas.splice(indice, 1);
                    $scope.ListadoGuiasPaqueteria.push(itemGuia)
                    $scope.verificarDescargas(itemGuia)
                }
            });
            if ($scope.ListadoGuiasDescargadas.length <= 0) {
                $scope.MostrarPendientes()
            }
            $scope.CalcularTotales();
            closeModal('modalEliminar');
        };

        $scope.EliminarGuia = function (indice) {
            $scope.BuscarUnidad()
            
            $scope.ListadoGuiasPaqueteria.forEach(itemGuia => {
                if (itemGuia.NumeroDocumentoRemesa == $scope.TmpNumeroDocumentoRemesa) {
                    var indice = itemGuia.listaUnidades.indexOf($scope.TmpEliminarUnidad)
                    itemGuia.listaUnidades.splice(indice, 1)
                    $scope.verificarDescargas(itemGuia)
                }

            });

            $scope.ListadoGuiasDescargadas.forEach(itemGuia => {
                if (itemGuia.NumeroDocumentoRemesa == $scope.TmpNumeroDocumentoRemesa) {
                    var indice = itemGuia.listaUnidades.indexOf($scope.TmpEliminarUnidad)
                    itemGuia.listaUnidades.splice(indice, 1)

                    indice = $scope.ListadoGuiasDescargadas.indexOf(itemGuia);
                    $scope.ListadoGuiasDescargadas.splice(indice, 1);
                    $scope.ListadoGuiasPaqueteria.push(itemGuia)
                    $scope.verificarDescargas(itemGuia)
                }                

            });

            $scope.ListadoGuiasRechazadas.forEach(itemGuia => {
                if (itemGuia.NumeroDocumentoRemesa == $scope.TmpNumeroDocumentoRemesa) {
                    var indice = itemGuia.listaUnidades.indexOf($scope.TmpEliminarUnidad)
                    itemGuia.listaUnidades.splice(indice, 1)
                }

            });

            $scope.CalcularTotales();
            closeModal('modalEliminarGuia');

            if ($scope.modalUnidadesRemesas.listaUnidades <= 0) {
                closeModal('modalUnidadesRemesas');
            } 

        };

        $scope.BuscarUnidad = function () {
            $scope.modalUnidadesRemesas.listaUnidades.some(itemGuia => {

                if (itemGuia.NumeroDocumentoRemesa == $scope.TmpNumeroDocumentoRemesa && itemGuia.NumeroUnidad == $scope.TmpEliminarGuia) {
                    $scope.TmpEliminarUnidad = itemGuia
                    return true                   
                }

            })
        }

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardarCargue');
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarCargue');

            if ($scope.DatosRequeridos()) {
                console.log('Conductor', $scope.NombreConductor)
                $scope.ListadoGuiasPaqueteria.forEach(itemGuia => {

                    var item = {
                        CiudadDestino: itemGuia.CiudadDestino,
                        CodigoEmpresa: itemGuia.CodigoEmpresa,
                        NumeroDocumentoRemesa: itemGuia.NumeroDocumentoRemesa,
                        NumeroRemesa: itemGuia.NumeroRemesa,
                        NumeroUnidad: 0,
                        ObservacionesVerificacion: itemGuia.ObservacionesVerificacion,
                        Peso: itemGuia.Peso,
                        EstadoGuia: itemGuia.EstadoGuia,
                        UnidadesActuales: itemGuia.UnidadesActuales
                    }

                    $scope.ListadoGuias.push(item)

                });

                $scope.ListadoGuiasRechazadas.forEach(itemGuia => {

                    var item = {
                        CiudadDestino: itemGuia.CiudadDestino,
                        CodigoEmpresa: itemGuia.CodigoEmpresa,
                        NumeroDocumentoRemesa: itemGuia.NumeroDocumentoRemesa,
                        NumeroRemesa: itemGuia.NumeroRemesa,
                        NumeroUnidad: 0,
                        ObservacionesVerificacion: itemGuia.ObservacionesVerificacion,
                        Peso: itemGuia.Peso,
                        EstadoGuia: itemGuia.EstadoGuia,
                        UnidadesActuales: itemGuia.UnidadesActuales
                    }

                    $scope.ListadoGuias.push(item)

                });

                $scope.ListadoGuiasDescargadas.forEach(itemGuia => {

                    var item = {
                        CiudadDestino: itemGuia.CiudadDestino,
                        CodigoEmpresa: itemGuia.CodigoEmpresa,
                        NumeroDocumentoRemesa: itemGuia.NumeroDocumentoRemesa,
                        NumeroRemesa: itemGuia.NumeroRemesa,
                        NumeroUnidad: 0,
                        ObservacionesVerificacion: itemGuia.ObservacionesVerificacion,
                        Peso: itemGuia.Peso,
                        EstadoGuia: itemGuia.EstadoGuia,
                        UnidadesActuales: itemGuia.UnidadesActuales
                    }

                    $scope.ListadoGuias.push(item)

                });

                var Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $routeParams.Numero,
                    Vehiculo: { Codigo: $scope.PlacaVehiculo.Codigo },
                    TipoVerificacion: {
                        codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESCARGUE
                    },
                    Observaciones: $scope.Observaciones,
                    Cantidad: $scope.Cantidad,
                    Peso: $scope.Peso,
                    Conductor: $scope.NombreConductor,
                    Semirremolque: $scope.Semirremolque, 
                    Estado: $scope.Estado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    ListadoGuias: $scope.ListadoGuias,
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                    NumeroPlanilla: $scope.NumeroPlanilla.Numero
                }
                $scope.MensajesConfirmacion = [];
                $scope.MensajesError = []
                console.log('Guardando', Entidad)

                ValidacionPlanillaDespachosFactory.Guardar(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            $scope.MensajesConfirmacion.push('Se han verificado ' + $scope.ListadoGuias.length + ' guías de la planilla ' + $scope.NumeroPlanilla);
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                            if ($scope.Estado.Codigo == 1) {                                
                                $scope.ListadoGuias.forEach(itemGuia => {

                                    if (itemGuia.NumeroUnidad == 0) {
                                        if (itemGuia.EstadoGuia == 6010) {
                                            var entidad = {
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                Remesa: {
                                                    Numero: itemGuia.NumeroRemesa
                                                },
                                                EstadoRemesaPaqueteria: {
                                                    Codigo: 6005
                                                },
                                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                                OficinaActual: {
                                                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo
                                                },
                                                Aforador: {
                                                    Codigo: $scope.Sesion.UsuarioAutenticado.Codigo
                                                },
                                                Sync: true
                                            }

                                            filtro = {
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                                EstadoGuia: { Codigo: 6005 },
                                                GestionDocumentosLegalizar: 1,
                                                OficinaActual: {
                                                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo
                                                },
                                                Remesa: {
                                                    Numero: itemGuia.NumeroRemesa,
                                                    NumeroDocumento: itemGuia.NumeroDocumentoRemesa
                                                },
                                                Sync: true
                                            }

                                            RemesaGuiasFactory.CambiarEstadoTrazabilidadGuia(filtro);
                                            RemesaGuiasFactory.InsertarEstadosRemesasDescargas(entidad);
                                            itemGuia.EstadoGuia = 6005
                                        }

                                        if (itemGuia.ObservacionesVerificacion != '') {

                                            var filtros = {
                                                Numero: itemGuia.NumeroRemesa,
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                                Observaciones: itemGuia.ObservacionesVerificacion
                                            }

                                            RemesaGuiasFactory.GuardarBitacoraGuias(filtros).
                                                then(function (response) {
                                                    if (response.data.ProcesoExitoso == true) {

                                                    } else {
                                                        ShowError(response.statusText)
                                                    }
                                                }, function (response) {
                                                    ShowError(response.statusText);
                                                });
                                        }
                                    }
                                });
                            }
                                                  

                            $scope.MostrarSeccionGuias = false;
                            $scope.ListadoGuias = [];
                            $scope.NumeroDocumentoPlanilla = '';

                            $scope.NumeroGuia = '';
                            $scope.UnidadGuia = '';
                            $scope.PlacaVehiculo = '';
                            $scope.NombreConductor = '';
                            $scope.NombreTenedor = '';
                            $scope.NombreRuta = '';
                            $scope.PlacaSemirremolque = '';
                            $scope.FechaSalida = '';
                            $scope.HoraSalida = '';

                            if ($scope.NumeroPlanilla > 0) {
                                ShowSuccess('Se modificó la planilla con el número ' + response.data.Datos);
                                document.location.href = '#!ConsultarPlanillaDescargue/' + response.data.Datos
                                $scope.NumeroPlanilla = '';
                            } else {
                                ShowSuccess('Se guardó la planilla con el número ' + response.data.Datos);
                                document.location.href = '#!ConsultarPlanillaDescargue/' + response.data.Datos
                            }

                        } else {
                            $scope.MensajesError.push(response.data.MensajeOperacion);
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                        }
                    });
            }
        }

        $scope.DatosRequeridos = function () {
            var continuar = true;
            if ($scope.ListadoGuias != undefined && $scope.ListadoGuias != null && $scope.ListadoGuias != '') {
                if ($scope.ListadoGuias.length == 0) {
                    $scope.MensajesError.push('debe existir por lo menos una guía verificada')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                    continuar = false;
                }
            } else {
                $scope.MensajesError.push('debe existir por lo menos una guía verificada')
                $("#window").animate({
                    scrollTop: $('.breadcrumb').offset().top
                }, 200);
            }
            return continuar;
        }

        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.NumeroPlanillaDescargue
            };
            ValidacionPlanillaDespachosFactory.Obtener(filtros).
                then(function (response) {
                    console.log('llego data', response)
                    if (response.data.ProcesoExitoso === true) {
                        $scope.NumeroPlanilla = {
                            Numero: response.data.Datos.NumeroPlanilla,
                            Planilla: ''
                        }
                        $scope.NumeroPlanillaDescargue = response.data.Datos.NumeroDocumento;
                        $scope.TipoPlanilla = CODIGO_TIPO_DOCUMENTO_PLANILLA_DESCARGUE
                        $scope.Fecha = new Date(response.data.Datos.Fecha);
                        $scope.PlacaVehiculo = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo);

                        if (response.data.Datos.Semirremolque.Codigo > 0) {
                            $scope.Semirremolque = $linq.Enumerable().From($scope.ListadoSemirremolques).First('$.Codigo == "' + response.data.Datos.Semirremolque.Codigo + '"');
                        }

                        $scope.ObtenerPlanilla();

                        $scope.Observaciones = response.data.Datos.Observaciones;

                        response.data.Datos.ListadoGuias.forEach(item => {

                            if (item.Observaciones.includes('RECHAZADA') || item.Observaciones.includes('Rechazada')) {

                                if (item.NumeroUnidad == 0) {

                                    $scope.ListadoGuiasRechazadas.push({
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        NumeroRemesa: item.NumeroRemesa,
                                        NumeroDocumentoRemesa: item.NumeroDocumentoRemesa,
                                        Peso: item.PesoRemesa,
                                        ObservacionesVerificacion: item.Observaciones,
                                        CiudadDestino: item.CiudadDestinatario,
                                        NumeroUnidad: item.NumeroUnidad,
                                        verUnidaes: true,
                                        faltaUnidad: false,
                                        st: '',
                                        listaUnidades: []
                                    });


                                } else {
                                    $scope.ListadoGuiasRechazadas.forEach(itemGuia => {

                                        if (itemGuia.NumeroRemesa == item.NumeroRemesa) {

                                            if (item.NumeroUnidad == 0) {
                                                itemGuia.ObservacionesVerificacion = item.Observaciones
                                            } else {
                                                itemGuia.listaUnidades.push({
                                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                    NumeroRemesa: item.NumeroRemesa,
                                                    NumeroDocumentoRemesa: item.NumeroDocumentoRemesa,
                                                    NumeroUnidad: item.NumeroUnidad,
                                                    UnidadesActuales: response.data.Datos.ListadoGuias.length,
                                                    Peso: item.PesoRemesa,
                                                    ObservacionesVerificacion: item.Observaciones,
                                                    CiudadDestino: itemGuia.CiudadDestino,
                                                    st: ''
                                                });
                                                itemGuia.verUnidaes = true
                                            }
                                        }
                                    });
                                }


                            } else {
                                $scope.ListadoGuiasPaqueteria.forEach(itemGuia => {


                                    if (itemGuia.NumeroRemesa == item.NumeroRemesa) {

                                        if (item.NumeroUnidad == 0) {
                                            itemGuia.ObservacionesVerificacion = item.Observaciones
                                        } else {
                                            itemGuia.listaUnidades.push({
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                NumeroRemesa: item.NumeroRemesa,
                                                NumeroDocumentoRemesa: item.NumeroDocumentoRemesa,
                                                NumeroUnidad: item.NumeroUnidad,
                                                UnidadesActuales: response.data.Datos.ListadoGuias.length,
                                                Peso: item.PesoRemesa,
                                                ObservacionesVerificacion: item.Observaciones,
                                                CiudadDestino: itemGuia.CiudadDestino,
                                                st: ''
                                            });
                                            itemGuia.verUnidaes = true
                                            $scope.verificarDescargas(itemGuia)
                                        }
                                    }
                                });
                            }

                           
                        });

                        $scope.CalcularTotales();

                        $scope.MostrarSeccionGuias = true;
                        $scope.Oficina = OficinasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.Oficina.Codigo, Sync: true }).Datos;
                        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + response.data.Datos.Estado);
                        if ($scope.Estado.Codigo==1) {
                            $scope.Deshabilitar = true
                        }
                        blockUI.stop();
                    }

                });
            $scope.MostrarPendientes()
        }

        $scope.MostrarPendientes = function () {
            $('#GuiasPendientes').show();
            $('#GuiasDescargadas').hide()
            $('#GuiasRechazadas').hide()
        }

        $scope.MostrarDescargadas = function () {
            $('#GuiasDescargadas').show();
            $('#GuiasPendientes').hide()
            $('#GuiasRechazadas').hide()
        }

        $scope.MostrarRechazadas = function () {
            $('#GuiasRechazadas').show()
            $('#GuiasDescargadas').hide()
            $('#GuiasPendientes').hide()
        }

        $scope.VolverMaster = function () {
            if ($scope.NumeroPlanilla > 0) {
                document.location.href = '#!ConsultarPlanillaDescargue/' + $scope.NumeroPlanilla;
            } else {
                document.location.href = '#!ConsultarPlanillaDescargue';
            }
        }

        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        }

        if ($routeParams.Numero > 0) {
            $scope.NumeroPlanillaDescargue = $routeParams.Numero
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Obteniendo Documento...");
            $timeout(function () { blockUI.message("Obteniendo Documento..."); Obtener(); }, 100);
        }

    }
]);