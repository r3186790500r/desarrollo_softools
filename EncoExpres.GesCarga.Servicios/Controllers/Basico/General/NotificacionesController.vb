﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General


''' <summary>
''' Controlador <see cref="NotificacionesController"/>
''' </summary>
<Authorize>
Public Class NotificacionesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Notificaciones) As Respuesta(Of IEnumerable(Of Notificaciones))
        Return New LogicaNotificaciones(CapaPersistenciaNotificaciones).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Notificaciones) As Respuesta(Of Notificaciones)
        Return New LogicaNotificaciones(CapaPersistenciaNotificaciones).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Notificaciones) As Respuesta(Of Long)
        Return New LogicaNotificaciones(CapaPersistenciaNotificaciones).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Notificaciones) As Respuesta(Of Boolean)
        Return New LogicaNotificaciones(CapaPersistenciaNotificaciones).Anular(entidad)
    End Function


End Class
