﻿EncoExpresApp.controller("ConsultarOrdenCargueCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'OrdenCargueFactory', 'ValorCatalogosFactory', 'blockUI', 'EmpresasFactory',
    function ($scope, $routeParams, $timeout, $linq, OrdenCargueFactory, ValorCatalogosFactory, blockUI, EmpresasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Documentos' }, { Nombre: 'Orden Cargue' }];
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        var filtros = {};
        $scope.pref = '';
        $scope.ListadoEstados = [];
        $scope.ListadoAnulacion = [];
        $scope.MostrarMensajeError = false
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.Anulado = 0;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ORDEN_CARGUE); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        //$scope.ModeloFechaFinal = new Date();
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];



        $scope.ListadoAnulacion = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CONCEPTO_ANULACION } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoAnulacion = response.data.Datos;
                        console.log($scope.ListadoAnulacion);
                    }
                    else {
                        $scope.ListadoPerfiles = []
                    }
                }
            }, function (response) {
            });
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');



        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

        /*Metodo buscar*/
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                if (DatosRequeridos()) {
                    $scope.Codigo = 0
                    Find()
                }
            }
        };
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarOrdenCargue';
            }
        };
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO_LUGAR } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRecaudo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoRecaudo = response.data.Datos;
                        $scope.ModalRecaudo = $scope.ListadoTipoRecaudo[0];
                    }
                    else {
                        $scope.ListadoTipoRecaudo = []
                    }
                }
            }, function (response) {
            });

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });

        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {

            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoOrdenCargue = [];

            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ModeloNumero,
                FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaInicial,
                FechaFinal: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaFinal,
                TerceroCliente: { Nombre: $scope.ModeloCliente },
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Vehiculo: { Placa: $scope.ModeloPlaca },
                Estado: $scope.ModalEstado.Codigo,
                NumeroManifiesto: $scope.ModeloManifiesto,
                NumeroPlanilla: $scope.ModeloPlanilla,
                AplicaConsultaMaster: 1
            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                OrdenCargueFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoOrdenCargue = response.data.Datos;
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoOrdenCargue = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }


                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();


        };

        /*------------------------------------------------------------------------------------Eliminar Oficinas-----------------------------------------------------------------------*/
        $scope.AnularOrdenCargue = function (codigo, CodigoAlterno, NumeroDocumento) {
            $scope.Codigo = codigo


            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalErrorCompleto = ''
            $scope.CodigoAlterno = CodigoAlterno
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnularOrdenCargue');
        };

        $scope.Anular = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CodigoUsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnula: $scope.ModeloCausaAnula,
                Numero: $scope.Codigo,
                ConceptoAnulacion: $scope.ModalConceptoAnulacion.Codigo
            };
            //if (DatosRequeridos()) {
            OrdenCargueFactory.Anular(entidad).

                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se Anulo la Orden de Cargue '+ $scope.NumeroDocumento);
                        closeModal('modalAnularOrdenCargue');
                        $scope.ModeloCausaAnula = '';
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeAnularOrdenCargue');
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'No se puede anular la orden de cargue ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText
                });
            //}

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            $scope.ModeloNumero = $routeParams.Numero;
            Find();
        }

        $scope.CerrarModal = function () {
            closeModal('modalAnularImpuesto');
            closeModal('modalMensajeAnuloImpuesto');
        }

        $scope.MaskNumero = function () {
            $scope.CodigoInicial = MascaraNumero($scope.CodigoInicial)
            $scope.CodigoFinal = MascaraNumero($scope.CodigoFinal)
            $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);

        };
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_ORCA;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento != undefined && $scope.NumeroDocumento != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }

        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '')
                && ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '')
                && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)
                && ($scope.ModeloManifiesto === null || $scope.ModeloManifiesto === undefined || $scope.ModeloManifiesto === '' || $scope.ModeloManifiesto === 0 || isNaN($scope.ModeloManifiesto) === true)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')
                || ($scope.ModeloManifiesto !== null && $scope.ModeloManifiesto !== undefined && $scope.ModeloManifiesto !== '')
            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaInicial < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }
                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial;
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal;
                    }
                }
            }
            return continuar
        }

    }]);