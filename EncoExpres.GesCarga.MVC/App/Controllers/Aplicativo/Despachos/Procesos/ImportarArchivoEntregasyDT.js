﻿EncoExpresApp.controller("ImportarArchivoEntregasyDTCtrl", ['$scope', '$timeout', '$linq', 'TercerosFactory', 'blockUI', 'RemesaGuiasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'ProductoTransportadosFactory', 'CiudadesFactory', 'SitiosTerceroClienteFactory', 'blockUIConfig', 'OficinasFactory', 'ZonasFactory', 'CargueMasivoRecoleccionesFactory', 'TarifarioVentasFactory', 'UnidadEmpaqueFactory', 'PlanillaDespachosFactory',
    function ($scope, $timeout, $linq, TercerosFactory, blockUI, RemesaGuiasFactory, ValorCatalogosFactory, TercerosFactory, ProductoTransportadosFactory, CiudadesFactory, SitiosTerceroClienteFactory, blockUIConfig, OficinasFactory, ZonasFactory, CargueMasivoRecoleccionesFactory, TarifarioVentasFactory, UnidadEmpaqueFactory, PlanillaDespachosFactory) {
        console.clear()



        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.continuar = false
        $scope.VerErrores = false;
        $scope.DeshabilitarCliente = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.ListadoRutasPuntosGestion = [];
        $scope.ListaHorariosEntrega = [];
        $scope.OficinaUsuario = OficinasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo, Sync: true }).Datos;
        $scope.Cliente = '';
        $scope.ClienteVista = '';
        $scope.ListadoZonasGeneral = [];
        $scope.ListadoSitiosEntregaGeneral = [];
        $scope.DataArchivo = []
        $scope.ManejoReexpedicionOficina = $scope.Sesion.UsuarioAutenticado.ManejoReexpedicionPorOficina;
        var ListaNuevosTerceros = [];
        //--Unidad empaque
        $scope.ListadoUnidadesEmpaque = [];
        $scope.ListadoUnidadesEmpaque = UnidadEmpaqueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos;
        var UltimoConsecutivoTercero = { Codigo: 0 }
        var strListaNuevosTerceros = '';
        $scope.Modelo = {
            Fecha: new Date(),
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,
            Remesa: {
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                Numero: 0,
                TipoRemesaDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },
                DetalleTarifaVenta: {},
                ProductoTransportado: '',
                Fecha: new Date(),
                Remitente: { Direccion: '' },
                Destinatario: { Direccion: '' },
            },
        }
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 20;
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Procesos' }, { Nombre: 'Importar Archivo Entregas y DT' }];
        $scope.ListadoZonasGeneral = ZonasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos;
        $scope.ListadoSitiosEntregaGeneral = SitiosTerceroClienteFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos

        $scope.ObtenerTarifario = function () {
            if ($scope.Cliente.Codigo != undefined) {
                PantallaBloqueoTarifario(ObtenerTarifario)
            }
        }

        $scope.TarifarioCliente = [];
        function ObtenerTarifario() {
            if ($scope.Cliente != undefined && $scope.Cliente != null && $scope.Cliente != '') {
                var filtroTarifarioCliente = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Cliente.Codigo,
                    CodigoLineaNegocio: CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA,
                    TarifarioProveedores: false,
                    TarifarioClientes: true
                }
                filtroTarifarioCliente.Sync = true;
                var ResponseTarifarioCliente = TercerosFactory.Consultar(filtroTarifarioCliente);

                if (ResponseTarifarioCliente != undefined) {
                    if (ResponseTarifarioCliente.ProcesoExitoso === true) {
                        if (ResponseTarifarioCliente.Datos.length > 0) {
                            $scope.TarifarioCliente = ResponseTarifarioCliente.Datos
                            console.log(ResponseTarifarioCliente.Datos)
                            blockUI.stop()
                        } else {
                            ShowError('El tarifario de este cliente no tiene Tarifas Paquetería')
                            blockUI.stop()
                        }
                    } else {
                        ShowError(ResponseTarifarioCliente.MensajeOperacion)
                        blockUI.stop()
                    }
                } else {
                    ShowError('El tarifario de este cliente no tiene Tarifas Paquetería')
                    blockUI.stop()
                }
            } else {
                blockUI.stop()
            }

        }

        $scope.AutocompleteSitiosClienteDescargue2 = function (value, cliente, ciudad) {
            $scope.ListadoSitiosDescargueAlterno2 = []
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    }
                    $scope.ListadoSitiosDescargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosDescargueAlterno2)
                }
            }
            return $scope.ListadoSitiosDescargueAlterno2
        }

        $scope.AutocompleteZonasCiudades = function (value, ciudad) {
            $scope.ListadoZonasCiudades = []

            /*Cargar Autocomplete de propietario*/
            blockUIConfig.autoBlock = false;
            var Response = ZonasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Ciudad: { Codigo: ciudad.Codigo },
                Sync: true
            })

            $scope.ListadoZonasCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoZonasCiudades)

            return $scope.ListadoZonasCiudades
        }
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoFormaPago = [];
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NO_APLICA) {
                                $scope.ListadoFormaPago.push(response.data.Datos[i])
                            }
                        }
                    }
                    else {
                        $scope.ListadoFormaPago = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de tipo entrega*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;
                    }
                }
            }, function (response) {
            });
        /*Cargar Autocomplete de CLIENTES*/
        $scope.ListaClientes = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, Sync: true }).Datos



        /*Cargar Autocomplete de CLIENTES*/
        $scope.AutocompleteClientes = function (value) {
            var ListadoClientes = []
            var ResponseListadoTerceros = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true });

            if (ResponseListadoTerceros.ProcesoExitoso === true) {
                ListadoClientes = []
                if (ResponseListadoTerceros.Datos.length > 0) {
                    ListadoClientes = ResponseListadoTerceros.Datos;
                }
                else {
                    ListadoClientes = [];
                }
            }

            return ListadoClientes
        }
        /*Autocomplete de productos*/
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoProductoTransportados = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoProductoTransportados = response.data.Datos;
                    }
                    else {
                        $scope.ListadoProductoTransportados = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de tipo identificaciones*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoIdentificacion = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoIdentificacion = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoIdentificacion = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de líneas negocio paquetería*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 216 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaLineasNegocioPaqueteria = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaLineasNegocioPaqueteria = response.data.Datos;
                    }
                    else {
                        $scope.ListaLineasNegocioPaqueteria = []
                    }
                }
            }, function (response) {
            });


        /*Cargar el combo de Horarios Entrega*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 217 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaHorariosEntrega = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaHorariosEntrega = response.data.Datos;
                    }
                    else {
                        $scope.ListaHorariosEntrega = []
                    }
                }
            }, function (response) {
            });
        $scope.ListaRecogerOficinaDestino = [];
        $scope.ListaRecogerOficinaDestino = [
            { Codigo: 0, Nombre: 'NO' },
            { Codigo: 1, Nombre: 'SI' }
        ];
        /*Ciudades*/
        CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCiudades = response.data.Datos;
                    }
                    else {
                        $scope.ListadoCiudades = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Estado GUÍA*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaEstadosGuia = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaEstadosGuia = response.data.Datos;

                        $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                        $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                        $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                        if ($scope.CodigoEstadoGuia !== undefined && $scope.CodigoEstadoGuia !== null) {
                            $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + $scope.CodigoEstadoGuia);

                        } else {
                            if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_PROPIA) {
                                $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_OFICINA_ORIGEN);

                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                    if ($scope.ListaOficinas.length > 0) {
                                        $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                    }
                                }
                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                            }
                            if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_CLIENTE) {
                                $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);


                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                    if ($scope.ListaOficinas.length > 0) {
                                        $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                    }
                                }

                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                            }
                            if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_RECEPTORIA) {
                                $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);

                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                    if ($scope.ListaOficinas.length > 0) {
                                        $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                    }
                                }
                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;
                            }
                        }


                    }
                    else {
                        $scope.ListaEstadosGuia = []
                    }
                }
            }, function (response) {
            });
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 20) {
                $scope.DataActual = []
                $scope.paginaActual = 1
                for (var i = 0; i < 20; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 20
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 20
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 20 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 20
                $scope.DataActual = []
                for (var i = a - 20; i < $scope.DataArchivo.length; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }

        //Paginacion MOdal de errores

        $scope.PrimerPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 20) {
                $scope.DataErrorActual = []
                $scope.paginaActualError = 1
                for (var i = 0; i < 20; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.AnteriorModalError = function () {
            if ($scope.paginaActualError > 1) {
                $scope.paginaActualError -= 1
                var a = $scope.paginaActualError * 20
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteModalError = function () {
            if ($scope.paginaActualError < $scope.totalPaginasErrores) {
                $scope.paginaActualError += 1
                var a = $scope.paginaActualError * 20
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    $scope.UltimaPaginaModalError()

                }
            } else if ($scope.paginaActualError == $scope.totalPaginasErrores) {
                $scope.UltimaPaginaModalError()
            }
        }
        $scope.UltimaPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 20 && $scope.totalPaginasErrores > 1) {
                $scope.paginaActualError = $scope.totalPaginasErrores
                var a = $scope.paginaActualError * 20
                $scope.DataErrorActual = []
                for (var i = a - 20; i < $scope.ListaErrores.length; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 20) {
                $scope.ProgramacionesNoGuardadasActual = []
                $scope.paginaActualNoGuardadas = 1
                for (var i = 0; i < 20; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }
        $scope.AnteriorNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas -= 1
                var a = $scope.paginaActualNoGuardadas * 20
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas < $scope.totalPaginasNoGuardadas) {
                $scope.paginaActualNoGuardadas += 1
                var a = $scope.paginaActualNoGuardadas * 20
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardados()

                }
            } else if ($scope.paginaActualNoGuardadas == $scope.totalPaginasNoGuardadas) {
                $scope.UltimaPaginaNoGuardados()
            }
        }
        $scope.UltimaPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 20 && $scope.totalPaginasNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas = $scope.totalPaginasNoGuardadas
                var a = $scope.paginaActualNoGuardadas * 20
                $scope.ProgramacionesNoGuardadasActual = []
                for (var i = a - 20; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 20) {
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                $scope.paginaActualNoGuardadasRemplazo = 1
                for (var i = 0; i < 20; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }
        $scope.AnteriorNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo -= 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo < $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.paginaActualNoGuardadasRemplazo += 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardadosRemplazo()

                }
            } else if ($scope.paginaActualNoGuardadasRemplazo == $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.UltimaPaginaNoGuardadosRemplazo()
            }
        }
        $scope.UltimaPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 20 && $scope.totalPaginasNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo = $scope.totalPaginasNoGuardadasRemplazo
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                for (var i = a - 20; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }


        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardaras = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardadasRemplazo = function (item) {
            if ($scope.Option.name == 'Omitido') {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Omitido'
                }
            } else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Remplazado'
                }
            }

        }
        $scope.LimpiarData = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            $scope.DataVerificada = []
            $scope.DataActual = []
            $scope.DataErrorActual = []
            $scope.DataArchivo = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            $scope.VerErrores = false

        }

        $scope.Archivo = '';
        $scope.ModalConfirmacionNuevoProceso = function () {
            showModal('modalConfirmacionCancelarProceso');
        }
        $scope.CerrarModalNuevoProceso = function (e) {
            var fileVal = document.getElementById("fileModal");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
            closeModal('modalConfirmacionCancelarProceso');


        }
        /*-----------------------------------------------------------------------funciones de lectura y apertura de archivos----------------------------------------------------------------------------*/

        $scope.ListaProgramaciones = []
        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined) {
                return true
            } else {
                return false
            }
        }
        var strIdentificacionesDestinatarios = '';
        function AsignarValoresTabla(sheet) {
            $scope.NumeroCargue = 0
            $scope.DataArchivo = []
            var DataTemporal = []
            $scope.DataActual = []
            $scope.paginaActual = 1

            blockUI.start();
            blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');

            $timeout(function () {
                try {
                    var data = []
                    var strPlanillas = '';
                    for (var i = 0; i < sheet.length; i++) {
                        strPlanillas = strPlanillas + sheet[i][1] + ','

                        $scope.DataArchivo.push({
                            Planilla: { Numero: 0, NumeroDocumento: sheet[i][1] },
                            DT: sheet[i][2],
                            Entrega: sheet[i][3]
                        });
                    }
                    $scope.totalRegistros = $scope.DataArchivo.length
                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / 20);
                    var ResponsePlanillas = PlanillaDespachosFactory.ConsultarListaPlanillas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Planillas: strPlanillas, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Sync: true }).Datos
                    if (ResponsePlanillas != undefined) {
                        if (ResponsePlanillas.length > 0) {
                            ValidarPlanillas($scope.DataArchivo, ResponsePlanillas)
                        } else {
                            ShowError('No se encontraron planillas en el sistema con los números indicados en el archivo');
                        }
                    } else {
                        ShowError('No se encontraron planillas en el sistema con los números indicados en el archivo');
                    }

                    console.log('archivo: ' + JSON.stringify(ResponsePlanillas));

                    blockUI.stop();
                    if ($scope.totalRegistros > 20) {
                        for (var i = 0; i < 20; i++) {
                            $scope.DataActual.push($scope.DataArchivo[i])
                        }

                    } else {
                        $scope.DataActual = $scope.DataArchivo
                    }
                    for (var i = 0; i < $scope.DataArchivo.length; i++) {

                        $scope.DataArchivo[i].Pos = i;


                    }

                    if ($scope.DataArchivo.length > 0) {
                        blockUI.stop();
                    } else {
                        ShowError('El archivo seleccionado no contiene datos para validar, por favor intente con otro archivo')
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)

                    }
                } catch (e) {
                    ShowError('Error en la lectura del archivo por favor intente nuevamente: ' + e)

                    blockUI.stop()
                    $timeout(blockUI.stop(), 1000)
                }



            }, 1000)
            //}

        }

        $scope.ValidarPLanillas = function () {
            ValidarPlanillas($scope.DataArchivo, []);
        }
        function ValidarPlanillas(DataArchivo, ResponsePlanillas) {
            var ContErroresCargue = 0;
            $scope.DataArchivo = $linq.Enumerable().From(DataArchivo).Select(function (x) {
                x.Error = 0
                x.stRow = x.NoActualizable == 1 ? x.stRow : ''
                x.msGeneral = x.NoActualizable == 1 ? x.msGeneral : ''
                x.Omitido = x.Omitido == undefined ? false : x.Omitido
                if (ResponsePlanillas.length > 0) {
                    try {
                        if (!x.Omitido) {
                            x.Planilla.Numero = $linq.Enumerable().From(ResponsePlanillas).First('$.NumeroDocumento==' + MascaraNumero(x.Planilla.NumeroDocumento)).Numero
                            x.Planilla.NumeroDocumento = MascaraNumero(x.Planilla.NumeroDocumento)
                        }
                    } catch (e) {
                        x.Planilla.Numero = 0
                        x.Planilla.NumeroDocumento = MascaraNumero(x.Planilla.NumeroDocumento)
                        x.Error = 1
                        x.stRow = 'background:yellow'
                        x.msGeneral = 'La planilla no se encuentra en el sistema o se encuentra anulada'
                        x.Omitido = true
                        ContErroresCargue++;
                    }





                }
                return x
            }).ToArray();
            if (ResponsePlanillas.length > 0) {
                $scope.DataArchivo.forEach(x => {
                    ResponsePlanillas.forEach(xPlanilla => {
                        if (x.Omitido == false && x.Error == 0) {
                            if (xPlanilla.Numero == x.Planilla.Numero) {
                                if (xPlanilla.Entrega != '' || xPlanilla.DT != '') {
                                    x.Error = 1;
                                    x.Omitido = true;
                                    x.stRow = 'background:yellow';
                                    x.msGeneral = 'Ya se ha cargado previamente un Número DT y Entrega para la remesa de esta planilla'
                                    ContErroresCargue++;
                                    x.NoActualizable = 1
                                }
                            }
                        }
                    })
                })
                $scope.DataArchivo.forEach(x => {
                    ResponsePlanillas.forEach(xPlanilla => {
                        if (x.Omitido == false && x.Error == 0) {
                            if (xPlanilla.Numero == x.Planilla.Numero) {
                                if (xPlanilla.NumeroFacturaRemesa > 0) {
                                    x.Error = 1;
                                    x.Omitido = true;
                                    x.stRow = 'background:yellow';
                                    x.msGeneral = 'La remesa de esta planilla ya está asociada a una factura'
                                    ContErroresCargue++;
                                    x.NoActualizable = 1
                                }
                            }
                        }
                    })
                })
            } else {
                $scope.DataArchivo.forEach(x => {
                    if (x.Omitido == false && x.Error == 0) {
                        if (x.NoActualizable > 0) {
                            x.Error = 1;
                            x.Omitido = true;
                            x.stRow = 'background:yellow';
                            x.msGeneral = x.msGeneral
                            ContErroresCargue++;
                            x.NoActualizable = 1
                        }
                    }
                })
            }

            try {
                for (var i = 0; i < $scope.DataArchivo.length; i++) {
                    if ($scope.DataArchivo[i].Error == 0) {
                        for (var j = 0; j < $scope.DataArchivo.length; j++) {
                            if (j != i) {
                                if ($scope.DataArchivo[i].Planilla.NumeroDocumento == $scope.DataArchivo[j].Planilla.NumeroDocumento) {
                                    if ($scope.DataArchivo[i].Omitido == false && $scope.DataArchivo[j].Omitido == false) {
                                        $scope.DataArchivo[i].Error = 1
                                        $scope.DataArchivo[i].stRow = 'background:yellow'
                                        $scope.DataArchivo[i].msGeneral = 'El número de planilla se encuentra duplicado en la lista'
                                        $scope.DataArchivo[j].Error = 1
                                        $scope.DataArchivo[j].stRow = 'background:yellow'
                                        $scope.DataArchivo[j].msGeneral = 'El número de planilla se encuentra duplicado en la lista'
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (e) {
                ShowError('Error en la validación de duplicados: ' + e);
            }


            $scope.ListaErrores = $linq.Enumerable().From($scope.DataArchivo).Where('$.Error==1 && $.Omitido == false').ToArray();
            $scope.ListaDatosCorrectos = $linq.Enumerable().From($scope.DataArchivo).Where('$.Error!=1 && $.Omitido == false').ToArray();

            if ($scope.ListaErrores.length > 0 || ContErroresCargue > 0) {
                ShowError('Se encontraron inconsistencias en los datos ingresados, por favor corrija los datos marcados o seleccione omitirlos.');
                $scope.paginaActualError = 1
                $scope.DataErrorActual = []
                $scope.totalRegistrosErrores = $scope.ListaErrores.length
                $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 20);
                if ($scope.totalRegistrosErrores > 20) {
                    for (var i = 0; i < 20; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    for (var i = 0; i < $scope.ListaErrores.length; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            } else if ($scope.ListaDatosCorrectos.length <= 0) {
                ShowError('No se encontraron registros para validar, por favor revise que la casilla de omitir no se encuentre marcada en al menos un registro')
            }
            else {


                showModal('modalConfirmacionGuardarPorgramacion')


                blockUI.stop();
            }
        }


        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            if ($scope.DataArchivo.Hoja1 != undefined) {
                $('#TablaCarge').show();
                $('#btnValida').show();

                $scope.DataArchivo = { Valores: $scope.DataArchivo.Hoja1 }
                AsignarValoresTabla()


            }
            else {
                ShowError('El arhivo cargado no corresponde al formato de carge masivo de las programaciones')
            }
            blockUI.stop()

        }

        $scope.handleFile = function (e) {

            var files = e.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = new Uint8Array(e.target.result);
                $scope.arr = fixdata(data);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(data, { type: 'array' });
                        //process_wb($scope.wb);
                        var worksheet = $scope.wb.Sheets[$scope.wb.SheetNames[0]];
                        var sheet = X.utils.sheet_to_json(worksheet, { header: 1 });
                        AsignarValoresTabla(sheet)

                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);

            });

        }


        $scope.CargueMasivo = function () {
            PantallaBloqueo($scope.Guardar)
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarPorgramacion')

            // se crean los terceros que se identifiquen como nuevos y se actualizan los códigos en DataArchivo:
            var ListaPlanillasGuardar = [];
            $scope.ListaDatosCorrectos.forEach(item => {
                ListaPlanillasGuardar.push({
                    Numero: item.Planilla.Numero,
                    DT: item.DT,
                    Entrega: item.Entrega
                });
            });

            PlanillaDespachosFactory.ActualizarEntregayDTRemesasporPlanilla({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, PlanillasDespacho: ListaPlanillasGuardar }).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        ShowSuccess('Se actualizaron las remesas de ' + response.data.Datos + ' planillas')
                        $scope.DataActual = []
                        $scope.DataArchivo = []
                        $scope.ListaDatosCorrectos = []
                        $scope.ListaErrores = []
                        blockUI.stop()
                    } else {
                        ShowError(response.data.MensajeOperacion)
                        blockUI.stop()
                    }
                }), function (response) {
                    ShowError(response.statusText)
                    blockUI.stop()
                };
            blockUI.stop()
        }

        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        function DatosRequeridos() {

        }
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (o) {
            return MascaraNumero(o)
        }
        $scope.MaskValoresGrid = function (o) {
            return MascaraValores(o)
        }
        $scope.MaskMayusGrid = function (o) {
            return MascaraMayus(o)
        }
        $scope.MaskplacaGrid = function (o) {
            return MascaraPlaca(o)
        }
        $scope.MaskplacaSemiremolqueGrid = function (o) {
            return MascaraPlacaSemirremolque(o)
        }

        $scope.GenerarPlanilla = function () {
            var entidad = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
            blockUI.start();
            $timeout(function () {
                blockUI.message('Generando Plantilla..');
            }, 1000);
            CargueMasivoRecoleccionesFactory.GenerarPlantilla(entidad).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var pdfAsDataUri = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data.Datos.Plantilla;
                    var link = document.createElement('a');
                    link.href = pdfAsDataUri;
                    link.download = "Plantilla_Cargue_Masivo_Recolecciones_Cliente.xlsx";
                    link.click();
                    //window.open(pdfAsDataUri);
                    blockUI.stop();
                    ShowSuccess('La plantilla se generó correctamente')
                    blockUI.stop();
                }
            }, function (response) {
                blockUI.stop();
                ShowError(response.statusText)
            })

        }
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);


        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/



        $scope.ObtenerTercero = function () {
            if ($scope.Cliente.Codigo != undefined) {
                PantallaBloqueoTercero(ObtenerTercero)
            }

        }
        function ObtenerTercero() {
            if ($scope.Cliente != undefined && $scope.Cliente != null && $scope.Cliente != '') {
                if ($scope.Cliente.Codigo > 0) {
                    $scope.Cliente = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Cliente.Codigo, Sync: true }).Datos;
                    $scope.Cliente.Cliente.Tarifario = TarifarioVentasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Cliente.Cliente.Tarifario.Codigo, Sync: true }).Datos;
                    console.log('Tarifario : ' + $scope.Cliente.Cliente.Tarifario)
                }
            }
            blockUI.stop()
        }



        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Guardando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        function PantallaBloqueoValidacion(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Validando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        function PantallaBloqueoTercero(fun) {
            blockUI.stop()
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Validando información Cliente...");
            $timeout(function () { blockUI.message("Validando información Cliente..."); fun(); }, 100);
        }
        function PantallaBloqueoTarifario(fun) {

            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Consultando información tarifario paquetería...");
            $timeout(function () { blockUI.message("Consultando información tarifario paquetería..."); fun(); }, 100);
        }
    }]);

