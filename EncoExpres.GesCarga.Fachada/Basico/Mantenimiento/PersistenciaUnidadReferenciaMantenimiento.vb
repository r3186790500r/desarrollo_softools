﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento

    Public NotInheritable Class PersistenciaUnidadReferenciaMantenimiento
        Implements IPersistenciaBase(Of UnidadReferenciaMantenimiento)
        Public Function Consultar(filtro As UnidadReferenciaMantenimiento) As IEnumerable(Of UnidadReferenciaMantenimiento) Implements IPersistenciaBase(Of UnidadReferenciaMantenimiento).Consultar
            Return New RepositorioUnidadReferenciaMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As UnidadReferenciaMantenimiento) As Long Implements IPersistenciaBase(Of UnidadReferenciaMantenimiento).Insertar
            Return New RepositorioUnidadReferenciaMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As UnidadReferenciaMantenimiento) As Long Implements IPersistenciaBase(Of UnidadReferenciaMantenimiento).Modificar
            Return New RepositorioUnidadReferenciaMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As UnidadReferenciaMantenimiento) As UnidadReferenciaMantenimiento Implements IPersistenciaBase(Of UnidadReferenciaMantenimiento).Obtener
            Return New RepositorioUnidadReferenciaMantenimiento().Obtener(filtro)
        End Function

    End Class
End Namespace
