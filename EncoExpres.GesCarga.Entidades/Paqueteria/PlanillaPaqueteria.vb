﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa

Namespace Paqueteria
    <JsonObject>
    Public Class PlanillaPaqueteria
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            Planilla = New Planillas With {
                .Numero = Read(lector, "Numero"),
                .NumeroDocumento = Read(lector, "Numero_Documento"),
                .Fecha = Read(lector, "Fecha"),
                .Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "VEHI_Placa")},
                .Conductor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .NombreCompleto = Read(lector, "NombreConductor")},
                .Tenedor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .NombreCompleto = Read(lector, "NombreTenedor")},
                .Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "SEMI_Placa")},
                .FechaHoraSalida = Read(lector, "Fecha_Hora_Salida"),
                .LineaNegocioTransporte = New LineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo")},
                .TipoLineaNegocioTransportes = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo")},
                .TarifaTransportes = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "TATC_Nombre")},
                .TipoTarifaTransportes = New TipoTarifaTransportes With {.Codigo = Read(lector, "TTTC_Codigo"), .Nombre = Read(lector, "TTTC_Nombre")},
                .Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "RUTA_Nombre"), .TipoRuta = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIRU_Codigo")}},
                .TarifarioCompras = Read(lector, "ETCC_Numero"),
                .Cantidad = Read(lector, "Cantidad"),
                .Peso = Read(lector, "Peso"),
                .ValorFleteTransportador = Read(lector, "Valor_Flete_Transportador"),
                .ValorAnticipo = Read(lector, "Valor_Anticipo"),
                .ValorImpuestos = Read(lector, "Valor_Impuestos"),
                .ValorPagarTransportador = Read(lector, "Valor_Pagar_Transportador"),
                .ValorFleteCliente = Read(lector, "Valor_Flete_Cliente"),
                .AnticipoPagadoA = Read(lector, "Anticipo_Pagado_A"),
                .PagoAnticipo = Read(lector, "PagoAnticipo"),
                .RemesaPadre = Read(lector, "ENRE_Numero_Documento_Masivo"),
                .RemesaMasivo = Read(lector, "ENRE_Numero_Masivo"),
                .NumeroCumplido = Read(lector, "ECPD_Numero"),
                .NumeroManifiesto = Read(lector, "Manifiesto"),
                .NumeroLiquidacion = Read(lector, "ELPD_NumeroDocumento"),
                .NumeroLegalizacionGastos = Read(lector, "ELGC_NumeroDocumento"),
                .NumeroLegalizarRecaudo = Read(lector, "ELGC_NumeroDocumento"),
                .FechaEntrega = Read(lector, "Fecha_Recibe")
            }
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo")}
            Observaciones = Read(lector, "Observaciones")
            RecibidoOficina = Read(lector, "Recibido_Oficina")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            NumeroPrecintoPaqueteria = Read(lector, "Precinto_Paqueteria")
            OficinaDestino = New Oficinas With {.Codigo = Read(lector, "OFIC_Destino")}

            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub

        <JsonProperty>
        Public Property TipoConsulta As Integer
        <JsonProperty>
        Public Property Planilla As Planillas
        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas
        <JsonProperty>
        Public Property ListadoPlanillaRecolecciones As IEnumerable(Of DetallePlanillaRecolecciones)
        <JsonProperty>
        Public Property ConsultaPLanillasenRuta As Short
        <JsonProperty>
        Public Property Conductores As IEnumerable(Of Terceros)
        <JsonProperty>
        Public Property NumeroLegalizacion As Integer
        <JsonProperty>
        Public Property RecibidoOficina As Integer
        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)
        <JsonProperty>
        Public Property Manifiesto As Manifiesto
        <JsonProperty>
        Public Property NumeroPrecintoPaqueteria As Integer
        <JsonProperty>
        Public Property OficinaDestino As Oficinas 'FP 2021 12 21 Agrega oficina destino a planilla paqueteria
        <JsonProperty>
        Public Property ParametrosCalculos As ParametrosCalculos
    End Class

    Public Class ParametrosCalculos
        Inherits Base

        Sub New()
        End Sub

        <JsonProperty>
        Public Property SugerirAnticipoPlanilla As Boolean
        <JsonProperty>
        Public Property DeshabilitarAnticipoPlanillaPaqueteria As Boolean
        <JsonProperty>
        Public Property FleteTarifaTransporte As Double
        <JsonProperty>
        Public Property ListadoRemesasFiltradas As IEnumerable(Of Remesas)
        <JsonProperty>
        Public Property ListadoRemesasGuardadas As IEnumerable(Of Remesas)
        <JsonProperty>
        Public Property ListadoImpuestos As IEnumerable(Of DetalleImpuestosPlanilla)
        <JsonProperty>
        Public Property ListadoTotalesRemesas As IEnumerable(Of TotalesRemesa)

    End Class
    Public Class TotalesRemesa
        Inherits Base

        Sub New()
        End Sub

        <JsonProperty>
        Public Property Cantidad As Integer
        <JsonProperty>
        Public Property ValorCliente As Double
        <JsonProperty>
        Public Property ValorTransportador As Double
        <JsonProperty>
        Public Property ValorSeguro As Double
        <JsonProperty>
        Public Property FormaPago As String

    End Class
End Namespace

