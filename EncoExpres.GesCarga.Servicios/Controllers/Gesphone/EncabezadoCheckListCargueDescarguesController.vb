﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Negocio.Gesphone

''' <summary>
''' Controlador <see cref="EncabezadoCheckListCargueDescarguesController"/>
''' </summary>
<Authorize>
Public Class EncabezadoCheckListCargueDescarguesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoCheckListCargueDescargues) As Respuesta(Of IEnumerable(Of EncabezadoCheckListCargueDescargues))
        Return New LogicaEncabezadoCheckListCargueDescargues(CapaPersistenciaEncabezadoCheckListCargueDescargues, CapaPersistenciaDetalleCheckListCargueDescargues).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoCheckListCargueDescargues) As Respuesta(Of EncabezadoCheckListCargueDescargues)
        Return New LogicaEncabezadoCheckListCargueDescargues(CapaPersistenciaEncabezadoCheckListCargueDescargues, CapaPersistenciaDetalleCheckListCargueDescargues).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoCheckListCargueDescargues) As Respuesta(Of Long)
        Return New LogicaEncabezadoCheckListCargueDescargues(CapaPersistenciaEncabezadoCheckListCargueDescargues, CapaPersistenciaDetalleCheckListCargueDescargues).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EncabezadoCheckListCargueDescargues) As Respuesta(Of Boolean)
        Return New LogicaEncabezadoCheckListCargueDescargues(CapaPersistenciaEncabezadoCheckListCargueDescargues, CapaPersistenciaDetalleCheckListCargueDescargues).Anular(entidad)
    End Function
End Class
