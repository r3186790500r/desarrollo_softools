﻿PRINT 'gsp_modificar_recolecciones'
GO
DROP PROCEDURE gsp_modificar_recolecciones
GO
CREATE PROCEDURE gsp_modificar_recolecciones 
(      
  @par_EMPR_Codigo SMALLINT,   
  @par_Numero NUMERIC,  
  @par_TIDO_Codigo NUMERIC = NULL,         
  @par_Numero_Documento NUMERIC = NULL,   
  @par_Fecha DATE = NULL,    
  @par_TERC_Codigo_Cliente NUMERIC = NULL,                
  @par_Contacto VARCHAR(50) = NULL,   
  @par_CIUD_Codigo NUMERIC = NULL,     
  @par_ZOCI_Codigo NUMERIC = NULL,   
  @par_Barrio VARCHAR(50) = NULL,        
  @par_Direccion VARCHAR(150) = NULL,        
  @par_Telefonos VARCHAR(100) = NULL,    
  @par_Mercancia VARCHAR(250) = NULL,        
  @par_UEPT_Codigo NUMERIC = NULL,   
  @par_Cantidad NUMERIC = NULL,        
  @par_Peso NUMERIC = NULL,        
  @par_Observaciones VARCHAR(500) = NULL,        
  @par_OFIC_Codigo NUMERIC = NULL,       
  @par_Estado SMALLINT,      
  @par_USUA_Codigo_Modifica SMALLINT      
)      
AS      
BEGIN      
 UPDATE      
  Encabezado_Recolecciones      
 SET      
  EMPR_Codigo = @par_EMPR_Codigo,    
  TIDO_Codigo = @par_TIDO_Codigo,         
  Numero_Documento = @par_Numero_Documento,   
  Fecha = @par_Fecha,    
  TERC_Codigo_Cliente = @par_TERC_Codigo_Cliente,                
  Nombre_Contacto = @par_Contacto,   
  CIUD_Codigo = @par_CIUD_Codigo,     
  ZOCI_Codigo = @par_ZOCI_Codigo,   
  Barrio = @par_Barrio,        
  Direccion = @par_Direccion,        
  Telefonos = @par_Telefonos,    
  Mercancia = @par_Mercancia,        
  UEPT_Codigo = @par_UEPT_Codigo,   
  Cantidad = @par_Cantidad,        
  Peso = @par_Peso,        
  Observaciones = @par_Observaciones,        
  OFIC_Codigo = @par_OFIC_Codigo,   
  Estado = @par_Estado,      
  Fecha_Modifica = GETDATE(),    
  USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica    
  
 WHERE      
  EMPR_Codigo = @par_EMPR_codigo     
  AND Numero = @par_Numero        

  SELECT Numero, Numero_Documento FROM Encabezado_Recolecciones WHERE EMPR_Codigo = @par_EMPR_codigo AND Numero = @par_Numero    
      
END      
-----------------------------------------------------

PRINT 'gsp_consultar_recolecciones'
GO