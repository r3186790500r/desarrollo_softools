﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


	PRINT 'gsp_Insertar_Impuestos'
GO
DROP PROCEDURE gsp_Insertar_Impuestos
GO
CREATE PROCEDURE gsp_Insertar_Impuestos(  
@par_EMPR_Codigo NUMERIC,  

@par_Codigo_Alterno VARCHAR(20),   
@par_Nombre VARCHAR(100),   
@par_Label VARCHAR(50),   
@par_CATA_TRAI_Codigo NUMERIC,
@par_CATA_TIIM_Codigo NUMERIC,
@par_Operacion CHAR,
@par_PLUC_Codigo NUMERIC,
@par_Valor_Tarifa NUMERIC(18,6),
@par_Valor_Base NUMERIC,
@par_Estado SMALLINT,
@par_USUA_Codigo_Crea SMALLINT
) 
AS BEGIN 

DECLARE @intCodigo NUMERIC = ( select MAX(Codigo) from Encabezado_Impuestos)
 

		INSERT INTO Encabezado_Impuestos(
		EMPR_Codigo,
		Codigo,
		Codigo_Alterno,
		Nombre,
		Label,
		CATA_TRAI_Codigo,
		CATA_TIIM_Codigo,
		Operacion,
		PLUC_Codigo,
		Valor_Tarifa,
		Valor_Base,
		Estado,
		USUA_Codigo_Crea,
		Fecha_Crea)  
		VALUES(
		@par_EMPR_Codigo,
		ISNULL(@intCodigo,0)+1,
		@par_Codigo_Alterno ,   
		@par_Nombre ,   
		@par_Label ,   
		@par_CATA_TRAI_Codigo ,
		@par_CATA_TIIM_Codigo ,
		@par_Operacion ,
		@par_PLUC_Codigo ,
		@par_Valor_Tarifa ,
		@par_Valor_Base ,
		@par_Estado ,
		@par_USUA_Codigo_Crea, 
		GETDATE())  
		select @@ROWCOUNT as Codigo
 
END
GO