﻿PRINT 'Tercero_Empleados'
GO
DROP TABLE Tercero_Empleados
GO
CREATE TABLE Tercero_Empleados(
 EMPR_Codigo smallint NOT NULL,
 TERC_Codigo numeric(18, 0) NOT NULL,
 CATA_CARG_Codigo numeric NULL,
 Fecha_Vinculacion datetime NULL,
 Salario money NULL,
 Valor_Auxilio_Transporte money NULL,
 Valor_Seguridad_Social money NULL,
 Valor_Aporte_Parafiscales money NULL,
 Valor_Provision_Prestaciones_Sociales money NULL,
 Empleado_Externo smallint NULL,
 Fecha_Finalizacion DATE NULL,
CATA_TICE_Codigo NUMERIC NOT NULL,
Porcentaje_Comision NUMERIC NULL,
Valor_Seguro_Vida MONEY NULL,
 CONSTRAINT [PK_Empleados] PRIMARY KEY CLUSTERED 
(
 EMPR_Codigo ASC,
 TERC_Codigo ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE Tercero_Empleados  WITH CHECK ADD  CONSTRAINT FK_Empleados_Terceros FOREIGN KEY(EMPR_Codigo,TERC_Codigo)
REFERENCES Terceros (EMPR_Codigo, Codigo)
GO

ALTER TABLE Tercero_Empleados CHECK CONSTRAINT FK_Empleados_Terceros
GO