﻿EncoExpresApp.factory('RecepcionGuiasFactory', ['$http', function ($http) {

    var urlService = GetUrl('RecepcionGuias');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.RecibirGuias = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/RecibirGuias';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
 
    return factory;
}]);