﻿	/*Crea:Johan Sebastian Borda Borda
	Fecha_Crea:19/03/2021
	Descripción_Crea:
	Modifica:Johan Sebastian Borda Borda
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


PRINT 'gsp_reporte_encabezado_legalizacion_recaudo_guias '
GO

DROP PROCEDURE gsp_reporte_encabezado_legalizacion_recaudo_guias
GO  
  
CREATE PROCEDURE gsp_reporte_encabezado_legalizacion_recaudo_guias             
(                
	 @par_EMPR_Codigo SMALLINT,    
	 @par_Numero NUMERIC,    
	 @par_Usuario NUMERIC  
)                
AS                 
BEGIN                
	 SELECT   
	 -- INFORMACIÓN EMPRESA      
	 EMPR.Nombre_Razon_Social AS NombreEmpresa,                     
	 EMPR.Numero_Identificacion AS IdentificacionEmpresa,                      
	 EMPR.Direccion AS DireccionEmpresa,      
	 EMPR.Telefonos AS TelefonoEmpresa,  
	 USIM.Nombre AS USUA_Imprime,  
   
	 --INFORMACION ENCABEZADO  
  
	 OFIC.Nombre AS Oficina,      
	 ISNULL(USMO.Nombre, USUA.Nombre) USUA_Modifica,  
	 ELRG.Numero,            
	 ELRG.Numero_Documento,            
	 ELRG.Fecha,  
	 ELRG.Valor_Contado,  
	 ELRG.Valor_Contra_Entrega,  
	 ELRG.Observaciones,  
	 ELRG.Estado,  
	 ELRG.Anulado  
  
	 FROM [dbo].[Encabezado_Legalizacion_Recaudo_Guias] ELRG    
   
	 LEFT JOIN Empresas EMPR    
	 ON ELRG.EMPR_Codigo = EMPR.Codigo    
      
	 LEFT JOIN Usuarios USIM           
	 ON USIM.EMPR_Codigo = @par_EMPR_Codigo          
	 AND USIM.Codigo = @par_Usuario    
  
	 LEFT JOIN Usuarios USUA    
	 ON USUA.EMPR_Codigo = ELRG.EMPR_Codigo   
	 AND USUA.Codigo = ELRG.USUA_Codigo_Crea    
    
	 LEFT JOIN Usuarios USMO    
	 ON USMO.EMPR_Codigo = ELRG.EMPR_Codigo   
	 AND USMO.Codigo = ELRG.USUA_Codigo_Modifica    
    
	 LEFT JOIN Oficinas OFIC    
	 ON ELRG.EMPR_Codigo = OFIC.EMPR_Codigo    
	 AND ELRG.OFIC_Codigo = OFIC.Codigo    
  
	 WHERE          
	 ELRG.EMPR_CODIGO = @par_EMPR_Codigo                
	 AND ELRG.NUMERO = @par_Numero        
   
 END   
 GO