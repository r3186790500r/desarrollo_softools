﻿Print 'gsp_Listado_Planilla_Despachos'
GO
DROP PROCEDURE gsp_Listado_Planilla_Despachos
GO          
CREATE PROCEDURE gsp_Listado_Planilla_Despachos           
(@par_EMPR_Codigo NUMERIC,              
@par_Numero_Documento_Inicial NUMERIC = null,              
@par_Numero_Documento_Final NUMERIC = null,              
@par_Fecha_Incial DATE = NULL,               
@par_Fecha_Final DATE = NULL,               
@par_Cliente VARCHAR(MAX) = NULL,               
@par_OFIC_Codigo NUMERIC = NULL ,              
@par_Estado NUMERIC = NULL ,              
@par_Ruta VARCHAR(50) = NULL )             
AS           
 BEGIN       
 
 	DECLARE @intAnulado SMALLINT = NULL
	SET @intAnulado = CASE @par_Estado WHEN NULL THEN NULL WHEN 2 THEN 1 ELSE 0 END
    SELECT @par_Estado = IIF (@par_Estado = 2,NULL,@par_Estado)
	    
SELECT           
EMPR.Nombre_Razon_Social              
,ENPD.Numero_Documento          
,ENPD.Fecha           
,VEHI.Placa          
,TIVE.Campo1 TipoVehiculo          
, ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'')                
+' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS NombreTenedor             
,TENE.Numero_Identificacion IdentificacionTenedor          
, ISNULL(COND.Razon_Social,'')+' '+ISNULL(COND.Nombre,'')                
+' '+ISNULL(COND.Apellido1,'')+' '+ISNULL(COND.Apellido2,'') AS NombreConductor              
,COND.Numero_Identificacion IdentificacionConductor          
          
,COND.Celulares           
,RUTA.Nombre ruta          
,ENPD.Valor_Flete_Transportador          
,ENPD.Valor_Anticipo           
,(ENPD.Valor_Flete_Transportador - ENPD.Valor_Flete_Transportador) AS ValorAPagar        
,OFIC.Nombre OFICINA           
,ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial              
,ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal              
,ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial              
,ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal              
,CASE ENPD.Anulado WHEN 1 THEN 'A' ELSE           
  CASE ENPD.ESTADO WHEN 1 THEN 'D' ELSE 'B'          
    END           
END as Estado          
          
 FROM Encabezado_Planilla_Despachos ENPD          
 INNER JOIN Empresas EMPR ON           
 ENPD.EMPR_Codigo = EMPR.Codigo           
          
 LEFT JOIN Vehiculos VEHI ON           
 ENPD.EMPR_Codigo = VEHI.EMPR_Codigo          
 AND  ENPD.VEHI_Codigo = VEHI.Codigo          
          
 LEFT JOIN Valor_Catalogos TIVE ON           
 VEHI.EMPR_Codigo = TIVE.EMPR_Codigo          
 AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo          
          
 LEFT JOIN Terceros TENE ON           
 ENPD.EMPR_Codigo = TENE.EMPR_Codigo           
 AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo          
           
 LEFT JOIN Terceros COND ON           
 ENPD.EMPR_Codigo = COND.EMPR_Codigo           
 AND ENPD.TERC_Codigo_Conductor = COND.Codigo          
          
          
          
 LEFT JOIN Rutas RUTA ON           
 ENPD.EMPR_Codigo = RUTA.EMPR_Codigo           
 AND ENPD.RUTA_Codigo = RUTA.Codigo           
          
 LEFT JOIN Oficinas OFIC ON           
 ENPD.EMPR_Codigo = OFIC.EMPR_Codigo           
 AND ENPD.OFIC_Codigo = OFIC.Codigo          
          
          
 WHERE ENPD.EMPR_Codigo = @par_EMPR_Codigo          
 AND ENPD.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENPD.Numero_Documento)              
 AND ENPD.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENPD.Numero_Documento)                
 AND ENPD.Fecha >= ISNULL( @par_Fecha_Incial,ENPD.Fecha)               
 AND ENPD.Fecha <= ISNULL( @par_Fecha_Final,ENPD.Fecha)               
 AND ENPD.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENPD.OFIC_Codigo)
 AND (TENE.Nombre + ' ' +TENE.Apellido1 + ' ' + TENE.Apellido2 + ' ' + TENE.Razon_Social LIKE '%'+@par_Cliente+'%' or @par_Cliente is null)               
 AND ENPD.Estado = ISNULL (@par_Estado,ENPD.Estado)              
 AND ENPD.Anulado = ISNULL(@intAnulado, ENPD.Anulado)
 AND (RUTA.Nombre LIKE '%'+@par_Ruta+'%' or @par_Ruta is null)      
        
 END          
 
GO