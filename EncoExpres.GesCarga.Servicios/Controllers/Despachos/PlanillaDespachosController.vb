﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo
Imports EncoExpres.GesCarga.Negocio.Despachos.Masivo
Imports EncoExpres.GesCarga.Negocio.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla

''' <summary>
''' Controlador <see cref="PlanillaDespachosController"/>
''' </summary>
<Authorize>
Public Class PlanillaDespachosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PlanillaDespachos) As Respuesta(Of IEnumerable(Of PlanillaDespachos))
        Return New LogicaPlanillaDespachos(CapaPersistenciaPlanillaDespachos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PlanillaDespachos) As Respuesta(Of PlanillaDespachos)
        Return New LogicaPlanillaDespachos(CapaPersistenciaPlanillaDespachos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PlanillaDespachos) As Respuesta(Of Long)
        Return New LogicaPlanillaDespachos(CapaPersistenciaPlanillaDespachos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PlanillaDespachos) As Respuesta(Of PlanillaDespachos)
        Return New LogicaPlanillaDespachos(CapaPersistenciaPlanillaDespachos).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("GenerarManifiesto")>
    Public Function GenerarManifiesto(ByVal entidad As Manifiesto) As Respuesta(Of Long)
        Return New LogicaManifiestos(CapaPersistenciaManifiestos).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Obtener_Detalle_Tiempos")>
    Public Function Obtener_Detalle_Tiempos(ByVal entidad As PlanillaDespachos) As Respuesta(Of IEnumerable(Of DetalleTiemposPlanillaDespachos))
        Return New LogicaPlanillaDespachos(CapaPersistenciaPlanillaDespachos).Obtener_Detalle_Tiempos(entidad)
    End Function

    <HttpPost>
    <ActionName("Obtener_Detalle_Tiempos_Remesas")>
    Public Function Obtener_Detalle_Tiempos_Remesas(ByVal entidad As PlanillaDespachos) As Respuesta(Of IEnumerable(Of DetalleTiemposPlanillaDespachos))
        Return New LogicaPlanillaDespachos(CapaPersistenciaPlanillaDespachos).Obtener_Detalle_Tiempos_Remesas(entidad)
    End Function

    <HttpPost>
    <ActionName("Insertar_Detalle_Tiempos")>
    Public Function Insertar_Detalle_Tiempos(ByVal entidad As PlanillaDespachos) As Respuesta(Of Long)
        Return New LogicaPlanillaDespachos(CapaPersistenciaPlanillaDespachos).Insertar_Detalle_Tiempos(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarListaPlanillas")>
    Public Function ConsultarListaPlanillas(ByVal entidad As PlanillaDespachos) As Respuesta(Of IEnumerable(Of PlanillaDespachos))
        Return New LogicaPlanillaDespachos(CapaPersistenciaPlanillaDespachos).ConsultarListaPlanillas(entidad)
    End Function

    <HttpPost>
    <ActionName("ActualizarEntregayDTRemesasporPlanilla")>
    Public Function ActualizarEntregayDTRemesasporPlanilla(ByVal entidad As PlanillaDespachos) As Respuesta(Of Long)
        Return New LogicaPlanillaDespachos(CapaPersistenciaPlanillaDespachos).ActualizarEntregayDTRemesasporPlanilla(entidad)
    End Function
End Class
