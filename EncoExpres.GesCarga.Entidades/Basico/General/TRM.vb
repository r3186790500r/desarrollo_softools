﻿Imports Newtonsoft.Json

Namespace Basico.General


    ''' <summary>
    ''' Clase <see cref="TRM"/>
    ''' </summary>

    <JsonObject>
    Public NotInheritable Class TRM

        Inherits BaseBasico
        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TRM"/>
        ''' </summary>
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Nombre_Corto = Read(lector, "Nombre_Corto")
            CodigoMoneda = New Monedas With {.Codigo = Read(lector, "MONE_Codigo")}
            Try
                Fecha = Read(lector, "Fecha")
                TotalRegistros = Read(lector, "TotalRegistros")
                PaginaObtener = Read(lector, "PaginaObtener")
                RegistrosPagina = Read(lector, "RegistrosPagina")
            Catch ex As Exception

            End Try
            Valor_Moneda_Local = Read(lector, "Valor_Moneda_Local")

        End Sub

        <JsonProperty>
        Public Property Indicador As Integer

        <JsonProperty>
        Public Property Valor_Moneda_Local As Integer

        <JsonProperty>
        Public Property CodigoMoneda As Monedas

        <JsonProperty>
        Public Property ValorMonedaLocal As Double

        <JsonProperty>
        Public Property UsuarioCodigo As Double

        <JsonProperty>
        Public Property Fecha As DateTime

        <JsonProperty>
        Public Property Nombre_Corto As String

        <JsonProperty>
        Public Property Lista As IEnumerable(Of TRM)

        <JsonProperty>
        Public Property PaginaObtener As Integer

    End Class

End Namespace