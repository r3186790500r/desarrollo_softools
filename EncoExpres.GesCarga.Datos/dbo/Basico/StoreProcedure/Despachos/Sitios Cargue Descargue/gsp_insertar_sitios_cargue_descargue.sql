﻿PRINT 'gsp_insertar_sitios_cargue_descargue'
GO
DROP PROCEDURE gsp_insertar_sitios_cargue_descargue
GO
CREATE PROCEDURE gsp_insertar_sitios_cargue_descargue  
(  
@par_EMPR_Codigo SMALLINT ,    
@par_Nombre VARCHAR (50),  
@par_Codigo_Tipo_Sitio NUMERIC,
@par_Codigo_Pais NUMERIC,
@par_Codigo_Ciudad NUMERIC,
@par_Direccion VARCHAR (150),  
@par_Codigo_Postal VARCHAR (50) = NULL,  
@par_Telefono VARCHAR (100),  
@par_Contacto VARCHAR (50) = NULL,  
@par_Estado SMALLINT,  
@par_USUA_Codigo_Crea SMALLINT  
)  
AS   
BEGIN  
 INSERT INTO Sitios_Cargue_Descargue   
 (  
 EMPR_Codigo,  
 Nombre,  
 CATA_TSCD_Codigo,
 PAIS_Codigo,
 CIUD_Codigo,
 Direccion,
 Codigo_Postal,
 Telefono,
 Contacto,
 Estado,  
 USUA_Codigo_Crea,  
 Fecha_Crea  
 )  
 VALUES  
 (  
 @par_EMPR_Codigo,  
 @par_Nombre, 
 @par_Codigo_Tipo_Sitio, 
 @par_Codigo_Pais,
 @par_Codigo_Ciudad,
 @par_Direccion,
 @par_Codigo_Postal,
 @par_Telefono,
 @par_Contacto,
 @par_Estado,  
 @par_USUA_Codigo_Crea,  
 GETDATE()  
 )  
 SELECT Codigo = @@identity  
  
END  
GO