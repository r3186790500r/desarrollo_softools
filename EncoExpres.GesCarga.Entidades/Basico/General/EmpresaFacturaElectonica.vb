﻿Imports Newtonsoft.Json
Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="EmpresaFacturaElectonica"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EmpresaFacturaElectonica
        Inherits BaseDocumento
        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="EmpresaFacturaElectonica"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EmpresaFacturaElectonica"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "Codigo_Empresa")
            Proveedor = New ValorCatalogos With {.Codigo = Read(lector, "PRFE_Codigo"), .Nombre = Read(lector, "PRFE_Nombre")}
            Usuario = Read(lector, "Usuario")
            Clave = Read(lector, "Clave")
            OperadorVirtual = Read(lector, "Operador_Virtual")
            PrefijoFactura = Read(lector, "Prefijo_Factura")
            ClaveTecnicaFactura = Read(lector, "Clave_Tecnica_Factura")
            ClaveExternaFactura = Read(lector, "Clave_Externa_Factura")
            PrefijoNotaCredito = Read(lector, "Prefijo_Nota_Credito")
            ClaveExternaNotaCredito = Read(lector, "Clave_Externa_Nota_Credito")
            PrefijoNotaDebito = Read(lector, "Prefijo_Nota_Debito")
            ClaveExternaNotaDebito = Read(lector, "Clave_Externa_Nota_Debito")
            Ambiente = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TAFE_Codigo")}
            ID_Empresa = Read(lector, "ID_Empresa")
            FirmaDigital = Read(lector, "Firma_Digital")
        End Sub

        <JsonProperty>
        Public Property Proveedor As ValorCatalogos
        <JsonProperty>
        Public Property Usuario As String
        <JsonProperty>
        Public Property Clave As String
        <JsonProperty>
        Public Property OperadorVirtual As String
        <JsonProperty>
        Public Property PrefijoFactura As String
        <JsonProperty>
        Public Property ClaveTecnicaFactura As String
        <JsonProperty>
        Public Property ClaveExternaFactura As String
        <JsonProperty>
        Public Property PrefijoNotaCredito As String
        <JsonProperty>
        Public Property ClaveExternaNotaCredito As String
        <JsonProperty>
        Public Property PrefijoNotaDebito As String
        <JsonProperty>
        Public Property ClaveExternaNotaDebito As String
        <JsonProperty>
        Public Property Ambiente As ValorCatalogos
        <JsonProperty>
        Public Property ID_Empresa As String
        <JsonProperty>
        Public Property FirmaDigital As String
    End Class

End Namespace
