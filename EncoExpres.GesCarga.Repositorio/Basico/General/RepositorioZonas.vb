﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Paqueteria

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioZonas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioZonas
        Inherits RepositorioBase(Of Zonas)
        Public Overrides Function Consultar(filtro As Zonas) As IEnumerable(Of Zonas)
            Dim lista As New List(Of Zonas)
            Dim item As Zonas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If filtro.Estado > 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Ciudad) Then
                    If filtro.Ciudad.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.Ciudad.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_zonas]")

                While resultado.Read
                    item = New Zonas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Zonas) As Long
            Dim inserto As Boolean = True
            Dim CodigoZona As String = 0
            Dim CodigoCiudad As Integer = 0
            Dim NombreZona As String = ""
            Dim CodigoEstado As String = 0
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                If Not IsNothing(entidad.ListaZonas) Then
                    For Each ListadoZonas In entidad.ListaZonas

                        CodigoZona = ListadoZonas.Codigo
                        CodigoCiudad = ListadoZonas.Ciudad.Codigo
                        NombreZona = ListadoZonas.Nombre
                        CodigoEstado = ListadoZonas.Estado

                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_Codigo", CodigoZona)
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo", CodigoCiudad)
                        conexion.AgregarParametroSQL("@par_Nombre", NombreZona)
                        conexion.AgregarParametroSQL("@par_Estado", CodigoEstado)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                        conexion.AgregarParametroSQL("@par_USUA_Modifica", entidad.UsuarioModifica.Codigo)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_zonas]")

                        While resultado.Read
                            entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                        End While

                        resultado.Close()
                    Next
                End If

            End Using

            Return entidad.Codigo
        End Function


        Public Function Anular(entidad As Zonas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo", entidad.Ciudad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_zonas]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Overrides Function Modificar(entidad As Zonas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As Zonas) As Zonas
            Throw New NotImplementedException()
        End Function

        Public Function ConsultarZonificacionGuias(filtro As Zonas) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.NumeroInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroInicial)
                End If
                If filtro.RemesaPaqueteria.NumeroPlanilla > 0 Then
                    conexion.AgregarParametroSQL("@par_Planilla", filtro.RemesaPaqueteria.NumeroPlanilla)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaInicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaFinal", filtro.FechaFinal, SqlDbType.Date)
                End If

                If filtro.CadenaCiudades.Length > 1 Then

                    conexion.AgregarParametroSQL("@par_Codigo_ciudad", filtro.CadenaCiudades)

                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_zonificacion_guias]")

                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                    lista.Add(item)
                End While



            End Using

            Return lista
        End Function

        Public Function InsertarZonificacionGuias(entidad As Zonas) As Long
            Dim inserto As Boolean = False
            Dim CONTADOR As Long = 0

            For Each listadetalleAuxi In entidad.ListaZonas
                Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_Numero", listadetalleAuxi.RemesaPaqueteria.Numero)
                        conexion.AgregarParametroSQL("@ZOCI_Codigo_Entrega", listadetalleAuxi.Numero)


                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_Zonas_Guia_paqueteria")

                        While resultado.Read
                            entidad.NumeroDocumento = resultado.Item("Numero").ToString()
                            inserto = True
                        End While
                        resultado.Close()
                    End Using
                    If inserto Then
                        transaccion.Complete()
                        CONTADOR = entidad.NumeroDocumento
                    Else
                        transaccion.Dispose()
                    End If
                End Using
                inserto = False
            Next
            entidad.NumeroDocumento = CONTADOR
            Return entidad.NumeroDocumento
        End Function


    End Class


End Namespace