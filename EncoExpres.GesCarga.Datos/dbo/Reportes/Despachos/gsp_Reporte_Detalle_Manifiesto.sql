﻿Print 'gsp_Reporte_Detalle_Manifiesto'
GO
DROP PROCEDURE gsp_Reporte_Detalle_Manifiesto
GO
CREATE PROCEDURE gsp_Reporte_Detalle_Manifiesto      
(
@par_EMPR_Codigo NUMERIC,      
@par_Numero NUMERIC      
)      
AS BEGIN       
      
      
SELECT       
ENRE.Numero_Documento AS NumeroRemesa,       
---unidad medida      
ENRE.Cantidad_Cliente ,       
ENRE.Peso_Cliente ,       
PRTR.Codigo AS CodigoProducto,      
PRTR.Nombre AS NombreProducto      
,UEPT.Nombre_Corto UnidadEmpaque      
,UMPT.Nombre_Corto UnidadMedida      
,ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'')        
+' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS TitutlarTenedor      
,TENE.Numero_Identificacion AS IdentificacionTenedor      
      
,ISNULL(TEPR.Razon_Social,'')+' '+ISNULL(TEPR.Nombre,'')        
+' '+ISNULL(TEPR.Apellido1,'')+' '+ISNULL(TEPR.Apellido2,'') AS TitutlarPropietario      
,TEPR.Numero_Identificacion AS IdentificacionPropietario       
      
,ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')        
+' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS TitutlarConductor      
      
,TECO.Numero_Identificacion AS IdentificacionConductor      
      
,ISNULL(TERE.Razon_Social,'')+' '+ISNULL(TERE.Nombre,'')        
+' '+ISNULL(TERE.Apellido1,'')+' '+ISNULL(TERE.Apellido2,'') AS NombreRemitente      
      
,TERE.Numero_Identificacion AS IdentificacionRemitente      
,CIRE.Nombre AS CiudadRemitente      
      
,ISNULL(TEDE.Razon_Social,'')+' '+ISNULL(TEDE.Nombre,'')        
+' '+ISNULL(TEDE.Apellido1,'')+' '+ISNULL(TEDE.Apellido2,'') AS NombreDestinatario      
,CIDE.Nombre AS CiudadDestinatario      
,TEDE.Numero_Identificacion AS IdentificacionDestinatario      
,NAPT.Campo1 Naturaleza      
,ENRE.Direccion_Remitente    
,ENRE.Direccion_Destinatario 
,ESOS.Horas_Pactadas_Cargue
,ESOS.Horas_Pactadas_Descargue     

FROM       
Encabezado_Manifiesto_Carga ENMA       
      
INNER JOIN Detalle_Manifiesto_Carga DEMA ON       
ENMA.EMPR_Codigo =  DEMA.EMPR_Codigo      
AND ENMA.Numero = DEMA.ENMC_Numero      
      
INNER JOIN Encabezado_Remesas ENRE ON       
DEMA.EMPR_Codigo = ENRE.EMPR_Codigo       
AND DEMA.ENRE_Numero = ENRE.Numero      

INNER JOIN Encabezado_Solicitud_Orden_Servicios ESOS ON
ENRE.EMPR_Codigo = ESOS.EMPR_Codigo
AND ENRE.ESOS_Numero = ESOS.Numero 
     
INNER JOIN Producto_Transportados PRTR ON       
ENRE.EMPR_Codigo = PRTR.EMPR_Codigo      
AND ENRE.PRTR_Codigo = PRTR.Codigo       
      
INNER JOIN Valor_Catalogos NAPT ON       
PRTR.EMPR_Codigo = NAPT.EMPR_Codigo      
AND PRTR.CATA_NAPT_Codigo = NAPT.Codigo      
      
INNER JOIN Terceros TEPR ON      
ENMA.EMPR_Codigo = TEPR.EMPR_Codigo       
AND ENMA.TERC_Codigo_Propietario = TEPR.Codigo      
      
INNER JOIN Terceros TENE ON       
ENMA.EMPR_Codigo = TENE.EMPR_Codigo       
AND ENMA.TERC_Codigo_Tenedor = TENE.Codigo      
      
INNER JOIN Terceros TECO ON      
ENMA.EMPR_Codigo = TECO.EMPR_Codigo       
AND ENMA.TERC_Codigo_Conductor = TECO.Codigo      
      
INNER JOIN Terceros TERE ON       
ENRE.EMPR_Codigo = TERE.EMPR_Codigo       
AND ENRE.TERC_Codigo_Remitente = TERE.Codigo       
      
INNER JOIN Terceros TEDE ON      
ENRE.EMPR_Codigo = TEDE.EMPR_Codigo       
AND ENRE.TERC_Codigo_Destinatario = TEDE.Codigo      
      
INNER JOIN Ciudades CIRE ON      
ENRE.EMPR_Codigo = CIRE.EMPR_Codigo       
AND ENRE.CIUD_Codigo_Remitente = CIRE.Codigo       
      
INNER JOIN Ciudades CIDE ON      
ENRE.EMPR_Codigo = CIDE.EMPR_Codigo       
AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo      
      
INNER JOIN  Unidad_Empaque_Producto_Transportados UEPT ON       
PRTR.EMPR_Codigo = UEPT.EMPR_Codigo      
AND PRTR.UEPT_Codigo = UEPT.Codigo      
      
INNER JOIN Unidad_Medida_Producto_Transportados UMPT ON       
PRTR.EMPR_Codigo = UMPT.EMPR_Codigo      
AND PRTR.UMPT_Codigo = UMPT.Codigo      
       
WHERE ENMA.EMPR_Codigo = @par_EMPR_Codigo      
AND ENMA.Numero = @par_Numero      
      
END       
GO