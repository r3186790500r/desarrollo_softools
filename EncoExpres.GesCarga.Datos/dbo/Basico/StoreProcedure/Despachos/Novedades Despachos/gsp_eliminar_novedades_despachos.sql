﻿PRINT 'gsp_eliminar_novedades_despachos'
GO
DROP PROCEDURE gsp_eliminar_novedades_despachos
GO
CREATE PROCEDURE gsp_eliminar_novedades_despachos 
(  
  @par_EMPR_Codigo SMALLINT,  
  @par_Codigo NUMERIC  
)  
AS  
BEGIN  
  
  DELETE Novedades_Despacho   
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
    AND Codigo = @par_Codigo  
  
    SELECT @@ROWCOUNT AS Codigo  
  
END  
GO
