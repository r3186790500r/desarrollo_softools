﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports System.Transactions


Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioMonedas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioMonedas
        Inherits RepositorioBase(Of Monedas)

        Public Overrides Function Consultar(filtro As Monedas) As IEnumerable(Of Monedas)
            Dim lista As New List(Of Monedas)
            Dim item As Monedas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If filtro.Nombre <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If
                If filtro.NombreCorto <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Nombre_Corto", filtro.NombreCorto)
                End If
                If filtro.Estado >= Cero Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_Monedas")

                While resultado.Read
                    item = New Monedas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Monedas) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Nombre_Corto", entidad.NombreCorto)
                    conexion.AgregarParametroSQL("@par_Simbolo", entidad.Simbolo)
                    conexion.AgregarParametroSQL("@par_Local", entidad.Local)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Insertar_Moneda")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While
                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Overrides Function Modificar(entidad As Monedas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Nombre_Corto", entidad.NombreCorto)
                conexion.AgregarParametroSQL("@par_Simbolo", entidad.Simbolo)
                conexion.AgregarParametroSQL("@par_Local", entidad.Local)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Modificar_Moneda")

                While resultado.Read
                    entidad.Codigo = resultado.Item("@par_MONE_Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Function MonedaLocals(entidad As Monedas) As Boolean
            Dim bolMonedaLocal As Boolean

            If Not IsNothing(entidad) Then
                If entidad.CodigoEmpresa > 0 Then
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Existe_Moneda_Local")

                        While resultado.Read
                            bolMonedaLocal = resultado.Item("ExisteLocal")
                        End While

                    End Using
                End If
            End If



            Return bolMonedaLocal
        End Function

        Public Overrides Function Obtener(filtro As Monedas) As Monedas
            Dim item As New Monedas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                conexion.AgregarParametroSQL("@par_Local", filtro.Local)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Obtener_Moneda")

                While resultado.Read
                    item = New Monedas(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As Monedas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Eliminar_Moneda")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace