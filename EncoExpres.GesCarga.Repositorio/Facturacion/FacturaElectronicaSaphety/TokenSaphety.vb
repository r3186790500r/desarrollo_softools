﻿Imports Newtonsoft.Json

Namespace Facturacion.FacturaElectronicaSaphety
    <JsonObject>
    Public Class TokenSaphety
        <JsonProperty>
        Public Property IsValid As Boolean
        <JsonProperty>
        Public Property access_token As String
        <JsonProperty>
        Public Property token_type As String
        <JsonProperty>
        Public Property expires As String
    End Class
End Namespace

