﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Negocio.ServicioCliente

''' <summary>
''' Controlador <see cref="DetalleSolicitudOrdenServiciosController"/>
''' </summary>
<Authorize>
Public Class DetalleSolicitudOrdenServiciosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleSolicitudOrdenServicios) As Respuesta(Of IEnumerable(Of DetalleSolicitudOrdenServicios))
        Return New LogicaDetalleSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleSolicitudOrdenServicios) As Respuesta(Of DetalleSolicitudOrdenServicios)
        Return New LogicaDetalleSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleSolicitudOrdenServicios) As Respuesta(Of Long)
        Return New LogicaDetalleSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleSolicitudOrdenServicios) As Respuesta(Of Boolean)
        Return New LogicaDetalleSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Anular(entidad)
    End Function
End Class
