﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Gesphone

    ''' <summary>
    ''' Clase que tiene la informacion de ciudades a sincronizar.
    ''' </summary>
    <JsonObject>
    Public Class SyncCiudades

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SyncCiudades"/>.
        ''' </summary>
        ''' <param name="registro"></param>
        Sub New(registro As Ciudades)
            Id = registro.Codigo
            Nom = registro.NombreCompleto
        End Sub

        ''' <summary>
        ''' Obtiene o establece el Identificador unico de la ciudad.
        ''' </summary>
        ''' <returns></returns>
        Public Property Id As Integer

        ''' <summary>
        ''' Obtiene o establece el nombre de la ciudad.
        ''' </summary>
        ''' <returns></returns>
        Public Property Nom As String

    End Class

End Namespace