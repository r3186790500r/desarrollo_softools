﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 27/10/2021 
-- Módulo: Gesphone

PRINT 'Agregar campo Novedad_Intento_Entrega en la tabla Detalle_Intentos_Entrega_Guias'
GO

IF COL_LENGTH('Detalle_Intentos_Entrega_Guias', 'Novedad_Intento_Entrega') IS NULL
BEGIN
    ALTER TABLE Detalle_Intentos_Entrega_Guias
    ADD [Novedad_Intento_Entrega] VARCHAR(250) NULL
END
ELSE
BEGIN
  PRINT 'El campo Novedad_Intento_Entrega ya existe en la tabla Detalle_Intentos_Entrega_Guias'
END
