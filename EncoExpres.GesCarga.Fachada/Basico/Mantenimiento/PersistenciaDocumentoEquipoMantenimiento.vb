﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento
    Public NotInheritable Class PersistenciaDocumentoEquipoMantenimiento
        Implements IPersistenciaBase(Of EquipoMantenimientoDocumento)
        Public Function Consultar(filtro As EquipoMantenimientoDocumento) As IEnumerable(Of EquipoMantenimientoDocumento) Implements IPersistenciaBase(Of EquipoMantenimientoDocumento).Consultar
            Return New RepositorioDocumentoEquipoMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EquipoMantenimientoDocumento) As Long Implements IPersistenciaBase(Of EquipoMantenimientoDocumento).Insertar
            Return New RepositorioDocumentoEquipoMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EquipoMantenimientoDocumento) As Long Implements IPersistenciaBase(Of EquipoMantenimientoDocumento).Modificar
            Return New RepositorioDocumentoEquipoMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EquipoMantenimientoDocumento) As EquipoMantenimientoDocumento Implements IPersistenciaBase(Of EquipoMantenimientoDocumento).Obtener
            Return New RepositorioDocumentoEquipoMantenimiento().Obtener(filtro)
        End Function

        Public Function InsertarTemporal(entidad As EquipoMantenimientoDocumento) As Long
            Return New RepositorioDocumentoEquipoMantenimiento().InsertarTemporal(entidad)
        End Function
        Public Function EliminarDocumento(entidad As EquipoMantenimientoDocumento) As Boolean
            Return New RepositorioDocumentoEquipoMantenimiento().EliminarTemporal(entidad)
        End Function
    End Class
End Namespace
