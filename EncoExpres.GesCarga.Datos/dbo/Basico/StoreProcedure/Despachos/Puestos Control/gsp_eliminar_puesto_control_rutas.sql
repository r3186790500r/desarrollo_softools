﻿PRINT 'gsp_eliminar_puesto_control_rutas'
go
drop procedure gsp_eliminar_puesto_control_rutas
go
create procedure gsp_eliminar_puesto_control_rutas  
(@par_EMPR_Codigo smallint,  
@par_RUTA_Codigo numeric)  
as  
begin  
delete Puesto_Control_Rutas where EMPR_Codigo = @par_EMPR_Codigo and RUTA_Codigo = @par_RUTA_Codigo  
end  
go