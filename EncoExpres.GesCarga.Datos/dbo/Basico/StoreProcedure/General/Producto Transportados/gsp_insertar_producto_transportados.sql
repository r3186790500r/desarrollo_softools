﻿PRINT 'gsp_insertar_producto_transportados'
GO
DROP PROCEDURE gsp_insertar_producto_transportados
GO
CREATE PROCEDURE gsp_insertar_producto_transportados  
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_Codigo_Alterno NUMERIC,  
 @par_Nombre VARCHAR(100),  
 @par_Descripcion VARCHAR(250),   
 @par_UMPT_Codigo NUMERIC,  
 @par_UEPT_Codigo NUMERIC,  
 @par_CATA_LIPT_Codigo NUMERIC,  
 @par_CATA_NAPT_Codigo NUMERIC,  
 @par_Estado SMALLINT,  
 @par_USUA_Codigo_Crea SMALLINT  
)  
AS  
BEGIN  
 INSERT INTO  
  Producto_Transportados  
  (  
  EMPR_Codigo,
  Codigo_Alterno,
  Nombre,
  Descripcion,
  UMPT_Codigo,
  UEPT_Codigo,
  CATA_LIPT_Codigo,
  CATA_NAPT_Codigo,
  Estado,  
  Fecha_Crea,
  USUA_Crea
  )  
 VALUES  
 (  
  @par_EMPR_Codigo,  
  @par_Codigo_Alterno,  
  @par_Nombre,   
  @par_Descripcion,
  @par_UMPT_Codigo,
  @par_UEPT_Codigo,
  @par_CATA_LIPT_Codigo,
  @par_CATA_NAPT_Codigo,
  @par_Estado,  
  GETDATE(),
  @par_USUA_Codigo_Crea
  
 )  
SELECT Codigo = @@identity  
END  
GO