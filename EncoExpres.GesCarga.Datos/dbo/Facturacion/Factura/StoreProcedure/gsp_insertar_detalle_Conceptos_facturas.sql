﻿
Print 'CREATE PROCEDURE gsp_insertar_detalle_Conceptos_facturas'
GO
DROP PROCEDURE gsp_insertar_detalle_Conceptos_facturas
GO

CREATE PROCEDURE gsp_insertar_detalle_Conceptos_facturas (  
@par_EMPR_Codigo	 smallint,  
@par_ENFA_Numero	 numeric, 
@par_DEND_ID 	 	 numeric, 
@par_COVE_Codigo 	 numeric,  
@par_Descripcion 	 VARCHAR(250), 
@par_Valor_Concepto  MONEY,  
@par_Valor_Impuestos MONEY,  
@par_ENRE_Numero 	 numeric,  
@par_ENOS_Numero 	 numeric,
@par_EMOE_Numero 	 numeric
)  
AS  
BEGIN  
  
 INSERT INTO Detalle_Conceptos_Facturas (  
 EMPR_Codigo,  
 ENFA_Numero,  
 DEND_ID, 
 COVE_Codigo,  
 Descripcion,
 Valor_Concepto,
 Valor_Impuestos,
 ENRE_Numero,
 ENOS_Numero,
 EMOE_Numero
 )  
 VALUES(  
 @par_EMPR_Codigo,  
 @par_ENFA_Numero, 
 @par_DEND_ID,
 @par_COVE_Codigo,
 @par_Descripcion,
 @par_Valor_Concepto,
 @par_Valor_Impuestos,
 @par_ENRE_Numero,
 @par_ENOS_Numero,
 @par_EMOE_Numero 
 )
 
 SELECT ENFA_Numero, Codigo
 FROM Detalle_Conceptos_Facturas
 WHERE EMPR_Codigo = @par_EMPR_Codigo  
 AND ENFA_Numero = @par_ENFA_Numero 
 AND Codigo = @@identity
 
END  
GO
