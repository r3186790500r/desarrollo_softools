﻿
Print 'CREATE TABLE DOCUMENTOS_FACTURA'
GO
DROP TABLE Documentos_Factura
GO

CREATE table Documentos_Factura
(
	EMPR_codigo smallint not null,
	ENFA_Numero numeric(18,0) not null,
	Codigo numeric(18,0)  Primary Key identity(1,1) not null,
	Fecha_Crea datetime null,
	Tipo_documento VARCHAR(50) null,
	Archivo VARCHAR(MAX) null,
	Descripcion VARCHAR(100) null,
);
GO