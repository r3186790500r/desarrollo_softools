ALTER PROCEDURE [dbo].[gsp_Consultar_Detalle_Facturas_SIESA_ENCOE]            
(            
@par_EMPR_Codigo SMALLINT,              
@par_ENFA_Numero NUMERIC  
)            
AS             
BEGIN            
	SELECT 
	ENRE.Numero,
	ENRE.Numero_Documento,
	ENRE.CATA_TIRE_Codigo,
	ENRE.Fecha,
	ENRE.Valor_Flete_Cliente AS Valor_Flete,
	ENRE.Cantidad_Cliente, 
	ENRE.Peso_Cliente,
	ISNULL(ENRE.Valor_Manejo_Cliente,0) AS Valor_Manejo,
	ISNULL(ENRE.Valor_Seguro_Cliente,0) AS Valor_Seguro,
	ISNULL(ENRE.Valor_Reexpedicion,0) AS Valor_Reexpedicion,
	ISNULL(ENRE.Valor_Cargue,0) AS Valor_Cargue,
	ISNULL(ENRE.Valor_Descargue,0) AS Valor_Descargue,
	ENRE.Total_Flete_Cliente AS Total_Flete,
	REPA.CIUD_Codigo_Origen AS Codigo_Ciudad_Origen,
	ORIG.Nombre AS Ciudad_Origen,
	REPA.CIUD_Codigo_Destino AS Codigo_Ciudad_Destino,
	DEST.Nombre AS Ciudad_Destino,
	REPA.Reexpedicion,
	REPA.OFIC_Codigo_Factura_Venta
			           
	FROM Detalle_Remesas_Facturas AS DERF           
          
	INNER JOIN Encabezado_Remesas AS ENRE         
	ON DERF.EMPR_Codigo = ENRE.EMPR_Codigo            
	AND DERF.ENRE_Numero = ENRE.Numero            
            
	LEFT JOIN Remesas_Paqueteria AS REPA            
	ON ENRE.EMPR_Codigo = REPA.EMPR_Codigo            
	AND ENRE.Numero = REPA.ENRE_Numero            
        
	-- Ciudad Origen Remesa	
	LEFT JOIN Ciudades AS ORIG            
	ON REPA.EMPR_Codigo = ORIG.EMPR_Codigo            
	AND REPA.CIUD_Codigo_Origen = ORIG.Codigo

	-- Ciudad Destino Remesa	
	LEFT JOIN Ciudades AS DEST            
	ON REPA.EMPR_Codigo = DEST.EMPR_Codigo            
	AND REPA.CIUD_Codigo_Origen = DEST.Codigo

	WHERE DERF.EMPR_Codigo = @par_EMPR_Codigo           
	AND DERF.ENFA_Numero = @par_ENFA_Numero          
             
END 