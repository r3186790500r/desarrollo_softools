USE [ENCOEXPRES]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Terceros]
ADD Privado_Sistema bit not null default 0;
GO

UPDATE [dbo].[Terceros] set Privado_Sistema = 1 where Numero_Identificacion = '222222222'
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_terceros_Autocomplete]    
(                               
 @par_EMPR_Codigo SMALLINT,                                              
 @par_Filtro VARCHAR(100) = NULL,                                              
 @par_Perfil_Tercero SMALLINT = NULL,                      
 @par_Codigo NUMERIC = null,    
 @par_Estado NUMERIC = NULL    
)                                              
AS                                              
BEGIN                                              
 IF @par_Codigo > 0              
 BEGIN                                              
  IF @par_Perfil_Tercero IS NULL    
  BEGIN    
   SELECT TOP(50)    
   0 As Obtener                                              
   ,TERC.EMPR_Codigo                                              
   ,TERC.Codigo                                              
   ,TERC.Numero_Identificacion                                              
   ,TERC.Digito_Chequeo                                         
   ,TERC.CATA_TINT_Codigo                                      
   ,TERC.CATA_TIID_Codigo                                      
   ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                                            
   ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                                            
   ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2                
   ,CONCAT(TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2) As NombreCompleto                                        
   ,ISNULL(TERC.Emails,'')AS Emails                                            
   ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                                              
   ,CIUD.Nombre AS NombreCuidadDireccion                                  
   ,ISNULL(TERC.Barrio,'') AS Barrio                                            
   ,ISNULL(TERC.Direccion,'')AS Direccion                               
   ,TERC.Codigo_Postal AS Codigo_Postal                                                  
   ,ISNULL(TERC.Telefonos,'')AS Telefonos                                              
   ,ISNULL(TERC.Celulares,'')AS Celulares                                              
   ,TERC.Estado                                           
   ,TERC.Justificacion_Bloqueo            
   ,TERC.TERC_Codigo_Beneficiario                                    
   ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                                              
   ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno                                  
   ,ROW_NUMBER() OVER(ORDER BY TERC.Codigo) AS RowNumber                                              
                                                     
   FROM                                               
   Terceros TERC LEFT JOIN Ciudades CIEX ON                                            
   TERC.EMPR_Codigo = CIEX.EMPR_Codigo                                            
   AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                            
                      
   LEFT JOIN Ciudades CIUD  ON                         
   TERC.EMPR_Codigo = CIUD.EMPR_Codigo                                              
   AND TERC.CIUD_Codigo = CIUD.Codigo                      
   WHERE                                               
   TERC.Codigo > 0                                   
   AND (TERC.Codigo = @par_Codigo OR @par_Codigo IS NULL)                      
   AND TERC.EMPR_Codigo = @par_EMPR_Codigo                                              
   AND (                        
   (TERC.Numero_Identificacion LIKE  '%'+@par_Filtro+ '%' )                                            
   OR ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%') OR (@par_Filtro IS NULL))                    
   )                                              
   AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)   
   AND TERC.Privado_Sistema = 0
   order by TERC.Nombre                                             
  END                                              
  ELSE                                              
  BEGIN    
   SELECT TOP(50)    
   0 As Obtener                                              
   ,TERC.EMPR_Codigo                                              
   ,TERC.Codigo                                          
   ,TERC.Numero_Identificacion                                              
   ,TERC.Digito_Chequeo                                        
   ,TERC.CATA_TINT_Codigo                                      
   ,TERC.CATA_TIID_Codigo                                       
   ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                                           
   ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2               
   ,CONCAT(TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2) As NombreCompleto                                         
   ,ISNULL(TERC.Emails,'') AS Emails                                              
   ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                                              
   ,CIUD.Nombre AS NombreCuidadDireccion                                              
   ,ISNULL(TERC.Barrio,'') AS Barrio                                            
   ,ISNULL(TERC.Direccion,'')AS Direccion                               
   ,TERC.Codigo_Postal AS Codigo_Postal                                 
   ,ISNULL(TERC.Telefonos,'') AS Telefonos                                       
   ,ISNULL(TERC.Celulares ,'') AS Celulares                                           
   ,TERC.Estado                                       
   ,TERC.TERC_Codigo_Beneficiario                                           
   ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                                              
   ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno                              
   ,TERC.Justificacion_Bloqueo            
   ,ROW_NUMBER() OVER(ORDER BY TERC.Nombre,TERC.Razon_Social) AS RowNumber                                              
   FROM                                               
                                                   
   Terceros TERC LEFT JOIN Ciudades CIEX ON                                            
   TERC.EMPR_Codigo = CIEX.EMPR_Codigo                                            
   AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                                               
                          
   LEFT JOIN Ciudades CIUD  ON                         
   TERC.EMPR_Codigo = CIUD.EMPR_Codigo                                              
   AND TERC.CIUD_Codigo = CIUD.Codigo                      
                                                 
   ,Perfil_Terceros PETE                                              
                                              
   WHERE                                                  
   TERC.Codigo > 0                                   
   AND (TERC.Codigo = @par_Codigo OR @par_Codigo IS NULL)                      
   AND TERC.EMPR_Codigo = @par_EMPR_Codigo                                              
   AND (                        
   (TERC.Numero_Identificacion LIKE  '%'+@par_Filtro+ '%' )                                            
   OR ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE RTRIM(LTRIM(@par_Filtro)) + '%' OR TERC.Razon_Social LIKE RTRIM(LTRIM(@par_Filtro)) + '%') OR (@par_Filtro IS NULL))                        
   )            
   AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)    
   AND TERC.EMPR_Codigo = PETE.EMPR_Codigo    
   AND TERC.Codigo = PETE.TERC_Codigo                                              
   AND PETE.Codigo = @par_Perfil_Tercero   
   AND TERC.Privado_Sistema = 0
   ORDER BY TERC.Nombre ASC             
  END                 
 END    
 ELSE              
 BEGIN    
  IF @par_Perfil_Tercero IS NULL                                              
  BEGIN    
   SELECT TOP(50)    
   0 As Obtener                                              
   ,TERC.EMPR_Codigo                                              
   ,TERC.Codigo                                              
   ,TERC.Numero_Identificacion                                              
   ,TERC.Digito_Chequeo                                         
   ,TERC.CATA_TINT_Codigo                                      
   ,TERC.CATA_TIID_Codigo                                      
   ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                                            
   ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                                            
   ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2            
   ,CONCAT(TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2) As NombreCompleto                                            
   ,ISNULL(TERC.Emails,'')AS Emails                                            
   ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                                              
   ,CIUD.Nombre AS NombreCuidadDireccion                                  
   ,ISNULL(TERC.Barrio,'') AS Barrio                                            
   ,ISNULL(TERC.Direccion,'')AS Direccion                               
   ,TERC.Codigo_Postal AS Codigo_Postal                                                  
   ,ISNULL(TERC.Telefonos,'')AS Telefonos                                              
   ,ISNULL(TERC.Celulares,'')AS Celulares                                              
   ,TERC.Estado                                           
   ,TERC.TERC_Codigo_Beneficiario                                    
   ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                                              
   ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno                                  
   ,TERC.Justificacion_Bloqueo            
   ,ROW_NUMBER() OVER(ORDER BY TERC.Codigo) AS RowNumber                                              
                                                     
   FROM                                               
   Terceros TERC LEFT JOIN Ciudades CIEX ON                                            
   TERC.EMPR_Codigo = CIEX.EMPR_Codigo                                            
   AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                            
                      
   LEFT JOIN Ciudades CIUD  ON                         
   TERC.EMPR_Codigo = CIUD.EMPR_Codigo                                              
   AND TERC.CIUD_Codigo = CIUD.Codigo                      
   WHERE                                               
   TERC.Codigo > 0                                   
   AND (TERC.Codigo = @par_Codigo OR @par_Codigo IS NULL)                      
   AND TERC.EMPR_Codigo = @par_EMPR_Codigo                                              
   AND (                        
   (TERC.Numero_Identificacion LIKE  '%'+@par_Filtro+ '%' )                                            
   OR ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%') OR (@par_Filtro IS NULL))                        
   )                                              
   AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL) 
   AND TERC.Privado_Sistema = 0
   order by TERC.Nombre                                             
  END                                              
  ELSE                                      
  BEGIN    
   SELECT TOP(50)    
   0 As Obtener                                              
   ,TERC.EMPR_Codigo                                              
   ,TERC.Codigo                                              
   ,TERC.Numero_Identificacion                                              
   ,TERC.Digito_Chequeo                                        
   ,TERC.CATA_TINT_Codigo                                      
   ,TERC.CATA_TIID_Codigo                                       
   ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                                           
   ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2                   
   ,CONCAT(TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2) As NombreCompleto                                     
   ,ISNULL(TERC.Emails,'') AS Emails                                              
   ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                                              
   ,CIUD.Nombre AS NombreCuidadDireccion                                              
   ,ISNULL(TERC.Barrio,'') AS Barrio                                 
   ,ISNULL(TERC.Direccion,'')AS Direccion                               
   ,TERC.Codigo_Postal AS Codigo_Postal                                 
   ,ISNULL(TERC.Telefonos,'') AS Telefonos                                       
   ,ISNULL(TERC.Celulares ,'') AS Celulares                                           
   ,TERC.Estado                                       
   ,TERC.TERC_Codigo_Beneficiario                                           
   ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                                              
   ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno                                  
   ,TERC.Justificacion_Bloqueo            
   ,ROW_NUMBER() OVER(ORDER BY TERC.Nombre,TERC.Razon_Social) AS RowNumber                                              
   FROM                                               
                                                   
   Terceros TERC LEFT JOIN Ciudades CIEX ON                                            
   TERC.EMPR_Codigo = CIEX.EMPR_Codigo                                            
   AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                                               
                          
   LEFT JOIN Ciudades CIUD  ON                         
   TERC.EMPR_Codigo = CIUD.EMPR_Codigo                                              
   AND TERC.CIUD_Codigo = CIUD.Codigo                      
                                                 
   ,Perfil_Terceros PETE                                              
                                              
   WHERE                                                  
   TERC.Codigo > 0                                   
   AND (TERC.Codigo = @par_Codigo OR @par_Codigo IS NULL)                      
   AND TERC.EMPR_Codigo = @par_EMPR_Codigo                                              
   AND (                        
   (TERC.Numero_Identificacion LIKE  '%'+@par_Filtro+ '%' )                                            
   OR ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%') OR (@par_Filtro IS NULL))                        
   )                                              
   AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)                                            
   AND TERC.EMPR_Codigo = PETE.EMPR_Codigo              
   AND TERC.Codigo = PETE.TERC_Codigo                                              
   AND PETE.Codigo = @par_Perfil_Tercero   
   AND TERC.Privado_Sistema = 0
   
   ORDER BY TERC.Nombre ASC                                       
  END              
 END                                              
END    
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_terceros]   
  (           
  @par_EMPR_Codigo SMALLINT,                          
  @par_Codigo NUMERIC = NULL,                          
  @par_Numero_Identificacion VARCHAR(15) = NULL ,                          
  @par_Codigo_Alterno VARCHAR(10) = NULL,                          
  @par_Nombre_Tercero VARCHAR(100) = NULL,                          
  @par_Nombre_Ciudad VARCHAR(100) = NULL,                          
  @par_Estado SMALLINT = NULL,                          
  @par_Perfil_Tercero SMALLINT = NULL,                          
  @par_NumeroPagina INT = NULL,                           
  @par_RegistrosPagina INT = NULL,
  @par_Privado_Sistema bit = NULL
  )                          
AS                          
BEGIN                          
  SET NOCOUNT ON;                          
  DECLARE @CantidadRegistros INT                          
                          
  IF @par_Perfil_Tercero IS NULL                          
  BEGIN                          
   SELECT @CantidadRegistros = (                          
    SELECT DISTINCT                           
    COUNT(TERC.Codigo)                           
   FROM                           
     Terceros TERC LEFT JOIN Ciudades CIEX ON                        
     TERC.EMPR_Codigo = CIEX.EMPR_Codigo                        
     AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                        
     ,Ciudades CIUD      
  LEFT JOIN Departamentos DEPA ON    
  CIUD.EMPR_Codigo = DEPA.EMPR_Codigo    
  AND CIUD.DEPA_Codigo = DEPA.Codigo                          
   WHERE                           
      TERC.EMPR_Codigo = CIUD.EMPR_Codigo                          
     AND TERC.CIUD_Codigo = CIUD.Codigo                                 
     AND TERC.Codigo > 0                          
     AND TERC.EMPR_Codigo = @par_EMPR_Codigo                          
     AND (TERC.Codigo = @par_Codigo OR  @par_Codigo IS NULL)            
     AND (TERC.Numero_Identificacion = @par_Numero_Identificacion OR @par_Numero_Identificacion IS NULL)                        
     AND ((TERC.Codigo_Alterno = @par_Codigo_Alterno) OR (@par_Codigo_Alterno IS NULL))                          
     AND ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%') OR (@par_Nombre_Tercero IS NULL))                          
     AND ((CIUD.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Ciudad)) + '%') OR (@par_Nombre_Ciudad IS NULL))                          
     AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)
	 AND (TERC.Privado_Sistema = 0 or @par_Privado_Sistema = 1)
   );                                 
  WITH Pagina AS                          
  (                          
  SELECT 0 As Obtener                          
     ,TERC.EMPR_Codigo                          
     ,TERC.Codigo                          
     ,TERC.Numero_Identificacion                          
     ,TERC.Digito_Chequeo                     
  ,TERC.CATA_TINT_Codigo                  
  ,TERC.CATA_TIID_Codigo                  
  ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                        
     ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                        
     ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                          
     ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                          
     ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                          
     ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2                          
     ,ISNULL(TERC.Emails,'')AS Emails                        
     ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                          
     ,CIUD.Nombre AS NombreCuidadDireccion      
  ,CONCAT(CIUD.Nombre,' (',DEPA.Nombre,')') AS CiudadDepartamento                 
  ,ISNULL(TERC.Barrio,'') AS Barrio                        
     ,ISNULL(TERC.Direccion,'')AS Direccion           
  ,ISNULL(TERC.Codigo_Postal,0) AS Codigo_Postal            
     ,ISNULL(TERC.Telefonos,'')AS Telefonos                          
     ,ISNULL(TERC.Celulares,'')AS Celulares                     
     ,TERC.Estado                       
  ,TERC.TERC_Codigo_Beneficiario                
     ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                          
     ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno              
     ,ROW_NUMBER() OVER(ORDER BY LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))),LTRIM(RTRIM(ISNULL(TERC.Nombre, '')))) AS RowNumber                          
                                 
   FROM                           
     Terceros TERC LEFT JOIN Ciudades CIEX ON                          
     TERC.EMPR_Codigo = CIEX.EMPR_Codigo                          
     AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo       
  ,Ciudades CIUD     
     LEFT JOIN Departamentos DEPA ON    
  CIUD.EMPR_Codigo = DEPA.EMPR_Codigo    
  AND CIUD.DEPA_Codigo = DEPA.Codigo            
                          
     WHERE      
  TERC.EMPR_Codigo = CIUD.EMPR_Codigo                          
     AND TERC.CIUD_Codigo = CIUD.Codigo                                 
     AND TERC.Codigo > 0                          
     AND TERC.EMPR_Codigo = @par_EMPR_Codigo                          
     AND (TERC.Codigo = @par_Codigo OR  @par_Codigo IS NULL)            
     AND (TERC.Numero_Identificacion = @par_Numero_Identificacion OR @par_Numero_Identificacion IS NULL)                        
     AND ((TERC.Codigo_Alterno = @par_Codigo_Alterno) OR (@par_Codigo_Alterno IS NULL))                          
     AND ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%') OR (@par_Nombre_Tercero IS NULL))                          
     AND ((CIUD.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Ciudad)) + '%') OR (@par_Nombre_Ciudad IS NULL))                          
     AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL) 
	 AND (TERC.Privado_Sistema = 0 or @par_Privado_Sistema = 1)
    )                          
                          
   SELECT DISTINCT                          
    0 As Obtener                          
     ,EMPR_Codigo                          
     ,Codigo                          
     ,Numero_Identificacion                          
     ,Digito_Chequeo                   
  ,CATA_TINT_Codigo                  
  ,CATA_TIID_Codigo                  
  ,CIUD_Codigo_Expedicion                     
  ,Ciudad_Expedicion                         
     ,Razon_Social                          
     ,Nombre                          
     ,Apellido1                          
     ,Apellido2                          
     ,Emails                          
     ,CIUD_Codigo_Direccion                          
     ,NombreCuidadDireccion          
  ,CiudadDepartamento       
     ,Barrio             
  ,Direccion           
  ,Codigo_Postal                         
     ,Telefonos                        
     ,Celulares                        
     ,Estado                          
     ,NombreEstado                          
     ,Codigo_Alterno                   
  ,ISNULL(TERC_Codigo_Beneficiario,0) AS TERC_Codigo_Beneficiario                
  ,0 AS PAIS_Codigo                     
     ,@CantidadRegistros AS TotalRegistros                          
     ,@par_NumeroPagina AS PaginaObtener                          
     ,@par_RegistrosPagina AS RegistrosPagina                          
   FROM                          
    Pagina                          
   WHERE                          
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)                          
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)                          
   ORDER BY Razon_Social,Nombre ASC                          
  END                          
  ELSE                          
  BEGIN                          
   SELECT @CantidadRegistros = (                          
    SELECT DISTINCT                           
    COUNT(TERC.Codigo)                      
    FROM                           
                               
     Terceros TERC LEFT JOIN Ciudades CIEX ON                        
     TERC.EMPR_Codigo = CIEX.EMPR_Codigo                        
     AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                           
     ,Ciudades CIUD                          
  LEFT JOIN Departamentos DEPA ON    
  CIUD.EMPR_Codigo = DEPA.EMPR_Codigo    
  AND CIUD.DEPA_Codigo = DEPA.Codigo      
  ,Perfil_Terceros PETE                  
                          
     WHERE      
  TERC.EMPR_Codigo = CIUD.EMPR_Codigo                          
     AND TERC.CIUD_Codigo = CIUD.Codigo                                   
     AND TERC.Codigo > 0                          
     AND TERC.EMPR_Codigo = @par_EMPR_Codigo                          
     AND (TERC.Codigo = @par_Codigo OR  @par_Codigo IS NULL)            
     AND (TERC.Numero_Identificacion = @par_Numero_Identificacion OR @par_Numero_Identificacion IS NULL)                
     AND ((TERC.Codigo_Alterno = @par_Codigo_Alterno) OR (@par_Codigo_Alterno IS NULL))                          
     AND ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%') OR (@par_Nombre_Tercero IS NULL))                          
     AND ((CIUD.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Ciudad)) + '%') OR (@par_Nombre_Ciudad IS NULL))                          
     AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)                          
     AND TERC.EMPR_Codigo = PETE.EMPR_Codigo                          
     AND TERC.Codigo = PETE.TERC_Codigo                          
     AND PETE.Codigo = @par_Perfil_Tercero
	 AND (TERC.Privado_Sistema = 0 or @par_Privado_Sistema = 1)
   );                                 
  WITH Pagina AS                          
  (                          
  SELECT 0 As Obtener                          
     ,TERC.EMPR_Codigo                          
     ,TERC.Codigo                          
     ,TERC.Numero_Identificacion                          
     ,TERC.Digito_Chequeo                    
  ,TERC.CATA_TINT_Codigo                  
  ,TERC.CATA_TIID_Codigo                   
  ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                       
     ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                          
     ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                          
     ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                          
     ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                          
     ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2                          
     ,ISNULL(TERC.Emails,'') AS Emails                          
     ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                          
     ,CIUD.Nombre AS NombreCuidadDireccion         
  ,CONCAT(CIUD.Nombre,' (',DEPA.Nombre,')') AS CiudadDepartamento                     
     ,ISNULL(TERC.Barrio,'') AS Barrio                        
     ,ISNULL(TERC.Direccion,'')AS Direccion           
  ,ISNULL(TERC.Codigo_Postal, 0) AS Codigo_Postal             
     ,ISNULL(TERC.Telefonos,'') AS Telefonos                   
     ,ISNULL(TERC.Celulares ,'') AS Celulares                       
     ,TERC.Estado                   
 ,TERC.TERC_Codigo_Beneficiario                       
     ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                          
     ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno              
     ,ROW_NUMBER() OVER(ORDER BY  LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))),LTRIM(RTRIM(ISNULL(TERC.Nombre, '')))) AS RowNumber                          
   FROM                           
                               
     Terceros TERC LEFT JOIN Ciudades CIEX ON                        
     TERC.EMPR_Codigo = CIEX.EMPR_Codigo                        
     AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                           
     ,Ciudades CIUD    
  LEFT JOIN Departamentos DEPA ON    
  CIUD.EMPR_Codigo = DEPA.EMPR_Codigo    
  AND CIUD.DEPA_Codigo = DEPA.Codigo                                    
     ,Perfil_Terceros PETE              
                
                          
   WHERE  TERC.EMPR_Codigo = CIUD.EMPR_Codigo                          
     AND TERC.CIUD_Codigo = CIUD.Codigo                                
     AND TERC.Codigo > 0                          
     AND TERC.EMPR_Codigo = @par_EMPR_Codigo                          
     AND (TERC.Codigo = @par_Codigo OR  @par_Codigo IS NULL)            
     AND (TERC.Numero_Identificacion = @par_Numero_Identificacion OR @par_Numero_Identificacion IS NULL)                        
     AND ((TERC.Codigo_Alterno = @par_Codigo_Alterno) OR (@par_Codigo_Alterno IS NULL))                          
     AND ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Tercero)) + '%') OR (@par_Nombre_Tercero IS NULL))                          
     AND ((CIUD.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Ciudad)) + '%') OR (@par_Nombre_Ciudad IS NULL))                          
     AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)                          
     AND TERC.EMPR_Codigo = PETE.EMPR_Codigo                          
     AND TERC.Codigo = PETE.TERC_Codigo                          
     AND PETE.Codigo = @par_Perfil_Tercero   
	 AND (TERC.Privado_Sistema = 0 or @par_Privado_Sistema = 1)
 )                          
                          
   SELECT DISTINCT                          
    0 As Obtener                          
     ,EMPR_Codigo                          
     ,Codigo                          
     ,Numero_Identificacion                          
     ,Digito_Chequeo                    
  ,CATA_TINT_Codigo                  
  ,CATA_TIID_Codigo                  
  ,CIUD_Codigo_Expedicion                        
  ,Ciudad_Expedicion                       
     ,Razon_Social                          
     ,Nombre                          
     ,Apellido1                          
     ,Apellido2                          
     ,Emails                          
     ,CIUD_Codigo_Direccion                          
     ,NombreCuidadDireccion       
  ,CiudadDepartamento                         
     ,Barrio             
  ,Direccion           
  ,Codigo_Postal                      
     ,Telefonos                         
     ,Celulares                         
     ,Estado                       
  ,ISNULL(TERC_Codigo_Beneficiario,0) AS TERC_Codigo_Beneficiario                
     ,NombreEstado                          
     ,Codigo_Alterno            
  ,0 AS PAIS_Codigo                     
     ,@CantidadRegistros AS TotalRegistros                          
     ,@par_NumeroPagina AS PaginaObtener                          
     ,@par_RegistrosPagina AS RegistrosPagina                          
   FROM                          
    Pagina                          
   WHERE                          
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)                          
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)                          
   ORDER BY Razon_Social,Nombre ASC                          
  END                          
END                    
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_obtener_tercero_x_identificacion]
  (                     
  @par_EMPR_Codigo SMALLINT,                                    
  @par_Filtro VARCHAR(100) = NULL                               
  )                                    
AS                                    
BEGIN                                    
 SELECT * FROM Terceros 
 WHERE EMPR_Codigo =  @par_EMPR_Codigo 
 AND Numero_Identificacion =   @par_Filtro
 AND Privado_Sistema = 0
END        
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_obtener_terceros]         
(                  
@par_EMPR_Codigo smallint,                  
@par_Codigo Numeric  = null,                
@par_Identificacion VARCHAR(20) = NULL                
)                  
AS                   
BEGIN                  
SELECT                   
1 as Obtener,                  
TERC.EMPR_Codigo,--                  
TERC.Codigo,--                  
ISNULL(TERC.Codigo_Alterno,'') AS  Codigo_Alterno,--                  
ISNULL(TERC.Codigo_Contable,0) AS  Codigo_Contable,                  
TERC.CATA_TINT_Codigo,                  
TERC.CATA_TIID_Codigo,                  
TERC.Numero_Identificacion,                  
TERC.Digito_Chequeo,                  
ISNULL(TERC.Razon_Social,'') AS Razon_Social ,                  
ISNULL(TERC.Representante_Legal,'') AS  Representante_Legal,                  
ISNULL(TERC.Nombre,'') AS  Nombre,                  
TERC.Apellido1,                  
ISNULL(TERC.Apellido2,'')Apellido2,                  
TERC.CATA_SETE_Codigo,                  
TERC.CIUD_Codigo_Identificacion,                  
TERC.CIUD_Codigo_Nacimiento,                  
TERC.CIUD_Codigo AS CIUD_Codigo_Direccion,                  
--0 as CIUD_Codigo_Direccion,                  
TERC.Direccion,                  
ISNULL(TERC.Barrio,'') AS Barrio,              
ISNULL(TERC.Codigo_Postal,0) AS Codigo_Postal ,                  
TERC.Telefonos,                  
ISNULL(TERC.Celulares,'') AS Celulares ,                  
ISNULL(TERC.Emails,'') AS Emails ,     
ISNULL(TERC.Correo_Facturacion,'') As Correo_Facturacion,
TERC.BANC_Codigo,                  
TERC.CATA_TICB_Codigo,                  
ISNULL(TERC.Numero_Cuenta_Bancaria,0) AS Numero_Cuenta_Bancaria ,                  
ISNULL(TERC.TERC_Codigo_Beneficiario,0) AS TERC_Codigo_Beneficiario,                  
ISNULL(TERC.Titular_Cuenta_Bancaria,'') AS  Titular_Cuenta_Bancaria,                  
TERC.Observaciones,                  
TERC.Foto,               
TERC.PAIS_Codigo,               
ISNULL(TERC.Justificacion_Bloqueo,'') AS Justificacion_Bloqueo ,                  
TERC.CATA_TIAN_Codigo,    
TERC.Estado   ,  
TERC.CATA_TIVC_Codigo   ,  
TERC.Cupo   ,  
TERC.Saldo     
FROM Terceros AS TERC                  
WHERE TERC.EMPR_Codigo = @par_EMPR_Codigo                  
AND TERC.Codigo = isnull(@par_Codigo  ,TERC.Codigo)                
AND TERC.Numero_Identificacion =  isnull(@par_Identificacion  ,TERC.Numero_Identificacion)
AND TERC.Privado_Sistema = 0
END     
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_terceros_general]
(
@par_EMPR_Codigo SMALLINT,
@par_Nombre VARCHAR(150) = NULL,
@par_Estado SMALLINT
)
AS
BEGIN

SELECT 
EMPR_Codigo,
Numero_Identificacion,
Codigo,
ISNULL(Razon_Social,'') +''+ISNULL(Nombre,'') + ' ' +ISNULL(Apellido1,'') + ' ' +ISNULL(Apellido2,' ') AS NombreTercero
FROM 
Terceros
WHERE 
EMPR_Codigo = @par_EMPR_Codigo
AND ((CONCAT(isnull(Razon_Social,''),isnull(Nombre,''),' ',Apellido1,' ',Apellido2) LIKE '%' + @par_Nombre + '%') OR (Numero_Identificacion LIKE '%' + @par_Nombre + '%') OR( @par_Nombre IS NULL))      
AND Estado = ISNULL(@par_Estado,Estado)
AND Privado_Sistema = 0
END
GO


