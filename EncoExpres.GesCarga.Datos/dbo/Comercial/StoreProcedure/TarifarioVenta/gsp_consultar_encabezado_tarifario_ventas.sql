﻿Print 'gsp_consultar_encabezado_tarifario_ventas'
GO
DROP PROCEDURE gsp_consultar_encabezado_tarifario_ventas
GO
CREATE PROCEDURE gsp_consultar_encabezado_tarifario_ventas      
(      
 @par_EMPR_Codigo SMALLINT,      
 @par_Codigo SMALLINT = NULL,      
 @par_Nombre VARCHAR(50) = NULL,      
 @par_Estado SMALLINT = NULL,      
 @par_Numero NUMERIC = NULL,      
 @par_Fecha_Vigencia_Inicio DATE = NULL,      
 @par_Fecha_Vigencia_Fin DATE = NULL,      
 @par_NumeroPagina INT = NULL,    
 @par_RegistrosPagina INT = NULL    
)      
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE      
 @CantidadRegistros INT      
 SELECT @CantidadRegistros = (      
 SELECT DISTINCT       
  COUNT(1)       
 FROM      
  Encabezado_Tarifario_Carga_Ventas      
 WHERE      
  EMPR_Codigo = @par_EMPR_Codigo      
  --AND Numero = ISNULL(@par_Codigo,Numero)      
  AND Numero = ISNULL(@par_Numero,Numero)      
  AND Fecha_Vigencia_Inicio >= ISNULL(@par_Fecha_Vigencia_Inicio,Fecha_Vigencia_Inicio)      
  AND Fecha_Vigencia_Fin <= ISNULL(@par_Fecha_Vigencia_Fin,Fecha_Vigencia_Fin)     
  AND ((Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))      
  AND Estado = ISNULL(@par_Estado,Estado   )      
  );             
 WITH Pagina AS      
 (      
      
 SELECT      
  EMPR_Codigo,      
  Numero,      
  Nombre,      
  Estado,  
  Fecha_Vigencia_Inicio,  
  Fecha_Vigencia_Fin,  
  Tarifario_Base,  
  ROW_NUMBER() OVER(ORDER BY Numero) AS RowNumber      
 FROM      
  Encabezado_Tarifario_Carga_Ventas      
 WHERE      
  EMPR_Codigo = @par_EMPR_Codigo      
  --AND Numero = ISNULL(@par_Codigo,Numero)      
  AND Numero = ISNULL(@par_Numero,Numero)      
  AND Fecha_Vigencia_Inicio >= ISNULL(@par_Fecha_Vigencia_Inicio,Fecha_Vigencia_Inicio)      
  AND Fecha_Vigencia_Fin <= ISNULL(@par_Fecha_Vigencia_Fin,Fecha_Vigencia_Fin)     
  AND ((Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))      
  AND Estado = ISNULL(@par_Estado,Estado   )  
  )     
 SELECT DISTINCT      
  0 As Obtener,  
  EMPR_Codigo,      
  Numero,      
  Nombre,      
  Estado,  
  Fecha_Vigencia_Inicio,     
  Fecha_Vigencia_Fin,  
  Tarifario_Base,  
 @CantidadRegistros AS TotalRegistros,      
 @par_NumeroPagina AS PaginaObtener,      
 @par_RegistrosPagina AS RegistrosPagina      
 FROM      
 Pagina      
 WHERE      
 RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, 1000)    
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, 1000)    
 ORDER BY Numero asc      
      
END      
GO