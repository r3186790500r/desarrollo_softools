﻿Print 'V_Periodo_Liquidacion'
GO
DROP VIEW V_Periodo_Liquidacion
GO
CREATE VIEW V_Periodo_Liquidacion 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 13
GO