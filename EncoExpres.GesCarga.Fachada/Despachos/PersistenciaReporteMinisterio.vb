﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Repositorio.Despachos.Procesos
Imports EncoExpres.GesCarga.Entidades.Despachos.Procesos
Imports EncoExpres.GesCarga.Repositorio.ControlViajes

Namespace Despachos
    Public NotInheritable Class PersistenciaReporteMinisterio
        Implements IPersistenciaBase(Of ReporteMinisterio)

        Public Function Consultar(filtro As ReporteMinisterio) As IEnumerable(Of ReporteMinisterio) Implements IPersistenciaBase(Of ReporteMinisterio).Consultar
            If filtro.ConsultaServicioAutomatico > 0 Then
                Return New RepositorioReporteMinisterioSW().Consultar(filtro)
            Else
                Return New RepositorioReporteMinisterio().Consultar(filtro)
            End If
        End Function

        Public Function Insertar(entidad As ReporteMinisterio) As Long Implements IPersistenciaBase(Of ReporteMinisterio).Insertar
            If entidad.ConsultaServicioAutomatico > 0 Then
                Return New RepositorioReporteMinisterioSW().Insertar(entidad)
            Else
                Return New RepositorioReporteMinisterio().Insertar(entidad)
            End If
        End Function

        Public Function Modificar(entidad As ReporteMinisterio) As Long Implements IPersistenciaBase(Of ReporteMinisterio).Modificar
            If entidad.ConsultaServicioAutomatico > 0 Then
                Return New RepositorioReporteMinisterioSW().Modificar(entidad)
            Else
                Return New RepositorioReporteMinisterio().Modificar(entidad)
            End If
        End Function

        Public Function Obtener(filtro As ReporteMinisterio) As ReporteMinisterio Implements IPersistenciaBase(Of ReporteMinisterio).Obtener
            If filtro.ConsultaServicioAutomatico > 0 Then
                Return New RepositorioReporteMinisterioSW().Obtener(filtro)
            Else
                Return New RepositorioReporteMinisterio().Obtener(filtro)
            End If
        End Function

        Public Function ReportarRNDC(filtro As ReporteMinisterio) As ReporteMinisterio
            If filtro.ConsultaServicioAutomatico > 0 Then
                Return New RepositorioReporteMinisterioSW().ReportarRNDC(filtro)
            Else
                Return New RepositorioReporteMinisterio().ReportarRNDC(filtro)
            End If
        End Function

        Public Function ConsultarRemesasSinManifiesto(filtro As ReporteMinisterio) As IEnumerable(Of ReporteMinisterio)
            Return New RepositorioDetalleSeguimientoVehiculos().ConsultarRemesasSinManifiesto(filtro)
        End Function
    End Class

End Namespace

