﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 07/12/2018
	Descripción_Crea:
	Modifica:Miguel Ortega
	Fecha_Modifica: 14/02/2019
	Descripción_Modifica: Agrega Valores numericos de fecha y estado del cierre
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

Print 'CREATE PROCEDURE sp_consultar_cierre_contable'
GO
DROP PROCEDURE sp_consultar_cierre_contable
GO

CREATE PROCEDURE sp_consultar_cierre_contable   
(      
 @par_EMPR_Codigo SMALLINT,        
 @par_Ano NUMERIC = NULL,       
 @par_Mes NUMERIC = NULL,     
 @par_Tipo_Documento NUMERIC = NULL  
)      
AS      
BEGIN        
	SELECT DISTINCT      
	CCDO.EMPR_Codigo      
	,CCDO.Ano      
	,CCDO.CATA_MESE_Codigo   
	,MESE.Campo1 AS Nombre_Mes
	,MESE.Campo2 AS Numero_Mes
	,CCDO.CATA_ESPC_Codigo       
	,ESPC.Campo1 AS Nombre_Estado_Cierre
	,ESPC.Campo2 AS Numero_Estado_Cierre
	,CCDO.TIDO_Codigo       
	,TIDO.Nombre AS Nombre_Tipo_Documento      
      
	FROM     
	Cierre_Contable_Documentos as CCDO      
      
	LEFT JOIN Valor_Catalogos AS MESE      
	ON CCDO.EMPR_Codigo = MESE.EMPR_Codigo      
	AND CCDO.CATA_MESE_Codigo = MESE.Codigo      
    
	LEFT JOIN Valor_Catalogos AS ESPC      
	ON CCDO.EMPR_Codigo = ESPC.EMPR_Codigo      
	AND CCDO.CATA_ESPC_Codigo = ESPC.Codigo   
    
	LEFT JOIN Tipo_Documentos AS TIDO      
	ON CCDO.EMPR_Codigo = TIDO.EMPR_Codigo      
	AND CCDO.TIDO_Codigo = TIDO.Codigo     
              
	WHERE     
    
	CCDO.EMPR_Codigo = @par_EMPR_Codigo         
	AND CCDO.Ano = ISNULL(@par_Ano, CCDO.Ano)      
	AND CCDO.CATA_MESE_Codigo = ISNULL(@par_Mes, CCDO.CATA_MESE_Codigo)    
	AND CCDO.TIDO_Codigo = ISNULL(@par_Tipo_Documento, CCDO.TIDO_Codigo)      
      
  END  