﻿PRINT 'gsp_insertar_puesto_control_rutas'
go
drop procedure gsp_insertar_puesto_control_rutas
go
create procedure gsp_insertar_puesto_control_rutas
(@par_EMPR_Codigo	smallint,
@par_RUTA_Codigo	numeric,
@par_PUCO_Codigo numeric,
@par_Orden numeric = null,
@par_Tiempo_Estimado smallint = null
)
as
begin
insert into Puesto_Control_Rutas 
(
EMPR_Codigo,
PUCO_Codigo,
RUTA_Codigo,
Orden,
Horas_Estimadas_Arribo
)
values
(@par_EMPR_Codigo,
@par_PUCO_Codigo,
@par_RUTA_Codigo,
ISNULL(@par_Orden,0),
@par_Tiempo_Estimado
)
SELECT 1 AS Numero
end
go