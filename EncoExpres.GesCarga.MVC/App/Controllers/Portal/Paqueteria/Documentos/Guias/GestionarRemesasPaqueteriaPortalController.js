﻿EncoExpresApp.controller("GestionarRemesasPaqueteriaPortalCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TarifarioVentasFactory', 'TarifaTransportesFactory',
    'TipoTarifaTransportesFactory', 'RutasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'TipoLineaNegocioTransportesFactory', 'TarifaTransportesFactory',
    'ProductoTransportadosFactory', 'CiudadesFactory', 'RemesaGuiasFactory', 'VehiculosFactory', 'OficinasFactory', 'blockUIConfig', 'SitiosTerceroClienteFactory', 'ImpuestoFacturasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TarifarioVentasFactory, TarifaTransportesFactory,
        TipoTarifaTransportesFactory, RutasFactory, ValorCatalogosFactory, TercerosFactory, TipoLineaNegocioTransportesFactory, TarifaTransportesFactory,
        ProductoTransportadosFactory, CiudadesFactory, RemesaGuiasFactory, VehiculosFactory, OficinasFactory, blockUIConfig, SitiosTerceroClienteFactory, ImpuestoFacturasFactory) {
        console.clear()
        $scope.Titulo = 'GESTIONAR REMESA';
        $scope.MapaSitio = $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Paqueteria' }, { Nombre: 'Remesa' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ManejoPesoVolumetrico = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria
        try {
            try {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_REMESAS_PAQUETERIA_PORTAL);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

            } catch (e) {

                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GUIAS_PAQUETERIA);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
            }
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }



        $scope.DeshabilitarOficinaDestino = false;
        $scope.ListadoDireccionesRemitente = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,
            Remesa: {
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                Numero: 0,
                TipoRemesaDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },
                //TipoRemesa: {Codigo: }
                DetalleTarifaVenta: {},
                ProductoTransportado: '',
                Fecha: new Date(),
                Remitente: { Direccion: '' },
                Destinatario: { Direccion: '' },
                Numeracion: ''
            },

        }


        $scope.Modal = {}

        $scope.CargarDatosFunciones = function () {
            $('#CamposTarifario').hide()
            $('#CamposTarifario2').hide()
            $('#CamposTarifario3').hide()
            $('.IntrasoValidoTarifario').hide()
            $('#DescripcionProducto').hide(100)
            /*Cargar el combo de Forma de pago*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoFormaPago = [];
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if (response.data.Datos[i].Codigo != CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NO_APLICA) {
                                    $scope.ListadoFormaPago.push(response.data.Datos[i])
                                }
                            }
                            try {
                                $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.Modelo.Remesa.FormaPago.Codigo);
                            } catch (e) {
                                try {
                                    //$scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO);
                                } catch (e) {
                                    $scope.Modelo.Remesa.FormaPago = $scope.ListadoFormaPago[1]
                                }
                            }
                        }
                        else {
                            $scope.ListadoFormaPago = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el listado de tipo rexpedición remesa paquetería*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_REEXPEDICION_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoReexpedicionRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoReexpedicionRemesaPaqueteria = response.data.Datos;
                            $scope.Modelo.Remesa.TipoReexpedicion = $scope.ListadoTipoReexpedicionRemesaPaqueteria[0]
                        }
                        else {
                            $scope.ListadoTipoReexpedicionRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de ESTADO DOCUMENTO REMESA*/

            //Por el momento solo en definitivo
            //$scope.ListaEstadoDocumento = [{ Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }];
            $scope.ListaEstadoDocumento = [{ Codigo: 1, Nombre: 'DEFINITIVO' }];

            if ($scope.CodigoEstadoDocumento == null || $scope.CodigoEstadoDocumento == undefined || $scope.CodigoEstadoDocumento == '') {
                $scope.Modelo.Estado = $scope.ListaEstadoDocumento[0];
            } else {
                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstadoDocumento).First('$.Codigo ==' + $scope.CodigoEstadoDocumento);
            }

            /*Cargar autocomplete oficinas*/
            $scope.ListaOficinas = [];
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaOficinas = response.data.Datos;

                        $scope.Modelo.OficinaDestino = $scope.ListaOficinas[0];
                        if ($scope.CodigoOficina !== undefined && $scope.CodigoOficina !== null) {
                            if ($scope.CodigoOficina > 0) {
                                $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.CodigoOficina);
                            }
                        }
                    }
                })


            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaEstadosGuia = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListaEstadosGuia = response.data.Datos;
                            if (!$scope.Modelo.Numero > 0) {
                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                                if ($scope.CodigoEstadoGuia !== undefined && $scope.CodigoEstadoGuia !== null) {
                                    $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + $scope.CodigoEstadoGuia);

                                } else {
                                    if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_PROPIA) {
                                        $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_OFICINA_ORIGEN);

                                        $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                            if ($scope.ListaOficinas.length > 0) {
                                                $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                            }
                                        }
                                        $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                                    }
                                    if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_CLIENTE) {
                                        $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);


                                        $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                            if ($scope.ListaOficinas.length > 0) {
                                                $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                            }
                                        }

                                        $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                                    }
                                    if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_RECEPTORIA) {
                                        $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);

                                        $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                            if ($scope.ListaOficinas.length > 0) {
                                                $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                            }
                                        }
                                        $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                    }
                                }
                            }
                        }
                        else {
                            $scope.ListaEstadosGuia = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de tipo entrega*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;

                            if ($scope.CodigoTipoEntregaRemesaPaqueteria !== undefined && $scope.CodigoTipoEntregaRemesaPaqueteria !== null) {
                                if ($scope.CodigoTipoEntregaRemesaPaqueteria > 0) {
                                    $scope.Modelo.TipoEntregaRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoEntregaRemesaPaqueteria).First('$.Codigo ==' + $scope.CodigoTipoEntregaRemesaPaqueteria);
                                } else {
                                    $scope.Modelo.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[1];
                                }
                            } else {
                                $scope.Modelo.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[1];
                            }

                        }

                    }
                }, function (response) {
                });

            /*Cargar el combo de tipo transporte*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_TRANSPORTE_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTransporteRemsaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoTransporteRemsaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoTransporteRemsaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoTransporteRemsaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoTransporteRemsaPaqueteria = $scope.ListadoTipoTransporteRemsaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo despacho*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DESPACHO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoDespachoRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoDespachoRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoDespachoRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoDespachoRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoDespachoRemesaPaqueteria = $scope.ListadoTipoDespachoRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo despacho*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TEMPERATURA_PRODUCTO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTemperaturaProductoRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTemperaturaProductoRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TemperaturaProductoRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTemperaturaProductoRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TemperaturaProductoRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TemperaturaProductoRemesaPaqueteria = $scope.ListadoTemperaturaProductoRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTemperaturaProductoRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo despacho*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SERVICIO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoServicioRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoServicioRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoServicioRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoServicioRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoServicioRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoServicioRemesaPaqueteria = $scope.ListadoTipoServicioRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoServicioRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de tipo interfaz*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 67 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoInterfazRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoInterfazRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoInterfazRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoInterfazRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoInterfazRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoInterfazRemesaPaqueteria = $scope.ListadoTipoInterfazRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoInterfazRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });



            /*Cargar Autocomplete de CLIENTES*/
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaClientes = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListaClientes = response.data.Datos;
                            try {
                                if ($scope.Modelo.Remesa.Cliente.Codigo > 0) {
                                    $scope.Modelo.Remesa.Cliente = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo ==' + $scope.Modelo.Remesa.Cliente.Codigo);
                                    $scope.ObtenerInformacionCliente();
                                }
                            } catch (e) {
                                $scope.Modelo.Remesa.Cliente = ''
                            }
                        }
                        else {
                            $scope.ListaClientes = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            /*Cargar Autocomplete de Proveedores*/
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaProveedores = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListaProveedores = response.data.Datos;
                        }
                        else {
                            $scope.ListaProveedores = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*Autocomplete de productos*/
            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, AplicaTarifario: -1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoProductoTransportados = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoProductoTransportados = response.data.Datos;
                            try {
                                if ($scope.Modelo.Remesa.ProductoTransportado.Codigo > 0) {
                                    $scope.Modelo.Remesa.ProductoTransportado = $linq.Enumerable().From($scope.ListadoProductoTransportados).First('$.Codigo ==' + $scope.Modelo.Remesa.ProductoTransportado.Codigo);
                                }
                            } catch (e) {
                                $scope.Modelo.Remesa.ProductoTransportado = ''
                            }
                        }
                        else {
                            $scope.ListadoProductoTransportados = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*Cargar el combo de tipo identificaciones*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoIdentificacion = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoIdentificacion = response.data.Datos;
                            $scope.ListadoTipoIdentificacion.splice(0, 1)
                            if ($scope.CodigoTipoIdentificacionRemitente !== undefined && $scope.CodigoTipoIdentificacionRemitente !== null) {
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRemitente);
                            } else {
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                            }
                            if ($scope.CodigoTipoIdentificacionDestinatario !== undefined && $scope.CodigoTipoIdentificacionDestinatario !== null) {
                                $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionDestinatario);
                            } else {
                                $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                            }

                        }
                        else {
                            $scope.ListadoTipoIdentificacion = []
                        }
                    }
                }, function (response) {
                });
            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

            ///*Ciudades*/
            //CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            //    then(function (response) {
            //        if (response.data.ProcesoExitoso === true) {
            //            if (response.data.Datos.length > 0) {
            //                $scope.ListadoCiudades = response.data.Datos;

            //                if ($scope.CodigoCiudadOrigen !== undefined && $scope.CodigoCiudadOrigen !== null) {

            //                    $scope.Modelo.Remesa.Remitente.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + $scope.CodigoCiudadOrigen);
            //                } else {
            //                    $scope.Modelo.Remesa.Remitente.Ciudad = '';
            //                }

            //                if ($scope.CodigoCiudadDestino !== undefined && $scope.CodigoCiudadDestino !== null) {
            //                    $scope.Modelo.Remesa.Destinatario.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + $scope.CodigoCiudadDestino);
            //                } else {
            //                    $scope.Modelo.Remesa.Destinatario.Ciudad = '';
            //                }


            //                //if ($scope.Modelo.Remesa.Remitente.Codigo > 0) {
            //                //    $scope.ObtenerRemitente(true)
            //                //}
            //                //if ($scope.Modelo.Remesa.Destinatario.Codigo > 0) {
            //                //    $scope.ObtenerDestinatario(true)
            //                //}
            //            }
            //            else {
            //                $scope.ListadoCiudades = [];
            //            }
            //        }
            //    }, function (response) {
            //        ShowError(response.statusText);
            //    });
            $scope.ListadoCiudades = []
            $scope.AutocompleteCiudades = function (value) {
                if (value.length > 0) {
                    if ((value.length % 3) == 0 || value.length == 2) {
                        /*Cargar Autocomplete de propietario*/
                        blockUIConfig.autoBlock = false;
                        var Response = CiudadesFactory.Consultar({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                        })
                        $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                    }
                }
                return $scope.ListadoCiudades
            }
            /*Autocomplete de vehiculos*/
            VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoVehiculos = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoVehiculos = response.data.Datos;
                            try {
                                if ($scope.Modelo.Remesa.Vehiculo.Codigo > 0) {
                                    $scope.Modelo.Remesa.Vehiculo = $linq.Enumerable().From($scope.ListadoVehiculos).First('$.Codigo ==' + $scope.Modelo.Remesa.Vehiculo.Codigo);
                                }
                            } catch (e) {
                                $scope.Modelo.Remesa.Vehiculo = ''
                            }
                        }
                        else {
                            $scope.ListadoVehiculos = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        }

        $scope.ObtenerInformacionCliente = function () {
            $('#CamposTarifario').hide(100)
            $('#CamposTarifario2').hide(100)
            $('#CamposTarifario3').hide(100)
            $scope.ListadoTipoLineaNegocioTransportes = []
            $scope.ListadoRutas = []
            $scope.ListadoTarifaTransportes = []

            if ($scope.Modelo.Remesa.Cliente !== undefined) {
                if ($scope.Modelo.Remesa.Cliente.Codigo > 0) {
                    TercerosFactory.Obtener($scope.Modelo.Remesa.Cliente).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.Codigo > 0) {
                                    if (response.data.Datos.Cliente != undefined) {
                                        if (response.data.Datos.Cliente.Tarifario !== undefined) {
                                            if (response.data.Datos.Cliente.Tarifario.Codigo > 0) {
                                                $scope.Modelo.Remesa.TarifarioVenta = response.data.Datos.Cliente.Tarifario
                                                $scope.ObtenerInformacionTarifario()
                                                $scope.Modelo.Remesa.Cliente = response.data.Datos
                                                if (!($scope.Modelo.Remesa.Numero > 0)) {
                                                    $scope.Modelo.Remesa.FormaPago = $scope.ListadoFormaPago[1]

                                                    ////FORMA DE PAGO CONTADO
                                                    //if ($scope.Modelo.Remesa.Cliente.Cliente.FormaPago.Codigo == CODIGO_FORMA_PAGO_CONTADO_CLIENTE) {
                                                    //    $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + CODIGO_FORMA_PAGO_CONTADO_REMESA);
                                                    //}
                                                    ////fORMA PAGO CREDITO
                                                    //else if ($scope.Modelo.Remesa.Cliente.Cliente.FormaPago.Codigo == CODIGO_FORMA_PAGO_CREDITO_CLIENTE) {
                                                    //    $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + CODIGO_FORMA_PAGO_CREDITO_REMESA);
                                                    //}
                                                }
                                            }
                                            else {
                                                ShowError('El cliente seleccionado no posee un tarifario de venta asignado');
                                            }
                                        }
                                        else {
                                            ShowError('Error al consultar la información del tarifario');
                                        }
                                    }
                                    else {
                                        ShowError('Error al consultar la información del cliente');
                                    }
                                } else {
                                    ShowError('Error al consultar la información del cliente');
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        }


        $scope.ValidarProducto = function () {
            if ($scope.Modelo.Remesa.ProductoTransportado !== undefined) {
                if ($scope.Modelo.Remesa.ProductoTransportado.Codigo > 0) {
                    $scope.Modelo.Remesa.DescripcionProductoTransportado = ''
                    $('#DescripcionProducto').hide(100)
                } else {
                    $scope.Modelo.Remesa.ProductoTransportado.inserta = true
                    $scope.Modelo.Remesa.DescripcionProductoTransportado = ''
                    $('#DescripcionProducto').show(100)
                }
            }
        }
        Array.in = function () {

        }
        //Calcular Flete
        $scope.Calcular = function () {
            if ($scope.Modelo.Remesa.CantidadCliente == undefined || $scope.Modelo.Remesa.CantidadCliente == 0 || $scope.Modelo.Remesa.CantidadCliente == "") {
                $scope.Modelo.Remesa.CantidadCliente = 1
            }
            $scope.Modelo.Remesa.ValorFleteCliente = 0
            $scope.Modelo.Remesa.ValorManejoCliente = 0
            $scope.Modelo.Remesa.ValorSeguroCliente = 0
            $scope.Modelo.Remesa.ValorFleteTransportador = 0
            var Calculo = false
            //Pendiente calcular fletes y consultar tarifas
            //if (ObjetoEnListado(27, $scope.ListaTipoTarifaCargaVenta)) {
            //    if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
            //        for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
            //            if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == 27)) {
            //                $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.Remesa.PesoCliente * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
            //                $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
            //                $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
            //                Calculo = true
            //            }
            //        }
            //    }
            //} else {
            $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente)
            if ($scope.ManejoPesoVolumetrico) {
                if ($scope.Modelo.Alto !== undefined && $scope.Modelo.Alto !== '' && $scope.Modelo.Alto !== '' &&
                    $scope.Modelo.Ancho !== undefined && $scope.Modelo.Ancho !== '' && $scope.Modelo.Ancho !== '' &&
                    $scope.Modelo.Largo !== undefined && $scope.Modelo.Largo !== '' && $scope.Modelo.Largo !== '') {
                    $scope.Modelo.PesoVolumetrico = ((RevertirMV($scope.Modelo.Alto) * RevertirMV($scope.Modelo.Ancho) * RevertirMV($scope.Modelo.Largo)) / 1000000) * 400
                    $scope.Modelo.PesoVolumetrico = $scope.Modelo.PesoVolumetrico.toFixed(2)
                }
                if (RevertirMV($scope.Modelo.Remesa.PesoCliente) > 0 || $scope.Modelo.PesoVolumetrico > 0) {
                    if (RevertirMV($scope.Modelo.Remesa.PesoCliente) > $scope.Modelo.PesoVolumetrico) {
                        $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente)
                    }
                    if ($scope.Modelo.PesoVolumetrico > RevertirMV($scope.Modelo.Remesa.PesoCliente)) {
                        $scope.Modelo.PesoCobrar = $scope.Modelo.PesoVolumetrico
                    }
                } else {
                    $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente)
                }
            } else {
                $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente)
            }
            if ($scope.Modelo.PesoCobrar > 0) {

                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 302) {
                    for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == $scope.Modelo.Remesa.ProductoTransportado.Codigo)) {
                            $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.Remesa.CantidadCliente * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                            try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                            $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                            $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                            Calculo = true
                        }
                    }
                }
                if ($scope.Modelo.PesoCobrar >= 5 && $scope.Modelo.PesoCobrar <= 30) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                    }

                }
                if ($scope.Modelo.PesoCobrar >= 30) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                    }
                }
                if ($scope.Modelo.PesoCobrar > 0 && $scope.Modelo.PesoCobrar <= 5) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                    }

                }
                if ($scope.Modelo.PesoCobrar > 5 && $scope.Modelo.PesoCobrar <= 10) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                    }
                }
                if ($scope.Modelo.PesoCobrar > 10 && $scope.Modelo.PesoCobrar <= 20) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                    }
                }
                if ($scope.Modelo.PesoCobrar > 20 && $scope.Modelo.PesoCobrar <= 30) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    try { $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado) / 100 } catch (e) { }
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }
                        }
                    }
                }
                //}
                if (Calculo == false) {
                    ShowError('No hay tarifa disponible para las condiciones especificadas')
                    //$scope.Modelo.Remesa.PesoCliente = 0
                }
                $scope.Modelo.Remesa.TotalFleteCliente = parseInt(RevertirMV($scope.Modelo.Remesa.ValorFleteCliente))
                $scope.Modelo.FletePactado = parseInt(RevertirMV($scope.Modelo.Remesa.ValorFleteCliente))
                try {
                    var valminimo = $scope.PaqueteriaMinimoValorSeguro * ($scope.PaqueteriaPorcentajeSeguro / 100)
                    if (parseInt(MascaraNumero($scope.Modelo.Remesa.ValorComercialCliente)) > 0) {
                        if ((parseInt(MascaraNumero($scope.Modelo.Remesa.ValorComercialCliente)) * ($scope.PaqueteriaPorcentajeSeguro / 100)) > valminimo) {
                            $scope.Modelo.Remesa.ValorSeguroCliente = parseInt(MascaraNumero($scope.Modelo.Remesa.ValorComercialCliente)) * ($scope.PaqueteriaPorcentajeSeguro / 100)
                        } else {
                            $scope.Modelo.Remesa.ValorSeguroCliente = $scope.PaqueteriaMinimoValorSeguro * ($scope.PaqueteriaPorcentajeSeguro / 100)
                        }
                    } else {
                        $scope.Modelo.Remesa.ValorSeguroCliente = $scope.PaqueteriaMinimoValorSeguro * ($scope.PaqueteriaPorcentajeSeguro / 100)
                    }
                } catch (e) {
                    try {
                        $scope.Modelo.Remesa.ValorSeguroCliente = $scope.PaqueteriaMinimoValorSeguro * ($scope.PaqueteriaPorcentajeSeguro / 100)
                    } catch (e) {
                    }
                }

                if ($scope.Modelo.Remesa.ValorManejoCliente > 0) {
                    $scope.Modelo.Remesa.TotalFleteCliente += $scope.Modelo.Remesa.ValorManejoCliente
                }
                if ($scope.Modelo.Remesa.ValorSeguroCliente > 0) {
                    $scope.Modelo.Remesa.TotalFleteCliente += $scope.Modelo.Remesa.ValorSeguroCliente
                }
                $scope.ValorReexpedicion = 0
                $scope.ValorReexpedicionExterna = 0
                try {
                    for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                        //$scope.Modelo.Remesa.TotalFleteCliente += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor))
                        $scope.ValorReexpedicion += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor))
                        if ($scope.ListaReexpedicionOficinas[i].TipoReexpedicion.Codigo == "11102") {
                            $scope.ValorReexpedicionExterna += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor))
                        }
                    }
                    $scope.ValorReexpedicion = MascaraValores($scope.ValorReexpedicion)
                    $scope.Modelo.ValorReexpedicion = $scope.ValorReexpedicion
                    if ($scope.ValorReexpedicionExterna > 0) {
                        $scope.Modelo.Remesa.TotalFleteCliente = parseFloat(MascaraDecimales($scope.Modelo.Remesa.TotalFleteCliente)) + parseFloat(MascaraDecimales($scope.ValorReexpedicionExterna))
                    }

                } catch (e) {
                }
                //--Calculos Factura
                $scope.ValorRemesas = $scope.Modelo.Remesa.TotalFleteCliente;
                //--Calculos Factura
            }
            $scope.MaskValores()
        }
        $scope.CalcularAlterno = function () {
            if ($scope.Modelo.Remesa.CantidadCliente == undefined || $scope.Modelo.Remesa.CantidadCliente == 0 || $scope.Modelo.Remesa.CantidadCliente == "") {
                $scope.Modelo.Remesa.CantidadCliente = 1
            }
            $scope.Modelo.Remesa.ValorFleteCliente = 0
            $scope.Modelo.Remesa.ValorManejoCliente = 0
            $scope.Modelo.Remesa.ValorSeguroCliente = 0
            $scope.Modelo.PesoVolumetrico = 0
            var Calculo = false
            $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente)
            if ($scope.ManejoPesoVolumetrico) {
                if ($scope.Modelo.Alto !== undefined && $scope.Modelo.Alto !== '' && $scope.Modelo.Alto !== '' &&
                    $scope.Modelo.Ancho !== undefined && $scope.Modelo.Ancho !== '' && $scope.Modelo.Ancho !== '' &&
                    $scope.Modelo.Largo !== undefined && $scope.Modelo.Largo !== '' && $scope.Modelo.Largo !== '') {
                    $scope.Modelo.PesoVolumetrico = ((RevertirMV($scope.Modelo.Alto) * RevertirMV($scope.Modelo.Ancho) * RevertirMV($scope.Modelo.Largo)) / 1000000) * 400
                    $scope.Modelo.PesoVolumetrico = $scope.Modelo.PesoVolumetrico.toFixed(2)
                }
                if (RevertirMV($scope.Modelo.Remesa.PesoCliente) > 0 || $scope.Modelo.PesoVolumetrico > 0) {
                    if (RevertirMV($scope.Modelo.Remesa.PesoCliente) > $scope.Modelo.PesoVolumetrico) {
                        $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente)
                    }
                    if ($scope.Modelo.PesoVolumetrico > RevertirMV($scope.Modelo.Remesa.PesoCliente)) {
                        $scope.Modelo.PesoCobrar = $scope.Modelo.PesoVolumetrico
                    }
                } else {
                    $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente)
                }
            } else {
                $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente)
            }

            //Pendiente calcular fletes y consultar tarifas
            //if (ObjetoEnListado(27, $scope.ListaTipoTarifaCargaVenta)) {
            //    if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
            //        for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
            //            if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == 27)) {
            //                $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.Remesa.PesoCliente * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
            //                $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
            //                $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
            //                Calculo = true
            //            }
            //        }
            //    }
            //} else {
            if ($scope.Modelo.PesoCobrar > 0) {
                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 302) {
                    for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == $scope.Modelo.Remesa.ProductoTransportado.Codigo)) {
                            $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.Remesa.CantidadCliente * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                            $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                            $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                            Calculo = true
                        }
                    }
                }
                if ($scope.Modelo.PesoCobrar >= 5 && $scope.Modelo.PesoCobrar <= 30) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                    }

                }
                if ($scope.Modelo.PesoCobrar >= 30) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                    }
                }
                if ($scope.Modelo.PesoCobrar > 0 && $scope.Modelo.PesoCobrar <= 5) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                    }

                }
                if ($scope.Modelo.PesoCobrar > 5 && $scope.Modelo.PesoCobrar <= 10) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                    }
                }
                if ($scope.Modelo.PesoCobrar > 10 && $scope.Modelo.PesoCobrar <= 20) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                    }
                }
                if ($scope.Modelo.PesoCobrar > 20 && $scope.Modelo.PesoCobrar <= 30) {
                    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[0], $scope.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[1], $scope.ListaTipoTarifaCargaVenta)) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 200) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }

                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 201) {
                            for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[0] || $scope.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[1])) {
                                    $scope.Modelo.Remesa.ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete
                                    $scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                                    $scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                                    Calculo = true
                                }
                            }
                        }
                    }
                }
                //}
                if (Calculo == false) {
                    //ShowError('No hay tarifa disponible para las condiciones especificadas')
                    $scope.Modelo.Remesa.PesoCliente = 0
                }

                //$scope.Modelo.Remesa.TotalFleteCliente = $scope.Modelo.Remesa.ValorFleteCliente
                $scope.Modelo.Remesa.TotalFleteCliente = parseInt(RevertirMV($scope.Modelo.Remesa.ValorFleteCliente))
                $scope.Modelo.FletePactado = parseInt(RevertirMV($scope.Modelo.Remesa.ValorFleteCliente))
                if ($scope.Modelo.Remesa.ValorManejoCliente > 0) {
                    $scope.Modelo.Remesa.TotalFleteCliente += $scope.Modelo.Remesa.ValorManejoCliente
                }
                if ($scope.Modelo.Remesa.ValorSeguroCliente > 0) {
                    $scope.Modelo.Remesa.TotalFleteCliente += $scope.Modelo.Remesa.ValorSeguroCliente
                }
                $scope.ValorReexpedicion = 0
                try {
                    for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                        //$scope.Modelo.Remesa.TotalFleteCliente += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor))
                        $scope.ValorReexpedicion += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor))
                    }
                    $scope.ValorReexpedicion = MascaraValores($scope.ValorReexpedicion)
                    $scope.Modelo.ValorReexpedicion = $scope.ValorReexpedicion
                } catch (e) {
                }
            }
            $scope.MaskValores()
        }
        $scope.ConsultarDatosClienteRemitente = function (Cliente) {
            $scope.BloqueoRemitenteNombre = false;
            $scope.BloqueoRemitenteIdentificacion = false;
            $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = Cliente.NumeroIdentificacion;
            $scope.Modelo.Remesa.Remitente.Codigo = Cliente.Codigo;
            $scope.ObtenerRemitente(false, Cliente);

        }

        $scope.ObtenerRemitente = function (Obtener, Cliente) {
            $scope.RemitenteNoRegistrado = false

            if ($scope.Modelo.Remesa.Remitente.NumeroIdentificacion !== undefined && $scope.Modelo.Remesa.Remitente.NumeroIdentificacion !== null && $scope.Modelo.Remesa.Remitente.NumeroIdentificacion !== '') {

                if (Obtener) {
                    $scope.Modelo.Remesa.Remitente.Codigo = 0
                }

                var FiltroRemitente = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Modelo.Remesa.Remitente.Codigo,
                    NumeroIdentificacion: $scope.Modelo.Remesa.Remitente.NumeroIdentificacion

                }

                TercerosFactory.Obtener(FiltroRemitente).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {

                                $scope.Modelo.Remesa.Remitente = response.data.Datos
                                if ($scope.Modelo.Remesa.Remitente.Direcciones.length > 0) {
                                    $scope.ListadoDireccionesRemitente = $scope.Modelo.Remesa.Remitente.Direcciones;
                                } else {
                                    $scope.ListadoDireccionesRemitente = []
                                }
                                if ($scope.Modelo.Remesa.Remitente.Direccion !== '' && $scope.Modelo.Remesa.Remitente.Direccion !== undefined && $scope.Modelo.Remesa.Remitente.Direccion !== null) {
                                    $scope.ListadoDireccionesRemitente.push({ Direccion: $scope.Modelo.Remesa.Remitente.Direccion });
                                    $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[0].Direccion;

                                }
                                $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[0].Direccion;
                                $scope.Modelo.Remesa.Remitente.Ciudad = $scope.CargarCiudad($scope.Modelo.Remesa.Remitente.Ciudad.Codigo)
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.Modelo.Remesa.Remitente.TipoIdentificacion.Codigo);
                                $scope.Modelo.Remesa.Remitente.Telefonos = $scope.Modelo.Remesa.Remitente.Telefonos.replace(';', ' - ')
                                $scope.BloqueoRemitente = true;
                                $scope.Modelo.Remesa.Remitente.Nombre = response.data.Datos.NombreCompleto;
                                if (Cliente !== undefined && Cliente !== null && Cliente !== '') {
                                    $scope.BloqueoRemitenteNombre = true;
                                    $scope.BloqueoRemitenteIdentificacion = true;

                                    $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = Cliente.NumeroIdentificacion;
                                    $scope.Modelo.Remesa.Remitente.Nombre = response.data.Datos.NombreCompleto;
                                    $scope.Modelo.Remesa.Destinatario = JSON.parse(JSON.stringify($scope.Modelo.Remesa.Remitente))
                                    $scope.Modelo.Remesa.Destinatario.Ciudad = ''
                                    $scope.ObtenerDestinatario()
                                } else {
                                    $scope.BloqueoRemitenteNombre = false;
                                    $scope.BloqueoRemitenteIdentificacion = false;
                                }
                                $scope.ObtenerTarifarioCliente()
                                try {
                                    $scope.ListadoLineaServicio = []
                                    $scope.ListadoLineaServicio = response.data.Datos.LineaServicio
                                    $scope.ListadoLineaServicio.push({ Codigo: 19400, Nombre: '(NO APLICA)' })
                                    if ($scope.ListadoLineaServicio.length > 0) {
                                        $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $linq.Enumerable().From($scope.ListadoLineaServicio).First('$.Codigo ==' + 19400);
                                        //$scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $scope.ListadoLineaServicio[0]
                                    }
                                } catch (e) {
                                    $scope.ListadoLineaServicio = []

                                }
                            }
                            else {
                                $scope.Modelo.Remesa.Remitente.Nombre = ''
                                $scope.ListadoDireccionesRemitente = []
                                $scope.Modelo.Remesa.Remitente.Direccion = ''
                                $scope.Modelo.Remesa.Remitente.Ciudad = ''
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                                $scope.Modelo.Remesa.Remitente.Telefonos = ''
                                $scope.BloqueoRemitente = false
                                $scope.BloqueoRemitenteNombre = false;
                                $scope.BloqueoRemitenteIdentificacion = false;
                                $scope.RemitenteNoRegistrado = true

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });

            }


        }

        $scope.ObtenerRemitenteAlterno = function (Codigo) {

            var FiltroRemitente = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: Codigo,
                NumeroIdentificacion: $scope.Modelo.Remesa.Remitente.NumeroIdentificacion

            }

            TercerosFactory.Obtener(FiltroRemitente).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            try {
                                $scope.ListadoLineaServicio = []
                                $scope.ListadoLineaServicio = response.data.Datos.LineaServicio
                                $scope.ListadoLineaServicio.push({ Codigo: 19400, Nombre: '(NO APLICA)' })
                                if ($scope.ListadoLineaServicio.length > 0) {
                                    $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $linq.Enumerable().From($scope.ListadoLineaServicio).First('$.Codigo ==' + 19400);
                                    //$scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $scope.ListadoLineaServicio[0]
                                }
                                try {
                                    $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $linq.Enumerable().From($scope.ListadoLineaServicio).First('$.Codigo ==' + $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente.Codigo);
                                } catch (e) {

                                }
                            } catch (e) {
                                $scope.ListadoLineaServicio = []

                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        }


        $scope.ObtenerDestinatario = function (Obtener) {
            $scope.DestinatarioNoRegistrado = false

            $scope.BloqueoDestinatario = false;
            if ($scope.Modelo.Remesa.Destinatario.NumeroIdentificacion !== undefined && $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion !== null && $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion !== '' && isNaN($scope.Modelo.Remesa.Destinatario.NumeroIdentificacion) !== true) {

                if (Obtener) {
                    $scope.Modelo.Remesa.Destinatario.Codigo = 0
                }


                var FiltroDestinatario = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Modelo.Remesa.Destinatario.Codigo,
                    NumeroIdentificacion: $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion
                }

                TercerosFactory.Obtener(FiltroDestinatario).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                //if (Obtener) {
                                //    $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion = response.data.Datos.NumeroIdentificacion
                                //    $scope.Modelo.Remesa.Destinatario.Nombre = response.data.Datos.NombreCompleto
                                //    $scope.Modelo.Remesa.Destinatario.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + response.data.Datos.Ciudad.Codigo);
                                //    $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.TipoIdentificacion.Codigo);

                                //}
                                //else {
                                $scope.Modelo.Remesa.Destinatario = response.data.Datos
                                $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion = response.data.Datos.NumeroIdentificacion
                                $scope.Modelo.Remesa.Destinatario.Nombre = response.data.Datos.NombreCompleto
                                $scope.Modelo.Remesa.Destinatario.Ciudad = ''
                                $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.TipoIdentificacion.Codigo);
                                if ($scope.Modelo.Remesa.Destinatario.Direcciones.length > 0) {
                                    $scope.ListadoDireccionesDestinatario = $scope.Modelo.Remesa.Destinatario.Direcciones;
                                } else {
                                    $scope.ListadoDireccionesDestinatario = []
                                }
                                if ($scope.Modelo.Remesa.Destinatario.Direccion !== '' && $scope.Modelo.Remesa.Destinatario.Direccion !== undefined && $scope.Modelo.Remesa.Destinatario.Direccion !== null) {
                                    $scope.ListadoDireccionesDestinatario.push({ Direccion: $scope.Modelo.Remesa.Destinatario.Direccion });
                                    $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[0].Direccion;

                                }
                                //$scope.Modelo.Remesa.Destinatario.Ciudad = $scope.CargarCiudad($scope.Modelo.Remesa.Destinatario.Ciudad.Codigo)
                                $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.Modelo.Remesa.Destinatario.TipoIdentificacion.Codigo);
                                $scope.Modelo.Remesa.Destinatario.Telefonos = $scope.Modelo.Remesa.Destinatario.Telefonos.replace(';', ' - ')

                                //}
                                $scope.BloqueoDestinatario = true
                            }
                            else {
                                $scope.Modelo.Remesa.Destinatario.Nombre = ''
                                $scope.ListadoDireccionesDestinatario = []
                                $scope.Modelo.Remesa.Destinatario.Direccion = ''
                                $scope.Modelo.Remesa.Destinatario.Ciudad = ''
                                $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                                $scope.Modelo.Remesa.Destinatario.Telefonos = ''
                                $scope.BloqueoDestinatario = false
                                //ShowInfo('El destinatario con identificación No.' + FiltroDestinatario.NumeroIdentificacion + ' no se encuentra registrado, sin embargo puede crearlo llenando el resto de campos.')
                                $scope.DestinatarioNoRegistrado = true

                            }
                        }
                        else {
                            $scope.Modelo.Destinatario.Nombre = ''
                            $scope.ListadoDireccionesDestinatario = []
                            $scope.Modelo.Destinatario.Direccion = ''
                            $scope.Modelo.Destinatario.Ciudad = ''
                            $scope.Modelo.Destinatario.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                            $scope.Modelo.Destinatario.Telefonos = ''
                            $scope.BloqueoDestinatario = false
                            //ShowInfo('El destinatario con identificación No.' + FiltroDestinatario.NumeroIdentificacion + ' no se encuentra registrado, sin embargo puede crearlo llenando el resto de campos.')
                            $scope.DestinatarioNoRegistrado = true
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });


            }


        }


        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando tarifario ventas código ' + $scope.Modelo.Numero);

            $timeout(function () {
                blockUI.message('Cargando tarifario ventas Código ' + $scope.Modelo.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
            };

            blockUI.delay = 1000;
            RemesaGuiasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.Remesa.Numero = response.data.Datos.Remesa.Numero;
                        $scope.Modelo.Remesa.NumeroDocumento = response.data.Datos.Remesa.NumeroDocumento;
                        $scope.Modelo.Remesa.Fecha = new Date(response.data.Datos.Remesa.Fecha);
                        $scope.Modelo.Remesa.Numeracion = response.data.Datos.Numeracion;

                        $scope.CodigoTipoEntregaRemesaPaqueteria = response.data.Datos.TipoEntregaRemesaPaqueteria.Codigo;

                        if ($scope.ListadoTipoEntregaRemesaPaqueteria !== undefined && $scope.ListadoTipoEntregaRemesaPaqueteria !== null) {
                            if ($scope.ListadoTipoEntregaRemesaPaqueteria.length > 0) {
                                $scope.Modelo.TipoEntregaRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoEntregaRemesaPaqueteria).First('$.Codigo ==' + $scope.CodigoTipoEntregaRemesaPaqueteria);
                            }
                        }


                        if (response.data.Datos.Remesa.Cliente.Codigo > 0) {
                            $scope.Modelo.Remesa.Cliente = response.data.Datos.Remesa.Cliente;
                            if ($scope.ListaClientes !== undefined && $scope.ListaClientes !== null) {
                                if ($scope.ListaClientes.length > 0) {
                                    try {
                                        $scope.Modelo.Remesa.Cliente = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo ==' + $scope.Modelo.Remesa.Cliente.Codigo);

                                    } catch (e) {

                                    }
                                }
                            }
                        }

                        $scope.Modelo.Remesa.NumeroDocumentoCliente = response.data.Datos.Remesa.NumeroDocumentoCliente;


                        ////-- REMITENTE
                        $scope.CodigoTipoIdentificacionRemitente = response.data.Datos.CodigoTipoIdentificacionRemitente;
                        if ($scope.ListadoTipoIdentificacion !== undefined && $scope.ListadoTipoIdentificacion !== null) {
                            if ($scope.ListadoTipoIdentificacion.length > 0) {
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRemitente);
                            }
                        }

                        $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = response.data.Datos.Remesa.Remitente.NumeroIdentificacion;
                        $scope.Modelo.Remesa.Remitente.Nombre = response.data.Datos.Remesa.Remitente.Nombre;
                        $scope.Modelo.Remesa.Remitente.Ciudad = $scope.CargarCiudad(response.data.Datos.Remesa.Remitente.Ciudad.Codigo)
                        $scope.Modelo.Remesa.Destinatario.Ciudad = $scope.CargarCiudad(response.data.Datos.Remesa.Destinatario.Ciudad.Codigo)
                        $scope.Modelo.Remesa.BarrioRemitente = response.data.Datos.Remesa.BarrioRemitente;
                        try {
                            $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = response.data.Datos.LineaSerivicioCliente
                            $scope.Modelo.Remesa.CentroCosto = response.data.Datos.CentroCosto
                            $scope.Modelo.Remesa.Remitente.SitioCargue = { SitioCliente: response.data.Datos.SitioCargue }
                            $scope.Modelo.Remesa.Destinatario.SitioDescargue = { SitioCliente: response.data.Datos.SitioDescargue }
                            $scope.Modelo.Remesa.FechaEntrega = new Date(response.data.Datos.FechaEntrega)

                            $scope.ObtenerRemitenteAlterno($scope.Modelo.Remesa.Remitente.Codigo)
                        } catch (e) {

                        }


                        //Para el remitente y destinatario por ahora se carga en el combo solo la dirección guardada
                        $scope.ListadoDireccionesRemitente = [];
                        $scope.ListadoDireccionesRemitente.push({ Direccion: response.data.Datos.Remesa.Remitente.Direccion })
                        $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[0].Direccion;

                        $scope.ListadoDireccionesDestinatario = [];
                        $scope.ListadoDireccionesDestinatario.push({ Direccion: response.data.Datos.Remesa.Destinatario.Direccion })
                        $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[0].Direccion;

                        $scope.Modelo.Remesa.CodigoPostalRemitente = response.data.Datos.Remesa.CodigoPostalRemitente;
                        $scope.Modelo.Remesa.Remitente.Telefonos = response.data.Datos.Remesa.Remitente.Telefonos;
                        $scope.Modelo.Remesa.ObservacionesRemitente = response.data.Datos.Remesa.ObservacionesRemitente;


                        ////-- DESTINATARIO
                        $scope.CodigoTipoIdentificacionDestinatario = response.data.Datos.CodigoTipoIdentificacionDestinatario;
                        if ($scope.ListadoTipoIdentificacion !== undefined && $scope.ListadoTipoIdentificacion !== null) {
                            if ($scope.ListadoTipoIdentificacion.length > 0) {
                                $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionDestinatario);
                            }
                        }

                        $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion = response.data.Datos.Remesa.Destinatario.NumeroIdentificacion;
                        $scope.Modelo.Remesa.Destinatario.Nombre = response.data.Datos.Remesa.Destinatario.Nombre;

                        $scope.Modelo.Remesa.BarrioDestinatario = response.data.Datos.Remesa.BarrioDestinatario;

                        $scope.Modelo.Remesa.CodigoPostalDestinatario = response.data.Datos.Remesa.CodigoPostalDestinatario;
                        $scope.Modelo.Remesa.Destinatario.Telefonos = response.data.Datos.Remesa.Destinatario.Telefonos;
                        $scope.Modelo.Remesa.ObservacionesDestinatario = response.data.Datos.Remesa.ObservacionesDestinatario;

                        ////--DETALLE
                        $scope.Modelo.Remesa.FormaPago = response.data.Datos.Remesa.FormaPago;
                        if ($scope.ListadoFormaPago !== undefined && $scope.ListadoFormaPago !== null) {
                            if ($scope.ListadoFormaPago.length > 0) {
                                $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.Modelo.Remesa.FormaPago.Codigo);
                            }
                        }

                        $scope.Modelo.Remesa.PesoCliente = response.data.Datos.Remesa.PesoCliente;
                        $scope.Modelo.Remesa.CantidadCliente = response.data.Datos.Remesa.CantidadCliente;
                        $scope.Modelo.Remesa.ValorComercialCliente = response.data.Datos.Remesa.ValorComercialCliente;

                        $scope.Modelo.Remesa.ProductoTransportado = response.data.Datos.Remesa.ProductoTransportado;

                        if ($scope.ListadoProductoTransportados !== undefined && $scope.ListadoProductoTransportados !== null) {
                            $scope.Modelo.Remesa.ProductoTransportado = $linq.Enumerable().From($scope.ListadoProductoTransportados).First('$.Codigo ==' + $scope.Modelo.Remesa.ProductoTransportado.Codigo);
                        }

                        $scope.CodigoTarifaCarga = response.data.Datos.CodigoTarifaTransporte;
                        $scope.CodigoTipoTarifaTransporte = response.data.Datos.CodigoTipoTarifaTransporte;
                        $scope.Modelo.DescripcionProductoTransportado = response.data.Datos.DescripcionProductoTransportado;


                        //Arma Tarifa consultada
                        $scope.ListaTarifaCarga = [{ Codigo: 0, Nombre: '(Seleccione Item)' },
                        { Codigo: response.data.Datos.CodigoTarifaTransporte, Nombre: response.data.Datos.NombreTarifa }];
                        $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + $scope.CodigoTarifaCarga);

                        //Arma Tipo de tarifa consultada
                        $scope.ListaTipoTarifaCargaVenta = [
                            { CodigoDetalleTarifa: 0, Codigo: 0, CodigoTarifa: 0, Nombre: '(Seleccione Item)' },
                            { CodigoDetalleTarifa: response.data.Datos.Remesa.DetalleTarifaVenta.Codigo, Codigo: $scope.CodigoTipoTarifaTransporte, CodigoTarifa: $scope.CodigoTarifaCarga, Nombre: response.data.Datos.NombreTipoTarifa },
                        ];
                        $scope.Modelo.Remesa.TipoTarifaCarga = $linq.Enumerable().From($scope.ListaTipoTarifaCargaVenta).First('$.Codigo ==' + $scope.CodigoTipoTarifaTransporte);

                        ////--LIQUIDACION
                        $scope.Modelo.Remesa.ValorFleteTransportador = response.data.Datos.Remesa.ValorFleteTransportador;
                        $scope.Modelo.Remesa.ValorFleteCliente = response.data.Datos.Remesa.ValorFleteCliente;
                        $scope.Modelo.Remesa.ValorManejoCliente = response.data.Datos.Remesa.ValorManejoCliente;
                        $scope.Modelo.Remesa.ValorSeguroCliente = response.data.Datos.Remesa.ValorSeguroCliente;
                        //$scope.Modelo.Remesa.ValorReexpedicion
                        $scope.Modelo.Remesa.TotalFleteCliente = response.data.Datos.Remesa.TotalFleteCliente;

                        $scope.Modelo.Remesa.Observaciones = response.data.Datos.Remesa.Observaciones;

                        ////--INFORMACION DESPACHO
                        $scope.Modelo.OficinaOrigen = response.data.Datos.OficinaOrigen;
                        if ($scope.Modelo.AjusteFlete > 0) {
                            $scope.Modelo.AjusteFlete = true
                        }

                        $scope.Modelo.Oficina = response.data.Datos.Oficina;

                        try {
                            $scope.CodigoOficina = response.data.Datos.OficinaDestino.Codigo;
                            if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.CodigoOficina);
                            }
                        } catch (e) {

                        }
                        $scope.Modelo.OficinaActual = response.data.Datos.OficinaActual;
                        $scope.Modelo.EstadoGuia = response.data.Datos.EstadoGuia;
                        $scope.Modelo.Remesa.NumeroPlanilla = response.data.Datos.Remesa.NumeroPlanilla;
                        $scope.Modelo.Remesa.Vehiculo = response.data.Datos.Remesa.Vehiculo;
                        $scope.Modelo.Remesa.Conductor = response.data.Datos.Remesa.Conductor;
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.CodigoEstadoDocumento = response.data.Datos.Estado;
                        ////-- ESTADO
                        if ($scope.ListaEstadoDocumento !== undefined && $scope.ListaEstadoDocumento !== null) {
                            if ($scope.ListaEstadoDocumento.length > 0) {
                                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstadoDocumento).First('$.Codigo ==' + response.data.Datos.Estado);
                            }
                        }

                        $scope.TipoRemesa = response.data.Datos.CodigoTipoRemesa;
                        $scope.AplicaReexpedicion = response.data.Datos.AplicaReexpedicion;
                        $scope.Modelo.EstadoGuia = response.data.Datos.EstadoGuia;
                        $scope.ListaReexpedicionOficinas = response.data.Datos.ListaReexpedicionOficinas
                        $scope.ValorReexpedicion = 0
                        try {
                            for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                                $scope.ValorReexpedicion += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor))
                            }
                            $scope.ValorReexpedicion = MascaraValores($scope.ValorReexpedicion)
                        } catch (e) {
                        }
                        if ($scope.CodigoEstadoDocumento == ESTADO_ACTIVO) {
                            $scope.Deshabilitar = true;
                            $scope.DeshabilitarOficinaDestino = true;
                            $scope.BloqueoRemitenteNombre = true;
                            $scope.BloqueoRemitenteIdentificacion = true;
                            $scope.BloqueoDestinatario = true;

                        } else {
                            $scope.Deshabilitar = false;
                            $scope.DeshabilitarOficinaDestino = false;
                            $scope.BloqueoRemitenteNombre = false;
                            $scope.BloqueoRemitenteIdentificacion = false;
                            $scope.BloqueoDestinatario = false;
                        }

                        $scope.MaskValores()
                    }
                    else {
                        ShowError('No se logro consultar el tarifario ventas código ' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarRemesasPaqueteriaPortal';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                        document.location.href = '#!ConsultarRemesasPaqueteriaPortal';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                $scope.Modelo.Remesa.Fecha = RetornarFechaEspecificaSinTimeZone($scope.Modelo.Remesa.Fecha);
                //if ($scope.Modelo.Remesa.Remitente.Codigo !== undefined && $scope.Modelo.Remesa.Remitente.Codigo > 0) {
                //    try {
                //        if ($scope.Modelo.Remesa.Remitente.Direccion.Direccion !== undefined) {
                //            $scope.Modelo.Remesa.Remitente.Direccion = $scope.Modelo.Remesa.Remitente.Direccion.Direccion
                //        }
                //    } catch (e) {

                //    }
                //}
                //if ($scope.Modelo.Remesa.Destinatario.Codigo !== undefined && $scope.Modelo.Remesa.Destinatario.Codigo > 0) {
                //    try {
                //        if ($scope.Modelo.Remesa.Destinatario.Direccion.Direccion !== undefined) {
                //            $scope.Modelo.Remesa.Destinatario.Direccion = $scope.Modelo.Remesa.Destinatario.Direccion.Direccion;
                //        }
                //    } catch (e) {

                //    }
                //}
                if ($scope.Modelo.Remesa.ProductoTransportado.Codigo == undefined || $scope.Modelo.Remesa.ProductoTransportado.Codigo == 0) {
                    $scope.Modelo.Remesa.ProductoTransportado = { Nombre: $scope.Modelo.Remesa.ProductoTransportado, Descripcion: $scope.Modelo.Remesa.DescripcionProductoTransportado }
                }



                var ObtjetoRemesa = {

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Modelo.Remesa.Numero,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },

                    TipoRemesa: { Codigo: $scope.TipoRemesa },
                    NumeroDocumentoCliente: $scope.Modelo.Remesa.NumeroDocumentoCliente,
                    FechaDocumentoCliente: MIN_DATE,
                    Fecha: $scope.Modelo.Remesa.Fecha,
                    Ruta: { Codigo: 0 },
                    ProductoTransportado: $scope.Modelo.Remesa.ProductoTransportado,
                    FormaPago: $scope.Modelo.Remesa.FormaPago,
                    Cliente: { Codigo: $scope.Modelo.Remesa.Cliente.Codigo },
                    Remitente: $scope.Modelo.Remesa.Remitente,

                    CiudadRemitente: { Codigo: $scope.Modelo.Remesa.Remitente.Ciudad.Codigo },
                    DireccionRemitente: $scope.Modelo.Remesa.Remitente.Direccion,
                    TelefonoRemitente: $scope.Modelo.Remesa.Remitente.Telefonos,
                    Observaciones: $scope.Modelo.Remesa.Observaciones,

                    CantidadCliente: $scope.Modelo.Remesa.CantidadCliente,
                    PesoCliente: RevertirMV($scope.Modelo.Remesa.PesoCliente),
                    PesoVolumetricoCliente: $scope.Modelo.Remesa.PesoVolumetricoCliente,

                    ValorFleteCliente: $scope.Modelo.Remesa.ValorFleteCliente,
                    ValorFleteTransportador: $scope.Modelo.Remesa.ValorFleteTransportador,
                    ValorManejoCliente: $scope.Modelo.Remesa.ValorManejoCliente,
                    ValorSeguroCliente: $scope.Modelo.Remesa.ValorSeguroCliente,
                    ValorDescuentoCliente: 0,
                    TotalFleteCliente: $scope.Modelo.Remesa.TotalFleteCliente,
                    ValorComercialCliente: $scope.Modelo.Remesa.ValorComercialCliente,

                    CantidadTransportador: $scope.Modelo.Remesa.CantidadTransportador,
                    PesoTransportador: $scope.Modelo.Remesa.PesoTransportador,
                    //ValorFleteTransportador: 0,
                    TotalFleteTransportador: $scope.Modelo.Remesa.TotalFleteTransportador,
                    Destinatario: {
                        Codigo: $scope.Modelo.Remesa.Destinatario.Codigo,
                        Ciudad: $scope.Modelo.Remesa.Destinatario.Ciudad,
                        TipoIdentificacion: $scope.Modelo.Remesa.Destinatario.TipoIdentificacion,
                        Direccion: $scope.Modelo.Remesa.Destinatario.Direccion,
                        Telefonos: $scope.Modelo.Remesa.Destinatario.Telefonos,
                        NumeroIdentificacion: $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion,
                        Nombre: $scope.Modelo.Remesa.Destinatario.Nombre,
                        Barrio: $scope.Modelo.Remesa.BarrioDestinatario,
                    },
                    CiudadDestinatario: { Codigo: $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo },
                    DireccionDestinatario: $scope.Modelo.Remesa.Destinatario.Direccion,
                    TelefonoDestinatario: $scope.Modelo.Remesa.Destinatario.Telefonos,

                    Estado: $scope.Modelo.Estado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },

                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },

                    TarifarioVenta: { Codigo: $scope.Modelo.Remesa.NumeroTarifarioVenta },
                    NumeroOrdenServicio: 0,

                    BarrioRemitente: $scope.Modelo.Remesa.BarrioRemitente,
                    CodigoPostalRemitente: $scope.Modelo.Remesa.CodigoPostalRemitente,
                    BarrioDestinatario: $scope.Modelo.Remesa.BarrioDestinatario,
                    CodigoPostalDestinatario: $scope.Modelo.Remesa.CodigoPostalDestinatario,
                    Estado: $scope.Modelo.Estado.Codigo,
                    DetalleTarifaVenta: { Codigo: $scope.Modelo.Remesa.TipoTarifaCarga.CodigoDetalleTarifa },


                }

                $scope.AplicaReexpedicion = 0;
                if ($scope.ListaReexpedicionOficinas !== undefined && $scope.ListaReexpedicionOficinas !== null) {
                    if ($scope.ListaReexpedicionOficinas.length > 0) {
                        $scope.AplicaReexpedicion = 1;
                    }
                }

                var ObtjetoFactura = {}

                if ($scope.Modelo.Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO) {
                    var ClienteFactura = 0;
                    if ($scope.Modelo.Remesa.Cliente == undefined || $scope.Modelo.Remesa.Cliente == '' || $scope.Modelo.Remesa.Cliente == null) {
                        ClienteFactura = $scope.Modelo.Remesa.Remitente.Codigo;
                    }
                    else {
                        ClienteFactura = $scope.Modelo.Remesa.Cliente.Codigo;
                    }
                    ObtjetoFactura = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_FACTURAS,
                        NumeroPreImpreso: CERO,
                        CataTipoFactura: CODIGO_CATALOGO_TIPO_FACTURA,
                        Fecha: $scope.Modelo.Remesa.Fecha,
                        OficinaFactura: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        Cliente: { Codigo: ClienteFactura },
                        FacturarA: { Codigo: ClienteFactura },
                        FormaPago: { Codigo: $scope.Modelo.Remesa.FormaPago.Codigo },
                        DiasPlazo: CERO,
                        Observaciones: "",

                        ValorRemesas: $scope.ValorRemesas,
                        ValorOtrosConceptos: CERO,
                        ValorDescuentos: CERO,
                        Subtotal: $scope.ValorRemesas,
                        ValorImpuestos: 0,
                        ValorFactura: $scope.ValorRemesas,
                        ValorTRM: CERO,
                        Moneda: { Codigo: 1 },
                        ValorMonedaAlterna: CERO,
                        ValorAnticipo: CERO,
                        ResolucionFacturacion: $scope.Sesion.UsuarioAutenticado.Oficinas.ResolucionFacturacion,
                        ControlImpresion: 1,
                        Estado: $scope.Modelo.Estado.Codigo,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Anulado: CERO,
                    }
                }

                $scope.RemesaPaqueteria = {
                    CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                    Remesa: ObtjetoRemesa,
                    Factura: ObtjetoFactura,
                    TransportadorExterno: { Codigo: 0 },
                    NumeroGuiaExterna: '',
                    TipoTransporteRemsaPaqueteria: $scope.Modelo.TipoTransporteRemsaPaqueteria,
                    TipoDespachoRemesaPaqueteria: $scope.Modelo.TipoDespachoRemesaPaqueteria,
                    TemperaturaProductoRemesaPaqueteria: $scope.Modelo.TemperaturaProductoRemesaPaqueteria,
                    TipoServicioRemesaPaqueteria: $scope.Modelo.TipoServicioRemesaPaqueteria,
                    TipoEntregaRemesaPaqueteria: $scope.Modelo.TipoEntregaRemesaPaqueteria,

                    TipoInterfazRemesaPaqueteria: $scope.Modelo.TipoInterfazRemesaPaqueteria,
                    FechaInterfazTracking: $scope.Modelo.FechaInterfaz, /// ----> fecha interfaz
                    FechaInterfazWMS: $scope.Modelo.FechaInterfaz, /////
                    UsuarioCrea: $scope.Modelo.UsuarioCrea,
                    Oficina: $scope.Modelo.Oficina,


                    EstadoRemesaPaqueteria: $scope.Modelo.EstadoGuia,

                    ObservacionesDestinatario: $scope.Modelo.Remesa.ObservacionesDestinatario,
                    ObservacionesRemitente: $scope.Modelo.Remesa.ObservacionesRemitente,

                    OficinaOrigen: { Codigo: $scope.Modelo.OficinaOrigen.Codigo },
                    OficinaDestino: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                    OficinaActual: { Codigo: $scope.Modelo.OficinaActual.Codigo },
                    DesripcionMercancia: $scope.Modelo.DescripcionProductoTransportado,
                    Reexpedicion: $scope.AplicaReexpedicion,
                    ListaReexpedicionOficinas: $scope.ListaReexpedicionOficinas,

                    //LineaSerivicioCliente: { Codigo: $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente.Codigo },
                    CentroCosto: $scope.Modelo.Remesa.CentroCosto,
                    //SitioCargue: { Codigo: $scope.Modelo.Remesa.Remitente.SitioCargue.Codigo },
                    //SitioDescargue: { Codigo: $scope.Modelo.Remesa.Destinatario.SitioDescargue.Codigo },
                    FechaEntrega: $scope.Modelo.Remesa.FechaEntrega,
                    ValorReexpedicion: $scope.Modelo.ValorReexpedicion,
                    Largo: $scope.Modelo.Largo,
                    Alto: $scope.Modelo.Alto,
                    Ancho: $scope.Modelo.Ancho,
                    PesoVolumetrico: $scope.Modelo.PesoVolumetrico,
                    PesoCobrar: $scope.Modelo.PesoCobrar,
                    FletePactado: $scope.Modelo.FletePactado,
                    AjusteFlete: $scope.Modelo.AjusteFlete == true ? 1 : 0,
                    Numeracion: $scope.Modelo.Remesa.Numeracion
                }
                try {
                    if ($scope.Modelo.Remesa.Remitente.LineaSerivicioCliente !== undefined && $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente !== '' && $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente !== null) {
                        $scope.RemesaPaqueteria.LineaSerivicioCliente = { Codigo: $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente.Codigo }
                    }
                } catch (e) {

                }
                try {
                    if ($scope.Modelo.Remesa.Remitente.SitioCargue !== undefined && $scope.Modelo.Remesa.Remitente.SitioCargue !== '' && $scope.Modelo.Remesa.Remitente.SitioCargue !== null) {
                        $scope.RemesaPaqueteria.SitioCargue = { Codigo: $scope.Modelo.Remesa.Remitente.SitioCargue.Codigo }
                    }
                } catch (e) {

                }
                try {
                    if ($scope.Modelo.Remesa.Destinatario.SitioDescargue !== undefined && $scope.Modelo.Remesa.Destinatario.SitioDescargue !== '' && $scope.Modelo.Remesa.Destinatario.SitioDescargue !== null) {
                        $scope.RemesaPaqueteria.SitioDescargue = { Codigo: $scope.Modelo.Remesa.Destinatario.SitioDescargue.Codigo }
                    }
                } catch (e) {

                }


                RemesaGuiasFactory.Guardar($scope.RemesaPaqueteria).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Numero == 0) {
                                    ShowSuccess('Se guardó la remesa con el número ' + response.data.Datos + '');
                                }
                                else {
                                    ShowSuccess('Se modificó la remesa con el número ' + response.data.Datos + '');
                                }
                                location.href = '#!ConsultarRemesasPaqueteriaPortal/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        function DatosRequeridosTarifas() {
            $scope.MensajesError = [];
            var Continuar = true;

            if ($scope.Modelo.Remesa.Remitente.Ciudad !== undefined && $scope.Modelo.Remesa.Remitente.Ciudad !== null) {
                if (!($scope.Modelo.Remesa.Remitente.Ciudad.Codigo > 0)) {
                    Continuar = false;
                }
            } else {
                Continuar = false;
            }

            if ($scope.Modelo.Remesa.Destinatario.Ciudad !== undefined && $scope.Modelo.Remesa.Destinatario.Ciudad !== null) {
                if (!($scope.Modelo.Remesa.Destinatario.Ciudad.Codigo > 0)) {
                    Continuar = false;
                }
            } else {
                Continuar = false;
            }

            if ($scope.Modelo.Remesa.FormaPago == undefined || $scope.Modelo.Remesa.FormaPago == null || $scope.Modelo.Remesa.FormaPago == '') {
                $scope.Modelo.Remesa.FormaPago = $scope.ListadoFormaPago[1]
            }

            if (Continuar == false) {
            }
            return Continuar;
        }

        $scope.ObtenerTarifarioCliente = function () {
            $scope.ListaAuxTipoTarifas = [];
            $scope.ListaTarifaCarga = [];
            $scope.ListaTipoTarifaCargaVenta = [];
            $scope.TarifarioInvalido = false

            if (DatosRequeridosTarifas() == true) {

                var TipoLineaNegocioAux = 0
                if ($scope.Modelo.Remesa.Remitente.Ciudad.Codigo == $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo) {
                    TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA;
                    CodigoTipoRemesa = CODIGO_TIPO_REMESA_URBANA;
                } else {
                    TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL;
                    CodigoTipoRemesa = CODIGO_TIPO_REMESA_NACIONAL;
                }

                $scope.CodigoCliente = 0;
                if ($scope.Modelo.Remesa.Cliente !== undefined && $scope.Modelo.Remesa.Cliente !== null && $scope.Modelo.Remesa.Cliente !== '') {
                    $scope.CodigoCliente = $scope.Modelo.Remesa.Cliente.Codigo;
                }

                $scope.TarifaCliente = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.CodigoCliente,
                    CodigoCiudadOrigen: $scope.Modelo.Remesa.Remitente.Ciudad.Codigo,
                    CodigoCiudadDestino: $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo,
                    CodigoLineaNegocio: CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA,
                    CodigoTipoLineaNegocio: TipoLineaNegocioAux,
                    CodigoFormaPago: $scope.Modelo.Remesa.FormaPago.Codigo,
                    TarifarioProveedores: false,
                    TarifarioClientes: true
                }
                TercerosFactory.Consultar($scope.TarifaCliente).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {


                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.NumeroTarifario = response.data.Datos[i].NumeroTarifarioVenta
                                var InsertaTarifa = true;
                                for (var j = 0; j < $scope.ListaTarifaCarga.length; j++) {
                                    if ($scope.ListaTarifaCarga[j].Codigo == response.data.Datos[i].TarifaCarga.Codigo) {
                                        InsertaTarifa = false;
                                    }
                                }
                                if (InsertaTarifa == true) {
                                    $scope.ListaTarifaCarga.push(response.data.Datos[i].TarifaCarga)
                                }
                                if ($scope.CodigoTarifaCarga !== undefined && $scope.CodigoTarifaCarga !== null) {
                                    if ($scope.CodigoTarifaCarga > 0) {
                                        $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + $scope.CodigoTarifaCarga);
                                        $scope.CargarTipoTarifa($scope.CodigoTarifaCarga);
                                    } else {
                                        try {
                                            $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + 302);//Porducto
                                        } catch (e) {
                                            $scope.Modelo.Remesa.TarifaCarga = $scope.ListaTarifaCarga[0];
                                        }
                                    }
                                } else {
                                    try {
                                        $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + 302);//Porducto
                                    } catch (e) {
                                        $scope.Modelo.Remesa.TarifaCarga = $scope.ListaTarifaCarga[0];
                                    }
                                    $scope.CargarTipoTarifa($scope.Modelo.Remesa.TarifaCarga.Codigo)
                                }


                                $scope.ListaAuxTipoTarifas.push({
                                    CodigoDetalleTarifa: response.data.Datos[i].CodigoDetalleTarifa,
                                    Codigo: response.data.Datos[i].TipoTarifaCarga.Codigo,
                                    CodigoTarifa: response.data.Datos[i].TarifaCarga.Codigo,
                                    Nombre: response.data.Datos[i].TipoTarifaCarga.Nombre,
                                    ValorFlete: response.data.Datos[i].ValorFlete,
                                    ValorManejo: response.data.Datos[i].ValorManejo,
                                    ValorSeguro: response.data.Datos[i].ValorSeguro,
                                    PorcentajeAfiliado: response.data.Datos[i].PorcentajeAfiliado
                                })

                                if (i == 0) {
                                    $scope.Modelo.Remesa.NumeroTarifarioVenta = response.data.Datos[i].NumeroTarifarioVenta;
                                }
                            }
                            try {
                                $scope.CargarTipoTarifa($scope.Modelo.Remesa.TarifaCarga.Codigo)

                            } catch (e) {

                            }
                            try {
                                $scope.ObtenerValoresPaqueteria()

                            } catch (e) {

                            }

                        }
                        else {
                            $scope.TarifarioInvalido = true
                        }
                    } else {
                        $scope.TarifarioInvalido = true
                    }
                    $scope.CalcularAlterno()
                });


            }

        }
        $scope.ObtenerValoresPaqueteria = function () {
            try {

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.NumeroTarifario,
                };

                blockUI.delay = 1000;
                TarifarioVentasFactory.Obtener(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {

                            if (response.data.Datos.Paqueteria !== undefined && response.data.Datos.Paqueteria !== '' && response.data.Datos.Paqueteria !== null) {
                                $scope.PaqueteriaMinimoValorSeguro = response.data.Datos.Paqueteria.MinimoValorSeguro
                                $scope.PaqueteriaPorcentajeSeguro = response.data.Datos.Paqueteria.PorcentajeSeguro
                            }
                        }
                    }, function (response) {
                    });

                blockUI.stop();
            } catch (e) {

            }
        }
        $scope.CargarTipoTarifa = function (CodigoTarifaCarga) {

            $scope.ListaTipoTarifaCargaVenta = [];

            //$scope.ListaTipoTarifaCargaVenta.push({ CodigoDetalleTarifa: 0, Codigo: 0, CodigoTarifa: 0, Nombre: '(Seleccione Item)' });

            $scope.ListaAuxTipoTarifas.forEach(function (item) {
                if (CodigoTarifaCarga == item.CodigoTarifa) {
                    $scope.ListaTipoTarifaCargaVenta.push(item);
                }
            });

            $scope.Modelo.Remesa.TipoTarifaCarga = $scope.ListaTipoTarifaCargaVenta[0];
            if ($scope.CodigoTipoTarifaTransporte !== undefined && $scope.CodigoTipoTarifaTransporte !== null) {
                if ($scope.CodigoTipoTarifaTransporte > 0) {
                    $scope.Modelo.Remesa.TipoTarifaCarga = $linq.Enumerable().From($scope.ListaTipoTarifaCargaVenta).First('$.Codigo ==' + $scope.CodigoTipoTarifaTransporte);
                }
            }


        }
        $scope.ListadoSitiosCargueAlterno2 = []
        $scope.AutocompleteSitiosClienteCargue = function (value, cliente, ciudad) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    }
                    $scope.ListadoSitiosCargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosCargueAlterno2)
                }
            }
            return $scope.ListadoSitiosCargueAlterno2
        }
        $scope.ListadoSitiosDescargueAlterno2 = []
        $scope.AutocompleteSitiosClienteDescargue = function (value, cliente, ciudad) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    }
                    $scope.ListadoSitiosDescargueAlterno2 = []
                    $scope.ListadoSitiosDescargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosDescargueAlterno2)
                }
            }
            return $scope.ListadoSitiosDescargueAlterno2
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var modelo = $scope.Modelo
            var dateEntMin = new Date();
            dateEntMin.setDate(dateEntMin.getDate() - 1);
            if (modelo.Remesa.FormaPago == undefined || modelo.Remesa.FormaPago == null || modelo.Remesa.FormaPago == '') {
                $scope.MensajesError.push('Debe ingresar la forma de pago');
                continuar = false;
            }
            else if (modelo.Remesa.FormaPago.Codigo == 6100) {
                $scope.MensajesError.push('Debe ingresar la forma de pago');
                continuar = false;
            }

            if ($scope.Modelo.Remesa.Remitente.Ciudad !== undefined && $scope.Modelo.Remesa.Remitente.Ciudad !== null && $scope.Modelo.Remesa.Destinatario.Ciudad !== undefined && $scope.Modelo.Remesa.Destinatario.Ciudad !== null) {
                if ($scope.Modelo.Remesa.Remitente.Ciudad.Codigo > 0 && $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo > 0) {
                    if ($scope.Modelo.Remesa.Remitente.Ciudad.Codigo !== $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo) {
                        $scope.TipoRemesa = CODIGO_TIPO_REMESA_NACIONAL;
                    } else {
                        $scope.TipoRemesa = CODIGO_TIPO_REMESA_URBANA;
                    }
                } else {
                    $scope.MensajesError.push('Debe seleccionar la ciudad para el remitente y el destinatario');
                    continuar = false;
                }

            } else {
                $scope.MensajesError.push('No hay ciudades seleccionadas para el remitente y/o destinatario');
                continuar = false;
            }

            if (ValidarCampo(modelo.Remesa.ProductoTransportado) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar el producto ');
                continuar = false;
            } else if (modelo.Remesa.ProductoTransportado.Codigo == undefined || modelo.Remesa.ProductoTransportado.Codigo == 0) {
                if (ValidarCampo(modelo.Remesa.DescripcionProductoTransportado) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar la descripción del producto');
                    continuar = false;
                }
            }

            if (ValidarCampo(modelo.PesoCobrar) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar el peso o las dimensiones');
                continuar = false;
            }
            if (ValidarCampo(modelo.Remesa.ValorComercialCliente) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar el valor comercial');
                continuar = false;
            }
            if (ValidarCampo(modelo.Remesa.CantidadCliente) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar la cantidad');
                continuar = false;
            }

            if (modelo.TipoEntregaRemesaPaqueteria.Codigo == 6600) {
                $scope.MensajesError.push('Debe seleccionar el tipo de entrega');
                continuar = false;
            }

            if (modelo.Remesa.Remitente.Codigo == 0 || modelo.Remesa.Remitente.Codigo == undefined) {
                if (ValidarCampo(modelo.Remesa.Remitente.NumeroIdentificacion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el número de identificación del remitente');
                    continuar = false;
                }
                if (modelo.Remesa.Remitente.TipoIdentificacion.Codigo == 100) {
                    $scope.MensajesError.push('Debe seleccionar el tipo de identificación del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Nombre) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el nombre del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Ciudad) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe seleccionar la ciudad del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Direccion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe seleccionar la dirección del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Telefonos) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el teléfono del remitente');
                    continuar = false;
                }

            }
            if (modelo.Remesa.Destinatario.Codigo == 0 || modelo.Remesa.Destinatario.Codigo == undefined) {
                if (ValidarCampo(modelo.Remesa.Destinatario.NumeroIdentificacion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el número de identificación del destinatario');
                    continuar = false;
                }
                if (modelo.Remesa.Destinatario.TipoIdentificacion.Codigo == 100) {
                    $scope.MensajesError.push('Debe seleccionar el tipo de identificación del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Nombre) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el nombre del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Ciudad) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe seleccionar la ciudad del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Direccion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe seleccionar la dirección del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Telefonos) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el teléfono del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.SitioDescargue) == OBJETO_SIN_DATOS && modelo.Remesa.Destinatario.Codigo > 0) {
                    $scope.MensajesError.push('Debe seleccionar el sitio de entrega');
                    continuar = false;
                }
            }
            if (modelo.Remesa.FechaEntrega == undefined || modelo.Remesa.FechaEntrega == '' || modelo.Remesa.FechaEntrega == null) {
                $scope.MensajesError.push('Debe ingresar la fecha de entrega');
                continuar = false;
            }
            else if (modelo.Remesa.FechaEntrega < dateEntMin) {
                $scope.MensajesError.push('la fecha entrega debe ser posterior a la fecha actual');
                continuar = false;
            }
            if (ValidarCampo(modelo.Remesa.ValorFleteCliente) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('No hay un flete asignado con las condiciones especificadas');
                continuar = false;
            }
            return continuar;
        }
        $('#Tarifa').show();
        $('#Producto').hide();
        $('#CondicionesComerciales').hide();
        $('#Remitente').hide();
        $('#Destinatario').hide();
        $('#Documentos').hide();

        $scope.MostrarProducto = function () {
            $('#Producto').show(100);
            $('#CondicionesComerciales').hide(100);
            $('#Remitente').hide(100);
            $('#Destinatario').hide(100);
            $('#Documentos').hide(100);
            $('#Tarifa').hide(100);
        }

        $scope.MostrarCondicionesComerciales = function () {
            $('#Producto').hide(100);
            $('#CondicionesComerciales').show(100);
            $('#Remitente').hide(100);
            $('#Destinatario').hide(100);
            $('#Documentos').hide(100);
            $('#Tarifa').hide(100);
        }

        $scope.MostrarRemitente = function () {
            $('#Producto').hide(100);
            $('#CondicionesComerciales').hide(100);
            $('#Remitente').show(100);
            $('#Destinatario').hide(100);
            $('#Documentos').hide(100);
            $('#Tarifa').hide(100);
        }

        $scope.MostrarDestinatario = function () {
            $('#Producto').hide(100);
            $('#CondicionesComerciales').hide(100);
            $('#Remitente').hide(100);
            $('#Destinatario').show(100);
            $('#Documentos').hide(100);
            $('#Tarifa').hide(100);
        }

        $scope.MostrarDocumentos = function () {
            $('#Producto').hide(100);
            $('#CondicionesComerciales').hide(100);
            $('#Remitente').hide(100);
            $('#Destinatario').hide(100);
            $('#Documentos').show(100);
            $('#Tarifa').hide(100);
        }
        $scope.MostrarTarifa = function () {
            $('#Producto').hide(100);
            $('#CondicionesComerciales').hide(100);
            $('#Remitente').hide(100);
            $('#Destinatario').hide(100);
            $('#Documentos').hide(100);
            $('#Tarifa').show(100);
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarRemesasPaqueteriaPortal/' + $scope.Modelo.Remesa.NumeroDocumento;
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            try {
                return parseInt(MascaraNumero(item))

            } catch (e) {
                return 0
            }
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        /* Obtener parametros*/
        try {
            var Parametros = $routeParams.Codigo.split(',')
            if (Parametros.length > 1) {
                $scope.Modelo.Numero = Parametros[0];
                $scope.aplicabase = true
                Obtener();
            } else {
                if ($routeParams.Codigo > 0) {
                    $scope.Modelo.Numero = $routeParams.Codigo;
                    $scope.CargarDatosFunciones();
                    Obtener();
                }
                else {
                    $scope.Modelo.Numero = 0;
                    $scope.CargarDatosFunciones();
                }

            }
        } catch (e) {
            if ($routeParams.Codigo > 0) {
                $scope.CargarDatosFunciones();
                $scope.Modelo.Numero = $routeParams.Codigo;
                Obtener();
            }
            else {
                $scope.Modelo.Numero = 0;
                $scope.CargarDatosFunciones()
            }

        }
        $scope.Modal = {}
        $scope.GuardarDireccionRemitente = function () {
            if ($scope.Modal.Ciudad == undefined || $scope.Modal.Ciudad == null || $scope.Modal.Ciudad == '' || $scope.Modal.Direccion == undefined || $scope.Modal.Direccion == null || $scope.Modal.Direccion == '') {
                ShowError('Debe ingresar la cuidad y la dirección')
            } else {
                $scope.Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Tercero: { Codigo: $scope.Modelo.Remesa.Remitente.Codigo },
                    Direccion: $scope.Modal.Direccion,
                    Telefonos: $scope.Modal.Telefonos,
                    Ciudad: $scope.Modal.Ciudad,
                    Barrio: $scope.Modal.Barrio,
                    CodigoPostal: $scope.Modal.CodigoPostal
                }
                TercerosFactory.InsertarDirecciones($scope.Entidad).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDireccionesRemitente.push($scope.Entidad)
                        $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[($scope.ListadoDireccionesRemitente.length - 1)]
                        if ($scope.Modelo.Remesa.Destinatario.Codigo == $scope.Modelo.Remesa.Remitente.Codigo) {
                            $scope.ListadoDireccionesDestinatario.push($scope.Entidad)
                            $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[($scope.ListadoDireccionesDestinatario.length - 1)]
                        }
                        closeModal('modalAdicionarDireccionRemitente')
                    } else {
                        ShowError('No se pudo guardar la dirección')
                    }
                });

            }
        }
        $scope.GuardarDireccionDestinatario = function () {
            if ($scope.Modal.Ciudad == undefined || $scope.Modal.Ciudad == null || $scope.Modal.Ciudad == '' || $scope.Modal.Direccion == undefined || $scope.Modal.Direccion == null || $scope.Modal.Direccion == '') {
                ShowError('Debe ingresar la cuidad y la dirección')
            } else {
                $scope.Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Tercero: { Codigo: $scope.Modelo.Remesa.Destinatario.Codigo },
                    Direccion: $scope.Modal.Direccion,
                    Telefonos: $scope.Modal.Telefonos,
                    Ciudad: $scope.Modal.Ciudad,
                    Barrio: $scope.Modal.Barrio,
                    CodigoPostal: $scope.Modal.CodigoPostal
                }
                TercerosFactory.InsertarDirecciones($scope.Entidad).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDireccionesDestinatario.push($scope.Entidad)
                        $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[($scope.ListadoDireccionesDestinatario.length - 1)]
                        if ($scope.Modelo.Remesa.Destinatario.Codigo == $scope.Modelo.Remesa.Remitente.Codigo) {
                            $scope.ListadoDireccionesRemitente.push($scope.Entidad)
                            $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[($scope.ListadoDireccionesRemitente.length - 1)]
                        }
                        closeModal('modalAdicionarDireccionDestinatario')
                    } else {
                        ShowError('No se pudo guardar la dirección')
                    }
                });

            }
        }
        $scope.AdicionarOficinas = function () {
            if ($scope.Modelo.Remesa.Oficina == undefined || $scope.Modelo.Remesa.Oficina == null || $scope.Modelo.Remesa.Oficina == '' ||
                $scope.Modelo.Remesa.TipoReexpedicion == undefined || $scope.Modelo.Remesa.TipoReexpedicion == null || $scope.Modelo.Remesa.TipoReexpedicion == ''

            ) {
                ShowError('Debe ingresar la oficina, el tipo de reexpedición')
            } else {
                if ($scope.Modelo.Remesa.TipoReexpedicion.Codigo == 11102 && ($scope.Modelo.Remesa.ProveedorExterno == undefined || $scope.Modelo.Remesa.ProveedorExterno == null || $scope.Modelo.Remesa.ProveedorExterno == '')) {
                    ShowError('Debe ingresar el proveedor externo')
                }
                else {
                    if ($scope.Modelo.Remesa.Valor == undefined || $scope.Modelo.Remesa.Valor == null || $scope.Modelo.Remesa.Valor == '' || $scope.Modelo.Remesa.Valor == 0 || $scope.Modelo.Remesa.Valor == '0') {
                        $scope.Modelo.Remesa.Valor = 0
                    }
                    try {
                        if ($scope.ListaReexpedicionOficinas.length > 0) {
                            var cont = 0
                            for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                                if ($scope.ListaReexpedicionOficinas[i].Oficina.Codigo == $scope.Modelo.Remesa.Oficina.Codigo) {
                                    cont++
                                    break;
                                }
                            }
                            if (cont > 0) {
                                ShowError('La oficina que intenta ingresar ya se encuentra registrada')
                                $scope.Modelo.Remesa.Oficina = ''
                                $scope.Modelo.Remesa.Valor = ''
                            } else {
                                $scope.ListaReexpedicionOficinas.push({ Oficina: $scope.Modelo.Remesa.Oficina, TipoReexpedicion: $scope.Modelo.Remesa.TipoReexpedicion, Valor: $scope.Modelo.Remesa.Valor, ProveedorExterno: $scope.Modelo.Remesa.ProveedorExterno, Observaciones: $scope.Modelo.Remesa.ObservacionesReexpedicion })
                                $scope.Modelo.Remesa.Oficina = ''
                                $scope.Modelo.Remesa.Valor = ''
                                $scope.Modelo.Remesa.ObservacionesReexpedicion = ''
                                $scope.Modelo.Remesa.ProveedorExterno = ''

                            }
                        } else {
                            $scope.ListaReexpedicionOficinas = []
                            $scope.ListaReexpedicionOficinas.push({ Oficina: $scope.Modelo.Remesa.Oficina, TipoReexpedicion: $scope.Modelo.Remesa.TipoReexpedicion, Valor: $scope.Modelo.Remesa.Valor, ProveedorExterno: $scope.Modelo.Remesa.ProveedorExterno, Observaciones: $scope.Modelo.Remesa.ObservacionesReexpedicion })
                            $scope.Modelo.Remesa.Oficina = ''
                            $scope.Modelo.Remesa.Valor = ''
                            $scope.Modelo.Remesa.ObservacionesReexpedicion = ''
                            $scope.Modelo.Remesa.ProveedorExterno = ''
                        }
                    } catch (e) {
                        $scope.ListaReexpedicionOficinas = []
                        $scope.ListaReexpedicionOficinas.push({ Oficina: $scope.Modelo.Remesa.Oficina, TipoReexpedicion: $scope.Modelo.Remesa.TipoReexpedicion, Valor: $scope.Modelo.Remesa.Valor, ProveedorExterno: $scope.Modelo.Remesa.ProveedorExterno, Observaciones: $scope.Modelo.Remesa.ObservacionesReexpedicion })
                        $scope.Modelo.Remesa.Oficina = ''
                        $scope.Modelo.Remesa.Valor = ''
                        $scope.Modelo.Remesa.ObservacionesReexpedicion = ''
                        $scope.Modelo.Remesa.ProveedorExterno = ''
                    }
                }
            }
            $scope.Calcular()
        }
        $scope.EliminarOficinaReexpedicion = function (i) {
            $scope.ListaReexpedicionOficinas.splice(i, 1);
            $scope.Calcular()
        }
        $scope.CalcularAjusteFlete = function () {
            var valMin = RevertirMV($scope.Modelo.FletePactado) - (RevertirMV($scope.Modelo.FletePactado) * 0.4)
            //var valMax = RevertirMV($scope.Modelo.FletePactado) + (RevertirMV($scope.Modelo.FletePactado) * 0.4)
            if (/*RevertirMV($scope.Modelo.Remesa.ValorFleteCliente) > valMax ||*/ RevertirMV($scope.Modelo.Remesa.ValorFleteCliente) < valMin) {
                ShowError("El valor del flete ajustado no puede ser inferior 40% al flete acordado")
                $scope.Modelo.Remesa.ValorFleteCliente = MascaraValores($scope.Modelo.FletePactado)
            }
            $scope.Modelo.Remesa.TotalFleteCliente = RevertirMV($scope.Modelo.Remesa.ValorFleteCliente)
            if (RevertirMV($scope.Modelo.Remesa.ValorManejoCliente) > 0) {
                $scope.Modelo.Remesa.TotalFleteCliente += RevertirMV($scope.Modelo.Remesa.ValorManejoCliente)
            }
            if (RevertirMV($scope.Modelo.Remesa.ValorSeguroCliente) > 0) {
                $scope.Modelo.Remesa.TotalFleteCliente += RevertirMV($scope.Modelo.Remesa.ValorSeguroCliente)
            }
            $scope.ValorReexpedicion = 0
            try {
                for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                    //$scope.Modelo.Remesa.TotalFleteCliente += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor))
                    $scope.ValorReexpedicion += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor))
                }
                $scope.ValorReexpedicion = MascaraValores($scope.ValorReexpedicion)
                $scope.Modelo.ValorReexpedicion = RevertirMV($scope.ValorReexpedicion)
            } catch (e) {
            }
            $scope.MaskValores()
        }
    }]);