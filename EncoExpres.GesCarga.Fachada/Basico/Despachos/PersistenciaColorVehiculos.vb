﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaColorVehiculos
        Implements IPersistenciaBase(Of ColorVehiculos)

        Public Function Consultar(filtro As ColorVehiculos) As IEnumerable(Of ColorVehiculos) Implements IPersistenciaBase(Of ColorVehiculos).Consultar
            Return New RepositorioColorVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ColorVehiculos) As Long Implements IPersistenciaBase(Of ColorVehiculos).Insertar
            Return New RepositorioColorVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ColorVehiculos) As Long Implements IPersistenciaBase(Of ColorVehiculos).Modificar
            Return New RepositorioColorVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ColorVehiculos) As ColorVehiculos Implements IPersistenciaBase(Of ColorVehiculos).Obtener
            Return New RepositorioColorVehiculos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ColorVehiculos) As Boolean
            Return New RepositorioColorVehiculos().Anular(entidad)
        End Function
    End Class

End Namespace

