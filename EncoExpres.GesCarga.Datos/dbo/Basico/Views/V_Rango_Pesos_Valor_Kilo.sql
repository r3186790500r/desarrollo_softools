﻿Print 'V_Rango_Pesos_Valor_Kilo'
GO
DROP VIEW V_Rango_Pesos_Valor_Kilo
GO
CREATE VIEW V_Rango_Pesos_Valor_Kilo 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 49
GO