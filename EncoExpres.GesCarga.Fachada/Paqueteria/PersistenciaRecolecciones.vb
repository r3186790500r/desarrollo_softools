﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Basico.General


Namespace Despachos.Paqueteria
    Public NotInheritable Class PersistenciaRecolecciones
        Implements IPersistenciaBase(Of Recolecciones)

        Public Function Consultar(filtro As Recolecciones) As IEnumerable(Of Recolecciones) Implements IPersistenciaBase(Of Recolecciones).Consultar
            Return New RepositorioRecolecciones().Consultar(filtro)
        End Function

        Public Function ConsultarENCOE(filtro As Recolecciones) As IEnumerable(Of Recolecciones)
            Return New RepositorioRecolecciones().ConsultarENCOE(filtro)
        End Function

        Public Function ConsultarRecoleccionesPorPlanillar(filtro As Recolecciones) As IEnumerable(Of Recolecciones)
            Return New RepositorioRecolecciones().ConsultarRecoleccionesPorPlanillar(filtro)
        End Function

        Public Function Insertar(entidad As Recolecciones) As Long Implements IPersistenciaBase(Of Recolecciones).Insertar
            Return New RepositorioRecolecciones().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Recolecciones) As Long Implements IPersistenciaBase(Of Recolecciones).Modificar
            Return New RepositorioRecolecciones().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Recolecciones) As Recolecciones Implements IPersistenciaBase(Of Recolecciones).Obtener
            Return New RepositorioRecolecciones().Obtener(filtro)
        End Function

        Public Function Anular(entidad As Recolecciones) As Boolean
            Return New RepositorioRecolecciones().Anular(entidad)
        End Function

        Public Function Cancelar(entidad As Recolecciones) As Boolean
            Return New RepositorioRecolecciones().Cancelar(entidad)
        End Function
        Public Function AsociarRecoleccionesPlanilla(entidad As Recolecciones) As Boolean
            Return New RepositorioRecolecciones().AsociarRecoleccionesPlanilla(entidad)
        End Function


        Public Function GuardarEntrega(entidad As Recolecciones) As Long
            Return New RepositorioRecolecciones().GuardarEntrega(entidad)
        End Function

    End Class

End Namespace

