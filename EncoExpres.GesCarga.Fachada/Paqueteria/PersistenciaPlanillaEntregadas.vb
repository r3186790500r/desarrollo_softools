﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Despachos.Paqueteria

Namespace Despachos.Paqueteria
    Public NotInheritable Class PersistenciaPlanillaEntregadas
        Implements IPersistenciaBase(Of PlanillaEntregadas)

        Public Function Consultar(filtro As PlanillaEntregadas) As IEnumerable(Of PlanillaEntregadas) Implements IPersistenciaBase(Of PlanillaEntregadas).Consultar
            Return New RepositorioPlanillaEntregadas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PlanillaEntregadas) As Long Implements IPersistenciaBase(Of PlanillaEntregadas).Insertar
            Return New RepositorioPlanillaEntregadas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PlanillaEntregadas) As Long Implements IPersistenciaBase(Of PlanillaEntregadas).Modificar
            Return New RepositorioPlanillaEntregadas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PlanillaEntregadas) As PlanillaEntregadas Implements IPersistenciaBase(Of PlanillaEntregadas).Obtener
            Return New RepositorioPlanillaEntregadas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As PlanillaEntregadas) As Boolean
            Return New RepositorioPlanillaEntregadas().Anular(entidad)
        End Function
        Public Function AnularAuxiliar(entidad As PlanillaEntregadas) As Boolean
            Return New RepositorioPlanillaEntregadas().AnularAuxiliar(entidad)
        End Function
    End Class

End Namespace

