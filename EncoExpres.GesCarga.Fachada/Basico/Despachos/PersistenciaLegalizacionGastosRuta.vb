﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos

    Public NotInheritable Class PersistenciaLegalizacionGastosRuta
        Implements IPersistenciaBase(Of LegalizacionGastosRuta)

        Public Function Consultar(filtro As LegalizacionGastosRuta) As IEnumerable(Of LegalizacionGastosRuta) Implements IPersistenciaBase(Of LegalizacionGastosRuta).Consultar
            Return New RepositorioLegalizacionGastosRuta().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As LegalizacionGastosRuta) As Long Implements IPersistenciaBase(Of LegalizacionGastosRuta).Insertar
            Return New RepositorioLegalizacionGastosRuta().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As LegalizacionGastosRuta) As Long Implements IPersistenciaBase(Of LegalizacionGastosRuta).Modificar
            Return New RepositorioLegalizacionGastosRuta().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As LegalizacionGastosRuta) As LegalizacionGastosRuta Implements IPersistenciaBase(Of LegalizacionGastosRuta).Obtener
            Return New RepositorioLegalizacionGastosRuta().Obtener(filtro)
        End Function
        Public Function Anular(entidad As LegalizacionGastosRuta) As Boolean
            Return New RepositorioLegalizacionGastosRuta().Anular(entidad)
        End Function

    End Class
End Namespace