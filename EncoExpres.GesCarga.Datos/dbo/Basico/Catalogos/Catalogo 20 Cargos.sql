﻿Print 'Catalogo 20 Cargos'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 20
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 20
GO
DELETE Catalogos WHERE Codigo = 20
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,20,'CARG','Cargos',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,20,2000,UPPER('(No Aplica)'),1,GETDATE()),
(1,20,2001,UPPER('Conductor'),1,GETDATE()),
(1,20,2002,UPPER('Asistente Administrativo'),1,GETDATE())
GO


-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,20,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,20,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 