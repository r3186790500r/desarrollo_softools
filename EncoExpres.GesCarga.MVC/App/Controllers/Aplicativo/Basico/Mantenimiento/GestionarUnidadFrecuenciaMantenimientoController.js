﻿EncoExpresApp.controller("GestionarUnidadFrecuenciaMantenimientoCtrl", ['$scope', '$routeParams', 'blockUI', '$timeout', 'ValorCatalogosFactory', '$linq', 'UnidadReferenciaMantenimientoFactory',
    function ($scope, $routeParams, blockUI, $timeout, ValorCatalogosFactory, $linq, UnidadReferenciaMantenimientoFactory) {
        $scope.MensajesError = [];
        $scope.MensajesErrorDetalle = [];
        $scope.ListadoEstados = [];
        $scope.ModificarDetalle = 0;
        $scope.Operacion = 'Adicionar';


        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_UNIDAD_FRECUENCIA);
        $scope.ListadoDocumentos = [{
            idRow: 1,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Referencia: '',
            Documento: '',
            Extencion: ''
        }]
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Mantenimiento' }, { Nombre: 'Unidad Frecuencias' }];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            
        };
        $scope.Modelo.ID_Documentos_Eliminar = [];

        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo; 
        }
        else {
            $scope.Modelo.Codigo = 0;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*Cargar el combo de estados*/

        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0];


        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR UNIDAD FRECUENCIAS';
            Obtener();
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando unidad frecuencias ' + $scope.Modelo.Codigo);
            
            $timeout(function () {
                blockUI.message('Cargando unidad  frecuencias ' + $scope.Modelo.Codigo);
            }, 200); 
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            }; 
            blockUI.delay = 1000;
            UnidadReferenciaMantenimientoFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Nombre = response.data.Datos.Nombre 
                        $scope.Modelo.NombreCorto = response.data.Datos.NombreCorto
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado.Codigo);
                    }
                    else {
                        ShowError('No se logro consultar la unidad frecuencias No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarUnidadFrecuenciaMantenimiento';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar el plan de mantenimiento No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarUnidadFrecuenciaMantenimiento';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if (parseInt($scope.Modelo.Codigo) > 0) {
                document.location.href = '#!ConsultarUnidadFrecuenciaMantenimiento/' + $scope.Modelo.Codigo;
            }
            else {
                document.location.href = '#!ConsultarUnidadFrecuenciaMantenimiento';
            }
        };

        // Metodo para guardar /modificar
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.Guardar = function () { 
            if (DatosRequeridos()) {
                UnidadReferenciaMantenimientoFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    blockUI.stop();
                                    ShowSuccess('Se guardó la unidad de frecuencia ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarUnidadFrecuenciaMantenimiento/' + response.data.Datos;
                                }
                                else {
                                    blockUI.stop();
                                    ShowSuccess('Se modificó la unidad de frecuencia ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarUnidadFrecuenciaMantenimiento/' + response.data.Datos;
                                }
                                closeModal('modalConfirmacionGuardar');
                                closeModal('modalInsertarMarcaEquipoMantenimiento');
                                //$scope.VolverMaster();
                            }
                            else {
                                blockUI.stop();
                                ShowError(response.data.MensajeOperacion);
                                $scope.VolverMaster();
                            }
                        }
                        else {
                            blockUI.stop();
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                blockUI.stop();

            }
            else {
                closeModal('modalConfirmacionGuardar', 1)
            }
        }
        // Valida los datos requridos 
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == '' || $scope.Modelo.Nombre == undefined) {
                $scope.MensajesError.push('Debe ingresar el nombre de la unidad de frecuencia de mantenimiento');
                continuar = false;
            }
            if ($scope.Modelo.NombreCorto == '' || $scope.Modelo.Nombre == undefined) {
                $scope.MensajesError.push('Debe ingresar el nombre corto de la unidad de frecuencia de mantenimiento');
                continuar = false;
            }
            if ($scope.Modelo.Estado == '' || $scope.Modelo.Estado == undefined) {
                $scope.MensajesError.push('Debe ingresar el estado de la unidad de frecuencia de mantenimiento');
                continuar = false;
            }
            else if ($scope.Modelo.Estado.Codigo == 1292) {
                $scope.MensajesError.push('Debe ingresar el estado de la unidad de frecuencia de mantenimiento');
                continuar = false;
            }

            return continuar

        }

        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        };
    }]);