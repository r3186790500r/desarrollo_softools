﻿PRINT 'gsp_anular_encabezado_cumplido_planilla_Despacho'
GO

DROP PROCEDURE gsp_anular_encabezado_cumplido_planilla_Despacho
GO

CREATE PROCEDURE gsp_anular_encabezado_cumplido_planilla_Despacho
(
	@par_EMPR_Codigo SMALLINT,
	@par_Numero NUMERIC,
	@par_USUA_Codigo_Anula INT,
	@par_Causa_Anula VARCHAR(50)
)
AS
BEGIN
	UPDATE
		Encabezado_Cumplido_Planilla_Despachos
	SET
		Anulado = 1,
		USUA_Codigo_Anula = @par_USUA_Codigo_Anula,
		Fecha_Anula = GETDATE(),
		Causa_Anula = @par_USUA_Codigo_Anula
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Numero = @par_Numero

	UPDATE
		Encabezado_Planilla_Despachos
	SET
		ECPD_Numero = NULL
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND ECPD_Numero = @par_Numero

	UPDATE
		Encabezado_Manifiesto_Carga
	SET
		ECPD_Numero = 0
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND ECPD_Numero = @par_Numero

	UPDATE
		Encabezado_Remesas
	SET
		ECPD_Numero = 0,
		Cumplido = 0
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND ECPD_Numero = @par_Numero

	SELECT @par_Numero AS Numero
END
GO