﻿EncoExpresApp.controller("GestionarPlanillaCargueDescargueCtrl", ['$scope', '$linq', '$timeout', 'blockUI', 'blockUIConfig', 'ValidacionPlanillaDespachosFactory', '$routeParams', 'PlanillaDespachosFactory', 'PlanillaGuiasFactory', 'RemesaGuiasFactory', 'PlanillaPaqueteriaFactory','VehiculosFactory','OficinasFactory',
    function ($scope, $linq, $timeout, blockUI, blockUIConfig, ValidacionPlanillaDespachosFactory, $routeParams, PlanillaDespachosFactory, PlanillaGuiasFactory, RemesaGuiasFactory, PlanillaPaqueteriaFactory,VehiculosFactory,OficinasFactory) {
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Planilla Cargue/Descargue' }, { Nombre: 'Gestionar' }];

        //Declaración Variables:

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.VALIDAR_PROCESO_CARGUE)
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta página');
            document.location.href = '#';
        }

        $scope.PlacaVehiculo = '';
        $scope.NombreConductor = '';
        $scope.NombreTenedor = '';
        $scope.NombreRuta = '';
        $scope.PlacaSemirremolque = '';
        $scope.MostrarSeccionGuias = false;
        $scope.Planilla = [];
        $scope.ListadoGuias = [];
        $scope.MensajesError = [];
        $scope.MensajesConfirmacion = [];
        $scope.NumeroPlanilla = '';
        $scope.NumeroDocumentoPlanilla = '';
        $scope.Observaciones = '';
        $scope.Cantidad = '';
        $scope.Peso = 0;
        $scope.CodigoPlanilla = '';
        $scope.Deshabilitar = false;
        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: 'BORRADOR' },
            {Codigo: 1, Nombre: 'DEFINITIVO'}
        ];

        $scope.ListadoTipoDocumentoPlanillas = [
            { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_CARGUE, Nombre: 'CARGUE' },
            { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESCARGUE, Nombre: 'DESCARGUE' }
        ];

        $scope.TipoPlanilla = $linq.Enumerable().From($scope.ListadoTipoDocumentoPlanillas).First('$.Codigo==' + CODIGO_TIPO_DOCUMENTO_PLANILLA_CARGUE);

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==0');
        $scope.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas;
        //Métodos:

        $scope.AutocompleteVehiculos = function (value) {
            $scope.ListaVehiculos = [];
            var Lista = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true, Estado: ESTADO_ACTIVO }).Datos;
            return Lista;
        }

        $scope.ObtenerVehiculo = function () {
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            $scope.MostrarSeccionGuias = false;
            if ($scope.PlacaVehiculo != undefined && $scope.PlacaVehiculo != null && $scope.PlacaVehiculo != '') {                

                var ResponseVehiculo = '';

                ResponseVehiculo = $scope.CargarVehiculosPlaca($scope.PlacaVehiculo.Placa); 
                if (ResponseVehiculo != undefined && ResponseVehiculo != '') {
                 
                    $scope.NombreConductor = $scope.CargarTercero(ResponseVehiculo.Conductor.Codigo).NombreCompleto;
                    $scope.NombreTenedor =  $scope.CargarTercero(ResponseVehiculo.Tenedor.Codigo).NombreCompleto;
                   
                    $scope.MostrarSeccionGuias = true;
                } else {

                    $scope.MensajesError.push('El vehículo ingresado no es válido');
                    $scope.PlacaVehiculo = '';
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }
               
            }
        }

        $scope.ValidarGuia = function () {
            var cont = 0;
            $scope.MensajesError = [];
            if ($scope.NumeroGuia != undefined && $scope.NumeroGuia != null && $scope.NumeroGuia != '' && !isNaN($scope.NumeroGuia)) {
                var Guia = ''
                Guia = RemesaGuiasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroInicial: $scope.NumeroGuia, Estado: ESTADO_ACTIVO, Sync: true, NumeroPlanilla: -1 }).Datos[0];

                if (Guia != '' && Guia != undefined) {

                    var ResponseGuia = ''
                    ResponseGuia = RemesaGuiasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: Guia.Remesa.Numero, Sync: true }).Datos;

                    if (ResponseGuia != '' && ResponseGuia != undefined) {

                        var ProcesoGuias = false;

                        if (ResponseGuia.OficinaActual.Codigo == $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo) {
                            ProcesoGuias = true;
                        } else if ($scope.TipoPlanilla.Codigo == CODIGO_TIPO_DOCUMENTO_PLANILLA_CARGUE) {
                            ProcesoGuias = false;

                            $scope.MensajesError.push('La guía ingresada no se encuentra en esta oficina')
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);

                        } else {
                            ProcesoGuias = true;
                        }
                        if (ProcesoGuias) {
                            var NumeroUnidad = 1;
                            if (ResponseGuia.ManejaDetalleUnidades > 0) {

                                if ($scope.ListadoGuias.length > 0) {
                                    var countListadoGuias = 0;
                                    $scope.ListadoGuias.forEach(itemGuia => {
                                        if (itemGuia.NumeroDocumentoRemesa == $scope.NumeroGuia) {
                                            countListadoGuias++;
                                        }
                                    });
                                    if (countListadoGuias == 0) {
                                        ResponseGuia.DetalleUnidades.forEach(item => {
                                            $scope.ListadoGuias.push({
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                NumeroRemesa: Guia.Remesa.Numero,
                                                NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                                NumeroUnidad: NumeroUnidad,
                                                Peso: item.Peso,
                                                ObservacionesVerificacion: ''
                                            });
                                            NumeroUnidad++;
                                        });
                                        $scope.CalcularTotales();
                                    } else {

                                        $scope.MensajesError.push('La guía ingresada ya se encuentra verificada')
                                        $("#window").animate({
                                            scrollTop: $('.breadcrumb').offset().top
                                        }, 200);
                                    }
                                } else {
                                    ResponseGuia.DetalleUnidades.forEach(item => {
                                        $scope.ListadoGuias.push({
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            NumeroRemesa: Guia.Remesa.Numero,
                                            NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                            NumeroUnidad: NumeroUnidad,
                                            Peso: item.Peso,
                                            ObservacionesVerificacion: ''
                                        });

                                        NumeroUnidad++;
                                    });
                                    $scope.CalcularTotales();
                                }
                            } else {

                                $scope.ListadoGuias.push({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    NumeroRemesa: Guia.Remesa.Numero,
                                    NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                    NumeroUnidad: NumeroUnidad,
                                    Peso: ResponseGuia.Remesa.PesoCliente,
                                    ObservacionesVerificacion: ''
                                });

                                $scope.CalcularTotales();
                            }
                        }
                    
                    }
                } else {
                    $scope.MensajesError.push('La guía ingresada no es válida')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }
            }else {
                $scope.MensajesError.push('La guía ingresada no es válida')
                $("#window").animate({
                    scrollTop: $('.breadcrumb').offset().top
                }, 200);
            }

               

               
            
        }

        $scope.CalcularTotales = function () {
            $scope.Cantidad = '';
            $scope.Peso = 0
            $scope.Cantidad = $scope.ListadoGuias.length;
            $scope.ListadoGuias.forEach(item => {
                $scope.Peso += item.Peso
            });
        }

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardarCargue');
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarCargue');

            if ($scope.DatosRequeridos()) {
                var Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.CodigoPlanilla,
                    Vehiculo: { Codigo: $scope.PlacaVehiculo.Codigo },                    
                    TipoVerificacion: { Codigo: $scope.TipoPlanilla.Codigo },
                    Observaciones: $scope.Observaciones,
                    Cantidad: $scope.Cantidad,
                    Peso: $scope.Peso,
                    Estado: $scope.Estado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    ListadoGuias: $scope.ListadoGuias,
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo }
                }
                $scope.MensajesConfirmacion = [];
                $scope.MensajesError = []
                ValidacionPlanillaDespachosFactory.Guardar(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            $scope.MensajesConfirmacion.push('Se han verificado ' + $scope.ListadoGuias.length + ' guías de la planilla ' + $scope.NumeroPlanilla);
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                            $scope.MostrarSeccionGuias = false;
                            $scope.ListadoGuias = [];
                            $scope.NumeroDocumentoPlanilla = '';
                            
                            $scope.NumeroGuia = '';
                            $scope.PlacaVehiculo = '';
                            $scope.NombreConductor = '';
                            $scope.NombreTenedor = '';
                            $scope.NombreRuta = '';
                            $scope.PlacaSemirremolque = '';
                            $scope.FechaSalida = '';
                            $scope.HoraSalida = '';

                            if ($scope.NumeroPlanilla > 0) {
                                ShowSuccess('Se modificó la planilla con el número ' + response.data.Datos);
                                document.location.href = '#!ConsultarPlanillaCargueDescargue/' + response.data.Datos
                                $scope.NumeroPlanilla = '';
                            } else {
                                ShowSuccess('Se guardó la planilla con el número ' + response.data.Datos);
                                document.location.href = '#!ConsultarPlanillaCargueDescargue/' + response.data.Datos
                            }

                        } else {
                            $scope.MensajesError.push(response.data.MensajeOperacion);
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                        }
                    });
            }
        }

        $scope.DatosRequeridos = function () {
            var continuar = true;
            if ($scope.ListadoGuias != undefined && $scope.ListadoGuias != null && $scope.ListadoGuias != '') {
                if ($scope.ListadoGuias.length == 0) {
                    $scope.MensajesError.push('debe existir por lo menos una guía verificada')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                    continuar = false;
                }
            } else {
                $scope.MensajesError.push('debe existir por lo menos una guía verificada')
                $("#window").animate({
                    scrollTop: $('.breadcrumb').offset().top
                }, 200);
            }
            return continuar;
        }

        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.CodigoPlanilla
            };
            ValidacionPlanillaDespachosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.NumeroPlanilla = response.data.Datos.NumeroDocumento;
                        $scope.TipoPlanilla = $linq.Enumerable().From($scope.ListadoTipoDocumentoPlanillas).First('$.Codigo==' + response.data.Datos.TipoDocumento);
                        $scope.Fecha = new Date(response.data.Datos.Fecha);
                        $scope.PlacaVehiculo = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo);
                        $scope.ObtenerVehiculo();
                        $scope.Observaciones = response.data.Datos.Observaciones;

                        response.data.Datos.ListadoGuias.forEach(item => {
                            $scope.ListadoGuias.push({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroRemesa: item.NumeroRemesa,
                                NumeroDocumentoRemesa: item.NumeroDocumentoRemesa,
                                NumeroUnidad: item.NumeroUnidad,
                                Peso: item.PesoRemesa,
                                ObservacionesVerificacion: item.Observaciones
                            });
                        });

                        $scope.CalcularTotales();

                        $scope.Oficina = OficinasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.Oficina.Codigo, Sync: true }).Datos;
                        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + response.data.Datos.Estado);

                        if ($scope.Estado.Codigo == 1) {
                            $scope.Deshabilitar = true
                        }
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    blockUI.stop();
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } document.location.href = '#!ConsultarPlanillaCargueDescargue'; }, 500);
                });
        }

        $scope.VolverMaster = function () {
            if ($scope.NumeroPlanilla > 0) {
                document.location.href = '#!ConsultarPlanillaCargueDescargue/' + $scope.NumeroPlanilla;
            } else {
                document.location.href = '#!ConsultarPlanillaCargueDescargue';
            }
        }

        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        }

        if ($routeParams.Numero > 0) {
            $scope.CodigoPlanilla = $routeParams.Numero
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Obteniendo Documento...");
            $timeout(function () { blockUI.message("Obteniendo Documento..."); Obtener(); }, 100);
        }


    }
]);