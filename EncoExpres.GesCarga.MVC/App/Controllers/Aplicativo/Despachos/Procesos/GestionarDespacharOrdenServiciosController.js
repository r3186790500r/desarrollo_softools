﻿EncoExpresApp.controller("GestionarDespacharOrdenServiciosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'EncabezadoSolicitudOrdenServiciosFactory', 'OficinasFactory',
    'VehiculosFactory', 'SemirremolquesFactory', 'TercerosFactory', 'CiudadesFactory', 'TarifarioComprasFactory', 'TarifarioVentasFactory', 'TipoTarifaTransporteCargaFactory', 'OrdenCargueFactory', 'RemesasFactory',
    'PlanillaDespachosFactory', 'ManifiestoFactory', 'RutasFactory', 'ImpuestosFactory', 'blockUIConfig', 'ProductoTransportadosFactory', 'ReporteMinisterioFactory', 'TipoTarifaTransportesFactory',
    'EnturnamientoFactory', 'EmpresasFactory', 'SitiosCargueDescargueFactory', 'SitiosTerceroClienteFactory', 'TipoDocumentosFactory', 'NovedadesDespachosFactory', 'DetalleNovedadesDespachosFactory', 'PeajesFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ValorCatalogosFactory, EncabezadoSolicitudOrdenServiciosFactory, OficinasFactory,
        VehiculosFactory, SemirremolquesFactory, TercerosFactory, CiudadesFactory, TarifarioComprasFactory, TarifarioVentasFactory, TipoTarifaTransporteCargaFactory, OrdenCargueFactory, RemesasFactory,
        PlanillaDespachosFactory, ManifiestoFactory, RutasFactory, ImpuestosFactory, blockUIConfig, ProductoTransportadosFactory, ReporteMinisterioFactory, TipoTarifaTransportesFactory,
        EnturnamientoFactory, EmpresasFactory, SitiosCargueDescargueFactory, SitiosTerceroClienteFactory, TipoDocumentosFactory, NovedadesDespachosFactory, DetalleNovedadesDespachosFactory, PeajesFactory) {
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Procesos' }, { Nombre: 'Despachar Orden Servicio' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.CodigoEstadoSolicitud = 0;
        $scope.ListadoDocumentosVencidos = [];
        try {
            if ($scope.Sesion.UsuarioAutenticado.ManejoDespachoProgramacion) {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_DESPCHAR_PROGRAMACION_ORDEN_SERVICIOS);
            }
            else {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_DESPCHAR_ORDEN_SERVICIOS);
            }
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.ListadoCiudadesDescargue = [];
        $scope.Modal = {};
        $scope.NoAsignarSemirremolque = false;
        $scope.ModalValorFOB = '';
        $scope.Modal.DevolucionContenedor = false;
        $scope.Modal.CiudadDevolucion = '';
        $scope.Modal.PatioDevolucion = '';
        $scope.Modal.FechaDevolucion = '';
        $scope.Modal.SICDDevolucionContenedor = '';
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ComboTipoManifiesto = false;
        $scope.CantidadViajesDia = '';
        $scope.Numero = 0;
        $scope.ListaOficinasDespacha = [];
        $scope.ListadoPermisosEspecificos = []
        $scope.NoVisualizarDocumento = false
        $scope.DeshabilitarValorEscolta = true;
        $scope.ListaPlaca = [];
        $scope.ListaPlacaSemi = [];
        $scope.ListaTenedores = [];
        $scope.ListaConductores = [];
        $scope.DeshabilitarRemesa = false;
        $scope.ModeloFecha = new Date();
        $scope.ListaAuxiliares = [];
        $scope.CodigoConductor = 0;
        $scope.ModalTotalFleteCliente = 0
        $scope.CodigoSemirremolque = 0;
        $scope.DeshabilitarRemesa = false;
        $scope.DeshabilitarAceptacion = false;
        $scope.DesabilitarConfirmacionRemesa = false;
        $scope.DeshabilitarDespacho = false;
        $scope.DeshabilitarPlanilla = false;
        $scope.DeshabilitarOrdenCargue = false;
        $scope.DeshabilitarTarifaTransportador = false;
        if ($scope.Sesion.UsuarioAutenticado.ManejoHabilitarModificarFleteCliente) {
            $scope.DeshabilitarTarifaCliente = false;
        } else {
            $scope.DeshabilitarTarifaCliente = true;
        }
        $scope.ModaloDocumentoCliente = '';
        $scope.DeshabilitarVehiculo = false
        $scope.ValidaPeso = true
        $scope.ModalSeguroAnticipo = 0
        $scope.AutorizacionFlete = 0
        $scope.ListaTrayectos = [];

        var ValidarAnticipoPlanillaDesdeFletesCompra = true
        var ValidacionUltimaInspeccionOrdenCargueRemesa = false
        var ValidarDesdeVehiculo = false
        var TipoDuenoVehiculo = 0;
        $scope.TipoDuenoVehiculo = 0;
        var fechaBloqueo1 = new Date(new Date().setSeconds(86400));
        var OrdenServicio = {};
        fechaBloqueo1.setHours(0)
        fechaBloqueo1.setMinutes(0)
        fechaBloqueo1.setMinutes(0)

        var fechaBloqueo2 = new Date(new Date().setSeconds(172800));
        fechaBloqueo2.setHours(0)
        fechaBloqueo2.setMinutes(0)
        fechaBloqueo2.setMinutes(0)

        $scope.fechaBloqueo1 = fechaBloqueo1
        $scope.fechaBloqueo2 = fechaBloqueo2

        $scope.Radio = { Anticipo: 'Conductor' }

        $scope.ListadoPermisos.forEach(function (item) {
            if (item.Padre == OPCION_MENU_DESPCHAR_ORDEN_SERVICIOS) {
                $scope.ListadoPermisosEspecificos.push(item)
            }
        });
        $scope.VisualizacionTarifario = false
        $scope.CambioVehiculoEnturnado = false

        $scope.ListadoPermisosEspecificos.forEach(function (item) {
            if (item.Codigo == PERMISO_VISUALIZAR_TARIFARIO) {
                $scope.VisualizacionTarifario = true
            }
            if (item.Codigo == PERMISO_CAMBIAR_VEHICULO_ENTURNADO) {
                $scope.CambioVehiculoEnturnado = true
            }

        });
        //--Validacion Proceso 66
        var ListadoNovedades = [];
        if ($scope.Sesion.UsuarioAutenticado.ManejoDespachoFormaDePago) {
            ListadoNovedades = NovedadesDespachosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: ESTADO_ACTIVO,
                Sync: true
            }).Datos;
        }
        //--Validacion Proceso 66

        $scope.EstadoSeccionOrdenCargue = 'collapsed-box'
        if ($scope.Sesion.UsuarioAutenticado.ManejoRequiereOrdenCargueDespacho) {
            $scope.EstadoSeccionOrdenCargue = ''
        }

        //--------------- CARGAR COMBOS ---------------------------------//

        $scope.ListaSemirremolque = []
        $scope.AsignarListasemirremolque = function (value) {

            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SemirremolquesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListaSemirremolque = ValidarListadoAutocomplete(Response.Datos, $scope.ListaSemirremolque)
                }
            }
            return $scope.ListaSemirremolque
        }


        //Autocomplete Ciudades Descargue:

        $scope.AutocompleteCiudadesDescargue = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosCargueDescargueFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, CodigoTipoSitio: CODIGO_TIPO_SITIO_DESCARGUE, Ciudad: { Codigo: $scope.Modal.CiudadDevolucion.Codigo }, Sync: true
                    })
                    $scope.ListadoCiudadesDescargue = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudadesDescargue)
                }
            }

            return $scope.ListadoCiudadesDescargue
        }

        //ComboTipoManifiesto:

        $scope.ListadoTiposManifiesto = [
            { Codigo: CODIGO_CATALOGO_TIPO_MANIFIESTO_VARIOS_VIAJES_EN_EL_DIA, Nombre: 'Varios Viajes Día' },
            { Codigo: CODIGO_CATALOGO_TIPO_MANIFIESTO_MASIVO, Nombre: 'General' }
        ];

        $scope.TipoManifiesto = $linq.Enumerable().From($scope.ListadoTiposManifiesto).First('$.Codigo == ' + CODIGO_CATALOGO_TIPO_MANIFIESTO_MASIVO);

        //ComboOficinaDespacha
        $scope.CargarOficinaDespacha = function () {
            $scope.ListaOficinasDespacha = []
            var Response = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true });
            $scope.ListaOficinasDespacha = ValidarListadoAutocomplete(Response.Datos, $scope.ListaOficinasDespacha)
            $scope.ModeloOficinaDespacha = $scope.ListaOficinasDespacha[0];
            if ($scope.CodigoOficinaDespacha !== undefined && $scope.CodigoOficinaDespacha !== null && $scope.CodigoOficinaDespacha !== '') {
                if ($scope.CodigoOficinaDespacha > 0) {
                    $scope.ModeloOficinaDespacha = $linq.Enumerable().From($scope.ListaOficinasDespacha).First('$.Codigo ==' + $scope.CodigoOficinaDespacha);
                }
            }
        }
        $scope.CargarOficinaDespacha()

        //Listado Tipo Tarifas
        $scope.CargarListadoTipoTarifaTransportes = function () {
            $scope.ListadoTipoTarifaTransportes = [];
            var Response = TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true })
            $scope.ListadoTipoTarifaTransportes = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTipoTarifaTransportes)
            TipoTarifaCarga = true;
        }
        $scope.CargarListadoTipoTarifaTransportes()

        //Carga Empresa
        $scope.CargarAceptacionElectronicaEmpresa = function () {
            var Response = EmpresasFactory.Obtener({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true })
            $scope.ModeloAceptacionElectronica = Response.Datos.AceptacionElectronica == 1 ? true : false;
            if (Response.Datos.AceptacionElectronica == 1) {
                $scope.DeshabilitarAceptacion = true;
            }
        }
        $scope.CargarAceptacionElectronicaEmpresa()

        //Autocomplete Funcionario
        $scope.ListaFuncionario = [];
        $scope.AutocompleteFuncionario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_EMPLEADO,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaFuncionario = ValidarListadoAutocomplete(Response.Datos, $scope.ListaFuncionario)
                }
            }
            return $scope.ListaFuncionario
        }

        //Autocomplete Vehículos
        $scope.ListaPlaca = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 2) == 0 || value.length == 1) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaPlaca = ValidarListadoAutocomplete(Response.Datos, $scope.ListaPlaca)
                }
            }
            return $scope.ListaPlaca
        }

        $scope.EncontrarVehiculos = function (value) {
            var dato
            $scope.ListaPlaca.forEach(vehiculo => {
                if (vehiculo.CodigoAlterno == value.CodigoAlterno) {
                    dato = vehiculo
                }
            });
            $scope.ValidarVehiculoDocumentos(dato, 1)
        }


        $scope.AutocompleteConductores = function (value) {
            var ResponseAutocompleteConductores = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true }).Datos

            return ResponseAutocompleteConductores
        }

        //Autocomplete Tenedores
        $scope.ListaTenedores = [];
        $scope.AutocompleteTenedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_TENEDOR,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaTenedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListaTenedores)
                }
            }
            return $scope.ListaTenedores
        }

        //Autocomplete Remitentes
        $scope.ListaRemitente = [];
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_REMITENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRemitente)
                }
            }
            return $scope.ListaRemitente
        }

        //Autocomplete Destinatario
        $scope.ListaDestinatario = [];
        $scope.AutocompleteDestinatario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_DESTINATARIO,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaDestinatario = ValidarListadoAutocomplete(Response.Datos, $scope.ListaDestinatario)
                }
            }
            return $scope.ListaDestinatario
        }

        //Autocomplete Conductores
        $scope.ListaConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CONDUCTOR,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListaConductores)
                }
            }
            return $scope.ListaConductores
        }

        //Autocomplete Ciudades
        $scope.ListaCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListaCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCiudades)
                }
            }
            return $scope.ListaCiudades
        }

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PRECINTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoPrecinto = response.data.Datos
                        $scope.ModeloTipoPrecinto = $scope.ListadoTipoPrecinto[0]
                    }
                    else {
                        $scope.ListadoTipoPrecinto = []
                    }
                }
            }, function (response) {
            });
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 191 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCausas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCausas = response.data.Datos;
                    }
                    else {
                        $scope.ListadoCausas = []
                    }
                }
            }, function (response) {
            });

        //-- Productos Transportados --//
        $scope.CargarProductos = function () {
            $scope.ListaProductoTransportado = [];
            var Response = ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1, Sync: true })
            $scope.ListaProductoTransportado = ValidarListadoAutocomplete(Response.Datos, $scope.ListaProductoTransportado)
            $scope.ListaProductoTransportado.push({ Nombre: '', Codigo: 0 })
        }
        $scope.CargarProductos()

        $scope.ListaFormaPago = [
            { Nombre: 'Otros', Codigo: 1 },
            { Nombre: 'Transferecia', Codigo: 2 }
        ];

        $scope.FormaPagoRecaudo = $scope.ListaFormaPago[0];

        $scope.ListadoSitiosCargueAlterno2 = [];
        $scope.AutocompleteSitiosClienteCargue = function (value, cliente, ciudad) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo;
                    }
                    $scope.ListadoSitiosCargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosCargueAlterno2);
                }
            }
            return $scope.ListadoSitiosCargueAlterno2;
        };


        /*---------------------------------------------------------------------------------FUNCIONES DE CARGUE Y OBTENER ------------------------------------------------------------------------*/

        $scope.ObtenerCliente = function () {

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.ModeloCliente.Codigo,
            };

            blockUI.delay = 1000;
            TercerosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ModeloCliente.NumeroIdentificacion = response.data.Datos.NumeroIdentificacion

                    }
                    else {
                        ShowError('No se logro consultar el tercero ');
                    }
                }, function (response) {
                    ShowError('No se logro consultar el tercero ');
                });

            blockUI.stop();

        }

        $scope.ListadoTarifasFiltradas = []

        $scope.ObtenerRuta = function () {
            if ($scope.ModalRuta.Codigo > 0) {
                $scope.PorcentajeAnticipoRuta = 0
                blockUI.delay = 1000;
                var Response = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.ModalRuta.Codigo, Sync: true })
                $scope.PorcentajeAnticipoRuta = Response.Datos.PorcentajeAnticipo
                $scope.CiudadOrigenRuta = Response.Datos.CiudadOrigen
                $scope.CiudadDestinoRuta = Response.Datos.CiudadDestino
                $scope.TipoRuta = Response.Datos.TipoRuta
                if ($scope.ModalTotalFleteTransportador !== undefined) {
                    if ($scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores && TipoDuenoVehiculo == CODIGO_CATALOGO_TIPO_DUENO_PROPIO) {
                        var Response = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.ModalRuta.Codigo, Sync: true })
                        $scope.ListaTrayectos = []
                        //$scope.ModalAnticipoPlanilla = 0
                        var valorFleteANticipoRutaGeneral = Math.round((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))
                        Response.Datos.Trayectos.forEach(item => {
                            $scope.ListaTrayectos.push({
                                RutaTrayecto: item.RutaTrayecto,
                                //Anticipo: MascaraValores(Math.round(((parseFloat(item.PorcentajeFlete) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))) * (parseFloat(item.RutaTrayecto.PorcentajeAnticipo) / 100))),
                                Anticipo: MascaraValores(Math.round(((parseFloat(item.PorcentajeFlete) / 100) * valorFleteANticipoRutaGeneral) /** (parseFloat(item.RutaTrayecto.PorcentajeAnticipo) / 100)*/)),
                                //ValorFlete: MascaraValores(Math.round((parseFloat(item.PorcentajeFlete) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))),
                                ValorFlete: MascaraValores(Math.round((parseFloat(item.PorcentajeFlete) / 100) * MascaraNumero($scope.ModalTotalFleteTransportador))),
                                Orden: item.Orden,
                                PorcentajeFlete: item.PorcentajeFlete,
                                PorcentajeAnticipo: item.RutaTrayecto.PorcentajeAnticipo,
                                PorcentajeAnticipoRuta: $scope.PorcentajeAnticipoRuta
                            });
                            //$scope.ModalAnticipoPlanilla += Math.round(((parseFloat(item.PorcentajeFlete) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))) * (parseFloat(item.RutaTrayecto.PorcentajeAnticipo) / 100))
                            // $scope.ModalAnticipoPlanilla += Math.round(((parseFloat(item.PorcentajeFlete) / 100) * Math.round((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))))  /** (parseFloat(item.RutaTrayecto.PorcentajeAnticipo) / 100))*/)
                        })
                        if (!$scope.Deshabilitar) $scope.ModalAnticipoPlanilla = MascaraValores(valorFleteANticipoRutaGeneral)
                        if ($scope.ListaTrayectos.length > 0 && $scope.ModalPlaca != undefined) {
                            var ordenMinimo = $linq.Enumerable().From($scope.ListaTrayectos).Min('$.Orden')
                            $scope.ListaTrayectos.forEach(itemT => {
                                if (itemT.Orden == ordenMinimo) {
                                    itemT.Conductor = itemT.Conductor == undefined || itemT.Conductor == null || itemT.Conductor == '' ? $scope.CargarTercero($scope.ModalPlaca.Conductor.Codigo) : itemT.Conductor
                                }
                            })
                            // $scope.ListaTrayectos[0].Conductor = $scope.ListaTrayectos[0].Conductor == undefined || $scope.ListaTrayectos[0].Conductor == null || $scope.ListaTrayectos[0].Conductor == '' ? $scope.CargarTercero($scope.ModalPlaca.Conductor.Codigo) : $scope.ListaTrayectos[0].Conductor
                        }
                    } else if ($scope.Sesion.UsuarioAutenticado.SugerirAnticipoPlanilla && !$scope.Deshabilitar) {
                        $scope.CalcularAnticipo()
                    } else if (!$scope.Deshabilitar) {
                        $scope.ModalAnticipoPlanilla = MascaraValores(Math.round(((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))))
                    }
                }
            }

        }

        $scope.ObtenerOrdenCargue = function (NumeroOrdenCargue) {

            var OrdenCargue = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: NumeroOrdenCargue,
                Sync: true
            }


            Response = OrdenCargueFactory.Obtener(OrdenCargue)

            if (Response.ProcesoExitoso === true) {

                //Datos redunante entre documentos (Podría cambiar estos datos si tiene remesa generada)
                $scope.CodigoVehiculo = 0;
                if (Response.Datos.Vehiculo !== undefined && Response.Datos.Vehiculo !== null) {
                    if (Response.Datos.Vehiculo.Codigo > 0) {
                        $scope.ModalPlaca = $scope.CargarVehiculos(Response.Datos.Vehiculo.Codigo)
                        $scope.AsignarDatosVehiculo($scope.ModalPlaca);
                    }
                }

                $scope.CodigoSemirremolque = 0;
                if (Response.Datos.Semirremolque !== undefined && Response.Datos.Semirremolque !== null) {
                    if (Response.Datos.Semirremolque.Codigo > 0) {
                        $scope.CodigoSemirremolque = Response.Datos.Semirremolque.Codigo;
                    }
                }


                if ($scope.ListaPlacaSemi !== undefined && $scope.ListaPlacaSemi !== null) {
                    if ($scope.ListaPlacaSemi.length > 0 && $scope.CodigoSemirremolque > 0) {
                        try {
                            var tempsemi = SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true, Codigo: $scope.CodigoSemirremolque }).Datos[0] //$linq.Enumerable().From($scope.ListaPlacaSemi).First('$.Codigo ==' + $scope.CodigoSemirremolque);
                            $scope.ModalSemirremolque = tempsemi
                        } catch (e) {

                        }
                    }
                }

                if (Response.Datos.Semirremolque != undefined) {
                    if (Response.Datos.Semirremolque.Codigo > 0) {
                        $scope.ModalSemirremolque = $scope.CargarSemirremolqueCodigo(Response.Datos.Semirremolque.Codigo);
                    }
                }

                $scope.ModalNumeroOrdenCargue = Response.Datos.NumeroDocumento;
                $scope.NumeroOrdenCargue = Response.Datos.Numero;
                $scope.NumeroDocumentoOrdenCargue = Response.Datos.NumeroDocumento;

                $scope.ModalFechaOrdenCargue = new Date(Response.Datos.Fecha);
                $scope.ModalValorAnticipo = Response.Datos.ValorAnticipo;
                $scope.ModelCiudCarg = $scope.CargarCiudad(Response.Datos.CiudadCargue.Codigo);
                $scope.ModelDireCarg = Response.Datos.DireccionCargue;
                $scope.ModalTeleCarg = Response.Datos.TelefonosCargue;
                $scope.ModalContacCarg = Response.Datos.ContactoCargue;
                $scope.ModelCiudDesc = $scope.CargarCiudad(Response.Datos.CiudadDescargue.Codigo);
                $scope.ModalCantidad = Response.Datos.CantidadCliente
                $scope.ModelDireDesc = Response.Datos.DireccionDescargue;
                $scope.ModalTeleDesc = Response.Datos.TelefonoDescargue;
                $scope.ModalContacDesc = Response.Datos.ContactoDescargue;
                $scope.ModalObservacionesOrdenCargue = Response.Datos.Observaciones;
                $scope.DeshabilitarDespacho = true;
                $scope.DeshabilitarOrdenCargue = true;
            }
        }

        $scope.ObtenerRemesa = function (NumeroRemesa) {

            var Remesa = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: NumeroRemesa
            }


            RemesasFactory.Obtener(Remesa).then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    //Datos redundante entre documentos (Podría cambiar estos datos si tiene remesa generada)
                    $scope.ListaPrecintos = response.data.Datos.ListaPrecintos
                    $scope.CodigoVehiculo = 0;
                    if (response.data.Datos.Vehiculo !== undefined && response.data.Datos.Vehiculo !== null) {
                        if (response.data.Datos.Vehiculo.Codigo > 0) {
                            $scope.ModalPlaca = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo)
                            $scope.AsignarDatosVehiculo($scope.ModalPlaca);
                        }
                    }
                    $scope.CodigoSemirremolque = 0;
                    if (response.data.Datos.Semirremolque !== undefined && response.data.Datos.Semirremolque !== null) {
                        if (response.data.Datos.Semirremolque.Codigo > 0) {
                            $scope.CodigoSemirremolque = response.data.Datos.Semirremolque.Codigo;
                        }
                    }

                    if ($scope.ListaPlacaSemi !== undefined && $scope.ListaPlacaSemi !== null) {
                        if ($scope.ListaPlacaSemi.length > 0 && $scope.CodigoSemirremolque > 0) {
                            try {
                                var tempsemi = SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true, Codigo: $scope.CodigoSemirremolque }).Datos[0] // $linq.Enumerable().From($scope.ListaPlacaSemi).First('$.Codigo ==' + $scope.CodigoSemirremolque);
                                $scope.ModalSemirremolque = tempsemi
                            } catch (e) {

                            }
                        }
                    }

                    if (response.data.Datos.Semirremolque != undefined) {
                        if (response.data.Datos.Semirremolque.Codigo > 0) {
                            $scope.ModalSemirremolque = $scope.CargarSemirremolqueCodigo(response.data.Datos.Semirremolque.Codigo);
                        }
                    }

                    $scope.ModalNumeroRemesa = response.data.Datos.NumeroDocumento;
                    $scope.NumeroRemesa = response.data.Datos.Numero;
                    $scope.NumeroDocumentoRemesa = response.data.Datos.NumeroDocumento;
                    $scope.ModalFechaRemesa = new Date(response.data.Datos.Fecha);
                    $scope.ModalDocCliente = response.data.Datos.NumeroDocumentoCliente;
                    $scope.ModalTipoRemesa = response.data.Datos.TipoRemesa.Nombre;
                    $scope.ModalRemitente = $scope.CargarTercero(response.data.Datos.Remitente.Codigo)
                    $scope.ModalDireccionRemitente = response.data.Datos.DireccionRemitente;
                    $scope.ModalTelefonosRemitente = response.data.Datos.TelefonoRemitente;
                    $scope.ModalNumeroConfirmacionMinisterio = response.data.Datos.NumeroConfirmacionMinisterioRemesa;
                    try { $scope.ModalCiudadRemitente = $scope.CargarCiudad(response.data.Datos.Remitente.Ciudad.Codigo); } catch (e) { }
                    try { $scope.ModalCiudadDestinatario = $scope.CargarCiudad(response.data.Datos.Destinatario.Ciudad.Codigo); } catch (e) { }
                    $scope.ModalDestinatario = $scope.CargarTercero(response.data.Datos.Destinatario.Codigo)
                    $scope.ModalDireccionDestinatario = response.data.Datos.DireccionDestinatario;
                    $scope.ModalTelefonosDestinatario = response.data.Datos.TelefonoDestinatario;

                    $scope.ModalObservacionesRemesa = response.data.Datos.Observaciones;
                    $scope.DeshabilitarRemesa = true;
                    $scope.DeshabilitarDespacho = true;
                    $scope.DeshabilitarTarifaTransportador = true;
                    $scope.DesabilitarConfirmacionRemesa = true;

                    $scope.ModalNumeroContenedor = response.data.Datos.NumeroContenedor;
                    $scope.ModalImportacionCiudadDevolucion = $scope.CargarCiudad(response.data.Datos.CiudadDevolucion.Codigo)
                    $scope.ModalImportacionPatioDevolucion = response.data.Datos.PatioDevolucion;
                    $scope.ModalImportacionFechaDevolucion = new Date(response.data.Datos.FechaDevolucion);
                    $scope.ModalMBLContenedor = response.data.Datos.MBLContenedor;
                    $scope.ModalHBLContenedor = response.data.Datos.HBLContenedor;
                    $scope.ModalDOContenedor = response.data.Datos.DOContenedor;
                    $scope.ModalValorFOB = MascaraValores(response.data.Datos.ValorFOB);
                    if (response.data.Datos.SICDDevolucionContenedor.Codigo > 0) {
                        $scope.Modal.DevolucionContenedor = true;
                    }
                    $scope.Modal.SICDDevolucionContenedor = { Codigo: response.data.Datos.SICDDevolucionContenedor.Codigo };
                    $scope.Modal.CiudadDevolucion = $scope.CargarCiudad(response.data.Datos.CiudadDevolucion.Codigo);
                    $scope.Modal.PatioDevolucion = response.data.Datos.PatioDevolucion;
                    if (new Date(response.data.Datos.FechaDevolucionContenedor) > new Date('1970-01-01')) {
                        $scope.Modal.FechaDevolucionContenedor = new Date(response.data.Datos.FechaDevolucionContenedor);
                    };

                    if (response.data.Datos.RemesaCortesia > 0) {
                        $scope.RemesaCortesia = true
                    }

                    $scope.ModalFleteCliente = MascaraValores(response.data.Datos.ValorFleteCliente);
                    $scope.ModalTotalFleteCliente = Math.round(response.data.Datos.TotalFleteCliente);
                    $scope.DeshabilitarTarifaCliente = true;

                }
            });
        }

        $scope.ObtenerPlanillaDespacho = function (NumeroPlanillaDespacho) {

            var PlanillaDespacho = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                Numero: NumeroPlanillaDespacho
            }


            PlanillaDespachosFactory.Obtener(PlanillaDespacho).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.Manifiesto.Tipo_Manifiesto.Codigo == 8813) {
                        $scope.TipoManifiesto = $linq.Enumerable().From($scope.ListadoTiposManifiesto).First('$.Codigo == ' + response.data.Datos.Manifiesto.Tipo_Manifiesto.Codigo);
                        $scope.CantidadViajesDia = response.data.Datos.Manifiesto.CantidadViajesDia;
                    }
                    $scope.AsignarDatosVehiculo($scope.ModalPlaca);
                    if (response.data.Datos.Semirremolque.Placa != null && response.data.Datos.Semirremolque.Placa != undefined && response.data.Datos.Semirremolque.Placa != '') {
                        $scope.ModalSemirremolque = $scope.CargarSemirremolque(Response.Datos.Semirremolque.Placa);
                    }
                    if (response.data.Datos.Semirremolque != undefined) {
                        if (response.data.Datos.Semirremolque.Codigo > 0) {
                            $scope.ModalSemirremolque = $scope.CargarSemirremolqueCodigo(response.data.Datos.Semirremolque.Codigo);
                        }
                    }
                    $scope.Radio.Anticipo = response.data.Datos.AnticipoPagadoA

                    $scope.ModalNumeroPlanilla = response.data.Datos.NumeroDocumento;
                    $scope.ModalFechaSalida = new Date(response.data.Datos.FechaHoraSalida);
                    $scope.ModalHoraSalida = RetornarHoras(response.data.Datos.FechaHoraSalida);
                    try {
                        $scope.ModalManifiesto = response.data.Datos.Manifiesto.NumeroDocumento
                        $scope.MostrarCampoAceptacion = false;
                    } catch (e) {
                    }

                    $scope.ListaTrayectos = []
                    if (response.data.Datos.ConductoresTrayectos !== null && response.data.Datos.ConductoresTrayectos.length > 0) {
                        response.data.Datos.ConductoresTrayectos.forEach(item => {
                            var Ruta = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.RutaTrayecto.Codigo, Sync: true }).Datos
                            //var RutaPlanilla = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.Ruta.Codigo, Sync: true }).Datos
                            //RutaPlanilla.Trayectos.forEach(itemRuta => {
                            //    if (Ruta.Codigo == itemRuta.RutaTrayecto.Codigo) {
                            //        Ruta.PorcentajeFlete = itemRuta.PorcentajeFlete
                            //    }
                            //})
                            $scope.ListaTrayectos.push({
                                RutaTrayecto: Ruta,
                                Anticipo: MascaraValores(item.ValorAnticipo),
                                PorcentajeFlete: item.PorcentajeFlete,
                                Conductor: $scope.CargarTercero(item.Conductor.Codigo),
                                PorcentajeAnticipo: item.PorcentajeAnticipo,
                                Orden: item.OrdenTrayecto,
                                PorcentajeAnticipoRuta: item.PorcentajeAnticipoRuta
                            })
                        })
                    }
                    console.log(response.data.Datos.ValorAnticipo)
                    $scope.ModalAnticipoPlanilla = MascaraValores(Math.round(response.data.Datos.ValorAnticipo))
                    $scope.ModalObservacionesPlanilla = response.data.Datos.Observaciones;

                    for (var i = 0; i < response.data.Datos.ListadoAuxiliar.length; i++) {
                        $scope.ListaAuxiliares.push({
                            Tercero: { Nombre: response.data.Datos.ListadoAuxiliar[i].Tercero.Nombre },
                            Horas: response.data.Datos.ListadoAuxiliar[i].Horas,
                            Valor: response.data.Datos.ListadoAuxiliar[i].Valor,
                            Observaciones: response.data.Datos.ListadoAuxiliar[i].Observaciones,
                        })
                    }
                    $scope.ModalSeguroAnticipo = MascaraValores(response.data.Datos.ValorSeguroPoliza)
                    if (response.data.Datos.PagoAnticipo > 0) {
                        $scope.FormaPagoRecaudo = $scope.ListaFormaPago[1]
                    } else {
                        $scope.FormaPagoRecaudo = $scope.ListaFormaPago[0]
                    }

                    $scope.DeshabilitarPlanilla = true;
                    $timeout(function () {
                        $scope.ModalAnticipoPlanilla = MascaraValores(Math.round(response.data.Datos.ValorAnticipo));
                        $scope.ModalTotalFleteTransportador = MascaraValores(response.data.Datos.ValorFleteTransportador);
                    }, 1000);
                }

            });
        }

        $scope.DesplegarDespacharViaje = function (DetalleDespacho) {

            $scope.CodigoDetalleSolicitud = DetalleDespacho.Codigo;
            $scope.ModalRuta = DetalleDespacho.Ruta;
            $scope.ModalCantidad = MascaraValores(DetalleDespacho.Cantidad);
            $scope.ModalPeso = MascaraValores(DetalleDespacho.PesoKilogramo);
            $scope.ModelCiudCarg = $scope.CargarCiudad(DetalleDespacho.CiudadCargue.Codigo);
            $scope.ModelFechaCargue = new Date(DetalleDespacho.FechaCargueEsos);
            //--Sitio Cargue
            $scope.ModelSitioCargue = DetalleDespacho.SitioCargue;
            $scope.ModelSitioCargue.DireccionSitio = DetalleDespacho.DireccionCargue;
            $scope.ModelSitioCargue.Telefono = DetalleDespacho.TelefonoCargue;
            $scope.ModelSitioCargue.Contacto = DetalleDespacho.ContactoCargue;
            $scope.ModelSitioCargue.SitioCliente = {
                Codigo: DetalleDespacho.SitioCargue.Codigo,
                Nombre: DetalleDespacho.SitioCargue.Nombre,
                DireccionSitio: DetalleDespacho.DireccionCargue,
                Telefono: DetalleDespacho.TelefonoCargue,
                Contacto: DetalleDespacho.ContactoCargue
            };
            //--Sitio Cargue
            $scope.ModelCiudDesc = $scope.CargarCiudad(DetalleDespacho.CiudadDestinatario.Codigo);
            $scope.ModalCiudadDestinatario = $scope.CargarCiudad(DetalleDespacho.CiudadDestinatario.Codigo);
            $scope.ModelFechaDescargue = new Date(DetalleDespacho.FechaDescargueEsos);
            //--Sitio Descargue
            $scope.ModelSitioDescargue = DetalleDespacho.SitioDescargue;
            $scope.ModelSitioDescargue.DireccionSitio = DetalleDespacho.DireccionDestino;
            $scope.ModelSitioDescargue.Telefono = DetalleDespacho.TelefonoDestino;
            $scope.ModelSitioDescargue.Contacto = DetalleDespacho.ContactoDestino;
            $scope.ModelSitioDescargue.SitioCliente = {
                Codigo: DetalleDespacho.SitioDescargue.Codigo,
                Nombre: DetalleDespacho.SitioDescargue.Nombre,
                DireccionSitio: DetalleDespacho.DireccionDestino,
                Telefono: DetalleDespacho.TelefonoDestino,
                Contacto: DetalleDespacho.ContactoDestino
            };
            //--Sitio Descargue
            $scope.ModelDireCarg = DetalleDespacho.DireccionCargue;
            $scope.ModalDireccionRemitente = DetalleDespacho.DireccionCargue;
            $scope.ModalTeleCarg = DetalleDespacho.TelefonoCargue;
            $scope.ModalContacCarg = DetalleDespacho.ContactoCargue;
            $scope.ModaloDocumentoCliente = DetalleDespacho.DocumentoCliente
            $scope.ModelDireDesc = DetalleDespacho.DireccionDestino;
            $scope.ModalDireccionDestinatario = DetalleDespacho.DireccionDestino;
            $scope.ModalTeleDesc = DetalleDespacho.TelefonoDestino;
            $scope.ModalTelefonosDestinatario = DetalleDespacho.TelefonoDestino;
            $scope.ModalContacDesc = DetalleDespacho.ContactoDestino;
            $scope.ModalFleteTransportador = MascaraValores(DetalleDespacho.FleteTransportador);

            $scope.ModalValorCargueTransportador = MascaraValores(DetalleDespacho.ValorCargueTransportador);
            $scope.ModalValorDescargueTransportador = MascaraValores(DetalleDespacho.ValorDescargueTransportador);

            $scope.ModalValorManejo = DetalleDespacho.ValorManejo
            $scope.ModalValorCargue = DetalleDespacho.ValorCargue
            $scope.ModalValorDescargue = DetalleDespacho.ValorDescargue

            $scope.ModalFleteCliente = MascaraValores(DetalleDespacho.FleteCliente);
            $scope.ModalTotalFleteCliente = Math.round(DetalleDespacho.FleteCliente + DetalleDespacho.ValorManejo + DetalleDespacho.ValorCargue + DetalleDespacho.ValorDescargue);
            $scope.ModalTotalFleteTransportador = MascaraValores(DetalleDespacho.ValorFleteTransportador)
            $scope.ModalTotalFleteTransportadorTEMP = MascaraValores(DetalleDespacho.ValorFleteTransportador)
            $scope.ModalFechaOrdenCargue = new Date($scope.ModeloFecha);
            $scope.ModalFechaRemesa = new Date($scope.ModeloFecha);
            $scope.ModalFechaSalida = new Date($scope.ModeloFecha);
            $scope.ModalNumeroContenedor = DetalleDespacho.NumeroContenedor;
            $scope.ModalImportacionCiudadDevolucion = $scope.CargarCiudad(DetalleDespacho.CiudadDevolucion.Codigo)
            $scope.ModalImportacionPatioDevolucion = DetalleDespacho.PatioDevolucion;
            $scope.ModalImportacionFechaDevolucion = new Date(DetalleDespacho.FechaDevolucion);
            $scope.ModalMBLContenedor = DetalleDespacho.MBLContenedor;
            $scope.ModalHBLContenedor = DetalleDespacho.HBLContenedor;
            $scope.ModalDOContenedor = DetalleDespacho.DOContenedor;
            $scope.ModalValorFOB = MascaraValores(DetalleDespacho.ValorFOB);
            if (DetalleDespacho.SitioDevolucionContenedor.Codigo > 0) {
                $scope.Modal.DevolucionContenedor = true;
            }
            $scope.Modal.SICDDevolucionContenedor = { Codigo: DetalleDespacho.SitioDevolucionContenedor.Codigo };
            $scope.Modal.CiudadDevolucion = $scope.CargarCiudad(DetalleDespacho.CiudadDevolucion.Codigo);
            $scope.Modal.PatioDevolucion = DetalleDespacho.PatioDevolucion;
            if (new Date(DetalleDespacho.FechaDevolucionContenedor) > new Date('1970-01-01')) {
                $scope.Modal.FechaDevolucionContenedor = new Date(DetalleDespacho.FechaDevolucionContenedor);
            }
            if (DetalleDespacho.CiudadCargue !== undefined) {
                $scope.ModalCiudadRemitente = $scope.CargarCiudad(DetalleDespacho.CiudadCargue.Codigo)
            } else if ($scope.ModeloCliente.Ciudad !== undefined) {
                $scope.ModalCiudadRemitente = $scope.CargarCiudad($scope.ModeloCliente.Ciudad.Codigo)
            }
            $scope.ModalDestinatario = $scope.CargarTercero(DetalleDespacho.Destinatario.Codigo)
            $scope.ModalDireccionDestinatario = DetalleDespacho.DireccionDestino;
            $scope.ModalTelefonosDestinatario = DetalleDespacho.TelefonoDestino;
            $scope.ModalTipoRemesa = DetalleDespacho.NombreTipoRemesa;
            $scope.ModalTarifaCompra = DetalleDespacho.TarifaTransporteCargaVenta;
            $scope.ModalTarifaVenta = DetalleDespacho.TarifaTransporteCargaVenta;
            //$scope.CodigoRuta = DetalleDespacho.Ruta.Codigo;
            $scope.CodigoTipoTarifaContrato = DetalleDespacho.TipoTarifaTransporteCargaVenta.Codigo;
            if (DetalleDespacho.ProductoTransportado.Codigo > 0) {
                $scope.ModalProducto = $scope.CargarProducto(DetalleDespacho.ProductoTransportado.Codigo);
            }
            $scope.ModalPermisoInvias = DetalleDespacho.PermisoInvias;
            $scope.CodigoTipoRemesa = DetalleDespacho.CodigoTipoRemesa;
            DetalleDespacho.FechaCargue = new Date(DetalleDespacho.FechaCargue)
            DetalleDespacho.FechaCargue.setHours(1)
            DetalleDespacho.FechaCargue.setMinutes(0)
            DetalleDespacho.FechaCargue.setMinutes(0)
            $scope.ValidaPeso = true
            if (DetalleDespacho.FechaCargue < $scope.fechaBloqueo2 && DetalleDespacho.FechaCargue >= $scope.fechaBloqueo1 && $scope.Sesion.UsuarioAutenticado.ManejoBloqueoDespachosFechasPosteriores) {
                $scope.NoVisualizarDocumento = true
            }
            if (DetalleDespacho.AplicaEscolta > 0) {
                $scope.ModalAplicaEscolta = true;
                $scope.ValorEscolta = DetalleDespacho.ValorEscolta;
            } else {
                $scope.ModalAplicaEscolta = false;
                $scope.ValorEscolta = 0;
            }
            if (DetalleDespacho.NumeroOrdenCargue > 0) {
                $scope.NoAsignarSemirremolque = true;
                $scope.ObtenerOrdenCargue(DetalleDespacho.NumeroOrdenCargue);
                $scope.ValidaPeso = false
            }

            if (DetalleDespacho.NumeroRemesa > 0) {
                $scope.NoAsignarSemirremolque = true;
                $scope.ObtenerRemesa(DetalleDespacho.NumeroRemesa);
                $scope.ModalValorFOB = MascaraValores(DetalleDespacho.ValorFOB);

                $scope.ValidaPeso = false
            }

            if (DetalleDespacho.NumeroPlanillaDespacho > 0) {
                $scope.NoAsignarSemirremolque = true;
                $scope.ObtenerPlanillaDespacho(DetalleDespacho.NumeroPlanillaDespacho);
                $scope.ValidaPeso = false


            }
            $scope.ObtenerRuta()

            if ($scope.Sesion.Empresa.GeneraManifiesto === 1) {
                if (DetalleDespacho.CodigoTipoRemesa == CODIGO_TIPO_REMESA_NACIONAL) {
                    $scope.MostrarCampoManifiesto = true;
                }
            }

            if ($scope.Sesion.Empresa.GeneraManifiesto === 1) {
                if (DetalleDespacho.CodigoTipoRemesa == CODIGO_TIPO_REMESA_NACIONAL) {
                    $scope.MostrarCampoAceptacion = true;
                }
            }
        }

        function ObtenerOrdenServicio() {

            blockUI.start('Cargando Orden de Servicio');

            $timeout(function () {
                blockUI.message('Cargando Orden de Servicio');
            }, 200);

            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                IDRegistroDetalle: $scope.IDCodigo,
                DesdeDespachoOrdenServicio: true,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO
            };

            blockUI.delay = 1000;
            EncabezadoSolicitudOrdenServiciosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        OrdenServicio = response.data.Datos;
                        if (response.data.Datos.LineaNegocioCarga.Codigo == 1) {
                            $scope.ComboTipoManifiesto = true;
                        }
                        $scope.ModeloNumero = response.data.Datos.NumeroDocumento;
                        $scope.ModeloOficinaDespacha = $linq.Enumerable().From($scope.ListaOficinasDespacha).First('$.Codigo ==' + response.data.Datos.OficinaDespacha.Codigo);
                        $scope.ModaloDocumentoCliente = response.data.Datos.DocumentoCliente
                        $scope.CodigoEstadoSolicitud = response.data.Datos.CodigoEstadoSolicitud;
                        $scope.ModeloCliente = response.data.Datos.Cliente;
                        $scope.ObtenerCliente()
                        $scope.ModeloLineaNegocioCarga = response.data.Datos.LineaNegocioCarga;
                        $scope.ModalDocCliente = response.data.Datos.DocumentoCliente;
                        $scope.ListaOrdenesServicios = [];
                        $scope.ModalTarifarioVenta = response.data.Datos.TarifarioVentas;
                        //Para filtrar tarifas en el detalle
                        $scope.CodigoLineaNegocio = response.data.Datos.LineaNegocioCarga.Codigo;
                        $scope.ModeloLineaNegocio = response.data.Datos.LineaNegocioCarga;
                        $scope.ModeloTipoLineaNegocio = response.data.Datos.TipoLineaNegocioCarga;
                        $scope.TipoVehiculoOrdenServicio = response.data.Datos.TipoVehiculo;
                        if ($scope.IDCodigo > 0) {
                            $scope.ListaOrdenesServicios[0] = $linq.Enumerable().From(response.data.Datos.DetalleDespacho).First('$.Codigo ==' + parseInt($scope.IDCodigo));
                            $scope.ModalTipoTarifaVenta = $scope.ListaOrdenesServicios[0].TipoTarifaTransporteCargaVenta.Nombre;
                            try {
                                $scope.ListaTipoTarifasCompra = []
                                $scope.ListaOrdenesServicios[0].TipoTarifaTransporteCargaVenta.CodgioTarifa = $scope.ListaOrdenesServicios[0].TarifaTransporteCargaVenta.Codigo
                                $scope.ListaTipoTarifasCompra.push($scope.ListaOrdenesServicios[0].TipoTarifaTransporteCargaVenta)
                                $scope.ModalTipoTarifaCompra = $scope.ListaTipoTarifasCompra[0]
                            } catch (e) {

                            }

                            $scope.ModalTarifaVenta = $scope.ListaOrdenesServicios[0].TarifaTransporteCargaVenta

                            $scope.DesplegarDespacharViaje($scope.ListaOrdenesServicios[0]);

                            $scope.ModalRemitente = $scope.CargarTercero(response.data.Datos.Remitente.Codigo)
                            $scope.ModalDireccionRemitente = response.data.Datos.DireccionCargue
                            $scope.ModalTelefonosRemitente = response.data.Datos.TelefonoCargue


                        }
                        if ($scope.CodigoLineaNegocio == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                            $scope.DeshabilitarTarifaTransportador = true;
                        }

                        if (response.data.Datos.SitioDevolucionContenedor != undefined && response.data.Datos.SitioDevolucionContenedor != null) {
                            if (response.data.Datos.SitioDevolucionContenedor.Codigo > 0) {
                                $scope.Modal.DevolucionContenedor = true;
                            }
                            $scope.Modal.SICDDevolucionContenedor = { Codigo: response.data.Datos.SitioDevolucionContenedor.Codigo };
                            $scope.Modal.CiudadDevolucion = $scope.CargarCiudad(response.data.Datos.CiudadDevolucion.Codigo);
                            $scope.Modal.PatioDevolucion = response.data.Datos.PatioDevolucion;
                            if (new Date(response.data.Datos.FechaDevolucionContenedor) > new Date('1970-01-01')) {
                                $scope.Modal.FechaDevolucionContenedor = new Date(response.data.Datos.FechaDevolucionContenedor);
                            };
                        }
                    }
                    else {
                        ShowError('No se logro consultar el despacho para la Orden de Servicio No. ' + $scope.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarDespacharOrdenServicios';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarDespacharOrdenServicios';
                });
            blockUI.stop();
        };

        // $scope.ModalValorFOB = MascaraValores($scope.ModalValorFOB);
        /*$("#FOB").change(function () {
            MascaraValores($scope.ModalValorFOB);
        });*/

        //----------------------------------------------------------------------FUNCIONES-------------------------------------------------------------------------------------------------------//

        $scope.AgregarAuxiliar = function () {
            if ($scope.ModeloFuncionario !== '' && $scope.ModeloFuncionario !== undefined && $scope.ModeloFuncionario !== null && $scope.ModeloFuncionario.Codigo !== 0 && $scope.ModeloFuncionario.Codigo !== undefined) {
                var concidencias = 0
                if ($scope.ListaAuxiliares.length > 0) {
                    for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                        if ($scope.ModeloFuncionario.Codigo === $scope.ListaAuxiliares[i].Tercero.Codigo) {
                            concidencias++
                            break;
                        }
                    }
                    if (concidencias > 0) {
                        ShowError('El funcionario ya fue ingresado')
                        $scope.ModeloFuncionario = '';
                    } else {
                        $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                        $scope.ModeloFuncionario = '';
                    }
                } else {
                    $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                    $scope.ModeloFuncionario = '';
                }
            } else {
                ShowError('Debe ingresar un funcionario valido');
            }
            $scope.ValidarDatosFuncionario();
        }

        $scope.ValidarDatosFuncionario = function () {
            for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                if ($scope.ListaAuxiliares[i].Horas === '' || $scope.ListaAuxiliares[i].Horas === undefined || $scope.ListaAuxiliares[i].Horas === null || $scope.ListaAuxiliares[i].Horas === 0) {
                    ShowError('Debe ingresar los detalles de horas trabajadas');
                } else {
                    $scope.ListaAuxiliares[i].Modificarfuncionario = false;
                }
            }
        }

        $scope.EliminarFuncionario = function (codigo) {
            if ($scope.ListaAuxiliares.length > 0) {
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (codigo === $scope.ListaAuxiliares[i].Codigo) {
                        $scope.ListaAuxiliares.splice(i, 1);
                    }
                }
            }
        };

        //Calculo Tarifas Transportador
        $scope.GestionarFletesCompra = function () {
            if ($scope.ModalTarifaCompra !== undefined && $scope.ModalTarifaCompra !== null) {
                var ModalCantidad = MascaraNumero(MascaraValores($scope.ModalCantidad));
                var ModalFleteTransportador = MascaraNumero(MascaraValores($scope.ModalFleteTransportador));
                var ModalPeso = MascaraNumero(MascaraValores($scope.ModalPeso));
                $scope.ModalTotalFleteTransportador = 0;
                /*
                    VF: Valor Total Flete
                    FT: Flete Transportador
                    C: Cantidad
                    PE: Peso Kg
                    RI: Rango Inferior
                    RS: Rango Superior
                */

                //Contenedor (VF = FT * C)
                if ($scope.ModalTarifaCompra.Codigo == TARIFA_CONTENEDOR) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalCantidad) * parseInt(ModalFleteTransportador);
                }

                //Devolucion Contenedor (VF = FT * C)
                if ($scope.ModalTarifaCompra.Codigo == TARIFRA_DEVOLUCION_CONTENEDOR) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalCantidad) * parseInt(ModalFleteTransportador);
                }

                //Cupo Vehiculo (VF = FT * C)
                if ($scope.ModalTarifaCompra.Codigo == TARIFA_CUPO_VEHICULO) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalFleteTransportador);
                }

                //Tarifa Dia / Hora (VF = FT * C)
                if ($scope.ModalTarifaCompra.Codigo == TARIFA_DIA || $scope.ModalTarifaCompra.Codigo == TARIFA_HORA) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalCantidad) * parseInt(ModalFleteTransportador);
                }
                //Movilización
                if ($scope.ModalTarifaCompra.Codigo == TARIFA_MOVILIZACION) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalFleteTransportador);
                }
                //Peso flete
                if ($scope.ModalTarifaVenta.Codigo == 301) {
                    //$scope.ModalTotalFleteTransportador = (parseInt(ModalFleteTransportador) / 1000) * parseInt(ModalPeso);
                    $scope.ModalTotalFleteTransportador = Math.round(parseInt(ModalPeso / 1000) * parseInt(ModalFleteTransportador));
                }
                //Peso Producto
                if ($scope.ModalTarifaCompra.Codigo == TARIFA_PESO_FLETE) {
                    //$scope.ModalTotalFleteTransportador = (parseInt(ModalFleteTransportador) / 1000) * parseInt(ModalPeso);
                    $scope.ModalTotalFleteTransportador = Math.round(parseInt(ModalPeso / 1000) * parseInt(ModalFleteTransportador));
                }
                if ($scope.ModalTarifaVenta.Codigo == 303) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalFleteTransportador);
                }
                //Producto
                if ($scope.ModalTarifaCompra.Codigo == 302) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalCantidad) * parseInt(ModalFleteTransportador);
                }
                //Peso Barril galon
                if ($scope.ModalTarifaCompra.Codigo == 6 || $scope.ModalTarifaCompra.Codigo == 300) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalFleteTransportador) * parseFloat(ModalCantidad);
                    inserto = true;
                }
                if ($scope.ModalTarifaCompra.Codigo == 301) {
                    //$scope.ModalTotalFleteTransportador = (parseInt(ModalFleteTransportador) / 1000) * parseFloat(ModalPeso);
                    $scope.ModalTotalFleteTransportador = parseFloat(ModalPeso / 1000) * parseInt(ModalFleteTransportador);
                    inserto = true;
                }

                $scope.ModalTotalFleteTransportadorTEMP = $scope.ModalTotalFleteTransportador;
                $scope.ModalTotalFleteTransportador = MascaraValores($scope.ModalTotalFleteTransportador);
                if ($scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores && TipoDuenoVehiculo == CODIGO_CATALOGO_TIPO_DUENO_PROPIO) {
                    var Response = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.ModalRuta.Codigo, Sync: true })
                    $scope.ListaTrayectos = []
                    $scope.ModalAnticipoPlanilla = 0
                    var valorFleteANticipoRutaGeneral = Math.round((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))
                    Response.Datos.Trayectos.forEach(item => {
                        $scope.ListaTrayectos.push({
                            RutaTrayecto: item.RutaTrayecto,
                            //Anticipo: MascaraValores(Math.round(((parseFloat(item.PorcentajeFlete) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))) * (parseFloat(item.RutaTrayecto.PorcentajeAnticipo) / 100))),
                            Anticipo: MascaraValores(Math.round(((parseFloat(item.PorcentajeFlete) / 100) * valorFleteANticipoRutaGeneral) /** (parseFloat(item.RutaTrayecto.PorcentajeAnticipo) / 100)*/)),
                            //ValorFlete: MascaraValores(Math.round((parseFloat(item.PorcentajeFlete) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))),
                            ValorFlete: MascaraValores(Math.round((parseFloat(item.PorcentajeFlete) / 100) * MascaraNumero($scope.ModalTotalFleteTransportador))),
                            Orden: item.Orden,
                            PorcentajeFlete: item.PorcentajeFlete,
                            PorcentajeAnticipo: item.RutaTrayecto.PorcentajeAnticipo,
                            PorcentajeAnticipoRuta: $scope.PorcentajeAnticipoRuta
                        });
                        //$scope.ModalAnticipoPlanilla += Math.round(((parseFloat(item.PorcentajeFlete) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))) * (parseFloat(item.RutaTrayecto.PorcentajeAnticipo) / 100))
                        //$scope.ModalAnticipoPlanilla +=Math.round(((parseFloat(item.PorcentajeFlete) / 100) * Math.round((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))) * (parseFloat(item.RutaTrayecto.PorcentajeAnticipo) / 100))
                    })
                    $scope.ModalAnticipoPlanilla = MascaraValores(valorFleteANticipoRutaGeneral)
                    if ($scope.ListaTrayectos.length > 0 && $scope.ModalPlaca != undefined) {
                        var ordenMinimo = $linq.Enumerable().From($scope.ListaTrayectos).Min('$.Orden')
                        $scope.ListaTrayectos.forEach(itemT => {
                            if (itemT.Orden == ordenMinimo) {
                                itemT.Conductor = itemT.Conductor == undefined || itemT.Conductor == null || itemT.Conductor == '' ? $scope.CargarTercero($scope.ModalPlaca.Conductor.Codigo) : itemT.Conductor
                            }
                        })
                        // $scope.ListaTrayectos[0].Conductor = $scope.ListaTrayectos[0].Conductor == undefined || $scope.ListaTrayectos[0].Conductor == null || $scope.ListaTrayectos[0].Conductor == '' ? $scope.CargarTercero($scope.ModalPlaca.Conductor.Codigo) : $scope.ListaTrayectos[0].Conductor
                    }
                } else if ($scope.Sesion.UsuarioAutenticado.SugerirAnticipoPlanilla) {
                    $scope.CalcularAnticipo()
                } else {
                    $scope.ModalAnticipoPlanilla = MascaraValores(Math.round(((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))))
                }

            }
        };

        //Tarifario Compra
        $scope.ConsultarTarifarioCompra = function (CodigoTenedor) {
            if ($scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta) {
                //$scope.ListaTipoTarifasCompra = []
                //Cantidad Viajes
                try {
                    if ($scope.ModalTarifaCompra.Codigo == TARIFA_GALON) {
                        $scope.ListaTipoTarifasCompra.push($linq.Enumerable().From($scope.ListaProductoTransportado).First('$.Nombre =="' + $scope.ModalTipoTarifaVenta + '"'));
                        $scope.ModalTipoTarifaCompra = $linq.Enumerable().From($scope.ListaTipoTarifasCompra).First('$.Nombre =="' + $scope.ModalTipoTarifaVenta + '"');
                    } else {
                        $scope.ListaTipoTarifasCompra.push($linq.Enumerable().From($scope.ListadoTipoTarifaTransportes).First('$.Nombre =="' + $scope.ModalTipoTarifaVenta + '"'));
                        $scope.ModalTipoTarifaCompra = $linq.Enumerable().From($scope.ListaTipoTarifasCompra).First('$.Nombre =="' + $scope.ModalTipoTarifaVenta + '"');
                    }
                } catch (e) {
                    $scope.ListaTipoTarifasCompra.push($linq.Enumerable().From($scope.ListadoTipoTarifaTransportes).First('$.Nombre =="' + $scope.ModalTipoTarifaVenta + '"'));
                    $scope.ModalTipoTarifaCompra = $linq.Enumerable().From($scope.ListaTipoTarifasCompra).First('$.Nombre =="' + $scope.ModalTipoTarifaVenta + '"');
                }
            } else {
                if (CodigoTenedor !== undefined && CodigoTenedor !== null && CodigoTenedor > 0) {
                    $scope.CodigoTenedor = CodigoTenedor
                    if ($scope.ModalRuta !== undefined) {
                        if ($scope.ModalRuta.Codigo > 0) {
                            var FiltroTarifa = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Codigo: CodigoTenedor,
                                TarifarioProveedores: true,
                                CodigoLineaNegocio: $scope.CodigoLineaNegocio,
                                CodigoTipoLineaNegocio: $scope.ModeloTipoLineaNegocio.Codigo,
                                TarifaCarga: $scope.ModalTarifaVenta,
                                CodigoRuta: $scope.ModalRuta.Codigo,
                                Sync: true
                            };
                            $scope.ModalTarifarioCompra = {};

                            var response = TercerosFactory.Consultar(FiltroTarifa);

                            if (response.ProcesoExitoso === true) {
                                if (response.Datos.length > 0) {
                                    $scope.ModalTarifarioCompra = response.Datos;
                                    $scope.ModalFleteTransportador = MascaraValores(MascaraNumero(response.Datos[0].ValorFlete));
                                    $scope.ListaTipoTarifasCompra = [];
                                    $scope.ListadoTarifasCompras = [];
                                    response.Datos.forEach(function (item) {
                                        var TarifaAux = {
                                            Codigo: item.TipoTarifaCarga.Codigo,
                                            Nombre: item.TipoTarifaCarga.Nombre,
                                            CodgioTarifa: item.TarifaCarga.Codigo,
                                            ValorFlete: item.ValorFlete
                                        }
                                        $scope.ListaTipoTarifasCompra.push(TarifaAux);
                                        $scope.ModalFleteTransportador = MascaraValores(item.ValorFlete)

                                    });
                                    try {
                                        $scope.ModalTipoTarifaCompra = $linq.Enumerable().From($scope.ListaTipoTarifasCompra).First('$.Nombre =="' + $scope.ModalTipoTarifaVenta + '"');
                                        $scope.ModalFleteTransportador = MascaraValores($scope.ModalTipoTarifaCompra.ValorFlete);
                                    } catch (e) {
                                        $scope.ModalTipoTarifaCompra = $scope.ListaTipoTarifasCompra[0];
                                        $scope.ModalFleteTransportador = MascaraValores(MascaraNumero($scope.ListaTipoTarifasCompra[0].ValorFlete));
                                    }
                                } else {
                                    if (!$scope.DeshabilitarDespacho) {
                                        ShowError("No existe un tarifario de compra para el vehículo y tenedor ingresados o no se encontraron tarifas disponibles")
                                        $scope.ModalConductor = ''
                                        $scope.ModalTenedor = ''
                                        $scope.ModalSemirremolque = ''
                                        $scope.ModalPlaca = ''
                                    } else {
                                        $scope.ModalTipoTarifaCompra = $linq.Enumerable().From($scope.ListadoTipoTarifaTransportes).First('$.Nombre =="' + $scope.ModalTipoTarifaVenta + '"');
                                    }
                                }
                            } else {
                                ShowError("No existe un tarifario de compra para el vehículo y tenedor ingresados")
                                $scope.ModalConductor = ''
                                $scope.ModalTenedor = ''
                                $scope.ModalSemirremolque = ''
                                $scope.ModalPlaca = ''
                            }

                        }
                    }

                } else {
                    if (!$scope.DeshabilitarDespacho) {
                        ShowError("No hay un tenedor ingresado para consultar el tarifario de compra")
                        $scope.ModalConductor = ''
                        $scope.ModalTenedor = ''
                        $scope.ModalSemirremolque = ''
                        $scope.ModalPlaca = ''
                        $scope.ModalTarifarioCompra = {};
                    }
                }
            }

        }

        $scope.CambiarSeleccionTarifaCompra = function (Tarifa) {
            if (Tarifa !== undefined) {
                $scope.ModalFleteTransportador = MascaraValores(MascaraNumero(Tarifa.ValorFlete));
                $scope.GestionarFletesCompra();
                $scope.GestionarFletesVenta();
            }
        }


        //----------------- GESTION DE TARIFAS ------------------------//
        $scope.ValidarFleteTransportador = function () {
            $scope.AutorizacionFlete = 0
            $scope.AutorizacionAnticipo = 0
            if ($scope.Sesion.UsuarioAutenticado.ManejoAutorizacionFlete) {
                if (parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)) > parseInt(MascaraNumero($scope.ModalTotalFleteTransportadorTEMP))) {
                    ShowConfirm('El valor del flete ingresado es superior al estipulado en el tarifario. ¿Desea solicitar autorización para el cambio de flete?', $scope.CambiarFlete, $scope.RechazoCambioFlete)
                } else {
                    $scope.CalcularAnticipo()
                }
            } else {
                $scope.CalcularAnticipo()
            }
        }

        $scope.CambiarFlete = function () {
            $scope.AutorizacionFlete = 1
            $scope.AutorizacionAnticipo = 0
            $scope.CalcularAnticipo()
        }

        $scope.RechazoCambioFlete = function () {
            $scope.AutorizacionFlete = 0
            $scope.ModalTotalFleteTransportador = MascaraValores($scope.ModalTotalFleteTransportadorTEMP)
            $scope.calcularAnticipo()
        }

        $scope.ValidarAnticipoPlanilla = function (anticipo) {
            $scope.AutorizacionAnticipo = 0
            anticipo = parseInt(MascaraNumero(anticipo))
            if (parseFloat($scope.PorcentajeAnticipoRuta) > 0) {
                if (anticipo > parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))) {
                    ShowError('El anticipo no puede se mayor al flete del transportador')
                    anticipo = 0
                    $scope.AutorizacionAnticipo = 0
                    if ($scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores) {
                        $scope.GestionarFletesCompra()
                        anticipo = parseInt(MascaraNumero($scope.ModalAnticipoPlanilla))
                    }
                }
                else if (anticipo > ((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))) {
                    $scope.AnticipoAutorizado = MascaraValores((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))
                    showModal('modalConfirmacionAnticipo')
                } else {
                    $scope.AutorizacionAnticipo = 0
                }
                $scope.ModalAnticipoPlanilla = MascaraValores(Math.round(parseInt(anticipo)))
            }
        }

        $scope.CalcularTotalAnticipos = function () {
            var Total = 0
            $scope.ListaTrayectos.forEach(item => {
                Total += MascaraNumero(item.Anticipo)
            })
            $scope.ModalAnticipoPlanilla = MascaraValores(Total)
        }

        $scope.ValidarConductorTrayecto = function (Conductor, index) {
            if (Conductor != undefined && Conductor != null && Conductor != '') {
                if ($scope.ListaTrayectos.length > 0) {
                    var conCon = 0
                    for (var i = 0; i < $scope.ListaTrayectos.length; i++) {
                        if (index != $scope.ListaTrayectos[i].Orden) {
                            if ($scope.ListaTrayectos[i].Conductor != undefined) {
                                if ($scope.ListaTrayectos[i].Conductor.Codigo == Conductor.Codigo) {
                                    $scope.ListaTrayectos[index].Conductor = '';
                                    conCon++;
                                }
                            }
                        }
                    }
                    if (conCon > 0) {
                        ShowError('Este conductor ya se encuentra en la lista')
                    }

                    $scope.ListaConductores = []
                    $scope.ListaTrayectos.forEach(item => {
                        if (item.Conductor != undefined || item.Conductor != null && item.Conductor != '') {
                            $scope.ListaConductores.push(item.Conductor);
                        }
                    })
                }
            }
        }

        $scope.ValidarAnticipoOrdenCargue = function (anticipo) {
            anticipo = parseInt(MascaraNumero(anticipo))
            if (parseFloat($scope.PorcentajeAnticipoRuta) > 0) {
                if (anticipo > ((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))) {
                    ShowError('El anticipo no puede ser mayor a ' + ((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))))
                    $scope.ModalValorAnticipo = 0
                } else {
                    $scope.ModalValorAnticipo = MascaraValores(parseInt(anticipo))
                }
            }
        }

        $scope.GenerarDespacho = function () {
            if (DatosRequeridosGenerarDesacho()) {
                BloqueoPantalla.start('Guardando...');
                var OrdenServicio = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Numero,
                    Estado: 1,
                    UsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                    TarifarioVentas: $scope.TarifarioVentas,
                    LineaNegocioCarga: { Codigo: $scope.CodigoLineaNegocio },
                    InsertaDetalle: 1,
                    Detalle: [{
                        Ruta: $scope.ModalRuta,
                        TipoLineaNegocioCarga: $scope.ModeloTipoLineaNegocio,
                        Tarifa: $scope.ModalTarifaVenta,
                        TipoTarifa: { Codigo: $scope.ModalTipoTarifaCompra.Codigo },
                        Producto: $scope.ModalProducto,
                        Cantidad: $scope.ModalCantidad,
                        Peso: $scope.ModalPeso,
                        ValorFleteCliente: Math.round(MascaraNumero(MascaraValores($scope.ModalTotalFleteCliente))),
                        DocumentoCliente: $scope.ModaloDocumentoCliente,
                        CiudadCargue: $scope.ModelCiudCarg,
                        DireccionCargue: $scope.ModelDireCarg,
                        TelefonoCargue: $scope.ModalTeleCarg,
                        ContactoCargue: $scope.ModalContacCarg,
                        Destinatario: $scope.ModeloCliente,
                        CiudadDestino: $scope.ModelCiudDesc,
                        DireccionDestino: $scope.ModelDireDesc,
                        TelefonoDestino: $scope.ModalTeleDesc,
                        ContactoDestino: $scope.ModalContacDesc

                    }]
                }
                EncabezadoSolicitudOrdenServiciosFactory.Guardar(OrdenServicio).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('Se guardó el despacho correctamente');
                                $scope.IDCodigo = response.data.Datos;
                                $scope.CodigoDetalleSolicitud = response.data.Datos;
                                $scope.ModalFechaOrdenCargue = new Date()
                                $scope.GuardarOrdenCargueSync()
                                ObtenerOrdenServicio()
                                //document.location.href = '#!GestionarDespacharOrdenServicios/' + $scope.Numero + '/' + response.data.Datos;
                                BloqueoPantalla.stop();
                            }
                            else {
                                ShowError(response.statusText);
                                BloqueoPantalla.stop();
                            }
                        }
                        else {
                            ShowError(response.statusText);
                            BloqueoPantalla.stop();
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        BloqueoPantalla.stop();
                    });
            }
        }

        $scope.CalcularAnticipo = function () {

            //El anticipo se calcula siempre y cuando tenga activa la validación procesos desde BD:
            if ($scope.Sesion.UsuarioAutenticado.SugerirAnticipoPlanilla == true) {
                var sumValores = 0
                var ListaGastosRuta = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.ModalRuta.Codigo, ConsultaLegalizacionGastosRuta: true, Sync: true }).Datos.LegalizacionGastosRuta;

                if ($scope.ModalPlaca != undefined) {

                    // solo se calcula el anticipo si el tipo de dueño del vehículo es propio:
                    if ($scope.ModalPlaca.TipoDueno.Codigo == CODIGO_CATALOGO_TIPO_DUENO_PROPIO) {
                        ListaGastosRuta.forEach(itemGasto => {
                            if (itemGasto.TipoVehiculo.Codigo == $scope.ModalPlaca.TipoVehiculo.Codigo) {
                                if (itemGasto.AplicaAnticipo == ESTADO_ACTIVO) {
                                    sumValores += itemGasto.Valor;
                                }
                            }
                        });

                        //Obtener el valor total de los peajes:
                        //Obtener Tipo de Vehículo y Número total de Ejes del Semirremolque de la Planilla:
                        if ($scope.ModalPlaca != undefined && $scope.ModalPlaca != null && $scope.ModalPlaca != {} && $scope.ModalPlaca != '') {
                            if ($scope.ModalPlaca.Codigo != 0 && $scope.ModalPlaca.Placa != '') {
                                var responseVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.CodigoEmpresa, Codigo: $scope.ModalPlaca.Codigo, Sync: true });
                                var responseSemirremolque = {};
                                var TotalEjes = 0;
                                if (responseVehiculo.ProcesoExitoso == true) {

                                    if ($scope.Semirremolque != undefined && $scope.Semirremolque != null && $scope.Semirremolque != {} && $scope.Semirremolque != '') {
                                        if ($scope.Modelo.Planilla.Semirremolque.Codigo != 0 && $scope.Modelo.Planilla.Semirremolque.Placa != '') {

                                            responseSemirremolque = SemirremolquesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Semirremolque.Codigo, Sync: true });
                                            if (responseSemirremolque.ProcesoExitoso == true) {

                                                // ejes del Semirremolque:
                                                TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                                            }

                                        } else {
                                            TotalEjes = 0;
                                        }
                                    }


                                    $scope.TotalPeajes = 0;


                                    //Consultar los peajes de la ruta de la Planilla:
                                    var ResponseRutas = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.ModalRuta.Codigo, ConsultarPeajes: true, Sync: true });

                                    if (ResponseRutas.ProcesoExitoso == true) {

                                        var ListaTarifasPeajesRuta = [];
                                        var PeajesRutasSize = ResponseRutas.Datos.PeajeRutas.length;

                                        $scope.ListadoPeajes = [];

                                        //Por cada peaje, consultar las tarifas parametrizadas:
                                        for (let i = 0; i < ResponseRutas.Datos.PeajeRutas.length; i++) {
                                            if (ResponseRutas.Datos.PeajeRutas[i].Peaje.Estado == ESTADO_ACTIVO) {
                                                let NombrePeaje = ResponseRutas.Datos.PeajeRutas[i].Peaje.Nombre;

                                                var ResponsePeajes = PeajesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: ResponseRutas.Datos.PeajeRutas[i].Peaje.Codigo, ConsultarTarifasPeajes: true, Sync: true })


                                                //Comparar si el tipo de vehículo de la solicitud, coincide con las tarifas parametrizadas en el peaje:
                                                for (let j = 0; j < ResponsePeajes.Datos.TarifasPeajes.length; j++) {
                                                    if (ResponsePeajes.Datos.TarifasPeajes[j].TipoVehiculo.Codigo == $scope.ModalPlaca.TipoVehiculo.Codigo) {

                                                        // si coincide, verificar primero que la solicitud no tenga semirremolque:
                                                        if (TotalEjes > 0) {
                                                            //si lo tiene agregar el valor según el número de ejes del semirremolque:                                                                

                                                            if (TotalEjes == 1) {
                                                                sumValores += ResponsePeajes.Datos.TarifasPeajes[j].ValorRemolque1Eje;
                                                            } else if (TotalEjes == 2) {
                                                                sumValores += ResponsePeajes.Datos.TarifasPeajes[j].ValorRemolque2Ejes;
                                                            } else if (TotalEjes == 3) {
                                                                sumValores += ResponsePeajes.Datos.TarifasPeajes[j].ValorRemolque3Ejes;
                                                            } else if (TotalEjes >= 4) {
                                                                sumValores += ResponsePeajes.Datos.TarifasPeajes[j].ValorRemolque4Ejes;
                                                            }


                                                        } else {
                                                            //si no lo tiene, agregar el valor parametrizado por defecto:

                                                            sumValores += ResponsePeajes.Datos.TarifasPeajes[j].Valor;

                                                        }
                                                    }

                                                }
                                            }
                                        }


                                    }
                                }



                            }
                        }
                        //Fin Conceptos de Peajes//


                        $scope.ModalAnticipoPlanilla = MascaraValores(sumValores);
                    }
                }
            } else {
                $scope.ModalAnticipoPlanilla = MascaraValores(Math.round((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))))
            }



        }

        //Calculo Tarifas Cliente
        $scope.GestionarFletesVenta = function () {
            if ($scope.ModalTarifaVenta !== undefined && $scope.ModalTarifaVenta !== null) {

                $scope.ModalTotalFleteCliente = 0;

                /*
                    VF: Valor Total Flete
                    FC: Flete Cliente
                    C: Cantidad
                    PE: Peso Kg
                    RI: Rango Inferior
                    RS: Rango Superior
                */

                var ModalCantidad = MascaraNumero(MascaraValores($scope.ModalCantidad));
                var ModalFleteCliente = MascaraNumero(MascaraValores($scope.ModalFleteCliente));
                var ModalCantidad = MascaraNumero(MascaraValores($scope.ModalCantidad));
                var ModalPeso = MascaraNumero(MascaraValores($scope.ModalPeso));

                //Contenedor (VF = FC * C)
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_CONTENEDOR) {
                    $scope.ModalTotalFleteCliente = Math.round(ModalCantidad * ModalFleteCliente)
                }

                //Devolucion Contenedor (VF = FC * C)
                if ($scope.ModalTarifaVenta.Codigo == TARIFRA_DEVOLUCION_CONTENEDOR) {
                    $scope.ModalTotalFleteCliente = Math.round(ModalCantidad * ModalFleteCliente)
                }

                //Cupo Vehiculo (VF = FC * C)
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_CUPO_VEHICULO) {
                    $scope.ModalTotalFleteCliente = Math.round(ModalFleteCliente)
                }

                //Tarifa Dia / Hora (VF = FC * C)
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_DIA || $scope.ModalTarifaVenta.Codigo == TARIFA_HORA) {
                    $scope.ModalTotalFleteCliente = Math.round(ModalCantidad * ModalFleteCliente);
                }
                //Movilización
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_MOVILIZACION) {
                    $scope.ModalTotalFleteCliente = Math.round(parseInt(ModalFleteCliente));
                }
                //Peso flete
                if ($scope.ModalTarifaVenta.Codigo == 301) {
                    //$scope.ModalTotalFleteCliente = Math.round((parseInt(ModalFleteCliente) / 1000) * parseInt(ModalPeso));
                    $scope.ModalTotalFleteCliente = Math.round(parseInt(ModalPeso / 1000) * parseInt(ModalFleteCliente));
                }
                //Peso Producto
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_PESO_FLETE) {
                    //$scope.ModalTotalFleteCliente = Math.round((parseInt(ModalFleteCliente) / 1000) * parseInt(ModalPeso))
                    $scope.ModalTotalFleteCliente = Math.round(parseInt(ModalPeso / 1000) * parseInt(ModalFleteCliente));
                }
                //Producto
                if ($scope.ModalTarifaVenta.Codigo == 302) {
                    $scope.ModalTotalFleteCliente = Math.round(ModalCantidad * ModalFleteCliente);
                }
                //Viajes
                if ($scope.ModalTarifaVenta.Codigo == 303) {
                    $scope.ModalTotalFleteCliente = parseInt(ModalFleteCliente);
                }
                //Peso Barril galon
                if ($scope.ModalTarifaVenta.Codigo == 6 || $scope.ModalTarifaVenta.Codigo == 300) {
                    $scope.ModalTotalFleteCliente = parseInt(ModalFleteCliente) * parseFloat(ModalCantidad);
                    inserto = true
                }

                //Peso flete
                if ($scope.ModalTarifaVenta.Codigo == 303) {
                    $scope.ModalTotalFleteCliente = Math.round(parseInt(ModalFleteCliente))
                }
                //Rango Peso Valor Kilo (SI (PE >= RI Y PE <= RS) ENTONCES  VF = FC * PE)
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_RANGO_PESO_VALOR_KILO) {

                    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_RANGO_PESOS_VALOR_KILO } }).
                        then(function (response) {
                            if (response.ProcesoExitoso === true) {

                                if (response.data.Datos.length > 0) {

                                    for (var i = 0; i < response.data.Datos.length; i++) {
                                        if (ModalPeso >= response.data.Datos[i].CampoAuxiliar2 && ModalPeso <= response.data.Datos[i].CampoAuxiliar3) {
                                            $scope.ModalTotalFleteCliente = Math.round(ModalFleteCliente * ModalPeso)
                                        }
                                    }

                                }

                            }
                        })



                }

                //Rango Peso Valor Fijo (SI (PE >= RI Y PE <= RS) ENTONCES  VF = FC)
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_RANGO_PESO_VALOR_FIJO) {

                    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_RANGO_PESOS_VALOR_FIJO } }).
                        then(function (response) {
                            if (response.ProcesoExitoso === true) {

                                if (response.data.Datos.length > 0) {

                                    for (var i = 0; i < response.data.Datos.length; i++) {
                                        if ($scope.ModalPeso >= response.data.Datos[i].CampoAuxiliar2 && $scope.ModalPeso <= response.data.Datos[i].CampoAuxiliar3) {
                                            $scope.ModalTotalFleteCliente = Math.round($scope.ModalFleteCliente)
                                        }
                                    }

                                }

                            }
                        })
                }

                //Reformatea valores
                $scope.ModalCantidad = MascaraValores(MascaraNumero(ModalCantidad));
                $scope.ModalFleteCliente = MascaraValores(MascaraNumero(ModalFleteCliente));
                $scope.ModalCantidad = MascaraValores(MascaraNumero(ModalCantidad));
                $scope.ModalPeso = MascaraValores(MascaraNumero(ModalPeso));

            }
        }

        //---------------- GENERAR DOCUMENTOS ---------------------------//

        //Ventana Confirmacion
        $scope.ConfirmarGuardarDocumentos = function (Documento) {
            window.scrollTo(top, top);
            $scope.DocumentoGenerar = Documento;

            //1 = Orden Cargue
            if (Documento == 1) {
                $scope.MensajeGuardar = '¿Desea guardar la Orden de Cargue?'
                showModal('modalConfirmacionGuardarDocumentos');
            }
            //2 = Remesa
            if (Documento == 2) {
                $scope.MensajeGuardar = '¿Desea guardar la Remesa?'
                showModal('modalConfirmacionGuardarDocumentos');
            }
            //3 = Planilla Despacho
            if (Documento == 3) {
                $scope.MensajeGuardar = '¿Desea guardar la Planilla de Despacho?'
                showModal('modalConfirmacionGuardarDocumentos');
            }
        }

        $scope.CerrarConfirmarGuardarDocumentos = function () {
            closeModal('modalConfirmacionGuardarDocumentos', 1);
        }
        //-----------------------

        //--Datos requeridos
        function DatosRequeridosDocumentos() {

            var Continuar = true;
            $scope.MensajesError = [];

            // 0 - Generales



            if ($scope.CodigoLineaNegocio !== CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                if ($scope.ModalPlaca == undefined || $scope.ModalPlaca == null || $scope.ModalPlaca == '') {

                    $scope.MensajesError.push('Debe ingresar un vehículo');
                    Continuar = false;
                } else {
                    if ($scope.ModalPlaca.Placa == '') {
                        $scope.MensajesError.push('Debe ingresar un vehículo');
                        Continuar = false;
                    } else {

                        if ($scope.ModalPlaca.TipoVehiculo.CampoAuxiliar3 == ESTADO_ACTIVO) {
                            if ($scope.ModalSemirremolque == undefined || $scope.ModalSemirremolque == null || $scope.ModalSemirremolque == '') {
                                $scope.MensajesError.push('El tipo de vehículo actual exige que ingrese un Semirremolque');
                                Continuar = false;
                            } else if ($scope.ModalSemirremolque.Codigo == 0) {
                                $scope.MensajesError.push('El tipo de vehículo actual exige que ingrese un Semirremolque');
                                Continuar = false;
                            }
                        }
                        if ($scope.ModalPlaca.Capacidad < parseInt($scope.ModalPeso.replaceAll(',', ''))) {
                            $scope.MensajesError.push('El peso ingresado supera la capacidad del vehículo');
                            Continuar = false;
                        }
                    }
                }

                if ($scope.ModalTenedor == undefined || $scope.ModalTenedor == null || $scope.ModalTenedor == '') {
                    $scope.MensajesError.push('No hay un tenedor seleccionado');
                    Continuar = false;
                } else {
                    if ($scope.ModalTenedor.Codigo == 0) {
                        $scope.MensajesError.push('Debe seleccionar un tenedor');
                        Continuar = false;
                    }
                }
                if (!$scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta) {
                    if ($scope.ModalTarifarioCompra == undefined || $scope.ModalTarifarioCompra == null || $scope.ModalTarifarioCompra == '') {
                        $scope.MensajesError.push('No hay un tarifario de compra que cumpla las condiciones especificadas del transportador');
                        Continuar = false;
                    } else {
                        if ($scope.ModalTarifarioCompra.length == 0) {
                            $scope.MensajesError.push('Debe ingresar un tarifario de compra según condiciones para el transportador');
                            Continuar = false;
                        }
                    }
                }
                //if ($scope.ModalTarifarioCompra == undefined || $scope.ModalTarifarioCompra == null || $scope.ModalTarifarioCompra == '') {
                //    $scope.MensajesError.push('No hay un tarifario de compra que cumpla las condiciones especificadas del transportador');
                //    Continuar = false;
                //} else {
                //    if ($scope.ModalTarifarioCompra.length == 0) {
                //        $scope.MensajesError.push('Debe ingresar un tarifario de compra según condiciones para el transportador');
                //        Continuar = false;
                //    }
                //}

            }


            if ($scope.ModalTarifarioVenta == undefined || $scope.ModalTarifarioVenta == null || $scope.ModalTarifarioVenta == '') {
                $scope.MensajesError.push('No hay un tarifario de venta que cumpla las condiciones especificadas del cliente');
                Continuar = false;
            } else {
                if ($scope.ModalTarifarioVenta.Codigo == 0) {
                    $scope.MensajesError.push('Debe ingresar un tarifario de venta según condiciones para el cliente');
                    Continuar = false;
                }
            }

            return Continuar;
        }

        function DatosRequeridosGenerarDesacho() {

            var Continuar = true;
            $scope.MensajesError = [];

            // 0 - Generales

            if ($scope.CodigoLineaNegocio !== CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                if ($scope.ModalPlaca == undefined || $scope.ModalPlaca == null || $scope.ModalPlaca == '') {

                    $scope.MensajesError.push('Debe ingresar un vehículo');
                    Continuar = false;
                } else {
                    if ($scope.ModalPlaca.Placa == '') {
                        $scope.MensajesError.push('Debe ingresar un vehículo');
                        Continuar = false;
                    }
                }

                if ($scope.ModalTenedor == undefined || $scope.ModalTenedor == null || $scope.ModalTenedor == '') {
                    $scope.MensajesError.push('No hay un tenedor seleccionado');
                    Continuar = false;
                } else {
                    if ($scope.ModalTenedor.Codigo == 0) {
                        $scope.MensajesError.push('Debe seleccionar un tenedor');
                        Continuar = false;
                    }
                }

                if (!$scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta) {
                    if ($scope.ModalTarifarioCompra == undefined || $scope.ModalTarifarioCompra == null || $scope.ModalTarifarioCompra == '') {
                        $scope.MensajesError.push('No hay un tarifario de compra que cumpla las condiciones especificadas del transportador');
                        Continuar = false;
                    } else {
                        if ($scope.ModalTarifarioCompra.length == 0) {
                            $scope.MensajesError.push('Debe ingresar un tarifario de compra según condiciones para el transportador');
                            Continuar = false;
                        }
                    }
                }
            }


            if ($scope.ModalTarifarioVenta == undefined || $scope.ModalTarifarioVenta == null || $scope.ModalTarifarioVenta == '') {
                $scope.MensajesError.push('No hay un tarifario de venta que cumpla las condiciones especificadas del cliente');
                Continuar = false;
            } else {
                if ($scope.ModalTarifarioVenta.Codigo == 0) {
                    $scope.MensajesError.push('Debe ingresar un tarifario de venta según condiciones para el cliente');
                    Continuar = false;
                }
            }
            if ($scope.ModalRuta == undefined || $scope.ModalRuta == null || $scope.ModalRuta == '') {
                $scope.MensajesError.push('Debe Ingresar la Ruta');
                Continuar = false;
            }
            if ($scope.ModalProducto == undefined || $scope.ModalProducto == null || $scope.ModalProducto == '') {
                $scope.MensajesError.push('Debe Ingresar el producto');
                Continuar = false;
            }
            if ($scope.ModalCantidad == undefined || $scope.ModalCantidad == null || $scope.ModalCantidad == '' || $scope.ModalCantidad == 0) {
                $scope.MensajesError.push('Debe Ingresar la cantidad');
                Continuar = false;
            }
            if ($scope.ModalPeso == undefined || $scope.ModalPeso == null || $scope.ModalPeso == '' || $scope.ModalPeso == 0) {
                $scope.MensajesError.push('Debe Ingresar el peso');
                Continuar = false;
            }
            if ($scope.ModelCiudCarg == undefined || $scope.ModelCiudCarg == null || $scope.ModelCiudCarg == '') {
                $scope.MensajesError.push('Debe Ingresar la ciudad de cargue');
                Continuar = false;
            }
            if ($scope.ModelDireCarg == undefined || $scope.ModelDireCarg == null || $scope.ModelDireCarg == '') {
                $scope.MensajesError.push('Debe Ingresar la dirección de cargue');
                Continuar = false;
            }
            if ($scope.ModalTeleCarg == undefined || $scope.ModalTeleCarg == null || $scope.ModalTeleCarg == '') {
                $scope.MensajesError.push('Debe Ingresar teléfono de cargue');
                Continuar = false;
            }
            if ($scope.ModalContacCarg == undefined || $scope.ModalContacCarg == null || $scope.ModalContacCarg == '') {
                $scope.MensajesError.push('Debe Ingresar el contacto de cargue');
                Continuar = false;
            }
            if ($scope.ModelCiudDesc == undefined || $scope.ModelCiudDesc == null || $scope.ModelCiudDesc == '') {
                $scope.MensajesError.push('Debe Ingresar la ciudad de descargue');
                Continuar = false;
            }
            if ($scope.ModelDireDesc == undefined || $scope.ModelDireDesc == null || $scope.ModelDireDesc == '') {
                $scope.MensajesError.push('Debe Ingresar la dirección de descargue');
                Continuar = false;
            }
            if ($scope.ModalTeleDesc == undefined || $scope.ModalTeleDesc == null || $scope.ModalTeleDesc == '') {
                $scope.MensajesError.push('Debe Ingresar teléfono de descargue');
                Continuar = false;
            }
            if ($scope.ModalContacDesc == undefined || $scope.ModalContacDesc == null || $scope.ModalContacDesc == '') {
                $scope.MensajesError.push('Debe Ingresar el contacto de descargue');
                Continuar = false;
            }
            return Continuar;
        }

        // 1 - Orden de Cargue
        function DatosRequeridosOrdenCargue() {
            var Continuar = true;
            if (DatosRequeridosDocumentos() == false) {
                Continuar = false;
            }
            $scope.MensajesErrorOrdenCargue = [];
            if ($scope.ModalFechaOrdenCargue == undefined || $scope.ModalFechaOrdenCargue == null || $scope.ModalFechaOrdenCargue == '') {
                $scope.MensajesErrorOrdenCargue.push('Debe ingresar la fecha de la orden de cargue');
                Continuar = false;
            }
            window.scrollTo(top, top);
            return Continuar;
        }

        // 2 - Remesa
        function DatosRequeridosRemesa() {
            var Continuar = true;
            if (DatosRequeridosDocumentos() == false) {
                Continuar = false;
            }
            $scope.MensajesErrorRemesa = [];
            if ($scope.ModalFechaRemesa == undefined || $scope.ModalFechaRemesa == null || $scope.ModalFechaRemesa == '') {
                $scope.MensajesErrorRemesa.push('Debe ingresar la fecha de la remesa');
                Continuar = false;
            }
            if (($scope.ModalNumeroOrdenCargue == 0 || $scope.ModalNumeroOrdenCargue == undefined || $scope.ModalNumeroOrdenCargue == '' || $scope.ModalNumeroOrdenCargue == null) && $scope.Sesion.UsuarioAutenticado.ManejoRequiereOrdenCargueDespacho) {
                $scope.MensajesErrorRemesa.push('Debe generar la orden de cargue');
                Continuar = false;
            }

            if ($scope.Modal.DevolucionContenedor == true || $scope.Modal.DevolucionContenedor == 1) {
                if ($scope.Modal.CiudadDevolucion == undefined || $scope.Modal.CiudadDevolucion == null || $scope.Modal.CiudadDevolucion == '') {
                    $scope.MensajesErrorRemesa.push('Debe ingresar una Ciudad Devolución');
                    Continuar = false;
                }
                if ($scope.Modal.PatioDevolucion == undefined || $scope.Modal.PatioDevolucion == null || $scope.Modal.PatioDevolucion == '') {
                    $scope.MensajesErrorRemesa.push('Debe ingresar un Patio Devolución');
                    Continuar = false;
                }
                if ($scope.Modal.FechaDevolucionContenedor == undefined || $scope.Modal.FechaDevolucionContenedor == null || $scope.Modal.FechaDevolucionContenedor == '') {
                    $scope.MensajesErrorRemesa.push('Debe ingresar una Fecha Devolución');
                    Continuar = false;
                } else if (new Date($scope.Modal.FechaDevolucionContenedor) < new Date().setUTCHours(0, 0, 0, 0)) {
                    $scope.MensajesErrorRemesa.push('la fecha devolución no puede ser anterior a la fecha actual');
                    Continuar = false;
                }
            }
            //--Validar Origen y Destino Remesa Ruta
            var ConcuerdaOrigenDestino = 0;
            if ($scope.ModalCiudadRemitente.Codigo == $scope.ModalRuta.CiudadOrigen.Codigo || $scope.ModalCiudadRemitente.Codigo == $scope.ModalRuta.CiudadDestino.Codigo) {
                ConcuerdaOrigenDestino += 1;
            }
            if ($scope.ModalCiudadDestinatario.Codigo == $scope.ModalRuta.CiudadOrigen.Codigo || $scope.ModalCiudadDestinatario.Codigo == $scope.ModalRuta.CiudadDestino.Codigo) {
                ConcuerdaOrigenDestino += 1;
            }
            if (ConcuerdaOrigenDestino == 0) {
                $scope.MensajesErrorRemesa.push('La ciudad del remitente o destinatario debe coincidir con alguna ciudad de la ruta');
                Continuar = false;
            }
            //--Validar Origen y Destino Remesa Ruta
            window.scrollTo(top, top);
            return Continuar;
        }

        // 3 - Planilla Despacho
        function DatosRequeridosPlanillaDespacho() {
            var Continuar = true;
            if (DatosRequeridosDocumentos() == false) {
                Continuar = false;
            }
            $scope.MensajesErrorPlanilla = [];
            if ($scope.ModalNumeroRemesa == undefined || $scope.ModalNumeroRemesa == null || $scope.ModalNumeroRemesa == '' || $scope.ModalNumeroRemesa == 0) {
                $scope.MensajesErrorPlanilla.push('La Planilla de Despacho no se puede guardar porque no hay una remesa generada para asociar');
                Continuar = false;
            }

            if ($scope.ModalFechaSalida == undefined || $scope.ModalFechaSalida == null || $scope.ModalFechaSalida == '') {
                $scope.MensajesErrorPlanilla.push('Debe ingresar la fecha de salida del viaje en la planilla de despacho');
                Continuar = false;
            }

            if ($scope.ModalHoraSalida == undefined || $scope.ModalHoraSalida == null || $scope.ModalHoraSalida == '') {
                $scope.MensajesErrorPlanilla.push('Debe ingresar la hora de salida del viaje');
                Continuar = false;
            }

            if ($scope.TipoManifiesto == 8813) {

                if ($scope.CantidadViajesDia == undefined || $scope.CantidadViajesDia == null || $scope.CantidadViajesDia == '' || isNaN($scope.CantidadViajesDia)) {
                    $scope.MensajesErrorPlanilla.push('Debe ingresar la cantidad de viajes en el día');
                    Continuar = false;
                }

            }

            if ($scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores) {
                if ($scope.ListaTrayectos.length > 0) {
                    $scope.ListaTrayectos.forEach(item => {
                        if (item.Conductor == undefined || item.Conductor == null || item.Conductor == '') {
                            Continuar = false;
                            $scope.MensajesErrorPlanilla.push('Todos los trayectos deben tener un conductor');
                        }
                    })
                }
            }

            window.scrollTo(top, top);
            return Continuar;
        }

        //--Guardar Documentos
        $scope.GuardarDocumentos = function () {

            if ($scope.DocumentoGenerar == 1) {
                $scope.GuardarOrdenCargue();
            }
            if ($scope.DocumentoGenerar == 2) {
                $scope.GuardarRemesa();
            }
            if ($scope.DocumentoGenerar == 3) {
                $scope.GuardarPlanillaDespacho();
            }

        }

        $scope.GuardarOrdenCargue = function () {
            ValidacionUltimaInspeccionOrdenCargueRemesa = true
            closeModal('modalConfirmacionGuardarDocumentos', 1);

            if (DatosRequeridosOrdenCargue() == true) {
                $scope.ModalNumeroOrdenCargue = 0;

                $scope.DocumentoOrdenCargue = {

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    DesdeDespacharSolicitud: true,
                    IDDetalleSolicitud: $scope.CodigoDetalleSolicitud,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_ORDEN_CARGUE },

                    NumeroSolicitudServicio: $scope.Numero,
                    Fecha: $scope.ModeloFecha,
                    TerceroCliente: $scope.ModeloCliente,
                    TerceroRemitente: { Codigo: $scope.ModeloCliente.Codigo },

                    Vehiculo: $scope.ModalPlaca,
                    Semirremolque: { Codigo: $scope.ModalSemirremolque == undefined ? 0 : $scope.ModalSemirremolque.Codigo },
                    TerceroConductor: $scope.ModalConductor,
                    CiudadCargue: $scope.ModelCiudCarg,
                    DireccionCargue: $scope.ModelDireCarg,
                    TelefonosCargue: $scope.ModalTeleCarg,
                    ContactoCargue: $scope.ModalContacCarg,
                    Observaciones: $scope.ModalObservacionesOrdenCargue,
                    Ruta: $scope.ModalRuta,
                    Producto: $scope.ModalProducto,
                    CantidadCliente: MascaraDecimales($scope.ModalCantidad),
                    PesoCliente: $scope.ModalPeso,
                    ValorAnticipo: $scope.ModalValorAnticipo,
                    CiudadDescargue: $scope.ModelCiudDesc,
                    DireccionDescargue: $scope.ModelDireDesc,
                    TelefonoDescargue: $scope.ModalTeleDesc,
                    ContactoDescargue: $scope.ModalContacDesc,

                    Estado: ESTADO_ACTIVO,

                    CodigoUsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                    Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
                    Numeracion: '',
                    FechaCargue: $scope.ModelFechaCargue,
                    FechaDescargue: $scope.ModelFechaDescargue
                };

                if ($scope.ModelSitioCargue != undefined && $scope.ModelSitioCargue != null && $scope.ModelSitioCargue != '') {
                    $scope.DocumentoOrdenCargue.SitioCargue = $scope.ModelSitioCargue.SitioCliente;
                }

                if ($scope.ModelSitioDescargue != undefined && $scope.ModelSitioDescargue != null && $scope.ModelSitioDescargue != '') {
                    $scope.DocumentoOrdenCargue.SitioDescargue = $scope.ModelSitioDescargue.SitioCliente;
                }


                BloqueoPantalla.start('Guardando...');

                OrdenCargueFactory.Guardar($scope.DocumentoOrdenCargue).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            $scope.ModalNumeroOrdenCargue = response.data.Datos;

                            for (var i = 0; i < $scope.ListaOrdenesServicios.length; i++) {
                                if ($scope.ListaOrdenesServicios[i].Codigo == $scope.CodigoDetalleSolicitud) {
                                    $scope.ListaOrdenesServicios[i].NumeroOrdenCargue = response.data.Numero;
                                    $scope.ListaOrdenesServicios[i].NumeroDocumentoOrdenCargue = response.data.Datos;
                                    $scope.NumeroOrdenCargue = response.data.Numero;
                                    $scope.NumeroDocumentoOrdenCargue = response.data.Datos;
                                    $scope.DeshabilitarOrdenCargue = true;
                                    $scope.DeshabilitarDespacho = true;
                                }
                            }

                            ShowSuccess('Se guardó la Orden de Cargue No. ' + $scope.ModalNumeroOrdenCargue + ' correctamente.\n');
                            BloqueoPantalla.stop()
                        }
                        else if (response.data.Datos == -1) {
                            ShowWarning('', 'Ya se generó una orden de cargue para el despacho actual');
                        }
                        else if (response.data.Datos == -2) {
                            ShowWarning('', 'El vehículo ingresado tiene planillas pendientes por cumplir');
                        } else if (response.data.Datos == -3) {
                            ShowWarning('', 'No es posible generar documentos desde el despacho actual dado que el registro ya no existe debido a que los valores del despacho fuerón modificados desde la programación lo cual genero un nuevo detalle con los nuevos valores');
                        }
                        BloqueoPantalla.stop()
                    }

                });
            }

        }

        $scope.GuardarOrdenCargueSync = function () {
            closeModal('modalConfirmacionGuardarDocumentos', 1);

            if (DatosRequeridosOrdenCargue() == true) {
                $scope.ModalNumeroOrdenCargue = 0;

                $scope.DocumentoOrdenCargue = {

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    DesdeDespacharSolicitud: true,
                    IDDetalleSolicitud: $scope.CodigoDetalleSolicitud,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_ORDEN_CARGUE },

                    NumeroSolicitudServicio: $scope.Numero,
                    Fecha: $scope.ModeloFecha,
                    TerceroCliente: $scope.ModeloCliente,
                    TerceroRemitente: { Codigo: $scope.ModeloCliente.Codigo },

                    Vehiculo: $scope.ModalPlaca,
                    Semirremolque: { Codigo: $scope.ModalSemirremolque == undefined ? 0 : $scope.ModalSemirremolque.Codigo },
                    TerceroConductor: $scope.ModalConductor,
                    CiudadCargue: $scope.ModelCiudCarg,
                    DireccionCargue: $scope.ModelDireCarg,
                    TelefonosCargue: $scope.ModalTeleCarg,
                    ContactoCargue: $scope.ModalContacCarg,
                    Observaciones: $scope.ModalObservacionesOrdenCargue,
                    Ruta: $scope.ModalRuta,
                    Producto: $scope.ModalProducto,
                    CantidadCliente: $scope.ModalCantidad,
                    PesoCliente: $scope.ModalPeso,
                    ValorAnticipo: $scope.ModalValorAnticipo,
                    CiudadDescargue: $scope.ModelCiudDesc,
                    DireccionDescargue: $scope.ModelDireDesc,
                    TelefonoDescargue: $scope.ModalTeleDesc,
                    ContactoDescargue: $scope.ModalContacDesc,

                    Estado: ESTADO_ACTIVO,

                    CodigoUsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                    Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
                    Numeracion: '',
                    Sync: true
                }
                BloqueoPantalla.start('Guardando...');

                Response = OrdenCargueFactory.Guardar($scope.DocumentoOrdenCargue)

                if (Response.ProcesoExitoso === true) {
                    if (Response.Datos > 0) {
                        $scope.ModalNumeroOrdenCargue = Response.Datos;

                        for (var i = 0; i < $scope.ListaOrdenesServicios.length; i++) {
                            if ($scope.ListaOrdenesServicios[i].Codigo == $scope.CodigoDetalleSolicitud) {
                                $scope.ListaOrdenesServicios[i].NumeroOrdenCargue = Response.Numero;
                                $scope.ListaOrdenesServicios[i].NumeroDocumentoOrdenCargue = Response.Datos;
                                $scope.NumeroOrdenCargue = Response.Numero;
                                $scope.NumeroDocumentoOrdenCargue = Response.Datos;
                                $scope.DeshabilitarOrdenCargue = true;
                                $scope.DeshabilitarDespacho = true;
                            }
                        }

                        ShowSuccess('Se guardó la Orden de Cargue No. ' + $scope.ModalNumeroOrdenCargue + ' correctamente.\n');
                        BloqueoPantalla.stop()
                    }
                    else if (Response.Datos == -1) {
                        ShowWarning('', 'Ya se generó una orden de cargue para el despacho actual');
                    }
                    else if (Response.Datos == -2) {
                        ShowWarning('', 'El vehículo ingresado tiene planillas pendientes por cumplir');
                    }
                    else if (response.data.Datos == -3) {
                        ShowWarning('', 'No es posible generar documentos desde el despacho actual dado que el registro ya no existe debido a que los valores del despacho fuerón modificados desde la programación lo cual genero un nuevo detalle con los nuevos valores');
                    }
                    BloqueoPantalla.stop()
                }

            }

        }

        $scope.GuardarRemesa = function () {
            ValidacionUltimaInspeccionOrdenCargueRemesa = true
            closeModal('modalConfirmacionGuardarDocumentos', 1);

            if (DatosRequeridosRemesa() == true) {
                $scope.ModalNumeroRemesa = 0;

                if ($scope.NumeroOrdenCargue == undefined || $scope.NumeroOrdenCargue == null || $scope.NumeroOrdenCargue == '') {
                    $scope.NumeroOrdenCargue = 0;
                }

                $scope.NumeroTarifarioCompra = 0;
                if ($scope.ModalTarifarioCompra !== undefined && $scope.ModalTarifarioCompra !== null) {
                    if ($scope.ModalTarifarioCompra.length > 0) {
                        $scope.NumeroTarifarioCompra = $scope.ModalTarifarioCompra[0].NumeroTarifarioCompra;
                    }
                }

                $scope.DocumentoRemesa = {

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    DesdeDespacharSolicitud: true,
                    IDDetalleSolicitud: $scope.CodigoDetalleSolicitud,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                    NumeroSolicitudServicio: $scope.Numero,
                    TipoRemesa: { Codigo: $scope.CodigoTipoRemesa },

                    NumeroDocumentoCliente: $scope.ModalDocCliente,
                    FechaDocumentoCliente: new Date(),
                    Fecha: $scope.ModeloFecha,
                    Ruta: { Codigo: $scope.ModalRuta.Codigo },
                    ProductoTransportado: { Codigo: $scope.ModalProducto.Codigo },
                    PermisoInvias: $scope.ModalPermisoInvias,
                    FormaPago: { Codigo: OrdenServicio.FormaPago.Codigo },
                    Semirremolque: { Codigo: $scope.ModalSemirremolque == undefined ? 0 : $scope.ModalSemirremolque.Codigo },

                    Cliente: { Codigo: $scope.ModeloCliente.Codigo },
                    FacturarA: OrdenServicio.Facturar.Codigo,
                    Remitente: { Codigo: $scope.ModalRemitente.Codigo },
                    Destinatario: { Codigo: OrdenServicio.Destinatario.Codigo },
                    CiudadRemitente: { Codigo: $scope.ModalCiudadRemitente.Codigo },
                    DireccionRemitente: $scope.ModalDireccionRemitente,
                    TelefonoRemitente: $scope.ModalTelefonosRemitente,
                    Observaciones: $scope.ModalObservacionesRemesa,
                    CantidadCliente: MascaraNumero(MascaraValores($scope.ModalCantidad)),
                    PesoCliente: MascaraNumero(MascaraValores($scope.ModalPeso)),
                    PesoVolumetricoCliente: 0,
                    DetalleTarifaVenta: { Codigo: $scope.IDCodigo },

                    ValorCargue: $scope.ModalValorCargue,
                    ValorDescargue: $scope.ModalValorDescargue,

                    ValorFleteCliente: MascaraNumero(MascaraValores($scope.ModalFleteCliente)),
                    ValorManejoCliente: 0, // Pendiente -> Porcentaje manejo * Valor mercancia
                    ValorSeguroCliente: $scope.ModalValorManejo, // Pendiente -> Porcenatje Seguro * Valor Mercancia
                    ValorDescuentoCliente: 0,
                    TotalFleteCliente: Math.round(MascaraNumero(MascaraValores($scope.ModalTotalFleteCliente))),
                    ValorComercialCliente: 0,
                    CantidadTransportador: MascaraNumero(MascaraValores($scope.ModalCantidad)),
                    PesoTransportador: MascaraNumero(MascaraValores($scope.ModalPeso)),
                    ValorFleteTransportador: MascaraNumero(MascaraValores($scope.ModalFleteTransportador)),
                    TotalFleteTransportador: MascaraNumero(MascaraValores($scope.ModalTotalFleteTransportador)),
                    Destinatario: { Codigo: $scope.ModalDestinatario.Codigo },
                    CiudadDestinatario: { Codigo: $scope.ModalCiudadDestinatario.Codigo },
                    DireccionDestinatario: $scope.ModalDireccionDestinatario,
                    TelefonoDestinatario: $scope.ModalTelefonosDestinatario,
                    Estado: ESTADO_ACTIVO,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Oficina: { Codigo: $scope.ModeloOficinaDespacha.Codigo },
                    Numeracion: '',
                    TarifarioCompra: { Numero: $scope.NumeroTarifarioCompra },
                    TarifarioVenta: { Codigo: $scope.ModalTarifarioVenta.Codigo },
                    NumeroOrdenServicio: $scope.Numero,
                    Vehiculo: { Codigo: $scope.ModalPlaca.Codigo },
                    Conductor: { Codigo: $scope.ModalConductor.Codigo },
                    OrdenCargue: { Numero: $scope.NumeroOrdenCargue },
                    NumeroContenedor: $scope.ModalNumeroContenedor,
                    MBLContenedor: $scope.ModalMBLContenedor,
                    HBLContenedor: $scope.ModalHBLContenedor,
                    DOContenedor: $scope.ModalDOContenedor,
                    ValorFOB: $scope.ModalValorFOB,
                    CiudadDevolucion: $scope.Modal.CiudadDevolucion != null ? $scope.Modal.CiudadDevolucion : $scope.ModalImportacionCiudadDevolucion,
                    PatioDevolucion: $scope.Modal.PatioDevolucion != null ? $scope.Modal.PatioDevolucion.Nombre : $scope.ModalImportacionPatioDevolucion,
                    FechaDevolucion: $scope.ModalImportacionFechaDevolucion,
                    FechaDevolucionContenedor: $scope.Modal.FechaDevolucionContenedor,
                    SICDDevolucionContenedor: $scope.Modal.SICDDevolucionContenedor,
                    ListaPrecintos: $scope.ListaPrecintos,
                    RemesaCortesia: $scope.RemesaCortesia ? 1 : 0
                }
                if ($scope.ModalSemirremolque !== undefined && $scope.ModalSemirremolque !== null && $scope.ModalSemirremolque !== '') {
                    $scope.DocumentoRemesa.Semirremolque = { Codigo: $scope.ModalSemirremolque.Codigo }
                } else {
                    $scope.DocumentoRemesa.Semirremolque = { Codigo: 0 }
                }

                BloqueoPantalla.start('Guardando...');
                RemesasFactory.Guardar($scope.DocumentoRemesa).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            $scope.ModalNumeroRemesa = response.data.Datos;
                            //--Validacion Proceso 66
                            var detalleNovedades = [];
                            if ($scope.Sesion.UsuarioAutenticado.ManejoDespachoFormaDePago) {
                                for (var i = 0; i < ListadoNovedades.length; i++) {
                                    var existe = false;
                                    var novedad = {};
                                    for (var j = 0; j < OrdenServicio.Conceptos.length; j++) {
                                        if (OrdenServicio.Conceptos[j].ConceptoVentas.Codigo == ListadoNovedades[i].Conceptos.COVECodigo) {
                                            existe = true;
                                            novedad = {
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                NumeroPlanilla: 0,
                                                NumeroRemesa: response.data.Numero,
                                                Novedad: { Codigo: ListadoNovedades[i].Codigo },
                                                Observaciones: OrdenServicio.Conceptos[j].Descripcion,
                                                ValorCompra: 0,
                                                ValorVenta: OrdenServicio.Conceptos[j].Valor,
                                                ValorCosto: OrdenServicio.Conceptos[j].Valor,
                                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                                Sync: true
                                            };
                                            break;
                                        }
                                    }
                                    if (existe) {
                                        detalleNovedades.push(novedad);
                                    }
                                }
                                for (var i = 0; i < detalleNovedades.length; i++) {
                                    DetalleNovedadesDespachosFactory.Guardar(detalleNovedades[i]);
                                }
                            }
                            //--Validacion Proceso 66
                            for (var i = 0; i < $scope.ListaOrdenesServicios.length; i++) {
                                if ($scope.ListaOrdenesServicios[i].Codigo == $scope.CodigoDetalleSolicitud) {
                                    $scope.ListaOrdenesServicios[i].NumeroRemesa = response.data.Numero;
                                    $scope.NumeroRemesa = response.data.Numero;
                                    $scope.ListaOrdenesServicios[i].NumeroDocumentoRemesa = response.data.Datos;
                                    $scope.NumeroDocumentoRemesa = response.data.Datos;
                                    $scope.ModalNumeroConfirmacionMinisterio = response.data.Datos.NumeroConfirmacionMinisterioRemesa;
                                    $scope.DesabilitarConfirmacionRemesa = true;
                                    $scope.DeshabilitarRemesa = true;
                                    $scope.DeshabilitarDespacho = true;
                                    $scope.DeshabilitarTarifaTransportador = true;
                                    $scope.DeshabilitarTarifaCliente = true;
                                }
                            }
                            ShowSuccess('Se guardó la Remesa No. ' + $scope.ModalNumeroRemesa + ' correctamente.\n');
                        } else if (response.data.Datos == -1) {
                            ShowWarning('', 'Ya se generó una remesa para el despacho actual');
                        } else if (response.data.Datos == -2) {
                            ShowWarning('', 'El vehículo ingresado tiene planillas pendientes por cumplir');
                        } else if (response.data.Datos == -3) {
                            ShowWarning('', 'No es posible generar documentos desde el despacho actual dado que el registro ya no existe debido a que los valores del despacho fuerón modificados desde la programación lo cual genero un nuevo detalle con los nuevos valores');
                        } else if (response.data.Datos == -4) {
                            ShowWarning('', 'El peso que intenta ingresar sumado a lo pendiente por despachar excede el saldo de la orden de servicio, intente con otro valor');
                        }
                    }
                    BloqueoPantalla.stop()
                });

            }
        }

        $scope.ListaPrecintos = []

        $scope.AgregarPrecintos = function () {
            if ($scope.CantidadPrecintos > 0 && $scope.CantidadPrecintos !== undefined && $scope.CantidadPrecintos !== null) {
                RemesasFactory.ObtenerPrecintosAleatorios({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Oficina: { Codigo: $scope.ModeloOficinaDespacha.Codigo },
                    TipoPrecinto: { Codigo: $scope.ModeloTipoPrecinto.Codigo },
                    CantidadPrecintos: $scope.CantidadPrecintos
                }).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListaPrecintos.push(response.data.Datos[i])
                            }
                            if (response.data.Datos.length < parseInt($scope.CantidadPrecintos)) {
                                ShowWarning('', "La cantidad solicitada no se encuentra disponible, se cargarón " + response.data.Datos.length + " Precintos")
                            }
                        } else {
                            ShowWarning('', 'No se encontraron precintos para la oficina actual')
                        }
                    }
                });
            } else {
                ShowError("Por favor ingrese la cantidad de precintos")
            }
        }

        $scope.GuardarPlanillaDespacho = function () {
            ValidacionUltimaInspeccionOrdenCargueRemesa = true
            closeModal('modalConfirmacionGuardarDocumentos', 1);
            if (DatosRequeridosPlanillaDespacho() == true) {
                BloqueoPantalla.start('Guardando...');
                ImpuestosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, CodigoTipoDocumento: 150, CodigoCiudad: $scope.CiudadOrigenRuta.Codigo, AplicaTipoDocumento: 1 }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoImpuestos = []
                            $scope.ListadoImpuestosFiltrado = []
                            $scope.TotalImpuestos = 0
                            if (response.data.Datos.length > CERO) {
                                $scope.ListadoImpuestos = response.data.Datos;
                                $scope.ListadoImpuestosFiltrado = []
                                if ($scope.ListadoImpuestos.length > 0) {
                                    for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                                        var impuesto = {
                                            Nombre: $scope.ListadoImpuestos[i].Nombre,
                                            CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                                            ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                                            ValorBase: $scope.ListadoImpuestos[i].valor_base,
                                        }
                                        if ($scope.ListadoImpuestos[i].valor_base < parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))) {
                                            impuesto.ValorBase = parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))
                                        }
                                        impuesto.ValorImpuesto = impuesto.ValorTarifa * impuesto.ValorBase
                                        $scope.TotalImpuestos += impuesto.ValorImpuesto
                                        $scope.ListadoImpuestosFiltrado.push(impuesto)
                                    }
                                }
                            }
                            else {
                                $scope.ListadoImpuestos = [];
                                $scope.ListadoImpuestosFiltrado = [];
                            }
                            $scope.ModalNumeroPlanilla = 0;
                            $scope.DetallePlanilla = [];
                            var RemesaPlanillar = {
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Numero: $scope.NumeroPlanilla,
                                Remesa: { Numero: $scope.NumeroRemesa, NumeroDocumento: $scope.NumeroDocumentoRemesa }
                            }
                            $scope.DetallePlanilla.push(RemesaPlanillar);
                            var FechaAux = Formatear_Fecha_Mes_Dia_Ano($scope.ModalFechaSalida) + ' ' + $scope.ModalHoraSalida;
                            FechaAux = new Date(FechaAux);
                            var ResponseTipoDocumentoPlanilla = TipoDocumentosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Sync: true }).Datos
                            var ConductoresTrayectos = []
                            if ($scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores) {
                                if ($scope.ListaTrayectos.length > 0) {
                                    $scope.ListaTrayectos.forEach(item => {
                                        ConductoresTrayectos.push({
                                            Codigo: item.Conductor.Codigo,
                                            RutaTrayecto: { Codigo: item.RutaTrayecto.Codigo },
                                            ValorFlete: MascaraNumero(item.ValorFlete),
                                            ValorAnticipo: MascaraNumero(item.Anticipo),
                                            PorcentajeFlete: item.PorcentajeFlete,
                                            PorcentajeAnticipo: item.PorcentajeAnticipo,
                                            PorcentajeAnticipoRuta: item.PorcentajeAnticipoRuta,
                                            OrdenTrayecto: item.Orden,
                                            CuentaPorPagar: {
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                                                CodigoAlterno: '',
                                                Fecha: $scope.ModeloFecha,
                                                Tercero: { Codigo: item.Conductor.Codigo },
                                                AplicaPSL: ($scope.Sesion.UsuarioAutenticado.ManejoAnticiposPSL && $scope.FormaPagoRecaudo.Codigo == 2) ? 1 : 0,
                                                DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO },
                                                CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO,
                                                Numeracion: '',
                                                CuentaPuc: { Codigo: 0 },
                                                ValorTotal: MascaraNumero(item.Anticipo),
                                                Abono: 0,
                                                Saldo: MascaraNumero(item.Anticipo),
                                                FechaCancelacionPago: $scope.ModeloFecha,
                                                Aprobado: 1,
                                                UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                                FechaAprobo: $scope.ModeloFecha,
                                                Oficina: { Codigo: $scope.ModeloOficinaDespacha.Codigo },
                                                Vehiculo: { Codigo: $scope.ModalPlaca.Codigo },
                                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                            }
                                        })
                                    })
                                }
                            }
                            $scope.DocumentoPlanillaDespacho = {

                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                DesdeDespacharSolicitud: true,
                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                                IDDetalleSolicitud: $scope.CodigoDetalleSolicitud,
                                NumeroSolicitud: $scope.Numero,

                                Numero: 0,
                                Fecha: $scope.ModeloFecha,
                                FechaHoraSalida: FechaAux,
                                Ruta: { Codigo: $scope.ModalRuta.Codigo },
                                Vehiculo: {
                                    Codigo: $scope.ModalPlaca.Codigo,
                                    TipoDueno: { Codigo: $scope.ModalPlaca.TipoDueno.Codigo }
                                },
                                Tenedor: { Codigo: $scope.ModalTenedor.Codigo },

                                Conductor: { Codigo: $scope.ModalConductor.Codigo },
                                Semirremolque: { Codigo: $scope.ModalSemirremolque == undefined ? 0 : $scope.ModalSemirremolque.Codigo },

                                Cantidad: $scope.ModalCantidad,
                                Peso: $scope.ModalPeso,
                                ValorFleteTransportador: $scope.AutorizacionFlete > 0 ? parseInt(MascaraNumero($scope.ModalTotalFleteTransportadorTEMP)) : parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)),
                                strValorFleteTransportador: MascaraValores($scope.AutorizacionFlete > 0 ? parseInt(MascaraNumero($scope.ModalTotalFleteTransportadorTEMP)) : parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))),
                                ValorAnticipo: $scope.ModalAnticipoPlanilla,
                                AutorizacionAnticipo: $scope.AutorizacionAnticipo,
                                AutorizacionFlete: $scope.AutorizacionFlete,
                                ValorFleteAutorizacion: parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)),
                                strValorFleteAutorizacion: MascaraValores(parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))),
                                ValorImpuestos: 0,

                                ValorPagarTransportador: parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)) - $scope.TotalImpuestos - parseInt(MascaraNumero($scope.ModalAnticipoPlanilla)),
                                ValorFleteCliente: $scope.ModalFleteCliente,
                                ValorSeguroMercancia: 0,
                                ValorOtrosCobros: 0,
                                ValorTotalCredito: 0,
                                ValorTotalContado: 0,
                                ValorTotalAlcobro: 0,
                                ValorSeguroPoliza: ($scope.ModalSeguroAnticipo !== undefined && $scope.ModalSeguroAnticipo !== '' && $scope.ModalSeguroAnticipo !== null) ? parseInt(MascaraNumero($scope.ModalSeguroAnticipo)) : 0,

                                TarifaTransportes: { Codigo: $scope.ModalTarifaCompra.Codigo },
                                TipoTarifaTransportes: $scope.ModalTipoTarifaCompra,


                                LineaNegocioTransportes: { Codigo: $scope.CodigoLineaNegocio },
                                TipoLineaNegocioTransportes: { Codigo: $scope.ModeloTipoLineaNegocio.Codigo },
                                NumeroTarifarioCompra: $scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta == true ? 0 : $scope.ModalTarifarioCompra[0].Codigo,
                                ValorAuxiliares: 0,
                                Numeracion: '',

                                Observaciones: $scope.ModalObservacionesPlanilla,
                                Estado: { Codigo: ESTADO_ACTIVO },
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                Detalles: $scope.DetallePlanilla,
                                Oficina: { Codigo: $scope.ModeloOficinaDespacha.Codigo },
                                DetalleImpuesto: $scope.ListadoImpuestosFiltrado,
                                AnticipoPagadoA: $scope.Radio.Anticipo,
                                GeneraComprobanteContable: ResponseTipoDocumentoPlanilla.GeneraComprobanteContable,
                                ConductoresTrayectos: ConductoresTrayectos,
                                Conductores: $scope.ListaConductores
                            }
                            if ($scope.ListaAuxiliares.length > 0) {
                                $scope.DocumentoPlanillaDespacho.DetallesAuxiliares = []
                                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                                    if (!$scope.ListaAuxiliares[i].Modificarfuncionario) {
                                        $scope.DocumentoPlanillaDespacho.DetallesAuxiliares.push({
                                            Funcionario: $scope.ListaAuxiliares[i].Tercero
                                            , NumeroHorasTrabajadas: $scope.ListaAuxiliares[i].Horas
                                            , Valor: $scope.ListaAuxiliares[i].Valor
                                            , Observaciones: $scope.ListaAuxiliares[i].Observaciones
                                        });
                                    }
                                }
                            }
                            if (parseInt($scope.ModalAnticipoPlanilla) > CERO) {
                                if ($scope.ListaTrayectos.length == 0) {
                                    $scope.DocumentoPlanillaDespacho.CuentaPorPagar = {
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                                        CodigoAlterno: '',
                                        Fecha: $scope.ModeloFecha,
                                        Tercero: { Codigo: ($scope.Radio.Tenedor == 'Tenedor' ? $scope.CodigoTenedor : $scope.ModalConductor.Codigo) },
                                        AplicaPSL: ($scope.Sesion.UsuarioAutenticado.ManejoAnticiposPSL && $scope.FormaPagoRecaudo.Codigo == 2) ? 1 : 0,
                                        DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO },
                                        CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO,
                                        //Numero: $scope.NumeroPlanilla,

                                        Numeracion: '',
                                        //Observaciones: 'PAGO ANTICIPO PLANILLA No. ' + $scope.NumeroDocumentoPlanilla + ' VALOR $ ' + $scope.ModalAnticipoPlanilla,
                                        CuentaPuc: { Codigo: 0 },
                                        ValorTotal: $scope.ModalAnticipoPlanilla,
                                        Abono: 0,
                                        Saldo: $scope.ModalAnticipoPlanilla,
                                        FechaCancelacionPago: $scope.ModeloFecha,
                                        Aprobado: 1,
                                        UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                        FechaAprobo: $scope.ModeloFecha,
                                        Oficina: { Codigo: $scope.ModeloOficinaDespacha.Codigo },
                                        Vehiculo: { Codigo: $scope.ModalPlaca.Codigo },
                                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                    };
                                }
                            }

                            //--Valida Manifiesto Para Generacion
                            if ($scope.Sesion.Empresa.GeneraManifiesto === 1 && $scope.TipoRuta.Codigo == 4401) {
                                //JV: 2018-06-20: Se genera el manifiesto a partir de la planilla si el país de la empresa lo aplica
                                $scope.ModeloAceptacionElectronica = $scope.ModeloAceptacionElectronica == true ? 1 : 0;
                                $scope.DocumentoPlanillaDespacho.Manifiesto = {
                                    Numero: 0,
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    NumeroPlanilla: 0,
                                    Tipo_Documento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO },
                                    Usuario_Crea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                    Tipo_Manifiesto: { Codigo: $scope.TipoManifiesto.Codigo },
                                    NumeroSolicitudServicio: $scope.Numero,
                                    IdDetalleSolicitudServicio: $scope.CodigoDetalleSolicitud,
                                    Valor_Anticipo: $scope.ModalAnticipoPlanilla,
                                    CuentaPorPagar: {},
                                    AceptacionElectronica: $scope.ModeloAceptacionElectronica,
                                    CantidadViajesDia: $scope.CantidadViajesDia
                                };
                            }
                            //--Valida Manifiesto Para Generacion


                            PlanillaDespachosFactory.Guardar($scope.DocumentoPlanillaDespacho).then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos > 0) {
                                        $scope.ModalNumeroPlanilla = response.data.Numero;
                                        $scope.DeshabilitarPlanilla = true;
                                        for (var i = 0; i < $scope.ListaOrdenesServicios.length; i++) {
                                            if ($scope.ListaOrdenesServicios[i].Codigo == $scope.CodigoDetalleSolicitud) {
                                                $scope.ListaOrdenesServicios[i].NumeroPlanillaDespacho = response.data.Numero;
                                                $scope.ListaOrdenesServicios[i].NumeroDocumentoPlanillaDespacho = response.data.Datos;
                                                $scope.NumeroPlanilla = response.data.Numero;
                                                $scope.NumeroDocumentoPlanilla = response.data.Datos;
                                            }
                                        }
                                        ShowSuccess('Se guardó la Planilla de Despacho No. ' + $scope.ModalNumeroPlanilla + ' correctamente.');
                                    }
                                    else if (response.data.Datos == -1) {
                                        ShowWarning('', 'Ya se generó una planilla para el despacho actual');
                                    } else if (response.data.Datos == -2) {
                                        ShowWarning('', 'Ya se generó una planilla para el despacho actual');
                                    } else if (response.data.Datos == -3) {
                                        ShowWarning('', 'El vehículo ingresado tiene planillas pendientes por cumplir');
                                    }
                                    else if (response.data.Datos == -4) {
                                        ShowWarning('', 'No es posible generar documentos desde el despacho actual dado que el registro ya no existe debido a que los valores del despacho fuerón modificados desde la programación lo cual genero un nuevo detalle con los nuevos valores');
                                    }
                                    else {
                                        ShowWarning('', 'Ya se generó una planilla para el despacho actual');
                                    }
                                    BloqueoPantalla.stop()
                                }
                            });
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        BloqueoPantalla.stop()
                    });



            }
        }
        //---------------------------------------------------------------//

        //------------------------------------- MASRCARAS -----------------------------------------//
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };

        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        };

        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope);
        }

        $scope.MaskNumeroGrid = function (item) {
            if (item !== undefined) {
                return MascaraNumero(item.toString())
            }
            else {
                return item
            }
        };

        $scope.MaskValoresGrid = function (item) {
            if (item !== undefined) {
                return MascaraValores(item.toString())
            }
            else {
                return item
            }
        };
        /*-----Asignaciones ----------------------------------------------------------- */

        $scope.AsignarValorEscolta = function (item) {
            if (item !== true) {
                $scope.ModalValorEscolta = 0;
                $scope.DeshabilitarValorEscolta = true;
            } else {
                $scope.ModalValorEscolta = $scope.ValorEscolta;
                $scope.DeshabilitarValorEscolta = false;
            }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ValidarVehiculoDocumentos = function (Vehiculo, opt) {
            if (Vehiculo != undefined && Vehiculo != null && Vehiculo != '') {
                var ResponsListaNegra = VehiculosFactory.ConsultarEstadoListaNegra({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: Vehiculo.Codigo,
                    Sync: true
                });
                if (ResponsListaNegra.ProcesoExitoso == false) {

                    if (VehiculosFactory.ConsultarDocumentosSoatRTM(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: Vehiculo.Codigo,
                            CodigoDetalleEpos: $scope.IdDetale,//IdDetale
                            Sync: true
                        }).ProcesoExitoso
                    ) {
                        $scope.ModalPlaca = Vehiculo
                        $scope.ModalCodigoVehiculo = Vehiculo.CodigoAlterno
                        $scope.AsignarDatosVehiculo(Vehiculo)

                        var vehiculoalterno = $scope.CargarVehiculos(Vehiculo.Codigo)
                        if (vehiculoalterno.Estado.Codigo == 0) {
                            ShowError("El vehículo " + vehiculoalterno.Placa + " esta inactivo por motivo '" + vehiculoalterno.JustificacionBloqueo + "'")
                            $scope.ModalConductor = '';
                            $scope.ModalTenedor = '';
                            $scope.ModalSemirremolque = '';
                            $scope.ModalPlaca = '';
                            ValidarAnticipoPlanillaDesdeFletesCompra = false
                            return false
                        } else {
                            var TerceroAlterno = $scope.CargarTercero(vehiculoalterno.Conductor.Codigo)
                            if (TerceroAlterno.Estado.Codigo == 0) {
                                ShowError("El conductor del vehículo " + vehiculoalterno.Placa + " de nombre '" + TerceroAlterno.NombreCompleto + "' esta inactivo por motivo '" + TerceroAlterno.JustificacionBloqueo + "'")
                                $scope.ModalConductor = '';
                                $scope.ModalTenedor = '';
                                $scope.ModalSemirremolque = '';
                                $scope.ModalPlaca = '';
                                ValidarAnticipoPlanillaDesdeFletesCompra = false
                                return false
                            } else {
                                if ($scope.ModalTipoTarifaVenta != undefined & $scope.ModalTipoTarifaVenta != '') {
                                    if (Vehiculo.TipoVehiculo.Nombre !== $scope.ModalTipoTarifaVenta) {
                                        ShowError('El tipo vehículo del vehículo ingresado no corresponde al asignado en la orden de servicio')
                                        $scope.ModalConductor = '';
                                        $scope.ModalTenedor = '';
                                        $scope.ModalSemirremolque = '';
                                        $scope.ModalPlaca = '';
                                        ValidarAnticipoPlanillaDesdeFletesCompra = false
                                        return false
                                    } else {
                                        ValidarAnticipoPlanillaDesdeFletesCompra = true
                                        if ($scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores && ValidarAnticipoPlanillaDesdeFletesCompra && ValidacionUltimaInspeccionOrdenCargueRemesa == false && ValidarDesdeVehiculo == true) {
                                            $scope.ValidarAnticipoPlanilla($scope.ModalAnticipoPlanilla)
                                        }
                                        return true
                                    }
                                } else {
                                    ValidarAnticipoPlanillaDesdeFletesCompra = true
                                    if ($scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores && ValidarAnticipoPlanillaDesdeFletesCompra && ValidacionUltimaInspeccionOrdenCargueRemesa == false && ValidarDesdeVehiculo == true) {
                                        $scope.ValidarAnticipoPlanilla($scope.ModalAnticipoPlanilla)
                                    }
                                    return true
                                }
                            }
                        }
                    }
                    else {
                        if (opt > 0) {
                            var ResponseDocuPendientes = VehiculosFactory.ConsultarListaDocumentosPendientes({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Codigo: Vehiculo.Codigo,
                                CodigoDetalleEpos: $scope.IdDetale,//IdDetale
                                Sync: true
                            }

                            ).Datos;
                            $scope.ListadoDocumentosVencidos = ResponseDocuPendientes;
                            if ($scope.ListadoDocumentosVencidos.length > 0) {
                                $scope.MensajeModalDocumentosProximosVencer = 'el Vehículo ' + $scope.ModalPlaca.Placa + ' tiene los siguientes documentos próximos a vencerse: ';
                            } else {
                                $scope.MensajeModalDocumentosProximosVencer = 'el Vehículo ' + $scope.ModalPlaca.Placa + ' no tiene documentos parametrizados';
                            }
                            //ShowError("El vehículo tiene documentos proximos a vencerse" + ResponseDocuPendientes.toString());
                            showModal('modalDocumentosVencidos');
                        }
                        $scope.ModalConductor = '';
                        $scope.ModalTenedor = '';
                        $scope.ModalSemirremolque = '';
                        $scope.ModalPlaca = '';
                        ValidarAnticipoPlanillaDesdeFletesCompra = false
                        return false
                    }
                }
                else {
                    if (opt > 0) {
                        ShowError("El vehículo se encuentra mal matriculado ante el RNDC")
                    }
                    $scope.ModalConductor = '';
                    $scope.ModalTenedor = '';
                    $scope.ModalSemirremolque = '';
                    $scope.ModalPlaca = '';
                    ValidarAnticipoPlanillaDesdeFletesCompra = false
                    return false
                }
            }
        }

        $scope.AsignarListaplaca = function ($viewValue) {
            return RetornarListaAutocomplete($viewValue, $scope.ListaPlaca, 'Placa', 'CodigoAlterno');
        }

        $scope.AsignarDatosVehiculo = function (Vehiculo) {
            TipoDuenoVehiculo = Vehiculo.TipoDueno.Codigo;
            $scope.TipoDuenoVehiculo = Vehiculo.TipoDueno.Codigo;
            $scope.ModalConductor = $scope.CargarTercero(Vehiculo.Conductor.Codigo);
            $scope.ModalTenedor = $scope.CargarTercero(Vehiculo.Tenedor.Codigo);
            if (Vehiculo.Semirremolque !== undefined) {
                if ($scope.ModalSemirremolque == undefined && !$scope.NoAsignarSemirremolque) {
                    if (Vehiculo.Semirremolque.Codigo > 0) {
                        try {
                            $scope.ModalSemirremolque = SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true, Codigo: Vehiculo.Semirremolque.Codigo }).Datos[0] // $linq.Enumerable().From($scope.ListaPlacaSemi).First('$.Codigo ==' + Vehiculo.Semirremolque.Codigo);
                        } catch (e) {
                        }
                    } else if (Vehiculo.Semirremolque.Placa !== undefined && Vehiculo.Semirremolque.Placa !== '' && Vehiculo.Semirremolque.Placa !== null) {
                        try {
                            $scope.ModalSemirremolque = SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true, Placa: Vehiculo.Semirremolque.Placa }).Datos[0]  //$linq.Enumerable().From($scope.ListaPlacaSemi).First('$.Placa == "' + Vehiculo.Semirremolque.Placa + '"');
                        } catch (e) {
                        }
                    }
                }
            }


            if ($scope.CodigoLineaNegocio != 2) {
                $scope.AplicAnticipoTenedor = true
                //$scope.Radio = { Anticipo: 'Tenedor' }
            }
            try {
                var FiltroTarifa = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModalTenedor.Codigo,
                    Sync: true
                }
                var response = TercerosFactory.Obtener(FiltroTarifa)
                if (response.ProcesoExitoso === true) {
                    if (response.Datos.Codigo > 0) {
                        if (response.Datos.TipoAnticipo.Codigo == 18702) {
                            $scope.AplicAnticipoTenedor = true
                            $scope.Radio = { Anticipo: 'Tenedor' }

                        } else {
                            $scope.AplicAnticipoTenedor = false
                            $scope.Radio = { Anticipo: 'Conductor' }
                        }
                    }
                }
                if (!$scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta) {
                    $scope.ConsultarTarifarioCompra($scope.ModalTenedor.Codigo);
                }
            } catch (e) {

            }
            if (!$scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta) {
                $scope.CambiarSeleccionTarifaCompra($scope.ModalTipoTarifaCompra);
                $scope.GestionarFletesCompra();
            }
        };

        $scope.ListaSemirremolque = []

        $scope.AsignarListasemirremolque = function (value) {

            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SemirremolquesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListaSemirremolque = ValidarListadoAutocomplete(Response.Datos, $scope.ListaSemirremolque)
                }
            }
            return $scope.ListaSemirremolque
        }

        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($scope.NumeroProgramacion > 0 && $scope.IdDetale > 0) {//IdDetale
                document.location.href = '#!ConsultarProcesoProgramarOrdenServicio/' + $scope.IdDetale;//IdDetale
            }
            else if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarDespacharOrdenServicios/' + $routeParams.Numero;
            } else {
                document.location.href = '#!ConsultarDespacharOrdenServicios';
            }
        };

        /* Obtener parametros del enrutador*/
        //Parametros
        //'/GestionarDespacharOrdenServicios/:Numero/:Codigo'
        ///GestionarDespacharOrdenServicios/:Numero/:Codigo/:Programacion/:IdDetalle
        //Parametros Orden------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        if ($routeParams.Numero !== undefined) {
            $scope.Numero = $routeParams.Numero;
        }
        else {
            $scope.Numero = 0;
        }
        //Parametros programación-----------------------------------------------------------------------------------------------------------------------------------------------------------------
        if ($routeParams.Codigo !== undefined) {
            $scope.IDCodigo = parseInt($routeParams.Codigo);
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        if ($scope.Numero > 0) {
            $scope.Titulo = 'DESPACHAR ORDEN SERVICIO';
            $scope.Deshabilitar = true;
            ObtenerOrdenServicio();
        }


    }]);