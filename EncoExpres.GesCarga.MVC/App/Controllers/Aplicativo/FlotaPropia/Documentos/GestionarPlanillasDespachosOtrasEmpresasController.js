﻿EncoExpresApp.controller("GestionarPlanillasDespachosOtrasEmpresasCtrl", ['$scope', '$linq', 'TarifaTransportesFactory', 'RutasFactory', 'blockUIConfig', 'VehiculosFactory', 'TercerosFactory', 'ProductoTransportadosFactory', 'PlanillasDespachosOtrasEmpresasFactory', '$routeParams','SemirremolquesFactory','blockUI','$timeout', 
    function ($scope, $linq, TarifaTransportesFactory, RutasFactory, blockUIConfig, VehiculosFactory, TercerosFactory, ProductoTransportadosFactory, PlanillasDespachosOtrasEmpresasFactory, $routeParams, SemirremolquesFactory, blockUI, $timeout) {

        // var urlService = GetUrl('PlanillasDespachosOtrasEmpresas');
        $scope.MapaSitio = [{ Nombre: 'Flota Propia' }, { Nombre: 'Documentos' }, { Nombre: 'Planillas Despachos Otras Empresas' }, { Nombre: 'Gestionar' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.MostrarMensajeError = false;
        $scope.NumeroAnular;
        $scope.NumeroDocumentoAnular;
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo==' + OPCION_MENU_PLANILLA_DESPACHOS_OTRAS_EMPRESAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

       
            $scope.ListaEstados = [

                { Nombre: 'DEFINITIVO', Codigo: 1 },
                { Nombre: 'BORRADOR', Codigo: 0 }

            ];
        
       

        //Lista Tipos Tarifa:

        $scope.ListadoTarifaTransportes = [];
        $scope.ListadoTarifaTransportes.push({ Nombre: "Seleccione...", Codigo: -1 });

        TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, OPOtrasEmpresas: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    //$scope.ListadoTarifaTransportes = [];
                    //$scope.ListadoTarifaTransportes.push({Nombre: "Seleccione", Codigo : -1});
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoTarifaTransportes.push(response.data.Datos[i]);
                        }
                        
                        //$scope.ListadoTarifaTransportes = response.data.Datos;
                        console.log($scope.ListadoTarifaTransportes);
                    }
                    else {
                        $scope.ListadoTarifaTransportes = []
                    }
                }
            }, function (response) {
            });


        //Listado Rutas:

        $scope.ListadoRutas = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos;

        //Lista Conductores:

        $scope.ListaConductores = [];
       
        $scope.Planilla = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            Numero: 0,
            Consecutivo: 0,
            TipoDocumento: 320,
            Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
            NumeroDocumento: '',
            Fecha: new Date(),
            Deshabilitar: '',
            NumeroDocumentoTransporte: 0,
            Cliente: '',
            DocumentoCliente: 0,
            Ruta: { Nombre: '', Codigo: 0 },
            Vehiculo: { Placa: '', Codigo: 0 },
            Semirremolque: { Placa: '', Codigo: 0 },
            Conductor:'',
            Producto: { Nombre: '', Codigo: 0 },
            Cantidad: '',
            Peso: '',
            TipoTarifa: '',
            FleteTransportador: 0,
            TotalFlete: 0,
            RetencionFuente: 0,
            RetencionICA: 0,
            AnticipoCliente: 0,
            ValorAPagar: 0,
            AnticipoPropio: 0,
            Observaciones: '',
            TarifarioCompra: 0,
            Estado: 0,
            Factura: 0
        };

        $scope.Planilla.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo == 0');
        $scope.Planilla.TipoTarifa = $linq.Enumerable().From($scope.ListadoTarifaTransportes).First('$.Codigo == -1');
        console.log("tipoTarifa", $scope.Planilla.TipoTarifa.Codigo);

        //Obtener Semirremolque al rellenar el campo vehículo:
        $scope.CargarConductorYSemirremolque = function () {
            
            $scope.Planilla.Semirremolque = $scope.Planilla.Vehiculo.Semirremolque;
            $scope.Planilla.Conductor = $scope.Planilla.Vehiculo.Conductor;
            console.log("Vehiculo:", $scope.Planilla.Conductor);
        }


        $scope.ObtenerInformacionTenedor = function (vehiculo) {

            if ($scope.Planilla.Vehiculo.Tenedor !== undefined) {
                if ($scope.Planilla.Vehiculo.Tenedor.Codigo > 0) {
                    $scope.Planilla.Vehiculo.Tenedor.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                    TercerosFactory.Obtener($scope.Planilla.Vehiculo.Tenedor).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.Codigo > 0) {
                                    if (response.data.Datos.Proveedor != undefined) {
                                        if (response.data.Datos.Proveedor.Tarifario !== undefined) {
                                            if (response.data.Datos.Proveedor.Tarifario.Codigo > 0) {
                                                $scope.Planilla.TarifarioCompra = response.data.Datos.Proveedor.Tarifario;
                                                $scope.ObtenerInformacionTarifario();
                                            }
                                        }
                                    }
                                }
                            }
                        });
                }
            }
            if (vehiculo !== undefined && vehiculo !== null && vehiculo !== '') {
                $scope.ListaConductores.push($scope.CargarTercero(vehiculo.Conductor.Codigo));

            }
        }


        $scope.ObtenerInformacionTarifario = function () {

        }





        //Filtros:

        $scope.FiltrarTarifas = function (Ruta) {
            if (Ruta.Codigo > 0 && Ruta !== undefined && Ruta !== null && Ruta !== "") {
                ListaTarifas = [];
                console.log(Ruta);
                /*for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                    console.log($scope.ListadoTarifas[i]);
                }*/
            }

        }


        //Calcular Valor a Pagar:

        $scope.CalcularPagar = function () {
            var totalFlete = 0;
            var retefuente = 0;
            var reteICA = 0;
            var acliente = 0;
            if ($scope.Planilla.TotalFlete) {
                totalFlete = $scope.Planilla.TotalFlete.replace(/,/g, "");
            }
            if ($scope.Planilla.RetencionFuente) {
                retefuente = $scope.Planilla.RetencionFuente.replace(/,/g, "");
            }
            if ($scope.Planilla.RetencionICA) {
                reteICA = $scope.Planilla.RetencionICA.replace(/,/g, "");
            }
            if ($scope.Planilla.AnticipoCliente) {
                acliente = $scope.Planilla.AnticipoCliente.replace(/,/g, "");
            }

            $scope.Planilla.ValorAPagar = totalFlete - retefuente - reteICA - acliente;
            $scope.Planilla.ValorAPagar = MascaraValores($scope.Planilla.ValorAPagar);
            console.log(retefuente);
            console.log($routeParams);

        }

        //Botones:

        //Cancelar:

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarPlanillasDespachosOtrasEmpresas/' + $scope.NumeroDocumento;
        };

        //Guardar:

        $scope.ConfirmacionGuardar = function () {
         
            if ($scope.DeshabilitarActualizar) {
                location.href = '#!ConsultarPlanillasDespachos/' + $scope.Modelo.Planilla.Numero;
            } else {
                showModal('modalConfirmacionGuardar');
            }
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                var FTrans = parseFloat($scope.Planilla.FleteTransportador.replace(/,/g, ""));
                var TFlete = parseFloat($scope.Planilla.TotalFlete.replace(/,/g, ""));
                var RFuente = parseFloat($scope.Planilla.RetencionFuente.replace(/,/g, ""));
                var rICA = parseFloat($scope.Planilla.RetencionICA.replace(/,/g, ""));
                var ACliente = parseFloat($scope.Planilla.AnticipoCliente.replace(/,/g, ""));
                var VPagar = parseFloat($scope.Planilla.ValorAPagar.replace(/,/g, ""));
                var APropio = parseFloat($scope.Planilla.AnticipoPropio.replace(/,/g, ""));

               
            var planilla = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Planilla.Numero,
                Fecha: $scope.Planilla.Fecha,
                TipoDocumento: $scope.Planilla.TipoDocumento,
                Oficina : $scope.Planilla.Oficina,
                NumeroDocumento : $scope.Planilla.NumeroDocumento,
                NumeroDocumentoTransporte: $scope.Planilla.NumeroDocumentoTransporte,
                Cliente: { Codigo: $scope.Planilla.Cliente.Codigo },
                DocumentoCliente: $scope.Planilla.DocumentoCliente,
                Ruta: { Codigo: $scope.Planilla.Ruta.Codigo },
                Vehiculo: { Codigo: $scope.Planilla.Vehiculo.Codigo },
                Semirremolque: { Codigo: $scope.Planilla.Semirremolque.Codigo },
                Conductor: { Codigo: $scope.Planilla.Conductor.Codigo },
                Producto: { Codigo: $scope.Planilla.Producto.Codigo },
                Cantidad: $scope.Planilla.Cantidad,
                Peso: $scope.Planilla.Peso,
                TipoTarifa: { Codigo: $scope.Planilla.TipoTarifa.Codigo },
                FleteTransportador: FTrans,
                TotalFlete: TFlete,
                RetencionFuente: RFuente,
                RetencionICA: rICA,
                AnticipoCliente: ACliente,
                ValorPagar: VPagar,
                AnticipoPropio: APropio,
                Observaciones: $scope.Planilla.Observaciones,
                Estado: $scope.Planilla.Estado.Codigo,
                CodigoUsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }

            }
            console.log(planilla);
            PlanillasDespachosOtrasEmpresasFactory.Guardar(planilla)
                .then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > CERO) {
                            if ($scope.Planilla.Numero == CERO) {
                                ShowSuccess('Se guardó la planilla de despachos con el número ' + response.data.Datos + '');
                            } else {
                                ShowSuccess('Se modificó la planilla de despachos con el número ' + response.data.Datos + '');
                            }
                             location.href = '#!ConsultarPlanillasDespachosOtrasEmpresas/' + response.data.Datos;
                        } else {
                            ShowError('no hay datos de retorno');
                        }
                    } else {
                        ShowError('funciona');
                        console.log(response);
                    }
                }, function (response) {

                    ShowError(response.statusText);

                });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var FTrans;
            var ACliente;
            var APropio;
            var VPaga;
            var TFlete;
            var continuar = true;
            if ($scope.Planilla.ValorAPagar == null || $scope.Planilla.ValorAPagar == 0 || $scope.Planilla.ValorAPagar == '0' || $scope.Planilla.ValorAPagar == '') {
                $scope.MensajesError.push('Debe ingresar un valor a pagar');
                continuar = false;
            } else {
                VPaga = parseFloat($scope.Planilla.ValorAPagar.replace(/,/g, ""));
            }
            //if ($scope.Planilla.AnticipoPropio == null || $scope.Planilla.AnticipoPropio == 0 || $scope.Planilla.AnticipoPropio == '0' || $scope.Planilla.AnticipoPropio == '' || $scope.Planilla.AnticipoPropio == undefined) {
            if ($scope.Planilla.AnticipoPropio == null || $scope.Planilla.AnticipoPropio == '' || $scope.Planilla.AnticipoPropio == undefined) {
                //$scope.MensajesError.push('Debe ingresar un valor de anticipo propio');
                //                continuar = false;
            } else {
                APropio = parseFloat($scope.Planilla.AnticipoPropio.replace(/,/g, ""));
            }
            //if ($scope.Planilla.FleteTransportador == null || $scope.Planilla.FleteTransportador == 0 || $scope.Planilla.FleteTransportador == '0' || $scope.Planilla.FleteTransportador == '' || $scope.Planilla.FleteTransportador == undefined) {
            if ($scope.Planilla.FleteTransportador == null || $scope.Planilla.FleteTransportador == '' || $scope.Planilla.FleteTransportador == undefined) {
                $scope.MensajesError.push('Debe ingresar un valor para flete transportador');
                continuar = false;
            } else {
                FTrans = parseFloat($scope.Planilla.FleteTransportador.replace(/,/g, ""));
            }

            if ($scope.Planilla.AnticipoCliente == null || $scope.Planilla.AnticipoCliente == 0 || $scope.Planilla.AnticipoCliente == '0' || $scope.Planilla.AnticipoCliente == '' || $scope.Planilla.AnticipoCliente == undefined) {
                //$scope.MensajesError.push('Debe ingresar un valor de anticipo cliente');
                //continuar = false;
            } else {
                ACliente = parseFloat($scope.Planilla.AnticipoCliente.replace(/,/g, ""));
            }

            if ($scope.Planilla.TotalFlete == null || $scope.Planilla.TotalFlete == 0 || $scope.Planilla.TotalFlete == '0' || $scope.Planilla.TotalFlete == '' || $scope.Planilla.TotalFlete == undefined) {
                $scope.MensajesError.push('Debe ingresar un valor total flete');
                continuar = false;
            } else {
                TFlete = parseFloat($scope.Planilla.TotalFlete.replace(/,/g, ""));

            }

            
            if ($scope.Planilla.NumeroDocumento != 0) {
                if ($scope.Planilla.NumeroDocumento == undefined || $scope.Planilla.NumeroDocumento == '' || $scope.Planilla.NumeroDocumento == null || isNaN($scope.Planilla.NumeroDocumento)) {
                    $scope.MensajesError.push('Debe ingresar un numero de documento');

                    continuar = false;
                }
            }

            if ($scope.Planilla.NumeroDocumento == 0) {
                $scope.MensajesError.push('El numero de documento no debe ser 0');

                continuar = false;
            }

            if ($scope.Planilla.Fecha == undefined || $scope.Planilla.Fecha == '' || $scope.Planilla.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar una fecha');
                continuar = false;
            }
            if ($scope.Planilla.NumeroDocumentoTransporte == undefined || $scope.Planilla.NumeroDocumentoTransporte == '' || $scope.Planilla.NumeroDocumentoTransporte == null) {
                $scope.MensajesError.push('Debe ingresar un número documento transporte');
                continuar = false;
            }
            if ($scope.Planilla.Cliente == undefined || $scope.Planilla.Cliente == '' || $scope.Planilla.Cliente == null) {
                $scope.MensajesError.push('Debe ingresar un cliente');
                continuar = false;
            }
            if ($scope.Planilla.DocumentoCliente == undefined || $scope.Planilla.DocumentoCliente == '' || $scope.Planilla.DocumentoCliente == null) {
                $scope.MensajesError.push('Debe ingresar un número documento cliente');
                continuar = false;
            }
            if ($scope.Planilla.Vehiculo == undefined || $scope.Planilla.Vehiculo == '' || $scope.Planilla.Vehiculo == null) {
                $scope.MensajesError.push('Debe ingresar un vehículo');
                continuar = false;
            }
            if ($scope.Planilla.Ruta == undefined || $scope.Planilla.Ruta == '' || $scope.Planilla.Ruta == null) {
                $scope.MensajesError.push('Debe ingresar una ruta');
                continuar = false;
            }
           
            if ($scope.Planilla.Conductor == undefined || $scope.Planilla.Conductor == '' || $scope.Planilla.Conductor == null) {
                $scope.MensajesError.push('Debe ingresar un conductor');
                continuar = false;
            }
            if ($scope.Planilla.Producto == undefined || $scope.Planilla.Producto == '' || $scope.Planilla.Producto == null) {
                $scope.MensajesError.push('Debe ingresar un producto');
                continuar = false;
            }
            if ($scope.Planilla.Cantidad == undefined || $scope.Planilla.Cantidad == '' || $scope.Planilla.Cantidad == null) {
                $scope.MensajesError.push('Debe ingresar una cantidad');
                continuar = false;
            }
            if ($scope.Planilla.Peso == undefined || $scope.Planilla.Peso == '' || $scope.Planilla.Peso == null) {
                $scope.MensajesError.push('Debe ingresar un peso');
                continuar = false;
            }
            if ($scope.Planilla.TipoTarifa == undefined || $scope.Planilla.TipoTarifa == '' || $scope.Planilla.TipoTarifa == null || $scope.Planilla.TipoTarifa == -1) {
                $scope.MensajesError.push('Debe seleccionar un tipo de tarifa');
                continuar = false;
            }
            /*if ($scope.Planilla.FleteTransportador == undefined || $scope.Planilla.FleteTransportador == '' || $scope.Planilla.FleteTransportador == null) {
                $scope.MensajesError.push('Debe ingresar un valor para flete transportador');
                continuar = false;
            }*/
           /* if ($scope.Planilla.TotalFlete == undefined || $scope.Planilla.TotalFlete == '' || $scope.Planilla.TotalFlete == null) {
                $scope.MensajesError.push('Debe ingresar un valor total flete');
                continuar = false;
            }*/
            if ($scope.Planilla.RetencionFuente == undefined || $scope.Planilla.RetencionFuente == '' || $scope.Planilla.RetencionFuente == null) {
                $scope.MensajesError.push('Debe ingresar un valor de retención en la fuente');
                continuar = false;
            }
            if ($scope.Planilla.RetencionICA == undefined || $scope.Planilla.RetencionICA == '' || $scope.Planilla.RetencionICA == null) {
                $scope.MensajesError.push('Debe ingresar un valor de retención ICA');
                continuar = false;
            }
            /*if ($scope.Planilla.AnticipoCliente == undefined || $scope.Planilla.AnticipoCliente == '' || $scope.Planilla.AnticipoCliente == null) {
                $scope.MensajesError.push('Debe ingresar un valor de anticipo cliente');
                continuar = false;
            }*/
            if (ACliente > TFlete ) {
                ShowError('El valor del anticipo cliente no puede ser mayor al valor total flete');
                continuar = false;
            }

           /* if ($scope.Planilla.AnticipoPropio == undefined || $scope.Planilla.AnticipoPropio == '' || $scope.Planilla.AnticipoPropio == null) {
                $scope.MensajesError.push('Debe ingresar un valor de anticipo propio');
                continuar = false;
            }*/

            if (APropio > VPaga) {
                ShowError('El valor del anticipo propio no puede ser mayor al valor a pagar');
                continuar = false;
            }
            return continuar
        }

        //Obtener Datos:
        function Obtener() {
            blockUI.start('Cargando Planilla Despachos...');

            $timeout(function () {
                blockUI.message('Cargando Planilla Despachos...');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Planilla.Numero,
            };

            blockUI.delay = 1000;
            PlanillasDespachosOtrasEmpresasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Anulado == 1 || response.data.Datos.Estado == 1) {
                            $scope.DeshabilitarActualizar = true;
                        }
                        console.log(response.data.Datos);
                        $scope.Planilla.Conductor = response.data.Datos.Conductor;
                        console.log("Conductor: ",$scope.Planilla.Conductor);
                        $scope.Planilla.Numero = response.data.Datos.Numero;
                        $scope.Planilla.Consecutivo = response.data.Datos.Consecutivo;
                        $scope.Planilla.NumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.Planilla.NumeroDocumentoTransporte = response.data.Datos.NumeroDocumentoTransporte;
                        $scope.Planilla.Factura = response.data.Datos.Facturado;
                        $scope.Planilla.Fecha = Date.parse(response.data.Datos.Fecha);
                        $scope.Planilla.Cliente = response.data.Datos.Cliente;
                        $scope.Planilla.DocumentoCliente = response.data.Datos.DocumentoCliente;
                        $scope.Planilla.Vehiculo = response.data.Datos.Vehiculo;
                        $scope.Planilla.Ruta = response.data.Datos.Ruta;
                        $scope.Planilla.Semirremolque = response.data.Datos.Semirremolque;
                        
                        $scope.Planilla.Producto = response.data.Datos.Producto;
                        $scope.Planilla.Cantidad = response.data.Datos.Cantidad;
                        $scope.Planilla.Peso = response.data.Datos.Peso;   
                        TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, OPOtrasEmpresas: 1 }).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    //$scope.ListadoTarifaTransportes = [];
                                    //$scope.ListadoTarifaTransportes.push({Nombre: "Seleccione", Codigo : -1});
                                    if (response.data.Datos.length > 0) {
                                        for (var i = 0; i < response.data.Datos.length; i++) {
                                            $scope.ListadoTarifaTransportes.push(response.data.Datos[i]);
                                        }

                                        //$scope.ListadoTarifaTransportes = response.data.Datos;
                                        console.log($scope.ListadoTarifaTransportes);
                                    }
                                    else {
                                        $scope.ListadoTarifaTransportes = []
                                    }
                                }
                            }, function (response) {
                            });
                        $scope.Planilla.TipoTarifa = $linq.Enumerable().From($scope.ListadoTarifaTransportes).First('$.Codigo ==' + response.data.Datos.TipoTarifa.Codigo);                   
                        if (response.data.Datos.Anulado == 1) {
                            $scope.ListaEstados = [

                                { Nombre: 'DEFINITIVO', Codigo: 1 },
                                { Nombre: 'BORRADOR', Codigo: 0 },
                                { Nombre: 'ANULADO', Codigo: 2 }

                            ];

                            $scope.Planilla.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo == 2');
                        } else {
                            $scope.Planilla.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                        }                        
                        $scope.Planilla.FleteTransportador = MascaraValores(response.data.Datos.FleteTransportador);
                        $scope.Planilla.TotalFlete = MascaraValores(response.data.Datos.TotalFlete);
                        $scope.Planilla.RetencionFuente = MascaraValores(response.data.Datos.RetencionFuente);
                        $scope.Planilla.RetencionICA = MascaraValores(response.data.Datos.RetencionICA);
                        $scope.Planilla.AnticipoCliente = MascaraValores(response.data.Datos.AnticipoCliente);
                        $scope.Planilla.RetencionFuente = MascaraValores(response.data.Datos.RetencionFuente);
                        $scope.Planilla.ValorAPagar = MascaraValores(response.data.Datos.ValorPagar);
                        $scope.Planilla.AnticipoPropio = MascaraValores(response.data.Datos.AnticipoPropio);
                        $scope.Planilla.Observaciones = response.data.Datos.Observaciones;
                    }
                    else {
                        ShowError('No se logro consultar la planilla ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarPlanillasDespachosOtrasEmpresas';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarPlanillasDespachosOtrasEmpresas';
                });

            blockUI.stop();
        };


        //Autocompletes:
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                }
            }
            return $scope.ListadoVehiculos
        }



        $scope.ListadoCliente = []
        $scope.AutocompleteCliente = function (value) {

            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 1) {

                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente)
                    

                }
            }
            return $scope.ListadoCliente
        }

        $scope.ListaConductoresPropios = []
        $scope.AutocompleteConductores = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 1) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CONDUCTOR,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaConductoresPropios = ValidarListadoAutocomplete(Response.Datos, $scope.ListaConductoresPropios)
                }
            }
            return $scope.ListaConductoresPropios
        }


        $scope.ListaProductosTransportados = [];
        $scope.AutocompleteProductos = function (value) {

            if (value.length > 0) {

                if ((value.length % 3) == 0 || value.length == 1) {
                    blockUIConfig.autoBlock = false;
                    var Response = ProductoTransportadosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Estado: 1,
                        ValorAutocomplete: value,
                        AplicaTarifario: -1,
                        Sync: true
                    });
                    $scope.ListaProductosTransportados = ValidarListadoAutocomplete(Response.Datos, $scope.ListaProductosTransportados)

                }
            }
            return $scope.ListaProductosTransportados
        }


        $scope.ListadoSemirremolques = [];
        $scope.AutocompleteSemirremolques = function (value) {
            console.log(value);
            if (value.length > 0) {
                if ((value.length % 3 == 0) || value.length == 1) {
                    blockUIConfig.autoBlock = false;
                    var Response = SemirremolquesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Estado: 1,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoSemirremolques = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSemirremolques)
                }

            }
            return $scope.ListadoSemirremolques
        }


        // Máscaras:

        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        }

        $scope.MaskSemirremolque = function (item) {
            MascaraPlacaSemirremolque(item);
        }
        $scope.maxLength = 8;
        $scope.MaskValores = function () {

            if ($scope.Planilla.Cantidad.length < 8) {
                $scope.maxLength = 8;
               
            }

            MascaraValoresGeneral($scope);
            if ($scope.maxLength == 8) {
                var arrayCantidad = $scope.Planilla.Cantidad.split(",");
                if (arrayCantidad.length >= 2) {
                    for (var i = 0; i < arrayCantidad.length; i++) {
                        if (i == arrayCantidad.length - 1) {
                            if (i == 1) {
                                if (arrayCantidad[i].length == 4) {
                                    if (arrayCantidad[i][3] !== '.') {
                                        
                                        for (var j = 0; j < arrayCantidad[i].length; j++) {
                                           
                                            if (j == 2) {
                                                if (arrayCantidad[i][j] != '.') {
                                                    var nArray = arrayCantidad[i].split('');
                                                    
                                                    var arrFinal = [];
                                                    arrFinal.push(nArray[0]);
                                                    arrFinal.push(nArray[1]);
                                                   
                                                    var ArrayNumeroFinal = [];
                                                    ArrayNumeroFinal.push(arrayCantidad[0]);
                                                    ArrayNumeroFinal.push(arrayCantidad[1]);
                                                    ArrayNumeroFinal.push(arrFinal.join(''));
                                                   
                                                    var NumeroFinal = ArrayNumeroFinal.join('');
                                                    
                                                    $scope.Planilla.Cantidad = parseFloat(NumeroFinal);
                                                    
                                                    $scope.Planilla.Cantidad = MascaraValores($scope.Planilla.Cantidad);
                                                    
                                                }
                                            }
                                        }

                                    } else {
                                       
                                        $scope.maxLength = 10;

                                    }
                                }
                            } else if(i == 2) {
                                if (arrayCantidad[i].length == 3) {
                                    if (arrayCantidad[i][2] !== '.') {
                                        
                                        for (var j = 0; j < arrayCantidad[i].length; j++) {
                                            
                                            if (j == 2) {
                                                if (arrayCantidad[i][j] != '.') {
                                                    var nArray = arrayCantidad[i].split('');
                                                    
                                                    var arrFinal = [];
                                                    arrFinal.push(nArray[0]);
                                                    arrFinal.push(nArray[1]);
                                                   
                                                    var ArrayNumeroFinal = [];
                                                    ArrayNumeroFinal.push(arrayCantidad[0]);
                                                    ArrayNumeroFinal.push(arrayCantidad[1]);
                                                    ArrayNumeroFinal.push(arrFinal.join(''));
                                                    
                                                    var NumeroFinal = ArrayNumeroFinal.join('');
                                                    
                                                    $scope.Planilla.Cantidad = parseFloat(NumeroFinal);
                                                   
                                                    $scope.Planilla.Cantidad = MascaraValores($scope.Planilla.Cantidad);
                                                    
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }

                    }

                    /* if (arrayCantidad[3][2] != '.') {
                         console.log('no hay punto');
                         console.log(arrayCantidad[2]);
                         arrayCantidad[2][2] = '';
                     }*/
                }
            }
            /*console.log($scope.Planilla.Cantidad);
            console.log("cantidad: ",$scope.Planilla.Cantidad.length);
            if ($scope.Planilla.Cantidad.length >= 8) {
                console.log('8valores');
                if ($scope.Planilla.Cantidad[7] == '.') {
                    $scope.maxLength = 10;
                    console.log('haypunto');
                } else {
                   // $scope.Planilla.Cantidad[7] = '';
                    $scope.Planilla.Cantidad.length = 8;
                    console.log('se dgito numero');
                }
            }*/

        };

        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }

        $scope.MaskDecimales = function () {
            MascaraDecimalesGeneral($scope);
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        }

        $scope.MaskSemirremolque = function (item) {
            MascaraPlacaSemirremolque(item);
        }
        $scope.MaskDecimales = function (cantidadDecimales) {

            var Cant = 0;
            var Pes = 0;

            if ($scope.Planilla.Cantidad != null || $scope.Planilla.Cantidad != 0 || $scope.Planilla.Cantidad != '0' || $scope.Planilla.Cantidad != '') {
                Cant = parseFloat($scope.Planilla.Cantidad.replace(/,/g, ""));
            }

            if ($scope.Planilla.Peso != null || $scope.Planilla.Peso != 0 || $scope.Planilla.Peso != '0' || $scope.Planilla.Peso != '') {
                Pes = parseFloat($scope.Planilla.Peso.replace(/,/g, ""));
            }

            if ($('input').length > 0) {
                a = $('input');
                for (var i = 0; i < a.length; i++) {
                    try {
                        for (var j = 0; j < a[i].attributes.length; j++) {
                            if (a[i].attributes[j].name == 'name' && a[i].attributes[j].value.includes('cantidad')) {
                               // $scope.Planilla.Cantidad = MascaraDecimales($scope.Planilla.Cantidad, cantidadDecimales);
                                $scope.Planilla.Cantidad = Number.parseFloat(Cant).toFixed(2);
                                $scope.Planilla.Cantidad = MascaraValores($scope.Planilla.Cantidad);
                            }
                            else if (a[i].attributes[j].name == 'name' && a[i].attributes[j].value.includes('peso')) {
                               // $scope.Planilla.Peso = MascaraDecimales($scope.Planilla.Peso, cantidadDecimales);
                                $scope.Planilla.Peso = Number.parseFloat(Pes).toFixed(2);
                                $scope.Planilla.Peso = MascaraValores($scope.Planilla.Peso);
                            }
                        }
                    } catch (e) {

                    }
                }
            }



        }


        try {
            var Parametros = $routeParams.Numero.split(',')
            if (Parametros.length > 1) {
                console.log('hay parametros', Parametros);
                $scope.Planilla.Numero = Parametros[CERO];
                $scope.aplicabase = true
                Obtener();
            } else {
                if ($routeParams.Numero > CERO) {
                    console.log('hay codigo', $routeParams.Numero);
                    $scope.Planilla.Numero = $routeParams.Numero;
                    console.log($scope.Planilla.Numero);
                    Obtener();
                }
                else {
                    $scope.Planilla.Numero = 0;
                   // $scope.CargarDatosFunciones()
                }

            }
        } catch (e) {
            if ($routeParams.Numero > CERO) {
                console.log('hay codigo con error', $routeParams.Numero);
                $scope.Planilla.Numero = $routeParams.Numero;
                console.log($scope.Planilla.Numero);
                Obtener();
            }
            else {
                console.log('no hay nada');
                console.log($routeParams);
                $scope.Planilla.Numero = 0;
              //  $scope.CargarDatosFunciones()
            }

        }
    }]);