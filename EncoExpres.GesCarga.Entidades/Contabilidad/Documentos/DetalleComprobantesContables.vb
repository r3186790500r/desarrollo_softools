﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Namespace Contabilidad.Documentos

    ''' <summary>
    ''' Clase <see cref=" DetalleComprobantesContables"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleComprobantesContables
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleComprobantesContables"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleComprobantesContables"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            Id = Read(lector, "ID")
            CuentaPLUN = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .CodigoCuenta = Read(lector, "NumeroCuenta")}
            ValorDebito = Read(lector, "Valor_Debito")
            ValorCredito = Read(lector, "Valor_Credito")
            ValorBase = Read(lector, "Valor_Base")
            Observaciones = Read(lector, "Observaciones")
            DocumentoCruce = Read(lector, "Documento_Cruce")
            Tercero = New Tercero With {.Codigo = Read(lector, "TERC_Codigo"), .Nombre = Read(lector, "NombreTercero")}
            CentroCosto = Read(lector, "Centro_Costo")
            Prefijo = Read(lector, "Prefijo")
            CodigoAnexo = Read(lector, "Codigo_Anexo")
            SufijoCodigoAnexo = Read(lector, "Sufijo_Codigo_Anexo")
            CampoAuxiliar = Read(lector, "Campo_Auxiliar")


        End Sub

        <JsonProperty>
        Public Property NumeroComprobante As Integer

        <JsonProperty>
        Public Property Id As Integer

        <JsonProperty>
        Public Property CuentaPLUN As PlanUnicoCuentas

        <JsonProperty>
        Public Property ValorDebito As Double

        <JsonProperty>
        Public Property ValorCredito As Double

        <JsonProperty>
        Public Property ValorBase As Double

        <JsonProperty>
        Public Property DocumentoCruce As Integer

        <JsonProperty>
        Public Property DocumentoCruceAlterno As Integer

        <JsonProperty>
        Public Property Tercero As Tercero

        <JsonProperty>
        Public Property CentroCosto As String

        <JsonProperty>
        Public Property Prefijo As String

        <JsonProperty>
        Public Property CodigoAnexo As String

        <JsonProperty>
        Public Property SufijoCodigoAnexo As String

        <JsonProperty>
        Public Property CampoAuxiliar As String

    End Class
End Namespace
