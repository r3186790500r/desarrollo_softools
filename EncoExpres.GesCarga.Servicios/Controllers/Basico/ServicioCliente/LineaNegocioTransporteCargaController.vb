﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente
Imports EncoExpres.GesCarga.Negocio.Basico.ServicioCliente

''' <summary>
''' Controlador <see cref="LineaNegocioTransporteCargaController"/>
''' </summary>
<Authorize>
Public Class LineaNegocioTransporteCargaController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As LineaNegocioTransporteCarga) As Respuesta(Of IEnumerable(Of LineaNegocioTransporteCarga))
        Return New LogicaLineaNegocioTransporteCarga(CapaPersistenciaLineaNegocioTransporteCarga).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As LineaNegocioTransporteCarga) As Respuesta(Of LineaNegocioTransporteCarga)
        Return New LogicaLineaNegocioTransporteCarga(CapaPersistenciaLineaNegocioTransporteCarga).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As LineaNegocioTransporteCarga) As Respuesta(Of Long)
        Return New LogicaLineaNegocioTransporteCarga(CapaPersistenciaLineaNegocioTransporteCarga).Guardar(entidad)
    End Function
End Class
