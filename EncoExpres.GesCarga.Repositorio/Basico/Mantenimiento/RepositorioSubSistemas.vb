﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico

Namespace Basico
    Public NotInheritable Class RepositorioSubSistemas
        Inherits RepositorioBase(Of SubSistemas)
        Public Overrides Function Consultar(filtro As SubSistemas) As IEnumerable(Of SubSistemas)
            Dim lista As New List(Of SubSistemas)
            Dim item As SubSistemas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Pagina) Then
                    If (filtro.Pagina) Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If (filtro.RegistrosPagina) Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.Nombre) Then
                    If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If
                If Not IsNothing(filtro.Codigo) Then
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado.Codigo >= 0 Then 'NO APLICA
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.TipoSubsistema) Then
                    If filtro.TipoSubsistema.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TISM_Codigo", filtro.TipoSubsistema.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.SistemaMantenimiento) Then
                    If filtro.SistemaMantenimiento.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_SIMA_Codigo", filtro.SistemaMantenimiento.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CodigoAlterno) Then
                    If filtro.CodigoAlterno <> "" Then
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                    End If
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_subsistema_mantenimientos]")

                While resultado.Read
                    item = New SubSistemas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As SubSistemas) As Long
            Dim inserto As Boolean = True
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                conexion.AgregarParametroSQL("@par_SIMA_Codigo", entidad.SistemaMantenimiento.Codigo)
                conexion.AgregarParametroSQL("@par_TISM_Codigo", entidad.TipoSubsistema.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_subsistema_mantenimientos]")

                While resultado.Read
                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While
            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As SubSistemas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                conexion.AgregarParametroSQL("@par_TISM_Codigo", entidad.TipoSubsistema.Codigo)
                conexion.AgregarParametroSQL("@par_SIMA_Codigo", entidad.SistemaMantenimiento.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_modificar_subsistema_mantenimientos]")

                While resultado.Read
                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While
                resultado.Close()
            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As SubSistemas) As SubSistemas
            Dim item As New SubSistemas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_subsistema_mantenimientos]")

                While resultado.Read
                    item = New SubSistemas(resultado)
                End While

            End Using

            Return item
        End Function
    End Class
End Namespace
