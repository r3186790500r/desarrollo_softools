﻿DROP PROCEDURE gsp_Obtener_detalle_Precinto    
GO
CREATE PROCEDURE gsp_Obtener_detalle_Precinto       
(@par_Empr_Codigo Numeric       
,@par_Numero numeric )      
AS BEGIN      
    
SELECT        
DAPO.ID IDPrecinto,      
DAPO.EMPR_Codigo,      
DAPO.Numero_Precinto,      
isnull(ENMC.Numero_Documento ,0) as NumeroMafiesto,      
isnull(ENPD.Numero_Documento ,0) as NumeroPlanillaDespacho,     
ISNULL(ENRE.Numero_Documento,0) AS NumeroRemesa,   
OFIC.Nombre NombreOficina,      
OFOR.Nombre NombreOficinaOrigen,      
DAPO.Estado,      
DAPO.Anulado      
FROM Detalle_Asignacion_Precintos_Oficina DAPO      
      
LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON       
DAPO.EMPR_Codigo = ENMC.EMPR_Codigo      
AND DAPO.ENMD_Numero = ENMC.Numero      
      
LEFT JOIN Encabezado_Planilla_Despachos ENPD ON       
DAPO.EMPR_Codigo = ENPD.EMPR_Codigo       
AND DAPO.ENPD_Numero = ENPD.Numero       
      
LEFT JOIN Oficinas OFIC on       
DAPO.EMPR_Codigo = OFIC.EMPR_Codigo      
AND DAPO.OFIC_Codigo = OFIC.Codigo      
      
LEFT JOIN Oficinas OFOR on       
DAPO.EMPR_Codigo = OFOR.EMPR_Codigo      
AND DAPO.OFIC_Codigo_Origen = OFOR.Codigo      
      
 LEFT JOIN Encabezado_Remesas ENRE ON   
DAPO.EMPR_Codigo = ENRE.EMPR_Codigo   
AND DAPO.ENRE_Numero = ENRE.Numero    
AND ENRE.TIDO_Codigo = 100  
  
WHERE DAPO.EMPR_Codigo = @par_Empr_Codigo      
AND DAPO.EAPO_Numero = @par_Numero      
AND (ENPD.TIDO_Codigo = 150 or ENPD.TIDO_Codigo  is null)  
AND DAPO.ENPD_Numero = 0    
AND DAPO.ENRE_Numero = 0    
AND DAPO.ENMD_Numero = 0    
END       
GO