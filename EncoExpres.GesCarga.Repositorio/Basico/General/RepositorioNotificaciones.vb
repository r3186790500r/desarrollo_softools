﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.General
    Public NotInheritable Class RepositorioNotificaciones
        Inherits RepositorioBase(Of Notificaciones)

        Public Overrides Function Consultar(filtro As Notificaciones) As IEnumerable(Of Notificaciones)
            Dim lista As New List(Of Notificaciones)
            Dim item As Notificaciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.UsuarioNotificacion) Then
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Notifica", filtro.UsuarioNotificacion.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_notificaciones]")

                While resultado.Read
                    item = New Notificaciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Notificaciones) As Long
            Throw New NotImplementedException()
        End Function


        Public Overrides Function Modificar(entidad As Notificaciones) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As Notificaciones) As Notificaciones
            Throw New NotImplementedException()

        End Function
        Public Function Anular(entidad As Notificaciones) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace