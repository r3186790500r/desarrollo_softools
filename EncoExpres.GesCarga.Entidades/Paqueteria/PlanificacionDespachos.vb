﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa

Namespace Paqueteria
    <JsonObject>
    Public Class PlanificacionDespachos
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            CiudadDestino = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino"), .Nombre = Read(lector, "CIUD_Destino")}
            CiudadOrigen = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Origen"), .Nombre = Read(lector, "CIUD_Origen")}
            NumeroRemesas = Read(lector, "NumeroRemesas")
            Cantidad = Read(lector, "Cantidad")
            Peso = Read(lector, "Peso")
            TotalFlete = Read(lector, "Total_Flete")
            VolumenM3 = Read(lector, "Volumen_M3")
            Remesa = New Remesas With {
                    .Numero = Read(lector, "Numero"),
                    .NumeroDocumento = Read(lector, "Numero_Documento"),
                    .Fecha = Read(lector, "Fecha"),
                    .Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .NombreCompleto = Read(lector, "NombreCliente")},
                    .CiudadRemitente = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Origen"), .Nombre = Read(lector, "CIOR_Nombre")},
                    .CiudadDestinatario = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino"), .Nombre = Read(lector, "CIDE_Nombre")},
                    .CantidadCliente = Read(lector, "Cantidad_Cliente"),
                    .PesoCliente = Read(lector, "Peso_Cliente"),
                    .FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FOPR_Codigo"), .Nombre = Read(lector, "FOPR_Nombre")},
                    .ValorFleteCliente = Read(lector, "Valor_Flete_Cliente"),
                    .TotalFleteCliente = Read(lector, "Total_Flete_Cliente"),
                    .NumeroDocumentoCliente = Read(lector, "Documento_Cliente"),
                    .Observaciones = Read(lector, "Observaciones")
             }
            ValorReexpedicion = Read(lector, "Valor_Reexpedicion")
            ValorSeguro = Read(lector, "Valor_Seguro")
            Reexpedicion = Read(lector, "Reexpedicion")
        End Sub

        <JsonProperty>
        Public Property Remesa As Remesas
        <JsonProperty>
        Public Property CiudadOrigen As Ciudades
        <JsonProperty>
        Public Property CiudadDestino As Ciudades
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property DocumentoCliente As Integer
        <JsonProperty>
        Public Property Remitente As Terceros
        <JsonProperty>
        Public Property VolumenM3 As Double
#Region "Resumen Remesas"
        <JsonProperty>
        Public Property NumeroRemesas As Integer
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property TotalFlete As Double
        <JsonProperty>
        Public Property ValorReexpedicion As Double
        <JsonProperty>
        Public Property ValorSeguro As Double
        <JsonProperty>
        Public Property Reexpedicion As Integer
        <JsonProperty>
        Public Property TipoDocumentoPlanilla As Integer
#End Region
    End Class
End Namespace


