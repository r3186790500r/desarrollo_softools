﻿PRINT 'gsp_obtener_evento_correos'
GO
DROP PROCEDURE gsp_obtener_evento_correos
GO
CREATE PROCEDURE gsp_obtener_evento_correos    
(      
@par_EMPR_Codigo SMALLINT,      
@par_EVCO_Codigo numeric = NULL,      
@par_Nombre varchar = NULL,      
@par_MOAP_Codigo smallint = NULL,      
@par_MEAP_Codigo numeric = NULL,      
@par_CATA_TEGC_Codigo numeric = NULL,      
@par_Estado smallint = NULL      
)      
AS      
BEGIN      
      
SELECT       
      
EVCO.EMPR_Codigo,      
EVCO.Codigo,      
EVCO.Nombre,      
EVCO.MOAP_Codigo,      
EVCO.MEAP_Codigo,      
EVCO.CATA_TEGC_Codigo,      
ISNULL(EVCO.Asunto_Correo,'') AS Asunto_Correo,  
ISNULL(EVCO.Encabezado_Correo,'') AS Encabezado_Correo,  
ISNULL(EVCO.Mensaje_Correo,'') AS Mensaje_Correo,  
EVCO.Adjuntar_Archivo_Correo,  
ISNULL(EVCO.Mensaje_Firma,'') AS Mensaje_Firma,    
EVCO.Estado,  
MEAP.Nombre as NombreMenu,  
MOAP.Nombre as NombreModulo  
  
FROM     
    
Evento_Correos AS EVCO,  
Menu_Aplicaciones AS MEAP,  
Modulo_Aplicaciones AS MOAP  
    
WHERE EVCO.EMPR_Codigo = @par_EMPR_Codigo   
     
AND EVCO.EMPR_Codigo = MEAP.EMPR_Codigo  
AND EVCO.MEAP_Codigo = MEAP.Codigo  
  
AND EVCO.EMPR_Codigo = MOAP.EMPR_Codigo  
AND EVCO.MOAP_Codigo = MOAP.Codigo  
  
AND EVCO.Codigo = ISNULL(@par_EVCO_Codigo ,EVCO.Codigo)      
AND EVCO.Nombre LIKE '%'+ISNULL( @par_Nombre ,EVCO.Nombre)  +'%'    
AND EVCO.MOAP_Codigo = ISNULL( @par_MOAP_Codigo ,EVCO.MOAP_Codigo)      
AND MEAP_Codigo = ISNULL( @par_MEAP_Codigo ,MEAP_Codigo)      
AND CATA_TEGC_Codigo =  ISNULL(@par_CATA_TEGC_Codigo ,CATA_TEGC_Codigo)      
AND EVCO.Estado = ISNULL( @par_Estado ,EVCO.Estado)      
END      
GO