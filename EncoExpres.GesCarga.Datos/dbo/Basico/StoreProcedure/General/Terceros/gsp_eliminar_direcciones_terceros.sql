﻿print ('gsp_eliminar_direcciones_terceros') 
go
drop procedure gsp_eliminar_direcciones_terceros
go
create procedure gsp_eliminar_direcciones_terceros
(@par_EMPR_Codigo SMALLINT,
@par_TERC_Codigo numeric)
as
begin
delete Tercero_Direcciones 
where 
EMPR_Codigo = @par_EMPR_Codigo
AND 
TERC_Codigo = @par_TERC_Codigo
end
go