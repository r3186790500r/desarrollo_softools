﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Public NotInheritable Class RepositarioSucursalesSIESA
    Inherits RepositorioBase(Of SucursalesSIESA)

    Public Overrides Function Insertar(entidad As SucursalesSIESA) As Long
        Dim inserto As Boolean = False


        Using transaccion = New TransactionScope(TransactionScopeOption.Required)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CATA_PETE_Codigo", entidad.Perfil_Terceros.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_TICS_Codigo", entidad.Tipo_Conector.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", entidad.Forma_Pago.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_SIESA", entidad.Codigo_SIESA)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_sucursales_SIESA")
                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()
                conexion.CloseConnection()

            End Using

            transaccion.Complete()

        End Using

        Return entidad.Codigo
    End Function

    Public Overrides Function Modificar(entidad As SucursalesSIESA) As Long
        Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

            conexion.CreateConnection()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_PETE_Codigo", entidad.Perfil_Terceros.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_TICS_Codigo", entidad.Tipo_Conector.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", entidad.Forma_Pago.Codigo)
            conexion.AgregarParametroSQL("@par_Codigo_SIESA", entidad.Codigo_SIESA)
            conexion.AgregarParametroSQL("@par_USUA_Modifica", entidad.UsuarioModifica.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_sucursales_SIESA")

            While resultado.Read
                entidad.Codigo = resultado.Item("Codigo").ToString()
            End While

            resultado.Close()

            If entidad.Codigo.Equals(Cero) Then
                Return Cero
            End If

        End Using

        Return entidad.Codigo
    End Function
    Public Overrides Function Consultar(filtro As SucursalesSIESA) As IEnumerable(Of SucursalesSIESA)

        Dim lista As New List(Of SucursalesSIESA)
        Dim item As SucursalesSIESA

        Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

            conexion.CreateConnection()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

            If Not IsNothing(filtro.Perfil_Terceros) Then
                If Not IsNothing(filtro.Perfil_Terceros.Nombre) Then
                    If (filtro.Perfil_Terceros.Nombre.Equals("(NO APLICA)")) Then
                        conexion.AgregarParametroSQL("@par_CATA_PETE_Name", "")
                    Else
                        conexion.AgregarParametroSQL("@par_CATA_PETE_Name", filtro.Perfil_Terceros.Nombre)
                    End If
                End If
            End If

            If Not IsNothing(filtro.Tipo_Conector) Then
                If Not IsNothing(filtro.Tipo_Conector.Nombre) Then
                    If (filtro.Tipo_Conector.Nombre.Equals("(NO APLICA)")) Then
                        conexion.AgregarParametroSQL("@par_CATA_TICS_Name", "")
                    Else
                        conexion.AgregarParametroSQL("@par_CATA_TICS_Name", filtro.Tipo_Conector.Nombre)
                    End If
                End If
            End If

            If Not IsNothing(filtro.Forma_Pago) Then
                If Not IsNothing(filtro.Forma_Pago.Nombre) Then
                    If (filtro.Forma_Pago.Nombre.Equals("(NO APLICA)")) Then
                        conexion.AgregarParametroSQL("@par_CATA_FPVE_Name", "")
                    Else
                        conexion.AgregarParametroSQL("@par_CATA_FPVE_Name", filtro.Forma_Pago.Nombre)
                    End If

                End If
            End If

            If Not IsNothing(filtro.Codigo_SIESA) Then
                conexion.AgregarParametroSQL("@par_Codigo_SIESA", filtro.Codigo_SIESA)
            End If

            If Not IsNothing(filtro.Codigo) Then
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
            End If

            If Not IsNothing(filtro.Pagina) Then
                If filtro.Pagina > Cero Then
                    conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                End If
            End If
            If Not IsNothing(filtro.RegistrosPagina) Then
                If filtro.RegistrosPagina > Cero Then
                    conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                End If
            End If
            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_sucursales_SIESA]")

            While resultado.Read
                item = New SucursalesSIESA(resultado)
                lista.Add(item)
            End While


        End Using

        Return lista

    End Function

    Public Overrides Function Obtener(filtro As SucursalesSIESA) As SucursalesSIESA
        Dim item As New SucursalesSIESA

        Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

            conexion.CreateConnection()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

            If filtro.Codigo > Cero Then
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
            End If

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_sucursales_SIESA]")

            While resultado.Read
                item = New SucursalesSIESA(resultado)
            End While

        End Using

        Return item
    End Function

    Public Function Anular(entidad As SucursalesSIESA) As Boolean
        Dim anulo As Boolean = False

        Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

            conexion.CreateConnection()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_sucursales_SIESA]")

            While resultado.Read
                anulo = IIf((resultado.Item("Numero")) > Cero, True, False)
            End While

        End Using

        Return anulo
    End Function

End Class
