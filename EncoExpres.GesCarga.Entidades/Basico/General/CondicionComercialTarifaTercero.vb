﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos

Namespace Basico.General
    Public Class CondicionComercialTarifaTercero
        Inherits Base

        Sub New()
        End Sub

        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)
            LineaNegocio = New LineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo")}
            TipoLineaNegocio = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo")}
            Tarifa = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo")}
            TipoTarifa = New TipoTarifaTransportes With {.Codigo = Read(lector, "TTTC_Codigo"), .ValorRangoInicial = Read(lector, "Rango_Inicial"), .ValorRangoFinal = Read(lector, "Rango_Final")}
            FleteMinimo = Read(lector, "Valor_Flete_Minimo")
            ManejoMinimo = Read(lector, "Valor_Manejo_Minimo")
            PorcentajeValorDeclarado = Read(lector, "Porcentaje_Valor_Declarado")
            PorcentajeFlete = Read(lector, "Porcentaje_Descuento_Flete")
            PorcentajeVolumen = Read(lector, "Porcentaje_Descuento_Volumen")

        End Sub

        <JsonProperty>
        Public Property LineaNegocio As LineaNegocioTransportes
        <JsonProperty>
        Public Property TipoLineaNegocio As TipoLineaNegocioTransportes
        <JsonProperty>
        Public Property Tarifa As TarifaTransportes
        <JsonProperty>
        Public Property TipoTarifa As TipoTarifaTransportes
        <JsonProperty>
        Public Property FleteMinimo As Long
        <JsonProperty>
        Public Property ManejoMinimo As Long
        <JsonProperty>
        Public Property PorcentajeValorDeclarado As Double
        <JsonProperty>
        Public Property PorcentajeFlete As Double
        <JsonProperty>
        Public Property PorcentajeVolumen As Double





    End Class
End Namespace
