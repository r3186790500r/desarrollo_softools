﻿EncoExpresApp.controller("ConsultarPlanMantenimientoEspecialCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', 'ValorCatalogosFactory', '$linq', 'PlanEquipoMantenimientoFactory', 'EquipoMantenimientoFactory', function ($scope, $timeout, $routeParams, blockUI, ValorCatalogosFactory, $linq, PlanEquipoMantenimientoFactory, EquipoMantenimientoFactory) {
    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
    $scope.Buscando = false;
    $scope.ResultadoSinRegistros = '';
    $scope.MensajesError = [];
    var filtros = {};
    $scope.ListadoEstados = [];
    $scope.totalRegistros = 0;
    $scope.paginaActual = 1;
    $scope.cantidadRegistrosPorPagina = 10;
    $scope.totalPaginas = 0;
    $scope.ListaEquipos = [];
    $scope.Codigo = '';
    $scope.CodigoAlterno = '';
    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PLAN_MANTENIMIENTO);
    $scope.MapaSitio = [{ Nombre: 'Mantenimiento' }, { Nombre: 'Documentos' }, { Nombre: 'Planes  Mantenimiento' }];
    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    // -------------------------- Constantes ---------------------------------------------------------------------//

    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };

    /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

    /*Cargar el combo de estados*/
    $scope.ListadoEstados = [
        { Codigo: -1, Nombre: '(NO APLICA)' },
        { Codigo: 0, Nombre: 'BORRADOR' },
        { Codigo: 1, Nombre: 'DEFINITIVO' },
        { Codigo: 2, Nombre: 'ANULADO' }
    ];

    $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
    /*Cargar el combo de equipos*/

    EquipoMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoEquipoMantenimiento = [];
                $scope.ListadoEquipoMantenimiento = response.data.Datos;
                for (var i = 0; i < $scope.ListadoEquipoMantenimiento.length; i++) {
                    var item = $scope.ListadoEquipoMantenimiento[i]
                    item.NombreCompleto = item.Nombre + ' (' + item.Vehiculo.Placa + ')'
                }
                $scope.EquipoMantenimiento = $scope.ListadoEquipoMantenimiento[0];
                $timeout(function () {
                    if ($('#equipo')[0].options[0].value == "?") {
                        $('#equipo')[0].options[0].remove()
                    }
                }, 50);

                $timeout(function () {
                    $('#equipo').selectpicker()
                }, 200);

            }
        }, function (response) {
            ShowError(response.statusText);
        });
    /*---------------------------------------------------------------------Funcion Buscar Plan Mantenimiento-----------------------------------------------------------------------------*/
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = '';
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);

        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoPlanMantenimiento = [];
        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Codigo,
            CodigoAlterno: $scope.CodigoAlterno,
            Nombre: $scope.Nombre,
            Estado: $scope.Estado.Codigo,
            Pagina: $scope.paginaActual,
            RegistrosPagina: $scope.cantidadRegistrosPorPagina
        };

        //DatosRequeridos();

        if ($scope.MensajesError.length == 0) {
            blockUI.delay = 1000;

            PlanEquipoMantenimientoFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoPlanMantenimiento = response.data.Datos

                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        else {
            $scope.Buscando = false;
        }
        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarPlanMantenimiento';
        }
    };
    /*-------------------------------------------------------------------------------------Anular Contrato-----------------------------------------------------------*/
    $scope.AbrirModalAsociacion = function (item) {
        $scope.ListaEquipos = []
        $scope.ItemTemp = item
        PlanEquipoMantenimientoFactory.ConsultarEquipos(item).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaEquipos = response.data.Datos
                        for (var i = 0; i < $scope.ListaEquipos.length; i++) {
                            var item = $scope.ListaEquipos[i]
                            item.NombreCompleto = item.Equipo.Nombre
                            item.Codigo = item.Equipo.Codigo
                        }
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
    }
    $scope.AdicionarEquipo = function () {
        var cont = 0
        for (var i = 0; i < $scope.ListaEquipos.length; i++) {
            var item = $scope.ListaEquipos[i]
            if (item.Codigo == $scope.EquipoMantenimiento.Codigo) {
                cont++
            }
        }
        if (cont > 0) {
            ShowError('Este equipo ya se encuentra agregado');
        } else {
            $scope.ListaEquipos.push($scope.EquipoMantenimiento)
        }
    }
    $scope.EliminarEquipo = function (index) {
        $scope.ListaEquipos.splice(index, 1)

    }
    $scope.AsociarEquipos = function () {
        if ($scope.ListaEquipos.length == 0) {
            ShowError('Ingrese alemenos 1 equipo');
        } else {
            closeModal('modalasociacionEquipos')
            var Entidad = []
            $scope.ItemTemp.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            for (var i = 0; i < $scope.ListaEquipos.length; i++) {
                var item = $scope.ListaEquipos[i]
                Entidad.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, PlanMantenimiento: $scope.ItemTemp, Equipo: item })
            }
            PlanEquipoMantenimientoFactory.InsertarEquipos(Entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Los equipos se asociarón correctamente al plan de mantenimiento: ' + $scope.ItemTemp.Nombre)
                    }
                }, function (response) {
                    ShowError('Los equipos no se asociarón correctamente por favor contacte al administrador del sistema');
                });
        }
    }

    /*Validacion boton anular */
    $scope.ConfirmacionAnularContrato = function (numero, EstadoDocumento) {
        $scope.estadoanulado = EstadoDocumento;
        $scope.NumeroAnulacion = numero
        if ($scope.ValidarPermisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
            if ($scope.estadoanulado.Codigo !== CODIGO_ESTADO_ANULADO) {
                showModal('modalConfirmacionAnularPlan');
            }
            else {
                closeModal('modalConfirmacionAnularPlan');
            }
        }
    };

    /*Funcion que solicita los datos de la anulación */
    $scope.SolicitarDatosAnulacion = function () {
        $scope.FechaAnulacion = new Date();
        closeModal('modalConfirmacionAnularPlan');
        showModal('modalDatosAnularPlan');
    };
    /*---------------------------------------------------------------------Funcion Anular Plan Mantenimiento ----------------------------------------------------------------------------*/
    $scope.Anular = function () {

        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: $scope.NumeroAnulacion,
            UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CausaAnulacion: $scope.CausaAnulacion,
        };

        PlanEquipoMantenimientoFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se anuló el plan de mantenimiento  No.' + entidad.Numero);
                    closeModal('modalDatosAnularPlan');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                ShowError(response.statusText);
            });
    };
    $scope.CambiarEstado = function (item) {
        $scope.itemEstado = item
        showModal('ModalEstado')
    }
    $scope.CambiarEstadoPlan = function () {
        PlanEquipoMantenimientoFactory.CambiarEstado($scope.itemEstado).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se cambió el estado a borrador del plan de mantenimiento:' + $scope.itemEstado.Nombre);
                    closeModal('ModalEstado');
                    $scope.itemEstado = ''
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                ShowError(response.statusText);
            });
    }
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    $scope.MaskNumero = function (option) {
        MascaraNumero(option)
    };

    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarPlanMantenimiento';
        }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Codigo = MascaraNumero($scope.Codigo) } catch (e) { }
    };
    $scope.urlASP = '';
    $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
    $scope.DesplegarInforme = function (Numero) {
        if ($scope.ValidarPermisos.AplicaImprimir == PERMISO_ACTIVO) {
            $scope.FiltroArmado = '';

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.Numero = Numero;
            $scope.NombreReporte = 'RepPlanMantenimiento'
            //Arma el filtro
            $scope.ArmarFiltro();
            //Llama a la pagina ASP con el filtro por GET

            window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + 1);

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';
        };
    };

    // funcion enviar parametros al proyecto ASP armando el filtro
    $scope.ArmarFiltro = function (Numero) {
        $scope.FiltroArmado = '';
        if ($scope.Numero !== undefined && $scope.Numero !== '' && $scope.Numero !== null) {
            $scope.FiltroArmado += '&Numero=' + $scope.Numero;
        }
    }

    if ($routeParams.Codigo > 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }

}]);