﻿PRINT 'gsp_obtener_detalle_manifiesto'
GO

DROP PROCEDURE gsp_obtener_detalle_manifiesto
GO

CREATE PROCEDURE gsp_obtener_detalle_manifiesto  
(  
	@par_EMPR_Codigo SMALLINT,  
	@par_ENMC_Numero INT  
)  
AS  
BEGIN
	SELECT
		DMCA.EMPR_Codigo
	   ,DMCA.ENMC_Numero
	   ,DMCA.ENRE_Numero
	   ,ENRE.Numero_Documento AS ENRE_NumeroDocumento
	   ,ENRE.TERC_Codigo_Cliente
	   ,CONCAT(CLIE.Nombre, ' ', CLIE.Apellido1, ' ', CLIE.Apellido2, ' ', CLIE.Razon_Social) AS Cliente
	   ,ENRE.PRTR_Codigo
	   ,PRTR.Nombre AS Producto
	   ,ENRE.Cantidad_Cliente
	   ,ENRE.Peso_Cliente
	   ,ENRE.TERC_Codigo_Destinatario
	   ,CONCAT(DEST.Nombre, ' ', DEST.Apellido1, ' ', DEST.Apellido2,' ',DEST.Razon_Social) AS Destinatario
	FROM Detalle_Manifiesto_Carga DMCA
		,Encabezado_Remesas ENRE
		,Terceros CLIE
		,Producto_Transportados PRTR
		,Terceros DEST
	WHERE DMCA.EMPR_Codigo = ENRE.EMPR_Codigo
	AND DMCA.ENRE_Numero = ENRE.Numero

	AND ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo

	AND ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
	AND ENRE.PRTR_Codigo = PRTR.Codigo

	AND ENRE.EMPR_Codigo = DEST.EMPR_Codigo
	AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo

	AND DMCA.EMPR_Codigo = @par_EMPR_Codigo
	AND DMCA.ENMC_Numero = @par_ENMC_Numero
END
GO