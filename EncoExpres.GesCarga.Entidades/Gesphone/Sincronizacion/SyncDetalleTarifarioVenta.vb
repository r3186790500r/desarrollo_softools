﻿Imports Newtonsoft.Json

Namespace Gesphone

    ''' <summary>
    ''' Clase que tiene la informacion correspondiente al detalle de un tarifario venta.
    ''' </summary>
    <JsonObject>
    Public Class SyncDetalleTarifarioVenta

        <JsonProperty>
        Public Property Id As Integer

        <JsonProperty>
        Public Property Ln As Integer

        <JsonProperty>
        Public Property Tln As Integer

        <JsonProperty>
        Public Property Tt As Integer

        <JsonProperty>
        Public Property Ttt As Integer

        <JsonProperty>
        Public Property Vf As Decimal

        <JsonProperty>
        Public Property Ve As Decimal

        <JsonProperty>
        Public Property O1 As Decimal

        <JsonProperty>
        Public Property O2 As Decimal

        <JsonProperty>
        Public Property O3 As Decimal

        <JsonProperty>
        Public Property O4 As Decimal

        <JsonProperty>
        Public Property Nttc As String

        <JsonProperty>
        Public Property Ntc As String

        <JsonProperty>
        Public Property Fpv As Decimal

        <JsonProperty>
        Public Property Ca As Integer

        <JsonProperty>
        Public Property Vc1 As Decimal

        <JsonProperty>
        Public Property Vc2 As String

        <JsonProperty>
        Public Property Vc3 As String

        <JsonProperty>
        Public Property Vm As Decimal

        <JsonProperty>
        Public Property Vs As Decimal

        <JsonProperty>
        Public Property Ps As Decimal

        <JsonProperty>
        Public Property Re As Integer

        <JsonProperty>
        Public Property Pr As Decimal

        <JsonProperty>
        Public Property Vc As Decimal

        <JsonProperty>
        Public Property Vd As Decimal

        <JsonProperty>
        Public Property Co As Decimal

        <JsonProperty>
        Public Property Cd As Decimal

        <JsonProperty>
        <NotMapped>
        Public Property CondicionComercial As Boolean

        <JsonProperty>
        <NotMapped>
        Public Property FleteMinimo As Decimal

        <JsonProperty>
        <NotMapped>
        Public Property ManejoMinimo As Decimal

        <JsonProperty>
        <NotMapped>
        Public Property PorcentajeValorDeclarado As Decimal

        <JsonProperty>
        <NotMapped>
        Public Property PorcentajeFlete As Decimal

        <JsonProperty>
        <NotMapped>
        Public Property PorcentajeVolumen As Decimal

    End Class

End Namespace
