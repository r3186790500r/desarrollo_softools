﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioValorCatalogos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioValorCatalogos
        Inherits RepositorioBase(Of ValorCatalogos)

        Public Overrides Function Consultar(filtro As ValorCatalogos) As IEnumerable(Of ValorCatalogos)
            Dim lista As New List(Of ValorCatalogos)
            Dim item As ValorCatalogos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Catalogo.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_CATA_Codigo", filtro.Catalogo.Codigo)
                End If

                If Len(filtro.Nombre) > 0 Then
                    conexion.AgregarParametroSQL("@par_Campo1", filtro.Nombre)
                End If

                If Len(filtro.CampoAuxiliar2) > 0 Then
                    conexion.AgregarParametroSQL("@par_Campo2", filtro.CampoAuxiliar2)
                End If

                If Len(filtro.CampoAuxiliar3) > 0 Then
                    conexion.AgregarParametroSQL("@par_Campo3", filtro.CampoAuxiliar3)
                End If

                If Len(filtro.CampoAuxiliar4) > 0 Then
                    conexion.AgregarParametroSQL("@par_Campo4", filtro.CampoAuxiliar4)
                End If

                If Len(filtro.CampoAuxiliar5) > 0 Then
                    conexion.AgregarParametroSQL("@par_Campo5", filtro.CampoAuxiliar5)
                End If

                If filtro.Estado > 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_valor_catalogos")

                While resultado.Read
                    item = New ValorCatalogos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ValorCatalogos) As Long
            Dim inserto As Boolean = True
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_Codigo", entidad.Catalogo.Codigo)
                conexion.AgregarParametroSQL("@par_Campo1", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Campo2", entidad.CampoAuxiliar2)
                conexion.AgregarParametroSQL("@par_Campo3", entidad.CampoAuxiliar3)
                conexion.AgregarParametroSQL("@par_Campo4", entidad.CampoAuxiliar4)
                conexion.AgregarParametroSQL("@par_Campo5", entidad.CampoAuxiliar5)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_valor_catalogos]")

                While resultado.Read
                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While
            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As ValorCatalogos) As Long
            Dim Modificodetalle As Boolean = True
            Dim ModificoPedido As Boolean = True

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Campo1", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Campo2", entidad.CampoAuxiliar2)
                conexion.AgregarParametroSQL("@par_Campo3", entidad.CampoAuxiliar3)
                conexion.AgregarParametroSQL("@par_Campo4", entidad.CampoAuxiliar4)
                conexion.AgregarParametroSQL("@par_Campo5", entidad.CampoAuxiliar5)
                conexion.AgregarParametroSQL("@par_USUA_Modifica", entidad.UsuarioModifica.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_modificar_valor_catalogos]")

                While resultado.Read
                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While

                resultado.Close()

                If resultado.RecordsAffected > 0 Then
                    Modificar = resultado.RecordsAffected
                    resultado.Close()
                End If

            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As ValorCatalogos) As ValorCatalogos
            Dim item As New ValorCatalogos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_valor_catalogos]")

                While resultado.Read
                    item = New ValorCatalogos(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As ValorCatalogos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_Codigo", entidad.Catalogo.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_valor_catalogos]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class

End Namespace