﻿
Print 'CREATE PROCEDURE gsp_consultar_impuestos_conceptos_facturacion'
GO
DROP PROCEDURE gsp_consultar_impuestos_conceptos_facturacion
GO

CREATE PROCEDURE gsp_consultar_impuestos_conceptos_facturacion (          
  @par_EMPR_Codigo SMALLINT  ,           
  @par_Codigo NUMERIC = NULL ,
  @par_CIUD_Codigo NUMERIC = NULL,        
  @par_NumeroPagina  NUMERIC = NULL,        
  @par_RegistrosPagina NUMERIC = NULL      
 )          
AS          
BEGIN          
	SET NOCOUNT ON;          
	DECLARE          
	@CantidadRegistros INT          
	SELECT @CantidadRegistros = (          
		SELECT DISTINCT           
		COUNT(1)           

		FROM Impuestos_Conceptos_Ventas AS ICLP 

		INNER JOIN       
		Encabezado_Impuestos AS ENIM      
		ON ICLP.EMPR_Codigo = ENIM.EMPR_Codigo       
		AND ICLP.ENIM_Codigo = ENIM.Codigo   
		   
		WHERE  ICLP.EMPR_Codigo = @par_EMPR_Codigo        
		AND ICLP.COVE_Codigo = ISNULL(@par_Codigo,ICLP.COVE_Codigo)        
		AND ENIM.Estado = 1      
	);                 
	WITH Pagina AS          
	(SELECT          
	ENIM.Codigo,      
	ENIM.Nombre,      
	ENIM.Operacion,
	ISNULL(CASE WHEN ENIM.CATA_TRAI_Codigo = 803 THEN 
	(SELECT Valor_Base
	 FROM Detalle_Ciudad_Impuestos AS DECI
	 WHERE DECI.EMPR_Codigo =  ENIM.EMPR_Codigo 
	 AND  DECI.ENIM_Codigo = ENIM.Codigo 
	 AND CIUD_Codigo = @par_CIUD_Codigo ) 
	 ELSE ENIM.Valor_Base END, ENIM.Valor_Base) AS Valor_Base,
	ISNULL(CASE WHEN ENIM.CATA_TRAI_Codigo = 803 THEN 
	(SELECT Valor_Tarifa 
	FROM Detalle_Ciudad_Impuestos AS DECI
	WHERE DECI.EMPR_Codigo =  ENIM.EMPR_Codigo 
	AND  DECI.ENIM_Codigo = ENIM.Codigo 
	AND CIUD_Codigo = @par_CIUD_Codigo ) 
	ELSE ENIM.Valor_Tarifa END, ENIM.Valor_Tarifa) AS Valor_Tarifa,
	ISNULL(PUC.Codigo_Cuenta,0) PLUC_Codigo,      
	ENIM.Estado,      
	ROW_NUMBER() OVER ( ORDER BY  ENIM.Codigo) AS RowNumber          
	FROM Impuestos_Conceptos_Ventas ICLP INNER JOIN       
	Encabezado_Impuestos ENIM      
	ON ICLP.EMPR_Codigo = ENIM.EMPR_Codigo       
	AND ICLP.ENIM_Codigo = ENIM.Codigo      
	LEFT JOIN Plan_Unico_Cuentas PUC      
	ON ENIM.EMPR_Codigo = PUC.EMPR_Codigo      
	AND ENIM.PLUC_Codigo = PUC.Codigo      
	WHERE  ICLP.EMPR_Codigo = @par_EMPR_Codigo        
	AND ICLP.COVE_Codigo = ISNULL(@par_Codigo,ICLP.COVE_Codigo)        
	AND ENIM.Estado = 1
    )         
   SELECT DISTINCT          
	Codigo,        
	Nombre,      
	Operacion,       
	Estado,      
	Valor_Base,      
	Valor_Tarifa,      
	PLUC_Codigo,      
	@CantidadRegistros AS TotalRegistros,          
	@par_NumeroPagina AS PaginaObtener,          
	@par_RegistrosPagina AS RegistrosPagina          
	FROM          
	Pagina          
	WHERE          
	RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                  
END 
GO	
