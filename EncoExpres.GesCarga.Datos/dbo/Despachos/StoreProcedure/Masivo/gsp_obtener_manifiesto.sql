﻿DROP PROCEDURE gsp_obtener_manifiesto
GO
CREATE PROCEDURE gsp_obtener_manifiesto
(              
	@par_EMPR_Codigo SMALLINT,              
	@par_Numero INT              
)              
AS              
BEGIN              
	SELECT              
	1 AS Obtener,              
	ENMC.EMPR_Codigo,              
	ENMC.Numero,              
	ENMC.Numero_Documento,              
	ENMC.Fecha,              
	ENMC.VEHI_Codigo,              
	VEHI.Placa AS PlacaVehiculo,              
	ENMC.TERC_Codigo_Propietario,              
	PROP.Numero_Identificacion AS IdentificacionPropietario,              
	CONCAT(PROP.Razon_Social,PROP.Nombre,' ',PROP.Apellido1,' ',PROP.Apellido2) AS NombrePropietario,              
	ENMC.TERC_Codigo_Tenedor,              
	TENE.Numero_Identificacion AS IdentificacionTenedor,              
	CONCAT(TENE.Razon_Social, TENE.Nombre,' ',TENE.Apellido1,' ',TENE.Apellido2) AS NombreTenedor,              
	ENMC.TERC_Codigo_Conductor,              
	COND.Numero_Identificacion AS IdentificacionConductor,              
	CONCAT(COND.Razon_Social, COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2) AS NombreConductor,              
	COND.Telefonos AS TelefonoConductor,              
	ISNULL(ENMC.TERC_Codigo_Segundo_Conductor,0) AS TERC_Codigo_Segundo_Conductor,              
	SECO.Numero_Identificacion AS IdentificacionSegundoConductor,              
	CONCAT(SECO.Razon_Social,SECO.Nombre,' ',SECO.Apellido1,' ',SECO.Apellido2) AS NombreSegundoConductor,              
	ENMC.TERC_Codigo_Afiliador,              
	AFIL.Numero_Identificacion AS IdentificacionAfiliador,              
	CONCAT(AFIL.Razon_Social,AFIL.Nombre,' ',AFIL.Apellido1,' ',AFIL.Apellido2) AS NombreAfiliador,              
	ENMC.SEMI_Codigo,              
	SEMI.Placa AS PlacaSemirremolque,              
	ENMC.TIVE_Codigo,              
	TIVE.Nombre AS TipoVehiculo,              
	ENMC.RUTA_Codigo,              
	RUTA.Nombre AS NombreRuta,              
	ENMC.Observaciones,              
	ENMC.Fecha_Cumplimiento,              
	ENMC.Cantidad_Total,              
	ENMC.Peso_Total,              
	ENMC.Valor_Flete,              
	ENMC.Valor_Retencion_Fuente,              
	ENMC.Valor_ICA,              
	ENMC.Valor_Otros_Descuentos,              
	ENMC.Valor_Flete_Neto,              
	ENMC.Valor_Anticipo,              
	ENMC.Valor_Pagar,              
	ENMC.TERC_Codigo_Aseguradora,              
	ASEG.Numero_Identificacion AS IdentificacionAseguradora,              
	CONCAT(ASEG.Razon_Social,ASEG.Nombre,' ',ASEG.Apellido1,' ',ASEG.Apellido2) AS NombreAseguradora,              
	ENMC.Numero_Poliza,              
	ENMC.Fecha_Vigencia_Poliza,              
	ENMC.ELPD_Numero,              
	ENMC.ECPD_Numero,              
	ENMC.Finalizo_Viaje,              
	ENMC.Anulado,              
	ENMC.Siniestro,              
	ENMC.Estado,              
	CASE ENMC.Estado WHEN 1 THEN 'Definitivo' ELSE 'Borrador' END AS NombreEstado,              
	ISNULL(ENMC.Numeracion,'') AS Numeracion,              
	ENMC.TIDO_Codigo,              
	TIDO.Nombre AS TipoDocumento,              
	ENMC.USUA_Codigo_Crea,              
	ENMC.Fecha_Crea,              
	ISNULL(ENMC.USUA_Codigo_Modifica,0) AS USUA_Codigo_Modifica,              
	ISNULL(ENMC.Fecha_Modifica,'') AS Fecha_Modifica,              
	ISNULL(ENMC.Fecha_Anula,'') AS Fecha_Anula,              
	ISNULL(ENMC.USUA_Codigo_Anula,0) AS USUA_Codigo_Anula,              
	ISNULL(ENMC.Causa_Anula,'') AS Causa_Anula,              
	ENMC.OFIC_Codigo,              
	OFIC.Nombre AS Oficina,              
	ENMC.CATA_TIMA_Codigo,              
	-- TIMA.Nombre AS TipoManifiesto,              
	ENMC.Numero_Manifiesto_Electronico,              
	ISNULL(ENMC.Fecha_Reporte_Manifiesto_Electronico,'') AS Fecha_Reporte_Manifiesto_Electronico,              
	ISNULL(ENMC.Mensaje_Manifiesto_Electronico,'') AS Mensaje_Manifiesto_Electronico,              
	ISNULL(ENMC.CATA_MAMC_Codigo,0) AS CATA_MAMC_Codigo,              
	ISNULL(MAMA.Nombre,'') AS MotivoAnulacion          
	,ENPD.Numero_Documento as NumeroDocumentoPlanilla    
	,ENMC.Aceptacion_Electronica as Aceptacion_Electronica   
	,TIMA.Codigo  
	,TIMA.Campo1  
	,ENMC.Cantidad_viajes_dia  
	FROM              
	Encabezado_Manifiesto_Carga ENMC LEFT JOIN Vehiculos VEHI ON              
	ENMC.EMPR_Codigo = VEHI.EMPR_Codigo AND ENMC.VEHI_Codigo = VEHI.Codigo              
                
	LEFT JOIN Terceros PROP ON              
	ENMC.EMPR_Codigo = PROP.EMPR_Codigo AND ENMC.TERC_Codigo_Propietario = PROP.Codigo              
                
	LEFT JOIN Terceros TENE ON              
	ENMC.EMPR_Codigo = TENE.EMPR_Codigo AND ENMC.TERC_Codigo_Tenedor = TENE.Codigo              
                
	LEFT JOIN Terceros COND ON              
	ENMC.EMPR_Codigo = COND.EMPR_Codigo AND ENMC.TERC_Codigo_Conductor = COND.Codigo              
                
	LEFT JOIN Terceros SECO ON              
	ENMC.EMPR_Codigo = SECO.EMPR_Codigo              
	AND ENMC.TERC_Codigo_Segundo_Conductor = SECO.Codigo              
                
	LEFT JOIN Terceros AFIL ON              
	ENMC.EMPR_Codigo = AFIL.EMPR_Codigo              
	AND ENMC.TERC_Codigo_Afiliador = AFIL.Codigo              
                
	LEFT JOIN Semirremolques SEMI ON              
	ENMC.EMPR_Codigo = SEMI.EMPR_Codigo              
	AND ENMC.SEMI_Codigo = SEMI.Codigo             
                
	LEFT JOIN V_Tipo_vehiculos TIVE ON              
	ENMC.EMPR_Codigo = TIVE.EMPR_Codigo              
	AND ENMC.TIVE_Codigo = TIVE.Codigo              
                
	LEFT JOIN Rutas RUTA ON              
	ENMC.EMPR_Codigo = RUTA.EMPR_Codigo              
	AND ENMC.RUTA_Codigo = RUTA.Codigo              
                
	LEFT JOIN Terceros ASEG ON              
	ENMC.EMPR_Codigo = ASEG.EMPR_Codigo              
	AND ENMC.TERC_Codigo_Aseguradora = ASEG.Codigo              
                
	LEFT JOIN Tipo_Documentos TIDO ON              
	ENMC.EMPR_Codigo = TIDO.EMPR_Codigo              
	AND ENMC.TIDO_Codigo = TIDO.Codigo              
                
	LEFT JOIN Oficinas OFIC ON              
	ENMC.EMPR_Codigo = OFIC.EMPR_Codigo              
	AND ENMC.OFIC_Codigo = OFIC.Codigo              
                
	-- LEFT JOIN V_Tipo_Manifiesto TIMA ON              
	--ENMC.EMPR_Codigo = TIMA.EMPR_Codigo              
	-- AND ENMC.CATA_TIMA_Codigo = TIMA.Codigo              
                
	LEFT JOIN Detalle_Integraciones_Planilla_Despachos DIPD              
	ON ENMC.EMPR_Codigo = DIPD.EMPR_Codigo AND ENMC.ENPD_Numero = DIPD.ENPD_Numero              
                
	LEFT JOIN V_Motivo_Anulacion_Manifiesto MAMA ON              
	ENMC.EMPR_Codigo = MAMA.EMPR_Codigo              
	AND ENMC.CATA_MAMC_Codigo = MAMA.Codigo             
            
	LEFT JOIN Encabezado_Planilla_Despachos ENPD   ON
	ENMC.EMPR_Codigo = ENPD.EMPR_Codigo
	AND ENMC.ENPD_Numero = ENPD.Numero
	  
	LEFT JOIN Valor_Catalogos AS  TIMA ON   
	ENMC.EMPR_Codigo = TIMA.EMPR_Codigo AND  
	ENMC.CATA_TIMA_Codigo = TIMA.Codigo  
	WHERE              
	ENMC.EMPR_Codigo = @par_EMPR_Codigo              
	AND ENMC.Numero = @par_Numero              
END
GO