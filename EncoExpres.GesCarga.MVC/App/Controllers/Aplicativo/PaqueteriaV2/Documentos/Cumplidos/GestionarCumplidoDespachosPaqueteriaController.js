﻿EncoExpresApp.controller("GestionarCumplidoDespachosPaqueteriaCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'CumplidosFactory', 'PlanillaDespachosFactory',
    'ValorCatalogosFactory', 'TercerosFactory', 'OficinasFactory', 'DocumentosFactory', 'SemirremolquesFactory', 'VehiculosFactory', 'blockUIConfig', 'TarifarioVentasFactory',
    'EncabezadoSolicitudOrdenServiciosFactory', 'RutasFactory', 'RemesaGuiasFactory','ManifiestoFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, CumplidosFactory, PlanillaDespachosFactory,
        ValorCatalogosFactory, TercerosFactory, OficinasFactory, DocumentosFactory, SemirremolquesFactory, VehiculosFactory, blockUIConfig, TarifarioVentasFactory,
        EncabezadoSolicitudOrdenServiciosFactory, RutasFactory, RemesaGuiasFactory,ManifiestoFactory) {
        console.clear();
        $scope.Titulo = 'GESTIONAR CUMPLIDO PLANILLA PAQUETERIA';
        $scope.MapaSitio = $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Cumplido' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.CUMPLIDO_DESPACHOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        //--Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        var VehiculoDespacho;
        var ListadoRemesasManifiesto = [];
        $scope.ListadoDocumentos = [];
        $scope.CadenaFormatos = ".BMP, .GIF, .JPG, .JPEG, .TIF, .TIFF, .PNG, .doc, .docx, .docm, .dotx, .dotm, .dot, .xps, .txt, .xml, .odt, .xlsx, .xlsm, .xlsb, .xltx, .xltm, .xls, .xlt, .xml, .xlam, .xla, .xlw, .xlr, .pdf"
        var RemesasPendientesCumplir = false;
        var PlanillaValidada = false;
        var hoy = new Date();
        var OficinaUsuario = {
            Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
        };

        $scope.ModeloFechaCumplido = new Date();
        $scope.DeshabilitarControlPlanilla = false;
        $scope.DeshabilitarGuardar = false;
        $scope.PlanillaUrbana = false;
        $scope.MostrarTipoCumplido = false;
        //--Transbordo
        var Cliente = {
            Codigo: 0,
            Tarifario: 0
        };
        var CodigoLineaNegocio = 0;
        var CodigoTipoLineaNegocio = 0;
        var TarifarioCompra = {};
        var NumeroOrdenServicio = 0;
        var CodigoTipoDespacho = 0;
        var TipoViajes = false;
        var TipoCupoTonelada = false;
        var TipoCantidadViajes = false;
        //--Transbordo
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        $scope.ListadoEstados = [
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR },
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO }
        ];
        $scope.ModeloEstado = $scope.ListadoEstados[CERO];

        $scope.CargarDatos;
        //--Cargar el combo Tipo Cumplido Manifiesto
        $scope.ListaTipoCumplidoManifiesto = [];
        $scope.ListaTipoCumplidoManifiesto = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CUMPLIDO_MANIFIESTO },
            Sync: true
        }).Datos;
        $scope.TipoCumplidoManifiesto = $linq.Enumerable().From($scope.ListaTipoCumplidoManifiesto).First('$.Codigo ==' + CODIGO_CATALOGO_TIPO_CUMPLIDO_MANIFIESTO_NORMAL);
        //--Cargar el combo Motivo Suspencion Manifiesto
        ListaMotivoSuspensionManifiesto = [];
        $scope.ListaMotivoSuspensionManifiesto = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_MOTIVO_SUSPENCION_MANIFIESTO },
            Sync: true
        }).Datos;
        $scope.MotivoSuspensionManifiesto = $scope.ListaMotivoSuspensionManifiesto[0];
        //--Cargar el combo Consecuencia Suspencion Manifiesto
        $scope.ListaConsecuenciaSuspencionManifiesto = [];
        $scope.ListaConsecuenciaSuspencionManifiesto = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_CONSECUENCIA_SUSPENCION_MANIFIESTO },
            Sync: true
        }).Datos;
        $scope.ConsecuenciaSuspencionManifiesto = $scope.ListaConsecuenciaSuspencionManifiesto[0];
        //Funciones Autocomplete
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos);
                }
            }
            return $scope.ListadoVehiculos;
        };
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        };
        $scope.ListaSemirremolque = [];
        $scope.AutoCompleteSemiRemolque = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = SemirremolquesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListaSemirremolque = ValidarListadoAutocomplete(Response.Datos, $scope.ListaSemirremolque);
                }
            }
            return $scope.ListaSemirremolque;
        };
        $scope.ValidarVehiculoDocumentos = function (Vehiculo, opt) {
            if (Vehiculo != undefined && Vehiculo != null && Vehiculo != '') {
                var ResponsListaNegra = VehiculosFactory.ConsultarEstadoListaNegra({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: Vehiculo.Codigo,
                    Sync: true
                });
                if (ResponsListaNegra.ProcesoExitoso == false) {
                    if (VehiculosFactory.ConsultarDocumentosSoatRTM(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: Vehiculo.Codigo,
                            CodigoDetalleEpos: $scope.IdDetale,
                            Sync: true
                        }).ProcesoExitoso
                    ) {
                        $scope.AsignarDatosVehiculo($scope.VehiculoTransbordo);
                        if (Vehiculo.Estado.Codigo == 0) {
                            ShowError("El vehículo " + Vehiculo.Placa + " esta inactivo por motivo '" + Vehiculo.JustificacionBloqueo + "'");
                            $scope.ConductorTransbordo = '';
                            $scope.TenedorTransbordo = '';
                            $scope.SemirremolqueTransbordo = '';
                            $scope.VehiculoTransbordo = '';
                            return false;
                        } else {
                            var TerceroAlterno = $scope.CargarTercero(Vehiculo.Conductor.Codigo);
                            if (TerceroAlterno.Estado.Codigo == 0) {
                                ShowError("El conductor del vehículo " + Vehiculo.Placa + " de nombre '" + TerceroAlterno.NombreCompleto + "' esta inactivo por motivo '" + TerceroAlterno.JustificacionBloqueo + "'");
                                $scope.ConductorTransbordo = '';
                                $scope.TenedorTransbordo = '';
                                $scope.SemirremolqueTransbordo = '';
                                $scope.VehiculoTransbordo = '';
                                return false;
                            } else {
                                if (VehiculoDespacho.TipoVehiculo.Codigo !== Vehiculo.TipoVehiculo.Codigo) {
                                    ShowError('El tipo vehículo del vehículo ingresado no corresponde al asignado en la orden de servicio');
                                    return false;
                                }
                                else {
                                    //--Consulta Tarifarios
                                    if ($scope.RutaTransbordo != undefined && $scope.RutaTransbordo != null && $scope.RutaTransbordo != '') {
                                        var Response = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.RutaTransbordo.Codigo, Sync: true });
                                                                              var CoincideDestino = false;
                                        for (var i = 0; i < $scope.ModeloListadoRemesasPlanilla.length; i++) {
                                            if ($scope.ModeloListadoRemesasPlanilla[i].CodigoCiudadDestinoRuta == Response.Datos.CiudadDestino.Codigo) {
                                                CoincideDestino = true;
                                                break;
                                            }
                                        }
                                        if (CoincideDestino) {
                                            GestionarTarifas();
                                            AsignarTarifas();
                                            $scope.GestionarFletesCompra();
                                        }
                                        else {
                                            ShowError("La ciudad destino de la ruta no coincide con la ciudad destino de las remesas asociadas");
                                            $scope.RutaTransbordo = '';
                                        }
                                    }
                                    return true;
                                }
                            }
                        }
                    }
                    else {
                        if (opt > 0) {
                            ShowError("El vehículo tiene documentos proximos a vencerse");
                        }
                        $scope.ConductorTransbordo = '';
                        $scope.TenedorTransbordo = '';
                        $scope.SemirremolqueTransbordo = '';
                        $scope.VehiculoTransbordo = '';
                        return false;
                    }
                }
                else {
                    if (opt > 0) {
                        ShowError("El vehículo se encuentra mal matriculado ante el RNDC");
                    }
                    $scope.ConductorTransbordo = '';
                    $scope.TenedorTransbordo = '';
                    $scope.SemirremolqueTransbordo = '';
                    $scope.VehiculoTransbordo = '';
                    return false;
                }
            }
        };
        $scope.AsignarDatosVehiculo = function (Vehiculo) {
            $scope.ConductorTransbordo = $scope.CargarTercero(Vehiculo.Conductor.Codigo);
            $scope.TenedorTransbordo = $scope.CargarTercero(Vehiculo.Tenedor.Codigo);
            if (Vehiculo.Semirremolque !== undefined) {
                if (Vehiculo.Semirremolque.Codigo > 0) {
                    try {
                        $scope.SemirremolqueTransbordo = SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true, Codigo: Vehiculo.Semirremolque.Codigo }).Datos[0];
                    } catch (e) { }
                } else if (Vehiculo.Semirremolque.Placa !== undefined && Vehiculo.Semirremolque.Placa !== '' && Vehiculo.Semirremolque.Placa !== null) {
                    try {
                        $scope.SemirremolqueTransbordo = SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true, Placa: Vehiculo.Semirremolque.Placa }).Datos[0];
                    } catch (e) { }
                }
            }
        };

        function ConsultarTiempos() {
            blockUI.start('Cargando fechas planilla número ' + $scope.ModeloNumeroPlanilla);
            $timeout(function () {
                blockUI.message('Cargando fechas planilla número ' + $scope.ModeloNumeroPlanilla);
            }, 200);
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.ModeloNumeroPlanilla
            };
            blockUI.delay = 1000;
            CumplidosFactory.ConsultarTiempos(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.FechaLlegadaCargue !== FORMATO_FECHA_EXCEPCION_BD) {
                            var f1 = new Date(response.data.Datos.FechaLlegadaCargue);
                            f1.setMilliseconds(0);
                            f1.setSeconds(0);
                            $scope.ModeloFechaLlegadaCargue = f1;
                        } else {
                            $scope.ModeloFechaLlegadaCargue = '';
                        }
                        if (response.data.Datos.FechaInicioCargue !== FORMATO_FECHA_EXCEPCION_BD) {
                            var f2 = new Date(response.data.Datos.FechaInicioCargue);
                            f2.setMilliseconds(0);
                            f2.setSeconds(0);
                            $scope.ModeloFechaInicioCargue = f2;
                        } else {
                            $scope.ModeloFechaInicioCargue = '';
                        }
                        if (response.data.Datos.FechaFinCargue !== FORMATO_FECHA_EXCEPCION_BD) {
                            var f3 = new Date(response.data.Datos.FechaFinCargue);
                            f3.setMilliseconds(0);
                            f3.setSeconds(0);
                            $scope.ModeloFechaFinCargue = f3;
                        } else {
                            $scope.ModeloFechaFinCargue = '';
                        }
                        if (response.data.Datos.FechaLlegadaDescargue !== FORMATO_FECHA_EXCEPCION_BD) {
                            var f4 = new Date(response.data.Datos.FechaLlegadaDescargue);
                            f4.setMilliseconds(0);
                            f4.setSeconds(0);
                            $scope.ModeloFechaLlegadaDescargue = f4;
                        } else {
                            $scope.ModeloFechaLlegadaDescargue = '';
                        }
                        if (response.data.Datos.FechaInicioDescargue !== FORMATO_FECHA_EXCEPCION_BD) {
                            var f5 = new Date(response.data.Datos.FechaInicioDescargue);
                            f5.setMilliseconds(0);
                            f5.setSeconds(0);
                            $scope.ModeloFechaInicioDescargue = f5;
                        } else {
                            $scope.ModeloFechaInicioDescargue = '';
                        }
                        if (response.data.Datos.FechaFinDescargue !== FORMATO_FECHA_EXCEPCION_BD) {
                            var f6 = new Date(response.data.Datos.FechaFinDescargue);
                            f6.setMilliseconds(0);
                            f6.setSeconds(0);
                            $scope.ModeloFechaFinDescargue = f6;
                        } else {
                            $scope.ModeloFechaFinDescargue = '';
                        }

                    }
                    else {
                        ShowError('No se logro consultar fechas de la planilla número ' + $scope.ModeloNumeroPlanilla + '. ' + response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        };

        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                Numero: $scope.ModeloNumero,
                Obtener: 1
            };
            blockUI.delay = 1000;
          
            CumplidosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        //--Detiene Bloqueo Pantalla

                
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                        $scope.ModeloNumero = response.data.Datos.Numero;
                        $scope.ModeloNumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.ModeloFechaCumplido = new Date(response.data.Datos.Fecha);
                        $scope.ModeloNumeroManifiesto = response.data.Datos.NumeroManifiesto;
                        $scope.ModeloNumeroPlanilla = response.data.Datos.NumeroPlanilla;
                        $scope.ModeloPlacaVehiculo = response.data.Datos.Vehiculo.Placa;
                        VehiculoDespacho = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo);
                        $scope.ModeloSemirremolque = response.data.Datos.PlacaSemirremolque;
                        $scope.ModeloTenedor = response.data.Datos.Tenedor.NombreCompleto;
                        $scope.ModeloConductor = response.data.Datos.Conductor.NombreCompleto;
                        $scope.ModeloFechaEntrega = new Date(response.data.Datos.FechaEntrega);
                        $scope.ModeloFechaInicioCargue = new Date(response.data.Datos.FechaInicioCargue);
                        $scope.ModeloFechaFinCargue = new Date(response.data.Datos.FechaFinCargue);
                        $scope.ModeloFechaInicioDescargue = new Date(response.data.Datos.FechaInicioDescargue);
                        $scope.ModeloFechaFinDescargue = new Date(response.data.Datos.FechaFinDescargue);
                        $scope.ModeloFechaLlegadaCargue = new Date(response.data.Datos.FechaLlegadaCargue);
                        $scope.ModeloFechaLlegadaDescargue = new Date(response.data.Datos.FechaLlegadaDescargue);
                        $scope.ModeloMultaExtemporaneidad = response.data.Datos.ValorMultaExtemporaneo;
                        $scope.ModeloNombreEntregoCumplido = response.data.Datos.NombreEntregoCumplido;
                        $scope.ModeloObservaciones = response.data.Datos.Observaciones;
                        $scope.NumeroInternoManifiesto = response.data.Datos.NumeroInternoManifiesto;
                        $scope.NumeroInternoPlanilla = response.data.Datos.NumeroInternoPlanilla;
                        var Planilla = PlanillaDespachosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: $scope.NumeroInternoPlanilla, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA, Sync: true }).Datos;
                        $scope.ModeloRuta = Planilla.Ruta.Nombre;
                        var NumeroManifiesto = Planilla.Manifiesto.Numero
                        $scope.ModeloListadoRemesasManifisestoPlanilla = ManifiestoFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: NumeroManifiesto, Sync: true }).Datos.ListaDetalleManifiesto;

                        //---Tipo de Cumplido y Motivos de suspencion
                        $scope.MostrarTipoCumplido = true;
                        $scope.TipoCumplidoManifiesto = response.data.Datos.TipoCumplidoManifiesto;
                        $scope.MotivoSuspensionManifiesto = response.data.Datos.MotivoSuspensionManifiesto;
                        $scope.ConsecuenciaSuspencionManifiesto = response.data.Datos.ConsecuenciaSuspencionManifiesto;
                        if ($scope.TipoCumplidoManifiesto.Codigo > 0 && $scope.ListaTipoCumplidoManifiesto.length > 0) {
                            $scope.TipoCumplidoManifiesto = $linq.Enumerable().From($scope.ListaTipoCumplidoManifiesto).First('$.Codigo ==' + $scope.TipoCumplidoManifiesto.Codigo);
                        }
                        if ($scope.MotivoSuspensionManifiesto.Codigo > 0 && $scope.ListaMotivoSuspensionManifiesto.length > 0) {
                            $scope.MotivoSuspensionManifiesto = $linq.Enumerable().From($scope.ListaMotivoSuspensionManifiesto).First('$.Codigo ==' + $scope.MotivoSuspensionManifiesto.Codigo);
                        }
                        if ($scope.ConsecuenciaSuspencionManifiesto.Codigo > 0 && $scope.ListaConsecuenciaSuspencionManifiesto.length > 0) {
                            $scope.ConsecuenciaSuspencionManifiesto = $linq.Enumerable().From($scope.ListaConsecuenciaSuspencionManifiesto).First('$.Codigo ==' + $scope.ConsecuenciaSuspencionManifiesto.Codigo);
                        }
                        if ($scope.ConsecuenciaSuspencionManifiesto.Codigo != 21004) {
                            $scope.ListaConsecuenciaSuspencionManifiesto.splice($scope.ListaConsecuenciaSuspencionManifiesto.findIndex(item => item.Codigo === 21004), 1);
                        }
                        if (response.data.Datos.VehiculoTransbordo.Codigo > CERO) {
                            $scope.VehiculoTransbordo = $scope.CargarVehiculos(response.data.Datos.VehiculoTransbordo.Codigo);
                        }
                        if (response.data.Datos.TenedorTransbordo.Codigo > CERO) {
                            $scope.TenedorTransbordo = $scope.CargarTercero(response.data.Datos.TenedorTransbordo.Codigo);
                        }
                        if (response.data.Datos.ConductorTransbordo.Codigo > CERO) {
                            $scope.ConductorTransbordo = $scope.CargarTercero(response.data.Datos.ConductorTransbordo.Codigo);
                        }
                        if (response.data.Datos.SemirremolqueTransbordo.Codigo > CERO) {
                            $scope.SemirremolqueTransbordo = $scope.CargarSemirremolqueCodigo(response.data.Datos.SemirremolqueTransbordo.Codigo);
                        }

                        var tmpRuta = response.data.Datos.RutaTransbordo;
                        var tmpTarifa = response.data.Datos.TarifaTransbordo;
                        var tmpTipoTarifa = response.data.Datos.TipoTarifaTransbordo;

                        if ($scope.TipoCumplidoManifiesto.Codigo == CODIGO_CATALOGO_TIPO_CUMPLIDO_MANIFIESTO_SUSPENSION) {
                            CargarInformacionTransbordo();
                            $scope.RutaTransbordo = $linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + tmpRuta.Codigo);
                            $scope.ValidarRutaTransbordo();
                            $scope.TarifaTransbordo = $linq.Enumerable().From($scope.ListadoTarifasTransporte).First('$.Codigo ==' + tmpTarifa.Codigo);
                            $scope.TipoTarifaTransbordo = $linq.Enumerable().From($scope.ListadoTipoTarifaFiltradas).First('$.Codigo ==' + tmpTipoTarifa.Codigo);
                            $scope.CantidadTransbordo = MascaraValores(response.data.Datos.CantidadTransbordo);
                            $scope.PesoTransbordo = MascaraValores(response.data.Datos.PesoTransbordo);
                            $scope.FleteTransportadorTransbordo = MascaraValores($scope.TipoTarifaTransbordo.ValorFlete);
                            $scope.TotalFleteTransportadorTransbordo = MascaraValores(response.data.Datos.ValorFleteTransportadorTransbordo);
                        }
                        //---Tipo de Cumplido y Motivos de suspencion
                        $scope.ModeloListadoRemesasCumplido = response.data.Datos.ListaCumplidoRemesas;

                        for (x = 0; x < $scope.ModeloListadoRemesasCumplido.length; x++) {
                            $scope.ModeloListadoRemesasCumplido[x].Seleccionado = true;

                        }

                        $scope.ModeloOficina = response.data.Datos.Oficina.Nombre;
                        $scope.ModeloEstado = response.data.Datos.Estado;
                        $scope.ManifiestoTransbordo = response.data.Datos.ManifiestoTransbordo;
                        $scope.PlanillaTransbordo = response.data.Datos.PlanillaTransbordo;

                        if ($scope.ListadoEstados !== undefined && $scope.ListadoEstados !== null) {
                            $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.ModeloEstado);
                        }
                        $scope.DeshabilitarControlPlanilla = true;
                        if ($scope.ModeloEstado.Codigo === ESTADO_DEFINITIVO) {
                            $scope.DeshabilitarCampo = true;
                            $scope.DeshabilitarGuardar = true;
                        }

                        $scope.ListadoDocumentos = response.data.Datos.ListaDocumentos;
                        $scope.ListadoDocumentos.forEach(function (item) {
                            item.ValorDocumento = 1;
                            item.AplicaEliminar = 1;
                            item.temp = true;
                        });
                        PlanillaValidada = true;
                    }
                    else {
                        //--Detiene Bloqueo Pantalla
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                        ShowError('No se logro consultar el cumplido número ' + $scope.ModeloNumeroDocumento + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarCumplidoDespachosPaqueteria';
                    }
                }, function (response) {
                    //--Detiene Bloqueo Pantalla
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                    ShowError(response.statusText);
                        document.location.href = '#!ConsultarCumplidoDespachosPaqueteria';
                });

            blockUI.stop();
        }

        $scope.ValidarPlanilla = function (Planilla, opt) {
          
            if (Planilla !== undefined && Planilla !== '' && Planilla !== null && Planilla > 0) {
                PlanillaValidada = false;
                var filtros = {};
                if (opt > 0) {//Buscar por manifiesto
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                        Manifiesto: { NumeroDocumento: Planilla },
                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        Obtener: 1,
                        Sync: true
                    };
                }
                else {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                        NumeroDocumento: Planilla,
                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        Obtener: 1,
                        Sync: true
                    };
                }
                 
                var RespPlanillaObt1 = PlanillaDespachosFactory.Obtener(filtros);

                //Validacion del proceso por empresa 
                if ($scope.Sesion.UsuarioAutenticado.ManejoTiemposCumplidoPaqueteria) {

                    //Tiempo sugerido en minutos
                    var TiempoSugerido = 120

                    //Asignación Tiempos Cargue/
                    //asignacion de fecha-Hora salida de la planilla 
                    var FechaHoraSalida = new Date(RespPlanillaObt1.Datos.FechaHoraSalida);
                                  
                    //se asigna  la fecha-hora salida   
                    $scope.ModeloFechaFinCargue = FechaHoraSalida.withoutSeconds().withoutMilliseconds();


                    //se realiza la resta del tiempo en mimutos a la fecha-hora inicio cargue  
                    var FechaInicioCargue = new Date(FechaHoraSalida)  
                    FechaInicioCargue.setMinutes(FechaInicioCargue.getMinutes() - TiempoSugerido);
                    $scope.ModeloFechaInicioCargue = FechaInicioCargue.withoutSeconds().withoutMilliseconds();

                    console.log(RespPlanillaObt1.Datos);
                  

                  //se realiza la resta del tiempo en mimutos a la fecha-hora llegada cargue   
                    var FechaLlegadaCargue = new Date(FechaInicioCargue)
                    FechaLlegadaCargue.setMinutes(FechaLlegadaCargue.getMinutes() - TiempoSugerido - 30);  
                    $scope.ModeloFechaLlegadaCargue = FechaLlegadaCargue.withoutSeconds().withoutMilliseconds();

                  //Asignación Tiempos  Descargue///
 
                    //Asignacion de Valor Duracion horas de la planilla
                    var DuracionHoras = RespPlanillaObt1.Datos.Ruta.DuracionHoras
                    
                    //asignacion de valir fella fin de viaje a  hora llegada
                    var fechaHoraLlegada = new Date($scope.ModeloFechaFinCargue); 

                    //Adicion de las horas de duracion de la ruta a la fecha Fin de Cargue 
                    fechaHoraLlegada.setHours(fechaHoraLlegada.getHours() + DuracionHoras);  
                    $scope.ModeloFechaLlegadaDescargue = fechaHoraLlegada 

                 
                   //Adicion  del tiempo sugerido a la  fecha Hora Llegada del viaje 
                    var FechaInicioDescargue = new Date(fechaHoraLlegada)   
                    FechaInicioDescargue.setMinutes(FechaInicioDescargue.getMinutes() + TiempoSugerido);  
                    $scope.ModeloFechaInicioDescargue = FechaInicioDescargue
                     
                    //Adicion  del tiempo sugerido a la  fecha Hora Inicio Descargue  del viaje 
                    var FechaFinDescargue = new Date(FechaInicioDescargue) 
                    FechaFinDescargue.setMinutes(FechaFinDescargue.getMinutes() + TiempoSugerido);
                    $scope.ModeloFechaFinDescargue = FechaFinDescargue 


                } 



                


                if (RespPlanillaObt1.ProcesoExitoso == true && RespPlanillaObt1.Datos.Numero > 0) {
                    if (RespPlanillaObt1.Datos.Anulado == 1 || RespPlanillaObt1.Datos.Estado.Codigo == ESTADO_BORRADOR) {
                        ShowError('La planilla número ' + Planilla + ' se encuentra anulada o en estado borrador');
                        PlanillaValidada = false;
                        LimpiarInformacion();
                    }
                    else {
                        if (RespPlanillaObt1.Datos.Cumplido.Numero > 0) {
                            ShowError('La planilla número ' + Planilla + ' ya se encuentra asociada a otro cumplido');
                            PlanillaValidada = false;
                            LimpiarInformacion();
                        }
                        else {
                            $scope.NumeroInternoPlanilla = RespPlanillaObt1.Datos.Numero;
                            $scope.ModeloNumeroPlanilla = RespPlanillaObt1.Datos.NumeroDocumento;
                            $scope.ModeloNumeroManifiesto = RespPlanillaObt1.Datos.Manifiesto.NumeroDocumento;
                            $scope.NumeroInternoManifiesto = RespPlanillaObt1.Datos.Manifiesto.Numero;

                            if (RespPlanillaObt1.Datos.Manifiesto.Numero === 0) {
                                $scope.PlanillaUrbana = true;
                                $scope.TipoCumplidoManifiesto = $linq.Enumerable().From($scope.ListaTipoCumplidoManifiesto).First('$.Codigo ==' + CODIGO_CATALOGO_TIPO_CUMPLIDO_MANIFIESTO_NORMAL);
                            }
                            else {
                                $scope.PlanillaUrbana = false;
                            }

                            $scope.ModeloPlacaVehiculo = RespPlanillaObt1.Datos.Vehiculo.Placa;
                            VehiculoDespacho = $scope.CargarVehiculos(RespPlanillaObt1.Datos.Vehiculo.Codigo);
                            $scope.ModeloSemirremolque = RespPlanillaObt1.Datos.Semirremolque.Placa;
                            $scope.ModeloTenedor = RespPlanillaObt1.Datos.NombreTenedor;
                            $scope.ModeloConductor = RespPlanillaObt1.Datos.NombreConductor;
                            $scope.ModeloFechaEntrega = new Date();
                            $scope.ModeloRuta = RespPlanillaObt1.Datos.Ruta.Nombre;
                            $scope.ModeloListadoRemesasPlanilla = RespPlanillaObt1.Datos.ListadoRemesas;
                            $scope.ModeloListadoRemesasManifisestoPlanilla = ManifiestoFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: RespPlanillaObt1.Datos.Manifiesto.Numero, Sync: true }).Datos.ListaDetalleManifiesto;
                            $scope.ModeloListadoRemesasPlanilla.forEach(item => {
                                item.Seleccionado = true;
                                item.CiudadOrigen = $scope.CargarCiudad(RemesaGuiasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.NumeroInternoRemesa, Sync: true }).Datos.Remesa.Remitente.Ciudad.Codigo).Nombre;
                                item.CiudadDestino = $scope.CargarCiudad(RemesaGuiasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.NumeroInternoRemesa, Sync: true }).Datos.Remesa.Destinatario.Ciudad.Codigo).Nombre;
                                item.FechaRecibe = new Date();
                                item.NombreRecibe = item.NombreCliente;
                                var Tercero = $scope.CargarTercero(item.CodigoCliente);
                                item.IdentificacionRecibe = Tercero == undefined ? '': Tercero.NumeroIdentificacion;
                                
                            });
                             ListadoRemesasManifiesto = [];
                            $scope.ModeloListadoRemesasManifisestoPlanilla.forEach(item => {
                                var Tercero = $scope.CargarTercero(item.Cliente.Codigo);
                                //var RemesaM = RemesaGuiasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.Remesa.Numero, Sync: true }).Datos;
                                ListadoRemesasManifiesto.push({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    NumeroRemesa: item.Remesa.Numero,
                                    FechaRecibe: new Date(),                                    
                                    NombreRecibe : item.Cliente.NombreCompleto,
                                    IdentificacionRecibe: Tercero == undefined ? '' : Tercero.NumeroIdentificacion,
                                    CantidadRecibida: RespPlanillaObt1.Datos.Cantidad,
                                    PesoRecibido: RespPlanillaObt1.Datos.Peso,
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    Oficina: {Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo}
                                });
                                
                            });
                           // $scope.ModeloListadoRemesasManifisestoPlanilla = ListadoRemesasManifiesto;

                            for (x = 0; x < $scope.ModeloListadoRemesasPlanilla.length; x++) {
                                $scope.ModeloListadoRemesasPlanilla[x].CantidadRecibida = MascaraValores($scope.ModeloListadoRemesasPlanilla[x].CantidadRemesa);
                                $scope.ModeloListadoRemesasPlanilla[x].PesoRecibido = MascaraValores($scope.ModeloListadoRemesasPlanilla[x].PesoRemesa);
                                $scope.ModeloListadoRemesasPlanilla[x].CantidadFaltante = 0;
                                $scope.ModeloListadoRemesasPlanilla[x].PesoFaltante = 0;
                                $scope.ModeloListadoRemesasPlanilla[x].PesoCargue = MascaraValores($scope.ModeloListadoRemesasPlanilla[x].PesoRemesa);
                                $scope.ModeloListadoRemesasPlanilla[x].PesoDescargue = MascaraValores($scope.ModeloListadoRemesasPlanilla[x].PesoRemesa);
                            }
                            //--Condicion Peso
                            var filtroterc = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Codigo: $scope.ModeloListadoRemesasPlanilla.length == 0 ? $scope.ModeloListadoRemesasManifisestoPlanilla[0].Cliente.Codigo : $scope.ModeloListadoRemesasPlanilla[0].CodigoCliente,
                                Sync: true
                            };
                            var RespTercero = TercerosFactory.Obtener(filtroterc);
                            if (RespTercero.ProcesoExitoso == true) {
                                $scope.CondicionesPeso = RespTercero.Datos.CondicionesPeso;
                                Cliente.Codigo = RespTercero.Datos.Codigo;
                                Cliente.Tarifario = RespTercero.Datos.Cliente.Tarifario.Codigo;
                            }
                            else {
                                ShowError('No se logro consultar el tercero ');
                            }
                            //--Condicion Peso
                            //--Informacion para Transbordo
                            CodigoLineaNegocio = RespPlanillaObt1.Datos.LineaNegocioTransportes.Codigo;
                            CodigoTipoLineaNegocio = RespPlanillaObt1.Datos.TipoLineaNegocioTransportes.Codigo;
                            NumeroOrdenServicio = RespPlanillaObt1.Datos.ListadoRemesas.length > 0 ? RespPlanillaObt1.Datos.ListadoRemesas[0].NumeroOrdenServicio : 0;
                            //--Informacion para Transbordo
                            //---Tiempo Planilla
                            var FiltroTiempos = RespPlanillaObt1.Datos;
                            FiltroTiempos.Sync = true;
                            var responPlanillaTiempos = PlanillaDespachosFactory.Obtener_Detalle_Tiempos(FiltroTiempos);
                            if (responPlanillaTiempos.ProcesoExitoso == true) {
                                if (responPlanillaTiempos.Datos.length > 0) {
                                    for (var j = 0; j < responPlanillaTiempos.Datos.length; j++) {
                                        var itemtiempo = responPlanillaTiempos.Datos[j];
                                        if (itemtiempo.SitioReporteSeguimientoVehicular.Codigo == 8203) {
                                            $scope.ModeloFechaLlegadaCargue = new Date(itemtiempo.FechaHora);
                                        }
                                        if (itemtiempo.SitioReporteSeguimientoVehicular.Codigo == 8204) {
                                            $scope.ModeloFechaInicioCargue = new Date(itemtiempo.FechaHora);
                                        }
                                        if (itemtiempo.SitioReporteSeguimientoVehicular.Codigo == 8205) {
                                            $scope.ModeloFechaFinCargue = new Date(itemtiempo.FechaHora);
                                        }
                                        if (itemtiempo.SitioReporteSeguimientoVehicular.Codigo == 8207) {
                                            $scope.ModeloFechaLlegadaDescargue = new Date(itemtiempo.FechaHora);
                                        }
                                        if (itemtiempo.SitioReporteSeguimientoVehicular.Codigo == 8208) {
                                            $scope.ModeloFechaInicioDescargue = new Date(itemtiempo.FechaHora);
                                        }
                                        if (itemtiempo.SitioReporteSeguimientoVehicular.Codigo == 8209) {
                                            $scope.ModeloFechaFinDescargue = new Date(itemtiempo.FechaHora);
                                        }
                                    }
                                }
                            }
                            //---Tiempo Planilla
                            $scope.ModeloListadoRemesasPlanilla.forEach(function (item) {
                                if (item.Cumplido === 1) {
                                    $scope.DeshabilitarCampo = true;
                                }
                                else {
                                    RemesasPendientesCumplir = true;
                                    $scope.DeshabilitarCampo = false;
                                }
                            });
                            PlanillaValidada = true;
                            $scope.MostrarTipoCumplido = true;
                        }
                    }
                }
                else {
                    ShowError('La planilla número ' + Planilla + ' no se encuentra en el sistema');
                    PlanillaValidada = false;
                    LimpiarInformacion();
                }
            }
        };

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {

            $scope.ConteoDocumentos = 0;
            if ($scope.ListadoDocumentos !== undefined && $scope.ListadoDocumentos !== null) {
                $scope.ConteoDocumentos = $scope.ListadoDocumentos.length;
            }


            closeModal('modalConfirmacionGuardar');
            window.scrollTo(top, top);
            if (DatosRequeridos()) {
                var Modelo = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_CUMPLIDO_PLANILLA_PAQUETERIA },
                    Numero: $scope.ModeloNumero,
                    NumeroDocumento: $scope.ModeloNumeroDocumento,
                    Fecha: $scope.ModeloFechaCumplido,
                    FechaEntrega: $scope.ModeloFechaEntrega,
                    NumeroPlanilla: $scope.NumeroInternoPlanilla,
                    NumeroManifiesto: $scope.NumeroInternoManifiesto,
                    FechaLlegadaCargue: $scope.ModeloFechaLlegadaCargue,
                    FechaLlegadaDescargue: $scope.ModeloFechaLlegadaDescargue,
                    FechaInicioCargue: $scope.ModeloFechaInicioCargue,
                    FechaFinCargue: $scope.ModeloFechaFinCargue,
                    FechaInicioDescargue: $scope.ModeloFechaInicioDescargue,
                    FechaFinDescargue: $scope.ModeloFechaFinDescargue,
                    ValorMultaExtemporaneo: $scope.ModeloMultaExtemporaneidad,
                    NombreEntregoCumplido: $scope.ModeloNombreEntregoCumplido,
                    Observaciones: $scope.ModeloObservaciones,
                    Estado: $scope.ModeloEstado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Oficina: { Codigo: OficinaUsuario.Codigo },
                    //ListaCumplidoRemesas: $scope.RemesasCumplir,
                    ListaRemesasManifiesto: ListadoRemesasManifiesto,
                    TipoCumplidoManifiesto: $scope.TipoCumplidoManifiesto,
                    MotivoSuspensionManifiesto: $scope.MotivoSuspensionManifiesto,
                    ConsecuenciaSuspencionManifiesto: $scope.ConsecuenciaSuspencionManifiesto,
                    VehiculoTransbordo: $scope.VehiculoTransbordo,
                    SemirremolqueTransbordo: $scope.SemirremolqueTransbordo,
                    RutaTransbordo: $scope.RutaTransbordo,
                    TarifaTransbordo: $scope.TarifaTransbordo,
                    TipoTarifaTransbordo: $scope.TipoTarifaTransbordo,
                    CantidadTransbordo: $scope.CantidadTransbordo != undefined ? MascaraNumero($scope.CantidadTransbordo) : 0,
                    PesoTransbordo: $scope.PesoTransbordo != undefined ? MascaraNumero($scope.PesoTransbordo) : 0,
                    ValorFleteTransportadorTransbordo: $scope.TotalFleteTransportadorTransbordo,
                    //PropietarioTransbordo: $scope.PropietarioTransbordo,
                    ConductorTransbordo: $scope.ConductorTransbordo,
                    TenedorTransbordo: $scope.TenedorTransbordo,
                    ConteoDocumentos: $scope.ConteoDocumentos
                };
                CumplidosFactory.Guardar(Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > 0) {
                                if ($scope.ModeloNumero === 0) {
                                    ShowSuccess('Se guardó el cumplido ' + response.data.Datos);
                                }
                                else {
                                    ShowSuccess('Se modificó el cumplido ' + response.data.Datos);
                                }
                                location.href = '#!ConsultarCumplidoDespachosPaqueteria/' + response.data.Datos;
                            }
                            else {
                                switch (response.data.Datos) {
                                    case -1:
                                        ShowError('Ya se guardó un cumplido con la planilla ingresada');
                                        break;
                                    case -2:
                                        ShowError('La planilla ingresada se cuentra anulada o en estado borrador');
                                        break
                                    case -3:
                                        ShowError('La planilla tiene viajes sin finalizar');
                                        break;
                                    default:
                                        ShowError(response.data.MensajeOperacion);
                                        break;
                                }
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var Continuar = true;
            hoy = new Date();

            if ($scope.ModeloFechaCumplido === undefined || $scope.ModeloFechaCumplido === '' || $scope.ModeloFechaCumplido == null) {
                $scope.MensajesError.push('Debe ingresar la fecha del cumplido');
                Continuar = false
            }
            if ($scope.ModeloNumeroPlanilla === undefined || $scope.ModeloNumeroPlanilla === '' || $scope.ModeloNumeroPlanilla == null || PlanillaValidada == false) {
                $scope.MensajesError.push('La planilla que intenta cumplir no es nacional');
                Continuar = false
            }
            if ($scope.ModeloFechaEntrega == undefined || $scope.ModeloFechaEntrega == '' || $scope.ModeloFechaEntrega == null) {
                $scope.MensajesError.push('Debe ingresar la fecha de la entrega');
                Continuar = false
            }
            else {
                var valFechaEntrega = ComparaFecha(hoy, $scope.ModeloFechaEntrega);
                if (valFechaEntrega == FECHA_ES_MENOR || valFechaEntrega == null) {
                    $scope.MensajesError.push('La fecha de la entrega debe ser inferior a la fecha hoy');
                    Continuar = false;
                }
            }

            if ($scope.ModeloFechaLlegadaCargue == undefined || $scope.ModeloFechaLlegadaCargue == '' || $scope.ModeloFechaLlegadaCargue == null) {
                $scope.MensajesError.push('Debe ingresar la fecha y hora de llegada del cargue');
                Continuar = false
            }
            else {
                var valFechaInicargue = ComparaFecha(hoy, $scope.ModeloFechaLlegadaCargue);
                if (valFechaInicargue == FECHA_ES_MENOR || valFechaInicargue == null) {
                    $scope.MensajesError.push('La fecha y hora de llegada del cargue debe ser inferior a la fecha y hora de hoy');
                    Continuar = false;
                }
            }


            if ($scope.ModeloFechaInicioCargue == undefined || $scope.ModeloFechaInicioCargue == '' || $scope.ModeloFechaInicioCargue == null) {
                $scope.MensajesError.push('Debe ingresar la fecha y hora del inicio del cargue');
                Continuar = false
            }
            else {
                var valFechaInicargue = ComparaFecha(hoy, $scope.ModeloFechaInicioCargue);
                if (valFechaInicargue == FECHA_ES_MENOR || valFechaInicargue == null) {
                    $scope.MensajesError.push('La fecha y hora del inicio del cargue debe ser inferior a la fecha y hora de hoy');
                    Continuar = false;
                }
            }

            if ($scope.ModeloFechaFinCargue == undefined || $scope.ModeloFechaFinCargue == '' || $scope.ModeloFechaFinCargue == null) {
                $scope.MensajesError.push('Debe ingresar la fecha y hora del fin del cargue');
                Continuar = false
            }
            else {
                var valFechaFincargue = ComparaFecha(hoy, $scope.ModeloFechaFinCargue);
                if (valFechaFincargue == FECHA_ES_MENOR || valFechaFincargue == null) {
                    $scope.MensajesError.push('La fecha y hora del fin del cargue debe ser inferior a la fecha y hora de hoy');
                    Continuar = false;
                }
            }

            if (($scope.ModeloFechaInicioCargue != undefined && $scope.ModeloFechaInicioCargue != '' && $scope.ModeloFechaInicioCargue != null) &&
                ($scope.ModeloFechaLlegadaCargue != undefined && $scope.ModeloFechaLlegadaCargue != '' && $scope.ModeloFechaLlegadaCargue != null) &&
                ($scope.ModeloFechaFinCargue != undefined && $scope.ModeloFechaFinCargue != '' && $scope.ModeloFechaFinCargue != null)

            ) {
                if ($scope.ModeloFechaLlegadaCargue >= $scope.ModeloFechaInicioCargue) {
                    $scope.MensajesError.push('La fecha y hora del inicio cargue debe ser posterior a la fecha y hora de llegada cargue');
                    Continuar = false;
                }
                if ($scope.ModeloFechaInicioCargue >= $scope.ModeloFechaFinCargue) {
                    $scope.MensajesError.push('La fecha y hora del fin cargue debe ser posterior a la fecha y hora del inicio cargue');
                    Continuar = false;
                }
            }


            if ($scope.ModeloFechaLlegadaDescargue == undefined || $scope.ModeloFechaLlegadaDescargue == '' || $scope.ModeloFechaLlegadaDescargue == null) {
                $scope.MensajesError.push('Debe ingresar la fecha y hora de llegada del descargue');
                Continuar = false
            }
            else {
                var valFechaInidescargue = ComparaFecha(hoy, $scope.ModeloFechaLlegadaDescargue);
                if (valFechaInidescargue == FECHA_ES_MENOR || valFechaInidescargue == null) {
                    $scope.MensajesError.push('La fecha y hora de llegada del descargue debe ser inferior a la fecha y hora de hoy');
                    Continuar = false;
                }
            }

            if ($scope.ModeloFechaInicioDescargue == undefined || $scope.ModeloFechaInicioDescargue == '' || $scope.ModeloFechaInicioDescargue == null) {
                $scope.MensajesError.push('Debe ingresar la fecha y hora del inicio del descargue');
                Continuar = false
            }
            else {
                var valFechaInidescargue = ComparaFecha(hoy, $scope.ModeloFechaInicioDescargue);
                if (valFechaInidescargue == FECHA_ES_MENOR || valFechaInidescargue == null) {
                    $scope.MensajesError.push('La fecha y hora del inicio del descargue debe ser inferior a la fecha y hora de hoy');
                    Continuar = false;
                }
            }


            if ($scope.ModeloFechaFinDescargue == undefined || $scope.ModeloFechaFinDescargue == '' || $scope.ModeloFechaFinDescargue == null) {
                $scope.MensajesError.push('Debe ingresar la fecha y hora del fin del descargue');
                Continuar = false
            }
            else {
                var valFechaFindescargue = ComparaFecha(hoy, $scope.ModeloFechaFinDescargue);
                if (valFechaFindescargue == FECHA_ES_MENOR || valFechaFindescargue == null) {
                    $scope.MensajesError.push('La fecha y hora del fin del descargue debe ser inferior a la fecha y hora de hoy');
                    Continuar = false;
                }
            }


            if (($scope.ModeloFechaInicioDescargue != undefined && $scope.ModeloFechaInicioDescargue != '' && $scope.ModeloFechaInicioDescargue != null) &&
                ($scope.ModeloFechaLlegadaDescargue != undefined && $scope.ModeloFechaLlegadaDescargue != '' && $scope.ModeloFechaLlegadaDescargue != null) &&
                ($scope.ModeloFechaFinDescargue != undefined && $scope.ModeloFechaFinDescargue != '' && $scope.ModeloFechaFinDescargue != null)

            ) {
                if ($scope.ModeloFechaLlegadaDescargue >= $scope.ModeloFechaInicioDescargue) {
                    $scope.MensajesError.push('La fecha y hora del inicio descargue debe ser posterior a la fecha y hora de llegada descargue');
                    Continuar = false;
                }
                if ($scope.ModeloFechaInicioDescargue >= $scope.ModeloFechaFinDescargue) {
                    $scope.MensajesError.push('La fecha y hora del fin descargue debe ser posterior a la fecha y hora del inicio descargue');
                    Continuar = false;
                }
            }
            if (($scope.ModeloFechaFinCargue != undefined && $scope.ModeloFechaFinCargue != '' && $scope.ModeloFechaFinCargue != null) &&
                ($scope.ModeloFechaInicioDescargue != undefined && $scope.ModeloFechaInicioDescargue != '' && $scope.ModeloFechaInicioDescargue != null)) {
                if ($scope.ModeloFechaFinCargue >= $scope.ModeloFechaLlegadaDescargue) {
                    $scope.MensajesError.push('La fecha y hora de llegada descargue debe ser posterior a la fecha y hora del fin cargue');
                    Continuar = false;
                }
            }
            if ($scope.ModeloNombreEntregoCumplido == undefined || $scope.ModeloNombreEntregoCumplido == '' || $scope.ModeloNombreEntregoCumplido == null) {
                $scope.ModeloNombreEntregoCumplido = '';
            }
            if ($scope.ModeloObservaciones == undefined || $scope.ModeloObservaciones == '' || $scope.ModeloObservaciones == null) {
                $scope.ModeloObservaciones = '';
            }

            //if ($scope.TipoCumplidoManifiesto.Codigo == CODIGO_CATALOGO_TIPO_CUMPLIDO_MANIFIESTO_NORMAL && ($scope.ModeloNumero == 0 || $scope.ModeloNumero == undefined)) {
            //    if ($scope.ModeloListadoRemesasPlanilla == undefined || $scope.ModeloListadoRemesasPlanilla == null) {
            //        Continuar = false;
            //        $scope.MensajesError.push('No existen remesas asociadas a la planilla para cumplir');
            //    }
            //    else {
            //        Continuar = ValidarRemesas();
            //    }
            //}

            if ($scope.TipoCumplidoManifiesto.Codigo == CODIGO_CATALOGO_TIPO_CUMPLIDO_MANIFIESTO_SUSPENSION && $scope.ConsecuenciaSuspencionManifiesto.Codigo != 21004) {
                if ($scope.VehiculoTransbordo == undefined || $scope.VehiculoTransbordo == '' || $scope.VehiculoTransbordo == null) {
                    $scope.MensajesError.push('Debe ingresar el vehículo transbordo');
                    Continuar = false;
                }
                if ($scope.ConductorTransbordo == undefined || $scope.ConductorTransbordo == '' || $scope.ConductorTransbordo == null) {
                    $scope.MensajesError.push('Debe ingresar el conductor transbordo');
                    Continuar = false;
                }
                if ($scope.TenedorTransbordo == undefined || $scope.TenedorTransbordo == '' || $scope.TenedorTransbordo == null) {
                    $scope.MensajesError.push('Debe ingresar el tenedor transbordo');
                    Continuar = false;
                }
                if ($scope.RutaTransbordo == undefined || $scope.RutaTransbordo == '' || $scope.RutaTransbordo == null) {
                    $scope.MensajesError.push('Debe ingresar la ruta transbordo');
                    Continuar = false;
                }
                if ($scope.TarifaTransbordo == undefined || $scope.TarifaTransbordo == '' || $scope.TarifaTransbordo == null) {
                    $scope.MensajesError.push('Debe ingresar la tarifa transbordo');
                    Continuar = false;
                }
                if ($scope.CantidadTransbordo == undefined || $scope.CantidadTransbordo == '' || $scope.CantidadTransbordo == null) {
                    $scope.MensajesError.push('Debe ingresar la cantidad transbordo');
                    Continuar = false;
                }
                if ($scope.PesoTransbordo == undefined || $scope.PesoTransbordo == '' || $scope.PesoTransbordo == null) {
                    $scope.MensajesError.push('Debe ingresar el peso transbordo');
                    Continuar = false;
                }
                if ($scope.TotalFleteTransportadorTransbordo == undefined || $scope.TotalFleteTransportadorTransbordo == '' || $scope.TotalFleteTransportadorTransbordo == null) {
                    $scope.MensajesError.push('Debe ingresar el valor flete transportador transbordo');
                    Continuar = false;
                }
            }
            else {
                //if ($scope.TipoCumplidoManifiesto.Codigo == CODIGO_CATALOGO_TIPO_CUMPLIDO_MANIFIESTO_SUSPENSION && $scope.ConsecuenciaSuspencionManifiesto.Codigo == 21004 && ($scope.ModeloNumero == 0 || $scope.ModeloNumero == undefined)) {
                //    if ($scope.ModeloListadoRemesasPlanilla == undefined || $scope.ModeloListadoRemesasPlanilla == null) {
                //        Continuar = false;
                //        $scope.MensajesError.push('No existen remesas asociadas a la planilla para cumplir');
                //    }
                //    else {
                //        Continuar = ValidarRemesas();
                //    }
                //}
            }

            if ($scope.ModeloOficina.Codigo == -1) { //TODAS
                Continuar = false;
                $scope.MensajesError.push('Debe seleccionar la oficina');
            }

            if ($scope.ModeloEstado.Codigo == -1) { //TODOS
                Continuar = false;
                $scope.MensajesError.push('Debe seleccionar el estado');
            }
            return Continuar;
        }

        function LimpiarInformacion() {
            //--Transbordo
            $scope.VehiculoTransbordo = '';
            $scope.SemirremolqueTransbordo = '';
            $scope.ConductorTransbordo = '';
            $scope.TenedorTransbordo = '';
            $scope.RutaTransbordo = '';
            $scope.TarifaTransbordo = '';
            $scope.ListadoTarifasTransporte = [];
            $scope.TipoTarifaTransbordo = '';
            $scope.ListadoTipoTarifaFiltradas = [];
            $scope.CantidadTransbordo = '';
            $scope.PesoTransbordo = '';
            $scope.FleteTransportadorTransbordo = '';
            $scope.TotalFleteTransportadorTransbordo = '';
            $scope.MostrarTipoCumplido = false;
            $scope.TipoCumplidoManifiesto = $linq.Enumerable().From($scope.ListaTipoCumplidoManifiesto).First('$.Codigo ==' + CODIGO_CATALOGO_TIPO_CUMPLIDO_MANIFIESTO_NORMAL);
            //--Transbordo
        }
        // -------Documentos ------------------------------//

        $scope.AgregarFilaDocumento = function () {

            if ($scope.ListadoDocumentos.length < 5) {
                var itmDoc = {
                    NombreAsignado: '',
                    AplicaDescargar: 0,
                    AplicaArchivo: 1,
                    ValorDocumento: 0,
                    TipoArchivoDocumento: $scope.CadenaFormatos
                }
                $scope.ListadoDocumentos.push(itmDoc);
            } else {
                ShowError("Máximo se permite cargar cinco (5) documentos")
            }


        }

        $scope.CargarArchivo = function (element) {

            $scope.IdPosiction = parseInt(element.id, 10)
            var continuar = true;

            if (element.files.length > 0) {

                $scope.PDF = element
                $scope.TipoDocumento = element
                blockUI.start();
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento = angular.element(this.ItemFoto)[0];
                } else {
                    angular.element(this.item)[0].Temporal = true
                    $scope.Documento = angular.element(this.item)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos: 0");
                    continuar = false;
                }
                if (continuar == true) {

                    if (element.files[0].size >= 3000000) //3 MB
                    {
                        ShowError("El archivo " + element.files[0].name + " supera los " + "3 MB" + ", intente con otro archivo", false);
                        continuar = false;
                    }

                    var Formatos = $scope.CadenaFormatos.split(',');
                    var cont = 0;
                    for (var i = 0; i < Formatos.length; i++) {
                        Formatos[i] = Formatos[i].replace(' ', '')
                        var Extencion = element.files[0].name.split('.')
                        //if (element.files[0].type == Formatos[i]) {
                        if ('.' + (Extencion[1].toUpperCase()) == Formatos[i].toUpperCase()) {
                            cont++
                        }


                    }
                    if (cont == 0) {
                        ShowError("Archivo invalido, por favor verifique el formato del archivo.  Formatos permitidos:" + $scope.CadenaFormatos);
                        continuar = false;
                    }


                }

                if (continuar == true) {
                    if ($scope.Documento.Archivo != undefined && $scope.Documento.Archivo != '' && $scope.Documento.Archivo != null) {
                        $scope.Documento.ArchivoTemporal = element.files[0]
                        showModal('modalConfirmacionRemplazarDocumento');
                    } else {
                        var reader = new FileReader();
                        $scope.Documento.ArchivoCargado = element.files[0]
                        reader.onload = $scope.AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }

                blockUI.stop();
            }
        }

        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                //imagenes, tienen un proceso aparte ya que se deben redimencionar para que no ocupen mucho espacio en la BD
                if ($scope.Documento.ArchivoCargado.type.includes("image")) {
                    var img = new Image()
                    img.src = e.target.result
                    $timeout(function () {
                        $('#Foto' + $scope.IdPosiction).show()
                        $('#FotoCargada' + $scope.IdPosiction).hide()
                        if (img.height > img.width) {
                            RedimencionarImagen('CanvasVertical', img, 699, 499)
                            $scope.Documento.Archivo = document.getElementById('CanvasVertical').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')

                        }
                        //la imagen es horizontal
                        else {
                            RedimencionarImagen('CanvasHorizontal', img, 499, 699)
                            $scope.Documento.Archivo = document.getElementById('CanvasHorizontal').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                        }
                        $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                        var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                        $scope.Documento.NombreDocumento = Extencion[0]
                        $scope.Documento.ExtensionDocumento = Extencion[1]
                        //if ($scope.Documento.TipoArchivoDocumento == 4301) {
                        //    RedimencionarImagen('Foto' + $scope.IdPosiction, img, 200, 200)
                        //    $scope.ItemFoto.NombreDocumento = Extencion[0]
                        //    $scope.ItemFoto.ValorDocumento = 1
                        //    $scope.ItemFoto.temp = true
                        //}
                        $scope.InsertarDocumento()
                    }, 100)
                }
                //Resto de archivos
                else {
                    $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                    $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                    var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                    $scope.Documento.NombreDocumento = Extencion[0]
                    $scope.Documento.ExtensionDocumento = Extencion[1]
                    $scope.InsertarDocumento()
                }
            });
        }

        $scope.SeleccionarObjeto = function (item) {
            $scope.DocumentoSeleccionado = item;
        }

        $scope.InsertarDocumento = function () {
            $timeout(function () {
                // Guardar Archivo temporal
                var Documento = {};

                Documento = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    NumeroCumplido: $scope.ModeloNumero,
                    NombreAsignado: $scope.Documento.NombreAsignado,
                    Nombre: $scope.Documento.NombreDocumento,
                    Extension: $scope.Documento.ExtensionDocumento,
                    Tipo: $scope.Documento.Tipo,
                    Archivo: $scope.Documento.Archivo,
                    Cumplido: true
                }



                DocumentosFactory.InsertarTemporal(Documento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            // Se guardó correctamente el pdf
                            if (response.data.Datos) {

                                $scope.ListadoDocumentos.forEach(function (item) {
                                    if (item.$$hashKey == $scope.DocumentoSeleccionado.$$hashKey) {
                                        item.Id = response.data.Datos;
                                        item.ValorDocumento = 1;
                                        item.AplicaEliminar = 1;
                                        item.temp = true;
                                    }
                                })

                                ShowSuccess('Se cargó el documento adjunto "' + $scope.Documento.NombreDocumento + '"');
                                $scope.Documento = '';
                            }
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }, 200)
        }

        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {

            if ($scope.ModeloNumero == undefined || $scope.ModeloNumero == null || $scope.ModeloNumero == '') {
                $scope.ModeloNumero = 0;
            }

            if (item.temp) {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Cumplido&ID=' + item.Id + '&USUA_Codigo=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Temp=1' + '&ENCU_Numero=' + parseInt($scope.ModeloNumero));
            }
            else {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Cumplido&ID=' + item.Id + '&ENCU_Numero=' + $scope.ModeloNumero + '&Temp=0');
            }


        };

        function LimpiarTemporalDocumentos() {

            var EliminarDocumento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                Cumplido: true
            }
            DocumentosFactory.EliminarDocumento(EliminarDocumento).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                    }
                    else {
                        MensajeError("Error", response.data.MensajeError, false);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.ElimiarDocumento = function (EliminarDocumento) {


            var EliminarDocumento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                NumeroCumplido: $scope.ModeloNumero,
                Id: EliminarDocumento.Id,
                Cumplido: true
            }
            DocumentosFactory.EliminarDocumento(EliminarDocumento).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                            var item = $scope.ListadoDocumentos[i];
                            if (item.Id == EliminarDocumento.Id) {
                                $scope.ListadoDocumentos.splice(i, 1);
                            }
                        }

                    }
                    else {
                        MensajeError("Error", response.data.MensajeError, false);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };

        //--------------------------------------------------//
        function ValidarRemesas() {
            var Continuar = true;
            $scope.RemesasCumplir = []
            if ($scope.ModeloListadoRemesasPlanilla.length > 0) {
                for (var i = 0; i < $scope.ModeloListadoRemesasPlanilla.length; i++) {
                    if ($scope.ModeloListadoRemesasPlanilla[i].Seleccionado) {
                        var fechaRemesaRecibe = null
                        var ValfechaRemesaRecibe = null
                        if ($scope.ModeloListadoRemesasPlanilla[i].FechaRecibe != undefined && $scope.ModeloListadoRemesasPlanilla[i].FechaRecibe != '' && $scope.ModeloListadoRemesasPlanilla[i].FechaRecibe != null) {
                            fechaRemesaRecibe = new Date($scope.ModeloListadoRemesasPlanilla[i].FechaRecibe);
                            ValfechaRemesaRecibe = ComparaFecha(fechaRemesaRecibe, hoy);
                        }
                        //if (
                        //    $scope.ModeloListadoRemesasPlanilla[i].IdentificacionRecibe == undefined || $scope.ModeloListadoRemesasPlanilla[i].IdentificacionRecibe == '' ||
                        //    $scope.ModeloListadoRemesasPlanilla[i].NombreRecibe == undefined || $scope.ModeloListadoRemesasPlanilla[i].NombreRecibe == '' ||
                        //    $scope.ModeloListadoRemesasPlanilla[i].CantidadRecibida === undefined || $scope.ModeloListadoRemesasPlanilla[i].CantidadRecibida === '' || $scope.ModeloListadoRemesasPlanilla[i].CantidadRecibida > $scope.ModeloListadoRemesasPlanilla[i].CantidadRemesa ||
                        //    $scope.ModeloListadoRemesasPlanilla[i].PesoRecibido === undefined || $scope.ModeloListadoRemesasPlanilla[i].PesoRecibido === '' || $scope.ModeloListadoRemesasPlanilla[i].PesoRecibido > $scope.ModeloListadoRemesasPlanilla[i].PesoRemesa ||
                        //    ValfechaRemesaRecibe == FECHA_ES_MAYOR || ValfechaRemesaRecibe == null
                        //) {
                        //    Continuar = false;
                        //    $scope.MensajesError.push('Por favor complete los campos de identificación, nombre, fecha (Menor a la actual), cantidad y peso recibido(no superiores a los valores de la remesa) en los registros seleccionados')
                        //}
                        //else {
                            for (var x = 0; x < $scope.ModeloListadoRemesasPlanilla.length; x++) {
                                var item = $scope.ModeloListadoRemesasPlanilla[x]
                                if ($scope.CondicionesPeso !== undefined) {
                                    if ($scope.CondicionesPeso.length > 0) {
                                        for (var i = 0; i < $scope.CondicionesPeso.length; i++) {
                                            if ($scope.CondicionesPeso[i].Producto.Codigo == item.CodigoProducto) {
                                                var BaseCalculoPeso = false
                                                var BaseCalculoCantidad = false
                                                var TipoCulaculoPorcentaje = false
                                                var TipoCulaculoCantidad = false
                                                var CondicionCobroTotal = false
                                                var CondicionCobroParcial = false
                                                //----------------------------------------------------------------------------------------------------
                                                if ($scope.CondicionesPeso[i].BaseCalculo.Codigo == 19601) {
                                                    BaseCalculoPeso = true
                                                }
                                                if ($scope.CondicionesPeso[i].BaseCalculo.Codigo == 19602) {
                                                    BaseCalculoCantidad = true
                                                }
                                                //----------------------------------------------------------------------------------------------------
                                                if ($scope.CondicionesPeso[i].TipoCalculo.Codigo == 19701) {
                                                    TipoCulaculoPorcentaje = true
                                                }
                                                if ($scope.CondicionesPeso[i].TipoCalculo.Codigo == 19702) {
                                                    TipoCulaculoCantidad = true
                                                }
                                                //----------------------------------------------------------------------------------------------------
                                                if ($scope.CondicionesPeso[i].CondicionCobro.Codigo == 19801) {
                                                    CondicionCobroTotal = true
                                                }
                                                if ($scope.CondicionesPeso[i].CondicionCobro.Codigo == 19802) {
                                                    CondicionCobroParcial = true
                                                }
                                                //----------------------------------------------------------------------------------------------------
                                                if (BaseCalculoPeso) {
                                                    if (TipoCulaculoPorcentaje) {
                                                        if (RevertirMV(item.PesoDescargue) < (RevertirMV(item.PesoCargue) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.PesoCargue)))) {
                                                            if (CondicionCobroTotal) {
                                                                item.ValorFlatante = MascaraValores((RevertirMV(item.PesoCargue) - RevertirMV(item.PesoDescargue)) * $scope.CondicionesPeso[i].ValorUnidad)
                                                            } else if (CondicionCobroParcial) {
                                                                item.ValorFlatante = MascaraValores(((RevertirMV(item.PesoCargue) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.PesoCargue))) - RevertirMV(item.PesoDescargue)) * $scope.CondicionesPeso[i].ValorUnidad)
                                                            }
                                                        }
                                                    } else if (TipoCulaculoCantidad) {
                                                        if (RevertirMV(item.PesoDescargue) < (RevertirMV(item.PesoCargue) - $scope.CondicionesPeso[i].Tolerancia)) {
                                                            if (CondicionCobroTotal) {
                                                                item.ValorFlatante = MascaraValores((iRevertirMV(tem.PesoCargue) - RevertirMV(item.PesoDescargue)) * $scope.CondicionesPeso[i].ValorUnidad)
                                                            } else if (CondicionCobroParcial) {
                                                                item.ValorFlatante = MascaraValores(((RevertirMV(item.PesoCargue) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.PesoCargue))) - RevertirMV(item.PesoDescargue)) * $scope.CondicionesPeso[i].ValorUnidad)
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (BaseCalculoCantidad) {
                                                    if (TipoCulaculoPorcentaje) {
                                                        if (RevertirMV(item.CantidadRecibida) < (RevertirMV(item.CantidadRemesa) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.CantidadRemesa)))) {
                                                            if (CondicionCobroTotal) {
                                                                item.ValorFlatante = MascaraValores((RevertirMV(item.CantidadRecibida) - RevertirMV(item.CantidadRemesa)) * $scope.CondicionesPeso[i].ValorUnidad)
                                                            } else if (CondicionCobroParcial) {
                                                                item.ValorFlatante = MascaraValores(((RevertirMV(item.CantidadRemesa) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.CantidadRemesa))) - RevertirMV(item.CantidadRecibida)) * $scope.CondicionesPeso[i].ValorUnidad)
                                                            }
                                                        }
                                                    } else if (TipoCulaculoCantidad) {
                                                        if (RevertirMV(item.CantidadRecibida) < (RevertirMV(item.CantidadRemesa) - $scope.CondicionesPeso[i].Tolerancia)) {
                                                            if (CondicionCobroTotal) {
                                                                item.ValorFlatante = MascaraValores((RevertirMV(item.CantidadRecibida) - RevertirMV(item.CantidadRemesa)) * $scope.CondicionesPeso[i].ValorUnidad)
                                                            } else if (CondicionCobroParcial) {
                                                                item.ValorFlatante = MascaraValores(((RevertirMV(item.CantidadRemesa) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.CantidadRemesa))) - RevertirMV(item.CantidadRecibida)) * $scope.CondicionesPeso[i].ValorUnidad)
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                $scope.RemesasCumplir.push({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    NumeroRemesa: item.NumeroInternoRemesa,
                                    FechaRecibe: item.FechaRecibe,
                                    NombreRecibe: item.NombreRecibe,
                                    IdentificacionRecibe: item.IdentificacionRecibe,
                                    TelefonoRecibe: item.TelefonoRecibe,
                                    ObservacionesRecibe: item.ObservacionesRecibe,
                                    CantidadRecibida: RevertirMV(item.CantidadRecibida),
                                    PesoRecibido: RevertirMV(item.PesoRecibido),
                                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                    /**/
                                    CantidadFaltante: RevertirMV(item.CantidadFaltante),
                                    PesoFaltante: RevertirMV(item.PesoFaltante),
                                    ValorFlatante: RevertirMV(item.ValorFlatante),
                                    PesoCargue: RevertirMV(item.PesoCargue),
                                    PesoDescargue: RevertirMV(item.PesoDescargue),
                                })
                            }
                        //}
                    }
                    else {
                        Continuar = false;
                        $scope.MensajesError.push('Debe seleccionar al menos una remesa para cumplir');
                    }
                }
            }
            else {
                //Continuar = false;
                //$scope.MensajesError.push('No existen remesas asociadas a la planilla para cumplir');
            }
            return Continuar;
        }

        $scope.CalcularFaltantes = function () {
            for (var x = 0; x < $scope.ModeloListadoRemesasPlanilla.length; x++) {
                var item = $scope.ModeloListadoRemesasPlanilla[x]
                item.ValorFlatante = 0
                item.PesoFaltante = 0
                item.PesoFaltante = MascaraValores(RevertirMV(item.PesoCargue) - RevertirMV(item.PesoDescargue))
                if ($scope.CondicionesPeso !== undefined) {
                    if ($scope.CondicionesPeso.length > 0) {
                        for (var i = 0; i < $scope.CondicionesPeso.length; i++) {
                            if ($scope.CondicionesPeso[i].Producto.Codigo == item.CodigoProducto) {
                                var BaseCalculoPeso = false
                                var BaseCalculoCantidad = false
                                var TipoCulaculoPorcentaje = false
                                var TipoCulaculoCantidad = false
                                var CondicionCobroTotal = false
                                var CondicionCobroParcial = false
                                //----------------------------------------------------------------------------------------------------
                                if ($scope.CondicionesPeso[i].BaseCalculo.Codigo == 19601) {
                                    BaseCalculoPeso = true
                                }
                                if ($scope.CondicionesPeso[i].BaseCalculo.Codigo == 19602) {
                                    BaseCalculoCantidad = true
                                }
                                //----------------------------------------------------------------------------------------------------
                                if ($scope.CondicionesPeso[i].TipoCalculo.Codigo == 19701) {
                                    TipoCulaculoPorcentaje = true
                                }
                                if ($scope.CondicionesPeso[i].TipoCalculo.Codigo == 19702) {
                                    TipoCulaculoCantidad = true
                                }
                                //----------------------------------------------------------------------------------------------------
                                if ($scope.CondicionesPeso[i].CondicionCobro.Codigo == 19801) {
                                    CondicionCobroTotal = true
                                }
                                if ($scope.CondicionesPeso[i].CondicionCobro.Codigo == 19802) {
                                    CondicionCobroParcial = true
                                }
                                //----------------------------------------------------------------------------------------------------
                                if (BaseCalculoPeso) {
                                    if (TipoCulaculoPorcentaje) {
                                        if (RevertirMV(item.PesoDescargue) < (RevertirMV(item.PesoCargue) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.PesoCargue)))) {
                                            if (CondicionCobroTotal) {
                                                item.ValorFlatante = MascaraValores((RevertirMV(item.PesoCargue) - RevertirMV(item.PesoDescargue)) * $scope.CondicionesPeso[i].ValorUnidad)
                                            } else if (CondicionCobroParcial) {
                                                item.ValorFlatante = MascaraValores(((RevertirMV(item.PesoCargue) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.PesoCargue))) - RevertirMV(item.PesoDescargue)) * $scope.CondicionesPeso[i].ValorUnidad)
                                            }
                                        }
                                    } else if (TipoCulaculoCantidad) {
                                        if (RevertirMV(item.PesoDescargue) < (RevertirMV(item.PesoCargue) - $scope.CondicionesPeso[i].Tolerancia)) {
                                            if (CondicionCobroTotal) {
                                                item.ValorFlatante = MascaraValores((RevertirMV(item.PesoCargue) - RevertirMV(item.PesoDescargue)) * $scope.CondicionesPeso[i].ValorUnidad)
                                            } else if (CondicionCobroParcial) {
                                                item.ValorFlatante = MascaraValores(((RevertirMV(item.PesoCargue) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.PesoCargue))) - RevertirMV(item.PesoDescargue)) * $scope.CondicionesPeso[i].ValorUnidad)
                                            }
                                        }
                                    }
                                }
                                else if (BaseCalculoCantidad) {
                                    if (TipoCulaculoPorcentaje) {
                                        if (RevertirMV(item.CantidadRecibida) < (RevertirMV(item.CantidadRemesa) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.CantidadRemesa)))) {
                                            if (CondicionCobroTotal) {
                                                item.ValorFlatante = MascaraValores((RevertirMV(item.CantidadRecibida) - RevertirMV(item.CantidadRemesa)) * $scope.CondicionesPeso[i].ValorUnidad)
                                            } else if (CondicionCobroParcial) {
                                                item.ValorFlatante = MascaraValores(((RevertirMV(item.CantidadRemesa) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.CantidadRemesa))) - RevertirMV(item.CantidadRecibida)) * $scope.CondicionesPeso[i].ValorUnidad)
                                            }
                                        }
                                    } else if (TipoCulaculoCantidad) {
                                        if (RevertirMV(item.CantidadRecibida) < (RevertirMV(item.CantidadRemesa) - $scope.CondicionesPeso[i].Tolerancia)) {
                                            if (CondicionCobroTotal) {
                                                item.ValorFlatante = MascaraValores((RevertirMV(item.CantidadRecibida) - RevertirMV(item.CantidadRemesa)) * $scope.CondicionesPeso[i].ValorUnidad)
                                            } else if (CondicionCobroParcial) {
                                                item.ValorFlatante = MascaraValores(((RevertirMV(item.CantidadRemesa) - ($scope.CondicionesPeso[i].Tolerancia * RevertirMV(item.CantidadRemesa))) - RevertirMV(item.CantidadRecibida)) * $scope.CondicionesPeso[i].ValorUnidad)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //-----------------------TRANSBORDO----------------------//
        $scope.ValidarTipoCumplido = function (TipoCumplido) {
            if (TipoCumplido != undefined && TipoCumplido != null && TipoCumplido != '' && $scope.ModeloNumeroPlanilla > 0) {
                if (TipoCumplido.Codigo == CODIGO_CATALOGO_TIPO_CUMPLIDO_MANIFIESTO_SUSPENSION) {
                    //Bloqueo Pantalla
                    blockUIConfig.autoBlock = true;
                    blockUIConfig.delay = 0;
                    blockUI.start("Obteniendo Informacion Orden Servicio...");
                    $timeout(function () { blockUI.message("Obteniendo Informacion Orden Servicio..."); }, 100);
                    //Bloqueo Pantalla
                    var filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Numero: NumeroOrdenServicio,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                        Sync: true
                    };
                    var RespuestaOrden = EncabezadoSolicitudOrdenServiciosFactory.Obtener(filtros);
                    if (RespuestaOrden.ProcesoExitoso == true) {
                        CodigoTipoDespacho = RespuestaOrden.Datos.TipoDespacho.Codigo;
                        switch (CodigoTipoDespacho) {
                            case 18201://Viajes
                                TipoViajes = true;
                                break;
                            case 18202://Cupo Tonelada
                                TipoCupoTonelada = true;
                                break;
                            case 18203://Cantidad Viajes
                                TipoCantidadViajes = true;
                                break;
                        }
                    }
                    ObtenerTarifarioVenta();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                }
            }
        };
        //--Tarifas Ventas
        $scope.ListadoRutas = [];
        $scope.ListadoTarifasVentas = [];
        function ObtenerTarifarioVenta() {
            var Response = TarifarioVentasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Cliente.Tarifario, Sync: true });
            $scope.TarifarioVentas = Response.Datos;
            $scope.ListadoRutas = [];
            $scope.ListadoTarifasVentas = [];
            for (var i = 0; i < $scope.TarifarioVentas.Tarifas.length; i++) {
                var item = $scope.TarifarioVentas.Tarifas[i];
                if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CodigoLineaNegocio
                    && item.TipoLineaNegocioTransportes.Codigo == CodigoTipoLineaNegocio) {
                    $scope.ListadoTarifasVentas.push(item);
                    if ($scope.ListadoRutas.length == 0) {
                        $scope.ListadoRutas.push(item.Ruta);
                    }
                    else {
                        var Existe = false;
                        for (var j = 0; j < $scope.ListadoRutas.length; j++) {
                            if ($scope.ListadoRutas[j].Codigo == item.Ruta.Codigo) {
                                Existe = true;
                                break;
                            }
                        }
                        if (Existe == false) {
                            $scope.ListadoRutas.push(item.Ruta);
                        }
                    }
                }
            }
        }
        //--Tarifas Ventas
        //--Validar Ruta
        $scope.ValidarRutaTransbordo = function () {
            if ($scope.RutaTransbordo != undefined && $scope.RutaTransbordo != null && $scope.RutaTransbordo != '') {
                if ($scope.VehiculoTransbordo != undefined && $scope.VehiculoTransbordo != null && $scope.VehiculoTransbordo != '') {
                    var Response = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.RutaTransbordo.Codigo, Sync: true });
                    var CoincideDestino = false;
                    for (var i = 0; i < $scope.ModeloListadoRemesasPlanilla.length; i++) {
                        if ($scope.ModeloListadoRemesasPlanilla[i].CodigoCiudadDestinoRuta == Response.Datos.CiudadDestino.Codigo) {
                            CoincideDestino = true;
                            break;
                        }
                    }
                    if (CoincideDestino) {
                        //--Consulta Tarifarios
                        GestionarTarifas();
                        AsignarTarifas();
                        $scope.GestionarFletesCompra();
                    }
                    else {
                        ShowError("La ciudad destino de la ruta no coincide con la ciudad destino de las remesas asociadas");
                        $scope.RutaTransbordo = '';
                    }
                }
            }
        };
        //--Validar Ruta
        //--Gestionar Tarifas Dependiendo de Validacion 1
        function GestionarTarifas() {
            $scope.ListadoTarifasCompras = [];
            $scope.ListaTipoTarifasCompra = [];
            if ($scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta == true) {
                //--Obtiene Tarifario Ventas
                for (var i = 0; i < $scope.ListadoTarifasVentas.length; i++) {
                    if ($scope.ListadoTarifasVentas[i].Ruta.Codigo == $scope.RutaTransbordo.Codigo) {
                        var TarifaAux = {
                            Codigo: $scope.ListadoTarifasVentas[i].TipoTarifaTransportes.Codigo,
                            Nombre: $scope.ListadoTarifasVentas[i].TipoTarifaTransportes.Nombre,
                            CodigoTarifa: $scope.ListadoTarifasVentas[i].TipoTarifaTransportes.TarifaTransporte.Codigo,
                            ValorFlete: $scope.ListadoTarifasVentas[i].ValorFleteTransportador
                        };
                        var Existe = false;
                        for (var j = 0; j < $scope.ListadoTarifasCompras.length; j++) {
                            if ($scope.ListadoTarifasCompras[j].Codigo == $scope.ListadoTarifasVentas[i].TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                Existe = true;
                                break;
                            }
                        }
                        if (Existe == false) {
                            $scope.ListadoTarifasCompras.push($scope.ListadoTarifasVentas[i].TipoTarifaTransportes.TarifaTransporte);
                        }
                        $scope.ListaTipoTarifasCompra.push(TarifaAux);
                    }
                }
                //console.log("TarifasVenta: ", $scope.ListadoTarifasCompras);
                //console.log("TipoTarifasVenta: ", $scope.ListaTipoTarifasCompra);
            }
            else {
                //--Obtiente Tarifario Compras
                var FiltroTarifa = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.TenedorTransbordo.Codigo,
                    TarifarioProveedores: true,
                    CodigoLineaNegocio: CodigoLineaNegocio,
                    CodigoTipoLineaNegocio: CodigoTipoLineaNegocio,
                    //TarifaCarga: $scope.ModalTarifaVenta,
                    CodigoRuta: $scope.RutaTransbordo.Codigo,
                    Sync: true
                };
                TarifarioCompra = {};
                var response = TercerosFactory.Consultar(FiltroTarifa);
                if (response.ProcesoExitoso === true) {
                    if (response.Datos.length > 0) {
                        TarifarioCompra = response.Datos;
                        for (var i = 0; i < TarifarioCompra.length; i++) {
                            if ($scope.ListadoTarifasVentas.length > 0) {
                                var Existe = false;
                                for (var j = 0; j < $scope.ListadoTarifasVentas.length.length; j++) {
                                    if (TarifarioCompra[i].TarifaCarga.Codigo == $scope.ListadoTarifasVentas[j].TipoTarifaTransportes.TarifaTransporte.Codigo &&
                                        TarifarioCompra[i].TipoTarifaCarga.Codigo == $scope.ListadoTarifasVentas[j].TipoTarifaTransportes.Codigo) {
                                        Existe = true;
                                        break;
                                    }
                                }
                                var TarifaAux = {
                                    Codigo: TarifarioCompra[i].TipoTarifaCarga.Codigo,
                                    Nombre: TarifarioCompra[i].TipoTarifaCarga.Nombre,
                                    CodigoTarifa: TarifarioCompra[i].TarifaCarga.Codigo,
                                    ValorFlete: TarifarioCompra[i].ValorFlete
                                };
                                $scope.ListadoTarifasCompras.push(TarifarioCompra[i].TarifaCarga);
                                $scope.ListaTipoTarifasCompra.push(TarifaAux);
                            }
                            else {
                                ShowError("El Cliente No tiene asociado Tarifas");
                                break;
                            }
                        }
                        //console.log("TarifasCompra: ", $scope.ListadoTarifasCompras);
                        //console.log("TipoTarifasCompra: ", $scope.ListaTipoTarifasCompra);
                    }
                    else {
                        ShowError("No existe un tarifario de compra para el vehículo y tenedor ingresados o no se encontraron tarifas disponibles");
                    }
                }
                else {
                    ShowError("No existe un tarifario de compra para el vehículo y tenedor ingresados");
                }
            }
        }
        //--Gestionar Tarifas Dependiendo de Validacion 1
        //--Asignar Tarifas A Partir De Los Listados Tarifas y Tipo Tarifas Compras
        $scope.ListadoTarifasTransporte = [];
        function AsignarTarifas() {
            $scope.ListadoTarifasTransporte = $scope.ListadoTarifasCompras;
            $scope.TarifaTransbordo = $scope.ListadoTarifasTransporte[0];
            $scope.CambioTarifaCompra($scope.TarifaTransbordo);
            //$scope.TipoTarifaTransbordo = $scope.ListaTipoTarifasCompra[0];
            //$scope.FleteTransportadorTransbordo = MascaraValores($scope.ListaTipoTarifasCompra[0].ValorFlete);
        }
        //--Calculos Totales Compra
        $scope.GestionarFletesCompra = function () {
            var Cantidad = $scope.CantidadTransbordo == undefined ? 0 : parseFloat(MascaraNumero(MascaraValores($scope.CantidadTransbordo)));
            var FleteTransportador = $scope.FleteTransportadorTransbordo == undefined ? 0 : parseFloat(MascaraDecimales(MascaraValores($scope.FleteTransportadorTransbordo)));
            var Peso = $scope.PesoTransbordo == undefined ? 0 : parseFloat(MascaraNumero(MascaraValores($scope.PesoTransbordo)));
            if (TipoCupoTonelada) {
                $scope.TotalFleteTransportadorTransbordo = MascaraValores(Math.round((FleteTransportador / 1000) * Peso));
            }
            else if ($scope.TarifaTransbordo !== undefined && $scope.TarifaTransbordo !== null) {
                $scope.TotalFleteTransportadorTransbordo = 0;

                //Contenedor (VF = FT * C)
                if ($scope.TarifaTransbordo.Codigo == TARIFA_CONTENEDOR) {
                    $scope.TotalFleteTransportadorTransbordo = Math.round(Cantidad * FleteTransportador);
                }

                //Devolucion Contenedor (VF = FT * C)
                if ($scope.TarifaTransbordo.Codigo == TARIFRA_DEVOLUCION_CONTENEDOR) {
                    $scope.TotalFleteTransportadorTransbordo = Math.round(Cantidad * FleteTransportador);
                }

                //Cupo Vehiculo (VF = FT * C)
                if ($scope.TarifaTransbordo.Codigo == TARIFA_CUPO_VEHICULO) {
                    $scope.TotalFleteTransportadorTransbordo = Math.round(FleteTransportador);
                }

                //Tarifa Dia / Hora (VF = FT * C)
                if ($scope.TarifaTransbordo.Codigo == TARIFA_DIA || $scope.TarifaTransbordo.Codigo == TARIFA_HORA) {
                    $scope.TotalFleteTransportadorTransbordo = Math.round(Cantidad * FleteTransportador);
                }
                //Movilización
                if ($scope.TarifaTransbordo.Codigo == TARIFA_MOVILIZACION) {
                    $scope.TotalFleteTransportadorTransbordo = Math.round(FleteTransportador);
                }
                if ($scope.TarifaTransbordo.Codigo == TARIFA_PESO_FLETE) {
                    $scope.TotalFleteTransportadorTransbordo = Math.round((FleteTransportador / 1000) * Peso);
                }
                if ($scope.TarifaTransbordo.Codigo == 303) {
                    $scope.TotalFleteTransportadorTransbordo = Math.round(FleteTransportador);
                }
                //Peso Barril galon
                if ($scope.TarifaTransbordo.Codigo == 6 || $scope.TarifaTransbordo.Codigo == 300) {
                    $scope.TotalFleteTransportadorTransbordo = Math.round(Cantidad * FleteTransportador);
                }
                if ($scope.TarifaTransbordo.Codigo == 301) {
                    $scope.TotalFleteTransportadorTransbordo = Math.round((FleteTransportador / 1000) * Peso);
                }
                $scope.TotalFleteTransportadorTransbordo = MascaraValores($scope.TotalFleteTransportadorTransbordo);
            }
        };
        //--Calculos Totales Compra
        //--Valida Cambio De Tarifa
        $scope.ListadoTipoTarifaFiltradas = [];
        $scope.CambioTarifaCompra = function (Tarifa) {
            $scope.ListadoTipoTarifaFiltradas = [];
            for (var i = 0; i < $scope.ListaTipoTarifasCompra.length; i++) {
                if ($scope.ListaTipoTarifasCompra[i].CodigoTarifa == Tarifa.Codigo) {
                    $scope.ListadoTipoTarifaFiltradas.push($scope.ListaTipoTarifasCompra[i]);
                }
            }
            $scope.TipoTarifaTransbordo = $scope.ListadoTipoTarifaFiltradas[0];
            $scope.FleteTransportadorTransbordo = MascaraValores($scope.TipoTarifaTransbordo.ValorFlete);
        };
        //--Valida Cambio De Tarifa
        //--Valida Cambio De Tipo Tarifa
        $scope.CambioTipoTarifaCompra = function (TipoTarifa) {
            if (TipoTarifa !== undefined) {
                $scope.FleteTransportadorTransbordo = MascaraValores(TipoTarifa.ValorFlete);
                $scope.GestionarFletesCompra();
            }
        };
        //--Valida Cambio De Tipo Tarifa
        //--Carga Informacion Cuando Es Obtener
        function CargarInformacionTransbordo() {
            var InfoPlanilla = PlanillaDespachosFactory.Obtener({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                Numero: $scope.NumeroInternoPlanilla,
                Obtener: 1,
                Sync: true
            });
            if (InfoPlanilla.ProcesoExitoso == true && InfoPlanilla.Datos.Numero > 0) {
                $scope.ModeloListadoRemesasPlanilla = InfoPlanilla.Datos.ListadoRemesas;
                //--Condicion Peso
                var filtroterc = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModeloListadoRemesasPlanilla[0].CodigoCliente,
                    Sync: true
                };
                var RespTercero = TercerosFactory.Obtener(filtroterc);
                if (RespTercero.ProcesoExitoso == true) {
                    $scope.CondicionesPeso = RespTercero.Datos.CondicionesPeso;
                    Cliente.Codigo = RespTercero.Datos.Codigo;
                    Cliente.Tarifario = RespTercero.Datos.Cliente.Tarifario.Codigo;
                }
                else {
                    ShowError('No se logro consultar el tercero ');
                }
                //--Condicion Peso
                //--Informacion para Transbordo
                CodigoLineaNegocio = InfoPlanilla.Datos.LineaNegocioTransportes.Codigo;
                CodigoTipoLineaNegocio = InfoPlanilla.Datos.TipoLineaNegocioTransportes.Codigo;
                NumeroOrdenServicio = InfoPlanilla.Datos.ListadoRemesas[0].NumeroOrdenServicio;
                //--Informacion para Transbordo
            }
            $scope.ValidarTipoCumplido($scope.TipoCumplidoManifiesto);

        }
        //-----------------------TRANSBORDO----------------------//

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarCumplidoDespachosPaqueteria/' + $scope.ModeloNumeroDocumento;
        };

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item)
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };
        //--Obtener Paramatros
        if ($routeParams.Numero > 0) {
            $scope.ModeloNumero = $routeParams.Numero;
            //Bloqueo Pantalla
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Obteniendo Informacion Cumplido...");
            $timeout(function () {
                blockUI.message("Obteniendo Informacion Cumplido...");
                Obtener();
            }, 100);
            //Bloqueo Pantalla
        }
        else {

            $scope.ModeloNumero = 0;
            $scope.ModeloOficina = OficinaUsuario.Nombre;
            $scope.aplicabase = true;
            LimpiarTemporalDocumentos(); 

        } 


   

        
        $scope.SeleTodoItem = function (seleccion) {
            if ($scope.ModeloListadoRemesasPlanilla.length > 0) {
                for (var i = 0; i < $scope.ModeloListadoRemesasPlanilla.length; i++) {
                    $scope.ModeloListadoRemesasPlanilla[i].FechaRecibe = seleccion == true ? new Date().withoutMilliseconds() : '';
                    $scope.ModeloListadoRemesasPlanilla[i].Seleccionado = seleccion;
                }
            }
        }

        $scope.ValidarCantidad = function (item) {
            if (RevertirMV(item.CantidadRecibida) > RevertirMV(item.CantidadRemesa)) {
                ShowError('La cantidad recibida no puede se mayor a la cantidad de la remesa')
                item.CantidadRecibida = MascaraValores(item.CantidadRemesa)
                item.CantidadFaltante = 0
            }
            else {
                item.CantidadFaltante = MascaraValores(RevertirMV(item.CantidadRemesa) - RevertirMV(item.CantidadRecibida))
                $scope.CalcularFaltantes()
            }
        }

        $scope.ValidarPeso = function (item) {
            if (RevertirMV(item.PesoRecibido) > RevertirMV(item.PesoRemesa)) {
                ShowError('El peso recibidó no puede se mayor al peso de la remesa')
                item.PesoRecibido = MascaraValores(item.PesoRemesa)
                item.PesoFaltante = 0
            }
            else {
                item.PesoFaltante = MascaraValores(RevertirMV(item.PesoRemesa) - RevertirMV(item.PesoRecibido))
                $scope.CalcularFaltantes()
            }
        }
    }]);