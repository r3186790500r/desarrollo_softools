﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_borrar_auxiliar_planilla_entregada'

DROP PROCEDURE gsp_borrar_auxiliar_planilla_entregada
GO

CREATE PROCEDURE gsp_borrar_auxiliar_planilla_entregada(  
@par_EMPR_Codigo NUMERIC,  
@par_Codigo NUMERIC,
@par_Codigo_Auxiliar NUMERIC
) 
AS BEGIN  
DECLARE @Codigo smallint = 0 		
DELETE  FROM Detalle_Auxiliares_Planilla_Entregas WHERE

EMPR_Codigo = @par_EMPR_Codigo AND  
ENPE_Numero = @par_Codigo AND 
TERC_Codigo_Funcionario = @par_Codigo_Auxiliar

SELECT @@ROWCOUNT AS Codigo

END  
GO