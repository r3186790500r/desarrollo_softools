﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Repositorio.Facturacion

Namespace Facturacion
    Public NotInheritable Class PersistenciaFacturasOtrosConceptos
        Implements IPersistenciaBase(Of Facturas)

        Public Function Consultar(filtro As Facturas) As IEnumerable(Of Facturas) Implements IPersistenciaBase(Of Facturas).Consultar
            Return New RepositorioFacturasOtrosConceptos().Consultar(filtro)
        End Function
        Public Function Insertar(entidad As Facturas) As Long Implements IPersistenciaBase(Of Facturas).Insertar
            Return New RepositorioFacturasOtrosConceptos().Insertar(entidad)
        End Function
        Public Function Modificar(entidad As Facturas) As Long Implements IPersistenciaBase(Of Facturas).Modificar
            Return New RepositorioFacturasOtrosConceptos().Modificar(entidad)
        End Function
        Public Function Obtener(filtro As Facturas) As Facturas Implements IPersistenciaBase(Of Facturas).Obtener
            Return New RepositorioFacturasOtrosConceptos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Facturas) As Boolean
            Return New RepositorioFacturasOtrosConceptos().Anular(entidad)
        End Function
        Public Function ObtenerDatosFacturaElectronicaSaphety(filtro As Facturas) As Facturas
            Return New RepositorioFacturasOtrosConceptos().ObtenerDatosFacturaElectronicaSaphety(filtro)
        End Function
        Public Function GuardarFacturaElectronica(entidad As Facturas) As Long
            Return New RepositorioFacturasOtrosConceptos().GuardarFacturaElectronica(entidad)
        End Function
        Public Function ObtenerDocumentoReporteSaphety(filtro As Facturas) As Facturas
            Return New RepositorioFacturasOtrosConceptos().ObtenerDocumentoReporteSaphety(filtro)
        End Function
    End Class
End Namespace
