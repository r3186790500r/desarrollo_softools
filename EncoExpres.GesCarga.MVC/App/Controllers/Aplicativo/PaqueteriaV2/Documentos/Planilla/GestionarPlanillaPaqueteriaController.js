﻿EncoExpresApp.controller("GestionarPlanillaPaqueteriaCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'blockUIConfig', 'CiudadesFactory', 'TercerosFactory',
    'VehiculosFactory', 'SemirremolquesFactory', 'ValorCatalogosFactory', 'ProductoTransportadosFactory', 'TipoLineaNegocioTransportesFactory', 'TarifaTransportesFactory',
    'TipoTarifaTransportesFactory', 'OficinasFactory', 'RutasFactory', 'TarifarioComprasFactory', 'ImpuestosFactory', 'RemesaGuiasFactory', 'ZonasFactory', 'RecoleccionesFactory',
    'PlanillaPaqueteriaFactory', 'ManifiestoFactory', 'PeajesFactory', 'PrecintosFactory', 'CierreContableDocumentosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, blockUIConfig, CiudadesFactory, TercerosFactory,
        VehiculosFactory, SemirremolquesFactory, ValorCatalogosFactory, ProductoTransportadosFactory, TipoLineaNegocioTransportesFactory, TarifaTransportesFactory,
        TipoTarifaTransportesFactory, OficinasFactory, RutasFactory, TarifarioComprasFactory, ImpuestosFactory, RemesaGuiasFactory, ZonasFactory, RecoleccionesFactory,
        PlanillaPaqueteriaFactory, ManifiestoFactory, PeajesFactory, PrecintosFactory, CierreContableDocumentosFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        console.clear();
        $scope.Titulo = 'GESTIONAR PLANILLA PAQUETERÍA';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Planilla Paquetería' }, { Nombre: 'Gestionar' }];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.PLANILLA);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        }
        catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.MENU_OPCION_GUIA = OPCION_MENU_PAQUETERIA.REMESAS;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Planilla: {
                Numero: 0,
                Fecha: new Date(),
                FechaSalida: new Date(),
                DetalleTarifarioCompra: {},
                CalcularContraentregas: true
            },
            TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
            Remesa: { Cliente: { NombreCompleto: '' }, Ruta: {} }
        };
        $scope.Planilla = {};
        $scope.ListadoFormaPago = [];
        $scope.FiltroRecogidas = {
            FormaPago: ""
        };
        $scope.CargarRemesas = {
            FechaInicial: new Date(),
            FechaFinal: new Date(),
            NumeroInicial: '',
            CiudadRemitente: '',
            CiudadDestinatario: '',
            Cliente: '',
            NumeroDocumentoCliente: '',
            Remitente: ''
        };

        $scope.ListaAuxiliares = [];
        $scope.DetallesAuxiliares = [];
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_BORRADOR);
        //--Remesas
        $scope.ListadoRemesas = [];
        $scope.ListadoRemesasFiltradas = [];
        $scope.ListadoRemesasGuardadas = [];
        //--Remesas
        //$scope.ListadoRemesasContraentregas = [];
        //--Recolecciones
        $scope.ListadoRecolecciones = [];
        $scope.ListadoRecoleccionesFiltradas = [];
        $scope.ListadoRemesasRecogidasFiltradas = [];
        $scope.ListadoRemesasRecogidasGuardadas = [];
        $scope.ListadoRecoleccionesGuardadas = [];
        $scope.ListadoZonas = [];
        $scope.ListadoReporte = [];
        //--Recolecciones
        $scope.ListadoTarifas = [];
        $scope.AplicaImpuestos = false;
        $scope.ListaCiudades = [];
        $scope.ListaCliente = [];
        $scope.ListaRemitente = [];
        $scope.ListadoConductores = [];
        $scope.ListaFuncionario = [];
        $scope.ListadoVehiculos = [];
        $scope.ListaSemirremolque = [];
        $scope.ListadoTipoEntregaRemesaPaqueteria = [];
        $scope.ListadoTipoTransporteRemsaPaqueteria = [];
        $scope.ListadoTipoDespachoRemesaPaqueteria = [];
        $scope.ListadoTipoServicioRemesaPaqueteria = [];
        $scope.ListadoProductoTransportados = [];
        $scope.ListadoTipoLineaNegocioTransportes = [];
        $scope.ListadoTarifaTransportes = [];
        $scope.ListadoTipoTarifaTransportes = [];
        $scope.ListadoOficinas = [];
        $scope.ListaRutasFiltradas = [];
        $scope.ListadoRutas = [];
        $scope.ListadoImpuestos = [];
        $scope.ListadoImpuestosFiltrado = [];
        $scope.ListaTarifas = [];
        $scope.ListadoTipoTarifas = [];
        $scope.ListaTipoTarifas = [];
        $scope.ListadoOficinaDestino = [];
        $scope.PagRemesas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.PagRemesasGuardadas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.PagRecoleccion = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.PagRecoleccionGuardadas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.PagRecogidasGuardadas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.PagGuiasRetorno = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.PagGuiasRetornoGuardadas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };

        $scope.ListadoZonas = [];
        $scope.ListadoTotalesRemesas = [];
        $scope.MostrarGuardadas = true;
        $scope.DeshabilitarAnticipo = false;
        $(".Tab-Recogidas").hide();
        var TMPConductor;
        $scope.ListadoLineaNegocioPaqueteria = [];
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoEngancheRemesasMasivoPaqueteria) {
            $scope.RemesaPadre = true;
        }
        if ($scope.Sesion.UsuarioAutenticado.DeshabilitarAnticipoPlanillaPaqueteria) {
            $scope.DeshabilitarAnticipo = true;
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Autocomplete
        //----Ciudades
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListaCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCiudades);
                }
            }
            return $scope.ListaCiudades;
        };
        //----Ciudades
        //----Cliente
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCliente);
                }
            }
            return $scope.ListaCliente;
        };
        //----Cliente
        //----Remitente
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_REMITENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRemitente);
                }
            }
            return $scope.ListaRemitente;
        };
        //----Remitente
        //----Conductor
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores);
                }
            }
            return $scope.ListadoConductores;
        };
        //----Conductor
        //----Funcionario
        $scope.AutocompleteFuncionario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_EMPLEADO,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaFuncionario = ValidarListadoAutocomplete(Response.Datos, $scope.ListaFuncionario);
                }
            }
            return $scope.ListaFuncionario;
        };
        //----Funcionario
        //----Vehiculos
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos);
                }
            }
            return $scope.ListadoVehiculos;
        };
        //----Vehiculos
        //----Semiremolques
        $scope.AutoCompleteSemiRemolque = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = SemirremolquesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListaSemirremolque = ValidarListadoAutocomplete(Response.Datos, $scope.ListaSemirremolque);
                }
            }
            return $scope.ListaSemirremolque;
        };
        //----Semiremolques
        //--Oficina Destino
        $scope.AutocompleteOficinaDestino = function (value) {
            $scope.ListadoOficinaDestino = [];
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    var Response = OficinasFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoOficinaDestino = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoOficinaDestino);
                }
            }
            return $scope.ListadoOficinaDestino;
        }
        //--Oficina Destino
        //----------Autocomplete
        //----------Init
        $scope.InitLoad = function () {
            //----Carga Todas Las Rutas
            $scope.ListadoRutas = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true }).Datos;
            //----Producto Transportado
            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoProductoTransportados = response.data.Datos;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            //----Tipo Linea Negocio Transporte
            TipoLineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            var ListaTLNT = response.data.Datos;
                            for (var i = 0; i < ListaTLNT.length; i++) {
                                if (ListaTLNT[i].LineaNegocioTransporte !== undefined && ListaTLNT[i].LineaNegocioTransporte !== null) {
                                    if (ListaTLNT[i].LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA) {
                                        $scope.ListadoTipoLineaNegocioTransportes.push(ListaTLNT[i]);
                                    }
                                }
                            }
                        }
                    }
                }, function (response) {
                });
            //----Tarifas Transporte
            TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.ListadoTarifaTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTarifaTransportesInicial = response.data.Datos;
                        }
                        else {
                            $scope.ListadoTarifaTransportesInicial = [];
                        }
                    }
                }, function (response) {
                });
            //----Tip Tarofas Transporte
            TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTarifaTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoTarifaTransportesInicial = response.data.Datos;
                        }
                        else {
                            $scope.ListadoTipoTarifaTransportesInicial = []
                        }
                    }
                }, function (response) {
                });
            //----Oficinas
            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                var responseOfic = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true });
                if (responseOfic.ProcesoExitoso == true) {
                    responseOfic.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });
                    $scope.CargarRemesas.OficinaOrigen = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                }
            } else {
                $scope.ListadoOficinas.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.CargarRemesas.OficinaOrigen = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
            }
            //--Forma de pago
            var ListaFormaPago = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS },
                Sync: true
            }).Datos;
            ListaFormaPago.splice(ListaFormaPago.findIndex(item => item.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA), 1);
            $scope.ListadoFormaPago.push({ Codigo: -1, Nombre: "(TODAS)" });
            $scope.ListadoFormaPago = $scope.ListadoFormaPago.concat(ListaFormaPago);
            $scope.FiltroRecogidas.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==-1');
            //----Sugerir Horas
            var fecha = new Date();
            $scope.Modelo.Planilla.HoraSalida = fecha.getHours() + ":" + (fecha.getMinutes() < 10 ? "0" + fecha.getMinutes() : fecha.getMinutes());
            //----Sugerir Horas
            //--Catalogo Linea Negocio Paqueteria
            $scope.ListadoLineaNegocioPaqueteria = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CATALOGO_LINEA_NEGOCIO_PAQUETERIA.CODIGO },
                Sync: true
            }).Datos;
            $scope.ListadoLineaNegocioPaqueteria.push({ Codigo: -1, Nombre: "(TODAS)" });
            $scope.CargarRemesas.LineaNegocioPaqueteria = $linq.Enumerable().From($scope.ListadoLineaNegocioPaqueteria).First('$.Codigo ==-1');
            //--Catalogo Linea Negocio Paqueteria
            //--Gestion Taps
            $scope.MostrarSeccionTap(1);
            //----Obtener Parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
                if ($routeParams.Numero > 0) {
                    $scope.Modelo.Planilla.Numero = $routeParams.Numero;
                    blockUIConfig.autoBlock = true;
                    blockUIConfig.delay = 0;
                    blockUI.start("Obteniendo Documento...");
                    $timeout(function () { blockUI.message("Obteniendo Documento..."); Obtener(); }, 100);
                }
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        $scope.VerificarConductor = function (Conductor) {
            if (Conductor != undefined && Conductor != null && Conductor != "") {
                //--Valida Documentos Vencidos
                //var TerceroAlterno = $scope.CargarTercero(Conductor.Codigo);
                blockUI.start("Consultando Conductor...");
                var TerceroAlterno = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Conductor.Codigo, Sync: true }).Datos;
                blockUI.stop();
                var Vehiculo = $scope.Modelo.Planilla.Vehiculo;
                if (TerceroAlterno.Estado.Codigo == 0) {
                    ShowError("El conductor del vehículo " + Vehiculo.Placa + " de nombre '" + TerceroAlterno.NombreCompleto + "' esta inactivo por motivo '" + TerceroAlterno.JustificacionBloqueo + "'");
                    $scope.Modelo.Planilla.Conductor = '';
                    return false;
                }
                else {
                    //--Valida asignacion vehiculo
                    if ($scope.Modelo.Planilla.Vehiculo.Conductor.Codigo !== Conductor.Codigo) {
                        ShowInfo("El conductor ingresado es diferente al parametrizado para el vehículo");
                    }
                    //--Valida licencia proxima a vencer
                    var plazoLicencia = new Date();
                    plazoLicencia.setDate(plazoLicencia.getDate() + 4);//plazo alerta 4 dias
                    var fechaLicencia = new Date(TerceroAlterno.Conductor.FechaVencimiento);
                    if (fechaLicencia <= plazoLicencia) {
                        if ((fechaLicencia - new Date()) <= 0) {
                            ShowError("El conductor ingresado tiene su licencia vencida");
                        } else {
                            ShowError("El conductor ingresado tiene su licencia próxima a vencer");
                        }
                    }
                }
            }
        };

        $scope.LimpiarRecogidas = function () {
            $scope.PagRecogidasGuardadas.array = $scope.ListadoRemesasRecogidasGuardadas;
            ResetPaginacion($scope.PagRecogidasGuardadas);
        };

        $scope.FiltrarRecogidas = function () {
            var Filtro = $scope.FiltroRecogidas;
            var nFiltrosAplica = 0;
            if (Filtro.CiudadDestinatario !== undefined) {
                nFiltrosAplica++;
            }
            if (Filtro.Cliente !== undefined) {
                nFiltrosAplica++;
            }
            $scope.ListadoRemesasRecogidasFiltradas = [];
            for (var i = 0; i < $scope.ListadoRemesasRecogidasGuardadas.length; i++) {
                var cont = 0
                var item = $scope.ListadoRemesasRecogidasGuardadas[i];
                if (Filtro.CiudadDestinatario !== undefined) {
                    if (item.Remesa.CiudadDestinatario.Nombre == Filtro.CiudadDestinatario.Nombre) {
                        cont++;
                    }
                }
                if (Filtro.Cliente !== undefined) {

                    if (item.Remesa.Cliente.Codigo > 0) {
                        if (item.Remesa.Cliente.Codigo == Filtro.Cliente.Codigo) {
                            cont++;
                        }
                    }

                    if (item.Remesa.Remitente.Codigo > 0) {
                        if (item.Remesa.Remitente.Codigo == Filtro.Cliente.Codigo) {
                            cont++;
                        }
                    }
                }
                item.Reporte = false
                if (cont == nFiltrosAplica) {
                    $scope.ListadoRemesasRecogidasFiltradas.push(item);
                }
            }
            $scope.PagRecogidasGuardadas.array = $scope.ListadoRemesasRecogidasFiltradas;
            ResetPaginacion($scope.PagRecogidasGuardadas);
        };

        //----AutoComplete Rutas Filtadas
        $scope.AutocompleteRutasFiltradas = function (value) {
            var tmparr = [];
            if (value.length > 0 && $scope.ListaRutasFiltradas != undefined) {
                for (var i = 0; i < $scope.ListaRutasFiltradas.length; i++) {
                    var NombreRuta = $scope.ListaRutasFiltradas[i].Nombre;
                    if (NombreRuta.likeFind(value + "%")) {
                        tmparr.push($scope.ListaRutasFiltradas[i]);
                    }
                }
            }
            return tmparr;
        };
        //----AutoComplete Rutas Filtadas
        //--Validar Vehiculo
        $scope.ValidarVehiculo = function () {
            if ($scope.Modelo.Planilla.Vehiculo != undefined && $scope.Modelo.Planilla.Vehiculo != null && $scope.Modelo.Planilla.Vehiculo != "") {
                var Vehiculo = $scope.Modelo.Planilla.Vehiculo;
                var ResponsListaNegra = VehiculosFactory.ConsultarEstadoListaNegra({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: Vehiculo.Codigo,
                    Sync: true
                });
                if (ResponsListaNegra.ProcesoExitoso == false) {
                    if (VehiculosFactory.ConsultarDocumentosSoatRTM({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: Vehiculo.Codigo,
                        Sync: true
                    }).ProcesoExitoso) {
                        if (Vehiculo.Estado.Codigo == 0) {
                            ShowError("El vehículo " + Vehiculo.Placa + " esta inactivo por motivo '" + Vehiculo.JustificacionBloqueo + "'");
                            $scope.Modelo.Planilla.Vehiculo = "";
                            $scope.CodigoAlternoVehiculo = "";
                            $scope.Modelo.Planilla.Tenedor = "";
                            return false;
                        }
                        else {
                            $scope.ObtenerInformacionTenedor();
                        }
                    }
                    else {
                        ShowError("El vehículo tiene documentos proximos a vencerse");
                        $scope.Modelo.Planilla.Vehiculo = "";
                        $scope.CodigoAlternoVehiculo = "";
                        $scope.Modelo.Planilla.Tenedor = "";
                        return false;
                    }
                }
                else {
                    ShowError("El vehículo se encuentra mal matriculado ante el RNDC");
                    $scope.Modelo.Planilla.Vehiculo = "";
                    $scope.CodigoAlternoVehiculo = "";
                    $scope.Modelo.Planilla.Tenedor = "";
                    return false;
                }
            }
        };
        //--Validar Vehiculo
        //--Informacion Tenedor
        $scope.ObtenerInformacionTenedor = function () {
            if ($scope.Modelo.Planilla.Vehiculo != undefined && $scope.Modelo.Planilla.Vehiculo != null && $scope.Modelo.Planilla.Vehiculo != '') {
                $scope.Modelo.Planilla.Semirremolque = $scope.Modelo.Planilla.Vehiculo.Semirremolque;
                $scope.Modelo.Planilla.Conductor = $scope.Modelo.Planilla.Vehiculo.Conductor;
                $scope.Modelo.Planilla.Tenedor = $scope.Modelo.Planilla.Vehiculo.Tenedor;
                TMPConductor = angular.copy($scope.Modelo.Planilla.Vehiculo.Conductor);
                $scope.Modelo.Planilla.Conductor = "";
                $scope.CodigoAlternoVehiculo = $scope.Modelo.Planilla.Vehiculo.CodigoAlterno;
                if ($scope.Modelo.Planilla.Tenedor !== undefined) {
                    if ($scope.Modelo.Planilla.Tenedor.Codigo > 0) {
                        var response = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.Planilla.Tenedor.Codigo, Sync: true });
                        if (response.ProcesoExitoso == true) {
                            if (response.Datos.Codigo > 0) {
                                if (response.Datos.Proveedor != undefined) {
                                    if (response.Datos.Proveedor.Tarifario !== undefined) {
                                        if (response.Datos.Proveedor.Tarifario.Codigo > 0) {
                                            $scope.Planilla.TarifarioCompra = response.Datos.Proveedor.Tarifario;
                                            $scope.Modelo.Planilla.Tenedor = {
                                                NombreCompleto: response.Datos.NombreCompleto,
                                                Codigo: response.Datos.Codigo,
                                                NumeroIdentificacion: response.Datos.NumeroIdentificacion
                                            };
                                            ObtenerInformacionTarifario();
                                        }
                                        else {
                                            ShowError('El tenedor del vehículo seleccionado no posee un tarifario de compra asignado');
                                            LimpiarEncabezado();
                                            $scope.Planilla.TarifarioCompra = {};
                                        }
                                    }
                                    else {
                                        ShowError('Error al consultar el tarifario del tenedor');
                                        LimpiarEncabezado();
                                        $scope.Planilla.TarifarioCompra = {};
                                    }
                                }
                                else {
                                    ShowError('Error al consultar la información del tenedor, no se encontró tarifario asociado');
                                    LimpiarEncabezado();
                                    $scope.Planilla.TarifarioCompra = {};
                                }
                            }
                            else {
                                ShowError('Error al consultar la información del tenedor');
                                LimpiarEncabezado();
                                $scope.Planilla.TarifarioCompra = {};
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                }
            }
        };
        $scope.ObternerVehiculo = function (CodigoAlternoVehiculo) {
            if (CodigoAlternoVehiculo != "" && CodigoAlternoVehiculo != undefined && CodigoAlternoVehiculo != null) {
                VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, CodigoAlterno: CodigoAlternoVehiculo }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                //$scope.Modelo.Planilla.Vehiculo = response.data.Datos[0];
                                $scope.Modelo.Planilla.Vehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos[0].Codigo, Sync: true }).Datos;
                                $scope.ObtenerInformacionTenedor();
                            }
                            else {
                                $scope.CodigoAlternoVehiculo = "";
                            }
                        }
                        else {
                            $scope.CodigoAlternoVehiculo = "";
                        }
                    }, function (response) {
                    });
            }
        };
        //--Obtener Tarifario
        function ObtenerInformacionTarifario() {
            var response = TarifarioComprasFactory.Obtener({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Planilla.TarifarioCompra.Codigo,
                Sync: true
            });
            if (response.ProcesoExitoso == true) {
                $scope.Planilla.TarifarioCompra = response.Datos;
                $scope.ListadoTarifas = [];
                $scope.ListaRutasFiltradas = [];
                for (var i = 0; i < $scope.Planilla.TarifarioCompra.Tarifas.length; i++) {
                    if ($scope.Planilla.TarifarioCompra.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO ||
                        $scope.Planilla.TarifarioCompra.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                        $scope.ListadoTarifas.push($scope.Planilla.TarifarioCompra.Tarifas[i]);
                    }
                }
                var numTarifas = 0;
                for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                    var aplica = 0;
                    for (var j = 0; j < $scope.ListadoTarifas.length; j++) {
                        if ($scope.ListadoRutas[i].Codigo == $scope.ListadoTarifas[j].Ruta.Codigo) {
                            aplica++;
                            numTarifas++;
                            break;
                        }
                    }
                    if (aplica > 0) {
                        $scope.ListaRutasFiltradas.push($scope.ListadoRutas[i]);
                    }
                    if (numTarifas >= $scope.ListadoTarifas.length) {
                        break;
                    }
                }
                if ($scope.Modelo.Planilla.Ruta != undefined) {
                    if ($scope.Modelo.Planilla.Ruta.Codigo > 0) {
                        $scope.Modelo.Planilla.Ruta = $linq.Enumerable().From($scope.ListaRutasFiltradas).First('$.Codigo ==' + $scope.Modelo.Planilla.Ruta.Codigo);
                        CargarImpuestos();
                        FiltrarTarifas();
                    }
                }
            }
            else {
                ShowError(response.statusText);
            }
        }
        $scope.ValidarRuta = function () {
            if ($scope.Modelo.Planilla.Ruta !== undefined && $scope.Modelo.Planilla.Ruta !== null && $scope.Modelo.Planilla.Ruta !== "") {
                if ($scope.Modelo.Planilla.Ruta.Codigo > 0) {
                    var Ruta = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.Planilla.Ruta.Codigo, Sync: true }).Datos;
                    //--Carga Impuestos
                    if ($scope.Modelo.Planilla.DetalleImpuesto !== undefined && $scope.Modelo.Planilla.DetalleImpuesto !== null && $scope.Modelo.Planilla.DetalleImpuesto !== []) {
                        $scope.ListadoImpuestosFiltrado = $scope.Modelo.Planilla.DetalleImpuesto;
                    }
                    else {
                        CargarImpuestos();
                    }
                    //--Carga Impuestos
                    //--Sugiere Oficina Destino
                    $scope.Modelo.OficinaDestino = $scope.CargarOficina(Ruta.OficinaDestino.Codigo);
                    //--Sugiere Oficina Destino
                    //--Filtra Tarifas
                    FiltrarTarifas();
                    //--Filtra Tarifas

                    CalcularAnticipo();
                    $scope.Modelo.Planilla.FechaLlegada = $scope.Modelo.Planilla.FechaSalida
                    $scope.Modelo.Planilla.FechaLlegada.setHours($scope.Modelo.Planilla.Ruta.DuracionHoras)
                }
            }
        };

        function CalcularAnticipo() {
            var resVal
            resVal = PlanillaPaqueteriaFactory.CalcularAnticipo({
                ParametrosCalculos: { SugerirAnticipoPlanilla: $scope.Sesion.UsuarioAutenticado.SugerirAnticipoPlanilla, DeshabilitarAnticipoPlanillaPaqueteria: $scope.Sesion.UsuarioAutenticado.DeshabilitarAnticipoPlanillaPaqueteria },
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Planilla: {
                    Ruta: { Codigo: $scope.Modelo.Planilla.Ruta.Codigo },
                    Vehiculo: {
                        Codigo: $scope.Modelo.Planilla.Vehiculo.Codigo,
                        TipoDueno: { Codigo: $scope.Modelo.Planilla.Vehiculo.TipoDueno ? $scope.Modelo.Planilla.Vehiculo.TipoDueno.Codigo : 0 },
                        TipoVehiculo: { Codigo: $scope.Modelo.Planilla.Vehiculo.TipoVehiculo ? $scope.Modelo.Planilla.Vehiculo.TipoVehiculo.Codigo : 0 },
                        Placa: $scope.Modelo.Planilla.Vehiculo.Placa
                    },
                    Semirremolque: { Codigo: $scope.Modelo.Planilla.Semirremolque ? $scope.Modelo.Planilla.Semirremolque.Codigo : 0, Placa: $scope.Modelo.Planilla.Semirremolque ? $scope.Modelo.Planilla.Semirremolque.Placa : '' },
                },
                Sync: true
            });

            if (resVal.ProcesoExitoso) {
                $scope.Modelo.Planilla.ValorAnticipo = MascaraValores(resVal.Datos.Planilla.ValorAnticipo);
            }
        }
        //--Validacion Precintos
        $scope.ValidarPrecinto = function (Precinto) {
            if (Precinto != undefined && Precinto != '' && Precinto != null && Precinto > 0) {
                var result = PrecintosFactory.ValidarPrecintoPlanillaPaqueteria({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: Precinto,
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                    Sync: true
                });
                if (result.ProcesoExitoso) {
                    if (result.Datos.Numero > 0) {
                        if (result.Datos.ValidaNumero > 0) {
                            if (!(result.Datos.ValidaNumero == $scope.Modelo.Planilla.Numero &&
                                result.Datos.ValidaTipoDocumento == CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA &&
                                $scope.Modelo.Planilla.Numero > 0)) {
                                $scope.Modelo.NumeroPrecintoPaqueteria = '';
                                switch (result.Datos.ValidaTipoDocumento) {
                                    case CODIGO_TIPO_DOCUMENTO_ORDEN_CARGUE:
                                        ShowError("El Precinto ya encuentra asignado a la Orden de Cargue No. " + result.Datos.ValidaNumeroDocumento);
                                        break;
                                    case CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA:
                                        ShowError("El Precinto ya encuentra asignado a la guía No. " + result.Datos.ValidaNumeroDocumento);
                                        break;
                                    case CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA:
                                        ShowError("El Precinto ya encuentra asignado a la Planilla Paquetería No. " + result.Datos.ValidaNumeroDocumento);
                                        break;
                                    default:
                                        ShowError("El Precinto ya encuentra asignado");
                                        break;
                                }
                            }
                        }
                    }
                    else {
                        $scope.Modelo.NumeroPrecintoPaqueteria = '';
                        ShowError("El Precinto no existe");
                    }
                }
                else {
                    $scope.Modelo.NumeroPrecintoPaqueteria = '';
                    ShowError("No se pudo consultar la diponibilidad del Precinto");
                }
            }
        };
        //--Validacion Precinto

        //----Carga Impuestos Documento
        function CargarImpuestos() {
            var response = ImpuestosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: ESTADO_DEFINITIVO,
                CodigoTipoDocumento: 135,
                CodigoCiudad: $scope.Modelo.Planilla.Ruta.CiudadOrigen.Codigo,
                AplicaTipoDocumento: 1,
                Sync: true
            });
            if (response.ProcesoExitoso == true) {
                if (response.Datos.length > CERO) {
                    $scope.ListadoImpuestos = response.Datos;
                    if ($scope.ListadoImpuestos.length > 0) {
                        for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                            var impuesto = {
                                Nombre: $scope.ListadoImpuestos[i].Nombre,
                                CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                                ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                                ValorBase: $scope.ListadoImpuestos[i].valor_base,
                                ValorImpuesto: 0
                            };
                            $scope.ListadoImpuestosFiltrado.push(impuesto);
                        }
                    }
                }
            }
            else {
                ShowError(response.statusText);
            }
        }
        //----Carga Impuestos Documento
        //----Filtrar Tarifas
        function FiltrarTarifas() {
            $scope.ListaTarifas = [];
            $scope.ListadoTipoTarifas = [];
            var Ruta = $scope.Modelo.Planilla.Ruta;
            for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                var item = $scope.ListadoTarifas[i];
                if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO ||
                    item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                    if (Ruta.Codigo == item.Ruta.Codigo) {
                        if ($scope.ListaTarifas.length == 0) {
                            $scope.ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte);
                        }
                        else {
                            var cont = 0;
                            for (var j = 0; j < $scope.ListaTarifas.length; j++) {
                                if ($scope.ListaTarifas[j].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                    cont++;
                                }
                            }
                            if (cont == 0) {
                                $scope.ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte);
                            }
                        }
                        item.Codigo = item.TipoTarifaTransportes.Codigo;
                        $scope.ListadoTipoTarifas.push(item);
                    }
                }
            }
            if ($scope.ListaTarifas.length == 0 && $scope.ListadoTipoTarifas == 0) {
                ShowError('La ruta ingresada no se encuentra asociada al tarifario de compras del tenedor ' + $scope.Modelo.Planilla.Vehiculo.Tenedor.NombreCompleto);
                LimpiarEncabezado();
            }
            else {
                $scope.Modelo.Planilla.TarifaTransportes = $scope.ListaTarifas[0];
                $scope.FiltrarTipoTarifa();
            }
        }
        //----Filtrar Tarifas
        //----Filtrar Tipo Tarifas
        $scope.FiltrarTipoTarifa = function (obtener) {
            $scope.ListaTipoTarifas = [];
            for (var i = 0; i < $scope.ListadoTipoTarifas.length; i++) {
                var item = $scope.ListadoTipoTarifas[i];
                if ($scope.Modelo.Planilla.TarifaTransportes.Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                    $scope.ListaTipoTarifas.push(item);
                }
            }
            //$scope.TipoTarifaTransportes = $scope.ListaTipoTarifas[0];
            if (obtener == undefined) {
                if ($scope.Modelo.Planilla.Vehiculo != undefined && $scope.Modelo.Planilla.Vehiculo != null && $scope.Modelo.Planilla.Vehiculo != "" &&
                    $scope.Modelo.Planilla.TarifaTransportes.Codigo == 3) {//--Existe vehiculo y la tarifa es tipo vehiculo
                    try {
                        $scope.TipoTarifaTransportes = $linq.Enumerable().From($scope.ListaTipoTarifas).First('$.NombreTipoTarifa ==' + "'" + $scope.Modelo.Planilla.Vehiculo.TipoVehiculo.Nombre + "'");
                    } catch (e) {
                        //$scope.TipoTarifaTransportes = $scope.ListaTipoTarifas[0];
                    }
                }
            }
            $scope.Calcular();
        };
        //----Filtrar Tipo Tarifas
        //----LimpiarEncabezado
        function LimpiarEncabezado() {
            $scope.Modelo.Planilla.Vehiculo = '';
            $scope.Modelo.Planilla.Semirremolque = '';
            $scope.Modelo.Planilla.Conductor = '';
            $scope.Modelo.Planilla.Tenedor = '';
            $scope.Modelo.Planilla.Conductor = '';
            $scope.Modelo.Planilla.Ruta = '';
            $scope.ListaTarifas = [];
            $scope.ListaRutasFiltradas = [];
            $scope.ListaTipoTarifas = [];
        }
        //----LimpiarEncabezado
        //----Navegacion Taps
        $scope.MostrarSeccionTap = function (Seccion) {
            $scope.CargarRemesas = {
                FechaInicial: new Date(),
                FechaFinal: new Date(),
                NumeroInicial: '',
                NumeroPlanillaCargue: '',
                CiudadRemitente: '',
                CiudadDestinatario: '',
                Cliente: '',
                NumeroDocumentoCliente: '',
                Remitente: '',
                LineaNegocioPaqueteria: $linq.Enumerable().From($scope.ListadoLineaNegocioPaqueteria).First('$.Codigo ==-1')
            };
            $scope.Filtro = {
                NumeroInicial: '',
                NumeroFinal: '',
                FechaInicial: '',
                FechaFinal: ''
            };
            if ($scope.ListadoOficinas.length > 0) {
                $scope.CargarRemesas.OficinaOrigen = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
            }
            $(".Remesas").hide();
            $(".Recolecciones").hide();
            $(".Recogidas").hide();
            $(".GuiasRetorno").hide();
            switch (Seccion) {
                case 1: //--Guias
                    $scope.TituloCargar = "Guías";
                    $(".Remesas").show();
                    $(".BoxCargar").show();
                    $(".BoxFiltrar").hide();
                    break;
                case 2: //--Recolecciones
                    $scope.TituloCargar = "Recolecciones";
                    $(".Recolecciones").show();
                    $(".BoxCargar").show();
                    $(".BoxFiltrar").show();
                    break;
                case 3: //--Recogidas
                    $scope.TituloCargar = "Guías Recogidas";
                    $(".Recogidas").show();
                    $(".BoxCargar").hide();
                    $(".BoxFiltrar").hide();
                    break;
                case 4: //--Guias Retorno
                    $scope.TituloCargar = "Guías Retorno";
                    $(".GuiasRetorno").show();
                    $(".BoxCargar").hide();
                    $(".BoxFiltrar").hide();
                    break;
            }
        };
        //----Navegacion Taps
        //----------------------------REMESAS-------------------------------//
        //--Cargar Remesas
        $scope.CargarListadoRemesas = function () {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Cargando Guías...");
            $timeout(function () { blockUI.message("Cargando Guías..."); ObtenerRemesas(); }, 100);
        };

        function ObtenerRemesas() {
            $scope.PagRemesas.Buscando = true;
            $scope.ListadoRemesas = [];
            $scope.ListadoRemesasFiltradas = [];
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                FechaInicial: $scope.CargarRemesas.FechaInicial,
                FechaFinal: $scope.CargarRemesas.FechaFinal,
                NumeroInicial: $scope.CargarRemesas.NumeroInicial,
                NumeroDocumentoPlanillaCargue: $scope.CargarRemesas.NumeroPlanillaCargue,
                Remesa: {
                    CiudadRemitente: $scope.CargarRemesas.CiudadRemitente,
                    CiudadDestinatario: $scope.CargarRemesas.CiudadDestinatario,
                    Cliente: $scope.CargarRemesas.Cliente,
                    NumeroDocumentoCliente: $scope.CargarRemesas.NumeroDocumentoCliente,
                    Remitente: $scope.CargarRemesas.Remitente
                },
                Zona: $scope.CargarRemesas.Zona,
                NumeroPlanilla: 0,
                Estado: ESTADO_DEFINITIVO,
                Anulado: ESTADO_NO_ANULADO,
                Reexpedicion: -1,
                CodigoOficinaActualUsuario: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                LineaNegocioPaqueteria: $scope.CargarRemesas.LineaNegocioPaqueteria,
                Sync: true
            };

            var response = RemesaGuiasFactory.ConsultarRemesasPorPlanillar(filtro);
            if (response.ProcesoExitoso === true) {
                if (response.Datos.length > 0) {
                    if ($scope.ListadoRemesasGuardadas != undefined && $scope.ListadoRemesasGuardadas.length > 0) {
                        //Verifica si existen remesas guardadas con la busqueda actual, para no generar duplicidad
                        for (var i = 0; i < response.Datos.length; i++) {
                            var Existe = false;
                            for (var j = 0; j < $scope.ListadoRemesasGuardadas.length; j++) {
                                if (response.Datos[i].Remesa.Numero == $scope.ListadoRemesasGuardadas[j].Remesa.Numero) {
                                    Existe = true;
                                    break;
                                }
                            }
                            if (!Existe) {
                                response.Datos[i].Reporte = false
                                $scope.ListadoRemesas.push(response.Datos[i]);
                            }
                        }
                    }
                    else {
                        $scope.ListadoRemesas = response.Datos;
                    }
                    $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas;
                    $scope.PagRemesas.array = $scope.ListadoRemesasFiltradas;
                    $scope.PagRemesas.ResultadoSinRegistros = '';
                    ResetPaginacion($scope.PagRemesas);
                }
                else {
                    $scope.PagRemesas.totalRegistros = 0;
                    $scope.PagRemesas.totalPaginas = 0;
                    $scope.PagRemesas.paginaActual = 1;
                    $scope.PagRemesas.array = [];
                    ResetPaginacion($scope.PagRemesas);
                    $scope.PagRemesas.ResultadoSinRegistros = 'No hay datos para mostrar';
                }
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
            }
            else {
                ShowError(response.statusText);
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
            }
            $scope.PagRemesas.Buscando = false;
        }
        //--Cargar Remesas

        //--Generar Reporte
        $scope.generarReporte = function () {
            var numero = ''

            $scope.ListadoRemesasGuardadas.forEach(item => {
                if (item.Reporte == true) {
                    $scope.ListadoReporte.push(item.Remesa.Numero)
                    if (numero == '') {
                        numero = item.Remesa.Numero
                    } else {
                        numero = numero + ',' + item.Remesa.Numero
                    }
                }
            });

            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NombreReporte = 'repPaqueteriaGuiasEntrega';

            //Llama a la pagina ASP con el filtro por GET
            window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&Numero=' + numero + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo);
            
        }


        $scope.generarReporteRecogidas = function () {
            var numero = ''
            $scope.PagRecogidasGuardadas.PagArray.forEach(item => {
                if (item.Reporte == true) {
                    $scope.ListadoReporte.push(item.Remesa.Numero)
                    if (numero == '') {
                        numero = item.Remesa.Numero
                    } else {
                        numero = numero + ',' + item.Remesa.Numero
                    }
                }
            });

            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NombreReporte = 'repPaqueteriaGuiasEntrega';

            //Llama a la pagina ASP con el filtro por GET
            window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&Numero=' + numero + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo);

        }

        //--Filtrar Remesas
        $scope.FiltrarRemesas = function () {
            var Filtro = $scope.Filtro;
            var nFiltrosAplica = 0;
            if (Filtro.CiudadDestinatario !== undefined) {
                nFiltrosAplica++;
            }
            if (Filtro.Cliente !== undefined) {
                nFiltrosAplica++;
            }            
            $scope.ListadoRemesasFiltradas = [];
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                var cont = 0
                var item = $scope.ListadoRemesasGuardadas[i];
                if (Filtro.CiudadDestinatario !== undefined) {
                    if (item.Remesa.CiudadDestinatario.Codigo == Filtro.CiudadDestinatario.Codigo) {
                        cont++;
                    }
                }
                if (Filtro.Cliente !== undefined) {

                    if (item.Remesa.Cliente.Codigo > 0) {
                        if (item.Remesa.Cliente.Codigo == Filtro.Cliente.Codigo) {
                            cont++;
                        }
                    }

                    if (item.Remesa.Remitente.Codigo > 0) {
                        if (item.Remesa.Remitente.Codigo == Filtro.Cliente.Codigo) {
                            cont++;
                        }
                    }                    
                }               
                item.Reporte = false
                if (cont == nFiltrosAplica) {
                    item.Calcular = true;                    
                    $scope.ListadoRemesasFiltradas.push(item);
                }
            }
            $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasFiltradas;
            ResetPaginacion($scope.PagRemesasGuardadas);
        };
        //--Filtrar Remesas
        //--Limpiar Filtro Remesas
        $scope.LimpiarRemesas = function () {
            $scope.ListadoRemesas = [];
            $scope.ListadoRemesasFiltradas = [];
            $scope.Calcular();
        };
        //--Limpiar Filtro Remesas
        //--Marcar Remesas
        $scope.MarcarRemesas = function () {
            for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                $scope.ListadoRemesasGuardadas.push($scope.ListadoRemesasFiltradas[i]);
            }
            $scope.ListadoRemesasFiltradas = [];

            $scope.PagRemesas.array = $scope.ListadoRemesasFiltradas;
            ResetPaginacion($scope.PagRemesas);

            $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
            ResetPaginacion($scope.PagRemesasGuardadas);

            $scope.Calcular();
        };
        //--Marcar Remesas
        //--Agregar Remesa a Planilla
        $scope.AgregarRemesaPlanilla = function (ENRE_Numero) {
            var indexReme = 0;
            for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                if ($scope.ListadoRemesasFiltradas[i].Remesa.Numero == ENRE_Numero) {
                    indexReme = i;
                    break;
                }
            }
            $scope.ListadoRemesasGuardadas.push($scope.ListadoRemesasFiltradas[indexReme]);
            $scope.ListadoRemesasFiltradas.splice(indexReme, 1);

            $scope.PagRemesas.array = $scope.ListadoRemesasFiltradas;
            ResetPaginacion($scope.PagRemesas);

            $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
            ResetPaginacion($scope.PagRemesasGuardadas);

            $scope.Calcular();
        };
        //--Agregar Remesa a Planilla
        //--Eliminar Remesa
        $scope.EliminarGuiaGuardada = function (item) {
            var index = 0;
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                if ($scope.ListadoRemesasGuardadas[i].Remesa.Numero == item.Remesa.Numero) {
                    index = i;
                    break;
                }
            }
            var ResponBorraGuia = PlanillaPaqueteriaFactory.EliminarGuia({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Planilla.Numero,
                Planilla: {
                    Detalles: [{ Remesa: { Numero: item.Remesa.Numero } }]
                },
                Sync: true
            });
            if (ResponBorraGuia.ProcesoExitoso == true) {
                $scope.ListadoRemesasGuardadas.splice(index, 1);
                $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
                var tmpPagAct = $scope.PagRemesasGuardadas.paginaActual;
                $scope.Calcular();
                ResetPaginacion($scope.PagRemesasGuardadas);
                if (tmpPagAct != 1) {
                    $scope.PagRemesasGuardadas.paginaActual = tmpPagAct - 1;
                    $scope.Siguiente($scope.PagRemesasGuardadas);
                }
            }
            else {
                ShowError(ResponBorraGuia.statusText);
            }
        };
        //--Eliminar Remesa
        //----------------------------REMESAS-------------------------------//
        //----------------------------RECOLECCIONES-------------------------------//
        //--Cargar Recolecciones
        $scope.CargarListadoRecolecciones = function () {
            if (DatosRequeridosRecolecciones()) {
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Recolecciones...");
                $timeout(function () { blockUI.message("Cargando Recolecciones..."); ObtenerRecolecciones(); }, 100);
            }
        };
        function ObtenerRecolecciones() {
            $scope.PagRecoleccion.Buscando = true;
            $scope.ListadoRecolecciones = [];
            $scope.ListadoRecoleccionesFiltradas = [];
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.CargarRemesas.NumeroInicial,
                FechaInicio: $scope.CargarRemesas.FechaInicial,
                FechaFin: $scope.CargarRemesas.FechaFinal,
                Ciudad: $scope.CargarRemesas.CiudadRemitente,
                Zonas: $scope.CargarRemesas.Zona,
                OficinaGestiona: $scope.CargarRemesas.OficinaOrigen,
                Cliente: $scope.CargarRemesas.Cliente,
                Estado: ESTADO_DEFINITIVO,
                Sync: true
            };
            var response = RecoleccionesFactory.ConsultarRecoleccionesPorPlanillar(filtro);
            if (response.ProcesoExitoso == true) {
                if (response.Datos.length > 0) {
                    if ($scope.ListadoRecoleccionesGuardadas != undefined && $scope.ListadoRecoleccionesGuardadas.length > 0) {
                        //Verifica si existen recolecciones guardadas con la busqueda actual, para no generar duplicidad
                        for (var i = 0; i < response.Datos.length; i++) {
                            var Existe = false;
                            for (var j = 0; j < $scope.ListadoRecoleccionesGuardadas.length; j++) {
                                if (response.Datos[i].Numero == $scope.ListadoRecoleccionesGuardadas[j].Numero) {
                                    Existe = true;
                                    break;
                                }
                            }
                            if (!Existe) {
                                $scope.ListadoRecolecciones.push(response.Datos[i]);
                            }
                        }
                    }
                    else {
                        $scope.ListadoRecolecciones = response.Datos;
                    }
                    $scope.ListadoRecoleccionesFiltradas = $scope.ListadoRecolecciones;
                    $scope.PagRecoleccion.array = $scope.ListadoRecoleccionesFiltradas;
                    $scope.PagRecoleccion.ResultadoSinRegistros = '';
                    ResetPaginacion($scope.PagRecoleccion);
                }
                else {
                    $scope.PagRecoleccion.totalRegistros = 0;
                    $scope.PagRecoleccion.totalPaginas = 0;
                    $scope.PagRecoleccion.paginaActual = 1;
                    $scope.PagRecoleccion.array = [];
                    ResetPaginacion($scope.PagRecoleccion);
                    $scope.PagRecoleccion.ResultadoSinRegistros = 'No hay datos para mostrar';
                }
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
            }
            else {
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                ShowError(response.statusText);
            }
            $scope.PagRecoleccion.Buscando = false;
        }
        function DatosRequeridosRecolecciones() {
            $scope.MensajesError = [];
            var continuar = true;
            var filtro = $scope.CargarRemesas;
            if ((filtro.FechaInicial === null || filtro.FechaInicial === undefined || filtro.FechaInicial === '')
                && (filtro.FechaFinal === null || filtro.FechaFinal === undefined || filtro.FechaFinal === '')
                && (filtro.NumeroInicial === null || filtro.NumeroInicial === undefined || filtro.NumeroInicial === '' || filtro.NumeroInicial === 0 || isNaN(filtro.NumeroInicial) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if ((filtro.NumeroInicial !== null && filtro.NumeroInicial !== undefined && filtro.NumeroInicial !== '' && filtro.NumeroInicial !== 0)
                || (filtro.FechaInicial !== null && filtro.FechaInicial !== undefined && filtro.FechaInicial !== '')
                || (filtro.FechaFinal !== null && filtro.FechaFinal !== undefined && filtro.FechaFinal !== '')

            ) {
                if ((filtro.FechaInicial !== null && filtro.FechaInicial !== undefined && filtro.FechaInicial !== '')
                    && (filtro.FechaFinal !== null && filtro.FechaFinal !== undefined && filtro.FechaFinal !== '')) {
                    if (filtro.FechaFinal < filtro.FechaInicial) {
                        filtro.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if (((filtro.FechaFinal - filtro.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        filtro.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }

                else {
                    if ((filtro.FechaInicial !== null && filtro.FechaInicial !== undefined && filtro.FechaInicial !== '')) {
                        filtro.FechaFinal = filtro.FechaInicial;
                    } else {
                        filtro.FechaInicial = filtro.FechaFinal;
                    }
                }
            }
            return continuar
        }
        //--Cargar Recolecciones
        //--Cargar Zonas Ciudad
        $scope.CambiarCiudad = function (ciudad) {
            $scope.ObjetoCiudad = ciudad;
            CambiarZona();
        };
        function CambiarZona() {
            if ($scope.ObjetoCiudad != undefined && $scope.ObjetoCiudad != null && $scope.ObjetoCiudad != '') {
                var response = ZonasFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Ciudad: $scope.ObjetoCiudad,
                    Estado: ESTADO_DEFINITIVO,
                    Sync: true
                });
                if (response.ProcesoExitoso === true) {
                    if (response.Datos.length > 0) {
                        $scope.ListadoZonas = [];
                        $scope.ListadoZonas.push({ Nombre: '(TODAS)', Codigo: 0 });
                        response.Datos.forEach(function (item) {
                            if (item.Codigo !== 0) {
                                $scope.ListadoZonas.push(item);
                            }
                        });
                        $scope.CargarRemesas.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==0');
                    }
                    else {
                        $scope.ListadoZonas = [];
                    }
                }
            }
            else {
                $scope.ListadoZonas = [];
                $scope.CargarRemesas.Zona = '';
            }
        }
        //--Cargar Zonas Ciudad
        //--Filtrar Recolecciones
        $scope.FiltrarRecolecciones = function () {
            var Filtro = $scope.Filtro;
            var nFiltrosAplica = 0;
            if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                nFiltrosAplica++;
            }
            if (Filtro.NumeroFinal !== undefined && Filtro.NumeroFinal > 0) {
                nFiltrosAplica++;
            }
            if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                nFiltrosAplica++;
            }
            if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                nFiltrosAplica++;
            }
            $scope.ListadoRecoleccionesFiltradas = [];
            for (var i = 0; i < $scope.ListadoRecolecciones.length; i++) {
                var cont = 0
                var item = $scope.ListadoRecolecciones[i];
                if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                    if (item.NumeroDocumento >= Filtro.NumeroInicial) {
                        cont++;
                    }
                }
                if (Filtro.NumeroFinal !== undefined && Filtro.NumeroFinal > 0) {
                    if (item.NumeroDocumento <= Filtro.NumeroFinal) {
                        cont++;
                    }
                }
                if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                    if (new Date(item.Fecha) >= Filtro.FechaInicial) {
                        cont++;
                    }
                }
                if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                    if (new Date(item.Fecha) <= Filtro.FechaFinal) {
                        cont++;
                    }
                }
                if (cont == nFiltrosAplica) {
                    item.Calcular = true;
                    $scope.ListadoRecoleccionesFiltradas.push(item);
                }
            }
            $scope.PagRecoleccion.array = $scope.ListadoRecoleccionesFiltradas;
            ResetPaginacion($scope.PagRecoleccion);
        };
        //--Filtrar Recolecciones
        //--Limpiar Filtro Remesas
        $scope.LimpiarRecolecciones = function () {
            $scope.ListadoRecolecciones = [];
            $scope.ListadoRecoleccionesFiltradas = [];
            $scope.Calcular();
        };
        //--Limpiar Filtro Recolecciones
        //--Marcar Recolecciones
        $scope.MarcarRecolecciones = function (chk) {
            for (var i = 0; i < $scope.ListadoRecoleccionesFiltradas.length; i++) {
                $scope.ListadoRecoleccionesGuardadas.push($scope.ListadoRecoleccionesFiltradas[i]);
            }
            $scope.ListadoRecoleccionesFiltradas = [];

            $scope.PagRecoleccion.array = $scope.ListadoRecoleccionesFiltradas;
            ResetPaginacion($scope.PagRecoleccion);

            $scope.PagRecoleccionGuardadas.array = $scope.ListadoRecoleccionesGuardadas;
            ResetPaginacion($scope.PagRecoleccionGuardadas);

            $scope.Calcular();
        };
        //--Marcar Recolecciones
        //--Agregar Recoleccion a Planilla
        $scope.AgregarRecoleccionlanilla = function (RECO_Numero) {
            var indexReco = 0;
            for (var i = 0; i < $scope.ListadoRecoleccionesFiltradas.length; i++) {
                if ($scope.ListadoRecoleccionesFiltradas[i].Numero == RECO_Numero) {
                    indexReco = i;
                    break;
                }
            }
            $scope.ListadoRecoleccionesGuardadas.push($scope.ListadoRecoleccionesFiltradas[indexReco]);
            $scope.ListadoRecoleccionesFiltradas.splice(indexReco, 1);

            $scope.PagRecoleccion.array = $scope.ListadoRecoleccionesFiltradas;
            ResetPaginacion($scope.PagRecoleccion);

            $scope.PagRecoleccionGuardadas.array = $scope.ListadoRecoleccionesGuardadas;
            ResetPaginacion($scope.PagRecoleccionGuardadas);

            $scope.Calcular();
        };
        //--Agregar Recoleccion a Planilla
        //--Eliminar Recoleccion
        $scope.EliminarRecoleccionGuardada = function (item) {
            var index = 0;
            for (var i = 0; i < $scope.ListadoRecoleccionesGuardadas.length; i++) {
                if ($scope.ListadoRecoleccionesGuardadas[i].Numero == item.Numero) {
                    index = i;
                    break;
                }
            }
            var ResponBorraRecoleccion = PlanillaPaqueteriaFactory.EliminarRecoleccion({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Planilla.Numero,
                ListadoPlanillaRecolecciones: [{ Numero: item.Numero }],
                Sync: true
            });
            if (ResponBorraRecoleccion.ProcesoExitoso == true) {
                $scope.ListadoRecoleccionesGuardadas.splice(index, 1);
                $scope.PagRecoleccionGuardadas.array = $scope.ListadoRecoleccionesGuardadas;
                var tmpPagAct = $scope.PagRecoleccionGuardadas.paginaActual;
                $scope.Calcular();
                ResetPaginacion($scope.PagRecoleccionGuardadas);
                if (tmpPagAct != 1) {
                    $scope.PagRecoleccionGuardadas.paginaActual = tmpPagAct - 1;
                    $scope.Siguiente($scope.PagRecoleccionGuardadas);
                }
            }
            else {
                ShowError(ResponBorraRecoleccion.MensajeOperacion);
            }
        };
        //--Eliminar Recoleccion
        //----------------------------RECOLECCION-------------------------------//
        //----------------------------AUXILIARES-----------------------------//
        $scope.AgregarAuxiliar = function () {
            if ($scope.ModeloFuncionario !== '' && $scope.ModeloFuncionario !== undefined && $scope.ModeloFuncionario !== null && $scope.ModeloFuncionario.Codigo !== 0 && $scope.ModeloFuncionario.Codigo !== undefined) {
                var concidencias = 0;
                if ($scope.ListaAuxiliares.length > 0) {
                    for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                        if ($scope.ModeloFuncionario.Codigo === $scope.ListaAuxiliares[i].Tercero.Codigo) {
                            concidencias++;
                            break;
                        }
                    }
                    if (concidencias > 0) {
                        ShowError('El funcionario ya fue ingresado');
                        $scope.ModeloFuncionario = '';
                    } else {
                        $scope.ListaAuxiliares.push({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto },
                            Modificarfuncionario: true,
                            Horas: 0,
                            Valor: 0,
                            Observaciones: ""
                        });
                        $scope.ModeloFuncionario = '';
                    }
                } else {
                    $scope.ListaAuxiliares.push({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto },
                        Modificarfuncionario: true,
                        Horas: 0,
                        Valor: 0,
                        Observaciones: ""
                    });
                    $scope.ModeloFuncionario = '';
                }
            } else {
                ShowError('Debe ingresar un funcionario valido');
            }
            //$scope.ValidarDatosFuncionario();
        };
        $scope.ValidarDatosFuncionario = function () {
            for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                //if ($scope.ListaAuxiliares[i].Horas === '' || $scope.ListaAuxiliares[i].Horas === undefined || $scope.ListaAuxiliares[i].Horas === null || $scope.ListaAuxiliares[i].Horas === 0
                //    || $scope.ListaAuxiliares[i].Valor === '' || $scope.ListaAuxiliares[i].Valor === undefined || $scope.ListaAuxiliares[i].Valor === null || $scope.ListaAuxiliares[i].Valor === 0) {
                //    //ShowError('Debe ingresar los detalles de horas trabajadas y valor por funcionario');
                //} else {
                $scope.ListaAuxiliares[i].Modificarfuncionario = false;
                //}
            }
        };
        $scope.EliminarFuncionario = function (codigo) {
            if ($scope.ListaAuxiliares.length > 0) {
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (codigo === $scope.ListaAuxiliares[i].Codigo) {
                        $scope.ListaAuxiliares.splice(i, 1);
                    }
                }
            }
        };
        //----------------------------AUXILIARES-----------------------------//
        //----------------------------CALCULAR-----------------------------//
        $scope.Calcular = function () {

            var resVal
            resVal = PlanillaPaqueteriaFactory.CalcularTotales({
                ParametrosCalculos: {
                    ListadoRemesasFiltradas: $scope.ListadoRemesasFiltradas.map(a => a.Remesa),
                    ListadoRemesasGuardadas: $scope.ListadoRemesasGuardadas.map(a => a.Remesa),
                    ListadoImpuestos: $scope.ListadoImpuestos,
                    FleteTarifaTransporte: $scope.TipoTarifaTransportes ? $scope.TipoTarifaTransportes.ValorFlete : 0,
                    ListadoTotalesRemesas: []
                },
                Planilla: { ValorAnticipo: $scope.Modelo.Planilla.ValorAnticipo },
                Sync: true
            });

            if (resVal.ProcesoExitoso) {
                $scope.ListadoTotalesRemesas = [];
                resVal.Datos.ParametrosCalculos.ListadoTotalesRemesas.forEach(item =>
                    $scope.ListadoTotalesRemesas.push({
                        Cantidad: item.Cantidad,
                        FormaPago: item.FormaPago,
                        ValorCliente: item.ValorCliente,
                        ValorTransportador: item.ValorTransportador,
                        ValorSeguro: item.ValorSeguro
                    }));

                $scope.ListadoImpuestosFiltrado = [];
                resVal.Datos.ParametrosCalculos.ListadoImpuestos.forEach(item =>
                    $scope.ListadoImpuestosFiltrado.push({
                        Nombre: item.Nombre,
                        CodigoImpuesto: item.Codigo,
                        ValorTarifa: item.Valor_tarifa,
                        ValorBase: item.valor_base,
                        ValorImpuesto: item.ValorImpuesto
                    }));
                $scope.Modelo.Planilla.Cantidad = resVal.Datos.Planilla.Cantidad;
                $scope.Modelo.Planilla.Peso = (resVal.Datos.Planilla.Peso).toFixed(2);
                $scope.Modelo.Planilla.ValorFleteTransportador = $scope.MaskValoresGrid(resVal.Datos.Planilla.ValorFleteTransportador);
                $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid(resVal.Datos.Planilla.ValorPagarTransportador);
                $scope.Modelo.Planilla.ValorImpuestos = $scope.MaskValoresGrid(resVal.Datos.Planilla.ValorImpuestos);
                $scope.MaskValores();
            }
        };
        //$scope.CalcularRetornoContado = function () {
        //    var valor = 0;
        //    for (var i = 0; i < $scope.ListadoGuiasRetornoFiltradas.length; i++) {
        //        var item = $scope.ListadoGuiasRetornoFiltradas[i];
        //        if (item.Seleccionado) {
        //            valor += item.Remesa.TotalFleteCliente; //--TotalFleteCliente
        //        }
        //    }
        //    for (var i = 0; i < $scope.ListadoGuiasRetornoGuardadas.length; i++) {
        //        var item = $scope.ListadoGuiasRetornoGuardadas[i];
        //        valor += item.Remesa.TotalFleteCliente; //--TotalFleteCliente
        //    }
        //    $scope.Modelo.Planilla.ValorRetornaContado = MascaraValores(valor);
        //};
        //----------------------------CALCULAR-----------------------------//
        //--Anticipo
        $scope.ValidarAnticipoPlanilla = function (anticipo) {
            if ($scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos) {
            } else {
                anticipo = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorAnticipo));
                if (parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) > 0) {
                    if (anticipo > ((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador)))) {
                        ShowError('El anticipo no puede ser mayor a ' + ((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))))
                        $scope.Modelo.Planilla.ValorAnticipo = 0;
                    } else {
                        $scope.Modelo.Planilla.ValorAnticipo = MascaraValores(anticipo);
                    }
                }
            }
        };
        //--Anticipo
        //----------------------------OBTENER-----------------------------//
        function Obtener() {

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Planilla.Numero
            };
            PlanillaPaqueteriaFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = angular.copy(response.data.Datos);
                        $scope.Modelo.Planilla.FechaSalida = new Date($scope.Modelo.Planilla.FechaHoraSalida);
                        $scope.Modelo.Planilla.Semirremolque = $scope.Modelo.Planilla.Semirremolque.Codigo == 0 ? '' : $scope.CargarSemirremolqueCodigo($scope.Modelo.Planilla.Semirremolque.Codigo);//Carga Semirremolque
                        $scope.Modelo.Planilla.HoraSalida = $scope.Modelo.Planilla.FechaSalida.getHours().toString();
                        $scope.Modelo.Planilla.FechaLlegada = new Date($scope.Modelo.Planilla.FechaEntrega)
                        if ($scope.Modelo.Planilla.FechaSalida.getMinutes().toString().length == 1) {
                            $scope.Modelo.Planilla.HoraSalida += ':0' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString();
                        } else {
                            $scope.Modelo.Planilla.HoraSalida += ':' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString();
                        }
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                        $scope.Modelo.Planilla.Fecha = new Date($scope.Modelo.Planilla.Fecha);

                        //--Remesas
                        if ($scope.Modelo.Planilla.Detalles !== undefined && $scope.Modelo.Planilla.Detalles !== null && $scope.Modelo.Planilla.Detalles !== []) {
                            for (var i = 0; i < $scope.Modelo.Planilla.Detalles.length; i++) {
                                var remesaPaqueteria = $scope.Modelo.Planilla.Detalles[i].RemesaPaqueteria;
                                remesaPaqueteria.Remesa = $scope.Modelo.Planilla.Detalles[i].Remesa;
                                remesaPaqueteria.Reporte = false
                                var remesa = $scope.Modelo.Planilla.Detalles[i];
                                if (remesa.RemesaPaqueteria.TipoProcesoGuia.Codigo == 23701 && remesa.RemesaPaqueteria.GuiaCreadaRutaConductor == 0) {
                                    //--Remesas Generales
                                    $scope.ListadoRemesasGuardadas.push(remesaPaqueteria);
                                }
                                else {
                                    //--Recogidas
                                    $scope.ListadoRemesasRecogidasGuardadas.push(remesaPaqueteria);
                                }
                            }
                            //--Paginacion Remesas
                            $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
                            ResetPaginacion($scope.PagRemesasGuardadas);
                            //--Paginacion Recogidas
                            $scope.ListadoRemesasRecogidasFiltradas = angular.copy($scope.ListadoRemesasRecogidasGuardadas);
                            $scope.PagRecogidasGuardadas.array = $scope.ListadoRemesasRecogidasFiltradas;
                            ResetPaginacion($scope.PagRecogidasGuardadas);
                            //--Paginacion Retorno Contado
                            //$scope.PagGuiasRetornoGuardadas.array = $scope.ListadoGuiasRetornoGuardadas;
                            //ResetPaginacion($scope.PagGuiasRetornoGuardadas);
                        }
                        //--Remesas
                        //--Recolecciones
                        if ($scope.Modelo.ListadoPlanillaRecolecciones !== undefined && $scope.Modelo.ListadoPlanillaRecolecciones !== null && $scope.Modelo.ListadoPlanillaRecolecciones !== []) {
                            $scope.ListadoRecoleccionesGuardadas = $scope.Modelo.ListadoPlanillaRecolecciones;
                            $scope.PagRecoleccionGuardadas.array = $scope.ListadoRecoleccionesGuardadas;
                            ResetPaginacion($scope.PagRecoleccionGuardadas);
                        }
                        //--Recolecciones
                        //--Auxiliares
                        if ($scope.Modelo.Planilla.DetallesAuxiliares !== undefined && $scope.Modelo.Planilla.DetallesAuxiliares !== null && $scope.Modelo.Planilla.DetallesAuxiliares !== []) {
                            for (var i = 0; i < $scope.Modelo.Planilla.DetallesAuxiliares.length; i++) {
                                $scope.ListaAuxiliares.push({
                                    Tercero: $scope.Modelo.Planilla.DetallesAuxiliares[i].Funcionario,
                                    Horas: $scope.Modelo.Planilla.DetallesAuxiliares[i].NumeroHorasTrabajadas,
                                    Valor: $scope.Modelo.Planilla.DetallesAuxiliares[i].Valor,
                                    Observaciones: $scope.Modelo.Planilla.DetallesAuxiliares[i].Observaciones,
                                    Aforador: $scope.Modelo.Planilla.DetallesAuxiliares[i].Aforador == 1 ? true : false,
                                    Modificarfuncionario: false
                                });
                            }
                        }
                        //--Auxiliares
                        $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.Modelo.Oficina.Codigo);
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado);

                        $scope.Modelo.Planilla.Vehiculo = $scope.CargarVehiculos($scope.Modelo.Planilla.Vehiculo.Codigo);
                        $scope.ObtenerInformacionTenedor();
                        $scope.Modelo.Planilla.Conductor = $scope.CargarTercero(response.data.Datos.Planilla.Conductor.Codigo);
                        $scope.Modelo.Planilla.Tenedor = $scope.CargarTercero($scope.Modelo.Planilla.Tenedor.Codigo);
                        $scope.Modelo.Planilla.Semirremolque = $scope.CargarSemirremolqueCodigo(response.data.Datos.Planilla.Semirremolque.Codigo);
                        $scope.Modelo.Planilla.TarifaTransportes = $linq.Enumerable().From($scope.ListaTarifas).First('$.Codigo ==' + response.data.Datos.Planilla.TarifaTransportes.Codigo);
                        $scope.FiltrarTipoTarifa(1);
                        $scope.TipoTarifaTransportes = $linq.Enumerable().From($scope.ListaTipoTarifas).First('$.Codigo ==' + response.data.Datos.Planilla.TipoTarifaTransportes.Codigo);

                        if ($scope.Modelo.OficinaDestino != undefined && $scope.Modelo.OficinaDestino != null && $scope.Modelo.OficinaDestino != '') {
                            if ($scope.Modelo.OficinaDestino.Codigo > 0) {
                                $scope.Modelo.OficinaDestino = $scope.CargarOficina($scope.Modelo.OficinaDestino.Codigo);
                            }
                        }

                        if (response.data.Datos.Anulado == ESTADO_ANULADO) {
                            $scope.ListadoEstados.push({ Nombre: 'ANULADO', Codigo: 2 });
                            $scope.Modelo.Estado = $scope.ListadoEstados[2];
                            $scope.DeshabilitarActualizar = true;
                            //--Totales
                            TotalesDefinitivo(response.data.Datos);
                            $(".Tab-Recogidas").show();
                        } else {
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.DeshabilitarActualizar = true;
                                //--Totales
                                TotalesDefinitivo(response.data.Datos);
                                $(".Tab-Recogidas").show();
                            }
                        }

                        $scope.MostrarSeccionTap(1);
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        $scope.MaskValores();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    blockUI.stop();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } document.location.href = '#!ConsultarPlanillaPaqueteria'; }, 500);
                });
        }
        function TotalesDefinitivo(response) {
            $scope.Modelo.Planilla.Cantidad = response.Planilla.Cantidad;
            $scope.Modelo.Planilla.Peso = response.Planilla.Peso;
            $scope.Modelo.Planilla.ValorFleteTransportador = response.Planilla.ValorFleteTransportador;
            $scope.Modelo.Planilla.ValorImpuestos = response.Planilla.ValorImpuestos;
            $scope.Modelo.Planilla.ValorPagarTransportador = response.Planilla.ValorPagarTransportador;
            if (response.Planilla.DetalleImpuesto !== undefined && response.Planilla.DetalleImpuesto !== null && response.Planilla.DetalleImpuesto !== []) {
                $scope.ListadoImpuestosFiltrado = response.Planilla.DetalleImpuesto;
            }
        }
        //----------------------------OBTENER-----------------------------//
        //----------------------------GUARDAR-----------------------------//
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo;
            var fecha1 = new Date(Modelo.Planilla.Fecha);
            var fecha2 = new Date(Modelo.Planilla.FechaSalida);
            var fecha3 = new Date();
            fecha1.setHours('00');
            fecha1.setMinutes('00');
            fecha1.setSeconds('00');
            fecha1.setMilliseconds('00');
            fecha2.setHours('00');
            fecha2.setMinutes('00');
            fecha2.setSeconds('00');
            fecha2.setMilliseconds('00');
            fecha3.setHours('00');
            fecha3.setMinutes('00');
            fecha3.setSeconds('00');
            fecha3.setMilliseconds('00');
            if (Modelo.Planilla.Fecha == undefined || Modelo.Planilla.Fecha == '' || Modelo.Planilla.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha ');
                continuar = false;
            }
            else if (fecha1 < fecha3) {
                $scope.MensajesError.push('La fecha ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Modelo.Planilla.Vehiculo == undefined || Modelo.Planilla.Vehiculo == '') {
                $scope.MensajesError.push('Debe ingresar el vehículo');
                continuar = false;
            }
            if (Modelo.Planilla.Conductor == undefined || Modelo.Planilla.Conductor == '') {
                $scope.MensajesError.push('Debe ingresar Conductor');
                continuar = false;
            }
            if (Modelo.Planilla.Ruta == undefined || Modelo.Planilla.Ruta == '') {
                $scope.MensajesError.push('Debe ingresar ruta');
                continuar = false;
            }
            if (Modelo.Planilla.FechaSalida == undefined || Modelo.Planilla.FechaSalida == '' || Modelo.Planilla.FechaSalida == null) {
                $scope.MensajesError.push('Debe ingresar la fecha de salida');
                continuar = false;
            }
            else if (fecha2 < fecha3) {
                $scope.MensajesError.push('La fecha de salida ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Modelo.Planilla.HoraSalida == undefined || Modelo.Planilla.HoraSalida == '' || Modelo.Planilla.HoraSalida == null) {
                $scope.MensajesError.push('Debe ingresar la hora de salida');
                continuar = false;
            }
            else {
                var horas = Modelo.Planilla.HoraSalida.split(':');
                if (horas.length < 2) {
                    $scope.MensajesError.push('Debe ingresar una hora valida');
                    continuar = false;
                } else if (fecha2 == fecha3) {
                    if (parseInt(horas[0]) < fecha3.getHours()) {
                        $scope.MensajesError.push('La hora ingresada debe ser mayor a la actual');
                        continuar = false;
                    } else if (parseInt(horas[0]) == fecha3.getHours()) {
                        if (parseInt(horas[1]) < fecha3.getMinutes()) {
                            $scope.MensajesError.push('La hora ingresada debe ser mayor a la actual');
                            continuar = false;
                        } else {
                            if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                                $scope.MensajesError.push('Debe ingresar una hora valida');
                                continuar = false;
                            }
                        }

                    }

                } else {
                    if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                        $scope.MensajesError.push('Debe ingresar una hora valida');
                        continuar = false;
                    }
                }

            }
            if ($scope.ListaAuxiliares.length > 0) {
                var countAfor = 0;
                var Aforador = 0;
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if ($scope.ListaAuxiliares[i].Aforador == 1 && $scope.ListaAuxiliares[i].Modificarfuncionario === false) {
                        countAfor++;
                        Aforador = i;
                    }
                }
                if (countAfor > 1) {
                    $scope.MensajesError.push('Sólo puede existir un aforador en la sección de auxiliares');
                    continuar = false;
                }
                else if (countAfor == 1) {
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_AFORADOR,
                        Codigo: $scope.ListaAuxiliares[Aforador].Tercero.Codigo,
                        Sync: true
                    });
                    if (Response.ProcesoExitoso) {
                        if (Response.Datos.length <= 0) {
                            $scope.MensajesError.push('El tercero que se seleccina como aforador, no tiene este perfil');
                            continuar = false;
                        }
                    }
                    else {
                        $scope.MensajesError.push('El tercero que se seleccina como aforador, no tiene este perfil');
                        continuar = false;
                    }
                }
                else if (countAfor == 0) {
                    $scope.MensajesError.push('Se debe diligenciar Aforador');
                    continuar = false;
                }
            }
            else {
                $scope.MensajesError.push('Se debe diligenciar Aforador');
                continuar = false;
            }
            if ($scope.TipoTarifaTransportes == undefined || $scope.TipoTarifaTransportes == '' || $scope.TipoTarifaTransportes == null) {
                $scope.MensajesError.push('Se debe asignar un tipo de tarifa');
                continuar = false;
            }
            else {
                if ($scope.TipoTarifaTransportes.Codigo == 0) {
                    $scope.MensajesError.push('Se debe asignar un tipo de tarifa');
                    continuar = false;
                }
            }
            if ($scope.Modelo.OficinaDestino == undefined || $scope.Modelo.OficinaDestino == null || $scope.Modelo.OficinaDestino == "") {
                $scope.MensajesError.push('Debe ingresar la oficina de destino');
                continuar = false;
            }
            //--Remesas
            //var CountReme = 0;
            //if ($scope.ListadoRemesasGuardadas != undefined) {
            //    if ($scope.ListadoRemesasGuardadas.length > 0) {
            //        CountReme = $scope.ListadoRemesasGuardadas.length;
            //    }

            //}
            //if ($scope.ListadoRemesasFiltradas != undefined) {
            //    for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
            //        if ($scope.ListadoRemesasFiltradas[i].Seleccionado == true) {
            //            CountReme += 1;
            //        }
            //    }
            //}
            ////--Remesas
            ////--Recolecciones
            //if ($scope.ListadoRecoleccionesGuardadas != undefined) {
            //    if ($scope.ListadoRecoleccionesGuardadas.length > 0) {
            //        CountReme = $scope.ListadoRecoleccionesGuardadas.length;
            //    }
            //}
            //if ($scope.ListadoRecoleccionesFiltradas != undefined) {
            //    for (var i = 0; i < $scope.ListadoRecoleccionesFiltradas.length; i++) {
            //        if ($scope.ListadoRecoleccionesFiltradas[i].Seleccionado == true) {
            //            CountReme += 1;
            //        }
            //    }
            //}
            ////--Recolecciones
            //if (CountReme <= 0) {
            //    $scope.MensajesError.push('Debe seleccionar al menos un detalle');
            //    continuar = false;
            //}

            return continuar;
        }
        $scope.ConfirmacionGuardar = function () {
            if ($scope.DeshabilitarActualizar) {
                location.href = '#!ConsultarPlanillaPaqueteria/' + $scope.Modelo.Planilla.Numero;
            } else {
                showModal('modalConfirmacionGuardar');
            }
        };
        $scope.PeriodoValido = true
        function FindCierre() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentos: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA },
                Sync: true
            };
            var RCierreContable = CierreContableDocumentosFactory.Consultar(filtros)
            if (RCierreContable.ProcesoExitoso === true) {
                if (RCierreContable.Datos.length > 0) {
                    $scope.ListadoTipoDocumentos = RCierreContable.Datos;
                    $scope.ValidarCierre();
                }
            }
        }

        $scope.ValidarCierre = function () {
            if ($scope.ListadoTipoDocumentos.length > 0) {
                var anoFecha = $scope.Modelo.Planilla.Fecha.getFullYear();
                var mesFecha = $scope.Modelo.Planilla.Fecha.getMonth();
                for (var k = 0; k < $scope.ListadoTipoDocumentos.length; k++) {
                    var item = $scope.ListadoTipoDocumentos[k];
                    var mes = MascaraNumero(item.Mes.CampoAuxiliar2);
                    if (item.Ano === anoFecha && mes === (mesFecha + 1) && item.EstadoCierre.Codigo === 18002) {
                        ShowError('La fecha se encuentra en un mes y año con cierre contable');
                        $scope.Modelo.Remesa.Fecha = '';
                        $scope.PeriodoValido = false
                        break;
                    }
                }
            }
        };
        $scope.ValidarGuardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                $scope.PeriodoValido = true
                FindCierre()
                if ($scope.PeriodoValido) {
                    blockUIConfig.autoBlock = true;
                    blockUIConfig.delay = 0;
                    blockUI.start("Generando Planilla...");
                    $timeout(function () { blockUI.message("Generando Planilla..."); Guardar(); }, 100);
                }
                //Bloqueo Pantalla
            }
        };
        function Guardar() {
            //--Remesas
            var ListadoDetalleRemesas = [];
            if ($scope.ListadoRemesasGuardadas !== undefined && $scope.ListadoRemesasGuardadas !== null && $scope.ListadoRemesasGuardadas !== '') {
                for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                    var guia = $scope.ListadoRemesasGuardadas[i];
                    ListadoDetalleRemesas.push({ Remesa: { Numero: guia.Remesa.Numero, NumeroDocumento: guia.Remesa.NumeroDocumento } });
                }
            }
            //--Remesas
            //--Recolecciones
            var ListadoDetalleRecolecciones = [];
            if ($scope.ListadoRecoleccionesGuardadas !== undefined && $scope.ListadoRecoleccionesGuardadas !== null && $scope.ListadoRecoleccionesGuardadas !== '') {
                for (var i = 0; i < $scope.ListadoRecoleccionesGuardadas.length; i++) {
                    var recoleccion = $scope.ListadoRecoleccionesGuardadas[i];
                    ListadoDetalleRecolecciones.push({ Numero: recoleccion.Numero });
                }
            }
            //--Recolecciones
            //--Genera Fecha Salida con hora
            $scope.Modelo.Planilla.FechaHoraSalida = new Date($scope.Modelo.Planilla.FechaSalida);

            var Horasminutos = [];
            Horasminutos = $scope.Modelo.Planilla.HoraSalida.split(':');
            if (Horasminutos.length > 0) {
                $scope.Modelo.Planilla.FechaHoraSalida.setHours(Horasminutos[0]);
                $scope.Modelo.Planilla.FechaHoraSalida.setMinutes(Horasminutos[1]);
            }

            //--Detalle Impuestos
            var ListadoDetalleImpuestos = [];
            if ($scope.ListadoImpuestosFiltrado.length > 0) {
                for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                    var ImpArr = $scope.ListadoImpuestosFiltrado[i];
                    var impuesto = {
                        CodigoImpuesto: ImpArr.CodigoImpuesto,
                        ValorTarifa: ImpArr.ValorTarifa,
                        ValorBase: ImpArr.ValorBase,
                        ValorImpuesto: ImpArr.ValorImpuesto
                    };
                    ListadoDetalleImpuestos.push(impuesto);
                }
            }
            //--Detalle Impuestos
            //--Auxiliares
            var DetallesAuxiliares = [];
            if ($scope.ListaAuxiliares.length > 0) {
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (!$scope.ListaAuxiliares[i].Modificarfuncionario) {
                        DetallesAuxiliares.push({
                            Funcionario: $scope.ListaAuxiliares[i].Tercero
                            , NumeroHorasTrabajadas: $scope.ListaAuxiliares[i].Horas
                            , Valor: $scope.ListaAuxiliares[i].Valor
                            , Observaciones: $scope.ListaAuxiliares[i].Observaciones
                            , Aforador: $scope.ListaAuxiliares[i].Aforador == true ? 1 : 0
                        });
                    }
                }
            }
            //--Auxiliares
            //--Genera Anticipo
            if (parseInt($scope.Modelo.Planilla.ValorAnticipo) > CERO) {
                $scope.Modelo.CuentaPorPagar = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                    CodigoAlterno: '',
                    Fecha: $scope.Modelo.Planilla.Fecha,
                    Tercero: { Codigo: $scope.Modelo.Planilla.Vehiculo.Conductor.Codigo },
                    DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA },
                    CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA,
                    Numeracion: '',
                    CuentaPuc: { Codigo: 0 },
                    ValorTotal: $scope.Modelo.Planilla.ValorAnticipo,
                    Abono: 0,
                    Saldo: $scope.Modelo.Planilla.ValorAnticipo,
                    FechaCancelacionPago: $scope.Modelo.Planilla.Fecha,
                    Aprobado: 1,
                    UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    FechaAprobo: $scope.Modelo.Planilla.Fecha,
                    Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                    Vehiculo: { Codigo: $scope.Modelo.Planilla.Vehiculo.Codigo },
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                };
            }
            //--Genera Anticipo
            //--Genera Obj Planilla
            var Planilla = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                Planilla: {
                    Numero: $scope.Modelo.Planilla.Numero,
                    Fecha: $scope.Modelo.Planilla.Fecha,
                    FechaHoraSalida: $scope.Modelo.Planilla.FechaHoraSalida,
                    FechaEntrega: $scope.Modelo.Planilla.FechaLlegada,
                    Ruta: { Codigo: $scope.Modelo.Planilla.Ruta.Codigo },
                    Vehiculo: {
                        Codigo: $scope.Modelo.Planilla.Vehiculo.Codigo,
                        TipoDueno: { Codigo: $scope.Modelo.Planilla.Vehiculo.TipoDueno ? $scope.Modelo.Planilla.Vehiculo.TipoDueno.Codigo : 0 },
                        TipoVehiculo: { Codigo: $scope.Modelo.Planilla.Vehiculo.TipoVehiculo ? $scope.Modelo.Planilla.Vehiculo.TipoVehiculo.Codigo : 0 },
                        Placa: $scope.Modelo.Planilla.Vehiculo.Placa
                    },
                    Tenedor: { Codigo: $scope.Modelo.Planilla.Tenedor.Codigo },
                    Conductor: { Codigo: $scope.Modelo.Planilla.Conductor.Codigo },
                    Semirremolque: {
                        Codigo: $scope.Modelo.Planilla.Semirremolque != '' && $scope.Modelo.Planilla.Semirremolque != null && $scope.Modelo.Planilla.Semirremolque != undefined ? $scope.Modelo.Planilla.Semirremolque.Codigo : 0,
                        Placa: $scope.Modelo.Planilla.Semirremolque ? $scope.Modelo.Planilla.Semirremolque.Placa : ''
                    },
                    Cantidad: $scope.Modelo.Planilla.Cantidad,
                    Peso: $scope.Modelo.Planilla.Peso,
                    ValorFleteTransportador: $scope.Modelo.Planilla.ValorFleteTransportador,
                    ValorAnticipo: $scope.Modelo.Planilla.ValorAnticipo,
                    ValorImpuestos: $scope.Modelo.Planilla.ValorImpuestos,
                    ValorPagarTransportador: $scope.Modelo.Planilla.ValorPagarTransportador,
                    ValorFleteCliente: $scope.Modelo.Planilla.ValorFleteCliente,
                    //ValorSeguroMercancia: $scope.Modelo.Planilla.ValorSeguroMercancia,
                    //ValorOtrosCobros: $scope.Modelo.Planilla.ValorOtrosCobros,
                    //ValorTotalCredito: $scope.Modelo.Planilla.ValorTotalCredito,
                    //ValorTotalContado: $scope.Modelo.Planilla.ValorTotalContado,
                    //ValorTotalAlcobro: $scope.Modelo.Planilla.ValorTotalAlcobro,
                    TarifaTransportes: { Codigo: $scope.Modelo.Planilla.TarifaTransportes.Codigo },
                    TipoTarifaTransportes: { Codigo: $scope.TipoTarifaTransportes.Codigo },
                    RemesaPadre: $scope.Modelo.Planilla.RemesaPadre,
                    RemesaMasivo: $scope.Modelo.Planilla.RemesaMasivo,
                    //ValorContraEntrega: $scope.Modelo.Planilla.ValorContraEntrega,
                    //ValorBruto: $scope.Modelo.Planilla.ValorBruto,
                    //Porcentaje: $scope.Modelo.Planilla.Porcentaje,
                    //ValorDescuentos: $scope.Modelo.Planilla.ValorDescuentos,
                    //FleteSugerido: $scope.Modelo.Planilla.FleteSugerido,
                    //ValorFondoAyuda: $scope.Modelo.Planilla.ValorFondoAyuda,
                    //ValorNetoPagar: $scope.Modelo.Planilla.ValorNetoPagar,
                    //ValorUtilidad: $scope.Modelo.Planilla.ValorUtilidad,
                    //ValorEstampilla: $scope.Modelo.Planilla.ValorEstampilla,
                    //GastosAgencia: $scope.Modelo.Planilla.GastosAgencia,
                    //ValorReexpedicion: $scope.Modelo.Planilla.ValorReexpedicion,
                    //ValorPlanillaAdicional: $scope.Modelo.Planilla.ValorPlanillaAdicional,
                    //CalcularContraentregas: $scope.Modelo.Planilla.CalcularContraentregas == true ? 1 : 0,
                    DetallesAuxiliares: DetallesAuxiliares,
                    Detalles: ListadoDetalleRemesas,
                    DetalleImpuesto: ListadoDetalleImpuestos
                },
                ListadoPlanillaRecolecciones: ListadoDetalleRecolecciones,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                Observaciones: $scope.Modelo.Observaciones,
                Estado: $scope.Modelo.Estado.Codigo,
                CuentaPorPagar: $scope.Modelo.CuentaPorPagar,
                NumeroPrecintoPaqueteria: $scope.Modelo.NumeroPrecintoPaqueteria,
                OficinaDestino: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                ParametrosCalculos: {
                    SugerirAnticipoPlanilla: $scope.Sesion.UsuarioAutenticado.SugerirAnticipoPlanilla,
                    DeshabilitarAnticipoPlanillaPaqueteria: $scope.Sesion.UsuarioAutenticado.DeshabilitarAnticipoPlanillaPaqueteria,
                    ListadoRemesasFiltradas: $scope.ListadoRemesasFiltradas.map(a => a.Remesa),
                    ListadoRemesasGuardadas: $scope.ListadoRemesasGuardadas.map(a => a.Remesa),
                    ListadoImpuestos: $scope.ListadoImpuestos,
                    FleteTarifaTransporte: $scope.TipoTarifaTransportes ? $scope.TipoTarifaTransportes.ValorFlete : 0,
                    ListadoTotalesRemesas: []
                }
            };
            if ($scope.Sesion.Empresa.GeneraManifiesto === 1 && $scope.Modelo.Estado.Codigo == 1 && $scope.Modelo.Planilla.Ruta.TipoRuta.Codigo == 4401
                && $scope.Sesion.UsuarioAutenticado.ManejoGenerarManifiestoPaqueteria) {
                Planilla.Manifiesto = {
                    Numero: 0,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    //NumeroDocumentoPlanilla: response.data.Datos,
                    Tipo_Documento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA },
                    Usuario_Crea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                    Tipo_Manifiesto: { Codigo: CODIGO_CATALOGO_TIPO_MANIFIESTO_PLANILLA_PAQUETERIA },
                    Valor_Anticipo: $scope.Modelo.Planilla.ValorAnticipo
                };
            }


            //--Genera Obj Planilla
            PlanillaPaqueteriaFactory.Guardar(Planilla).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > CERO) {
                            if ($scope.Modelo.Planilla.Numero == CERO) {
                                ShowSuccess('Se guardó la planilla con el número ' + response.data.Datos);
                            }
                            else {
                                ShowSuccess('Se modificó la planilla con el número ' + response.data.Datos);
                            }
                            //if ($scope.Sesion.Empresa.GeneraManifiesto === 1 && $scope.Modelo.Estado.Codigo == 1 && $scope.Modelo.Planilla.Ruta.TipoRuta.Codigo == 4401 && $scope.Sesion.UsuarioAutenticado.ManejoGenerarManifiestoPaqueteria) {
                            //    var Manifiesto = {
                            //        Numero: 0,
                            //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            //        NumeroDocumentoPlanilla: response.data.Datos,
                            //        Tipo_Documento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA },
                            //        Usuario_Crea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            //        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                            //        Tipo_Manifiesto: { Codigo: CODIGO_CATALOGO_TIPO_MANIFIESTO_PLANILLA_PAQUETERIA },
                            //        Valor_Anticipo: $scope.Modelo.Planilla.ValorAnticipo,
                            //        Sync: true
                            //    };
                            //    var responseMani = ManifiestoFactory.Guardar(Manifiesto);
                            //    if (responseMani.ProcesoExitoso == true) {
                            //        if (responseMani.Datos == CERO) {
                            //            ShowError(response.statusText);
                            //        }
                            //    }
                            //    else {
                            //        ShowError(response.statusText);
                            //    }
                            //}
                            $timeout(function () {
                                blockUI.stop();
                                blockUIConfig.autoBlock = false;
                                if (blockUI.state().blocking == true) { blockUI.reset(); }
                                location.href = '#!ConsultarPlanillaPaqueteria/' + response.data.Datos;
                            }, 500);
                        }
                        else if (response.data.Datos == -2) {
                            ShowError('Algunas de las guías que intenta guardar ya se enceuntran asociadas a otra planilla');
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        }
                        else if (response.data.Datos == -3) {
                            ShowWarning('', 'El vehículo ingresado tiene planillas pendientes por cumplir');
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        }
                        else {
                            ShowError(response.statusText);
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                });
        }
        //----------------------------GUARDAR-----------------------------//
        //--------------------------PAGINACION-----------------------------//
        function ResetPaginacion(obj) {
            obj.totalRegistros = obj.array.length;
            obj.totalPaginas = Math.ceil(obj.totalRegistros / 10);
            $scope.PrimerPagina(obj);
        }
        $scope.PrimerPagina = function (obj) {
            if (obj.totalRegistros > 10) {
                obj.PagArray = [];
                obj.paginaActual = 1;
                for (var i = 0; i < 10; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            } else {
                obj.PagArray = obj.array;
            }
        };
        $scope.Anterior = function (obj) {
            if (obj.paginaActual > 1) {
                obj.paginaActual -= 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                }
            }
            else {
                $scope.PrimerPagina(obj);
            }
        };
        $scope.Siguiente = function (obj) {
            if (obj.paginaActual < obj.totalPaginas) {
                obj.paginaActual += 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                } else {
                    $scope.UltimaPagina(obj);
                }
            } else if (obj.paginaActual == obj.totalPaginas) {
                $scope.UltimaPagina(obj);
            }
        };
        $scope.UltimaPagina = function (obj) {
            if (obj.totalRegistros > 10 && obj.totalPaginas > 1) {
                obj.paginaActual = obj.totalPaginas;
                var a = obj.paginaActual * 10;
                obj.PagArray = [];
                for (var i = a - 10; i < obj.array.length; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            }
        }
        //--------------------------PAGINACION-----------------------------//
        //--Volver mastes
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Planilla.NumeroDocumento == undefined) {
                document.location.href = '#!ConsultarPlanillaPaqueteria';
            }
            else {
                document.location.href = '#!ConsultarPlanillaPaqueteria/' + $scope.Modelo.Planilla.NumeroDocumento;
            }
        };
        //----Mascaras
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item);
        };
        $scope.MaskValoresGrid = function (item) {
            if (item != undefined && item != '') {
                return MascaraValores(item);
            }
            return '';
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope);
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        //----Mascaras
        //----------------------------Funciones Generales---------------------------------//
    }]);