﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Contabilidad.Documentos
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo

Namespace Contabilidad

    ''' <summary>
    ''' Clase <see cref="RepositorioEncabezadoComprobantesContables"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEncabezadoComprobantesContables
        Inherits RepositorioBase(Of EncabezadoComprobantesContables)

        Public Overrides Function Consultar(filtro As EncabezadoComprobantesContables) As IEnumerable(Of EncabezadoComprobantesContables)
            Dim lista As New List(Of EncabezadoComprobantesContables)
            Dim item As EncabezadoComprobantesContables
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.FechaInicial <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                End If

                If filtro.FechaFinal <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If

                If filtro.DocumentoOrigen.Codigo <> 2600 Then
                    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", filtro.DocumentoOrigen.Codigo)
                End If

                If Not IsNothing(filtro.NumeroDocumento) And filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If filtro.Anulado <> -1 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Anulado)
                End If

                If filtro.InterfazContable > -1 Then
                    conexion.AgregarParametroSQL("@par_Estado_Interfaz", filtro.InterfazContable)
                End If

                'conexion.AgregarParametroSQL("@par_Estado_Interfaz", filtro.EstadoInterfazContable.Codigo)

                conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_encabezado_comprobante_contables]")

                While resultado.Read
                    item = New EncabezadoComprobantesContables(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Modificar(entidad As EncabezadoComprobantesContables) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As EncabezadoComprobantesContables) As EncabezadoComprobantesContables
            Dim item As New EncabezadoComprobantesContables

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_encabezado_comprobante_contables]")

                While resultado.Read
                    item = New EncabezadoComprobantesContables(resultado)
                End While

                resultado.Close()

                If item.Numero > 0 Then
                    item.DetalleCuentas = ConsultarListaDetalleComprobanteContable(item.CodigoEmpresa, item.Numero, conexion, resultado)
                End If

            End Using

            Return item
        End Function

        Public Function Anular(entidad As EncabezadoComprobantesContables) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo_Anula", entidad.OficinaAnula.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_anular_encabezado_coprobante_contables]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Overrides Function Insertar(entidad As EncabezadoComprobantesContables) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                If entidad.Masivo > 0 Then
                    Dim resultado As IDataReader
                    Dim Numero As Long = 0
                    For Each lista In entidad.ListaDocumentos
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_Numero", lista.NumeroDocumento)
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumentoOrigen.Codigo)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_generar_movimiento_contable")

                        While resultado.Read
                            Numero = resultado.Item("Numero")
                        End While
                        resultado.Close()
                        If Numero > 0 Then
                            Numero = 1
                        Else
                            Return Cero
                        End If

                    Next
                    Return Numero
                Else
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.NumeroDocumento)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumentoOrigen.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_generar_movimiento_contable]")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero")
                    End While
                    resultado.Close()
                    If entidad.Numero > 0 Then
                        Return entidad.Numero
                    Else
                        Return Cero
                    End If

                End If

            End Using
        End Function

        Public Function ConsultarListaDetalleComprobanteContable(CodiEmpresa As Short, Nume As Long, ByRef conexion As DataBaseFactory, resultado As IDataReader) As IEnumerable(Of DetalleComprobantesContables)
            Dim item As DetalleComprobantesContables
            Dim listaDetalle As New List(Of DetalleComprobantesContables)

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENCC_Numero", Nume)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_detalle_comprobante_contables]")

            While resultado.Read
                item = New DetalleComprobantesContables(resultado)
                listaDetalle.Add(item)
            End While
            resultado.Close()

            Return listaDetalle

        End Function

        Public Function GenerarMovimientoContable(Codigo_Empresa As Short, Numero As Integer, TIDO_Codigo As Integer, ByRef conexion As DataBaseFactory, resultado As IDataReader) As Boolean
            Dim Genero As Boolean = False

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", Codigo_Empresa)
            conexion.AgregarParametroSQL("@par_Numero", Numero)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", TIDO_Codigo)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_generar_movimiento_contable]")

            While resultado.Read
                Genero = IIf((resultado.Item("Numero")) > 0, True, False)
            End While
            resultado.Close()

            Return Genero
        End Function

        Public Function GenerarMovimientoContableAnulacion(Codigo_Empresa As Short, Numero As Integer, TIDO_Codigo As Integer, ByRef conexion As DataBaseFactory, resultado As IDataReader) As Boolean
            Dim Genero As Boolean = False

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", Codigo_Empresa)
            conexion.AgregarParametroSQL("@par_Numero", Numero)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", TIDO_Codigo)

            resultado = conexion.ExecuteReaderStoreProcedure("gsp_generar_movimiento_contable_anulacion")

            While resultado.Read
                Genero = IIf((resultado.Item("Numero")) > 0, True, False)
            End While
            resultado.Close()

            Return Genero
        End Function


        Public Function GenerarMovimientoContablePlanilla(Codigo_Empresa As Short, Numero As Integer, TIDO_Codigo As Integer, Planilla As PlanillaDespachos, ByRef conexion As DataBaseFactory, resultado As IDataReader) As Boolean
            Dim Genero As Boolean = False

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", Codigo_Empresa)
            conexion.AgregarParametroSQL("@par_Numero", Numero)
            conexion.AgregarParametroSQL("@par_Numero_Documento", Planilla.NumeroDocumento)
            If Planilla.Fecha > Date.MinValue Then
                conexion.AgregarParametroSQL("@par_Fecha", Planilla.Fecha, SqlDbType.DateTime)
            End If
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", TIDO_Codigo)
            If Not IsNothing(Planilla.Vehiculo) Then
                conexion.AgregarParametroSQL("@par_CATA_TIDU_Codigo", Planilla.Vehiculo.TipoDueno.Codigo)
            End If
            If Not IsNothing(Planilla.Conductor) Then
                conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", Planilla.Conductor.Codigo)
            End If
            If Not IsNothing(Planilla.Conductor) Then
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", Planilla.Oficina.Codigo)
            End If
            conexion.AgregarParametroSQL("@par_Anticipo", Planilla.ValorAnticipo, SqlDbType.Money)
            If Planilla.Anulado = 1 Then
                conexion.AgregarParametroSQL("@par_Anular", Planilla.Anulado)
            End If

            resultado = conexion.ExecuteReaderStoreProcedure("gsp_generar_movimiento_contable_planilla_masivo")

            While resultado.Read
                Genero = IIf((resultado.Item("Numero")) > 0, True, False)
            End While
            resultado.Close()

            Return Genero
        End Function

    End Class
End Namespace