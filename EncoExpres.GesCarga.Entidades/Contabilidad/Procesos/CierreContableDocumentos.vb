﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Contabilidad.Procesos

    ''' <summary>
    ''' Clase <see cref=" CierreContableDocumentos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class CierreContableDocumentos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CierreContableDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CierreContableDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Ano = Read(lector, "Ano")
            Mes = New ValorCatalogos With {.Codigo = Read(lector, "CATA_MESE_Codigo"), .Nombre = Read(lector, "Nombre_Mes"), .CampoAuxiliar2 = Read(lector, "Numero_Mes")}
            TipoDocumentos = New TipoDocumentos With {.Codigo = Read(lector, "TIDO_Codigo"), .Nombre = Read(lector, "Nombre_Tipo_Documento")}
            EstadoCierre = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESPC_Codigo"), .Nombre = Read(lector, "Nombre_Estado_Cierre"), .CampoAuxiliar2 = Read(lector, "Numero_Estado_Cierre")}

        End Sub

        <JsonProperty>
        Public Property Ano As Integer
        <JsonProperty>
        Public Property Mes As ValorCatalogos
        <JsonProperty>
        Public Property TipoDocumentos As TipoDocumentos
        <JsonProperty>
        Public Property EstadoCierre As ValorCatalogos
        <JsonProperty>
        Public Property ListadoDocumentosCierre As IEnumerable(Of CierreContableDocumentos)

    End Class
End Namespace
