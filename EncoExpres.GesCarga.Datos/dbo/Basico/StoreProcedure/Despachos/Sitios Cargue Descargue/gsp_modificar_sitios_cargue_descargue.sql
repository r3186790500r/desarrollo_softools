﻿
PRINT 'gsp_modificar_sitios_cargue_descargue'
GO
DROP PROCEDURE gsp_modificar_sitios_cargue_descargue
GO
CREATE PROCEDURE gsp_modificar_sitios_cargue_descargue 
(  
@par_EMPR_Codigo SMALLINT ,  
@par_Codigo NUMERIC,  
@par_Nombre VARCHAR (50),  
@par_Codigo_Tipo_Sitio NUMERIC,
@par_Codigo_Pais NUMERIC,
@par_Codigo_Ciudad NUMERIC,
@par_Direccion VARCHAR (150),  
@par_Codigo_Postal VARCHAR (50) = NULL,  
@par_Telefono VARCHAR (100),  
@par_Contacto VARCHAR (50) = NULL,  
@par_Estado SMALLINT,   
@par_USUA_Codigo_Modifica SMALLINT  
)  
AS   
BEGIN  
UPDATE Sitios_Cargue_Descargue 
SET   
Nombre = @par_Nombre,  
CATA_TSCD_Codigo = @par_Codigo_Tipo_Sitio,
PAIS_Codigo = @par_Codigo_Pais,
CIUD_Codigo = @par_Codigo_Ciudad,
Direccion = @par_Direccion,  
Codigo_Postal = @par_Codigo_Postal,  
Telefono = @par_Telefono,  
Contacto = @par_Contacto, 
Estado =  @par_Estado,  
USUA_Codigo_Modifica =  @par_USUA_Codigo_Modifica,  
Fecha_Modifica = GETDATE()  
  
WHERE   
EMPR_Codigo =  @par_EMPR_Codigo  
AND Codigo =  @par_Codigo  
  
SELECT @par_Codigo As Codigo  
END  
GO