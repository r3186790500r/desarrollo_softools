﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Utilitarios
Imports EncoExpres.GesCarga.Negocio.Utilitarios

<Authorize>
Public Class BandejaSalidaCorreosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As BandejaSalidaCorreos) As Respuesta(Of IEnumerable(Of BandejaSalidaCorreos))
        Return New LogicaBandejaSalidaCorreos(CapaPersistenciaBandejaSalidaCorreos).Consultar(filtro)
    End Function


End Class