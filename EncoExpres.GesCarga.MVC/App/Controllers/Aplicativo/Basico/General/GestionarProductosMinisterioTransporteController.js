﻿EncoExpresApp.controller('GestionarProductosMinisterioTransporteCtrl', ['$scope','$timeout','$linq','blockUI','$routeParams','ProductosMinisterioTransporteFactory',
    function ($scope, $timeout,$linq,blockUI,$routeParams,ProductosMinisterioTransporteFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Productos Ministerio Transporte' }, { Nombre: 'Gestionar' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PRODUCTOS_MINISTERIO_TRANSPORTE);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.MensajesError = [];


        $scope.ListadoEstados = [
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        $scope.Nombre = '';
        $scope.CodigoAlterno = '';
        $scope.Codigo = '';
        $scope.Descripcion = '';

        //Máscaras:
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };


        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarProductosMinisterioTransporte/';
        };

        //Obtener Parametros:
        if ($routeParams.Codigo > 0) {
            $scope.Codigo = $routeParams.Codigo;
            Obtener();
        }


        //Guardar:
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Codigo,
                    Nombre: $scope.Nombre,
                    CodigoAlterno: $scope.CodigoAlterno,
                    Descripcion: $scope.Descripcion,
                    Estado: $scope.Estado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }

                ProductosMinisterioTransporteFactory.Guardar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Codigo == 0) {
                                    ShowSuccess('Se guardó el Producto "' + $scope.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se Modificó el Producto "' + $scope.Nombre + '"');
                                }
                                location.href = '#!ConsultarProductosMinisterioTransporte/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.CodigoAlterno == undefined || $scope.CodigoAlterno == null || $scope.CodigoAlterno == '') {
                $scope.MensajesError.push('Debe ingresar un Código alterno');
            }
            if ($scope.Nombre == undefined || $scope.Nombre == '' || $scope.Nombre == null) {
                $scope.MensajesError.push('Debe ingresar un Nombre');
                continuar = false;
            }
            if ($scope.Descripcion == undefined || $scope.Descripcion == '' || $scope.Descripcion == null) {
                $scope.MensajesError.push('Debe ingresar una Descripción');
                continuar = false;
            }
            return continuar;
        }

        function Obtener() {
            blockUI.start('Cargando Producto No. ' + $scope.CodigoAlterno);

            $timeout(function () {
                blockUI.message('Cargando Producto No.' + $scope.CodigoAlterno);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            blockUI.delay = 1000;
            ProductosMinisterioTransporteFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Codigo = response.data.Datos.Codigo;
                        $scope.CodigoAlterno = response.data.Datos.CodigoAlterno;
                        $scope.Nombre = response.data.Datos.Nombre;
                        $scope.Descripcion = response.data.Datos.Descripcion;
                        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                    }
                    else {
                        ShowError('No se logro consultarel Producto código ' + $scope.CodigoAlterno + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarProductosMinisterioTransporte';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                        document.location.href = '#!ConsultarProductosMinisterioTransporte';
                });

            blockUI.stop();
        };
    }
]);