﻿Imports AvansatFaro.wsAvansatFaro
Imports System.Configuration.ConfigurationManager
Imports System.Threading
Imports System.Data.SqlClient

Public Class svFaro

#Region "Variables"

    Private thrProcDesp As Thread
    Private thrProcNove As Thread
    Private wsFaro As faro
    Private intEmpr As Integer
    Private strUsua As String
    Private strNitEmp As String
    Private strClav As String
    Private intDiasRepoDesp As Integer
    Private intDiasConsNove As Integer
    Private strNombreConexion As String
    Private strCadenaDeConexionSQL As String
    Private sqlConexion As SqlConnection
    Private sqlComando As SqlCommand

    'Variables reporte despacho
    Private intNitTran As Integer
    Private intMani As Integer
    Private dteFechDesp As DateTime
    Private strCiudOrig As String
    Private strCiudDest As String
    Private strPlacVehi As String
    Private intModeVehi As Integer
    Private strCodiMarcVehi As String
    Private intCodiLineVehi As Integer
    Private strCodiColoVehi As String
    Private intIdenCond As Integer
    Private strNombCond As String
    Private strCiudCond As String
    Private strTeleCond As String
    Private strCeluCond As String
    Private strObseMani As String
    Private intRuta As Integer
    Private strNombRuta As String
    Private intTipoRuta As Integer
    Private strCodiConfVehi As String
    Private strCodiCarrVehi As String
    Private strSeriMotoVehi As String
    Private strSoat As String
    Private dteFechFinSoat As Date
    Private strNombAsegSoat As String
    Private strNumeTarjPropVehi As String
    Private strPlacSemi As String
    Private strCodiCateLiceCond As String
    Private strDireCond As String
    Private intIdenPropVehi As Integer
    Private strNombPropVehi As String
    Private strCiudProp As String
    Private strDireProp As String
    Private intCodiOperCeluCond As Integer

#End Region

#Region "Constantes"

    Const NO_ANULADO As Integer = 0
    Const VALOR_VACIO As String = ""
    Const REPORTADO_CON_EXITO As Integer = 2
    Const REPORTADO_SIN_EXITO As Integer = -1
    Const EN_TRANSITO As Integer = 0
    Const PENDIENTE_POR_REPORTAR As Integer = 1

#End Region

#Region "Constructor"

    Sub New()
        InitializeComponent()
        'Me.Faro = New faro
        'Me.ProcesoReporteDespachos = New Thread(AddressOf Iniciar_Proceso_Despacho)
        'Me.ProcesoReporteDespachos.Start()
        'Me.ProcesoConsultaNovedades = New Thread(AddressOf Iniciar_Proceso_Novedades)
        'Me.ProcesoConsultaNovedades.Start()
    End Sub

#End Region

#Region "Propiedades"

    Public Property Faro() As faro
        Get
            Return wsFaro
        End Get
        Set(ByVal value As faro)
            wsFaro = value
        End Set
    End Property

    Public Property ProcesoReporteDespachos() As Thread
        Get
            Return thrProcDesp
        End Get
        Set(ByVal value As Thread)
            thrProcDesp = value
        End Set
    End Property

    Public Property ProcesoConsultaNovedades() As Thread
        Get
            Return thrProcNove
        End Get
        Set(ByVal value As Thread)
            thrProcNove = value
        End Set
    End Property

    Public Property Empresa() As Integer
        Get
            Return intEmpr
        End Get
        Set(ByVal value As Integer)
            intEmpr = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return strUsua
        End Get
        Set(ByVal value As String)
            strUsua = value
        End Set
    End Property

    Public Property NitEmpresa() As String
        Get
            Return strNitEmp
        End Get
        Set(ByVal value As String)
            strNitEmp = value
        End Set
    End Property

    Public Property Clave() As String
        Get
            Return strClav
        End Get
        Set(ByVal value As String)
            strClav = value
        End Set
    End Property

    Public Property DiasReportarDespacho() As Integer
        Get
            Return intDiasRepoDesp
        End Get
        Set(ByVal value As Integer)
            intDiasRepoDesp = value
        End Set
    End Property

    Public Property DiasConsultarNovedades() As Integer
        Get
            Return intDiasConsNove
        End Get
        Set(ByVal value As Integer)
            intDiasConsNove = value
        End Set
    End Property

    Public Property NombreConexion() As String
        Get
            Return Me.strNombreConexion
        End Get
        Set(ByVal value As String)
            Me.strNombreConexion = value
        End Set
    End Property

    Public Property CadenaDeConexionSQL() As String
        Get
            Return Me.strCadenaDeConexionSQL
        End Get
        Set(ByVal value As String)
            Me.strCadenaDeConexionSQL = value
        End Set
    End Property

    Public Property ConexionSQL() As SqlConnection
        Get
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ComandoSQL() As SqlCommand
        Get
            Return Me.sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComando = value
        End Set
    End Property

    Public Property NitTransportadora() As Integer
        Get
            Return intNitTran
        End Get
        Set(ByVal value As Integer)
            intNitTran = value
        End Set
    End Property

    Public Property Manifiesto() As Integer
        Get
            Return intMani
        End Get
        Set(ByVal value As Integer)
            intMani = value
        End Set
    End Property

    Public Property FechaDespacho() As DateTime
        Get
            Return dteFechDesp
        End Get
        Set(ByVal value As DateTime)
            dteFechDesp = value
        End Set
    End Property

    Public Property CiudadOrigen() As String
        Get
            Return strCiudOrig
        End Get
        Set(ByVal value As String)
            strCiudOrig = value
        End Set
    End Property

    Public Property CiudadDestino() As String
        Get
            Return strCiudDest
        End Get
        Set(ByVal value As String)
            strCiudDest = value
        End Set
    End Property

    Public Property PlacaVehiculo() As String
        Get
            Return strPlacVehi
        End Get
        Set(ByVal value As String)
            strPlacVehi = value
        End Set
    End Property

    Public Property ModeloVehiculo() As Integer
        Get
            Return intModeVehi
        End Get
        Set(ByVal value As Integer)
            intModeVehi = value
        End Set
    End Property

    Public Property MarcaVehiculo() As String
        Get
            Return strCodiMarcVehi
        End Get
        Set(ByVal value As String)
            strCodiMarcVehi = value
        End Set
    End Property

    Public Property LineaVehiculo() As Integer
        Get
            Return intCodiLineVehi
        End Get
        Set(ByVal value As Integer)
            intCodiLineVehi = value
        End Set
    End Property

    Public Property ColorVehiculo() As String
        Get
            Return strCodiColoVehi
        End Get
        Set(ByVal value As String)
            strCodiColoVehi = value
        End Set
    End Property

    Public Property IdentificacionConductor() As Integer
        Get
            Return intIdenCond
        End Get
        Set(ByVal value As Integer)
            intIdenCond = value
        End Set
    End Property

    Public Property NombreConductor() As String
        Get
            Return strNombCond
        End Get
        Set(ByVal value As String)
            strNombCond = value
        End Set
    End Property

    Public Property CiudadConductor() As String
        Get
            Return strCiudCond
        End Get
        Set(ByVal value As String)
            strCiudCond = value
        End Set
    End Property

    Public Property TelefonoConductor() As String
        Get
            Return strTeleCond
        End Get
        Set(ByVal value As String)
            strTeleCond = value
        End Set
    End Property

    Public Property CelularConductor() As String
        Get
            Return strCeluCond
        End Get
        Set(ByVal value As String)
            strCeluCond = value
        End Set
    End Property

    Public Property ObservacionesManifiesto() As String
        Get
            Return strObseMani
        End Get
        Set(ByVal value As String)
            strObseMani = value
        End Set
    End Property

    Public Property CodigoRuta() As Integer
        Get
            Return intRuta
        End Get
        Set(ByVal value As Integer)
            intRuta = value
        End Set
    End Property

    Public Property NombreRuta() As String
        Get
            Return strNombRuta
        End Get
        Set(ByVal value As String)
            strNombRuta = value
        End Set
    End Property

    Public Property TipoRuta() As Integer
        Get
            Return intTipoRuta
        End Get
        Set(ByVal value As Integer)
            intTipoRuta = value
        End Set
    End Property

    Public Property ConfiguracionVehiculo() As String
        Get
            Return strCodiConfVehi
        End Get
        Set(ByVal value As String)
            strCodiConfVehi = value
        End Set
    End Property

    Public Property CodigoCarroceria() As String
        Get
            Return strCodiCarrVehi
        End Get
        Set(ByVal value As String)
            strCodiCarrVehi = value
        End Set
    End Property

    Public Property SerieMotor() As String
        Get
            Return strSeriMotoVehi
        End Get
        Set(ByVal value As String)
            strSeriMotoVehi = value
        End Set
    End Property

    Public Property Soat() As String
        Get
            Return strSoat
        End Get
        Set(ByVal value As String)
            strSoat = value
        End Set
    End Property

    Public Property FechaVenceSoat() As Date
        Get
            Return dteFechFinSoat
        End Get
        Set(ByVal value As Date)
            dteFechFinSoat = value
        End Set
    End Property

    Public Property NombreAseguradoraSoat() As String
        Get
            Return strNombAsegSoat
        End Get
        Set(ByVal value As String)
            strNombAsegSoat = value
        End Set
    End Property

    Public Property TarjetaPropiedad() As String
        Get
            Return strNumeTarjPropVehi
        End Get
        Set(ByVal value As String)
            strNumeTarjPropVehi = value
        End Set
    End Property

    Public Property PlacaSemiremolque() As String
        Get
            Return strPlacSemi
        End Get
        Set(ByVal value As String)
            strPlacSemi = value
        End Set
    End Property

    Public Property CategoriaLicencia() As String
        Get
            Return strCodiCateLiceCond
        End Get
        Set(ByVal value As String)
            strCodiCateLiceCond = value
        End Set
    End Property

    Public Property DireccionConductor() As String
        Get
            Return strDireCond
        End Get
        Set(ByVal value As String)
            strDireCond = value
        End Set
    End Property

    Public Property IdentificacionPropietario() As Integer
        Get
            Return intIdenPropVehi
        End Get
        Set(ByVal value As Integer)
            intIdenPropVehi = value
        End Set
    End Property

    Public Property NombrePropietario() As String
        Get
            Return strNombPropVehi
        End Get
        Set(ByVal value As String)
            strNombPropVehi = value
        End Set
    End Property

    Public Property CiudadPropietario() As String
        Get
            Return strCiudProp
        End Get
        Set(ByVal value As String)
            strCiudProp = value
        End Set
    End Property

    Public Property DireccionPropietario() As String
        Get
            Return strDireProp
        End Get
        Set(ByVal value As String)
            strDireProp = value
        End Set
    End Property

    Public Property OperadorCelular() As Integer
        Get
            Return intCodiOperCeluCond
        End Get
        Set(ByVal value As Integer)
            intCodiOperCeluCond = value
        End Set
    End Property

#End Region

#Region "Funcionamiento del servicio"

    Private Sub Cargar_Valores_AppConfig()
        Me.Empresa = AppSettings.Item("CodigoEmpresa")
        Me.Usuario = AppSettings.Item("UsuarioWS")
        Me.Clave = AppSettings.Item("ClaveWS")
        Me.DiasReportarDespacho = AppSettings.Item("DiasReportarDespacho")
        Me.DiasConsultarNovedades = AppSettings.Item("DiasConsultarNovedades")
        Me.CadenaDeConexionSQL = AppSettings.Item("ConexionSQLGestrans")
        Me.NitEmpresa = AppSettings.Item("NitEmpresa")
    End Sub

    Private Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                ConexionSQL.Open()
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try

    End Function

#Region "Proceso Reporte Despachos"

    Private Sub Iniciar_Proceso_Despacho()
        Try
            Me.Faro = New faro
            While (True)
                Call Cargar_Valores_AppConfig()
                Call Consultar_Despachos_Pendientes()
                Thread.Sleep(Val(AppSettings.Get("TiempoSuspencionProcesoDespacho")))
            End While
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Consultar_Despachos_Pendientes()
        Dim strSQL As String
        Dim dsDataSet As New DataSet

        strSQL = "SELECT Numero FROM Encabezado_Manifiesto_Carga"
        strSQL += " WHERE EMPR_Codigo = " & Val(Me.Empresa)
        strSQL += " AND Fecha >= DATEADD(DAY,-" & Val(Me.DiasReportarDespacho) & ",CONVERT(DATETIME,'" & Format(Date.Today, "yyyy-MM-dd") & "',101))"
        strSQL += " AND ISNULL(Reporto_Empresa_Puesto_Control,1) = " & PENDIENTE_POR_REPORTAR
        strSQL += " AND Anulado = " & NO_ANULADO

        Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
        dsDataSet = Me.Retorna_Dataset(strSQL)

        If dsDataSet.Tables(0).Rows.Count > 0 Then
            For Each Registro As DataRow In dsDataSet.Tables(0).Rows
                Call Inicializar_Variables()
                Me.Manifiesto = Val(Registro.Item("Numero"))
                Call Reportar_Despachos()
            Next
        End If
    End Sub

    Private Sub Inicializar_Variables()
        Me.NitTransportadora = 0
        Me.Manifiesto = 0
        Me.FechaDespacho = Date.Today
        Me.CiudadOrigen = 0
        Me.CiudadDestino = 0
        Me.PlacaVehiculo = String.Empty
        Me.ModeloVehiculo = 0
        Me.MarcaVehiculo = 0
        Me.LineaVehiculo = 0
        Me.ColorVehiculo = 0
        Me.IdentificacionConductor = 0
        Me.NombreConductor = String.Empty
        Me.CiudadConductor = 0
        Me.TelefonoConductor = String.Empty
        Me.CelularConductor = String.Empty
        Me.ObservacionesManifiesto = String.Empty
        Me.CodigoRuta = 0
        Me.NombreRuta = String.Empty
        Me.TipoRuta = 0
        Me.ConfiguracionVehiculo = 0
        Me.CodigoCarroceria = 0
        Me.SerieMotor = String.Empty
        Me.Soat = String.Empty
        Me.FechaVenceSoat = Date.Today
        Me.NombreAseguradoraSoat = String.Empty
        Me.TarjetaPropiedad = String.Empty
        Me.PlacaSemiremolque = String.Empty
        Me.CategoriaLicencia = 0
        Me.DireccionConductor = String.Empty
        Me.IdentificacionPropietario = 0
        Me.NombrePropietario = String.Empty
        Me.CiudadPropietario = 0
        Me.DireccionPropietario = String.Empty
        Me.OperadorCelular = 0
        Me.strNitEmp = String.Empty
    End Sub

    Private Sub Asignar_Valores(ByVal registro As DataRow)
        Try
            Me.NitTransportadora = AppSettings.Item("NitEmpresa")
            Me.FechaDespacho = registro.Item("FechaDespacho")
            Me.CiudadOrigen = registro.Item("CIUD_Origen")
            Me.CiudadDestino = registro.Item("CIUD_Destino")
            Me.PlacaVehiculo = registro.Item("Placa")
            Me.ModeloVehiculo = registro.Item("Modelo")
            Me.MarcaVehiculo = registro.Item("MARC_Codigo")
            Me.LineaVehiculo = registro.Item("LINE_Codigo")
            Me.ColorVehiculo = registro.Item("COLO_Codigo")
            Me.IdentificacionConductor = registro.Item("IdenConductor")
            Me.NombreConductor = registro.Item("NombreConductor")
            Me.CiudadConductor = registro.Item("CiudadConductor")
            Me.TelefonoConductor = registro.Item("TeleConductor")
            Me.CelularConductor = registro.Item("CeluConductor")
            Me.ObservacionesManifiesto = registro.Item("Observaciones")
            Me.CodigoRuta = registro.Item("RUTA_Codigo")
            Me.NombreRuta = registro.Item("NombreRuta")
            Me.TipoRuta = registro.Item("TipoRuta")
            Me.ConfiguracionVehiculo = IIf(registro.Item("ConfiguracionVehiculo").Equals(DBNull.Value), "", registro.Item("ConfiguracionVehiculo"))
            Me.CodigoCarroceria = registro.Item("Carroceria")
            Me.SerieMotor = registro.Item("Numero_Motor")
            Me.Soat = registro.Item("Poliza_SOAT")
            Me.FechaVenceSoat = registro.Item("Fecha_Vence_SOAT")
            Me.NombreAseguradoraSoat = registro.Item("AseguradoraSOAT")
            Me.TarjetaPropiedad = registro.Item("Tarjeta_Propiedad")
            Me.PlacaSemiremolque = registro.Item("SEMI_Placa")
            Me.CategoriaLicencia = registro.Item("CateLiceCond")
            Me.DireccionConductor = registro.Item("DireConductor")
            Me.IdentificacionPropietario = registro.Item("IdenPropietario")
            Me.NombrePropietario = registro.Item("NombrePropietario")
            Me.CiudadPropietario = registro.Item("CIUD_Propietario")
            Me.DireccionPropietario = registro.Item("DirePropietario")
            'Me.OperadorCelular = Registro.Item("OperadorCelular")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Reportar_Despachos()
        Try
            Dim strSQL As String
            Dim dsDataSet As New DataSet
            Dim intReportado As Integer
            Dim strMensajeError As String
            Dim strConfirmacionFaro As String
            Dim arrayControsSeguim As arrayControsSeguim = Nothing
            Dim arrayDataAgencia As arrayDataAgencia = Nothing
            Dim arrayDataGps As arrayDataGps = Nothing
            Dim arrayDataRem As arrayDataRem = Nothing

            Dim dteFechDesp_Format As String
            Dim strFechVencSoat_Format As String

            strSQL = "SELECT * "
            strSQL += " FROM V_Reporte_Despacho_Faro"
            strSQL += " WHERE Manifiesto = " & Me.Manifiesto

            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            dsDataSet = Me.Retorna_Dataset(strSQL)

            If dsDataSet.Tables(0).Rows.Count > 0 Then
                For Each Registro As DataRow In dsDataSet.Tables(0).Rows
                    Call Asignar_Valores(Registro)

                    dteFechDesp_Format = Format(Me.FechaDespacho, "yyyy-MM-dd HH:mm:ss")
                    strFechVencSoat_Format = Format(Me.FechaVenceSoat, "yyyy-MM-dd")

                    intReportado = 0
                    strMensajeError = String.Empty

                    strConfirmacionFaro = Me.Faro.setSeguim(Me.Usuario, Me.Clave, Me.NitTransportadora, Me.Manifiesto, dteFechDesp_Format, Me.CiudadOrigen,
                                                            Me.CiudadDestino, Me.PlacaVehiculo, Me.ModeloVehiculo, Me.MarcaVehiculo, Me.LineaVehiculo,
                                                            Me.ColorVehiculo, Me.IdentificacionConductor, Me.NombreConductor, Me.CiudadConductor,
                                                            Me.TelefonoConductor, Me.CelularConductor, Me.ObservacionesManifiesto, Me.CodigoRuta, Me.NombreRuta,
                                                            Me.TipoRuta, Me.ConfiguracionVehiculo, Me.CodigoCarroceria, VALOR_VACIO, Me.SerieMotor, Me.Soat, strFechVencSoat_Format,
                                                            Me.NombreAseguradoraSoat, Me.TarjetaPropiedad, Me.PlacaSemiremolque, Me.CategoriaLicencia, Me.DireccionConductor,
                                                            Me.IdentificacionPropietario, Me.NombrePropietario, Me.CiudadPropietario, Me.DireccionPropietario, VALOR_VACIO,
                                                            arrayControsSeguim, arrayDataAgencia, 0, arrayDataGps, arrayDataRem, VALOR_VACIO)

                    If strConfirmacionFaro.Contains("code_resp:1000") Or strConfirmacionFaro = ("code_resp:6001; msg_resp:Numero de manifiesto repetido.") Then
                        intReportado = REPORTADO_CON_EXITO
                    Else
                        intReportado = REPORTADO_SIN_EXITO
                        strMensajeError = strConfirmacionFaro
                    End If
                    Call Actualizar_Confirmacion_Manifiesto(intReportado, strMensajeError)
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Actualizar_Confirmacion_Manifiesto(ByVal Reportado As Integer, ByVal Mensaje As String)
        Try
            'Se utiliza el mismo sp de Destino Seguro, ya que para el número de confirmación de Faro y mensaje de error, Se están utilizando los campos de Destino Seguro
            Dim ComandoSQL As SqlCommand = New SqlCommand("sp_actualizar_destino_seguro_manifiestos", Me.ConexionSQL)
            ComandoSQL.CommandType = CommandType.StoredProcedure

            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.Empresa
            ComandoSQL.Parameters.Add("@par_Numero", SqlDbType.Int) : ComandoSQL.Parameters("@par_Numero").Value = Me.Manifiesto
            ComandoSQL.Parameters.Add("@par_Reportado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Reportado").Value = Reportado
            ComandoSQL.Parameters.Add("@par_Error", SqlDbType.VarChar, 200) : ComandoSQL.Parameters("@par_Error").Value = Mensaje
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
                ComandoSQL.ExecuteNonQuery()

                Me.ConexionSQL.Close()
            End If
        Catch ex As Exception

            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Sub

#End Region

#Region "Proceso Consulta Novedades"

    Private Sub Iniciar_Proceso_Novedades()
        Try
            While (True)
                Call Cargar_Valores_AppConfig()
                Call Consultar_Seguimientos_Transito()
                Thread.Sleep(Val(AppSettings.Get("TiempoSuspencionProcesoNovedades")))
            End While
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Consultar_Seguimientos_Transito()
        Dim strSQL As String
        Dim dsDataSet As New DataSet

        strSQL = "SELECT Numero FROM V_Novedades_Seguimiento_Vehiculos WHERE"
        strSQL += " EMPR_Codigo = " & Me.Empresa
        'strSQL += " AND Fecha >= DATEADD(DAY,-" & Val(Me.DiasConsultaNovedades) & ",CONVERT(DATETIME,'" & Format(Date.Today, "yyyy-MM-dd") & "',101))"
        strSQL += " AND Destino_Seguro = " & REPORTADO_CON_EXITO
        strSQL += " AND Anulado = " & NO_ANULADO
        strSQL += " AND Estado = " & EN_TRANSITO

        Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
        dsDataSet = Me.Retorna_Dataset(strSQL)

        If dsDataSet.Tables(0).Rows.Count > 0 Then
            Call Consultar_Novedades_Faro()
        End If
    End Sub

    Private Sub Consultar_Novedades_Faro()
        Try
            Dim arrMensaje As String()
            Dim strSQL
            Dim ComandoSQL As SqlCommand
            Dim dsDataSet As New DataSet
            Dim strMensajeError As String
            Dim strConfirmacionFaro As String


            strSQL = "SELECT Numero, NitTransportadora, PlacaVehiculo "
            strSQL += "FROM V_Reporte_Despacho_Faro WHERE EMPR_Codigo = " & Me.Empresa & " AND Numero = " & Val(Me.Manifiesto)

            ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            dsDataSet = Me.Retorna_Dataset(strSQL)

            If dsDataSet.Tables(0).Rows.Count > 0 Then
                For Each Registro As DataRow In dsDataSet.Tables(0).Rows
                    Me.NitTransportadora = Registro.Item("NitTransportadora").ToString()
                    Me.PlacaVehiculo = Registro.Item("PlacaVehiculo").ToString()
                    Me.Manifiesto = Val(Registro.Item("Numero"))



                    If Not strConfirmacionFaro.Contains("0-0-No hay novedades") And Not strConfirmacionFaro.Contains("ERROR DE AUTENTICACION") Then
                        arrMensaje = Split(strConfirmacionFaro, ";") 'Se realiza un array a las novedades retornadas las cuales vienen separadas por ;
                        For Each arrNovedades As String In arrMensaje 'Se realiza el recorrido a todas las novedades
                            arrMensaje = Split(arrNovedades, ",") 'Se realiza un array a la lista de cada novedad
                            If arrMensaje(0) <> String.Empty Then
                                'Me.FechaReporteNovedad = arrMensaje(8)
                                'strFechRepo = Format(Me.FechaReporteNovedad, "yyyy-MM-dd")
                                'strHoraFechRepo = Format(Me.FechaReporteNovedad, "HH")
                                'Me.Ubicacion = arrMensaje(5)
                                'Me.ObservacionesNovedad = arrMensaje(7)
                                'If Not Existe_Novedad(arrMensaje(2), arrMensaje(5), strFechRepo, strHoraFechRepo) Then
                                '    Call Insertar_Detalle_Seguimiento_Vehicular(arrMensaje(2), arrMensaje(8), arrMensaje(5), arrMensaje(7))
                                'End If
                            End If
                        Next
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        Me.ProcesoReporteDespachos = New Thread(AddressOf Iniciar_Proceso_Despacho)
        Me.ProcesoReporteDespachos.Start()
        'Me.ProcesoConsultaNovedades = New Thread(AddressOf Iniciar_Proceso_Novedades)
        'Me.ProcesoConsultaNovedades.Start()
    End Sub

    Protected Overrides Sub OnStop()
        Me.ProcesoReporteDespachos.Abort()
        'Me.ProcesoConsultaNovedades.Abort()
    End Sub

#End Region

End Class
