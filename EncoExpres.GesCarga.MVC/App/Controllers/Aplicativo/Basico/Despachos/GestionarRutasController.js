﻿EncoExpresApp.controller("GestionarRutasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'RutasFactory', 'CiudadesFactory', 'OficinasFactory', 'ValorCatalogosFactory', 'blockUIConfig',
    function ($scope, $routeParams, $timeout, $linq, blockUI, RutasFactory, CiudadesFactory, OficinasFactory, ValorCatalogosFactory, blockUIConfig) {

        $scope.Titulo = 'GESTIONAR RUTAS';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Rutas' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        var NombreRutaObtener = ''
        var CodigoAlternoRutaObtener = ''
        $scope.CampoNombreModificado = false
        $scope.CampoCodigoAlternoModificado = false
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_RUTAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoCiudades = []
        $scope.ListadoCiudadesOrigen = []
        $scope.ListadoEstados = []
        $scope.ListadoTipoRutas = []

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVA" },
            { Codigo: 1, Nombre: "ACTIVA" },
        ]

        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }

        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }

        $scope.AutocompleteOficinas = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    var Response = OficinasFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoOficinas = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoOficinas)
                }
            }
            return $scope.ListadoOficinas
        }



        /*Cargar el combo de tipo semirremolque*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_RUTA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRutas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoRutas = response.data.Datos;
                        try {
                            $scope.Modelo.TipoRuta = $linq.Enumerable().From($scope.ListadoTipoRutas).First('$.Codigo ==' + $scope.Modelo.TipoRuta.Codigo);
                        } catch (e) {
                            $scope.Modelo.TipoRuta = $scope.ListadoTipoRutas[0]
                        }
                    }
                    else {
                        $scope.ListadoTipoRutas = []
                    }
                }
            }, function (response) {
            });

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando rutas código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando rutas Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            RutasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo.Codigo = response.data.Datos.Codigo;
                        $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                        CodigoAlternoRutaObtener = $scope.Modelo.CodigoAlterno
                        $scope.Modelo.Nombre = response.data.Datos.Nombre;
                        NombreRutaObtener = $scope.Modelo.Nombre;
                        $scope.Modelo.DuracionHoras = response.data.Datos.DuracionHoras;
                        $scope.Modelo.PorcentajeAnticipo = response.data.Datos.PorcentajeAnticipo;
                        $scope.Modelo.Kilometros = response.data.Datos.Kilometros;
                        $scope.Modelo.PlanPuntos = response.data.Datos.PlanPuntos;
                        $scope.Modelo.CiudadOrigen = $scope.CargarCiudad(response.data.Datos.CiudadOrigen.Codigo);
                        $scope.Modelo.CiudadDestino = $scope.CargarCiudad(response.data.Datos.CiudadDestino.Codigo);
                        $scope.Modelo.OficinaDestino = $scope.CargarOficina(response.data.Datos.OficinaDestino.Codigo);
                        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)
                        if ($scope.ListadoTipoRutas.length > 0) {
                            $scope.Modelo.TipoRuta = $linq.Enumerable().From($scope.ListadoTipoRutas).First('$.Codigo ==' + response.data.Datos.TipoRuta.Codigo);
                        } else {
                            $scope.Modelo.TipoRuta = response.data.Datos.TipoRuta
                        }
                    }
                    else {
                        ShowError('No se logro consultar la ruta código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarRutas';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarRutas';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
            RutasFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó la ruta "' + $scope.Modelo.Nombre + '"');
                                location.href = '#!ConsultarRutas/' + response.data.Datos;
                            } else {
                                ShowSuccess('Se modificó la ruta "' + $scope.Modelo.Nombre + '"');
                                location.href = '#!ConsultarRutas/' + $scope.Modelo.Codigo;
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    if (response.statusText.includes('IX_Rutas_Codigo_Alterno')) {
                        ShowError('No se pudo guardar, el código alterno ingresado ya está siendo utilizado')
                    } else {
                        ShowError(response.statusText);
                    }
                });
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            var NombreExiste = RutasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Nombre: $scope.Modelo.Nombre,

                Estado: ESTADO_ACTIVO,
                Pagina: 1,
                RegistrosPagina: 10
                , Sync: true
            }).Datos

            if (NombreExiste.length > 0) {
                var cN = 0
                NombreExiste.forEach(item => {
                    if (item.Nombre == $scope.Modelo.Nombre) {
                        cN++
                    }
                })
                if (cN == 0) {
                    NombreExiste = []
                }
            }
            var CodigoExiste = RutasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,

                CodigoAlterno: $scope.Modelo.CodigoAlterno,
                Estado: ESTADO_ACTIVO,
                Pagina: 1,
                RegistrosPagina: 10
                , Sync: true
            }).Datos
            if (CodigoExiste.length > 0) {
                var cC = 0
                CodigoExiste.forEach(item => {
                    if (item.CodigoAlterno == $scope.Modelo.CodigoAlterno) {
                        cC++
                    }
                })
                if (cC == 0) {
                    CodigoExiste = []
                }
            }


            if ($scope.Modelo.Nombre === undefined || $scope.Modelo.Nombre === '' && $scope.Modelo.Nombre === null) {
                $scope.MensajesError.push('Debe ingresar el nombre de la ruta');
                continuar = false;
            }

            if (NombreExiste.length > 0) {
                if ($scope.CampoNombreModificado) {
                    if (NombreRutaObtener != '') {
                        if ($scope.Modelo.Nombre != NombreRutaObtener) {
                            $scope.MensajesError.push('El Nombre de la ruta ya se encuentra registrado');
                            continuar = false;
                        }
                    }
                }
            }

            if (CodigoExiste.length > 0) {
                if ($scope.CampoCodigoAlternoModificado) {
                    if (CodigoAlternoRutaObtener != '') {
                        if ($scope.Modelo.CodigoAlterno != CodigoAlternoRutaObtener) {
                            $scope.MensajesError.push('El Código Alterno de la ruta ya se encuentra registrado');
                            continuar = false;
                        }
                    }
                }
            }
            if ($scope.Modelo.CiudadOrigen == undefined || $scope.Modelo.CiudadOrigen == null || $scope.Modelo.CiudadOrigen == "") {
                $scope.MensajesError.push('Debe ingresar la ciudad de origen');
                continuar = false;
            }
            if ($scope.Modelo.CiudadDestino == undefined || $scope.Modelo.CiudadDestino == null || $scope.Modelo.CiudadDestino == "") {
                $scope.MensajesError.push('Debe ingresar la ciudad de destino');
                continuar = false;
            }
            if ($scope.Modelo.OficinaDestino == undefined || $scope.Modelo.OficinaDestino == null || $scope.Modelo.OficinaDestino == "") {
                $scope.MensajesError.push('Debe ingresar la oficina de destino');
                continuar = false;
            }
            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
                $scope.MensajesError.push('Debe ingresar el nombre de la ruta');
                continuar = false;
            }
            if ($scope.Modelo.PorcentajeAnticipo !== undefined || $scope.Modelo.PorcentajeAnticipo !== "" || $scope.Modelo.PorcentajeAnticipo !== 0 || $scope.Modelo.PorcentajeAnticipo !== null || $scope.Modelo.PorcentajeAnticipo !== "0") {
                if ($scope.Modelo.PorcentajeAnticipo > 100) {
                    $scope.MensajesError.push('El porcentaje de anticipo no puede exceder el 100%');
                    continuar = false;
                }
            }
            if ($scope.Modelo.TipoRuta.Codigo === 4400) {
                $scope.MensajesError.push('Debe seleccionar el tipo de ruta');
                continuar = false;
            }

            return continuar;
        }

        $scope.ValidarPuntos = function (puntos) {
            if (puntos !== undefined && puntos !== null && puntos !== '') {
                if (puntos > 1000) {
                    $scope.Modelo.PlanPuntos = '';
                    ShowWarning('', 'Los puntos no pueden ser más de 1000');
                }
            }
        };

        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope);
        };
        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarRutas/' + $scope.Modelo.Codigo;
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase(); } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.PlanPuntos = MascaraNumero($scope.Modelo.PlanPuntos); } catch (e) { }
        };
    }]);