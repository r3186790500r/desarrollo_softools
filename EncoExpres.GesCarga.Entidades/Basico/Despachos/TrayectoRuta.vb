﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" Rutas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TrayectoRuta
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Rutas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Rutas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")}
            RutaTrayecto = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo_Trayecto"), .Nombre = Read(lector, "NombreRutaTrayecto"), .CiudadOrigen = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Origen_Trayecto"), .Nombre = Read(lector, "NombreCiudadOrigenTrayecto")}, .CiudadDestino = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino_Trayecto"), .Nombre = Read(lector, "NombreCiudadDestinoTrayecto")}, .PorcentajeAnticipo = Read(lector, "Porcentaje_Anticipo_Trayecto")}
            Orden = Read(lector, "Orden")
            PorcentajeFlete = Read(lector, "Porcentaje_Flete")
        End Sub

        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property RutaTrayecto As Rutas
        <JsonProperty>
        Public Property Orden As Integer
        <JsonProperty>
        Public Property PorcentajeFlete As Double
    End Class
End Namespace
