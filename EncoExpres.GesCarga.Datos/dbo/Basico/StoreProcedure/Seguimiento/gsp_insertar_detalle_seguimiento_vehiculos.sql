﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 17/06/2019
	Descripción_Crea: 
	Modifica: Geferson Latorre
	Fecha_Modifica: 27/06/2019
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


PRINT 'gsp_insertar_detalle_seguimiento_vehiculos'
GO
DROP PROCEDURE gsp_insertar_detalle_seguimiento_vehiculos
GO 
CREATE PROCEDURE gsp_insertar_detalle_seguimiento_vehiculos 
(                
@par_EMPR_Codigo SMALLINT,                
@par_ENPD_Numero NUMERIC = NULL,                
@par_ENOC_Numero NUMERIC = NULL,        
@par_Numero_Manifiesto NUMERIC = NULL,              
@par_Ubicacion VARCHAR(250) = NULL,                
@par_Longitud NUMERIC (18, 10)  = NULL,                
@par_Latitud NUMERIC (18, 10)  = NULL,                
@par_PUCO_Codigo NUMERIC = NULL,       
@par_CATA_TOSV_Codigo NUMERIC  = NULL,               
@par_CATA_SRSV_Codigo NUMERIC = NULL,                
@par_CATA_NOSV_Codigo NUMERIC = NULL,                
@par_Observaciones VARCHAR(250) = NULL,                
@par_Kilometros_Vehiculo NUMERIC = NULL,                 
@par_Reportar_Cliente SMALLINT = NULL,       
@par_Prioritario SMALLINT = NULL,            
@par_Envio_Reporte_Cliente SMALLINT = NULL,                
@par_Fecha_Reporte_Cliente DATETIME = NULL,                
@par_USUA_Codigo_Crea SMALLINT                
)                
AS                
BEGIN                
DECLARE @sma_Orden SMALLINT = 1   
DECLARE @Fin_Viaje SMALLINT = 0               
              
IF @par_ENPD_Numero <> 0  AND  @par_ENPD_Numero = 0        
BEGIN              
SELECT TOP 1 @sma_Orden = ISNULL(Orden,0) + 1 FROM Detalle_Seguimiento_Vehiculos                
WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENOC_Numero = @par_ENOC_Numero ORDER BY Orden DESC    
IF @par_CATA_SRSV_Codigo = 8210
BEGIN
UPDATE Detalle_Seguimiento_Vehiculos SET Fin_Viaje = 1 WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENOC_Numero = @par_ENOC_Numero
SELECT @Fin_Viaje = 1
END  
END                
ELSE         
BEGIN              
SELECT  TOP 1  @sma_Orden = ISNULL(Orden,0) + 1 FROM Detalle_Seguimiento_Vehiculos                 
WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_ENPD_Numero  ORDER BY Orden DESC       
UPDATE Encabezado_Planilla_Despachos SET Ultimo_Seguimiento_Vehicular = GETDATE() WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENPD_Numero  
IF @par_CATA_SRSV_Codigo = 8210
BEGIN
UPDATE Detalle_Seguimiento_Vehiculos SET Fin_Viaje = 1 WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_ENPD_Numero  
SELECT @Fin_Viaje = 1 
END          
END              
                
INSERT INTO Detalle_Seguimiento_Vehiculos               
(                
EMPR_Codigo,                
ENPD_Numero,                
ENOC_Numero,        
ENMC_Numero,              
Fecha_Reporte,                
CATA_TOSV_Codigo,                
Ubicacion,                
Longitud,                
Latitud,                
PUCO_Codigo,                
CATA_SRSV_Codigo,                
CATA_NOSV_Codigo,                
Observaciones,                
Kilometros_Vehiculo,                
Reportar_Cliente,   
Prioritario,               
Envio_Reporte_Cliente,                
Fecha_Reporte_Cliente,                
USUA_Codigo_Crea,                
Fecha_Crea,                
Anulado,                
Orden,
Fin_Viaje            
)                
VALUES                 
(                
@par_EMPR_Codigo,                
ISNULL(@par_ENPD_Numero, 0),                
ISNULL(@par_ENOC_Numero, 0),        
ISNULL(@par_Numero_Manifiesto, 0),                
GETDATE(),                
ISNULL(@par_CATA_TOSV_Codigo, 8000),                
ISNULL(@par_Ubicacion, ''),                
ISNULL(@par_Longitud, 0),                
ISNULL(@par_Latitud, 0),                
@par_PUCO_Codigo,                
ISNULL(@par_CATA_SRSV_Codigo, 8200),                
ISNULL(@par_CATA_NOSV_Codigo, 8100),                
ISNULL(@par_Observaciones,''),                
ISNULL(@par_Kilometros_Vehiculo, 0),                
ISNULL(@par_Reportar_Cliente, 0),       
ISNULL(@par_Prioritario, 0),           
ISNULL(@par_Envio_Reporte_Cliente, 0),                
ISNULL(@par_Fecha_Reporte_Cliente, '1900-01-01'),                
@par_USUA_Codigo_Crea,                
GETDATE(),                
0,                
@sma_Orden,
@Fin_Viaje               
)          
                
SELECT @@IDENTITY AS Codigo                
                
END 
GO