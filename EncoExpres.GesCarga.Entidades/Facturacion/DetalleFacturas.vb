﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla

Namespace Facturacion
    <JsonObject>
    Public NotInheritable Class DetalleFacturas
        Inherits BaseDetalleDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroEncabezado = Read(lector, "ENFA_Numero")
            Remesas = New Remesas With {
                .Numero = Read(lector, "ENRE_Numero"),
                .NumeroDocumento = Read(lector, "ENRE_Numero_Documento"),
                .TipoDocumento = New TipoDocumentos With {.Codigo = Read(lector, "ENRE_TIDO_Codigo")},
                .MBLContenedor = Read(lector, "MBL_Contenedor"),
                .NumeroOrdenServicio = Read(lector, "ENOS_Numero_Documento"),
                .Planilla = New Planillas With {.NumeroDocumento = Read(lector, "ENPD_Numero_Documento")},
                .Manifiesto = New Manifiesto With {.NumeroDocumento = Read(lector, "ENMC_Numero_Documento")},
                .Fecha = Read(lector, "Fecha"), .Ruta = New Rutas With {.Nombre = Read(lector, "NombreRuta")},
                .NumeroDocumentoCliente = Read(lector, "Numero_Documento_Cliente"),
                .ValorFleteCliente = Read(lector, "Valor_Flete_Cliente"),
                .ProductoTransportado = New ProductoTransportados With {.Nombre = Read(lector, "NombreProducto")},
                .CantidadCliente = Read(lector, "Cantidad_Cliente"),
                .PesoCliente = Read(lector, "Peso_Cliente"),
                .TotalFleteCliente = Read(lector, "Total_Flete_Cliente"),
                .Observaciones = Read(lector, "Observaciones"),
                .Remitente = New Terceros With {.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Remitente"), .Nombre = Read(lector, "CiudadRemitente")}},
                .Destinatario = New Terceros With {.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destinatario"), .Nombre = Read(lector, "CiudadDestinatario")}},
                .Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Placa")},
                .TipoDespacho = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TDOS_Codigo"), .Nombre = Read(lector, "TipoDespacho")}
            }

        End Sub

        <JsonProperty>
        Public Property Remesas As Remesas
        <JsonProperty>
        Public Property Modifica As Short

    End Class
End Namespace
