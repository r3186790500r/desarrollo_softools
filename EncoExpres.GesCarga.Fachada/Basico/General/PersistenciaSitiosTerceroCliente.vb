﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Repositorio.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Operacion

Namespace Basico.General
    Public NotInheritable Class PersistenciaSitiosTerceroCliente
        Implements IPersistenciaBase(Of SitiosTerceroCliente)

        Public Function Consultar(filtro As SitiosTerceroCliente) As IEnumerable(Of SitiosTerceroCliente) Implements IPersistenciaBase(Of SitiosTerceroCliente).Consultar
            Return New RepositorioSitiosTerceroCliente().Consultar(filtro)
        End Function

        Public Function ConsultarOrdenServicio(filtro As SitiosTerceroCliente) As IEnumerable(Of SitiosTerceroCliente)
            Return New RepositorioSitiosTerceroCliente().ConsultarOrdenServicio(filtro)
        End Function

        Public Function Insertar(entidad As SitiosTerceroCliente) As Long Implements IPersistenciaBase(Of SitiosTerceroCliente).Insertar
            Return New RepositorioSitiosTerceroCliente().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As SitiosTerceroCliente) As Long Implements IPersistenciaBase(Of SitiosTerceroCliente).Modificar
            Return New RepositorioSitiosTerceroCliente().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As SitiosTerceroCliente) As SitiosTerceroCliente Implements IPersistenciaBase(Of SitiosTerceroCliente).Obtener
            Return New RepositorioSitiosTerceroCliente().Obtener(filtro)
        End Function

    End Class

End Namespace

