﻿CREATE PROCEDURE gsp_obtener_planilla_despacho_formato_gastos
(
@par_EMPR_Codigo smallint,
@par_Numero numeric = null,
@par_Usuario varchar(255) = null
)
AS
BEGIN
SELECT
ENPD.Numero_Documento,
EMPR.Numero_Identificacion,
ENPD.Fecha,
VEHI.Placa,
SEMI.Placa AS Semirremolque,
VEHI.Kilometraje,
ENPD.Valor_Flete_Transportador,
ENPD.Observaciones,
RUTA.Nombre,
(SELECT Nombre FROM Usuarios WHERE Usuarios.EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_Usuario) AS Usuario,
RTRIM(LTRIM(CONCAT(ISNULL(CLIE.Razon_Social,''),' ',ISNULL(CLIE.Nombre,''),' ',ISNULL(CLIE.Apellido1,''),' ',ISNULL(CLIE.Apellido2,'')))) AS Cliente


FROM 
Encabezado_Planilla_Despachos ENPD

LEFT JOIN  Vehiculos VEHI ON
ENPD.EMPR_Codigo = VEHI.EMPR_Codigo AND
ENPD.VEHI_Codigo = VEHI.Codigo

LEFT JOIN Empresas EMPR ON
ENPD.EMPR_Codigo = EMPR.Codigo

LEFT JOIN  Semirremolques SEMI ON
ENPD.EMPR_Codigo = SEMI.EMPR_Codigo AND
ENPD.SEMI_Codigo = SEMI.Codigo

LEFT JOIN Rutas RUTA ON
ENPD.EMPR_Codigo = RUTA.EMPR_Codigo AND
ENPD.RUTA_Codigo = RUTA.Codigo

LEFT JOIN Encabezado_Remesas ENRE ON
ENPD.EMPR_Codigo = ENRE.EMPR_Codigo AND
ENPD.Numero = ENRE.ENPD_Numero

LEFT JOIN Terceros CLIE ON
ENPD.EMPR_Codigo = CLIE.EMPR_Codigo AND
ENRE.TERC_Codigo_Cliente = CLIE.Codigo



WHERE ENPD.EMPR_Codigo = @par_EMPR_Codigo AND ENPD.Numero = @par_Numero
END
GO
