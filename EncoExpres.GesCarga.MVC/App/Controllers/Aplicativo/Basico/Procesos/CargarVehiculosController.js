﻿EncoExpresApp.controller("CargarVehiculosCtrl", ['$scope', '$timeout', '$linq', 'TercerosFactory', 'blockUI', 'ValorCatalogosFactory', 'VehiculosFactory', 'blockUIConfig', 'MarcaVehiculosFactory', 'LineaVehiculosFactory', 'ColorVehiculosFactory',
    function ($scope, $timeout, $linq, TercerosFactory, blockUI, ValorCatalogosFactory, VehiculosFactory, blockUIConfig, MarcaVehiculosFactory, LineaVehiculosFactory, ColorVehiculosFactory) {
        console.clear()



        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.continuar = false
        $scope.VerErrores = false;
        $scope.DeshabilitarCliente = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.ListadoRutasPuntosGestion = [];
        $scope.Modelo = {
            Fecha: new Date(),
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,

        }
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Procesos' }, { Nombre: 'Cargue Vehículos' }];
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/


        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DUEÑO_VEHICULO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoDueno = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoDueno = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoDueno = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de tipo vehiculo*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoVehiculo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoVehiculo = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoVehiculo = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de tipo carroceria*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CARROCERIA }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoCarroceria = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoCarroceria = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoCarroceria = []
                    }
                }
            }, function (response) {
            });
        MarcaVehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoMarcas = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoMarcas = response.data.Datos;
                    }
                    else {
                        $scope.ListadoMarcas = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.AsignarLineaVehiculo = function (Marca) {
            if (Marca.Codigo > 0) {
                FiltroMarca = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Marca: Marca, Estado: 1, Sync: true }
                var Response = LineaVehiculosFactory.Consultar(FiltroMarca)
                ListadoLineas = []
                if (Response.Datos.length > 0) {
                    ListadoLineas = angular.copy(Response.Datos);
                }
            }
            return ListadoLineas
        }
        //Funciones Autocomplete
        $scope.ListadoPropietarios = [];
        $scope.AutocompletePropietario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_PROPIETARIO,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoPropietarios = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoPropietarios)
                }
            }
            return $scope.ListadoPropietarios
        }
        $scope.ListadoTenedores = [];

        $scope.AutocompleteTenedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            CadenaPerfiles: PERFIL_TENEDOR,
                            ValorAutocomplete: value, Sync: true
                        })
                    $scope.ListadoTenedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTenedores)
                }
            }
            return $scope.ListadoTenedores
        }

        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }

        $scope.ListadoAseguradoras = [];
        $scope.AutocompleteAseguradora = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_ASEGURADORA, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoAseguradoras = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoAseguradoras)
                }
            }
            return $scope.ListadoAseguradoras
        }
        $scope.ListadoAfiliadores = [];
        $scope.AutocompleteAfilidor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_EMPRESA_AFILIADORA,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoAfiliadores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoAfiliadores)
                }
            }
            return $scope.ListadoAfiliadores
        }
        $scope.ListadoProveedores = [];
        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR_GPS, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoProveedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedores)
                }
            }
            return $scope.ListadoProveedores
        }
        $scope.AutocompleteColores = function (value) {
            $scope.ListadoColores = [];
            if (value.length > 0) {
                /*Cargar Autocomplete de propietario*/
                blockUIConfig.autoBlock = false;
                var Response = ColorVehiculosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    ValorAutocomplete: value,
                    Sync: true
                })
                $scope.ListadoColores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoColores)
            }
            return $scope.ListadoColores
        }

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.DataActual = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.DataActual = []
                for (var i = a - 10; i < $scope.DataArchivo.length; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }

        //Paginacion MOdal de errores

        $scope.PrimerPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10) {
                $scope.DataErrorActual = []
                $scope.paginaActualError = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.AnteriorModalError = function () {
            if ($scope.paginaActualError > 1) {
                $scope.paginaActualError -= 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteModalError = function () {
            if ($scope.paginaActualError < $scope.totalPaginasErrores) {
                $scope.paginaActualError += 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    $scope.UltimaPaginaModalError()

                }
            } else if ($scope.paginaActualError == $scope.totalPaginasErrores) {
                $scope.UltimaPaginaModalError()
            }
        }
        $scope.UltimaPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10 && $scope.totalPaginasErrores > 1) {
                $scope.paginaActualError = $scope.totalPaginasErrores
                var a = $scope.paginaActualError * 10
                $scope.DataErrorActual = []
                for (var i = a - 10; i < $scope.ListaErrores.length; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10) {
                $scope.ProgramacionesNoGuardadasActual = []
                $scope.paginaActualNoGuardadas = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }
        $scope.AnteriorNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas -= 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas < $scope.totalPaginasNoGuardadas) {
                $scope.paginaActualNoGuardadas += 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardados()

                }
            } else if ($scope.paginaActualNoGuardadas == $scope.totalPaginasNoGuardadas) {
                $scope.UltimaPaginaNoGuardados()
            }
        }
        $scope.UltimaPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10 && $scope.totalPaginasNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas = $scope.totalPaginasNoGuardadas
                var a = $scope.paginaActualNoGuardadas * 10
                $scope.ProgramacionesNoGuardadasActual = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10) {
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                $scope.paginaActualNoGuardadasRemplazo = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }
        $scope.AnteriorNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo -= 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo < $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.paginaActualNoGuardadasRemplazo += 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardadosRemplazo()

                }
            } else if ($scope.paginaActualNoGuardadasRemplazo == $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.UltimaPaginaNoGuardadosRemplazo()
            }
        }
        $scope.UltimaPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10 && $scope.totalPaginasNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo = $scope.totalPaginasNoGuardadasRemplazo
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }


        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardaras = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardadasRemplazo = function (item) {
            if ($scope.Option.name == 'Omitido') {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Omitido'
                }
            } else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Remplazado'
                }
            }

        }
        $scope.LimpiarData = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            $scope.DataVerificada = []
            $scope.DataActual = []
            $scope.DataErrorActual = []
            $scope.DataArchivo = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            $scope.VerErrores = false
        }

        /*-----------------------------------------------------------------------funciones de lectura y apertura de archivos----------------------------------------------------------------------------*/

        $scope.ListaProgramaciones = []
        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined || item == '') {
                return true
            } else {
                return false
            }
        }
        function AsignarValoresTabla() {
            $scope.NumeroCargue = 0

            if ($scope.DataArchivo.Valores.length > 0) {
                var DataTemporal = []
                for (var i = 0; i < $scope.DataArchivo.Valores.length; i++) {
                    var item = $scope.DataArchivo.Valores[i]

                    if (!Eval(item.Placa)) {
                        DataTemporal.push(item)
                    }
                }
                $scope.DataArchivo = DataTemporal
                $scope.totalRegistros = ($scope.DataArchivo.length)
                $scope.paginaActual = 1
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                blockUI.start();
                blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');
                $timeout(function () {
                    try {
                        var data = []
                        for (var i = 0; i < $scope.DataArchivo.length; i++) {
                            var DataItem = $scope.DataArchivo[i]
                            DataItem.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                            DataItem.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                            if (!Eval(DataItem.Placa)) { try { DataItem.Placa = MascaraPlaca(DataItem.Placa) } catch (e) { } }
                            if (!Eval(DataItem.Codigo_Interno)) { try { DataItem.CodigoAlterno =DataItem.Codigo_Interno } catch (e) { } }
                            if (!Eval(DataItem.Fecha_Vinculacion)) { try { DataItem.FechaCrea = new Date(DataItem.Fecha_Vinculacion) } catch (e) { } }
                            if (!Eval(DataItem.Fecha_Inactivacion)) { try { DataItem.FechaModifica = new Date(DataItem.Fecha_Inactivacion) } catch (e) { } }
                            if (!Eval(DataItem.Identificacion_Propietario)) { try { DataItem.Propietario = $scope.CargarTerceroIdentificacion(DataItem.Identificacion_Propietario); } catch (e) { } }
                            if (!Eval(DataItem.Identificación_Tenedor)) { try { DataItem.Tenedor = $scope.CargarTerceroIdentificacion(DataItem.Identificación_Tenedor); } catch (e) { } }
                            if (!Eval(DataItem.Identificación_Conductor)) { try { DataItem.Conductor = $scope.CargarTerceroIdentificacion(DataItem.Identificación_Conductor); } catch (e) { } }
                            if (!Eval(DataItem.Tipo_Dueno)) { try { DataItem.TipoDueno = $linq.Enumerable().From($scope.ListadoTipoDueno).First('$.Codigo ==' + DataItem.Tipo_Dueno); } catch (e) { DataItem.TipoDueno = $scope.ListadoTipoDueno[0] } } else { DataItem.TipoDueno = $scope.ListadoTipoDueno[0] }
                            if (!Eval(DataItem.Tipo_Vehiculo)) { try { DataItem.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo ==' + DataItem.Tipo_Vehiculo); } catch (e) { DataItem.TipoVehiculo = $scope.ListadoTipoVehiculo[0] } } else { DataItem.TipoVehiculo = $scope.ListadoTipoVehiculo[0] }
                            if (!Eval(DataItem.Tarjeta_Propiedad)) { try { DataItem.TarjetaPropiedad = MascaraNumero(DataItem.Tarjeta_Propiedad); } catch (e) { } }
                            if (!Eval(DataItem.Tipo_Carroceria)) { try { DataItem.TipoCarroceria = $linq.Enumerable().From($scope.ListadoTipoCarroceria).First('$.Codigo ==' + DataItem.Tipo_Carroceria); } catch (e) { DataItem.TipoCarroceria = $scope.ListadoTipoCarroceria[0] } } else { DataItem.TipoCarroceria = $scope.ListadoTipoCarroceria[0] }
                            if (!Eval(DataItem.Numero_Motor)) { try { DataItem.NumeroMotor = MascaraMayus(DataItem.Numero_Motor); } catch (e) { } }
                            if (!Eval(DataItem.Modelo)) { try { DataItem.Modelo = MascaraNumero(DataItem.Modelo); } catch (e) { } }
                            if (!Eval(DataItem.Modelo_Repotenciado)) { try { DataItem.ModeloRepotenciado = MascaraNumero(DataItem.Modelo_Repotenciado); } catch (e) { } }
                            if (!Eval(DataItem.Numero_Serie)) { try { DataItem.Chasis = MascaraMayus(DataItem.Numero_Serie); } catch (e) { } }
                            if (!Eval(DataItem.Color)) { try { DataItem.Color = $scope.AutocompleteColores(DataItem.Color)[0]; } catch (e) { } }
                            if (!Eval(DataItem.Marca)) {
                                try {
                                    DataItem.Marca = $linq.Enumerable().From($scope.ListadoMarcas).First('$.Nombre =="' + MascaraMayus(DataItem.Marca) + '"');
                                    DataItem.ListadoLineas = $scope.AsignarLineaVehiculo(DataItem.Marca)
                                    if (!Eval(DataItem.Linea)) {
                                        try {
                                            DataItem.Linea = $linq.Enumerable().From(DataItem.ListadoLineas).First('$.Nombre =="' + MascaraMayus(DataItem.Linea) + '"');
                                        } catch (e) {
                                            DataItem.Linea
                                        }
                                    }
                                } catch (e) { }
                            }
                            if (!Eval(DataItem.Peso_Bruto)) { try { DataItem.PesoBruto = MascaraValores(DataItem.Peso_Bruto); } catch (e) { } }
                            if (!Eval(DataItem.Capacidad)) { try { DataItem.Capacidad = MascaraValores(DataItem.Capacidad); } catch (e) { } }
                            if (!Eval(DataItem.Peso_Vacio)) { try { DataItem.PesoTara = MascaraValores(DataItem.Peso_Vacio); } catch (e) { } }
                            if (!Eval(DataItem.Capacidad_Galones)) { try { DataItem.CapacidadGalones = MascaraValores(DataItem.Capacidad_Galones); } catch (e) { } }
                            if (!Eval(DataItem.No_Ejes)) { try { DataItem.NumeroEjes = MascaraValores(DataItem.No_Ejes); } catch (e) { } }
                            if (!Eval(DataItem.Cilindraje)) { try { DataItem.Cilindraje = MascaraValores(DataItem.Cilindraje); } catch (e) { } }
                            if (!Eval(DataItem.Kilometraje)) { try { DataItem.Kilometraje = MascaraValores(DataItem.Kilometraje); } catch (e) { } }
                            if (!Eval(DataItem.URL_Gps)) { try { DataItem.URLGPS = MascaraMayus(DataItem.URL_Gps); } catch (e) { } }
                            if (!Eval(DataItem.Identificacion_Proveedor_GPS)) { try { DataItem.ProveedorGPS = $scope.CargarTerceroIdentificacion(DataItem.Identificacion_Proveedor_GPS); } catch (e) { } }
                            if (!Eval(DataItem.Identificador_GPS)) { try { DataItem.IdentificadorGPS = MascaraMayus(DataItem.Identificador_GPS); } catch (e) { } }
                            if (!Eval(DataItem.Usuario_GPS)) { try { DataItem.UsuarioGPS = MascaraMayus(DataItem.Usuario_GPS); } catch (e) { } }
                            if (!Eval(DataItem.Clave_GPS)) { try { DataItem.ClaveGPS = MascaraMayus(DataItem.Clave_GPS); } catch (e) { } }
                            if (!Eval(DataItem.Telefono_GPS)) { try { DataItem.TelefonoGPS = MascaraNumero(DataItem.Telefono_GPS); } catch (e) { } }
                            if (!Eval(DataItem.Estado)) { try { DataItem.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + DataItem.Estado); } catch (e) { DataItem.Estado = $scope.ListadoEstados[0] } } else { DataItem.Estado = $scope.ListadoEstados[0] }
                            if (!Eval(DataItem.Justificacion_Inactividad)) { try { DataItem.JustificacionBloqueo = MascaraMayus(DataItem.Justificacion_Inactividad); } catch (e) { } }

                        }
                        if ($scope.totalRegistros > 10) {
                            for (var i = 0; i < 10; i++) {
                                $scope.DataActual.push($scope.DataArchivo[i])
                            }
                        } else {
                            $scope.DataActual = $scope.DataArchivo
                        }
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    } catch (e) {
                        ShowError('Error en la lectura del archivo por favor intente nuevamente')
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    }


                }, 1000)
            }

        }
        var conterroresprevalidar = 0

        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            if ($scope.DataArchivo.Valores != undefined) {
                $scope.DataArchivo = { Valores: $scope.DataArchivo.Valores }
                AsignarValoresTabla()
            }
            else {
                ShowError('El arhivo cargado no corresponde al formato de carge masivo del tarifario')
            }
            blockUI.stop()

        }

        $scope.handleFile = function (e) {
            var files = e.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = e.target.result;
                $scope.arr = fixdata(data);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(btoa($scope.arr), { type: 'base64' });
                        process_wb($scope.wb);
                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);
            });
        }

        function restrow(item) {
            item.st = {}
            item.ms = {}
        }

        $scope.ValidarDocumento = function () {
            $scope.ListaErrores = []
            $scope.MensajesError = []
            /*
                        blockUI.start();
                        $timeout(function() {
                            blockUI.message('Validando Archivo');
                        }, 1000);*/
            var contadorgeneral = 0
            var contadorRegistros = 0
            $scope.DataVerificada = []
            $scope.DataVerificadatmp = []
            for (var i = 0; i < $scope.DataArchivo.length; i++) {
                var item = $scope.DataArchivo[i]
                item.ID = i
                //resetea todos los estilos de la fila que se va a evaluar
                restrow(item);
                var errores = 0
                //Condicion para saber si aplican las validaciones (Si el registro no tiene una ruta seleccionada no validara el resto de campos)
                if (item.Omitido !== true) {
                    contadorRegistros++
                    if (ValidarCampo(item.Placa) == 1) {
                        item.st.Placa = stError; item.ms.Placa = ('Debe ingresar la placa');
                        errores++
                    }
                    if (ValidarCampo(item.Propietario) == 1) {
                        item.st.Propietario = stError; item.ms.Propietario = ('Debe ingresar un propietario');
                        errores++
                    } else if (ValidarCampo(item.Propietario, undefined, true) == 2) {
                        item.st.Propietario = stError; item.ms.Propietario = ('Debe ingresar un propietario valido');
                        errores++
                    }
                    if (ValidarCampo(item.Tenedor) == 1) {
                        item.st.Tenedor = stError; item.ms.Tenedor = ('Debe ingresar un tenedor');
                        errores++
                    } else if (ValidarCampo(item.Tenedor, undefined, true) == 2) {
                        item.st.Tenedor = stError; item.ms.Tenedor = ('Debe ingresar un tenedor valido');
                        errores++
                    }
                    if (ValidarCampo(item.Conductor) == 1) {
                        item.st.Conductor = stError; item.ms.Conductor = ('Debe ingresar un conductor');
                        errores++
                    } else if (ValidarCampo(item.Conductor, undefined, true) == 2) {
                        item.st.Conductor = stError; item.ms.Conductor = ('Debe ingresar un conductor valido');
                        errores++
                    }
                    //if (ValidarCampo(item.Afiliador) == 1) {
                    //    item.st.Afiliador = stError; item.ms.Afiliador = ('Debe ingresar un afiliador');
                    //    errores++
                    //} else if (ValidarCampo(item.Afiliador, undefined, true) == 2) {
                    //    item.st.Afiliador = stError; item.ms.Afiliador = ('Debe ingresar un afiliador valido');
                    //    errores++
                    //}
                    if (item.TipoDueno.Codigo == 2100)  /*No aplica*/ {
                        item.st.TipoDueno = stError; item.ms.TipoDueno = ('Debe seleccionar el tipo de dueño');
                        errores++
                    }
                    if (item.TipoVehiculo.Codigo == 2200)  /*No aplica*/ {
                        item.st.TipoVehiculo = stError; item.ms.TipoVehiculo = ('Debe seleccionar el tipo de vehículo');
                        errores++
                    }
                    if (ValidarCampo(item.TarjetaPropiedad) == 1) {
                        item.st.TarjetaPropiedad = stError; item.ms.TarjetaPropiedad = ('Debe ingresar la tarjeta de propiedad');
                        errores++
                    }
                    if (item.TipoCarroceria.Codigo == 2300)  /*No aplica*/ {
                        item.st.TipoCarroceria = stError; item.ms.TipoCarroceria = ('Debe seleccionar el tipo de carrocería');
                        errores++
                    }
                    if (ValidarCampo(item.NumeroMotor) == 1) {
                        item.st.NumeroMotor = stError; item.ms.NumeroMotor = ('Debe ingresar el número de motor');
                        errores++
                    }
                    if (ValidarCampo(item.Modelo) == 1) {
                        item.st.Modelo = stError; item.ms.Modelo = ('Debe ingresar el Modelo');
                        errores++
                    }
                    if (ValidarCampo(item.Chasis) == 1) {
                        item.st.Chasis = stError; item.ms.Chasis = ('Debe ingresar el número de serie/chasis');
                        errores++
                    }
                    if (ValidarCampo(item.Color) == 1) {
                        item.st.Color = stError; item.ms.Color = ('Debe ingresar un color');
                        errores++
                    } else if (ValidarCampo(item.Color, undefined, true) == 2) {
                        item.st.Color = stError; item.ms.Color = ('Debe ingresar un color valido');
                        errores++
                    }
                    if (ValidarCampo(item.Marca) == 1) {
                        item.st.Marca = stError; item.ms.Marca = ('Debe ingresar una marca');
                        errores++
                    } else if (ValidarCampo(item.Marca, undefined, true) == 2) {
                        item.st.Marca = stError; item.ms.Marca = ('Debe ingresar una marca valida');
                        errores++
                    }
                    if (ValidarCampo(item.Linea) == 1) {
                        item.st.Linea = stError; item.ms.Linea = ('Debe ingresar una línea');
                        errores++
                    } else if (ValidarCampo(item.Linea, undefined, true) == 2) {
                        item.st.Linea = stError; item.ms.Linea = ('Debe ingresar una línea valida');
                        errores++
                    }
                    if (ValidarCampo(item.PesoBruto) == 1) {
                        item.st.PesoBruto = stError; item.ms.PesoBruto = ('Debe ingresar el peso bruto');
                        errores++
                    }

                    if (ValidarCampo(item.Capacidad) == 1) {
                        item.st.Capacidad = stError; item.ms.Capacidad = ('Debe ingresar la capacidad');
                        errores++
                    }
                    if (ValidarCampo(item.NumeroEjes) == 1) {
                        item.st.NumeroEjes = stError; item.ms.NumeroEjes = ('Debe ingresar el número de ejes');
                        errores++
                    }
                    if (ValidarCampo(item.Cilindraje) == 1) {
                        item.st.Cilindraje = stError; item.ms.Cilindraje = ('Debe ingresar el clilindraje');
                        errores++
                    }
                    if (item.Estado.Codigo == 0)  /*Bloqueado*/ {
                        if (ValidarCampo(item.JustificacionBloqueo) == 1) {
                            item.st.JustificacionBloqueo = stError; item.ms.JustificacionBloqueo = ('Debe ingresar justificación del bloqueo');
                            errores++
                        }
                    }


                    if (errores > 0) {
                        item.st.Row = stwarning
                        $scope.ListaErrores.push(item)
                        contadorgeneral++
                    }
                    else {
                        item.stRow = ''
                        $scope.DataVerificada.push(item)
                    }
                }
                else {
                    restrow(item);
                }
            }
            //blockUI.stop();

            if (contadorgeneral > 0) {
                ShowError('Se encontraron inconsistencias en los datos ingresados, por favor corrija los datos marcados o seleccione omitirlos.');
                $scope.paginaActualError = 1
                $scope.DataErrorActual = []
                $scope.totalRegistrosErrores = $scope.ListaErrores.length
                $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 10);
                if ($scope.totalRegistrosErrores > 10) {
                    for (var i = 0; i < 10; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    for (var i = 0; i < $scope.ListaErrores.length; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                if (contadorRegistros == 0) {
                    ShowError('No se encontraron registros para validar, por favor revise que la casilla de omitir no se encuentre marcada en al menos un registro')
                } else {
                    ShowWarningConfirm('Los datos se verificaron correctamente.¿Desea continuar con el cargue de la información?', $scope.Guardar)
                }
            }


        }

        $scope.Guardar = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        $scope.GuardarErrados = function () {
            //closeModal('ModalErrores')
            data = []
            for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                if (!$scope.ProgramacionesNoGuardadas[i].Omitido && $scope.ProgramacionesNoGuardadas[i].Option != 'Omitido') {
                    if ($scope.ProgramacionesNoGuardadas[i].Option == 'Remplazado') {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 1
                    } else {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 0
                    }
                    data.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
            $scope.DataVerificada = data
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        var contGuard = 0
        var contGuardGeneral = 0
        var contGeneral = 0
        var Guardaros = 0
        function GuardarCompleto(inicio, opt) {
            contGuard++
            if (contGuard > $scope.DataVerificada.length) {

                ShowSuccess('Se guardaron ' + contGuardGeneral + ' de ' + contGeneral + ' vehículos')
                closeModal('modalConfirmacionRemplazarProgramacion');
                closeModal('ModalErrores');
                $scope.LimpiarData()

            } else {

                blockUI.start();
                $timeout(function () {
                    blockUI.message('Guardando vehículos');
                }, 1000);
                var Vehiculo = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Placa: $scope.DataVerificada[inicio].Placa,
                    CodigoAlterno: $scope.DataVerificada[inicio].CodigoAlterno,
                    FechaCrea: $scope.DataVerificada[inicio].FechaCrea,
                    FechaModifica: $scope.DataVerificada[inicio].FechaModifica,
                    Propietario: {
                        Codigo: $scope.DataVerificada[inicio].Propietario.Codigo
                    },
                    Tenedor: {
                        Codigo: $scope.DataVerificada[inicio].Tenedor.Codigo
                    },
                    Conductor: {
                        Codigo: $scope.DataVerificada[inicio].Conductor.Codigo
                    },
                    Color: $scope.DataVerificada[inicio].Color,
                    Marca: $scope.DataVerificada[inicio].Marca,
                    Linea: $scope.DataVerificada[inicio].Linea,
                    Modelo: $scope.DataVerificada[inicio].Modelo,
                    ModeloRepotenciado: $scope.DataVerificada[inicio].ModeloRepotenciado,
                    TipoVehiculo: $scope.DataVerificada[inicio].TipoVehiculo,
                    Cilindraje: $scope.DataVerificada[inicio].Cilindraje,
                    TipoCarroceria: $scope.DataVerificada[inicio].TipoCarroceria,
                    NumeroEjes: $scope.DataVerificada[inicio].NumeroEjes,
                    NumeroMotor: $scope.DataVerificada[inicio].NumeroMotor,
                    Chasis: $scope.DataVerificada[inicio].Chasis,
                    PesoBruto: $scope.DataVerificada[inicio].PesoBruto,
                    Kilometraje: $scope.DataVerificada[inicio].Kilometraje,
                    Estado: $scope.DataVerificada[inicio].Estado,
                    TipoDueno: $scope.DataVerificada[inicio].TipoDueno,
                    TarjetaPropiedad: $scope.DataVerificada[inicio].TarjetaPropiedad,
                    Capacidad: $scope.DataVerificada[inicio].Capacidad,
                    CapacidadGalones: $scope.DataVerificada[inicio].CapacidadGal,
                    PesoTara: $scope.DataVerificada[inicio].PesoTara,
                    ProveedorGPS: {
                        Codigo: $scope.DataVerificada[inicio].ProveedorGPS !== undefined && $scope.DataVerificada[inicio].ProveedorGPS !== '' ? $scope.DataVerificada[inicio].ProveedorGPS.Codigo : 0
                    },
                    TipoCombustible: {
                        Codigo: 22902
                    },
                    URLGPS: $scope.DataVerificada[inicio].URLGPS,
                    UsuarioGPS: $scope.DataVerificada[inicio].UsuarioGPS,
                    ClaveGPS: $scope.DataVerificada[inicio].ClaveGPS,
                    TelefonoGPS: $scope.DataVerificada[inicio].TelefonoGPS,
                    JustificacionBloqueo: $scope.DataVerificada[inicio].JustificacionBloqueo,
                    IdentificadorGPS: $scope.DataVerificada[inicio].IdentificadorGPS
                }
                VehiculosFactory.Guardar(Vehiculo).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        Guardaros++
                        contGuardGeneral++
                        GuardarCompleto(contGuard)
                        blockUI.stop();
                    }
                    else {
                        ShowError(response.statusText);
                        $scope.ProgramacionesNoGuardadas.push($scope.DataVerificada[inicio])
                        GuardarCompleto(contGuard)
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $scope.ProgramacionesNoGuardadas.push($scope.DataVerificada[inicio])
                    GuardarCompleto(contGuard)
                    blockUI.stop();
                })
            }
        }


        $scope.ModalConfirmacionNuevoProceso = function () {
            showModal('modalConfirmacionCancelarProceso');
        }

        $scope.CerrarModalNuevoProceso = function (e) {
            var fileVal = document.getElementById("fileModal");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
            closeModal('modalConfirmacionCancelarProceso');


        }

        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (o) {
            return MascaraNumero(o)
        }
        $scope.MaskValoresGrid = function (o) {
            return MascaraValores(o)
        }
        $scope.MaskMayusGrid = function (o) {
            return MascaraMayus(o)
        }
        $scope.MaskplacaGrid = function (o) {
            return MascaraPlaca(o)
        }
        $scope.MaskplacaSemiremolqueGrid = function (o) {
            return MascaraPlacaSemirremolque(o)
        }

        $scope.GenerarPlanilla = function () {
            var entidad = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
            blockUI.start();
            $timeout(function () {
                blockUI.message('Generando Plantilla..');
            }, 1000);
            VehiculosFactory.GenerarPlanitilla(entidad).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var pdfAsDataUri = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data.Datos.Planitlla;
                    var link = document.createElement('a');
                    link.href = pdfAsDataUri;
                    link.download = "Plantilla_Cargue_Masivo_Vehiculos.xlsx";
                    link.click();
                    //window.open(pdfAsDataUri);
                    blockUI.stop();
                    ShowSuccess('La plantilla se generó correctamente')
                }
            }, function (response) {
                blockUI.stop();
                ShowError(response.statusText)
            })

        }

        $scope.DataActual = []
        //$scope.DataActual.push({})
    }]);

