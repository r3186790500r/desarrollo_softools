﻿EncoExpresApp.controller("GestionarListaChequeoVehiculosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'SitiosCargueDescargueFactory', 'VehiculosFactory', 'SemirremolquesFactory', 'DocumentoInspeccionPreoperacionalFactory', 'InspeccionPreoperacionalFactory', 'FormularioInspeccionesFactory', 'TercerosFactory','SeguridadUsuariosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, SitiosCargueDescargueFactory, VehiculosFactory, SemirremolquesFactory, DocumentoInspeccionPreoperacionalFactory, InspeccionPreoperacionalFactory, FormularioInspeccionesFactory, TercerosFactory, SeguridadUsuariosFactory) {

        $scope.Titulo = 'GESTIONAR INSPECCIÓN';

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CHECK_VEHICULO_CONDUCTORES); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.MapaSitio = [{ Nombre: 'Gesphone' }, { Nombre: 'Inspecciones' }];

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoFormatosInspeccion = [];
        $scope.ListaPuntosGestion = [];
        $scope.ListaVehiculos = [];
        $scope.ListadoResponsables = [];
        $scope.ListaSitioCargueDescargue = [];
        $scope.CorreosClientes = [];
        $scope.CorreosTransportador = []
        $scope.CorreosConductor = []
        $scope.ListadoSecciones = [];
        $scope.ListaVehiculo = [];
        $scope.ListaSemirremolque = [];
        $scope.ListaConductor = [];
        $scope.ListaTransportador = [];
        $scope.ListaCliente = [];
        $scope.ListaAutoriza = [];
        $scope.ListaSitiosCargueDescargue = [];
        $scope.DeshabilitarActualizar = false;
        $scope.ValidarDatosDetalle = false;
        $scope.Deshabilitar = false;
        $scope.MostrarCanvasFirma = true;
        $scope.CodigoConductorUsuarioLogueado = '';
        $scope.list = {
            Fecha: new Date(),
            Fotos: [],
        };


        $scope.FechaActual = new Date();
        $scope.HoraActual = RetornarHoras($scope.FechaActual);
        $scope.FechaActual = RetornarFechaSinHora($scope.FechaActual)

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Numero: 0,
            CodigoAlterno: '',
            Nombre: '',
            FechaInspeccion: new Date(),
            FechaInicioHora: new Date(),
            FechaFinHora: new Date(),
            Estado: 0,
            HoraInicio: RetornarHoras($scope.FechaActual),
        }
        $scope.AutocompleteVehiculos = function (value) {
            var ListaPlacas = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1, Conductor: { Codigo: $scope.CodigoConductorUsuarioLogueado }, ValorAutocomplete: value, Sync: true }).Datos;
            return ListaPlacas;
        }
        var Listado_Vehiculos = $scope.AutocompleteVehiculos('');
        Listado_Vehiculos.forEach(vehiculo => {
            if (vehiculo.Conductor.Codigo == $scope.Sesion.UsuarioAutenticado.Conductor.Codigo) {
                
                $scope.Modelo.Vehiculo = $scope.CargarVehiculos(vehiculo.Codigo);              
            }
        });

        if ($scope.Modelo.Vehiculo != null || $scope.Modelo.Vehiculo != undefined) {
            $scope.Modelo.Semirremolque = $scope.CargarSemirremolqueCodigo($scope.Modelo.Vehiculo.Semirremolque.Codigo);
            $scope.CodigoSemirremolque = $scope.Modelo.Semirremolque.Codigo;
        }

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "BORRADOR" },
            { Codigo: 1, Nombre: "DEFINITIVO" },
        ]

        /* Obtener parametros*/
        if ($routeParams.Numero > 0) {
            $scope.Modelo.Numero = $routeParams.Numero;
            $scope.Titulo = 'CONSULTAR INSPECCIÓN PRE-OPERACIONAL';
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
            Obtener();
        } else {
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }
       
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar Autocomplete de Conductor*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_CONDUCTOR }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaConductor = response.data.Datos;
                        if ($scope.CodigoConductor != undefined && $scope.CodigoConductor != '' && $scope.CodigoConductor != 0 && $scope.CodigoConductor != null) {
                            if ($scope.CodigoConductor > 0) {
                               // $scope.Modelo.Conductor = $linq.Enumerable().From($scope.ListaConductor).First('$.Codigo ==' + $scope.CodigoConductor);
                            } else {
                               // $scope.Modelo.Conductor = $linq.Enumerable().From($scope.ListaConductor).First('$.Codigo == 0');
                            }
                        } else {
                            //$scope.Modelo.Conductor = $linq.Enumerable().From($scope.ListaConductor).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListaConductor = [];
                        $scope.ModeloConductor = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.Modelo.Conductor = $scope.CargarTercero($scope.Sesion.UsuarioAutenticado.Conductor.Codigo);
        /*Cargar Autocomplete de Transportador*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_EMPRESA_TRANSPORTADORA }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaTransportador = response.data.Datos;
                        if ($scope.CodigoTransportador != undefined && $scope.CodigoTransportador != '' && $scope.CodigoTransportador != 0 && $scope.CodigoTransportador != null) {
                            if ($scope.CodigoTransportador > 0) {
                                $scope.Modelo.Transportador = $linq.Enumerable().From($scope.ListaTransportador).First('$.Codigo ==' + $scope.CodigoTransportador);
                            } else {
                                $scope.Modelo.Transportador = $linq.Enumerable().From($scope.ListaTransportador).First('$.Codigo == 0');
                            }
                        } else {

                          //  $scope.Modelo.Transportador = $linq.Enumerable().From($scope.ListaTransportador).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListaTransportador = [];
                        $scope.Modelo.Transportador = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar Autocomplete de Cliente*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_CLIENTE }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaCliente = response.data.Datos;
                        if ($scope.CodigoCliente != undefined && $scope.CodigoCliente != '' && $scope.CodigoCliente != 0 && $scope.CodigoCliente != null) {
                            if ($scope.CodigoCliente > 0) {
                                $scope.Modelo.Cliente = $linq.Enumerable().From($scope.ListaCliente).First('$.Codigo ==' + $scope.CodigoCliente);
                            } else {
                                $scope.Modelo.Cliente = $linq.Enumerable().From($scope.ListaCliente).First('$.Codigo == 0');
                            }
                        } else {
                           // $scope.Modelo.Cliente = $linq.Enumerable().From($scope.ListaCliente).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListaCliente = [];
                        $scope.Modelo.Cliente = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar Autocomplete de Autoriza*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_EMPLEADO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaAutoriza = response.data.Datos;
                        if ($scope.CodigoAutoriza != undefined && $scope.CodigoAutoriza != '' && $scope.CodigoAutoriza != 0 && $scope.CodigoAutoriza != null) {
                            if ($scope.CodigoAutoriza > 0) {
                                $scope.Modelo.Autoriza = $linq.Enumerable().From($scope.ListaAutoriza).First('$.Codigo ==' + $scope.CodigoAutoriza);
                            } else {
                                $scope.Modelo.Autoriza = $linq.Enumerable().From($scope.ListaAutoriza).First('$.Codigo == 0');
                            }
                        } else {
                           // $scope.Modelo.Autoriza = $linq.Enumerable().From($scope.ListaAutoriza).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListaAutoriza = [];
                        $scope.Modelo.Autoriza = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar Autocomplete de Responsable*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_EMPLEADO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoResponsables = []
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                        $scope.ListadoResponsables = response.data.Datos;
                        if ($scope.CodigoResponsable !== undefined && $scope.CodigoResponsable !== '' && $scope.CodigoResponsable !== null && $scope.CodigoResponsable !== 0) {
                            $scope.Modelo.Responsable = $linq.Enumerable().From($scope.ListadoResponsables).First('$.Codigo ==' + $scope.CodigoResponsable);
                        }
                        else {
                            try {
                                $scope.Modelo.Responsable = $linq.Enumerable().From($scope.ListadoResponsables).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Empleado.Codigo);
                            } catch (e) {
                            }
                        }
                    }
                    else {
                        $scope.ListadoResponsables = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        // Sitios Cargue Descargue
        SitiosCargueDescargueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListaSitiosCargueDescargue = response.data.Datos;
                        if ($scope.CodigoSitioCargue != undefined && $scope.CodigoSitioCargue != '' && $scope.CodigoSitioCargue != 0 && $scope.CodigoSitioCargue != null) {
                            if ($scope.CodigoSitioCargue > 0) {
                                $scope.Modelo.SitioCargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==' + $scope.CodigoSitioCargue);
                            } else {
                                $scope.Modelo.SitioCargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.Modelo.SitioCargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==0');
                        }
                        if ($scope.CodigoSitioDescargue != undefined && $scope.CodigoSitioDescargue != '' && $scope.CodigoSitioDescargue != 0 && $scope.CodigoSitioDescargue != null) {
                            if ($scope.CodigoSitioDescargue > 0) {
                                $scope.Modelo.SitioDescargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==' + $scope.CodigoSitioDescargue);
                            } else {
                                $scope.Modelo.SitioDescargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.Modelo.SitioDescargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo ==0');
                        }
                    }
                    else {
                        $scope.SitiosCargueDescargue = [];
                        $scope.Modelo.SitioCargue = null;
                        $scope.Modelo.SitioDescargue = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        // Vehiculo
     

        SeguridadUsuariosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    $scope.CodigoConductorUsuarioLogueado = response.data.Datos.Conductor.Codigo;
                }
            });


        

        // Semirremolques
        SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Placa: '', Codigo: 0 })
                        $scope.ListaSemirremolque = response.data.Datos;
                        if ($scope.CodigoSemirremolque != undefined && $scope.CodigoSemirremolque != '' && $scope.CodigoSemirremolque != 0 && $scope.CodigoSemirremolque != null) {
                            if ($scope.CodigoSemirremolque > 0) {
                                $scope.ModeloSemirremolque = $linq.Enumerable().From($scope.ListaSemirremolque).First('$.Codigo ==' + $scope.CodigoSemirremolque);
                            } else {
                                $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListaSemirremolque).First('$.Codigo == 0');
                            }
                        } else {
                            $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListaSemirremolque).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListaSemirremolque = [];
                        $scope.Modelo.Semirremolque = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //Cargar semirremolque de la placa vehiculo
        $scope.CargarSemirremolque = function (modelovehiculo) {
            // Vehiculo
            VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: modelovehiculo.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.CodigoRemolqueVehiculo = response.data.Datos.Semirremolque.Codigo;                        
                        $scope.Modelo.Semirremolque = $scope.CargarSemirremolqueCodigo(response.data.Datos.Semirremolque.Codigo);                        
                        $scope.CodigoConductorVehiculo = response.data.Datos.Conductor.Codigo;
                        $scope.Modelo.Conductor = $linq.Enumerable().From($scope.ListaConductor).First('$.Codigo == ' + $scope.CodigoConductorVehiculo);
                    } else {
                        $scope.ListaSemirremolque = [];
                        $scope.Modelo.Semirremolque = null;
                        $scope.ListaConductor = [];
                        $scope.Modelo.Conductor = null;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/



        var ListaFormatoInspeccion = FormularioInspeccionesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos;
        if (ListaFormatoInspeccion.length > 0) {
            $scope.ListadoFormatosInspeccion = [];
            ListaFormatoInspeccion.push({ Nombre: '', Codigo: 0 })
            $scope.ListadoFormatosInspeccion = ListaFormatoInspeccion;

            if ($scope.CodigoFormato !== undefined && $scope.CodigoFormato !== '' && $scope.CodigoFormato !== null) {
                $scope.Modelo.FormatoInspeccion = $linq.Enumerable().From($scope.ListadoFormatosInspeccion).First('$.Codigo ==' + $scope.CodigoFormato);
            } else {
                $scope.Modelo.FormatoInspeccion = $scope.ListadoFormatosInspeccion[$scope.ListadoFormatosInspeccion.length - 1]
            }
        }

        $scope.ListTitem = function (item) {
            if (item.items) {
                item.items = false
            } else {
                item.items = true
            }
        }

        $scope.ConsultarFormatoInspeccion = function (CodigoFormato) {
            if (CodigoFormato !== undefined && CodigoFormato !== null && CodigoFormato !== '') {
                FormularioInspeccionesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CodigoFormato, ConsularDetalle: 1 }).
                    then(function (response) {
                        $scope.ListadoSecciones = [];
                        if (response.data.ProcesoExitoso === true) {
                            $scope.Modelo.TiempoVigencia = response.data.Datos.TiempoVigencia
                            $scope.HabilitarFirma = response.data.Datos.Firma == 1 ? true : false;
                            response.data.Datos.SeccionFormulario.forEach(function (item) {
                                if (item.EstadoDocumento.Codigo == 1) {

                                    item.items = true,
                                        item.Relevante = 0;
                                    item.Cumple = 0;
                                    item.ListadoItemSeccion = [];
                                    response.data.Datos.itemFormulario.forEach(function (itemSeccion) {
                                        if (itemSeccion.EstadoDocumento.Codigo == 1) {
                                            if (itemSeccion.TipoControl.Codigo == CODIGO_TIPO_CONTROL_CHECK) {
                                                itemSeccion.Valor = true
                                            }
                                            if (itemSeccion.Seccion == item.Codigo) {
                                                if (itemSeccion.Relevante == 1) {
                                                    itemSeccion.Relevante = true;
                                                }
                                                else {
                                                    itemSeccion.Relevante = false;
                                                }
                                                itemSeccion.Cumple = true;
                                                item.ListadoItemSeccion.push(itemSeccion)
                                            }
                                        }
                                    })
                                    $scope.ListadoSecciones.push(item)
                                }
                            })
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                $scope.HabilitarFirma = false;
            }
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando inspección No. ' + $scope.Modelo.Numero);

            $timeout(function () {
                blockUI.message('Cargando inspección No.' + $scope.Modelo.Numero);
            }, 1000);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                ConsularDetalle: 1,
            };

            blockUI.delay = 1000;
            InspeccionPreoperacionalFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.DeshabilitarActualizar = true;

                        $scope.Modelo = JSON.parse(JSON.stringify(response.data.Datos))
                        $scope.Modelo.Conductor = $scope.CargarTercero(response.data.Datos.Conductor.Codigo);


                        $scope.Modelo.FechaInspeccion = new Date(response.data.Datos.FechaInspeccion);

                        if (response.data.Datos.FechaAutoriza == '1900-01-01T00:00:00') {
                            $scope.Modelo.FechaAutoriza = new Date();
                        } else {
                            $scope.Modelo.FechaAutoriza = new Date(response.data.Datos.FechaAutoriza);
                        }

                        var FechaMinima = RetornaFechaSinHoras(FORMATO_FECHA_MINIMA)

                        /*valida el formato de fecha en dd/mm/yyyy*/
                        var fechaInicio = new Date(response.data.Datos.FechaInicioHora);
                        fechaInicio.setHours(0);
                        fechaInicio.setMinutes(0);
                        fechaInicio.setSeconds(0);
                        fechaInicio.setMilliseconds(0);

                        $scope.Modelo.FechaInicioHora = fechaInicio;

                        /*valida el formato de fecha en dd/mm/yyyy*/
                        var fechaFin = new Date(response.data.Datos.FechaFinHora);
                        fechaFin.setHours(0);
                        fechaFin.setMinutes(0);
                        fechaFin.setSeconds(0);
                        fechaFin.setMilliseconds(0);

                        $scope.Modelo.FechaFinHora = fechaFin;

                        $scope.Modelo.FechaInicioHora = Formatear_Fecha($scope.Modelo.FechaInicioHora, FORMATO_FECHA_s)
                        $scope.Modelo.FechaFinHora = Formatear_Fecha($scope.Modelo.FechaFinHora, FORMATO_FECHA_s)
                        FechaMinima = Formatear_Fecha(FechaMinima, FORMATO_FECHA_s)

                        if (new Date(response.data.Datos.FechaInicioHora) < MIN_DATE) {
                            $scope.Modelo.FechaInicioHora = new Date()
                            $scope.Modelo.HoraInicio = RetornarHoras($scope.Modelo.FechaInicioHora)
                        }
                        else {
                            $scope.Modelo.FechaInicioHora = fechaInicio
                            $scope.Modelo.HoraInicio = RetornarHoras(response.data.Datos.FechaInicioHora);
                        }
                        if (new Date(response.data.Datos.FechaFinHora) < MIN_DATE) {
                            $scope.Modelo.FechaFinHora = new Date()
                            $scope.Modelo.HoraFin = '00:00'
                        }
                        else {
                            $scope.Modelo.FechaFinHora = fechaFin
                            $scope.Modelo.HoraFin = RetornarHoras(response.data.Datos.FechaFinHora);
                        }
                        $scope.ListadoSecciones = [];

                        try {
                            if (response.data.Datos.Detalle.length > 0) {
                                response.data.Datos.Detalle.forEach(function (item) {
                                    if (item.SeccionFormulario > 0) {
                                        if ($scope.ListadoSecciones.length == 0) {
                                            $scope.ListadoSecciones.push({
                                                CodigoEmpresa: item.CodigoEmpresa,
                                                Codigo: item.SeccionFormulario,
                                                Nombre: item.SeccionFormularioNombre,
                                                Orden: item.OrdenSeccion,
                                                Relevante: 0,
                                                Cumple: 0,
                                                ListadoItemSeccion: [],
                                                CodigoDetalle: item.Codigo,
                                            });
                                        } else {
                                            var count = 0;
                                            $scope.ListadoSecciones.forEach(seccion => {
                                                if (seccion != undefined && seccion.Codigo == item.SeccionFormulario) {
                                                    count++;
                                                }
                                            });
                                                if (count == 0) {
                                                    $scope.ListadoSecciones.push({
                                                        CodigoEmpresa: item.CodigoEmpresa,
                                                        Codigo: item.SeccionFormulario,
                                                        Nombre: item.SeccionFormularioNombre,
                                                        Orden: item.OrdenSeccion,
                                                        Relevante: 0,
                                                        Cumple: 0,
                                                        ListadoItemSeccion: [],
                                                        CodigoDetalle: item.Codigo,
                                                    });
                                                } 
                                            
                                        }

                                    }
                                
                                });
                            
                                response.data.Datos.Detalle.forEach(function (item) {
                                    $scope.ListadoSecciones.forEach(function (itemSeccion) {
                                        if (itemSeccion.Codigo == item.SeccionFormulario && item.ItemFormulario > 0) {
                                            if (item.TipoControl == CODIGO_TIPO_CONTROL_DATE) {
                                                item.Valor = new Date(item.Valor)
                                                item.Valor = RetornaFechaSinHoras(item.Valor)
                                            }
                                            else if (item.TipoControl == CODIGO_TIPO_CONTROL_NUMERO) {
                                                item.Valor = Math.ceil(item.Valor)
                                            }
                                            else if (item.TipoControl == CODIGO_TIPO_CONTROL_CHECK) {
                                                if (item.Valor == 1) {
                                                    item.Valor = true
                                                    
                                                    $("#" + item.Nombre).prop("checked", true);
                                                }
                                                else {
                                                    item.Valor = false
                                                   
                                                }
                                            }
                                            if (item.Relevante == 1) {
                                                item.Relevante = true
                                            }
                                            else {
                                                item.Relevante = false
                                            }
                                            if (item.Cumple == 1) {
                                                item.Cumple = true
                                            }
                                            else {
                                                item.Cumple = false
                                            }
                                            itemSeccion.ListadoItemSeccion.push({
                                                CodigoEmpresa: item.CodigoEmpresa,
                                                Codigo: item.ItemFormulario,
                                                Nombre: item.Nombre,
                                                Orden: item.OrdenItem,
                                                TipoControl: { Codigo: item.TipoControl },
                                                Valor: item.Valor,
                                                Relevante: item.Relevante,
                                                Cumple: item.Cumple,
                                                Tamano: item.Tamano,
                                                CodigoDetalle: item.Codigo,
                                            })
                                        }
                                    });
                                });
                            } else {
                                $scope.ConsultarFormatoInspeccion($scope.Modelo.FormatoInspeccion.Codigo)
                            }
                        } catch (e) {
                            $scope.ConsultarFormatoInspeccion($scope.Modelo.FormatoInspeccion.Codigo)
                        }

                        $scope.CodigoFormato = response.data.Datos.FormatoInspeccion.Codigo
                        if ($scope.ListadoFormatosInspeccion.length > 0 && $scope.CodigoFormato > 0) {
                            $scope.Modelo.FormatoInspeccion = $linq.Enumerable().From($scope.ListadoFormatosInspeccion).First('$.Codigo == ' + response.data.Datos.FormatoInspeccion.Codigo);
                            $scope.HabilitarFirma = $scope.Modelo.FormatoInspeccion.Firma == 1 ? true : false;
                        }

                        if ($scope.HabilitarFirma == true && response.data.Datos.Firma != null) {
                            var image = new Image();
                            var srcImage = "data:image/png;base64," + response.data.Datos.Firma;
                            image.src = srcImage;
                            $scope.ImgFirma = srcImage;
                            var img = cargaContextoCanvas('Firma');
                            img.drawImage(image, 0, 0);
                        }

                        $scope.CodigoConductor = response.data.Datos.Conductor.Codigo;
                        if ($scope.ListaConductor !== null && $scope.ListaConductor !== undefined) {
                            if ($scope.ListaConductor.length > 0) {
                                $scope.Modelo.Conductor = $linq.Enumerable().From($scope.ListaConductor).First('$.Codigo == ' + response.data.Datos.Conductor.Codigo);
                            }
                        };

                        $scope.CodigoVehiculo = response.data.Datos.Vehiculo.Codigo;
                        if ($scope.ListaVehiculo !== null && $scope.ListaVehiculo !== undefined) {
                            if ($scope.ListaVehiculo.length > 0) {
                                $scope.Modelo.Vehiculo = $linq.Enumerable().From($scope.ListaVehiculo).First('$.Codigo == ' + response.data.Datos.Vehiculo.Codigo);
                            }
                        };

                        $scope.CodigoSemirremolque = response.data.Datos.Semirremolque.Codigo;
                        if ($scope.ListaSemirremolque !== null && $scope.ListaSemirremolque !== undefined) {
                            if ($scope.ListaSemirremolque.length > 0) {
                                $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListaSemirremolque).First('$.Codigo == ' + response.data.Datos.Semirremolque.Codigo);
                            }
                        };

                        $scope.CodigoTransportador = response.data.Datos.Transportador.Codigo;
                        if ($scope.ListaTransportador !== null && $scope.ListaTransportador !== undefined) {
                            if ($scope.ListaTransportador.length > 0) {
                               // $scope.Modelo.Transportador = $linq.Enumerable().From($scope.ListaTransportador).First('$.Codigo == ' + response.data.Datos.Transportador.Codigo);
                                if (response.data.Datos.Transportador.Codigo != undefined || response.data.Datos.Transportador.Codigo != null) {
                                    $scope.Modelo.Transportador = $scope.CargarTercero(response.data.Datos.Transportador.Codigo);
                                }
                            }
                        };

                        $scope.CodigoCliente = response.data.Datos.Cliente.Codigo;
                        if ($scope.ListaCliente !== null && $scope.ListaCliente !== undefined) {
                            if ($scope.ListaCliente.length > 0) {
                                $scope.Modelo.Cliente = $linq.Enumerable().From($scope.ListaCliente).First('$.Codigo == ' + response.data.Datos.Cliente.Codigo);
                            }
                        };

                        $scope.CodigoSitioCargue = response.data.Datos.SitioCargue.Codigo;
                        if ($scope.CodigoSitioCargue !== null && $scope.CodigoSitioCargue !== undefined && $scope.CodigoSitioCargue !== 0) {
                            if ($scope.ListaSitiosCargueDescargue.length > 0) {
                                $scope.Modelo.SitioCargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo == ' + response.data.Datos.SitioCargue.Codigo);
                            }
                        };

                        $scope.CodigoSitioDescargue = response.data.Datos.SitioDescargue.Codigo;
                        if ($scope.CodigoSitioDescargue !== null && $scope.CodigoSitioDescargue !== undefined && $scope.CodigoSitioDescargue !== 0) {
                            if ($scope.ListaSitiosCargueDescargue.length > 0) {
                                $scope.Modelo.SitioDescargue = $linq.Enumerable().From($scope.ListaSitiosCargueDescargue).First('$.Codigo == ' + response.data.Datos.SitioDescargue.Codigo);
                            }
                        };

                        $scope.CodigoAutoriza = response.data.Datos.Autoriza.Codigo;
                        if ($scope.ListaAutoriza !== null && $scope.ListaAutoriza !== undefined) {
                            if ($scope.ListaAutoriza.length > 0) {
                                $scope.Modelo.Autoriza = $linq.Enumerable().From($scope.ListaAutoriza).First('$.Codigo == ' + response.data.Datos.Autoriza.Codigo);
                            }
                        };

                        $scope.CodigoResponsable = response.data.Datos.Responsable.Codigo
                        if ($scope.CodigoResponsable == 0) {
                            $scope.CodigoResponsable = $scope.Sesion.UsuarioAutenticado.Empleado.Codigo;
                        }
                        if ($scope.ListadoResponsables.length > 1 && $scope.CodigoResponsable > 0) {
                            $scope.Modelo.Responsable = $linq.Enumerable().From($scope.ListadoResponsables).First('$.Codigo == ' + response.data.Datos.Responsable.Codigo);
                        }

                        if (response.data.Datos.Estado == 0) {
                            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        } else if (response.data.Datos.Estado == 1) {
                            $scope.Deshabilitar = true;
                            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        } else {
                            if (response.data.Datos.Anulado == 1) {
                                $scope.MostrarCanvasFirma = false;
                                $scope.Deshabilitar = true;
                                $scope.ListadoEstados.push({ Nombre: "ANULADO", Codigo: -1 })
                                $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
                            } else {
                                $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                                if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                    $scope.MostrarCanvasFirma = false;
                                    $scope.Deshabilitar = true;
                                }
                                else {
                                    $scope.MostrarCanvasFirma = true;
                                    $scope.Deshabilitar = false;
                                }
                            }
                        }
                    }
                    else {
                        ShowError('No se logro consultar la inspección No. ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarInspeccionesVehiculos';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                        document.location.href = '#!ConsultarInspeccionesVehiculos';
                });

            blockUI.stop();
       
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            $scope.ValidarDatosDetalle = false

   

            if (DatosRequeridos()) {
                if ($scope.ValidarDatosDetalle == true) {
                    showModal('modalConfirmacionDatosDetalle');
                }
                else {
                    showModal('modalConfirmacionGuardar');
                }
            }
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            closeModal('modalConfirmacionDatosDetalle');
            $scope.ListaDetalle = [];

            $scope.ListadoAuxiSecciones = [];

            $scope.ListadoAuxiSecciones = JSON.parse(JSON.stringify($scope.ListadoSecciones))

            $scope.ListadoSecciones.forEach(function (item) {
                $scope.ListaDetalle.push({
                    CodigoEmpresa: item.CodigoEmpresa,
                    SeccionFormulario: item.Codigo,
                    ItemFormulario: 0,
                    Nombre: item.Nombre,
                    OrdenSeccion: item.Orden,
                    OrdenItem: 0,
                    TipoControl: CODIGO_TIPO_CONTROL_NO_APLICA,
                    Valor: '',
                    Relevante: 0,
                    Cumple: 0,
                    Codigo: item.CodigoDetalle
                })

                item.ListadoItemSeccion.forEach(function (itemDetalle) {
                    if (itemDetalle.TipoControl.Codigo == CODIGO_TIPO_CONTROL_CHECK) {
                        if (itemDetalle.Valor == true) {
                            itemDetalle.Valor = 1
                        } else {
                            itemDetalle.Valor = 0
                        }
                    }
                    if (itemDetalle.Relevante == true) {
                        itemDetalle.Relevante = 1
                    } else {
                        itemDetalle.Relevante = 0
                    }
                    if (itemDetalle.Cumple == true) {
                        itemDetalle.Cumple = 1
                    } else {
                        itemDetalle.Cumple = 0
                    }

                    $scope.ListaDetalle.push({
                        CodigoEmpresa: itemDetalle.CodigoEmpresa,
                        Codigo: itemDetalle.CodigoDetalle,
                        SeccionFormulario: item.Codigo,
                        ItemFormulario: itemDetalle.Codigo,
                        Nombre: itemDetalle.Nombre,
                        OrdenSeccion: item.Orden,
                        OrdenItem: itemDetalle.Orden,
                        TipoControl: itemDetalle.TipoControl.Codigo,
                        Valor: itemDetalle.Valor,
                        Relevante: itemDetalle.Relevante,
                        Cumple: itemDetalle.Cumple,
                        Tamano: itemDetalle.Tamano,
                    })
                })

            })
            $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_INSPECCIONES

            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
            $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }

            $scope.Modelo.FechaInicioHora = RetornarHoraFecha($scope.Modelo.FechaInicioHora, $scope.Modelo.HoraInicio)
            $scope.Modelo.FechaFinHora = RetornarHoraFecha($scope.Modelo.FechaFinHora, $scope.Modelo.HoraFin)
            //$scope.Modelo.FechaInspeccion = RetornarFechaEspecificaSinTimeZone($scope.Modelo.FechaInspeccion)

            //$scope.Modelo.FechaAutoriza = RetornarFechaEspecificaSinTimeZone($scope.Modelo.FechaAutoriza)

            $scope.Modelo.Detalle = $scope.ListaDetalle
            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            if ($scope.HabilitarFirma == true) {
                $scope.Modelo.Firma = image.src.replace(/data:image\/png;base64,/g, '');
            }
            else {
                $scope.Modelo.Firma = undefined;
            }
   

            InspeccionPreoperacionalFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Numero === 0) {
                                ShowSuccess('Se guardó la inspección No. ' + response.data.Datos);
                                location.href = '#!ConsultarInspeccionesVehiculos/' + response.data.Datos;
                            } else {
                                ShowSuccess('Se modificó la inspección No. ' + $scope.Modelo.Numero);
                                location.href = '#!ConsultarInspeccionesVehiculos/' + $scope.Modelo.Numero;
                            }
                        }
                        else {
                            ShowError(response.statusText);
                            $scope.list.Fotos = $scope.ListadoAuxiliarFotos
                            $scope.ListadoSecciones = JSON.parse(JSON.stringify($scope.ListadoAuxiSecciones))
                        }
                    }
                    else {
                        ShowError(response.statusText);
                        $scope.list.Fotos = $scope.ListadoAuxiliarFotos
                        $scope.ListadoSecciones = JSON.parse(JSON.stringify($scope.ListadoAuxiSecciones))
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $scope.ListadoSecciones = JSON.parse(JSON.stringify($scope.ListadoAuxiSecciones))
                    $scope.list.Fotos = $scope.ListadoAuxiliarFotos
                });
        };


        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            $scope.ValidarDatosDetalle = false
            if ($scope.Modelo.CodigoAlterno == undefined || $scope.Modelo.CodigoAlterno == null) {
                $scope.Modelo.CodigoAlterno = ''
            }
            if ($scope.Modelo.FormatoInspeccion == undefined || $scope.Modelo.FormatoInspeccion == '' || $scope.Modelo.FormatoInspeccion == null || $scope.Modelo.FormatoInspeccion.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar el formato inspección');
                continuar = false;
            }
            if ($scope.Modelo.FechaInspeccion == undefined || $scope.Modelo.FechaInspeccion == '' || $scope.Modelo.FechaInspeccion == null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }
            else {
                var FechaInspeccion = RetornarFechaSinHora($scope.Modelo.FechaInspeccion)
                if (FechaInspeccion < $scope.FechaActual) {
                    $scope.MensajesError.push('La fecha debe ser igual o superior a la fecha actual ');
                    continuar = false;
                }
            }
            if ($scope.Modelo.TiempoVigencia == undefined || $scope.Modelo.TiempoVigencia == '' || $scope.Modelo.TiempoVigencia == null) {
                $scope.MensajesError.push('Debe ingresar el tiempo vigencia');
                continuar = false;
            }
            if ($scope.Modelo.Vehiculo == undefined || $scope.Modelo.Vehiculo == '' || $scope.Modelo.Vehiculo == null || $scope.Modelo.Vehiculo.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar placa');
                continuar = false;
            }
            if ($scope.Modelo.Responsable == undefined || $scope.Modelo.Responsable == '' || $scope.Modelo.Responsable == null || $scope.Modelo.Responsable.Codigo == 0) {
                $scope.MensajesError.push('Debe  ingresar un responsable');
                continuar = false;
            }
            if ($scope.Modelo.FechaInicioHora == undefined || $scope.Modelo.FechaInicioHora == '' || $scope.Modelo.FechaInicioHora == null) {
                $scope.MensajesError.push('Debe ingresar la fecha inicio');
                continuar = false;
            }
            if ($scope.Modelo.HoraInicio == undefined || $scope.Modelo.HoraInicio == '' || $scope.Modelo.HoraInicio == null) {
                $scope.MensajesError.push('Debe ingresar la hora inicio');
                continuar = false;
            }
            if ($scope.Modelo.HoraFin == undefined || $scope.Modelo.HoraFin == '' || $scope.Modelo.HoraFin == null) {
                $scope.MensajesError.push('Debe ingresar la hora fin');
                continuar = false;
            }
            if ($scope.Modelo.HoraInicio !== undefined && $scope.Modelo.HoraInicio !== "" && $scope.Modelo.HoraInicio !== null) {
                var HoraInicio = $scope.Modelo.HoraInicio;
                try {
                    var arrHora = HoraInicio.split(":");
                    var hora = arrHora[0];
                    var minutos = arrHora[1];
                    if (parseInt(hora) < 0 || parseInt(hora) > 23) {
                        $scope.MensajesError.push('La hora inicio  no tiene el formato correcto');
                        continuar = false;
                    }
                    if (continuar == true) {
                        if (parseInt(minutos) < 0 || parseInt(minutos) > 59) {
                            $scope.MensajesError.push('La hora inicio no tiene el formato correcto');
                            continuar = false;
                        }
                    }
                }
                catch (err) {
                    if (continuar == false) {
                        $scope.MensajesError.push('La hora inicio no tiene el formato correcto');
                        continuar = false;
                    }
                }
            }
            if ($scope.Modelo.HoraFin !== undefined && $scope.Modelo.HoraFin !== "" && $scope.Modelo.HoraFin !== null) {
                var HoraFin = $scope.Modelo.HoraFin;
                try {
                    var arrHora = HoraFin.split(":");
                    var hora = arrHora[0];
                    var minutos = arrHora[1];
                    if (parseInt(hora) < 0 || parseInt(hora) > 23) {
                        $scope.MensajesError.push('La hora fin  no tiene el formato correcto');
                        continuar = false;
                    }
                    if (continuar == true) {
                        if (parseInt(minutos) < 0 || parseInt(minutos) > 59) {
                            $scope.MensajesError.push('La hora fin no tiene el formato correcto');
                            continuar = false;
                        }
                    }
                }
                catch (err) {
                    if (continuar == false) {
                        $scope.MensajesError.push('La hora fin no tiene el formato correcto');
                        continuar = false;
                    }
                }
            }
            if ($scope.ListadoSecciones.length > 0) {
                $scope.ListadoSecciones.forEach(function (item) {
                    item.ListadoItemSeccion.forEach(function (itemseccion) {
                        if (itemseccion.TipoControl.Codigo !== CODIGO_TIPO_CONTROL_CHECK) {
                            if (itemseccion.Valor == undefined || itemseccion.Valor == null || itemseccion.Valor == '') {
                                $scope.MensajesError.push('Debe ingresar la información del item ' + item.Nombre + ' en el campo ' + itemseccion.Nombre);
                                continuar = false;
                            }
                        }
                        else {
                            if (itemseccion.Valor == false) {
                                itemseccion.Cumple = false
                            }
                        }
                    })
                })
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar;
        }

        $scope.MarcarCumple = function (detalle) {
            $scope.ListadoSecciones.forEach(function (item) {
                item.ListadoItemSeccion.forEach(function (itemseccion) {
                    if (detalle.$$hashKey == itemseccion.$$hashKey) {
                        if (detalle.Valor == true) {
                            itemseccion.Cumple = true
                        }
                    }
                })
            })
        }

        $scope.LimpiarFirma = function () {
            try {
                var canvas = document.getElementById('Firma');
                var context = canvas.getContext('2d');
                context.clearRect(0, 0, canvas.width, canvas.height);
            } catch (e) {

            }
        };


        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarInspeccionesVehiculos/' + $scope.Modelo.Numero;
        };

    

        $scope.MaskPlaca = function () {
            try { $scope.Modelo.Vehiculo = MascaraPlaca($scope.Modelo.Vehiculo) } finally { }
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.FormatoInspeccion = $scope.Modelo.FormatoInspeccion.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.TiempoVigencia = MascaraNumero($scope.Modelo.TiempoVigencia) } catch (e) { }
        };
        $scope.MaskHora = function () {
            try { $scope.Modelo.HoraInicio = MascaraHora($scope.Modelo.HoraInicio) } catch (e) { }
            try { $scope.Modelo.HoraFin = MascaraHora($scope.Modelo.HoraFin) } catch (e) { }
        };
        $scope.MaskHoraGrid = function (value) {
            if (value != undefined) {
                return MascaraHora(value)
            }
        };
        $scope.MaskNumeroGird = function (value) {
            return MascaraNumero(value)
        }

    }]);