﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos


Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="EncabezadoCheckListCargueDescargues"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoCheckListCargueDescargues
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoCheckListCargueDescargues"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoCheckListCargueDescargues"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            Fecha = Read(lector, "Fecha")
            FechaLlegadaCargue = Read(lector, "Fecha_Hora_Llegada_Cargue")
            FechaInicioCargue = Read(lector, "Fecha_Hora_Inicio_Cargue")
            FechaFinCargue = Read(lector, "Fecha_Hora_Fin_Cargue")
            FechaLlegadaDescargue = Read(lector, "Fecha_Hora_Llegada_Descargue")
            FechaInicioDescargue = Read(lector, "Fecha_Hora_Inicio_Descargue")
            FechaFinDescargue = Read(lector, "Fecha_Hora_Fin_Descargue")
            NumeroOperarios = Read(lector, "Numero_Operarios")
            SitioCargueDescargue = Read(lector, "Sitio_Cargue_Descargue")
            Observaciones = Read(lector, "Observaciones")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            NumeroCargue = Read(lector, "ECCD_Numero_Cargue")
            FechaVigencia = Read(lector, "Fecha_Vigencia")
            FechaProgramacion = Read(lector, "Fecha_Programacion")
            TipoDocumentoSoporte = Read(lector, "TIDO_Documento_Soporte")
            NumeroDocumentoSoporte = Read(lector, "Numero_Documento_Soporte")
            FechaSolicitudOperacion = Read(lector, "Fecha_Solicitud_Operacion")
            FechaProgramacionCargue = Read(lector, "Fecha_Programa_Cargue")
            ContactoCliente = Read(lector, "Contacto_Cliente")
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente")}
            CiudadCargue = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Cargue")}
            Responsable = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Responsable")}
            Firma = Read(lector, "Firma")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
                Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "PlacaVehiculo")}
                Semiremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "PlacaSemiremolque")}
                Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .NombreCompleto = Read(lector, "NombreConductor")}

            Else
                Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}
                Semiremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo")}
                Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor")}
            End If
        End Sub
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Semiremolque As Semirremolques
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property FechaLlegadaCargue As DateTime
        <JsonProperty>
        Public Property FechaInicioCargue As DateTime
        <JsonProperty>
        Public Property FechaFinCargue As DateTime
        <JsonProperty>
        Public Property FechaLlegadaDescargue As DateTime
        <JsonProperty>
        Public Property FechaInicioDescargue As DateTime
        <JsonProperty>
        Public Property FechaFinDescargue As DateTime
        <JsonProperty>
        Public Property NumeroOperarios As Integer
        <JsonProperty>
        Public Property SitioCargueDescargue As String
        <JsonProperty>
        Public Property Responsable As Terceros
        <JsonProperty>
        Public Property NumeroCargue As Integer
        <JsonProperty>
        Public Property FechaVigencia As DateTime
        <JsonProperty>
        Public Property FechaProgramacion As DateTime
        <JsonProperty>
        Public Property TipoDocumentoSoporte As Integer
        <JsonProperty>
        Public Property NumeroDocumentoSoporte As Integer
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property FechaSolicitudOperacion As DateTime
        <JsonProperty>
        Public Property CiudadCargue As Ciudades
        <JsonProperty>
        Public Property FechaProgramacionCargue As DateTime
        <JsonProperty>
        Public Property ContactoCliente As String
        <JsonProperty>
        Public Property Firma As Byte()
        <JsonProperty>
        Public Property Placa As String
        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleCheckListCargueDescargues)
    End Class
End Namespace
