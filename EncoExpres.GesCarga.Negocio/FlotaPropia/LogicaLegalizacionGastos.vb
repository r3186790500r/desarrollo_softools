﻿Imports EncoExpres.GesCarga.Fachada.FlotaPropia
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.FlotaPropia


Namespace Despachos
    Public NotInheritable Class LogicaLegalizacionGastos
        Inherits LogicaBase(Of LegalizacionGastos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of LegalizacionGastos)

        Sub New(persistencia As IPersistenciaBase(Of LegalizacionGastos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As LegalizacionGastos) As Respuesta(Of IEnumerable(Of LegalizacionGastos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of LegalizacionGastos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of LegalizacionGastos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As LegalizacionGastos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long
            Dim validarDatos As Respuesta(Of LegalizacionGastos)

            If Not IsNothing(entidad.ParametrosCalculos) Then
                validarDatos = CalcularValores(entidad)
                If validarDatos.ProcesoExitoso Then
                    entidad = validarDatos.Datos
                End If
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Numero, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As LegalizacionGastos) As Respuesta(Of LegalizacionGastos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of LegalizacionGastos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of LegalizacionGastos)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>

        Public Function Anular(entidad As LegalizacionGastos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaLegalizacionGastos).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function GuardarFacturaTerpel(entidad As DetalleLegalizacionGastos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaLegalizacionGastos).GuardarFacturaTerpel(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(anulo)
            End If

            Return New Respuesta(Of Boolean)(anulo)
        End Function

        ' Funcion que recalcula valores pertinentes monetarios para mostrarlos en el front y otra vez cuando se llame guardar
        ' Utima modificacion por: Felipe Pieschacon
        ' Fecha ultimos cambios: 2021 12 08
        Public Function CalcularValores(entidad As LegalizacionGastos) As Respuesta(Of LegalizacionGastos)
            Dim strError As String = String.Empty

            If IsNothing(entidad) Then
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
                Return New Respuesta(Of LegalizacionGastos)(entidad) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            entidad.ValorGastos = 0
            entidad.ValorAnticipos = 0
            entidad.ValorReanticipos = 0
            entidad.SaldoConductor = 0
            entidad.SaldoEmpresa = 0
            entidad.TotalGalones = 0
            entidad.ValorTotalCombustible = 0
            entidad.ValorPromedioGalon = 0
            entidad.TotalGalonesAutorizados = 0
            Dim TotalGalonesEstimados As Double = 0
            entidad.TotalPeajes = 0

            For Each item In entidad.ParametrosCalculos.ListaPlanillaConceptos

                '---CalcularValorGalon
                For Each item1 In item.Data
                    If Not IsNothing(item1.ValorGalones) AndAlso Not IsNothing(item1.CantidadGalones) AndAlso item1.CantidadGalones > 0 AndAlso item1.ValorGalones > 0 Then
                        item1.Valor = Math.Round(item1.ValorGalones * item1.CantidadGalones, 2)
                        'Else
                        'item1.Valor = 0
                    End If
                Next
                '---CalcularValorGalon

                '---CalcularTermo
                item.Planilla.Horas_Termo = 0
                item.Planilla.Galones_Adicionales_Termo = 0
                item.Planilla.Galon_Estimado_Termo = 0
                item.Planilla.Galones_Adicionales_Termo = 0
                If item.Planilla.Horas_Fin >= 0 AndAlso item.Planilla.Horas_Inicio >= 0 Then
                    If item.Planilla.Horas_Fin > item.Planilla.Horas_Inicio Then
                        item.Planilla.Horas_Termo = item.Planilla.Horas_Fin - item.Planilla.Horas_Inicio
                        If item.Planilla.Horas_Galon_Termo > 0 Then
                            item.Planilla.Galon_Estimado_Termo = Math.Round(item.Planilla.Horas_Termo / item.Planilla.Horas_Galon_Termo, 2)
                            If item.Planilla.Horas_Adicionales_Termo > 0 Then
                                item.Planilla.Galones_Adicionales_Termo = Math.Round(item.Planilla.Horas_Adicionales_Termo / item.Planilla.Horas_Galon_Termo, 2)
                            End If
                        End If
                    Else
                        'ShowError('Las horas fin deben ser mayor a las horas inicio')
                        item.Planilla.Horas_Fin = Nothing
                    End If
                End If
                '---CalcularTermo

                '---CalcularKm y CalcularTotalesGalon
                item.Planilla.Total_Km = 0
                If item.Planilla.Km_Inicial >= 0 AndAlso item.Planilla.Km_Final >= 0 Then
                    If item.Planilla.Km_Inicial > item.Planilla.Km_Final Then
                        'ShowError('El Km Inicial debe ser menor al Km Final')
                        item.Planilla.Km_Final = 0
                    Else
                        item.Planilla.Total_Km = item.Planilla.Km_Final - item.Planilla.Km_Inicial
                        item.Planilla.Gal_Estimados = Math.Round(item.Planilla.Total_Km / item.Planilla.Km_Galon)
                        If item.Planilla.Galon_Estimado_Termo >= 0 Then
                            item.Planilla.Total_Galones = Math.Round(item.Planilla.Gal_Estimados + item.Planilla.Galon_Estimado_Termo, 2)
                        Else
                            item.Planilla.Total_Galones = Math.Round(item.Planilla.Gal_Estimados, 2)
                        End If
                    End If
                End If
                '---CalcularKm y CalcularTotalesGalon

                For Each item1 In item.Data
                    If Not IsNothing(item1.Valor) AndAlso Not item1.TieneChip Then
                        entidad.ValorGastos += item1.Valor
                    End If
                    If item1.ConceptoNombre.Contains("COMBUSTIBLE") Then
                        If Not IsNothing(item1.CantidadGalones) AndAlso item1.CantidadGalones > 0 Then
                            entidad.TotalGalones += item1.CantidadGalones
                            entidad.ValorTotalCombustible += item1.Valor
                        End If
                    End If
                    If item1.ConceptoNombre.Contains("PEAJES") Then
                        entidad.TotalPeajes += item1.Valor
                    End If
                Next
                'entidad.ValorAnticipos += Math.Round(item.Planilla.ValorAnticipo)
                If item.Planilla.Total_Galones > 0 Then
                    TotalGalonesEstimados += item.Planilla.Total_Galones
                End If
            Next
            For Each item In entidad.ParametrosCalculos.Comprobantes
                If item.DocumentoOrigenCodigo = 2605 Or item.DocumentoOrigenCodigo = 2609 Or item.DocumentoOrigenCodigo = 2617 Then
                    entidad.ValorReanticipos += item.ValorPagoTotal
                End If
                If item.DocumentoOrigenCodigo = 2604 Or item.DocumentoOrigenCodigo = 2613 Then
                    entidad.ValorAnticipos += item.ValorPagoTotal
                End If
            Next
            If entidad.TotalGalones > 0 Then
                entidad.ValorPromedioGalon = Math.Round(entidad.ValorTotalCombustible / entidad.TotalGalones)
            End If
            entidad.TotalGalonesAutorizados = Math.Round(TotalGalonesEstimados - entidad.TotalGalones, 2)
            If ((entidad.ValorAnticipos + entidad.ValorReanticipos) - entidad.ValorGastos) <= 0 Then
                entidad.SaldoEmpresa = 0
            Else
                entidad.SaldoEmpresa = ((entidad.ValorAnticipos + entidad.ValorReanticipos) - entidad.ValorGastos)
            End If
            If (entidad.ValorGastos - (entidad.ValorAnticipos + entidad.ValorReanticipos)) <= 0 Then
                entidad.SaldoConductor = 0
            Else
                entidad.SaldoConductor = (entidad.ValorGastos - (entidad.ValorAnticipos + entidad.ValorReanticipos))
            End If
            entidad.TotalGalones = (Math.Round(entidad.TotalGalones))

            Return New Respuesta(Of LegalizacionGastos)(entidad) With {.ProcesoExitoso = True}

        End Function

    End Class
End Namespace