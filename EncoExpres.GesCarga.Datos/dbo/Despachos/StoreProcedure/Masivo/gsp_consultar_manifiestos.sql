﻿PRINT 'gsp_consultar_manifiestos'
GO
DROP PROCEDURE gsp_consultar_manifiestos
GO
CREATE PROCEDURE gsp_consultar_manifiestos  
(                                      
	@par_EMPR_Codigo SMALLINT,                                      
	@par_Numero NUMERIC = NULL,                                      
	@par_Fecha_Inicial DATE = NULL,                                      
	@par_Fecha_Final DATE = NULL,                     
	@par_Placa VARCHAR(20) = NULL,                        
	@par_Codigo_Conductor NUMERIC = NULL,                        
	@par_Conductor VARCHAR(50)  = NULL,                                       
	@par_Estado INT = NULL,                                      
	@par_Anulado SMALLINT = NULL ,                                              
	@par_OFIC_Codigo INT = NULL,                                      
	@par_CATA_TIMA_Codigo NUMERIC = NULL,                        
	@par_NumeroPagina INT = NULL,                                      
	@par_RegistrosPagina INT = NULL,                  
	@par_Usuario_Consulta INT = 0                                    
)                                      
AS                                      
BEGIN                    
	 SET @par_OFIC_Codigo = CASE @par_OFIC_Codigo WHEN -1 THEN NULL ELSE @par_OFIC_Codigo END    
	 SET NOCOUNT ON;                                       
	 DECLARE @CantidadRegistros INT                                      
	 SELECT @CantidadRegistros =                                      
	 (                                      
	  SELECT DISTINCT                                      
	  COUNT(1)                                      
	  FROM    
    
	  Encabezado_Manifiesto_Carga ENMC                                      
	  LEFT JOIN Empresas EMPR ON                                 
	  ENMC.EMPR_Codigo = EMPR.Codigo          
         
	  LEFT JOIN Encabezado_Cumplido_Planilla_Despachos AS ECPD      
	  ON ENMC.EMPR_Codigo = ECPD.EMPR_Codigo      
	  AND ENMC.Numero = ECPD.ENMC_Numero      
                                
	  LEFT JOIN Vehiculos VEHI                                         
	  ON ENMC.EMPR_Codigo = VEHI.EMPR_Codigo                                     
	  AND ENMC.VEHI_Codigo = VEHI.Codigo                                      
                                      
	  LEFT JOIN Terceros PROP ON                                      
	  ENMC.EMPR_Codigo = PROP.EMPR_Codigo AND ENMC.TERC_Codigo_Propietario = PROP.Codigo                                      
                                      
	  LEFT JOIN Terceros TENE ON                                      
	  ENMC.EMPR_Codigo = TENE.EMPR_Codigo AND ENMC.TERC_Codigo_Tenedor = TENE.Codigo                                      
                                      
	  LEFT JOIN Terceros COND ON                                      
	  ENMC.EMPR_Codigo = COND.EMPR_Codigo AND ENMC.TERC_Codigo_Conductor = COND.Codigo                                      
                                      
	  LEFT JOIN Terceros SECO ON                                      
	  ENMC.EMPR_Codigo = SECO.EMPR_Codigo                                      
	  AND ENMC.TERC_Codigo_Segundo_Conductor = SECO.Codigo                                      
                                      
	  LEFT JOIN Terceros AFIL ON                                      
	  ENMC.EMPR_Codigo = AFIL.EMPR_Codigo                                      
	  AND ENMC.TERC_Codigo_Afiliador = AFIL.Codigo                                      
                                      
	  LEFT JOIN Semirremolques SEMI ON                                      
	  ENMC.EMPR_Codigo = SEMI.EMPR_Codigo                                      
	  AND ENMC.SEMI_Codigo = SEMI.Codigo                                      
                                      
	  LEFT JOIN V_Tipo_vehiculos TIVE ON                                      
	  ENMC.EMPR_Codigo = TIVE.EMPR_Codigo                                      
	  AND ENMC.TIVE_Codigo = TIVE.Codigo                                      
                                      
	  LEFT JOIN Rutas RUTA ON                                      
	  ENMC.EMPR_Codigo = RUTA.EMPR_Codigo             
	  AND ENMC.RUTA_Codigo = RUTA.Codigo                                      
                 
	  LEFT JOIN Ciudades CIOR ON                                  
	  RUTA.EMPR_Codigo = CIOR.EMPR_Codigo                                      
	  AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo                                      
                                
	  LEFT JOIN Ciudades CIDE ON                                      
	  RUTA.EMPR_Codigo = CIDE.EMPR_Codigo                                      
	  AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo                                      
                                      
	  --LEFT JOIN Producto_Transportados PRTR ON                                
	  --ENMC.EMPR_Codigo = PRTR.EMPR_Codigo                           
	  --AND                                  
                                
	  LEFT JOIN Detalle_Despacho_Orden_Servicios DDOS on                                 
	  ENMC.EMPR_Codigo = DDOS.EMPR_Codigo                                 
	  AND ENMC.Numero = DDOS.ENMC_Numero                                
                                
	  LEFT JOIN Producto_Transportados PRTR ON                                 
	  DDOS.EMPR_Codigo =  PRTR.EMPR_Codigo                                 
	  AND DDOS.PRTR_Codigo = PRTR.Codigo                                
                                
	  LEFT JOIN Terceros ASEG ON                                      
	  ENMC.EMPR_Codigo = ASEG.EMPR_Codigo                                      
	  AND ENMC.TERC_Codigo_Aseguradora = ASEG.Codigo                                      
                                      
	  LEFT JOIN Tipo_Documentos TIDO ON                                      
	  ENMC.EMPR_Codigo = TIDO.EMPR_Codigo                                      
	  AND ENMC.TIDO_Codigo = TIDO.Codigo                                      
                                      
	  LEFT JOIN Oficinas OFIC ON                                      
	  ENMC.EMPR_Codigo = OFIC.EMPR_Codigo                                      
	  AND ENMC.OFIC_Codigo = OFIC.Codigo                                      
                                      
	  LEFT JOIN V_Tipo_Manifiesto TIMA ON                                      
	  ENMC.EMPR_Codigo = TIMA.EMPR_Codigo                                      
	  AND ENMC.CATA_TIMA_Codigo = TIMA.Codigo                                      
                                      
	  LEFT JOIN Detalle_Integraciones_Planilla_Despachos DIPD                                      
	  ON ENMC.EMPR_Codigo = DIPD.EMPR_Codigo AND ENMC.ENPD_Numero = DIPD.ENPD_Numero                                      
                                         
	  LEFT JOIN V_Motivo_Anulacion_Manifiesto MAMA ON                                      
	  ENMC.EMPR_Codigo = MAMA.EMPR_Codigo                                      
	  AND ENMC.CATA_MAMC_Codigo = MAMA.Codigo                                   
                                 
	  LEFT JOIN Encabezado_Planilla_Despachos ENPD ON    
	  ENMC.EMPR_Codigo = ENPD.EMPR_Codigo    
	  AND ENMC.ENPD_Numero = ENPD.Numero     
                                      
	  WHERE                                      
	  ENMC.EMPR_Codigo = @par_EMPR_Codigo                                      
	  AND ENMC.Numero_Documento = ISNULL(@par_Numero,ENMC.Numero_Documento)                                      
	  AND (CONVERT (DATE, ENMC.Fecha) >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)    
	  AND (CONVERT (DATE, ENMC.Fecha) <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)    
	  AND (ENMC.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)                    
	  AND ENMC.TERC_Codigo_Conductor = ISNULL(@par_Codigo_Conductor,ENMC.TERC_Codigo_Conductor)                        
	  AND ((COND.Nombre + ' ' + COND.Apellido1 + ' ' + COND.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%' OR COND.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%') OR (@par_Conductor IS NULL))                        
	  AND ENMC.Estado = ISNULL(@par_Estado, ENMC.Estado)       
	  AND VEHI.Placa = ISNULL(@par_Placa, VEHI.Placa)                  
	  AND ENMC.Anulado = ISNULL (@par_Anulado,ENMC.Anulado )          
	  AND (ENMC.CATA_TIMA_Codigo = @par_CATA_TIMA_Codigo OR @par_CATA_TIMA_Codigo IS NULL)          
	  AND (ENMC.OFIC_Codigo IN (                  
	  SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE USOF.USUA_Codigo = @par_Usuario_Consulta                  
	  ) OR @par_Usuario_Consulta IS NULL OR @par_Usuario_Consulta = 0)                                  
	 );                                      
	 WITH Pagina                                      
	 AS          
	 (    
	  SELECT DISTINCT    
	  EMPR.Nombre_Razon_Social Empresa,                                
	  ENMC.EMPR_Codigo,                                      
	  ENMC.Numero,                                      
	  ENMC.Numero_Documento,                                      
	  ENMC.Fecha,              
	  ENMC.VEHI_Codigo,                                      
	  VEHI.Placa AS PlacaVehiculo,                                      
	  ENMC.TERC_Codigo_Propietario,                                      
	  PROP.Numero_Identificacion AS IdentificacionPropietario,                                      
	  CONCAT(PROP.Nombre,' ',PROP.Apellido1,' ',PROP.Apellido2) AS NombrePropietario,                                      
	  ENMC.TERC_Codigo_Tenedor,                                      
	  TENE.Numero_Identificacion AS IdentificacionTenedor,                                      
	  CONCAT(TENE.Nombre,' ',TENE.Apellido1,' ',TENE.Apellido2) AS NombreTenedor,                                      
	  ENMC.TERC_Codigo_Conductor,                              
	  COND.Numero_Identificacion AS IdentificacionConductor,                                      
	  CONCAT(COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2) AS NombreConductor,                                      
	  COND.Telefonos AS TelefonoConductor,                                      
	  ISNULL(ENMC.TERC_Codigo_Segundo_Conductor,0) AS TERC_Codigo_Segundo_Conductor,                                      
	  SECO.Numero_Identificacion AS IdentificacionSegundoConductor,                                      
	  CONCAT(SECO.Nombre,' ',SECO.Apellido1,' ',SECO.Apellido2) AS NombreSegundoConductor,                                      
	  ENMC.TERC_Codigo_Afiliador,                                      
	  AFIL.Numero_Identificacion AS IdentificacionAfiliador,                                      
	  CONCAT(AFIL.Nombre,' ',AFIL.Apellido1,' ',AFIL.Apellido2) AS NombreAfiliador,                                      
	  ENMC.SEMI_Codigo,                                      
	  SEMI.Placa AS PlacaSemirremolque,                                      
	  ENMC.TIVE_Codigo,                                      
	  TIVE.Nombre AS TipoVehiculo,                                      
	  ENMC.RUTA_Codigo,                                      
	  RUTA.Nombre AS NombreRuta,                                      
	  ENMC.Observaciones,                                      
	  ENMC.Fecha_Cumplimiento,                                      
	  ENMC.Cantidad_Total,                       
	  ENMC.Peso_Total,                                      
	  ENMC.Valor_Flete,                                      
	  ENMC.Valor_Retencion_Fuente,                                      
	  ENMC.Valor_ICA,                                      
	  ENMC.Valor_Otros_Descuentos,                                      
	  ENMC.Valor_Flete_Neto,                                      
	  ENMC.Valor_Anticipo,                                      
	  ENMC.Valor_Pagar,                            
	  ENMC.TERC_Codigo_Aseguradora,                                      
	  ASEG.Numero_Identificacion AS IdentificacionAseguradora,                                      
	  CONCAT(ASEG.Nombre,' ',ASEG.Apellido1,' ',ASEG.Apellido2) AS NombreAseguradora,                                      
	  ENMC.Numero_Poliza,                                      
	  ENMC.Fecha_Vigencia_Poliza,                              
	  ENMC.ELPD_Numero,                     
	  ECPD.Numero_Documento AS ECPD_Numero,                                      
	  ENMC.Finalizo_Viaje,                                      
	  ENMC.Anulado,                                      
	  ENMC.Siniestro,                                      
	  ENMC.Estado,                                      
	  CASE ENMC.Estado WHEN 1 THEN 'Definitivo' ELSE 'Borrador' END AS NombreEstado,                                      
	  ISNULL(ENMC.Numeracion,'') AS Numeracion,                                      
	  ENMC.TIDO_Codigo,                                      
	  TIDO.Nombre AS TipoDocumento,                                      
	  ENMC.USUA_Codigo_Crea,                                      
	  ENMC.Fecha_Crea,                                      
	  ISNULL(ENMC.USUA_Codigo_Modifica,0) AS USUA_Codigo_Modifica,                                      
	  ISNULL(ENMC.Fecha_Modifica,'') AS Fecha_Modifica,                                      
	  ISNULL(ENMC.Fecha_Anula,'') AS Fecha_Anula,                                      
	  ISNULL(ENMC.USUA_Codigo_Anula,0) AS USUA_Codigo_Anula,                                      
	  ISNULL(ENMC.Causa_Anula,'') AS Causa_Anula,                                      
	  ENMC.OFIC_Codigo,                                      
	  OFIC.Nombre AS Oficina,                
	  ENMC.CATA_TIMA_Codigo,                                      
	  ISNULL(TIMA.Nombre,'') AS TipoManifiesto,                                      
	  ISNULL(ENMC.Numero_Manifiesto_Electronico,0) AS Numero_Manifiesto_Electronico,                                      
	  ISNULL(ENMC.Fecha_Reporte_Manifiesto_Electronico,'') AS Fecha_Reporte_Manifiesto_Electronico,                                      
	  ISNULL(ENMC.Mensaje_Manifiesto_Electronico,'') AS Mensaje_Manifiesto_Electronico,                                      
	  ISNULL(ENMC.CATA_MAMC_Codigo,0) AS CATA_MAMC_Codigo,                                      
	  ISNULL(MAMA.Nombre,'') AS MotivoAnulacion,                                      
	  ROW_NUMBER() OVER (ORDER BY ENMC.Numero DESC) AS RowNumber                                      
	  ,CIOR.Nombre CiudadOrigen                    
	  ,CIOR.DEPA_Codigo As CIOR_DEPA_Codigo                                
	  ,CIDE.DEPA_Codigo As CIDE_DEPA_Codigo                    
	  ,CIDE.Nombre CiudadDestino                                
	  ,ISNULL(PRTR.Nombre, '') AS NombreProducto    
	  ,ISNULL(ENPD.Numero_Documento, 0) as NumeroDocumentoPlanilla                          
	  ,ISNULL(ENPD.Numero, 0) as NumeroPlanilla                          
	  FROM                                      
	  Encabezado_Manifiesto_Carga ENMC                                      
	  LEFT JOIN Empresas EMPR ON                                 
	  ENMC.EMPR_Codigo = EMPR.Codigo          
         
            
	  LEFT JOIN Encabezado_Cumplido_Planilla_Despachos AS ECPD      
	  ON ENMC.EMPR_Codigo = ECPD.EMPR_Codigo      
	  AND ENMC.Numero = ECPD.ENMC_Numero      
                                
                                
	  LEFT JOIN Vehiculos VEHI                                         
	  ON ENMC.EMPR_Codigo = VEHI.EMPR_Codigo                                     
	  AND ENMC.VEHI_Codigo = VEHI.Codigo                                      
                                      
	  LEFT JOIN Terceros PROP ON                                      
	  ENMC.EMPR_Codigo = PROP.EMPR_Codigo AND ENMC.TERC_Codigo_Propietario = PROP.Codigo                                      
                                      
	  LEFT JOIN Terceros TENE ON                                      
	  ENMC.EMPR_Codigo = TENE.EMPR_Codigo AND ENMC.TERC_Codigo_Tenedor = TENE.Codigo                                      
                                      
	  LEFT JOIN Terceros COND ON                       
	  ENMC.EMPR_Codigo = COND.EMPR_Codigo AND ENMC.TERC_Codigo_Conductor = COND.Codigo                                      
                                      
	  LEFT JOIN Terceros SECO ON                                      
	  ENMC.EMPR_Codigo = SECO.EMPR_Codigo                                      
	  AND ENMC.TERC_Codigo_Segundo_Conductor = SECO.Codigo                                      
                                      
	  LEFT JOIN Terceros AFIL ON                                      
	  ENMC.EMPR_Codigo = AFIL.EMPR_Codigo                                      
	  AND ENMC.TERC_Codigo_Afiliador = AFIL.Codigo                                      
                                      
	  LEFT JOIN Semirremolques SEMI ON                        
	  ENMC.EMPR_Codigo = SEMI.EMPR_Codigo                                      
	  AND ENMC.SEMI_Codigo = SEMI.Codigo                                      
                                      
	  LEFT JOIN V_Tipo_vehiculos TIVE ON                                      
	  ENMC.EMPR_Codigo = TIVE.EMPR_Codigo                                      
	  AND ENMC.TIVE_Codigo = TIVE.Codigo                                      
                                      
	  LEFT JOIN Rutas RUTA ON                                      
	  ENMC.EMPR_Codigo = RUTA.EMPR_Codigo                                      
	  AND ENMC.RUTA_Codigo = RUTA.Codigo                                      
                                
	  LEFT JOIN Ciudades CIOR ON                                      
	  RUTA.EMPR_Codigo = CIOR.EMPR_Codigo                                      
	  AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo                               
                                
	  LEFT JOIN Ciudades CIDE ON                                      
	  RUTA.EMPR_Codigo = CIDE.EMPR_Codigo                                      
	  AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo                                      
                                      
	  --LEFT JOIN Producto_Transportados PRTR ON                                
	  --ENMC.EMPR_Codigo = PRTR.EMPR_Codigo                                
	  --AND                                  
                                
	  LEFT JOIN Detalle_Despacho_Orden_Servicios DDOS on                                 
	  ENMC.EMPR_Codigo = DDOS.EMPR_Codigo                                 
	  AND ENMC.Numero = DDOS.ENMC_Numero                                
                                
	  LEFT JOIN Producto_Transportados PRTR ON                                 
	  DDOS.EMPR_Codigo =  PRTR.EMPR_Codigo                                 
	  AND DDOS.PRTR_Codigo = PRTR.Codigo                                
                                
	  LEFT JOIN Terceros ASEG ON                                      
	  ENMC.EMPR_Codigo = ASEG.EMPR_Codigo                                      
	  AND ENMC.TERC_Codigo_Aseguradora = ASEG.Codigo                                      
                                 
	  LEFT JOIN Tipo_Documentos TIDO ON                                      
	  ENMC.EMPR_Codigo = TIDO.EMPR_Codigo                                      
	  AND ENMC.TIDO_Codigo = TIDO.Codigo                                      
                                      
	  LEFT JOIN Oficinas OFIC ON                                      
	  ENMC.EMPR_Codigo = OFIC.EMPR_Codigo                                      
	  AND ENMC.OFIC_Codigo = OFIC.Codigo                                      
                                      
	  LEFT JOIN V_Tipo_Manifiesto TIMA ON                                      
	  ENMC.EMPR_Codigo = TIMA.EMPR_Codigo                                      
	  AND ENMC.CATA_TIMA_Codigo = TIMA.Codigo                                      
                                      
	  LEFT JOIN Detalle_Integraciones_Planilla_Despachos DIPD                                      
	  ON ENMC.EMPR_Codigo = DIPD.EMPR_Codigo AND ENMC.ENPD_Numero = DIPD.ENPD_Numero                                      
                                         
	  LEFT JOIN V_Motivo_Anulacion_Manifiesto MAMA ON                                      
	  ENMC.EMPR_Codigo = MAMA.EMPR_Codigo                                      
	  AND ENMC.CATA_MAMC_Codigo = MAMA.Codigo                                  
                  
	  LEFT JOIN Encabezado_Planilla_Despachos ENPD ON    
	  ENMC.EMPR_Codigo = ENPD.EMPR_Codigo    
	  AND ENMC.ENPD_Numero = ENPD.Numero                                    
                                     
	  WHERE                                      
	  ENMC.EMPR_Codigo = @par_EMPR_Codigo                                      
	  AND ENMC.Numero_Documento = ISNULL(@par_Numero,ENMC.Numero_Documento)                                      
	  AND (CONVERT (DATE, ENMC.Fecha) >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)    
	  AND (CONVERT (DATE, ENMC.Fecha) <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)     
	  AND (ENMC.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)                    
	  AND ENMC.TERC_Codigo_Conductor = ISNULL(@par_Codigo_Conductor,ENMC.TERC_Codigo_Conductor)                        
	  AND ((COND.Nombre + ' ' + COND.Apellido1 + ' ' + COND.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%' OR COND.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%') OR (@par_Conductor IS NULL))                                     
	  AND ENMC.Estado = ISNULL(@par_Estado, ENMC.Estado)                                      
	  AND ENMC.Anulado = ISNULL (@par_Anulado,ENMC.Anulado )                                              
	  AND VEHI.Placa = ISNULL(@par_Placa, VEHI.Placa)                   
	  AND (ENMC.CATA_TIMA_Codigo = @par_CATA_TIMA_Codigo OR @par_CATA_TIMA_Codigo IS NULL)          
	  AND (ENMC.OFIC_Codigo IN (                  
	   SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE USOF.USUA_Codigo = @par_Usuario_Consulta                  
	  ) OR @par_Usuario_Consulta IS NULL OR @par_Usuario_Consulta = 0)                                  
	 )                                      
	 SELECT DISTINCT                                      
	 0 AS Obtener,                                      
	 Empresa ,                               
	 EMPR_Codigo,                                      
	 Numero,                                      
	 Numero_Documento,                                      
	 Fecha,                                      
	 VEHI_Codigo,                                      
	 PlacaVehiculo,                                      
	 TERC_Codigo_Propietario,                                      
	 IdentificacionPropietario,                                      
	 NombrePropietario,                                      
	 TERC_Codigo_Tenedor,                                      
	 IdentificacionTenedor,                                      
	 NombreTenedor,                                      
	 TERC_Codigo_Conductor,                                      
	 IdentificacionConductor,                                      
	 NombreConductor,                                      
	 TelefonoConductor,                                      
	 TERC_Codigo_Segundo_Conductor,                                      
	 IdentificacionSegundoConductor,                                      
	 NombreSegundoConductor,                                      
	 TERC_Codigo_Afiliador,                                      
	 IdentificacionAfiliador,                                      
	 NombreAfiliador,                                      
	 SEMI_Codigo,                                      
	 PlacaSemirremolque,                                      
	 TIVE_Codigo,                                      
	 TipoVehiculo,                           
	 RUTA_Codigo,                                      
	 NombreRuta,                                      
	 Observaciones,                                      
	 Fecha_Cumplimiento,                                      
	 Cantidad_Total,                                      
	 Peso_Total,                                      
	 Valor_Flete,                                      
	 Valor_Retencion_Fuente,                                      
	 Valor_ICA,                                      
	 Valor_Otros_Descuentos,                                      
	 Valor_Flete_Neto,                                      
	 Valor_Anticipo,                                      
	 Valor_Pagar,             
	 TERC_Codigo_Aseguradora,                                      
	 IdentificacionAseguradora,                          
	 NombreAseguradora,                                      
	 Numero_Poliza,                                      
	 Fecha_Vigencia_Poliza,                                      
	 ELPD_Numero,                                      
	 ECPD_Numero,                                      
	 Finalizo_Viaje,                                      
	 Anulado,                                      
	 Siniestro,                                      
	 Estado,                                      
	 NombreEstado,                                      
	 Numeracion,                                      
	 TIDO_Codigo,                 
	 TipoDocumento,                                      
	 USUA_Codigo_Crea,                                      
	 Fecha_Crea,                                      
	 USUA_Codigo_Modifica,                                      
	 Fecha_Modifica,                                      
	 Fecha_Anula,                                      
	 USUA_Codigo_Anula,                                      
	 Causa_Anula,                                      
	 OFIC_Codigo,                                      
	 Oficina,                                      
	 CATA_TIMA_Codigo,                                      
	 TipoManifiesto,                                      
	 Numero_Manifiesto_Electronico,                                      
	 Fecha_Reporte_Manifiesto_Electronico,                                      
	 Mensaje_Manifiesto_Electronico,                                      
	 CATA_MAMC_Codigo,                                      
	 MotivoAnulacion,                                      
	 CiudadOrigen,                     
	 CIOR_DEPA_Codigo,                    
	 CIDE_DEPA_Codigo,                    
	 CiudadDestino,                                
	 NombreProducto,                                
	 NumeroDocumentoPlanilla,                          
	 NumeroPlanilla,    
	 @CantidadRegistros AS TotalRegistros,                                      
	 @par_NumeroPagina AS PaginaObtener,                                      
	 @par_RegistrosPagina AS RegistrosPagina                                      
	 FROM Pagina                                      
	 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                  
	 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                      
	 ORDER BY Numero                                      
END
GO