﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioDocumentos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDocumentos
        Inherits RepositorioBase(Of Documentos)
        Public Overrides Function Consultar(filtro As Documentos) As IEnumerable(Of Documentos)
            Dim lista As New List(Of Documentos)
            Dim item As Documentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Terceros Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.CodigoTerceros)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_documento_terceros]")
                ElseIf filtro.Vehiculo Then
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", filtro.CodigoVehiculo)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_documento_vehiculos]")
                ElseIf filtro.Semirremolques Then
                    conexion.AgregarParametroSQL("@par_SEMI_Codigo", filtro.CodigoSemirremolques)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_documento_semirremolques]")
                ElseIf filtro.Cumplido Then
                    conexion.AgregarParametroSQL("@par_ENCU_Numero", filtro.NumeroCumplido)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo", filtro.UsuarioCrea.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_documento_cumplido_despachos]")
                ElseIf filtro.CumplidoRemesa Then
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", filtro.Numero)
                    If filtro.Id > 0 Then
                        conexion.AgregarParametroSQL("@par_ID", filtro.Id)
                    End If
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_documento_cumplido_remesa_despachos")
                ElseIf filtro.Remesa Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", filtro.Numero)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_documento_remesa")
                End If
                While resultado.Read
                    item = New Documentos(resultado)
                    lista.Add(item)
                End While

                resultado.Close()


            End Using
            Return lista
        End Function
        Public Function ConsultarFotos(filtro As Documentos) As IEnumerable(Of Documentos)
            Dim lista As New List(Of Documentos)
            Dim item As Documentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.CodigoTerceros)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_foto_terceros]")

                While resultado.Read
                    item = New Documentos(resultado)
                    lista.Add(item)
                End While

                resultado.Close()


            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Documentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As Documentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As Documentos) As Documentos
            Dim item As Documentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CDGD_Codigo", filtro.Configuracion.Codigo)
                If filtro.Terceros Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.CodigoTerceros)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_documento_terceros]")
                ElseIf filtro.Vehiculo Then
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", filtro.CodigoVehiculo)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_documento_vehiculos]")
                ElseIf filtro.Semirremolques Then
                    conexion.AgregarParametroSQL("@par_SEMI_Codigo", filtro.CodigoSemirremolques)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_documento_semirremolques]")
                End If
                While resultado.Read
                    item = New Documentos(resultado)
                End While

                resultado.Close()


            End Using
            Return item
        End Function

        Public Function InsertarTemporal(entidad As Documentos) As Long
            Dim RegistroAfectado As Integer = 0
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                If entidad.Cumplido Then

                    conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_ENCU_Numero", entidad.NumeroCumplido)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.NombreAsignado)
                    conexion.AgregarParametroSQL("@par_NombreArchivo", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Archivo", entidad.Archivo, SqlDbType.VarBinary)
                    conexion.AgregarParametroSQL("@par_ExtensionArchivo", entidad.Extension)
                    conexion.AgregarParametroSQL("@par_Tipo", entidad.Tipo)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_t_documento_cumplido_despachos]")

                    While resultado.Read
                        RegistroAfectado = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While

                ElseIf entidad.CumplidoRemesa Then
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_NombreArchivo", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Archivo", entidad.Archivo, SqlDbType.VarBinary)
                    conexion.AgregarParametroSQL("@par_Tipo", entidad.Tipo)
                    conexion.AgregarParametroSQL("@par_ExtensionArchivo", entidad.Extension)
                    conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                    If entidad.Id > 0 Then
                        conexion.AgregarParametroSQL("@par_ID", entidad.Id)
                    End If
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_t_documento_cumplido_remesa")

                    While resultado.Read
                        RegistroAfectado = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While
                Else
                    conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Configuracion.Codigo)
                    conexion.AgregarParametroSQL("@par_Archivo", entidad.Archivo, SqlDbType.VarBinary)
                    conexion.AgregarParametroSQL("@par_Nombre_Documento", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Extension_Documento", entidad.Extension)
                    conexion.AgregarParametroSQL("@par_Tipo", entidad.Tipo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_t_archivos]")

                    While resultado.Read
                        RegistroAfectado = Convert.ToInt64(resultado.Item("RegistrosAfectados").ToString())
                    End While
                End If

            End Using
            Return RegistroAfectado
        End Function

        Public Function GuardarDocumentoTercero(entidad As Documentos) As Long
            If entidad.CodigoTerceros > 0 Then
                Dim RegistroAfectado As Integer = 0
                Dim inserto As Boolean = False
                Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                    Dim resultado As IDataReader

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                    conexion.AgregarParametroSQL("@par_Emisor", entidad.Emisor)
                    conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
                    If entidad.FechaEmision > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Emision", entidad.FechaEmision, SqlDbType.DateTime)
                    End If
                    If entidad.FechaVence > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vence", entidad.FechaVence, SqlDbType.DateTime)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Configuracion.Codigo)
                    conexion.AgregarParametroSQL("@par_Elimina_Archivo", 0)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.CodigoTerceros)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_documento_terceros]")
                    While resultado.Read
                        RegistroAfectado = Convert.ToInt64(resultado.Item("RegistrosAfectados").ToString())
                    End While
                    resultado.Close()
                End Using
                Return RegistroAfectado
            Else
                Dim RegistroAfectado As Integer = 0
                Dim inserto As Boolean = False
                Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                    Dim resultado As IDataReader

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                    conexion.AgregarParametroSQL("@par_Emisor", entidad.Emisor)
                    conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
                    If entidad.FechaEmision > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Emision", entidad.FechaEmision, SqlDbType.DateTime)
                    End If
                    If entidad.FechaVence > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vence", entidad.FechaVence, SqlDbType.DateTime)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Configuracion.Codigo)
                    conexion.AgregarParametroSQL("@par_Elimina_Archivo", 0)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.CodigoVehiculo)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_documento_vehiculos]")
                    While resultado.Read
                        RegistroAfectado = Convert.ToInt64(resultado.Item("RegistrosAfectados").ToString())
                    End While
                    resultado.Close()
                End Using
                Return RegistroAfectado
            End If
        End Function

        Public Function EliminarTemporal(entidad As Documentos) As Boolean
            Dim inserto As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                If entidad.Cumplido Then
                    conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)
                    If entidad.NumeroCumplido > 0 Then
                        conexion.AgregarParametroSQL("@par_ENCU_Numero", entidad.NumeroCumplido)
                    End If
                    If entidad.Id > 0 Then
                        conexion.AgregarParametroSQL("@par_ID", entidad.Id)
                    End If
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_t_documento_cumplido_despachos]")
                ElseIf entidad.CumplidoRemesa Then
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)
                    If entidad.Id > 0 Then
                        conexion.AgregarParametroSQL("@par_ID", entidad.Id)
                    End If
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_t_documento_cumplido_remesa]")
                Else
                    If entidad.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_t_archivos]")
                End If


                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While

            End Using
            Return inserto
        End Function
        Public Function EliminarFotos(CodigoEmpresa As Integer, CodigoTercero As Integer) As Boolean
            Dim inserto As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", CodigoTercero)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_fotos_tercero]")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While

            End Using
            Return inserto
        End Function
        Public Function InsertarDocumentoVehiculo(entidad As Documentos) As Boolean

            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Dim resultado As IDataReader

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                conexion.AgregarParametroSQL("@par_Emisor", entidad.Emisor)
                conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
                If entidad.FechaEmision > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Emision", entidad.FechaEmision, SqlDbType.DateTime)
                End If
                If entidad.FechaVence > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Vence", entidad.FechaVence, SqlDbType.DateTime)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Configuracion.Codigo)
                conexion.AgregarParametroSQL("@par_Elimina_Archivo", entidad.EliminaDocumento)
                conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.CodigoVehiculo)
                conexion.AgregarParametroSQL("@par_Aplica_Inactivacion", entidad.AplicaInactivacion)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_documento_vehiculos]")
                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While

                ''Translada el archivo de la tabla temporal a la tabal de los documentos del vehiculo
                resultado.Close()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Configuracion.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.CodigoVehiculo)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_transladar_documento_vehiculos]")
                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While
                resultado.Close()
            End Using
            Return inserto
        End Function

        Public Function InsertarDocumentoTercero(entidad As Documentos) As Boolean

            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Dim resultado As IDataReader

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                conexion.AgregarParametroSQL("@par_Emisor", entidad.Emisor)
                conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
                If entidad.FechaEmision > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Emision", entidad.FechaEmision, SqlDbType.DateTime)
                End If
                If entidad.FechaVence > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Vence", entidad.FechaVence, SqlDbType.DateTime)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Configuracion.Codigo)
                conexion.AgregarParametroSQL("@par_Elimina_Archivo", entidad.EliminaDocumento)

                conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.CodigoTerceros)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_documento_terceros]")
                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While

                ''Translada el archivo de la tabla temporal a la tabal de los documentos del vehiculo
                resultado.Close()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Configuracion.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.CodigoTerceros)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_transladar_documento_terceros]")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While
                resultado.Close()
            End Using
            Return inserto
        End Function

        Public Function InsertarFotosTercero(entidad As Documentos) As Boolean

            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Dim resultado As IDataReader

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.CodigoTerceros)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_Nombre_Documento", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Archivo", entidad.Archivo, SqlDbType.VarBinary)
                conexion.AgregarParametroSQL("@par_Extension", entidad.Extension)
                conexion.AgregarParametroSQL("@par_Tipo", entidad.Tipo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_fotos_terceros]")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While

                resultado.Close()
            End Using
            Return inserto
        End Function


        Public Function InsertarDocumentoSemirremolque(entidad As Documentos) As Boolean

            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Dim resultado As IDataReader

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                conexion.AgregarParametroSQL("@par_Emisor", entidad.Emisor)
                conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
                If entidad.FechaEmision > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Emision", entidad.FechaEmision, SqlDbType.DateTime)
                End If
                If entidad.FechaVence > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Vence", entidad.FechaVence, SqlDbType.DateTime)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Configuracion.Codigo)
                conexion.AgregarParametroSQL("@par_Elimina_Archivo", entidad.EliminaDocumento)

                conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.CodigoSemirremolques)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_documento_semirremolques]")
                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While

                ''Translada el archivo de la tabla temporal a la tabal de los documentos del vehiculo
                resultado.Close()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Configuracion.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.CodigoSemirremolques)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_transladar_documento_semirremolques]")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While
                resultado.Close()
            End Using
            Return inserto
        End Function


        Public Function InsertarDocumentosCumplido(entidad As Documentos) As Boolean

            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Dim resultado As IDataReader

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENCU_Numero", entidad.NumeroCumplido)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_A_Temporal", entidad.HaciaTemporal)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_trasladar_documento_cumplido_despachos]")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While
                resultado.Close()
            End Using
            Return inserto
        End Function

        Public Function InsertarDocumentosCumplidoRemesa(entidad As Documentos) As Long

            Dim Codigo As Integer = 0
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Dim resultado As IDataReader

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)
                If entidad.Id > 0 Then
                    conexion.AgregarParametroSQL("@par_ID", entidad.Id)
                End If
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_trasladar_documento_cumplido_remesa")

                While resultado.Read
                    Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While
                resultado.Close()
            End Using
            Return Codigo
        End Function

        Public Function EliminarDocumentosCumplidoRemesa(entidad As Documentos) As Boolean
            Dim inserto As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)
                If entidad.Id > 0 Then
                    conexion.AgregarParametroSQL("@par_ID", entidad.Id)
                End If
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_documento_cumplido_remesa")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While

            End Using
            Return inserto
        End Function

        Public Function InsertarDocumentoRemesa(entidad As Documentos) As Long
            Dim RegistroAfectado As Integer = 0
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                If entidad.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                End If
                If entidad.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)
                End If
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                If entidad.Temporal Then
                    conexion.AgregarParametroSQL("@par_NombreArchivo", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Archivo", entidad.Archivo, SqlDbType.VarBinary)
                    conexion.AgregarParametroSQL("@par_Tipo", entidad.Tipo)
                    conexion.AgregarParametroSQL("@par_ExtensionArchivo", entidad.Extension)
                    conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_t_documentos_remesa")
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_documentos_remesa")
                End If

                While resultado.Read
                    RegistroAfectado = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While
            End Using
            Return RegistroAfectado
        End Function

        Public Function EliminarDocumentoRemesa(entidad As Documentos) As Boolean
            Dim Eliminar As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)

                If entidad.Temporal Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_t_documentos_remesa")
                Else
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_documentos_remesa")
                End If

                While resultado.Read
                    Eliminar = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While

            End Using
            Return Eliminar
        End Function

    End Class

End Namespace