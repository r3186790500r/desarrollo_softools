﻿EncoExpresApp.controller("CumplidoRemesasCtrl", ['$scope', '$timeout', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'RemesasFactory', 'RutasFactory', 'CiudadesFactory', 'TercerosFactory', 'VehiculosFactory', 'blockUIConfig',
    function ($scope, $timeout, $linq, blockUI, $routeParams, ValorCatalogosFactory, RemesasFactory, RutasFactory, CiudadesFactory, TercerosFactory, VehiculosFactory, blockUIConfig) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Masivo' }, { Nombre: 'Cumplir Remesas' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        var Continuar;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {}, Detalle: {} }
        }

        try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CUMPLIR_REMESAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaFinal = new Date();
        }

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.Numero = $routeParams.Codigo;
                Find();
            }

        }
        /*Cargar el combo de ESTADO DE RESEMAS PAQUETERIA*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEstadoRemesaPaqueteria = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoEstadoRemesaPaqueteria = response.data.Datos;

                        $scope.Modelo.EstadoRemesaPaqueteria = $scope.ListadoEstadoRemesaPaqueteria[0]

                    }
                    else {
                        $scope.ListadoEstadoRemesaPaqueteria = []
                    }
                }
            }, function (response) {
            });

        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoRutasInical = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoRutasInical = response.data.Datos
                        $scope.Modelo.Ruta = ''
                    }
                    else {
                        $scope.ListadoRutasInical = []
                    }
                }
            }, function (response) {
            });
        $scope.ListadoCiudades = [];
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }

        $scope.ListadoCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente)
                }
            }
            return $scope.ListadoCliente
        }
        $scope.ListaPlaca = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 2) == 0 || value.length == 1) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaPlaca = ValidarListadoAutocomplete(Response.Datos, $scope.ListaPlaca)
                }
            }
            return $scope.ListaPlaca
        }
        /*Cargar el combo de tipo entrega*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                    if (response.data.Datos.length > CERO) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;

                        $scope.Modelo.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[CERO]

                    }
                    else {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de tipo transporte*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_TRANSPORTE_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoTransporteRemsaPaqueteria = [];
                    if (response.data.Datos.length > CERO) {
                        $scope.ListadoTipoTransporteRemsaPaqueteria = response.data.Datos;

                        $scope.Modelo.TipoTransporteRemsaPaqueteria = $scope.ListadoTipoTransporteRemsaPaqueteria[CERO]

                    }
                    else {
                        $scope.ListadoTipoTransporteRemsaPaqueteria = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de tipo despacho*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DESPACHO_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoDespachoRemesaPaqueteria = [];
                    if (response.data.Datos.length > CERO) {
                        $scope.ListadoTipoDespachoRemesaPaqueteria = response.data.Datos;

                        $scope.Modelo.TipoDespachoRemesaPaqueteria = $scope.ListadoTipoDespachoRemesaPaqueteria[CERO]

                    }
                    else {
                        $scope.ListadoTipoDespachoRemesaPaqueteria = []
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de tipo servicio*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SERVICIO_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoServicioRemesaPaqueteria = [];
                    if (response.data.Datos.length > CERO) {
                        $scope.ListadoTipoServicioRemesaPaqueteria = response.data.Datos;

                        $scope.Modelo.TipoServicioRemesaPaqueteria = $scope.ListadoTipoServicioRemesaPaqueteria[CERO]

                    }
                    else {
                        $scope.ListadoTipoServicioRemesaPaqueteria = []
                    }
                }
            }, function (response) {
            });
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        function DatosRequeridos() {
            var continuar = true
            $scope.MensajesError = []
            var Modelo = $scope.Modelo
            if (Modelo.NumeroInicial !== undefined && Modelo.NumeroInicial > 0 && Modelo.NumeroFinal !== undefined && Modelo.NumeroFinal > 0) {
                if (Modelo.NumeroInicial > Modelo.NumeroFinal) {
                    $scope.MensajesError.push('El número inicial no puede ser mayor al número final')
                    continuar = false
                }
            }
            if (Modelo.FechaInicial !== undefined && Modelo.FechaFinal !== undefined) {
                if (Modelo.FechaInicial > Modelo.FechaFinal) {
                    $scope.MensajesError.push('La fecha inicial no puede ser mayor a la fecha final')
                    continuar = false
                }
            }
            if (Modelo.NumeroPlanillaInicial !== undefined && Modelo.NumeroPlanillaInicial > 0 && Modelo.NumeroPlanillaFinal !== undefined && Modelo.NumeroPlanillaFinal > 0) {
                if (Modelo.NumeroPlanillaInicial > Modelo.NumeroPlanillaFinal) {
                    $scope.MensajesError.push('El número inicial de la planilla no puede ser mayor al número final')
                    continuar = false
                }
            }
            if (Modelo.FechaPlanillaInicial !== undefined && Modelo.FechaPlanillaFinal) {
                if (Modelo.FechaPlanillaInicial > Modelo.FechaPlanillaInicial) {
                    $scope.MensajesError.push('La fecha inicial de la planilla no puede ser mayor a la fecha final')
                    continuar = false
                }
            }
            return continuar
        }
        function Find() {
            if (DatosRequeridos()) {

                blockUI.start('Buscando registros ...');

                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);
                $scope.Buscando = true;
                $scope.MensajesError = [];
                $scope.ListadoRemesas = [];
                if ($scope.Buscando) {
                    if ($scope.MensajesError.length == 0) {
                        blockUI.delay = 1000;
                        //$scope.Modelo.NumeroPlanilla = -1
                        $scope.Modelo.Estado = 1;
                        $scope.Modelo.Detalle;
                        $scope.Modelo.Cumplido = 0;
                        $scope.Modelo.TipoDocumento = { Codigo: 100 };
                        $scope.Modelo.Oficina = { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo };
                        $scope.Modelo.Pagina = $scope.paginaActual;
                        $scope.Modelo.RegistrosPagina = $scope.cantidadRegistrosPorPaginas;
                        $scope.Modelo.Cumplida = CODIGO_UNO;
                        RemesasFactory.ConsultarCumplidoRemesas($scope.Modelo).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    $scope.Modelo.Codigo = 0
                                    if (response.data.Datos.length > 0) {
                                        for (var i = 0; i < response.data.Datos.length; i++) {
                                            response.data.Datos[i].CantidadRecibe = response.data.Datos[i].CantidadCliente;
                                            response.data.Datos[i].PesoRecibe = response.data.Datos[i].PesoCliente;
                                            $scope.ListadoRemesas.push(response.data.Datos[i])

                                        }
                                        if ($scope.ListadoRemesas.length == 0) {
                                            $scope.totalRegistros = 0;
                                            $scope.totalPaginas = 0;
                                            $scope.paginaActual = 1;
                                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                            $scope.Buscando = false;
                                        }
                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                    }
                                    else {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                    var listado = [];
                                    response.data.Datos.forEach(function (item) {
                                        listado.push(item);
                                    });
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }
                blockUI.stop();
            }
        }
        $scope.SlcTodo = function (Seleccion) {
            if (Seleccion) {
                for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                    $scope.ListadoRemesas[i].Seleccionado = true
                }
            } else {
                for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                    $scope.ListadoRemesas[i].Seleccionado = false
                }
            }
        }

        $scope.ValidarRemesasCumplir = function (Remesas) {
            $scope.RemesasCumplir = []
            for (var i = 0; i < Remesas.length; i++) {
                if (Remesas[i].Seleccionado) {
                    Continuar = true;
                    Remesas[i].CantidadRecibida = Remesas[i].CantidadCliente;
                    Remesas[i].PesoRecibido = Remesas[i].PesoCliente;
                    var hoy = new Date()
                    if (
                        Remesas[i].IdentificacionRecibe == undefined || Remesas[i].IdentificacionRecibe == '' ||
                        Remesas[i].NombreRecibe == undefined || Remesas[i].NombreRecibe == '' ||
                        Remesas[i].CantidadRecibida === undefined || Remesas[i].CantidadRecibida === '' || Remesas[i].CantidadRecibe > Remesas[i].CantidadRecibida ||
                        Remesas[i].PesoRecibido === undefined || Remesas[i].PesoRecibido === '' || Remesas[i].PesoRecibe > Remesas[i].PesoRecibido ||
                        Remesas[i].FechaRecibe == undefined || Remesas[i].FechaRecibe == '' || Remesas[i].FechaRecibe == null || Remesas[i].FechaRecibe > hoy
                    ) {
                        $scope.ListadoRemesas[i].stRow = 'background:yellow';
                        Continuar = false;
                    } else {
                        $scope.ListadoRemesas[i].stRow = ''
                    }
                    $scope.RemesasCumplir.push(Remesas[i])
                }
            }

            if ($scope.RemesasCumplir.length > 0) {
                if (Continuar) {
                    $scope.Cumplir()
                } else {
                    ShowError('Por favor complete los campos de identificación, nombre, fecha (Menor a la actual), cantidad y peso recibido(no superiores a los valores de la remesa) en los registros seleccionados')
                    showModal('ModalErrores')
                }
            } else {
                ShowError('Debe seleccionar al menos una remesa para cumplir')
            }
        }

        $scope.Cumplir = function () {
            showModal('modalConfirmacionGuardar')
        }

        $scope.ConfirmacionCumplir = function () {
            var Modelo = {
                CumplidoRemesas: []
            }
            for (var i = 0; i < $scope.RemesasCumplir.length; i++) {
                var item = $scope.RemesasCumplir[i]
                Modelo.CumplidoRemesas.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    NumeroRemesa: item.Numero,
                    FechaRecibe: item.FechaRecibe,
                    NombreRecibe: item.NombreRecibe,
                    IdentificacionRecibe: item.IdentificacionRecibe,
                    TelefonoRecibe: item.TelefonoRecibe,
                    ObservacionesRecibe: item.ObservacionesRecibe,
                    CantidadRecibida: item.CantidadRecibida,
                    PesoRecibido: item.PesoRecibido,
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo }
                })
            }
            RemesasFactory.CumplirRemesas(Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Remesas cumplidas correctamente');
                        closeModal('ModalErrores')
                        closeModal('modalConfirmacionGuardar')
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError = response.statusText

                });

        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item)
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };
    }]);