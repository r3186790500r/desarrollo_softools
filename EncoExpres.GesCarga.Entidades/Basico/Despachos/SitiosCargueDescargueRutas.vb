﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" SitiosCargueDescargue"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class SitiosCargueDescargueRutas
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SitiosCargueDescargue"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SitiosCargueDescargue"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Sitio = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo"), .Nombre = Read(lector, "SitioCargueDescargue")}
            TipoVehiculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIVE_Codigo"), .Nombre = Read(lector, "TipoVehiculo")}
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo")}
            ValorCargue = Read(lector, "Valor_Cargue")
            ValorDescargue = Read(lector, "Valor_Descargue")
            AplicaAnticipo = Read(lector, "Aplica_Anticipo")
        End Sub


        <JsonProperty>
        Public Property ValorDescargue As Long
        <JsonProperty>
        Public Property ValorCargue As Long
        <JsonProperty>
        Public Property Sitio As SitiosCargueDescargue
        <JsonProperty>
        Public Property TipoVehiculo As ValorCatalogos
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property AplicaAnticipo As Integer
    End Class
End Namespace
