﻿EncoExpresApp.controller("ConsultarInterfazContablePslCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'InterfazContablePSLFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ValorCatalogosFactory, InterfazContablePSLFactory) {
        $scope.Titulo = 'CONSULTAR PROCESOS PSL';
        $scope.MapaSitio = [{ Nombre: 'Contabilidad' }, { Nombre: 'Procesos' }, { Nombre: 'Consultar Proceso PSL' }];

        //---------------------------------------------VARIABLES---------------------------------------------//
        var NullDate = '1/01/1900';
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.Deshabilitar = false;
        $scope.MensajesError = [];
        var filtros = {};
        $scope.pref = '';
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONSULTAR_PROCESOS_PSL);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.ListadoIntefazContablePSL = [];
        $scope.ListadoTipoDocumentos = [];

        //---------------------------------------------FUNCION DE PAGINACIÓN---------------------------------------------//
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        //---------------------------------------------FUNCION DE PAGINACIÓN---------------------------------------------//
        //------------------------------------------------------------OBTENER INFORMACION------------------------------------------------------------//
        //-- catalogo tipo documento psl
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DOUMENTO_PSL } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoDocumentos.push({ Nombre: '(TODOS)', Codigo: -1 });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoTipoDocumentos.push({ Codigo: response.data.Datos[i].CampoAuxiliar2, Nombre: response.data.Datos[i].Nombre })
                        }
                        $scope.TipoDocumentos = $linq.Enumerable().From($scope.ListadoTipoDocumentos).First('$.Codigo == -1');
                    }
                    else {
                        $scope.ListadoTipoDocumentos = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //------------------------------------------------------------FUNCIONES GENERALES ------------------------------------------------------------//
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    Find();
                }
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoIntefazContablePSL = [];
            if ($scope.Buscando) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumentoOrigen: $scope.Numero,
                    CATATipoDocumento: { Codigo: $scope.TipoDocumentos.Codigo },
                    FechaInicial: $scope.FechaInicial,
                    FechaFinal: $scope.FechaFinal,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                }
                blockUI.delay = 1000;
                InterfazContablePSLFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoIntefazContablePSL = response.data.Datos

                                for (var i = 0; i < $scope.ListadoIntefazContablePSL.length; i++) {
                                    var fechavalMensaje = new Date($scope.ListadoIntefazContablePSL[i].FechaMensajePSL);
                                    fechavalMensaje.setHours(0, 0, 0, 0);
                                    var fechaNull = new Date(NullDate);
                                    if (fechavalMensaje.getTime() == fechaNull.getTime()) {
                                        $scope.ListadoIntefazContablePSL[i].FechaMensajePSL = '';
                                    }
                                }

                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.stuatusText);
                    });
                blockUI.stop();
            }
        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicial = new Date();
            $scope.FechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicial === null || $scope.FechaInicial === undefined || $scope.FechaInicial === '')
                && ($scope.FechaFinal === null || $scope.FechaFinal === undefined || $scope.FechaFinal === '')
                && ($scope.Numero === null || $scope.Numero === undefined || $scope.Numero === '' || $scope.Numero === 0 || isNaN($scope.Numero) === true)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.Numero !== null && $scope.Numero !== undefined && $scope.Numero !== '' && $scope.Numero !== 0)
                || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')

            ) {
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                    && ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                    if ($scope.FechaFinal < $scope.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }
                else {
                    if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                        $scope.FechaFinal = $scope.FechaInicial;
                    } else {
                        $scope.FechaInicial = $scope.FechaFinal;
                    }
                }
            }
            if ($scope.Numero === undefined || $scope.Numero === '' || $scope.Numero === null)
                $scope.Numero = 0;

            return continuar;
        }
    }]);