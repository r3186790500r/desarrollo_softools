import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/internal/operators';
// Interfaces
import { Respuesta, Departamento } from '../interfaces/ifaGeneral';
// Constantes
const UrlApiGuardar = "http://localhost:50395/api/v1/Departamentos/Guardar";
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token',
    'observe': 'response'
  })
}

@Injectable({
  providedIn: 'root'
})
export class serDepartamentos {
  
  constructor(protected http: HttpClient) { }

  public apiGuardar(objDepartamento: Departamento): Observable<Respuesta> {

    return this.http.post<Respuesta>(UrlApiGuardar,
      {
        CodigoEmpresa: objDepartamento.CodigoEmpresa,
        Pais: { Codigo: objDepartamento.Pais.Codigo },
        Nombre: objDepartamento.Nombre,
        CodigoAlterno: objDepartamento.CodigoAlterno,
        Estado: objDepartamento.Estado,
        UsuarioCrea: objDepartamento.UsuarioCrea
      }, httpOptions);
  }
  
}
