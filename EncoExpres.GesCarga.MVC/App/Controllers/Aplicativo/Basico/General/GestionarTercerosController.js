﻿EncoExpresApp.controller("GestionarTercerosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'SitiosCargueDescargueFactory', 'CiudadesFactory', 'TercerosFactory',
    'ValorCatalogosFactory', 'BancosFactory', 'DocumentosFactory', 'GestionDocumentosFactory', 'TarifarioVentasFactory', 'TarifarioComprasFactory', 'ImpuestosFactory',
    'EventoCorreosFactory', 'PaisesFactory', 'blockUIConfig', 'ZonasFactory', 'OficinasFactory', 'ProductoTransportadosFactory', 'UnidadMedidaFactory', 'UsuariosFactory', 'LineaNegocioTransportesFactory',
    'TipoLineaNegocioTransportesFactory', 'TarifaTransportesFactory', 'ProductoTransportadosFactory', 'ServicioSIESAFactory', 'TipoTarifaTransportesFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, SitiosCargueDescargueFactory, CiudadesFactory, TercerosFactory,
        ValorCatalogosFactory, BancosFactory, DocumentosFactory, GestionDocumentosFactory, TarifarioVentasFactory, TarifarioComprasFactory, ImpuestosFactory,
        EventoCorreosFactory, PaisesFactory, blockUIConfig, ZonasFactory, OficinasFactory, ProductoTransportadosFactory, UnidadMedidaFactory, UsuariosFactory, LineaNegocioTransportesFactory,
        TipoLineaNegocioTransportesFactory, TarifaTransportesFactory, ProductoTransportadosFactory, ServicioSIESAFactory, TipoTarifaTransportesFactory) {
        console.clear();
        $scope.Titulo = 'GESTIONAR TERCEROS';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Terceros' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_TERCEROS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.DeshabilitarCrear = $scope.Permisos.DeshabilitarCrear;
        $scope.ListaCorreos = [];
        $scope.ListaSitios = [];
        $scope.ListadoDocumentos = [];
        $scope.ListadoFotos = [];
        $scope.ListadoFotosEnviar = [];
        $scope.ContMedios = 0
        $scope.perfilConductor = false;
        $scope.DeshabilitarSede = false;
        $scope.ListadoDirecciones = [{ Direccion: '', Codigo: 0, Estado: 1 }];
        $scope.SitiosTerceroCliente = [];
        $scope.IdPosiction = 0;
        $scope.Documento = {};
        $scope.DeshabilitarEstado = false;


        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            CodigoAlterno: 0,
            CodigoContable: 0,
            DigitoChequeo: 0,
            Codigo: 0,
            Cliente: { Codigo: 0, Impuestos: [] },
            Conductor: { Codigo: 0 },
            Empleado: { Codigo: 0 },
            Proveedor: { Codigo: 0 },
            Observaciones: '',
            SitiosTerceroCliente: [{}],
            UsuaCodigoCrea: $scope.Sesion.UsuarioAutenticado.Codigo,
            TerceroClienteCupoSedes: [],
            CondicionesPeso: [],
            Estado: 1,
            FormasDePago: []
        };
        $scope.ListaResultadoFiltroConsultaSitios = [];
        $scope.SitiosTerceroClienteTotal = [];
        $scope.ListaSitiosGrid = [];
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.totalPaginas = 0;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.ListadoZonas = [];
        $scope.BloquearZona = true;
        $scope.ListadoDireccionesTercero = [];
        $scope.VerificacionIdentificaion = true;
        $scope.DeshabilitarNoId = false;
        $scope.VerCondicionesComerciales = false;
        /* ObtenerObtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.PermisoGuardar = !$scope.DeshabilitarActualizar;
            $scope.Modelo.Codigo = $routeParams.Codigo;
            $scope.VerificacionIdentificaion = false;
            $scope.DeshabilitarNoId = true;
        }
        else {
            $scope.PermisoGuardar = !$scope.DeshabilitarCrear;
            $scope.Modelo.Codigo = 0;
        }
        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */
        $scope.ListaPerfiles = [];
        $scope.ListadoPaises = [];
        $scope.MensajesErrorRecorrido = [];
        $scope.ListadoTipoDocumentoVehiculos = [];
        $scope.ListadoTipoDocumentoVehiculosVerificados = [];
        $scope.DeshabilitarEstado = false;
        $scope.DeshabilitarCondicionesComerciales = true;
        $scope.ItemFoto = {};
        $scope.Modelo.Foto = '';
        $('#FotoCargada').hide();
        $scope.ListadoCiudades = [];
        $scope.ListadoDirecciones = [];
        $scope.ListadoNovedades = [];
        $scope.ListadoCompletoNovedades = [];
        $scope.ListaNovedadesTercero = [];
        $scope.ModalNovedades = {};
        $scope.NumeroPaginaNovedades = 1;
        $scope.CantidadRegistrosPorPaginaNovedades = 15;
        $scope.ListadoFiltradoNovedadesTercero = [];
        $scope.totalPaginasNovedades = 0;
        $scope.Modelo.Conductor.BloqueadoAltoRiesgo = true
        $scope.contTarifarios = 0;
        var ResponseNovedadesTercero = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NOVEDADES_TERCERO }, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
        if (ResponseNovedadesTercero != undefined) {
            $scope.ListadoCompletoNovedades = ResponseNovedadesTercero;
        }


        /*Cargar el combo de LineaNegocioTransportes*/
        var ResponseLineaNegocio = LineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true });

        if (ResponseLineaNegocio.ProcesoExitoso === true) {
            $scope.ListadoLineaNegocioTransportes = [];
            if (ResponseLineaNegocio.Datos.length > 0) {
                // $scope.ListadoLineaNegocioTransportes.push({ Nombre: '(NO APLICA)' });
                for (var i = 0; i < ResponseLineaNegocio.Datos.length; i++) {
                    $scope.ListadoLineaNegocioTransportes.push(ResponseLineaNegocio.Datos[i]);
                }
                //$scope.Modal.LineaNegocioTransportes = $scope.ListadoLineaNegocioTransportes[0]

            }
            else {
                $scope.ListadoLineaNegocioTransportes = []
            }
        }


        /*Cargar el combo de TipoLineaNegocioTransportes*/
        var ResponseTipoLineaNegocio = TipoLineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true });

        if (ResponseTipoLineaNegocio.ProcesoExitoso === true) {
            $scope.ListadoTipoLineaNegocioTransportes = [];
            if (ResponseTipoLineaNegocio.Datos.length > 0) {
                $scope.ListadoTipoLineaNegocioTransportes = ResponseTipoLineaNegocio.Datos;

            }
            else {
                $scope.ListadoTipoLineaNegocioTransportes = []
            }
        }


        /*Cargar el combo de TarifaTransportes*/
        var ResponseTarifaTransportes = TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true });

        if (ResponseTarifaTransportes.ProcesoExitoso === true) {
            $scope.ListadoTarifaTransportes = [];
            if (ResponseTarifaTransportes.Datos.length > 0) {
                $scope.ListadoTarifaTransportes = ResponseTarifaTransportes.Datos;
                //$scope.Modal.TarifaTransportes = $scope.ListadoTarifaTransportes[0]

            }
            else {
                $scope.ListadoTarifaTransportes = []
            }
        }


        /*ListadoProductosTransportados*/
        var ResponseProductosTransportados = ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, AplicaTarifario: 1, Sync: true });

        if (ResponseProductosTransportados.ProcesoExitoso === true) {
            $scope.ListadoProductosTransportados = [];
            if (ResponseProductosTransportados.Datos.length > 0) {
                $scope.ListadoProductosTransportados = ResponseProductosTransportados.Datos;
                for (var j = 0; j < $scope.ListadoProductosTransportados.length; j++) {
                    $scope.ListadoProductosTransportados[j] = { Codigo: $scope.ListadoProductosTransportados[j].Codigo, Nombre: $scope.ListadoProductosTransportados[j].Nombre }
                }

            }
            else {
                $scope.ListadoProductosTransportados = []
            }
        }


        /*Cargar el combo de TipoTarifaTransportes*/
        var ResponseTipoTarifa = TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true });

        if (ResponseTipoTarifa.ProcesoExitoso === true) {
            $scope.ListadoTipoTarifaTransportes = [];
            if (ResponseTipoTarifa.Datos.length > 0) {
                $scope.ListadoTipoTarifaTransportes = ResponseTipoTarifa.Datos;

            }
            else {
                $scope.ListadoTipoTarifaTransportes = []
            }
        }

        $scope.ListadoDirecciones.push({ Direccion: '', Codigo: 0, Estado: 1 })

        $scope.AsignarJustificacionBloqueo = function (cod) {
            if (cod == 0) {
                $('#JustificacionBloqueo').show();
            } else {
                $('#JustificacionBloqueo').hide();
                $scope.Modelo.JustificacionBloqueo = '';
            }
        };
        //--------------------------------------------------------------------------------------------------------------------------|
        //-----------------------------------------------AUTOCOMPLETES--------------------------------------------------------------|
        //--------------------------------------------------------------------------------------------------------------------------|
        //--Ciudades
        $scope.ListadoCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        };
        //--Ciudades
        //--Emisor
        $scope.ListadoEmisor = [];
        $scope.AutocompleteEmisor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_PROVEEDOR,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoEmisor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoEmisor)
                }
            }
            return $scope.ListadoEmisor
        }
        //--Emisor
        //--------------------------------------------------------------------------------------------------------------------------|


        //Cargar Listado Tipo Documentos Gestión Cliente:
        $scope.ListadoTiposDocumentoGestionCliente = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DOCUMENTOS_GESTION_CLIENTE }, Estado: ESTADO_ACTIVO, Sync: true }).Datos;


        // Cargar Listado Tipo Documentos Gestión Cliente:
        $scope.ListadoTipoGestionConductorDocumentos = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_GESTION_CONDUCTOR_DOCUMENTOS }, Estado: ESTADO_ACTIVO, Sync: true }).Datos;

        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ];

        /*Cargar el combo de forma pago */
        var ResponseFormasPago = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS }, Estado: 1, Sync: true })
        //then(function (response) {
        if (ResponseFormasPago.ProcesoExitoso === true) {
            $scope.ListadoFormaPago = [];
            if (ResponseFormasPago.Datos.length > 0) {
                $scope.ListadoFormaPago = ResponseFormasPago.Datos;
                $scope.ListadoFormaPago.splice(0, 1);
                try {
                    $scope.Modelo.Cliente.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.Modelo.Cliente.FormaPago.Codigo);


                } catch (e) {
                    try {
                        $scope.Modelo.Cliente.FormaPago = $scope.ListadoFormaPago[0]
                    } catch (e) {
                    }
                }
            }
            else {
                $scope.ListadoFormaPago = []
                console.log('lista vacía')
            }
        } else {
            console.log('No se encontro la lista')
        }

        $scope.CargarDatosFunciones = function () {
            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

            //$scope.ListadoEstados = [
            //    { Codigo: 1, Nombre: "ACTIVO" },
            //    { Codigo: 0, Nombre: "INACTIVO" },
            //];
            try {
                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado.Codigo);
                $scope.AsignarJustificacionBloqueo($scope.Modelo.Estado.Codigo);
            } catch (e) {
                $scope.Modelo.Estado = $scope.ListadoEstados[0];
            }

            /* Cargar combo de sitios cargue y descargue*/
            SitiosCargueDescargueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaSitios = response.data.Datos;
                            try {
                                for (var i = 0; i < $scope.Modelo.SitiosTerceroCliente.length; i++) {
                                    try {
                                        $scope.Modelo.SitiosTerceroCliente[i].SitioCliente = $linq.Enumerable().From($scope.ListaSitios).First('$.Codigo ==' + $scope.Modelo.SitiosTerceroCliente[i].SitioCliente.Codigo);
                                    } catch (e) { }
                                }
                            } catch (e) { }
                        } else {
                            $scope.ListaSitios = []
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*Cargar el combo de tipo naturalezas*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_NATURALEZA_TERCERO }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoNaturalezas = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNaturalezas = response.data.Datos;
                            try {
                                $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.Modelo.TipoNaturaleza.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoNaturaleza = $scope.ListadoNaturalezas[0]
                            }
                        }
                        else {
                            $scope.ListadoNaturalezas = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo anticipo*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 187 }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoAnticipo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoAnticipo = response.data.Datos;
                            try {
                                $scope.Modelo.TipoAnticipo = $linq.Enumerable().From($scope.ListadoTipoAnticipo).First('$.Codigo ==' + $scope.Modelo.TipoAnticipo.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoAnticipo = $scope.ListadoTipoAnticipo[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoAnticipo = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo identificaciones*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoIdentificacionGeneral = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoIdentificacionGeneral = response.data.Datos;
                            $scope.AsignarTipoIdentificacion()
                        }
                        else {
                            $scope.ListadoTipoIdentificacion = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo naturalezas*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 192 }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoValidacionCupo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoValidacionCupo = response.data.Datos;
                            try {
                                $scope.Modelo.TipoValidacionCupo = $linq.Enumerable().From($scope.ListadoValidacionCupo).First('$.Codigo ==' + $scope.Modelo.TipoValidacionCupo.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoValidacionCupo = $scope.ListadoValidacionCupo[0]
                            }
                        }
                        else {
                            $scope.ListadoValidacionCupo = []
                        }
                    }
                }, function (response) {
                });
            //Combo lista evento correos----------------------------------------------------------------------
            //Funcion agregar Email al formulario-------------------------------------------------------------
            EventoCorreosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaEventoCorreos = [];
                        $scope.ListaEventoCorreos = response.data.Datos;
                        for (var i = 0; i < $scope.ListaEventoCorreos.length; i++) {
                            $scope.ListaEventoCorreos[i].Nombre = $scope.ListaEventoCorreos[i].Nombre.toUpperCase()
                        }
                    }
                }, function (response) {
                    $scope.Buscando = false;
                });
            $scope.AsignarTipoIdentificacion = function () {
                if ($scope.Modelo.TipoNaturaleza != undefined) {
                    if ($scope.Modelo.TipoNaturaleza.Codigo === 501) {
                        $scope.ListadoTipoIdentificacion = []
                        for (var i = 0; i < $scope.ListadoTipoIdentificacionGeneral.length; i++) {
                            if ($scope.ListadoTipoIdentificacionGeneral[i].Codigo !== 102) {
                                $scope.ListadoTipoIdentificacion.push($scope.ListadoTipoIdentificacionGeneral[i])
                            }
                        }
                        try {
                            $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.Modelo.TipoIdentificacion.Codigo);
                        } catch (e) {
                            $scope.Modelo.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                        }
                        $scope.Juridica = false
                        $scope.Modelo.RazonSocial = ''
                        $scope.Modelo.RepresentanteLegal = ''
                        $scope.Natural = true
                    } else if ($scope.Modelo.TipoNaturaleza.Codigo === 502) {
                        $scope.ListadoTipoIdentificacion = []
                        for (var i = 0; i < $scope.ListadoTipoIdentificacionGeneral.length; i++) {
                            if ($scope.ListadoTipoIdentificacionGeneral[i].Codigo === 102) {
                                $scope.ListadoTipoIdentificacion.push($scope.ListadoTipoIdentificacionGeneral[i])
                            }
                        }
                        try {
                            $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.Modelo.TipoIdentificacion.Codigo);
                        } catch (e) {
                            $scope.Modelo.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                        }
                        $scope.Juridica = true
                        $scope.Natural = false
                        $scope.Modelo.Nombre = ''
                        $scope.Modelo.PrimeroApellido = ''
                        $scope.Modelo.SegundoApellido = ''
                        $scope.Modelo.Sexo = $scope.ListadoSexos[0]
                        $scope.Modelo.CiudadExpedicionIdentificacion = ''
                        $scope.Modelo.CiudadNacimiento = ''
                    } else {
                        $scope.Juridica = false
                        $scope.Natural = false
                        $scope.Modelo.RazonSocial = ''
                        $scope.Modelo.RepresentanteLegal = ''
                        $scope.Modelo.Nombre = ''
                        $scope.Modelo.PrimeroApellido = ''
                        $scope.Modelo.SegundoApellido = ''
                        $scope.Modelo.Sexo = $scope.ListadoSexos[0]
                        $scope.Modelo.CiudadExpedicionIdentificacion = ''
                        $scope.Modelo.CiudadNacimiento = ''
                    }

                }

            }
            /*Cargar el combo de Sexos*/
            var responseSexo = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SEXO_TERCERO_PERSONA_NATURAL }, Estado: 1, Sync: true });
            if (responseSexo.ProcesoExitoso) {
                $scope.ListadoSexos = [];
                if (responseSexo.Datos.length > 0) {
                    $scope.ListadoSexos = responseSexo.Datos;
                    try {
                        $scope.Modelo.Sexo = $linq.Enumerable().From($scope.ListadoSexos).First('$.Codigo ==' + $scope.Modelo.Sexo.Codigo);
                    } catch (e) {
                        $scope.Modelo.Sexo = $scope.ListadoSexos[0]
                    }
                }
                else {
                    $scope.ListadoSexos = []
                }
            }



            $('#DiasPlazo').hide()
            $scope.AsignarFormaPago = function (cod) {
                if (cod == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                    $('#DiasPlazo').show();
                } else {
                    $('#DiasPlazo').hide();
                    $scope.Modelo.Cliente.DiasPlazo = 0
                }

            }
            /*Cargar el combo de forma pago */
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_COBRO }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoFormaCobro = [];
                        $scope.ListadoFormaCobro = response.data.Datos;

                        $scope.ListadoFormaCobro.splice(0, 1)
                        try {
                            for (var j = 0; j < $scope.ListadoFormaCobro.length; j++) {
                                for (var i = 0; i < $scope.Modelo.FormasDePago.length; i++) {
                                    if ($scope.Modelo.FormasDePago[i].Codigo == $scope.ListadoFormaCobro[j].Codigo) {
                                        $scope.ListadoFormaCobro[j].Estado = true
                                    }
                                }
                            }
                            $scope.AsignarFormasPago()
                        } catch (e) {
                            for (var i = 0; i < $scope.ListadoFormaCobro.length; i++) {
                                var perfil = $scope.ListadoFormaCobro[i]
                                perfil.Estado = false
                            }
                        }
                        $scope.ListadoFormaCobro.Asc = true
                        OrderBy('Nombre', undefined, $scope.ListadoFormaCobro);

                    }
                    else {
                        $scope.ListadoFormaCobro = []
                    }

                }, function (response) {
                });


            $('#DiasPlazo2').hide()
            $scope.AsignarFormaCobro = function (cod) {
                if (cod == 8801) {
                    $('#DiasPlazo2').show()
                } else {
                    $('#DiasPlazo2').hide()
                    $scope.Modelo.Proveedor.DiasPlazo = 0
                }

            }
            //Tipo Documento Terceros
            //$scope.ListadoTipoDocumentoVehiculos = [];
            //$scope.CargarComboTipoDocumento = function () {
            //    for (var i = 0; i < $scope.ListadoCatalogosCombos.length; i++) {
            //        if ($scope.ListadoCatalogosCombos[i].Catalogo.Codigo == 146) {
            //            $scope.ListadoTipoDocumentoVehiculos.push($scope.ListadoCatalogosCombos[i])
            //        }
            //    }
            //}
            //$scope.CargarComboTipoDocumento();
            /*Cargar el combo de Tipo cta bancarias*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CUENTA_BANCARIA }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoBancos = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoBancos = response.data.Datos;
                            try {
                                $scope.Modelo.TipoBanco = $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo ==' + $scope.Modelo.TipoBanco.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoBanco = $scope.ListadoTipoBancos[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoBancos = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar los check de perfiles*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_PERFIL_TERCEROS }, Estado: 1 }).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.perfilTerceros = response.data.Datos;
                            $scope.perfilTerceros.splice(0, 1)
                            try {
                                for (var j = 0; j < $scope.perfilTerceros.length; j++) {
                                    for (var i = 0; i < $scope.Modelo.Perfiles.length; i++) {
                                        if ($scope.Modelo.Perfiles[i].Codigo == $scope.perfilTerceros[j].Codigo) {
                                            $scope.perfilTerceros[j].Estado = true
                                        }
                                    }
                                }
                                $scope.verificacionPerfiles()
                            } catch (e) {
                                for (var i = 0; i < $scope.perfilTerceros.length; i++) {
                                    var perfil = $scope.perfilTerceros[i]
                                    perfil.Estado = false
                                }
                            }
                            $scope.perfilTerceros.Asc = true
                            OrderBy('Nombre', undefined, $scope.perfilTerceros);

                        }
                        else {
                            $scope.perfilTerceros = []
                        }
                    }
                }, function (response) {
                });

            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 194 }, Estado: 1 }). //Linea Servicio
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.LineaServicioTerceros = response.data.Datos;
                            $scope.ContLineas = 0
                            $scope.LineaServicioTerceros.splice(0, 1)
                            try {
                                for (var j = 0; j < $scope.LineaServicioTerceros.length; j++) {
                                    for (var i = 0; i < $scope.Modelo.LineaServicio.length; i++) {
                                        if ($scope.Modelo.LineaServicio[i].Codigo == $scope.LineaServicioTerceros[j].Codigo) {
                                            $scope.LineaServicioTerceros[j].Estado = true
                                        }
                                    }
                                }

                            } catch (e) {
                                for (var i = 0; i < $scope.LineaServicioTerceros.length; i++) {
                                    var perfil = $scope.LineaServicioTerceros[i]
                                    perfil.Estado = false
                                }
                            }
                            $scope.LineaServicioTerceros.Asc = true
                            OrderBy('Nombre', undefined, $scope.LineaServicioTerceros);
                            $scope.verificacionLineas()
                        }
                        else {
                            $scope.LineaServicioTerceros = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de Tipo Sangres*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DE_SANGRE }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoSangres = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoSangres = response.data.Datos;
                            try {
                                $scope.Modelo.Conductor.TipoSangre = $linq.Enumerable().From($scope.ListadoTipoSangres).First('$.Codigo ==' + $scope.Modelo.Conductor.TipoSangre.Codigo);
                            } catch (e) {
                                $scope.Modelo.Conductor.TipoSangre = $scope.ListadoTipoSangres[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoSangres = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de Tipo Contrato*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONTRATO_CONDUCTOR }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoContrato = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoContrato = response.data.Datos;
                            try {
                                $scope.Modelo.Conductor.TipoContrato = $linq.Enumerable().From($scope.ListadoTipoContrato).First('$.Codigo ==' + $scope.Modelo.Conductor.TipoContrato.Codigo);
                            } catch (e) {
                                $scope.Modelo.Conductor.TipoContrato = $scope.ListadoTipoContrato[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoContrato = []
                        }
                    }
                }, function (response) {
                });

            //LISTADO CATEGORISA CANTIDAD DE VIAJES
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CATEGORIAS_VIAJES_CONDUCTOR }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoViajesConductor = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoViajesConductor = response.data.Datos;
                            try {
                                $scope.Modelo.Conductor.ViajesConductor = $linq.Enumerable().From($scope.ListadoViajesConductor).First('$.Codigo ==' + $scope.Modelo.Conductor.ViajesConductor.Codigo);
                            } catch (e) {
                                $scope.Modelo.Conductor.ViajesConductor = $scope.ListadoViajesConductor[0]
                            }

                        }
                        else {
                            $scope.ListadoViajesConductor = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de Tipo Contrato empleados*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONTRATO_EMPLEADO }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoContratoEmpleados = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoContratoEmpleados = response.data.Datos;
                            try {
                                $scope.Modelo.Empleado.TipoContrato = $linq.Enumerable().From($scope.ListadoTipoContratoEmpleados).First('$.Codigo ==' + $scope.Modelo.Empleado.TipoContrato.Codigo);
                            } catch (e) {
                                $scope.Modelo.Empleado.TipoContrato = $scope.ListadoTipoContratoEmpleados[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoContrato = []
                        }
                    }
                }, function (response) {
                });

            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MODALIDAD_FACTURACION_PESO_CLIENTE }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaModalidadFacturacionPeso = [];
                        $scope.ListaModalidadFacturacionPeso.push({ Codigo: 0, Nombre: '(NO APLICA)' });
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                $scope.ListaModalidadFacturacionPeso.push(item);
                            });
                        }

                        if ($scope.CodigoModalidadFacturacion !== undefined && $scope.CodigoModalidadFacturacion !== null && $scope.CodigoModalidadFacturacion > 0) {
                            $scope.Modelo.ModalidadFacturacionPeso = $linq.Enumerable().From($scope.ListaModalidadFacturacionPeso).First('$.Codigo ==' + $scope.CodigoModalidadFacturacion);
                        }

                    }
                });

            /*Cargar el combo de Categoria Licencia*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CATEGORIAS_LICENCIAS_DE_CONDUCCION }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCategorias = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCategorias = response.data.Datos;
                            try {
                                $scope.Modelo.Conductor.CategoriaLicencia = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo ==' + $scope.Modelo.Conductor.CategoriaLicencia.Codigo);
                            } catch (e) {
                                $scope.Modelo.Conductor.CategoriaLicencia = $scope.ListadoCategorias[0]
                            }
                        }
                        else {
                            $scope.ListadoCategorias = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de departamento empleado*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DEPARTAMENTO_EMPLEADOS }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDepartamentosEmpleado = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoDepartamentosEmpleado = response.data.Datos;
                            try {
                                $scope.Modelo.Empleado.Departamento = $linq.Enumerable().From($scope.ListadoDepartamentosEmpleado).First('$.Codigo ==' + $scope.Modelo.Empleado.Departamento.Codigo);
                            } catch (e) {
                                $scope.Modelo.Empleado.Departamento = $scope.ListadoDepartamentosEmpleado[0]
                            }
                        }
                        else {
                            $scope.ListadoDepartamentosEmpleado = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de cargos*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CARGOS }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCargos = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCargos = response.data.Datos;
                            try {
                                $scope.Modelo.Empleado.Cargo = $linq.Enumerable().From($scope.ListadoCargos).First('$.Codigo ==' + $scope.Modelo.Empleado.Cargo.Codigo);
                            } catch (e) {
                                $scope.Modelo.Empleado.Cargo = $scope.ListadoCargos[0]
                            }
                        }
                        else {
                            $scope.ListadoCargos = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de Bancos*/
            BancosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoBancos = [];
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoBancos.push(item);
                        });
                        try {
                            $scope.Modelo.Cliente.Impuestos
                            $scope.Modelo.Banco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + $scope.Modelo.Banco.Codigo);
                        } catch (e) {
                            $scope.Modelo.Banco = $scope.ListadoBancos[0]
                        }
                    }
                }, function (response) {
                });

            /*Cargar Autocomplete de Grupos empresariales*/
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_COMERCIAL, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoRepresentanteComerciales = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoRepresentanteComerciales = response.data.Datos;
                            try {
                                if ($scope.Modelo.Cliente.RepresentanteComercial.Codigo > 0) {
                                    $scope.Modelo.Cliente.RepresentanteComercial = $linq.Enumerable().From($scope.ListadoRepresentanteComerciales).First('$.Codigo ==' + $scope.Modelo.Cliente.RepresentanteComercial.Codigo);
                                }
                            } catch (e) {
                                $scope.Modelo.RepresentanteComercial = ''
                            }
                        }
                        else {
                            $scope.ListadoGrupoEmpresariales = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            $scope.AutocompleteAnalistasCartera = function (value) {

                var ResponseAnalistasCartera = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_EMPLEADO, ValorAutocomplete: value, Sync: true, Estado: ESTADO_ACTIVO }).Datos;

                return ResponseAnalistasCartera;

            }

            /*Cargar el combo de paises*/
            PaisesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.push({ Nombre: 'Seleccione país', Codigo: 0 })
                            $scope.ListadoPaises = response.data.Datos;
                            if ($scope.CodigoPais > 0) {
                                $scope.Modelo.Pais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.CodigoPais);
                            }
                            else {
                                $scope.Modelo.Pais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.Sesion.Empresa.Pais.Codigo);
                            }
                        } else {
                            $scope.ListadoPaises = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            //Elimina todos los archivos temporales asociados a este usuario
            DocumentosFactory.EliminarDocumento({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo } }).
                then(function (response) {
                }, function (response) {
                    ShowError(response.statusText);
                });

            //Carga los documentos
            GestionDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, EntidadDocumento: { Codigo: 2 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDocumentos = []
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                var item = response.data.Datos[i]
                                if (item.Habilitado == 1) {
                                    if (item.Documento.Codigo == 1) {

                                        $scope.ListadoFotos = [];

                                        for (var f = 0; f < item.Cantidad_repeticiones; f++) {
                                            var temp = angular.copy(item);
                                            temp.IDNew = f;
                                            try {
                                                temp.FotoCargada = $scope.tempfotos[f].FotoCargada
                                                temp.Extension = $scope.tempfotos[f].Extension
                                                temp.ExtensionDocumento = $scope.tempfotos[f].Extension
                                                temp.ArchivoCargado = { name: $scope.tempfotos[f].NombreDocumento, type: $scope.tempfotos[f].Tipo }
                                                temp.Archivo = $scope.tempfotos[f].Archivo
                                                temp.Tipo = $scope.tempfotos[f].Tipo
                                                $scope.ListadoFotos.push(temp);
                                                $('#FotoCargada' + f.toString()).show()

                                            } catch (e) {
                                                $scope.ListadoFotos.push(temp);

                                            }
                                        }

                                        //$scope.ItemFoto = item
                                    }
                                    else {
                                        $scope.ListadoDocumentos.push(item)
                                    }
                                }

                            }
                            try {
                                var esImagen = false
                                if ($scope.Modelo.Documentos.length > 0) {
                                    for (var j = 0; j < $scope.ListadoDocumentos.length; j++) {
                                        $scope.ListadoDocumentos[j]
                                        for (var k = 0; k < $scope.Modelo.Documentos.length; k++) {
                                            var doc2 = $scope.Modelo.Documentos[k]
                                            if ($scope.ListadoDocumentos[j].Codigo == doc2.Codigo) {
                                                $scope.ListadoDocumentos[j].Referencia = doc2.Referencia
                                                $scope.ListadoDocumentos[j].Emisor = doc2.Emisor
                                                try {
                                                    if (new Date(doc2.FechaEmision) < MIN_DATE) {
                                                        $scope.ListadoDocumentos[j].FechaEmision = ''
                                                    } else {
                                                        $scope.ListadoDocumentos[j].FechaEmision = new Date(doc2.FechaEmision)
                                                    }

                                                } catch (e) {
                                                    $scope.ListadoDocumentos[j].FechaEmision = ''
                                                }
                                                try {
                                                    if (new Date(doc2.FechaVence) < MIN_DATE) {
                                                        $scope.ListadoDocumentos[j].FechaVence = ''
                                                    } else {
                                                        $scope.ListadoDocumentos[j].FechaVence = new Date(doc2.FechaVence)
                                                    }
                                                } catch (e) {
                                                    $scope.ListadoDocumentos[j].FechaVence = ''
                                                }
                                                $scope.ListadoDocumentos[j].ValorDocumento = doc2.ValorDocumento

                                            } else {
                                            }
                                        }
                                    }
                                    $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListadoSemirremolques).First('$.Codigo ==' + $scope.Modelo.Semirremolque.Codigo);
                                }
                            } catch (e) {
                            }
                            $scope.AsignarLicencia2();
                        }
                        else {
                            $scope.Documentos = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            TarifarioVentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
                then(function (response) {
                    $scope.ListadoTarifarios = [];
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTarifarios = response.data.Datos
                            try {
                                $scope.Modelo.Cliente.Tarifario = $linq.Enumerable().From($scope.ListadoTarifarios).First('$.Codigo ==' + $scope.Modelo.Cliente.Tarifario.Codigo);
                            } catch (e) {
                                $scope.Modelo.Cliente.Tarifario = $scope.ListadoTarifarios[0]
                            }

                            try {
                                for (var j = 0; j < $scope.ListadoTarifarios.length; j++) {
                                    for (var i = 0; i < $scope.Modelo.Cliente.TarifariosPaq.length; i++) {
                                        if ($scope.Modelo.Cliente.TarifariosPaq[i].Codigo == $scope.ListadoTarifarios[j].Codigo) {
                                            $scope.ListadoTarifarios[j].Habilitado = true
                                            $scope.CargarValoresPaqueteria($scope.ListadoTarifarios[j])
                                        }
                                    }
                                }
                            } catch (e) {
                                for (var i = 0; i < $scope.ListadoTarifarios.length; i++) {
                                    var tarif = $scope.ListadoTarifarios[i]
                                    tarif.Estado = false
                                }
                            }
                            $scope.ListadoTarifarios.Asc = true
                            OrderBy('Nombre', undefined, $scope.ListadoTarifarios);

                        }
                        else {
                            $scope.ListadoTarifarios = []
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            TarifarioComprasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
                then(function (response) {
                    $scope.ListadoTarifariosCompra = [];
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTarifariosCompra = response.data.Datos
                        try {
                            $scope.Modelo.Proveedor.Tarifario = $linq.Enumerable().From($scope.ListadoTarifariosCompra).First('$.Codigo ==' + $scope.Modelo.Proveedor.Tarifario.Codigo);
                        } catch (e) {
                            $scope.Modelo.Proveedor.Tarifario = $scope.ListadoTarifariosCompra[0]
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*Cargar los impuestos*/
            ImpuestosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoImpuestos = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoImpuestos = response.data.Datos;
                            $scope.ListadoImpuestosVenta = []
                            $scope.ListadoImpuestosCompra = []
                            for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                                if ($scope.ListadoImpuestos[i].Tipo_Impuesto.Codigo == CODIGO_TIMPO_IMPUESTO_COMPRA) {
                                    $scope.ListadoImpuestosCompra.push($scope.ListadoImpuestos[i])
                                }
                                if ($scope.ListadoImpuestos[i].Tipo_Impuesto.Codigo == CODIGO_TIMPO_IMPUESTO_VENTA) {
                                    $scope.ListadoImpuestosVenta.push($scope.ListadoImpuestos[i])
                                }
                            }
                            //try {
                            //    $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.Modelo.TipoNaturaleza.Codigo);
                            //} catch (e) {
                            //    $scope.Modelo.TipoNaturaleza = $scope.ListadoNaturalezas[0]
                            //}
                        }
                        else {
                            $scope.ListadoImpuestos = []
                        }
                    }
                }, function (response) {
                });

            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1, CodigoCiudad: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoDireccionesTercero.push({ Nombre: '(TODAS)', Codigo: -1 });
                            response.data.Datos.forEach(function (item) {
                                if (item.Codigo !== 0) {
                                    $scope.ListadoDireccionesTercero.push(item);
                                }
                            });
                            $scope.ModeloOficinasCiudad = $scope.ListadoDireccionesTercero[0];
                        }
                        else {
                            $scope.ModeloOficinasCiudad = $scope.ListadoDireccionesTercero[0];
                        }
                    }

                }, function (response) {
                });
        }

        //---------------- Evento Correos Terceros ---------------- //
        var isEditEventoCorreo = false;
        $scope.ModalEventoCorreo = {
            EventoCorreo: '',
            TerceroDireccion: '',
            Email: '',
        }
        $scope.AgregarEventoCorreo = function () {
            LimpiarmodalEventoCorreo();
            showModal('modalEventoCorreo');
        };

        function LimpiarmodalEventoCorreo() {
            $scope.ModalEventoCorreo = {
                EventoCorreo: '',
                TerceroDireccion: '',
                Email: '',
            }
            $scope.ModalEventoCorreo.EventoCorreo = $linq.Enumerable().From($scope.ListaEventoCorreos).First('$.Codigo == ' + $scope.ListaEventoCorreos[0].Codigo);
            $scope.ModalEventoCorreo.TerceroDireccion = $linq.Enumerable().From($scope.ListadoDireccionesTercero).First('$.Codigo == -1');
            $scope.MensajesErrorEventoCorreo = [];
            isEditEventoCorreo = false;
        }

        $scope.CargarEventoCorreo = function () {
            if (DatosRequeridosEventoCorreo()) {
                if (isEditEventoCorreo == false) {
                    if ($scope.ModalEventoCorreo.TerceroDireccion.Codigo == -1) {
                        $scope.ModalEventoCorreo.TerceroDireccion = {};
                    }
                    $scope.ListaCorreos.push($scope.ModalEventoCorreo);
                    OrdenarIndicesEventoCorreo();
                }
                else {
                    if ($scope.ModalEventoCorreo.TerceroDireccion.Codigo == -1) {
                        $scope.ModalEventoCorreo.TerceroDireccion = {};
                    }
                    $scope.ListaCorreos[$scope.ModalEventoCorreo.indice] = $scope.ModalEventoCorreo;
                }
                closeModal('modalEventoCorreo');
            }
        };

        function DatosRequeridosEventoCorreo() {
            $scope.MensajesErrorEventoCorreo = [];
            var continuar = true;

            if ($scope.ModalEventoCorreo.EventoCorreo.Codigo <= 0) {
                $scope.MensajesErrorEventoCorreo.push('Debe seleccionar un evento correo');
                continuar = false;
            }

            if ($scope.ModalEventoCorreo.TerceroDireccion.Codigo <= 0) {
                $scope.MensajesErrorEventoCorreo.push('Debe seleccionar una sede');
                continuar = false;
            }
            if ($scope.ModalEventoCorreo.Email == '' || $scope.ModalEventoCorreo.Email == undefined || $scope.ModalEventoCorreo.Email == null) {
                $scope.MensajesErrorEventoCorreo.push('Debe ingresar un email');
                continuar = false;
            }

            var indice = $scope.ModalEventoCorreo.indice != undefined ? $scope.ModalEventoCorreo.indice : -1;
            if (continuar == true) {
                if ($scope.ListaCorreos.length > 0) {
                    for (var i = 0; i < $scope.ListaCorreos.length; i++) {
                        if (indice != i) {
                            if ($scope.ModalEventoCorreo.Email == $scope.ListaCorreos[i].Email && $scope.ModalEventoCorreo.EventoCorreo.Codigo == $scope.ListaCorreos[i].EventoCorreo.Codigo &&
                                $scope.ModalEventoCorreo.TerceroDireccion.Codigo == $scope.ListaCorreos[i].TerceroDireccion.Codigo) {
                                $scope.MensajesErrorEventoCorreo.push('El correo electrónico ya fue ingresado para la sede');
                                continuar = false;
                                break;
                            }
                        }
                    }
                }
            }
            return continuar;
        }

        //--Eliminar
        $scope.TmpEliminarEventoCorreo = {};
        $scope.ConfirmaEliminarEventoCorreo = function (item) {
            $scope.TmpEliminarEventoCorreo = { Indice: item.indice };
            showModal('modalEliminarEventoCorreo');
        };

        $scope.EliminarEventoCorreo = function (indice) {
            $scope.ListaCorreos.splice(indice, 1);
            OrdenarIndicesEventoCorreo();
            closeModal('modalEliminarEventoCorreo');
        };

        function OrdenarIndicesEventoCorreo() {
            for (var i = 0; i < $scope.ListaCorreos.length; i++) {
                $scope.ListaCorreos[i].indice = i;
            }
        }

        $scope.EditarEventoCorreo = function (item) {
            LimpiarmodalEventoCorreo();
            $scope.ModalEventoCorreo = angular.copy(item);
            $scope.ModalEventoCorreo.EventoCorreo = $linq.Enumerable().From($scope.ListaEventoCorreos).First('$.Codigo == ' + item.EventoCorreo.Codigo);
            if (item.TerceroDireccion.Codigo != undefined && item.TerceroDireccion.Codigo > 0) {
                $scope.ModalEventoCorreo.TerceroDireccion = $linq.Enumerable().From($scope.ListadoDireccionesTercero).First('$.Codigo == ' + item.TerceroDireccion.Codigo);
            }
            isEditEventoCorreo = true;
            showModal('modalEventoCorreo');
        }
        //---------------- Evento Correos Terceros ---------------- //
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR TERCERO';
            $scope.Deshabilitar = true;
            ObtenerTercero();
        } else {
            $scope.CargarDatosFunciones()
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function ObtenerTercero() {
            BloqueoPantalla.start('Cargando...');
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };
            blockUI.delay = 1000;
            TercerosFactory.Obtener(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo.FormasDePago = response.data.Datos.FormasPago
                        $scope.obtenerFormasPago($scope.Modelo.FormasDePago);

                        $scope.Modelo.Documentos = [];
                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.CorreoFacturaElectronica = response.data.Datos.CorreoFacturacion;
                        try { $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.Ciudad.Codigo) } catch (e) { }
                        try { $scope.Modelo.CiudadExpedicionIdentificacion = $scope.CargarCiudad(response.data.Datos.CiudadExpedicionIdentificacion.Codigo) } catch (e) { }
                        try { $scope.Modelo.CiudadNacimiento = $scope.CargarCiudad(response.data.Datos.CiudadNacimiento.Codigo) } catch (e) { }

                        var telefonos = response.data.Datos.Telefonos.split(';')
                        $scope.Telefono = telefonos[0]
                        if (telefonos[1] !== undefined) {
                            $scope.Telefono2 = telefonos[1]
                        }
                        $scope.CodigoPais = response.data.Datos.Pais.Codigo
                        if ($scope.CodigoPais > 0 && $scope.ListadoPaises.length > 0) {
                            $scope.Modelo.Pais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.CodigoPais);
                        }

                        $scope.SitiosTerceroCliente = [];
                        try {
                            for (var i = 0; i < response.data.Datos.SitiosTerceroCliente.length; i++) {
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.Ciudad = response.data.Datos.SitiosTerceroCliente[i].Ciudad;
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.Zona = response.data.Datos.SitiosTerceroCliente[i].Zona;
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.ValorCargueCliente = MascaraValores(response.data.Datos.SitiosTerceroCliente[i].ValorCargueCliente);
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.ValorDescargueCliente = MascaraValores(response.data.Datos.SitiosTerceroCliente[i].ValorDescargueCliente);
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.ValorCargueTransportador = MascaraValores(response.data.Datos.SitiosTerceroCliente[i].ValorCargueTransportador);
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.ValorDescargueTransportador = MascaraValores(response.data.Datos.SitiosTerceroCliente[i].ValorDescargueTransportador);
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.Estado = response.data.Datos.SitiosTerceroCliente[i].Estado > 0 ? true : false;
                                $scope.SitiosTerceroCliente.push(response.data.Datos.SitiosTerceroCliente[i].SitioCliente);
                            }
                        } catch (e) {
                        }

                        $scope.SitiosTerceroClienteTotal = [];
                        $scope.ListaSitiosGrid = [];

                        $scope.ListaCupoSede = response.data.Datos.TerceroClienteCupoSedes;
                        $scope.ListadoPesoCumplido = response.data.Datos.CondicionesPeso;
                        OrdenarIndicesPesoCumplido();

                        $scope.AsignarCupoSaldo($scope.Modelo.TipoValidacionCupo);
                        try {
                            $scope.CodigoModalidadFacturacion = response.data.Datos.Cliente.ModalidadFacturacionPeso.Codigo;

                        } catch (e) {

                        }
                        try {
                            if (response.data.Datos.Cliente.ManejaCondicionesPesoCumplido == 1) {
                                $scope.Modelo.CondicionPesoCumplido = true;
                            } else {
                                $scope.Modelo.CondicionPesoCumplido = false;
                            }

                            $scope.Modelo.MargenUtilidad = response.data.Datos.Cliente.MargenUtilidad;
                            $scope.Modelo.DiasCierreFacturacion = response.data.Datos.Cliente.DiasCierreFacturacion;
                            //$scope.Modelo.CorreoFacturaElectronica = response.data.Datos.Cliente.CorreoFacturaElectronica;

                            if (response.data.Datos.Cliente.CondicionesComercialesTarifas == 1) {
                                $scope.Modelo.CondicionesComercialesTarifas = true;
                                $scope.VerCondicionesComerciales = true;
                            } else {
                                $scope.Modelo.CondicionesComercialesTarifas = false;
                                $scope.VerCondicionesComerciales = false;
                            }

                            if (response.data.Datos.Cliente.GestionDocumentos == 1) {
                                $scope.Modelo.GestionDocumentos = true;
                                $scope.VerGestionDocumentos = true
                            } else {
                                $scope.Modelo.GestionDocumentos = false;
                                $scope.VerGestionDocumentos = false
                            }

                        } catch (e) {

                        }

                        if ($scope.ListaModalidadFacturacionPeso !== undefined && $scope.ListaModalidadFacturacionPeso !== null) {
                            if ($scope.ListaModalidadFacturacionPeso.length > 0) {
                                $scope.Modelo.ModalidadFacturacionPeso = $linq.Enumerable().From($scope.ListaModalidadFacturacionPeso).First('$.Codigo ==' + $scope.CodigoModalidadFacturacion);
                            }
                        }


                        if ($scope.SitiosTerceroCliente.length > 0) {
                            $scope.SitiosTerceroCliente.forEach(function (itemSitios) {
                                $scope.SitiosTerceroClienteTotal.push(itemSitios);
                            });
                            for (var i = 0; i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                    $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                                }
                            }
                        }

                        if ($scope.SitiosTerceroClienteTotal.length > 0) {
                            $scope.totalRegistros = $scope.SitiosTerceroClienteTotal.length;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        } else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }

                        //var q = 0;
                        //var x = 0;
                        //if (response.data.Datos.ListadoFOTOS != undefined) {
                        //    for (q = 0; q < $scope.ListadoFotos.length; q++) {

                        //        for (var x = 0; x < response.data.Datos.ListadoFOTOS.length; x++) {

                        //            if (response.data.Datos.ListadoFOTOS[x].Numero == $scope.ListadoFotos[q].IDNew) {

                        //                $scope.Documento = response.data.Datos.ListadoFOTOS[x]
                        //                $scope.ListadoFotos[q].id = response.data.Datos.ListadoFOTOS[x].Id

                        //                $scope.IdPosiction = response.data.Datos.ListadoFOTOS[x].Id
                        //                reader.onload = $scope.AsignarArchivo($scope.Documento);
                        //                reader.readAsDataURL(element.files[0]);
                        //            }
                        //        }
                        //    }
                        //}
                        $scope.tempfotos = response.data.Datos.ListadoFOTOS
                        for (var i = 0; i < $scope.tempfotos.length; i++) {
                            if ($scope.tempfotos[i].Extension == 'png') {
                                $scope.tempfotos[i].FotoCargada = 'data:image/png;base64,' + $scope.tempfotos[i].Archivo
                            } else {
                                $scope.tempfotos[i].FotoCargada = 'data:image/jpeg;base64,' + $scope.tempfotos[i].Archivo
                            }
                        }
                        if (response.data.Datos.Conductor !== undefined && response.data.Datos.Conductor !== null) {
                            $scope.ValidarCategoria();
                            if (response.data.Datos.Conductor.Propio == 1) {
                                response.data.Datos.Conductor.Propio = true
                            } if (response.data.Datos.Conductor.Afiliado == 1) {
                                response.data.Datos.Conductor.Afiliado = true
                            } if (response.data.Datos.Conductor.BloqueadoAltoRiesgo == 1) {
                                response.data.Datos.Conductor.BloqueadoAltoRiesgo = true
                            }

                            if (ValidarFecha(response.data.Datos.Conductor.FechaVencimiento) == 0) {
                                $scope.Modelo.Conductor.FechaVencimiento = new Date(response.data.Datos.Conductor.FechaVencimiento)
                            }
                            if (ValidarFecha(response.data.Datos.Conductor.FechaUltimoViaje) == 0) {
                                $scope.Modelo.Conductor.FechaUltimoViaje = new Date(response.data.Datos.Conductor.FechaUltimoViaje)
                            }

                        } else {
                            $scope.Modelo.Conductor = { Codigo: 0 }

                        }
                        if (response.data.Datos.Empleado !== undefined && response.data.Datos.Empleado !== null) {
                            if (ValidarFecha(response.data.Datos.Empleado.FechaVinculacion) == 0) {
                                $scope.Modelo.Empleado.FechaVinculacion = new Date(response.data.Datos.Empleado.FechaVinculacion)
                                if ($scope.Modelo.Empleado.FechaVinculacion < MIN_DATE) {
                                    $scope.Modelo.Empleado.FechaVinculacion = '';
                                }

                            }
                            if (ValidarFecha(response.data.Datos.Empleado.FechaFinalizacion) == 0) {

                                $scope.Modelo.Empleado.FechaFinalizacion = new Date(response.data.Datos.Empleado.FechaFinalizacion)
                                if ($scope.Modelo.Empleado.FechaFinalizacion < MIN_DATE) {
                                    $scope.Modelo.Empleado.FechaFinalizacion = ''
                                }
                            }
                        } else {
                            $scope.Modelo.Empleado = { Codigo: 0 }
                        }
                        if (response.data.Datos.Cliente == undefined || response.data.Datos.Cliente == null) {
                            $scope.Modelo.Cliente = { Codigo: 0 }
                        }
                        else {
                            $scope.Modelo.Cliente.Codigo = response.data.Datos.Codigo;
                        }

                        if (response.data.Datos.Proveedor == undefined || response.data.Datos.Proveedor == null) {
                            $scope.Modelo.Proveedor = { Codigo: 0 }
                        }

                        if (response.data.Datos.Direcciones.length > 0) {
                            $scope.DireccionesTemp = response.data.Datos.Direcciones;
                            $scope.ListadoDireccionesTercero = response.data.Datos.Direcciones;
                            $scope.ListadoDireccionesTercero.push({ Codigo: -1, Nombre: "" });
                            $scope.DireccionEVCO = $linq.Enumerable().From($scope.ListadoDireccionesTercero).First('$.Codigo == -1');
                        }
                        else {
                            $scope.ListadoDirecciones = [{ Direccion: '', Codigo: 0, Nombre: '', Estado: 1 }];
                        }
                        try {
                            $scope.ListadoDirecciones = []
                            if ($scope.DireccionesTemp != undefined) {
                                for (var i = 0; i < $scope.DireccionesTemp.length; i++) {
                                    var ciud
                                    try {
                                        try { ciud = $scope.CargarCiudad(response.data.Datos.Direcciones[i].Ciudad.Codigo) } catch (e) { }
                                    } catch (e) {
                                        ciud = ''
                                    }
                                    $scope.ListadoDirecciones.push(
                                        {
                                            Direccion: $scope.DireccionesTemp[i].Direccion,
                                            Ciudad: ciud,
                                            Telefonos: $scope.DireccionesTemp[i].Telefonos,
                                            Barrio: $scope.DireccionesTemp[i].Barrio,
                                            CodigoPostal: $scope.DireccionesTemp[i].CodigoPostal,
                                            Codigo: $scope.DireccionesTemp[i].Codigo,
                                            CodigoAlterno: $scope.DireccionesTemp[i].CodigoAlterno,
                                            Nombre: $scope.DireccionesTemp[i].Nombre,
                                            Estado: $scope.DireccionesTemp[i].Estado
                                        })
                                }
                            }
                            else {
                                $scope.ListadoDirecciones = [{ Direccion: '', Codigo: 0, Estado: 1 }]
                            }

                        } catch (e) {
                        };

                        $scope.AsignarNombre()
                        if (response.data.Datos.Beneficiario !== undefined && response.data.Datos.Beneficiario !== null) {
                            if (response.data.Datos.Beneficiario.Codigo > 0) {
                                var beneficiario = $scope.CargarTercero(response.data.Datos.Beneficiario.Codigo)
                                $scope.ModalIdentificacionBeneficiario = beneficiario.NumeroIdentificacion
                                $scope.ModalNombreBeneficiario = beneficiario.NombreCompleto
                                $scope.Modelo.Beneficiario = { Codigo: beneficiario.Codigo }
                            }
                        }
                        try {
                            $scope.ListaCorreos = $scope.Modelo.ListadoCorreos;
                            OrdenarIndicesEventoCorreo();
                        } catch (e) {
                        }
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        $scope.Modelo.UsuaCodigoCrea = $scope.Sesion.UsuarioAutenticado.Codigo;
                        $timeout(function () {
                            $scope.CargarDatosFunciones()
                        }, 200);

                        //$scope.ListadoEstados = [
                        //    { Codigo: 1, Nombre: "ACTIVO" },
                        //    { Codigo: 0, Nombre: "INACTIVO" },
                        //];
                        if ($scope.Modelo.Cliente.Codigo > 0) {
                            $scope.Modelo.Cliente.BloquearDespachos = response.data.Datos.Cliente.BloquearDespachos == 1 ? true : false;
                            $scope.Modelo.Cliente.AnalistaCartera = $scope.CargarTercero(response.data.Datos.Cliente.AnalistaCartera.Codigo);
                            if (response.data.Datos.Cliente.CondicionesComerciales != undefined && response.data.Datos.Cliente.CondicionesComerciales != null) {
                                $scope.Modelo.Cliente.ListadoCondicionesComerciales = [];


                                response.data.Datos.Cliente.CondicionesComerciales.forEach(item => {
                                    $scope.CargarTipoLineaFiltrosGeneral(item);
                                    $scope.CargarTarifaFiltrosGeneral(item);
                                    $scope.CargarTipoTarifaFiltrosGeneral(item)


                                    $scope.Modelo.Cliente.ListadoCondicionesComerciales.push({
                                        ListadoTipoLineaNegocioTransportesFiltrosGeneral: item.ListadoTipoLineaNegocioTransportesFiltrosGeneral,
                                        ListadoTarifaTransportesFiltrado: item.ListadoTarifaTransportesFiltrado,
                                        ListadoTipoTarifaTransportesFiltrado: item.ListadoTipoTarifaTransportesFiltrado,
                                        LineaNegocio: $linq.Enumerable().From($scope.ListadoLineaNegocioTransportes).First('$.Codigo==' + item.LineaNegocio.Codigo),
                                        TipoLineaNegocio: $linq.Enumerable().From(item.ListadoTipoLineaNegocioTransportesFiltrosGeneral).First('$.Codigo==' + item.TipoLineaNegocio.Codigo),
                                        Tarifa: $linq.Enumerable().From(item.ListadoTarifaTransportesFiltrado).First('$.Codigo==' + item.Tarifa.Codigo),
                                        TipoTarifa: $linq.Enumerable().From(item.ListadoTipoTarifaTransportesFiltrado).First('$.Codigo==' + item.TipoTarifa.Codigo),
                                        FleteMinimo: MascaraValores(item.FleteMinimo),
                                        ManejoMinimo: MascaraValores(item.ManejoMinimo),
                                        PorcentajeValorDeclarado: item.PorcentajeValorDeclarado,
                                        PorcentajeFlete: item.PorcentajeFlete,
                                        PorcentajeVolumen: item.PorcentajeVolumen
                                    });
                                });


                            }


                            if (response.data.Datos.Cliente.GestionDocumentos != undefined && response.data.Datos.Cliente.GestionDocumentos != null) {
                                $scope.Modelo.Cliente.ListadoGestionDocumentos = [];
                                response.data.Datos.Cliente.ListaGestionDocumentos.forEach(item => {

                                    $scope.Modelo.Cliente.ListadoGestionDocumentos.push({
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        Tercero: { Codigo: $scope.Modelo.Codigo },
                                        TipoDocumento: $linq.Enumerable().From($scope.ListadoTiposDocumentoGestionCliente).First('$.Codigo==' + item.TipoDocumento.Codigo),
                                        TipoGestion: $linq.Enumerable().From($scope.ListadoTipoGestionConductorDocumentos).First('$.Codigo==' + item.TipoGestion.Codigo),
                                        GeneraCobro: item.GeneraCobro == 1 ? true : false,
                                        Estado: $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + item.Estado.Codigo)
                                    });
                                });
                            }
                        }

                        $scope.ListaNovedadesTercero = response.data.Datos.ListaNovedades;
                        $scope.ListaNovedadesTercero.forEach(item => {
                            item.Hora = new Date(item.Fecha).getHours();
                            item.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + item.Estado);
                        });
                        Paginar();

                        blockUI.delay = 1000;
                        $scope.AsignarLicencia2();

                        if ($scope.Modelo.Codigo > 0) {
                            $scope.verificarExistencia();
                        }

                    }
                    else {
                        ShowError('No se logro consultar el tercero No.' + $scope.Modelo.NumeroIdentificacion + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#consultarTerceros';
                    }
                    BloqueoPantalla.stop();
                }, function (response) {
                    ShowError('No se logro consultar el tercero No.' + $scope.Modelo.NumeroIdentificacion + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#consultarTerceros';
                    BloqueoPantalla.stop();
                });


        }


        $scope.CargarTerceroEstudio = function () {
            BloqueoPantalla.start('Cargando tercero ...');
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroAutorizacion: $scope.Modelo.NumeroEstudioSeguridad
            };
            if ($scope.DatosRequeridosEstudio()) {
                TercerosFactory.ObtenerTerceroEstudioSeguridad(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Numero > 0) {

                                if (response.data.Datos.Anulado === 0) {
                                    var sinAsignar = true;
                                    for (var i = 0; i < $scope.perfilTerceros.length; i++) {
                                        if ($scope.Modelo.NumeroIdentificacion == response.data.Datos.Propietario.NumeroIdentificacion) {
                                            if ($scope.perfilTerceros[i].Estado === true && $scope.perfilTerceros[i].Codigo === 1408) {
                                                if (response.data.Datos.Propietario.TipoNaturaleza.Codigo !== 0 && response.data.Datos.Propietario.TipoNaturaleza.Codigo !== undefined) {
                                                    $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + response.data.Datos.Propietario.TipoNaturaleza.Codigo);
                                                    $scope.AsignarTipoIdentificacion();
                                                    $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.Propietario.TipoIdentificacion.Codigo);
                                                }
                                                $scope.Modelo.DigitoChequeo = response.data.Datos.Propietario.DigitoChequeo;
                                                $scope.Modelo.Nombre = response.data.Datos.Propietario.Nombre;
                                                $scope.Modelo.PrimeroApellido = response.data.Datos.Propietario.PrimeroApellido;
                                                $scope.Modelo.SegundoApellido = response.data.Datos.Propietario.SegundoApellido;
                                                $scope.Modelo.CiudadExpedicionIdentificacion = $scope.CargarCiudad(response.data.Datos.Propietario.CiudadExpedicionIdent.Codigo);
                                                $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.Propietario.Ciudad.Codigo);
                                                $scope.Modelo.Direccion = response.data.Datos.Propietario.Direccion;
                                                $scope.Telefono = response.data.Datos.Propietario.Telefonos;
                                                $scope.Modelo.Celular = response.data.Datos.Propietario.Celular;
                                                sinAsignar = false;
                                                try {
                                                    for (var k = 0; k < $scope.ListadoDocumentos.length; k++) {
                                                        var item1 = $scope.ListadoDocumentos[k]
                                                        for (var l = 0; l < response.data.Datos.ListadoDocumentos.length; l++) {
                                                            var item2 = response.data.Datos.ListadoDocumentos[l]
                                                            if ((item2.EOESCodigo == 2 || item2.EOESCodigo == 9 || item2.EOESCodigo == 16) && item1.Codigo == item2.CodigoConfiguracion) {
                                                                item1.Referencia = item2.Referencia
                                                                item1.Emisor = item2.Emisor
                                                                item1.FechaEmision = new Date(item2.FechaEmision)
                                                                item1.FechaVence = new Date(item2.FechaVence)

                                                            }
                                                        }
                                                    }
                                                } catch (e) {

                                                }

                                            }
                                        } else if ($scope.Modelo.NumeroIdentificacion == response.data.Datos.PropietarioRemolque.NumeroIdentificacion) {
                                            if ($scope.perfilTerceros[i].Estado === true && $scope.perfilTerceros[i].Codigo === 1408) {
                                                if (response.data.Datos.PropietarioRemolque.TipoNaturaleza.Codigo !== 0 && response.data.Datos.PropietarioRemolque.TipoNaturaleza.Codigo !== undefined) {
                                                    $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + response.data.Datos.PropietarioRemolque.TipoNaturaleza.Codigo);
                                                    $scope.AsignarTipoIdentificacion();
                                                    $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.PropietarioRemolque.TipoIdentificacion.Codigo);
                                                }
                                                $scope.Modelo.DigitoChequeo = response.data.Datos.PropietarioRemolque.DigitoChequeo;
                                                $scope.Modelo.Nombre = response.data.Datos.PropietarioRemolque.Nombre;
                                                $scope.Modelo.PrimeroApellido = response.data.Datos.PropietarioRemolque.PrimeroApellido;
                                                $scope.Modelo.SegundoApellido = response.data.Datos.PropietarioRemolque.SegundoApellido;
                                                $scope.Modelo.CiudadExpedicionIdentificacion = $scope.CargarCiudad(response.data.Datos.PropietarioRemolque.CiudadExpedicionIdent.Codigo);
                                                $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.PropietarioRemolque.Ciudad.Codigo);
                                                $scope.Modelo.Direccion = response.data.Datos.PropietarioRemolque.Direccion;
                                                $scope.Telefono = response.data.Datos.PropietarioRemolque.Telefonos;
                                                $scope.Modelo.Celular = response.data.Datos.PropietarioRemolque.Celular;
                                                sinAsignar = false;
                                                try {
                                                    for (var k = 0; k < $scope.ListadoDocumentos.length; k++) {
                                                        var item1 = $scope.ListadoDocumentos[k]
                                                        for (var l = 0; l < response.data.Datos.ListadoDocumentos.length; l++) {
                                                            var item2 = response.data.Datos.ListadoDocumentos[l]
                                                            if ((item2.EOESCodigo == 5 || item2.EOESCodigo == 12 || item2.EOESCodigo == 19) && item1.Codigo == item2.CodigoConfiguracion) {
                                                                item1.Referencia = item2.Referencia
                                                                item1.Emisor = item2.Emisor
                                                                item1.FechaEmision = new Date(item2.FechaEmision)
                                                                item1.FechaVence = new Date(item2.FechaVence)



                                                            }
                                                        }
                                                    }
                                                } catch (e) {

                                                }
                                            }
                                        } else if ($scope.Modelo.NumeroIdentificacion == response.data.Datos.Conductor.NumeroIdentificacion) {
                                            if ($scope.perfilTerceros[i].Estado === true && $scope.perfilTerceros[i].Codigo === 1403) {
                                                if (response.data.Datos.Conductor.TipoNaturaleza.Codigo !== 0 && response.data.Datos.Conductor.TipoNaturaleza.Codigo !== undefined) {
                                                    $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + response.data.Datos.Conductor.TipoNaturaleza.Codigo);
                                                    $scope.AsignarTipoIdentificacion();
                                                    $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.Conductor.TipoIdentificacion.Codigo);
                                                }
                                                $scope.Modelo.DigitoChequeo = response.data.Datos.Conductor.DigitoChequeo;
                                                $scope.Modelo.Nombre = response.data.Datos.Conductor.Nombre;
                                                $scope.Modelo.PrimeroApellido = response.data.Datos.Conductor.PrimeroApellido;
                                                $scope.Modelo.SegundoApellido = response.data.Datos.Conductor.SegundoApellido;
                                                $scope.Modelo.CiudadExpedicionIdentificacion = $scope.CargarCiudad(response.data.Datos.Conductor.CiudadExpedicionIdent.Codigo);
                                                $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.Conductor.Ciudad.Codigo);
                                                $scope.Modelo.Direccion = response.data.Datos.Conductor.Direccion;
                                                $scope.Telefono = response.data.Datos.Conductor.Telefonos;
                                                $scope.Modelo.Celular = response.data.Datos.Conductor.Celular;
                                                sinAsignar = false;
                                                try {
                                                    for (var k = 0; k < $scope.ListadoDocumentos.length; k++) {
                                                        var item1 = $scope.ListadoDocumentos[k]
                                                        for (var l = 0; l < response.data.Datos.ListadoDocumentos.length; l++) {
                                                            var item2 = response.data.Datos.ListadoDocumentos[l]
                                                            if ((item2.EOESCodigo == 4 || item2.EOESCodigo == 11 || item2.EOESCodigo == 18) && item1.Codigo == item2.CodigoConfiguracion) {
                                                                item1.Referencia = item2.Referencia
                                                                item1.Emisor = item2.Emisor
                                                                item1.FechaEmision = new Date(item2.FechaEmision)
                                                                item1.FechaVence = new Date(item2.FechaVence)
                                                            }
                                                        }
                                                    }
                                                } catch (e) {

                                                }
                                            }
                                        } else if ($scope.Modelo.NumeroIdentificacion == response.data.Datos.Tenedor.NumeroIdentificacion) {
                                            if ($scope.perfilTerceros[i].Estado === true && $scope.perfilTerceros[i].Codigo === 1412) {
                                                if (response.data.Datos.Tenedor.TipoNaturaleza.Codigo !== 0 && response.data.Datos.Tenedor.TipoNaturaleza.Codigo !== undefined) {
                                                    $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + response.data.Datos.Tenedor.TipoNaturaleza.Codigo);
                                                    $scope.AsignarTipoIdentificacion();
                                                    $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.Tenedor.TipoIdentificacion.Codigo);
                                                }
                                                $scope.Modelo.DigitoChequeo = response.data.Datos.Tenedor.DigitoChequeo;
                                                $scope.Modelo.Nombre = response.data.Datos.Tenedor.Nombre;
                                                $scope.Modelo.PrimeroApellido = response.data.Datos.Tenedor.PrimeroApellido;
                                                $scope.Modelo.SegundoApellido = response.data.Datos.Tenedor.SegundoApellido;
                                                $scope.Modelo.CiudadExpedicionIdentificacion = $scope.CargarCiudad(response.data.Datos.Tenedor.CiudadExpedicionIdent.Codigo);
                                                $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.Tenedor.Ciudad.Codigo);
                                                $scope.Modelo.Direccion = response.data.Datos.Tenedor.Direccion;
                                                $scope.Telefono = response.data.Datos.Tenedor.Telefonos;
                                                $scope.Modelo.Celular = response.data.Datos.Tenedor.Celular;
                                                sinAsignar = false;
                                                try {
                                                    for (var k = 0; k < $scope.ListadoDocumentos.length; k++) {
                                                        var item1 = $scope.ListadoDocumentos[k]
                                                        for (var l = 0; l < response.data.Datos.ListadoDocumentos.length; l++) {
                                                            var item2 = response.data.Datos.ListadoDocumentos[l]
                                                            if ((item2.EOESCodigo == 3 || item2.EOESCodigo == 10 || item2.EOESCodigo == 17) && item1.Codigo == item2.CodigoConfiguracion) {
                                                                item1.Referencia = item2.Referencia
                                                                item1.Emisor = item2.Emisor
                                                                item1.FechaEmision = new Date(item2.FechaEmision)
                                                                item1.FechaVence = new Date(item2.FechaVence)
                                                            }
                                                        }
                                                    }
                                                } catch (e) {

                                                }
                                            }
                                        }
                                    }
                                    if (sinAsignar === true) {
                                        ShowError('Los perfiles seleccionados no corresponden al No. identificación ' + $scope.Modelo.NumeroIdentificacion + ' ni a la autorización ingresada');
                                        $scope.Modelo.NumeroIdentificacion = '';
                                        $scope.Modelo.DigitoChequeo = '';
                                        $scope.Modelo.Nombre = '';
                                        $scope.Modelo.PrimeroApellido = '';
                                        $scope.Modelo.SegundoApellido = '';
                                        $scope.Modelo.CiudadExpedicionIdentificacion = '';
                                        $scope.Modelo.Ciudad = '';
                                        $scope.Modelo.Direccion = '';
                                        $scope.Telefono = '';
                                        $scope.Modelo.Celular = '';
                                    } else {
                                        ShowSuccess('Información cargada correctamente, por favor selecionar los perfiles requeridos y diligenciar los campos obligatorios');
                                    }
                                } else {
                                    ShowError('El estudio de seguridad autorizado se encuentra anulado');
                                }
                            } else {
                                ShowError('No se encontro estudio de seguridad con el número de autorización ingresado');
                            }
                            BloqueoPantalla.stop();
                        } else {
                            ShowError(response.data.MensajeOperacion);
                            BloqueoPantalla.stop();
                        }
                        BloqueoPantalla.stop();
                    }, function (response) {
                        ShowError(response.data.MensajeOperacion);
                        BloqueoPantalla.stop();
                    });
            }
            BloqueoPantalla.stop();
        };
        $scope.DatosRequeridosEstudio = function () {
            $scope.MensajesError = [];
            var continuar = true;
            var PerfilSeleccionado = 0;
            for (var i = 0; i < $scope.perfilTerceros.length; i++) {
                if ($scope.perfilTerceros[i].Estado) {
                    PerfilSeleccionado++;
                }
            }
            if (PerfilSeleccionado === 0) {
                $scope.MensajesError.push('Debe Seleccionar por lo menos un perfil');
                continuar = false;
            }
            if ($scope.Modelo.TipoNaturaleza === undefined || $scope.Modelo.TipoNaturaleza === '' || $scope.Modelo.TipoNaturaleza === null || $scope.Modelo.TipoNaturaleza.Codigo === 500) {
                $scope.MensajesError.push('Debe ingresar un tipo naturaleza');
                continuar = false;
            }
            if ($scope.Modelo.TipoIdentificacion === undefined || $scope.Modelo.TipoIdentificacion === '' || $scope.Modelo.TipoIdentificacion === null || $scope.Modelo.TipoIdentificacion.Codigo === 100) {
                $scope.MensajesError.push('Debe ingresar un tipo identificación');
                continuar = false;
            }
            if ($scope.Modelo.NumeroIdentificacion === undefined || $scope.Modelo.NumeroIdentificacion === '' || $scope.Modelo.NumeroIdentificacion === null || $scope.Modelo.NumeroIdentificacion === 0) {
                $scope.MensajesError.push('Debe ingresar el número de identificación');
                continuar = false;
            }
            if ($scope.Modelo.NumeroEstudioSeguridad === undefined || $scope.Modelo.NumeroEstudioSeguridad === '' || $scope.Modelo.NumeroEstudioSeguridad === null || $scope.Modelo.NumeroEstudioSeguridad === 0) {
                $scope.MensajesError.push('Debe ingresar el número de autorización del estudio de seguiridad');
                continuar = false;
            }
            return continuar;
        };

        /*Se cambia la zona de la ciudad*/
        $scope.CambiarZonasCiudad = function (ciudad) {
            if (ciudad !== '') {
                if (ciudad.Codigo !== undefined) {
                    $scope.ObjetoCiudad = ciudad;
                    $scope.BloquearZona = false;
                    CambiarZonaCiudadDestinatario();
                } else {
                    $scope.FiltroZona = '';
                    $scope.ListadoZonas = [];
                    $scope.BloquearZona = true;
                }
            } else {
                $scope.FiltroZona = '';
                $scope.ListadoZonas = [];
                $scope.BloquearZona = true;
            }
        };

        /*Cargar combo lista zonas ciudades*/
        function CambiarZonaCiudadDestinatario() {
            ZonasFactory.Consultar({ Ciudad: $scope.ObjetoCiudad, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoZonas = [];
                        $scope.ListadoZonas.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Codigo !== 0) {
                                    $scope.ListadoZonas.push(item);
                                }
                            });
                            $scope.FiltroZona = $scope.ListadoZonas[0];
                        } else {
                            $scope.FiltroZona = $scope.ListadoZonas[0];
                        }
                    }
                }), function (response) {
                };
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        //--------------------------------------------------------------------------------Funciones paginación sitios-----------------------------------------------------------------        
        $scope.PrimerPagina = function () {
            if ($scope.paginaActual > 1) {
                if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                    $scope.paginaActual = 1;
                    $scope.ListaSitiosGrid = [];
                    var i = 0;
                    for (i = 0; i <= $scope.ListaResultadoFiltroConsultaSitios.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina) {
                            $scope.ListaSitiosGrid.push($scope.ListaResultadoFiltroConsultaSitios[i]);
                        }
                    }
                } else {
                    if ($scope.SitiosTerceroClienteTotal.length > 0) {
                        $scope.paginaActual = 1;
                        $scope.ListaSitiosGrid = [];
                        var i = 0;
                        for (i = 0; i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina) {
                                $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                            }
                        }
                    }
                }
            }
        };
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.ListaSitiosGrid = [];
                $scope.PaginaAuxiliar = $scope.paginaActual;
                $scope.paginaActual += 1;
                if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListaResultadoFiltroConsultaSitios.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListaSitiosGrid.push($scope.ListaResultadoFiltroConsultaSitios[i]);
                        }
                    }
                } else {
                    if ($scope.SitiosTerceroClienteTotal.length > 0) {
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                            }
                        }
                    }
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                    $scope.ListaSitiosGrid = [];
                    $scope.paginaActual -= 1;
                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListaResultadoFiltroConsultaSitios.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListaSitiosGrid.push($scope.ListaResultadoFiltroConsultaSitios[i]);
                        }
                    }
                } else {
                    if ($scope.SitiosTerceroClienteTotal.length > 0) {
                        $scope.ListaSitiosGrid = [];
                        $scope.paginaActual -= 1;
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                            }
                        }
                    }
                }
            }
        };
        $scope.UltimaPagina = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                    if ($scope.SitiosTerceroClienteTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListaSitiosGrid = [];
                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListaResultadoFiltroConsultaSitios.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListaSitiosGrid.push($scope.ListaResultadoFiltroConsultaSitios[i]);
                            }
                        }
                    }
                } else {
                    if ($scope.SitiosTerceroClienteTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListaSitiosGrid = [];
                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                            }
                        }
                    }
                }
            }
        }

        ////-----------------------------------------------------------------------------------------------Funcion Filtrar recoleciones--------------------------------------------------------
        $scope.FiltrarSitios = function () {

            if ($scope.SitiosTerceroClienteTotal.length > 0) {
                if (($scope.FiltroCiudad !== '' && $scope.FiltroCiudad !== null && $scope.FiltroCiudad !== undefined)
                    || ($scope.FiltroSitioCliente !== undefined && $scope.FiltroSitioCliente !== '' && $scope.FiltroSitioCliente !== null)) {

                    $scope.ListaSitiosGrid = [];
                    var FiltroConsulta = '';

                    if ($scope.FiltroCiudad !== '' && $scope.FiltroCiudad !== null && $scope.FiltroCiudad !== undefined) {
                        FiltroConsulta += '$.Ciudad.Codigo == ' + $scope.FiltroCiudad.Codigo;
                    }
                    if ($scope.FiltroZona !== '' && $scope.FiltroZona !== null && $scope.FiltroZona !== undefined) {
                        if ($scope.FiltroZona.Codigo !== 0) {
                            FiltroConsulta += ' && $.Zona.Codigo == ' + $scope.FiltroZona.Codigo;
                        }
                    }
                    if ($scope.FiltroCiudad !== '' && $scope.FiltroCiudad !== null && $scope.FiltroCiudad !== undefined) {
                        if ($scope.FiltroSitioCliente !== '' && $scope.FiltroSitioCliente !== null && $scope.FiltroSitioCliente !== undefined) {
                            FiltroConsulta += ' && $.Codigo == ' + $scope.FiltroSitioCliente.Codigo;
                        }
                    } else {
                        if ($scope.FiltroSitioCliente !== '' && $scope.FiltroSitioCliente !== null && $scope.FiltroSitioCliente !== undefined) {
                            FiltroConsulta += '$.Codigo == ' + $scope.FiltroSitioCliente.Codigo;
                        }
                    }

                    $scope.ListaResultadoFiltroConsultaSitios = [];
                    $scope.ListaResultadoFiltroConsultaSitios = $linq.Enumerable().From($scope.SitiosTerceroClienteTotal).Where(FiltroConsulta).ToArray();

                    if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                        $scope.ListaSitiosGrid = [];
                        $scope.paginaActual = 1;
                        var i = 0;
                        for (i = 0; i <= $scope.ListaResultadoFiltroConsultaSitios.length - 1; i++) {
                            if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                $scope.ListaSitiosGrid.push($scope.ListaResultadoFiltroConsultaSitios[i]);
                            }
                        }
                        if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                            $scope.totalRegistros = $scope.ListaResultadoFiltroConsultaSitios.length;
                            $scope.totalPaginas = Math.ceil($scope.ListaResultadoFiltroConsultaSitios.length / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                    } else {
                        $scope.totalRegistros = 0;
                        $scope.totalPaginas = 0;
                        $scope.paginaActual = 1;
                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                        $scope.Buscando = false;
                    }
                } else {
                    $scope.LimpiarFiltroTarifas();
                }
            }
        };

        ////---------------------------------------------------------------------------------------------Limpiar filtro recolecciones----------------------------------------------------------
        $scope.LimpiarFiltroTarifas = function () {
            $scope.FiltroCiudad = '';
            $scope.FiltroZona = '';
            $scope.FiltroSitioCliente = '';
            $scope.ListadoZonas = [];
            $scope.BloquearZona = true;
            $scope.ListaResultadoFiltroConsultaSitios = [];

            if ($scope.SitiosTerceroClienteTotal.length > 0) {
                $scope.ListaSitiosGrid = [];
                var i = 0;
                for (i = 0; i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                    if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                        $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                    }
                }
                if ($scope.SitiosTerceroClienteTotal.length > 0) {
                    $scope.totalRegistros = $scope.SitiosTerceroClienteTotal.length;
                    $scope.totalPaginas = Math.ceil($scope.SitiosTerceroClienteTotal.length / $scope.cantidadRegistrosPorPagina);
                    $scope.Buscando = false;
                    $scope.ResultadoSinRegistros = '';
                } else {
                    $scope.totalRegistros = 0;
                    $scope.totalPaginas = 0;
                    $scope.paginaActual = 1;
                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                    $scope.Buscando = false;
                }
            }
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            if ($scope.PermisoGuardar) {
                showModal('modalConfirmacionGuardar');
            } else {
                ShowError('No tiene permisos para realizar esta acción');
            }
        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridosTercero()) {

                if ($scope.Telefono2 !== undefined && $scope.Telefono2 !== '') {
                    if ($scope.Telefono !== undefined) {
                        $scope.Modelo.Telefonos = $scope.Telefono.toString() + ';' + $scope.Telefono2.toString()
                    } else {
                        $scope.Modelo.Telefonos = $scope.Telefono2.toString()
                    }
                } else {
                    if ($scope.Telefono !== undefined) {
                        $scope.Modelo.Telefonos = $scope.Telefono.toString()
                    }
                }
                $scope.Modelo.SitiosTerceroCliente = []
                $scope.Modelo.SitiosTerceroClienteModifica = []
                if ($scope.SitiosTerceroClienteTotal.length > 0) {
                    for (var i = 0; i < $scope.SitiosTerceroClienteTotal.length; i++) {
                        var sitio = angular.copy($scope.SitiosTerceroClienteTotal[i])
                        sitio.Estado = sitio.Estado ? 1 : 0;
                        if ($scope.SitiosTerceroClienteTotal[i].Nuevo > 0) {
                            $scope.Modelo.SitiosTerceroCliente.push(sitio)
                        }
                        else if ($scope.SitiosTerceroClienteTotal[i].Modifica > 0) {
                            $scope.Modelo.SitiosTerceroClienteModifica.push(sitio)
                        }
                        if ($scope.SitiosTerceroClienteTotal[i].Estado && $scope.SitiosTerceroClienteTotal[i].Nuevo == undefined) {
                            $scope.Modelo.SitiosTerceroCliente.push(sitio)
                        }

                    }
                }

                $scope.Modelo.CadenaPerfiles = ''
                for (var i = 0; i < $scope.perfilTerceros.length; i++) {
                    var Perfil = $scope.perfilTerceros[i]
                    if (Perfil.Estado) {
                        if ($scope.Modelo.CadenaPerfiles == undefined || $scope.Modelo.CadenaPerfiles == '') {
                            $scope.Modelo.CadenaPerfiles = Perfil.Codigo.toString() + ','
                        } else {
                            $scope.Modelo.CadenaPerfiles = $scope.Modelo.CadenaPerfiles.toString() + Perfil.Codigo.toString() + ','
                        }
                    }
                }

                $scope.Modelo.CadenaFormasPago = ''
                $scope.ListadoFormaPago.forEach(function (item) {
                    if (item.Estado === true) {
                        $scope.Modelo.CadenaFormasPago = $scope.Modelo.CadenaFormasPago + item.Codigo + ',';
                    }
                });

                $scope.Modelo.LineaServicio = []
                for (var i = 0; i < $scope.LineaServicioTerceros.length; i++) {
                    var Linea = $scope.LineaServicioTerceros[i]
                    if (Linea.Estado) {
                        $scope.Modelo.LineaServicio.push({ Codigo: $scope.LineaServicioTerceros[i].Codigo })
                    }
                }
                if ($scope.Modelo.Conductor.Propio) {
                    $scope.Modelo.Conductor.Propio = 1
                }
                if ($scope.Modelo.Conductor.Afiliado) {
                    $scope.Modelo.Conductor.Afiliado = 1
                }

                if ($scope.Modelo.CondicionPesoCumplido == true) {
                    $scope.Modelo.Cliente.ManejaCondicionesPesoCumplido = 1;
                } else {
                    $scope.Modelo.Cliente.ManejaCondicionesPesoCumplido = 0;
                }

                if ($scope.Modelo.CondicionesComercialesTarifas == true) {
                    $scope.Modelo.Cliente.CondicionesComercialesTarifas = 1;
                    $scope.Modelo.Cliente.CondicionesComerciales = [];
                    $scope.Modelo.Cliente.ListadoCondicionesComerciales.forEach(item => {
                        $scope.Modelo.Cliente.CondicionesComerciales.push({
                            LineaNegocio: { Codigo: item.LineaNegocio.Codigo },
                            TipoLineaNegocio: { Codigo: item.TipoLineaNegocio.Codigo },
                            Tarifa: { Codigo: item.Tarifa.Codigo },
                            TipoTarifa: { Codigo: item.TipoTarifa.Codigo },
                            FleteMinimo: RevertirMV(item.FleteMinimo),
                            ManejoMinimo: RevertirMV(item.ManejoMinimo),
                            PorcentajeValorDeclarado: parseFloat(item.PorcentajeValorDeclarado),
                            PorcentajeFlete: parseFloat(item.PorcentajeFlete),
                            PorcentajeVolumen: parseFloat(item.PorcentajeVolumen)
                        });
                    });

                } else {
                    $scope.Modelo.Cliente.CondicionesComercialesTarifas = 0;
                }

                if ($scope.Modelo.GestionDocumentos == true) {
                    $scope.Modelo.Cliente.GestionDocumentos = 1;
                    $scope.Modelo.Cliente.ListaGestionDocumentos = [];
                    $scope.Modelo.Cliente.ListadoGestionDocumentos.forEach(item => {
                        $scope.Modelo.Cliente.ListaGestionDocumentos.push({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Tercero: { Codigo: $scope.Modelo.Codigo },
                            TipoDocumento: { Codigo: item.TipoDocumento.Codigo },
                            TipoGestion: { Codigo: item.TipoGestion.Codigo },
                            GeneraCobro: item.GeneraCobro == true ? 1 : 0,
                            Estado: { Codigo: item.Estado.Codigo }
                        });
                    });

                } else {
                    $scope.Modelo.Cliente.GestionDocumentos = 0;
                }

                //--------- tarifarios paqueteria -----------//
                $scope.Modelo.Cliente.TarifariosPaq = [];
                $scope.ListadoTarifarios.forEach(item => {
                    if (item.Habilitado) $scope.Modelo.Cliente.TarifariosPaq.push({ Codigo: item.Codigo });
                });


                $scope.Modelo.Cliente.MargenUtilidad = $scope.Modelo.MargenUtilidad;
                $scope.Modelo.Cliente.ModalidadFacturacionPeso = $scope.Modelo.ModalidadFacturacionPeso;
                $scope.Modelo.Cliente.DiasCierreFacturacion = $scope.Modelo.DiasCierreFacturacion;
                $scope.Modelo.Cliente.BloquearDespachos = $scope.Modelo.Cliente.BloquearDespachos == true ? 1 : 0;
                $scope.Modelo.CorreoFacturacion = $scope.Modelo.CorreoFacturaElectronica;

                //-- Evento Correos
                var ListadoTMPListaCorreos = [];
                for (var i = 0; i < $scope.ListaCorreos.length; i++) {
                    var tmpEventoCorreo = {
                        EventoCorreo: { Codigo: $scope.ListaCorreos[i].EventoCorreo.Codigo },
                        TerceroDireccion: { Codigo: $scope.ListaCorreos[i].TerceroDireccion.Codigo == undefined ? 0 : $scope.ListaCorreos[i].TerceroDireccion.Codigo },
                        Email: $scope.ListaCorreos[i].Email
                    }
                    ListadoTMPListaCorreos.push(tmpEventoCorreo);
                }
                $scope.Modelo.ListadoCorreos = ListadoTMPListaCorreos;
                //-- Evento Correos

                //-- Cupo Cumplido
                var ListadoTMPPesoCumplido = [];
                for (var i = 0; i < $scope.ListadoPesoCumplido.length; i++) {
                    var tmpPesoCumplido = {
                        Producto: { Codigo: $scope.ListadoPesoCumplido[i].Producto.Codigo, Nombre: $scope.ListadoPesoCumplido[i].Producto.Nombre },
                        UnidadEmpaque: { Codigo: $scope.ListadoPesoCumplido[i].UnidadEmpaque.Codigo, Nombre: $scope.ListadoPesoCumplido[i].UnidadEmpaque.Nombre },
                        BaseCalculo: { Codigo: $scope.ListadoPesoCumplido[i].BaseCalculo.Codigo, Nombre: $scope.ListadoPesoCumplido[i].BaseCalculo.Nombre },
                        ValorUnidad: $scope.ListadoPesoCumplido[i].ValorUnidad,
                        TipoCalculo: { Codigo: $scope.ListadoPesoCumplido[i].TipoCalculo.Codigo, Nombre: $scope.ListadoPesoCumplido[i].TipoCalculo.Nombre },
                        Tolerancia: $scope.ListadoPesoCumplido[i].Tolerancia,
                        CondicionCobro: { Codigo: $scope.ListadoPesoCumplido[i].CondicionCobro.Codigo, Nombre: $scope.ListadoPesoCumplido[i].CondicionCobro.Nombre }
                    }
                    ListadoTMPPesoCumplido.push(tmpPesoCumplido);
                }
                $scope.Modelo.CondicionesPeso = ListadoTMPPesoCumplido;
                //-- Cupo Cumplido

                $scope.Modelo.ListadoFOTOS = $scope.ListadoFotosEnviar;

                if ($scope.Modelo.TipoNaturaleza.Codigo == 501) {
                    if (!($scope.Modelo.Nombre == '' || $scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == null)) {
                        $scope.Modelo.RazonSocial = ''
                    }

                } else {
                    if (!($scope.Modelo.RazonSocial == '' || $scope.Modelo.RazonSocial == undefined || $scope.Modelo.RazonSocial == null)) {
                        $scope.Modelo.Nombre = ''
                        $scope.Modelo.PrimeroApellido = ''
                        $scope.Modelo.SegundoApellido = ''
                    }
                }


                $scope.Modelo.ListaNovedades = [];
                BloqueoPantalla.start('Guardando...');

                var Entidad = {

                    Barrio: $scope.Modelo.Barrio,
                    Banco: { Codigo: $scope.Modelo.Banco.Codigo },
                    Ciudad: { Codigo: $scope.Modelo.Ciudad.Codigo },

                    CiudadExpedicionIdentificacion: $scope.Modelo.CiudadExpedicionIdentificacion,
                    CiudadNacimiento: $scope.Modelo.CiudadNacimiento,
                    Celular: $scope.Modelo.Celular,
                    Cliente: {
                        FormaPago: { Codigo: $scope.Modelo.Cliente.FormaPago == undefined ? 0 : $scope.Modelo.Cliente.FormaPago.Codigo },
                        BloquearDespachos: $scope.Modelo.Cliente.BloquearDespachos,
                        RepresentanteComercial: { Codigo: $scope.Modelo.Cliente.RepresentanteComercial == undefined ? 0 : $scope.Modelo.Cliente.RepresentanteComercial.Codigo },
                        Tarifario: { Codigo: $scope.Modelo.Cliente.Tarifario.Codigo },
                        TarifariosPaq: $scope.Modelo.Cliente.TarifariosPaq,
                        DiasPlazo: $scope.Modelo.Cliente.DiasPlazo,
                        MargenUtilidad: $scope.Modelo.Cliente.MargenUtilidad,
                        ManejaCondicionesPesoCumplido: $scope.Modelo.Cliente.ManejaCondicionesPesoCumplido,
                        CondicionesComercialesTarifas: $scope.Modelo.Cliente.CondicionesComercialesTarifas,
                        CondicionesComerciales: $scope.Modelo.Cliente.CondicionesComerciales,
                        GestionDocumentos: $scope.Modelo.Cliente.GestionDocumentos,
                        ListaGestionDocumentos: $scope.Modelo.Cliente.ListaGestionDocumentos,
                        AnalistaCartera: { Codigo: $scope.Modelo.Cliente.AnalistaCartera == undefined ? 0 : $scope.Modelo.Cliente.AnalistaCartera.Codigo },
                        DiasCierreFacturacion: $scope.Modelo.Cliente.DiasCierreFacturacion,
                        ModalidadFacturacionPeso: { Codigo: $scope.Modelo.Cliente.ModalidadFacturacionPeso == undefined ? 0 : $scope.Modelo.Cliente.ModalidadFacturacionPeso.Codigo },
                        Impuestos: $scope.Modelo.Cliente.Impuestos
                    },
                    TipoValidacionCupo: { Codigo: $scope.Modelo.TipoValidacionCupo == undefined ? 0 : $scope.Modelo.TipoValidacionCupo.Codigo },
                    Cupo: $scope.Modelo.Cupo,
                    Saldo: $scope.Modelo.Saldo,
                    Proveedor: {
                        Tarifario: { Codigo: $scope.Modelo.Proveedor.Tarifario == undefined ? 0 : $scope.Modelo.Proveedor.Tarifario.Codigo },
                        FormaCobro: { Codigo: $scope.Modelo.Proveedor.FormaCobro == undefined ? 0 : $scope.Modelo.Proveedor.FormaCobro.Codigo },
                        DiasPlazo: $scope.Modelo.Proveedor.DiasPlazo,
                        Impuestos: $scope.Modelo.Proveedor.Impuestos
                    },
                    Codigo: $scope.Modelo.Codigo,
                    CodigoAlterno: $scope.Modelo.CodigoAlterno,
                    CodigoContable: $scope.Modelo.CodigoContable,
                    CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                    CondicionesPeso: $scope.Modelo.CondicionesPeso,
                    Conductor: {
                        TipoContrato: { Codigo: $scope.Modelo.Conductor == undefined ? 0 : $scope.Modelo.Conductor.TipoContrato.Codigo },
                        TipoSangre: { Codigo: $scope.Modelo.Conductor == undefined ? 0 : $scope.Modelo.Conductor.TipoSangre.Codigo },
                        CategoriaLicencia: { Codigo: $scope.Modelo.Conductor == undefined ? 0 : $scope.Modelo.Conductor.CategoriaLicencia.Codigo },
                        NumeroLicencia: $scope.Modelo.Conductor == undefined ? 0 : $scope.Modelo.Conductor.NumeroLicencia,
                        FechaVencimiento: $scope.Modelo.Conductor == undefined ? '' : $scope.Modelo.Conductor.FechaVencimiento,
                        ViajesConductor: { Codigo: $scope.Modelo.Conductor == undefined ? '' : $scope.Modelo.Conductor.ViajesConductor },
                        Propio: $scope.Modelo.Conductor == undefined ? 0 : $scope.Modelo.Conductor.Propio,
                        Afiliado: $scope.Modelo.Conductor == undefined ? 0 : $scope.Modelo.Conductor.Afiliado,
                        BloqueadoAltoRiesgo: $scope.Modelo.Conductor == undefined ? 0 : $scope.Modelo.Conductor.BloqueadoAltoRiesgo,

                        FechaUltimoViaje: $scope.Modelo.Conductor == undefined ? '' : $scope.Modelo.Conductor.FechaUltimoViaje,
                        ReferenciasPersonales: $scope.Modelo.Conductor == undefined ? '' : $scope.Modelo.Conductor.ReferenciasPersonales,

                    },
                    Correo: $scope.Modelo.Correo,
                    CorreoFacturaElectronica: $scope.Modelo.CorreoFacturaElectronica,
                    CorreoFacturacion: $scope.Modelo.CorreoFacturacion,
                    DigitoChequeo: $scope.Modelo.DigitoChequeo,
                    Direccion: $scope.Modelo.Direccion,
                    Direcciones: $scope.Modelo.Direcciones,
                    Documentos: $scope.Modelo.Documentos,
                    Empleado: {
                        FechaVinculacion: $scope.Modelo.Empleado == undefined ? '' : $scope.Modelo.Empleado.FechaVinculacion,
                        FechaFinalizacion: $scope.Modelo.Empleado == undefined ? '' : $scope.Modelo.Empleado.FechaFinalizacion,
                        TipoContrato: { Codigo: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.TipoContrato.Codigo },
                        Cargo: { Codigo: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.Cargo.Codigo },
                        Salario: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.Salario,
                        ValorAuxilioTransporte: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.ValorAuxilioTransporte,
                        ValorSeguridadSocial: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.ValorSeguridadSocial,
                        ValorAporteParafiscales: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.ValorAporteParafiscales,
                        PorcentajeComision: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.PorcentajeComision,
                        ValorSeguroVida: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.ValorSeguroVida,
                        ValorProvisionPrestacionesSociales: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.ValorProvisionPrestacionesSociales,
                        Externo: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.Externo,
                        Departamento: { Codigo: $scope.Modelo.Empleado == undefined ? 0 : $scope.Modelo.Empleado.Departamento.Codigo },

                    },
                    Estado: { Codigo: $scope.Modelo.Estado.Codigo },
                    Foto: $scope.Modelo.Estado.Foto,
                    LineaServicio: $scope.Modelo.LineaServicio,
                    ListaNovedades: [],
                    ListadoCorreos: $scope.Modelo.ListadoCorreos,
                    ListadoFOTOS: $scope.Modelo.ListadoFOTOS,
                    Nombre: $scope.Modelo.Nombre,
                    NumeroIdentificacion: $scope.Modelo.NumeroIdentificacion,
                    Observaciones: $scope.Modelo.Observaciones,
                    Pais: { Codigo: $scope.Modelo.Pais.Codigo },
                    PrimeroApellido: $scope.Modelo.PrimeroApellido,
                    RazonSocial: $scope.Modelo.RazonSocial,
                    RepresentanteComercial: { Codigo: $scope.Modelo.RepresentanteComercial == undefined ? 0 : $scope.Modelo.RepresentanteComercial.Codigo },
                    RepresentanteLegal: $scope.Modelo.RepresentanteLegal,
                    SegundoApellido: $scope.Modelo.SegundoApellido,
                    Sexo: { Codigo: $scope.Modelo.Sexo.Codigo },
                    SitiosTerceroCliente: $scope.Modelo.SitiosTerceroCliente,
                    Telefonos: $scope.Modelo.Telefonos,
                    TerceroClienteCupoSedes: $scope.Modelo.TerceroClienteCupoSedes,
                    TipoAnticipo: { Codigo: $scope.Modelo.TipoAnticipo.Codigo },
                    TipoBanco: { Codigo: $scope.Modelo.TipoBanco.Codigo },
                    TipoIdentificacion: { Codigo: $scope.Modelo.TipoIdentificacion.Codigo },
                    TipoNaturaleza: { Codigo: $scope.Modelo.TipoNaturaleza.Codigo },
                    TipoValidacionCupo: { Codigo: $scope.Modelo.TipoValidacionCupo.Codigo },
                    UsuaCodigoCrea: $scope.Modelo.UsuaCodigoCrea,
                    CadenaPerfiles: $scope.Modelo.CadenaPerfiles,
                    CuentaBancaria: $scope.Modelo.CuentaBancaria,
                    Beneficiario: $scope.Modelo.Beneficiario,
                    TitularCuentaBancaria: $scope.Modelo.TitularCuentaBancaria,
                    CadenaFormasPago: $scope.Modelo.CadenaFormasPago
                }
                TercerosFactory.Guardar(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Nombre == "") {
                                    ShowSuccess('Se guardó el tercero ' + $scope.Modelo.RazonSocial);
                                } else {
                                    ShowSuccess('Se guardó el tercero ' + $scope.Modelo.Nombre + ' ' + $scope.Modelo.PrimeroApellido);
                                }

                                if ($scope.Modelo.Codigo != response.data.Datos) {
                                    if ($scope.Sesion.UsuarioAutenticado.ManejoCrearUsuarioConductor) {
                                        if ($scope.btnconductor) {
                                            Habilitado = 1;
                                            Login = 0;
                                            Manager = 0;
                                            ConsultaOtrasOficinas = 0;
                                            Externo = 0;

                                            $scope.Correo = ''
                                            $scope.Telefono = ''
                                            $scope.Celular = ''

                                            $scope.Correo = $scope.Modelo.Correo
                                            $scope.Telefono = $scope.Modelo.Celular
                                            $scope.Celular = $scope.Modelo.Celular

                                            $scope.Conductor = { NombreCompleto: '', Codigo: 0 };
                                            $scope.Conductor.Codigo = response.data.Datos;

                                            $scope.Empleado = { NombreCompleto: '', Codigo: 0 };
                                            $scope.Cliente = { NombreCompleto: '', Codigo: 0 };
                                            $scope.Proveedor = { NombreCompleto: '', Codigo: 0 };

                                            //$scope.AsignarConductor();

                                            parametros =
                                            {
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                Codigo: 0,
                                                CodigoUsuario: $scope.Modelo.NumeroIdentificacion,
                                                Nombre: $scope.Modelo.Nombre + ' ' + $scope.Modelo.PrimeroApellido,
                                                Descripcion: 'CONDUCTOR',
                                                Clave: $scope.Modelo.NumeroIdentificacion + '*',
                                                Cliente: $scope.Cliente,
                                                DiasCambioClave: 360,
                                                Mensaje: '',
                                                Manager,
                                                Empleado: $scope.Empleado,
                                                Conductor: $scope.Conductor,
                                                Proveedor: $scope.Proveedor,
                                                Correo: $scope.Modelo.Correo,
                                                Telefono: $scope.Modelo.Celular,
                                                Celular: $scope.Modelo.Celular,
                                                //Codigo GESPHONE 
                                                AplicacionUsuario: 203,
                                                Habilitado,
                                                Login,
                                                ConsultaOtrasOficinas: ConsultaOtrasOficinas
                                            };

                                            UsuariosFactory.Guardar(parametros).then(function (response) {
                                                if (response.data.ProcesoExitoso == true) {
                                                    if ($scope.Codigo == 0) {
                                                        ShowSuccess('Se guardó el Usuario ' + $scope.Nombre);
                                                    }
                                                    else {
                                                        ShowSuccess('Se guardó el Usuario ' + $scope.Nombre);
                                                    }
                                                }
                                            }, function (response) {
                                                ShowError(response.statusText);
                                            });
                                        }
                                    }
                                }
                                location.href = '#!ConsultarTerceros/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                                //$scope.cargarLisatdoTerceros();
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                            //$scope.cargarLisatdoTerceros();
                        }
                        BloqueoPantalla.stop();
                    }, function (response) {
                        ShowError(response.statusText);
                        //$scope.cargarLisatdoTerceros();
                        BloqueoPantalla.stop();
                    });
            }
        };

        //Funciones del listado sitios cargue y descargue--------------------------------------------------------------
        $scope.ValidarSitio = function (index) {
            var cont = 0;
            for (var i = 0; i < $scope.Modelo.SitiosTerceroCliente.length; i++) {
                if (i !== index) {
                    var item = $scope.Modelo.SitiosTerceroCliente[i]
                    if (item.Codigo == $scope.Modelo.SitiosTerceroCliente[index].SitioCliente.Codigo) {
                        cont++;
                        break;
                    }
                }
            }
            if (cont > 0) {
                ShowError('El sitio ya está asociada');
                $scope.Modelo.SitiosTerceroCliente[index].SitioCliente = undefined;
            }
        };

        //Funcion agrupar listados por evento--------------------------------------------------------------
        $scope.AgruparListado = function () {
            $scope.ListaCorreosAgrupado = []
            $scope.ListaCorreosAgrupado = AgruparListados($scope.ListaCorreos, 'EventoCorreo');
        };

        $scope.ListTitem = function (item) {
            if (item.items) {
                item.items = false;
            } else {
                item.items = true;
            }
        };

        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        $scope.ValidarCategoria = function () {
            if ($scope.Modelo.Conductor.Propio) {
                document.getElementById("cmbCantViajes").disabled = true;
                document.getElementById("cmbCondAfili").disabled = true;
                $scope.Modelo.Conductor.Afiliado = 0
            } else {
                document.getElementById("cmbCondAfili").disabled = false;
                document.getElementById("cmbCantViajes").disabled = true;
            }
            if ($scope.Modelo.Conductor.Afiliado) {
                $scope.Modelo.Conductor.Propio = 0
                document.getElementById("cmbCantViajes").disabled = true;
                document.getElementById("cmbCondPropio").disabled = true;

            } else {
                document.getElementById("cmbCondPropio").disabled = false;


            }




        }


        function DatosRequeridosTercero() {
            $scope.MensajesError = [];
            window.scrollTo(top, top);
            var modelo = $scope.Modelo
            var continuar = true;
            var listadoDirrecionesConfirmadas = []
            $scope.MensajesError = []

            if ($scope.ListadoDirecciones.length > 0) {
                for (var i = 0; i < $scope.ListadoDirecciones.length; i++) {
                    if ($scope.ListadoDirecciones[i].Direccion != undefined && $scope.ListadoDirecciones[i].Direccion != '') {
                        listadoDirrecionesConfirmadas.push($scope.ListadoDirecciones[i])
                    }
                }
                $scope.Modelo.Direcciones = listadoDirrecionesConfirmadas

            }
            if (modelo.TipoNaturaleza.Codigo == 500)  /*No aplica*/ {
                $scope.MensajesError.push('Debe seleccionar el tipo de naturaleza');
                continuar = false;
            }
            else if (modelo.TipoNaturaleza.Codigo == 501)  /*Natural*/ {
                if (modelo.TipoIdentificacion.Codigo == 100)  /*No aplica*/ {
                    $scope.MensajesError.push('Debe seleccionar el tipo de identificación');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Nombre) == 1) {
                    $scope.MensajesError.push('Debe ingresar el nombre');
                    continuar = false;
                }
                if (ValidarCampo(modelo.PrimeroApellido) == 1) {
                    $scope.MensajesError.push('Debe ingresar el primer apellido');
                    continuar = false;
                }
                //campos quitados bajo el ticket 0000535
                //if (modelo.Sexo.Codigo == 600)  /*No aplica*/ {
                //    $scope.MensajesError.push('Debe seleccionar el sexo');
                //    continuar = false;
                //}
                //if (ValidarCampo(modelo.CiudadExpedicionIdentificacion) == 1) {
                //    $scope.MensajesError.push('Debe ingresar la ciudad de expedición de la identificación');
                //    continuar = false;
                //} else if (ValidarCampo(modelo.CiudadExpedicionIdentificacion, undefined, true) == 2) {
                //    $scope.MensajesError.push('Debe ingresar una ciudad de expedición de la identificación válida');
                //    continuar = false;
                //}
                //if (ValidarCampo(modelo.CiudadNacimiento) == 1) {
                //    $scope.MensajesError.push('Debe ingresar la ciudad de nacimiento');
                //    continuar = false;
                //} else
                //    if (ValidarCampo(modelo.CiudadNacimiento, undefined, true) == 2) {
                //    $scope.MensajesError.push('Debe ingresar una ciudad de expedición nacimiento válida');
                //    continuar = false;
                //}
            }
            else if (modelo.TipoNaturaleza.Codigo == 502)  /*Juridica*/ {
                if (modelo.TipoIdentificacion.Codigo == 100)  /*No aplica*/ {
                    $scope.MensajesError.push('Debe seleccionar el tipo de identificación');
                    continuar = false;
                }
                if (modelo.DigitoChequeo == undefined || modelo.DigitoChequeo.toString() == '' || modelo.DigitoChequeo == null || isNaN(modelo.DigitoChequeo)) {
                    $scope.MensajesError.push('Debe ingresar el digito de chequeo');
                    continuar = false;
                } else if (ValidarCampo(modelo.NumeroIdentificacion) == 0) {
                    if (!$scope.ValidarDigitoChequeo(modelo.NumeroIdentificacion, modelo.DigitoChequeo)) {
                        $scope.MensajesError.push('Debe ingresar un digito de chequeo válido');
                        continuar = false;
                    }
                }
                if (ValidarCampo(modelo.RazonSocial) == 1) {
                    $scope.MensajesError.push('Debe ingresar la razón social');
                    continuar = false;
                }
                if (ValidarCampo(modelo.RepresentanteLegal) == 1) {
                    $scope.MensajesError.push('Debe ingresar el representante legal');
                    continuar = false;
                }
            }
            if (ValidarCampo(modelo.NumeroIdentificacion) == 1) {
                $scope.MensajesError.push('Debe ingresar el número de identificación');
                continuar = false;
            }
            else if (ValidarCampo(modelo.NumeroIdentificacion, 5) == 3) {
                $scope.MensajesError.push('El número de identificacion debe tener al menos 5 digitos');
                continuar = false;
            }
            if (ValidarCampo(modelo.Ciudad) == 1) {
                $scope.MensajesError.push('Debe ingresar la ciudad');
                continuar = false;
            } else if (ValidarCampo(modelo.Ciudad, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar una ciudad válida');
                continuar = false;
            }
            if (ValidarCampo(modelo.Direccion) == 1) {
                $scope.MensajesError.push('Debe ingresar la dirección');
                continuar = false;
            }
            if (ValidarCampo($scope.Telefono) == 1) {
                $scope.MensajesError.push('Debe ingresar el teléfono 1');
                continuar = false;
            }
            else if (ValidarCampo($scope.Telefono, 7) == 3) {
                $scope.MensajesError.push('El número de teléfono debe tener al menos 7 digitos');
                continuar = false;
            }
            if (ValidarCampo($scope.Telefono2) != 1) {
                if (ValidarCampo($scope.Telefono2, 7) == 3) {
                    $scope.MensajesError.push('El número de teléfono 2 debe tener al menos 7 digitos');
                    continuar = false;
                }
            }
            if (ValidarCampo(modelo.Correo) == 1) {
                $scope.MensajesError.push('Debe ingresar el e-mail');
                continuar = false;
            }

            /*Valida el Proveedor*/
            if ($scope.btnproveedor) {
                var Proveedor = modelo.Proveedor
                if (Proveedor !== null && Proveedor !== undefined && Proveedor !== '') {
                    if ($scope.Modelo.Proveedor.Tarifario == undefined || $scope.Modelo.Proveedor.Tarifario == null || $scope.Modelo.Proveedor.Tarifario == '') {
                        $scope.MensajesError.push('Debe ingresar un tarifario de compra');
                        continuar = false;
                    }
                    if (Proveedor.Tarifario == undefined || Proveedor.Tarifario == null || Proveedor.Tarifario == '') {
                        $scope.MensajesError.push('Debe ingresar un tarifario de compra');
                        continuar = false;
                    }
                    if (Proveedor.FormaCobro == undefined || Proveedor.FormaCobro == null || Proveedor.FormaCobro == '') {
                        $scope.MensajesError.push('Debe ingresar una forma de cobro');
                        continuar = false;
                    }
                    if (Proveedor.FormaCobro.Codigo == 8801) {//Credito
                        if (ValidarCampo(Proveedor.DiasPlazo) == 1) {
                            $scope.MensajesError.push('Debe ingresar los dias de plazo del proveedor');
                            continuar = false;
                        }
                    }
                }
            }
            $scope.Modelo.Documentos = [];
            var cont = 0
            for (var i = 0; i < $scope.perfilTerceros.length; i++) {
                if ($scope.perfilTerceros[i].Estado) {
                    cont++
                    /*Valida el cliente*/
                    if ($scope.perfilTerceros[i].Codigo == 1401) {
                        var Cliente = modelo.Cliente
                        if (ValidarCampo(Cliente.RepresentanteComercial) == 1) {
                            $scope.MensajesError.push('Debe ingresar el representante comercial');
                            continuar = false;
                        } else if (ValidarCampo(Cliente.RepresentanteComercial, undefined, true) == 2) {
                            $scope.MensajesError.push('Debe ingresar un representante comercial valido');
                            continuar = false;
                        } else if (ValidarCampo($scope.ContMedios, undefined, true) == 1) {
                            $scope.MensajesError.push('Debe ingresar almenos una Forma de pago');
                            continuar = false;
                        }
                        else {
                            $scope.Modelo.Cliente.RepresentanteComercial = { Codigo: Cliente.RepresentanteComercial.Codigo, NombreCompleto: Cliente.RepresentanteComercial.NombreCompleto }
                        }
                        if (Cliente.FormaPago != undefined) {
                            if (Cliente.FormaPago.Codigo == 1503) {//Credito


                                if (ValidarCampo(Cliente.DiasPlazo) == 1) {
                                    $scope.MensajesError.push('Debe ingresar los dias de plazo del cliente');
                                    continuar = false;
                                }
                            }
                        }

                        if ($scope.Modelo.CondicionesComercialesTarifas == true || $scope.Modelo.CondicionesComercialesTarifas == 1) {
                            $scope.Modelo.Cliente.ListadoCondicionesComerciales.forEach(item => {
                                if (item.LineaNegocio.Codigo == undefined || item.TipoLineaNegocio.Codigo == undefined || item.Tarifa.Codigo == undefined || item.TipoTarifa.Codigo == undefined) {
                                    $scope.MensajesError.push('Debe diligenciar todos los registros en las condiciones comerciales del cliente');
                                    continuar = false;
                                }
                            });
                        }

                        if ($scope.Modelo.GestionDocumentos == true || $scope.Modelo.GestionDocumentos == 1) {
                            $scope.Modelo.Cliente.ListadoGestionDocumentos.forEach(item => {
                                if (item.TipoDocumento.Codigo == undefined || item.TipoGestion.Codigo == undefined || item.GeneraCobro == undefined || item.Estado.Codigo == undefined) {
                                    $scope.MensajesError.push('Debe diligenciar todos los registros en la gestión de documentos del cliente');
                                    continuar = false;
                                }
                            });
                        }

                        var tarifpaq = false;
                        $scope.ListadoTarifarios.forEach(item => {
                            if (item.Habilitado) tarifpaq = true;
                        });
                        if (!tarifpaq) {
                            $scope.MensajesError.push('Debe seleccionar al menos un tarifario para paquetería');
                            continuar = false;
                        }
                        if (ValidarCampo(Cliente.Tarifario, 1, true) > 0) {
                            $scope.MensajesError.push('Debe seleccionar un tarifario para masivos');
                            continuar = false;
                        }

                    }
                    /*Valida el Conductor*/
                    else if ($scope.perfilTerceros[i].Codigo == 1403) {
                        var Conductor = modelo.Conductor

                        if (ValidarCampo(Conductor.NumeroLicencia) == 1) {
                            $scope.MensajesError.push('Debe ingresar el número de licencia');
                            continuar = false;
                        }

                        if (ValidarFecha(Conductor.FechaVencimiento) == 1) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento');
                            continuar = false;
                        } else if (ValidarFecha(Conductor.FechaVencimiento, false, true) == 3) {
                            $scope.MensajesError.push('La fecha de vencimiento de la licencia debe ser mayor a la fecha actual');
                            continuar = false;
                        }
                        if (Conductor.CategoriaLicencia.Codigo == 1800) {
                            $scope.MensajesError.push('Debe seleccionar la categoria de la licencia');
                            continuar = false;
                        }
                        if (Conductor.TipoSangre.Codigo == 1900) {
                            $scope.MensajesError.push('Debe seleccionar el tipo de sangre');
                            continuar = false;
                        }
                        for (var f = 0; f < $scope.ListadoDocumentos.length; f++) {

                            var documento = $scope.ListadoDocumentos[f]
                            if (documento.TipoArchivoDocumento.Codigo !== 4301) {

                                if (ValidarCampo(documento.Referencia) == 1) {
                                    if (documento.AplicaReferencia == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                                        $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    }
                                }
                                if (ValidarCampo(documento.Emisor) == 1) {
                                    if (documento.AplicaEmisor == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                                        $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    }
                                }
                                if (ValidarFecha(documento.FechaEmision) == 1) {
                                    if (documento.AplicaFechaEmision == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                                        $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    } else {
                                        documento.FechaEmision = ''
                                    }
                                } else {
                                    var f = new Date();
                                    if (documento.FechaEmision > f) {
                                        $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    }
                                }
                                if (ValidarFecha(documento.FechaVence) == 1) {
                                    if (documento.AplicaFechaVencimiento == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                                        $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    } else {
                                        documento.FechaVence = ''
                                    }
                                } else {
                                    if ($scope.Modelo.Estado.Codigo == 1) {
                                        if (documento.AplicaFechaVencimiento != CAMPO_DOCUMENTO_NO_APLICA) {
                                            var f = new Date();
                                            if (documento.FechaVence < f) {
                                                $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                                                continuar = false;
                                                DocumentoValido = false;
                                            }
                                        } else {
                                            documento.FechaVence = ''
                                        }
                                    }
                                    //var f = new Date();
                                    //if (documento.FechaVence < f) {
                                    //    $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                                    //    continuar = false;
                                    //    DocumentoValido = false;
                                    //}
                                }
                            }

                            //if (ValidarFecha(Conductor.FechaUltimoViaje) == 1) {
                            //    $scope.MensajesError.push('Debe ingresar la fecha del último viaje');
                            //    continuar = false;
                            //} else if (ValidarFecha(Conductor.FechaUltimoViaje, true) == 2) {
                            //    $scope.MensajesError.push('La fecha del último viaje debe ser menor a la fecha actual');
                            //    continuar = false;
                            //}
                        }
                    }
                    /*Valida el empleado*/
                    else if ($scope.perfilTerceros[i].Codigo == 1405) {
                        var Empleado = modelo.Empleado
                        if (ValidarFecha(Empleado.FechaVinculacion) == 1) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vinculación');
                            continuar = false;
                        } else if (ValidarFecha(Empleado.FechaVinculacion, true) == 2) {
                            $scope.MensajesError.push('La fecha de vinculación debe ser menor a la fecha actual');
                            continuar = false;
                        }
                        if (ValidarFecha(Empleado.FechaFinalizacion) == 1) {

                        } else if (ValidarFecha(Empleado.FechaFinalizacion, false, true) == 3) {
                            $scope.MensajesError.push('La fecha de finalización debe ser mayor a la fecha actual');
                            continuar = false;
                        }
                        if (Empleado.Departamento.Codigo == 8700) {
                            $scope.MensajesError.push('Debe seleccionar el Departamento');
                            continuar = false;
                        }
                        if (Empleado.Cargo.Codigo == 2000) {
                            $scope.MensajesError.push('Debe seleccionar el cargo');
                            continuar = false;
                        }
                    }

                }
            }
            if (cont == 0) {
                $scope.MensajesError.push('Debe seleccionar al menos 1 perfil');
                continuar = false;
            }
            if (modelo.Banco.Codigo > 0 || modelo.TipoBanco.Codigo > 400 || (modelo.CuentaBancaria !== undefined && modelo.CuentaBancaria !== '' && modelo.CuentaBancaria !== 0 && modelo.CuentaBancaria !== '0') || (modelo.TitularCuentaBancaria !== undefined && modelo.TitularCuentaBancaria !== '')) {
                if (modelo.Banco.Codigo == 0) {
                    $scope.MensajesError.push('Debe ingresar el banco');
                    continuar = false;
                }
                //if (modelo.TipoBanco.Codigo == 400) {
                //    $scope.MensajesError.push('Debe ingresar el tipo de cuenta bancaria');
                //    continuar = false;
                //}
                //if (ValidarCampo(modelo.CuentaBancaria) == 1) {
                //    $scope.MensajesError.push('Debe ingresar la cuenta bancaria');
                //    continuar = false;
                //}
                //if (ValidarCampo(modelo.TitularCuentaBancaria) == 1) {
                //    $scope.MensajesError.push('Debe ingresar el titular de la cuenta');
                //    continuar = false;
                //}
            }
            if (modelo.Estado.Codigo == 0) {
                if (ValidarCampo(modelo.JustificacionBloqueo) == 1) {
                    $scope.MensajesError.push('Debe ingresar la justificación de la inactividad');
                    continuar = false;
                }
            }
            $scope.Modelo.Documentos = []
            ////Documentos
            for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                var Cont = 0
                var documento = $scope.ListadoDocumentos[i]


                $scope.Modelo.Documentos.push(
                    {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Terceros: true,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Configuracion: { Codigo: documento.Codigo },
                        Referencia: documento.Referencia,
                        Emisor: documento.Emisor,
                        FechaEmision: documento.FechaEmision,
                        FechaVence: documento.FechaVence,
                        EliminaDocumento: documento.ValorDocumento
                    }
                )
            }

            var x = 0;
            $scope.ListadoFotosEnviar = [];
            for (var x = 0; x < $scope.ListadoFotos.length; x++) {

                var foto = $scope.ListadoFotos[x];
                if (foto.Archivo != undefined) {


                    $scope.ListadoFotosEnviar.push(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            Terceros: true,
                            Numero: x,
                            Nombre: foto.ArchivoCargado.name,
                            Archivo: foto.Archivo,
                            Extension: foto.ExtensionDocumento,
                            Tipo: foto.ArchivoCargado.type
                        }
                    )
                }
            }


            //$scope.Modelo.Documentos.push(
            //    {
            //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //        Terceros: true,
            //        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            //        Configuracion: { Codigo: $scope.ItemFoto.Codigo },
            //        Referencia: $scope.ItemFoto.Referencia,
            //        Emisor: $scope.ItemFoto.Emisor,
            //        FechaEmision: $scope.ItemFoto.FechaEmision,
            //        FechaVence: $scope.ItemFoto.FechaVence,
            //        EliminaDocumento: $scope.ItemFoto.ValorDocumento
            //    }
            //)
            if ($scope.Modelo.TipoValidacionCupo.Codigo == CODIGO_CATALOGO_TIPO_VALIDACIÓN_CUPO_OFICINA) {
                if ($scope.ListaCupoSede.length <= 0) {
                    $scope.MensajesError.push('Debe ingresar al menos una oficina para el cupo por oficina');
                    continuar = false;
                }
            }

            //Cliente
            if ($scope.Modelo.Cupo > $scope.Modelo.Saldo) {
                $scope.MensajesError.push('El cupo no puede ser superior al saldo');
                continuar = false;
            }

            if ($scope.Modelo.MargenUtilidad > 0 && $scope.Modelo.MargenUtilidad > 100) {
                $scope.MensajesError.push('El porcentaje del Márgen de Utilidad no puede superar el 100%');
                continuar = false;
            }

            if ($scope.Modelo.DiasCierreFacturacion > 0 && $scope.Modelo.DiasCierreFacturacion > 31) {
                $scope.MensajesError.push('Los días de Cierre de Facturación debe comprender un valor entre 1 a 31');
                continuar = false;
            }

            return continuar;
        }

        function ValidarCampo(objeto, minlength, Esobjeto) {
            var resultado = 0
            if (objeto == undefined || objeto == '' || objeto == null || objeto == 0 || objeto == '0') {
                resultado = 1
            } else {
                if (resultado == 0) {
                    if (minlength !== undefined) {
                        if (objeto.length < minlength) {
                            resultado = 3
                        }
                        else {
                            resultado = 0
                        }
                    }
                } if (resultado == 0) {
                    if (Esobjeto) {
                        if (objeto.Codigo == undefined || objeto.Codigo == '' || objeto.Codigo == null || objeto.Codigo == 0) {
                            resultado = 2
                        }
                    }
                }
            }
            return resultado
        }
        function ValidarFecha(objeto, MayorActual, MenorActual) {
            var resultado = 0
            var now = new Date()
            if (objeto == undefined || objeto == '' || objeto == null) {
                resultado = 1
            } else {
                if (resultado == 0) {
                    if (MayorActual) {
                        if (objeto > now) {
                            resultado = 2
                        }
                    }
                    else if (MenorActual) {
                        if (objeto < now) {
                            resultado = 3
                        }
                    }
                }
            }
            return resultado
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarTerceros';
        };



        // Metodo para verificar el perfil seleccionado
        var ConditionConuctor = false
        var ConditionEmpleado = false
        //Inicializacion tabs
        $scope.btncliente = false;
        $scope.btnempleado = false;
        $scope.btnconductor = false;
        $scope.btnproveedor = false;
        $scope.btnpasajero = false;

        //inicializacion de divisiones
        $('#infoBasica').show(); $('#Perfiles').hide(); $('#Localizacion').hide(); $('#cuentabanco').hide();
        $('#impuestos').hide(); $('#pasajero').hide(); $('#cliente').hide(); $('#conductor').hide(); $('#empleado').hide(); $('#listacorreos').hide();
        $('#seguridad').hide(); $('#foto').hide(); $('#estado').hide(); $('#Infovisible').hide(); $('#documentos').hide(); $('#proveedor').hide(); $('#novedades').hide()

        $scope.MostrarInfobasica = function () {
            $('#infoBasica').show()
            $('#Infovisible').hide();
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarPerfiles = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').show()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarLocalizacion = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').show()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#documentos').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarCuenta = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').show()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarImpuesto = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').show()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarPasajeros = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').show()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarCliente = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').show()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarConductor = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').show()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarEmpleado = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').show()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarSeguridad = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').show()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarFoto = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').show()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }
        $scope.MostrarEstado = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').show()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }

        $scope.MostrarNovedades = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').show()
        }
        $scope.MostrarDocumentos = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').show()
            $('#proveedor').hide()
            $('#novedades').hide()

        }
        $scope.MostrarProveedor = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').show()
            $('#novedades').hide()
        }
        $scope.MostrarListaCorreos = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').show()
            $('#documentos').hide()
            $('#proveedor').hide()
            $('#novedades').hide()
        }


        $scope.AsignarNombre = function () {
            if ($scope.Modelo.Nombre == "") {
                $scope.NombreCompleto = $scope.Modelo.RazonSocial
            } else {
                $scope.NombreCompleto = $scope.Modelo.Nombre + ' ' + $scope.Modelo.PrimeroApellido + ' ' + $scope.Modelo.SegundoApellido
            }

        }
        $scope.CargarNombreBeneficiario = function (ModalIdentificacionBeneficiario) {
            if (ModalIdentificacionBeneficiario !== undefined && ModalIdentificacionBeneficiario !== null && ModalIdentificacionBeneficiario !== '') {

                filtrosBeneficiario = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.ModalIdentificacionBeneficiario,
                };

                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosBeneficiario).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ModalNombreBeneficiario = response.data.Datos.NombreCompleto
                            $scope.Beneficiario.Codigo = response.data.Datos.Codigo
                        }
                        else {
                            ShowError('No existe el tercero con identificación no.' + $scope.ModalIdentificacionBeneficiario + '. ' + response.data.MensajeOperacion);
                            $scope.ModalIdentificacionBeneficiario = ''
                            $scope.ModalNombreBeneficiario = ''
                        }
                    }, function (response) {
                        ShowError('No existe el tercero con identificación no.' + $scope.ModalIdentificacionBeneficiario + '. ' + response.data.MensajeOperacion);
                        $scope.ModalIdentificacionBeneficiario = ''
                        $scope.ModalNombreBeneficiario = ''
                    });
            }
        };
        $scope.verificarCiudad = function (CodigoAlterno) {
            var response = CiudadesFactory.ConsultarAlterno({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Codigo: CodigoAlterno, Sync: true })
            return response.Datos[0]
        }

        $scope.verificarExistencia = function () {
            BloqueoPantalla.start('Cargando...');
            if ($scope.Modelo.NumeroIdentificacion !== undefined && $scope.Modelo.NumeroIdentificacion !== null && $scope.Modelo.NumeroIdentificacion !== '' && $scope.Modelo.NumeroIdentificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.Modelo.NumeroIdentificacion,
                };
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0 && $scope.VerificacionIdentificaion) {
                                ShowError('El tercero con identificación no.' + $scope.Modelo.NumeroIdentificacion + ' ya existe con el nombre de ' + response.data.Datos.NombreCompleto);
                                $scope.Modelo.NumeroIdentificacion = "";
                            } else {

                                filtros = {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Nit: $scope.Modelo.NumeroIdentificacion
                                }
                                $scope.ListadoTercerosSIESA = []
                                $scope.ListadoSucursalesSIESA = []
                                $scope.perfilTercerosSIESA = []
                                $scope.ListadoFormaPagoSIESA = []
                                var valoresAceptados = /^[0-9]+$/;
                                ServicioSIESAFactory.ConsultarTercero(filtros).then(function (response) {
                                    $scope.ListadoTercerosSIESA = response.data
                                    if ($scope.ListadoTercerosSIESA.length > 0) {

                                        $scope.Modelo.Nombre = response.data[0].Nombres
                                        $scope.Modelo.PrimeroApellido = response.data[0].Apellido1
                                        $scope.Modelo.SegundoApellido = response.data[0].Apellido2
                                        $scope.Modelo.RazonSocial = response.data[0].RazonSocial
                                        $scope.Modelo.Direccion = response.data[0].Direccion
                                        $scope.Modelo.RepresentanteLegal = (response.data[0].Contacto == "" ? $scope.Modelo.RepresentanteLegal : response.data[0].Contacto)
                                        try {
                                            $scope.Modelo.Cliente.RepresentanteComercial = $linq.Enumerable().From($scope.ListadoRepresentanteComerciales).First('$.NumeroIdentificacion ==' + response.data[0].CedulaVendedor)
                                        } catch (e) {

                                        }
                                        $scope.Modelo.Ciudad = $scope.verificarCiudad((response.data[0].CodigoCiudad))
                                        $scope.Telefono = response.data[0].Telefono
                                        $scope.Modelo.Correo = response.data[0].Email

                                        $scope.ListadoTercerosSIESA.forEach(function (item) {
                                            if (!($scope.ListadoSucursalesSIESA.includes(item.Sucursal))) {
                                                $scope.ListadoSucursalesSIESA.push(item.Sucursal);
                                                if (item.Sucursal == '001') {
                                                    $scope.perfilTercerosSIESA = $scope.perfilTercerosSIESA.concat($scope.perfilTerceros.filter(function (perfil) {
                                                        if (perfil.Codigo == 1401 || perfil.Codigo == 1404 || perfil.Codigo == 1410 || perfil.Codigo == 4902 || perfil.Codigo == 4903) {
                                                            return perfil.Codigo;
                                                        }
                                                    }));

                                                    $scope.ListadoFormaPagoSIESA = $scope.ListadoFormaPagoSIESA.concat($scope.ListadoFormaPago.filter(function (pago) {
                                                        if (pago.Codigo == 4902 || pago.Codigo == 4903) {
                                                            return pago.Codigo
                                                        }
                                                    }));

                                                } else if (item.Sucursal == 'CRD') {
                                                    $scope.perfilTercerosSIESA = $scope.perfilTercerosSIESA.concat($scope.perfilTerceros.filter(function (perfil) {
                                                        if (perfil.Codigo == 1401) {
                                                            return perfil.Codigo;
                                                        }
                                                    }));

                                                    $scope.ListadoFormaPagoSIESA = $scope.ListadoFormaPagoSIESA.concat($scope.ListadoFormaPago.filter(function (pago) {
                                                        return pago.Codigo == 4901;
                                                    }));

                                                } else if (item.Sucursal == 'PRV') {
                                                    $scope.perfilTercerosSIESA = $scope.perfilTercerosSIESA.concat($scope.perfilTerceros.filter(function (perfil) {
                                                        if (perfil.Codigo == 1402 || perfil.Codigo == 1406 || perfil.Codigo == 1407 || perfil.Codigo == 1409 || perfil.Codigo == 1411 || perfil.Codigo == 1413 || perfil.Codigo == 1414 || perfil.Codigo == 1415) {
                                                            return perfil.Codigo;
                                                        }
                                                    }));
                                                } else if (item.Sucursal == 'ANT') {
                                                    $scope.perfilTercerosSIESA = $scope.perfilTercerosSIESA.concat($scope.perfilTerceros.filter(function (perfil) {
                                                        if (perfil.Codigo == 1403) {
                                                            return perfil.Codigo;
                                                        }
                                                    }));
                                                } else if (item.Sucursal == 'NMM') {
                                                    $scope.perfilTercerosSIESA = $scope.perfilTercerosSIESA.concat($scope.perfilTerceros.filter(function (perfil) {
                                                        return perfil.Codigo == 1405;
                                                    }));
                                                } else if (item.Sucursal == 'NOM') {
                                                    $scope.perfilTercerosSIESA = $scope.perfilTercerosSIESA.concat($scope.perfilTerceros.filter(function (perfil) {
                                                        if (perfil.Codigo == 1405 || perfil.Codigo == 1413 || perfil.Codigo == 1415) {
                                                            return perfil.Codigo;
                                                        }
                                                    }));
                                                } else if (item.Sucursal.match(valoresAceptados)) {
                                                    $scope.perfilTercerosSIESA = $scope.perfilTercerosSIESA.concat($scope.perfilTerceros.filter(function (perfil) {
                                                        if (perfil.Codigo == 1408 || perfil.Codigo == 1412) {
                                                            return perfil.Codigo;
                                                        }
                                                    }));
                                                }
                                            }
                                        });

                                        $scope.perfilTerceros = $scope.perfilTercerosSIESA.filter((valor, indice) => {
                                            return $scope.perfilTercerosSIESA.indexOf(valor) === indice;
                                        });

                                        $scope.ListadoFormaPago = $scope.ListadoFormaPagoSIESA

                                        $scope.verificacionPerfiles()
                                    } else {

                                        $scope.ListadoEstados = [
                                            { Codigo: 0, Nombre: "INACTIVO" },
                                        ];

                                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==0');
                                        $scope.Modelo.JustificacionBloqueo = "No existe tercero en SIESA"
                                        $scope.ModalNovedades.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==0');
                                        $scope.ModalNovedades.Justificacion = "No existe tercero en SIESA"
                                        $scope.ValidarEstadoModalNovedades();
                                        ShowError("No existe el tercero en SIESA");
                                        $scope.Modelo.NumeroIdentificacion = ''

                                        $scope.novedadTercerosSIESA = ($scope.ListaNovedadesTercero.filter(function (novedad) {
                                            if (novedad.JustificacionNovedad == "No existe tercero en SIESA") {
                                                return novedad;
                                            }
                                        }));
                                        if (!($scope.novedadTercerosSIESA.length > 0) && $scope.Modelo.Codigo > 0) {
                                            $scope.GuardarNovedad()
                                        }

                                    }

                                }, function (response) {
                                    ShowError(response.statusText);
                                });



                            }
                        }
                    });

            }
            BloqueoPantalla.stop();
        };
        $scope.ObtenerBeneficiario = function () {
            if ($scope.ModalIdentificacionBeneficiario !== undefined && $scope.ModalIdentificacionBeneficiario !== null && $scope.ModalIdentificacionBeneficiario !== '' && $scope.ModalIdentificacionBeneficiario !== 0) {
                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.ModalIdentificacionBeneficiario,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ModalNombreBeneficiario = response.data.Datos.NombreCompleto
                                $scope.Modelo.Beneficiario = { Codigo: response.data.Datos.Codigo }
                            } else {
                                $scope.ModalNombreBeneficiario = ''
                                $scope.ModalIdentificacionBeneficiario = "";
                                ShowError('No se encontro ningún beneficiario asociado al número de identificación ingresado')
                            }
                        }
                    });
            } else {
                $scope.ModalNombreBeneficiario = ''
                $scope.ModalIdentificacionBeneficiario = "";
                $scope.Modelo.Beneficiario = { Codigo: 0 }
            }
        };

        $scope.AsignarConductor = function () {
            TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Conductor.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Conductor = response.data.Datos
                        try { $scope.EmailConductor = $scope.Conductor.Correo } catch (e) { }
                        try { $scope.TelefonoConductor = $scope.Conductor.Telefono } catch (e) { }
                        try { $scope.CelularConductor = $scope.Conductor.Celular } catch (e) { }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.ValidarDigitoChequeo = function (numeroIdentificacion, digitoChequeo) {
            var valido = true;

            var digitoValidoChequeo = 0;
            var sumaDigito = 0;
            var j = 0;
            var tamano = 0;
            var strNumeroIdentificacion = numeroIdentificacion.toString();

            var peso = [3, 7, 13, 17, 19, 23, 29, 37, 41, 43, 47, 53, 59, 67, 71];

            if (strNumeroIdentificacion.length <= 15) {
                tamano = strNumeroIdentificacion.length;
                for (var i = strNumeroIdentificacion.length; i > 0; i--) {
                    tamano = tamano - 1;
                    Aux = strNumeroIdentificacion.substring(tamano, i);
                    sumaDigito = sumaDigito + (Aux * peso[j]);
                    j = j + 1;
                }

                digitoValidoChequeo = sumaDigito % 11;

                if (digitoValidoChequeo > 1) {
                    digitoValidoChequeo = 11 - digitoValidoChequeo;
                }

                if (digitoValidoChequeo == digitoChequeo) {
                    continuar = true;
                } else {
                    continuar = false;
                }

            } else {
                continuar = false;
            }

            return continuar;
        }
        var archivo


        $scope.Tercero = { codigo: $scope.Modelo.Codigo };

        Array.prototype.unique = function (a) {
            return function () { return this.filter(a) }
        }(function (a, b, c) {
            return c.indexOf(a, b + 1) < 0
        });

        $scope.CargarArchivo = function (element) {
            $scope.IdPosiction = parseInt(element.id, 10)
            var continuar = true;
            if (element.files.length > 0) {
                $scope.PDF = element
                $scope.TipoDocumento = element
                blockUI.start();
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento = angular.element(this.ItemFoto)[0];
                } else {
                    angular.element(this.item)[0].Temporal = true
                    $scope.Documento = angular.element(this.item)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, poar favor verifique el formato del archivo  /nFormatos permitidos:" + $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2);
                    continuar = false;
                }
                if (continuar == true) {
                    if ($scope.Documento.TipoArchivoDocumento.Codigo !== 4300) {
                        var Formatos = $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2.split(',')
                        var cont = 0
                        for (var i = 0; i < Formatos.length; i++) {
                            Formatos[i] = Formatos[i].replace(' ', '')
                            var Extencion = element.files[0].name.split('.')
                            //if (element.files[0].type == Formatos[i]) {
                            if ('.' + (Extencion[1].toUpperCase()) == Formatos[i].toUpperCase()) {
                                cont++
                            }


                        }
                        if (cont == 0) {
                            ShowError("Archivo invalido, poar favor verifique el formato del archivo  Formatos permitidos:" + $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2);
                            continuar = false;
                        }
                    }
                    else if (element.files[0].size >= $scope.Documento.Size) //8 MB
                    {
                        ShowError("El archivo " + element.files[0].name + " supera los " + $scope.Documento.NombreTamano + ", intente con otro archivo", false);
                        continuar = false;
                    }
                }
                if (continuar == true) {
                    if ($scope.Documento.Archivo != undefined && $scope.Documento.Archivo != '' && $scope.Documento.Archivo != null) {
                        $scope.Documento.ArchivoTemporal = element.files[0]
                        showModal('modalConfirmacionRemplazarDocumento');
                    } else {
                        var reader = new FileReader();
                        $scope.Documento.ArchivoCargado = element.files[0]
                        reader.onload = $scope.AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }

                blockUI.stop();
            }
        }

        $scope.RemplazarDocumento = function () {
            var reader = new FileReader();
            $scope.Documento.ArchivoCargado = $scope.Documento.ArchivoTemporal
            reader.onload = $scope.AsignarArchivo;
            reader.readAsDataURL($scope.Documento.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumento');
        }


        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                //imagenes, tienen un proceso aparte ya que se deben redimencionar para que no ocupen mucho espacio en la BD
                if ($scope.Documento.ArchivoCargado.type.includes("image")) {
                    var img = new Image()
                    img.src = e.target.result
                    $timeout(function () {
                        $('#Foto' + $scope.IdPosiction).show()
                        $('#FotoCargada' + $scope.IdPosiction).hide()
                        if (img.height > img.width) {
                            RedimencionarImagen('CanvasVertical', img, 699, 499)
                            $scope.Documento.Archivo = document.getElementById('CanvasVertical').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')

                        }
                        //la imagen es horizontal
                        else {
                            RedimencionarImagen('CanvasHorizontal', img, 499, 699)
                            $scope.Documento.Archivo = document.getElementById('CanvasHorizontal').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                        }
                        $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                        var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                        $scope.Documento.NombreDocumento = Extencion[0]
                        $scope.Documento.ExtensionDocumento = Extencion[1]
                        if ($scope.Documento.TipoArchivoDocumento.Codigo == 4301) {
                            RedimencionarImagen('Foto' + $scope.IdPosiction, img, 200, 200)
                            $scope.ItemFoto.NombreDocumento = Extencion[0]
                            $scope.ItemFoto.ValorDocumento = 1
                            $scope.ItemFoto.temp = true
                        }
                        $scope.InsertarDocumento()
                    }, 100)
                }
                //Resto de archivos
                else {
                    $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                    $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                    var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                    $scope.Documento.NombreDocumento = Extencion[0]
                    $scope.Documento.ExtensionDocumento = Extencion[1]
                    $scope.InsertarDocumento()
                }
            });
        }

        $scope.InsertarDocumento = function () {
            $timeout(function () {
                // Guardar Archivo temporal
                var Documento = {};
                if ($scope.Documento.Codigo == 201) {
                    Documento = {
                        CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                        Configuracion: { Codigo: $scope.IdPosiction },
                        Archivo: $scope.Documento.Archivo,
                        Nombre: $scope.Documento.NombreDocumento,
                        Extension: $scope.Documento.ExtensionDocumento,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Tipo: $scope.Documento.Tipo
                    }
                } else {
                    Documento = {
                        CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                        Configuracion: { Codigo: $scope.Documento.Codigo },
                        Archivo: $scope.Documento.Archivo,
                        Nombre: $scope.Documento.NombreDocumento,
                        Extension: $scope.Documento.ExtensionDocumento,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Tipo: $scope.Documento.Tipo
                    }
                }


                DocumentosFactory.InsertarTemporal(Documento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            // Se guardó correctamente el pdf
                            if (response.data.Datos) {
                                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                                    var item = $scope.ListadoDocumentos[i]
                                    if (item.Codigo == $scope.Documento.Codigo) {
                                        item.ValorDocumento = 1
                                        item.temp = true
                                    }
                                }
                                ShowSuccess('Se cargó el documento adjunto "' + $scope.Documento.NombreDocumento + '"');
                                $scope.Documento = '';

                            }
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }, 200)
        }

        $scope.ElimiarDocumento = function (EliminarDocumento) {
            $scope.EliminarDocumento = EliminarDocumento
            //showModal('modalConfirmacionEliminarDocumento');
            $scope.IdPosiction = $scope.EliminarDocumento.IDNew;
            $scope.BorrarDocumento();
        };

        $scope.BorrarDocumento = function () {
            //closeModal('modalConfirmacionEliminarDocumento');
            $scope.EliminarDocumento.NombreDocumento = ''
            if ($scope.EliminarDocumento.temp) {
                $scope.EliminarDocumento.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                DocumentosFactory.EliminarDocumento($scope.EliminarDocumento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoDocumentos.forEach(function (item) {
                                if (item.Codigo == $scope.EliminarDocumento.Codigo) {
                                    item.ValorDocumento = 0
                                }
                            })
                            if ($scope.EliminarDocumento.TipoArchivoDocumento.Codigo == 4301) {
                                var img = new Image()
                                RedimencionarImagen('Foto' + $scope.IdPosiction, img, 200, 200)
                                $scope.EliminarDocumento.ValorDocumento = 0
                                $('#Foto' + $scope.IdPosiction).hide()
                                $scope.EliminarDocumento.FotoCargada = ''
                                $scope.EliminarDocumento.Archivo = undefined
                                $('#FotoCargada' + $scope.IdPosiction).hide()
                            }
                        }
                        else {
                            MensajeError("Error", response.data.MensajeError, false);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                    if ($scope.EliminarDocumento.Codigo == $scope.ListadoDocumentos[i].Codigo) {
                        $scope.ListadoDocumentos[i].ValorDocumento = 0
                    }
                }
                if ($scope.EliminarDocumento.TipoArchivoDocumento.Codigo == 4301) {
                    var img = new Image()
                    RedimencionarImagen('Foto' + $scope.IdPosiction, img, 200, 200)
                    $scope.EliminarDocumento.ValorDocumento = 0;
                    $scope.EliminarDocumento.NombreDocumento = '';
                    $('#Foto' + $scope.IdPosiction).hide()
                    $scope.EliminarDocumento.FotoCargada = ''
                    $scope.EliminarDocumento.Archivo = undefined
                    $('#FotoCargada' + $scope.IdPosiction).hide()
                }

            }
        }

        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {
            if (item.temp) {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Terceros&Codigo=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&CDGD_Codigo=' + item.Codigo + '&Temp=1');
            }
            else {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Terceros&Codigo=' + $scope.Modelo.Codigo + '&CDGD_Codigo=' + item.Codigo + '&Temp=0');
            }


        };

        $scope.MediosTerceros = []


        $scope.obtenerFormasPago = function (FormaPago) {
            if ($scope.Modelo.FormasDePago != null) {
                for (var j = 0; j < $scope.ListadoFormaPago.length; j++) {
                    for (var i = 0; i < $scope.Modelo.FormasDePago.length; i++) {
                        if ($scope.Modelo.FormasDePago[i].Codigo == $scope.ListadoFormaPago[j].Codigo) {
                            $scope.ListadoFormaPago[j].Estado = true


                            if ($scope.ListadoFormaPago[j].Estado) {
                                $scope.ContMedios++
                            } else {
                                $scope.ContMedios--
                            }


                        }
                    }

                }
            }

        }


        $scope.AsignarFormasPago = function (FormaPago) {

            if (FormaPago.Codigo == 4900) {
                if (FormaPago.Estado) {
                    $scope.ContMedios++
                } else {
                    $scope.ContMedios--
                }
            }
            if (FormaPago.Codigo == 4901) {
                if (FormaPago.Estado) {
                    $scope.ContMedios++
                } else {
                    $scope.ContMedios--
                }
            }
            if (FormaPago.Codigo == 4902) {
                if (FormaPago.Estado) {
                    $scope.ContMedios++
                } else {
                    $scope.ContMedios--
                }
            }
            if (FormaPago.Codigo == 4903) {
                if (FormaPago.Estado) {
                    $scope.ContMedios++
                } else {
                    $scope.ContMedios--
                }
            }

        };

        $scope.verificacionPerfiles = function () {
            $scope.ContPerfiltes = 0
            var prov = {}
            var tene = {}
            $scope.btncliente = false;
            $scope.btnempleado = false;
            ConditionEmpleado = false;
            $scope.btnconductor = false;
            ConditionConuctor = false;
            $scope.btnseguridad = false;
            $scope.btnproveedor = false;
            $scope.DivProveedor = false;

            $scope.perfilTerceros.forEach(function (perfil) {

                if (perfil.Codigo == 1401)  /*Cliente*/ {
                    if (perfil.Estado == true) {
                        $scope.btncliente = true;
                    } else {
                        $scope.btncliente = false;
                    }
                }

                if (perfil.Codigo == 1405)  /*Empleado*/ {
                    if (perfil.Estado == true) {
                        $scope.btnempleado = true;
                        ConditionEmpleado = true;
                    } else {
                        $scope.btnempleado = false;
                        ConditionEmpleado = false;
                    }
                }
                if (perfil.Codigo == 1403) /*conductor*/ {
                    if (perfil.Estado == true) {
                        $scope.btnconductor = true;
                        ConditionConuctor = true;
                    }
                    else {
                        $scope.btnconductor = false;
                        ConditionConuctor = false;
                    }
                }
                if (perfil.Codigo == 1409) /*Proveedor*/ {
                    prov = perfil
                }
                if (perfil.Codigo == 1412)  /*tenedor*/ {
                    tene = perfil

                }
                if (ConditionConuctor == true || ConditionEmpleado == true) {
                    $scope.btnseguridad = true;
                } else {
                    $scope.btnseguridad = false;
                }

                if (prov.Estado || tene.Estado) {
                    $scope.btnproveedor = true;
                    $scope.DivProveedor = true;
                } else {
                    $scope.btnproveedor = false;
                    $scope.DivProveedor = false;
                }
                if (perfil.Estado == true) {
                    $scope.ContPerfiltes++
                }
            }
            )

        };

        $scope.verificacionLineas = function () {
            $scope.ContLineas = 0
            $scope.LineaServicioTerceros.forEach(function (Linea) {
                if (Linea.Estado == true) {
                    $scope.ContLineas++
                }
            }
            )

        };
        $scope.DesactivarDireccion = function (Direccion) {

            if (Direccion.Estado !== 1) {
                Direccion.Estado = 1;
            } else {
                Direccion.Estado = 0;
            }
        };

        $scope.AgregarDireccion = function () {
            try {
                if (($scope.ListadoDirecciones[$scope.ListadoDirecciones.length - 1].Direccion == '') || ($scope.ListadoDirecciones[$scope.ListadoDirecciones.length - 1].Nombre == '')) {
                    ShowError('El registro anterior debe ingresarle tanto el nombre como la dirección')
                } else {
                    if ($scope.ListadoDirecciones.length >= 20) {
                        ShowError('No se pueden agregar mas direcciones, El número maximo de registros permitidos es 20')
                    } else {
                        $scope.ListadoDirecciones.push({ Direccion: '', Codigo: 0, Estado: 1 })
                    }
                }
            } catch (e) {
                $scope.ListadoDirecciones.push({ Direccion: '', Codigo: 0, Estado: 1 })
            }
        }
        $scope.AgregarImpuestoVenta = function () {
            var INT = 0
            if ($scope.Modelo.Cliente.Impuestos == undefined || $scope.Modelo.Cliente.Impuestos == '' || $scope.Modelo.Cliente.Impuestos == null) {
                $scope.Modelo.Cliente.Impuestos = []
                $scope.Modelo.Cliente.Impuestos.push($scope.ImpuestoVenta)
                $scope.ImpuestoVenta = ''
            }
            else {
                if ($scope.ImpuestoVenta == undefined || $scope.ImpuestoVenta == '' || $scope.ImpuestoVenta == null) {
                    ShowError('Debe seleccionar el impuesto')
                } else {
                    var cont = 0
                    for (var i = 0; i < $scope.Modelo.Cliente.Impuestos.length; i++) {
                        var item = $scope.Modelo.Cliente.Impuestos[i]
                        if (item.Codigo == $scope.ImpuestoVenta.Codigo) {
                            cont++
                            break;
                        }
                    }
                    if (cont > 0) {
                        ShowError('El impuesto ya se encuentra ingresado')
                    } else {
                        $scope.Modelo.Cliente.Impuestos.push($scope.ImpuestoVenta)
                    }
                }
                $scope.ImpuestoVenta = ''
            }
        };

        $scope.AgregarSitio = function () {
            if ($scope.SitioCliente == undefined || $scope.SitioCliente == '' || $scope.SitioCliente == null) {
                ShowError('Debe seleccionar un sitio');
            } else {
                $scope.SitioCliente.Nuevo = 1
                $scope.SitioCliente.Estado = true
                if ($scope.SitiosTerceroCliente.length <= CERO) {
                    $scope.SitiosTerceroCliente = [];
                    $scope.SitiosTerceroCliente.push($scope.SitioCliente);
                    $scope.ListaSitiosGrid.push($scope.SitioCliente);
                    $scope.SitiosTerceroClienteTotal.push($scope.SitioCliente);
                    $scope.SitioCliente = '';
                }
                else {
                    var cont = 0;
                    for (var i = 0; i < $scope.SitiosTerceroCliente.length; i++) {
                        var item = $scope.SitiosTerceroCliente[i].Codigo;
                        if (item == $scope.SitioCliente.Codigo) {
                            cont++;
                            break;
                        }
                    }
                    if (cont > 0) {
                        ShowError('El sitio ingresado ya se encuentra ingresado');
                    } else {
                        $scope.SitiosTerceroClienteTotal.push($scope.SitioCliente);
                        $scope.ListaSitiosGrid.push($scope.SitioCliente);
                        if ($scope.SitiosTerceroClienteTotal.length > 0) {
                            $scope.totalRegistros = $scope.SitiosTerceroClienteTotal.length;
                            $scope.totalPaginas = Math.ceil($scope.SitiosTerceroClienteTotal.length / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        } else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }
                $scope.SitioCliente = '';
            }
        };

        $scope.EliminarSitio = function (codigo, nombre) {
            showModal('modalConfirmacionEliminar');
            $scope.tempCodigo = codigo;
            $scope.tempNombre = nombre;
        };

        $scope.ConfirmarEliminar = function () {
            closeModal('modalConfirmacionEliminar');

            for (var i = 0; i < $scope.SitiosTerceroClienteTotal.length; i++) {
                var posicion = i;
                var item = $scope.SitiosTerceroClienteTotal[i].Codigo;
                if (item === $scope.tempCodigo) {
                    $scope.SitiosTerceroClienteTotal.splice(posicion, 1);
                }
            }

            for (var j = 0; j < $scope.ListaSitiosGrid.length; j++) {
                var posicion2 = j;
                var item2 = $scope.ListaSitiosGrid[j].Codigo;
                if (item2 === $scope.tempCodigo) {
                    $scope.ListaSitiosGrid.splice(posicion2, 1);
                }
            }

            if ($scope.SitiosTerceroClienteTotal.length > 0) {
                $scope.totalRegistros = $scope.SitiosTerceroClienteTotal.length;
                $scope.totalPaginas = Math.ceil($scope.SitiosTerceroClienteTotal.length / $scope.cantidadRegistrosPorPagina);
                $scope.Buscando = false;
                $scope.ResultadoSinRegistros = '';
            } else {
                $scope.totalRegistros = 0;
                $scope.totalPaginas = 0;
                $scope.paginaActual = 1;
                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                $scope.Buscando = false;
            }
        };

        $scope.EliminarImpuestoVenta = function (index) {
            $scope.Modelo.Cliente.Impuestos.splice(index, 1);
        };
        $scope.AgregarImpuestoCompra = function () {
            if ($scope.Modelo.Proveedor.Impuestos == undefined || $scope.Modelo.Proveedor.Impuestos == '' || $scope.Modelo.Proveedor.Impuestos == null) {
                $scope.Modelo.Proveedor.Impuestos = []
                $scope.Modelo.Proveedor.Impuestos.push($scope.ImpuestoCompra)
                $scope.ImpuestoCompra = ''
            }
            else {
                if ($scope.ImpuestoCompra == undefined || $scope.ImpuestoCompra == '' || $scope.ImpuestoCompra == null) {
                    ShowError('Debe seleccionar el impuesto')
                } else {
                    var cont = 0
                    for (var i = 0; i < $scope.Modelo.Proveedor.Impuestos.length; i++) {
                        var item = $scope.Modelo.Proveedor.Impuestos[i]
                        if (item.Codigo == $scope.ImpuestoCompra.Codigo) {
                            cont++
                            break;
                        }
                    }
                    if (cont > 0) {
                        ShowError('El impuesto ya se encuentra ingresado')
                    } else {
                        $scope.Modelo.Proveedor.Impuestos.push($scope.ImpuestoCompra)
                    }
                }
                $scope.ImpuestoCompra = ''
            }
        }
        $scope.EliminarImpuestoCompra = function (index) {
            $scope.Modelo.Proveedor.Impuestos.splice(index, 1)
        }
        //------------------------------------------------------------ Validacion Cupo Oficina ------------------------------------------------------------//
        //--AutoComplete Oficinas--//
        $scope.ListadoOficinas = [];
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.AsignarListadoOficinas = function (value) {
            return RetornarListaAutocomplete(value, $scope.ListadoOficinas, 'Nombre', 'Codigo');
        }

        $scope.DeshabilitarCupo = true;
        $scope.DeshabilitarSaldo = true;
        $scope.DeshabilitarGridCupoOficina = false;
        $scope.AsignarCupoSaldo = function (TipoValidacionCupo) {
            switch (TipoValidacionCupo.Codigo) {
                case CODIGO_CATALOGO_TIPO_VALIDACIÓN_CUPO_GENERAL:
                    $scope.DeshabilitarCupo = false;
                    $scope.DeshabilitarSaldo = false;
                    $scope.DeshabilitarGridCupoOficina = false;
                    break;
                case CODIGO_CATALOGO_TIPO_VALIDACIÓN_CUPO_OFICINA:
                    $scope.DeshabilitarCupo = true;
                    $scope.DeshabilitarSaldo = true;
                    $scope.Modelo.Saldo = 0;
                    $scope.Modelo.Cupo = 0;
                    $scope.DeshabilitarGridCupoOficina = true;
                    break;

                default:
                    $scope.DeshabilitarCupo = true;
                    $scope.DeshabilitarSaldo = true;
                    $scope.DeshabilitarGridCupoOficina = false;
                    break;
            }
        };
        //--Modal Cupo Oficina--//
        $scope.ListaCupoSede = [];
        function LimpiarmodalCupoSede() {
            $scope.ModalSede = '';
            $scope.ModalCupo = '';
            $scope.ModalSaldo = '';
            $scope.ListadoCupoDirecciones = [];

            $scope.ListadoDirecciones.forEach(function (item) {
                if (item.Estado == 1 && item.Codigo > 0) {
                    $scope.ListadoCupoDirecciones.push(item);
                }
            });
        }


        $scope.AgregarCupoSede = function () {
            LimpiarmodalCupoSede();
            showModal('modalCupoSede');
        };

        $scope.CerrarModalCupoSede = function () {
            $scope.MensajesErrorCupoSede = [];
            closeModal('modalCupoSede');
        };

        $scope.CargarCupoSede = function () {

            if ($scope.EditarCupo !== undefined && $scope.EditarCupo !== null) {
                if (DatosRequeridosCupoSede()) {
                    $scope.CerrarModalCupoSede();

                    $scope.ListaCupoSede.forEach(function (item) {
                        if (item.Codigo == $scope.EditarCupo.Codigo) {
                            item.Cupo = $scope.ModalCupo;
                            item.Saldo = $scope.ModalSaldo;
                        }
                    });
                    $scope.EditarCupo = null;
                    $scope.DeshabilitarSede = false;
                }
            }
            else {

                if (DatosRequeridosCupoSede()) {
                    $scope.CerrarModalCupoSede();

                    var SedeDuplicada = 0
                    if ($scope.ListaCupoSede.length > 0) {
                        $scope.ListaCupoSede.forEach(function (item) {
                            if (item.Codigo == $scope.ModalSede.Codigo) {
                                SedeDuplicada = 1;
                            }
                        });
                    }

                    if (SedeDuplicada == 0) {
                        $scope.ListaCupoSede.push({
                            Codigo: $scope.ModalSede.Codigo,
                            Nombre: $scope.ModalSede.Nombre,
                            Cupo: MascaraNumero($scope.ModalCupo),
                            Saldo: MascaraNumero($scope.ModalSaldo)
                        });
                        OrdenarIndicesSedes();
                    } else {
                        ShowError('Ya se asignó un cupo para la sede ' + $scope.ModalSede.Nombre.toUpperCase() + '.')
                    }
                }
            }
        };

        $scope.EditarCupoSede = function (Sede) {

            $scope.ModalSede = $linq.Enumerable().From($scope.ListadoDirecciones).First('$.Codigo ==' + Sede.Codigo);
            $scope.ModalCupo = MascaraNumero(Sede.Cupo);
            $scope.ModalSaldo = MascaraNumero(Sede.Saldo);
            $scope.EditarCupo = Sede;
            $scope.DeshabilitarSede = true;
            showModal('modalCupoSede');

        }

        function DatosRequeridosCupoSede() {
            $scope.MensajesErrorCupoSede = [];
            var continuar = true;
            if ($scope.ModalSede == undefined || $scope.ModalSede == "" || $scope.ModalSede == null) {
                $scope.MensajesErrorCupoSede.push('Debe seleccionar una Sede');
                continuar = false;
            }
            if ($scope.ModalCupo == undefined || $scope.ModalCupo == "" || $scope.ModalCupo == null) {
                $scope.MensajesErrorCupoSede.push('Debe ingresar cupo');
                continuar = false;
            }
            if ($scope.ModalSaldo == undefined || $scope.ModalSaldo == "" || $scope.ModalSaldo == null) {
                $scope.MensajesErrorCupoSede.push('Debe ingresar saldo');
                continuar = false;
            }
            if ($scope.ModalCupo > $scope.ModalSaldo) {
                $scope.MensajesErrorCupoSede.push('El valor del cupo no puede ser mayor al saldo');
                continuar = false;
            }

            return continuar;
        }
        //--Modal Cupo Oficina--//

        //--Eliminar Cupo Oficina--//
        $scope.TmpEliminarSede = {};
        $scope.ConfirmaEliminarSede = function (Nombre, Codigo, indice) {
            $scope.TmpEliminarSede = { Nombre: Nombre, Codigo: Codigo, Indice: indice };
            showModal('modalEliminarCupoOficina');
        };
        $scope.EliminarOficina = function (indice) {
            $scope.ListaCupoSede.splice(indice, 1);
            OrdenarIndicesSedes();
            closeModal('modalEliminarCupoOficina');
        };

        function OrdenarIndicesSedes() {
            $scope.Modelo.TerceroClienteCupoSedes = $scope.ListaCupoSede;
            for (var i = 0; i < $scope.ListaCupoSede.length; i++) {
                $scope.ListaCupoSede[i].indice = i;
            }
        }
        //--Eliminar Cupo Oficina--//
        //------------------------------------------------------------ Validacion Cupo Oficina ------------------------------------------------------------//

        //------------------------------------------------------------ Validacion Valor Faltante Peso Cumplido ------------------------------------------------------------//
        var isEditPesoCumplido = false;
        //$scope.DeshabilitarPesoCumplido = true;
        $scope.ListadoPesoCumplido = [];
        $scope.ListaProductoTransportado = [];
        $scope.ListadoUnidadEmpaque = [];
        $scope.ListadoBaseCalculo = [];
        $scope.ListadoTipoCalculo = [];
        $scope.ListadoCondicionCobro = [];
        $scope.ModalPesoCumplido = {
            Producto: '',
            UnidadEmpaque: '',
            BaseCalculo: '',
            ValorUnidad: '',
            TipoCalculo: '',
            Tolerancia: '',
            CondicionCobro: '',
        }
        //--Producto Transportado
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        //$scope.ListaProductoTransportado = response.data.Datos;
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListaProductoTransportado.push({ Codigo: response.data.Datos[i].Codigo, Nombre: response.data.Datos[i].Nombre })
                        }
                    }
                    else {
                        $scope.ListaProductoTransportado = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.AsignarProductoTransportado = function (value) {
            return RetornarListaAutocomplete(value, $scope.ListaProductoTransportado, 'Nombre', 'Codigo');
        }

        //--Unidad Medida
        UnidadMedidaFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        //$scope.ListadoUnidadEmpaque.push({ Nombre: 'SELECCIONE', Codigo: -1 });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoUnidadEmpaque.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                        }
                        $scope.ModalPesoCumplido.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo == ' + response.data.Datos[0].Codigo);
                    }
                    else {
                        $scope.ListadoUnidadEmpaque = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //-- Cata Base Calculo
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_BASE_CALCULO_PRODUCTO_CLIENTE }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        //$scope.ListadoBaseCalculo.push({ Nombre: 'SELECCIONE', Codigo: -1 });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoBaseCalculo.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                        }
                        $scope.ModalPesoCumplido.BaseCalculo = $linq.Enumerable().From($scope.ListadoBaseCalculo).First('$.Codigo == ' + response.data.Datos[0].Codigo);
                    }
                    else {
                        $scope.ListadoBaseCalculo = [];
                    }
                }
            }, function (response) {
            });

        //-- Cata Tipo Calculo
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CALCULO_PRODUCTO_CLIENTE }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        //$scope.ListadoTipoCalculo.push({ Nombre: 'SELECCIONE', Codigo: -1 });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoTipoCalculo.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                        }
                        $scope.ModalPesoCumplido.TipoCalculo = $linq.Enumerable().From($scope.ListadoTipoCalculo).First('$.Codigo == ' + response.data.Datos[0].Codigo);
                    }
                    else {
                        $scope.ListadoTipoCalculo = [];
                    }
                }
            }, function (response) {
            });

        //-- Cata Condicion Cobro
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CONDICION_COBRO_PRODUCTO_CLIENTE }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        //$scope.ListadoCondicionCobro.push({ Nombre: 'SELECCIONE', Codigo: -1 });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoCondicionCobro.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                        }
                        $scope.ModalPesoCumplido.CondicionCobro = $linq.Enumerable().From($scope.ListadoCondicionCobro).First('$.Codigo == ' + response.data.Datos[0].Codigo);
                    }
                    else {
                        $scope.ListadoCondicionCobro = [];
                    }
                }
            }, function (response) {
            });

        $scope.AgregarPesoCumplido = function () {
            LimpiarmodalPesoCumplido();
            showModal('modalPesoCumplido');
        };
        function LimpiarmodalPesoCumplido() {
            $scope.ModalPesoCumplido = {
                Producto: '',
                UnidadEmpaque: '',
                BaseCalculo: '',
                ValorUnidad: '',
                TipoCalculo: '',
                Tolerancia: '',
                CondicionCobro: '',
            };
            $scope.ModalPesoCumplido.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo == ' + $scope.ListadoUnidadEmpaque[0].Codigo);
            $scope.ModalPesoCumplido.BaseCalculo = $linq.Enumerable().From($scope.ListadoBaseCalculo).First('$.Codigo == ' + $scope.ListadoBaseCalculo[0].Codigo);
            $scope.ModalPesoCumplido.TipoCalculo = $linq.Enumerable().From($scope.ListadoTipoCalculo).First('$.Codigo == ' + $scope.ListadoTipoCalculo[0].Codigo);
            $scope.ModalPesoCumplido.CondicionCobro = $linq.Enumerable().From($scope.ListadoCondicionCobro).First('$.Codigo == ' + $scope.ListadoCondicionCobro[0].Codigo);
            $scope.MensajesErrorPesoCumplido = [];
            isEditPesoCumplido = false;
        }

        $scope.CargarPesoCumplido = function () {
            if (DatosRequeridosPesoCumplido()) {
                if (isEditPesoCumplido == false) {
                    $scope.ListadoPesoCumplido.push($scope.ModalPesoCumplido);
                    OrdenarIndicesPesoCumplido();
                }
                else {
                    $scope.ListadoPesoCumplido[$scope.ModalPesoCumplido.indice] = $scope.ModalPesoCumplido;
                }
                //$scope.Modelo.CondicionesPeso = $scope.ListadoPesoCumplido;
                closeModal('modalPesoCumplido');
            }
        };

        function DatosRequeridosPesoCumplido() {
            $scope.MensajesErrorPesoCumplido = [];
            var continuar = true;
            if ($scope.ModalPesoCumplido.Producto == undefined || $scope.ModalPesoCumplido.Producto == "" || $scope.ModalPesoCumplido.Producto == null) {
                $scope.MensajesErrorPesoCumplido.push('Debe seleccionar un producto');
                continuar = false;
            }

            if ($scope.ModalPesoCumplido.UnidadEmpaque.Codigo <= 0) {
                $scope.MensajesErrorPesoCumplido.push('Debe seleccionar una unidad de empaque');
                continuar = false;
            }

            if ($scope.ModalPesoCumplido.BaseCalculo.Codigo <= 0) {
                $scope.MensajesErrorPesoCumplido.push('Debe seleccionar una base de calculo');
                continuar = false;
            }

            if ($scope.ModalPesoCumplido.ValorUnidad == undefined || $scope.ModalPesoCumplido.ValorUnidad == "" || $scope.ModalPesoCumplido.ValorUnidad == null || isNaN($scope.ModalPesoCumplido.ValorUnidad)) {
                $scope.MensajesErrorPesoCumplido.push('Digite valor unidad');
                continuar = false;
            }

            if ($scope.ModalPesoCumplido.TipoCalculo.Codigo <= 0) {
                $scope.MensajesErrorPesoCumplido.push('Debe seleccionar tipo de calculo');
                continuar = false;
            }

            if ($scope.ModalPesoCumplido.Tolerancia == undefined || $scope.ModalPesoCumplido.Tolerancia == "" || $scope.ModalPesoCumplido.Tolerancia == null || isNaN($scope.ModalPesoCumplido.Tolerancia)) {
                $scope.MensajesErrorPesoCumplido.push('Digite tolerancia');
                continuar = false;
            }

            if ($scope.ModalPesoCumplido.CondicionCobro.Codigo <= 0) {
                $scope.MensajesErrorPesoCumplido.push('Debe seleccionar condición de cobro');
                continuar = false;
            }

            var indice = $scope.ModalPesoCumplido.indice != undefined ? $scope.ModalPesoCumplido.indice : -1;

            if (continuar == true) {
                for (var i = 0; i < $scope.ListadoPesoCumplido.length; i++) {
                    if (indice != i) {
                        if ($scope.ListadoPesoCumplido[i].Producto.Codigo == $scope.ModalPesoCumplido.Producto.Codigo) {
                            $scope.MensajesErrorPesoCumplido.push('El producto ya existe');
                            continuar = false;
                            break;
                        }
                    }
                }
            }

            return continuar;
        }

        //--Eliminar
        $scope.TmpEliminarPesoCumplido = {};
        $scope.ConfirmaEliminarPesoCumplido = function (item) {
            $scope.TmpEliminarPesoCumplido = { Indice: item.indice };
            showModal('modalEliminarPesCumplido');
        };

        $scope.EliminarPesoCumplido = function (indice) {
            $scope.ListadoPesoCumplido.splice(indice, 1);
            OrdenarIndicesPesoCumplido();
            closeModal('modalEliminarPesCumplido');
        };

        function OrdenarIndicesPesoCumplido() {
            //$scope.Modelo.CondicionesPeso = $scope.ListadoPesoCumplido;
            for (var i = 0; i < $scope.ListadoPesoCumplido.length; i++) {
                $scope.ListadoPesoCumplido[i].indice = i;
            }
        }

        $scope.EditarPesoCumplido = function (item) {
            LimpiarmodalPesoCumplido();
            $scope.ModalPesoCumplido = angular.copy(item);
            $scope.ModalPesoCumplido.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo == ' + item.UnidadEmpaque.Codigo);
            $scope.ModalPesoCumplido.BaseCalculo = $linq.Enumerable().From($scope.ListadoBaseCalculo).First('$.Codigo == ' + item.BaseCalculo.Codigo);
            $scope.ModalPesoCumplido.TipoCalculo = $linq.Enumerable().From($scope.ListadoTipoCalculo).First('$.Codigo == ' + item.TipoCalculo.Codigo);
            $scope.ModalPesoCumplido.CondicionCobro = $linq.Enumerable().From($scope.ListadoCondicionCobro).First('$.Codigo == ' + item.CondicionCobro.Codigo);
            isEditPesoCumplido = true;
            showModal('modalPesoCumplido');
        }

        //------------------------------------------------------------ Validacion Valor Faltante Peso Cumplido ------------------------------------------------------------//


        $scope.VerificarEmailFacturaElectronica = function () {
            if ($scope.Modelo.CorreoFacturaElectronica == undefined || $scope.Modelo.CorreoFacturaElectronica == null || $scope.Modelo.CorreoFacturaElectronica == '') {
                $scope.Modelo.CorreoFacturaElectronica = $scope.Modelo.Correo;
            }
        }


        $scope.AgregarCondicionComercial = function () {
            if ($scope.ParametrosPaqueteriaTarifario !== undefined && $scope.ParametrosPaqueteriaTarifario !== null && $scope.ParametrosPaqueteriaTarifario !== '') {
                if ($scope.Modelo.Cliente.ListadoCondicionesComerciales == undefined || $scope.Modelo.Cliente.ListadoCondicionesComerciales == null || $scope.Modelo.Cliente.ListadoCondicionesComerciales == '') {
                    $scope.Modelo.Cliente.ListadoCondicionesComerciales = [];
                    $scope.Modelo.Cliente.ListadoCondicionesComerciales.push({
                        LineaNegocio: {},
                        TipoLineaNegocio: {},
                        Tarifa: {},
                        TipoTarifa: {},
                        FleteMinimo: MascaraValores($scope.ParametrosPaqueteriaTarifario.MinimoValorUnidad),
                        ManejoMinimo: MascaraValores($scope.ParametrosPaqueteriaTarifario.MinimoValorManejo),
                        PorcentajeValorDeclarado: $scope.ParametrosPaqueteriaTarifario.PorcentajeManejo,
                        PorcentajeFlete: '',
                        PorcentajeVolumen: ''

                    });
                } else {
                    $scope.Modelo.Cliente.ListadoCondicionesComerciales.push({
                        LineaNegocio: {},
                        TipoLineaNegocio: {},
                        Tarifa: {},
                        TipoTarifa: {},
                        FleteMinimo: MascaraValores($scope.ParametrosPaqueteriaTarifario.MinimoValorUnidad),
                        ManejoMinimo: MascaraValores($scope.ParametrosPaqueteriaTarifario.MinimoValorManejo),
                        PorcentajeValorDeclarado: $scope.ParametrosPaqueteriaTarifario.PorcentajeManejo,
                        PorcentajeFlete: '',
                        PorcentajeVolumen: ''

                    });
                }
            }
        }

        $scope.CargarValoresPaqueteria = function (tarifario) {
            if (tarifario.Habilitado) {
                $scope.contTarifarios++;
            } else {
                $scope.contTarifarios--;
            }

            if (tarifario.TarifarioBase) {
                if (tarifario.Habilitado) {
                    $scope.ParametrosPaqueteriaTarifario = TarifarioVentasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: tarifario.Codigo, Sync: true }).Datos.Paqueteria
                    $scope.DeshabilitarCondicionesComerciales = false;
                } else {
                    $scope.ParametrosPaqueteriaTarifario = '';
                    $scope.DeshabilitarCondicionesComerciales = true;
                    $scope.Modelo.CondicionesComercialesTarifas = false;
                    $scope.EliminarCondicionesComerciales();
                }
            }

        }

        $scope.AgregarGestionDocumentos = function () {
            if ($scope.Modelo.Cliente.ListadoGestionDocumentos == undefined && $scope.Modelo.Cliente.ListadoGestionDocumentos == null && $scope.Modelo.Cliente.ListadoGestionDocumentos == '') {
                $scope.Modelo.Cliente.ListadoGestionDocumentos = [];
                $scope.Modelo.Cliente.ListadoGestionDocumentos.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Tercero: { Codigo: $scope.Modelo.Codigo },
                    TipoDocumento: {},
                    TipoGestion: {},
                    GeneraCobro: false,
                    Estado: { Codigo: ESTADO_ACTIVO }

                });
            } else {
                $scope.Modelo.Cliente.ListadoGestionDocumentos.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Tercero: { Codigo: $scope.Modelo.Codigo },
                    TipoDocumento: {},
                    TipoGestion: {},
                    GeneraCobro: false,
                    Estado: { Codigo: ESTADO_ACTIVO }

                });
            }
        }

        $scope.EliminarCondicionComercial = function (index) {
            if ($scope.Modelo.Cliente.ListadoCondicionesComerciales != undefined) {
                $scope.Modelo.Cliente.ListadoCondicionesComerciales.splice(index, 1);
            }
        }

        $scope.EliminarGestionDocumento = function (index) {
            if ($scope.Modelo.Cliente.ListadoGestionDocumentos != undefined) {
                $scope.Modelo.Cliente.ListadoGestionDocumentos.splice(index, 1);
            }
        }

        $scope.ValidarCondicionComercial = function (item, index) {

            if ($scope.Modelo.Cliente.ListadoCondicionesComerciales.length == 1) {
                if (item.TipoTarifa.ValorRangoFinal == 5) {
                    item.ManejoMinimo = MascaraValores($scope.ParametrosPaqueteriaTarifario.MinimoValorSeguro)
                } else {
                    item.ManejoMinimo = MascaraValores($scope.ParametrosPaqueteriaTarifario.MinimoValorManejo)
                }
            }

            for (var i = 0; i < $scope.Modelo.Cliente.ListadoCondicionesComerciales.length; i++) {
                if (index != i) {
                    itemListado = $scope.Modelo.Cliente.ListadoCondicionesComerciales[i];
                    if (itemListado.LineaNegocio.Codigo != undefined) {
                        if (item.LineaNegocio.Codigo == itemListado.LineaNegocio.Codigo) {
                            if (itemListado.TipoLineaNegocio.Codigo != undefined) {
                                if (item.TipoLineaNegocio.Codigo == itemListado.TipoLineaNegocio.Codigo) {
                                    if (itemListado.Tarifa.Codigo != undefined) {
                                        if (item.Tarifa.Codigo == itemListado.Tarifa.Codigo) {
                                            if (itemListado.TipoTarifa.Codigo != undefined) {
                                                if (item.TipoTarifa.Codigo == itemListado.TipoTarifa.Codigo) {
                                                    ShowError('Esta condición comercial ya existe para este cliente');
                                                    item.LineaNegocio = {};
                                                    item.TipoLineaNegocio = {};
                                                    item.Tarifa = {};
                                                    item.TipoTarifa = {};
                                                } else {
                                                    if (item.TipoTarifa.ValorRangoFinal == 5) {
                                                        item.ManejoMinimo = MascaraValores($scope.ParametrosPaqueteriaTarifario.MinimoValorSeguro)
                                                    } else {
                                                        item.ManejoMinimo = MascaraValores($scope.ParametrosPaqueteriaTarifario.MinimoValorManejo)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        $scope.ValidarGestionDocumento = function (item, index) {

            for (var i = 0; i < $scope.Modelo.Cliente.ListadoGestionDocumentos.length; i++) {
                if (index != i) {
                    var itemGestion = $scope.Modelo.Cliente.ListadoGestionDocumentos[i];
                    if (itemGestion.TipoDocumento.Codigo != undefined) {
                        if (item.TipoDocumento.Codigo == itemGestion.TipoDocumento.Codigo) {

                            ShowError('Esta gestión ya existe para este cliente');
                            item.TipoDocumento = {};
                            item.TipoGestion = {};
                            item.GeneraCobro = false;
                            item.Estado = {};

                        }
                    }
                }
            }

        }

        $scope.ConfirmarCambiosCondicionesComerciales = function () {
            if ($scope.Modelo.CondicionesComercialesTarifas == false || $scope.Modelo.CondicionesComercialesTarifas == 0) {
                ShowConfirm('Desea eliminar las condiciones comerciales para este Cliente?', $scope.EliminarCondicionesComerciales, function () { $scope.VerCondicionesComerciales = true; $scope.Modelo.CondicionesComercialesTarifas = true; $scope.Modelo.Cliente.ListadoCondicionesComerciales = $scope.Modelo.Cliente.ListadoCondicionesComerciales });
            } else {
                $scope.VerCondicionesComerciales = true;
            }
        }
        $scope.ConfirmarCambiosGestionDocumentos = function () {
            if ($scope.Modelo.GestionDocumentos == false || $scope.Modelo.GestionDocumentos == 0) {
                ShowConfirm('Desea eliminar la gestión de documentos para este Cliente?', $scope.EliminarGestionDocumentos, function () { $scope.VerGestionDocumentos = true; $scope.Modelo.GestionDocumentos = true; $scope.Modelo.Cliente.ListadoGestionDocumentos = $scope.Modelo.Cliente.ListadoGestionDocumentos });
            } else {
                $scope.VerGestionDocumentos = true;
            }
        }
        $scope.EliminarCondicionesComerciales = function () {
            $scope.VerCondicionesComerciales = false;
            $scope.Modelo.Cliente.ListadoCondicionesComerciales = '';
        }

        $scope.EliminarGestionDocumentos = function () {
            $scope.VerGestionDocumentos = false;
            $scope.Modelo.Cliente.ListadoGestionDocumentos = '';
        }

        $scope.CargarTipoLineaFiltrosGeneral = function (item) {
            if ($scope.Modelo.Cliente.LineaNegocio != undefined && $scope.Modelo.Cliente.LineaNegocio != null && $scope.Modelo.Cliente.LineaNegocio != '') {
                // $scope.HabilitarBotonesFiltros = false;
            }

            item.ListadoTipoLineaNegocioTransportesFiltrosGeneral = []
            $scope.ListadoTipoTarifaTransportesFiltrado = []
            $scope.ListadoTarifaTransportesFiltrado = []
            //$scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral.push({ Nombre: '(NO APLICA)' });
            for (var i = 0; i < $scope.ListadoTipoLineaNegocioTransportes.length; i++) {
                var item1 = $scope.ListadoTipoLineaNegocioTransportes[i]
                if (item1.LineaNegocioTransporte.Codigo == item.LineaNegocio.Codigo) {
                    item.ListadoTipoLineaNegocioTransportesFiltrosGeneral.push(item1)
                }
            }
            //$scope.TipoLineaNegocio = $scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral[0]
            $scope.HabilitarLineaNegocioFiltrosGeneral = false
            //$scope.HabilitarTipoLineaNegocio = true
            //$scope.HabilitarTipoTarifa = true


            //if (Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA) {
            //    $scope.HabilitarRutaFiltrosGeneral = false
            //    $scope.HabilitarFormaPagoFiltrosGeneral = true
            //}
            //else {
            //    $scope.HabilitarRutaFiltrosGeneral = true
            //    $scope.HabilitarFormaPagoFiltrosGeneral = false
            //}

        }

        $scope.CargarTarifaFiltrosGeneral = function (item) {
            item.ListadoTarifaTransportesFiltrado = []
            //$scope.ListadoTarifaTransportesFiltrado.push({ Nombre: '(NO APLICA)' });
            for (var i = 0; i < $scope.ListadoTarifaTransportes.length; i++) {
                var item1 = $scope.ListadoTarifaTransportes[i]
                if (item1.TipoLineaNegocioTransporte.Codigo == item.TipoLineaNegocio.Codigo) {
                    item.ListadoTarifaTransportesFiltrado.push(item1)
                }
            }
            //$scope.Modal.TarifaTransportes = $scope.ListadoTarifaTransportesFiltrado[0]
            $scope.HabilitarLineaNegocioFiltrosGeneral = false
            //$scope.HabilitarTipoLineaNegocioFiltrosGeneral = false
            //$scope.HabilitarTipoTarifa = true
        }

        $scope.CargarTipoTarifaFiltrosGeneral = function (item) {
            item.ListadoTipoTarifaTransportesFiltrado = []
            if (item.Tarifa.Codigo == 300 || item.Tarifa.Codigo == 6 || item.Tarifa.Codigo == 302 || item.Tarifa.Codigo == 5) { //BARRIL GALON TONELADA PRODUCTO
                item.ListadoTipoTarifaTransportesFiltrado = $scope.ListadoProductosTransportados
            }
            else {
                for (var i = 0; i < $scope.ListadoTipoTarifaTransportes.length; i++) {
                    var item1 = $scope.ListadoTipoTarifaTransportes[i]
                    if (item1.TarifaTransporte.Codigo == item.Tarifa.Codigo) {
                        item.ListadoTipoTarifaTransportesFiltrado.push(item1)
                    }
                } if (item.ListadoTipoTarifaTransportesFiltrado.length == 0) {
                    item.ListadoTipoTarifaTransportesFiltrado.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                }
            }
            //$scope.Modal.TipoTarifaTransportes = $scope.ListadoTipoTarifaTransportesFiltrado[0]
            //$scope.HabilitarTipoTarifa = false


        }

        //------------------------------------------------------------ Mascaras ------------------------------------------------------------//
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item);
        };
        $scope.MaskMayus = function (option) {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item);
        };
        $scope.MaskMinus = function () {
            try { $scope.Modelo.Correo = $scope.Modelo.Correo.toLowerCase() } catch (e) { }
        }
        $scope.MaskDireccion = function (option) {
            try { $scope.Modelo.Direccion = MascaraDireccion($scope.Modelo.Direccion) } catch (e) { }
        };
        $scope.MaskTelefono = function (option) {
            try { $scope.Modelo.Telefono = MascaraTelefono($scope.Modelo.Telefono) } catch (e) { }
            try { $scope.Modelo.Fax = MascaraTelefono($scope.Modelo.Fax) } catch (e) { }
        };
        $scope.MaskCelular = function (option) {
            try { $scope.Modelo.Celular = MascaraCelular($scope.Modelo.Celular) } catch (e) { }
        };
        $scope.MaskSelect = function (option, select) {
            try { MascaraNaturalezaTercero(option, select) } catch (e) { }
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item);
        };
        $scope.AsignarLicencia = function (item) {
            if (item.Documento.Nombre == 'Licencia Conducción') {
                $scope.Modelo.Conductor.NumeroLicencia = angular.copy(item.Referencia)
                $scope.Modelo.Conductor.FechaVencimiento = new Date(angular.copy(item.FechaVence))

            }
        }
        $scope.AsignarLicencia2 = function () {
            if ($scope.ListadoDocumentos != undefined) {
                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                    var item = $scope.ListadoDocumentos[i]
                    if (item.Documento.Nombre == 'Licencia Conducción') {
                        item.Referencia = $scope.Modelo.Conductor.NumeroLicencia
                        item.FechaVence = new Date($scope.Modelo.Conductor.FechaVencimiento)
                    }
                }
            }
        }
        $scope.AsignarModificacion = function (item) {
            if (!(item.Nuevo > 0)) {
                item.Modifica = 1
            }
        }

        //Novedades:

        $scope.PrimerPaginaNovedades = function () {
            $scope.NumeroPaginaNovedades = 1;
            Paginar();
        }
        $scope.AnteriorNovedades = function () {
            if ($scope.NumeroPaginaNovedades > 1) {
                $scope.NumeroPaginaNovedades--;
                Paginar();
            }

        }
        $scope.SiguienteNovedades = function () {
            if ($scope.NumeroPaginaNovedades < (Math.ceil($scope.ListaNovedadesTercero.length / $scope.CantidadRegistrosPorPaginaNovedades))) {
                $scope.NumeroPaginaNovedades++;
                Paginar();
            }
        }
        $scope.UltimaPaginaNovedades = function () {
            $scope.NumeroPaginaNovedades = Math.ceil($scope.ListaNovedadesTercero.length / $scope.CantidadRegistrosPorPaginaNovedades);
            Paginar();
        }



        function Paginar() {
            $scope.ListadoFiltradoNovedadesTercero = [];
            var ListaNovedadesOrdenado = $scope.ListaNovedadesTercero.sort((a, b) => { return (new Date(b.Fecha) - new Date(a.Fecha)) })
            $scope.totalPaginasNovedades = Math.ceil(ListaNovedadesOrdenado.length / $scope.CantidadRegistrosPorPaginaNovedades);
            var RegistroInicio = ($scope.NumeroPaginaNovedades * $scope.CantidadRegistrosPorPaginaNovedades) - $scope.CantidadRegistrosPorPaginaNovedades;
            var RegistroFin = ($scope.NumeroPaginaNovedades * $scope.CantidadRegistrosPorPaginaNovedades) - 1
            if (ListaNovedadesOrdenado.length > 0) {
                for (var i = 0; i < ListaNovedadesOrdenado.length; i++) {
                    if (i >= RegistroInicio && i <= RegistroFin) {
                        $scope.ListadoFiltradoNovedadesTercero.push(ListaNovedadesOrdenado[i])
                    }
                }
            }
        }

        $scope.AgregarNovedadTercero = function () {
            $scope.ListadoNovedades = [];
            $scope.perfilTerceros.forEach(item => {
                if (item.Estado == true || item.Estado == 1) {
                    if (item.Codigo == PERFIL_CONDUCTOR) {
                        $scope.ListadoCompletoNovedades.forEach(itemNovedad => {
                            if (itemNovedad.CampoAuxiliar2 == 1) {
                                if ($scope.ListadoNovedades.length == 0) {
                                    $scope.ListadoNovedades.push(itemNovedad);
                                } else {
                                    var countCoincidencias = 0
                                    $scope.ListadoNovedades.forEach(itemNov => {
                                        if (itemNovedad.Nombre == itemNov.Nombre) {
                                            countCoincidencias++;
                                        }
                                    });
                                    if (countCoincidencias == 0) {
                                        $scope.ListadoNovedades.push(itemNovedad);
                                    }
                                }
                            }
                        });
                    } else if (item.Codigo == PERFIL_TENEDOR || item.Codigo == PERFIL_PROPIETARIO) {
                        $scope.ListadoCompletoNovedades.forEach(itemNovedad => {
                            if (itemNovedad.CampoAuxiliar2 == 2) {
                                if ($scope.ListadoNovedades.length == 0) {
                                    $scope.ListadoNovedades.push(itemNovedad);
                                } else {
                                    // Verificar que el concepto no sea igual a un concepto de otro perfil en el caso de que el tercero tenga varios perfiles(Conductor, propietario o tenedor):
                                    var countCoincidencias = 0
                                    $scope.ListadoNovedades.forEach(itemNov => {
                                        if (itemNovedad.Nombre == itemNov.Nombre) {
                                            countCoincidencias++;
                                        }
                                    });
                                    if (countCoincidencias == 0) {
                                        $scope.ListadoNovedades.push(itemNovedad);
                                    }
                                }
                            }
                        });
                    }

                }
            });

            $scope.ModalNovedades.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==1');
            if ($scope.ListadoNovedades.length > 0) {
                $scope.ModalNovedades.Novedad = $scope.ListadoNovedades[0];
            }
            $scope.ModalNovedades.Justificacion = '';
            showModal('modalAgregarNovedad');

        }

        $scope.ValidarEstadoModalNovedades = function () {
            $scope.ListadoNovedades = [];
            $scope.ListadoCompletoNovedades.forEach(itemNovedad => {
                if ($scope.ModalNovedades.Estado.Codigo == ESTADO_ACTIVO) {

                    if (itemNovedad.CampoAuxiliar4 == ESTADO_ACTIVO) {
                        $scope.ListadoNovedades.push(itemNovedad);
                    }
                } else {
                    if (itemNovedad.CampoAuxiliar4 == ESTADO_INACTIVO) {
                        $scope.ListadoNovedades.push(itemNovedad);
                    }
                }
                $scope.ModalNovedades.Novedad = $scope.ListadoNovedades[0];
            });

        }


        $scope.GuardarNovedad = function () {
            BloqueoPantalla.start('Guardando Novedad...');
            var Novedad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
                Estado: $scope.ModalNovedades.Estado,
                Novedad: { Codigo: $scope.ModalNovedades.Novedad.Codigo },
                JustificacionNovedad: $scope.ModalNovedades.Justificacion,
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            }

            TercerosFactory.GuardarNovedad(Novedad).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        $scope.ListaNovedadesTercero.push({
                            Fecha: new Date(),
                            Hora: new Date().getHours(),
                            Responsable: { NombreCompleto: $scope.Sesion.UsuarioAutenticado.Empleado.Nombre },
                            Estado: Novedad.Estado,
                            Novedad: $scope.ModalNovedades.Novedad,
                            JustificacionNovedad: Novedad.JustificacionNovedad

                        });

                        $scope.ListaNovedadesTercero.forEach(item => {
                            item.Fecha = new Date(item.Fecha)
                        });

                        $scope.Modelo.JustificacionBloqueo = Novedad.JustificacionNovedad;
                        if (Novedad.Estado.Codigo == ESTADO_ACTIVO) {
                            $('#JustificacionBloqueo').hide();
                        } else {
                            $('#JustificacionBloqueo').show();
                        }
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + Novedad.Estado.Codigo);
                        Paginar();

                        ShowSuccess('Se guardó la novedad con éxito');
                        location.href = '#!ConsultarTerceros/' + $scope.Modelo.Codigo;

                    } else {
                        ShowError(response.statusText)
                    }
                });
            BloqueoPantalla.stop();
            closeModal('modalAgregarNovedad');
        }

        $scope.DesplegarExcel = function (OpcionPDf, OpcionEXCEL, OpcionListado) {

            // $scope.OpcionLista = OpcionListado;

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NombreReporte = NOMBRE_LISTADO_EXCEL_NOVEDADES_TERCERO;




            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&TERC_Codigo=' + $scope.Modelo.Codigo + '&OpcionExportarPdf=' + OpcionPDf);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&TERC_Codigo=' + $scope.Modelo.Codigo + '&OpcionExportarExcel=' + OpcionEXCEL);
            }



        };


        //Fin Novedades


        if ($scope.Sesion.UsuarioAutenticado.ManejoEstadosNovedades) {
            $scope.DeshabilitarEstado = true;
        }


        $scope.MaskPorcentaje = function (value) {
            var numeroarray = [];
            for (var i = 0; i < value.length; i++) {
                patron = /[0123456789.]/
                if (i == 0 && value[i] == '-') {
                    numeroarray.push(value[i])
                }
                else if (patron.test(value[i])) {
                    if (patron.test(value[i]) == ' ') {
                        option[i] = '';
                    } else {
                        numeroarray.push(value[i])
                    }
                }
            }

            var val = numeroarray.join('');
            $scope.Modelo.Empleado.PorcentajeComision = val;
            var nval = [];
            if (val[1] != '.') {
                if (val.length == 3) {
                    if (val[0] != 1 && val[0] != "1" && val[2] != ".") {
                        $scope.MaxLength = 3;
                        nval.push(val[0]);
                        nval.push(val[1]);
                        $scope.Modelo.Empleado.PorcentajeComision = nval.join('');
                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.Empleado.PorcentajeComision = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.Empleado.PorcentajeComision = nval.join('');
                        }
                    } else if (val[2] == ".") {
                        $scope.MaxLength = 5;
                    }
                    else if ((val[0] == 1 || val[0] == "1") && val[2] != ".") {
                        $scope.MaxLength = 3;

                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.Empleado.PorcentajeComision = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            $scope.Modelo.Empleado.PorcentajeComision = nval.join('');
                        }
                    }

                }
            } else {
                $scope.MaxLength = 4;
            }
        }

        $scope.MaskPorcentaje2 = function (value, item) {
            var numeroarray = [];
            for (var i = 0; i < value.length; i++) {
                patron = /[0123456789.]/
                if (i == 0 && value[i] == '-') {
                    numeroarray.push(value[i])
                }
                else if (patron.test(value[i])) {
                    if (patron.test(value[i]) == ' ') {
                        option[i] = '';
                    } else {
                        numeroarray.push(value[i])
                    }
                }
            }

            var val = numeroarray.join('');
            item = val;
            var nval = [];
            if (val[1] != '.') {
                if (val.length == 3) {
                    if (val[0] != 1 && val[0] != "1" && val[2] != ".") {
                        var MaxLength = 3;
                        nval.push(val[0]);
                        nval.push(val[1]);
                        item = nval.join('');
                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        }
                    } else if (val[2] == ".") {
                        MaxLength = 5;
                    }
                    else if ((val[0] == 1 || val[0] == "1") && val[2] != ".") {
                        MaxLength = 3;

                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        }
                    }

                } else if (val.length > 3) {
                    if (val[2] == ".") {
                        if (val.length == 4) {
                            nval.push(val[0]);
                            nval.push(val[1]);
                            nval.push(val[2]);
                            if (val[3] != ".") {
                                nval.push(val[3]);
                            }
                            item = nval.join('');
                        } else if (val.length >= 5) {
                            nval.push(val[0]);
                            nval.push(val[1]);
                            nval.push(val[2]);
                            if (val[3] != ".") {
                                nval.push(val[3]);
                                if (val[4] != ".") {
                                    nval.push(val[4]);
                                }
                            }

                            item = nval.join('');
                        }
                    } else {
                        nval.push(val[0]);
                        nval.push(val[1]);
                        nval.push(val[2]);
                        item = nval.join('');
                    }

                }
            } else {
                nval.push(val[0]);
                nval.push(val[1]);
                if (val[2] != ".") {
                    nval.push(val[2]);
                    if (val[3] != ".") {
                        nval.push(val[3]);
                    }
                }


                item = nval.join('');
                MaxLength = 4;
            }
            return item;
        }
    }]);