﻿EncoExpresApp.controller("ConsultarEmpresasCtrl", ['$scope', '$routeParams', '$timeout', 'EmpresasFactory', '$linq', 'blockUI', function ($scope, $routeParams, $timeout, EmpresasFactory, $linq, blockUI) {

    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Empresas' }];
    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
    Find();
    var filtros = {};
    $scope.cantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.ModalErrorCompleto = ''
    $scope.ModalError = ''
    $scope.MostrarMensajeError = false
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_EMPRESAS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.ListadoEmpresasMaster = [];

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */

    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
            document.location.href = '#!GestionarEmpresas';
        }
    };
    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
            $scope.Codigo = 0
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoEmpresasMaster = [];
        if ($scope.Buscando) {
            if ($scope.MensajesError.length === 0) {
                filtros = {
                    Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                }
                blockUI.delay = 1000;
                EmpresasFactory.ConsultarMaster(filtros).
                       then(function (response) {
                           if (response.data.ProcesoExitoso === true) {
                               if (response.data.Datos.length > 0) {
                                   
                                   $scope.ListadoEmpresasMaster = response.data.Datos

                                   $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                   $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                   $scope.Buscando = false;
                                   $scope.ResultadoSinRegistros = '';
                               }
                               else {
                                   $scope.totalRegistros = 0;
                                   $scope.totalPaginas = 0;
                                   $scope.paginaActual = 1;
                                   $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                   $scope.Buscando = false;
                               }
                           }
                       }, function (response) {
                           ShowError(response.statusText);
                       });
            }
        }
        blockUI.stop();
    }
    /*------------------------------------------------------------------------------------Eliminar Ciudades-----------------------------------------------------------------------*/

    /* Obtener parametros*/
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    $scope.MaskNumero = function (option) {
        try { $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno) } catch (e) { }
    };

}]);