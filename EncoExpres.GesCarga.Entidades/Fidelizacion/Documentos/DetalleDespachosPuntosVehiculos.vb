﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Namespace Fidelizacion.Documentos
    <JsonObject>
    Public NotInheritable Class DetalleDespachosPuntosVehiculos
        'Inherits BaseBasico
        Inherits BaseDocumento


        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "VEHI_Placa")}
            Tenedor = New Tercero With {.Codigo = Read(lector, "TENE_Codigo"), .NombreCompleto = Read(lector, "TENE_Nombre")}
            Conductor = New Tercero With {.Codigo = Read(lector, "COND_Codigo"), .NombreCompleto = Read(lector, "COND_Nombre")}

            Manifiesto = New Manifiesto With {.Numero = Read(lector, "ENMC_Numero"), .NumeroDocumento = Read(lector, "ENMC_NumeroDocumento")}
            Planilla = New Planillas With {.Numero = Read(lector, "ENPD_Numero"), .NumeroDocumento = Read(lector, "ENPD_NumeroDocumento")}
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "RUTA_Nombre")}
            Puntos = Read(lector, "Puntos")
            FechaInicioVigencia = Read(lector, "FechaInicio")
            FechaFinVigencia = Read(lector, "FechaFin")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            UsuarioCrea = New Usuarios With {.CodigoUsuario = Read(lector, "USUA_CodigoUsuario")}
            FechaCrea = Read(lector, "Fecha_Crea")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub

        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Tenedor As Tercero
        <JsonProperty>
        Public Property Conductor As Tercero
        <JsonProperty>
        Public Property Manifiesto As Manifiesto
        <JsonProperty>
        Public Property Planilla As Planillas
        <JsonProperty>
        Public Property NumeroManifiesto As Long
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property Puntos As Long
        <JsonProperty>
        Public Property FechaInicioVigencia As Date
        <JsonProperty>
        Public Property FechaFinVigencia As Date
        '''Filtros Fechas
        <JsonProperty>
        Public Property FechaInicioVigenciaDesde As Date
        <JsonProperty>
        Public Property FechaInicioVigenciaHasta As Date
        <JsonProperty>
        Public Property FechaFinVigenciaDesde As Date
        <JsonProperty>
        Public Property FechaFinVigenciaHasta As Date

    End Class
End Namespace
