﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Negocio.ControlViajes

''' <summary>
''' Controlador <see cref="DocumentoInspeccionPreoperacionalController"/>
''' </summary>
<Authorize>
Public Class DocumentoInspeccionPreoperacionalController
    Inherits Base


    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DocumentoInspeccionPreoperacional) As Respuesta(Of IEnumerable(Of DocumentoInspeccionPreoperacional))
        Return New LogicaDocumentoInspeccionPreoperacional(CapaPersistenciaDocumentoInspeccionPreoperacional).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("InsertarTemporal")>
    Public Function Guardar(ByVal entidad As DocumentoInspeccionPreoperacional) As Respuesta(Of Long)
        Return New LogicaDocumentoInspeccionPreoperacional(CapaPersistenciaDocumentoInspeccionPreoperacional).InsertarTemporal(entidad)
    End Function
    <HttpPost>
    <ActionName("EliminarFoto")>
    Public Function EliminarFoto(ByVal entidad As DocumentoInspeccionPreoperacional) As Respuesta(Of Boolean)
        Return New LogicaDocumentoInspeccionPreoperacional(CapaPersistenciaDocumentoInspeccionPreoperacional).EliminarFoto(entidad)
    End Function

    <HttpPost>
    <ActionName("LimpiarTemporalUsuario")>
    Public Function LimpiarTemporalUsuario(ByVal entidad As DocumentoInspeccionPreoperacional) As Respuesta(Of Boolean)
        Return New LogicaDocumentoInspeccionPreoperacional(CapaPersistenciaDocumentoInspeccionPreoperacional).LimpiarTemporalUsuario(entidad)
    End Function

End Class
