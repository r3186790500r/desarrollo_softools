﻿EncoExpresApp.controller("GestionarTarifarioComprasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TarifarioComprasFactory', 'LineaNegocioTransportesFactory', 'TarifaTransportesFactory', 'TipoLineaNegocioTransportesFactory', 'TipoTarifaTransportesFactory', 'RutasFactory', 'ValorCatalogosFactory', 'ProductoTransportadosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TarifarioComprasFactory, LineaNegocioTransportesFactory, TarifaTransportesFactory, TipoLineaNegocioTransportesFactory, TipoTarifaTransportesFactory, RutasFactory, ValorCatalogosFactory, ProductoTransportadosFactory) {
        console.clear()
        $scope.Titulo = 'GESTIONAR TARIFARIO COMPRAS';
        $scope.MapaSitio = [{ Nombre: 'Comercial' }, { Nombre: 'Documentos' }, { Nombre: 'Tarifario Compras' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_TARIFARIO_COMPRAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.verData = false

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.NumeroTarifario = 0;
        $scope.HabilitarBotonesFiltros = true;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }
        $scope.Modal = {}

        $scope.CargarDatosFunciones = function () {
            $scope.ListadoEstados = [
                { Codigo: 0, Nombre: "INACTIVA" },
                { Codigo: 1, Nombre: "ACTIVA" },
            ]
            $scope.AplicaPaqueteria = false
            try {
                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado.Codigo);
                for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                    var tarifa = $scope.Modelo.Tarifas[i]
                    for (var j = 0; j < $scope.ListadoEstados.length; j++) {
                        var estado = $scope.ListadoEstados[j]
                        try {
                            if (tarifa.Estado.Codigo == estado.Codigo) {
                                tarifa.Estado = estado
                            }
                        } catch (e) {

                        }

                    }
                }
            } catch (e) {
                $scope.Modelo.Estado = $scope.ListadoEstados[1]
            }
            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
            /*Cargar el combo de LineaNegocioTransportes*/
            LineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoLineaNegocioTransportes = [];
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if (response.data.Datos[i].Codigo == 1) {
                                    $scope.ListadoLineaNegocioTransportes.push(response.data.Datos[i]);
                                }
                            }
                            $scope.LineaNegocio = $scope.ListadoLineaNegocioTransportes[0]
                            $scope.Modal.LineaNegocioTransportes = $scope.ListadoLineaNegocioTransportes[0]
                            
                            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                var tarifa = $scope.Modelo.Tarifas[i]
                                for (var j = 0; j < $scope.ListadoLineaNegocioTransportes.length; j++) {
                                    var linea = $scope.ListadoLineaNegocioTransportes[j]
                                    try {
                                        if (tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == linea.Codigo) {
                                            tarifa.LineaNegocioTransportes = linea
                                        }
                                    } catch (e) {

                                    }

                                }
                                tarifa.Mostrar = true;
                            }
                            try { $scope.ListadoTarifas = $scope.Modelo.Tarifas } catch (e) { }
                        }
                        else {
                            $scope.ListadoLineaNegocioTransportes = []
                        }
                    }
                }, function (response) {
                });

            TarifarioComprasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTarifarioVentas = [];
                            $scope.ListadoTarifarioVentas = response.data.Datos

                            $scope.ListadoTarifarioVentas.forEach(function (item) {
                                if (item.TarifarioBase == 1) {
                                    $scope.habilitarTarifarioBase = true
                                }
                            })
                        }
                    }
                })
            /*Cargar el combo de TarifaTransportes*/
            TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTarifaTransportes = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTarifaTransportes = response.data.Datos;
                            //$scope.Modal.TarifaTransportes = $scope.ListadoTarifaTransportes[0]
                            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                var tarifa = $scope.Modelo.Tarifas[i]
                                for (var j = 0; j < $scope.ListadoTarifaTransportes.length; j++) {
                                    var lstarifa = $scope.ListadoTarifaTransportes[j]
                                    try {
                                        if (tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == lstarifa.Codigo) {
                                            tarifa.TarifaTransportes = lstarifa
                                        }
                                    } catch (e) {

                                    }

                                }
                            }
                            try { $scope.ListadoTarifas = $scope.Modelo.Tarifas } catch (e) { }
                        }
                        else {
                            $scope.ListadoTarifaTransportes = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TipoLineaNegocioTransportes*/
            TipoLineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoLineaNegocioTransportes = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoLineaNegocioTransportes = response.data.Datos;

                            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                var tarifa = $scope.Modelo.Tarifas[i]
                                for (var j = 0; j < $scope.ListadoTipoLineaNegocioTransportes.length; j++) {
                                    var Linea = $scope.ListadoTipoLineaNegocioTransportes[j]
                                    try {
                                        if (tarifa.TipoLineaNegocioTransportes.Codigo == Linea.Codigo) {
                                            tarifa.TipoLineaNegocioTransportes = Linea
                                        }
                                    } catch (e) {

                                    }

                                }
                            }
                            try { $scope.ListadoTarifas = $scope.Modelo.Tarifas} catch (e) { }
                        }
                        else {
                            $scope.ListadoTipoLineaNegocioTransportes = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TipoTarifaTransportes*/
            TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTarifaTransportes = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoTarifaTransportes = response.data.Datos;
                            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                var tarifa = $scope.Modelo.Tarifas[i]
                                for (var j = 0; j < $scope.ListadoTipoTarifaTransportes.length; j++) {
                                    var Linea = $scope.ListadoTipoTarifaTransportes[j]
                                    try {
                                        if (tarifa.TipoTarifaTransportes.Codigo == Linea.Codigo && tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == Linea.TarifaTransporte.Codigo && !(tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 301 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 300 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 302 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 6 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 5)) {
                                            tarifa.TipoTarifaTransportes = Linea
                                        }
                                    } catch (e) {

                                    }

                                }
                            }
                            try { $scope.ListadoTarifas = $scope.Modelo.Tarifas } catch (e) { }
                        }
                        else {
                            $scope.ListadoTipoTarifaTransportes = []
                        }
                        $scope.CargarTipoLineaFiltrosGeneral($scope.LineaNegocio.Codigo)
                    }
                }, function (response) {
                });
            RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoRutas = [];
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListadoRutas.push(response.data.Datos[i]);
                            }
                            $scope.Modal.Rutas = $scope.ListadoRutas[0]
                            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                var tarifa = $scope.Modelo.Tarifas[i]
                                for (var j = 0; j < $scope.ListadoRutas.length; j++) {
                                    var ruta = $scope.ListadoRutas[j]
                                    try {
                                        if (tarifa.Ruta.Codigo == ruta.Codigo) {
                                            tarifa.Ruta = ruta
                                        }
                                    } catch (e) {

                                    }

                                }
                            }
                            try { $scope.ListadoTarifas = $scope.Modelo.Tarifas } catch (e) { }
                        }
                        else {
                            $scope.ListadoRutas = []
                        }
                    }
                }, function (response) {
                });
            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, AplicaTarifario: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoProductosTransportados = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoProductosTransportados = response.data.Datos;
                            for (var j = 0; j < $scope.ListadoProductosTransportados.length; j++) {
                                $scope.ListadoProductosTransportados[j] = { Codigo: $scope.ListadoProductosTransportados[j].Codigo, Nombre: $scope.ListadoProductosTransportados[j].Nombre }
                            }
                            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                var tarifa = $scope.Modelo.Tarifas[i]
                                for (var j = 0; j < $scope.ListadoProductosTransportados.length; j++) {
                                    var Linea = JSON.parse(JSON.stringify($scope.ListadoProductosTransportados[j]))
                                    try {
                                        if (tarifa.TipoTarifaTransportes.Codigo == Linea.Codigo && (tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 301 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 300 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 6 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 302 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 5)) {
                                            Linea.TarifaTransporte = tarifa.TipoTarifaTransportes.TarifaTransporte
                                            tarifa.TipoTarifaTransportes = Linea
                                        }
                                    } catch (e) {

                                    }

                                }
                            }
                            try { $scope.ListadoTarifas = $scope.Modelo.Tarifas} catch (e) { }
                        }
                        else {
                            $scope.ListadoProductosTransportados = []
                        }
                    }
                }, function (response) {
                });
            
        }


        $scope.CargarTipoLinea = function (Codigo) {
            $scope.ListadoTipoLineaNegocioTransportesFiltrado = []
            $scope.ListadoTipoTarifaTransportesFiltrado = []
            $scope.ListadoTarifaTransportesFiltrado = []
            $scope.ListadoTipoLineaNegocioTransportesFiltrado.push({ Nombre: '(NO APLICA)' });
            for (var i = 0; i < $scope.ListadoTipoLineaNegocioTransportes.length; i++) {
                var item = $scope.ListadoTipoLineaNegocioTransportes[i]
                if (item.LineaNegocioTransporte.Codigo == Codigo) {
                    $scope.ListadoTipoLineaNegocioTransportesFiltrado.push(item)
                }
            }
            $scope.Modal.TipoLineaNegocioTransportes = $scope.ListadoTipoLineaNegocioTransportesFiltrado[0]
            $scope.HabilitarLineaNegocio = false
            $scope.HabilitarTipoLineaNegocio = true
            $scope.HabilitarTipoTarifa = true
            $scope.HabilitarRuta = true
            $scope.HabilitarFormaPago = false

        }
        $scope.CargarTipoLineaFiltrosGeneral = function (Codigo) {
            if ($scope.LineaNegocio != undefined && $scope.LineaNegocio != null && $scope.LineaNegocio != '') {
                $scope.HabilitarBotonesFiltros = false;
            }

            $scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral = []
            $scope.ListadoTipoTarifaTransportesFiltrado = []
            $scope.ListadoTarifaTransportesFiltrado = []
            $scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral.push({ Nombre: '(NO APLICA)' });
            for (var i = 0; i < $scope.ListadoTipoLineaNegocioTransportes.length; i++) {
                var item = $scope.ListadoTipoLineaNegocioTransportes[i]
                if (item.LineaNegocioTransporte.Codigo == Codigo) {
                    $scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral.push(item)
                }
            }
            $scope.TipoLineaNegocio = $scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral[0]
            $scope.HabilitarLineaNegocioFiltrosGeneral = false
            $scope.HabilitarRutaFiltrosGeneral = true
            $scope.HabilitarFormaPagoFiltrosGeneral = false

        }
        $scope.CargarTarifa = function (Codigo) {
            $scope.ListadoTarifaTransportesFiltrado = []
            $scope.ListadoTarifaTransportesFiltrado.push({ Nombre: '(NO APLICA)' });
            for (var i = 0; i < $scope.ListadoTarifaTransportes.length; i++) {
                var item = $scope.ListadoTarifaTransportes[i]
                if (item.TipoLineaNegocioTransporte.Codigo == Codigo) {
                    $scope.ListadoTarifaTransportesFiltrado.push(item)
                }
            }
            $scope.Modal.TarifaTransportes = $scope.ListadoTarifaTransportesFiltrado[0]
            $scope.HabilitarLineaNegocio = false
            $scope.HabilitarTipoLineaNegocio = false
            $scope.HabilitarTipoTarifa = true
        }
        $scope.CargarTarifaFiltrosGeneral = function (Codigo) {
            $scope.ListadoTarifaTransportesFiltrado = []
            $scope.ListadoTarifaTransportesFiltrado.push({ Nombre: '(NO APLICA)' });
            for (var i = 0; i < $scope.ListadoTarifaTransportes.length; i++) {
                var item = $scope.ListadoTarifaTransportes[i]
                if (item.TipoLineaNegocioTransporte.Codigo == Codigo) {
                    $scope.ListadoTarifaTransportesFiltrado.push(item)
                }
            }
            $scope.Modal.TarifaTransportes = $scope.ListadoTarifaTransportesFiltrado[0]
            $scope.HabilitarLineaNegocioFiltrosGeneral = false
            //$scope.HabilitarTipoLineaNegocioFiltrosGeneral = false
            //$scope.HabilitarTipoTarifa = true
        }
        $scope.CargarTipoTarifa = function (Codigo) {
            $scope.ListadoTipoTarifaTransportesFiltrado = []
            if (Codigo == 300 || Codigo == 6 || Codigo == 302 || Codigo == 5) { //BARRIL GALON TONELADA PRODUCTO
                $scope.ListadoTipoTarifaTransportesFiltrado = $scope.ListadoProductosTransportados
            }
            else {
                for (var i = 0; i < $scope.ListadoTipoTarifaTransportes.length; i++) {
                    var item = $scope.ListadoTipoTarifaTransportes[i]
                    if (item.TarifaTransporte.Codigo == Codigo) {
                        $scope.ListadoTipoTarifaTransportesFiltrado.push(item)
                    }
                } if ($scope.ListadoTipoTarifaTransportesFiltrado.length == 0) {
                    $scope.ListadoTipoTarifaTransportesFiltrado.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                }
            }
            $scope.Modal.TipoTarifaTransportes = $scope.ListadoTipoTarifaTransportesFiltrado[0]
            $scope.HabilitarTipoTarifa = false
        }
        $scope.CargarTipoTarifaFiltrosGeneral = function (Codigo) {
            $scope.ListadoTipoTarifaTransportesFiltrado = []
            if (Codigo == 300 || Codigo == 6 || Codigo == 302 || Codigo == 5) { //BARRIL GALON TONELADA PRODUCTO
                $scope.ListadoTipoTarifaTransportesFiltrado = $scope.ListadoProductosTransportados
            }
            else {
                for (var i = 0; i < $scope.ListadoTipoTarifaTransportes.length; i++) {
                    var item = $scope.ListadoTipoTarifaTransportes[i]
                    if (item.TarifaTransporte.Codigo == Codigo) {
                        $scope.ListadoTipoTarifaTransportesFiltrado.push(item)
                    }
                } if ($scope.ListadoTipoTarifaTransportesFiltrado.length == 0) {
                    $scope.ListadoTipoTarifaTransportesFiltrado.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                }
            }
            $scope.Modal.TipoTarifaTransportes = $scope.ListadoTipoTarifaTransportesFiltrado[0]
            //$scope.HabilitarTipoTarifa = false
        }

        $scope.FiltrarRutas = function () {
            $scope.ListadoRutasFiltrado = []
            var cteTipoRuta
            if ($scope.Modal.TipoLineaNegocioTransportes.Nombre.toUpperCase() == 'Urbana'.toUpperCase() || $scope.Modal.TipoLineaNegocioTransportes.Nombre.toUpperCase() == 'Urbano'.toUpperCase()) {
                cteTipoRuta = 4402
            } else if ($scope.Modal.TipoLineaNegocioTransportes.Nombre.toUpperCase() != '(NO APLICA)'.toUpperCase()) {
                cteTipoRuta = 4401
            }
            for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                var ruta = $scope.ListadoRutas[i]
                if (ruta.TipoRuta.Codigo == cteTipoRuta) {
                    $scope.ListadoRutasFiltrado.push(ruta)
                }
            }

        }
        $scope.FiltrarRutasFiltrosGeneral = function () {
            $scope.ListadoRutasFiltrosGeneral = []
            var cteTipoRuta
            if ($scope.TipoLineaNegocio.Nombre.toUpperCase() == 'Urbana'.toUpperCase() || $scope.TipoLineaNegocio.Nombre.toUpperCase() == 'Urbano'.toUpperCase()) {
                cteTipoRuta = 4402
            } else if ($scope.TipoLineaNegocio.Nombre.toUpperCase() != '(NO APLICA)'.toUpperCase()) {
                cteTipoRuta = 4401
            }
            for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                var ruta = $scope.ListadoRutas[i]
                if (ruta.TipoRuta.Codigo == cteTipoRuta) {
                    $scope.ListadoRutasFiltrosGeneral.push(ruta)
                }
            }
            if ($scope.TipoLineaNegocio.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL) {
                //$scope.HabilitarCiudadOrigenDestino = true
                //$scope.HabilitarCiudadUrbana = false
            }
            else if ($scope.TipoLineaNegocio.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA) {
                //$scope.HabilitarCiudadOrigenDestino = false
                //$scope.HabilitarCiudadUrbana = true
            }
            else {
                //$scope.HabilitarCiudadOrigenDestino = false
                //$scope.HabilitarCiudadUrbana = false
            }
        }

        $scope.LimpiarFiltrosGeneral = function () {
            $scope.LineaNegocio = '';
            $scope.TipoLineaNegocio = '';
            $scope.CiudadOrigen = '';
            $scope.CiudadDestino = '';
            $scope.Tarifa = '';
            $scope.TipoTarifa = '';
            $scope.FormaPago = '';
            $scope.FiltroRuta = '';
            $scope.HabilitarBotonesFiltros = false;
            $scope.FiltrarTarifarioPaqueteria();
            $scope.verData = false
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando tarifario Compras código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando tarifario Compras Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            TarifarioComprasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo = response.data.Datos
                        $scope.NumeroTarifario = response.data.Datos.Codigo
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.FechaInicio = new Date(response.data.Datos.FechaInicio)
                        $scope.Modelo.FechaFin = new Date(response.data.Datos.FechaFin)
                        $scope.DeshabilitarCampos = true
                        for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                            var tarifa = $scope.Modelo.Tarifas[i]
                            if (new Date(tarifa.FechaInicioVigencia) < MIN_DATE) {
                                tarifa.FechaInicioVigencia = ''
                            }
                            if (new Date(tarifa.FechaFinVigencia) < MIN_DATE) {
                                tarifa.FechaFinVigencia = ''
                            }
                            tarifa.ValorFlete = $scope.MaskValoresGrid(tarifa.ValorFlete)
                            tarifa.ValorEscolta = $scope.MaskValoresGrid(tarifa.ValorEscolta)
                        }
                        if ($scope.Modelo.TarifarioBase == 1) {
                            $scope.habilitarTarifarioBase = false
                            $scope.Modelo.TarifarioBase = true
                        }
                        $scope.CargarDatosFunciones()
                        if ($scope.aplicabase) {
                            $scope.Modelo.Codigo = 0
                            $scope.DeshabilitarCampos = false
                            $scope.Modelo.Nombre = ''
                            $scope.Modelo.Estado = $scope.ListadoEstados[1]
                            $scope.Modelo.TarifarioBase = false
                            ShowSuccess('El tarifario se copio correctamente')
                        }
                    }
                    else {
                        ShowError('No se logro consultar el tarifario Compras código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarTarifarioCompras';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarTarifarioCompras';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                if ($scope.Modelo.TarifarioBase) {
                    $scope.Modelo.TarifarioBase = 1
                } else {
                    $scope.Modelo.TarifarioBase = 0
                }

               var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Modelo.Codigo,
                    TarifarioBase: $scope.Modelo.TarifarioBase,
                    FechaInicio: $scope.Modelo.FechaInicio,
                    FechaFin: $scope.Modelo.FechaFin,
                    Estado: $scope.Modelo.Estado,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }                    
                };

                filtros.Tarifas = []

                $scope.ListadoTarifas.forEach(item => {
                    if (item.Ediatado == true) {
                        filtros.Tarifas.push(item)
                    }
                });

                TarifarioComprasFactory.Guardar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el tarifario Compras "' + $scope.Modelo.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se modificó el tarifario Compras "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = '#!ConsultarTarifarioCompras/' + $scope.Modelo.Codigo;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var modelo = $scope.Modelo

            if (ValidarCampo(modelo.Nombre) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar el nombre ');
                continuar = false;
            }
            if (ValidarFecha(modelo.FechaInicio) == FECHA_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar la fecha de inicio ');
                continuar = false;
            }
            if (ValidarFecha(modelo.FechaFin) == FECHA_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar la fecha fin ');
                continuar = false;
            } else if (ValidarFecha(modelo.FechaFin, false, true) == FECHA_ES_MENOR) {
                $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha actual');
                continuar = false;
            }
            if (ValidarFecha(modelo.FechaInicio) == FECHA_VALIDA && ValidarFecha(modelo.FechaFin, false, true) == FECHA_VALIDA) {
                if (modelo.FechaInicio > modelo.FechaFin) {
                    $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha de inicio');
                    continuar = false;
                }
            }
            if ($scope.Modelo.Tarifas.length == 0) {
                $scope.MensajesError.push('Debe ingresar al menos una tarifa');
                continuar = false;
            }
            return continuar;
        }
        $scope.NuevaTarifa = function () {
            $scope.ListadoLineaNegocioTransportesFiltrado = $scope.ListadoLineaNegocioTransportes
            $scope.Modal.LineaNegocioTransportes = $scope.ListadoLineaNegocioTransportesFiltrado[0]
            $('#Secciontarifa').show()
            $('#Seccionpaqueteria').hide()
            $scope.MensajesErrorModal = [];
            $scope.Modal.LineaNegocioTransportes = $scope.ListadoLineaNegocioTransportes[0]
            $scope.Modal.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
            $scope.Modal.Ruta = ''
            $scope.Modal.ValorFlete = ''
            $scope.Modal.ValorEscolta = ''
            $scope.Modal.ValorKgAdicional = ''
            $scope.Modal.ValorAdicional1 = ''
            $scope.Modal.ValorAdicional2 = ''
            $scope.Modal.FechaFinVigencia = $scope.Modelo.FechaFin
            $scope.Modal.FechaInicioVigencia = $scope.Modelo.FechaInicio
            $scope.CargarTipoLinea($scope.Modal.LineaNegocioTransportes.Codigo)
            $('#LineaNegocio').hide()
            $('#TipoTarifa').hide()
            showModal('ModalNuevaTarifa')
        }


        function DatosRequeridosNuevaTarifa() {
            $scope.MensajesErrorModal = [];
            window.scrollTo(top, top);
            var modelo = $scope.Modal
            var continuar = true;
            if (ValidarCampo(modelo.LineaNegocioTransportes, undefined, true) > 0) {
                $scope.MensajesErrorModal.push('Debe ingresar la linea de transporte');
                continuar = false;
            }
            if (ValidarCampo(modelo.TipoLineaNegocioTransportes, undefined, true) > 0) {
                $scope.MensajesErrorModal.push('Debe ingresar el tipo de linea de transporte');
                continuar = false;
            }
            if (ValidarCampo(modelo.TarifaTransportes, undefined, true) > 0) {
                $scope.MensajesErrorModal.push('Debe ingresar la tarifa de transporte');
                continuar = false;
            }
            if (ValidarCampo(modelo.TipoTarifaTransportes, undefined, true) > 0 && modelo.TarifaTransportes.Codigo !== 5 && modelo.TarifaTransportes.Codigo !== 301 && modelo.TarifaTransportes.Codigo !== 303) {
                $scope.MensajesErrorModal.push('Debe ingresar el tipo de tarifa de transporte');
                continuar = false;
            }
            if (ValidarCampo(modelo.Ruta, undefined, true) == 1) {
                $scope.MensajesErrorModal.push('Debe ingresar la ruta');
                continuar = false;
            }
            if (ValidarCampo(modelo.ValorFlete) == 1) {
                $scope.MensajesErrorModal.push('Debe ingresar el flete');
                continuar = false;
            }
            if (ValidarFecha(modelo.FechaFin, false, true) == FECHA_ES_MENOR) {
                $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha actual');
                continuar = false;
            }
            if (ValidarFecha(modelo.FechaInicio) == FECHA_VALIDA && ValidarFecha(modelo.FechaFin, false, true) == FECHA_VALIDA) {
                if (modelo.FechaInicio > modelo.FechaFin) {
                    $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha de inicio');
                    continuar = false;
                }
            }
            return continuar
        }
        $scope.Modelo.Tarifas = []
        $scope.ListadoTarifas = []
        $scope.AgregarTarifa = function () {
            if (DatosRequeridosNuevaTarifa()) {
                var cont = 0
                for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                    var item = $scope.Modelo.Tarifas[i]
                    if (
                        item.LineaNegocioTransportes.Codigo == $scope.Modal.LineaNegocioTransportes.Codigo
                        && item.TipoLineaNegocioTransportes.Codigo == $scope.Modal.TipoLineaNegocioTransportes.Codigo
                        && item.TarifaTransportes.Codigo == $scope.Modal.TarifaTransportes.Codigo
                        && item.TipoTarifaTransportes.Codigo == $scope.Modal.TipoTarifaTransportes.Codigo
                        && item.Ruta.Codigo == $scope.Modal.Ruta.Codigo
                    ) {
                        cont++
                    }
                    item.Mostrar = true;
                    $scope.Modal.Mostrar = true;
                }
                if (cont > 0) {
                    ShowError('Ya hay una tarifa ingresada con las opciones seleccionadas')
                } else {
                    $scope.Modal.TipoTarifaTransportes.TarifaTransporte = { Codigo: $scope.Modal.TarifaTransportes.Codigo }
                    $scope.Modal.Ediatado = true
                    $scope.Modelo.Tarifas.push(JSON.parse(JSON.stringify($scope.Modal)))
                    if ($scope.ListadoTarifas.length == 0) {
                        $scope.ListadoTarifas = $scope.Modelo.Tarifas;
                    } else {
                        var Inserto = false
                        //for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                        //    if ($scope.ListadoTarifas[i].LineaNegocioTransportes.Codigo == $scope.Modal.LineaNegocioTransportes.Codigo) {
                        //        if ($scope.ListadoTarifas[i].TipoLineaNegocioTransportes.Codigo == $scope.Modal.TipoLineaNegocioTransportes.Codigo) {
                        //            $scope.ListadoTarifas.push(JSON.parse(JSON.stringify($scope.Modal)))
                        //            Inserto = true
                        //            break;
                        //        }
                        //    }
                        //}
                        if (!Inserto) {
                            $scope.ListadoTarifas = $scope.Modelo.Tarifas;
                        }
                    }
                    closeModal('ModalNuevaTarifa')
                }
            }
        }
        $scope.ListTarifas = function (item) {
            if (item.lstTipoLineaNegocioTransportes == true) {
                item.lstTipoLineaNegocioTransportes = false
            } else {
                item.lstTipoLineaNegocioTransportes = true
            }
        }
        $scope.ListTipoTarifas = function (item) {
            if (item.lstData == true) {
                item.lstData = false
            } else {
                item.lstData = true
            }
        }
        $scope.EliminarTarifa = function (item, index, lista) {
            showModal('modalConfirmacionEliminar')
            $scope.tempItemElimina = item
            $scope.tempItemindex = index
            $scope.lista = lista
        }
        $scope.ConfirmarEliminarTarifa = function () {
            closeModal('modalConfirmacionEliminar')
            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                var item = $scope.Modelo.Tarifas[i]
                if (
                    item.LineaNegocioTransportes.Codigo == $scope.tempItemElimina.LineaNegocioTransportes.Codigo
                    && item.TipoLineaNegocioTransportes.Codigo == $scope.tempItemElimina.TipoLineaNegocioTransportes.Codigo
                    && item.TarifaTransportes.Codigo == $scope.tempItemElimina.TarifaTransportes.Codigo
                    && item.TipoTarifaTransportes.Codigo == $scope.tempItemElimina.TipoTarifaTransportes.Codigo
                    && item.Ruta.Codigo == $scope.tempItemElimina.Ruta.Codigo
                ) {
                    $scope.Modelo.Tarifas.splice(i, 1)
                    $scope.lista.splice($scope.tempItemindex, 1)
                    break;
                }
            }
        }
        $scope.AsignarValor = function (item2) {
            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                var item = $scope.Modelo.Tarifas[i]
                if (
                    item.LineaNegocioTransportes.Codigo == item2.LineaNegocioTransportes.Codigo
                    && item.TipoLineaNegocioTransportes.Codigo == item2.TipoLineaNegocioTransportes.Codigo
                    && item.TarifaTransportes.Codigo == item2.TarifaTransportes.Codigo
                    && item.TipoTarifaTransportes.Codigo == item2.TipoTarifaTransportes.Codigo
                    && item.Ruta.Codigo == item2.Ruta.Codigo
                ) {
                    $scope.Modelo.Tarifas[i].ValorFlete = item2.ValorFlete
                    $scope.Modelo.Tarifas[i].PorcentajeAfiliado = item2.PorcentajeAfiliado
                    $scope.Modelo.Tarifas[i].ValorEscolta = item2.ValorEscolta
                    $scope.Modelo.Tarifas[i].ValorKgAdicional = item2.ValorKgAdicional
                    $scope.Modelo.Tarifas[i].ValorAdicional1 = item2.ValorAdicional1
                    $scope.Modelo.Tarifas[i].ValorAdicional2 = item2.ValorAdicional2
                    $scope.Modelo.Tarifas[i].FechaInicioVigencia = item2.FechaInicioVigencia
                    $scope.Modelo.Tarifas[i].FechaFinVigencia = item2.FechaFinVigencia
                    $scope.Modelo.Tarifas[i].Estado = item2.Estado
                    $scope.Modelo.Tarifas[i].Ediatado = true

                }
            }
            try { $scope.ListadoTarifas = $scope.Modelo.Tarifas } catch (e) { }
        }
        $scope.ValidarFlete = function (item) {
            if (item.ValorFlete == '' || item.ValorFlete == 0 || item.ValorFlete == undefined) {
                return item.temValorFlete
            } else {
                return item.ValorFlete
            }
        }

        $scope.VerificarExistencia = function () {
            blockUI.start('Cargando tarifario Compras código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando tarifario Compras Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Nombre: $scope.Modelo.Nombre,
            };

            blockUI.delay = 1000;
            TarifarioComprasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            ShowError('Ya hay un tarifario guardado con el nombre ingresado')
                            $scope.Modelo.Nombre = ''
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            blockUI.stop();
        };


        $scope.LineaNegocio = '';
        $scope.TipoLineaNegocio = '';
        $scope.CiudadOrigen = '';
        $scope.CiudadDestino = '';
        $scope.Tarifa = '';
        $scope.TipoTarifa = '';
        $scope.FormaPago = '';
        $scope.FiltroRuta = '';
        $scope.ListadoTarifas.forEach(item => {
                    item.Mostrar = true;
        });
        $scope.FiltrarTarifarioPaqueteria = function () {
            $scope.verData = true
            $scope.ListadoTarifas.forEach(item => {
                if ($scope.LineaNegocio != undefined && $scope.LineaNegocio != null && $scope.LineaNegocio != '') {
                   
                    if ($scope.LineaNegocio.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO) {
                        if (item.LineaNegocioTransportes.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO) {
                            item.Mostrar = true;
                            if ($scope.FiltroRuta != undefined && $scope.FiltroRuta != null && $scope.FiltroRuta != '') {
                                if (item.Ruta.Codigo == $scope.FiltroRuta.Codigo) {
                                    //item.Mostrar = true;
                                } else {
                                    item.Mostrar = false;
                                }
                            }

                            if (item.TipoLineaNegocioTransportes.Codigo == $scope.TipoLineaNegocio.Codigo) {

                            } else {
                            item.Mostrar = false;
                            }

                            if ($scope.Tarifa != undefined && $scope.Tarifa != null && $scope.Tarifa != '') {
                                if (item.TarifaTransportes.Codigo == $scope.Tarifa.Codigo) {
                                    //item.Mostrar = true;
                                } else {
                                    item.Mostrar = false;
                                }
                            }
                            if ($scope.TipoTarifa != undefined && $scope.TipoTarifa != null && $scope.TipoTarifa != '') {
                                if (item.TipoTarifaTransportes.Codigo == $scope.TipoTarifa.Codigo) {
                                    //item.Mostrar = true;
                                } else {
                                    item.Mostrar = false;
                                }
                            }

                        } else {
                            $scope.ListadoTarifas.forEach(item => {
                                item.Mostrar = false;
                            });
                        }
                    } else {
                        //lineaNegocio.Data.forEach(tipolineanegocio => {
                        //    tipolineanegocio.Data.forEach(item => {
                        //        item.Mostrar = true;
                        //    });
                        //});
                    }


                    //if ($scope.LineaNegocio.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                    //    if (lineaNegocio.LineaNegocioTransportes.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                    //        lineaNegocio.Data.forEach(tipolineaNegocio => {
                    //            tipolineaNegocio.Data.forEach(item => {
                    //                item.Mostrar = true;
                    //                if ($scope.FiltroRuta != undefined && $scope.FiltroRuta != null && $scope.FiltroRuta != '') {
                    //                    if (item.Ruta.Codigo == $scope.FiltroRuta.Codigo) {
                    //                        //item.Mostrar = true;
                    //                    } else {
                    //                        item.Mostrar = false;
                    //                    }
                    //                }

                    //                if ($scope.Tarifa != undefined && $scope.Tarifa != null && $scope.Tarifa != '') {
                    //                    if (item.TarifaTransportes.Codigo == $scope.Tarifa.Codigo) {
                    //                        //item.Mostrar = true;
                    //                    } else {
                    //                        item.Mostrar = false;
                    //                    }
                    //                }
                    //                if ($scope.TipoTarifa != undefined && $scope.TipoTarifa != null && $scope.TipoTarifa != '') {
                    //                    if (item.TipoTarifaTransportes.Codigo == $scope.TipoTarifa.Codigo) {
                    //                        // item.Mostrar = true;
                    //                    } else {
                    //                        item.Mostrar = false;
                    //                    }
                    //                }

                    //            });
                    //        });
                    //    } else {
                    //        lineaNegocio.Data.forEach(tipolineanegocio => {
                    //            tipolineanegocio.Data.forEach(item => {
                    //                item.Mostrar = false;
                    //            });
                    //        });
                    //    }
                    //} else {
                    //    //lineaNegocio.Data.forEach(tipolineanegocio => {
                    //    //    tipolineanegocio.Data.forEach(item => {
                    //    //        item.Mostrar = true;
                    //    //    });
                    //    //});
                    //}
                } else {
                    $scope.ListadoTarifas.forEach(item => {
                        item.Mostrar = true;
                    });
                }
            });
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarTarifarioCompras/'// + $scope.Modelo.Codigo;
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        try {
            var Parametros = $routeParams.Codigo.split(',')
            if (Parametros.length > 1) {
                $scope.Modelo.Codigo = Parametros[0];
                $scope.aplicabase = true
                Obtener();
            } else {
                if ($routeParams.Codigo > 0) {
                    $scope.Modelo.Codigo = $routeParams.Codigo;
                    Obtener();
                }
                else {
                    $scope.Modelo.Codigo = 0;
                    $scope.CargarDatosFunciones()
                }

            }
        } catch (e) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.Codigo = $routeParams.Codigo;
                Obtener();
            }
            else {
                $scope.Modelo.Codigo = 0;
                $scope.CargarDatosFunciones()
            }

        }

        $scope.ReplicarVigencias = function () {
            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                $scope.Modelo.Tarifas[i].FechaInicioVigencia = $scope.Modelo.FechaInicio
                $scope.Modelo.Tarifas[i].FechaFinVigencia = $scope.Modelo.FechaFin
            }

            $scope.ListadoTarifas = $scope.Modelo.Tarifas;
            closeModal('ModalReplicarVIgencia')

        }
        $scope.MostrarReplicar = function () {
            if ($scope.ListadoTarifas.length > 0) {
                showModal('ModalReplicarVIgencia')
            }
        }
        $scope.Date = function (d) {
            return new Date(d)
        }
        $scope.CopyItem = function (d) {
            var Objet = JSON.parse(JSON.stringify(new Object(d)))
            return Objet

        }
    }]);