﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Almacen
Imports EncoExpres.GesCarga.Negocio.Almacen

<Authorize>
Public Class DetalleDocumentoAlmacenesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleDocumentoAlmacenes) As Respuesta(Of IEnumerable(Of DetalleDocumentoAlmacenes))
        Return New LogicaDetalleDocumentoAlmacenes(CapaPersistenciaDetalleDocumentoAlmacenes).Consultar(filtro)
    End Function

End Class
