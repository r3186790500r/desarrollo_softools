﻿PRINT 'gsp_insertar_encabezado_precinto'
GO
DROP PROCEDURE gsp_insertar_encabezado_precinto
GO
CREATE PROCEDURE gsp_insertar_encabezado_precinto 
(        
@par_Empr_Codigo Numeric ,        
@par_Tipo_Asignacion Numeric ,        
@par_Tipo_Precinto Numeric,         
@par_Fecha_Entrega Datetime,        
@par_Numero_Incial Numeric,         
@par_Numero_Final Numeric,         
@par_Usua_Crea Numeric,        
@par_Oficina Numeric,         
@par_Oficina_Destino Numeric,         
@par_Terc_Responsable Numeric         
)        
AS BEGIN        
      
DECLARE @NumExiste numeric = 0      
SELECT @NumExiste =  COUNT(*) FROM Detalle_Asignacion_Precintos_Oficina WHERE EMPR_Codigo = @par_Empr_Codigo      
AND Numero_Precinto >= @par_Numero_Incial       
AND Numero_Precinto <= @par_Numero_Final       
      
IF @NumExiste > 0      
  BEGIN      
       SELECT 0 AS Numero        
  END       
ELSE      
  BEGIN       
   INSERT INTO Encabezado_Asignacion_Precintos_Oficina         
      --1        
      (EMPR_Codigo,        
      CATA_TAPR_Codigo,        
      OFIC_Codigo,      
      OFIC_Codigo_Destino,        
      --5        
      CATA_TPRE_Codigo,        
      Fecha_Entrega,        
      TERC_Codigo_Responsable,        
      Numero_Precinto_Inicial,        
      Numero_Precinto_Final,        
      --10        
      anulado,        
      USUA_Codigo_Crea,        
      Fecha_Crea)        
      VALUES (        
      --1        
      @par_Empr_Codigo,        
      @par_Tipo_Asignacion,        
      @par_Oficina,        
      @par_Oficina_Destino,        
      --5        
      @par_Tipo_Precinto,        
      @par_Fecha_Entrega,        
      @par_Terc_Responsable,        
      @par_Numero_Incial,        
      @par_Numero_Final,        
      --10        
      0,        
      @par_Usua_Crea,        
      CONVERT(DATE , GETDATE())    )      
      
   select @@IDENTITY as Numero        
         
   END       
END      
GO
