﻿PRINT 'gsp_consultar_lista_rndc_empresa'
GO
DROP PROCEDURE gsp_consultar_lista_rndc_empresa
GO   
CREATE PROCEDURE gsp_consultar_lista_rndc_empresa 
(@par_EMPR_Codigo SMALLINT)    
    
AS    
BEGIN    
SELECT      
EMPR_Codigo AS Codigo_Empresa    
,ISNULL(Codigo_Regional_Ministerio, '') AS Codigo_Regional_Ministerio     
,ISNULL(Codigo_Empresa_Ministerio, 0) AS Codigo_Empresa_Ministerio    
,ISNULL(Usuario_Manifiesto_Electronico, '') AS Usuario_Manifiesto_Electronico    
,ISNULL(Numero_Resolucion_Habilitacion_Empresa, 0) AS Numero_Resolucion_Habilitacion_Empresa    
,ISNULL(Clave_Manifiesto_Electronico, '') AS Clave_Manifiesto_Electronico    
    
FROM Empresa_Manifiesto_Electronico 
    
WHERE     
EMPR_Codigo = @par_EMPR_Codigo       
    
END    
GO    