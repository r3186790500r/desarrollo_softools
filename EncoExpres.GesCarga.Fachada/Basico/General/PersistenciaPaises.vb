﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaPaises
        Implements IPersistenciaBase(Of Paises)

        Public Function Consultar(filtro As Paises) As IEnumerable(Of Paises) Implements IPersistenciaBase(Of Paises).Consultar
            Return New RepositorioPaises().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Paises) As Long Implements IPersistenciaBase(Of Paises).Insertar
            Return New RepositorioPaises().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Paises) As Long Implements IPersistenciaBase(Of Paises).Modificar
            Return New RepositorioPaises().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Paises) As Paises Implements IPersistenciaBase(Of Paises).Obtener
            Return New RepositorioPaises().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Paises) As Boolean
            Return New RepositorioPaises().Anular(entidad)
        End Function
    End Class

End Namespace

