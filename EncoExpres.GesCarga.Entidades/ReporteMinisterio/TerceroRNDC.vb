﻿'Imports Microsoft.VisualBasic
'Imports System.Xml
'Imports System.Data
'Imports System.Data.SqlClient
'Imports System.Collections.Generic
'Imports EncoExpres.GesCarga.Entidades.Basico.General
'Imports EncoExpres.GesCarga.Entidades.Entidades.ReporteMinisterio.General

'Namespace Entidades.ReporteMinisterio
'    Public Class TerceroRNDC
'#Region "Propiedades"

'        Public NUMNITEMPRESATRANSPORTE As String
'        Public CODTIPOIDTERCERO As String
'        Public NUMIDTERCERO As String
'        Public NOMIDTERCERO As String
'        Public PRIMERAPELLIDOIDTERCERO As String
'        Public SEGUNDOAPELLIDOIDTERCERO As String
'        Public CODSEDETERCERO As String
'        Public NOMSEDETERCERO As String
'        Public NUMTELEFONOCONTACTO As String
'        Public NUMCELULARPERSONA As String
'        Public NOMENCLATURADIRECCION As String
'        Public CODMUNICIPIORNDC As String
'        Public CODCATEGORIALICENCIACONDUCCION As String
'        Public NUMLICENCIACONDUCCION As String
'        Public FECHAVENCIMIENTOLICENCIA As String
'        Public Codigo As Long

'        Dim strSQL As String
'        Dim solicitud As Solicitud
'        Dim objGeneral As General
'        Dim acceso As Acceso
'        Dim wsManifiestoElectronico As ManifiestoElectronico.IBPMServicesservice
'        Dim strRespuesta As String
'        'Dim objInterfaz As InterfazMinisterioTransporte

'        Public Property EnlaceMinisterio As String
'            Get
'                Return Me.wsManifiestoElectronico.Url
'            End Get
'            Set(value As String)
'                Me.wsManifiestoElectronico.Url = value
'            End Set
'        End Property

'#End Region

'#Region "Constantes"
'        Public Const NOMBRE_TAG As String = "variables"
'        Public Const NOMBRE_TAG_CONSULTA As String = "documento"
'        Public Const TIPO_IDENTIFICACION_NIT As String = "N"
'        Public Const TIPO_IDENTIFICACION_CEDULA As String = "C"
'        Public Const CODIGO_SEDE_DEFECTO As String = "0"
'        Public Const APELLIDO_X_DEFECTO As String = "APELLIDO"
'        Public Const DIRECCION_X_DEFECTO As String = "(NO APLICA)"
'        Public Const TELEFONO_X_DEFECTO As String = "1234567"
'        Public Const DIGITOS_TELEFONO As Int16 = 7
'        Public Const ERROR_TERCERO_DUPLICADO As String = "Error TER015: El tercero y la sede que está registrando ya existe con todos los datos iguales. No debe volver a registrarlo"

'#End Region

'#Region "Constructor"
'        Public Sub New(ByVal acceso As Acceso, ByVal solicitud As Solicitud)
'            Me.solicitud = New Solicitud
'            Me.acceso = New Acceso
'            objGeneral = New General

'            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
'            Me.solicitud = solicitud
'            Me.acceso = acceso
'            'Me.objInterfaz = New InterfazMinisterioTransporte()
'        End Sub

'        Sub New()
'            solicitud = New Solicitud
'            acceso = New Acceso
'            objGeneral = New General
'            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
'            'Me.objInterfaz = New InterfazMinisterioTransporte()
'        End Sub
'#End Region

'#Region "Funciones Privadas"

'#End Region

'#Region "Funciones Publicas"

'        Public Sub AgregarVariablesAObjetoTercero(ByRef entidadTercero As Terceros, Empresa As Empresas)
'            Try

'                Me.NUMNITEMPRESATRANSPORTE = Empresa.NumeroIdentificacion
'                Select Case entidadTercero.TipoIdentificacion.Codigo
'                    Case 100 'no Aplica
'                    Case 101 'Cedula
'                    Case 103 'cedula extrangera
'                        Me.CODTIPOIDTERCERO = TIPO_IDENTIFICACION_CEDULA
'                    Case 102 ' NIT
'                        Me.CODTIPOIDTERCERO = TIPO_IDENTIFICACION_NIT
'                    Case Else
'                        Me.CODTIPOIDTERCERO = TIPO_IDENTIFICACION_NIT
'                End Select

'                Me.NUMIDTERCERO = entidadTercero.NumeroIdentificacion
'                If entidadTercero.RazonSocial = String.Empty Then
'                    Me.NOMIDTERCERO = entidadTercero.Nombre
'                    Me.PRIMERAPELLIDOIDTERCERO = entidadTercero.PrimeroApellido
'                    Me.SEGUNDOAPELLIDOIDTERCERO = entidadTercero.SegundoApellido
'                Else
'                    Me.NOMIDTERCERO = entidadTercero.RazonSocial
'                    Me.PRIMERAPELLIDOIDTERCERO = entidadTercero.PrimeroApellido
'                    Me.SEGUNDOAPELLIDOIDTERCERO = entidadTercero.SegundoApellido
'                End If


'                Me.CODSEDETERCERO = entidadTercero.Ciudad.Codigo
'                Me.NOMSEDETERCERO = entidadTercero.Ciudad.Nombre
'                Me.NUMTELEFONOCONTACTO = entidadTercero.Telefonos
'                Me.NUMCELULARPERSONA = entidadTercero.Celular
'                Me.NOMENCLATURADIRECCION = entidadTercero.Direccion
'                Me.CODMUNICIPIORNDC = entidadTercero.Ciudad.Codigo
'                Me.EnlaceMinisterio = Empresa.ListadoRNDC.EnlaceWSMinisterio


'            Catch ex As Exception

'            End Try
'        End Sub

'        Public Sub Crear_XML_Tercero(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByVal lstVariables As List(Of String), ByVal Tipos_Rol As Tipos_Roles)
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)

'                Select Case TipoFormato
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                        Dim txtNodo As XmlText = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODTIPOIDTERCERO")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOIDTERCERO)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMIDTERCERO")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDTERCERO)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NOMIDTERCERO")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NOMIDTERCERO)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        If Me.PRIMERAPELLIDOIDTERCERO <> String.Empty And Me.CODTIPOIDTERCERO <> "N" Then
'                            NuevoElemento = XmlDocumento.CreateElement("PRIMERAPELLIDOIDTERCERO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.PRIMERAPELLIDOIDTERCERO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.SEGUNDOAPELLIDOIDTERCERO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("SEGUNDOAPELLIDOIDTERCERO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.SEGUNDOAPELLIDOIDTERCERO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        ' El teléfono debe ir solo sin espacios ni nada
'                        Me.NUMTELEFONOCONTACTO = Replace(Me.NUMTELEFONOCONTACTO, "-", "")
'                        Me.NUMTELEFONOCONTACTO = Replace(Me.NUMTELEFONOCONTACTO, " ", "")

'                        'Me.NUMTELEFONOCONTACTO = objInterfaz.Formatear_Campo(Me.NUMTELEFONOCONTACTO, 7, InterfazMinisterioTransporte.CAMPO_CEROS_IZQUIERDA)
'                        NuevoElemento = XmlDocumento.CreateElement("NUMTELEFONOCONTACTO")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMTELEFONOCONTACTO)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NOMENCLATURADIRECCION")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NOMENCLATURADIRECCION)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        If Len(Me.CODMUNICIPIORNDC) > 0 Then
'                            'Me.CODMUNICIPIORNDC = objInterfaz.Formatear_Campo(Me.CODMUNICIPIORNDC, 8, InterfazMinisterioTransporte.CAMPO_CARACTER_CEROS)
'                            NuevoElemento = XmlDocumento.CreateElement("CODMUNICIPIORNDC")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODMUNICIPIORNDC)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        'If Me.CODTIPOIDTERCERO = TIPO_IDENTIFICACION_NIT Then
'                        If Me.CODSEDETERCERO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODSEDETERCERO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODSEDETERCERO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NOMSEDETERCERO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NOMSEDETERCERO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NOMSEDETERCERO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If
'                        'Else
'                        '    NuevoElemento = XmlDocumento.CreateElement("NOMSEDETERCERO")
'                        '    txtNodo = XmlDocumento.CreateTextNode(CODIGO_SEDE_DEFECTO)
'                        '    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        '    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        'End If

'                        If Me.NUMLICENCIACONDUCCION <> String.Empty Then
'                            If Len(Me.NUMLICENCIACONDUCCION) > 15 Then
'                                Me.NUMLICENCIACONDUCCION = Mid(Me.NUMLICENCIACONDUCCION, Len(Me.NUMLICENCIACONDUCCION) - 15, 15)
'                            End If

'                            NuevoElemento = XmlDocumento.CreateElement("NUMLICENCIACONDUCCION")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMLICENCIACONDUCCION)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                            If Me.CODCATEGORIALICENCIACONDUCCION <> String.Empty Then
'                                NuevoElemento = XmlDocumento.CreateElement("CODCATEGORIALICENCIACONDUCCION")
'                                txtNodo = XmlDocumento.CreateTextNode(Me.CODCATEGORIALICENCIACONDUCCION)
'                                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                            End If

'                            If Me.FECHAVENCIMIENTOLICENCIA <> String.Empty Then
'                                NuevoElemento = XmlDocumento.CreateElement("FECHAVENCIMIENTOLICENCIA")
'                                txtNodo = XmlDocumento.CreateTextNode(Me.FECHAVENCIMIENTOLICENCIA)
'                                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                            End If

'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA
'                        Dim strAxuliarCampos As String = String.Empty
'                        For Each Items As String In lstVariables
'                            If strAxuliarCampos = String.Empty Then
'                                strAxuliarCampos = Items
'                            Else
'                                strAxuliarCampos &= "," & Items
'                            End If
'                        Next
'                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("NUMIDTERCERO" & strAxuliarCampos)
'                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
'                        xmlElementoRoot.AppendChild(NuevoElemento)
'                        xmlElementoRoot = XmlDocumento.DocumentElement

'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODTIPOIDTERCERO")
'                        txtNodo = XmlDocumento.CreateTextNode("'" & Me.CODTIPOIDTERCERO & "'")
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMIDTERCERO")
'                        txtNodo = XmlDocumento.CreateTextNode("'" & Me.NUMIDTERCERO & "'")
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        If Tipos_Rol <> Tipos_Roles.CONDUCTOR Then
'                            If Me.CODSEDETERCERO <> String.Empty Then
'                                NuevoElemento = XmlDocumento.CreateElement("CODSEDETERCERO")
'                                txtNodo = XmlDocumento.CreateTextNode(Me.CODSEDETERCERO)
'                                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                            End If
'                        End If

'                End Select
'            Catch ex As Exception
'            End Try
'        End Sub
'        Public Sub Crear_XML_Consulta_Tercero(ByRef XmlDocumento As XmlDocument)
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)

'                xmlElementoRoot = XmlDocumento.DocumentElement

'                Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("TERIDENTIFICACION")
'                xmlElementoRoot.LastChild.AppendChild(txtNodo)

'                xmlElementoRoot = XmlDocumento.DocumentElement
'                NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
'                xmlElementoRoot.AppendChild(NuevoElemento)

'                NuevoElemento = XmlDocumento.CreateElement("NUMIDTERCERO")
'                txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDTERCERO)
'                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'            Catch ex As Exception

'            End Try
'        End Sub
'        Public Function Reportar_Tercero(ByRef TextoXmlTercero As String, ByVal TipoFormato As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String), ByVal Tipos_Rol As Tipos_Roles) As Double
'            Try
'                Reportar_Tercero = General.CERO
'                strErrores = String.Empty

'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                Me.acceso.Crear_XML_Acceso(xmlDoc)
'                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_TERCERO)
'                Me.Crear_XML_Tercero(xmlDoc, TipoFormato, lstVariables, Tipos_Rol)
'                TextoXmlTercero = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Tercero = Obtener_Mensaje_XML(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, strErrores, lstVariables)

'            Catch ex As Exception
'                Reportar_Tercero = 0
'                strErrores = ex.Message
'            End Try
'        End Function
'        Public Function Obtener_Mensaje_XML(ByVal strMensaje As String, ByVal TipoFormato As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String)) As Double
'            Try
'                Obtener_Mensaje_XML = General.CERO
'                Dim xmlRespuesta As New XmlDocument
'                xmlRespuesta.LoadXml(strMensaje)
'                Dim lstxmlObjetos As New List(Of XmlElement)
'                For Each Item As XmlNode In xmlRespuesta.DocumentElement
'                    lstxmlObjetos.Add(Item)
'                Next
'                Select Case TipoFormato
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
'                            Obtener_Mensaje_XML = General.CERO
'                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
'                        End If
'                        If xmlRespuesta.GetElementsByTagName("ingresoid").Count > 0 Then
'                            Obtener_Mensaje_XML = Val(xmlRespuesta.GetElementsByTagName("ingresoid")(0).InnerText)
'                            strErrores = String.Empty
'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA
'                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
'                            Obtener_Mensaje_XML = General.CERO
'                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
'                        End If
'                        If xmlRespuesta.GetElementsByTagName("numidtercero").Count > 0 Then
'                            Obtener_Mensaje_XML = Val(xmlRespuesta.GetElementsByTagName("numidtercero")(0).InnerText)
'                            strErrores = String.Empty
'                            For i As Integer = 0 To lstVariables.Count - 1
'                                If xmlRespuesta.GetElementsByTagName(lstVariables(i).ToLower).Count > 0 Then
'                                    lstVariables(i) = xmlRespuesta.GetElementsByTagName(lstVariables(i).ToLower)(0).InnerText
'                                Else
'                                    lstVariables(i) = String.Empty
'                                End If
'                            Next
'                        End If
'                End Select
'            Catch ex As Exception
'                strErrores = ex.Message
'            End Try
'        End Function
'        Public Function Guardar_Tercero_Confirmacion(ByVal dblNumeroConfirmacion As Double, ByVal lonCodigoEmpresa As Long, ByVal strCodigoUsuario As String, ByRef strMensaje As String) As Boolean
'            Try
'                If lonCodigoEmpresa <> General.CERO Then
'                    Me.objGeneral.ComandoSQL = New SqlCommand
'                    Me.objGeneral.ComandoSQL.CommandText = "sp_tercero_reporte_ministerio"
'                    Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                    Me.objGeneral.ComandoSQL.Parameters.Clear()
'                    Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.CadenaDeConexionSQL)

'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Empr_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Empr_Codigo").Value = lonCodigoEmpresa
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Terc_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Terc_Codigo").Value = Codigo
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Confirmacion", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Confirmacion").Value = dblNumeroConfirmacion
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Usua_reporta", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Usua_reporta").Value = strCodigoUsuario
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Cantidad_Insertados", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Cantidad_Insertados").Direction = ParameterDirection.Output

'                    If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
'                        Me.objGeneral.ComandoSQL.Connection.Open()
'                    End If

'                    Me.objGeneral.ComandoSQL.ExecuteReader()

'                    If Val(Me.objGeneral.ComandoSQL.Parameters("@par_Cantidad_Insertados").Value) > General.CERO Then
'                        Guardar_Tercero_Confirmacion = True
'                    Else
'                        Guardar_Tercero_Confirmacion = False
'                    End If
'                Else
'                    Guardar_Tercero_Confirmacion = True
'                End If
'            Catch ex As Exception
'                strMensaje &= ex.Message
'                Guardar_Tercero_Confirmacion = False
'            End Try
'        End Function

'#End Region

'    End Class
'End Namespace
