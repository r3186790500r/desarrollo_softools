﻿EncoExpresApp.controller("ConsultarConceptosGastosCrtl", ['$scope', '$timeout', 'TarifarioVentasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'ConceptoGastosFactory', 'OficinasFactory', 'ImpuestosFactory',
    function ($scope, $timeout, TarifarioVentasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, ConceptoGastosFactory, OficinasFactory, ImpuestosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Flota Propia' }, { Nombre: 'Concepto Gastos' }, { Nombre: 'Consultar' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.MensajesErrorAnula = [];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListaRecorridosConsultados = [];
        $scope.ListaResultadoFiltroConsulta = [];
        $scope.ListadoRecorridosTotal = [];
        $scope.Codigo = 0;
        $scope.CodigoImpuesto = 0;
        $scope.ListadoImpuestos = [];

        $scope.NombreConcepto = '';
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,

        }
        $scope.ListadoConceptoLiquidacion = [];
        $scope.ListadoAuxiliar = [];
        $scope.NumeroDocumento = 0;
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 },


        ];


        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONCEPTOS_GASTOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarConceptosGastos';
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.ModeloCodigo = $routeParams.Codigo;

                Find();
            }
        }
       

        function Find() {
            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoConceptos = [];
            if ($scope.Buscando) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModeloCodigo,
                    CodigoAlterno: $scope.ModeloCodigoAlterno,
                    Pagina: $scope.paginaActual,
                    Nombre: $scope.ModeloNombre,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                    Estado: $scope.ModalEstado.Codigo,
                    AplicaConsultaMaster: 1,
                    ConceptoSistema : 1
                };
                if ($scope.MensajesError.length == 0) {
                    blockUI.delay = 1000;
                    ConceptoGastosFactory.Consultar(filtros).
                        then(function (response) {
                            console.log(response);
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) { 
                                        $scope.ListadoConceptos = response.data.Datos
                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalRegistros = $scope.totalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                        $scope.Buscando = true;
                                        $scope.ResultadoSinRegistros = '';
                                    }
                                    else {
                                        $scope.ListadoConceptoLiquidacion = "";
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
            $scope.ModeloCodigo = ''
            $scope.ModeloNombre = ''
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.Anular = function (codigo, CodigoAlterno) {
            $scope.Codigo = codigo
            $scope.NumeroDocumento = CodigoAlterno
            $scope.ModalErrorCompleto = ''
            $scope.CodigoAlterno = CodigoAlterno
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };




        $scope.ConfirmaAnular = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Observaciones: $scope.ModeloCausaAnula,
                Codigo: $scope.Codigo,
                AplicaImpuesto: ESTADO_INACTIVO
            };
            ConceptoGastosFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se Elimino el concepto: ' + $scope.NumeroDocumento);
                        closeModal('modalAnular');

                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                        showModal('modalMensajeConfirmacionAnularConcepto');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede Eliminar el concepto ' + $scope.CodigoAlterno + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });



        };

        $scope.CerrarModal = function () {
            closeModal('modalAnular');
            closeModal('modalMensajeConfirmacionAnularConcepto');
        }

    }]);