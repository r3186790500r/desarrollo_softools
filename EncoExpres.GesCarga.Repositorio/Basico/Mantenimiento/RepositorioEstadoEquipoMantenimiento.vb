﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Mantenimiento

Namespace Mantenimiento
    Public NotInheritable Class RepositorioEstadoEquipoMantenimiento
        Inherits RepositorioBase(Of EstadoEquipoMantenimiento)
        Public Overrides Function Consultar(filtro As EstadoEquipoMantenimiento) As IEnumerable(Of EstadoEquipoMantenimiento)
            Dim lista As New List(Of EstadoEquipoMantenimiento)
            Dim item As EstadoEquipoMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Nombre) And filtro.Nombre <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre.ToString)
                End If
                If Not IsNothing(filtro.Codigo) Then
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado.Codigo >= 0 Then

                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_Estado_Equipo_Mantenimientos]")

                While resultado.Read
                    item = New EstadoEquipoMantenimiento(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As EstadoEquipoMantenimiento) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_insertar_estado_equipo_mantenimiento")

                While resultado.Read
                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While
                resultado.Close()
            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As EstadoEquipoMantenimiento) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_modificar_estado_equipo_mantenimiento")

                While resultado.Read
                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While
                resultado.Close()
            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As EstadoEquipoMantenimiento) As EstadoEquipoMantenimiento
            Dim item As New EstadoEquipoMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_obtener_Estado_Equipo_Mantenimientos")

                While resultado.Read
                    item = New EstadoEquipoMantenimiento(resultado)
                End While
                resultado.Close()
            End Using

            Return item
        End Function
    End Class
End Namespace
