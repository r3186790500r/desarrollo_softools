CREATE PROCEDURE [dbo].[gsp_Consultar_Detalle_Factura_SIESA_PLEXA]    
(      
 @par_EMPR_Codigo smallint,      
 @par_ENFA_Numero Numeric      
)      
AS      
BEGIN      
	SELECT       
	ENFA.EMPR_Codigo,      
	ENFA.Numero,      
	ENRE.Numero ENRE_Numero,      
	ENRE.Numero_Documento,      
	ENRE.VEHI_Codigo,      
	VEHI.Placa,      
	ENRE.Total_Flete_Cliente AS TotalFlete,      
	ENRE.Cantidad_Cliente AS Cantidad,      
	ENRE.Peso_Cliente AS Peso,    
	ENRE.PRTR_Codigo,    
	PRTR.Nombre AS NombreProducto,    
	PRTR.UMPT_Codigo,    
	LIPT.Campo1 AS Tipo_Operacion, 
    LIPT.Campo2 AS Codigo_Centro_Operacion,
	UMPT.Nombre_Corto UMPT_NombreCorto,    
	UMPT.Descripcion UMPT_Descripcion,   
	
	ROW_NUMBER() OVER (ORDER BY ENRE.Numero DESC) AS NumeroRegistro      
      
	FROM Encabezado_Facturas ENFA      
      
	INNER JOIN Detalle_Remesas_Facturas AS DERF
	ON ENFA.EMPR_Codigo = DERF.EMPR_Codigo      
	AND ENFA.Numero = DERF.ENFA_Numero      
      
	LEFT JOIN Encabezado_Remesas AS ENRE
	ON DERF.EMPR_Codigo = ENRE.EMPR_Codigo      
	AND DERF.ENRE_Numero = ENRE.Numero      
      
	INNER JOIN Vehiculos AS VEHI
	ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo    
	AND ENRE.VEHI_Codigo = VEHI.Codigo    
    
	INNER JOIN Producto_Transportados AS PRTR
	ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo    
	AND ENRE.PRTR_Codigo = PRTR.Codigo    
    
	INNER JOIN Unidad_Medida_Producto_Transportados AS UMPT
	ON PRTR.EMPR_Codigo = UMPT.EMPR_Codigo    
	AND PRTR.UMPT_Codigo = UMPT.Codigo    
    	
	INNER JOIN Valor_Catalogos AS LIPT --Linea Producto (PGR,GLP,SECA,etc)
	ON PRTR.EMPR_Codigo = LIPT.EMPR_Codigo
	AND PRTR.CATA_LIPT_Codigo = LIPT.Codigo

	WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo       
	AND ENFA.Numero = @par_ENFA_Numero      
END    
GO


