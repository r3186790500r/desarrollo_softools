﻿Imports EncoExpres.GesCarga.Entidades

Public MustInherit Class LogicaBase(Of T)

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="filtro"></param>
    ''' <returns></returns>
    MustOverride Function Consultar(filtro As T) As Respuesta(Of IEnumerable(Of T))

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="filtro"></param>
    ''' <returns></returns>
    MustOverride Function Obtener(filtro As T) As Respuesta(Of T)

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="entidad"></param>
    ''' <returns></returns>
    MustOverride Function Guardar(entidad As T) As Respuesta(Of Long)

End Class
