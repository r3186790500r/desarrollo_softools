﻿
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria

Namespace Despachos.Paqueteria
    Public NotInheritable Class LogicaDetallePlanillaDespachos
        Inherits LogicaBase(Of DetallePlanillaDespachos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of DetallePlanillaDespachos)

        Sub New(persistencia As IPersistenciaBase(Of DetallePlanillaDespachos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As DetallePlanillaDespachos) As Respuesta(Of IEnumerable(Of DetallePlanillaDespachos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetallePlanillaDespachos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetallePlanillaDespachos))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As DetallePlanillaDespachos) As Respuesta(Of DetallePlanillaDespachos)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Guardar(entidad As DetallePlanillaDespachos) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace