﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class RepositorioDetallePolizasSolicitudOrdenServicios
        Inherits RepositorioBase(Of DetallePolizasSolicitudOrdenServicios)
        Public Overrides Function Consultar(filtro As DetallePolizasSolicitudOrdenServicios) As IEnumerable(Of DetallePolizasSolicitudOrdenServicios)
            Dim lista As New List(Of DetallePolizasSolicitudOrdenServicios)
            Dim item As DetallePolizasSolicitudOrdenServicios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_polizas_solicitus_orden_servicios")
                While resultado.Read
                    item = New DetallePolizasSolicitudOrdenServicios(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As DetallePolizasSolicitudOrdenServicios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetallePolizasSolicitudOrdenServicios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetallePolizasSolicitudOrdenServicios) As DetallePolizasSolicitudOrdenServicios
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetallePolizasSolicitudOrdenServicios) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
