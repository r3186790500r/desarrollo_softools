﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="DocumentosController"/>
''' </summary>
<Authorize>
Public Class DocumentosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Documentos) As Respuesta(Of IEnumerable(Of Documentos))
        Return New LogicaDocumentos(CapaPersistenciaDocumentos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Documentos) As Respuesta(Of Documentos)
        Return New LogicaDocumentos(CapaPersistenciaDocumentos).Obtener(filtro)
    End Function
    <HttpPost>
    <ActionName("InsertarTemporal")>
    Public Function Guardar(ByVal entidad As Documentos) As Respuesta(Of Long)
        Return New LogicaDocumentos(CapaPersistenciaDocumentos).InsertarTemporal(entidad)
    End Function
    <HttpPost>
    <ActionName("EliminarDocumento")>
    Public Function EliminarDocumento(ByVal entidad As Documentos) As Respuesta(Of Boolean)
        Return New LogicaDocumentos(CapaPersistenciaDocumentos).EliminarDocumento(entidad)
    End Function
    <HttpPost>
    <ActionName("GuardarDocumentoTercero")>
    Public Function GuardarDocumentoTercero(ByVal entidad As Documentos) As Respuesta(Of Long)
        Return New LogicaDocumentos(CapaPersistenciaDocumentos).GuardarDocumentoTercero(entidad)
    End Function
    <HttpPost>
    <ActionName("InsertarDocumentosCumplidoRemesa")>
    Public Function InsertarDocumentosCumplidoRemesa(ByVal entidad As Documentos) As Respuesta(Of Long)
        Return New LogicaDocumentos(CapaPersistenciaDocumentos).InsertarDocumentosCumplidoRemesa(entidad)
    End Function

    <ActionName("EliminarDocumentoCumplidoRemesa")>
    Public Function EliminarDocumentosCumplidoRemesa(ByVal entidad As Documentos) As Respuesta(Of Boolean)
        Return New LogicaDocumentos(CapaPersistenciaDocumentos).EliminarDocumentosCumplidoRemesa(entidad)
    End Function

    <HttpPost>
    <ActionName("InsertarDocumentoRemesa")>
    Public Function InsertarDocumentoRemesa(ByVal entidad As Documentos) As Respuesta(Of Long)
        Return New LogicaDocumentos(CapaPersistenciaDocumentos).InsertarDocumentoRemesa(entidad)
    End Function

    <ActionName("EliminarDocumentoRemesa")>
    Public Function EliminarDocumentoRemesa(ByVal entidad As Documentos) As Respuesta(Of Boolean)
        Return New LogicaDocumentos(CapaPersistenciaDocumentos).EliminarDocumentoRemesa(entidad)
    End Function
End Class
