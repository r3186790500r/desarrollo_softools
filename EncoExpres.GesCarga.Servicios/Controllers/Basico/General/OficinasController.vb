﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="OficinasController"/>
''' </summary>
<Authorize>
Public Class OficinasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Oficinas) As Respuesta(Of IEnumerable(Of Oficinas))
        Return New LogicaOficinas(CapaPersistenciaOficinas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Oficinas) As Respuesta(Of Oficinas)
        Return New LogicaOficinas(CapaPersistenciaOficinas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Oficinas) As Respuesta(Of Long)
        Return New LogicaOficinas(CapaPersistenciaOficinas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Oficinas) As Respuesta(Of Boolean)
        Return New LogicaOficinas(CapaPersistenciaOficinas).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarRegionesPaises")>
    Public Function ConsultarRegionesPaises(ByVal entidad As RegionesPaises) As Respuesta(Of IEnumerable(Of RegionesPaises))
        Return New LogicaOficinas(CapaPersistenciaOficinas).ConsultarRegionesPaises(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarConsecutivosPorOficina")>
    Public Function ConsultarConXOficina(ByVal entidad As Oficinas) As Respuesta(Of IEnumerable(Of TipoDocumentos))
        Return New LogicaOficinas(CapaPersistenciaOficinas).ConsultarConTipoDocumento(entidad)
    End Function

    <HttpPost>
    <ActionName("InsertarConsecutivos")>
    Public Function InsertarLegalizacionGastosRuta(ByVal entidad As Oficinas) As Respuesta(Of Boolean)
        Return New LogicaOficinas(CapaPersistenciaOficinas).InsertarConsecutivos(entidad)
    End Function
End Class
