﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Repositorio.Gesphone

Namespace Gesphone
    Public NotInheritable Class PersistenciaEncabezadoCheckListCargueDescargues
        Implements IPersistenciaBase(Of EncabezadoCheckListCargueDescargues)

        Public Function Consultar(filtro As EncabezadoCheckListCargueDescargues) As IEnumerable(Of EncabezadoCheckListCargueDescargues) Implements IPersistenciaBase(Of EncabezadoCheckListCargueDescargues).Consultar
            Return New RepositorioEncabezadoCheckListCargueDescargues().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoCheckListCargueDescargues) As Long Implements IPersistenciaBase(Of EncabezadoCheckListCargueDescargues).Insertar
            Return New RepositorioEncabezadoCheckListCargueDescargues().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoCheckListCargueDescargues) As Long Implements IPersistenciaBase(Of EncabezadoCheckListCargueDescargues).Modificar
            Return New RepositorioEncabezadoCheckListCargueDescargues().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoCheckListCargueDescargues) As EncabezadoCheckListCargueDescargues Implements IPersistenciaBase(Of EncabezadoCheckListCargueDescargues).Obtener
            Return New RepositorioEncabezadoCheckListCargueDescargues().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EncabezadoCheckListCargueDescargues) As Boolean
            Return New RepositorioEncabezadoCheckListCargueDescargues().Anular(entidad)
        End Function
    End Class

End Namespace

