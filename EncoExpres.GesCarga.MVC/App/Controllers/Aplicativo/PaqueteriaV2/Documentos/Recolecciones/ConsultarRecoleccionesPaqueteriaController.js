﻿EncoExpresApp.controller("ConsultarRecoleccionesPaqueteriaCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'RecoleccionesFactory', 'OficinasFactory',
    'ZonasFactory', 'TercerosFactory', 'ValorCatalogosFactory',
    function ($scope, $timeout, $linq, blockUI, blockUIConfig, $routeParams, RecoleccionesFactory, OficinasFactory, ZonasFactory,
        TercerosFactory, ValorCatalogosFactory) {

        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Recolecciones' }];
        $scope.Titulo = "CONSULTAR RECOLECCIONES";
        try {
            try {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.RECOLECCIONES);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

            } catch (e) {

                //$scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_RECOLECCIONES_GESPHONE);
                //$scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                //$scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                //$scope.DeshabilitarEliminarAnular = true
                //$scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                //$scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
            }
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }

        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListaEstadoProgramacion = [];
        $scope.Numero = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.ListaRecorridosConsultados = [];
        $scope.HabilitarFecha = false;
        $scope.FechaInicio = new Date();
        $scope.FechaFin = new Date();
        $scope.ListadoOficinasCiudad = [];
        $scope.ListadoCliente = [];
        $scope.ListadoRemitente = [];
        $scope.ListadoEstadosRecoleccion = [];
        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.ModeloEstado = $scope.ListadoEstados[0];
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //--Clientes
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //--Clientes
        //--Remitente
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_REMITENTE,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoRemitente);
                }
            }
            return $scope.ListadoRemitente;
        };
        //--Remitente
        //----------AutoComplete
        //--------------------------Init-------------------------//
        $scope.InitLoad = function () {
            //--Catalogo Estados Recoleccion
            var tmpListaEstadoRecoleccion = [];
            tmpListaEstadoRecoleccion = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CATALOGO_ESTADOS_RECOLECCIONES.CODIGO },
                Estado: ESTADO_DEFINITIVO,
                Sync: true
            }).Datos;
            tmpListaEstadoRecoleccion.splice(tmpListaEstadoRecoleccion.findIndex(item => item.Codigo === CATALOGO_ESTADOS_RECOLECCIONES.NOAPLICA), 1);
            $scope.ListadoEstadosRecoleccion.push({ Nombre: '(TODAS)', Codigo: -1 });
            $scope.ListadoEstadosRecoleccion = $scope.ListadoEstadosRecoleccion.concat(tmpListaEstadoRecoleccion);
            $scope.ModeloEstadoRecoleccion = $linq.Enumerable().From($scope.ListadoEstadosRecoleccion).First('$.Codigo ==-1');
            //--Catalogo Estados Recoleccion
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1, CodigoCiudad: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoOficinasCiudad.push({ Nombre: '(TODAS)', Codigo: 0 });
                            response.data.Datos.forEach(function (item) {
                                if (item.Codigo !== 0) {
                                    $scope.ListadoOficinasCiudad.push(item);
                                }
                            });
                            $scope.ModeloOficinasCiudad = $scope.ListadoOficinasCiudad[0];
                        }
                        else {
                            $scope.ModeloOficinasCiudad = $scope.ListadoOficinasCiudad[0];
                        }
                    }
                }, function (response) {
                    });
            //--Obtener Zonas Ciudad
            $scope.ObtenerZonas();
            //--Obtener Zonas Ciudad
            //--Obtener parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
                if ($routeParams.Numero > 0) {
                    $scope.ModeloOficinasCiudad = { Codigo: 0 };
                    $scope.NumeroDocumento = $routeParams.Numero;
                    Find();
                    $scope.NumeroDocumento = '';
                }
            }
        };
        //--------------------------Init-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //-------------------------Funciones Generales-------------------------------//
        //--Zonas
        $scope.ObtenerZonas = function () {
            if ($scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad != undefined && $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad != null && $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad != '') {
                if ($scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo > 0) {
                    $scope.ListadoZonas = [];
                    $scope.ListadoZonas = ZonasFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Estado: ESTADO_DEFINITIVO,
                        Ciudad: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo },
                        Sync: true
                    }).Datos;
                }
            }
            $scope.ListadoZonas.push({ Codigo: -1, Nombre: "(TODAS)" });
            $scope.ModeloZonas = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==-1');
        };
        //-- Zonas

        //----Funciones paginación
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        //----Funciones paginación
        //--Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRecoleccionesPaqueteria';
            }
        };
        //--Buscar
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                    Find()
                }
            }
        };
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoRecolecciones = [];

            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                NumeroDocumento: $scope.NumeroDocumento,
                FechaInicio: $scope.FechaInicio,
                FechaFin: $scope.FechaFin,
                Oficinas: $scope.ModeloOficinasCiudad,
                Ciudad: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad,
                Zonas: $scope.ModeloZonas,
                Cliente: $scope.ModeloCliente,
                Remitente: $scope.ModeloRemitente,
                Estado: $scope.ModeloEstado.Codigo,
                EstadoRecoleccion: $scope.ModeloEstadoRecoleccion,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina
            };
            blockUI.delay = 1000;
            RecoleccionesFactory.ConsultarENCOE(Filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoRecolecciones = response.data.Datos;

                            for (var i = 0; i < $scope.ListadoRecolecciones.length; i++) {
                                if (new Date($scope.ListadoRecolecciones[i].FechaPlanilla) < MIN_DATE) {
                                    $scope.ListadoRecolecciones[i].FechaPlanilla = '';
                                }
                            }
                            for (var j = 0; j < $scope.ListadoRecolecciones.length; j++) {
                                if ($scope.ListadoRecolecciones[j].Peso === 0) {
                                    $scope.ListadoRecolecciones[j].Peso = '';
                                }
                            }
                            for (var k = 0; k < $scope.ListadoRecolecciones.length; k++) {
                                if ($scope.ListadoRecolecciones[k].NumeroPlanilla === 0) {
                                    $scope.ListadoRecolecciones[k].NumeroPlanilla = '';
                                }
                            }
                            for (var l = 0; l < $scope.ListadoRecolecciones.length; l++) {
                                if ($scope.ListadoRecolecciones[l].Cliente.Codigo === 0) {
                                    $scope.ListadoRecolecciones[l].Cliente.Nombre = '';
                                }
                            }
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = true;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.ListadoRecolecciones = "";
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        }
        //--Buscar
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroDocumento === null || $scope.NumeroDocumento === undefined || $scope.NumeroDocumento === '' || $scope.NumeroDocumento === 0 || isNaN($scope.NumeroDocumento) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        }
        //----Eliminar Recoleccion
        $scope.EliminarRecoleccion = function (numero, nombreMercancia, Documento) {
            $scope.CodigoNumero = numero
            $scope.NumeroDocumento = Documento
            $scope.NombreMercancia = nombreMercancia
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarRecolecciones');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.CodigoNumero,
            };

            RecoleccionesFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se anulo la recolección ' + $scope.NumeroDocumento + ' - ' + $scope.NombreMercancia);
                        closeModal('modalEliminarRecolecciones');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarRecolecciones');
                    $scope.ModalError = 'No se puede anular la recolección de ' + $scope.NombreMercancia + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };

        $scope.CerrarModal = function () {
            closeModal('modalEliminarRecolecciones');
            closeModal('modalMensajeEliminarRecolecciones');
        }
        //----Eliminar Recoleccion
        //----Mascaras
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        //----Mascaras
    }]);