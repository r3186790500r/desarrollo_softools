﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.OrdenCargue
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Entidades.Tesoreria

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" EncabezadoDocumentos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoDocumentos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")


        End Sub
        <JsonProperty>
        Public Property OrdenServicio As EncabezadoSolicitudOrdenServicios
        <JsonProperty>
        Public Property Programacion As EncabezadoProgramacionOrdenServicios
        <JsonProperty>
        Public Property OrdenCargue As OrdenCargue
        <JsonProperty>
        Public Property Manifiesto As Manifiesto
        <JsonProperty>
        Public Property Remesa As Remesas
        <JsonProperty>
        Public Property Planilla As Planillas
        <JsonProperty>
        Public Property Cumplido As Cumplido
        <JsonProperty>
        Public Property Liquidacion As Liquidacion
        <JsonProperty>
        Public Property FacturaVentas As Facturas
        <JsonProperty>
        Public Property Comprobante As EncabezadoDocumentoComprobantes
    End Class
End Namespace
