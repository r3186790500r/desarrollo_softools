﻿EncoExpresApp.factory('ServicioSIESAFactory', ['$http', function ($http) {

    var urlService = GetUrl('ServiciosSIESA');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.ConsultarCAJAS = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarCajasSIESA';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.ConsultarOficinas = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarOficinaSIESA';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.ConsultarTercero = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarTerceroSIESA';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.ImportarTerceroSIESA = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ImportarTerceroSIESA';
        request.data = filtro;if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ConsultarSucursalesTenedor = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarSucursalesTenedorSIESA';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    return factory;
}]);