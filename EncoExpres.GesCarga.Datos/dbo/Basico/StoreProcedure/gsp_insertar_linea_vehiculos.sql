﻿PRINT 'gsp_insertar_linea_vehiculos'
GO
DROP PROCEDURE gsp_insertar_linea_vehiculos
GO
CREATE PROCEDURE gsp_insertar_linea_vehiculos
(
@par_EMPR_Codigo SMALLINT ,
@par_Codigo_Alterno VARCHAR (20),
@par_MAVE_Codigo NUMERIC,
@par_Nombre VARCHAR (50),
@par_Estado SMALLINT,
@par_USUA_Codigo_Crea SMALLINT
)
AS 
BEGIN
	INSERT INTO Linea_Vehiculos 
	(
	EMPR_Codigo,
	Codigo_Alterno,
	MAVE_Codigo,
	Nombre,
	Estado,
	USUA_Codigo_Crea,
	Fecha_Crea
	)
	VALUES
	(
	@par_EMPR_Codigo,
	@par_Codigo_Alterno,
	@par_MAVE_Codigo,
	@par_Nombre,
	@par_Estado,
	@par_USUA_Codigo_Crea,
	GETDATE()
	)
	SELECT Codigo = @@identity

END
GO