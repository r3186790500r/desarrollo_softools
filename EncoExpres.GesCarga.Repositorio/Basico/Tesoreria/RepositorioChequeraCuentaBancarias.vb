﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioChequeraCuentaBancarias"/>
    ''' </summary>
    Public NotInheritable Class RepositorioChequeraCuentaBancarias
        Inherits RepositorioBase(Of ChequeraCuentaBancarias)

        Public Overrides Function Consultar(filtro As ChequeraCuentaBancarias) As IEnumerable(Of ChequeraCuentaBancarias)
            Dim lista As New List(Of ChequeraCuentaBancarias)
            Dim item As ChequeraCuentaBancarias

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.NombreCuentaBancaria) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Cuenta_Bancaria", filtro.NombreCuentaBancaria)
                End If
                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_chequeras]")

                While resultado.Read
                    item = New ChequeraCuentaBancarias(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As ChequeraCuentaBancarias) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CUBA_Codigo", entidad.CuentaBancaria.Codigo)
                conexion.AgregarParametroSQL("@par_Cheque_Inicial", entidad.ChequeInicial)
                conexion.AgregarParametroSQL("@par_Cheque_Final", entidad.ChequeFinal)
                conexion.AgregarParametroSQL("@par_Cheque_Actual", entidad.ChequeActual)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_chequera_cuenta_bancarias")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Overrides Function Modificar(entidad As ChequeraCuentaBancarias) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_CUBA_Codigo", entidad.CuentaBancaria.Codigo)
                conexion.AgregarParametroSQL("@par_Cheque_Inicial", entidad.ChequeInicial)
                conexion.AgregarParametroSQL("@par_Cheque_Final", entidad.ChequeFinal)
                conexion.AgregarParametroSQL("@par_Cheque_Actual", entidad.ChequeActual)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_chequera_cuenta_bancarias")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Overrides Function Obtener(filtro As ChequeraCuentaBancarias) As ChequeraCuentaBancarias
            Dim item As New ChequeraCuentaBancarias

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("dbo.gsp_obtener_chequera_cuenta_bancarias")

                While resultado.Read
                    item = New ChequeraCuentaBancarias(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As ChequeraCuentaBancarias) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_chequera_cuenta_bancarias]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace