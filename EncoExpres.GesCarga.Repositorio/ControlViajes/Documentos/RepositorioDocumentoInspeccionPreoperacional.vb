﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports System.Transactions

Namespace ControlViajes

    ''' <summary>
    ''' Clase <see cref="RepositorioDocumentoInspeccionPreoperacional"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDocumentoInspeccionPreoperacional
        Inherits RepositorioBase(Of DocumentoInspeccionPreoperacional)

        Public Overrides Function Consultar(filtro As DocumentoInspeccionPreoperacional) As IEnumerable(Of DocumentoInspeccionPreoperacional)
            Dim lista As New List(Of DocumentoInspeccionPreoperacional)
            Dim item As DocumentoInspeccionPreoperacional

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero >= Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_documento_inspecciones]")

                While resultado.Read
                    item = New DocumentoInspeccionPreoperacional(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DocumentoInspeccionPreoperacional) As Long
            Throw New NotImplementedException()
        End Function



        Public Overrides Function Modificar(entidad As DocumentoInspeccionPreoperacional) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DocumentoInspeccionPreoperacional) As DocumentoInspeccionPreoperacional
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DocumentoInspeccionPreoperacional) As Boolean
            Throw New NotImplementedException()
        End Function


        Public Function InsertarTemporal(entidad As DocumentoInspeccionPreoperacional) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENIS_Numero", entidad.NumeroInspeccion)
                    conexion.AgregarParametroSQL("@par_Nombre_Documento", entidad.NombreDocumento)
                    conexion.AgregarParametroSQL("@par_Extencion_Documento", entidad.ExtencionDocumento)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Documento", entidad.Documento, SqlDbType.Image)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_t_documento_inspecciones]")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While


                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        inserto = False
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using
            Return entidad.Codigo
        End Function
        Public Function EliminarTemporal(entidad As DocumentoInspeccionPreoperacional) As Boolean
            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Eliminar_Temporal", entidad.EliminarTemporal)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_t_documento_inspecciones]")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using
            Return inserto
        End Function
        Public Function LimpiarTemporalUsuario(entidad As DocumentoInspeccionPreoperacional) As Boolean
            Dim inserto As Boolean = False

            Using contextoConexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                contextoConexion.CreateConnection()
                contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                contextoConexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)
                contextoConexion.AgregarParametroSQL("@par_Numero", entidad.NumeroInspeccion)
                Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_limpiar_t_documento_inspecciones]")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
                End While
            End Using
            Return inserto
        End Function
        Public Function TrasladarFotos(entidad As DocumentoInspeccionPreoperacional, ByRef contextoConexion As DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ENIS_Numero", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_transladar_documento_inspecciones]")

            While resultado.Read
                inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
            End While
            resultado.Close()

            Return inserto
        End Function


    End Class

End Namespace