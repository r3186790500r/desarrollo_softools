﻿Print 'Catalogo 34 tipo Semirremolque'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 34
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 34
GO
DELETE Catalogos WHERE Codigo = 34
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,34,'TISE','Tipo Semirremolque',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,34,3400,UPPER('(NO APLICA)'),1,GETDATE()),
(1,34,3401,UPPER('Semiremolque de 1 Eje'),1,GETDATE()),
(1,34,3402,UPPER('Remolque Balanceado de 3 Ejes'),1,GETDATE()),
(1,34,3403,UPPER('Semiremolque de 2 Ejes'),1,GETDATE()),
(1,34,3404,UPPER('Semiremolque de 3 Ejes'),1,GETDATE()),
(1,34,3405,UPPER('Semiremolque de 4 o Mas Ejes'),1,GETDATE()),
(1,34,3406,UPPER('Remolque de 2 Ejes'),1,GETDATE()),
(1,34,3407,UPPER('Remolque de 3 Ejes'),1,GETDATE()),
(1,34,3408,UPPER('Remolque de 4 Ejes'),1,GETDATE()),
(1,34,3409,UPPER('Remolque Balanceado de 1 Ejes'),1,GETDATE()),
(1,34,3410,UPPER('Remolque Balanceado de 2 Ejes'),1,GETDATE())

GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,34,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,34,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 