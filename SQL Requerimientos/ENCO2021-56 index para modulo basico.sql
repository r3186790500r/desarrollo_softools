USE [ENCOEXPRES]
GO
------------------------------------------------------------------ CAJAS (req 014)----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Cajas].[IX_Caja_Oficina]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Caja_Oficina] ON [dbo].[Cajas]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC,
	[OFIC_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ CAJAS (req 014)----------------------------------------------------
------------------------------------------------------------------ Ciudades----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Ciudades].[IX_Ciudad_Codigo_DANE]
GO

--delete from [ENCOEXPRES].[dbo].[Ciudades] where [Codigo] in(1346,1347,1348) -elimina ciudades extranjeras
--delete from [ENCOEXPRES].[dbo].[Ciudades] where [Codigo] in (378,634,799,862,341,315,267,252,1058,690,420,1282) elimina ciudades repetidas mas antiguas

CREATE UNIQUE NONCLUSTERED INDEX [IX_Ciudad_Codigo_DANE] ON [dbo].[Ciudades]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Ciudades----------------------------------------------------
------------------------------------------------------------------ Productos Transportados----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Producto_Transportados].[IX_Producto_Transportados_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Producto_Transportados_Nombre] ON [dbo].[Producto_Transportados]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Productos Transportados----------------------------------------------------
------------------------------------------------------------------ Departamentos----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Departamentos].[IX_Departamentos_Codigo_DANE]
GO

--delete from [ENCOEXPRES].[dbo].[Departamentos] where [Codigo] in(48,49,50,51,54,57) -elimina departamentos extranjeros
--delete from [ENCOEXPRES].[dbo].[Departamentos] where [Codigo] = 4 -elimina distrito capital repetido

CREATE UNIQUE NONCLUSTERED INDEX [IX_Departamentos_Codigo_DANE] ON [dbo].[Departamentos]
(
	[EMPR_Codigo] ASC,
	[Codigo_DANE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Departamentos----------------------------------------------------
------------------------------------------------------------------ Unidades Empaque----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Unidad_Empaque_Producto_Transportados].[IX_Unidad_Empaque_Producto_Transportados_Nombre_Corto]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Unidad_Empaque_Producto_Transportados_Nombre_Corto] ON [dbo].[Unidad_Empaque_Producto_Transportados]
(
	[EMPR_Codigo] ASC,
	[Nombre_Corto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Unidades Empaque----------------------------------------------------
------------------------------------------------------------------ Impuestos----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Encabezado_Impuestos].[IX_Encabezado_Impuestos_Codigo_Alterno]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Encabezado_Impuestos_Codigo_Alterno] ON [dbo].[Encabezado_Impuestos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Impuestos----------------------------------------------------
------------------------------------------------------------------ Colores Veh�culos----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Color_Vehiculos].[IX_Color_Vehiculos_Nombre]
GO

--delete from [ENCOEXPRES].[dbo].[Color_Vehiculos] where [Codigo] = 800 elimina color AMARILLO LIMON mas antiguo 
--delete from [ENCOEXPRES].[dbo].[Color_Vehiculos] where [Codigo] in (805,798,1235,13,1530,14) elimina colores repetidos mas antiguos

CREATE UNIQUE NONCLUSTERED INDEX [IX_Color_Vehiculos_Nombre] ON [dbo].[Color_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

DROP INDEX IF EXISTS [dbo].[Color_Vehiculos].[IX_Color_Vehiculos_Codigo_Alterno]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Color_Vehiculos_Codigo_Alterno] ON [dbo].[Color_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Colores Veh�culos----------------------------------------------------
------------------------------------------------------------------ Marcas Veh�culos----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Marca_Vehiculos].[IX_Marca_Vehiculos_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Marca_Vehiculos_Nombre] ON [dbo].[Marca_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

DROP INDEX IF EXISTS [dbo].[Marca_Vehiculos].[IX_Marca_Vehiculos_Codigo_Alterno]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Marca_Vehiculos_Codigo_Alterno] ON [dbo].[Marca_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Marcas Veh�culos----------------------------------------------------
------------------------------------------------------------------ Lineas Veh�culos----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Linea_Vehiculos].[IX_Linea_Vehiculos_Nombre]
GO
--delete from [ENCOEXPRES].[dbo].[Linea_Vehiculos] where [Codigo] = 347 elimina linea repetida mas antigua JNC 200 ZH
--delete from [ENCOEXPRES].[dbo].[Linea_Vehiculos] where [Codigo] in(1553,844,1507,10499,1706,634,936,1228,1740,538,537,1890,1647,1648,1615,810,1377,1544,474,1563,845,1643,401,1564,544,545,546,539,540,541,542,543,600)
--elimina linea repetida mas antigua
CREATE UNIQUE NONCLUSTERED INDEX [IX_Linea_Vehiculos_Nombre] ON [dbo].[Linea_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[MAVE_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

DROP INDEX IF EXISTS [dbo].[Linea_Vehiculos].[IX_Linea_Vehiculos_Codigo_Alterno]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Linea_Vehiculos_Codigo_Alterno] ON [dbo].[Linea_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC, 
	[MAVE_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Lineas Veh�culos----------------------------------------------------
------------------------------------------------------------------ Rutas----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Rutas].[IX_Rutas_Codigo_Alterno]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Rutas_Codigo_Alterno] ON [dbo].[Rutas]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Rutas----------------------------------------------------
------------------------------------------------------------------ Marcas Semirremolques----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Marca_Semirremolques].[IX_Marca_Semirremolques_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Marca_Semirremolques_Nombre] ON [dbo].[Marca_Semirremolques]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

DROP INDEX IF EXISTS [dbo].[Marca_Semirremolques].[IX_Marca_Semirremolques_Codigo_Alterno]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Marca_Semirremolques_Codigo_Alterno] ON [dbo].[Marca_Semirremolques]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Marcas Semirremolques----------------------------------------------------
------------------------------------------------------------------ Puestos de Control----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Puesto_Controles].[IX_Puesto_Controles_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Puesto_Controles_Nombre] ON [dbo].[Puesto_Controles]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

DROP INDEX IF EXISTS [dbo].[Puesto_Controles].[IX_Puesto_Controles_Codigo_Alterno]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Puesto_Controles_Codigo_Alterno] ON [dbo].[Puesto_Controles]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Puestos de Control----------------------------------------------------
------------------------------------------------------------------ Conceptos Liquidaci�n----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Conceptos_Liquidacion_Planilla_Despachos].[IX_Conceptos_Liquidacion_Planilla_Despachos_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Conceptos_Liquidacion_Planilla_Despachos_Nombre] ON [dbo].[Conceptos_Liquidacion_Planilla_Despachos]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Conceptos Liquidaci�n----------------------------------------------------
------------------------------------------------------------------ Novedades Despachos----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Novedades_Despacho].[IX_Novedades_Despacho_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Novedades_Despacho_Nombre] ON [dbo].[Novedades_Despacho]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Novedades Despachos----------------------------------------------------
------------------------------------------------------------------ Conceptos Gastos----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Conceptos_Legalizacion_Gastos_Conductor].[IX_Conceptos_Legalizacion_Gastos_Conductor_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Conceptos_Legalizacion_Gastos_Conductor_Nombre] ON [dbo].[Conceptos_Legalizacion_Gastos_Conductor]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Conceptos Gastos----------------------------------------------------
------------------------------------------------------------------ Plan �nico Cuentas----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Plan_Unico_Cuentas].[IX_Plan_Unico_Cuentas_Codigo_Cuenta]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Plan_Unico_Cuentas_Codigo_Cuenta] ON [dbo].[Plan_Unico_Cuentas]
(
	[EMPR_Codigo] ASC,
	[Codigo_Cuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Plan �nico Cuentas----------------------------------------------------
------------------------------------------------------------------ Bancos----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Bancos].[IX_Bancos_Codigo_Cuenta]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Bancos_Codigo_Cuenta] ON [dbo].[Bancos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Bancos----------------------------------------------------
------------------------------------------------------------------ Cuentas Bancarias----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Cuenta_Bancarias].[IX_Cuenta_Bancarias_Numero_Cuenta]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Cuenta_Bancarias_Numero_Cuenta] ON [dbo].[Cuenta_Bancarias]
(
	[EMPR_Codigo] ASC,
	[Numero_Cuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

DROP INDEX IF EXISTS [dbo].[Cuenta_Bancarias].[IX_Cuenta_Bancarias_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Cuenta_Bancarias_Nombre] ON [dbo].[Cuenta_Bancarias]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Cuentas Bancarias----------------------------------------------------
------------------------------------------------------------------ Conceptos Contables----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Encabezado_Concepto_Contables].[IX_Encabezado_Concepto_Contables_Codigo_Alterno]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Encabezado_Concepto_Contables_Codigo_Alterno] ON [dbo].[Encabezado_Concepto_Contables]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

DROP INDEX IF EXISTS [dbo].[Encabezado_Concepto_Contables].[IX_Encabezado_Concepto_Contables_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Encabezado_Concepto_Contables_Nombre] ON [dbo].[Encabezado_Concepto_Contables]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Conceptos Contables----------------------------------------------------
------------------------------------------------------------------ Movimientos Contables----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Encabezado_Parametrizacion_Contables].[IX_Encabezado_Parametrizacion_Contables_Codigo_Alterno]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Encabezado_Parametrizacion_Contables_Codigo_Alterno] ON [dbo].[Encabezado_Parametrizacion_Contables]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

DROP INDEX IF EXISTS [dbo].[Encabezado_Parametrizacion_Contables].[IX_Encabezado_Parametrizacion_Contables_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Encabezado_Parametrizacion_Contables_Nombre] ON [dbo].[Encabezado_Parametrizacion_Contables]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Movimientos Contables----------------------------------------------------
------------------------------------------------------------------ Cuentas Cambistas----------------------------------------------------
--------MISMO QUE CUENTAS BANCARIAS, SOLO VARIA TIPO DE CUENTA
------------------------------------------------------------------ Cuentas Cambistas----------------------------------------------------
------------------------------------------------------------------ Conceptos Facturaci�n----------------------------------------------------
DROP INDEX IF EXISTS [dbo].[Conceptos_Ventas].[IX_Conceptos_Ventas_Nombre]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Conceptos_Ventas_Nombre] ON [dbo].[Conceptos_Ventas]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
------------------------------------------------------------------ Conceptos Facturaci�n----------------------------------------------------