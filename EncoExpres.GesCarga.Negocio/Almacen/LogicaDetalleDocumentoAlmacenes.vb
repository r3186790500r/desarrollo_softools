﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Almacen
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Almacen
    Public NotInheritable Class LogicaDetalleDocumentoAlmacenes
        Inherits LogicaBase(Of DetalleDocumentoAlmacenes)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of DetalleDocumentoAlmacenes)

        Sub New(persistencia As IPersistenciaBase(Of DetalleDocumentoAlmacenes))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As DetalleDocumentoAlmacenes) As Respuesta(Of IEnumerable(Of DetalleDocumentoAlmacenes))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleDocumentoAlmacenes))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleDocumentoAlmacenes))(CType(consulta, IEnumerable(Of DetalleDocumentoAlmacenes)))
        End Function

        Public Overrides Function Guardar(entidad As DetalleDocumentoAlmacenes) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As DetalleDocumentoAlmacenes) As Respuesta(Of DetalleDocumentoAlmacenes)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of DetalleDocumentoAlmacenes)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of DetalleDocumentoAlmacenes)(consulta)
        End Function
    End Class
End Namespace
