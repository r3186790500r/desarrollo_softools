﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 17/06/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


PRINT 'gsp_obtener_detalle_seguimiento_vehiculos'
GO
DROP PROCEDURE gsp_obtener_detalle_seguimiento_vehiculos
GO
CREATE PROCEDURE gsp_obtener_detalle_seguimiento_vehiculos 
(                  
@par_EMPR_Codigo SMALLINT,                  
@par_Codigo NUMERIC = NULL,    
@par_Tipo_Consulta NUMERIC = NULL                
)                  
AS                  
BEGIN         
  IF @par_Tipo_Consulta = 0 --Ordenes      
      
    BEGIN               
 SELECT TOP 1           
  1 AS Obtener,     
  0 AS Tipo_Consulta,              
  DESV.EMPR_Codigo,                    
  DESV.ENPD_Numero,    
  DESV.ENOC_Numero,     
  DESV.ENMC_Numero,                   
  DESV.ID,           
  VEHI.CATA_TIVE_Codigo,    
  TIVE.Campo1 AS Tipo_Vehiculo,     
  ENOC.TERC_Codigo_Remitente AS TERC_Codigo_Tenedor,      
  ISNULL(TENE.Razon_Social, '')+ISNULL(TENE.Nombre, '') + ' '+ ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2, '')  AS Nombre_Tenedor,                                
  ENOC.TERC_Codigo_Conductor,                  
  ISNULL(COND.Razon_Social, '')+ISNULL(COND.Nombre, '') + ' '+ ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') AS Nombre_Conductor,     
  COND.Numero_Identificacion AS Identificacion_Conductor,     
  COND.Celulares AS Celular_Conductor,      
  ENOC.VEHI_Codigo,     
  ISNULL(VEHI.Placa, '') AS Vehiculo,                    
  ENOC.SEMI_Codigo,     
  ISNULL(SEMI.Placa, '') AS Semirremolque,                   
  ENOC.RUTA_Codigo,      
  ISNULL(RUTA.Nombre, '') AS Ruta,       
  '' AS URL_GPS,                                   
  ISNULL(VEHI.Usuario_GPS, '') AS Usuario_GPS,    
  ISNULL(VEHI.Clave_GPS, '') AS Clave_GPS,                   
  DESV.Orden,                   
  ENOC.Numero_Documento AS Numero_Documento_Orden,       
  0 AS Numero_Documento_Planilla,  
  ISNULL(DESV.CATA_SRSV_Codigo, 0) AS CATA_SRSV_Codigo,     
  ISNULL(DESV.CATA_NOSV_Codigo, 0) AS CATA_NOSV_Codigo,     
  ISNULL(DESV.PUCO_Codigo, 0) AS PUCO_Codigo,     
  ISNULL(DESV.Ubicacion, '') AS Ubicacion,     
  ISNULL(DESV.Latitud, 0) AS Latitud,     
  ISNULL(DESV.Longitud, 0) AS Longitud,     
  ISNULL(DESV.Observaciones, '') AS Observaciones,     
  ISNULL(DESV.Reportar_Cliente, 0) AS Reportar_Cliente,  
  ISNULL(DESV.Prioritario, 0) AS Prioritario 
   FROM                        
   Detalle_Seguimiento_Vehiculos DESV                  
                
   LEFT JOIN Encabezado_Orden_Cargues AS ENOC                
   ON DESV.EMPR_Codigo = ENOC.EMPR_Codigo                  
   AND DESV.ENOC_Numero = ENOC.Numero                  
                                    
   LEFT JOIN (SELECT EMPR_Codigo, ISNULL(MAX(Orden),0) AS Orden, ENOC_Numero FROM Detalle_Seguimiento_Vehiculos GROUP BY EMPR_Codigo, ENOC_Numero) AS DESE                  
   ON DESV.EMPR_Codigo = DESE.EMPR_Codigo                   
   AND DESV.ENOC_Numero = DESE.ENOC_Numero                    
    
   LEFT JOIN Terceros AS COND                  
   ON (ENOC.EMPR_Codigo = COND.EMPR_Codigo                  
   AND ENOC.TERC_Codigo_Conductor  = COND.Codigo)                   
                    
   LEFT JOIN Terceros AS TENE                  
   ON (ENOC.EMPR_Codigo = TENE.EMPR_Codigo                  
   AND ENOC.TERC_Codigo_Remitente = TENE.Codigo)                  
                  
   LEFT JOIN Vehiculos AS VEHI                  
   ON (ENOC.EMPR_Codigo = VEHI.EMPR_Codigo                  
   AND ENOC.VEHI_Codigo  = VEHI.Codigo)     
       
   LEFT JOIN Valor_Catalogos AS TIVE                  
   ON VEHI.EMPR_Codigo = TIVE.EMPR_Codigo                  
   AND VEHI.CATA_TIVE_Codigo  = TIVE.Codigo                     
                  
   LEFT JOIN Semirremolques AS SEMI                  
   ON (ENOC.EMPR_Codigo = SEMI.EMPR_Codigo                  
   AND ENOC.SEMI_Codigo  = SEMI.Codigo)         
       
   LEFT JOIN Rutas AS RUTA                  
   ON (ENOC.EMPR_Codigo = RUTA.EMPR_Codigo                  
   AND ENOC.RUTA_Codigo  = RUTA.Codigo)               
               
   WHERE             
   DESV.EMPR_Codigo = @par_EMPR_Codigo            
   AND DESV.ID = ISNULL(@par_Codigo, DESV.ID)                      
    END    
    
  ELSE IF @par_Tipo_Consulta = 1 --Planillas      
      
    BEGIN      SELECT TOP 1          
  1 AS Obtener,      
  1 AS Tipo_Consulta,             
  DESV.EMPR_Codigo,                    
  DESV.ENPD_Numero,    
  DESV.ENOC_Numero,     
  DESV.ENMC_Numero,                   
  DESV.ID,           
  VEHI.CATA_TIVE_Codigo,    
  TIVE.Campo1 AS Tipo_Vehiculo,     
  ENPD.TERC_Codigo_Tenedor,      
  ISNULL(TENE.Razon_Social, '')+ISNULL(TENE.Nombre, '') + ' '+ ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2, '')  AS Nombre_Tenedor,                                
  ENPD.TERC_Codigo_Conductor,                  
  ISNULL(COND.Razon_Social, '')+ISNULL(COND.Nombre, '') + ' '+ ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') AS Nombre_Conductor,     
  COND.Numero_Identificacion AS Identificacion_Conductor,     
  COND.Celulares AS Celular_Conductor,      
  ENPD.VEHI_Codigo,     
  ISNULL(VEHI.Placa, '') AS Vehiculo,                    
  ENPD.SEMI_Codigo,     
  ISNULL(SEMI.Placa, '') AS Semirremolque,                   
  ENPD.RUTA_Codigo,      
  ISNULL(RUTA.Nombre, '') AS Ruta,       
  '' AS URL_GPS,                                   
  ISNULL(VEHI.Usuario_GPS, '') AS Usuario_GPS,    
  ISNULL(VEHI.Clave_GPS, '') AS Clave_GPS,                   
  DESV.Orden,                   
  ENPD.Numero_Documento AS Numero_Documento_Planilla,       
  0 AS Numero_Documento_Orden,    
   ISNULL(DESV.CATA_SRSV_Codigo, 0) AS CATA_SRSV_Codigo,     
  ISNULL(DESV.CATA_NOSV_Codigo, 0) AS CATA_NOSV_Codigo,     
  ISNULL(DESV.PUCO_Codigo, 0) AS PUCO_Codigo,     
  ISNULL(DESV.Ubicacion, '') AS Ubicacion,     
  ISNULL(DESV.Latitud, 0) AS Latitud,     
  ISNULL(DESV.Longitud, 0) AS Longitud,     
  ISNULL(DESV.Observaciones, '') AS Observaciones,     
  ISNULL(DESV.Reportar_Cliente, 0) AS Reportar_Cliente,
  ISNULL(DESV.Prioritario, 0) AS Prioritario         
   FROM                        
   Detalle_Seguimiento_Vehiculos DESV                  
                
   LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                
   ON DESV.EMPR_Codigo = ENPD.EMPR_Codigo                  
   AND DESV.ENPD_Numero = ENPD.Numero                  
                                    
   LEFT JOIN (SELECT EMPR_Codigo, ISNULL(MAX(Orden),0) AS Orden, ENPD_Numero FROM Detalle_Seguimiento_Vehiculos GROUP BY EMPR_Codigo, ENPD_Numero)  AS DESE                  
   ON DESV.EMPR_Codigo = DESE.EMPR_Codigo                   
   AND DESV.ENPD_Numero = DESE.ENPD_Numero      
    
   LEFT JOIN Terceros AS COND                  
   ON (ENPD.EMPR_Codigo = COND.EMPR_Codigo                  
   AND ENPD.TERC_Codigo_Conductor  = COND.Codigo)                   
                    
   LEFT JOIN Terceros AS TENE                  
   ON (ENPD.EMPR_Codigo = TENE.EMPR_Codigo                  
   AND ENPD.TERC_Codigo_Tenedor  = TENE.Codigo)                  
                  
   LEFT JOIN Vehiculos AS VEHI                  
   ON (ENPD.EMPR_Codigo = VEHI.EMPR_Codigo                  
   AND ENPD.VEHI_Codigo  = VEHI.Codigo)     
       
   LEFT JOIN Valor_Catalogos AS TIVE                  
   ON VEHI.EMPR_Codigo = TIVE.EMPR_Codigo                  
   AND VEHI.CATA_TIVE_Codigo  = TIVE.Codigo                     
                  
   LEFT JOIN Semirremolques AS SEMI                  
   ON (ENPD.EMPR_Codigo = SEMI.EMPR_Codigo                  
   AND ENPD.SEMI_Codigo  = SEMI.Codigo)         
       
   LEFT JOIN Rutas AS RUTA                  
   ON (ENPD.EMPR_Codigo = RUTA.EMPR_Codigo                  
   AND ENPD.RUTA_Codigo  = RUTA.Codigo)               
               
   WHERE             
   DESV.EMPR_Codigo = @par_EMPR_Codigo            
   AND DESV.ID = ISNULL(@par_Codigo, DESV.ID)        
    END    
    
END     
GO