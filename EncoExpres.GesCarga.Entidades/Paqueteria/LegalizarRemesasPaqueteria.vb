﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Paqueteria
    <JsonObject>
    Public NotInheritable Class LegalizarRemesasPaqueteria
        Inherits BaseDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")
            Observaciones = Read(lector, "Observaciones")
            Cantidad = Read(lector, "Cantidad")
            Peso = Read(lector, "Peso")
            TotalContado = Read(lector, "Total_Contado")
            TotalContraEntregas = Read(lector, "Total_Contra_Entrega")
            TotalCredito = Read(lector, "Total_Credito")
            TotalRecaudoTercero = Read(lector, "Total_Recaudo_Tercero")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "OFIC_Nombre")}
            DetalleRemesa = New DetalleLegalizarRemesasPaqueteria With {
                .NovedadLegalizarRemesas = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NLGU_Codigo"), .Nombre = Read(lector, "CATA_NLGU_Nombre")},
                .GestionDocumentos = Read(lector, "Gestion_Documentos"),
                .EntregaRecaudo = Read(lector, "Entrega_Recaudo"),
                .EntregaArchivo = Read(lector, "Entrega_Archivo"),
                .EntregaCartera = Read(lector, "Entrega_Cartera"),
                .Observaciones = Read(lector, "Observaciones")
            }
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property TotalContado As Double
        <JsonProperty>
        Public Property TotalContraEntregas As Double
        <JsonProperty>
        Public Property TotalCredito As Double
        <JsonProperty>
        Public Property TotalRecaudoTercero As Double
        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleLegalizarRemesasPaqueteria)
        <JsonProperty>
        Public Property DetalleRemesa As DetalleLegalizarRemesasPaqueteria
    End Class
End Namespace

