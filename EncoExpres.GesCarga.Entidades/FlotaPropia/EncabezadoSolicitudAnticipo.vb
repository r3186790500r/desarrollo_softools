﻿Imports Newtonsoft.Json

Namespace FlotaPropia
    Public Class EncabezadoSolicitudAnticipo
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")
            Observaciones = Read(lector, "Observaciones")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            UsuarioCrea = Read(lector, "USUA_Codigo_Crea")
            CausaAnulacion = Read(lector, "Causa_Anula")

        End Sub
        <JsonProperty>
        Public Property Solicitudes As IEnumerable(Of DetalleSolicitudAnticipo)
        <JsonProperty>
        Public Overloads Property NumeroDocumento As Long

    End Class
End Namespace




