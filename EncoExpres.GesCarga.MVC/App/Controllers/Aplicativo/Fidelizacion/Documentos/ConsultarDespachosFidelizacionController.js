﻿EncoExpresApp.controller("ConsultarDespachosFidelizacionCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'TercerosFactory', 'VehiculosFactory',
    'DetalleDespachosPuntosVehiculosFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, TercerosFactory, VehiculosFactory, DetalleDespachosPuntosVehiculosFactory) {

        $scope.Titulo = "CONSULTAR DESPACHOS FIDELIZACIÓN";
        $scope.MapaSitio = [{ Nombre: 'Fidelización' }, { Nombre: 'Documentos' }, { Nombre: 'Despachos Fidelización' }];
        //------------------------------------------------------------DECLARACION VARIABLES------------------------------------------------------------//
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.Deshabilitar = false;
        $scope.MensajesError = [];
        var filtros = {};
        $scope.pref = '';
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_DESPACHOS_FIDELIZACION); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Vehiculo: "",
            Tenedor: "",
            Conductor: ""
        }

        $scope.Codigo = CERO;

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'VENCIDO', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        //------------------------------------------------------------PAGINACION------------------------------------------------------------//
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        $scope.Imprimir = function () {
            Print();
        };
        $scope.ImprimirReporte = function (numero) {
            $scope.NumeroFactura = numero;
            PrintReporte();
        };
        //------------------------------------------------------------OBTENER INFORMACION------------------------------------------------------------//
        //--Informacion Tenedero
        $scope.ObtenerInformacionTenedor = function () {
            if ($scope.Modelo.Vehiculo.Tenedor !== undefined) {
                $scope.Modelo.Tenedor = $scope.Modelo.Vehiculo.Tenedor;
            }
            if ($scope.Modelo.Vehiculo.Conductor !== undefined) {
                $scope.Modelo.Conductor = $scope.Modelo.Vehiculo.Conductor;
            }
        }
        //------------------------------------------------------------FUNCIONES AUTOCOMPLETE------------------------------------------------------------//
        //--Vehiculos
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos);
                }
            }
            return $scope.ListadoVehiculos;
        }
        //--Conductores
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores);
                }
            }
            return $scope.ListadoConductores;
        }
        //--Tenedores
        $scope.ListadoTenedor = [];
        $scope.AutocompleteTenedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_TENEDOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoTenedor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTenedor);
                }
            }
            return $scope.ListadoTenedor;
        }
        //------------------------------------------------------------FUNCIONES GENERALES------------------------------------------------------------//
        //--Nuevo Registro
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarDespachosFidelizacion';
            }
        };
        //--Buscar
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                if (DatosRequeridos()) {
                    $scope.Codigo = 0;
                    Find();
                }
            }
        };
        function Find() {
            $scope.ListaFidelizacionDespachos = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            var CodigoVehiculo = $scope.Modelo.Vehiculo.Codigo == undefined ? 0 : $scope.Modelo.Vehiculo.Codigo;
            var CodigoTenedor = $scope.Modelo.Tenedor.Codigo == undefined ? 0 : $scope.Modelo.Tenedor.Codigo;
            var CodigoConductor = $scope.Modelo.Conductor.Codigo == undefined ? 0 : $scope.Modelo.Conductor.Codigo;

            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Vehiculo: { Codigo: CodigoVehiculo },
                Tenedor: { Codigo: CodigoTenedor },
                Conductor: { Codigo: CodigoConductor },
                Planilla: { NumeroDocumento: $scope.Modelo.Planilla },
                Manifiesto: { NumeroDocumento: $scope.Modelo.Manifiesto },
                Estado: $scope.Modelo.Estado.Codigo,
                FechaInicioVigenciaDesde: $scope.Modelo.IniVigeDesde,
                FechaInicioVigenciaHasta: $scope.Modelo.IniVigeHasta,
                FechaFinVigenciaDesde: $scope.Modelo.FinVigeDesde,
                FechaFinVigenciaHasta: $scope.Modelo.FinVigeHasta,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
            };

            blockUI.delay = 1000;
            DetalleDespachosPuntosVehiculosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaFidelizacionDespachos = response.data.Datos;
                            /*----------------------------*/
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if (($scope.Modelo.IniVigeDesde === null || $scope.Modelo.IniVigeDesde === undefined || $scope.Modelo.IniVigeDesde === '') &&
                ($scope.Modelo.IniVigeHasta === null || $scope.Modelo.IniVigeHasta === undefined || $scope.Modelo.IniVigeHasta === '') &&
                ($scope.Modelo.FinVigeDesde === null || $scope.Modelo.FinVigeDesde === undefined || $scope.Modelo.FinVigeDesde === '') &&
                ($scope.Modelo.FinVigeHasta === null || $scope.Modelo.FinVigeHasta === undefined || $scope.Modelo.FinVigeHasta === '') &&
                ($scope.Modelo.Vehiculo === null || $scope.Modelo.Vehiculo === undefined || $scope.Modelo.Vehiculo === '') &&
                ($scope.Modelo.Tenedor === null || $scope.Modelo.Tenedor === undefined || $scope.Modelo.Tenedor === '') &&
                ($scope.Modelo.Conductor === null || $scope.Modelo.Conductor === undefined || $scope.Modelo.Conductor === '')) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas, vehículo, tenedor o conductor');
                continuar = false;
            }

            if (($scope.Modelo.IniVigeDesde !== null && $scope.Modelo.IniVigeDesde !== undefined && $scope.Modelo.IniVigeDesde !== '') &&
                ($scope.Modelo.IniVigeHasta !== null && $scope.Modelo.IniVigeHasta !== undefined && $scope.Modelo.IniVigeHasta !== '')) {
                if ($scope.Modelo.IniVigeHasta < $scope.Modelo.IniVigeDesde) {
                    $scope.MensajesError.push('Hasta debe ser posterior a Desde en Fecha Inicio Vigencia');
                    continuar = false;
                }
                else if ((($scope.Modelo.IniVigeHasta - $scope.Modelo.IniVigeDesde) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                    $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias en Fecha Inicio Vigencia');
                    continuar = false
                }
            }

            if (($scope.Modelo.FinVigeDesde !== null && $scope.Modelo.FinVigeDesde !== undefined && $scope.Modelo.FinVigeDesde !== '') &&
                ($scope.Modelo.FinVigeHasta !== null && $scope.Modelo.FinVigeHasta !== undefined && $scope.Modelo.FinVigeHasta !== '')) {
                if ($scope.Modelo.FinVigeHasta < $scope.Modelo.FinVigeDesde) {
                    $scope.MensajesError.push('Hasta debe ser posterior a Desde en Fecha Fin Vigencia');
                    continuar = false;
                }
                else if ((($scope.Modelo.FinVigeHasta - $scope.Modelo.FinVigeDesde) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                    $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias en Fecha Fin Vigencia');
                    continuar = false
                }
            }
            /*else {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                    $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial;
                } else {
                    $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal;
                }
            }*/
            return continuar;
        }

        //------------------------------------------------------------PARAMETROS------------------------------------------------------------//
        if ($routeParams !== undefined && $routeParams.Codigo !== null) {
            $scope.ListaFidelizacionDespachos = [];
            if (parseInt($routeParams.Codigo) > CERO || parseInt($routeParams.Numero) > CERO) {
                $scope.Codigo = parseInt($routeParams.Codigo);
                $scope.Vehiculo = parseInt($routeParams.Numero);
                //-- Filtro de busqueda en Get 
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Codigo,
                    Vehiculo: { Codigo: $scope.Vehiculo },
                    Estado: $scope.Modelo.Estado.Codigo,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina
                }
                DetalleDespachosPuntosVehiculosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaFidelizacionDespachos = response.data.Datos;
                                /*----------------------------*/
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                //-- Filtro de busqueda en Get 
            }
        }
        //------------------------------------------------------------ANULACION------------------------------------------------------------//
        //--Validacion boton anular
        $scope.ConfirmarAnularDespachoFidelizacion = function (Codigo, Vehiculo) {
            $scope.CodigoAnulacion = Codigo;
            $scope.CodigoVehiculoAnulacion = Vehiculo.Codigo;
            $scope.PlacaVehiculoAnulacion = Vehiculo.Placa;
            $scope.CausaAnulacion = "";
            $scope.MensajesErrorAnula = [];
            showModal('modalConfirmacionAnularDespachos');
        };
        //--Funcion que solicita los datos de la anulación
        $scope.SolicitarDatosAnulacion = function () {
            $scope.FechaAnulacion = new Date();
            closeModal('modalConfirmacionAnularDespachos');
            showModal('modalDatosAnularDespachos');
        };
        //--Funcion Anulacion
        $scope.Anular = function () {
            if (DatosRequeridosAnulacion()) {
                var entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.CodigoAnulacion,
                    Vehiculo: { Codigo: $scope.CodigoVehiculoAnulacion },
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnulacion: $scope.CausaAnulacion,
                };

                DetalleDespachosPuntosVehiculosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.Numero = $scope.NumeroDocumentoAnulacion
                            ShowSuccess('Se anuló el despacho del vehículo ' + $scope.PlacaVehiculoAnulacion);
                            closeModal('modalDatosAnularDespachos');
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        //--Datos Requeridos Anulacion
        function DatosRequeridosAnulacion() {
            $scope.MensajesErrorAnula = [];
            var continuar = true;
            if ($scope.CausaAnulacion == "") {
                $scope.MensajesErrorAnula.push('se debe ingresar causa anulación');
                continuar = false;
            }
            return continuar;
        }
    }]);