﻿Imports Newtonsoft.Json

Namespace Basico.Facturacion
    <JsonObject>
    Public NotInheritable Class ConceptoNotasFacturas
        Inherits BaseDocumento

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            Nombre = Read(lector, "Nombre")
            ConceptoSistema = Read(lector, "Concepto_Sistema")

            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property ConceptoSistema As Integer
    End Class
End Namespace

