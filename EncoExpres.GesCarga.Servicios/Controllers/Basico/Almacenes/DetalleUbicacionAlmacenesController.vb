﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Negocio.Basico.Almacen

''' <summary>
''' Controlador <see cref="DetalleUbicacionAlmacenesController"/>
''' </summary>
<Authorize>
Public Class DetalleUbicacionAlmacenesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleUbicacionAlmacenes) As Respuesta(Of IEnumerable(Of DetalleUbicacionAlmacenes))
        Return New LogicaDetalleUbicacionAlmacenes(CapaPersistenciaDetalleUbicacionAlmacenes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleUbicacionAlmacenes) As Respuesta(Of DetalleUbicacionAlmacenes)
        Return New LogicaDetalleUbicacionAlmacenes(CapaPersistenciaDetalleUbicacionAlmacenes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleUbicacionAlmacenes) As Respuesta(Of Long)
        Return New LogicaDetalleUbicacionAlmacenes(CapaPersistenciaDetalleUbicacionAlmacenes).Guardar(entidad)
    End Function

End Class
