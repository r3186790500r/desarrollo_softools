﻿'Imports System.Data
'Imports System.Data.SqlClient

'Namespace Entidades.ReporteMinisterio
'    Public Class Oficina

'#Region "Declaracion Variables"

'        Dim strSQL As String
'        Dim objGeneral As General
'        Public strError As String
'        Public bolModificar As Boolean

'        Private intCodigo As Integer
'        Private strNombre As String
'        Private objCiudad As Ciudad
'        Private objEmpresa As Empresas
'        Private strDireccion As String
'        Private strTelefono1 As String
'        Private strTelefono2 As String
'        Private strCelular As String
'        Private strEmail As String
'        Private strFax As String
'        Private strResolucionFacturacionDian As String
'        Private intFueraDeLinea As Integer
'        Private intPrincipal As Integer
'        Private intFotoMercancia As Integer
'        Private intEstado As Integer
'        Private strPrefijo As String
'        Private strUsuario As String

'#End Region

'#Region "Declaracion Constantes"

'        Public Const OFICINA_SUCURSAL As Byte = 0
'        Public Const EXIGE_FOTO_MANIFIESTO As Byte = 1
'        Public Const TODAS_LAS_OFICINAS As Byte = 0
'        Public Const ESTADO_OFICINA_ACTIVO As Byte = 1
'        Public Const ESTADO_OFICINA_INACTIVO As Byte = 0
'        Public Const CODIGO_OFICINA_PRINCIPAL As Byte = 1

'#End Region

'#Region "Constructor"

'        Sub New()
'            Me.objEmpresa = New Empresas(0)
'            Me.strUsuario = ""
'            Me.objGeneral = New General()
'            Me.intCodigo = 0
'            Me.strNombre = ""
'            Me.objCiudad = New Ciudad(Me.objEmpresa)
'            Me.strDireccion = ""

'            Me.strEmail = ""
'            Me.strTelefono1 = ""
'            Me.strTelefono2 = ""
'            Me.strResolucionFacturacionDian = ""
'            Me.intFueraDeLinea = 0
'            Me.intPrincipal = 0

'            Me.intFotoMercancia = 0
'            Me.intEstado = 0
'            Me.strPrefijo = ""
'        End Sub

'        Sub New(ByRef Empresa As Empresas)

'            Me.objEmpresa = Empresa
'            Me.strUsuario = ""
'            Me.objGeneral = New General()
'            Me.intCodigo = 0
'            Me.strNombre = ""
'            Me.objCiudad = New Ciudad(Me.objEmpresa)
'            Me.strDireccion = ""

'            Me.strEmail = ""
'            Me.strTelefono1 = ""
'            Me.strTelefono2 = ""
'            Me.strResolucionFacturacionDian = ""
'            Me.intFueraDeLinea = 0
'            Me.intPrincipal = 0

'            Me.intFotoMercancia = 0
'            Me.intEstado = 0
'            Me.strPrefijo = ""
'        End Sub

'        Sub New(ByRef Empresa As Empresas, ByVal Codigo As Integer)

'            Me.objEmpresa = Empresa
'            Me.strUsuario = ""
'            Me.objGeneral = New General()
'            Me.intCodigo = 0
'            Me.strNombre = ""
'            Me.objCiudad = New Ciudad(Me.objEmpresa)
'            Me.strDireccion = ""

'            Me.strEmail = ""
'            Me.strTelefono1 = ""
'            Me.strTelefono2 = ""
'            Me.strResolucionFacturacionDian = ""
'            Me.intFueraDeLinea = 0
'            Me.intPrincipal = 0

'            Me.intFotoMercancia = 0
'            Me.intEstado = 0
'            Me.strPrefijo = ""
'        End Sub


'#End Region

'#Region "Atributos"

'        Public Property Codigo() As Integer
'            Get
'                Return Me.intCodigo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCodigo = value
'            End Set
'        End Property

'        Public Property FueraDeLinea() As Integer
'            Get
'                Return Me.intFueraDeLinea
'            End Get
'            Set(ByVal value As Integer)
'                Me.intFueraDeLinea = value
'            End Set
'        End Property

'        Public Property Principal() As Integer
'            Get
'                Return Me.intPrincipal
'            End Get
'            Set(ByVal value As Integer)
'                Me.intPrincipal = Math.Abs(value)
'            End Set
'        End Property

'        Public Property Ciudad() As Ciudad
'            Get
'                Return Me.objCiudad
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudad = value
'            End Set
'        End Property

'        Public Property Celular() As String
'            Get
'                Return Me.strCelular
'            End Get
'            Set(ByVal value As String)
'                Me.strCelular = value
'            End Set
'        End Property

'        Public Property Direccion() As String
'            Get
'                Return Me.strDireccion
'            End Get
'            Set(ByVal value As String)
'                Me.strDireccion = value
'            End Set
'        End Property

'        Public Property Fax() As String
'            Get
'                Return Me.strFax
'            End Get
'            Set(ByVal value As String)
'                Me.strFax = value
'            End Set
'        End Property

'        Public Property Telefono1() As String
'            Get
'                Return Me.strTelefono1
'            End Get
'            Set(ByVal value As String)
'                Me.strTelefono1 = value
'            End Set
'        End Property

'        Public Property Telefono2() As String
'            Get
'                Return Me.strTelefono2
'            End Get
'            Set(ByVal value As String)
'                Me.strTelefono2 = value
'            End Set
'        End Property

'        Public Property Email() As String
'            Get
'                Return Me.strEmail
'            End Get
'            Set(ByVal value As String)
'                Me.strEmail = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        Public Property ResolucionFacturacionDian() As String
'            Get
'                Return Me.strResolucionFacturacionDian
'            End Get
'            Set(ByVal value As String)
'                Me.strResolucionFacturacionDian = value
'            End Set
'        End Property

'        Public Property FotoMercancia() As Integer
'            Get
'                Return Me.intFotoMercancia
'            End Get
'            Set(ByVal value As Integer)
'                Me.intFotoMercancia = Math.Abs(value)
'            End Set
'        End Property

'        Public Property Estado() As Integer
'            Get
'                Return Me.intEstado
'            End Get
'            Set(ByVal value As Integer)
'                Me.intEstado = value
'            End Set
'        End Property

'        Public Property Prefijo() As String
'            Get
'                Return Me.strPrefijo
'            End Get
'            Set(ByVal value As String)
'                Me.strPrefijo = value
'            End Set
'        End Property

'        Public Property Usuario() As String
'            Get
'                Return Me.strUsuario
'            End Get
'            Set(ByVal value As String)
'                Me.strUsuario = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean
'            Try

'                Dim sdrOficina As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Nombre, CIUD_Codigo, Direccion, Telefono1, Telefono2, Celular, Email,"
'                strSQL += " Fax, Fuera_Linea, Principal, Resolucion_DIAN, Foto_Mercancia, Estado, Prefijo FROM Oficinas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrOficina = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrOficina.Read() Then

'                    Me.strNombre = sdrOficina("Nombre").ToString()
'                    Me.objCiudad.Consultar(Me.objEmpresa, Val(sdrOficina("CIUD_Codigo").ToString()))
'                    Me.strDireccion = sdrOficina("Direccion").ToString()
'                    Me.strTelefono1 = sdrOficina("Telefono1").ToString()
'                    Me.strTelefono2 = sdrOficina("Telefono2").ToString()

'                    Me.strCelular = sdrOficina("Celular").ToString()
'                    Me.strEmail = sdrOficina("Email").ToString()
'                    Me.strFax = sdrOficina("Fax").ToString()
'                    Me.intFueraDeLinea = Val(sdrOficina("Fuera_Linea").ToString())
'                    Me.intPrincipal = Val(sdrOficina("Principal").ToString())

'                    Me.strResolucionFacturacionDian = sdrOficina("Resolucion_DIAN").ToString()
'                    Me.intFotoMercancia = Val(sdrOficina("Foto_Mercancia").ToString())
'                    Me.intEstado = Val(sdrOficina("Estado").ToString())
'                    Me.strPrefijo = sdrOficina("Prefijo").ToString()

'                    Consultar = True

'                Else
'                    Me.intCodigo = 0
'                    Consultar = False
'                End If

'                sdrOficina.Close()
'                sdrOficina.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = General.CERO
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean
'            Try
'                Dim sdrOficina As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                Me.objEmpresa = Empresa
'                Me.intCodigo = Codigo

'                strSQL = "SELECT EMPR_Codigo, Nombre, CIUD_Codigo, Direccion, Telefono1, Telefono2, Celular, Email,"
'                strSQL += " Fax, Fuera_Linea, Principal, Resolucion_DIAN, Foto_Mercancia, Estado, Prefijo FROM Oficinas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrOficina = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrOficina.Read() Then

'                    Me.strNombre = sdrOficina("Nombre").ToString()
'                    Me.objCiudad.Datos_Existe_Registro(Me.objEmpresa, Val(sdrOficina("CIUD_Codigo").ToString()))
'                    Me.strDireccion = sdrOficina("Direccion").ToString()
'                    Me.strTelefono1 = sdrOficina("Telefono1").ToString()
'                    Me.strTelefono2 = sdrOficina("Telefono2").ToString()

'                    Me.strCelular = sdrOficina("Celular").ToString()
'                    Me.strEmail = sdrOficina("Email").ToString()
'                    Me.strFax = sdrOficina("Fax").ToString()
'                    Me.intFueraDeLinea = Val(sdrOficina("Fuera_Linea").ToString())
'                    Me.intPrincipal = Val(sdrOficina("Principal").ToString())

'                    Me.strResolucionFacturacionDian = sdrOficina("Resolucion_DIAN").ToString()
'                    Me.intFotoMercancia = Val(sdrOficina("Foto_Mercancia").ToString())
'                    Me.intEstado = Val(sdrOficina("Estado").ToString())
'                    Me.strPrefijo = sdrOficina("Prefijo").ToString()
'                    Consultar = True
'                Else
'                    Me.intCodigo = 0
'                    Consultar = False
'                End If

'                sdrOficina.Close()
'                sdrOficina.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro() As Boolean
'            Try
'                Dim sdrOficina As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo, Nombre FROM Oficinas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrOficina = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrOficina.Read() Then
'                    Existe_Registro = True
'                    Me.intCodigo = sdrOficina("Codigo").ToString()
'                    Me.strNombre = sdrOficina("Nombre").ToString()
'                Else
'                    Me.intCodigo = 0
'                    Existe_Registro = False
'                End If
'                sdrOficina.Close()
'                sdrOficina.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = General.CERO
'                Existe_Registro = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.intCodigo = Codigo

'                Dim sdrOficina As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Nombre, CIUD_Codigo, Principal FROM Oficinas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrOficina = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrOficina.Read() Then
'                    If Me.strNombre = "" Then
'                        Me.strNombre = sdrOficina("Nombre").ToString()
'                    End If
'                    If Me.intPrincipal = General.CERO Then
'                        Me.intPrincipal = Val(sdrOficina("Principal").ToString())
'                    End If
'                    Existe_Registro = True
'                Else
'                    Me.intCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrOficina.Close()
'                sdrOficina.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Existe_Registro = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                If Datos_Requeridos(Mensaje) Then
'                    If Not bolModificar Then
'                        Guardar = Insertar_SQL()
'                    Else
'                        Guardar = Modificar_SQL()
'                    End If
'                Else
'                    Guardar = False
'                End If
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Guardar = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Mensaje = Me.strError
'            End Try
'        End Function


'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Eliminar = Borrar_SQL()
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Eliminar = False
'                Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'#End Region

'#Region "Funciones Privadas"

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Dim intCont As Short
'                Me.strError = ""
'                intCont = 0
'                Dim intCodigoAuxiliar As Integer = intCodigo

'                Datos_Requeridos = True

'                If Me.intCodigo = 0 Then
'                    intCont += 1
'                    Me.strError = intCont & ". Debe ingresar el código de la oficina"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.intCodigo <> 0 And Existe_Registro(Me.objEmpresa, Me.intCodigo) Then

'                    If Me.bolModificar = False Then
'                        intCont += 1
'                        strError += intCont & ". El código digitado para la oficina ya se encuentra registrado"
'                        strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                Else
'                    Me.intCodigo = intCodigoAuxiliar
'                End If

'                If Len(Trim(Me.strNombre)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el nombre de la oficina"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strDireccion)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar la dirección de la oficina"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strTelefono1)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el teléfono principal de la oficina"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strEmail)) > 0 Then
'                    If Not Me.objGeneral.Email_Valido(Me.strEmail) Then
'                        intCont += 1
'                        Me.strError += intCont & ". El Email ingresado no es válido"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If
'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Datos_Requeridos = False
'            End Try

'        End Function

'        Private Function Insertar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_oficinas", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_CIUD_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_CIUD_Codigo").Value = Me.objCiudad.Codigo
'                ComandoSQL.Parameters.Add("@par_Direccion", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Direccion").Value = Me.strDireccion

'                ComandoSQL.Parameters.Add("@par_Telefono1", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Telefono1").Value = Me.strTelefono1
'                ComandoSQL.Parameters.Add("@par_Telefono2", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Telefono2").Value = Me.strTelefono2
'                ComandoSQL.Parameters.Add("@par_Fax", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Fax").Value = Me.strFax
'                ComandoSQL.Parameters.Add("@par_Celular", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Celular").Value = Me.strCelular
'                ComandoSQL.Parameters.Add("@par_Email", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Email").Value = Me.strEmail

'                ComandoSQL.Parameters.Add("@par_Fuera_Linea", SqlDbType.Int) : ComandoSQL.Parameters("@par_Fuera_Linea").Value = Me.intFueraDeLinea
'                ComandoSQL.Parameters.Add("@par_Principal", SqlDbType.Int) : ComandoSQL.Parameters("@par_Principal").Value = Me.intPrincipal
'                ComandoSQL.Parameters.Add("@par_Resolucion_DIAN", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Resolucion_DIAN").Value = Me.strResolucionFacturacionDian
'                ComandoSQL.Parameters.Add("@par_Foto_Mercancia", SqlDbType.Int) : ComandoSQL.Parameters("@par_Foto_Mercancia").Value = Me.intFotoMercancia
'                ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Crea").Value = Me.strUsuario
'                ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Estado").Value = Me.intEstado

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                Else
'                    Insertar_SQL = False
'                End If

'            Catch ex As Exception
'                Insertar_SQL = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function


'        Private Function Modificar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_oficinas", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_CIUD_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_CIUD_Codigo").Value = Me.objCiudad.Codigo
'                ComandoSQL.Parameters.Add("@par_Direccion", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Direccion").Value = Me.strDireccion

'                ComandoSQL.Parameters.Add("@par_Telefono1", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Telefono1").Value = Me.strTelefono1
'                ComandoSQL.Parameters.Add("@par_Telefono2", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Telefono2").Value = Me.strTelefono2
'                ComandoSQL.Parameters.Add("@par_Fax", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Fax").Value = Me.strFax
'                ComandoSQL.Parameters.Add("@par_Celular", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Celular").Value = Me.strCelular
'                ComandoSQL.Parameters.Add("@par_Email", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Email").Value = Me.strEmail

'                ComandoSQL.Parameters.Add("@par_Fuera_Linea", SqlDbType.Int) : ComandoSQL.Parameters("@par_Fuera_Linea").Value = Me.intFueraDeLinea
'                ComandoSQL.Parameters.Add("@par_Principal", SqlDbType.Int) : ComandoSQL.Parameters("@par_Principal").Value = Me.intPrincipal
'                ComandoSQL.Parameters.Add("@par_Resolucion_DIAN", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Resolucion_DIAN").Value = Me.strResolucionFacturacionDian
'                ComandoSQL.Parameters.Add("@par_Foto_Mercancia", SqlDbType.Int) : ComandoSQL.Parameters("@par_Foto_Mercancia").Value = Me.intFotoMercancia
'                ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Modifica").Value = Me.strUsuario
'                ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Estado").Value = Me.intEstado

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'            Catch ex As Exception
'                Modificar_SQL = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Private Function Borrar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_oficinas", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If

'            Catch ex As Exception
'                Borrar_SQL = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'#End Region
'    End Class

'End Namespace
