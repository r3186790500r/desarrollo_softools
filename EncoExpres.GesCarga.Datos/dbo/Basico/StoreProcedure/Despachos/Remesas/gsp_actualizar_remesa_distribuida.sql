﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 07/12/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_actualizar_remesa_distribuida'
GO
DROP PROCEDURE gsp_actualizar_remesa_distribuida
GO
CREATE PROCEDURE gsp_actualizar_remesa_distribuida 
(      
@par_EMPR_Codigo SMALLINT,      
@par_Numero NUMERIC        
      
)      
AS          
 BEGIN        
  UPDATE Encabezado_Remesas SET Distribuido = 1    

  WHERE EMPR_Codigo = @par_EMPR_Codigo      
  AND Numero = @par_Numero      
    
  SELECT @@ROWCOUNT AS Distribuido
        
END      
GO
