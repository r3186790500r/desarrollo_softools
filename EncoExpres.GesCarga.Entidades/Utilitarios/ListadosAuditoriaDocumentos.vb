﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Utilitarios

    ''' <summary>
    ''' Clase <see cref="ListadosAuditoriaDocumentos"></see>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ListadosAuditoriaDocumentos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ListadosAuditoriaDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ListadosAuditoriaDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto DataReader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            UsuarioCrea = New Usuarios With {.Nombre = Read(lector, "Usuario_Crea")}
            UsuarioModifica = New Usuarios With {.Nombre = Read(lector, "Usuario_Modifica")}
            UsuarioAnula = New Usuarios With {.Nombre = Read(lector, "Usuario_Anula")}
            FechaUsuarioCrea = Read(lector, "Fecha_Crea")
            Try
                FechaUsuarioModifica = Read(lector, "Fecha_Modifica")
            Catch

            End Try
            FechaUsuarioAnula = Read(lector, "Fecha_Anula")
            CausaAnula = Read(lector, "Causa_Anulacion")
            NombreEstado = Read(lector, "Nombre_Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
            FechaDocumento = Read(lector, "FechaDocumento")
            UrlConsulta = Read(lector, "UrlConsulta")
        End Sub

        <JsonProperty>
        Public Property NumeroInicio As Integer
        <JsonProperty>
        Public Property NumeroFin As Integer
        <JsonProperty>
        Public Property TipoConsulta As Integer
        <JsonProperty>
        Public Property FechaInicio As DateTime
        <JsonProperty>
        Public Property FechaFin As DateTime
        <JsonProperty>
        Public Property FechaUsuarioCrea As DateTime
        <JsonProperty>
        Public Property FechaUsuarioModifica As DateTime
        <JsonProperty>
        Public Property FechaUsuarioAnula As DateTime
        <JsonProperty>
        Public Property CausaAnula As String
        <JsonProperty>
        Public Property NombreUsuario As String
        <JsonProperty>
        Public Property NombreEstado As String
        <JsonProperty>
        Public Property FechaDocumento As DateTime
        <JsonProperty>
        Public Property UrlConsulta As String

    End Class

End Namespace