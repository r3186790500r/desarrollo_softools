﻿PRINT 'gsp_obtener_perfiles'
GO
DROP PROCEDURE gsp_obtener_perfiles
GO
CREATE PROCEDURE gsp_obtener_perfiles  
(  
@par_EMPR_Codigo smallint,  
@par_Codigo Numeric  = null,
@par_Identificacion VARCHAR(20) = NULL
)  
AS   
BEGIN  
SELECT   
PETE.EMPR_Codigo,  
PETE.Codigo  
FROM Perfil_Terceros  AS PETE LEFT JOIN Terceros AS TERC ON 
TERC.EMPR_Codigo = PETE.EMPR_Codigo
AND TERC.Codigo = PETE.TERC_Codigo
WHERE PETE.EMPR_Codigo = @par_EMPR_Codigo  
AND TERC.Codigo = isnull(@par_Codigo  ,TERC.Codigo)
AND TERC.Numero_Identificacion =  isnull(@par_Identificacion  ,TERC.Numero_Identificacion)
END  
GO