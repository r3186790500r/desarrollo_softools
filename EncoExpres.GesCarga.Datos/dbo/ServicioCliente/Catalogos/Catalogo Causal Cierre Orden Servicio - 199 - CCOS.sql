﻿PRINT 'Catalogo Causal Cierre Orden Servicio - 199 - CCOS'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 199
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 199
GO
DELETE Catalogos WHERE Codigo = 199
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos (EMPR_Codigo, Codigo, Nombre_Corto, Nombre, Actualizable, Numero_Columnas, Numero_Campos_Llave)
SELECT Codigo, 199, 'CCOS','Catalogo Causal Cierre Orden Servicio', 0, 1, 1 FROM Empresas
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 199, 19901, 'Incumplimiento', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 199, 19902, 'Solicitud de Cliente', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 199, 19903, 'Orden Cumplida', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 199, 19904, 'Error', '', '', '', 1, 1, GETDATE() FROM Empresas
GO