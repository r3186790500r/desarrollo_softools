USE [GESCARGA50_DESA]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Encabezado_Remesas]
DROP COLUMN if exists [Nombre_Remitente];
GO

ALTER TABLE [dbo].[Encabezado_Remesas]
ADD [Nombre_Remitente] varchar(150);
GO


ALTER TABLE [dbo].[Encabezado_Remesas]
DROP COLUMN if exists [Email_Remitente];
GO

ALTER TABLE [dbo].[Encabezado_Remesas]
ADD [Email_Remitente] varchar(100);
GO


ALTER TABLE [dbo].[Encabezado_Remesas]
DROP COLUMN if exists [Nombre_Destinatario];
GO

ALTER TABLE [dbo].[Encabezado_Remesas]
ADD [Nombre_Destinatario] varchar(150);
GO


ALTER TABLE [dbo].[Encabezado_Remesas]
DROP COLUMN if exists [Email_Destinatario];
GO

ALTER TABLE [dbo].[Encabezado_Remesas]
ADD [Email_Destinatario] varchar(100);
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_encabezado_remesas]    
(                                                          
 @par_EMPR_Codigo     smallint,                                                          
 @par_TIDO_Codigo     numeric,                                                          
 @par_CATA_TIRE_Codigo    int = NULL,        
                                                  
 @par_Documento_Cliente    varchar(30),                                                          
 @par_Fecha_Documento_Cliente  date,                                                          
                                                        
 @par_Fecha       date,                                                          
 @par_RUTA_Codigo     numeric,                                                          
 @par_PRTR_Codigo     numeric,                                                          
 @par_CATA_FOPR_Codigo    numeric,                                                          
 @par_TERC_Codigo_Cliente   numeric = null,                                                          
                                                        
 @par_TERC_Codigo_Remitente   numeric = null,                                                          
 @par_CIUD_Codigo_Remitente   numeric,                                                          
 @par_Direccion_Remitente   varchar(150)  = NULL,                                                          
 @par_Telefonos_Remitente   varchar(100)  = NULL,                                                          
 @par_Observaciones     varchar(100),                                                          
                                                        
 @par_Cantidad_Cliente    numeric (18, 2),                                                          
 @par_Peso_Cliente     numeric (18, 2),                                                          
 @par_Peso_Volumetrico_Cliente  numeric (18, 2),                                                          
 @par_DTCV_Codigo numeric = NULL,                                                        
 @par_Valor_Flete_Cliente   money,                                                          
 @par_Valor_Manejo_Cliente   money,                                                          
                                                        
 @par_Valor_Seguro_Cliente   money,                                                          
 @par_Valor_Descuento_Cliente  money,                                                          
 @par_Total_Flete_Cliente   money,                                                          
 @par_Valor_Comercial_Cliente  money,                                                          
 @par_Cantidad_Transportador   numeric (18, 2),                                                          
                                                        
 @par_Peso_Transportador    numeric (18, 2),                                                          
 @par_Valor_Flete_Transportador  money,                                                          
 @par_Valor_Reexpedicion  money =  NULL,                                                          
 @par_Total_Flete_Transportador  money,                                                          
 @par_TERC_Codigo_Destinatario  numeric,                                                          
 @par_CIUD_Codigo_Destinatario  numeric,                                                          
                                                        
 @par_Direccion_Destinatario   varchar(150)  = NULL,                                                          
 @par_Telefonos_Destinatario   varchar(100)  = NULL,                                                          
 @par_Estado       smallint,                                                          
 @par_USUA_Codigo_Crea    smallint,                                                          
 @par_OFIC_Codigo     smallint,    
                                                        
 @par_Numeracion      varchar(50) = NULL,                                                          
 @par_ETCC_Numero     numeric = NULL,                                                          
 @par_ETCV_Numero     numeric = NULL,                                                          
 @par_ESOS_Numero     numeric = NULL,                                                          
 @par_VEHI_Codigo     numeric = NULL,                                               
                                                        
 @par_TERC_Codigo_Conductor   numeric = NULL,                                                        
 @par_ENOC_Numero    numeric = NULL,                                                        
 @par_SEMI_Codigo    numeric = NULL,                    
                                                        
 @par_Barrio_Remitente   varchar(50) = NULL,                                
 @par_Codigo_Postal_Remitente varchar(50) = NULL,                                                        
 @par_Barrio_Destinatario varchar(50) = NULL,                                                        
 @par_Numero_Contenedor varchar(50) = NULL,                                                       
 @par_MBL_Contenedor varchar(50) = NULL,                                                        
 @par_HBL_Contenedor varchar(50) = NULL,                                                        
 @par_DO_Contenedor varchar(50) = NULL,                                                        
 @par_Valor_FOB MONEY = NULL,                                                        
 @par_Codigo_Postal_Destinatario varchar(50) = NULL,                                        
 @par_ID_Solicitud NUMERIC = NULL,                                      
 @par_CIUD_Codigo_Devolucion_Contenedor numeric= NULL,                                      
 @par_Remesa_Cortesia SMALLINT = NULL,                                      
 @par_Patio_Devolcuion varchar(100)= NULL,                                      
 @par_Fecha_Devolcuion date= NULL,                            
 @par_Permiso_Invias VARCHAR(50) = NULL       ,                        
                        
 /*Modificación AE 30/05/2020*/                        
 @par_Fecha_Devolucion_Contenedor date = NULL,                        
 @par_SICD_Devolucion_Contenedor numeric = NULL,                      
 /*Fin Modificación*/                        
 @par_Comision_Oficina Money = NULL,                      
 @par_Comision_Agencista Money = NULL,              
 @par_Valor_Otros Money = NULL,              
 @par_Valor_Cargue Money = NULL,              
 @par_Valor_Descargue Money = NULL   ,          
 @par_Valor_Declarado Money = Null  ,
 @par_TERC_Codigo_Facturar_A NUMERIC(18,0) = NULL,

  /*Modificación FP 2021/12/12*/                        
 @par_Nombre_Remitente varchar(150)  = NULL,
 @par_Email_Remitente varchar(100)  = NULL,
 @par_Nombre_Destinatario varchar(150)  = NULL,
 @par_Email_Destinatario varchar(100)  = NULL
  /*Fin Modificación*/                        
)                                                          
AS                                                          
BEGIN            
 Declare @RemesaNumeracion NUMERIC = 0            
 --Valida si el vehículo no tiene cumplidos pendientes por realizar                          
 IF (SELECT COUNT (*) FROM Encabezado_Planilla_Despachos            
 WHERE EMPR_CODIGO = @par_EMPR_Codigo                             
 AND VEHI_CODIGO = @par_VEHI_Codigo                             
 AND Anulado = 0                             
 AND (ECPD_Numero = 0 OR ECPD_Numero IS NULL))>0                             
 AND EXISTS (SELECT EMPR_Codigo FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = 3 AND Estado = 1)                                            
 BEGIN                                            
 SELECT -2 AS Numero, 0 AS Numero_Documento --El vehículo tiene cumplidos pendientes por realizar                                           
 END                                            
 ELSE                                            
 BEGIN            
 DECLARE @Continuar NUMERIC = 1            
 --Verifica la existencia del detalle de la orden                          
 IF @par_ID_Solicitud > 0            
 BEGIN             
 IF NOT EXISTS (SELECT * FROM Detalle_Despacho_Orden_Servicios WHERE  EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud)                                        
 BEGIN                                        
 SET @Continuar = 0                                        
 END                                        
 END            
            
 --Validacion Preimpreso Cootranstame            
 IF (EXISTS(SELECT PROC_Codigo FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = 41 AND Estado = 1)--Preimpreso obligatorio            
 AND @par_TIDO_Codigo = 110 -- Solo para remesas paqueteria            
 )            
 BEGIN            
 SELECT TOP 1 @RemesaNumeracion = Numero_Documento from Encabezado_Remesas              
 where EMPR_Codigo = @par_EMPR_Codigo              
 AND Numeracion = @par_Numeracion            
 AND Anulado = 0            
 IF @RemesaNumeracion > 0            
 BEGIN            
 SET @Continuar = 0            
 END            
 END            
 --Validacion Preimpreso Cootranstame            
            
            
 IF @Continuar > 0            
 BEGIN            
 IF @par_ID_Solicitud > 0            
 BEGIN            
 ---VALIDA Y ACTUALIZA LOS CUPOS Y SALDOS DE LA ORDEN DE SERVICIO PARA LOS PROCESOS DE CUPO TONELADA Y CANTIDAD VIAJES                          
 DECLARE @Peso_Detalle numeric                                         
 DECLARE @Peso_Pendiente numeric                                         
 DECLARE @Saldo numeric                                         
 DECLARE @TipoDespacho numeric                                         
 SELECT @Peso_Detalle = Peso FROM Detalle_Despacho_Orden_Servicios WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud                                        
 IF @par_Peso_Cliente > @Peso_Detalle or @par_Peso_Cliente < @Peso_Detalle            
 BEGIN            
 select @TipoDespacho = CATA_TDOS_Codigo from Encabezado_Solicitud_Orden_Servicios                             
 where EMPR_Codigo = @par_EMPR_Codigo AND Numero = (SELECT ESOS_Numero FROM Detalle_Despacho_Orden_Servicios WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud)                                        
                                        
 select @Saldo = (Saldo_Tipo_Despacho + @Peso_Detalle) - @par_Peso_Cliente                             
 from Encabezado_Solicitud_Orden_Servicios                             
 where EMPR_Codigo = @par_EMPR_Codigo AND Numero = (SELECT ESOS_Numero FROM Detalle_Despacho_Orden_Servicios WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud)                                      
                            
 select @Peso_Pendiente = sum(Cantidad_Despacho)                             
 from Detalle_Programacion_Orden_Servicios                             
 where EMPR_Codigo = @par_EMPR_Codigo                           
 AND EPOS_Numero = ( select EPOS_Numero from Detalle_Programacion_Orden_Servicios where EMPR_Codigo = @par_EMPR_Codigo AND id = (SELECT DPOS_ID FROM Detalle_Despacho_Orden_Servicios WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud))       
  
     
         
              
            
 AND ID NOT IN (SELECT DPOS_ID FROM Detalle_Despacho_Orden_Servicios WHERE EMPR_Codigo = @par_EMPR_Codigo AND ESOS_Numero = @par_ESOS_Numero)                            
 if (@Saldo-@Peso_Pendiente) < 0 and @TipoDespacho = 18202            
 begin                                        
 SET @Continuar = 0                                        
 end                                        
 else                                         
 begin                                        
 if @TipoDespacho = 18202            
 begin                                        
 update Encabezado_Solicitud_Orden_Servicios                             
 set Saldo_Tipo_Despacho = @Saldo                             
 where EMPR_Codigo = @par_EMPR_Codigo AND Numero = (SELECT ESOS_Numero FROM Detalle_Despacho_Orden_Servicios WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud)                           
 end                            
 --update Encabezado_Orden_Cargues                             
 --set Peso_Cliente = @par_Peso_Cliente, Cantidad_Cliente = @par_Cantidad_Cliente                              
 --where EMPR_Codigo = @par_EMPR_Codigo AND Numero = (SELECT ENOC_Numero FROM Detalle_Despacho_Orden_Servicios WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud)                                   
                               
 --update Detalle_Despacho_Orden_Servicios                             
 --set Peso = @par_Peso_Cliente, Cantidad = @par_Cantidad_Cliente, Flete_Cliente = @par_Valor_Flete_Cliente, Valor_Flete_Cliente = @par_Total_Flete_Cliente                              
 --WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud                                        
                               
 update Detalle_Programacion_Orden_Servicios                             
 set Cantidad_Despacho = @par_Peso_Cliente,Valor_Flete_Cliente = @par_Valor_Flete_Cliente                            
 where EMPR_Codigo = @par_EMPR_Codigo AND ID = (SELECT DPOS_ID FROM Detalle_Despacho_Orden_Servicios WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud)                                       
 end            
 END                                        
 END            
 IF @Continuar > 0            
 BEGIN                      
 --Actualiza Valores De detalle despacho y orden de cargue                      
 update Encabezado_Orden_Cargues                             
 set Peso_Cliente = @par_Peso_Cliente, Cantidad_Cliente = @par_Cantidad_Cliente                              
 where EMPR_Codigo = @par_EMPR_Codigo AND Numero = (SELECT ENOC_Numero FROM Detalle_Despacho_Orden_Servicios WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud)                                   
                               
 update Detalle_Despacho_Orden_Servicios                             
 set Peso = @par_Peso_Cliente, Cantidad = @par_Cantidad_Cliente, Flete_Cliente = @par_Valor_Flete_Cliente, Valor_Flete_Cliente = @par_Total_Flete_Cliente                              
 WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_ID_Solicitud                      
                      
 DECLARE @ENRE_Numero NUMERIC = 0                                                            
 DECLARE @numNumeroDocumento NUMERIC = 0                                 
 DECLARE @Valor_Comision_Oficina MONEY = 0                            
 DECLARE @Valor_Comision_Agencista MONEY = 0                              
                              
 EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, @par_OFIC_Codigo, @numNumeroDocumento OUTPUT                                             
 --CALCULA COMISIONES                      
 if(@par_Comision_Oficina IS NULL)            
 BEGIN                      
 SELECT @Valor_Comision_Oficina =( @par_Valor_Flete_Cliente*Porcentaje_Comision)/100,                              
 @Valor_Comision_Agencista =( @par_Valor_Flete_Cliente*Porcentaje_Comision_Agencista)/100                              
 FROM Oficinas WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_OFIC_Codigo                              
 END                      
 ELSE                      
 BEGIN                      
 SET @Valor_Comision_Oficina = @par_Comision_Oficina                      
 SET @Valor_Comision_Agencista = @par_Comision_Agencista                      
 END            

 DECLARE @FacturarAOrdenServicio NUMERIC(18,0)
 SET @FacturarAOrdenServicio = (SELECT TERC_Codigo_Facturar FROM Encabezado_Solicitud_Orden_Servicios WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ESOS_Numero)

 INSERT INTO Encabezado_Remesas (            
 EMPR_Codigo,                                                          
 TIDO_Codigo,                                                          
 Numero_Documento,                                                          
 CATA_TIRE_Codigo,                                                          
 Documento_Cliente,                                                          
 --5                                                          
                   
 Fecha_Documento_Cliente,                                                          
 Fecha,                                                   
 RUTA_Codigo,                                                          
 PRTR_Codigo,                                
 CATA_FOPR_Codigo,                                                 
 --10                                                          
                                                          
 TERC_Codigo_Cliente,                                                          
 TERC_Codigo_Remitente,                                                          
 CIUD_Codigo_Remitente,                                                          
 Direccion_Remitente,                                                          
 Telefonos_Remitente,                                                          
 --15                                                     
                                                          
 Observaciones,                                                          
 Cantidad_Cliente,                                                       
 Peso_Cliente,                                                          
 Peso_Volumetrico_Cliente,                                                          
 DTCV_Codigo,                        
 --20                                                          
               
 Valor_Flete_Cliente,                                                          
 Valor_Manejo_Cliente,                                       
 Valor_Seguro_Cliente,                                                          
 Valor_Descuento_Cliente,                                                          
 Total_Flete_Cliente,                                                          
 --25                                                          
 Valor_Comercial_Cliente,                                                          
 Cantidad_Transportador,                                                          
 Peso_Transportador,                                                          
 Valor_Flete_Transportador,                                                          
 Valor_Reexpedicion,                                                          
 Total_Flete_Transportador,                                                          
 --30                                                          
                                            
 TERC_Codigo_Destinatario,                                                          
 CIUD_Codigo_Destinatario,                                                          
 Direccion_Destinatario,                                                          
 Telefonos_Destinatario,                                                          
 Anulado,                                                          
 --35                                                          
                                                          
 Estado,                                                          
 Fecha_Crea,                                           
 USUA_Codigo_Crea,                                                          
 Fecha_Modifica,                                                          
 USUA_Codigo_Modifica,                                                          
 --40                                                          
                                                          
 Fecha_Anula,                                                          
 USUA_Codigo_Anula,                                                        
 Causa_Anula,                                        
 OFIC_Codigo,                                                          
 Numeracion,                                                          
 --45                                                          
                                                          
 ETCC_Numero,                                                          
 ETCV_Numero,                                   
 ESOS_Numero,                                                          
 ECPD_Numero,                                                          
 ENMC_Numero,                                        
 --50                                                          
                                      
 ENPD_Numero,                                                          
 ENFA_Numero,                                                          
 VEHI_Codigo,                                                          
 TERC_Codigo_Conductor ,                                                         
 --54                                                          
 ENOC_Numero,                                                        
 SEMI_Codigo,                                                        
 --56                            
                                                        
 Cumplido,                                                        
 ENPE_Numero,                                                          
 ENPR_Numero,                        
                                                        
 Barrio_Remitente,                                                        
 Codigo_Postal_Remitente,                                                        
 Barrio_Destinatario,                                                        
 Codigo_Postal_Destinatario  ,                                                      
                                                      
 Numero_Contenedor,                                                      
 MBL_Contenedor,                               
 HBL_Contenedor,                                                      
 DO_Contenedor,                                                      
 Valor_FOB,                                                    
 Distribucion,                                              
 TEDI_Codigo,                                      
 CIUD_Codigo_Devolucion_Contenedor,                                      
 Patio_Devolcuion,                                      
 Fecha_Devolcuion ,                                
 Remesa_Cortesia,                              
 Valor_Comision_Oficina ,                              
 Valor_Comision_Agencista,                            
 Permiso_Invias,                        
                        
 /*Modificación AE 30/05/2020*/                        
 SICD_Codigo_Devolucion_Contenedor,                        
 Fecha_Devolucion_Contenedor,              
 /*Fin Modificación*/              
 Valor_Otros,              
 Valor_Cargue,              
 Valor_Descargue,          
 Valor_Declarado  ,
 TERC_Codigo_Facturara,

   /*Modificación FP 2021/12/12*/                        
 Nombre_Remitente,
 Email_Remitente,
 Nombre_Destinatario,
 Email_Destinatario
  /*Fin Modificación*/
 )                                                          
                                                          
 VALUES (                                                          
 @par_EMPR_Codigo,                                                          
 @par_TIDO_Codigo,                                                          
 @numNumeroDocumento,                                          
 ISNULL(@par_CATA_TIRE_Codigo, 0),                                                        
 @par_Documento_Cliente,                                                          
 --5                                                          
                                                          
 @par_Fecha_Documento_Cliente,                                                          
 @par_Fecha,                                                          
 @par_RUTA_Codigo,                                                          
 @par_PRTR_Codigo,                                                          
 @par_CATA_FOPR_Codigo,                                                          
 --10                                                          
                                                          
 ISNULL(@par_TERC_Codigo_Cliente,0),                                                         
 ISNULL(@par_TERC_Codigo_Remitente,0),                                  
 @par_CIUD_Codigo_Remitente,                                                          
 ISNULL(@par_Direccion_Remitente,''),                                                  
 ISNULL(@par_Telefonos_Remitente,''),                                                          
 --15                         
                                                          
 @par_Observaciones,                                                          
 @par_Cantidad_Cliente,                                                          
 @par_Peso_Cliente,                                            
 @par_Peso_Volumetrico_Cliente,                                                          
 ISNULL(@par_DTCV_Codigo,0),                                                        
 --20                                              
                                                          
 @par_Valor_Flete_Cliente,                                                          
 @par_Valor_Manejo_Cliente,                                                   
 @par_Valor_Seguro_Cliente,                                                          
 @par_Valor_Descuento_Cliente,                                                          
 @par_Total_Flete_Cliente,                            
 --25                                            
                                                          
 @par_Valor_Comercial_Cliente,                                                          
 @par_Cantidad_Transportador,                                                          
 @par_Peso_Transportador,                                                          
 @par_Valor_Flete_Transportador,                               
 @par_Valor_Reexpedicion,                              
 @par_Total_Flete_Transportador,                                                      
 --30                                    
                  
 @par_TERC_Codigo_Destinatario,                                                          
 @par_CIUD_Codigo_Destinatario,                                                          
 ISNULL(@par_Direccion_Destinatario,''),                                                          
 ISNULL(@par_Telefonos_Destinatario,''),                                                          
 0,                                                          
 --35                                                          
                                                          
 @par_Estado,                                                          
 GETDATE(),                                                          
 @par_USUA_Codigo_Crea,                                                          
 NULL,                  
 NULL,                                                          
 --40                                                          
                                                          
 NULL,                                                          
 NULL,                                                          
 '',                                                          
 @par_OFIC_Codigo,                                                          
 ISNULL(@par_Numeracion, ''),                                                        
 --45                                                          
                                                          
 ISNULL(@par_ETCC_Numero,0),                                                        
 ISNULL(@par_ETCV_Numero,0),                                    
 ISNULL(@par_ESOS_Numero,0),                                                        
 0,                                               
 0,                                                          
 --50                                                          
                                                          
 0,                                                          
 0,                                                          
 ISNULL(@par_VEHI_Codigo,0),                                       
 ISNULL(@par_TERC_Codigo_Conductor,0),                                                        
 --54                                                          
 ISNULL(@par_ENOC_Numero,0),                                                        
 ISNULL(@par_SEMI_Codigo,01),                                                        
 0,                                                  
 0,                          
 0,                                                        
                                                  
 ISNULL(@par_Barrio_Remitente,''),                                                        
 ISNULL(@par_Codigo_Postal_Remitente,''),                                                        
 ISNULL(@par_Barrio_Destinatario,''),                                                        
 ISNULL(@par_Codigo_Postal_Destinatario,'') ,                                                      
                                                        
 @par_Numero_Contenedor,                                                        
 @par_MBL_Contenedor,                                                        
 @par_HBL_Contenedor,                                                        
 @par_DO_Contenedor,                                                        
 @par_Valor_FOB,                                                    
 1,                                              
 (SELECT TEDI_Codigo from Encabezado_Solicitud_Orden_Servicios where EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ESOS_Numero),                               
 @par_CIUD_Codigo_Devolucion_Contenedor,                                      
 @par_Patio_Devolcuion,                                      
 @par_Fecha_Devolcuion ,                                
 @par_Remesa_Cortesia ,                              
 @Valor_Comision_Oficina ,                              
 @Valor_Comision_Agencista,                            
 @par_Permiso_Invias     ,                        
                        
 /*Modificación AE 30/05/2020*/                        
 @par_SICD_Devolucion_Contenedor,                        
 @par_Fecha_Devolucion_Contenedor,              
 /*Fin Modificación*/                 
 @par_Valor_Otros,              
 @par_Valor_Cargue,              
 @par_Valor_Descargue ,          
 @par_Valor_Declarado  ,
 @par_TERC_Codigo_Facturar_A,

  /*Modificación FP 2021/12/12*/                        
 ISNULL(@par_Nombre_Remitente ,(SELECT ISNULL(Razon_Social+Nombre+' '+Apellido1+' '+Apellido2,'') FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Remitente)),
 ISNULL(@par_Email_Remitente ,(SELECT ISNULL(Correo_Facturacion, '') FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Remitente)),
 ISNULL(@par_Nombre_Destinatario ,(SELECT ISNULL(Razon_Social+Nombre+' '+Apellido1+' '+Apellido2,'') FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Destinatario)),
ISNULL(@par_Email_Destinatario ,(SELECT ISNULL(Correo_Facturacion, '') FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Destinatario))
/*Fin Modificación*/
 )                                                          
                                                  
 SELECT @ENRE_Numero = @@IDENTITY                                                    
                                                    
 IF @par_TIDO_Codigo = 100            
 BEGIN                            
 INSERT INTO Detalle_Distribucion_Remesas            
 (                                                    
 EMPR_Codigo,                                                    
 ENRE_Numero,                                                    
 PRTR_Codigo,                                                    
 UEPT_Codigo,                                                    
 Cantidad,                                                
 Peso,                                                    
 Documento_Cliente,                                                    
 Fecha_Documento_Cliente,                  
 TERC_Codigo_Destinatario,                                                    
 CATA_TIID_Codigo_Destinatario,                                                    
 Numero_Identificacion_Destinatario,                                                    
 Nombre_Destinatario,                                                    
 CIUD_Codigo_Destinatario,                                                    
 ZOCI_Codigo_Destinatario,                                                    
 Barrio_Destinatario,                                                    
 Direccion_Destinatario,                                                    
 Codigo_Postal_Destinatario,                                           
 Telefonos_Destinatario,                      
 Observaciones_Entrega,                                                    
 CATA_ESDR_Codigo,                                           
 Cantidad_Recibe,                                                    
 Peso_Recibe,                                                    
 Fecha_Crea,                                                    
 USUA_Codigo_Crea              
 )                                                    
 values                                                    
 (                                                    
 @par_EMPR_Codigo,                                                    
 @ENRE_Numero,                                                    
 @par_PRTR_Codigo,                                                    
 (SELECT UEPT_Codigo FROM Producto_Transportados WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_PRTR_Codigo),                                                    
 @par_Cantidad_Cliente,                                                    
 @par_Peso_Cliente,                                                    
 @par_Documento_Cliente,                                                    
 @par_Fecha_Documento_Cliente,                                      
 @par_TERC_Codigo_Destinatario,                                                    
 (SELECT CATA_TIID_Codigo FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Destinatario),                                                    
 (SELECT Numero_Identificacion FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Destinatario),                                                    
 ISNULL(@par_Nombre_Destinatario ,(SELECT ISNULL(Razon_Social+Nombre+' '+Apellido1+' '+Apellido2,'') FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Destinatario)),                                                    
 @par_CIUD_Codigo_Destinatario,                                                    
 0,                                                    
 @par_Barrio_Destinatario,                                                    
 @par_Direccion_Destinatario,                                                    
 @par_Codigo_Postal_Destinatario,                                                    
 @par_Telefonos_Destinatario,                                                    
 '',                                                    
 11401,               
 0,                                                    
 0,                                                    
 GETDATE(),                                                    
 @par_USUA_Codigo_Crea                
 )                                                    
 END                                                    
                                                  
 exec gsp_cerrar_despacho_turno_enturnamiento_despachos @par_EMPR_Codigo,@par_VEHI_Codigo                                              
                                                    
 SELECT Numero, Numero_Documento FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo                                                          
 AND Numero_Documento = @numNumeroDocumento                                             
 AND TIDO_Codigo = @par_TIDO_Codigo                                           
 END                                        
 ELSE                                        
 BEGIN                                         
 SELECT -4 AS Numero, 0 AS Numero_Documento --El peso de la remesa excede el cupo disponible de la orden                                          
 END                                        
 END                                        
 ELSE                                        
 BEGIN            
 IF @RemesaNumeracion > 0            
 BEGIN            
 SELECT -5 AS Numero, 0 AS Numero_Documento -- Ya Existe Preimpreso            
 END            
 ELSE            
 BEGIN            
 SELECT -3 AS Numero, 0 AS Numero_Documento -- No existe detalle de la orden            
 END      
 END                                        
 END                                                          
 END  
 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_modificar_encabezado_remesa]  
(                
 @par_EMPR_Codigo smallint,      
 @par_TIDO_Codigo numeric = NULL,                 
 @par_Numero NUMERIC,              
 @par_CATA_TIRE_Codigo int = NULL,                  
 @par_Documento_Cliente varchar(30),                  
 @par_Fecha_Documento_Cliente date,                  
 @par_Remesa_Cortesia SMALLINT = NULL,          
 /*5*/          
          
 @par_Fecha date,                  
 @par_RUTA_Codigo numeric,                  
 @par_PRTR_Codigo numeric,                  
 @par_CATA_FOPR_Codigo numeric,                  
 @par_TERC_Codigo_Cliente numeric = NULL,                  
 /*10*/          
                
 @par_TERC_Codigo_Remitente numeric  = NULL,                  
 @par_CIUD_Codigo_Remitente numeric,                  
 @par_Direccion_Remitente varchar(150),                  
 @par_Telefonos_Remitente varchar(100),                  
 @par_Observaciones varchar(100),                  
 /*15*/          
              
 @par_Cantidad_Cliente numeric (18, 2),                  
 @par_Peso_Cliente numeric (18, 2),                  
 @par_Peso_Volumetrico_Cliente numeric (18, 2),                  
 @par_DTCV_Codigo numeric = NULL,                
 @par_Valor_Flete_Cliente money,                  
 @par_Valor_Manejo_Cliente money,                  
 @par_Comision_Oficina Money = NULL,          
 @par_Comision_Agencista Money = NULL,          
 /*20*/          
                
 @par_Valor_Seguro_Cliente money,                  
 @par_Valor_Descuento_Cliente  money,                  
 @par_Total_Flete_Cliente   money,                  
 @par_Valor_Comercial_Cliente  money,                  
 @par_Cantidad_Transportador   numeric (18, 2),                  
 /*25*/              
           
 @par_Peso_Transportador    numeric (18, 2),                  
 @par_Valor_Flete_Transportador  money,                  
 @par_Valor_Reexpedicion  money =  NULL,          
 @par_Total_Flete_Transportador  money,                  
 @par_TERC_Codigo_Destinatario  numeric,                  
 @par_CIUD_Codigo_Destinatario  numeric,          
 /*30*/          
                
 @par_Direccion_Destinatario varchar(150),                  
 @par_Telefonos_Destinatario varchar(100),                  
 @par_Estado smallint,                  
 @par_USUA_Codigo_Crea smallint,                  
 @par_OFIC_Codigo smallint,                  
 /*35*/                
      
 @par_ETCC_Numero numeric = NULL,                  
 @par_ETCV_Numero numeric = NULL,                  
 @par_ESOS_Numero numeric = NULL,                  
 @par_TERC_Codigo_Conductor numeric = NULL,                
 @par_VEHI_Codigo numeric = NULL,                  
                
 @par_Barrio_Remitente   varchar(50) = NULL,                
 @par_Codigo_Postal_Remitente varchar(50) = NULL,                
 @par_Barrio_Destinatario varchar(50) = NULL,                
 @par_Codigo_Postal_Destinatario varchar(50) = NULL,                
 @par_Numeracion varchar(50) = NULL,  
 @par_Valor_Otros Money = NULL,  
 @par_Valor_Cargue Money = NULL,  
 @par_Valor_Descargue Money = NULL,
 
   /*Modificación FP 2021/12/12*/                        
 @par_Nombre_Remitente varchar(150)  = NULL,
 @par_Email_Remitente varchar(100)  = NULL,
 @par_Nombre_Destinatario varchar(150)  = NULL,
 @par_Email_Destinatario varchar(100)  = NULL
  /*Fin Modificación*/    
)                  
AS                  
BEGIN      
 Declare @RemesaNumeracion NUMERIC = 0      
 Declare @Continuar NUMERIC = 1      
 --Validacion Preimpreso Cootranstame      
 IF (EXISTS(SELECT PROC_Codigo FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = 41 AND Estado = 1)--Preimpreso obligatorio      
 AND @par_TIDO_Codigo = 110 -- Solo para remesas paqueteria      
 )      
 BEGIN      
  SELECT TOP 1 @RemesaNumeracion = Numero_Documento from Encabezado_Remesas        
  where EMPR_Codigo = @par_EMPR_Codigo        
  AND Numeracion = @par_Numeracion         
  AND Anulado = 0      
  AND (Numero <> @par_Numero or @par_Numero is null)      
  AND @par_Numeracion <> '0'


  IF @RemesaNumeracion > 0      
  BEGIN      
   SET @Continuar = 0  
  END      
 END      
 --Validacion Preimpreso Cootranstame      
 IF @Continuar = 1  
 BEGIN      
  UPDATE Encabezado_Remesas SET      
  CATA_TIRE_Codigo = ISNULL(@par_CATA_TIRE_Codigo, 0),               
  Documento_Cliente = @par_Documento_Cliente,              
  Fecha_Documento_Cliente = @par_Fecha_Documento_Cliente,              
  Remesa_Cortesia = @par_Remesa_Cortesia,          
          
  Fecha =  @par_Fecha,              
  RUTA_Codigo =  @par_RUTA_Codigo,              
  PRTR_Codigo =  @par_PRTR_Codigo,              
  CATA_FOPR_Codigo  = @par_CATA_FOPR_Codigo,              
  TERC_Codigo_Cliente  = ISNULL(@par_TERC_Codigo_Cliente, TERC_Codigo_Cliente),        
          
  TERC_Codigo_Remitente  = ISNULL(@par_TERC_Codigo_Remitente, TERC_Codigo_Remitente),        
  CIUD_Codigo_Remitente  = @par_CIUD_Codigo_Remitente,                  
  Direccion_Remitente  = @par_Direccion_Remitente,                  
  Telefonos_Remitente  = @par_Telefonos_Remitente,              
  Observaciones  = @par_Observaciones,            
          
  Cantidad_Cliente =  @par_Cantidad_Cliente,                  
  Peso_Cliente  = @par_Peso_Cliente,                  
  Peso_Volumetrico_Cliente  = @par_Peso_Volumetrico_Cliente,                  
  DTCV_Codigo  = ISNULL(@par_DTCV_Codigo,0),              
  Valor_Flete_Cliente  = @par_Valor_Flete_Cliente,                  
  Valor_Manejo_Cliente =  @par_Valor_Manejo_Cliente,                 
  Valor_Comision_Oficina = @par_Comision_Oficina,          
  Valor_Comision_Agencista = @par_Comision_Agencista,          
          
  Valor_Seguro_Cliente =  @par_Valor_Seguro_Cliente,                 
  Valor_Descuento_Cliente =  @par_Valor_Descuento_Cliente,                  
  Total_Flete_Cliente =  @par_Total_Flete_Cliente,              
  Valor_Comercial_Cliente =  @par_Valor_Comercial_Cliente,                  
  Cantidad_Transportador =  @par_Cantidad_Transportador,                  
          
  Peso_Transportador =  @par_Peso_Transportador,              
  Valor_Flete_Transportador =  @par_Valor_Flete_Transportador,                  
  Valor_Reexpedicion = @par_Valor_Reexpedicion,          
  Total_Flete_Transportador =  @par_Total_Flete_Transportador,              
  TERC_Codigo_Destinatario =  @par_TERC_Codigo_Destinatario,                  
  CIUD_Codigo_Destinatario =  @par_CIUD_Codigo_Destinatario,                  
          
  Direccion_Destinatario =  @par_Direccion_Destinatario,                  
  Telefonos_Destinatario =  @par_Telefonos_Destinatario,                        
  Estado =  @par_Estado,             
  Fecha_Modifica =  GETDATE(),                  
  USUA_Codigo_Modifica =  @par_USUA_Codigo_Crea,                  
  OFIC_Codigo =  @par_OFIC_Codigo,                  
             
  ETCC_Numero =  ISNULL(@par_ETCC_Numero,0),                  
  ETCV_Numero =  ISNULL(@par_ETCV_Numero,0),                  
  ESOS_Numero  =  ISNULL(@par_ESOS_Numero,0),                  
  TERC_Codigo_Conductor =  ISNULL(@par_TERC_Codigo_Conductor,0),                 
  VEHI_Codigo =   ISNULL(@par_VEHI_Codigo,0),                  
          
  --54                  
  /*Los siguientes campos no son utilizados en el repositorio*/          
  --ENOC_Numero  =  ISNULL(@par_ENOC_Numero,0),                
  --SEMI_Codigo  = ISNULL(@par_SEMI_Codigo,0),                
  --56                
                
  Barrio_Remitente =  ISNULL(@par_Barrio_Remitente,''),               
  Codigo_Postal_Remitente =     ISNULL(@par_Codigo_Postal_Remitente,''),              
  Barrio_Destinatario =    ISNULL(@par_Barrio_Destinatario,''),                
  Codigo_Postal_Destinatario  =   ISNULL(@par_Codigo_Postal_Destinatario,''),              
  Numeracion  = IIF(@par_Numeracion = '0', '',ISNULL(@par_Numeracion, '')),  
  Valor_Otros = @par_Valor_Otros,  
  Valor_Cargue = @par_Valor_Cargue,  
  Valor_Descargue = @par_Valor_Descargue,
  
     /*Modificación FP 2021/12/12*/                        
 Nombre_Remitente =  ISNULL(@par_Nombre_Remitente ,(SELECT ISNULL(Razon_Social+Nombre+' '+Apellido1+' '+Apellido2,'') FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Remitente)),
 Email_Remitente =  ISNULL(@par_Email_Remitente ,(SELECT ISNULL(Correo_Facturacion, '') FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Remitente)),
 Nombre_Destinatario =  ISNULL(@par_Nombre_Destinatario ,(SELECT ISNULL(Razon_Social+Nombre+' '+Apellido1+' '+Apellido2,'') FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Destinatario)),
 Email_Destinatario = ISNULL(@par_Email_Destinatario ,(SELECT ISNULL(Correo_Facturacion, '') FROM Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Destinatario))
  /*Fin Modificación*/
              
  WHERE EMPR_Codigo = @par_EMPR_Codigo                  
  AND Numero = @par_Numero                  
              
  SELECT Numero, Numero_Documento FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo                  
  AND Numero = @par_Numero        
 END      
 ELSE      
 BEGIN      
  SELECT -5 AS Numero, 0 AS Numero_Documento -- Ya Existe Preimpreso      
 END            
END  
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_obtener_remesa_paqueteria]  
(                        
 @par_EMPR_Codigo smallint,                    
 @par_Numero Numeric                    
)                        
AS                    
BEGIN                    
 select DISTINCT                         
 1 as Obtener,                        
 ENRE.EMPR_Codigo,                          
 ENRE.Fecha,                          
 ENRE.Numero,                        
 ENRE.Numeracion,                         
 ENRE.Numero_Documento,                          
 ENRE.Remesa_Cortesia,                        
 ENRE.Documento_Cliente,                          
 ENRE.Observaciones,                          
 ENRE.Fecha_Documento_Cliente,                          
 ENRE.CATA_FOPR_Codigo,                          
 ENRE.TERC_Codigo_Cliente,                          
 ENRE.DTCV_Codigo,                          
 ENRE.PRTR_Codigo,                          
 ENRE.Peso_Cliente,                          
 ENRE.Cantidad_Cliente,                          
 ENRE.Valor_Comercial_Cliente,                          
 ENRE.Peso_Volumetrico_Cliente,                          
 ENPR.CATA_TERP_Codigo,                          
 ENPR.CATA_TTRP_Codigo,                          
 ENPR.CATA_TDRP_Codigo,                          
 ENPR.CATA_TIRP_Codigo,                          
 ENPR.CATA_TPRP_Codigo,                          
 ENPR.CATA_TSRP_Codigo,                          
                        
 ENRE.TERC_Codigo_Remitente,                          
 ENRE.CIUD_Codigo_Remitente,                          
 ENRE.Direccion_Remitente,                          
 ENRE.Telefonos_Remitente,                          
 ENRE.TERC_Codigo_Destinatario,                          
 ENRE.CIUD_Codigo_Destinatario, 
 CIDE.Nombre as CiudadDestino, 
 ENRE.Direccion_Destinatario,                          
 ENRE.Telefonos_Destinatario,                          
 ENRE.Valor_Flete_Cliente,                          
 ENRE.Valor_Manejo_Cliente,                          
 ENRE.Valor_Seguro_Cliente,                          
 ENRE.Valor_Descuento_Cliente,                          
 ENRE.Total_Flete_Cliente,                        
 ENRE.Valor_Comision_Oficina,                        
 ENRE.Valor_Comision_Agencista,                        
 ENRE.Total_Flete_Transportador,                          
 ENRE.Estado,                        
 ISNULL(ENPR.OFIC_Codigo_Origen,0) AS OFIC_Codigo_Origen,                        
 ISNULL(OFOR.Nombre, '') AS NombreOficinaOrigen,                        
 ISNULL(DTCV.ETCV_Numero,DTCV2.ETCV_Numero) As ETCV_Numero,                        
 ISNULL(DTCV.LNTC_Codigo,DTCV2.LNTC_Codigo) As LNTC_Codigo,                          
 ISNULL(DTCV.TLNC_Codigo,DTCV2.TLNC_Codigo) As TLNC_Codigo,                        
 ISNULL(DTCV.RUTA_Codigo,DTCV2.RUTA_Codigo) As RUTA_Codigo,                        
 ISNULL(DTCV.TATC_Codigo,DTCV2.TATC_Codigo) As TATC_Codigo,                        
 ISNULL(TTTC.NombreTarifa,'') As NombreTarifa,                        
 ISNULL(DTCV.TTTC_Codigo,DTCV2.TTTC_Codigo) As TTTC_Codigo,                        
 ISNULL(TTTC.Nombre,'') As NombreTipoTarifa,                        
                        
 ENPR.Fecha_Interfaz_Cargue_Archivo,                        
 ENPR.Fecha_Interfaz_Cargue_WMS,                        
 ISNULL(ENRE.VEHI_Codigo,0) as VEHI_Codigo,              
 ISNULL(VEHI.Placa,'') As VEHI_Placa,  
 ISNULL(VEHI.Codigo_Alterno,'') As VEHI_CodigoAlterno,  
 ISNULL(ENRE.SEMI_Codigo,0) As SEMI_Codigo,                         
                        
 ENRE.CATA_TIRE_Codigo,                         
 ISNULL(ENPR.Reexpedicion,0) As Reexpedicion,                        
 ISNULL(ENRE.Valor_Reexpedicion,0) As Valor_Reexpedicion,              
 REMI.CATA_TIID_Codigo As TIID_Remitente,                        
 REMI.Numero_Identificacion As Identificacion_Remitente,   
 /*Modificación FP 2021/12/12*/
ISNULL(ENRE.Nombre_Remitente, RTRIM(LTRIM(ISNULL(REMI.Razon_Social,'') + ISNULL(REMI.Nombre,'') + ' ' + ISNULL(REMI.Apellido1, '') + ' ' + ISNULL(REMI.Apellido2,'')))) AS NombreRemitente,
/*Fin Modificación*/  
/*Modificación FP 2021/12/12*/                        
ISNULL(ENRE.Email_Remitente, ISNULL(REMI.Correo_Facturacion, '')) as Correo_Remitente,
/*Fin Modificación*/
 ISNULL(ENRE.Barrio_Remitente,'') As Barrio_Remitente,      
 ISNULL(ENRE.Codigo_Postal_Remitente,'') As Codigo_Postal_Remitente,                        
 ISNULL(ENPR.Observaciones_Remitente,'') As Observaciones_Remitente,                        
                        
 DEST.CATA_TIID_Codigo As TIID_Destinatario,                        
 DEST.Numero_Identificacion As Identificacion_Destinatario,
 /*Modificación FP 2021/12/12*/
ISNULL(ENRE.Nombre_Destinatario, LTRIM(RTRIM(ISNULL(DEST.Razon_Social,'') + ISNULL(DEST.Nombre,'') + ' ' + ISNULL(DEST.Apellido1, '') + ' ' + ISNULL(DEST.Apellido2,'')))) AS NombreDestinatario,
/*Fin Modificación*/
/*Modificación FP 2021/12/12*/                        
ISNULL(ENRE.Email_Destinatario, ISNULL(DEST.Correo_Facturacion, '')) as Correo_Destinatario,
/*Fin Modificación*/
 ISNULL(ENRE.Barrio_Destinatario,'') As Barrio_Destinatario,                          
                          
                         
 ISNULL(ENRE.Codigo_Postal_Destinatario, '') As Codigo_Postal_Destinatario,                        
 ISNULL(ENPR.Observaciones_Destinatario,'') As Observaciones_Destinatario,                        
 ISNULL(ENPR.Descripcion_Mercancia,'') As Descripcion_Mercancia,                        
 ISNULL(ENPR.OFIC_Codigo_Origen,0) As OFIC_Codigo_Origen,                        
 ISNULL(ENPR.OFIC_Codigo_Destino,0) As OFIC_Codigo_Destino,                           
 ISNULL(ENPR.OFIC_Codigo_Actual,0) As OFIC_Codigo_Actual,                        
 --ISNULL(OFOR.Nombre,'') As NombreOficinaOrigen,                        
 ISNULL(OFDE.Nombre,'') As NombreOficinaDestino,                        
 ISNULL(OFAC.Nombre,'') As NombreOficinaActual,                        
                     
 ENPR.CATA_ESRP_Codigo,              
 DERP.Fecha_Crea AS Fecha_Estado_Remesa,          
 OFER.Nombre AS Oficina_Nombre_Estado_Remesa,          
 ESRP.Nombre As NombreEstadoRemesa,                        
 ENRE.ENPD_Numero,                          
 CASE                         
 WHEN ENRE.ENPR_Numero = 0 THEN                         
 --(CASE WHEN ENPD.Numero_Documento = 0 THEN ENPE.Numero_Documento ELSE ENPD.Numero_Documento END )                         
 IIF(ENRE.ENPD_Numero = 0,ENPE.Numero_Documento,ENPD.Numero_Documento)                        
 ELSE EPRE.Numero_Documento END  AS NumeroDocumentoPlanilla,               
 ENPD.Fecha AS FechaPlanillaActual,                     
 ISNULL(ENFA.Numero_Documento, 0) AS ENFA_Numero_Documento,                      
 ISNULL(COND.Razon_Social,'') + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,                        
 ENRE.OFIC_Codigo,                        
 OFIC.Nombre As NombreOficina,                          
 ENPR.Centro_Costo,                          
 ENPR.CATA_LISC_Codigo,                          
 SICA.Nombre AS SitioCargue,                        
 ENPR.SICD_Codigo_Cargue,                          
 SIDE.Nombre AS SitioDescargue,                          
 ENPR.SICD_Codigo_Descargue,                          
 ENPR.Fecha_Entrega,                          
 ENRE.Valor_Flete_Transportador,                          
 ENRE.Total_Flete_Transportador,                        
 ENPR.Largo,                        
 ENPR.Alto,                        
 ENPR.Ancho,                        
 ENPR.Peso_Volumetrico,                        
 ENPR.Peso_A_Cobrar,                        
 ENPR.Flete_Pactado,                        
 ENPR.Ajuste_Flete,                          
 ENPR.CATA_LNPA_Codigo,                          
 ENPR.Recoger_Oficina_Destino,                        
 ENPR.OFIC_Codigo_Recibe,                        
 OFRE.Nombre AS OFIC_Nombre_Recibe,                        
 ENPR.Maneja_Detalle_Unidades,                        
 ENPR.ZOCI_Codigo_Entrega,                        
 ENPR.Latitud,                        
 ENPR.Longitud,                        
 ENPR.CATA_HERP_Codigo,                          
 ENPR.Liquidar_Unidad,                        
 ISNULL(ENRE.Valor_Otros, 0) Valor_Otros,                        
 ISNULL(ENRE.Valor_Cargue, 0) Valor_Cargue,                        
 ISNULL(ENRE.Valor_Descargue, 0) Valor_Descargue,                      
 ISNULL(ENPR.Devolucion, '') Devolucion,                      
 ISNULL(ENPR.Fecha_Devolucion, '') Fecha_Devolucion,          
 ISNULL(ELRG.Numero_Documento, 0) ELRG_NumeroDocumento,                
 ISNULL(ENPR.TERC_Codigo_Aforador, 0) TERC_Codigo_Aforador,                  
 ISNULL(ENPR.OFIC_Codigo_Registro_Manual, 0) OFIC_Codigo_Registro_Manual,      
 ISNULL(NERP.Campo1, '') AS NovedadRecibe,    
 ISNULL(ELGU.Numero_Documento, 0) AS ELGU_NumeroDocumento    
      
 from         
 Remesas_Paqueteria as ENPR                        
                          
 LEFT JOIN Sitios_Cargue_Descargue AS SICA ON                          
 ENPR.EMPR_Codigo = SICA.EMPR_Codigo                          
 AND ENPR.SICD_Codigo_Cargue = SICA.Codigo                          
                          
 LEFT JOIN Sitios_Cargue_Descargue AS SIDE ON                          
 ENPR.EMPR_Codigo = SIDE.EMPR_Codigo                          
 AND ENPR.SICD_Codigo_Descargue = SIDE.Codigo                          
                          
 INNER JOIN Encabezado_Remesas AS ENRE                          
 ON ENPR.EMPR_Codigo = ENRE.EMPR_Codigo                        
 AND ENPR.ENRE_Numero = ENRE.Numero                          
                          
 INNER JOIN Terceros As REMI ON                        
 ENRE.EMPR_Codigo = REMI.EMPR_Codigo                        
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                          
                         
 INNER JOIN Terceros As DEST ON                        
 ENRE.EMPR_Codigo = DEST.EMPR_Codigo                        
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                         
                         
 INNER JOIN V_Estado_Remesa_Paqueteria AS ESRP ON                        
 ENPR.EMPR_Codigo = ESRP.EMPR_Codigo                        
 AND ENPR.CATA_ESRP_Codigo = ESRP.Codigo                        
                        
 INNER JOIn Oficinas AS OFIC ON                        
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                        
 AND ENRE.OFIC_Codigo = OFIC.Codigo                        
                        
 LEFT JOIN Oficinas As OFOR ON                        
 ENPR.EMPR_Codigo = OFOR.EMPR_Codigo                        
 AND ENPR.OFIC_Codigo_Origen = OFOR.Codigo                        
                        
 LEFT JOIN Oficinas As OFDE ON                        
 ENPR.EMPR_Codigo = OFDE.EMPR_Codigo                        
 AND ENPR.OFIC_Codigo_Destino = OFDE.Codigo                        
                         
 LEFT JOIN Oficinas As OFAC ON                        
 ENPR.EMPR_Codigo = OFAC.EMPR_Codigo                        
 AND ENPR.OFIC_Codigo_Actual = OFAC.Codigo                        
                        
 LEFT JOIN Oficinas As OFRE ON                        
 ENPR.EMPR_Codigo = OFRE.EMPR_Codigo                        
 AND ENPR.OFIC_Codigo_Recibe = OFRE.Codigo                        
                           
 LEFT JOIN Detalle_Tarifario_Carga_Ventas AS DTCV                          
 ON ENPR.EMPR_Codigo = DTCV.EMPR_Codigo                         
 AND ENRE.DTCV_Codigo = DTCV.Codigo                        
                          
 LEFT JOIN Detalle_Tarifario_Carga_Ventas AS DTCV2                          
 ON ENPR.EMPR_Codigo = DTCV2.EMPR_Codigo                          
 AND ENRE.EMPR_Codigo = DTCV2.EMPR_Codigo                          
 AND ENRE.ETCV_Numero = DTCV2.ETCV_Numero                          
 AND ENRE.CIUD_Codigo_Remitente = DTCV2.CIUD_Codigo_Origen                          
 AND ENRE.CIUD_Codigo_Destinatario= DTCV2.CIUD_Codigo_Destino                        
 AND ENRE.CATA_FOPR_Codigo= DTCV2.CATA_FPVE_Codigo  
 
  LEFT JOIN Ciudades CIDE on  ENRE.EMPR_Codigo = CIDE.EMPR_Codigo
   AND   ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo                        
 LEFT JOIN V_Tipo_Tarifa_Transporte_Carga As TTTC ON                        
 ISNULL(DTCV.EMPR_Codigo,DTCV2.EMPR_Codigo) = TTTC.EMPR_Codigo                        
 AND ISNULL(DTCV.TATC_Codigo,DTCV2.TATC_Codigo) = TTTC.TATC_Codigo                        
 AND ISNULL(DTCV.TTTC_Codigo,DTCV2.TTTC_Codigo) = TTTC.Codigo                        
                        
 LEFT JOIN Vehiculos AS VEHI ON                        
 ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                        
 AND ENRE.VEHI_Codigo = VEHI.Codigo                        
                        
 LEFT JOIN Terceros As COND ON                        
 ENRE.EMPR_Codigo = COND.EMPR_Codigo                        
 AND ENRE.TERC_Codigo_Conductor = COND.Codigo            
 AND ENRE.TERC_Codigo_Conductor > 0            
              
 LEFT JOIN Encabezado_Planilla_Despachos As ENPD ON                        
 ENPD.EMPR_Codigo = ENPR.EMPR_Codigo                        
 AND ENPD.Numero = ENPR.ENPD_Numero_Ultima_Planilla          
                        
 LEFT JOIN Encabezado_Planilla_Despachos As EPRE ON                        
 EPRE.EMPR_Codigo = ENRE.EMPR_Codigo                     
 AND EPRE.Numero = ENRE.ENPR_Numero                         
                        
 LEFT JOIN Encabezado_Planilla_Despachos As ENPE ON                        
 ENPE.EMPR_Codigo = ENRE.EMPR_Codigo                        
 AND ENPE.Numero = ENRE.ENPE_Numero                      
                      
 LEFT JOIN Encabezado_Facturas As ENFA ON                        
 ENRE.EMPR_Codigo = ENFA.EMPR_Codigo                        
 AND ENRE.ENFA_Numero = ENFA.Numero                    
          
 LEFT JOIN Detalle_Legalizacion_Recaudo_Guias As DLRG ON                        
 ENRE.EMPR_Codigo = DLRG.EMPR_Codigo                        
 AND ENRE.Numero = DLRG.ENRE_Numero                    
                    
 LEFT JOIN Encabezado_Legalizacion_Recaudo_Guias As ELRG ON                        
 DLRG.EMPR_Codigo = ELRG.EMPR_Codigo                        
 AND DLRG.ELRG_Numero = ELRG.Numero              
              
 LEFT JOIN (              
 SELECT TOP 1 ERPA.* FROM Detalle_Estados_Remesas_Paqueteria ERPA              
 LEFT JOIN Encabezado_Planilla_Despachos ENDP1 ON              
 ERPA.EMPR_Codigo = ENDP1.EMPR_Codigo              
 AND ERPA.ENPD_Numero = ENDP1.Numero      
     
 WHERE ERPA.EMPR_Codigo = @par_EMPR_Codigo              
 AND ERPA.ENRE_Numero = @par_Numero              
 AND ISNULL(ENDP1.Anulado, 0) = 0          
 AND ERPA.OFIC_Codigo IS NOT NULL          
              
 ORDER BY ID DESC              
 ) AS DERP ON              
 ENPR.EMPR_Codigo = DERP.EMPR_Codigo                        
 AND ENPR.CATA_ESRP_Codigo = DERP.CATA_ESPR_Codigo          
          
 LEFT JOIN Oficinas OFER ON          
 DERP.EMPR_Codigo = OFER.EMPR_Codigo          
 AND DERP.OFIC_Codigo = OFER.Codigo       
       
 LEFT JOIN Valor_Catalogos NERP ON          
 ENPR.EMPR_Codigo = NERP.EMPR_Codigo          
 AND ENPR.CATA_NERP_Codigo = NERP.Codigo     
     
 LEFT JOIN (      
 SELECT DLGU.EMPR_Codigo, DLGU.ENRE_Numero, MAX(ENLG.Numero_Documento) AS Numero_Documento FROM Detalle_Legalizacion_Guias DLGU      
 INNER JOIN Encabezado_Legalizacion_Guias ENLG ON            
 ENLG.EMPR_Codigo = DLGU.EMPR_Codigo            
 AND ENLG.Numero = DLGU.ELGU_Numero      
 WHERE DLGU.EMPR_Codigo = @par_EMPR_Codigo      
 AND DLGU.ENRE_Numero = @par_Numero    
 AND ENLG.Estado = 1    
 AND ENLG.Anulado = 0    
 group by DLGU.EMPR_Codigo, DLGU.ENRE_Numero      
 ) AS ELGU ON      
 ENRE.EMPR_Codigo = ELGU.EMPR_Codigo            
 AND ENRE.Numero = ELGU.ENRE_Numero              
             
 WHERE                      
 ENRE.EMPR_Codigo = @par_EMPR_Codigo                        
 AND ENRE.Numero = @par_Numero                        
 AND ENRE.TIDO_Codigo = 110 --> Guía                        
END  
GO

/*----------Ediciones a SP solo agregan referencia a nombres de remitentes y destinatarios en encabezado remesas------------------*/
CREATE OR ALTER PROCEDURE [dbo].[gps_Reporte_Guias](    
 @par_EMPR_Codigo numeric,       
 @par_Numero_Documento numeric      
)      
AS BEGIN       
SELECT       
ENRE.Numero_Documento      
,EMPR.Numero_Identificacion NitEmpresa      
,ENRE.Fecha AS Fecha      
,OFIC.Nombre AS NombreOficina      
,OFIC.Direccion AS DireccionOficina      
,OFIC.Telefono AS TelefonoOficina      
,REMI.Numero_Identificacion AS IdenRemitente
     /*Modificación FP 2021/12/12*/                        
,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')
+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente
  /*Fin Modificación*/
,ENRE.Direccion_Remitente       
,ENRE.Telefonos_Remitente   
     /*Modificación FP 2021/12/12*/                        
,ISNULL(ENRE.Email_Remitente, REMI.Correo_Facturacion) as CorreoRemitente  
  /*Fin Modificación*/
,REMI.Codigo_Postal as CodPostalRemitente  
,CIRE.Nombre CiudadRemitente      
  
,DEST.Numero_Identificacion AS IdenDestinatario      
     /*Modificación FP 2021/12/12*/                        
,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')        
+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
  /*Fin Modificación*/
,ENRE.Direccion_Destinatario       
,ENRE.Telefonos_Destinatario  
     /*Modificación FP 2021/12/12*/                        
,ISNULL(ENRE.Email_Destinatario, DEST.Correo_Facturacion) as CorreoDestinatario 
  /*Fin Modificación*/
,DEST.Codigo_Postal as CodPostalDestinatario      
  
,CIDE.Nombre CiudadDestinatario      
,LING.Campo1 as LineaNegocio  
 ,ENRE.Observaciones  
 ,REPA.Fecha_Recibe    
  
,CASE    
when   ENRE.CATA_FOPR_Codigo  = 4901  then  'CR'  
when   ENRE.CATA_FOPR_Codigo  = 4902  then  'CC'  
when   ENRE.CATA_FOPR_Codigo  = 4903  then  'CE'  
END   
as FormaPago  
,ENFE.CUFE
,ENFA.Numero_Documento as NoFactura
,VEHI.Placa      
,ENRE.Peso_Cliente      
,ENRE.Cantidad_Cliente      
,ENRE.Valor_Flete_Cliente      
,ENRE.Valor_Manejo_Cliente       
,ENRE.Valor_Comercial_Cliente AS DECLARADO      
,ENRE.Total_Flete_Cliente AS TOTAL_PAGAR      
,PRTR.Nombre Producto      
,USCR.Nombre AS Elaboro    
,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue           
      
FROM  Encabezado_Remesas ENRE    
    
LEFT JOIN Remesas_Paqueteria REPA ON    
ENRE.EMPR_Codigo = REPA.EMPR_Codigo    
AND ENRE.Numero = REPA.ENRE_Numero    
  
INNER JOIN  Valor_Catalogos LING on   
LING.EMPR_Codigo =  REPA.EMPR_Codigo  
and  REPA.CATA_LNPA_Codigo = LING.Codigo  
  
INNER JOIN  Valor_Catalogos FORP on   
FORP.EMPR_Codigo =  REPA.EMPR_Codigo  
and  ENRE.CATA_FOPR_Codigo = FORP.Codigo  

LEFT JOIN Encabezado_Facturas ENFA
on  ENFA.EMPR_Codigo = ENRE.EMPR_Codigo
and ENFA.Numero  =  ENRE.ENFA_Numero
 
 LEFT JOIN Factura_Electronica ENFE
on  ENFE.EMPR_Codigo = ENRE.EMPR_Codigo
and ENFE.ENFA_Numero  =  ENRE.ENFA_Numero
  
 
  
LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON    
REPA.EMPR_Codigo = SICD_DESC.EMPR_Codigo    
AND REPA.SICD_Codigo_Descargue = SICD_DESC.Codigo    
    
,Empresas EMPR      
,Oficinas OFIC      
,Vehiculos VEHI      
,Terceros REMI      
,Terceros DEST      
,Ciudades CIRE      
,Ciudades CIDE      
,Producto_Transportados PRTR      
,Usuarios USCR      
,Encabezado_Tarifario_Carga_Ventas ENTA      
      
WHERE ENRE.EMPR_Codigo = EMPR.Codigo      
AND ENRE.EMPR_Codigo = OFIC.EMPR_Codigo      
AND ENRE.OFIC_Codigo = OFIC.Codigo       
AND ENRE.EMPR_Codigo = VEHI.EMPR_Codigo      
AND ENRE.VEHI_Codigo = VEHI.Codigo       
      
AND ENRE.EMPR_Codigo = REMI.EMPR_Codigo      
AND ENRE.TERC_Codigo_Remitente = REMI.Codigo       
AND ENRE.EMPR_Codigo = DEST.EMPR_Codigo      
AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo       
      
AND ENRE.EMPR_Codigo = CIRE.EMPR_Codigo      
AND ENRE.CIUD_Codigo_Remitente = CIRE.Codigo       
AND ENRE.EMPR_Codigo = CIDE.EMPR_Codigo      
AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo       
      
AND ENRE.EMPR_Codigo = PRTR.EMPR_Codigo      
AND ENRE.PRTR_Codigo = PRTR.Codigo      
      
      
AND ENRE.EMPR_Codigo = USCR.EMPR_Codigo       
AND ENRE.USUA_Codigo_Crea = USCR.Codigo      
      
AND ENRE.EMPR_Codigo = ENTA.EMPR_Codigo      
AND ENRE.ETCV_Numero = ENTA.Numero      
AND ENRE.TIDO_Codigo = 110      
      
AND ENRE.EMPR_Codigo = @par_EMPR_Codigo      
AND ENRE.Numero = @par_Numero_Documento      
END 

print 'gsp_consultar_guias_norecibidas'
 /* -------------Anotacion FP 2021/12/12: Posible error al migrar script, se comenta por si es deliberado
/****** Object:  StoredProcedure [dbo].[gsp_consultar_guias_norecibidas]    Script Date: 4/08/2021 11:15:02 a. m. ******/
DROP PROCEDURE [dbo].[gsp_consultar_guias_norecibidas]
 ---------------fin anotacion */
GO

CREATE OR ALTER  PROCEDURE [dbo].[gsp_consultar_detalle_planilla_cargue_descargue]      
(      
@par_EMPR_Codigo smallint,      
@par_ENPD_Numero numeric      
)      
AS       
BEGIN      
 SELECT          DPCD.EMPR_Codigo,
                DPCD.EPCD_Numero,
                DPCD.ENRE_Numero                 AS NumeroRemesa,
                DPCD.Numero_etiqueta_unidad      AS Numero_Unidad,
                DPCD.Observaciones,
                ENRE.Numero_documento            AS Numero_Documento_Remesa,
                ENRE.Peso_Cliente                AS PesoRemesa, 
                Isnull(CLIE.razon_social, '') + ' '
                + Isnull(CLIE.nombre, '') + ' '
                + Isnull(CLIE.apellido1, '') + ' '
                + Isnull(CLIE.apellido2, '')     AS NombreCliente,
				/*Modificación FP 2021/12/12*/                        
				ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')
				+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente,
				/*Fin Modificación*/
                PRTR.nombre                      AS NombreProducto,
                FOPA.campo1                      AS FormaPago,
                ENRE.ciud_Codigo_remitente       CIUD_Codigo_Origen,
                ENRE.ciud_Codigo_destinatario    CIUD_Codigo_Destino,
                CIOR.nombre                      AS CiudadOrigen,
                CIDE.nombre                      AS CiudadDestino,
                ENRE.total_flete_cliente         AS TotalFleteCliente,
                ENRE.valor_seguro_cliente,
                ENRE.cantidad_cliente,
                ENRE.peso_cliente  

FROM  Detalle_Planilla_Cargue_Descargue DPCD
       
	   LEFT JOIN Encabezado_Remesas ENRE
              ON DPCD.EMPR_Codigo = ENRE.EMPR_Codigo
                 AND DPCD.ENRE_Numero = ENRE.Numero
      
	   INNER JOIN Remesas_Paqueteria AS ENRP
               ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo
                  AND ENRP.ENRE_Numero = ENRE.Numero
       
	   LEFT JOIN Terceros AS CLIE
              ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
                 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo
       
	   LEFT JOIN Terceros AS REMI
              ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo
                 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo
       
	   LEFT JOIN Terceros AS DEST
              ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo
                 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo
        
       
	   LEFT JOIN producto_transportados AS PRTR
              ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
                 AND ENRE.PRTR_Codigo = PRTR.Codigo 

	   LEFT JOIN Ciudades CIOR
              ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo
                 AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo
       
	   LEFT JOIN Ciudades CIDE
              ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo
                 AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo

	   LEFT JOIN Valor_Catalogos FOPA
              ON ENRE.EMPR_Codigo = FOPA.EMPR_Codigo
                 AND ENRE.CATA_FOPR_Codigo = FOPA.Codigo
      
WHERE DPCD.EMPR_Codigo = @par_EMPR_Codigo AND      
DPCD.EPCD_Numero = @par_ENPD_Numero      
      
END 
GO

  CREATE OR ALTER  PROCEDURE  [dbo].[gsp_obtener_encabezado_planilla_despachos_x_manifiesto]                             
(                                        
@par_EMPR_Codigo SMALLINT,                                        
@par_TIDO_Codigo SMALLINT,                                        
@par_Numero_Manifiesto NUMERIC = NULL,                                  
@par_Numero   NUMERIC = null,                                  
@par_Numero_Documento NUMERIC = NULL,                                  
@par_OFIC_Codigo SMALLINT = NULL                                  
)                                  
AS                                  
BEGIN                                  
                       
SELECT                                  
1 AS Obtener                                  
,ENPD.EMPR_Codigo                                  
,ENPD.Numero                                  
,ENPD.TIDO_Codigo                                  
,ENPD.Numero_Documento                                  
,ENPD.ENMC_Numero                                  
,ENPD.Fecha_Hora_Salida                                  
,ENPD.Fecha                                  
,ISNULL(ENPD.LNTC_Codigo_Compra, 0) AS LNTC_Codigo_Compra                                  
,ISNULL(ENPD.TLNC_Codigo_Compra, 0) AS TLNC_Codigo_Compra                                  
,ISNULL(ENPD.RUTA_Codigo, 0) AS RUTA_Codigo      
    
,ENPD.TATC_Codigo_Compra                                  
,ENPD.TTTC_Codigo_Compra                                  
,ISNULL(ENPD.ETCC_Numero, 0) AS ETCC_Numero                                  
,ENPD.VEHI_Codigo                                  
,ISNULL(ENPD.SEMI_Codigo,'') AS SEMI_Codigo                                  
,ISNULL(SEMI.Placa,'') AS SEMI_PLaca                                  
,ENPD.TERC_Codigo_Tenedor                                  
,ENPD.TERC_Codigo_Conductor                                  
,ENPD.Cantidad                                  
,ENPD.Peso                                  
,                                  
--sub divicion                                      
ENPD.Valor_Flete_Transportador                                  
, --quitar la palabra valor                                       
ENPD.Valor_Anticipo                                  
,ENPD.Valor_Impuestos                                  
,ENPD.Valor_Pagar_Transportador     
,ENPD.Valor_Flete_Cliente                                  
,ENPD.Valor_Seguro_Mercancia                                  
,ENPD.Valor_Otros_Cobros                                  
,ENPD.Valor_Total_Credito                                  
,ENPD.Valor_Total_Contado                                  
,ENPD.Valor_Total_Alcobro                                  
,ISNULL(ENPD.Valor_Auxiliares  ,0) AS Valor_Auxiliares                                
,                
--fin division                                       
ENPD.Observaciones                                  
,ENPD.Anulado                                  
,ENPD.Estado                        
,ENPD.Fecha_Crea                                  
,ENPD.USUA_Codigo_Crea                                  
,ISNULL(ENPD.Fecha_Modifica, '') AS Fecha_Modifica                     
,ISNULL(ENPD.USUA_Codigo_Modifica, 0) AS USUA_Codigo_Modifica                                  
,ISNULL(ENPD.Fecha_Anula, '') AS Fecha_Anula                                  
,ISNULL(ENPD.USUA_Codigo_Anula, 0)                                  
,ISNULL(ENPD.Causa_Anula, '') AS Causa_Anula                                  
,ENPD.OFIC_Codigo                                  
,OFIC.Nombre AS NombreOficina                                
,ISNULL(ENPD.Numeracion  ,0) AS Numeracion                                
,                                  
--Datos para el obtener                                       
ISNULL(LNTC.Nombre,0) AS LineaNegocio                          
, ISNULL(TLNC.Nombre,0) AS TipoLineaNegocio                                  
,'' AS TipoTarifa                                  
,'' AS NombreTrarifa                     
,ENPD.TATC_Codigo_Compra AS NumeroTarifario                         
,VEHI.Placa AS PlacaVehiculo                                  
,SEMI.Placa AS SemiRemolPlaca                                  
,ISNULL(COND.Razon_Social, '') + ' ' + ISNULL(COND.Nombre, '')                                  
+ ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') AS NombreConductor                                  
,COND.Numero_Identificacion AS IdentificacionConductor                                  
,ISNULL(TENE.Razon_Social, '') + ' ' + ISNULL(TENE.Nombre, '')                                  
+ ' ' + ISNULL(TENE.Apellido1, '') + ' ' + ISNULL(TENE.Apellido2, '') AS NombreTenedor                                  
,TENE.Numero_Identificacion AS IdentificacionTenedor                                  
,RUTA.Nombre AS NombreRuta  
,RUTA.Duracion_Horas
,                                  
---- REMESA                                       
ISNULL(ENRE.Numero_Documento, 0) AS NumeroRemesa                                  
,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '')                                  
+ ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                  
,ISNULL(PRTR.Nombre, '') AS NombreProducto                                  
,ISNULL(ENRE.Cantidad_Cliente, 0) AS CantidadRemesa                                  
,ISNULL(ENRE.Peso_Cliente, 0) AS PesoRemesa 
     /*Modificación FP 2021/12/12*/                        
,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')        
+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
  /*Fin Modificación*/                              
,CASE WHEN ENPD.ECPD_Numero IS NULL THEN 0 WHEN ECPD.Estado = 0 THEN 0 ELSE ENPD.ECPD_Numero END AS ECPD_Numero                                  
,ISNULL(ECPD.Numero_Documento,0) AS NumeroCumplido                                  
,ISNULL(ENPD.ENMC_Numero,0) AS ENMC_Numero                                  
,ISNULL(ENMC.Numero_Documento,0) AS NumeroManifiesto                                  
,ISNULL(ENPD.ELPD_Numero,0) AS ELPD_Numero                                  
,ISNULL(ELPD.Numero_Documento,0) AS NumeroLiquidacion                        
,Valor_Seguro_Poliza                      
,ENPD.Anticipo_Pagado_A                    
,CASE WHEN ENDC.Codigo_Alterno = 'TRANSFERENCIA' THEN 1 ELSE 0 END AS PagoAnticipo                
,(SELECT SUM(CURE.Peso_Descargue)FROM Cumplido_Remesas AS CURE             
INNER JOIN Encabezado_Remesas AS ENRE ON           
CURE.EMPR_Codigo = ENRE.EMPR_Codigo             
AND CURE.ENRE_Numero = ENRE.Numero             
WHERE ENRE.EMPR_Codigo=ENPD.EMPR_Codigo            
and ENRE.ENPD_Numero = ENPD.Numero)             
AS PesoCumplido            
,            
(SELECT SUM(CURE.Peso_Cargue)FROM Cumplido_Remesas AS CURE            
INNER JOIN Encabezado_Remesas AS ENRE ON             
CURE.EMPR_Codigo = ENRE.EMPR_Codigo             
AND CURE.ENRE_Numero = ENRE.Numero             
WHERE ENRE.EMPR_Codigo=ENPD.EMPR_Codigo             
and ENRE.ENPD_Numero = ENPD.Numero)             
AS PesoCargue            
,            
(SELECT SUM(CURE.Peso_Faltante)FROM Cumplido_Remesas AS CURE             
INNER JOIN Encabezado_Remesas AS ENRE ON             
CURE.EMPR_Codigo = ENRE.EMPR_Codigo             
AND CURE.ENRE_Numero = ENRE.Numero             
WHERE ENRE.EMPR_Codigo=ENPD.EMPR_Codigo             
and ENRE.ENPD_Numero = ENPD.Numero)             
AS PesoFaltante           
          
,USCU.Nombre as UsuarioCumplido            
,CIOR.Nombre AS CiudadOrigen            
,CIDE.Nombre AS CiudadDestino            
,ECPD.Fecha_Crea AS FechaCumplido            
,CASE WHEN ENPD.TATC_Codigo_Compra = 301 THEN (ENPD.Valor_Flete_Transportador/ ENPD.Peso)*1000 ELSE ENPD.Valor_Flete_Transportador END AS FleteUnitario            
,CASE WHEN ENPD.TATC_Codigo_Compra = 301 THEN 'Cupo Tonelada' ELSE 'Cantidad Viajes' END AS TipoDespacho            
     
FROM Encabezado_Manifiesto_Carga ENMC          
  
INNER JOIN Encabezado_Planilla_Despachos ENPD                               
ON ENMC.EMPR_Codigo = ENPD.EMPR_Codigo                                  
AND ENMC.ENPD_Numero = ENPD.Numero      
  
LEFT JOIN Encabezado_Remesas ENRE                                  
ON ENPD.EMPR_Codigo = ENRE.EMPR_Codigo                                  
AND ENPD.Numero = ENRE.ENPD_Numero                                  
                                  
---- PLACA VEHICULO                                      
LEFT JOIN Vehiculos VEHI                                  
ON ENMC.EMPR_Codigo = VEHI.EMPR_Codigo                                  
AND ENMC.VEHI_Codigo = VEHI.Codigo                                  
---- SEMIREMOLQUE                                      
LEFT JOIN Semirremolques SEMI                                  
ON ENMC.EMPR_Codigo = SEMI.EMPR_Codigo                                  
AND ENMC.SEMI_Codigo = SEMI.Codigo                                  
---- TENEDOR                                      
LEFT JOIN Terceros TENE                 
ON ENMC.EMPR_Codigo = TENE.EMPR_Codigo                                  
AND ENMC.TERC_Codigo_Tenedor = TENE.Codigo                                  
---- CONDUCTOR                                      
LEFT JOIN Terceros COND                                  
ON ENMC.EMPR_Codigo = COND.EMPR_Codigo                                  
AND ENMC.TERC_Codigo_Conductor = COND.Codigo                                  
---- LINEA DE NEGOCIO                                      
LEFT JOIN Linea_Negocio_Transporte_Carga LNTC                                  
ON ENPD.EMPR_Codigo = LNTC.EMPR_Codigo                                  
AND ENPD.LNTC_Codigo_Compra = LNTC.Codigo                                  
---- TIPO DE LINEA DE NEGOCIO                                      
LEFT JOIN Tipo_Linea_Negocio_Carga TLNC                                  
ON ENPD.EMPR_Codigo = TLNC.EMPR_Codigo                                  
AND ENPD.TLNC_Codigo_Compra = TLNC.Codigo                                  
AND LNTC.Codigo = TLNC.LNTC_Codigo                                  
---- RUTA                                       
LEFT JOIN Rutas RUTA                                  
ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo                                  
AND ENPD.RUTA_Codigo = RUTA.Codigo                  
            
LEFT JOIN Ciudades as CIOR            
ON CIOR.EMPR_Codigo = RUTA.EMPR_Codigo            
AND CIOR.Codigo = RUTA.CIUD_Codigo_Origen            
            
LEFT JOIN Ciudades as CIDE            
ON CIDE.EMPR_Codigo = RUTA.EMPR_Codigo            
AND CIDE.Codigo = RUTA.CIUD_Codigo_Destino            
            
---- TARIFARIO                                       
LEFT JOIN V_Tipo_Tarifa_Transporte_Carga VTTC                                  
ON ENPD.EMPR_Codigo = VTTC.EMPR_Codigo                                  
AND ENPD.TATC_Codigo_Compra = VTTC.TATC_Codigo                                  
AND ENPD.TTTC_Codigo_Compra = VTTC.Codigo                                  
---- OFICINA                      
LEFT JOIN Oficinas OFIC                                  
ON ENPD.EMPR_Codigo = OFIC.EMPR_Codigo                                  
AND ENPD.OFIC_Codigo = OFIC.Codigo                                  
---- PRODUCTO                                       
LEFT JOIN Producto_Transportados PRTR                                  
ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo             
AND ENRE.PRTR_Codigo = PRTR.Codigo                                  
---- CLIENTE                          
LEFT JOIN Terceros CLIE                                  
ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                  
AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                  
                                  
---- DESTINATARIO                                       
LEFT JOIN Terceros DEST                                  
ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                  
AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                  
                                  
LEFT JOIN Encabezado_Cumplido_Planilla_Despachos ECPD                                  
ON ENMC.EMPR_Codigo = ENPD.EMPR_Codigo                                  
AND ENMC.Numero = ECPD.ENMC_Numero             
            
LEFT JOIN Usuarios AS USCU            
ON USCU.EMPR_Codigo = ECPD.EMPR_Codigo            
AND USCU.Codigo = ECPD.USUA_Codigo_Crea            
                                  
                              
                                  
LEFT JOIN Encabezado_Liquidacion_Planilla_Despachos ELPD                                  
ON ENPD.EMPR_Codigo = ELPD.EMPR_Codigo                                  
AND ENPD.ELPD_Numero = ELPD.Numero                                  
                  
LEFT JOIN Encabezado_Documento_Cuentas AS ENDC                  
ON ENDC.CATA_DOOR_Codigo = 2604                  
AND ENDC.EMPR_Codigo = ENPD.EMPR_Codigo                  
AND ENDC.Codigo_Documento_Origen = ENPD.Numero                  
                  
WHERE ENPD.EMPR_Codigo = @par_EMPR_Codigo                                  
AND ENPD.TIDO_Codigo = @par_TIDO_Codigo                                  
AND ENMC.Numero_Documento = @par_Numero_Manifiesto                            
                            
--SELECT @Numero AS Numero                                  
END                

 --------------   Obtener Detalle  planilla despachos  para traer la ruta ----------  
   print 'gsp_consultar_detalle_estados_remesas_paqueteria'
GO

CREATE OR ALTER  PROCEDURE  [dbo].[gsp_obtener_encabezado_planilla_despachos]
(                                                    
	@par_EMPR_Codigo SMALLINT,                                                    
	@par_TIDO_Codigo SMALLINT,                                                    
	@par_Numero   NUMERIC = null,                                              
	@par_Numero_Documento NUMERIC = NULL,                                              
	@par_Numero_Manifiesto NUMERIC = NULL,                                              
	@par_OFIC_Codigo SMALLINT = NULL                                              
)                                              
AS                                              
BEGIN                                              
                                               
	DECLARE @Numero NUMERIC                                              
                                              
	IF @par_Numero_Documento > = 0 BEGIN
		SELECT                                              
		@Numero = Numero                                              
		FROM                                              
		Encabezado_Planilla_Despachos                                              
		WHERE                                              
		EMPR_Codigo = @par_EMPR_Codigo                                              
		AND Numero_Documento = @par_Numero_Documento                                              
		AND TIDO_Codigo = @par_TIDO_Codigo                                              
		--AND OFIC_Codigo = @par_OFIC_Codigo                                              
	END                                         
	ELSE IF @par_Numero_Manifiesto > = 0                                        
	BEGIN                                        
		SELECT                                              
		@Numero = ENPD.Numero                                              
		FROM                                              
		Encabezado_Planilla_Despachos AS ENPD                                        
		LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC                                        
		ON    ENPD.EMPR_Codigo = ENMC.EMPR_Codigo                                        
		and ENPD.ENMC_Numero = ENMC.Numero                                        
		WHERE                                              
		ENPD.EMPR_Codigo = @par_EMPR_Codigo                                              
		AND ENMC.Numero_Documento = @par_Numero_Manifiesto
	END                                             
	ELSE BEGIN                                              
		SET @Numero = @par_Numero                                              
	END                                              
                                              
	SELECT                                              
	1 AS Obtener                                              
	,ENPD.EMPR_Codigo                                              
	,ENPD.Numero                                              
	,ENPD.TIDO_Codigo                                              
	,ENPD.Numero_Documento                                              
	,ENPD.ENMC_Numero              
	,ENMC.CATA_TIMA_Codigo              
	,ENMC.Cantidad_viajes_dia                                             
	,ENPD.Fecha_Hora_Salida                                              
	,ENPD.Fecha                                              
	,ISNULL(ENPD.LNTC_Codigo_Compra, 0) AS LNTC_Codigo_Compra                                              
	,ISNULL(ENPD.TLNC_Codigo_Compra, 0) AS TLNC_Codigo_Compra                                              
	,ISNULL(ENPD.RUTA_Codigo, 0) AS RUTA_Codigo                                              
	,ENPD.TATC_Codigo_Compra            
	,TATC.Nombre AS TATC_Nombre                                         
	,ENPD.TTTC_Codigo_Compra            
	,VTTC.Nombre AS TTTC_Nombre          
	,ISNULL(ENPD.ETCC_Numero, 0) AS ETCC_Numero                                              
	,ENPD.VEHI_Codigo                                        
	,ISNULL(ENPD.SEMI_Codigo,'') AS SEMI_Codigo                                              
	,ISNULL(SEMI.Placa,'') AS SEMI_PLaca                                              
	,ENPD.TERC_Codigo_Tenedor           
	,ENPD.TERC_Codigo_Conductor                                              
	,ENPD.Cantidad                                              
	,ENPD.Peso                                              
	,                                              
	--sub divicion                                                  
	ENPD.Valor_Flete_Transportador                                              
	, --quitar la palabra valor                                     
	ENPD.Valor_Anticipo                                              
	,ENPD.Valor_Impuestos                                              
	,ENPD.Valor_Pagar_Transportador                 
	,ENPD.Valor_Flete_Cliente                                              
	,ENPD.Valor_Seguro_Mercancia                                              
	,ENPD.Valor_Otros_Cobros                                              
	,ENPD.Valor_Total_Credito                                              
	,ENPD.Valor_Total_Contado                                              
	,ENPD.Valor_Total_Alcobro                                              
	,ISNULL(ENPD.Valor_Auxiliares  ,0) AS Valor_Auxiliares                                            
	,                            
	--fin division                                                   
	ENPD.Observaciones                                              
	,ENPD.Anulado                                              
	,ENPD.Estado                                    
	,ENPD.Fecha_Crea                                              
	,ENPD.USUA_Codigo_Crea                                              
	,ISNULL(ENPD.Fecha_Modifica, '') AS Fecha_Modifica                                 
	,ISNULL(ENPD.USUA_Codigo_Modifica, 0) AS USUA_Codigo_Modifica                                              
	,ISNULL(ENPD.Fecha_Anula, '') AS Fecha_Anula                                              
	,ISNULL(ENPD.USUA_Codigo_Anula, 0)                                              
	,ISNULL(ENPD.Causa_Anula, '') AS Causa_Anula                                              
	,ENPD.OFIC_Codigo                                              
	,OFIC.Nombre AS NombreOficina                                            
	,ISNULL(ENPD.Numeracion  ,0) AS Numeracion                                            
	,                                              
	--Datos para el obtener                                                   
	ISNULL(LNTC.Nombre,0) AS LineaNegocio                                      
	, ISNULL(TLNC.Nombre,0) AS TipoLineaNegocio                                              
	,'' AS TipoTarifa                                              
	,'' AS NombreTrarifa                                              
	,ENPD.TATC_Codigo_Compra AS NumeroTarifario            
	,VEHI.Placa AS PlacaVehiculo                                              
	,SEMI.Placa AS SemiRemolPlaca                                              
	,ISNULL(COND.Razon_Social, '') + ' ' + ISNULL(COND.Nombre, '')                                              
	+ ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') AS NombreConductor                                              
	,COND.Numero_Identificacion AS IdentificacionConductor
	,ENPD.TERC_Codigo_Tenedor
	,ISNULL(TENE.Razon_Social, '') + ' ' + ISNULL(TENE.Nombre, '')                                          
	+ ' ' + ISNULL(TENE.Apellido1, '') + ' ' + ISNULL(TENE.Apellido2, '') AS NombreTenedor                                              
	,TENE.Numero_Identificacion AS IdentificacionTenedor                                              
	,RUTA.Nombre AS NombreRuta         
	,RUTA.Duracion_Horas     
	,                                   
	---- REMESA                                                   
	ISNULL(ENRE.Numero_Documento, 0) AS NumeroRemesa                                              
	,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '')                                              
	+ ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                              
	,ISNULL(PRTR.Nombre, '') AS NombreProducto                                              
	,ISNULL(ENRE.Cantidad_Cliente, 0) AS CantidadRemesa                                              
	,ISNULL(ENRE.Peso_Cliente, 0) AS PesoRemesa
	     /*Modificación FP 2021/12/12*/                        
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')        
	+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
  /*Fin Modificación*/                     
	,CASE WHEN ENPD.ECPD_Numero IS NULL THEN 0 WHEN ECPD.Estado = 0 THEN 0 ELSE ENPD.ECPD_Numero END AS ECPD_Numero                                              
	,ISNULL(ECPD.Numero_Documento,0) AS NumeroCumplido                                              
	,ISNULL(ENPD.ENMC_Numero,0) AS ENMC_Numero                                              
	,ISNULL(ENMC.Numero_Documento,0) AS NumeroManifiesto                                              
	,ISNULL(ENPD.ELPD_Numero,0) AS ELPD_Numero                                              
	,ISNULL(ELPD.Numero_Documento,0) AS NumeroLiquidacion                                    
	,Valor_Seguro_Poliza                                  
	,ENPD.Anticipo_Pagado_A                                
	,CASE WHEN ENDC.Codigo_Alterno = 'TRANSFERENCIA' THEN 1 ELSE 0 END AS PagoAnticipo                            
	,(SELECT SUM(CURE.Peso_Descargue)FROM Cumplido_Remesas AS CURE                         
	INNER JOIN Encabezado_Remesas AS ENRE ON                       
	CURE.EMPR_Codigo = ENRE.EMPR_Codigo                         
	AND CURE.ENRE_Numero = ENRE.Numero                         
	WHERE ENRE.EMPR_Codigo=ENPD.EMPR_Codigo                        
	and ENRE.ENPD_Numero = ENPD.Numero)                         
	AS PesoCumplido                        
	,                        
	(SELECT SUM(CURE.Peso_Cargue)FROM Cumplido_Remesas AS CURE                        
	INNER JOIN Encabezado_Remesas AS ENRE ON                         
	CURE.EMPR_Codigo = ENRE.EMPR_Codigo                         
	AND CURE.ENRE_Numero = ENRE.Numero                         
	WHERE ENRE.EMPR_Codigo=ENPD.EMPR_Codigo                         
	and ENRE.ENPD_Numero = ENPD.Numero)                         
	AS PesoCargue                        
	,                        
	(SELECT SUM(CURE.Peso_Faltante)FROM Cumplido_Remesas AS CURE                         
	INNER JOIN Encabezado_Remesas AS ENRE ON                         
	CURE.EMPR_Codigo = ENRE.EMPR_Codigo                         
	AND CURE.ENRE_Numero = ENRE.Numero                         
	WHERE ENRE.EMPR_Codigo=ENPD.EMPR_Codigo                         
	and ENRE.ENPD_Numero = ENPD.Numero)                         
	AS PesoFaltante                       
                      
	,USCU.Nombre as UsuarioCumplido                        
	,CIOR.Nombre AS CiudadOrigen                        
	,CIDE.Nombre AS CiudadDestino                        
	,ECPD.Fecha_Crea AS FechaCumplido                        
	,CASE WHEN ENPD.TATC_Codigo_Compra = 301 THEN (ENPD.Valor_Flete_Transportador/ ENPD.Peso)*1000 ELSE ENPD.Valor_Flete_Transportador END AS FleteUnitario                        
	,CASE WHEN ENPD.TATC_Codigo_Compra = 301 THEN 'Cupo Tonelada' ELSE 'Cantidad Viajes' END AS TipoDespacho             
	,ENPD.ELGC_Numero      
                                              
	FROM Encabezado_Planilla_Despachos ENPD                                              
	LEFT JOIN Encabezado_Remesas ENRE                                              
	ON ENPD.EMPR_Codigo = ENRE.EMPR_Codigo                                              
	AND ENPD.Numero = ENRE.ENPD_Numero                                              
                                              
	---- PLACA VEHICULO                                                  
	LEFT JOIN Vehiculos VEHI                                              
	ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo                                              
	AND ENPD.VEHI_Codigo = VEHI.Codigo             
                                             
	---- SEMIREMOLQUE                                                  
	LEFT JOIN Semirremolques SEMI                                              
	ON ENPD.EMPR_Codigo = SEMI.EMPR_Codigo                                              
	AND ENPD.SEMI_Codigo = SEMI.Codigo                                    
	---- TENEDOR                                                  
	LEFT JOIN Terceros TENE                             
	ON ENPD.EMPR_Codigo = TENE.EMPR_Codigo                                              
	AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo                                              
	---- CONDUCTOR                                                
	LEFT JOIN Terceros COND                                              
	ON ENPD.EMPR_Codigo = COND.EMPR_Codigo                                              
	AND ENPD.TERC_Codigo_Conductor = COND.Codigo                                              
	---- LINEA DE NEGOCIO                                                  
	LEFT JOIN Linea_Negocio_Transporte_Carga LNTC                                              
	ON ENPD.EMPR_Codigo = LNTC.EMPR_Codigo                                              
	AND ENPD.LNTC_Codigo_Compra = LNTC.Codigo                                              
	---- TIPO DE LINEA DE NEGOCIO                                                  
	LEFT JOIN Tipo_Linea_Negocio_Carga TLNC                                              
	ON ENPD.EMPR_Codigo = TLNC.EMPR_Codigo                                              
	AND ENPD.TLNC_Codigo_Compra = TLNC.Codigo                                              
	AND LNTC.Codigo = TLNC.LNTC_Codigo                               
	---- RUTA                                                   
	LEFT JOIN Rutas RUTA                                              
	ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo                                              
	AND ENPD.RUTA_Codigo = RUTA.Codigo                              
                        
	LEFT JOIN Ciudades as CIOR                        
	ON CIOR.EMPR_Codigo = RUTA.EMPR_Codigo                        
	AND CIOR.Codigo = RUTA.CIUD_Codigo_Origen                        
                        
	LEFT JOIN Ciudades as CIDE                        
	ON CIDE.EMPR_Codigo = RUTA.EMPR_Codigo                        
	AND CIDE.Codigo = RUTA.CIUD_Codigo_Destino                        
            
	LEFT JOIN Tarifa_Transporte_Carga AS TATC                  
	ON ENPD.EMPR_Codigo = TATC.EMPR_Codigo                  
	AND ENPD.TATC_Codigo_Compra = TATC.Codigo             
            
	---- TARIFARIO                                                   
	LEFT JOIN V_Tipo_Tarifa_Transporte_Carga VTTC                                              
	ON ENPD.EMPR_Codigo = VTTC.EMPR_Codigo                                              
	AND ENPD.TATC_Codigo_Compra = VTTC.TATC_Codigo                                              
	AND ENPD.TTTC_Codigo_Compra = VTTC.Codigo                                              
	---- OFICINA                                  
	LEFT JOIN Oficinas OFIC                                              
	ON ENPD.EMPR_Codigo = OFIC.EMPR_Codigo                                              
	AND ENPD.OFIC_Codigo = OFIC.Codigo                                              
	---- PRODUCTO                                                   
	LEFT JOIN Producto_Transportados PRTR                                              
	ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                         
	AND ENRE.PRTR_Codigo = PRTR.Codigo                                              
	---- CLIENTE    
	LEFT JOIN Terceros CLIE                                              
	ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                              
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                              
                                              
	---- DESTINATARIO                                                   
	LEFT JOIN Terceros DEST                                              
	ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                              
	AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                              
                                              
	LEFT JOIN Encabezado_Cumplido_Planilla_Despachos ECPD                                              
	ON ENPD.EMPR_Codigo = ENPD.EMPR_Codigo                                              
	AND ENPD.Numero = ECPD.ENPD_Numero                         
                        
	LEFT JOIN Usuarios AS USCU                        
	ON USCU.EMPR_Codigo = ECPD.EMPR_Codigo                        
	AND USCU.Codigo = ECPD.USUA_Codigo_Crea                        
                                              
	LEFT JOIN Encabezado_Manifiesto_Carga ENMC                                              
	ON ENPD.EMPR_Codigo = ENMC.EMPR_Codigo                                              
	AND ENPD.ENMC_Numero = ENMC.Numero                                              
                                  
	LEFT JOIN Encabezado_Liquidacion_Planilla_Despachos ELPD                                              
	ON ENPD.EMPR_Codigo = ELPD.EMPR_Codigo                                              
	AND ENPD.ELPD_Numero = ELPD.Numero                                              
                              
	LEFT JOIN Encabezado_Documento_Cuentas AS ENDC                              
	ON ENDC.CATA_DOOR_Codigo = 2604                              
	AND ENDC.EMPR_Codigo = ENPD.EMPR_Codigo                     
	AND ENDC.Codigo_Documento_Origen = ENPD.Numero                              
                              
	WHERE ENPD.EMPR_Codigo = @par_EMPR_Codigo                                              
	AND ENPD.TIDO_Codigo = @par_TIDO_Codigo                                              
	AND ENPD.Numero = @Numero                                        
                                        
	--SELECT @Numero AS Numero                                              
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesa_paqueteria_pendientes_control_entregas]
(                            
 @par_EMPR_Codigo SMALLINT,  
 @par_Numero numeric = NULL,  
 @par_FechaInicial Date = NULL,  
 @par_FechaFinal Date = NULL,  
 @par_NumeroPlanillaDespacho numeric = NULL,  
 @par_NumeroPlanillaEntrega numeric = NULL,  
 @par_NumeroPlanilla numeric = null,
 @par_VEHI_Codigo numeric= NULL,  
 @par_Ciudad_Origen varchar (50) = NULL,  
 @par_Ciudad_Destino varchar (50) = NULL,  
 @par_TERC_Codigo_Cliente numeric = NULL,  
 @par_Documento_Cliente varchar(50) = NULL,  
 @par_CATA_TERP_Codigo numeric = NULL,  
 @par_OFIC_Codigo numeric = NULL,  
 @par_Usuario_Consulta INT  = NULL,   
 @par_NumeroPagina INT = NULL,  
 @par_RegistrosPagina INT = NULL  
)                            
AS                                     
BEGIN                            
 IF @par_FechaInicial <> NULL BEGIN                            
  SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                            
 END                            
                            
 IF @par_FechaFinal <> NULL BEGIN                            
  SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                            
 END                            
                            
 SET NOCOUNT ON;                                  
 DECLARE @CantidadRegistros INT                            
 SELECT                            
 @CantidadRegistros = (SELECT DISTINCT                            
  COUNT(1)                            
  FROM Remesas_Paqueteria ENRP  
  
  INNER JOIN Encabezado_Remesas AS ENRE  
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo  
  AND ENRP.ENRE_Numero = ENRE.Numero  
  
  LEFT JOIN Terceros AS CLIE  
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo  
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo  
  
  INNER JOIN Encabezado_Planilla_Despachos AS ENPD  
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo  
  AND ENRE.ENPD_Numero = ENPD.Numero  
  AND ENPD.TIDO_Codigo = 135 --Planilla Despacho Paqueteria  
  
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPE  
  ON ENRE.EMPR_Codigo = ENPE.EMPR_Codigo  
  AND ENRE.ENPE_Numero = ENPE.Numero  
  AND ENPE.TIDO_Codigo = 210 --Planilla Entregas Paqueteria  
  
  LEFT JOIN Ciudades CIOR  
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo  
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo  
  
  LEFT JOIN Ciudades CIDE  
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo  
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo     
  
  LEFT JOIN Valor_Catalogos TERP   
  ON ENRP.EMPR_Codigo = TERP.EMPR_Codigo  
  AND ENRP.CATA_TERP_Codigo = TERP.Codigo  
  
  LEFT JOIN                  
  Oficinas OFIC ON                  
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                  
  AND ENRE.OFIC_Codigo = OFIC.Codigo  
  
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo  
  AND (ENRE.Numero_Documento = @par_Numero OR @par_Numero IS NULL)  
  AND (ENRE.Fecha >= @par_FechaInicial OR @par_FechaInicial IS NULL)  
  AND (ENRE.Fecha <= @par_FechaFinal OR @par_FechaFinal IS NULL)  
  AND (ENPD.Numero_Documento = @par_NumeroPlanilla OR @par_NumeroPlanilla IS NULL)    
  AND (ENRE.VEHI_Codigo = @par_VEHI_Codigo OR @par_VEHI_Codigo IS NULL)  
  AND (CIOR.Codigo = @par_Ciudad_Origen OR @par_Ciudad_Origen IS NULL)  
  AND (CIDE.Codigo = @par_Ciudad_Destino OR @par_Ciudad_Destino IS NULL)  
  AND (ENRE.TERC_Codigo_Cliente = @par_TERC_Codigo_Cliente OR @par_TERC_Codigo_Cliente IS NULL)  
  AND (CLIE.Numero_Identificacion like  '%' + @par_Documento_Cliente + '%' OR @par_Documento_Cliente IS NULL)  
  AND (ENRP.CATA_TERP_Codigo = @par_CATA_TERP_Codigo OR @par_CATA_TERP_Codigo IS NULL)  
  AND (OFIC.Codigo IN (SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE EMPR_Codigo = @par_EMPR_Codigo AND USOF.USUA_Codigo = @par_Usuario_Consulta) OR @par_Usuario_Consulta IS NULL)               
  AND ENRE.Estado = 1  
  AND ENRE.Anulado = 0  
  AND (ENRE.Entregado = 0 OR ENRE.Entregado IS NULL)  
 );                            
 WITH Pagina                            
 AS                            
 (  
  SELECT DISTINCT  
  ENRE.Numero,  
  ENRE.Numero_Documento,  
  ENRE.fecha,  
  ENRE.Observaciones,  
  ISNULL(ENPD.Numero, 0) ENPD_Numero,  
  ISNULL(ENPD.Numero_Documento, 0) NumeroDocumentoPlanilla,  
  ISNULL(ENPE.Numero, 0) ENPE_Numero,  
  ISNULL(ENPE.Numero_Documento, 0) PlanillaEntrega,  
  ENRE.TERC_Codigo_Cliente,  
  ISNULL(CLIE.Razon_Social,'')+' '+ISNULL(CLIE.Nombre,'') +' '+ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,'') AS NombreCliente,    
  ENRE.PRTR_Codigo,  
  PRTR.Nombre NombreProducto,  
  ENRE.Cantidad_Cliente,  
  ENRE.Peso_Cliente,  
  ISNULL(COND.Razon_Social,'')+' '+ISNULL(COND.Nombre,'') +' '+ISNULL(COND.Apellido1,'')+' '+ISNULL(COND.Apellido2,'') AS NombreConductor,   
  ENRE.VEHI_Codigo,  
  VEHI.Placa,  
  CIDE.Nombre CiudadDestino,
	/*Modificación FP 2021/12/12*/                        
	ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')        
	+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario,  
  /*Fin Modificación*/  
  ENRP.Nombre_Recibe,  
  ENRE.OFIC_Codigo,                  
  OFIC.Nombre As NombreOficinaActual,      
  
  ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber                            
  FROM Remesas_Paqueteria ENRP  
  
  INNER JOIN Encabezado_Remesas AS ENRE  
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo  
  AND ENRP.ENRE_Numero = ENRE.Numero  
  
  LEFT JOIN Terceros AS CLIE  
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo  
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo  
  
  LEFT JOIN Terceros AS COND  
  ON ENRE.EMPR_Codigo = COND.EMPR_Codigo  
  AND ENRE.TERC_Codigo_Conductor = COND.Codigo  
  
  LEFT JOIN Terceros AS DEST  
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo  
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo  
  
  INNER JOIN Encabezado_Planilla_Despachos AS ENPD  
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo  
  AND ENRE.ENPD_Numero = ENPD.Numero  
  AND ENPD.TIDO_Codigo = 135 --Planilla Despacho Paqueteria  
  
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPE  
  ON ENRE.EMPR_Codigo = ENPE.EMPR_Codigo  
  AND ENRE.ENPE_Numero = ENPE.Numero  
  AND ENPE.TIDO_Codigo = 210 --Planilla Entregas Paqueteria  
  
  LEFT JOIN Ciudades CIOR  
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo  
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo  
  
  LEFT JOIN Ciudades CIDE  
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo  
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo     
  
  LEFT JOIN Valor_Catalogos TERP   
  ON ENRP.EMPR_Codigo = TERP.EMPR_Codigo  
  AND ENRP.CATA_TERP_Codigo = TERP.Codigo  
  
  LEFT JOIN Producto_Transportados PRTR   
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo  
  AND ENRE.PRTR_Codigo = PRTR.Codigo  
  
  LEFT JOIN Vehiculos VEHI   
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo  
  AND ENRE.VEHI_Codigo = VEHI.Codigo  
  
  LEFT JOIN                  
  Oficinas OFIC ON                  
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                  
  AND ENRE.OFIC_Codigo = OFIC.Codigo     
  
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo  
  AND (ENRE.Numero_Documento = @par_Numero OR @par_Numero IS NULL)  
  AND (ENRE.Fecha >= @par_FechaInicial OR @par_FechaInicial IS NULL)  
  AND (ENRE.Fecha <= @par_FechaFinal OR @par_FechaFinal IS NULL)  
  AND (ENPD.Numero_Documento = @par_NumeroPlanilla OR @par_NumeroPlanilla IS NULL)    
  AND (ENRE.VEHI_Codigo = @par_VEHI_Codigo OR @par_VEHI_Codigo IS NULL)  
  AND (CIOR.Codigo = @par_Ciudad_Origen OR @par_Ciudad_Origen IS NULL)  
  AND (CIDE.Codigo = @par_Ciudad_Destino OR @par_Ciudad_Destino IS NULL)  
  AND (ENRE.TERC_Codigo_Cliente = @par_TERC_Codigo_Cliente OR @par_TERC_Codigo_Cliente IS NULL)  
  AND (CLIE.Numero_Identificacion like  '%' + @par_Documento_Cliente + '%' OR @par_Documento_Cliente IS NULL)  
  AND (ENRP.CATA_TERP_Codigo = @par_CATA_TERP_Codigo OR @par_CATA_TERP_Codigo IS NULL)  
  AND (OFIC.Codigo IN (SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE EMPR_Codigo = @par_EMPR_Codigo AND USOF.USUA_Codigo = @par_Usuario_Consulta) OR @par_Usuario_Consulta IS NULL)               
  AND ENRE.Estado = 1  
  AND ENRE.Anulado = 0  
  AND (ENRE.Entregado = 0 OR ENRE.Entregado IS NULL)  
 )  
 SELECT DISTINCT    
 Numero,  
 Numero_Documento,  
 Fecha,  
 Observaciones,  
 ENPD_Numero,  
 NumeroDocumentoPlanilla,  
 ENPE_Numero,  
 PlanillaEntrega,  
 TERC_Codigo_Cliente,  
 NombreCliente,    
 PRTR_Codigo,  
 NombreProducto,  
 Cantidad_Cliente,  
 Peso_Cliente,  
 NombreConductor,   
 VEHI_Codigo,  
 Placa,  
 CiudadDestino,  
 NombreDestinatario,   
 Nombre_Recibe,  
 OFIC_Codigo,                  
 NombreOficinaActual,    
                           
 @CantidadRegistros AS TotalRegistros,  
 @par_NumeroPagina AS PaginaObtener,  
 @par_RegistrosPagina AS RegistrosPagina  
 FROM Pagina  
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
 ORDER BY Numero ASC  
END     
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesa_paqueteria_pendientes_control_entrega]
(                          
	@par_EMPR_Codigo SMALLINT,
	@par_Numero numeric = NULL,
	@par_FechaInicial Date = NULL,
	@par_FechaFinal Date = NULL,
	@par_NumeroPlanillaDespacho numeric = NULL,
	@par_NumeroPlanillaEntrega numeric = NULL,
	@par_VEHI_Codigo numeric= NULL,
	@par_Ciudad_Origen varchar (50) = NULL,
	@par_Ciudad_Destino varchar (50) = NULL,
	@par_TERC_Codigo_Cliente numeric = NULL,
	@par_Documento_Cliente varchar(50) = NULL,
	@par_CATA_TERP_Codigo numeric = NULL,
	@par_OFIC_Codigo numeric = NULL,
	@par_Usuario_Consulta INT  = NULL, 
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL
)                          
AS                                   
BEGIN                          
	IF @par_FechaInicial <> NULL BEGIN                          
		SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                          
	END                          
                          
	IF @par_FechaFinal <> NULL BEGIN                          
		SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                          
	END                          
                          
	SET NOCOUNT ON;                                
	DECLARE @CantidadRegistros INT                          
	SELECT                          
	@CantidadRegistros = (SELECT DISTINCT                          
		COUNT(1)                          
		FROM Remesas_Paqueteria ENRP

		INNER JOIN Encabezado_Remesas AS ENRE
		ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo
		AND ENRP.ENRE_Numero = ENRE.Numero

		LEFT JOIN Terceros AS CLIE
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo

		LEFT JOIN Encabezado_Planilla_Despachos AS ENPD
		ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo
		AND ENRE.ENPD_Numero = ENPD.Numero
		AND ENPD.TIDO_Codigo = 130 --Planilla Despacho Paqueteria

		LEFT JOIN Encabezado_Planilla_Despachos AS ENPE
		ON ENRE.EMPR_Codigo = ENPE.EMPR_Codigo
		AND ENRE.ENPE_Numero = ENPE.Numero
		AND ENPE.TIDO_Codigo = 210 --Planilla Entregas Paqueteria

		LEFT JOIN Ciudades CIOR
		ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo
		AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo

		LEFT JOIN Ciudades CIDE
		ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo
		AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo   

		LEFT JOIN Valor_Catalogos TERP 
		ON ENRP.EMPR_Codigo = TERP.EMPR_Codigo
		AND ENRP.CATA_TERP_Codigo = TERP.Codigo

		LEFT JOIN                
		Oficinas OFIC ON                
		ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                
		AND ENRE.OFIC_Codigo = OFIC.Codigo

		WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo
		AND (ENRE.Numero_Documento = @par_Numero OR @par_Numero IS NULL)
		AND (ENRE.Fecha >= @par_FechaInicial OR @par_FechaInicial IS NULL)
		AND (ENRE.Fecha <= @par_FechaFinal OR @par_FechaFinal IS NULL)
		AND (ENPD.Numero_Documento = @par_NumeroPlanillaDespacho OR @par_NumeroPlanillaDespacho IS NULL)
		AND (ENPE.Numero_Documento = @par_NumeroPlanillaEntrega OR @par_NumeroPlanillaEntrega IS NULL)
		AND (ENRE.VEHI_Codigo = @par_VEHI_Codigo OR @par_VEHI_Codigo IS NULL)
		AND (CIOR.Codigo = @par_Ciudad_Origen OR @par_Ciudad_Origen IS NULL)
		AND (CIDE.Codigo = @par_Ciudad_Destino OR @par_Ciudad_Destino IS NULL)
		AND (ENRE.TERC_Codigo_Cliente = @par_TERC_Codigo_Cliente OR @par_TERC_Codigo_Cliente IS NULL)
		AND (CLIE.Numero_Identificacion like  '%' + @par_Documento_Cliente + '%' OR @par_Documento_Cliente IS NULL)
		AND (ENRP.CATA_TERP_Codigo = @par_CATA_TERP_Codigo OR @par_CATA_TERP_Codigo IS NULL)
		AND (OFIC.Codigo IN (SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE EMPR_Codigo = @par_EMPR_Codigo AND USOF.USUA_Codigo = @par_Usuario_Consulta) OR @par_Usuario_Consulta IS NULL)             
		AND ENRE.Estado = 1
		AND ENRE.Anulado = 0
		AND (ENRE.Entregado = 0 OR ENRE.Entregado IS NULL)
	);                          
	WITH Pagina                          
	AS                          
	(
		SELECT DISTINCT
		ENRE.Numero,
		ENRE.Numero_Documento,
		ENRE.fecha,
		ENRE.Observaciones,
		ISNULL(ENPD.Numero, 0) ENPD_Numero,
		ISNULL(ENPD.Numero_Documento, 0) NumeroDocumentoPlanilla,
		ISNULL(ENPE.Numero, 0) ENPE_Numero,
		ISNULL(ENPE.Numero_Documento, 0) PlanillaEntrega,
		ENRE.TERC_Codigo_Cliente,
		ISNULL(CLIE.Razon_Social,'')+' '+ISNULL(CLIE.Nombre,'') +' '+ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,'') AS NombreCliente,  
		ENRE.PRTR_Codigo,
		PRTR.Nombre NombreProducto,
		ENRE.Cantidad_Cliente,
		ENRE.Peso_Cliente,
		ISNULL(COND.Razon_Social,'')+' '+ISNULL(COND.Nombre,'') +' '+ISNULL(COND.Apellido1,'')+' '+ISNULL(COND.Apellido2,'') AS NombreConductor, 
		ENRE.VEHI_Codigo,
		VEHI.Placa,
		CIDE.Nombre CiudadDestino,
		
		/*Modificación FP 2021/12/12*/                        
		ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')        
		+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario,  
	  /*Fin Modificación*/ 
		ENRP.Nombre_Recibe,
		ENRE.OFIC_Codigo,                
		OFIC.Nombre As NombreOficinaActual,    

		ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber                          
		FROM Remesas_Paqueteria ENRP

		INNER JOIN Encabezado_Remesas AS ENRE
		ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo
		AND ENRP.ENRE_Numero = ENRE.Numero

		LEFT JOIN Terceros AS CLIE
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo

		LEFT JOIN Terceros AS COND
		ON ENRE.EMPR_Codigo = COND.EMPR_Codigo
		AND ENRE.TERC_Codigo_Conductor = COND.Codigo

		LEFT JOIN Terceros AS DEST
		ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo
		AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo

		LEFT JOIN Encabezado_Planilla_Despachos AS ENPD
		ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo
		AND ENRE.ENPD_Numero = ENPD.Numero
		AND ENPD.TIDO_Codigo = 130 --Planilla Despacho Paqueteria

		LEFT JOIN Encabezado_Planilla_Despachos AS ENPE
		ON ENRE.EMPR_Codigo = ENPE.EMPR_Codigo
		AND ENRE.ENPE_Numero = ENPE.Numero
		AND ENPE.TIDO_Codigo = 210 --Planilla Entregas Paqueteria

		LEFT JOIN Ciudades CIOR
		ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo
		AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo

		LEFT JOIN Ciudades CIDE
		ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo
		AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo   

		LEFT JOIN Valor_Catalogos TERP 
		ON ENRP.EMPR_Codigo = TERP.EMPR_Codigo
		AND ENRP.CATA_TERP_Codigo = TERP.Codigo

		LEFT JOIN Producto_Transportados PRTR 
		ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
		AND ENRE.PRTR_Codigo = PRTR.Codigo

		LEFT JOIN Vehiculos VEHI 
		ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo
		AND ENRE.VEHI_Codigo = VEHI.Codigo

		LEFT JOIN                
		Oficinas OFIC ON                
		ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                
		AND ENRE.OFIC_Codigo = OFIC.Codigo   

		WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo
		AND (ENRE.Numero_Documento = @par_Numero OR @par_Numero IS NULL)
		AND (ENRE.Fecha >= @par_FechaInicial OR @par_FechaInicial IS NULL)
		AND (ENRE.Fecha <= @par_FechaFinal OR @par_FechaFinal IS NULL)
		AND (ENPD.Numero_Documento = @par_NumeroPlanillaDespacho OR @par_NumeroPlanillaDespacho IS NULL)
		AND (ENPE.Numero_Documento = @par_NumeroPlanillaEntrega OR @par_NumeroPlanillaEntrega IS NULL)
		AND (ENRE.VEHI_Codigo = @par_VEHI_Codigo OR @par_VEHI_Codigo IS NULL)
		AND (CIOR.Codigo = @par_Ciudad_Origen OR @par_Ciudad_Origen IS NULL)
		AND (CIDE.Codigo = @par_Ciudad_Destino OR @par_Ciudad_Destino IS NULL)
		AND (ENRE.TERC_Codigo_Cliente = @par_TERC_Codigo_Cliente OR @par_TERC_Codigo_Cliente IS NULL)
		AND (CLIE.Numero_Identificacion like  '%' + @par_Documento_Cliente + '%' OR @par_Documento_Cliente IS NULL)
		AND (ENRP.CATA_TERP_Codigo = @par_CATA_TERP_Codigo OR @par_CATA_TERP_Codigo IS NULL)
		AND ENRE.Estado = 1
		AND ENRE.Anulado = 0
		AND (ENRE.Entregado = 0 OR ENRE.Entregado IS NULL)
	)
	SELECT DISTINCT  
	Numero,
	Numero_Documento,
	Fecha,
	Observaciones,
	ENPD_Numero,
	NumeroDocumentoPlanilla,
	ENPE_Numero,
	PlanillaEntrega,
	TERC_Codigo_Cliente,
	NombreCliente,  
	PRTR_Codigo,
	NombreProducto,
	Cantidad_Cliente,
	Peso_Cliente,
	NombreConductor, 
	VEHI_Codigo,
	Placa,
	CiudadDestino,
	NombreDestinatario, 
	Nombre_Recibe,
	OFIC_Codigo,                
	NombreOficinaActual,  
	                        
	@CantidadRegistros AS TotalRegistros,
	@par_NumeroPagina AS PaginaObtener,
	@par_RegistrosPagina AS RegistrosPagina
	FROM Pagina
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
	ORDER BY Numero ASC
END   
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_detalle_remesas_planilla_despachos]  
(                
	@par_EMPR_Codigo NUMERIC ,                   
	@par_Numero NUMERIC                   
)                
AS                
	BEGIN                
	SELECT                
	ENRE.EMPR_Codigo                
	,DEPD.ENPD_Numero                
	,ENRE.Numero                
	,ENRE.Numero_Documento AS NumeroRemesa              
	,ENRE.Fecha                
	,ENRE.TERC_Codigo_Cliente                
	,LTRIM(RTRIM(CONCAT(CLIE.Nombre,' ',CLIE.Apellido1,' ',CLIE.Apellido2,' ',CLIE.Razon_Social))) AS NombreCliente                
	,ISNULL(ENRE.Documento_Cliente,0) AS Documento_Cliente                
	,PRTR.Nombre AS NombreProducto                
	,ENRE.PRTR_Codigo    
	,ENRE.Cantidad_Cliente AS CantidadRemesa                
	,ENRE.Peso_Cliente AS PesoRemesa                
	,ENRE.TERC_Codigo_Destinatario
	/*Modificación FP 2021/12/12*/                        
	,ISNULL(ENRE.Nombre_Destinatario, 
	LTRIM(RTRIM(CONCAT(DEST.Nombre,' ',DEST.Apellido1,' ',DEST.Apellido2,' ',DEST.Razon_Social)))) AS NombreDestinatario 
	/*Fin Modificación*/ 
	,ENRE.Valor_Flete_Cliente
	,ENRE.Total_Flete_Transportador         
	,RUTA.Nombre AS NombreRuta  
	,RUTA.CIUD_Codigo_Destino  
	,DEST.Direccion AS DireccionDestinatario  
	,CIDE.Nombre AS CiudadDestinatario  
	,ENRE.ESOS_Numero
	,ENRE.Observaciones
	FROM              
	Detalle_Planilla_Despachos DEPD                
                 
	LEFT JOIN Encabezado_Remesas ENRE                
	ON DEPD.EMPR_Codigo = ENRE.EMPR_Codigo AND DEPD.ENRE_Numero = ENRE.Numero                
                 
	---- PRODUCTO                   
	LEFT JOIN Producto_Transportados PRTR                
	ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo AND ENRE.PRTR_Codigo = PRTR.Codigo                
                
	---- CLIENTE                   
	LEFT JOIN Terceros CLIE                
	ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                
                 
	---- DESTINATARIO                
	LEFT JOIN Terceros DEST                
	ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo          
          
	LEFT JOIN Ciudades CIDE        
	ON DEST.EMPR_Codigo = CIDE.EMPR_Codigo AND DEST.CIUD_Codigo = CIDE.Codigo        
            
	LEFT JOIN RUTAS RUTA          
	ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo AND ENRE.RUTA_Codigo = RUTA.Codigo            
                
	WHERE                
	DEPD.EMPR_Codigo = @par_EMPR_Codigo                
	AND DEPD.ENPD_Numero = @par_Numero                
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_detalle_recaudo_remesas_paqueteria]
(          
	@par_EMPR_Codigo smallint,          
	@par_ELRG_Numero NUMERIC          
)          
AS          
BEGIN        
	SELECT         
	DLRG.ELRG_Numero,        
	DLRG.ENRE_Numero AS Numero,        
	DLRG.Numero_Documento_Remesa AS Numero_Documento,        
	DLRG.CATA_FPVE_Codigo,        
	FPVE.Campo1 AS CATA_FPVE_Nombre,      
	DLRG.CATA_FPDC_Codigo,      
	DLRG.Fecha_Pago,      
	DLRG.Documento_Pago,      
	DLRG.Valor,        
	DLRG.Observaciones,        
	DLRG.Legalizo,        
	IIF(ENRE.TERC_Codigo_Cliente > 0, ENRE.TERC_Codigo_Cliente, ENRE.TERC_Codigo_Remitente) AS TERC_Codigo_Remitente,
	/*Modificación FP 2021/12/12*/ 
	ISNULL(ENRE.Nombre_Remitente, IIF(ENRE.TERC_Codigo_Cliente > 0,         
	ISNULL(CLIE.Razon_Social,'') + ' ' + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,''),
	ISNULL(REMI.Razon_Social,'') + ' ' + ISNULL(REMI.Nombre,'') + ' ' + ISNULL(REMI.Apellido1,'') + ' ' + ISNULL(REMI.Apellido2,''))        
	) AS NombreRemitente, 
	/*Fin Modificación*/ 
	ISNULL(ENRE.TERC_Codigo_Destinatario, 0) As TERC_Codigo_Destinatario,
	/*Modificación FP 2021/12/12*/                        
	ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')        
	+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario,  
	/*Fin Modificación*/ 
	ENPD.Numero_Documento AS ENDP_NumeroDocumento,    
	ELGU.Numero_Documento AS ELGU_NumeroDocumento,
	ISNULL(RESP.Codigo, 0) AS TERC_Codigo_Responsable,
	ISNULL(RESP.Razon_Social,'') + ' ' + ISNULL(RESP.Nombre,'') + ' ' + ISNULL(RESP.Apellido1,'') + ' ' + ISNULL(RESP.Apellido2,'') As TERC_Nombre_Responsable
        
	FROM Detalle_Legalizacion_Recaudo_Guias DLRG        
        
	LEFT JOIN Encabezado_Remesas ENRE ON        
	DLRG.EMPR_Codigo = ENRE.EMPR_Codigo        
	AND DLRG.ENRE_Numero = ENRE.Numero
        
	LEFT JOIN Terceros AS CLIE                                                  
	ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                  
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                  
                                                   
	LEFT JOIN Terceros AS DEST                                     
	ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                  
	AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                  
                                                   
	LEFT JOIN Terceros AS REMI                                                  
	ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                  
	AND ENRE.TERC_Codigo_Remitente = REMI.Codigo

	LEFT JOIN Terceros AS RESP                                                  
	ON DLRG.EMPR_Codigo = RESP.EMPR_Codigo                                                  
	AND DLRG.TERC_Codigo_Responsable = RESP.Codigo
        
	LEFT JOIN Valor_Catalogos AS FPVE ON    
	DLRG.EMPR_Codigo = FPVE.EMPR_Codigo    
	AND DLRG.CATA_FPVE_Codigo = FPVE.Codigo    
    
	LEFT JOIN Encabezado_Planilla_Despachos AS ENPD ON    
	ENRE.EMPR_Codigo = ENPD.EMPR_Codigo    
	AND ENRE.ENPD_Numero = ENPD.Numero    
    
	LEFT JOIN (    
	SELECT DLGU.EMPR_Codigo, DLGU.ENRE_Numero, MAX(ENLG.Numero_Documento) AS Numero_Documento  FROM Detalle_Legalizacion_Guias DLGU    
	INNER JOIN Encabezado_Legalizacion_Guias ENLG ON          
	ENLG.EMPR_Codigo = DLGU.EMPR_Codigo          
	AND ENLG.Numero = DLGU.ELGU_Numero    
	WHERE DLGU.EMPR_Codigo = @par_EMPR_Codigo  
	group by DLGU.EMPR_Codigo, DLGU.ENRE_Numero    
	) AS ELGU ON    
	ENRE.EMPR_Codigo = ELGU.EMPR_Codigo          
	AND ENRE.Numero = ELGU.ENRE_Numero    
        
	WHERE DLRG.EMPR_Codigo = @par_EMPR_Codigo    
	AND DLRG.ELRG_Numero = @par_ELRG_Numero    
END
GO

  CREATE OR ALTER  PROCEDURE  [dbo].[gsp_consultar_guia_paqueteria_control_entregas_planilla_entrega]        
      
(                                                          
 @par_EMPR_Codigo SMALLINT,                                                          
 @par_NumeroInicial numeric = NULL,                                                          
 @par_NumeroFinal numeric = NULL,                                                          
 @par_FechaInicial Date = NULL,                                                          
 @par_FechaFinal Date = NULL,                                                          
 @par_NumeroPlanillaInicial numeric = NULL,                                                          
 @par_NumeroPlanillaFinal numeric = NULL,                                                          
 @par_FechaPlanillaInicial Date = NULL,                                                          
 @par_FechaPlanillaFinal Date = NULL,                                                          
 @par_Numero numeric = NULL,                                                          
 @par_Fecha date = NULL,                                                          
 @par_Documento_Cliente varchar(30) = NULL,                                                          
 @par_NombreCliente varchar (150) = NULL,                                                          
 @par_TERC_Codigo_Cliente numeric = NULL,                                                          
 @par_PRTR_Codigo numeric = NULL,                                                          
 @par_VEHI_Codigo numeric= NULL,                                                          
 @par_NombreRuta varchar (150) = NULL,                                                          
 @par_Ciudad_Origen varchar (50) = NULL,                                                          
 @par_Ciudad_Destino varchar (50) = NULL,                                                          
 @par_RUTA_Codigo numeric= NULL,                                                          
 @par_NombreDestinatario varchar (150) = NULL,                                                          
 @par_NombreRemitente varchar(150) = NULL,                                                          
 @par_CATA_TERP_Codigo numeric = NULL,                                                          
 @par_CATA_TTRP_Codigo numeric= NULL,                                                          
 @par_CATA_TDRP_Codigo numeric= NULL,                                                          
 @par_CATA_TSRP_Codigo numeric= NULL,                                                          
 @par_ENPD_Numero numeric = NULL,                                                          
 @par_Anulado numeric= NULL,                                                          
 @par_Estado numeric = NULL,                                                          
 @par_Cumplido smallint = NULL,                                                          
 @par_Planillado smallint = NULL,                                                          
 @par_planilla_entrega INT = NULL,                                                          
 @par_planilla_recoleccion INT = NULL,                                                          
 @par_TIDO_Codigo INT = NULL,                                                          
 @par_planilla_despachos NUMERIC = NULL,                                                   
 @par_Codigo_Ciudad_Planilla_Recolecciones NUMERIC = NULL,                                                      
 @par_Codigo_Oficina_Planilla_Recolecciones NUMERIC = NULL,                                                           
 @par_NumeroPagina INT = NULL,                                              
 @par_RegistrosPagina INT = NULL,                                              
 @par_CATA_ESRP_Codigo INT = NULL,                                              
 @par_OFIC_Codigo_Actual_Usuario INT = NULL,                                            
 @par_OFIC_Codigo_Actual NUMERIC = NULL,                                            
 @par_OFIC_Codigo INT = NULL,                        
 @par_Reexpedicion INT = NULL,                        
 @par_Ciudad_Destino_Codigo INT = NULL,                    
 @par_OFIC_Codigo_Origen NUMERIC = NULL,          
        
  @par_OFIC_Codigo_Recibe  NUMERIC = NULL,        
  @par_RecogerOfficina   NUMERIC = NULL,        
  @par_Barrio   varchar (150) = NULL,        
   /*Modificación AE  9/06/2020*/                    
 @par_Numeracion varchar(50) = null,               
 /*Fin modificación*/                    
           
  @par_ZOCI_Nombre varchar(max) = null      
)                                                          
AS                                             
                                             
 IF @par_FechaInicial <> NULL BEGIN                                                          
 SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                          
 END                                                          
                       
 IF @par_FechaFinal <> NULL BEGIN                                                          
 SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                       
 END                                                          
                                                          
 SET NOCOUNT ON;                                                                
 DECLARE @CantidadRegistros INT                                        
 SELECT                                                          
 @CantidadRegistros = (SELECT DISTINCT                                       
  COUNT(1)                                                          
  FROM Remesas_Paqueteria ENRP                                                          
                                                          
  INNER JOIN Encabezado_Remesas AS ENRE                                                    
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                          
  AND ENRP.ENRE_Numero = ENRE.Numero                                                          
                                                          
  INNER JOIN Oficinas AS OFIC ON                                            
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                            
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                            
                                          
  LEFT JOIN Terceros AS CLIE                                   
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                          
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                          
                                                  
  LEFT JOIN Terceros AS REMI                                                          
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                          
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                          
                                                          
  LEFT JOIN Terceros AS DEST                                                          
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                          
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                          
                                                      
  LEFT JOIN Producto_Transportados AS PRTR                                                          
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                                          
  AND ENRE.PRTR_Codigo = PRTR.Codigo                                                          
                                                          
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                          
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                         
  AND ENRE.ENPD_Numero = ENPD.Numero                                                          
  AND ENPD.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, 130) --Planilla Paquetería                                                     
                                                          
  AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                  
  AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                                          
                                                      
  LEFT JOIN Ciudades CIOR             
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                          
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                          
                                                          
  LEFT JOIN Ciudades CIDE                                                          
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                          
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                    
                      
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                    
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                    
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                        
                                      
  INNER JOIN Oficinas AS OFAC ON                                            
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                            
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                          
                  
  LEFT JOIN Remesas_Paqueteria REPA ON                
  ENRE.EMPR_Codigo = REPA.EMPR_Codigo AND                
  ENRE.Numero = REPA.ENRE_Numero                               
                
  LEFT JOIN Zona_Ciudades ZOCI ON              
  REPA.EMPR_Codigo = ZOCI.EMPR_Codigo AND              
  REPA.ZOCI_Codigo_Entrega = ZOCI.Codigo              
              
                                 
                                                          
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                                          
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                                       
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                                          
  AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal, ENRE.Numero_Documento)                                                    
                                                          
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                          
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                                          
                                                          
                                      
  AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                                      
  AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                                                                
                                                        
  AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                          
  AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                                          
                                                          
  AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                          
  AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                            
  OR (@par_NombreCliente IS NULL))                                                          
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                                          
  AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                                          
  AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                          
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                                          
  AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                                          
                                                          
  AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo) 
  	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/
                                                          
  AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                          
  AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                  
  AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                                          
  AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                                          
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                                          
  AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                              
  AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                          
 -- AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                          
    AND ((ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero) AND @par_TIDO_Codigo = 130) OR (ENRE.ENPR_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPR_Numero) AND ISNULL(@par_TIDO_Codigo,205) = 205)     OR (ENRE.ENPE_Numero = ISNULL(
@par_planilla_despachos, ENRE.ENPE_Numero) AND @par_TIDO_Codigo = 210)         )     
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                                              
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)                    
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)                  
                                              
  AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                                                     
  AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                                        
  AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                                           
  AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)                        
  AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)                        
  AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)                      
  /*Modificación AE  9/06/2020*/                      
  AND ENRE.Numeracion LIKE ISNULL(@par_Numeracion,ENRE.Numeracion) + '%'             
  /*Fin modificación*/                    
          
             
 AND  (REPA.OFIC_Codigo_Recibe  =  @par_OFIC_Codigo_Recibe  or @par_OFIC_Codigo_Recibe IS NULL)           
  AND  ENRP.Recoger_Oficina_Destino  = ISNULL( @par_RecogerOfficina, ENRP.Recoger_Oficina_Destino )          
   AND ENRE.Barrio_Destinatario  LIKE  '%'   + ISNULL(  @par_Barrio ,ENRE.Barrio_Destinatario) + '%'               
         
          
          
  AND REPA.CATA_ESRP_Codigo IN (6001,6025,6030,6035,6010)       
   AND ((ZOCI.Nombre LIKE @par_ZOCI_Nombre +'%')OR(@par_ZOCI_Nombre IS NULL))      
 );                                                          
 WITH Pagina                                                          
 AS                                                          
 (                        
  SELECT                                                          
  ENRE.EMPR_Codigo            
  ,REPA.OFIC_Codigo_Recibe        
  ,ENRE.Numero                                                      
  ,ENRE.Numero_Documento                     
   /*Modificación AE  9/06/2020*/                     
  ,ENRE.Numeracion                    
  /*Fin modificación*/                                                        
  ,ENRE.Fecha                                                          
  ,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                                          
  ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                                          
  ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                                      
  ,ENRE.TERC_Codigo_Cliente                                                          
  ,ENRE.CATA_FOPR_Codigo                                           
  ,FOPR.Campo1 AS FormaPago                                                          
  ,ENRE.RUTA_Codigo                                                          
  ,ENRP.CIUD_Codigo_Origen                                                          
  ,ENRP.CIUD_Codigo_Destino                                                          
  ,CIOR.Nombre AS CiudadOrigen                       
  ,CIDE.Nombre AS CiudadDestino                                    
  ,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue                                    
  ,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue                                           
  ,ISNULL(ENRP.OFIC_Codigo_Origen, 0) AS OFIC_Codigo_Origen                                          
  ,ISNULL(OFIC.Nombre, '') AS NombreOficinaOrigen                                             
  ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                                             
  ,ENRP.CATA_ESRP_Codigo                                        
  ,ENRE.Observaciones                                                          
  ,(ENRE.Valor_Flete_Cliente) AS  Valor_Flete_Cliente                                                     
  ,(ENRE.Total_Flete_Cliente) AS Total_Flete_Cliente                                                          
  ,ENRE.Valor_Seguro_Cliente                                                          
  ,ENRE.Cantidad_Cliente                                                          
 ,ENRE.Peso_Cliente                 
  ,REPA.Peso_Volumetrico                
  ,REPA.Peso_A_Cobrar               
  ,REPA.CATA_NERP_Codigo            
  ,REPA.Observaciones_Recibe                                                      
  ,ENRE.Estado                        
  ,ENRE.Anulado                                                          
  ,PRTR.Nombre AS NombreProducto                                                          
  ,ENRE.Documento_Cliente                                             
  ,ENRE.OFIC_Codigo                                            
  ,OFIC.Nombre As NombreOficina                                                         
  ,'' AS NombreRuta                                            
  ,ENRE.Valor_Flete_Transportador                               
  ,ENRE.Valor_Reexpedicion                        
  ,ENRP.Reexpedicion
  /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
	/*Fin Modificación*/     
  ,ISNULL(REPA.Secuencia,0) AS Secuencia              
  ,ZOCI.Nombre AS Zona              
  ,ENRE.Telefonos_Destinatario              
  ,ENRE.Barrio_Destinatario              
  ,ENRE.Direccion_Destinatario              
  ,DEST.Numero_Identificacion AS Identificacion_Destinatario              
  ,DEST.CATA_TIID_Codigo AS Tipo_Identificacion_Destinatario              
  ,REMI.CATA_TIID_Codigo AS Tipo_Identificacion_Remitente              
  ,REMI.Numero_Identificacion AS Identificacion_Remitente              
  ,REMI.Telefonos AS Telefonos_Remitente              
  ,ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber                                                          
  FROM Remesas_Paqueteria AS ENRP                                                          
                                                          
  INNER JOIN Encabezado_Remesas AS ENRE                                                          
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                          
  AND ENRP.ENRE_Numero = ENRE.Numero                                                          
                                                      
  INNER JOIN Oficinas AS OFIC ON                                            
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                            
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                     
                                          
  LEFT JOIN Terceros AS CLIE                           
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                          
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                          
                                                          
  LEFT JOIN Terceros AS REMI                                                          
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                          
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                          
                     
  LEFT JOIN Terceros AS DEST                                                          
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                          
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                          
                                                          
  LEFT JOIN Producto_Transportados AS PRTR                                                          
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                            
  AND ENRE.PRTR_Codigo = PRTR.Codigo                       
                                                          
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                          
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                          
  AND ENRE.ENPD_Numero = ENPD.Numero     
   AND ENPD.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, 130) --Planilla Paquetería                                                                                  
                                            
  AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                                          
  AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                                          
                                                          
                                                      
  LEFT JOIN Ciudades CIOR                                                          
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo               
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                          
                                                
  LEFT JOIN Ciudades CIDE                                                          
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                        
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                    
                                       
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                    
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                    
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                                  
                                                          
  INNER JOIN Oficinas AS OFAC ON                                            
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                            
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                    
                                        
  LEFT JOIN Valor_Catalogos AS FOPR ON                                  
  ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                  
  AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                    
                  
   LEFT JOIN Remesas_Paqueteria REPA ON                
  ENRE.EMPR_Codigo = REPA.EMPR_Codigo AND                
  ENRE.Numero = REPA.ENRE_Numero                         
                
   LEFT JOIN Zona_Ciudades ZOCI ON              
  REPA.EMPR_Codigo = ZOCI.EMPR_Codigo AND              
  REPA.ZOCI_Codigo_Entrega = ZOCI.Codigo                       
                                  
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                               
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                                
                                                     
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                                          
  AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal, ENRE.Numero_Documento)                                  
                                                          
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                          
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                                          
  AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                          
  AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                                          
                                                          
  AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                                      
  AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                                                         
                                                                         
                                                        
  AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)           
  AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                                          
  OR (@par_NombreCliente IS NULL))                                                          
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                                          
  AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                              
  AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                          
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                                          
  AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                                           
                                                          
  AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo)
    	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                                          
                                                          
  AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                          
  AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                                          
  AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                               
  AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                                          
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                                          
  AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                          
  AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                          
 -- AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                          
    AND ((ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero) AND @par_TIDO_Codigo = 130) OR (ENRE.ENPR_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPR_Numero) AND ISNULL(@par_TIDO_Codigo,205) = 205)     OR (ENRE.ENPE_Numero = ISNULL(
@par_planilla_despachos, ENRE.ENPE_Numero) AND @par_TIDO_Codigo = 210)         )     
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                              
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                                                        
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)                  
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)                                              
  AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                               
  AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                                        
  AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                                       
  AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)                        
  AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)                        
  AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)                        
  /*Modificación AE  9/06/2020*/                      
  AND ENRE.Numeracion LIKE ISNULL(@par_Numeracion,ENRE.Numeracion) + '%'                    
  /*Fin modificación*/            
            
  /*Modificacion DC 16/03/2021*/          
AND  (REPA.OFIC_Codigo_Recibe  =  @par_OFIC_Codigo_Recibe  or @par_OFIC_Codigo_Recibe IS NULL)            
  AND  ENRP.Recoger_Oficina_Destino  = ISNULL( @par_RecogerOfficina, ENRP.Recoger_Oficina_Destino )          
   AND ENRE.Barrio_Destinatario  LIKE '%' + ISNULL(  @par_Barrio ,ENRE.Barrio_Destinatario) + '%'          
         
   /*Fin modificación*/            
   AND REPA.CATA_ESRP_Codigo IN (6001,6025,6030,6035,6010)               
   AND ((ZOCI.Nombre LIKE @par_ZOCI_Nombre +'%')OR(@par_ZOCI_Nombre IS NULL))      
                                                        
 )                                                          
 SELECT DISTINCT                                                          
 0 AS Obtener                                                          
 ,EMPR_Codigo           
 ,OFIC_Codigo_Recibe        
 ,Numero                                                          
 ,Numero_Documento                    
 /*Modificación AE 9/06/2020*/                    
 ,Numeracion                    
 /*Fin Modificación*/                                                        
 ,Fecha                                                          
 ,NumeroDocumentoPlanilla                                                          
 ,VEHI_Codigo                                                          
 ,NombreCliente                                                          
 ,TERC_Codigo_Cliente                                                          
 ,CATA_FOPR_Codigo                                       
 ,FormaPago                                  
 ,RUTA_Codigo                                                          
 ,CIUD_Codigo_Origen                                                          
 ,CIUD_Codigo_Destino                                                          
 ,CiudadOrigen                                                          
 ,CiudadDestino                                
 ,SICD_Codigo_Descargue                                    
 ,SitioDescargue                                         
 ,OFIC_Codigo_Origen                                            
 ,NombreOficinaOrigen                                                    
 ,Observaciones                                                          
 ,Valor_Flete_Cliente                                                          
 ,Total_Flete_Cliente                                                          
 ,Valor_Seguro_Cliente                                                          
 ,Cantidad_Cliente                                       
 ,Peso_Cliente                
 ,Peso_Volumetrico                
 ,Peso_A_Cobrar                                                          
 ,Estado                                      
 ,Anulado                              
 ,NombreProducto                                                 
 ,Documento_Cliente                                           
 ,OFIC_Codigo                                            
 ,NombreOficina                                   
 ,NombreRuta                                                          
 ,NombreOficinaActual                                        
 ,CATA_ESRP_Codigo                                         
 ,Valor_Flete_Transportador           
 ,NombreRemitente                               
 ,Valor_Reexpedicion                        
 ,Reexpedicion                            
 , NombreDestinatario                        
 ,Secuencia                      
 ,Zona              
 ,Telefonos_Destinatario               
 ,Barrio_Destinatario              
 ,Direccion_Destinatario              
 ,Identificacion_Destinatario              
 ,Tipo_Identificacion_Destinatario              
 ,Tipo_Identificacion_Remitente              
 ,Identificacion_Remitente              
 ,Telefonos_Remitente              
 ,CATA_NERP_Codigo             
 ,Observaciones_Recibe              
              
 ,@CantidadRegistros AS TotalRegistros                                                          
 ,@par_NumeroPagina AS PaginaObtener                                                          
 ,@par_RegistrosPagina AS RegistrosPagina                                                          
 FROM Pagina                                                          
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                          
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                          
 ORDER BY Numero ASC        
 
 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_guias_norecibidas_paqueteria]          
(                                                  
  @par_EMPR_Codigo SMALLINT,                                                  
  @par_NumeroInicial numeric = NULL,                                                  
  @par_NumeroFinal numeric = NULL,                                                  
  @par_FechaInicial Date = NULL,                                                  
  @par_FechaFinal Date = NULL,                                                  
  @par_NumeroPlanillaInicial numeric = NULL,                                                  
  @par_NumeroPlanillaFinal numeric = NULL,                                                  
  @par_FechaPlanillaInicial Date = NULL,                                                  
  @par_FechaPlanillaFinal Date = NULL,                                                  
  @par_Numero numeric = NULL,                                                  
  @par_Fecha date = NULL,                                                  
  @par_Documento_Cliente varchar(30) = NULL,                                                  
  @par_NombreCliente varchar (150) = NULL,                                                  
  @par_TERC_Codigo_Cliente numeric = NULL,                                                  
  @par_PRTR_Codigo numeric = NULL,                                                  
  @par_VEHI_Codigo numeric= NULL,                                                  
  @par_NombreRuta varchar (150) = NULL,                                                  
  @par_RUTA_Codigo numeric= NULL,                                                  
  @par_NombreDestinatario varchar (150) = NULL,                                                  
  @par_NombreRemitente varchar(150) = NULL,                                                  
  @par_CATA_TERP_Codigo numeric = NULL,                                                  
  @par_CATA_TTRP_Codigo numeric= NULL,                                                  
  @par_CATA_TDRP_Codigo numeric= NULL,                                                  
  @par_CATA_TSRP_Codigo numeric= NULL,                                                  
  @par_ENPD_Numero numeric = NULL,                                                  
  @par_Anulado numeric= NULL,                                                  
  @par_Estado numeric = NULL,                                                  
  @par_Cumplido smallint = NULL,                                                  
  @par_Planillado smallint = NULL,                                                  
  @par_planilla_entrega INT = NULL,                                                  
  @par_planilla_recoleccion NUMERIC = NULL,                                                  
  @par_TIDO_Codigo INT = NULL,                                                  
  @par_planilla_despachos NUMERIC = NULL,                                                  
  @par_NumeroPagina INT = NULL,                                                  
  @par_RegistrosPagina INT = NULL,                                                 
  @par_OFIC_Codigo INT = NULL ,                                                 
  @par_CATA_ESRP_Codigo INT = NULL,                                          
  @par_ZOCI_Codigo INT =NULL,                                          
  @par_CIUD_Codigo INT =NULL,                                    
  @par_CIUD_Codigo_Origen INT = NULL,                                   
  @par_CIUD_Codigo_Actual INT = NULL,                                  
  @par_OFIC_Codigo_Actual INT = NULL ,                                  
  @par_OFIC_Codigo_Origen  INT = NULL,                                  
  @par_OFIC_Codigo_Actual_Usuario INT = NULL                                  
                                  
)                                                  
                                                
AS                                                           
BEGIN                                     
 IF @par_FechaInicial <> NULL BEGIN                    
  SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                           
 END                                                  
              
 IF @par_FechaFinal <> NULL BEGIN                                                  
  SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                                  
 END                   
                                
                    
                                
                                    
 SET NOCOUNT ON;                                     
 DECLARE @CantidadRegistros INT                                  
 SELECT                                                  
  @CantidadRegistros = (                              
  SELECT DISTINCT                                                  
    COUNT(1)                                                  
   FROM Remesas_Paqueteria ENRP                            
                                                 
                                                  
   INNER JOIN Encabezado_Remesas AS ENRE                                                  
    ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                  
    AND ENRP.ENRE_Numero = ENRE.Numero                                             
                                              
   LEFT JOIN Terceros AS CLIE                                                  
    ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                  
    AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                  
                                                  
   LEFT JOIN Terceros AS REMI                                                  
    ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                  
    AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                  
                                                  
   LEFT JOIN Terceros AS DEST                                                  
    ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                  
    AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                  
                                                  
   LEFT JOIN Rutas AS RUTA                                                  
    ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                                                  
    AND ENRE.RUTA_Codigo = RUTA.Codigo                                                  
                                           
   LEFT JOIN Producto_Transportados AS PRTR                                                  
    ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                                  
    AND ENRE.PRTR_Codigo = PRTR.Codigo                                                  
                                                  
   INNER JOIN Encabezado_Planilla_Despachos AS ENPD                                                  
    ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                  
    AND ENRE.ENPD_Numero = ENPD.Numero                                                  
    AND ENPD.TIDO_Codigo = 135                                                     
                                            
   LEFT JOIN Ciudades CIOR                                                  
   ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                  
   AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                  
                                                  
  LEFT JOIN Ciudades CIDE                                                  
   ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                  
   AND ENRP.CIUD_Codigo_Destino  = CIDE.Codigo          
             
   LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON     
   ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo          
   AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo          
                                              
  LEFT JOIN Encabezado_Planilla_Despachos ENPR                                              
 ON ENRE.EMPR_Codigo = ENPR.EMPR_Codigo                                              
 AND ENRE.ENPR_Numero = ENPR.Numero
 
 
  LEFT JOIN Oficinas OFRE ON
 ENRE.EMPR_Codigo = OFRE.EMPR_Codigo AND
 ENRP.OFIC_Codigo_Recibe = OFRE.Codigo                                              
                                                  
   WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                                  
   AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                                  
   AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal, ENRE.Numero_Documento)                                                  
                                          
   AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                  
   AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                                  
                                                  
   AND ENRE.ENPD_Numero >= ISNULL(@par_NumeroPlanillaInicial, ENRE.ENPD_Numero)                                                  
   AND ENRE.ENPD_Numero <= ISNULL(@par_NumeroPlanillaFinal, ENRE.ENPD_Numero)                                                  
   AND (ENRP.CATA_ESRP_Codigo IN (6010,6015,6001,6035))                      
                                                
                                                    
   AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                  
      AND (ENPR.Numero_Documento = @par_planilla_recoleccion OR @par_planilla_recoleccion IS NULL )                                 
                                     
   AND (ENPD.Numero_Documento = @par_planilla_despachos OR @par_planilla_despachos IS NULL)                
   AND (ENRE.ENPR_Numero <>0  OR ENRE.ENPR_Numero IS NULL                          
   OR ENRE.ENPD_Numero <>0)                              
   AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                  
   AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                                  
   OR (@par_NombreCliente IS NULL))                                                  
   AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                                  
   AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                                 
   AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                  
   AND RUTA.Nombre LIKE '%' + ISNULL(@par_NombreRuta, RUTA.Nombre) + '%'                                                  
                                                  
   AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo)
     	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                                
                                                  
   AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                  
   AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                                  
   AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                                  
   AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                                  
   AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                          
   AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                  
   AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                  
   AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                  
                                               
 --   AND ENRP.OFIC_Codigo_Destino = ISNULL (@par_OFIC_Codigo,ENRP.OFIC_Codigo_Destino)                                    
 --AND (ENRP.OFIC_Codigo_Actual = ISNULL (@par_OFIC_Codigo_Actual,ENRP.OFIC_Codigo_Actual)                                   
 --AND ENRP.OFIC_Codigo_Actual <> @par_OFIC_Codigo_Actual_Usuario)                      
 -- AND ENRP.OFIC_Codigo_Origen = ISNULL (@par_OFIC_Codigo_Origen,ENRP.OFIC_Codigo_Origen)                                    
 --AND ENRP.CIUD_Codigo_Origen = ISNULL( @par_CIUD_Codigo_Origen,ENRP.CIUD_Codigo_Origen)                                   
                              
    AND ENRP.CIUD_Codigo_Destino = ISNULL(@par_CIUD_Codigo,ENRP.CIUD_Codigo_Destino)                                         
    AND ENRP.ZOCI_Codigo_Entrega = ISNULL(@par_ZOCI_Codigo,ENRP.ZOCI_Codigo_Entrega)                                          
    AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                    
   );                                                  
 WITH Pagina                                                  
 AS                                                  
(SELECT                                                  
   ENRE.EMPR_Codigo                                                  
     ,ENRE.Numero                                                  
     ,ENRE.Numero_Documento                                 
     ,ENRE.Fecha                                                  
     ,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                      
     ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo
	 ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                                  
	   /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	/*Fin Modificación*/ 
	,ENRE.TERC_Codigo_Cliente          
  ,ENRE.TERC_Codigo_Remitente                                          
     ,ENRE.CATA_FOPR_Codigo      
  ,FOPA.Campo1 AS FormaPago                                                
     ,ENRE.RUTA_Codigo                                                  
     ,ENRE.CIUD_Codigo_Remitente   CIUD_Codigo_Origen                                              
     ,ENRE.CIUD_Codigo_Destinatario     CIUD_Codigo_Destino                                            
     ,CIOR.Nombre AS CiudadOrigen                                           
     ,CIDE.Nombre AS CiudadDestino          
  ,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue          
  ,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue          
     ,ENRE.Observaciones                                                  
     ,ENRE.Valor_Flete_Cliente                                                  
     ,ENRE.Total_Flete_Cliente                               
     ,ENRE.Valor_Seguro_Cliente           
     ,ENRE.Cantidad_Cliente                                                  
     ,ENRE.Peso_Cliente                                                  
     ,ENRE.Estado                                                  
     ,PRTR.Nombre AS NombreProducto                                                  
     ,ENRE.Documento_Cliente                                                  
     ,RUTA.Nombre AS NombreRuta                         
  ,ENPR.Numero_Documento as PlanillaRecoleccion   
  ,OFDE.Nombre as NombreOficinaDestino
  ,ENRP.OFIC_Codigo_Destino
  ,OFDE.Nombre as OFIC_Nombre_Recibe
  ,ENRP.OFIC_Codigo_Recibe
  ,ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber                                                  
  FROM Remesas_Paqueteria AS ENRP                             
                                                
                                                  
  INNER JOIN Encabezado_Remesas AS ENRE                                                  
 ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                   
   AND ENRP.ENRE_Numero = ENRE.Numero                                                  
                                                  
                                              
  LEFT JOIN Terceros AS CLIE                                                  
   ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                  
   AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                  
                                                  
  LEFT JOIN Terceros AS REMI                                                  
   ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                  
   AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                  
                                                  
  LEFT JOIN Terceros AS DEST                                          
   ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                  
   AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                  
                                                  
  LEFT JOIN Rutas AS RUTA                                                  
   ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                                                  
   AND ENRE.RUTA_Codigo = RUTA.Codigo                                                  
                                                  
  LEFT JOIN Producto_Transportados AS PRTR                              
   ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                                  
   AND ENRE.PRTR_Codigo = PRTR.Codigo                                                  
                                                  
  INNER JOIN Encabezado_Planilla_Despachos AS ENPD                                                  
   ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                  
   AND ENRE.ENPD_Numero = ENPD.Numero                                                  
   AND ENPD.TIDO_Codigo = 135 --PlanillaPaqueteria                                                    
                                                  
   AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                                  
   AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                                  
                                                  
  LEFT JOIN Ciudades CIOR                                                
   ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                  
   AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                  
                                                  
  LEFT JOIN Ciudades CIDE                                              
   ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                  
   AND ENRP.CIUD_Codigo_Destino  = CIDE.Codigo          
             
   LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON      
   ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo          
   AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                               
                                                
     LEFT JOIN Encabezado_Planilla_Despachos ENPR                                              
 ON ENRE.EMPR_Codigo = ENPR.EMPR_Codigo                                              
 AND ENRE.ENPR_Numero = ENPR.Numero            
     
 LEFT JOIN Valor_Catalogos FOPA ON    
 ENRE.EMPR_Codigo = FOPA.EMPR_Codigo AND    
 ENRE.CATA_FOPR_Codigo = FOPA.Codigo                   
 
 LEFT JOIN Oficinas OFDE ON
 ENRE.EMPR_Codigo = OFDE.EMPR_Codigo AND
 ENRP.OFIC_Codigo_Destino = OFDE.Codigo

  LEFT JOIN Oficinas OFRE ON
 ENRE.EMPR_Codigo = OFRE.EMPR_Codigo AND
 ENRP.OFIC_Codigo_Recibe = OFRE.Codigo
                                                  
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                                  
   AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                                  
   AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal, ENRE.Numero_Documento)                                                  
                                                  
   AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                  
   AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                                  
                            
   AND ENRE.ENPD_Numero >= ISNULL(@par_NumeroPlanillaInicial, ENRE.ENPD_Numero)                                                  
   AND ENRE.ENPD_Numero <= ISNULL(@par_NumeroPlanillaFinal, ENRE.ENPD_Numero)                                                  
   AND (ENRP.CATA_ESRP_Codigo IN (6010,6015,6001,6035))                      
                                       
  AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                  
    AND (ENPR.Numero_Documento = @par_planilla_recoleccion OR @par_planilla_recoleccion IS NULL )                                 
   AND (ENPD.Numero_Documento = @par_planilla_despachos OR @par_planilla_despachos IS NULL)                
   AND (ENRE.ENPR_Numero <>0  OR ENRE.ENPR_Numero IS NULL                          
         OR ENRE.ENPD_Numero <>0)                                               
                              
   AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                  
   AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                                  
   OR (@par_NombreCliente IS NULL))                                                  
   AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                                  
   AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                                  
   AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                  
   AND RUTA.Nombre LIKE '%' + ISNULL(@par_NombreRuta, RUTA.Nombre) + '%'                                    
                                                  
   AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo)   
     	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/

   AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                  
   AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                                  
   AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                                  
   AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                                  
   AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                                  
   AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)     
   AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                  
   AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                  
                                              
 --  AND ENRP.OFIC_Codigo_Destino = ISNULL (@par_OFIC_Codigo,ENRP.OFIC_Codigo_Destino)                                    
 --  AND (ENRP.OFIC_Codigo_Actual = ISNULL (@par_OFIC_Codigo_Actual,ENRP.OFIC_Codigo_Actual)                                   
 --AND ENRP.OFIC_Codigo_Actual <> @par_OFIC_Codigo_Actual_Usuario)                             
                                  
 --  AND ENRP.OFIC_Codigo_Origen = ISNULL (@par_OFIC_Codigo_Origen,ENRP.OFIC_Codigo_Origen)                                    
   AND ENRP.CIUD_Codigo_Origen = ISNULL( @par_CIUD_Codigo_Origen,ENRP.CIUD_Codigo_Origen)                              
                                     
   AND ENRP.CIUD_Codigo_Destino = ISNULL(@par_CIUD_Codigo,ENRP.CIUD_Codigo_Destino)                                      
 --  AND ENRP.ZOCI_Codigo_Entrega = ISNULL(@par_ZOCI_Codigo,ENRP.ZOCI_Codigo_Entrega)                       
                                     
   AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                                         
  )                                                  
                                          
 SELECT DISTINCT                                                  
  0 AS Obtener                                                  
    ,EMPR_Codigo                                                  
    ,Numero                                                  
    ,Numero_Documento                                                  
    ,Fecha                                                  
    ,NumeroDocumentoPlanilla                                                  
    ,VEHI_Codigo                                                  
    ,NombreCliente                                  
    ,TERC_Codigo_Cliente      
 ,NombreRemitente  
 ,TERC_Codigo_Remitente                                              
    ,CATA_FOPR_Codigo       
 ,FormaPago                                               
    ,RUTA_Codigo                                                     
    ,CIUD_Codigo_Origen                                                  
    ,CIUD_Codigo_Destino                                            
    ,CiudadOrigen                                                  
    ,CiudadDestino          
 ,SICD_Codigo_Descargue          
 ,SitioDescargue                                                 
    ,Observaciones                                                  
    ,Valor_Flete_Cliente                                                  
    ,Total_Flete_Cliente                                                  
    ,Valor_Seguro_Cliente                                                  
    ,Cantidad_Cliente                                                  
    ,Peso_Cliente                                                  
    ,Estado                                                  
    ,NombreProducto                                               
    ,Documento_Cliente                                                  
    ,NombreRuta                         
 ,PlanillaRecoleccion         
 ,NombreOficinaDestino
 ,OFIC_Codigo_Destino
 ,OFIC_Nombre_Recibe
 ,OFIC_Codigo_Recibe
    ,@CantidadRegistros AS TotalRegistros                                                  
    ,@par_NumeroPagina AS PaginaObtener                                                  
    ,@par_RegistrosPagina AS RegistrosPagina                                                  
 FROM Pagina                                                  
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                  
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                  
 ORDER BY Numero ASC                                                  
END          
GO

CREATE OR ALTER  PROCEDURE [dbo].[gsp_consultar_remesa_paqueteria]      
(                                                                            
 @par_EMPR_Codigo SMALLINT,                                                                            
 @par_NumeroInicial numeric = NULL,                                                                            
 @par_NumeroFinal numeric = NULL,                                                                            
 @par_FechaInicial Date = NULL,                                                                            
 @par_FechaFinal Date = NULL,            
 @par_Linea_Negocio_Paqueteria NUMERIC = NULL,            
 @par_NumeroPlanillaInicial numeric = NULL,                                                                            
 @par_NumeroPlanillaFinal numeric = NULL,                                                                            
 @par_FechaPlanillaInicial Date = NULL,                                                                            
 @par_FechaPlanillaFinal Date = NULL,                                                                            
 @par_Numero numeric = NULL,                                                                            
 @par_Fecha date = NULL,                                                                            
 @par_Documento_Cliente varchar(30) = NULL,                                                                            
 @par_NombreCliente varchar (150) = NULL,                                                                            
 @par_TERC_Codigo_Cliente numeric = NULL,                                                                            
 @par_PRTR_Codigo numeric = NULL,                                                                            
 @par_VEHI_Codigo numeric= NULL,                                                                            
 @par_NombreRuta varchar (150) = NULL,                                                                            
 @par_Ciudad_Origen varchar (50) = NULL,                                                                            
 @par_Ciudad_Destino varchar (50) = NULL,                                                                            
 @par_RUTA_Codigo numeric= NULL,                                                                            
 @par_NombreDestinatario varchar (150) = NULL,                                                                            
 @par_NombreRemitente varchar(150) = NULL,                                                                            
 @par_CATA_TERP_Codigo numeric = NULL,                                                                            
 @par_CATA_TTRP_Codigo numeric= NULL,                                                                            
 @par_CATA_TDRP_Codigo numeric= NULL,                                                                            
 @par_CATA_TSRP_Codigo numeric= NULL,                                                                            
 @par_ENPD_Numero numeric = NULL,                                                                            
 @par_Anulado numeric= NULL,                                                                            
 @par_Estado numeric = NULL,                                                                            
 @par_Cumplido smallint = NULL,                                                                            
 @par_Planillado smallint = NULL,                                                                            
 @par_planilla_entrega INT = NULL,                                                                            
 @par_planilla_recoleccion INT = NULL,                                                                            
 @par_TIDO_Codigo INT = NULL,                                                                            
 @par_planilla_despachos NUMERIC = NULL,                  
 @par_Codigo_Ciudad_Planilla_Recolecciones NUMERIC = NULL,                                                                        
 @par_Codigo_Oficina_Planilla_Recolecciones NUMERIC = NULL,                                                        
 @par_NumeroPagina INT = NULL,                                                                
 @par_RegistrosPagina INT = NULL,                                                                
 @par_CATA_ESRP_Codigo INT = NULL,                 
 @par_OFIC_Codigo_Actual_Usuario INT = NULL,                         
 @par_OFIC_Codigo_Actual NUMERIC = NULL,                             
 @par_OFIC_Codigo INT = NULL,                                          
 @par_Reexpedicion INT = NULL,                                          
 @par_CIUD_Codigo_Destino INT = NULL,                                      
 @par_OFIC_Codigo_Origen NUMERIC = NULL,                                    
 /*Modificación AE  9/06/2020*/                                      
 @par_Numeracion varchar(50) = null,                                          
 /*Fin modificación*/                                      
 @par_Forma_Pago INT = null,                            
 @par_USUA_Codigo_Consultar NUMERIC = NULL                            
)                                                                            
AS                                                                          
BEGIN                                                                            
 IF @par_FechaInicial <> NULL BEGIN                                                                            
 SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                                            
 END                                                                            
                                         
 IF @par_FechaFinal <> NULL BEGIN                                                                            
 SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                                         
 END                                                                            
                                                                            
 SET NOCOUNT ON;                                                                                  
 DECLARE @CantidadRegistros INT                                                          
 SELECT                                                                            
 @CantidadRegistros = (SELECT DISTINCT                                                         
 COUNT(1)                                                                            
 FROM Remesas_Paqueteria ENRP                                                                            
                                                                            
 INNER JOIN Encabezado_Remesas AS ENRE                                                                      
 ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                                            
 AND ENRP.ENRE_Numero = ENRE.Numero                                                                            
                                                                            
 INNER JOIN Oficinas AS OFIC ON                                                              
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                                              
 AND ENRE.OFIC_Codigo = OFIC.Codigo                                                              
                                                            
 LEFT JOIN Terceros AS CLIE                                                     
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                            
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                            
       
 LEFT JOIN Terceros AS REMI                                                                            
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                          
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                            
                                                                            
 LEFT JOIN Terceros AS DEST                           
 ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                            
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                                          
                                                                        
 LEFT JOIN Producto_Transportados AS PRTR                                                               
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                                                            
 AND ENRE.PRTR_Codigo = PRTR.Codigo                  
                                                                            
 LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                                            
 ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                                  
 AND ENRE.ENPD_Numero = ENPD.Numero                                   
 AND ENPD.TIDO_Codigo IN(130,135)--Planilla Despacho Guias, Planilla Paqueteria                  
 AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                                                   
 AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                     
                   
 LEFT JOIN Vehiculos AS VEHI                  
 ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                  
 AND ENRE.VEHI_Codigo = VEHI.Codigo                  
                                                                     
 LEFT JOIN Ciudades CIOR                                                                   
 ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                     
 AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                            
                                                                            
 LEFT JOIN Ciudades CIDE                                                                            
 ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                                            
 AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                                      
                                               
 LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                                      
 ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                                      
 AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                             
                                                             
 INNER JOIN Oficinas AS OFAC ON                                                              
 ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                                              
 AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo            
            
 LEFT JOIN Valor_Catalogos AS FOPR ON                                                    
 ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                                    
 AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo            
             
 LEFT JOIN Valor_Catalogos AS LNPA ON                                                    
 ENRP.EMPR_Codigo = LNPA.EMPR_Codigo                                                    
 AND ENRP.CATA_LNPA_Codigo = LNPA.Codigo            
                          
 LEFT JOIN Encabezado_Facturas ENFA ON                          
 ENRE.EMPR_Codigo = ENFA.EMPR_Codigo AND                          
 ENRE.ENFA_Numero = ENFA.Numero      
        LEFT JOIN Zona_Ciudades As ZOCI ON                                
 ENRP.EMPR_Codigo = ZOCI.EMPR_Codigo                                
 AND ENRP.ZOCI_Codigo_Entrega = ZOCI.Codigo                       
                      
 WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                       
 AND ENRE.Numero_Documento = ISNULL(@par_Numero,ENRE.Numero_Documento)                      
 AND ENRE.TIDO_Codigo = 110                                      
 AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                                                         
                                        
 AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                                            
 AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)            
 AND (ENRP.CATA_LNPA_Codigo = @par_Linea_Negocio_Paqueteria OR @par_Linea_Negocio_Paqueteria IS NULL)                      
 AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                      
 AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                      
                                                                          
 AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                                            
 AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                                                            
                                                                            
 AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                                            
 AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                                              
 OR (@par_NombreCliente IS NULL))                                                                            
 AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                                                            
 AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                                                            
 AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                                            
 --AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                                                            
 --AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                                                            
                                                                            
 AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo) 
   	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                                                           
                                                                            
 AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                                            
 AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                                              
 AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                                                     
 AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)           
 AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado )                                                                            
 AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                                
 AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                                            
 AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                                            
 --AND ((ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero) AND @par_TIDO_Codigo = 130) OR (ENRE.ENPR_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPR_Numero) AND ISNULL(@par_TIDO_Codigo,205) = 205)  )            
 AND ENRE.ENPR_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPR_Numero)                    
 AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                                                        
 AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                     
 AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)                                      
 AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)                                    
                        
 AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                                                          
 AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                                                             
 AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)                                          
 AND (ENRP.CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino OR @par_CIUD_Codigo_Destino IS NULL)                                          
 AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)                                        
 /*Modificación AE  9/06/2020*/                                        
 AND ENRE.Numeracion = ISNULL(@par_Numeracion,ENRE.Numeracion)                      
 AND ENRE.CATA_FOPR_Codigo = ISNULL(@par_Forma_Pago, ENRE.CATA_FOPR_Codigo)                            
 AND (ENRE.USUA_Codigo_Crea = @par_USUA_Codigo_Consultar OR ENRE.USUA_Codigo_Modifica = @par_USUA_Codigo_Consultar OR @par_USUA_Codigo_Consultar IS NULL)                            
 /*Fin modificación*/                             
 );                                                                            
 WITH Pagina                                                                            
 AS                                                                            
 (                                          
 SELECT                                                                            
 ENRE.EMPR_Codigo                                    
 ,ENRE.Numero                                                                            
 ,ENRE.Numero_Documento                                       
 /*Modificación AE  9/06/2020*/                                       
 ,ENRE.Numeracion                                      
 /*Fin modificación*/                                             
 ,ENRE.Fecha            
 ,ENRP.CATA_LNPA_Codigo            
 ,LNPA.Campo1 AS CATA_LNPA_Nombre            
 ,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                                                            
 ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                                                            
 ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                                                        
                               
 ,ENRE.TERC_Codigo_Cliente                                  
 ,ENRE.TERC_Codigo_Remitente                                                    
 ,ENRE.CATA_FOPR_Codigo                                                         
 ,FOPR.Campo1 AS FormaPago                                                                            
 ,ENRE.RUTA_Codigo                                                                            
 ,ENRP.CIUD_Codigo_Origen                                                                            
 ,ENRP.CIUD_Codigo_Destino                                                                            
 ,CIOR.Nombre AS CiudadOrigen               
 ,CIDE.Nombre AS CiudadDestino                                                      
 ,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue                                                      
 ,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue                                                             
 ,ISNULL(ENRP.OFIC_Codigo_Origen, 0) AS OFIC_Codigo_Origen                                                            
 ,ISNULL(OFIC.Nombre, '') AS NombreOficinaOrigen                                                               
 ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                                                               
 ,ENRP.CATA_ESRP_Codigo                                                          
 ,ENRE.Observaciones                                                              
 --,(ENRE.Valor_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS  Valor_Flete_Cliente                                                                       
 --,(ENRE.Total_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS Total_Flete_Cliente                          
 ,ENRE.Valor_Flete_Cliente                        
 ,ENRE.Total_Flete_Cliente                        
 ,ENRE.Valor_Seguro_Cliente                                                                            
 ,ENRE.Cantidad_Cliente                                                                            
 ,ENRE.Peso_Cliente                                   
 ,ENRP.Peso_Volumetrico                                  
 ,ENRP.Peso_A_Cobrar                                                             
 ,ENRE.Estado                                                                            
 ,ENRE.Anulado                                                                            
 ,PRTR.Nombre AS NombreProducto                                                                            
 ,ENRE.Documento_Cliente                                                               
 ,ENRE.OFIC_Codigo                                                              
 ,OFIC.Nombre As NombreOficina                                                                           
 ,'' AS NombreRuta                                                              
 ,ENRE.Valor_Flete_Transportador                                                 
 ,ENRE.Valor_Reexpedicion                                          
 ,ENRP.Reexpedicion 
	/*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
	/*Fin Modificación*/ 
	,ISNULL(ENFA.Numero, 0) AS ENFA_Numero                       
 ,ISNULL(ENFA.Numero_Documento, 0) AS ENFA_Numero_Documento                  
 ,ISNULL(VEHI.Placa, '') AS VEHI_Placa                  
 ,ISNULL(VEHI.Codigo_Alterno, '') AS VEHI_CodigoAlterno            
 ,ISNULL(ELRG.Numero_Documento, 0) ELRG_NumeroDocumento      
 ,ISNULL(ENLG.Numero_Documento, 0) AS ELGU_NumeroDocumento         
 ,ISNULL(ZOCI.Nombre, '') As Zona      
 ,ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber                                                                            
 FROM Remesas_Paqueteria AS ENRP                                                                            
                                                                            
 INNER JOIN Encabezado_Remesas AS ENRE                                        
 ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                      
 AND ENRP.ENRE_Numero = ENRE.Numero                               
                                                                        
 INNER JOIN Oficinas AS OFIC ON                                                              
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                                              
 AND ENRE.OFIC_Codigo = OFIC.Codigo                                                             
                                        
 LEFT JOIN Terceros AS CLIE                                             
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                  
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                            
                                                                            
 LEFT JOIN Terceros AS REMI                                                                            
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                            
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                            
       LEFT JOIN Terceros AS DEST                                                                            
 ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                            
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                                            
                                                                            
 LEFT JOIN Producto_Transportados AS PRTR                   
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                              
 AND ENRE.PRTR_Codigo = PRTR.Codigo                                                                            
                                                                            
 LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                                            
 ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                                  
 AND ENRE.ENPD_Numero = ENPD.Numero                                   
 AND ENPD.TIDO_Codigo IN(130,135)--Planilla Despacho Guias, Planilla Paqueteria                  
 AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                                                   
 AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                     
                   
 LEFT JOIN Vehiculos AS VEHI                  
 ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                  
 AND ENRE.VEHI_Codigo = VEHI.Codigo                                                 
                                                                
 LEFT JOIN Ciudades CIOR                                                                            
 ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                            
 AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                            
                                           
 LEFT JOIN Ciudades CIDE                                                                            
 ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                          
 AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                                      
                                                         
 LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                                      
 ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                             
 AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                                                    
                                                                            
 INNER JOIN Oficinas AS OFAC ON                                                              
 ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                                              
 AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                                      
                                                          
 LEFT JOIN Valor_Catalogos AS FOPR ON                                                    
 ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                                    
 AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo            
             
 LEFT JOIN Valor_Catalogos AS LNPA ON                                                    
 ENRP.EMPR_Codigo = LNPA.EMPR_Codigo                                                    
 AND ENRP.CATA_LNPA_Codigo = LNPA.Codigo                      
                            
 LEFT JOIN Encabezado_Facturas ENFA ON             
 ENRE.EMPR_Codigo = ENFA.EMPR_Codigo AND                          
 ENRE.ENFA_Numero = ENFA.Numero                          
                                         
 LEFT JOIN Detalle_Legalizacion_Recaudo_Guias As DLRG ON                                
 ENRE.EMPR_Codigo = DLRG.EMPR_Codigo                                
 AND ENRE.Numero = DLRG.ENRE_Numero                    
     
     
   LEFT JOIN Detalle_Legalizacion_Guias DLGU    
   on DLGU.EMPR_Codigo = ENRE.EMPR_Codigo    
   and DLGU.ENRE_Numero = ENRE.Numero    
    
 LEFT JOIN Encabezado_Legalizacion_Guias ENLG ON                  
 ENLG.EMPR_Codigo = DLGU.EMPR_Codigo                  
 AND ENLG.Numero = DLGU.ELGU_Numero       
                            
 LEFT JOIN Encabezado_Legalizacion_Recaudo_Guias As ELRG ON                                
 DLRG.EMPR_Codigo = ELRG.EMPR_Codigo                                
 AND DLRG.ELRG_Numero = ELRG.Numero      
     
 LEFT JOIN Zona_Ciudades As ZOCI ON                                
 ENRP.EMPR_Codigo = ZOCI.EMPR_Codigo                                
 AND ENRP.ZOCI_Codigo_Entrega = ZOCI.Codigo      
        
 WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                      
 AND ENRE.Numero_Documento = ISNULL(@par_Numero,ENRE.Numero_Documento)                      
 AND ENRE.TIDO_Codigo = 110                      
 AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                                                  
                                                                      
 AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                                            
 AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)            
 AND (ENRP.CATA_LNPA_Codigo = @par_Linea_Negocio_Paqueteria OR @par_Linea_Negocio_Paqueteria IS NULL)            
 AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                      
 AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                      
            
 AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                                            
 AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)              
                                             
 AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                                            
 AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                      
 OR (@par_NombreCliente IS NULL))                                                                            
 AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)          
 AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                                                
 AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                                            
 --AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                                                            
 --AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                                                             
                                                                            
 AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo)     
   	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                                                           
                                                              
 AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                                            
 AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                                                            
 AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                                                 
 AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                                                            
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado )  
 AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                                            
 AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                                            
 AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                     
 --AND ((ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero) AND @par_TIDO_Codigo = 130) OR (ENRE.ENPR_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPR_Numero) AND ISNULL(@par_TIDO_Codigo,205) = 205)  )            
 AND ENRE.ENPR_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPR_Numero)                    
 AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                                                
 AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                                                                          
 AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)                                    
 AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)                                                                
                               
 AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                                                          
 AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                                                         
 AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)                                          
 AND (ENRP.CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino OR @par_CIUD_Codigo_Destino IS NULL)                                          
 AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)                                          
 /*Modificación AE  9/06/2020*/                                        
 AND ENRE.Numeracion = ISNULL(@par_Numeracion,ENRE.Numeracion)                      
 /*Fin modificación*/                                      
 AND ENRE.CATA_FOPR_Codigo = ISNULL(@par_Forma_Pago, ENRE.CATA_FOPR_Codigo)                            
 AND (ENRE.USUA_Codigo_Crea = @par_USUA_Codigo_Consultar OR ENRE.USUA_Codigo_Modifica = @par_USUA_Codigo_Consultar OR @par_USUA_Codigo_Consultar IS NULL)                            
 )                                                                            
 SELECT DISTINCT                                      
 0 AS Obtener                                                                            
 ,EMPR_Codigo                                                              
 ,Numero                                                                            
 ,Numero_Documento                                      
 /*Modificación AE 9/06/2020*/                                      
 ,Numeracion                                      
 /*Fin Modificación*/                                                                          
 ,Fecha            
 ,CATA_LNPA_Codigo            
 ,CATA_LNPA_Nombre                                                                       
 ,NumeroDocumentoPlanilla                                                                            
 ,VEHI_Codigo                                                                            
 ,NombreCliente                                                                            
 ,TERC_Codigo_Cliente          
 ,NombreRemitente                              
 ,TERC_Codigo_Remitente                                                                   
 ,CATA_FOPR_Codigo                                                         
 ,FormaPago                                                    
 ,RUTA_Codigo                                                                        
 ,CIUD_Codigo_Origen                                                                            
 ,CIUD_Codigo_Destino                                   
 ,CiudadOrigen                                                                            
 ,CiudadDestino                                                  
 ,SICD_Codigo_Descargue                                                      
 ,SitioDescargue                                                           
 ,OFIC_Codigo_Origen                                                              
 ,NombreOficinaOrigen                                                                      
 ,Observaciones                                                                            
 ,Valor_Flete_Cliente                                                                            
 ,Total_Flete_Cliente                                                                            
 ,Valor_Seguro_Cliente                       
 ,Cantidad_Cliente                                                         
 ,Peso_Cliente                          
 ,Peso_Volumetrico                                  
 ,Peso_A_Cobrar                                                                            
 ,Estado                                                        
 ,Anulado                                                
 ,NombreProducto                                                                   
 ,Documento_Cliente                       
 ,OFIC_Codigo                                                              
 ,NombreOficina                                                     
 ,NombreRuta                                                                            
 ,NombreOficinaActual                                                        
 ,CATA_ESRP_Codigo                      
 ,Valor_Flete_Transportador                                                    
 ,NombreRemitente                                                 
 ,Valor_Reexpedicion                                   
 ,Reexpedicion                                              
 , NombreDestinatario                          
 ,ENFA_Numero                          
 ,ENFA_Numero_Documento                  
 ,VEHI_Placa                  
 ,VEHI_CodigoAlterno        
 ,ELRG_NumeroDocumento     
 ,ELGU_NumeroDocumento    
 ,Zona      
 ,@CantidadRegistros AS TotalRegistros                                                                            
 ,@par_NumeroPagina AS PaginaObtener                                                                            
 ,@par_RegistrosPagina AS RegistrosPagina                                                                 
 FROM Pagina                                                                            
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                                     
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                                            
 ORDER BY Numero ASC                                                                 
END 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesa_paqueteria_asociar_planilla]            
(                                  
  @par_EMPR_Codigo SMALLINT,                                  
  @par_NumeroInicial numeric = NULL,                                  
  @par_NumeroFinal numeric = NULL,                                  
  @par_FechaInicial Date = NULL,                                  
  @par_FechaFinal Date = NULL,                                  
  @par_NumeroPlanillaInicial numeric = NULL,                                  
  @par_NumeroPlanillaFinal numeric = NULL,                                  
  @par_FechaPlanillaInicial Date = NULL,                                  
  @par_FechaPlanillaFinal Date = NULL,                                  
  @par_Numero numeric = NULL,                                  
  @par_Fecha date = NULL,                                  
  @par_Documento_Cliente varchar(30) = NULL,                                  
  @par_NombreCliente varchar (150) = NULL,                                  
  @par_TERC_Codigo_Cliente numeric = NULL,                                  
  @par_PRTR_Codigo numeric = NULL,                                  
  @par_VEHI_Codigo numeric= NULL,                                  
  @par_NombreRuta varchar (150) = NULL,                                  
  @par_Ciudad_Origen varchar (50) = NULL,                                  
  @par_Ciudad_Destino varchar (50) = NULL,                                  
  @par_RUTA_Codigo numeric= NULL,                                  
  @par_NombreDestinatario varchar (150) = NULL,                                  
  @par_NombreRemitente varchar(150) = NULL,                                  
  @par_CATA_TERP_Codigo numeric = NULL,                                  
  @par_CATA_TTRP_Codigo numeric= NULL,                                  
  @par_CATA_TDRP_Codigo numeric= NULL,                                  
  @par_CATA_TSRP_Codigo numeric= NULL,                                  
  @par_ENPD_Numero numeric = NULL,                                  
  @par_Anulado numeric= NULL,                                  
  @par_Estado numeric = NULL,                                  
  @par_Cumplido smallint = NULL,                                  
  @par_Planillado smallint = NULL,                                  
  @par_planilla_entrega INT = NULL,                                  
  @par_planilla_recoleccion INT = NULL,                                  
  @par_TIDO_Codigo INT = NULL,                                  
  @par_planilla_despachos NUMERIC = NULL,                           
  @par_Codigo_Ciudad_Planilla_Recolecciones NUMERIC = NULL,                              
  @par_Codigo_Oficina_Planilla_Recolecciones NUMERIC = NULL,                                   
  @par_NumeroPagina INT = NULL,                      
  @par_RegistrosPagina INT = NULL,                      
  @par_CATA_ESRP_Codigo INT = NULL,                      
  @par_OFIC_Codigo_Actual_Usuario INT = NULL,                    
  @par_OFIC_Codigo_Actual NUMERIC = NULL,                    
  @par_OFIC_Codigo INT = NULL                    
                                   
)                                  
AS                                           
BEGIN                                  
 IF @par_FechaInicial <> NULL BEGIN                                  
  SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                  
 END                                  
                                  
 IF @par_FechaFinal <> NULL BEGIN                                  
  SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                  
 END                                  
                                  
 SET NOCOUNT ON;                                        
 DECLARE @CantidadRegistros INT                                  
 SELECT                                  
  @CantidadRegistros = (SELECT DISTINCT                                  
    COUNT(1)                                  
  FROM Remesas_Paqueteria ENRP                                  
                                  
   INNER JOIN Encabezado_Remesas AS ENRE                                  
    ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                  
    AND ENRP.ENRE_Numero = ENRE.Numero                                  
                                  
   INNER JOIN Oficinas AS OFIC ON                    
   ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                    
   AND ENRE.OFIC_Codigo = OFIC.Codigo                    
                  
   LEFT JOIN Terceros AS CLIE                            
    ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                  
    AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                  
                          
   LEFT JOIN Terceros AS REMI                                  
    ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                  
    AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                  
                                  
   LEFT JOIN Terceros AS DEST                                  
    ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                  
    AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                  
                              
   LEFT JOIN Producto_Transportados AS PRTR                                  
    ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                  
    AND ENRE.PRTR_Codigo = PRTR.Codigo                                  
                                  
   LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                  
    ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                        
    AND ENRE.ENPD_Numero = ENPD.Numero                                  
    AND ENPD.TIDO_Codigo = 130 --Planilla Despacho Guias                                              
                                  
    AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                  
    AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                  
                              
 LEFT JOIN Ciudades CIOR                         
   ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                  
   AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                  
                                  
  LEFT JOIN Ciudades CIDE                                  
   ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                  
   AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo            
               
   LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON            
   ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo            
   AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                
                   
      INNER JOIN Oficinas AS OFAC ON                    
   ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                    
   AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                   
                                  
   WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                  
   AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                               
   AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                  
   AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                  
   AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                  
   AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                  
   AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                  
   OR (@par_NombreCliente IS NULL))                                  
   AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                  
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                  
   AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                  
   AND ENRE.Anulado = 0        
   AND ENRE.Estado = 1          
   AND ISNULL(ENRE.Cumplido,0) = 0          
   AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                  
   AND ENRE.ENPD_Numero =0          
   AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                      
   AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)                      
   AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                  
   AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                
   AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                      
   AND ENRP.CATA_ESRP_Codigo IN (6001,6005,6015,6017,6020,6040,6010)          
   );                                  
 WITH Pagina                                  
 AS                                  
 (SELECT                                  
   ENRE.EMPR_Codigo                                  
     ,ENRE.Numero                                  
     ,ENRE.Numero_Documento                                  
     ,ENRE.Fecha                                  
     ,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                  
     ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                  
     ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente  
	 /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	/*Fin Modificación*/ 
  ,ENRE.TERC_Codigo_Remitente  
     ,ENRE.TERC_Codigo_Cliente                                  
     ,ENRE.CATA_FOPR_Codigo                 
  ,FOPR.Campo1 AS FormaPago        
     ,ENRE.RUTA_Codigo                                  
     ,ENRP.CIUD_Codigo_Origen                                  
     ,ENRP.CIUD_Codigo_Destino                                  
     ,CIOR.Nombre AS CiudadOrigen                                  
     ,CIDE.Nombre AS CiudadDestino            
  ,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue            
  ,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue                   
  ,ISNULL(ENRP.OFIC_Codigo_Origen, 0) AS OFIC_Codigo_Origen                  
  ,ISNULL(OFIC.Nombre, '') AS NombreOficinaOrigen                     
  ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                     
     ,ENRP.CATA_ESRP_Codigo                
     ,ENRE.Observaciones                                  
     ,ENRE.Valor_Flete_Cliente                                  
     ,ENRE.Total_Flete_Cliente                                  
     ,ENRE.Valor_Seguro_Cliente                                  
     ,ENRE.Cantidad_Cliente                                  
     ,ENRP.Peso_A_Cobrar as Peso_Cliente                                  
     ,ENRE.Estado                                  
     ,PRTR.Nombre AS NombreProducto                                  
     ,ENRE.Documento_Cliente                     
  ,ENRE.OFIC_Codigo                    
     ,OFIC.Nombre As NombreOficina                                 
     ,'' AS NombreRuta               
  ,ENRE.Valor_Flete_Transportador        
 ,ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber                                  
  FROM Remesas_Paqueteria AS ENRP                                  
                                  
  INNER JOIN Encabezado_Remesas AS ENRE                                  
   ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                  
   AND ENRP.ENRE_Numero = ENRE.Numero                                  
                              
   INNER JOIN Oficinas AS OFIC ON                    
   ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                    
   AND ENRE.OFIC_Codigo = OFIC.Codigo                   
                  
  LEFT JOIN Terceros AS CLIE                                  
   ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                  
   AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                  
                                  
  LEFT JOIN Terceros AS REMI       
   ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                  
   AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                  
                                  
  LEFT JOIN Terceros AS DEST                                  
   ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                  
   AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo               
                                  
  LEFT JOIN Producto_Transportados AS PRTR                                  
   ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                  
   AND ENRE.PRTR_Codigo = PRTR.Codigo                                  
                                  
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                  
   ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                  
   AND ENRE.ENPD_Numero = ENPD.Numero                                  
   AND ENPD.TIDO_Codigo = 130 --Planilla Despacho Guias                                              
                                  
   AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                  
   AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                  
                              
                              
 LEFT JOIN Ciudades CIOR                                  
   ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                  
   AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                  
                                  
  LEFT JOIN Ciudades CIDE                                  
   ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                  
   AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo            
               
   LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON            
   ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo            
   AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                          
                                  
   INNER JOIN Oficinas AS OFAC ON                    
   ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                    
   AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                   
           
   LEFT JOIN Valor_Catalogos AS FOPR ON        
   ENRE.EMPR_Codigo = FOPR.EMPR_Codigo        
   AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo        
        
    WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                  
   AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                               
   AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                  
   AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                  
   AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                  
   AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                  
   AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                  
   OR (@par_NombreCliente IS NULL))                                  
   AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                  
   AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                  
   AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                  
   AND ENRE.Anulado = 0          
   AND ENRE.Estado = 1          
   AND ISNULL(ENRE.Cumplido,0) = 0          
   AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                  
   AND ENRE.ENPD_Numero =0          
   AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                      
   AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)                      
   AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)               
   AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                
   AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                   
   AND ENRP.CATA_ESRP_Codigo IN (6001,6005,6015,6017,6020,6040)               
                                
  )                                  
           SELECT DISTINCT                                  
  0 AS Obtener                                  
    ,EMPR_Codigo                                  
    ,Numero                                  
    ,Numero_Documento                                  
    ,Fecha                                  
    ,NumeroDocumentoPlanilla                                  
    ,VEHI_Codigo                                  
    ,NombreCliente                                  
    ,TERC_Codigo_Cliente      
 ,NombreRemitente,  
 TERC_Codigo_Remitente                              
    ,CATA_FOPR_Codigo                        
 ,FormaPago        
    ,RUTA_Codigo                                  
    ,CIUD_Codigo_Origen                                  
    ,CIUD_Codigo_Destino                                  
    ,CiudadOrigen                                  
    ,CiudadDestino            
 ,SICD_Codigo_Descargue            
 ,SitioDescargue                            
 ,OFIC_Codigo_Origen                    
 ,NombreOficinaOrigen                            
    ,Observaciones                                  
    ,Valor_Flete_Cliente                                  
    ,Total_Flete_Cliente                                  
    ,Valor_Seguro_Cliente                                  
    ,Cantidad_Cliente                                  
    ,Peso_Cliente                                  
    ,Estado                                  
    ,NombreProducto                     
    ,Documento_Cliente                   
    ,OFIC_Codigo                    
    ,NombreOficina                                   
    ,NombreRuta                                  
 ,NombreOficinaActual                
 ,CATA_ESRP_Codigo                 
 ,Valor_Flete_Transportador        
    ,@CantidadRegistros AS TotalRegistros                                  
    ,@par_NumeroPagina AS PaginaObtener                                  
    ,@par_RegistrosPagina AS RegistrosPagina                                  
 FROM Pagina                                  
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                  
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                  
 ORDER BY Numero ASC                                  
END           
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesa_paqueteria_control_entregas]          
(                                            
 @par_EMPR_Codigo SMALLINT,                                            
 @par_NumeroInicial numeric = NULL,                                            
 @par_NumeroFinal numeric = NULL,                                            
 @par_FechaInicial Date = NULL,                                            
 @par_FechaFinal Date = NULL,                                            
 @par_NumeroPlanillaInicial numeric = NULL,                                            
 @par_NumeroPlanillaFinal numeric = NULL,                                            
 @par_FechaPlanillaInicial Date = NULL,                                            
 @par_FechaPlanillaFinal Date = NULL,                                            
 @par_Numero numeric = NULL,                                            
 @par_Fecha date = NULL,                                            
 @par_Documento_Cliente varchar(30) = NULL,                                            
 @par_NombreCliente varchar (150) = NULL,                                            
 @par_TERC_Codigo_Cliente numeric = NULL,                                            
 @par_PRTR_Codigo numeric = NULL,                                            
 @par_VEHI_Codigo numeric= NULL,                                            
 @par_NombreRuta varchar (150) = NULL,                                            
 @par_Ciudad_Origen varchar (50) = NULL,                                            
 @par_Ciudad_Destino varchar (50) = NULL,                                            
 @par_RUTA_Codigo numeric= NULL,                                            
 @par_NombreDestinatario varchar (150) = NULL,                                            
 @par_NombreRemitente varchar(150) = NULL,                                            
 @par_CATA_TERP_Codigo numeric = NULL,                                            
 @par_CATA_TTRP_Codigo numeric= NULL,                                            
 @par_CATA_TDRP_Codigo numeric= NULL,                                            
 @par_CATA_TSRP_Codigo numeric= NULL,                                            
 @par_ENPD_Numero numeric = NULL,                                            
 @par_Anulado numeric= NULL,                                            
 @par_Estado numeric = NULL,                                            
 @par_Cumplido smallint = NULL,                                            
 @par_Planillado smallint = NULL,                                            
 @par_planilla_entrega INT = NULL,                                            
 @par_planilla_recoleccion INT = NULL,                                            
 @par_TIDO_Codigo INT = NULL,                                            
 @par_planilla_despachos NUMERIC = NULL,                                     
 @par_Codigo_Ciudad_Planilla_Recolecciones NUMERIC = NULL,                                        
 @par_Codigo_Oficina_Planilla_Recolecciones NUMERIC = NULL,                                             
 @par_NumeroPagina INT = NULL,                                
 @par_RegistrosPagina INT = NULL,                                
 @par_CATA_ESRP_Codigo INT = NULL,                                
 @par_OFIC_Codigo_Actual_Usuario INT = NULL,                              
 @par_OFIC_Codigo_Actual NUMERIC = NULL,                              
 @par_OFIC_Codigo INT = NULL,          
 @par_Reexpedicion INT = NULL,          
 @par_Ciudad_Destino_Codigo INT = NULL,      
 @par_OFIC_Codigo_Origen NUMERIC = NULL,    
   /*Modificación AE  9/06/2020*/      
 @par_Numeracion varchar(50) = null          
 /*Fin modificación*/      
)                                            
AS                                                     
BEGIN                                            
 IF @par_FechaInicial <> NULL BEGIN                                            
 SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                            
 END                                            
         
 IF @par_FechaFinal <> NULL BEGIN                                            
 SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                         
 END                                            
                                            
 SET NOCOUNT ON;                                                  
 DECLARE @CantidadRegistros INT                          
 SELECT                                            
 @CantidadRegistros = (SELECT DISTINCT                         
  COUNT(1)                                            
  FROM Remesas_Paqueteria ENRP                                            
                                            
  INNER JOIN Encabezado_Remesas AS ENRE                                      
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                            
  AND ENRP.ENRE_Numero = ENRE.Numero                                            
                                            
  INNER JOIN Oficinas AS OFIC ON                              
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                              
  AND ENRE.OFIC_Codigo = OFIC.Codigo                              
                            
  LEFT JOIN Terceros AS CLIE                     
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                            
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                            
                                    
  LEFT JOIN Terceros AS REMI                                            
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                            
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                            
                                            
  LEFT JOIN Terceros AS DEST                                            
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                            
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                            
                                        
  LEFT JOIN Producto_Transportados AS PRTR                                            
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                            
  AND ENRE.PRTR_Codigo = PRTR.Codigo                                            
                                            
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                            
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                  
  AND ENRE.ENPD_Numero = ENPD.Numero                                            
  AND ENPD.TIDO_Codigo = 130 --Planilla Despacho Guias                                                        
                                            
  AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                            
  AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                            
                                        
  LEFT JOIN Ciudades CIOR                                   
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                            
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                            
                                            
  LEFT JOIN Ciudades CIDE                                            
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                            
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                      
                         
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                      
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                      
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                          
                             
  INNER JOIN Oficinas AS OFAC ON                              
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                              
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo            
    
  LEFT JOIN Remesas_Paqueteria REPA ON  
  ENRE.EMPR_Codigo = REPA.EMPR_Codigo AND  
  ENRE.Numero = REPA.ENRE_Numero                 
  
  LEFT JOIN Zona_Ciudades ZOCI ON
  REPA.EMPR_Codigo = ZOCI.EMPR_Codigo AND
  REPA.ZOCI_Codigo_Entrega = ZOCI.Codigo

                   
                                            
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                            
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                         
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                            
  AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal, ENRE.Numero_Documento)                                      
                                            
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                            
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                            
                                            
                        
  AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                        
  AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                                                  
                                          
  AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                            
  AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                            
                                            
  AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                            
  AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                              
  OR (@par_NombreCliente IS NULL))                                            
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                            
  AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                            
  AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                            
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                            
  AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                            
                                            
  AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo)
    	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                           
                                            
  AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                            
  AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                              
  AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                            
  AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                            
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                            
  AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                
  AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                            
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                            
  AND ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero)                                        
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                        
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                                
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)      
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)    
                                
  AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                                       
  AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                          
  AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                             
  AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)          
  AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)          
  AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)        
  /*Modificación AE  9/06/2020*/        
  AND ENRE.Numeracion LIKE ISNULL(@par_Numeracion,ENRE.Numeracion) + '%'      
  /*Fin modificación*/      
  AND REPA.CATA_ESRP_Codigo IN (6001,6025,6030,6035)
 );                                            
 WITH Pagina                                            
 AS                                            
 (          
  SELECT                                            
  ENRE.EMPR_Codigo                                            
  ,ENRE.Numero                                            
  ,ENRE.Numero_Documento       
   /*Modificación AE  9/06/2020*/       
  ,ENRE.Numeracion      
  /*Fin modificación*/                                          
  ,ENRE.Fecha                                            
  ,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                            
  ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                            
  ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                        
  ,ENRE.TERC_Codigo_Cliente                                            
  ,ENRE.CATA_FOPR_Codigo                             
  ,FOPR.Campo1 AS FormaPago                                            
  ,ENRE.RUTA_Codigo                                            
  ,ENRP.CIUD_Codigo_Origen                                            
  ,ENRP.CIUD_Codigo_Destino                                            
  ,CIOR.Nombre AS CiudadOrigen                                            
  ,CIDE.Nombre AS CiudadDestino                      
  ,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue                      
  ,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue                             
  ,ISNULL(ENRP.OFIC_Codigo_Origen, 0) AS OFIC_Codigo_Origen                            
  ,ISNULL(OFIC.Nombre, '') AS NombreOficinaOrigen                               
  ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                               
  ,ENRP.CATA_ESRP_Codigo                          
  ,ENRE.Observaciones                                            
  ,(ENRE.Valor_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS  Valor_Flete_Cliente                                       
  ,(ENRE.Total_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS Total_Flete_Cliente                                            
  ,ENRE.Valor_Seguro_Cliente                                            
  ,ENRE.Cantidad_Cliente                                            
 ,ENRE.Peso_Cliente   
  ,REPA.Peso_Volumetrico  
  ,REPA.Peso_A_Cobrar 
  ,REPA.CATA_NERP_Codigo  
  ,REPA.Observaciones_Recibe                                        
  ,ENRE.Estado                                            
  ,ENRE.Anulado                                            
  ,PRTR.Nombre AS NombreProducto                                            
  ,ENRE.Documento_Cliente                               
  ,ENRE.OFIC_Codigo                              
  ,OFIC.Nombre As NombreOficina                                           
  ,'' AS NombreRuta                              
  ,ENRE.Valor_Flete_Transportador                 
  ,ENRE.Valor_Reexpedicion          
  ,ENRP.Reexpedicion     
    /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
	/*Fin Modificación*/  
	,ISNULL(REPA.Secuencia,0) AS Secuencia
  ,ZOCI.Nombre AS Zona
  ,ENRE.Telefonos_Destinatario
  ,ENRE.Barrio_Destinatario
  ,ENRE.Direccion_Destinatario
  ,DEST.Numero_Identificacion AS Identificacion_Destinatario
  ,DEST.CATA_TIID_Codigo AS Tipo_Identificacion_Destinatario
  ,REMI.CATA_TIID_Codigo AS Tipo_Identificacion_Remitente
  ,REMI.Numero_Identificacion AS Identificacion_Remitente
  ,REMI.Telefonos AS Telefonos_Remitente
  ,ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber                                            
  FROM Remesas_Paqueteria AS ENRP                                            
                                            
  INNER JOIN Encabezado_Remesas AS ENRE                                            
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                            
  AND ENRP.ENRE_Numero = ENRE.Numero                                            
                                        
  INNER JOIN Oficinas AS OFIC ON                              
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                              
  AND ENRE.OFIC_Codigo = OFIC.Codigo                             
                            
  LEFT JOIN Terceros AS CLIE             
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                            
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                            
                                            
  LEFT JOIN Terceros AS REMI                                            
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                            
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                            
                                            
  LEFT JOIN Terceros AS DEST                                            
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                            
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                            
                                            
  LEFT JOIN Producto_Transportados AS PRTR                                            
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo              
  AND ENRE.PRTR_Codigo = PRTR.Codigo                                            
                                            
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                            
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                            
  AND ENRE.ENPD_Numero = ENPD.Numero                                            
  AND ENPD.TIDO_Codigo = 130 --Planilla Despacho Guias                                                        
                              
  AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                            
  AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                            
                                            
                                        
  LEFT JOIN Ciudades CIOR                                            
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                            
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                            
                                  
  LEFT JOIN Ciudades CIDE                                            
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo          
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                      
                         
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                      
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                      
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                    
                                            
  INNER JOIN Oficinas AS OFAC ON                              
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                              
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                      
                          
  LEFT JOIN Valor_Catalogos AS FOPR ON                    
  ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                    
  AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo      
    
   LEFT JOIN Remesas_Paqueteria REPA ON  
  ENRE.EMPR_Codigo = REPA.EMPR_Codigo AND  
  ENRE.Numero = REPA.ENRE_Numero           
  
   LEFT JOIN Zona_Ciudades ZOCI ON
  REPA.EMPR_Codigo = ZOCI.EMPR_Codigo AND
  REPA.ZOCI_Codigo_Entrega = ZOCI.Codigo         
                    
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                 
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                  
                                       
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                            
  AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal, ENRE.Numero_Documento)                                            
                                            
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                            
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                            
  AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                            
  AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                            
                                            
  AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                        
  AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                                           
                                                           
                                          
  AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                            
  AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                            
  OR (@par_NombreCliente IS NULL))                                            
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                            
  AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                
  AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                            
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                            
  AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                             
                                            
  AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo)
    	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                        
                                            
                                            
  AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                            
  AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                            
  AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                 
  AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                            
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                            
  AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                            
  AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                            
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                            
  AND ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero)                                        
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                                          
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)    
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)                                
  AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                 
  AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                          
  AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                         
  AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)          
  AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)          
  AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)          
  /*Modificación AE  9/06/2020*/        
  AND ENRE.Numeracion LIKE ISNULL(@par_Numeracion,ENRE.Numeracion) + '%'      
  /*Fin modificación*/      

  AND REPA.CATA_ESRP_Codigo IN (6001,6025,6030,6035)
                                          
 )                                            
 SELECT DISTINCT                                            
 0 AS Obtener                                            
 ,EMPR_Codigo                                            
 ,Numero                                            
 ,Numero_Documento      
 /*Modificación AE 9/06/2020*/      
 ,Numeracion      
 /*Fin Modificación*/                                          
 ,Fecha                                            
 ,NumeroDocumentoPlanilla                                            
 ,VEHI_Codigo                                            
 ,NombreCliente                                            
 ,TERC_Codigo_Cliente                                            
 ,CATA_FOPR_Codigo                         
 ,FormaPago                    
 ,RUTA_Codigo                                            
 ,CIUD_Codigo_Origen                                            
 ,CIUD_Codigo_Destino                                            
 ,CiudadOrigen                                            
 ,CiudadDestino                  
 ,SICD_Codigo_Descargue                      
 ,SitioDescargue                           
 ,OFIC_Codigo_Origen                              
 ,NombreOficinaOrigen                                      
 ,Observaciones                                            
 ,Valor_Flete_Cliente                                            
 ,Total_Flete_Cliente                                            
 ,Valor_Seguro_Cliente                                            
 ,Cantidad_Cliente                         
 ,Peso_Cliente  
 ,Peso_Volumetrico  
 ,Peso_A_Cobrar                                            
 ,Estado                        
 ,Anulado                
 ,NombreProducto                                   
 ,Documento_Cliente                             
 ,OFIC_Codigo                              
 ,NombreOficina                     
 ,NombreRuta                                            
 ,NombreOficinaActual                          
 ,CATA_ESRP_Codigo                           
 ,Valor_Flete_Transportador                    
 ,NombreRemitente                 
 ,Valor_Reexpedicion          
 ,Reexpedicion              
 , NombreDestinatario          
 ,Secuencia        
 ,Zona
 ,Telefonos_Destinatario 
 ,Barrio_Destinatario
 ,Direccion_Destinatario
 ,Identificacion_Destinatario
 ,Tipo_Identificacion_Destinatario
 ,Tipo_Identificacion_Remitente
 ,Identificacion_Remitente
 ,Telefonos_Remitente
 ,CATA_NERP_Codigo
 ,Observaciones_Recibe

 ,@CantidadRegistros AS TotalRegistros                                            
 ,@par_NumeroPagina AS PaginaObtener                                            
 ,@par_RegistrosPagina AS RegistrosPagina                                            
 FROM Pagina                                            
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                            
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                            
 ORDER BY Numero ASC                                            
END          
GO

CREATE OR ALTER  PROCEDURE  [dbo].[gsp_consultar_remesa_paqueteria_control_entregas_Ofcinas]      
(                                                      
 @par_EMPR_Codigo SMALLINT,                                                      
 @par_NumeroInicial numeric = NULL,                                                      
 @par_NumeroFinal numeric = NULL,                                                      
 @par_FechaInicial Date = NULL,                                                      
 @par_FechaFinal Date = NULL,                                                      
 @par_NumeroPlanillaInicial numeric = NULL,                                                      
 @par_NumeroPlanillaFinal numeric = NULL,                                                      
 @par_FechaPlanillaInicial Date = NULL,                                                      
 @par_FechaPlanillaFinal Date = NULL,                                                      
 @par_Numero numeric = NULL,                                                      
 @par_Fecha date = NULL,                                                      
 @par_Documento_Cliente varchar(30) = NULL,                                                      
 @par_NombreCliente varchar (150) = NULL,                                                      
 @par_TERC_Codigo_Cliente numeric = NULL,                                                      
 @par_PRTR_Codigo numeric = NULL,                                                      
 @par_VEHI_Codigo numeric= NULL,                                                      
 @par_NombreRuta varchar (150) = NULL,                                                      
 @par_Ciudad_Origen varchar (50) = NULL,                                                      
 @par_Ciudad_Destino varchar (50) = NULL,                                                      
 @par_RUTA_Codigo numeric= NULL,                                                      
 @par_NombreDestinatario varchar (150) = NULL,                                                      
 @par_NombreRemitente varchar(150) = NULL,                                                      
 @par_CATA_TERP_Codigo numeric = NULL,                                                      
 @par_CATA_TTRP_Codigo numeric= NULL,                                                      
 @par_CATA_TDRP_Codigo numeric= NULL,                                                      
 @par_CATA_TSRP_Codigo numeric= NULL,                                                      
 @par_ENPD_Numero numeric = NULL,                                                      
 @par_Anulado numeric= NULL,                                                      
 @par_Estado numeric = NULL,                                                      
 @par_Cumplido smallint = NULL,                                                      
 @par_Planillado smallint = NULL,                                                      
 @par_planilla_entrega INT = NULL,                                                      
 @par_planilla_recoleccion INT = NULL,                                                      
 @par_TIDO_Codigo INT = NULL,                                                      
 @par_planilla_despachos NUMERIC = NULL,                                               
 @par_Codigo_Ciudad_Planilla_Recolecciones NUMERIC = NULL,                                                  
 @par_Codigo_Oficina_Planilla_Recolecciones NUMERIC = NULL,                                                       
 @par_NumeroPagina INT = NULL,                                          
 @par_RegistrosPagina INT = NULL,                                          
 @par_CATA_ESRP_Codigo INT = NULL,                                          
 @par_OFIC_Codigo_Actual_Usuario INT = NULL,                                        
 @par_OFIC_Codigo_Actual NUMERIC = NULL,                                        
 @par_OFIC_Codigo INT = NULL,                    
 @par_Reexpedicion INT = NULL,                    
 @par_Ciudad_Destino_Codigo INT = NULL,                
 @par_OFIC_Codigo_Origen NUMERIC = NULL,      
    
  @par_OFIC_Codigo_Recibe  NUMERIC = NULL,    
  @par_RecogerOfficina   NUMERIC = NULL,    
  @par_Barrio   varchar (150) = NULL,    
   /*Modificación AE  9/06/2020*/                
 @par_Numeracion varchar(50) = null           
 /*Fin modificación*/                
       
    
)                                                      
AS                                         
                                         
 IF @par_FechaInicial <> NULL BEGIN                                                      
 SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                      
 END                                                      
                   
 IF @par_FechaFinal <> NULL BEGIN                                                      
 SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                   
 END                                                      
                                                      
 SET NOCOUNT ON;                                                            
 DECLARE @CantidadRegistros INT                                    
 SELECT                                                      
 @CantidadRegistros = (SELECT DISTINCT                                   
  COUNT(1)                                                      
  FROM Remesas_Paqueteria ENRP                                                      
                                                      
  INNER JOIN Encabezado_Remesas AS ENRE                                                
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                      
  AND ENRP.ENRE_Numero = ENRE.Numero                                                      
                                                      
  INNER JOIN Oficinas AS OFIC ON                                        
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                        
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                        
                                      
  LEFT JOIN Terceros AS CLIE                               
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                      
                                              
  LEFT JOIN Terceros AS REMI                                                      
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                      
                                                      
  LEFT JOIN Terceros AS DEST                                                      
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                      
                                                  
  LEFT JOIN Producto_Transportados AS PRTR                                                      
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                                      
  AND ENRE.PRTR_Codigo = PRTR.Codigo                                                      
                                                      
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                      
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                            
  AND ENRE.ENPD_Numero = ENPD.Numero                                                      
  AND ENPD.TIDO_Codigo = 135 --Planilla Paquetería                                                                  
                                                      
  AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                                      
  AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                                      
                                                  
  LEFT JOIN Ciudades CIOR         
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                      
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                      
                                                      
  LEFT JOIN Ciudades CIDE                                                      
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                      
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                
                  
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                    
                                  
  INNER JOIN Oficinas AS OFAC ON                                        
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                        
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                      
              
  LEFT JOIN Remesas_Paqueteria REPA ON            
  ENRE.EMPR_Codigo = REPA.EMPR_Codigo AND            
  ENRE.Numero = REPA.ENRE_Numero                           
            
  LEFT JOIN Zona_Ciudades ZOCI ON          
  REPA.EMPR_Codigo = ZOCI.EMPR_Codigo AND          
  REPA.ZOCI_Codigo_Entrega = ZOCI.Codigo         
    
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                                      
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                                   
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                                      
  AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal, ENRE.Numero_Documento)                                                
                                                      
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                      
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                                      
                                                      
                                  
  AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                                  
  AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                                                            
                                                    
  AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                      
  AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                                      
                                                      
  AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                      
  AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                        
  OR (@par_NombreCliente IS NULL))                                                      
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                                      
  AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                                      
  AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                      
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                                      
  AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                                      
                                                      
  AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo)
    	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                                    
                                                      
  AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                      
  AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                              
  AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                                      
  AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                                      
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                                      
  AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                          
  AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                      
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                      
  AND ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero)                                                  
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                                  
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                                          
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)                
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)              
                                          
  AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                                                 
  AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                                    
  AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                                       
  AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)                    
  AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)                    
  AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)                  
  /*Modificación AE  9/06/2020*/                  
  AND ENRE.Numeracion LIKE ISNULL(@par_Numeracion,ENRE.Numeracion) + '%'         
  /*Fin modificación*/                
      
   /*Modificacion 16/03/2021*/      
 AND  (REPA.OFIC_Codigo_Recibe  =  @par_OFIC_Codigo_Recibe  or @par_OFIC_Codigo_Recibe IS NULL)       
  AND  ENRP.Recoger_Oficina_Destino  = ISNULL( @par_RecogerOfficina, ENRP.Recoger_Oficina_Destino )      
   AND (ENRE.Barrio_Destinatario  LIKE  '%'   +  @par_Barrio + '%' OR @par_Barrio IS NULL)         
   /*Fin modificación*/        
              
 );                                                      
 WITH Pagina                                                      
 AS                                            
 (                    
  SELECT                                                      
  ENRE.EMPR_Codigo        
  ,REPA.OFIC_Codigo_Recibe    
  ,ENRE.Numero                                                  
  ,ENRE.Numero_Documento                 
   /*Modificación AE  9/06/2020*/                 
  ,ENRE.Numeracion                
  /*Fin modificación*/                                                    
  ,ENRE.Fecha                                                      
  ,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                                      
  ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                                      
  ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                                  
  ,ENRE.TERC_Codigo_Cliente                                                      
  ,ENRE.CATA_FOPR_Codigo                                       
  ,FOPR.Campo1 AS FormaPago                                                      
  ,ENRE.RUTA_Codigo                                                      
  ,ENRP.CIUD_Codigo_Origen                                                      
  ,ENRP.CIUD_Codigo_Destino                                                      
  ,CIOR.Nombre AS CiudadOrigen                   
  ,CIDE.Nombre AS CiudadDestino                                
  ,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue                                
  ,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue                                       
  ,ISNULL(ENRP.OFIC_Codigo_Origen, 0) AS OFIC_Codigo_Origen                                      
  ,ISNULL(OFIC.Nombre, '') AS NombreOficinaOrigen                                         
  ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                                         
  ,ENRP.CATA_ESRP_Codigo                                    
  ,ENRE.Observaciones                                                      
  ,(ENRE.Valor_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS  Valor_Flete_Cliente                                                 
  ,(ENRE.Total_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS Total_Flete_Cliente                                                      
  ,ENRE.Valor_Seguro_Cliente                                                      
  ,ENRE.Cantidad_Cliente                                                      
 ,ENRE.Peso_Cliente             
  ,REPA.Peso_Volumetrico            
  ,REPA.Peso_A_Cobrar           
  ,REPA.CATA_NERP_Codigo        
  ,REPA.Observaciones_Recibe                                                  
  ,ENRE.Estado                                                      
  ,ENRE.Anulado                                                      
  ,PRTR.Nombre AS NombreProducto                                                      
  ,ENRE.Documento_Cliente                                         
  ,ENRE.OFIC_Codigo                                        
  ,OFIC.Nombre As NombreOficina                                                     
  ,'' AS NombreRuta                                        
  ,ENRE.Valor_Flete_Transportador                           
  ,ENRE.Valor_Reexpedicion                    
  ,ENRP.Reexpedicion
    /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
	/*Fin Modificación*/
	,ISNULL(REPA.Secuencia,0) AS Secuencia          
  ,ZOCI.Nombre AS Zona          
  ,ENRE.Telefonos_Destinatario          
  ,ENRE.Barrio_Destinatario          
  ,ENRE.Direccion_Destinatario          
  ,DEST.Numero_Identificacion AS Identificacion_Destinatario          
  ,DEST.CATA_TIID_Codigo AS Tipo_Identificacion_Destinatario          
  ,REMI.CATA_TIID_Codigo AS Tipo_Identificacion_Remitente          
  ,REMI.Numero_Identificacion AS Identificacion_Remitente          
  ,REMI.Telefonos AS Telefonos_Remitente          
  ,ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber                                                      
  FROM Remesas_Paqueteria AS ENRP                                                      
                                                      
  INNER JOIN Encabezado_Remesas AS ENRE                                                      
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                      
  AND ENRP.ENRE_Numero = ENRE.Numero                                                      
                                                  
  INNER JOIN Oficinas AS OFIC ON                                        
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                        
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                 
                                      
  LEFT JOIN Terceros AS CLIE                       
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                      
                                                      
  LEFT JOIN Terceros AS REMI                                                      
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                      
                 
  LEFT JOIN Terceros AS DEST                                                      
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                      
                                                      
  LEFT JOIN Producto_Transportados AS PRTR                                                      
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                        
  AND ENRE.PRTR_Codigo = PRTR.Codigo                   
                                                      
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                      
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                      
  AND ENRE.ENPD_Numero = ENPD.Numero                                                      
   AND ENPD.TIDO_Codigo = 135 --Planilla Paquetería                                                                     
                                        
  AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                                      
  AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                                      
                                                      
                                                  
  LEFT JOIN Ciudades CIOR                                                      
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                      
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                      
                                            
  LEFT JOIN Ciudades CIDE                                                      
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                    
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                
                                   
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                              
           
  INNER JOIN Oficinas AS OFAC ON                                        
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                        
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                
                                    
  LEFT JOIN Valor_Catalogos AS FOPR ON                              
  ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                              
  AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                
              
   LEFT JOIN Remesas_Paqueteria REPA ON            
  ENRE.EMPR_Codigo = REPA.EMPR_Codigo AND            
  ENRE.Numero = REPA.ENRE_Numero                     
            
   LEFT JOIN Zona_Ciudades ZOCI ON          
  REPA.EMPR_Codigo = ZOCI.EMPR_Codigo AND          
  REPA.ZOCI_Codigo_Entrega = ZOCI.Codigo                   
                              
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                           
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                            
                                                 
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                                      
  AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal, ENRE.Numero_Documento)                              
                                                      
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                      
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                                      
  AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                      
  AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                                      
                                                      
  AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                                  
  AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                                                     
                                                                     
                                                    
  AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                      
  AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                                      
  OR (@par_NombreCliente IS NULL))                                                      
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                                      
  AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                          
  AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                      
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                                      
  AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                                       
                                                      
  AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo) 
    	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                                      
                                                      
  AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                      
  AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                                      
  AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                           
  AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                                      
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                                      
  AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                      
  AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                      
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                      
  AND ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero)                  
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                          
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                                                    
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)              
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)                                          
  AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                           
  AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                                    
  AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                                   
  AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)                    
  AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)                    
  AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)                    
  /*Modificación AE  9/06/2020*/                  
  AND ENRE.Numeracion LIKE ISNULL(@par_Numeracion,ENRE.Numeracion) + '%'                
  /*Fin modificación*/        
        
  /*Modificacion 16/03/2021*/      
AND  (REPA.OFIC_Codigo_Recibe  =  @par_OFIC_Codigo_Recibe  or @par_OFIC_Codigo_Recibe IS NULL)        
  AND  ENRP.Recoger_Oficina_Destino  = ISNULL( @par_RecogerOfficina, ENRP.Recoger_Oficina_Destino )      
  AND (ENRE.Barrio_Destinatario  LIKE  '%'   +  @par_Barrio + '%' OR @par_Barrio IS NULL)      
     
   /*Fin modificación*/        
           
                                                    
 )                                                      
 SELECT DISTINCT                                                      
 0 AS Obtener                                                      
 ,EMPR_Codigo       
 ,OFIC_Codigo_Recibe    
 ,Numero                                                      
 ,Numero_Documento                
 /*Modificación AE 9/06/2020*/                
 ,Numeracion                
 /*Fin Modificación*/                                                    
 ,Fecha                                                      
 ,NumeroDocumentoPlanilla                                                      
 ,VEHI_Codigo                                                      
 ,NombreCliente                                                      
 ,TERC_Codigo_Cliente                                                      
 ,CATA_FOPR_Codigo                                   
 ,FormaPago                              
 ,RUTA_Codigo                                                      
 ,CIUD_Codigo_Origen                                                      
 ,CIUD_Codigo_Destino                                                      
 ,CiudadOrigen                                                      
 ,CiudadDestino                            
 ,SICD_Codigo_Descargue                                
 ,SitioDescargue                                     
 ,OFIC_Codigo_Origen                                        
 ,NombreOficinaOrigen                                                
 ,Observaciones                                                      
 ,Valor_Flete_Cliente                                                      
 ,Total_Flete_Cliente                                                      
 ,Valor_Seguro_Cliente                                                      
 ,Cantidad_Cliente                                   
 ,Peso_Cliente            
 ,Peso_Volumetrico            
 ,Peso_A_Cobrar                                                      
 ,Estado                                  
 ,Anulado                          
 ,NombreProducto                                             
 ,Documento_Cliente                                       
 ,OFIC_Codigo                                        
 ,NombreOficina                               
 ,NombreRuta                                                      
 ,NombreOficinaActual                                    
 ,CATA_ESRP_Codigo                                     
 ,Valor_Flete_Transportador                              
 ,NombreRemitente                           
 ,Valor_Reexpedicion                    
 ,Reexpedicion                        
 , NombreDestinatario                    
 ,Secuencia                  
 ,Zona          
 ,Telefonos_Destinatario           
 ,Barrio_Destinatario          
 ,Direccion_Destinatario          
 ,Identificacion_Destinatario          
 ,Tipo_Identificacion_Destinatario          
 ,Tipo_Identificacion_Remitente          
 ,Identificacion_Remitente          
 ,Telefonos_Remitente          
 ,CATA_NERP_Codigo         
 ,Observaciones_Recibe          
          
 ,@CantidadRegistros AS TotalRegistros                                                      
 ,@par_NumeroPagina AS PaginaObtener                                                      
 ,@par_RegistrosPagina AS RegistrosPagina                                                      
 FROM Pagina                                                      
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                      
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                      
 ORDER BY Numero ASC                                                      

GO

CREATE OR ALTER  PROCEDURE  [dbo].[gsp_consultar_remesa_paqueteria_control_entregas_planilla_paqueteria]      
(                                                      
 @par_EMPR_Codigo SMALLINT,                                                      
 @par_NumeroInicial numeric = NULL,                                                      
 @par_NumeroFinal numeric = NULL,                                                      
 @par_FechaInicial Date = NULL,                                                      
 @par_FechaFinal Date = NULL,                                                      
 @par_NumeroPlanillaInicial numeric = NULL,                                                      
 @par_NumeroPlanillaFinal numeric = NULL,                                                      
 @par_FechaPlanillaInicial Date = NULL,                                                      
 @par_FechaPlanillaFinal Date = NULL,                                                      
 @par_Numero numeric = NULL,                                                      
 @par_Fecha date = NULL,                                                      
 @par_Documento_Cliente varchar(30) = NULL,                                                      
 @par_NombreCliente varchar (150) = NULL,                                                      
 @par_TERC_Codigo_Cliente numeric = NULL,                                                      
 @par_PRTR_Codigo numeric = NULL,                                                      
 @par_VEHI_Codigo numeric= NULL,                                                      
 @par_NombreRuta varchar (150) = NULL,                                                      
 @par_Ciudad_Origen varchar (50) = NULL,                                                      
 @par_Ciudad_Destino varchar (50) = NULL,                                                      
 @par_RUTA_Codigo numeric= NULL,                                                      
 @par_NombreDestinatario varchar (150) = NULL,                                                      
 @par_NombreRemitente varchar(150) = NULL,                                                      
 @par_CATA_TERP_Codigo numeric = NULL,                                                      
 @par_CATA_TTRP_Codigo numeric= NULL,                                                      
 @par_CATA_TDRP_Codigo numeric= NULL,                                                      
 @par_CATA_TSRP_Codigo numeric= NULL,                                                      
 @par_ENPD_Numero numeric = NULL,                                                      
 @par_Anulado numeric= NULL,                                                      
 @par_Estado numeric = NULL,                                                      
 @par_Cumplido smallint = NULL,                                                      
 @par_Planillado smallint = NULL,                                                      
 @par_planilla_entrega INT = NULL,                                                      
 @par_planilla_recoleccion INT = NULL,                                                      
 @par_TIDO_Codigo INT = NULL,                                                      
 @par_planilla_despachos NUMERIC = NULL,                                               
 @par_Codigo_Ciudad_Planilla_Recolecciones NUMERIC = NULL,                                                  
 @par_Codigo_Oficina_Planilla_Recolecciones NUMERIC = NULL,                                                       
 @par_NumeroPagina INT = NULL,                                          
 @par_RegistrosPagina INT = NULL,                                          
 @par_CATA_ESRP_Codigo INT = NULL,                                          
 @par_OFIC_Codigo_Actual_Usuario INT = NULL,                                        
 @par_OFIC_Codigo_Actual NUMERIC = NULL,                                        
 @par_OFIC_Codigo INT = NULL,                    
 @par_Reexpedicion INT = NULL,                    
 @par_Ciudad_Destino_Codigo INT = NULL,                
 @par_OFIC_Codigo_Origen NUMERIC = NULL,      
    
  @par_OFIC_Codigo_Recibe  NUMERIC = NULL,    
  @par_RecogerOfficina   NUMERIC = NULL,    
  @par_Barrio   varchar (150) = NULL,    
   /*Modificación AE  9/06/2020*/                
 @par_Numeracion varchar(50) = null           
 /*Fin modificación*/                
       
    
)                                                      
AS                                         
                                         
 IF @par_FechaInicial <> NULL BEGIN                                                      
 SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                      
 END                                                      
                   
 IF @par_FechaFinal <> NULL BEGIN                                                      
 SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                   
 END                                                      
                                                      
 SET NOCOUNT ON;                                                            
 DECLARE @CantidadRegistros INT                                    
 SELECT                                                      
 @CantidadRegistros = (SELECT DISTINCT                                   
  COUNT(1)                                                      
  FROM Remesas_Paqueteria ENRP                                                      
                                                      
  INNER JOIN Encabezado_Remesas AS ENRE                                                
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                      
  AND ENRP.ENRE_Numero = ENRE.Numero                                                      
                                                      
  INNER JOIN Oficinas AS OFIC ON                                        
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                        
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                        
                                      
  LEFT JOIN Terceros AS CLIE                               
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                      
                                              
  LEFT JOIN Terceros AS REMI                                                      
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                      
                                                      
  LEFT JOIN Terceros AS DEST                                                      
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                      
                                                  
  LEFT JOIN Producto_Transportados AS PRTR                                                      
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                                      
  AND ENRE.PRTR_Codigo = PRTR.Codigo                                                      
                                                      
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                      
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                            
  AND ENRE.ENPD_Numero = ENPD.Numero                                                      
  AND ENPD.TIDO_Codigo = 135 --Planilla Paquetería                                                                  
                                                      
  AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                                      
  AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                                      
                                                  
  LEFT JOIN Ciudades CIOR         
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                      
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                      
                                                      
  LEFT JOIN Ciudades CIDE                                                      
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                      
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                
                  
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                    
                                  
  INNER JOIN Oficinas AS OFAC ON                                        
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                        
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                      
              
  LEFT JOIN Remesas_Paqueteria REPA ON            
  ENRE.EMPR_Codigo = REPA.EMPR_Codigo AND            
  ENRE.Numero = REPA.ENRE_Numero                           
            
  LEFT JOIN Zona_Ciudades ZOCI ON          
  REPA.EMPR_Codigo = ZOCI.EMPR_Codigo AND          
  REPA.ZOCI_Codigo_Entrega = ZOCI.Codigo          
          
                             
                                                      
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                                      
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                                   
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                                      
  AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal, ENRE.Numero_Documento)                                                
                                                      
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                      
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                                      
                                                      
                                  
  AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                                  
  AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                                                            
                                                    
  AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                      
  AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                                      
                                                      
  AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                      
  AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                        
  OR (@par_NombreCliente IS NULL))                                                      
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                                      
  AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                                      
  AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                      
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                                      
  AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                                      
                                                      
  AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo)  
  	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                                     
                                                      
  AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                      
  AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                              
  AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                                      
  AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                                      
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                                      
  AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                          
  AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                      
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                      
  AND ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero)                                                  
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                                  
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                                          
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)                
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)              
                                          
  AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                                                 
  AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                                    
  AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                                       
  AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)                    
  AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)                    
  AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)                  
  /*Modificación AE  9/06/2020*/                  
  AND ENRE.Numeracion LIKE ISNULL(@par_Numeracion,ENRE.Numeracion) + '%'         
  /*Fin modificación*/                
      
   /*Modificacion 16/03/2021*/      
 AND  (REPA.OFIC_Codigo_Recibe  =  @par_OFIC_Codigo_Recibe  or @par_OFIC_Codigo_Recibe IS NULL)       
  AND  ENRP.Recoger_Oficina_Destino  = ISNULL( @par_RecogerOfficina, ENRP.Recoger_Oficina_Destino )      
   AND (ENRE.Barrio_Destinatario  LIKE  '%'   + @par_Barrio + '%' OR @par_Barrio IS NULL )         
   /*Fin modificación*/        
      
      
  AND REPA.CATA_ESRP_Codigo IN (6001,6025,6030,6035,6010)          
 );                                                      
 WITH Pagina                                                      
 AS                                                      
 (                    
  SELECT                                                      
  ENRE.EMPR_Codigo        
  ,REPA.OFIC_Codigo_Recibe    
  ,ENRE.Numero                                                  
  ,ENRE.Numero_Documento                 
   /*Modificación AE  9/06/2020*/                 
  ,ENRE.Numeracion                
  /*Fin modificación*/                                                    
  ,ENRE.Fecha                                                      
  ,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                                      
  ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                                      
  ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                                  
  ,ENRE.TERC_Codigo_Cliente                                                      
  ,ENRE.CATA_FOPR_Codigo                                       
  ,FOPR.Campo1 AS FormaPago                                                      
  ,ENRE.RUTA_Codigo                                                      
  ,ENRP.CIUD_Codigo_Origen                                                      
  ,ENRP.CIUD_Codigo_Destino                                                      
  ,CIOR.Nombre AS CiudadOrigen                   
  ,CIDE.Nombre AS CiudadDestino                                
  ,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue                                
  ,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue                                       
  ,ISNULL(ENRP.OFIC_Codigo_Origen, 0) AS OFIC_Codigo_Origen                                      
  ,ISNULL(OFIC.Nombre, '') AS NombreOficinaOrigen                                         
  ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                                         
  ,ENRP.CATA_ESRP_Codigo                                    
  ,ENRE.Observaciones                                                      
  ,(ENRE.Valor_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS  Valor_Flete_Cliente                                                 
  ,(ENRE.Total_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS Total_Flete_Cliente                                                      
  ,ENRE.Valor_Seguro_Cliente                                                      
  ,ENRE.Cantidad_Cliente                                                      
 ,ENRE.Peso_Cliente             
  ,REPA.Peso_Volumetrico            
  ,REPA.Peso_A_Cobrar           
  ,REPA.CATA_NERP_Codigo        
  ,REPA.Observaciones_Recibe                                                  
  ,ENRE.Estado                                                      
  ,ENRE.Anulado                                                      
  ,PRTR.Nombre AS NombreProducto                                                      
  ,ENRE.Documento_Cliente                                         
  ,ENRE.OFIC_Codigo                                        
  ,OFIC.Nombre As NombreOficina                                                     
  ,'' AS NombreRuta                                        
  ,ENRE.Valor_Flete_Transportador                           
  ,ENRE.Valor_Reexpedicion                    
  ,ENRP.Reexpedicion 
  /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
	/*Fin Modificación*/  
	,ISNULL(REPA.Secuencia,0) AS Secuencia            ,ZOCI.Nombre AS Zona          
  ,ENRE.Telefonos_Destinatario          
  ,ENRE.Barrio_Destinatario          
  ,ENRE.Direccion_Destinatario          
  ,DEST.Numero_Identificacion AS Identificacion_Destinatario          
  ,DEST.CATA_TIID_Codigo AS Tipo_Identificacion_Destinatario          
  ,REMI.CATA_TIID_Codigo AS Tipo_Identificacion_Remitente          
  ,REMI.Numero_Identificacion AS Identificacion_Remitente          
  ,REMI.Telefonos AS Telefonos_Remitente          
  ,ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber                                                      
  FROM Remesas_Paqueteria AS ENRP                                                      
                                                      
  INNER JOIN Encabezado_Remesas AS ENRE                                                      
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                      
  AND ENRP.ENRE_Numero = ENRE.Numero                                                      
                                                  
  INNER JOIN Oficinas AS OFIC ON                                        
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                        
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                 
                                      
  LEFT JOIN Terceros AS CLIE                       
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                      
                                                      
  LEFT JOIN Terceros AS REMI                                                      
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                      
                 
  LEFT JOIN Terceros AS DEST                                                      
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                      
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                      
                                                      
  LEFT JOIN Producto_Transportados AS PRTR                                                      
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                        
  AND ENRE.PRTR_Codigo = PRTR.Codigo                   
                                                      
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                      
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                      
  AND ENRE.ENPD_Numero = ENPD.Numero                                                      
   AND ENPD.TIDO_Codigo = 135 --Planilla Paquetería                                                                     
                                        
  AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                                      
  AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                                                      
                                                      
                                                  
  LEFT JOIN Ciudades CIOR                                                      
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                      
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                      
                                            
  LEFT JOIN Ciudades CIDE                                                      
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                    
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                
                                   
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo           
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                              
                                                      
  INNER JOIN Oficinas AS OFAC ON                                        
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                        
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                
                                    
  LEFT JOIN Valor_Catalogos AS FOPR ON                              
  ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                              
  AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                
              
   LEFT JOIN Remesas_Paqueteria REPA ON            
  ENRE.EMPR_Codigo = REPA.EMPR_Codigo AND            
  ENRE.Numero = REPA.ENRE_Numero                     
            
   LEFT JOIN Zona_Ciudades ZOCI ON          
  REPA.EMPR_Codigo = ZOCI.EMPR_Codigo AND          
  REPA.ZOCI_Codigo_Entrega = ZOCI.Codigo                   
                              
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                                           
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                            
                                                 
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                                      
  AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal, ENRE.Numero_Documento)                              
                                                      
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                      
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                                      
  AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                      
  AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                                      
                                                      
  AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                                  
  AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                                                     
                                                                     
                                                    
  AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                      
  AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                                      
  OR (@par_NombreCliente IS NULL))                                                      
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                                      
  AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                          
  AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                      
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                                      
  AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                                       
                                                      
  AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo)
  	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/        
                                                      
  AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                      
  AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                                      
  AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                           
  AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                                      
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                                      
  AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                      
  AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                      
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                      
  AND ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero)                  
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                          
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                                                    
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)              
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)                                          
  AND ENRE.Numero_Documento= ISNULL(@par_Numero,ENRE.Numero_Documento)                           
  AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                                    
  AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                                   
  AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)                    
  AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)                    
  AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)                    
  /*Modificación AE  9/06/2020*/                  
  AND ENRE.Numeracion LIKE ISNULL(@par_Numeracion,ENRE.Numeracion) + '%'                
  /*Fin modificación*/        
        
  /*Modificacion 16/03/2021*/      
AND  (REPA.OFIC_Codigo_Recibe  =  @par_OFIC_Codigo_Recibe  or @par_OFIC_Codigo_Recibe IS NULL)        
  AND  ENRP.Recoger_Oficina_Destino  = ISNULL( @par_RecogerOfficina, ENRP.Recoger_Oficina_Destino )      
  AND (ENRE.Barrio_Destinatario  LIKE  '%'   + @par_Barrio + '%' OR @par_Barrio IS NULL )       
     
   /*Fin modificación*/        
   AND REPA.CATA_ESRP_Codigo IN (6001,6025,6030,6035,6010)            
                                                    
 )                                                      
 SELECT DISTINCT                                                      
 0 AS Obtener                                                      
 ,EMPR_Codigo       
 ,OFIC_Codigo_Recibe    
 ,Numero                                                      
 ,Numero_Documento                
 /*Modificación AE 9/06/2020*/                
 ,Numeracion                
 /*Fin Modificación*/                                                    
 ,Fecha                                                      
 ,NumeroDocumentoPlanilla                                                      
 ,VEHI_Codigo                                                      
 ,NombreCliente                                                      
 ,TERC_Codigo_Cliente                                                      
 ,CATA_FOPR_Codigo                                   
 ,FormaPago                              
 ,RUTA_Codigo                                                      
 ,CIUD_Codigo_Origen                                                      
 ,CIUD_Codigo_Destino                                                      
 ,CiudadOrigen                                                      
 ,CiudadDestino                            
 ,SICD_Codigo_Descargue                                
 ,SitioDescargue                                     
 ,OFIC_Codigo_Origen                                        
 ,NombreOficinaOrigen                                                
 ,Observaciones                                                      
 ,Valor_Flete_Cliente                                                      
 ,Total_Flete_Cliente                                                      
 ,Valor_Seguro_Cliente                                                      
 ,Cantidad_Cliente                                   
 ,Peso_Cliente            
 ,Peso_Volumetrico            
 ,Peso_A_Cobrar                                                      
 ,Estado                                  
 ,Anulado                          
 ,NombreProducto                                             
 ,Documento_Cliente                                       
 ,OFIC_Codigo                                        
 ,NombreOficina                               
 ,NombreRuta                                                      
 ,NombreOficinaActual                                    
 ,CATA_ESRP_Codigo                                     
 ,Valor_Flete_Transportador                              
 ,NombreRemitente                           
 ,Valor_Reexpedicion                    
 ,Reexpedicion                        
 , NombreDestinatario                    
 ,Secuencia                  
 ,Zona          
 ,Telefonos_Destinatario           
 ,Barrio_Destinatario          
 ,Direccion_Destinatario          
 ,Identificacion_Destinatario          
 ,Tipo_Identificacion_Destinatario          
 ,Tipo_Identificacion_Remitente          
 ,Identificacion_Remitente          
 ,Telefonos_Remitente          
 ,CATA_NERP_Codigo         
 ,Observaciones_Recibe          
          
 ,@CantidadRegistros AS TotalRegistros                                                      
 ,@par_NumeroPagina AS PaginaObtener                                                      
 ,@par_RegistrosPagina AS RegistrosPagina                                                      
 FROM Pagina                                                      
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                      
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                      
 ORDER BY Numero ASC 
 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_anuladas_reporte_rndc]     
(                  
 @par_EMPR_Codigo  smallint,          
 @par_ENRE_NumeroDocumento NUMERIC = NULL,          
 @par_ENMC_NumeroDocumento NUMERIC = NULL,          
 @par_Fecha_Inicial date = NULL,          
 @par_Fecha_Final date = NULL,      
 @par_OFIC_Codigo NUMERIC = NULL,            
 @par_NumeroPagina INT = NULL,                  
 @par_RegistrosPagina INT = NULL                  
)                  
AS                  
BEGIN                  
          
 DECLARE @CantidadRegistros NUMERIC            
 SELECT @CantidadRegistros = (                  
  SELECT DISTINCT COUNT(1)                   
  FROM Detalle_Manifiesto_Carga AS DMCA            
            
  LEFT JOIN Encabezado_Remesas AS ENRE            
  ON DMCA.EMPR_Codigo = ENRE.EMPR_Codigo             
  AND DMCA.ENRE_Numero = ENRE.Numero            
              
  LEFT JOIN Terceros AS CLIE             
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo            
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo            
          
  LEFT JOIN Terceros AS REMI             
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo            
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo            
              
  LEFT JOIN Vehiculos AS VEHI             
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo            
  AND ENRE.VEHI_Codigo = VEHI.Codigo          
          
  LEFT JOIN Semirremolques AS SEMI          
  ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo            
  AND ENRE.SEMI_Codigo = SEMI.Codigo            
              
  LEFT JOIN Rutas AS RUTA             
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo            
  AND ENRE.RUTA_Codigo = RUTA.Codigo            
              
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS            
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo            
  AND ENRE.ESOS_Numero = ESOS.Numero          
                
  LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC            
  ON DMCA.EMPR_Codigo = ENMC.EMPR_Codigo             
  AND DMCA.ENMC_Numero = ENMC.Numero          
            
  WHERE DMCA.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENRE.Estado = 1-- Estado Remesa Definitivo          
  AND ENRE.Anulado = 1  -- Remesa No Anulada          
  AND ENMC.Estado = 1 -- Manifiesto en Definitivo        
  AND DMCA.Numero_Remesa_Electronico > 0     
  --AND ENRE.ENMC_Numero = ENMC.Numero    
  AND (DMCA.Numero_Anulacion_Remesa_Electronico = 0 OR DMCA.Numero_Anulacion_Remesa_Electronico IS NULL)  
  AND (DMCA.Numero_Anulacion_Cumplido_Electronico > 0 OR( DMCA.Numero_Cumplido_Electronico = 0 OR  DMCA.Numero_Cumplido_Electronico IS NULL))  
  AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)            
  AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)          
  AND ENRE.Numero_Documento = ISNULL(@par_ENRE_NumeroDocumento,ENRE.Numero_Documento)          
  AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)          
  AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino        
  AND (ENRE.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)

 );                  
 WITH Pagina                  
 AS                  
 ( SELECT          
  0 As Obtener,          
  1 AS OpcionReporte,          
  ENRE.EMPR_Codigo,          
  ENRE.Numero,          
  ENRE.Numero_Documento AS NumeroDocumento,          
  ENRE.Fecha,          
  ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente,          
  ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente,          
  ISNULL(ENRE.TERC_Codigo_Remitente, 0) AS TERC_Codigo_Remitente, 
	/*Modificación FP 2021/12/12*/
	ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente,
	/*Fin Modificación*/  
  ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo,          
  ISNULL(VEHI.Placa,0) AS PlacaVehiculo,          
  ISNULL(ENRE.SEMI_Codigo, 0) AS SEMI_Codigo,          
  ISNULL(SEMI.Placa, 0) AS PlacaSemirremolque,          
  ISNULL(ENRE.RUTA_Codigo,0) AS RUTA_Codigo,          
  ISNULL(RUTA.Nombre, 0) AS NombreRuta,          
  ISNULL(ENRE.ENMC_Numero, 0) AS ENMC_Numero,          
  ENMC.Numero_Documento AS NumeroDocumentoManifiesto,          
  ENRE.ESOS_Numero,          
  ISNULL(ESOS.Numero_Documento, 0) AS NumeroDocumentoOrdenservicio,          
  ISNULL(DMCA.Mensaje_Anulacion_Remesa_Electronico, '') AS Mensaje,          
            
  ROW_NUMBER() OVER (ORDER BY ENRE.Numero) AS RowNumber                  
          
  FROM Detalle_Manifiesto_Carga AS DMCA            
            
  LEFT JOIN Encabezado_Remesas AS ENRE            
  ON DMCA.EMPR_Codigo = ENRE.EMPR_Codigo             
  AND DMCA.ENRE_Numero = ENRE.Numero            
              
  LEFT JOIN Terceros AS CLIE             
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo            
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo            
          
  LEFT JOIN Terceros AS REMI             
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo              
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo            
              
  LEFT JOIN Vehiculos AS VEHI             
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo            
  AND ENRE.VEHI_Codigo = VEHI.Codigo          
          
  LEFT JOIN Semirremolques AS SEMI          
  ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo            
  AND ENRE.SEMI_Codigo = SEMI.Codigo            
              
  LEFT JOIN Rutas AS RUTA             
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo            
  AND ENRE.RUTA_Codigo = RUTA.Codigo            
              
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS            
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo            
  AND ENRE.ESOS_Numero = ESOS.Numero            
                
  LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC            
  ON DMCA.EMPR_Codigo = ENMC.EMPR_Codigo             
  AND DMCA.ENMC_Numero = ENMC.Numero          
            
  WHERE DMCA.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENRE.Estado = 1-- Estado Remesa Definitivo          
  AND ENRE.Anulado = 1  -- Remesa No Anulada          
  AND ENMC.Estado = 1 -- Manifiesto en Definitivo        
  AND DMCA.Numero_Remesa_Electronico > 0     
  --AND ENRE.ENMC_Numero = ENMC.Numero    
  AND (DMCA.Numero_Anulacion_Remesa_Electronico = 0 OR DMCA.Numero_Anulacion_Remesa_Electronico IS NULL)  
  AND (DMCA.Numero_Anulacion_Cumplido_Electronico > 0 OR( DMCA.Numero_Cumplido_Electronico = 0 OR  DMCA.Numero_Cumplido_Electronico IS NULL))  
  AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)            
  AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)          
  AND ENRE.Numero_Documento = ISNULL(@par_ENRE_NumeroDocumento,ENRE.Numero_Documento)          
  AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)          
  AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino          
  AND (ENRE.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)

 )                  
          
 SELECT          
 Obtener,          
 OpcionReporte,          
 EMPR_Codigo,          
 Numero,          
 NumeroDocumento,          
 Fecha,          
 TERC_Codigo_Cliente,          
 NombreCliente,          
 TERC_Codigo_Remitente,          
 NombreRemitente,          
 VEHI_Codigo,          
 PlacaVehiculo,          
 SEMI_Codigo,          
 PlacaSemirremolque,          
 RUTA_Codigo,          
 NombreRuta,          
 ENMC_Numero,          
 NumeroDocumentoManifiesto,          
 ESOS_Numero,          
 NumeroDocumentoOrdenservicio,          
 Mensaje,          
           
 @CantidadRegistros AS TotalRegistros,          
 @par_NumeroPagina AS PaginaObtener,          
 @par_RegistrosPagina AS RegistrosPagina          
          
 FROM Pagina          
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)           
END   
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_cumplido_anuladas_reporte_rndc]     
(                  
 @par_EMPR_Codigo  smallint,          
 @par_ENRE_NumeroDocumento NUMERIC = NULL,          
 @par_ENMC_NumeroDocumento NUMERIC = NULL,          
 @par_Fecha_Inicial date = NULL,          
 @par_Fecha_Final date = NULL,            
 @par_OFIC_Codigo NUMERIC = NULL,     
 @par_NumeroPagina INT = NULL,                  
 @par_RegistrosPagina INT = NULL                  
)                  
AS                  
BEGIN                  
          
 DECLARE @CantidadRegistros NUMERIC            
 SELECT @CantidadRegistros = (                  
  SELECT DISTINCT COUNT(1)                   
  FROM Detalle_Manifiesto_Carga AS DMCA            
            
  LEFT JOIN Encabezado_Remesas AS ENRE            
  ON DMCA.EMPR_Codigo = ENRE.EMPR_Codigo             
  AND DMCA.ENRE_Numero = ENRE.Numero            
              
  LEFT JOIN Terceros AS CLIE             
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo            
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo            
          
  LEFT JOIN Terceros AS REMI             
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo            
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo            
              
  LEFT JOIN Vehiculos AS VEHI             
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo            
  AND ENRE.VEHI_Codigo = VEHI.Codigo          
          
  LEFT JOIN Semirremolques AS SEMI          
  ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo            
  AND ENRE.SEMI_Codigo = SEMI.Codigo            
              
  LEFT JOIN Rutas AS RUTA             
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo            
  AND ENRE.RUTA_Codigo = RUTA.Codigo            
              
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS            
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo            
  AND ENRE.ESOS_Numero = ESOS.Numero          
                
  LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC            
  ON DMCA.EMPR_Codigo = ENMC.EMPR_Codigo             
  AND DMCA.ENMC_Numero = ENMC.Numero          
  
  LEFT JOIN Encabezado_Cumplido_Planilla_Despachos AS ENCP               
  ON ENMC.EMPR_Codigo = ENCP.EMPR_Codigo              
  AND ENMC.ENPD_Numero = ENCP.ENPD_Numero   
  
  WHERE DMCA.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENRE.Estado = 1-- Estado Remesa Definitivo          
  AND ENMC.Estado = 1 -- Manifiesto en Definitivo       
  AND ENCP.Anulado = 1  -- Remesa No Anulada            
  AND ENCP.Numero_Anulacion_Cumplido_Electronico > 0  
  AND DMCA.Numero_Remesa_Electronico > 0     
  --AND ENRE.ENMC_Numero = ENMC.Numero          
  AND (DMCA.Numero_Anulacion_Remesa_Electronico = 0 OR DMCA.Numero_Anulacion_Remesa_Electronico IS NULL)--Cumplido de remesa sin reportar          
  AND (DMCA.Numero_Anulacion_Cumplido_Electronico = 0 OR DMCA.Numero_Anulacion_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar          
  AND DMCA.Numero_Cumplido_Electronico > 0   
  AND ENMC.Numero_Anulacion_Cumplido_Electronico > 0  
  AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)            
  AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)          
  AND ENRE.Numero_Documento = ISNULL(@par_ENRE_NumeroDocumento,ENRE.Numero_Documento)          
  AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)          
  AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino      
  AND (ENRE.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)
 );                  
 WITH Pagina                  
 AS                  
 ( SELECT          
  0 As Obtener,          
  1 AS OpcionReporte,          
  ENRE.EMPR_Codigo,          
  ENRE.Numero,          
  ENRE.Numero_Documento AS NumeroDocumento,          
  ENRE.Fecha,          
  ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente,          
  ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente,          
  ISNULL(ENRE.TERC_Codigo_Remitente, 0) AS TERC_Codigo_Remitente,  
    /*Modificación FP 2021/12/12*/
	ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente,  
	/*Fin Modificación*/  
  ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo,          
  ISNULL(VEHI.Placa,0) AS PlacaVehiculo,          
  ISNULL(ENRE.SEMI_Codigo, 0) AS SEMI_Codigo,          
  ISNULL(SEMI.Placa, 0) AS PlacaSemirremolque,          
  ISNULL(ENRE.RUTA_Codigo,0) AS RUTA_Codigo,          
  ISNULL(RUTA.Nombre, 0) AS NombreRuta,          
  ISNULL(ENRE.ENMC_Numero, 0) AS ENMC_Numero,          
  ENMC.Numero_Documento AS NumeroDocumentoManifiesto,          
  ENRE.ESOS_Numero,          
  ISNULL(ESOS.Numero_Documento, 0) AS NumeroDocumentoOrdenservicio,          
  ISNULL(DMCA.Mensaje_Anulacion_Cumplido_Electronico, '') AS Mensaje,          
            
  ROW_NUMBER() OVER (ORDER BY ENRE.Numero) AS RowNumber                  
          
  FROM Detalle_Manifiesto_Carga AS DMCA            
            
  LEFT JOIN Encabezado_Remesas AS ENRE            
  ON DMCA.EMPR_Codigo = ENRE.EMPR_Codigo             
  AND DMCA.ENRE_Numero = ENRE.Numero            
              
  LEFT JOIN Terceros AS CLIE             
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo            
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo            
          
  LEFT JOIN Terceros AS REMI             
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo              
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo            
              
  LEFT JOIN Vehiculos AS VEHI             
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo            
  AND ENRE.VEHI_Codigo = VEHI.Codigo          
          
  LEFT JOIN Semirremolques AS SEMI          
  ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo            
  AND ENRE.SEMI_Codigo = SEMI.Codigo            
              
  LEFT JOIN Rutas AS RUTA             
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo            
  AND ENRE.RUTA_Codigo = RUTA.Codigo            
              
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS            
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo            
  AND ENRE.ESOS_Numero = ESOS.Numero            
                
  LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC            
  ON DMCA.EMPR_Codigo = ENMC.EMPR_Codigo             
  AND DMCA.ENMC_Numero = ENMC.Numero          
  
  LEFT JOIN Encabezado_Cumplido_Planilla_Despachos AS ENCP               
  ON ENMC.EMPR_Codigo = ENCP.EMPR_Codigo              
  AND ENMC.ENPD_Numero = ENCP.ENPD_Numero   
            
  WHERE DMCA.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENRE.Estado = 1-- Estado Remesa Definitivo          
  AND ENMC.Estado = 1 -- Manifiesto en Definitivo       
  AND ENCP.Anulado = 1  -- Remesa No Anulada            
  AND ENCP.Numero_Anulacion_Cumplido_Electronico > 0  
  AND DMCA.Numero_Remesa_Electronico > 0     
  --AND ENRE.ENMC_Numero = ENMC.Numero          
  AND (DMCA.Numero_Anulacion_Remesa_Electronico = 0 OR DMCA.Numero_Anulacion_Remesa_Electronico IS NULL)--Cumplido de remesa sin reportar          
  AND (DMCA.Numero_Anulacion_Cumplido_Electronico = 0 OR DMCA.Numero_Anulacion_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar          
  AND DMCA.Numero_Cumplido_Electronico > 0   
  AND ENMC.Numero_Anulacion_Cumplido_Electronico > 0  
  AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)            
  AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)          
  AND ENRE.Numero_Documento = ISNULL(@par_ENRE_NumeroDocumento,ENRE.Numero_Documento)          
  AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)          
  AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino        
  AND (ENRE.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)
 )                  
          
 SELECT          
 Obtener,          
 OpcionReporte,          
 EMPR_Codigo,          
 Numero,          
 NumeroDocumento,          
 Fecha,          
 TERC_Codigo_Cliente,          
 NombreCliente,          
 TERC_Codigo_Remitente,          
 NombreRemitente,          
 VEHI_Codigo,          
 PlacaVehiculo,          
 SEMI_Codigo,          
 PlacaSemirremolque,          
 RUTA_Codigo,          
 NombreRuta,          
 ENMC_Numero,          
 NumeroDocumentoManifiesto,          
 ESOS_Numero,          
 NumeroDocumentoOrdenservicio,          
 Mensaje,          
           
 @CantidadRegistros AS TotalRegistros,          
 @par_NumeroPagina AS PaginaObtener,          
 @par_RegistrosPagina AS RegistrosPagina          
          
 FROM Pagina          
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)           
END   
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_cumplido_inicial_reporte_rndc]         
(                      
 @par_EMPR_Codigo  smallint,              
 @par_ENRE_NumeroDocumento NUMERIC = NULL,              
 @par_ENMC_NumeroDocumento NUMERIC = NULL,              
 @par_Fecha_Inicial date = NULL,              
 @par_Fecha_Final date = NULL,     
  @par_OFIC_Codigo NUMERIC = NULL,         
 @par_NumeroPagina INT = NULL,                      
 @par_RegistrosPagina INT = NULL                      
)                      
AS                      
BEGIN                      
              
 DECLARE @CantidadRegistros NUMERIC                
 SELECT @CantidadRegistros = (                      
  SELECT DISTINCT COUNT(1)                       
  FROM Detalle_Manifiesto_Carga AS DMCA                
                
  LEFT JOIN Encabezado_Remesas AS ENRE                
  ON DMCA.EMPR_Codigo = ENRE.EMPR_Codigo                 
  AND DMCA.ENRE_Numero = ENRE.Numero                
                  
  LEFT JOIN Terceros AS CLIE                 
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                
              
  LEFT JOIN Terceros AS REMI                 
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                
                  
  LEFT JOIN Vehiculos AS VEHI                 
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                
  AND ENRE.VEHI_Codigo = VEHI.Codigo              
              
  LEFT JOIN Semirremolques AS SEMI              
  ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo                
  AND ENRE.SEMI_Codigo = SEMI.Codigo                
                  
  LEFT JOIN Rutas AS RUTA                 
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                
  AND ENRE.RUTA_Codigo = RUTA.Codigo                
                  
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS                
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                
  AND ENRE.ESOS_Numero = ESOS.Numero              
                    
  LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC                
  ON DMCA.EMPR_Codigo = ENMC.EMPR_Codigo                 
  AND DMCA.ENMC_Numero = ENMC.Numero              
                
  WHERE DMCA.EMPR_Codigo = @par_EMPR_Codigo            
  AND ENRE.Estado = 1-- Estado Remesa Definitivo              
  AND ENRE.Cumplido = 0-- Estado Remesa Definitivo              
  AND ENRE.Anulado = 0  -- Remesa No Anulada              
  AND ENMC.Estado = 1 -- Manifiesto en Definitivo            
  AND (DMCA.Numero_Remesa_Electronico > 0)          
  AND (ENMC.Numero_Manifiesto_Electronico > 0)          
  AND (DMCA.Numero_Cumplido_Inicial_Electronico = 0 OR DMCA.Numero_Cumplido_Inicial_Electronico IS NULL)--Cumplido de remesa sin reportar              
  AND (DMCA.Numero_Cumplido_Electronico = 0 OR DMCA.Numero_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar           
  AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)                
  AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)              
  AND ENRE.Numero_Documento = ISNULL(@par_ENRE_NumeroDocumento,ENRE.Numero_Documento)              
  AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)              
  AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino           
  AND EXISTS(SELECT * FROM Detalle_Tiempos_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = ENRE.Numero AND CATA_TLRE_Codigo = 20205)--Salida Cargue      
  AND EXISTS(SELECT * FROM Detalle_Tiempos_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = ENRE.Numero AND CATA_TLRE_Codigo = 20206)--LLegada Descargue      
  AND (ENRE.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)
 );                      
 WITH Pagina                      
 AS                      
 ( SELECT              
  0 As Obtener,              
  1 AS OpcionReporte,              
  ENRE.EMPR_Codigo,              
  ENRE.Numero,              
  ENRE.Numero_Documento AS NumeroDocumento,              
  ENRE.Fecha,              
  ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente,              
  ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente,              
  ISNULL(ENRE.TERC_Codigo_Remitente, 0) AS TERC_Codigo_Remitente, 
    /*Modificación FP 2021/12/12*/
	ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente,  
	/*Fin Modificación*/  
  ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo,              
  ISNULL(VEHI.Placa,0) AS PlacaVehiculo,              
  ISNULL(ENRE.SEMI_Codigo, 0) AS SEMI_Codigo,              
  ISNULL(SEMI.Placa, 0) AS PlacaSemirremolque,              
  ISNULL(ENRE.RUTA_Codigo,0) AS RUTA_Codigo,              
  ISNULL(RUTA.Nombre, 0) AS NombreRuta,              
  ISNULL(ENRE.ENMC_Numero, 0) AS ENMC_Numero,              
  ENMC.Numero_Documento AS NumeroDocumentoManifiesto,              
  ENRE.ESOS_Numero,              
  ISNULL(ESOS.Numero_Documento, 0) AS NumeroDocumentoOrdenservicio,              
  ISNULL(DMCA.Mensaje_Cumplido_Inicial_Electronico, '') AS Mensaje,              
                
  ROW_NUMBER() OVER (ORDER BY ENRE.Numero) AS RowNumber                      
              
  FROM Detalle_Manifiesto_Carga AS DMCA                
                
  LEFT JOIN Encabezado_Remesas AS ENRE                
  ON DMCA.EMPR_Codigo = ENRE.EMPR_Codigo                 
  AND DMCA.ENRE_Numero = ENRE.Numero                
                  
  LEFT JOIN Terceros AS CLIE                 
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                
              
  LEFT JOIN Terceros AS REMI                 
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                  
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                
                  
  LEFT JOIN Vehiculos AS VEHI                 
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                
  AND ENRE.VEHI_Codigo = VEHI.Codigo              
              
  LEFT JOIN Semirremolques AS SEMI              
  ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo                
  AND ENRE.SEMI_Codigo = SEMI.Codigo                
                  
  LEFT JOIN Rutas AS RUTA                 
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                
  AND ENRE.RUTA_Codigo = RUTA.Codigo                
                  
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS                
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                
  AND ENRE.ESOS_Numero = ESOS.Numero                
                    
  LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC                
  ON DMCA.EMPR_Codigo = ENMC.EMPR_Codigo                 
  AND DMCA.ENMC_Numero = ENMC.Numero              
                
  WHERE DMCA.EMPR_Codigo = @par_EMPR_Codigo            
   AND ENRE.Estado = 1-- Estado Remesa Definitivo              
  AND ENRE.Cumplido = 0-- Estado Remesa Definitivo              
  AND ENRE.Anulado = 0  -- Remesa No Anulada              
  AND ENMC.Estado = 1 -- Manifiesto en Definitivo            
  AND (DMCA.Numero_Remesa_Electronico > 0)          
  AND (ENMC.Numero_Manifiesto_Electronico > 0)          
  AND (DMCA.Numero_Cumplido_Inicial_Electronico = 0 OR DMCA.Numero_Cumplido_Inicial_Electronico IS NULL)--Cumplido de remesa sin reportar              
  AND (DMCA.Numero_Cumplido_Electronico = 0 OR DMCA.Numero_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar              
  AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)                
  AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)              
  AND ENRE.Numero_Documento = ISNULL(@par_ENRE_NumeroDocumento,ENRE.Numero_Documento)              
  AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)              
  AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino           
  AND EXISTS(SELECT * FROM Detalle_Tiempos_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = ENRE.Numero AND CATA_TLRE_Codigo = 20205)--Salida Cargue      
  AND EXISTS(SELECT * FROM Detalle_Tiempos_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = ENRE.Numero AND CATA_TLRE_Codigo = 20206)--LLegada Descargue      
  AND (ENRE.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)
 )                      
              
 SELECT              
 Obtener,              
 OpcionReporte,              
 EMPR_Codigo,              
 Numero,              
 NumeroDocumento,              
 Fecha,              
 TERC_Codigo_Cliente,              
 NombreCliente,              
 TERC_Codigo_Remitente,              
 NombreRemitente,              
 VEHI_Codigo,              
 PlacaVehiculo,              
 SEMI_Codigo,              
 PlacaSemirremolque,              
 RUTA_Codigo,              
 NombreRuta,              
 ENMC_Numero,              
 NumeroDocumentoManifiesto,              
 ESOS_Numero,              
 NumeroDocumentoOrdenservicio,              
 Mensaje,              
               
 @CantidadRegistros AS TotalRegistros,              
 @par_NumeroPagina AS PaginaObtener,              
 @par_RegistrosPagina AS RegistrosPagina              
              
 FROM Pagina              
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)              
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)               
END           
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_cumplido_reporte_rndc]     
(                  
 @par_EMPR_Codigo  smallint,          
 @par_ENRE_NumeroDocumento NUMERIC = NULL,          
 @par_ENMC_NumeroDocumento NUMERIC = NULL,          
 @par_Fecha_Inicial date = NULL,          
 @par_Fecha_Final date = NULL,            
  @par_OFIC_Codigo NUMERIC = NULL,            
 @par_NumeroPagina INT = NULL,                  
 @par_RegistrosPagina INT = NULL                  
)                  
AS                  
BEGIN                  
          
 DECLARE @CantidadRegistros NUMERIC            
 SELECT @CantidadRegistros = (                  
  SELECT DISTINCT COUNT(1)                   
  FROM Detalle_Manifiesto_Carga AS DMCA            
            
  LEFT JOIN Encabezado_Remesas AS ENRE            
  ON DMCA.EMPR_Codigo = ENRE.EMPR_Codigo             
  AND DMCA.ENRE_Numero = ENRE.Numero            
              
  LEFT JOIN Terceros AS CLIE             
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo            
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo            
          
  LEFT JOIN Terceros AS REMI             
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo            
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo            
              
  LEFT JOIN Vehiculos AS VEHI             
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo            
  AND ENRE.VEHI_Codigo = VEHI.Codigo          
          
  LEFT JOIN Semirremolques AS SEMI          
  ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo            
  AND ENRE.SEMI_Codigo = SEMI.Codigo            
              
  LEFT JOIN Rutas AS RUTA             
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo            
  AND ENRE.RUTA_Codigo = RUTA.Codigo            
              
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS            
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo            
  AND ENRE.ESOS_Numero = ESOS.Numero          
                
  LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC            
  ON DMCA.EMPR_Codigo = ENMC.EMPR_Codigo             
  AND DMCA.ENMC_Numero = ENMC.Numero          
            
  WHERE DMCA.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENRE.Estado = 1-- Estado Remesa Definitivo          
  AND ENRE.Cumplido = 1-- Estado Remesa Definitivo          
  AND ENRE.Anulado = 0  -- Remesa No Anulada          
  AND ENMC.Estado = 1 -- Manifiesto en Definitivo        
  AND (ENMC.Numero_Manifiesto_Electronico > 0)      
  AND (DMCA.Numero_Remesa_Electronico > 0)      
  AND ENRE.ENMC_Numero = ENMC.Numero          
  AND (DMCA.Numero_Cumplido_Electronico = 0 OR DMCA.Numero_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar          
  AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)            
  AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)          
  AND ENRE.Numero_Documento = ISNULL(@par_ENRE_NumeroDocumento,ENRE.Numero_Documento)          
  AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)          
  AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino        
   AND (ENRE.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)
 );                  
 WITH Pagina                  
 AS                  
 ( SELECT          
  0 As Obtener,          
  1 AS OpcionReporte,          
  ENRE.EMPR_Codigo,          
  ENRE.Numero,          
  ENRE.Numero_Documento AS NumeroDocumento,          
  ENRE.Fecha,          
  ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente,          
  ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente,          
  ISNULL(ENRE.TERC_Codigo_Remitente, 0) AS TERC_Codigo_Remitente, 
    /*Modificación FP 2021/12/12*/
	ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente,
  	/*Fin Modificación*/
  ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo,          
  ISNULL(VEHI.Placa,0) AS PlacaVehiculo,          
  ISNULL(ENRE.SEMI_Codigo, 0) AS SEMI_Codigo,          
  ISNULL(SEMI.Placa, 0) AS PlacaSemirremolque,          
  ISNULL(ENRE.RUTA_Codigo,0) AS RUTA_Codigo,          
  ISNULL(RUTA.Nombre, 0) AS NombreRuta,          
  ISNULL(ENRE.ENMC_Numero, 0) AS ENMC_Numero,          
  ENMC.Numero_Documento AS NumeroDocumentoManifiesto,          
  ENRE.ESOS_Numero,          
  ISNULL(ESOS.Numero_Documento, 0) AS NumeroDocumentoOrdenservicio,          
  ISNULL(DMCA.Mensaje_Cumplido_Electronico, '') AS Mensaje,          
            
  ROW_NUMBER() OVER (ORDER BY ENRE.Numero) AS RowNumber                  
          
  FROM Detalle_Manifiesto_Carga AS DMCA            
            
  LEFT JOIN Encabezado_Remesas AS ENRE            
  ON DMCA.EMPR_Codigo = ENRE.EMPR_Codigo             
  AND DMCA.ENRE_Numero = ENRE.Numero            
              
  LEFT JOIN Terceros AS CLIE             
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo            
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo            
          
  LEFT JOIN Terceros AS REMI             
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo              
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo            
              
  LEFT JOIN Vehiculos AS VEHI             
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo            
  AND ENRE.VEHI_Codigo = VEHI.Codigo          
          
  LEFT JOIN Semirremolques AS SEMI          
  ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo            
  AND ENRE.SEMI_Codigo = SEMI.Codigo            
              
  LEFT JOIN Rutas AS RUTA             
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo            
  AND ENRE.RUTA_Codigo = RUTA.Codigo            
              
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS            
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo            
  AND ENRE.ESOS_Numero = ESOS.Numero            
                
  LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC            
  ON DMCA.EMPR_Codigo = ENMC.EMPR_Codigo             
  AND DMCA.ENMC_Numero = ENMC.Numero          
            
  WHERE DMCA.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENRE.Estado = 1-- Estado Remesa Definitivo          
  AND ENRE.Cumplido = 1-- Estado Remesa Definitivo          
  AND ENRE.Anulado = 0  -- Remesa No Anulada          
  AND ENMC.Estado = 1 -- Manifiesto en Definitivo        
  AND (ENMC.Numero_Manifiesto_Electronico > 0)      
  AND (DMCA.Numero_Remesa_Electronico > 0)      
  AND ENRE.ENMC_Numero = ENMC.Numero          
  AND (DMCA.Numero_Cumplido_Electronico = 0 OR DMCA.Numero_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar          
  AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)            
  AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)          
  AND ENRE.Numero_Documento = ISNULL(@par_ENRE_NumeroDocumento,ENRE.Numero_Documento)          
  AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)          
  AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino        
   AND (ENRE.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)
 )                  
          
 SELECT          
 Obtener,          
 OpcionReporte,          
 EMPR_Codigo,          
 Numero,          
 NumeroDocumento,          
 Fecha,          
 TERC_Codigo_Cliente,          
 NombreCliente,          
 TERC_Codigo_Remitente,          
 NombreRemitente,          
 VEHI_Codigo,          
 PlacaVehiculo,          
 SEMI_Codigo,          
 PlacaSemirremolque,          
 RUTA_Codigo,          
 NombreRuta,          
 ENMC_Numero,          
 NumeroDocumentoManifiesto,          
 ESOS_Numero,          
 NumeroDocumentoOrdenservicio,          
 Mensaje,          
           
 @CantidadRegistros AS TotalRegistros,          
 @par_NumeroPagina AS PaginaObtener,          
 @par_RegistrosPagina AS RegistrosPagina          
          
 FROM Pagina          
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)           
END       
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_pendientes_legalizar_paqueteria]
(                                                                        
	@par_EMPR_Codigo SMALLINT,                  
	@par_TIDO_Codigo NUMERIC = NULL,                  
	@par_NumeroInicial NUMERIC = NULL,                  
	@par_FechaInicial DATE = NULL,                  
	@par_FechaFinal DATE = NULL,                    
	@par_TERC_Codigo_Conductor NUMERIC = NULL,              
	@par_ENPD_Numero NUMERIC = NULL,                
	@par_OFIC_Actual NUMERIC = NULL                
)              
AS                                                                                 
BEGIN          
 DECLARE @FormaPagoContado INT = NULL          
 DECLARE @FormaPagoContraEntrega INT = NULL     
 IF @par_FechaInicial <> NULL BEGIN                                                                        
 SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                                                        
 END                                                                        
                                                                        
 IF @par_FechaFinal <> NULL BEGIN                                                                        
 SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                                                        
 END          
           
           
 IF @par_TERC_Codigo_Conductor IS NOT NULL          
 BEGIN          
  SELECT          
  ENRE.EMPR_Codigo                
  ,ENRE.Numero                
  ,ENRE.Numero_Documento                
  ,ENRE.TIDO_Codigo                
  ,ENRE.Numeracion                
  ,ENRE.Fecha                
  ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                
  ,ENRE.TERC_Codigo_Cliente                
  ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                
  ,ENRE.TERC_Codigo_Remitente 
      /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	/*Fin Modificación*/  
  ,ENRE.TERC_Codigo_Destinatario
     /*Modificación FP 2021/12/12*/
  	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
  	/*Fin Modificación*/
  ,ENRE.CATA_FOPR_Codigo                                             
  ,FOPR.Campo1 AS FormaPago                                                                        
  ,ENRE.RUTA_Codigo                                                                        
  ,ENRP.CIUD_Codigo_Origen                             
  ,ENRP.CIUD_Codigo_Destino                                                          
  ,CIOR.Nombre AS CiudadOrigen                
  ,CIDE.Nombre AS CiudadDestino                
  ,ENRP.OFIC_Codigo_Actual                
  ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                
  ,ENRP.CATA_ESRP_Codigo                
  ,ESRP.Campo1 AS NombreEstadoRemesa                
  ,ENRE.Observaciones                          
  ,(ENRE.Valor_Flete_Cliente) AS  Valor_Flete_Cliente                  
  ,ENRE.Total_Flete_Cliente AS Total_Flete_Cliente                
  ,ENRE.Cantidad_Cliente                                                                        
  ,ENRE.Peso_Cliente                                                                        
  ,ENRE.Estado                                                                        
  ,ENRE.Anulado                                                                        
  ,PRTR.Nombre AS NombreProducto                                                                        
  ,ENRE.Documento_Cliente                                                           
  ,ENRE.OFIC_Codigo                                                          
  ,OFIC.Nombre As NombreOficina           
  ,'' AS NombreRuta                                                          
  ,ENRE.Valor_Flete_Transportador                                             
  ,ENRE.Valor_Reexpedicion                                      
  ,ENRP.Reexpedicion                
  ,VEHI.Placa                
  ,ENPD.Numero_Documento AS NumeroDocumentoPlanilla                
  ,ENPD.Fecha AS FechaPlanilla            
  ,ELRG.Numero_Documento AS ELRG_NumeroDocumento            
  ,ISNULL(ENRP.Guia_Creada_Ruta_Conductor, 0) Guia_Creada_Ruta_Conductor              
  ,ISNULL(DELG.Gestion_Documentos, -1) AS Gestion_Documentos_Legalizar              
  ,ISNULL(DELG.Entrega_Recaudo, -1) AS Entrega_Recaudo_Legalizar           
              
  FROM Remesas_Paqueteria AS ENRP                         
                                                                   
  INNER JOIN Encabezado_Remesas AS ENRE                                                                        
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                               
  AND ENRP.ENRE_Numero = ENRE.Numero              
              
  LEFT JOIN (      
 SELECT EMPR_Codigo, ENRE_Numero, MAX(Gestion_Documentos) AS Gestion_Documentos, MAX(Entrega_Recaudo) AS Entrega_Recaudo        
 from Detalle_Legalizacion_Guias      
 WHERE Legalizo = 0      
 GROUP BY EMPR_Codigo, ENRE_Numero      
  ) AS DELG      
  ON ENRE.EMPR_Codigo = DELG.EMPR_Codigo              
  AND ENRE.Numero = DELG.ENRE_Numero               
                                                                    
  INNER JOIN Oficinas AS OFIC ON                                                          
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                                          
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                        
                                                        
  LEFT JOIN Terceros AS CLIE                                                      
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                        
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                        
                                                                        
  LEFT JOIN Terceros AS REMI                                                                        
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                        
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                        
                                                                        
  LEFT JOIN Terceros AS DEST                                                                        
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                        
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                    
                                                                        
  LEFT JOIN Producto_Transportados AS PRTR                                                                        
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                          
  AND ENRE.PRTR_Codigo = PRTR.Codigo                  
                  
  LEFT JOIN Ciudades CIOR                                                                        
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                 
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                        
                                                              
  LEFT JOIN Ciudades CIDE                                                                        
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                                        
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                                  
                                      
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                                  
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                                  
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                                               
                                                                        
  INNER JOIN Oficinas AS OFAC ON                                                          
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                                          
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                                  
                                                      
  LEFT JOIN Valor_Catalogos AS FOPR ON                
  ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                
  AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                
                
  LEFT JOIN Valor_Catalogos AS ESRP ON                
  ENRP.EMPR_Codigo = ESRP.EMPR_Codigo                
  AND ENRP.CATA_ESRP_Codigo = ESRP.Codigo                
                
  LEFT JOIN Encabezado_Planilla_Despachos ENPD ON                
  ENPD.EMPR_Codigo = ENRP.EMPR_Codigo                
  AND ENPD.Numero = ENRP.ENPD_Numero_Ultima_Planilla            
            
  LEFT JOIN Encabezado_Legalizacion_Recaudo_Guias ELRG ON                
  ELRG.EMPR_Codigo = ENRE.EMPR_Codigo                
  AND ELRG.Numero = ENRE.ELRG_Numero          
          
  LEFT JOIN Vehiculos VEHI ON                
  VEHI.EMPR_Codigo = ENRE.EMPR_Codigo                
  AND VEHI.Codigo = ENRE.VEHI_Codigo                
                
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                
  AND ENRE.Numero_Documento = ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                    
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                    
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                
  AND(ENRP.TERC_Codigo_Ultimo_Conductor =  @par_TERC_Codigo_Conductor OR @par_TERC_Codigo_Conductor IS NULL)          
  AND (ENPD.Numero_Documento = @par_ENPD_Numero OR @par_ENPD_Numero IS NULL)              
  AND (ENRP.CATA_ESRP_Codigo IN(6010, 6030))--Despachada, Cumplida          
  AND ISNULL(ENRP.Legalizo_Guia,0) = 0          
  AND ENRE.Anulado = 0                                                                      
  AND ENRE.Estado = 1          
            
  UNION           
  --Consultar Remesas Manuales donde el aforador = conductor          
  SELECT          
  ENRE.EMPR_Codigo                
  ,ENRE.Numero                
  ,ENRE.Numero_Documento                
  ,ENRE.TIDO_Codigo                
  ,ENRE.Numeracion                
  ,ENRE.Fecha                
  ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                
  ,ENRE.TERC_Codigo_Cliente                
  ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                
  ,ENRE.TERC_Codigo_Remitente    
    /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	/*Fin Modificación*/  
  ,ENRE.TERC_Codigo_Destinatario
     /*Modificación FP 2021/12/12*/
  	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
  	/*Fin Modificación*/  
  ,ENRE.CATA_FOPR_Codigo                                             
  ,FOPR.Campo1 AS FormaPago                                                                        
  ,ENRE.RUTA_Codigo                                                                        
  ,ENRP.CIUD_Codigo_Origen                             
  ,ENRP.CIUD_Codigo_Destino                                                          
  ,CIOR.Nombre AS CiudadOrigen           
  ,CIDE.Nombre AS CiudadDestino                
  ,ENRP.OFIC_Codigo_Actual                
  ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                
  ,ENRP.CATA_ESRP_Codigo                
  ,ESRP.Campo1 AS NombreEstadoRemesa                
  ,ENRE.Observaciones                          
  ,(ENRE.Valor_Flete_Cliente) AS  Valor_Flete_Cliente                  
  ,ENRE.Total_Flete_Cliente AS Total_Flete_Cliente                
  ,ENRE.Cantidad_Cliente                                                                        
  ,ENRE.Peso_Cliente                                                                        
  ,ENRE.Estado                                                                        
  ,ENRE.Anulado                                                                        
,PRTR.Nombre AS NombreProducto                                                                        
  ,ENRE.Documento_Cliente                                                           
  ,ENRE.OFIC_Codigo                                                          
  ,OFIC.Nombre As NombreOficina                                                                       
  ,'' AS NombreRuta                                                          
  ,ENRE.Valor_Flete_Transportador                                             
  ,ENRE.Valor_Reexpedicion                                      
  ,ENRP.Reexpedicion                
  ,VEHI.Placa                
  ,ENPD.Numero_Documento AS NumeroDocumentoPlanilla                
  ,ENPD.Fecha AS FechaPlanilla            
  ,ELRG.Numero_Documento AS ELRG_NumeroDocumento            
  ,ISNULL(ENRP.Guia_Creada_Ruta_Conductor, 0) Guia_Creada_Ruta_Conductor              
  ,ISNULL(DELG.Gestion_Documentos, -1) AS Gestion_Documentos_Legalizar              
  ,ISNULL(DELG.Entrega_Recaudo, -1) AS Entrega_Recaudo_Legalizar             
              
  FROM Remesas_Paqueteria AS ENRP                         
                                                                   
  INNER JOIN Encabezado_Remesas AS ENRE                                                                        
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                               
  AND ENRP.ENRE_Numero = ENRE.Numero              
              
  LEFT JOIN (      
 SELECT EMPR_Codigo, ENRE_Numero, MAX(Gestion_Documentos) AS Gestion_Documentos, MAX(Entrega_Recaudo) AS Entrega_Recaudo        
 from Detalle_Legalizacion_Guias      
 WHERE Legalizo = 0      
 GROUP BY EMPR_Codigo, ENRE_Numero      
  ) AS DELG      
  ON ENRE.EMPR_Codigo = DELG.EMPR_Codigo              
  AND ENRE.Numero = DELG.ENRE_Numero               
                                                                    
  INNER JOIN Oficinas AS OFIC ON                                                          
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                                          
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                        
                                                        
  LEFT JOIN Terceros AS CLIE                                                      
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                        
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                        
                                                                        
  LEFT JOIN Terceros AS REMI                                                                        
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                        
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                        
                                                                        
  LEFT JOIN Terceros AS DEST                                                                        
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                     
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                    
                                                                        
  LEFT JOIN Producto_Transportados AS PRTR                                                                        
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                          
  AND ENRE.PRTR_Codigo = PRTR.Codigo                  
                  
  LEFT JOIN Ciudades CIOR                                 
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                 
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                        
                                                              
  LEFT JOIN Ciudades CIDE                                                                        
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                         
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                                  
                                                     
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                                  
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                                  
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                                                
                                                                        
  INNER JOIN Oficinas AS OFAC ON                                                          
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                                          
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                                  
                                                      
  LEFT JOIN Valor_Catalogos AS FOPR ON                
  ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                
  AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                
                
  LEFT JOIN Valor_Catalogos AS ESRP ON                
  ENRP.EMPR_Codigo = ESRP.EMPR_Codigo                
  AND ENRP.CATA_ESRP_Codigo = ESRP.Codigo                
                
  LEFT JOIN Encabezado_Planilla_Despachos ENPD ON                
  ENPD.EMPR_Codigo = ENRP.EMPR_Codigo                
  AND ENPD.Numero = ENRP.ENPD_Numero_Ultima_Planilla            
            
  LEFT JOIN Encabezado_Legalizacion_Recaudo_Guias ELRG ON                
  ELRG.EMPR_Codigo = ENRE.EMPR_Codigo                
  AND ELRG.Numero = ENRE.ELRG_Numero          
          
  LEFT JOIN Vehiculos VEHI ON                
  VEHI.EMPR_Codigo = ENRE.EMPR_Codigo                
  AND VEHI.Codigo = ENRE.VEHI_Codigo                
                
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                
  AND ENRP.TERC_Codigo_Aforador =  @par_TERC_Codigo_Conductor          
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                
  AND ENRE.Numero_Documento = ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                    
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                    
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)          
  AND (ENPD.Numero_Documento = @par_ENPD_Numero OR @par_ENPD_Numero IS NULL)              
  AND ISNULL(ENRP.Legalizo_Guia,0) = 0          
  AND ISNULL(ENRE.Numeracion, '') != ''        
  AND ENRE.Anulado = 0                                                                      
  AND ENRE.Estado = 1          
            
          
  ORDER BY Numero ASC          
 END          
          
 IF @par_OFIC_Actual IS NOT NULL          
 BEGIN          
  SET @FormaPagoContado = 4902  
  SET @FormaPagoContraEntrega = 4903  
  SELECT          
  ENRE.EMPR_Codigo                
  ,ENRE.Numero                
  ,ENRE.Numero_Documento                
  ,ENRE.TIDO_Codigo                
  ,ENRE.Numeracion                
  ,ENRE.Fecha                
  ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                
  ,ENRE.TERC_Codigo_Cliente                
  ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                
  ,ENRE.TERC_Codigo_Remitente 
    /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	/*Fin Modificación*/  
  ,ENRE.TERC_Codigo_Destinatario  
  /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
	/*Fin Modificación*/  
  ,ENRE.CATA_FOPR_Codigo                                             
  ,FOPR.Campo1 AS FormaPago                                                                        
  ,ENRE.RUTA_Codigo                                                                        
  ,ENRP.CIUD_Codigo_Origen                             
  ,ENRP.CIUD_Codigo_Destino                                                          
  ,CIOR.Nombre AS CiudadOrigen                
  ,CIDE.Nombre AS CiudadDestino                
  ,ENRP.OFIC_Codigo_Actual                
  ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual         
  ,ENRP.CATA_ESRP_Codigo                
  ,ESRP.Campo1 AS NombreEstadoRemesa                
  ,ENRE.Observaciones                          
  ,(ENRE.Valor_Flete_Cliente) AS  Valor_Flete_Cliente                  
  ,ENRE.Total_Flete_Cliente AS Total_Flete_Cliente                
  ,ENRE.Cantidad_Cliente                                                                        
  ,ENRE.Peso_Cliente                                                                        
  ,ENRE.Estado                                                                        
  ,ENRE.Anulado                                                                        
  ,PRTR.Nombre AS NombreProducto                                                                        
  ,ENRE.Documento_Cliente                                                           
  ,ENRE.OFIC_Codigo                                                          
  ,OFIC.Nombre As NombreOficina                                                                       
  ,'' AS NombreRuta      
  ,ENRE.Valor_Flete_Transportador                                             
  ,ENRE.Valor_Reexpedicion                                      
  ,ENRP.Reexpedicion                
  ,VEHI.Placa                
  ,ENPD.Numero_Documento AS NumeroDocumentoPlanilla                
  ,ENPD.Fecha AS FechaPlanilla            
  ,ELRG.Numero_Documento AS ELRG_NumeroDocumento            
  ,ISNULL(ENRP.Guia_Creada_Ruta_Conductor, 0) Guia_Creada_Ruta_Conductor              
  ,ISNULL(DELG.Gestion_Documentos, -1) AS Gestion_Documentos_Legalizar              
  ,ISNULL(DELG.Entrega_Recaudo, -1) AS Entrega_Recaudo_Legalizar            
              
  FROM Remesas_Paqueteria AS ENRP                         
                                                                   
  INNER JOIN Encabezado_Remesas AS ENRE                                                                        
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                               
  AND ENRP.ENRE_Numero = ENRE.Numero              
              
  LEFT JOIN (      
 SELECT EMPR_Codigo, ENRE_Numero, MAX(Gestion_Documentos) AS Gestion_Documentos, MAX(Entrega_Recaudo) AS Entrega_Recaudo        
 from Detalle_Legalizacion_Guias      
 WHERE Legalizo = 0      
 GROUP BY EMPR_Codigo, ENRE_Numero      
  ) AS DELG      
  ON ENRE.EMPR_Codigo = DELG.EMPR_Codigo              
  AND ENRE.Numero = DELG.ENRE_Numero               
                                                                    
  INNER JOIN Oficinas AS OFIC ON                                                          
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                                          
  AND ENRE.OFIC_Codigo = OFIC.Codigo       
                                                        
  LEFT JOIN Terceros AS CLIE                                                      
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                        
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                        
                                                                        
  LEFT JOIN Terceros AS REMI                                                                        
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                        
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo          
                                                                        
  LEFT JOIN Terceros AS DEST                                                                        
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                        
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                    
                                                                        
  LEFT JOIN Producto_Transportados AS PRTR                                                                        
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                          
  AND ENRE.PRTR_Codigo = PRTR.Codigo                  
              
  LEFT JOIN Ciudades CIOR                                                                        
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                 
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                        
                                                              
  LEFT JOIN Ciudades CIDE                                                                        
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                                        
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                                  
                                                     
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                                  
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                                  
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                                                
                                                                        
  INNER JOIN Oficinas AS OFAC ON                                                          
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                                          
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                                  
                                                      
  LEFT JOIN Valor_Catalogos AS FOPR ON                
  ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                
  AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                
                
  LEFT JOIN Valor_Catalogos AS ESRP ON                
  ENRP.EMPR_Codigo = ESRP.EMPR_Codigo                
  AND ENRP.CATA_ESRP_Codigo = ESRP.Codigo                
                
  LEFT JOIN Encabezado_Planilla_Despachos ENPD ON                
  ENPD.EMPR_Codigo = ENRP.EMPR_Codigo                
  AND ENPD.Numero = ENRP.ENPD_Numero_Ultima_Planilla            
            
  LEFT JOIN Encabezado_Legalizacion_Recaudo_Guias ELRG ON           
  ELRG.EMPR_Codigo = ENRE.EMPR_Codigo                
  AND ELRG.Numero = ENRE.ELRG_Numero             
                
  LEFT JOIN Vehiculos VEHI ON                
  VEHI.EMPR_Codigo = ENRE.EMPR_Codigo                
  AND VEHI.Codigo = ENRE.VEHI_Codigo                
                
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                
  AND ENRE.Numero_Documento = ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                    
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                    
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)          
  AND (ENPD.Numero_Documento = @par_ENPD_Numero OR @par_ENPD_Numero IS NULL)              
  AND ((ENRP.OFIC_Codigo_Origen = @par_OFIC_Actual OR @par_OFIC_Actual IS NULL) OR          
    (ENRP.OFIC_Codigo_Entrega = @par_OFIC_Actual OR @par_OFIC_Actual IS NULL))          
  AND ENRE.CATA_FOPR_Codigo IN(@FormaPagoContado, @FormaPagoContraEntrega)   
  AND ISNULL(ENRE.ELRG_Numero, 0) = 0          
  AND (ENRP.CATA_ESRP_Codigo IN(6005, 6030))--Cumplido          
  AND ISNULL(ENRP.Legalizo_Guia, 0) = 0          
  AND ENRE.Anulado = 0          
  AND ENRE.Estado = 1          
 END          
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_pendientes_legalizar_recaudo]
(                                                                      
	@par_EMPR_Codigo SMALLINT,            
	@par_TIDO_Codigo NUMERIC = NULL,            
	@par_NumeroInicial NUMERIC = NULL,            
	@par_FechaInicial DATE = NULL,            
	@par_FechaFinal DATE = NULL,            
	@par_ENPD_Numero NUMERIC = NULL,             
	@par_ELGU_Numero NUMERIC = NULL,    
	@par_COND_Codigo NUMERIC = NULL,    
	@par_OFIC_Actual_Codigo NUMERIC = NULL    
)                                                                      
AS                                                                               
BEGIN                                                                      
	IF @par_FechaInicial <> NULL BEGIN                                                                      
	SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                                                      
	END                                                                      
                                                                      
	IF @par_FechaFinal <> NULL BEGIN                                                                      
	SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                                                      
	END                
   
	IF @par_COND_Codigo IS NOT NULL OR @par_ENPD_Numero IS NOT NULL  
	BEGIN  
		SELECT    
		ENRE.EMPR_Codigo,        
		ENRE.Numero,            
		ENRE.Numero_Documento,            
		IIF(ENRE.TERC_Codigo_Cliente > 0, ENRE.TERC_Codigo_Cliente, ENRE.TERC_Codigo_Remitente) AS TERC_Codigo_Remitente, 
		/*Modificación FP 2021/12/12*/ 
		ISNULL(ENRE.Nombre_Remitente, IIF(ENRE.TERC_Codigo_Cliente > 0,         
		ISNULL(CLIE.Razon_Social,'') + ' ' + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,''),
		ISNULL(REMI.Razon_Social,'') + ' ' + ISNULL(REMI.Nombre,'') + ' ' + ISNULL(REMI.Apellido1,'') + ' ' + ISNULL(REMI.Apellido2,''))        
		) AS NombreRemitente, 
		/*Fin Modificación*/ 
		ISNULL(ENRE.TERC_Codigo_Destinatario, 0) As TERC_Codigo_Destinatario,    
		/*Modificación FP 2021/12/12*/ 
		ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario,
		/*Fin Modificación*/ 
		ENRE.CATA_FOPR_Codigo,            
		FOPR.Campo1 AS FormaPago,            
		ENRE.Total_Flete_Cliente AS Total_Flete_Cliente,            
		ENPD.Numero_Documento AS NumeroDocumentoPlanilla,            
		ELGU.Numero_Documento AS ELGU_NumeroDocumento,
		ISNULL(COND.Codigo, 0) AS TERC_Codigo_Responsable,
		ISNULL(COND.Razon_Social,'') + ' ' + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As TERC_Nombre_Responsable
            
		FROM Remesas_Paqueteria AS ENRP                       
                                                                      
		INNER JOIN Encabezado_Remesas AS ENRE                                                                      
		ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                                      
		AND ENRP.ENRE_Numero = ENRE.Numero             
            
		LEFT JOIN Terceros AS CLIE                                                    
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                      
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                      
                                                                      
		LEFT JOIN Terceros AS REMI                                                                      
		ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                      
		AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                      
                                                                      
		LEFT JOIN Terceros AS DEST                                                                      
		ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                      
		AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                  

		LEFT JOIN Terceros AS COND
		ON ENRP.EMPR_Codigo = COND.EMPR_Codigo
		AND ENRP.TERC_Codigo_Ultimo_Conductor = COND.Codigo
                                                
		LEFT JOIN Valor_Catalogos AS FOPR ON              
		ENRE.EMPR_Codigo = FOPR.EMPR_Codigo              
		AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo              
            
		LEFT JOIN Encabezado_Planilla_Despachos ENPD ON              
		ENPD.EMPR_Codigo = ENRP.EMPR_Codigo              
		AND ENPD.Numero = ENRP.ENPD_Numero_Ultima_Planilla
        
		LEFT JOIN (        
		SELECT DLGU.EMPR_Codigo, DLGU.ENRE_Numero, MAX(ENLG.Numero_Documento) AS Numero_Documento FROM Detalle_Legalizacion_Guias DLGU        
		INNER JOIN Encabezado_Legalizacion_Guias ENLG ON              
		ENLG.EMPR_Codigo = DLGU.EMPR_Codigo              
		AND ENLG.Numero = DLGU.ELGU_Numero        
		WHERE DLGU.EMPR_Codigo = @par_EMPR_Codigo        
		AND (ENLG.Numero_Documento = @par_ELGU_Numero OR @par_ELGU_Numero IS NULL)        
		GROUP BY DLGU.EMPR_Codigo, DLGU.ENRE_Numero        
		) AS ELGU ON        
		ENRE.EMPR_Codigo = ELGU.EMPR_Codigo              
		AND ENRE.Numero = ELGU.ENRE_Numero        
        
		WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo              
		AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)              
		AND ENRE.Numero_Documento = ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                  
		AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                  
		AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)              
		AND (ENPD.Numero_Documento = @par_ENPD_Numero OR @par_ENPD_Numero IS NULL)            
		AND (ELGU.Numero_Documento = @par_ELGU_Numero OR @par_ELGU_Numero IS NULL)            
		AND (ENRP.TERC_Codigo_Ultimo_Conductor = @par_COND_Codigo OR @par_COND_Codigo IS NULL)  
		AND ISNULL(ENRE.ELRG_Numero, 0) = 0  
		AND ((ENRP.Guia_Creada_Ruta_Conductor = 1 AND ENRE.CATA_FOPR_Codigo = 4902) OR --Creada desde GESPHONE, Contado  
		(ENRP.CATA_ESRP_Codigo = 6030 AND ENRE.CATA_FOPR_Codigo  = 4903)) -- Cumplida, Forma pago Contra entrega)  
		AND ENRE.Anulado = 0  
		AND ENRE.Estado = 1  
		ORDER BY Numero ASC        
	END  
  
	IF @par_OFIC_Actual_Codigo IS NOT NULL  
	BEGIN  
		SELECT    
		ENRE.EMPR_Codigo,        
		ENRE.Numero,            
		ENRE.Numero_Documento,            
		IIF(ENRE.TERC_Codigo_Cliente > 0, ENRE.TERC_Codigo_Cliente, ENRE.TERC_Codigo_Remitente) AS TERC_Codigo_Remitente,       
		/*Modificación FP 2021/12/12*/ 
		ISNULL(ENRE.Nombre_Remitente, IIF(ENRE.TERC_Codigo_Cliente > 0,         
		ISNULL(CLIE.Razon_Social,'') + ' ' + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,''),
		ISNULL(REMI.Razon_Social,'') + ' ' + ISNULL(REMI.Nombre,'') + ' ' + ISNULL(REMI.Apellido1,'') + ' ' + ISNULL(REMI.Apellido2,''))        
		) AS NombreRemitente, 
		/*Fin Modificación*/  
		ISNULL(ENRE.TERC_Codigo_Destinatario, 0) As TERC_Codigo_Destinatario,   
		/*Modificación FP 2021/12/12*/ 
		ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario,
		/*Fin Modificación*/ 
		ENRE.CATA_FOPR_Codigo,            
		FOPR.Campo1 AS FormaPago,            
		ENRE.Total_Flete_Cliente AS Total_Flete_Cliente,
		ENPD.Numero AS ENPD_Numero,           
		ENPD.Numero_Documento AS NumeroDocumentoPlanilla,            
		ELGU.Numero_Documento AS ELGU_NumeroDocumento,
		ISNULL(AFOR.Codigo, 0) AS TERC_Codigo_Responsable,
		ISNULL(AFOR.Razon_Social,'') + ' ' + ISNULL(AFOR.Nombre,'') + ' ' + ISNULL(AFOR.Apellido1,'') + ' ' + ISNULL(AFOR.Apellido2,'') As TERC_Nombre_Responsable            
            
		FROM Remesas_Paqueteria AS ENRP                       
                                                                      
		INNER JOIN Encabezado_Remesas AS ENRE                                                                      
		ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                                      
		AND ENRP.ENRE_Numero = ENRE.Numero             
            
		LEFT JOIN Terceros AS CLIE                                                    
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                      
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo         
                                                                      
		LEFT JOIN Terceros AS REMI                                                                      
		ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                      
		AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                      
                                                                      
		LEFT JOIN Terceros AS DEST                                                                      
		ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                      
		AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo
                                        
		LEFT JOIN Valor_Catalogos AS FOPR ON              
		ENRE.EMPR_Codigo = FOPR.EMPR_Codigo              
		AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo              
            
		LEFT JOIN Encabezado_Planilla_Despachos ENPD ON              
		ENPD.EMPR_Codigo = ENRP.EMPR_Codigo              
		AND ENPD.Numero = ENRP.ENPD_Numero_Ultima_Planilla

		LEFT JOIN (
			SELECT EMPR_Codigo, ENPD_Numero, 
			TERC_Codigo_Funcionario AS Codigo
			FROM Detalle_Auxiliares_Planilla_Despachos
			WHERE EMPR_Codigo = @par_EMPR_Codigo
			AND Aforador = 1
		) AS DAPD
		ON ENPD.EMPR_Codigo = DAPD.EMPR_Codigo
		AND ENPD.Numero = DAPD.ENPD_Numero

		LEFT JOIN Terceros AS AFOR                                                                      
		ON DAPD.EMPR_Codigo = AFOR.EMPR_Codigo                                                                      
		AND DAPD.Codigo = AFOR.Codigo

		LEFT JOIN (        
			SELECT DLGU.EMPR_Codigo, DLGU.ENRE_Numero, MAX(ENLG.Numero_Documento) AS Numero_Documento FROM Detalle_Legalizacion_Guias DLGU        
			INNER JOIN Encabezado_Legalizacion_Guias ENLG ON              
			ENLG.EMPR_Codigo = DLGU.EMPR_Codigo              
			AND ENLG.Numero = DLGU.ELGU_Numero        
			WHERE DLGU.EMPR_Codigo = @par_EMPR_Codigo        
			AND (ENLG.Numero_Documento = @par_ELGU_Numero OR @par_ELGU_Numero IS NULL)        
			GROUP BY DLGU.EMPR_Codigo, DLGU.ENRE_Numero        
		) AS ELGU ON        
		ENRE.EMPR_Codigo = ELGU.EMPR_Codigo              
		AND ENRE.Numero = ELGU.ENRE_Numero        
        
		WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo              
		AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)              
		AND ENRE.Numero_Documento = ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                                  
		AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                  
		AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)              
		AND (ENPD.Numero_Documento = @par_ENPD_Numero OR @par_ENPD_Numero IS NULL)            
		AND (ELGU.Numero_Documento = @par_ELGU_Numero OR @par_ELGU_Numero IS NULL)  
		AND ISNULL(ENRE.ELRG_Numero, 0) = 0  
		AND ((ENRP.OFIC_Codigo_Origen = @par_OFIC_Actual_Codigo AND ENRE.CATA_FOPR_Codigo = 4902) OR --Guias contado realizadas por oficina  
		(ENRP.OFIC_Codigo_Entrega = @par_OFIC_Actual_Codigo AND ENRE.CATA_FOPR_Codigo = 4903 AND ENRP.CATA_ESRP_Codigo = 6030))--Guias Entregadas por oficina con forma pago contra entrega  
		AND ENRE.Anulado = 0                                                                    
		AND ENRE.Estado = 1              
		ORDER BY Numero ASC      
	END    
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_pendientes_planillar_paqueteria]
(                                                                
	@par_EMPR_Codigo SMALLINT,                            
	@par_NumeroInicial numeric = NULL,        
	@par_FechaInicial Date = NULL,                            
	@par_FechaFinal Date = NULL,                            
	@par_Documento_Cliente varchar(30) = NULL,                                                                
	@par_NombreCliente varchar (150) = NULL,                                                                
	@par_TERC_Codigo_Cliente numeric = NULL,                            
	@par_Ciudad_Origen varchar (50) = NULL,                                                                
	@par_Ciudad_Destino varchar (50) = NULL,                            
	@par_NombreDestinatario varchar (150) = NULL,                            
	@par_NombreRemitente varchar(150) = NULL,                            
	@par_ENPD_Numero numeric = NULL,                            
	@par_Anulado numeric= NULL,                            
	@par_Estado numeric = NULL,                            
	@par_TIDO_Codigo INT = NULL,                            
	@par_NumeroPagina INT = NULL,                                                    
	@par_RegistrosPagina INT = NULL,                                                    
	@par_OFIC_Codigo_Actual_Usuario INT = NULL,                            
	@par_Ciudad_Origen_Codigo INT = NULL,                    
	@par_Ciudad_Destino_Codigo INT = NULL,                    
	@par_Reexpedicion INT = NULL,        
	@par_NumeroDocumentoPlanillaCargue numeric = NULL,    
	@par_LineaNegocioPaqueteria numeric = NULL,
	@par_ZOCI_Codigo NUMERIC = NULL
)                                                                
AS                                                                         
BEGIN                                                                
	IF @par_FechaInicial <> NULL BEGIN                                                                
	SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                                                
	END                                                                
                                                                
	IF @par_FechaFinal <> NULL BEGIN                                                                
	SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                                                
	END                                                                
                                                                
	SET NOCOUNT ON;                                                                      
	DECLARE @CantidadRegistros INT                                                                
	SELECT                                                                
	@CantidadRegistros = (SELECT DISTINCT                                             
		COUNT(1)                                                                
		FROM Remesas_Paqueteria ENRP                                                                
                                                                
		INNER JOIN Encabezado_Remesas AS ENRE                                                          
		ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                                
		AND ENRP.ENRE_Numero = ENRE.Numero                                                                
                                                                
		INNER JOIN Oficinas AS OFIC ON                                                  
		ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                                  
		AND ENRE.OFIC_Codigo = OFIC.Codigo                                                  
                                                
		LEFT JOIN Terceros AS CLIE                                         
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo     
                                                        
		LEFT JOIN Terceros AS REMI            
		ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                
		AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                
                               
		LEFT JOIN Terceros AS DEST                                                                
		ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                
		AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                                
                
		LEFT JOIN Producto_Transportados AS PRTR                                                                
		ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                                                
		AND ENRE.PRTR_Codigo = PRTR.Codigo                                   
                                                                
		LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                        
		ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                      
		AND ENRE.ENPD_Numero = ENPD.Numero                                                
		AND ENPD.TIDO_Codigo In(130, 135) --Planilla Despacho Guias, Planilla Paqueteria      
                                                            
		LEFT JOIN Ciudades CIOR                                                       
		ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                
		AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                
                                                                
		LEFT JOIN Ciudades CIDE                                                                
		ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                                
		AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                          
                                             
		LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                          
		ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                          
		AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                              
                                                 
		INNER JOIN Oficinas AS OFAC ON                                                  
		ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                                  
		AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo        
        
		LEFT JOIN Valor_Catalogos AS FOPR ON                                        
		ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                        
		AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo      
      
		LEFT JOIN(      
		SELECT DPCD.EMPR_Codigo, DPCD.ENRE_Numero, MAX(ENPC.Numero_Documento) AS Numero_Documento, MAX(ENPC.TIDO_Codigo) AS TIDO_Codigo      
		FROM Detalle_Planilla_Cargue_Descargue DPCD      
		INNER JOIN Encabezado_Planilla_Cargue_Descargue ENPC ON        
		DPCD.EMPR_Codigo = ENPC.EMPR_Codigo        
		AND DPCD.EPCD_Numero = ENPC.Numero      
		GROUP BY DPCD.EMPR_Codigo, DPCD.ENRE_Numero      
		) AS EPCD ON      
		EPCD.EMPR_Codigo = ENRE.EMPR_Codigo      
		AND EPCD.ENRE_Numero = ENRE.Numero        
                                                                
		WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                            
		AND ENRE.Numero_Documento = ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                            
		AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                            
		AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                            
		AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                                
		AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                                 
		OR (@par_NombreCliente IS NULL))                                                                
		AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                            
		AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                            
		AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'  
  		/*Modificación FP 2021/12/12*/
	  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')
	  OR (@par_NombreRemitente IS NULL))                                                          
	  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')
	  OR (@par_NombreDestinatario IS NULL))
  		/*Fin Modificación*/                          
		AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                                    
		AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                                                                 
		AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                            
		AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                            
		AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                       
		AND (ENRP.CIUD_Codigo_Origen = @par_Ciudad_Origen_Codigo OR @par_Ciudad_Origen_Codigo IS NULL)                           
		AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)
		AND (ENRP.ZOCI_Codigo_Entrega = @par_ZOCI_Codigo OR @par_ZOCI_Codigo IS NULL)
		AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)            
		AND ((EPCD.Numero_Documento = @par_NumeroDocumentoPlanillaCargue AND EPCD.TIDO_Codigo = 360) OR @par_NumeroDocumentoPlanillaCargue IS NULL)        
		AND (ENRP.CATA_LNPA_Codigo = @par_LineaNegocioPaqueteria OR @par_LineaNegocioPaqueteria IS NULL)    
	);                                                
	WITH Pagina                  
	AS                                                                
	(                              
		SELECT DISTINCT                  
		ENRE.EMPR_Codigo                                                                
		,ENRE.Numero                                                                
		,ENRE.Numero_Documento                                                                
		,ENRE.Numeracion                  
		,ENRE.Fecha                  
		,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                                                
		,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                                                
		,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                                            
		,ENRE.TERC_Codigo_Cliente                  
		,ENRE.TERC_Codigo_Remitente 
		,ENRE.Direccion_Remitente
		,ENRE.Direccion_Destinatario                                                         
		,ENRE.CATA_FOPR_Codigo                                     
		,FOPR.Campo1 AS FormaPago                                                                
		,ENRE.RUTA_Codigo                                                                
		,ENRP.CIUD_Codigo_Origen                     
		,ENRP.CIUD_Codigo_Destino
		,CIOR.Nombre AS CiudadOrigen             
		,CIDE.Nombre AS CiudadDestino                                          
		,ENRP.ZOCI_Codigo_Entrega
		,ZOCI.Nombre AS Zona
		,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue                                          
		,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue                                                 
		,ISNULL(ENRP.OFIC_Codigo_Origen, 0) AS OFIC_Codigo_Origen                                                
		,ISNULL(OFIC.Nombre, '') AS NombreOficinaOrigen                                                   
		,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                                                   
		,ENRP.CATA_ESRP_Codigo                                              
		,ENRE.Observaciones                  
		--,(ENRE.Valor_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS  Valor_Flete_Cliente                        
		,(ENRE.Valor_Flete_Cliente) AS  Valor_Flete_Cliente                          
		--,(ENRE.Total_Flete_Cliente - ENRE.Valor_Seguro_Cliente - ENRE.Valor_Comision_Oficina) AS Total_Flete_Cliente              
		,ENRE.Total_Flete_Cliente AS Total_Flete_Cliente              
		,ENRE.Valor_Seguro_Cliente                                            
		,ENRE.Cantidad_Cliente                                                           
		,ENRP.Peso_A_Cobrar AS  Peso_Cliente                                                               
		,ENRE.Estado                                                                
		,ENRE.Anulado                                                                
		,PRTR.Nombre AS NombreProducto                                                                
		,ENRE.Documento_Cliente                                                   
		,ENRE.OFIC_Codigo                                                  
		,OFIC.Nombre As NombreOficina                                                               
		,'' AS NombreRuta                                                  
		,ENRE.Valor_Flete_Transportador                                     
		,ENRE.Valor_Reexpedicion                              
		,ENRP.Reexpedicion  
		/*Modificación FP 2021/12/12*/
		,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente
		,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario
		/*Fin Modificación*/  
		,ENRP.OFIC_Codigo_Actual                
		,ROW_NUMBER() OVER (ORDER BY ENRE.Numero ) AS RowNumber                                                                
		FROM Remesas_Paqueteria AS ENRP                 
                                                                
		INNER JOIN Encabezado_Remesas AS ENRE                                                                
		ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                                
		AND ENRP.ENRE_Numero = ENRE.Numero                                                                
                                                            
		INNER JOIN Oficinas AS OFIC ON                                                  
		ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                                  
		AND ENRE.OFIC_Codigo = OFIC.Codigo                                
                                                
		LEFT JOIN Terceros AS CLIE                                              
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                
                                                                
		LEFT JOIN Terceros AS REMI                                                                
		ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                
		AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                  
                                                                
		LEFT JOIN Terceros AS DEST                                                                
		ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                
		AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                            
                                                                
		LEFT JOIN Producto_Transportados AS PRTR                                                                
		ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                  
		AND ENRE.PRTR_Codigo = PRTR.Codigo                                                                
        
		LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                                
		ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                                
		AND ENRE.ENPD_Numero = ENPD.Numero                                                                
		AND ENPD.TIDO_Codigo In(130, 135) --Planilla Despacho Guias, Planilla Paqueteria      
                                                              
		LEFT JOIN Ciudades CIOR                                                                
		ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                     
		AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                
                                                      
		LEFT JOIN Ciudades CIDE                                                                
		ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                                
		AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                          
                                             
		LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                          
		ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                          
		AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                                        
                                                                
		INNER JOIN Oficinas AS OFAC ON                                                  
		ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                                  
		AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo        
        
		LEFT JOIN Valor_Catalogos AS FOPR ON                                        
		ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                        
		AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo      
      
		LEFT JOIN(      
		SELECT DPCD.EMPR_Codigo, DPCD.ENRE_Numero, MAX(ENPC.Numero_Documento) AS Numero_Documento, MAX(ENPC.TIDO_Codigo) AS TIDO_Codigo      
		FROM Detalle_Planilla_Cargue_Descargue DPCD      
		INNER JOIN Encabezado_Planilla_Cargue_Descargue ENPC ON        
		DPCD.EMPR_Codigo = ENPC.EMPR_Codigo        
		AND DPCD.EPCD_Numero = ENPC.Numero      
		GROUP BY DPCD.EMPR_Codigo, DPCD.ENRE_Numero      
		) AS EPCD ON      
		EPCD.EMPR_Codigo = ENRE.EMPR_Codigo      
		AND EPCD.ENRE_Numero = ENRE.Numero

		LEFT JOIN Zona_Ciudades AS ZOCI ON
		ENRP.EMPR_Codigo = ZOCI.EMPR_Codigo                                        
		AND ENRP.ZOCI_Codigo_Entrega = ZOCI.Codigo
                
		WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                            
		AND ENRE.Numero_Documento = ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                            
		AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                            
		AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                            
		AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                       
		AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                                  
		OR (@par_NombreCliente IS NULL))                                      
		AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                            
		AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                            
		AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%' 
  		/*Modificación FP 2021/12/12*/
	  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')
	  OR (@par_NombreRemitente IS NULL))                                                          
	  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')
	  OR (@par_NombreDestinatario IS NULL))
  		/*Fin Modificación*/                        
		AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                                                
		AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                        
		AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                            
		AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                            
		AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                    
		AND (ENRP.CIUD_Codigo_Origen = @par_Ciudad_Origen_Codigo OR @par_Ciudad_Origen_Codigo IS NULL)                              
		AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)
		AND (ENRP.ZOCI_Codigo_Entrega = @par_ZOCI_Codigo OR @par_ZOCI_Codigo IS NULL)           
		AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)        
		AND ((EPCD.Numero_Documento = @par_NumeroDocumentoPlanillaCargue AND EPCD.TIDO_Codigo = 360) OR @par_NumeroDocumentoPlanillaCargue IS NULL)                        
		AND (ENRP.CATA_LNPA_Codigo = @par_LineaNegocioPaqueteria OR @par_LineaNegocioPaqueteria IS NULL)
	)                                                                
	SELECT                                                                
	0 AS Obtener                            
	,EMPR_Codigo                                                                
	,Numero                                                                
	,Numero_Documento                  
	,Numeracion                                                             
	,Fecha                                                                
	,NumeroDocumentoPlanilla                                                                
	,VEHI_Codigo                                            
	,NombreCliente                                                                
	,TERC_Codigo_Cliente                                                                
	,CATA_FOPR_Codigo                                             
	,FormaPago                                        
	,RUTA_Codigo                                                                
	,CIUD_Codigo_Origen                                                                
	,CIUD_Codigo_Destino                                                               
	,CiudadOrigen                                                                
	,CiudadDestino
	,ZOCI_Codigo_Entrega
	,Zona
	,SICD_Codigo_Descargue                                          
	,SitioDescargue                                               
	,OFIC_Codigo_Origen                                                  
	,NombreOficinaOrigen                                                          
	,Observaciones                                                                
	,Valor_Flete_Cliente                       
	,Total_Flete_Cliente                                                                
	,Valor_Seguro_Cliente                                                    
	,Cantidad_Cliente                                                                
	,Peso_Cliente                                                                
	,Estado                                            
	,Anulado                                    
	,NombreProducto                                                       
	,Documento_Cliente                                                 
	,OFIC_Codigo                                                  
	,NombreOficina                                         
	,NombreRuta                                                                
	,NombreOficinaActual                                              
	,CATA_ESRP_Codigo                                               
	,Valor_Flete_Transportador                                        
	,NombreRemitente             
	,TERC_Codigo_Remitente 
	,Direccion_Remitente 
	,Direccion_Destinatario  
	,Valor_Reexpedicion                              
	,Reexpedicion                                  
	,NombreDestinatario                
	,OFIC_Codigo_Actual                
	,@CantidadRegistros AS TotalRegistros                                                                
	,@par_NumeroPagina AS PaginaObtener                                
	,@par_RegistrosPagina AS RegistrosPagina                                                                
	FROM Pagina                                                                
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                                
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                                
	ORDER BY CiudadDestino, isnull(nullif(Numeracion,''),'z') asc, Numero ASC                  
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_pendientes_recibir_oficina_destino]
(
	@par_EMPR_Codigo SMALLINT,
	@par_TIDO_Codigo NUMERIC,
	@par_NumeroInicial NUMERIC = NULL,                                                  
	@par_NumeroFinal NUMERIC = NULL,                                                  
	@par_FechaInicial DATE = NULL,                                                  
	@par_FechaFinal DATE = NULL,
	@par_ENDP_Numero NUMERIC = NULL,                                                  
	@par_ENPD_Numero_Documento NUMERIC = NULL,
	@par_ENPD_TIDO_Codigo NUMERIC,
	@par_Oficina_Recibe_Codigo NUMERIC = NULL
)
AS
BEGIN
	SELECT DISTINCT
	REPA.EMPR_Codigo                                           
	,ENRE.Numero                                                  
	,ENRE.Numero_Documento                                 
	,ENRE.Fecha
	,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                      
	,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                                  
	,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                                  
	/*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente
	/*Fin Modificación*/  
	,ENRE.TERC_Codigo_Cliente          
	,ENRE.TERC_Codigo_Remitente                                          
	,ENRE.CATA_FOPR_Codigo      
	,FOPA.Campo1 AS FormaPago                                                
	,ENRE.RUTA_Codigo                                                  
	,ENRE.CIUD_Codigo_Remitente CIUD_Codigo_Origen                                              
	,ENRE.CIUD_Codigo_Destinatario CIUD_Codigo_Destino                                            
	,CIOR.Nombre AS CiudadOrigen                                           
	,CIDE.Nombre AS CiudadDestino
	,ENRE.Observaciones                                                  
	,ENRE.Valor_Flete_Cliente                                                  
	,ENRE.Total_Flete_Cliente                               
	,ENRE.Valor_Seguro_Cliente           
	,ENRE.Cantidad_Cliente                                                  
	,ENRE.Peso_Cliente                                                  
	,ENRE.Estado                                                  
	,PRTR.Nombre AS NombreProducto                                                  
	,ENRE.Documento_Cliente
	,REPA.Recoger_Oficina_Destino
	,REPA.OFIC_Codigo_Recibe

	FROM Remesas_Paqueteria REPA

	INNER JOIN Encabezado_Remesas ENRE ON
	REPA.EMPR_Codigo = ENRE.EMPR_Codigo
	AND REPA.ENRE_Numero = ENRE.Numero

	LEFT JOIN Terceros AS CLIE                                                  
	ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                  
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                  
                                                  
	LEFT JOIN Terceros AS REMI                                                  
	ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                  
	AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                  
                                                  
	LEFT JOIN Terceros AS DEST                                                  
	ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                  
	AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                  

	LEFT JOIN Producto_Transportados AS PRTR
	ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
	AND ENRE.PRTR_Codigo = PRTR.Codigo
                                                  
	INNER JOIN Encabezado_Planilla_Despachos AS ENPD
	ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo
	AND ENRE.ENPD_Numero = ENPD.Numero
	AND ENPD.TIDO_Codigo = @par_ENPD_TIDO_Codigo

	LEFT JOIN Ciudades CIOR                                                  
	ON REPA.EMPR_Codigo = CIOR.EMPR_Codigo                                                  
	AND REPA.CIUD_Codigo_Origen = CIOR.Codigo                                                  
                                                  
	LEFT JOIN Ciudades CIDE                                                  
	ON REPA.EMPR_Codigo = CIDE.EMPR_Codigo                                                  
	AND REPA.CIUD_Codigo_Destino  = CIDE.Codigo          
             
	LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON     
	REPA.EMPR_Codigo = SICD_DESC.EMPR_Codigo          
	AND REPA.SICD_Codigo_Descargue = SICD_DESC.Codigo 

	LEFT JOIN Valor_Catalogos FOPA ON    
	ENRE.EMPR_Codigo = FOPA.EMPR_Codigo AND    
	ENRE.CATA_FOPR_Codigo = FOPA.Codigo       

	WHERE REPA.EMPR_Codigo = @par_EMPR_Codigo
	AND ENRE.TIDO_Codigo = @par_TIDO_Codigo
	AND (ENRE.Numero_Documento >= @par_NumeroInicial OR @par_NumeroInicial IS NULL)
	AND (ENRE.Numero_Documento <= @par_NumeroFinal OR @par_NumeroFinal IS NULL)
	AND (ENRE.Fecha >= @par_FechaInicial OR @par_FechaInicial IS NULL)
	AND (ENRE.Fecha <= @par_FechaFinal OR @par_FechaFinal IS NULL)
	AND (ENRE.ENPD_Numero = @par_ENDP_Numero OR @par_ENDP_Numero IS NULL)
	AND (ENPD.Numero_Documento = @par_ENPD_Numero_Documento OR @par_ENPD_Numero_Documento IS NULL)
	AND (ENPD.TIDO_Codigo = @par_ENPD_TIDO_Codigo OR @par_ENPD_TIDO_Codigo IS NULL)
	AND (REPA.OFIC_Codigo_Recibe = @par_Oficina_Recibe_Codigo OR @par_Oficina_Recibe_Codigo IS NULL)
	AND (REPA.OFIC_Codigo_Actual <> REPA.OFIC_Codigo_Recibe)
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_planillas_despachos]        
(                                                        
 @par_EMPR_Codigo SMALLINT,                                                        
 @par_OFIC_Codigo INT = NULL,                                                        
 @par_Numero NUMERIC = NULL,                                                        
 @par_Fecha_Inicial DATETIME = NULL,                                                        
 @par_Fecha_Final DATETIME = NULL,                                                        
 @par_CLIE_Codigo NUMERIC = NULL,        
 @par_ESOS_Numero_Documento NUMERIC = NULL,                                                       
 @par_CIUD_Codigo_Destino NUMERIC = NULL,                                                      
 @par_CIUD_Codigo_origen NUMERIC = NULL,                                                      
 @par_ENPD_Numero NUMERIC = NULL,                                                        
 @par_Estado SMALLINT = NULL,                                                        
 @par_Anulado SMALLINT = NULL,  
 @par_TIDO_Codigo SMALLINT = NULL,                                                        
 @par_NumeroPagina INT = NULL,                                                        
 @par_RegistrosPagina INT = NULL      
  
)                                                        
AS                                                        
BEGIN                                                        
 SET NOCOUNT ON;                                                         
 DECLARE @CantidadRegistros INT                                                        
 SELECT @CantidadRegistros =                                                        
 (                                                        
  SELECT DISTINCT                                                        
  COUNT(1)                                                        
  FROM                                                        
  Encabezado_Remesas ENRE                       
                         
  LEFT JOIN Tipo_Documentos TIDO                                                        
  ON ENRE.EMPR_Codigo = TIDO.EMPR_Codigo                                   
  AND ENRE.TIDO_Codigo = TIDO.Codigo                                                           
                                                           
  LEFT JOIN Rutas RUTA                    
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                                   
  AND ENRE.RUTA_Codigo = RUTA.Codigo                                                        
                           
  LEFT JOIN Producto_Transportados PRTR         
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                   
  AND ENRE.PRTR_Codigo = PRTR.Codigo                                                        
                                                        
  LEFT JOIN Terceros CLIE                                                        
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                   
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                        
                                                        
  LEFT JOIN Terceros REMI                                                        
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                   
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                        
                                                        
  LEFT JOIN Terceros COND                                           
  ON ENRE.EMPR_Codigo = COND.EMPR_Codigo                                   
  AND ENRE.TERC_Codigo_Conductor = COND.Codigo                                     
                                  
  LEFT JOIN Ciudades CIUD                                                        
  ON ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                                   
  AND ENRE.CIUD_Codigo_Remitente = CIUD.Codigo                            
                                                        
  LEFT JOIN Ciudades CIDE                                                        
  ON ENRE.EMPR_Codigo = CIDE.EMPR_Codigo                                   
  AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo                                                        
                                                        
  LEFT JOIN Oficinas OFIC                                                        
  ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                   
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                                        
                                                
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS                                                        
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                                   
  AND ENRE.ESOS_Numero = ESOS.Numero                                                        
                           
  LEFT JOIN Detalle_Despacho_Orden_Servicios AS DDOS                       
  ON ENRE.EMPR_Codigo = DDOS.EMPR_Codigo                        
  AND ENRE.Numero = DDOS.ENRE_Numero                        
                        
  LEFT JOIN Linea_Negocio_Transporte_Carga AS LNTC                      
  ON DDOS.EMPR_Codigo = LNTC.EMPR_Codigo                        
  AND DDOS.LNTC_Codigo = LNTC.Codigo                        
                       
  LEFT JOIN Tercero_Direcciones As TEDI                     
  ON ENRE.EMPR_Codigo = TEDI.EMPR_Codigo                    
  AND ENRE.TEDI_Codigo = TEDI.Codigo     
      
  WHERE                                
  ENRE.EMPR_Codigo = @par_EMPR_Codigo                                                        
  AND ENRE.TIDO_Codigo = 100                                               
  AND ENRE.Anulado = ISNULL (@par_Anulado,ENRE.Anulado)        
  AND ENRE.Numero_Documento = ISNULL(@par_Numero, ENRE.Numero_Documento)                                           
  AND ENRE.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ENRE.Fecha) AND ISNULL(@par_Fecha_Final, ENRE.Fecha)                                    
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                                   
  AND ENRE.Estado = ISNULL(@par_Estado,ENRE.Estado)                                                        
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                             
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_CLIE_Codigo, ENRE.TERC_Codigo_Cliente)                                       
  AND (ESOS.Numero_Documento = @par_ESOS_Numero_Documento OR @par_ESOS_Numero_Documento IS NULL)                                       
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                                    
  AND ENRE.CIUD_Codigo_Destinatario = ISNULL(@par_CIUD_Codigo_Destino, ENRE.CIUD_Codigo_Destinatario)                                       
  AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_CIUD_Codigo_origen, ENRE.CIUD_Codigo_Remitente)                                                                   
      
 );                                                        
 WITH Pagina                                                 
 AS                                                        
 (                
  SELECT                                           
  ENRE.EMPR_Codigo,                                                        
  ENRE.Numero,                                                        
  ENRE.TIDO_Codigo,                                                        
  TIDO.Nombre AS TipoDocumento,                                                  
  ENRE.Numero_Documento,                                                        
  ENRE.CATA_TIRE_Codigo,                                                        
  ISNULL(ENRE.Documento_Cliente,'') AS Documento_Cliente,                                                        
  ISNULL(ENRE.Fecha_Documento_Cliente,'') AS Fecha_Documento_Cliente,                                                        
  ENRE.Fecha,                                                        
  ENRE.RUTA_Codigo,                                                        
  RUTA.Nombre AS NombreRuta,                                                        
  ENRE.PRTR_Codigo,                                                     
  PRTR.Nombre AS NombreProducto,                                                        
  ENRE.CATA_FOPR_Codigo,                                                        
  ENRE.TERC_Codigo_Cliente,                                                        
  CONCAT(ISNULL(CLIE.Razon_Social,''),CLIE.Nombre,' ',CLIE.Apellido1,' ',CLIE.Apellido2) AS NombreCliente,                                                        
  CLIE.Numero_Identificacion AS IdentificacionCliente,                                                        
  ENRE.TERC_Codigo_Remitente,                                                        
  CONCAT(ISNULL(REMI.Razon_Social,''),REMI.Nombre,' ',REMI.Apellido1,' ',REMI.Apellido2) AS Remitente,
	/*Modificación FP 2021/12/12*/
	ISNULL(ENRE.Nombre_Remitente, CONCAT(ISNULL(REMI.Razon_Social,''),REMI.Nombre,' ',REMI.Apellido1,' ',REMI.Apellido2)) AS NombreRemitente,
	/*Fin Modificación*/  
  REMI.Numero_Identificacion AS IdentificacionRemitente,                                                        
  ENRE.CIUD_Codigo_Remitente,                                                        
  CIUD.Nombre AS CiudadRemitente,                                                        
  ENRE.Direccion_Remitente,                                                        
  ENRE.Telefonos_Remitente,                                                        
  ENRE.Observaciones,                                                        
  ENRE.Cantidad_Cliente,                                                        
  ENRE.Peso_Cliente,                                    
  ENRE.Peso_Volumetrico_Cliente,                                                        
  0 as DTCV_Codigo,                                                  
  0 AS TarifarioVenta,                                                        
  ENRE.Valor_Flete_Cliente,                                            
  ENRE.Valor_Manejo_Cliente,                                                        
  ENRE.Valor_Seguro_Cliente,                                                        
  ENRE.Valor_Descuento_Cliente,                                                        
  ENRE.Total_Flete_Cliente,                                                        
  ENRE.Valor_Comercial_Cliente,                                                        
  ENRE.Cantidad_Transportador,                                                        
  ENRE.Peso_Transportador,                                                        
  ENRE.Valor_Flete_Transportador,                                                        
  ENRE.Total_Flete_Transportador,                                                        
  ENRE.TERC_Codigo_Destinatario,                                                        
  ENRE.CIUD_Codigo_Destinatario,                                                        
  CIDE.Nombre AS CiudadDestinatario,                                                        
  ENRE.Direccion_Destinatario,                               
  ENRE.Telefonos_Destinatario,                                                        
  ENRE.Anulado,                                                        
  ENRE.Estado,                                                        
  ENRE.Fecha_Crea,                                                        
  ENRE.USUA_Codigo_Crea,                                                        
  ENRE.Fecha_Modifica,                                                        
  ENRE.USUA_Codigo_Modifica,                          
  ENRE.Fecha_Anula,                                                        
  ENRE.USUA_Codigo_Anula,                                                        
  ENRE.Causa_Anula,                                                        
  ENRE.OFIC_Codigo,                        
  OFIC.Nombre AS NombreOficina,                                                        
  ENRE.ETCC_Numero,                                                        
  ENRE.ETCV_Numero,                                                        
  ENRE.ESOS_Numero,                                                        
  ESOS.Numero_Documento AS OrdenServicio,                                              
  ENRE.ENPD_Numero AS NumeroPlanilla,                                                        
  ENRE.ENMC_Numero AS NumeroManifiesto,                                                        
  ISNULL(ENRE.ECPD_Numero,0) AS ECPD_Numero,                                                        
  ISNULL(ENRE.ENFA_Numero,0) AS ENFA_Numero,                                                        
  ENRE.VEHI_Codigo,                                                        
  ENRE.TERC_Codigo_Conductor,                                                        
  CONCAT(COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2) AS NombreConductor,                                                        
  COND.Numero_Identificacion AS IdentificacionConductor,                                                        
  COND.Telefonos AS TelefonoConductor,                                                        
  ENRE.SEMI_Codigo,                                                  
  ISNULL(PRTR.UEPT_Codigo, 0) AS Unidad_Empaque,                                             
  ISNULL(ENRE.Distribucion,0) AS Distribucion,                                    
  DDOS.LNTC_Codigo,                        
  LNTC.Nombre as LineaNegocio,                        
  TEDI.Nombre As NombreSede,          
    
  /* Modificación: AE 27/05/2020*/    
  ENRE.Numero_Contenedor,    
 ESOS.Numero AS CodigoOrdenServicio,    
 /*Fin Modificación*/    
                    
  ROW_NUMBER() OVER (ORDER BY ENRE.Numero DESC) AS RowNumber                                                        
 FROM                                                        
  Encabezado_Remesas ENRE                       
                         
  LEFT JOIN Tipo_Documentos TIDO                                                        
  ON ENRE.EMPR_Codigo = TIDO.EMPR_Codigo                                   
  AND ENRE.TIDO_Codigo = TIDO.Codigo                                                           
                                                           
  LEFT JOIN Rutas RUTA                    
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                                   
  AND ENRE.RUTA_Codigo = RUTA.Codigo                                                        
                           
  LEFT JOIN Producto_Transportados PRTR         
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                   
  AND ENRE.PRTR_Codigo = PRTR.Codigo                                                        
                                                        
  LEFT JOIN Terceros CLIE                                                        
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                   
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                        
                                                        
  LEFT JOIN Terceros REMI                                                        
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                   
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                        
                                                        
  LEFT JOIN Terceros COND                                           
  ON ENRE.EMPR_Codigo = COND.EMPR_Codigo                                   
  AND ENRE.TERC_Codigo_Conductor = COND.Codigo                                     
                                  
  LEFT JOIN Ciudades CIUD                                                        
  ON ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                                   
  AND ENRE.CIUD_Codigo_Remitente = CIUD.Codigo                                                        
                                                        
  LEFT JOIN Ciudades CIDE                                                        
  ON ENRE.EMPR_Codigo = CIDE.EMPR_Codigo                                   
  AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo                                                        
                                                        
  LEFT JOIN Oficinas OFIC                                                        
  ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                   
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                                        
                                                
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS                                                        
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                                   
  AND ENRE.ESOS_Numero = ESOS.Numero                                                        
                           
  LEFT JOIN Detalle_Despacho_Orden_Servicios AS DDOS                       
  ON ENRE.EMPR_Codigo = DDOS.EMPR_Codigo                        
  AND ENRE.Numero = DDOS.ENRE_Numero                        
                        
  LEFT JOIN Linea_Negocio_Transporte_Carga AS LNTC                      
  ON DDOS.EMPR_Codigo = LNTC.EMPR_Codigo                        
  AND DDOS.LNTC_Codigo = LNTC.Codigo                        
                       
  LEFT JOIN Tercero_Direcciones As TEDI                     
  ON ENRE.EMPR_Codigo = TEDI.EMPR_Codigo                    
  AND ENRE.TEDI_Codigo = TEDI.Codigo     
      
  WHERE                                
  ENRE.EMPR_Codigo = @par_EMPR_Codigo                                                        
  AND ENRE.TIDO_Codigo = 100                                               
  AND ENRE.Anulado = ISNULL (@par_Anulado,ENRE.Anulado)        
  AND ENRE.Numero_Documento = ISNULL(@par_Numero, ENRE.Numero_Documento)                                           
  AND ENRE.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ENRE.Fecha) AND ISNULL(@par_Fecha_Final, ENRE.Fecha)                                    
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                                   
  AND ENRE.Estado = ISNULL(@par_Estado,ENRE.Estado)                                                        
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                             
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_CLIE_Codigo, ENRE.TERC_Codigo_Cliente)                                       
  AND (ESOS.Numero_Documento = @par_ESOS_Numero_Documento OR @par_ESOS_Numero_Documento IS NULL)                                       
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                                    
  AND ENRE.CIUD_Codigo_Destinatario = ISNULL(@par_CIUD_Codigo_Destino, ENRE.CIUD_Codigo_Destinatario)                                       
  AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_CIUD_Codigo_origen, ENRE.CIUD_Codigo_Remitente)                                                                   
    
        
 )                                                        
 SELECT DISTINCT                                        
 0 AS Obtener,                                                        
 EMPR_Codigo,                                                        
 Numero,                                                        
 TIDO_Codigo,                                                        
 TipoDocumento,                                              
 Numero_Documento,                                                        
 CATA_TIRE_Codigo,                                                        
 Documento_Cliente,                                                        
 Fecha_Documento_Cliente,                                                        
 Fecha,            
 RUTA_Codigo,                                                
 NombreRuta,                              
 PRTR_Codigo,                                
 NombreProducto,                                    
 CATA_FOPR_Codigo,                                                        
 TERC_Codigo_Cliente,                                                        
 NombreCliente,                                                        
 IdentificacionCliente,                                                        
 TERC_Codigo_Remitente,                                                        
 Remitente,     
 NombreRemitente,                                                   
 IdentificacionRemitente,                                                 
 CIUD_Codigo_Remitente,                                                        
 CiudadRemitente,                                                        
 Direccion_Remitente,                                                        
 Telefonos_Remitente,                                                        
 Observaciones,                                                        
 Cantidad_Cliente,                                                        
 Peso_Cliente,                             
 Peso_Volumetrico_Cliente,                                                        
 DTCV_Codigo,                                                        
 TarifarioVenta,                                                        
 Valor_Flete_Cliente,                                                        
 Valor_Manejo_Cliente,                                                        
 Valor_Seguro_Cliente,                                                        
 Valor_Descuento_Cliente,                                                        
 Total_Flete_Cliente,                                                        
 Valor_Comercial_Cliente,                                                        
 Cantidad_Transportador,                          
 Peso_Transportador,                                                        
 Valor_Flete_Transportador,                                                        
 Total_Flete_Transportador,                                                        
 TERC_Codigo_Destinatario,                                                        
 CIUD_Codigo_Destinatario,                                                        
 CiudadDestinatario,                                             
 Direccion_Destinatario,                                                        
 Telefonos_Destinatario,                                                        
 Anulado,                                                        
 Estado,                                                        
 Fecha_Crea,                                                        
 USUA_Codigo_Crea,                                                        
 Fecha_Modifica,                                                        
 USUA_Codigo_Modifica,                                                        
 Fecha_Anula,                                                        
 USUA_Codigo_Anula,                                                        
 Causa_Anula,                                                        
 OFIC_Codigo,                                                        
 NombreOficina,                                                        
 ETCC_Numero,                                                        
 ETCV_Numero,                                                        
 ESOS_Numero,                                                        
 OrdenServicio,                                                        
 NumeroPlanilla,                            
 NumeroManifiesto,                                                        
 ECPD_Numero,                                           
 ENFA_Numero,                                                        
 VEHI_Codigo,             
 SEMI_Codigo,                                              
 TERC_Codigo_Conductor,                                          
 NombreConductor,                                                        
 IdentificacionConductor,                                                        
 TelefonoConductor,                                                        
 Unidad_Empaque,                                             
 Distribucion,                                                 
 LNTC_Codigo,                        
 LineaNegocio,                       
 NombreSede,                     
 @CantidadRegistros AS TotalRegistros,                                                        
 @par_NumeroPagina AS PaginaObtener,                                             
 @par_RegistrosPagina AS RegistrosPagina,      
 '(NO APLICA)' As Naviera             ,    
 Numero_Contenedor    ,    
 CodigoOrdenServicio        
 FROM Pagina                           
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                        
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                        
 ORDER BY Numero                                                        
END        
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_reporte_rndc]   
(              
	@par_EMPR_Codigo  smallint,      
	@par_ENRE_NumeroDocumento NUMERIC = NULL,      
	@par_ENMC_NumeroDocumento NUMERIC = NULL,      
	@par_Fecha_Inicial date = NULL,      
	@par_Fecha_Final date = NULL,        
	@par_OFIC_Codigo NUMERIC = NULL,              
	@par_NumeroPagina INT = NULL,              
	@par_RegistrosPagina INT = NULL              
)              
AS              
BEGIN              
      
	DECLARE @CantidadRegistros NUMERIC        
	SELECT @CantidadRegistros = (              
		SELECT DISTINCT COUNT(1)               
		FROM Detalle_Manifiesto_Carga AS DMCA        
        
		LEFT JOIN Encabezado_Remesas AS ENRE        
		ON DMCA.EMPR_Codigo = ENRE.EMPR_Codigo         
		AND DMCA.ENRE_Numero = ENRE.Numero        
          
		LEFT JOIN Terceros AS CLIE         
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo        
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo        
      
		LEFT JOIN Terceros AS REMI         
		ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo        
		AND ENRE.TERC_Codigo_Remitente = REMI.Codigo        
          
		LEFT JOIN Vehiculos AS VEHI         
		ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo        
		AND ENRE.VEHI_Codigo = VEHI.Codigo      
      
		LEFT JOIN Semirremolques AS SEMI      
		ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo        
		AND ENRE.SEMI_Codigo = SEMI.Codigo        
          
		LEFT JOIN Rutas AS RUTA         
		ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo        
		AND ENRE.RUTA_Codigo = RUTA.Codigo        
          
		LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS        
		ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo        
		AND ENRE.ESOS_Numero = ESOS.Numero      
            
		LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC        
		ON DMCA.EMPR_Codigo = ENMC.EMPR_Codigo         
		AND DMCA.ENMC_Numero = ENMC.Numero
  
		LEFT JOIN Producto_Transportados AS PRTR        
		ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo         
		AND ENRE.PRTR_Codigo = PRTR.Codigo       
        
		WHERE DMCA.EMPR_Codigo = @par_EMPR_Codigo    
		AND ENRE.Estado = 1-- Estado Remesa Definitivo      
		AND ENRE.Anulado = 0  -- Remesa No Anulada      
		AND ENMC.Estado = 1 -- Manifiesto en Definitivo    
		AND (DMCA.Numero_Remesa_Electronico = 0  OR DMCA.Numero_Remesa_Electronico IS NULL)--Remesa Sin Reportar      
		AND ENRE.ENMC_Numero = ENMC.Numero      
		AND (DMCA.Numero_Cumplido_Electronico = 0 OR DMCA.Numero_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar      
		AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)        
		AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)      
		AND ENRE.Numero_Documento = ISNULL(@par_ENRE_NumeroDocumento,ENRE.Numero_Documento)      
		AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)      
		AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino    
		AND (ENRE.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)  
	);              
	WITH Pagina              
	AS              
		( SELECT      
		0 As Obtener,      
		1 AS OpcionReporte,      
		ENRE.EMPR_Codigo,      
		ENRE.Numero,      
		ENRE.Numero_Documento AS NumeroDocumento,      
		ENRE.Fecha,      
		ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente,      
		ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente,      
		ISNULL(ENRE.TERC_Codigo_Remitente, 0) AS TERC_Codigo_Remitente, 
		/*Modificación FP 2021/12/12*/
		ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente,
		/*Fin Modificación*/  
		ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo,      
		ISNULL(VEHI.Placa,0) AS PlacaVehiculo,      
		ISNULL(ENRE.SEMI_Codigo, 0) AS SEMI_Codigo,      
		ISNULL(SEMI.Placa, 0) AS PlacaSemirremolque,      
		ISNULL(ENRE.RUTA_Codigo,0) AS RUTA_Codigo,      
		ISNULL(RUTA.Nombre, 0) AS NombreRuta,      
		ISNULL(ENRE.ENMC_Numero, 0) AS ENMC_Numero,      
		ENMC.Numero_Documento AS NumeroDocumentoManifiesto,      
		ENRE.ESOS_Numero,
		ENRE.Permiso_Invias,  
		ISNULL(ESOS.Numero_Documento, 0) AS NumeroDocumentoOrdenservicio,      
		ISNULL(DMCA.Mensaje_Remesa_Electronico, '') AS Mensaje,      
		ISNULL(PRTR.Nombre, '') AS PRTR_Nombre,
        
		ROW_NUMBER() OVER (ORDER BY ENRE.Numero) AS RowNumber              
      
		FROM Detalle_Manifiesto_Carga AS DMCA        
        
		LEFT JOIN Encabezado_Remesas AS ENRE        
		ON DMCA.EMPR_Codigo = ENRE.EMPR_Codigo         
		AND DMCA.ENRE_Numero = ENRE.Numero        
          
		LEFT JOIN Terceros AS CLIE         
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo        
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo        
      
		LEFT JOIN Terceros AS REMI         
		ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo          
		AND ENRE.TERC_Codigo_Remitente = REMI.Codigo        
          
		LEFT JOIN Vehiculos AS VEHI         
		ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo        
		AND ENRE.VEHI_Codigo = VEHI.Codigo      
      
		LEFT JOIN Semirremolques AS SEMI      
		ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo        
		AND ENRE.SEMI_Codigo = SEMI.Codigo        
          
		LEFT JOIN Rutas AS RUTA         
		ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo        
		AND ENRE.RUTA_Codigo = RUTA.Codigo        
          
		LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS        
		ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo        
		AND ENRE.ESOS_Numero = ESOS.Numero        
            
		LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC        
		ON DMCA.EMPR_Codigo = ENMC.EMPR_Codigo         
		AND DMCA.ENMC_Numero = ENMC.Numero 
  
		LEFT JOIN Producto_Transportados AS PRTR        
		ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo         
		AND ENRE.PRTR_Codigo = PRTR.Codigo     
        
		WHERE DMCA.EMPR_Codigo = @par_EMPR_Codigo    
		AND ENRE.Estado = 1-- Estado Remesa Definitivo      
		AND ENRE.Anulado = 0  -- Remesa No Anulada      
		AND ENMC.Estado = 1 -- Manifiesto en Definitivo    
		AND (DMCA.Numero_Remesa_Electronico = 0  OR DMCA.Numero_Remesa_Electronico IS NULL)--Remesa Sin Reportar      
		AND ENRE.ENMC_Numero = ENMC.Numero      
		AND (DMCA.Numero_Cumplido_Electronico = 0 OR DMCA.Numero_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar      
		AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)        
		AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)      
		AND ENRE.Numero_Documento = ISNULL(@par_ENRE_NumeroDocumento,ENRE.Numero_Documento)      
		AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)      
		AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino    
		AND (ENRE.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)  
	)              
      
	SELECT      
	Obtener,      
	OpcionReporte,      
	EMPR_Codigo,      
	Numero,      
	NumeroDocumento,      
	Fecha,      
	TERC_Codigo_Cliente,      
	NombreCliente,      
	TERC_Codigo_Remitente,      
	NombreRemitente,      
	VEHI_Codigo,      
	PlacaVehiculo,      
	SEMI_Codigo,      
	PlacaSemirremolque,      
	RUTA_Codigo,      
	NombreRuta,      
	ENMC_Numero,      
	NumeroDocumentoManifiesto,      
	ESOS_Numero,
	Permiso_Invias,     
	NumeroDocumentoOrdenservicio,      
	Mensaje,
	PRTR_Nombre,     
       
	@CantidadRegistros AS TotalRegistros,      
	@par_NumeroPagina AS PaginaObtener,      
	@par_RegistrosPagina AS RegistrosPagina      
      
	FROM Pagina      
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)      
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)       
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_retorno_contado_planilla_paqueteria]  
(                                                        
	@par_EMPR_Codigo SMALLINT,  
	@par_TIDO_Codigo NUMERIC = NULL,  
	@par_NumeroInicial NUMERIC = NULL,  
	@par_FechaInicial DATE = NULL,  
	@par_FechaFinal DATE = NULL,  
	@par_Documento_Cliente VARCHAR(30) = NULL,  
	@par_TERC_Codigo_Cliente NUMERIC = NULL,  
	@par_TERC_Codigo_Remitente NUMERIC = NULL,  
	@par_OFIC_Codigo_Actual_Usuario NUMERIC = NULL,  
	@par_Ciudad_Origen_Codigo NUMERIC = NULL,  
	@par_Ciudad_Destino_Codigo NUMERIC = NULL  
)                                                        
AS
BEGIN                                                        
 IF @par_FechaInicial <> NULL BEGIN                                                        
 SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                                        
 END                                                        
                                                        
 IF @par_FechaFinal <> NULL BEGIN                                                        
 SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                                        
 END  
      
 SELECT  
 ENRE.EMPR_Codigo                                                        
 ,ENRE.Numero                                                        
 ,ENRE.Numero_Documento                                                        
 ,ENRE.Numeracion          
 ,ENRE.Fecha  
 ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                                        
 ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                                    
 ,ENRE.TERC_Codigo_Cliente          
 ,ENRE.TERC_Codigo_Remitente                                                  
 ,ENRE.CATA_FOPR_Codigo                             
 ,FOPR.Campo1 AS FormaPago                                                        
 ,ENRE.RUTA_Codigo                                                        
 ,ENRP.CIUD_Codigo_Origen             
 ,ENRP.CIUD_Codigo_Destino                                          
 ,CIOR.Nombre AS CiudadOrigen                                                        
 ,CIDE.Nombre AS CiudadDestino                                  
 ,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue                                  
 ,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue                                         
 ,ISNULL(ENRP.OFIC_Codigo_Origen, 0) AS OFIC_Codigo_Origen                                        
 ,ISNULL(OFIC.Nombre, '') AS NombreOficinaOrigen                                           
 ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                                           
 ,ENRP.CATA_ESRP_Codigo                                      
 ,ENRE.Observaciones          
 ,(ENRE.Valor_Flete_Cliente) AS  Valor_Flete_Cliente  
 ,ENRE.Total_Flete_Cliente AS Total_Flete_Cliente    
 ,ENRE.Valor_Seguro_Cliente                                                        
 ,ENRE.Cantidad_Cliente                                                        
 ,ENRE.Peso_Cliente                                                        
 ,ENRE.Estado                                                        
 ,ENRE.Anulado                                                        
 ,PRTR.Nombre AS NombreProducto                                                        
 ,ENRE.Documento_Cliente                                           
 ,ENRE.OFIC_Codigo                                          
 ,OFIC.Nombre As NombreOficina                                                       
 ,'' AS NombreRuta                                          
 ,ENRE.Valor_Flete_Transportador                             
 ,ENRE.Valor_Reexpedicion                      
 ,ENRP.Reexpedicion 
 /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario
	/*Fin Modificación*/  
 ,ENRP.OFIC_Codigo_Actual                                                       
 FROM Remesas_Paqueteria AS ENRP         
                                                        
 INNER JOIN Encabezado_Remesas AS ENRE                                                        
 ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                        
 AND ENRP.ENRE_Numero = ENRE.Numero                                                        
                                                    
 INNER JOIN Oficinas AS OFIC ON                                          
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                          
 AND ENRE.OFIC_Codigo = OFIC.Codigo                        
                                        
 LEFT JOIN Terceros AS CLIE                                      
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                        
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                        
                                                        
 LEFT JOIN Terceros AS REMI                                                        
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                        
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                        
                                                        
 LEFT JOIN Terceros AS DEST                                                        
 ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                        
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                    
                                                        
 LEFT JOIN Producto_Transportados AS PRTR                                                        
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                          
 AND ENRE.PRTR_Codigo = PRTR.Codigo  
  
 LEFT JOIN Ciudades CIOR                                                        
 ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                 
 AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                        
                                              
 LEFT JOIN Ciudades CIDE                                                        
 ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                        
 AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                  
                                     
 LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                  
 ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                  
 AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                                
                                                        
 INNER JOIN Oficinas AS OFAC ON                                          
 ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                          
 AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                  
                                      
 LEFT JOIN Valor_Catalogos AS FOPR ON                                
 ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                
 AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                                
        
 WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                    
 AND ENRE.Numero_Documento = ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)                    
 AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                    
 AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)            
 AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                               
 AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                    
 AND ENRE.Anulado = 0                                                      
 AND ENRE.Estado = 1  
 AND (ENRE.ENPD_Numero = 0 OR ENRE.ENPD_Numero IS NULL)  
 AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)  
 AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)            
 AND (ENRP.CIUD_Codigo_Origen = @par_Ciudad_Origen_Codigo OR @par_Ciudad_Origen_Codigo IS NULL)                      
 AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)  
 AND ENRE.CATA_FOPR_Codigo = 4902 -- Forma Pago Contado  
 --AND ENRP.CATA_ESRP_Codigo = 6020 -- Recibida en oficina destino  
 ORDER BY CiudadDestino, isnull(nullif(Numeracion,''),'z') asc, Numero ASC          
END  
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_zonificacion_guias]              
  (                                                                              
 @par_EMPR_Codigo SMALLINT,                                                                              
 @par_Numero numeric = NULL,                                                                              
 @par_Planilla  numeric = NULL,                                                                              
 @par_FechaInicial Date = NULL,             
 @par_FechaFinal Date = NULL,            
 @par_Codigo_ciudad   VARCHAR(1000) = NULL,      
 @par_NumeroPagina INT = NULL,    
 @par_RegistrosPagina INT = NULL      
       
)                                                                              
AS                                                                            
       
 BEGIN                                                                
 SET NOCOUNT ON;                                                                              
 DECLARE @CantidadRegistros INT                                                      
 SELECT                                                                        
 @CantidadRegistros = (SELECT DISTINCT                                                     
 COUNT(1)                                                                        
 FROM Remesas_Paqueteria AS ENRP            
            
 INNER JOIN Encabezado_Remesas AS ENRE                                          
 ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                                              
 AND ENRP.ENRE_Numero = ENRE.Numero 
  AND ENRE.Numero_Documento = ISNULL(@par_Numero,ENRE.Numero_Documento)    
 AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha )                                                                     
 AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)  
                                                                          
      
 LEFT JOIN Terceros AS CLIE                                               
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                              
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                              
                                                                              
 LEFT JOIN Terceros AS REMI                                                                              
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                              
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                              
                                             
 LEFT JOIN Terceros AS DEST                                                                              
 ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                              
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                                              
                
 LEFT JOIN Producto_Transportados AS PRTR                     
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                          
 AND ENRE.PRTR_Codigo = PRTR.Codigo                                                                              
                             
 LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                            
 ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                 
 AND ENRE.ENPD_Numero = ENPD.Numero                                     
 AND ENPD.TIDO_Codigo IN(130,135)--Planilla Despacho Guias, Planilla Paqueteria             
                     
 LEFT JOIN Vehiculos AS VEHI                    
 ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                    
 AND ENRE.VEHI_Codigo = VEHI.Codigo                                                   
                                                                  
 LEFT JOIN Ciudades CIOR                                                                              
 ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                              
 AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                              
                                                                    
 LEFT JOIN Ciudades CIDE                                                                              
 ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                            
 AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                                                                    
                                                   
                                                            
 LEFT JOIN Valor_Catalogos AS FOPR ON                                                      
 ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                                      
 AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo              
               
 LEFT JOIN Valor_Catalogos AS LNPA ON                                                      
 ENRP.EMPR_Codigo = LNPA.EMPR_Codigo                                                      
 AND ENRP.CATA_LNPA_Codigo = LNPA.Codigo                        
                              
 LEFT JOIN Encabezado_Facturas ENFA ON                            
 ENRE.EMPR_Codigo = ENFA.EMPR_Codigo AND                            
 ENRE.ENFA_Numero = ENFA.Numero                                                      
where             
 ENRE.EMPR_Codigo =   @par_EMPR_Codigo            
 AND ENRP.CATA_ESRP_Codigo  in (6005,6010)            
 AND ENRE.Estado  = 1         
 AND (ENPD.Numero_Documento = ISNULL(@par_Planilla,ENPD.Numero_Documento) OR @par_Planilla IS NULL  )    
 AND  (CIDE.Codigo in (SELECT * FROM dbo.Func_Dividir_String( @par_Codigo_ciudad , ',') )          
 OR @par_Codigo_ciudad IS NULL) 
  AND  ZOCI_Codigo_Entrega = 0  OR ZOCI_Codigo_Entrega is Null                       
 );                                                                        
 WITH Pagina                                                                        
 AS                                                                        
 (                                      
 SELECT           
 ENRE.EMPR_Codigo            
 ,ENRP.ZOCI_Codigo_Entrega        
 ,ENPD.Numero_Documento as NumeroDocumentoPlanilla            
 ,ENRE.Fecha            
 ,ENRP.CATA_ESRP_Codigo,ENRE.Estado              
 ,ENRE.Numero                                                                              
 ,ENRE.Numero_Documento as Numero_Documento               
 ,ENRP.CIUD_Codigo_Origen                                                                              
 ,ENRP.CIUD_Codigo_Destino                                                                              
 ,CIOR.Nombre AS CiudadOrigen                                       
 ,CIDE.Nombre AS CiudadDestino             
 ,ENRE.Direccion_Destinatario 
/*Modificación FP 2021/12/12*/
,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente
,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario
/*Fin Modificación*/  
 ,ENRE.CATA_FOPR_Codigo                                                               
 ,FOPR.Campo1 AS FormaPago            
 ,ENRE.CATA_TIRE_Codigo            
 ,ENRE.Peso_Cliente                                     
 ,ENRP.Peso_Volumetrico                                    
 ,ENRP.Peso_A_Cobrar                
 ,ENRE.Cantidad_Cliente              
 ,ENRE.Total_Flete_Cliente           
 ,LNPA.Campo1 AS CATA_LNPA_Nombre      
 ,ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber       
FROM Remesas_Paqueteria AS ENRP            
            
 INNER JOIN Encabezado_Remesas AS ENRE                                          
 ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                                              
 AND ENRP.ENRE_Numero = ENRE.Numero   
  AND ENRE.Numero_Documento = ISNULL(@par_Numero,ENRE.Numero_Documento)    
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha )                                                                     
 AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)                                                             
                                          
 LEFT JOIN Terceros AS CLIE                                               
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                              
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                              
                                                                              
 LEFT JOIN Terceros AS REMI                                                                              
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                              
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                              
                                             
 LEFT JOIN Terceros AS DEST                                                                              
 ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                              
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                                              
                
 LEFT JOIN Producto_Transportados AS PRTR                     
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                          
 AND ENRE.PRTR_Codigo = PRTR.Codigo                                                                              
                             
 LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                                              
 ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                                    
 AND ENRE.ENPD_Numero = ENPD.Numero                                     
 AND ENPD.TIDO_Codigo IN(130,135)--Planilla Despacho Guias, Planilla Paqueteria                    
                        
                     
 LEFT JOIN Vehiculos AS VEHI                    
 ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                    
 AND ENRE.VEHI_Codigo = VEHI.Codigo                                                   
                                                                  
 LEFT JOIN Ciudades CIOR                                                                              
 ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                              
 AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                              
                                                                    
 LEFT JOIN Ciudades CIDE                                                                              
 ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                  
 AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                                        
                                                     
                                                            
 LEFT JOIN Valor_Catalogos AS FOPR ON                                                      
 ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                                      
 AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo              
               
 LEFT JOIN Valor_Catalogos AS LNPA ON                                                      
 ENRP.EMPR_Codigo = LNPA.EMPR_Codigo                                                      
 AND ENRP.CATA_LNPA_Codigo = LNPA.Codigo                        
                              
 LEFT JOIN Encabezado_Facturas ENFA ON                            
 ENRE.EMPR_Codigo = ENFA.EMPR_Codigo AND                            
 ENRE.ENFA_Numero = ENFA.Numero                            
                                                          
where             
ENRE.EMPR_Codigo =   @par_EMPR_Codigo            
AND ENRP.CATA_ESRP_Codigo  in (6005,6010)            
AND ENRE.Estado  = 1            
AND ENRE.Numero_Documento = ISNULL(@par_Numero,ENRE.Numero_Documento)           
     
AND (ENPD.Numero_Documento = ISNULL(@par_Planilla,ENPD.Numero_Documento) OR @par_Planilla IS NULL  )    
    
AND  (CIDE.Codigo in (SELECT * FROM dbo.Func_Dividir_String( @par_Codigo_ciudad , ',') )          
OR @par_Codigo_ciudad IS NULL)          
 AND  ZOCI_Codigo_Entrega = 0  OR ZOCI_Codigo_Entrega is Null          
  
)                                                                        
 SELECT DISTINCT                                                                        
    EMPR_Codigo            
 ,ZOCI_Codigo_Entrega            
 ,NumeroDocumentoPlanilla            
 ,Fecha            
 ,CATA_ESRP_Codigo      
 ,Estado              
 ,Numero                                                                              
 , Numero_Documento               
 ,CIUD_Codigo_Origen                                                                              
 ,CIUD_Codigo_Destino                                                                              
 ,CiudadOrigen                                                                              
 ,CiudadDestino             
 ,Direccion_Destinatario            
 , NombreRemitente                                                                              
 , NombreDestinatario              
 ,CATA_FOPR_Codigo                                                               
 ,  FormaPago            
 ,CATA_TIRE_Codigo            
 ,Peso_Cliente                                     
 ,Peso_Volumetrico                                    
 ,Peso_A_Cobrar                
 ,Cantidad_Cliente              
 ,Total_Flete_Cliente           
 , CATA_LNPA_Nombre                                            
 ,@CantidadRegistros AS TotalRegistros                                                                        
 ,@par_NumeroPagina AS PaginaObtener                                                                        
 ,@par_RegistrosPagina AS RegistrosPagina                                                             
 FROM Pagina                                                                        
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                                        
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                                        
 ORDER BY Numero ASC                                                             
END        
              
            
            
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_Listado_Remesas_Paqueteria]    
(                      
 @par_EMPR_Codigo NUMERIC,                      
 @par_TIDO_Codigo NUMERIC,                                
 @par_Numero_Documento_Inicial NUMERIC = null,                                    
 @par_Numero_Documento_Final NUMERIC = null,                                    
 @par_Fecha_Incial DATE = NULL,                                     
 @par_Fecha_Final DATE = NULL,                                
 @par_CATA_FOPR_Codigo NUMERIC = NULL,                      
 @par_Reexpediccion NUMERIC = NULL,                      
 @par_Pendientes_Cumplir NUMERIC = NULL,                      
 @par_Pendientes_Facturar NUMERIC = NULL,                      
 @par_Oficina NUMERIC = NULL,                  
 @par_CLIE_Codigo NUMERIC = NULL,                  
 @par_Estado NUMERIC = NULL,          
 @par_Ofic_Actual Numeric = Null                    
)                                   
AS                                 
BEGIN                    
                     
 Declare @Estadolist NUMERIC = NULL;                    
 Declare @Anuladolist NUMERIC = NULL;                    
 Declare @Planilla_Despacho NUMERIC = 130;                    
 Declare @Planilla_Recoleccion Numeric = 205;                    
 Declare @Planilla_Entregas Numeric = 210;                    
 IF (@par_Estado IS NOT NULL)                    
 BEGIN                    
  IF(@par_Estado IN (0,1))                    
  BEGIN                    
   SET @Estadolist = @par_Estado;                    
  END                    
  ELSE                    
  BEGIN                    
   SET @Anuladolist = 1;                    
  END                                 
 END                    
                    
 SELECT                                 
 EMPR.Nombre_Razon_Social,    
 ENRE.Numero,    
 ENRE.Numero_Documento,                      
 ENRE.Fecha,                      
 ISNULL(VEHI.Placa, '') AS Placa,                      
 IIF(ENRE.TERC_Codigo_Cliente > 0,              
 ISNULL(CLIE.Razon_Social,'')+' '+ISNULL(CLIE.Nombre,'') +' '+ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,''),              
 ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'') +' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')              
 )  AS NombreCliente, 
 IIF(ENRE.TERC_Codigo_Cliente > 0,
 ISNULL(CLIE.Numero_Identificacion, ''),
 ISNULL(REMI.Numero_Identificacion, '')) AS IdentificacionCliente,                      
 --CLIE.Numero_Identificacion AS IdentificacionCliente,                      
 CIOR.Nombre AS CiudadOrigen,                      
 CIDE.Nombre AS CiudadDestino,                  
 ENRE.CATA_FOPR_Codigo AS FOPR_Codigo,                    
 FOPR.Campo1 AS FOPR_Nombre,                      
 ENRE.Valor_Flete_Cliente,                  
 ENRE.Total_Flete_Cliente,                  
 ENRE.Numeracion,            
 ENRE.Valor_Seguro_Cliente As Seguro,            
 ENRE.Valor_Flete_Transportador, 
/*Modificación FP 2021/12/12*/
LTRIM(ISNULL(ENRE.Nombre_Remitente, CONCAT(ISNULL(REMI.Razon_Social,''),' ',ISNULL(REMI.Nombre,''),' ',ISNULL(REMI.Apellido1,''),' ',ISNULL(REMI.Apellido2,'')))) AS NombreRemitente,
/*Fin Modificación*/  
 LTRIM(CONCAT(ISNULL(TECC.Razon_Social,''),' ',ISNULL(TECC.Nombre,''),' ',ISNULL(TECC.Apellido1,''),' ',ISNULL(TECC.Apellido2,''))) AS NombreRepresentante,                
 IIF(REPA.Reexpedicion = 0, 'NO', 'SI' ) AS Reexpedicion,                      
 ISNULL(ENPD.Numero_Documento, 0) AS ENPD_NumeroDocumento,                    
 CASE ENPD.TIDO_Codigo                     
 WHEN @Planilla_Despacho THEN 'Despacho'                    
 WHEN @Planilla_Recoleccion THEN 'Recolección'                    
 WHEN @Planilla_Entregas THEN 'Entregas'                    
 ELSE ''                             
 END AS ENPD_Tipo,                      
 ENRE.Peso_Cliente,                      
 ENRE.Cantidad_Cliente,                   
 ENRE.OFIC_Codigo,                     
 OFIC.Nombre NombreOficina,                      
 ISNULL(ENRE.Valor_Comision_Oficina, 0) Valor_Comision_Oficina ,                
 ISNULL(ENRE.Valor_Comision_Agencista, 0) Valor_Comision_Agencista,             
 ISNULL(ENRE.Valor_Reexpedicion, 0) Valor_Reexpedicion,    
 OFAC.Nombre As NombreOficinaActual,             
 ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial,                      
 ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal,                      
 ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial,                      
 ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal,                      
 CASE ENRE.Anulado WHEN 1 THEN 'A' ELSE                      
 CASE ENRE.ESTADO WHEN 1 THEN 'D' ELSE 'B'                      
 END                                 
 END AS Estado,  
 CASE  
WHEN CLIE.CATA_TIID_Codigo = 101 OR REMI.CATA_TIID_Codigo = 101 THEN 'CC'  
WHEN CLIE.CATA_TIID_Codigo = 102 OR REMI.CATA_TIID_Codigo = 102 THEN 'NIT'  
WHEN CLIE.CATA_TIID_Codigo = 103 OR REMI.CATA_TIID_Codigo = 103 THEN 'CE'  
ELSE 'PASAPORTE' END AS Tipo_Documento,  
CLIE.Emails,  
ENRE.Documento_Cliente,  
ENRE.Valor_Comercial_Cliente,  
ENRE.Valor_Flete_Cliente AS Flete_Cliente  
  
                                
 FROM Encabezado_Remesas ENRE                                
 INNER JOIN Empresas EMPR ON                                 
 ENRE.EMPR_Codigo = EMPR.Codigo                                 
                                
 LEFT JOIN Vehiculos VEHI ON                                 
 ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                                
 AND  ENRE.VEHI_Codigo = VEHI.Codigo           
                                
 LEFT JOIN Terceros CLIE ON                                 
 ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                 
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo              
              
 LEFT JOIN Terceros REMI ON                                 
 ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                 
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                 
                      
 LEFT JOIN Ciudades CIOR ON                              
 ENRE.EMPR_Codigo = CIOR.EMPR_Codigo                                 
 AND ENRE.CIUD_Codigo_Remitente = CIOR.Codigo                      
                      
 LEFT JOIN Ciudades CIDE ON                              
 ENRE.EMPR_Codigo = CIDE.EMPR_Codigo                                 
 AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo                           
                       
 LEFT JOIN Valor_Catalogos FOPR ON                                 
 ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                 
 AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                                       
                                
 LEFT JOIN Oficinas OFIC ON                                 
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                 
 AND ENRE.OFIC_Codigo = OFIC.Codigo                      
                
 LEFT JOIN Terceros TECC ON                
 ENRE.EMPR_Codigo = TECC.EMPR_Codigo AND                
 OFIC.TERC_Codigo_Comision = TECC.Codigo                
                      
 LEFT JOIN Encabezado_Planilla_Despachos ENPD ON                      
 ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                 
 AND ENRE.ENPD_Numero = ENPD.Numero                      
                       
 LEFT JOIN Remesas_Paqueteria REPA ON                      
 ENRE.EMPR_Codigo = REPA.EMPR_Codigo                                 
 AND ENRE.Numero = REPA.ENRE_Numero            
           
 LEFT JOIN Oficinas OFAC ON          
 ENRE.EMPR_Codigo = OFAC.EMPR_Codigo AND          
 REPA.ENRE_Numero = ENRE.Numero AND          
 REPA.OFIC_Codigo_Actual = OFAC.Codigo          
    
 WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                    
 AND ENRE.TIDO_Codigo = @par_TIDO_Codigo                             
 AND ENRE.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENRE.Numero_Documento)                                    
 AND ENRE.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENRE.Numero_Documento)                                      
 AND ENRE.Fecha >= ISNULL( @par_Fecha_Incial,ENRE.Fecha)                                     
 AND ENRE.Fecha <= ISNULL( @par_Fecha_Final,ENRE.Fecha)              
 AND ENRE.OFIC_Codigo = ISNULL(@par_Oficina,ENRE.OFIC_Codigo)                    
 AND ENRE.TERC_Codigo_Cliente = ISNULL (@par_CLIE_Codigo, ENRE.TERC_Codigo_Cliente)                    
 --AND ENRE.Estado = ISNULL(@Estadolist,ENRE.Estado)                    
 --AND (ENRE.Anulado = @Anuladolist OR @Anuladolist IS NULL)                
 AND ENRE.Estado = (IIF(@par_Estado is null, ENRE.Estado ,IIF(@par_Estado = 1,1,IIF(@par_Estado = 0, 0,ENRE.Estado))))        
 AND ENRE.Anulado = (IIF(@par_Estado = 2,1,IIF(@par_Estado is null,ENRE.Anulado,0)))                 
 AND ENRE.CATA_FOPR_Codigo = ISNULL (@par_CATA_FOPR_Codigo, ENRE.CATA_FOPR_Codigo)                      
 AND REPA.Reexpedicion = ISNULL (@par_Reexpediccion, REPA.Reexpedicion)                      
 AND ENRE.Cumplido = ISNULL(@par_Pendientes_Cumplir, ENRE.Cumplido)                      
 AND ENRE.ENFA_Numero = ISNULL(@par_Pendientes_Cumplir, ENRE.ENFA_Numero)           
 AND REPA.OFIC_Codigo_Actual = ISNULL(@par_Ofic_Actual,REPA.OFIC_Codigo_Actual)    
 ORDER BY CIDE.Nombre, isnull(nullif(ENRE.Numeracion,''),'z') asc, ENRE.Numero ASC              
END    
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_Listado_Remesas_Paqueteria_Inventarios]          
(                  
  @par_EMPR_Codigo NUMERIC,                  
  @par_TIDO_Codigo NUMERIC,                            
  @par_Numero_Documento_Inicial NUMERIC = null,                                
  @par_Numero_Documento_Final NUMERIC = null,                                
  @par_Fecha_Incial DATE = NULL,                                 
  @par_Fecha_Final DATE = NULL,                            
  @par_CATA_FOPR_Codigo NUMERIC = NULL,                  
  @par_Reexpediccion NUMERIC = NULL,                  
  @par_Pendientes_Cumplir NUMERIC = NULL,                  
  @par_Pendientes_Facturar NUMERIC = NULL,                  
  @par_Oficina NUMERIC = NULL,              
  @par_CLIE_Codigo NUMERIC = NULL,              
  @par_Estado NUMERIC = NULL,      
  @par_Ofic_Actual Numeric = Null                
)                               
AS                             
BEGIN                
                 
 Declare @Estadolist NUMERIC = NULL;                
 Declare @Anuladolist NUMERIC = NULL;                
 Declare @Planilla_Despacho NUMERIC = 130;                
 Declare @Planilla_Recoleccion Numeric = 205;                
 Declare @Planilla_Entregas Numeric = 210;                
 IF (@par_Estado IS NOT NULL)                
 BEGIN                
 IF(@par_Estado IN (0,1))                
 BEGIN                
 SET @Estadolist = @par_Estado;                
 END                
 ELSE                
 BEGIN                
 SET @Anuladolist = 1;                
 END                             
 END                
                
 SELECT                             
 EMPR.Nombre_Razon_Social,                  
 ENRE.Numero_Documento,                  
 ENRE.Fecha,                  
 ISNULL(VEHI.Placa, '') AS Placa,                  
 IIF(ENRE.TERC_Codigo_Cliente > 0,          
  ISNULL(CLIE.Razon_Social,'')+' '+ISNULL(CLIE.Nombre,'') +' '+ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,''),          
  ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'') +' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')          
 )  AS NombreCliente,          
 CLIE.Numero_Identificacion IdentificacionCliente,                  
 CIOR.Nombre AS CiudadOrigen,                  
 CIDE.Nombre AS CiudadDestino,              
 ENRE.CATA_FOPR_Codigo AS FOPR_Codigo,                
 FOPR.Campo1 AS FOPR_Nombre,                  
 ENRE.Valor_Flete_Cliente,              
 ENRE.Total_Flete_Cliente,              
 ENRE.Numeracion,        
 ENRE.Valor_Seguro_Cliente As Seguro,        
 ENRE.Valor_Flete_Transportador,  
/*Modificación FP 2021/12/12*/
LTRIM(ISNULL(ENRE.Nombre_Remitente, CONCAT(ISNULL(REMI.Razon_Social,''),' ',ISNULL(REMI.Nombre,''),' ',ISNULL(REMI.Apellido1,''),' ',ISNULL(REMI.Apellido2,'')))) AS NombreRemitente,
/*Fin Modificación*/  
 LTRIM(CONCAT(ISNULL(TECC.Razon_Social,''),' ',ISNULL(TECC.Nombre,''),' ',ISNULL(TECC.Apellido1,''),' ',ISNULL(TECC.Apellido2,''))) AS NombreRepresentante,            
 IIF(REPA.Reexpedicion = 0, 'NO', 'SI' ) AS Reexpedicion,                  
 ISNULL(ENPD.Numero_Documento, 0) AS ENPD_NumeroDocumento,                
 CASE ENPD.TIDO_Codigo                 
 WHEN @Planilla_Despacho THEN 'Despacho'                
 WHEN @Planilla_Recoleccion THEN 'Recolección'                
 WHEN @Planilla_Entregas THEN 'Entregas'                
 ELSE ''                         
 END AS ENPD_Tipo,                  
 ENRE.Peso_Cliente,                  
 ENRE.Cantidad_Cliente,               
 ENRE.OFIC_Codigo,                 
 OFIC.Nombre NombreOficina,                  
 ENRE.Valor_Comision_Oficina,            
 ENRE.Valor_Comision_Agencista,         
 OFAC.Nombre As NombreOficinaActual,         
 ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial,                  
 ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal,                  
 ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial,                  
 ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal,                  
 CASE ENRE.Anulado WHEN 1 THEN 'A' ELSE                  
 CASE ENRE.ESTADO WHEN 1 THEN 'D' ELSE 'B'                  
 END                             
 END AS Estado                  
                            
 FROM Encabezado_Remesas ENRE                            
 INNER JOIN Empresas EMPR ON                             
 ENRE.EMPR_Codigo = EMPR.Codigo                             
                            
LEFT JOIN Vehiculos VEHI ON                             
 ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                            
 AND  ENRE.VEHI_Codigo = VEHI.Codigo       
                            
 LEFT JOIN Terceros CLIE ON                             
 ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                             
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo          
          
 LEFT JOIN Terceros REMI ON                             
 ENRE.EMPR_Codigo = REMI.EMPR_Codigo                             
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                             
                  
 LEFT JOIN Ciudades CIOR ON                          
 ENRE.EMPR_Codigo = CIOR.EMPR_Codigo                             
 AND ENRE.CIUD_Codigo_Remitente = CIOR.Codigo                  
                  
 LEFT JOIN Ciudades CIDE ON                          
 ENRE.EMPR_Codigo = CIDE.EMPR_Codigo                             
 AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo                       
                   
 LEFT JOIN Valor_Catalogos FOPR ON                             
 ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                             
 AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                                   
                            
 LEFT JOIN Oficinas OFIC ON                             
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                             
 AND ENRE.OFIC_Codigo = OFIC.Codigo                  
            
 LEFT JOIN Terceros TECC ON            
 ENRE.EMPR_Codigo = TECC.EMPR_Codigo AND            
 OFIC.TERC_Codigo_Comision = TECC.Codigo            
                  
 LEFT JOIN Encabezado_Planilla_Despachos ENPD ON                  
 ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                             
 AND ENRE.ENPD_Numero = ENPD.Numero                  
                   
 LEFT JOIN Remesas_Paqueteria REPA ON                  
 ENRE.EMPR_Codigo = REPA.EMPR_Codigo                             
 AND ENRE.Numero = REPA.ENRE_Numero        
       
 LEFT JOIN Oficinas OFAC ON      
 ENRE.EMPR_Codigo = OFAC.EMPR_Codigo AND      
 REPA.ENRE_Numero = ENRE.Numero AND      
 REPA.OFIC_Codigo_Actual = OFAC.Codigo      
  
    
                  
 WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                
 AND ENRE.TIDO_Codigo = @par_TIDO_Codigo                         
 AND ENRE.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENRE.Numero_Documento)                                
 AND ENRE.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENRE.Numero_Documento)                                  
 AND ENRE.Fecha >= ISNULL( @par_Fecha_Incial,ENRE.Fecha)                                 
 AND ENRE.Fecha <= ISNULL( @par_Fecha_Final,ENRE.Fecha)                                     
 AND ENRE.OFIC_Codigo = ISNULL(@par_Oficina,ENRE.OFIC_Codigo)                
 AND ENRE.TERC_Codigo_Cliente = ISNULL (@par_CLIE_Codigo, ENRE.TERC_Codigo_Cliente)                
 --AND ENRE.Estado = ISNULL(@Estadolist,ENRE.Estado)                
 --AND (ENRE.Anulado = @Anuladolist OR @Anuladolist IS NULL)            
  AND ENRE.Estado = (IIF(@par_Estado is null, ENRE.Estado ,IIF(@par_Estado = 1,1,IIF(@par_Estado = 0, 0,ENRE.Estado))))    
 AND ENRE.Anulado = (IIF(@par_Estado = 2,1,IIF(@par_Estado is null,ENRE.Anulado,0)))             
 AND ENRE.CATA_FOPR_Codigo = ISNULL (@par_CATA_FOPR_Codigo, ENRE.CATA_FOPR_Codigo)                  
 AND REPA.Reexpedicion = ISNULL (@par_Reexpediccion, REPA.Reexpedicion)                  
 AND ENRE.Cumplido = ISNULL(@par_Pendientes_Cumplir, ENRE.Cumplido)                  
 AND ENRE.ENFA_Numero = ISNULL(@par_Pendientes_Cumplir, ENRE.ENFA_Numero)       
 AND REPA.OFIC_Codigo_Actual = ISNULL(@par_Ofic_Actual,REPA.OFIC_Codigo_Actual)        
 AND REPA.CATA_ESRP_Codigo IN (6005,6020,2035,6040)         
END          
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_obtener_detalle_legalizar_guias_paqueteria]
(      
	@par_EMPR_Codigo SMALLINT,          
	@par_Numero NUMERIC    
)          
AS          
BEGIN    
	SELECT      
	ENRE.EMPR_Codigo    
	,ENRE.Numero    
	,ENRE.Numero_Documento    
	,ENRE.TIDO_Codigo    
	,ENRE.Fecha    
	,ENRE.TERC_Codigo_Cliente    
	,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente    
	,ENRE.TERC_Codigo_Remitente
	/*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente
	,ENRE.TERC_Codigo_Destinatario  
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario
	/*Fin Modificación*/  
	,ENRE.CATA_FOPR_Codigo                                 
	,FOPR.Campo1 AS FormaPago                                                            
	,ENRE.RUTA_Codigo                                                            
	,ENRP.CIUD_Codigo_Origen                 
	,ENRP.CIUD_Codigo_Destino                                              
	,CIOR.Nombre AS CiudadOrigen    
	,CIDE.Nombre AS CiudadDestino    
	,ENRP.OFIC_Codigo_Actual    
	,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual    
	,ENRP.CATA_ESRP_Codigo    
	,ESRP.Campo1 AS NombreEstadoRemesa           
	,(ENRE.Valor_Flete_Cliente) AS  Valor_Flete_Cliente      
	,ENRE.Total_Flete_Cliente AS Total_Flete_Cliente    
	,ENRE.Cantidad_Cliente    
	,ENRE.Peso_Cliente                                                                                 
	,ENRE.Valor_Flete_Transportador    
	,VEHI.Placa    
	,ENPD.Numero_Documento AS NumeroDocumentoPlanilla    
	,ENPD.Fecha AS FechaPlanilla    
	,ELRG.Numero_Documento AS ELRG_NumeroDocumento
	,DLGU.CATA_NLGU_Codigo    
	,DLGU.Gestion_Documentos    
	,DLGU.Entrega_Recaudo    
	,DLGU.Entrega_Archivo    
	,DLGU.Entrega_Cartera    
	,DLGU.Observaciones    
	,DLGU.Valor_Recaudo_Tercero  
	,DLGU.Legalizo
	,ENRP.Guia_Creada_Ruta_Conductor
    
	FROM Detalle_Legalizacion_Guias DLGU    
    
	INNER JOIN Remesas_Paqueteria AS ENRP    
	ON ENRP.EMPR_Codigo = DLGU.EMPR_Codigo    
	AND ENRP.ENRE_Numero = DLGU.ENRE_Numero    
                                                            
	INNER JOIN Encabezado_Remesas AS ENRE                                                            
	ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                            
	AND ENRP.ENRE_Numero = ENRE.Numero                                                            
                                                        
	INNER JOIN Oficinas AS OFIC ON                                              
	ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                              
	AND ENRE.OFIC_Codigo = OFIC.Codigo                            
                                            
	LEFT JOIN Terceros AS CLIE                                          
	ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                            
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                            
                                                            
	LEFT JOIN Terceros AS REMI                                                            
	ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                            
	AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                            
                                                            
	LEFT JOIN Terceros AS DEST                                                            
	ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                            
	AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                        
                                                            
	LEFT JOIN Producto_Transportados AS PRTR                                                            
	ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                        
	AND ENRE.PRTR_Codigo = PRTR.Codigo      
      
	LEFT JOIN Ciudades CIOR                                                            
	ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                     
	AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                            
                                                  
	LEFT JOIN Ciudades CIDE                                                            
	ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                            
	AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                      
                                         
	LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                      
	ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                      
	AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                                    
                                                            
	INNER JOIN Oficinas AS OFAC ON                                              
	ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                              
	AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                      
                                          
	LEFT JOIN Valor_Catalogos AS FOPR ON    
	ENRE.EMPR_Codigo = FOPR.EMPR_Codigo    
	AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo    
    
	LEFT JOIN Valor_Catalogos AS ESRP ON    
	ENRP.EMPR_Codigo = ESRP.EMPR_Codigo    
	AND ENRP.CATA_ESRP_Codigo = ESRP.Codigo    
    
	LEFT JOIN Encabezado_Planilla_Despachos ENPD ON    
	ENPD.EMPR_Codigo = ENRP.EMPR_Codigo    
	AND ENPD.Numero = ENRP.ENPD_Numero_Ultima_Planilla

	LEFT JOIN Encabezado_Legalizacion_Recaudo_Guias ELRG ON    
	ELRG.EMPR_Codigo = ENRE.EMPR_Codigo    
	AND ELRG.Numero = ENRE.ELRG_Numero 
    
	LEFT JOIN Vehiculos VEHI ON    
	VEHI.EMPR_Codigo = ENRE.EMPR_Codigo    
	AND VEHI.Codigo = ENRE.VEHI_Codigo  
    
	WHERE DLGU.EMPR_Codigo = @par_EMPR_Codigo    
	AND DLGU.ELGU_Numero = @par_Numero    
     
	ORDER BY Numero ASC        
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_obtener_remesa]                  
(                                      
 @par_EMPR_Codigo SMALLINT,                                    
 @par_Numero INT                                      
)                                      
AS                                    
BEGIN                                    
 SELECT                                    
  1 AS Obtener                                    
    ,ENRE.EMPR_Codigo                                    
    ,ENRE.Numero                                    
    ,ENRE.TIDO_Codigo                                    
    ,TIDO.Nombre AS TipoDocumento                                    
    ,ENRE.Numero_Documento                                    
    ,ENRE.CATA_TIRE_Codigo                                    
    ,TIRE.Nombre AS TipoRemesa                                    
    ,ISNULL(ENRE.Documento_Cliente, '') AS Documento_Cliente                                    
    ,ISNULL(ENRE.Fecha_Documento_Cliente, '') AS Fecha_Documento_Cliente                                    
    ,ENRE.Fecha                                    
    ,ENRE.RUTA_Codigo                                    
    ,RUTA.Nombre AS NombreRuta                                    
    ,ENRE.PRTR_Codigo                                    
    ,PRTR.Nombre AS NombreProducto                                   
    ,PRTR.UEPT_Codigo AS Unidad_Empaque                                 
    ,ENRE.CATA_FOPR_Codigo                                    
    ,ENRE.TERC_Codigo_Cliente                                    
    ,CONCAT(CLIE.Nombre, ' ', CLIE.Apellido1, ' ', CLIE.Apellido2, ' ', CLIE.Razon_Social) AS NombreCliente                                    
    ,ENRE.TERC_Codigo_Remitente   
	/*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente
	/*Fin Modificación*/  
    ,ENRE.CIUD_Codigo_Remitente                                    
    ,CIRE.Nombre AS CiudadRemitente                                    
    ,ENRE.Direccion_Remitente                                    
    ,ENRE.Telefonos_Remitente                                    
    ,ENRE.Observaciones                                    
    ,ENRE.Cantidad_Cliente                                    
    ,ENRE.Peso_Cliente                                    
    ,ENRE.Peso_Volumetrico_Cliente                                    
    ,ENRE.DTCV_Codigo                                    
    ,ENRE.Valor_Flete_Cliente                                    
    ,ENRE.Valor_Manejo_Cliente                                    
    ,ENRE.Valor_Seguro_Cliente                                    
    ,ENRE.Valor_Descuento_Cliente                                    
    ,ENRE.Total_Flete_Cliente                                    
    ,ENRE.Valor_Comercial_Cliente                                    
    ,ENRE.Cantidad_Transportador                                    
    ,ENRE.Peso_Transportador                                    
    ,ENRE.Valor_Flete_Transportador                                    
    ,ENRE.Total_Flete_Transportador                                    
    ,ENRE.TERC_Codigo_Destinatario
	/*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario
	/*Fin Modificación*/  
	 ,DEST.CATA_TIID_Codigo                             
	 ,DEST.Numero_Identificacion                             
	 ,DEST.Telefonos                             
    ,ENRE.CIUD_Codigo_Destinatario                                    
    ,CIDE.Nombre AS CiudadDestinatario                                    
    ,ENRE.Direccion_Destinatario                                    
    ,ENRE.Telefonos_Destinatario                                    
    ,ENRE.Anulado                                    
    ,ENRE.Estado                                    
    ,ENRE.Fecha_Crea                                    
    ,ENRE.USUA_Codigo_Crea                        
    ,USCR.Codigo_Usuario AS UsuarioCrea                                    
    ,ISNULL(ENRE.Fecha_Modifica, '') AS Fecha_Modifica                                    
    ,ISNULL(ENRE.USUA_Codigo_Modifica, '') AS USUA_Codigo_Modifica                                    
    ,ISNULL(ENRE.Fecha_Anula, '') AS Fecha_Anula                                    
    ,ISNULL(ENRE.USUA_Codigo_Anula, '') AS USUA_Codigo_Anula                                    
    ,ISNULL(ENRE.Causa_Anula, '') AS Causa_Anula                           
    ,ENRE.OFIC_Codigo                                    
    ,OFIC.Nombre AS NombreOficina                                    
    ,ISNULL(ENRE.Numeracion, '') AS Numeracion                                    
    ,ENRE.ETCC_Numero AS NumeroTarifarioCompra                                    
    ,ENRE.ETCV_Numero AS NumeroTarifarioVenta                                    
    ,ENRE.ESOS_Numero                                    
    ,ESOS.Numero_Documento AS OrdenServicio                                    
    ,ISNULL(ENRE.ENOC_Numero,0) AS NumeroOrdenCargue                                    
    ,ISNULL(ENOC.Numero_Documento,0) AS NumeroDocumentoOrdenCargue                                    
    ,ENRE.ENPD_Numero AS NumeroPlanilla                                    
    ,ISNULL(ENPD.Numero_Documento,0) AS NumeroDocumentoPlanilla                                    
    ,ENRE.ENMC_Numero AS NumeroManifiesto                                    
    ,ISNULL(ENMC.Numero_Documento,0) AS NumeroDocumentoManifiesto                                    
    ,ENRE.ECPD_Numero AS NumeroCumplido                                    
    ,ENRE.ENFA_Numero AS NumeroFactura                                    
    ,ENRE.VEHI_Codigo                                    
    ,ISNULL(ENRE.SEMI_Codigo, 0) AS SEMI_Codigo                                    
    ,VEHI.Placa                                    
    ,ISNULL(SEMI.Placa, '') AS PlacaSemirremolque                                    
    ,ENRE.TERC_Codigo_Conductor                                    
    ,CONCAT(COND.Nombre, ' ', COND.Apellido1, ' ', COND.Apellido2, ' ', COND.Razon_Social) AS NombreConductor                                    
    ,VEHI.TERC_Codigo_Tenedor                                    
    ,CONCAT(TENE.Nombre, ' ', TENE.Apellido1, ' ', TENE.Apellido2, ' ', TENE.Razon_Social) AS NombreTenedor                                    
    ,COND.Telefonos AS TelefonoConductor                                    
    ,DTCV.TATC_Codigo_Venta As TATC_Codigo                                   
    ,TTTC.NombreTarifa AS TipoTarifa                                    
    ,TTTC.Codigo AS CodigoValorTipoTarifa                                    
    ,TTTC.Nombre AS NombreValorTipoTarifa                                    
    ,ISNULL(VEHI.CATA_TIVE_Codigo,0) As CATA_TIVE_Codigo                                  
    ,ISNULL(TIVE.Nombre,'') AS TipoVehiculo                                    
    , CASE WHEN ENRE.Numeracion IS NULL OR ENRE.Numeracion = '' THEN NULL ELSE  ENRE.Numeracion END AS Numero_Remesa_Electronico                              
 ,ENRE.Numero_Contenedor                              
 ,ENRE.MBL_Contenedor                              
 ,ENRE.HBL_Contenedor                              
 ,ENRE.DO_Contenedor                              
 ,ENRE.Valor_FOB                                    
 ,ENRE.Entregado                                    
 ,REPA.Fecha_Recibe                          
,REPA.CATA_TIID_Codigo_Recibe                          
,REPA.Numero_Identificacion_Recibe                          
,REPA.Nombre_Recibe                          
,REPA.Telefonos_Recibe                          
,REPA.Cantidad_Recibe                          
,REPA.Peso_Recibe                          
,REPA.Observaciones_Recibe                          
,REPA.Firma_Recibe                  
,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue          ,        
ENRE.SICD_Codigo_Devolucion_Contenedor,        
ENRE.Fecha_Devolucion_Contenedor  ,      
ENRE.CIUD_Codigo_Devolucion_Contenedor,      
ENRE.Patio_Devolcuion  ,    
ENRE.Valor_Declarado  ,  
ENRE.DT_SAP,  
ENRE.Entrega_SAP,  
ENRE.TERC_Codigo_Facturara,  
LTRIM(RTRIM(CONCAT(FACA.Razon_Social,' ',FACA.Nombre,' ',FACA.Apellido1,' ',FACA.Apellido2))) As NombreFacturarA  
 FROM                                    
  Encabezado_Remesas ENRE                  
                  
  LEFT JOIN Tipo_Documentos TIDO                                    
  ON ENRE.EMPR_Codigo = TIDO.EMPR_Codigo AND ENRE.TIDO_Codigo = TIDO.Codigo                                    
                                    
  LEFT JOIN V_Tipo_Remesas TIRE                                    
  ON ENRE.EMPR_Codigo = TIRE.EMPR_Codigo AND ENRE.CATA_TIRE_Codigo = TIRE.Codigo                                    
                                    
  LEFT JOIN Rutas RUTA                                    
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo AND ENRE.RUTA_Codigo = RUTA.Codigo                                    
                    
  LEFT JOIN Producto_Transportados PRTR                                    
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo AND ENRE.PRTR_Codigo = PRTR.Codigo                                    
                                    
  LEFT JOIN Terceros CLIE                                    
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo            
    
  LEFT JOIN Terceros FACA                                    
  ON ENRE.EMPR_Codigo = FACA.EMPR_Codigo AND ENRE.TERC_Codigo_Facturara = FACA.Codigo   
                                    
  LEFT JOIN Terceros REMI                                    
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                    
                                    
  LEFT JOIN Ciudades CIRE                                    
  ON ENRE.EMPR_Codigo = CIRE.EMPR_Codigo AND ENRE.CIUD_Codigo_Remitente = CIRE.Codigo                                    
                                    
  LEFT JOIN Terceros DEST                                    
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                    
                                    
  LEFT JOIN Ciudades CIDE                                    
  ON ENRE.EMPR_Codigo = CIDE.EMPR_Codigo AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo                                    
                                    
  LEFT JOIN Usuarios USCR                                    
  ON ENRE.EMPR_Codigo = USCR.EMPR_Codigo AND ENRE.USUA_Codigo_Crea = USCR.Codigo                                    
                                    
  LEFT JOIN Oficinas OFIC                                    
  ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo AND ENRE.OFIC_Codigo = OFIC.Codigo                                    
                                     
  LEFT JOIN Terceros COND                                    
  ON ENRE.EMPR_Codigo = COND.EMPR_Codigo AND ENRE.TERC_Codigo_Conductor = COND.Codigo                                    
                                    
                                    
  --LEFT JOIN Detalle_Tarifario_Carga_Ventas DTCV                                    
  --ON ENRE.EMPR_Codigo = DTCV.EMPR_Codigo AND ENRE.DTCV_Codigo = DTCV.Codigo                                    
                                    
  LEFT JOIN Detalle_Despacho_Orden_Servicios DTCV ON                                  
  ENRE.EMPR_Codigo = DTCV.EMPR_Codigo                                   
  AND ENRE.DTCV_Codigo = DTCV.ID                                  
                                
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS                                    
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo AND ENRE.ESOS_Numero = ESOS.Numero                                    
                                   
 -----------                                  
                                  
  LEFT JOIN V_Tipo_Tarifa_Transporte_Carga TTTC                                    
  ON DTCV.EMPR_Codigo = TTTC.EMPR_Codigo                     
  AND DTCV.TTTC_Codigo_Venta = ISNULL(TTTC.Codigo,0)            
  AND DTCV.TATC_Codigo_Venta = TTTC.TATC_Codigo                                    
                                  
  LEFT JOIN Vehiculos VEHI                                    
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo AND ENRE.VEHI_Codigo = VEHI.Codigo                                    
                                  
  --                                  
  LEFT JOIN Terceros TENE                                    
  ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo                                     
                                  
  LEFT JOIN V_Tipo_vehiculos TIVE                             
  ON VEHI.EMPR_Codigo = TIVE.EMPR_Codigo AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo                                    
  --                                  
                                  
  LEFT JOIN Semirremolques SEMI                                    
  ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo AND ENRE.SEMI_Codigo = SEMI.Codigo                                    
                                  
  LEFT JOIN Detalle_Manifiesto_Carga DMCA                                    
  ON ENRE.EMPR_Codigo = DMCA.EMPR_Codigo                         
  AND ENRE.Numero = DMCA.ENRE_Numero                                    
                                    
  LEFT JOIN Encabezado_Manifiesto_Carga ENMC           
  ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo AND ENRE.ENMC_Numero = ENMC.Numero                                    
                                    
  LEFT JOIN Encabezado_Orden_Cargues ENOC                                    
  ON ENRE.EMPR_Codigo = ENOC.EMPR_Codigo AND ENRE.ENOC_Numero = ENOC.Numero                                    
                                      
  LEFT JOIN Encabezado_Planilla_Despachos ENPD                                    
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo AND ENRE.ENPD_Numero = ENPD.Numero                                    
                          
  LEFT JOIN Remesas_Paqueteria as REPA                          
  on ENRE.EMPR_Codigo = REPA.EMPR_Codigo                          
  AND ENRE.Numero = REPA.ENRE_Numero                          
                    
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                  
  REPA.EMPR_Codigo = SICD_DESC.EMPR_Codigo                  
  AND REPA.SICD_Codigo_Descargue = SICD_DESC.Codigo                  
                        
                                    
 WHERE                                    
  ENRE.EMPR_Codigo = @par_EMPR_Codigo                                    
  AND ENRE.Numero = @par_Numero                                    
END                 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_Reporte_Detalle_Manifiesto]            
(      
@par_EMPR_Codigo NUMERIC,            
@par_Numero NUMERIC            
)            
AS BEGIN             
            
            
SELECT             
ENRE.Numero_Documento AS NumeroRemesa,             
---unidad medida            
ENRE.Cantidad_Cliente ,             
ENRE.Peso_Cliente ,             
PRTR.Codigo AS CodigoProducto,            
PRTR.Nombre AS NombreProducto            
,UEPT.Nombre_Corto UnidadEmpaque            
,UMPT.Nombre_Corto UnidadMedida            
,ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'')              
+' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS TitutlarTenedor            
,TENE.Numero_Identificacion AS IdentificacionTenedor            
            
,ISNULL(TEPR.Razon_Social,'')+' '+ISNULL(TEPR.Nombre,'')              
+' '+ISNULL(TEPR.Apellido1,'')+' '+ISNULL(TEPR.Apellido2,'') AS TitutlarPropietario            
,TEPR.Numero_Identificacion AS IdentificacionPropietario             
            
,ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')              
+' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS TitutlarConductor            
            
,TECO.Numero_Identificacion AS IdentificacionConductor            
/*Modificación FP 2021/12/12*/
,ISNULL(ENRE.Nombre_Remitente, ISNULL(TERE.Razon_Social,'')+' '+ISNULL(TERE.Nombre,'')+' '+ISNULL(TERE.Apellido1,'')+' '+ISNULL(TERE.Apellido2,'')) AS NombreRemitente
/*Fin Modificación*/            
,TERE.Numero_Identificacion AS IdentificacionRemitente      
,CASE WHEN TERE.CATA_TIID_Codigo = 101 THEN 'CC' WHEN TERE.CATA_TIID_Codigo = 102 THEN 'NIT' WHEN TERE.CATA_TIID_Codigo = 103 THEN 'CE' END AS TipoIdentificaciónRemitente    
,CIRE.Nombre AS CiudadRemitente                
/*Modificación FP 2021/12/12*/
,ISNULL(ENRE.Nombre_Destinatario, ISNULL(TEDE.Razon_Social,'')+' '+ISNULL(TEDE.Nombre,'')+' '+ISNULL(TEDE.Apellido1,'')+' '+ISNULL(TEDE.Apellido2,'')) AS NombreDestinatario
/*Fin Modificación*/  		
,CASE WHEN TEDE.CATA_TIID_Codigo = 101 THEN 'CC' WHEN TEDE.CATA_TIID_Codigo = 102 THEN 'NIT' WHEN TEDE.CATA_TIID_Codigo = 103 THEN 'CE' END AS TipoIdentificaciónDestinatario    
,CIDE.Nombre AS CiudadDestinatario            
,TEDE.Numero_Identificacion AS IdentificacionDestinatario            
,NAPT.Campo1 Naturaleza            
,ENRE.Direccion_Remitente          
,ENRE.Direccion_Destinatario       
,ISNULL(ESOS.Horas_Pactadas_Cargue    ,0) Horas_Pactadas_Cargue  
,ISNULL(ESOS.Horas_Pactadas_Descargue    , 0) Horas_Pactadas_Descargue  
/*Modi*/  
,ISNULL(ENRE.Permiso_Invias , ' ' )   Permiso_Invias      
FROM             
Encabezado_Manifiesto_Carga ENMA             
            
INNER JOIN Detalle_Manifiesto_Carga DEMA ON             
ENMA.EMPR_Codigo =  DEMA.EMPR_Codigo            
AND ENMA.Numero = DEMA.ENMC_Numero            
            
INNER JOIN Encabezado_Remesas ENRE ON             
DEMA.EMPR_Codigo = ENRE.EMPR_Codigo             
AND DEMA.ENRE_Numero = ENRE.Numero            
      
left JOIN Encabezado_Solicitud_Orden_Servicios ESOS ON      
ENRE.EMPR_Codigo = ESOS.EMPR_Codigo      
AND ENRE.ESOS_Numero = ESOS.Numero       
           
INNER JOIN Producto_Transportados PRTR ON             
ENRE.EMPR_Codigo = PRTR.EMPR_Codigo            
AND ENRE.PRTR_Codigo = PRTR.Codigo             
            
INNER JOIN Valor_Catalogos NAPT ON             
PRTR.EMPR_Codigo = NAPT.EMPR_Codigo            
AND PRTR.CATA_NAPT_Codigo = NAPT.Codigo            
            
INNER JOIN Terceros TEPR ON            
ENMA.EMPR_Codigo = TEPR.EMPR_Codigo             
AND ENMA.TERC_Codigo_Propietario = TEPR.Codigo            
            
INNER JOIN Terceros TENE ON             
ENMA.EMPR_Codigo = TENE.EMPR_Codigo             
AND ENMA.TERC_Codigo_Tenedor = TENE.Codigo            
            
INNER JOIN Terceros TECO ON            
ENMA.EMPR_Codigo = TECO.EMPR_Codigo             
AND ENMA.TERC_Codigo_Conductor = TECO.Codigo            
            
INNER JOIN Terceros TERE ON             
ENRE.EMPR_Codigo = TERE.EMPR_Codigo             
AND ESOS.TERC_Codigo_Remitente = TERE.Codigo             
            
INNER JOIN Terceros TEDE ON            
ENRE.EMPR_Codigo = TEDE.EMPR_Codigo             
AND ENRE.TERC_Codigo_Destinatario = TEDE.Codigo            
            
INNER JOIN Ciudades CIRE ON            
ENRE.EMPR_Codigo = CIRE.EMPR_Codigo             
AND ENRE.CIUD_Codigo_Remitente = CIRE.Codigo             
            
INNER JOIN Ciudades CIDE ON       
ENRE.EMPR_Codigo = CIDE.EMPR_Codigo             
AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo            
            
INNER JOIN  Unidad_Empaque_Producto_Transportados UEPT ON             
PRTR.EMPR_Codigo = UEPT.EMPR_Codigo            
AND PRTR.UEPT_Codigo = UEPT.Codigo            
            
INNER JOIN Unidad_Medida_Producto_Transportados UMPT ON             
PRTR.EMPR_Codigo = UMPT.EMPR_Codigo            
AND PRTR.UMPT_Codigo = UMPT.Codigo            
             
WHERE ENMA.EMPR_Codigo = @par_EMPR_Codigo            
AND ENMA.Numero = @par_Numero            
            
END    
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_Reporte_Detalle_Planilla_Guias_Recolecciones]
(
	@par_EMPR_Codigo NUMERIC,
	@par_ENPD_Numero NUMERIC
)
AS 
BEGIN
	SELECT 
	ENRE.Numero_Documento,
	ENRE.Fecha,
	ISNULL(CLIE.Razon_Social,'')+' '+ISNULL(CLIE.Nombre,'') +' '+ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,'') AS NombreCliente,
	/*Modificación FP 2021/12/12*/
	ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente,
	ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario,
	/*Fin Modificación*/  
	ISNULL(CDES.Nombre, '') AS CiudadDestino,
	ENRE.Barrio_Destinatario,
	ENRE.Direccion_Destinatario,
	ENRE.Telefonos_Destinatario,
	PRTR.Nombre,
	ENRE.Cantidad_Cliente,
	ENRE.Peso_Cliente,
	ENRE.Observaciones
	
	FROM Detalle_Planilla_Despachos DEPD, Encabezado_Remesas ENRE

	LEFT JOIN Terceros CLIE ON
	ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo

	LEFT JOIN Terceros REMI ON
	ENRE.EMPR_Codigo = REMI.EMPR_Codigo
	AND ENRE.TERC_Codigo_Remitente = REMI.Codigo

	LEFT JOIN Terceros DEST ON
	ENRE.EMPR_Codigo = DEST.EMPR_Codigo
	AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo

	LEFT JOIN Ciudades CDES ON
	ENRE.EMPR_Codigo = CDES.EMPR_Codigo
	AND ENRE.CIUD_Codigo_Destinatario = CDES.Codigo

	LEFT JOIN Producto_Transportados AS PRTR ON
	ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
	AND ENRE.PRTR_Codigo = PRTR.Codigo

	WHERE DEPD.EMPR_Codigo = @par_EMPR_Codigo
	and DEPD.ENPD_Numero = @par_ENPD_Numero
	AND DEPD.ENRE_Numero = ENRE.Numero
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_reporte_guia_paqueteria]             
(                                
  @par_EMPR_Codigo numeric                                         
 , @par_Numero_Documento numeric                      
                      
)                                        
AS BEGIN              
 declare @Precientos varchar(max)                  
 select @Precientos= CONCAT(@Precientos,CONVERT(VARCHAR(50),DAPO.Numero_Precinto) + ' ' +TPRE.Campo1+ ' ,')                          
 from                             
                            
 Detalle_Asignacion_Precintos_Oficina as DAPO                            
 inner join Encabezado_Asignacion_Precintos_Oficina as EAPO                            
 ON DAPO.EMPR_Codigo = EAPO.EMPR_Codigo                            
 AND DAPO.EAPO_Numero = EAPO.Numero                            
                            
 INNER JOIN Valor_Catalogos AS TPRE              
 ON EAPO.EMPR_Codigo = TPRE.EMPR_Codigo              
 AND EAPO.CATA_TPRE_Codigo = TPRE.Codigo              
                            
 WHERE DAPO.EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_Numero_Documento              
 select @Precientos = SUBSTRING (@Precientos,1,len(@Precientos)-1)              
              
 SELECT DISTINCT                                        
 SUBSTRING( EMPR.Nombre_Razon_Social, 1,20)     as Nombre_Razon_Social                                     
 , EMPR.Numero_Identificacion NIT                                        
 , EMPR.Direccion                                         
 , EMPR.Telefonos TelefonoEmpresa                                        
 , ENRE.Numero_Documento AS Remesa                                        
 , CIOR.Nombre AS CiudadOrigen                                        
 , CIDE.Nombre AS CiudadDestino             
 ,DEPA.Nombre AS DepartamentoDestino          
 , ENRE.Numero_Contenedor                                
 ,ESOS.Numero_Documento AS NumeroOrdenServicio              
 ,ESOS.TLNC_Codigo              
 ,IEOS.Declaracion_Importacion              
 ,IEOS.Fecha_Vencimiento AS IEOS_Fecha_Vencimiento              
 , ENRE.FECHA 
 	/*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(TERE.Razon_Social,'')+' '+ISNULL(TERE.Nombre,'')+' '+ISNULL(TERE.Apellido1,'')+' '+ISNULL(TERE.Apellido2,'')) AS NombreRemitente
	/*Fin Modificación*/      
  , REMEPA.Fecha_Entrega        
 ,ENRE.Codigo_Postal_Remitente  as CodigoPostalRemitente    
 /*Modificación FP 2021/12/12*/                        
,ISNULL(ENRE.Email_Remitente, SUBSTRING( TERE.Correo_Facturacion  , 1, 20)) as CorreoRemitente  
 /*Fin Modificación*/
 ,TERE.Numero_Identificacion IdentificacionRemitente            
         
         
        
 , CIRE.Nombre AS Ciudad_remitente                                    
 ,TERE.Telefonos TelefonoRemitente 
  
 , SUBSTRING(  ENRE.Direccion_Remitente  ,1,19) as      Direccion_Remitente                              
 , SUBSTRING(ENRE.Direccion_Destinatario   ,1,19) as Direccion_Destinatario                                      
 ,  EMPR.Telefonos                                        
 ,  SUBSTRING(ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')                                          
 +' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,''), 1, 20) AS NombreConductor                                        
 , EMPR.Telefonos TelefonoConductor                                         
 , TECO.Numero_Identificacion CedulaConductor                                         
 , ENRE.Observaciones                                         
 , VEHI.Placa                                        
 , ENRE.Cantidad_Cliente                                        
 --                                        
 ,REMEPA.Peso_A_Cobrar as Peso_Cliente                                         
 ,PRTR.Nombre AS NombreProducto             
 ,REMEPA.Descripcion_Mercancia AS DescripcionMercancia        
   ,REMEPA.Largo         
   ,REMEPA.Alto          
        
  ,ENRE.Valor_Flete_Cliente         
 ,ENRE.Valor_Manejo_Cliente         
 ,ENRE.Valor_Seguro_Cliente         
 ,ENRE.Valor_Descuento_Cliente         
 ,ENRE.Total_Flete_Cliente         
 ,ENRE.Valor_Comercial_Cliente        
          
 ,ENRE.Valor_Otros         
 ,ENRE.Valor_Cargue         
 ,ENRE.Valor_Descargue        
        
,CASE                 
 WHEN ENRE.ENPR_Numero = 0 THEN                 
 --(CASE WHEN ENPD.Numero_Documento = 0 THEN ENPE.Numero_Documento ELSE ENPD.Numero_Documento END )                 
 IIF(ENRE.ENPD_Numero = 0,EPRE.Numero_Documento,ENPD.Numero_Documento)                
 ELSE EPRE.Numero_Documento END  AS NumeroDocumentoPlanilla          
        
 ,VALC.Campo1 as NovedadEntrega        
 ,VACHRE.Campo1 as HorarioEntrega        
 ,OFRE.Nombre AS OFIC_Nombre_Recibe        
    ,TEDE.Numero_Identificacion as  IdentificacionDestinatario   
	/*Modificación FP 2021/12/12*/
,ISNULL(ENRE.Nombre_Destinatario, SUBSTRING( ISNULL(TEDE.Razon_Social,'')+' '+ISNULL(TEDE.Nombre,'')+' '+ISNULL(TEDE.Apellido1,'')+' '+ISNULL(TEDE.Apellido2,''), 1, 20)) AS NombreDestinatario
/*Fin Modificación*/  
/*Modificación FP 2021/12/12*/                        
,ISNULL(ENRE.Email_Destinatario, SUBSTRING( TEDE.Correo_Facturacion , 1, 20)) as CorreoDestinatario  
 /*Fin Modificación*/
 ,ENRE.Codigo_Postal_Destinatario as CodigoPostalDestinatario        
        
        
        
 ,TEDE.Telefonos TelefonoDestinatario                                
 ,ISNULL(UEPT.Descripcion,UEPT2.Descripcion) UnidadEmpaque                                        
 ,ENRE.Documento_Cliente Remision                                         
 , ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'')                                          
 +' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS NombreCliente                                     
 ,CICL.Nombre as CiudadCliente                                       
 ,TECL.Numero_Identificacion IdentificacionCliente                                    
 , TECL.Direccion DireccionCliente                                        
 ,  TECL.Telefonos Telefonocliente                                      
 ,USCR.Nombre UsuarioCrea              
 ,TTTC.Nombre AS TipoTarifa              
 ,IIF(TLNC.Codigo IN(103,104,105,106), TTTC.Nombre, '') AS TamanoContenedor              
 ,ENRE.Numero_Contenedor                                
 ,ENRE.MBL_Contenedor                                
 ,ENRE.HBL_Contenedor                                
 ,isnull(PTDR.Nombre,PRTR.Nombre) AS ProductoDistribucion                               
 ,isnull(DDRE.Peso,ENRE.Peso_Transportador) AS pesoDistribucion                               
 ,isnull(DDRE.Cantidad,ENRE.Cantidad_Transportador) AS CantidadDistrubucion                              
 ,isnull(DDRE.Cantidad_Recibe,ENRE.Cantidad_Transportador) AS CantidadDistrubucionRecibe                              
 ,isnull(CIDD.Nombre,CIDE.Nombre) AS CiudadDistribucion                               
 ,isnull(DDRE.Direccion_Destinatario,ENRE.Direccion_Destinatario) AS Direccion_Destinatario                            
 ,OFIC.Nombre AS Oficina                            
 ,SICA.Nombre AS SitioCargue                            
 ,SIDE.Nombre AS SitioDescargue                            
 ,NAPT.Campo1 AS NaturalezaProducto                         
 ,TIDU.Campo1 AS TipoVinculacion                            
 ,ENOC.Numero_Documento AS OrdenCargue                            
 ,@Precientos as Precintos                            
 ,'' as USUA_Imprime                      
 ,SEMI.Placa AS Placa_SEMI             
 ,DOCURFAC.Numero_Documento_Gestion as Factura        
 ,DOCURFAC.Numero_Hojas as HojasFacturas        
 ,DOCURCAR.Numero_Documento_Gestion as CartaPorte        
 ,DOCURCAR.Numero_Hojas as HojasCarta        
        
                
 ,CASE                 
 WHEN ENRE.Anulado = 1 THEN 'ANULADO'                
 WHEN ENRE.Estado  = 1 THEN 'DEFINITIVO'                
 WHEN ENRE.Estado  = 0 THEN 'BORRADOR'END AS Estado                
                  
             
 FROM Encabezado_Remesas   ENRE                                 
 INNER JOIN  Empresas EMPR  ON                             
 ENRE.EMPR_Codigo = EMPR.Codigo               
 LEFT JOIN Encabezado_Planilla_Despachos As EPRE ON                
 EPRE.EMPR_Codigo = ENRE.EMPR_Codigo                
 AND EPRE.Numero = ENRE.ENPR_Numero             
        LEFT JOIN  Cumplido_Remesas AS CUMR  on        
 ENRE.EMPR_Codigo = CUMR.EMPR_Codigo and         
  ENRE.Numero  = CUMR.ENRE_Numero        
        
  LEFT JOIN   Remesa_Gestion_Documentos   as DOCURFAC on         
  ENRE.EMPR_Codigo = DOCURFAC.EMPR_Codigo        
  and  ENRE.Numero = DOCURFAC.ENRE_Numero         
  and   DOCURFAC.CATA_TDGC_Codigo = 22508        
        
        
    LEFT JOIN   Remesa_Gestion_Documentos   as DOCURCAR on         
  ENRE.EMPR_Codigo = DOCURCAR.EMPR_Codigo        
  and  ENRE.Numero = DOCURCAR.ENRE_Numero         
  and  DOCURCAR.CATA_TDGC_Codigo = 22504        
        
LEFT JOIN Valor_Catalogos AS VALC on         
 CUMR.EMPR_Codigo = VALC.EMPR_Codigo and         
   CUMR.CATA_NERP_Codigo =  VALC.Codigo        
        
        
  LEFT JOIN Encabezado_Planilla_Despachos As ENPD ON                
 ENPD.EMPR_Codigo = ENRE.EMPR_Codigo                
 AND ENPD.Numero = ENRE.ENPD_Numero          
        
 LEFT JOIN Encabezado_Orden_Cargues AS ENOC                            
 ON ENRE.EMPR_Codigo = ENOC.EMPR_Codigo                            
 AND ENRE.ENOC_Numero = ENOC.Numero                            
                           
 LEFT JOIN Remesas_Paqueteria REMEPA         
 ON ENRE.EMPR_Codigo = REMEPA.EMPR_Codigo                            
 AND ENRE.Numero = REMEPA.ENRE_Numero            
        
                 
 LEFT JOIN Oficinas As OFRE ON                
 REMEPA.EMPR_Codigo = OFRE.EMPR_Codigo                
 AND REMEPA.OFIC_Codigo_Recibe = OFRE.Codigo          
            
LEFT JOIN Valor_Catalogos AS VACHRE on         
REMEPA.EMPR_Codigo  = VACHRE.EMPR_Codigo          
and REMEPA.CATA_HERP_Codigo = VACHRE.Codigo        
          
        
 LEFT JOIN Terceros TECO  ON                                       
 ENRE.EMPR_Codigo = TECO.EMPR_Codigo                                        
 AND ENRE.TERC_Codigo_Conductor = TECO.Codigo            
           
 LEFT JOIN Terceros TERE  ON                                       
 ENRE.EMPR_Codigo = TERE.EMPR_Codigo                                        
 AND ENRE.TERC_Codigo_Remitente = TERE.Codigo            
         
        
                                    
 LEFT JOIN Ciudades CIRE  ON                                      
 TERE.EMPR_Codigo = CIRE.EMPR_Codigo                                        
 AND TERE.CIUD_Codigo = CIRE.Codigo                                        
                                    
 INNER JOIN Usuarios USCR ON                                 
 ENRE.EMPR_Codigo = USCR.EMPR_Codigo                                
 AND ENRE.USUA_Codigo_Crea = USCR.Codigo                                    
                                    
 LEFT JOIN Terceros TEDE  ON                                      
 ENRE.EMPR_Codigo = TEDE.EMPR_Codigo                                        
 AND ENRE.TERC_Codigo_Destinatario = TEDE.Codigo             
           
 LEFT JOIN Ciudades CIOR  ON                                      
 ENRE.EMPR_Codigo = CIOR.EMPR_Codigo                                        
 AND ENRE.CIUD_Codigo_Remitente = CIOR.Codigo               
           
 LEFT JOIN Ciudades CIDE  ON             
 ENRE.EMPR_Codigo = CIDE.EMPR_Codigo                                        
 AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo               
           
 LEFT JOIN Departamentos DEPA on  CIDE.DEPA_Codigo = DEPA.Codigo          
           
                                      
 LEFT JOIN Oficinas OFIC  ON                                      
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                       
 AND ENRE.OFIC_Codigo = OFIC.Codigo                                  
                                       
 LEFT JOIN Vehiculos VEHI  ON                                       ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                                        
 AND ENRE.VEHI_Codigo = VEHI.Codigo                                 
                            
 LEFT JOIN Valor_Catalogos TIDU ON                            
 VEHI.EMPR_Codigo = TIDU.EMPR_Codigo                   
 AND VEHI.CATA_TIDV_Codigo = TIDU.Codigo                            
                                
 LEFT JOIN Producto_Transportados PRTR  ON                                 
 ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                         
 AND ENRE.PRTR_Codigo = PRTR.Codigo              
                  
 LEFT JOIN Valor_Catalogos NAPT  ON                                      
 NAPT.EMPR_Codigo = PRTR.EMPR_Codigo                                         
 AND NAPT.Codigo = PRTR.CATA_NAPT_Codigo                                   
                            
                        
 LEFT JOIN Terceros TECL  ON                                      
 ENRE.EMPR_Codigo = TECL.EMPR_Codigo                                        
 AND ENRE.TERC_Codigo_Cliente = TECL.Codigo            
          
                                    
 LEFT JOIN Ciudades CICL  ON                    
 TECL.EMPR_Codigo = CICL.EMPR_Codigo                                    
 AND TECL.CIUD_Codigo = CICL.Codigo                                   
                                
  LEFT  JOIN Encabezado_Solicitud_Orden_Servicios ESOS ON                                 
 ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                                 
 AND ENRE.ESOS_Numero = ESOS.Numero                                
                        
 LEFT JOIN Unidad_Empaque_Producto_Transportados UEPT  ON                                      
 ESOS.EMPR_Codigo = UEPT.EMPR_Codigo                  AND ESOS.UEPT_Codigo = UEPT.Codigo                                        
                        
 LEFT JOIN Unidad_Empaque_Producto_Transportados UEPT2  ON                                      
 PRTR.EMPR_Codigo = UEPT.EMPR_Codigo                                        
 AND PRTR.UEPT_Codigo = UEPT.Codigo                           
                        
 left  JOIN Detalle_Despacho_Orden_Servicios DSOS ON                                 
 ENRE.EMPR_Codigo = DSOS.EMPR_Codigo                                 
 AND ENRE.Numero = DSOS.ENRE_Numero           
           
 /**select * from Detalle_Despacho_Orden_Servicios where EMPR_Codigo = 10 and ENRE_Numero = 1082591*-**/          
               
 LEFT JOIN Importacion_Exportacion_Solicitud_Orden_Servicios IEOS ON              
 ESOS.EMPR_Codigo = IEOS.EMPR_Codigo                                 
 AND ESOS.Numero = IEOS.ESOS_Numero              
                            
 LEFT JOIN Sitios_Cargue_Descargue AS SICA ON                            
 ESOS.EMPR_Codigo = SICA.EMPR_Codigo                            
 AND ESOS.SICD_Codigo_Cargue = SICA.Codigo                            
                            
 LEFT JOIN Sitios_Cargue_Descargue AS SIDE ON                            
 ESOS.EMPR_Codigo = SIDE.EMPR_Codigo                            
 AND ESOS.SICD_Codigo_Descargue = SIDE.Codigo                            
                            
 LEFT JOIN V_Tipo_Tarifa_Transporte_Carga TTTC ON                                     
 DSOS.EMPR_Codigo = TTTC.EMPR_Codigo                                     
 AND DSOS.TTTC_Codigo_Venta = TTTC.Codigo                                     
 AND DSOS.TATC_Codigo_Venta = TTTC.TATC_Codigo              
              
 LEFT JOIN Tipo_Linea_Negocio_Carga TLNC ON                                     
 ESOS.EMPR_Codigo = TLNC.EMPR_Codigo                                     
 AND ESOS.TLNC_Codigo = TLNC.Codigo                                     
                                         
 LEFT JOIN Detalle_Distribucion_Remesas DDRE ON                               
 ENRE.EMPR_Codigo = DDRE.EMPR_Codigo                               
 AND ENRE.Numero = DDRE.ENRE_Numero                              
                              
 LEFT JOIN Ciudades CIDD ON                       
 DDRE.EMPR_Codigo = CIDD.EMPR_Codigo                               
 AND DDRE.CIUD_Codigo_Destinatario = CIDD.Codigo                              
                              
 LEFT JOIN Producto_Transportados PTDR ON                               
 DDRE.EMPR_Codigo = PTDR.EMPR_Codigo                               
 AND DDRE.PRTR_Codigo = PTDR.Codigo                              
                              
 LEFT JOIN Semirremolques SEMI  ON                     
 ENRE.EMPR_Codigo = SEMI.EMPR_Codigo                    
 AND ENRE.SEMI_Codigo = SEMI.Codigo                    
                    
 WHERE                                    
 ENRE.EMPR_Codigo = @par_EMPR_Codigo                                      
 AND ENRE.Numero = @par_Numero_Documento                  
                   
END 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_Reporte_Remesa]      
(                        
 @par_EMPR_Codigo numeric                                 
 , @par_Numero_Documento numeric              
 , @par_Usuario numeric              
)                                
AS BEGIN      
 declare @Precientos varchar(max)          
 select @Precientos= CONCAT(@Precientos,CONVERT(VARCHAR(50),DAPO.Numero_Precinto) + ' ' +TPRE.Campo1+ ' ,')                  
 from                     
                    
 Detalle_Asignacion_Precintos_Oficina as DAPO                    
 inner join Encabezado_Asignacion_Precintos_Oficina as EAPO                    
 ON DAPO.EMPR_Codigo = EAPO.EMPR_Codigo                    
 AND DAPO.EAPO_Numero = EAPO.Numero                    
                    
 INNER JOIN Valor_Catalogos AS TPRE      
 ON EAPO.EMPR_Codigo = TPRE.EMPR_Codigo      
 AND EAPO.CATA_TPRE_Codigo = TPRE.Codigo      
                    
 WHERE DAPO.EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_Numero_Documento      
 select @Precientos = SUBSTRING (@Precientos,1,len(@Precientos)-1)      
      
 SELECT DISTINCT                                
 EMPR.Nombre_Razon_Social                                 
 , EMPR.Numero_Identificacion NIT                                
 , EMPR.Direccion                                 
 , EMPR.Telefonos TelefonoEmpresa                                
 , ENRE.Numero_Documento AS Remesa                                
 , CIOR.Nombre AS CiudadOrigen                                
 , CIDE.Nombre AS CiudadDestino                         
 , ENRE.Numero_Contenedor                        
 ,ESOS.Numero_Documento AS NumeroOrdenServicio      
 ,ESOS.TLNC_Codigo      
 ,IEOS.Declaracion_Importacion      
 ,IEOS.Fecha_Vencimiento AS IEOS_Fecha_Vencimiento      
 , ENRE.FECHA
 /*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(TERE.Razon_Social,'')+' '+ISNULL(TERE.Nombre,'')+' '+ISNULL(TERE.Apellido1,'')+' '+ISNULL(TERE.Apellido2,'')) AS NombreRemitente
	/*Fin Modificación*/                                 
 ,TERE.Numero_Identificacion IdentificacionRemitente                             
 , CIRE.Nombre AS Ciudad_remitente                            
 ,TERE.Telefonos TelefonoRemitente                          
 , TERE.Direccion AS Direccion_Remitente                              
 , ENRE.Direccion_Destinatario                                
 ,  EMPR.Telefonos                                
 , ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')                                  
 +' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS NombreConductor                                
 , EMPR.Telefonos TelefonoConductor                                 
 , TECO.Numero_Identificacion CedulaConductor                                 
 , ENRE.Observaciones                                 
 , VEHI.Placa                                
 , ENRE.Cantidad_Cliente                                
 --                                
 ,ENRE.Peso_Cliente                                 
 ,PRTR.Nombre AS NombreProducto 
 /*Modificación FP 2021/12/12*/
,ISNULL(ENRE.Nombre_Destinatario, ISNULL(TEDE.Razon_Social,'')+' '+ISNULL(TEDE.Nombre,'')+' '+ISNULL(TEDE.Apellido1,'')+' '+ISNULL(TEDE.Apellido2,'')) AS NombreDestinatario
/*Fin Modificación*/                                 
 ,TEDE.Telefonos TelefonoDestinatario                        
 ,ISNULL(UEPT.Descripcion,UEPT2.Descripcion) UnidadEmpaque                                
 ,ENRE.Documento_Cliente Remision                                 
 , ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'')                                  
 +' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS NombreCliente                             
 ,CICL.Nombre as CiudadCliente                               
 ,TECL.Numero_Identificacion IdentificacionCliente                            
 , TECL.Direccion DireccionCliente                                
 ,  TECL.Telefonos Telefonocliente                              
 ,USCR.Nombre UsuarioCrea      
 ,TTTC.Nombre AS TipoTarifa      
 ,IIF(TLNC.Codigo IN(103,104,105,106), TTTC.Nombre, '') AS TamanoContenedor      
 ,ENRE.Numero_Contenedor                        
 ,ENRE.MBL_Contenedor                   
 ,ENRE.HBL_Contenedor                        
 ,isnull(PTDR.Nombre,PRTR.Nombre) AS ProductoDistribucion                       
 ,isnull(DDRE.Peso,ENRE.Peso_Transportador) AS pesoDistribucion                       
 ,isnull(DDRE.Cantidad,ENRE.Cantidad_Transportador) AS CantidadDistrubucion                      
 ,isnull(DDRE.Cantidad_Recibe,ENRE.Cantidad_Transportador) AS CantidadDistrubucionRecibe                                       
 ,isnull(CIDD.Nombre,CIDE.Nombre) AS CiudadDistribucion                       
 ,isnull(DDRE.Direccion_Destinatario,ENRE.Direccion_Destinatario) AS Direccion_Destinatario                    
 ,OFIC.Nombre AS Oficina                    
 ,SICA.Nombre AS SitioCargue                    
 ,SIDE.Nombre AS SitioDescargue                    
 ,NAPT.Campo1 AS NaturalezaProducto                 
 ,TIDU.Campo1 AS TipoVinculacion                    
 ,ENOC.Numero_Documento AS OrdenCargue                    
 ,@Precientos as Precintos                    
 ,(Select  Nombre FROM Usuarios where EMPR_Codigo = @par_EMPR_Codigo and Codigo = @par_Usuario) as USUA_Imprime              
 ,SEMI.Placa AS Placa_SEMI            
        
 ,CASE         
 WHEN ENRE.Anulado = 1 THEN 'ANULADO'        
 WHEN ENRE.Estado  = 1 THEN 'DEFINITIVO'        
 WHEN ENRE.Estado  = 0 THEN 'BORRADOR'END AS Estado        
          
            
 FROM Encabezado_Remesas   ENRE                                
 INNER JOIN  Empresas EMPR  ON                     
 ENRE.EMPR_Codigo = EMPR.Codigo                       
                    
 LEFT JOIN Encabezado_Orden_Cargues AS ENOC                    
 ON ENRE.EMPR_Codigo = ENOC.EMPR_Codigo                    
 AND ENRE.ENOC_Numero = ENOC.Numero                    
                    
 LEFT JOIN Terceros TECO  ON                               
 ENRE.EMPR_Codigo = TECO.EMPR_Codigo                                
 AND ENRE.TERC_Codigo_Conductor = TECO.Codigo                                 
                                
                            
                            
 INNER JOIN Usuarios USCR ON                         
 ENRE.EMPR_Codigo = USCR.EMPR_Codigo                        
 AND ENRE.USUA_Codigo_Crea = USCR.Codigo                            
                            
 LEFT JOIN Terceros TEDE  ON                              
 ENRE.EMPR_Codigo = TEDE.EMPR_Codigo                                
 AND ENRE.TERC_Codigo_Destinatario = TEDE.Codigo       
     
 LEFT JOIN Encabezado_Planilla_Despachos ENPD ON    
 ENRE.EMPR_Codigo = ENPD.EMPR_Codigo AND    
 ENRE.ENPD_Numero = ENPD.Numero    
    
      
 LEFT JOIN Rutas RUTA ON    
 ENRE.EMPR_Codigo = RUTA.EMPR_Codigo AND    
 ENPD.RUTA_Codigo = RUTA.Codigo    
     
 LEFT JOIN Ciudades CIOR  ON                              
 ENRE.EMPR_Codigo = CIOR.EMPR_Codigo                                
 AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo        
     
 LEFT JOIN Ciudades CIDE  ON                              
 ENRE.EMPR_Codigo = CIDE.EMPR_Codigo                                
 AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo                                
                              
 LEFT JOIN Oficinas OFIC  ON                              
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                               
 AND ENRE.OFIC_Codigo = OFIC.Codigo                          
                               
 LEFT JOIN Vehiculos VEHI  ON                              
 ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                                
 AND ENRE.VEHI_Codigo = VEHI.Codigo                         
                    
 LEFT JOIN Valor_Catalogos TIDU ON                    
 VEHI.EMPR_Codigo = TIDU.EMPR_Codigo                    
 AND VEHI.CATA_TIDV_Codigo = TIDU.Codigo                    
                        
 LEFT JOIN Producto_Transportados PRTR  ON                              
 ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                 
 AND ENRE.PRTR_Codigo = PRTR.Codigo                                
                    
 LEFT JOIN Valor_Catalogos NAPT  ON                              
 NAPT.EMPR_Codigo = PRTR.EMPR_Codigo                                 
 AND NAPT.Codigo = PRTR.CATA_NAPT_Codigo                           
                    
                
 LEFT JOIN Terceros TECL  ON                              
 ENRE.EMPR_Codigo = TECL.EMPR_Codigo                                
 AND ENRE.TERC_Codigo_Cliente = TECL.Codigo                                 
                            
 LEFT JOIN Ciudades CICL  ON            
 TECL.EMPR_Codigo = CICL.EMPR_Codigo                                
 AND TECL.CIUD_Codigo = CICL.Codigo                           
                        
  LEFT  JOIN Encabezado_Solicitud_Orden_Servicios ESOS ON                         
 ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                         
 AND ENRE.ESOS_Numero = ESOS.Numero                        
  
  LEFT JOIN Terceros TERE  ON                               
 ENRE.EMPR_Codigo = TERE.EMPR_Codigo                                
 AND ESOS.TERC_Codigo_Remitente = TERE.Codigo   
                            
 LEFT JOIN Ciudades CIRE  ON                              
 TERE.EMPR_Codigo = CIRE.EMPR_Codigo                                
 AND TERE.CIUD_Codigo = CIRE.Codigo       
                
 LEFT JOIN Unidad_Empaque_Producto_Transportados UEPT  ON                              
 ESOS.EMPR_Codigo = UEPT.EMPR_Codigo               
 AND ESOS.UEPT_Codigo = UEPT.Codigo                                
                
 LEFT JOIN Unidad_Empaque_Producto_Transportados UEPT2  ON                              
 PRTR.EMPR_Codigo = UEPT.EMPR_Codigo                                
 AND PRTR.UEPT_Codigo = UEPT.Codigo                   
                
 INNER JOIN Detalle_Despacho_Orden_Servicios DSOS ON                         
 ENRE.EMPR_Codigo = DSOS.EMPR_Codigo                         
 AND ENRE.Numero = DSOS.ENRE_Numero      
       
 LEFT JOIN Importacion_Exportacion_Solicitud_Orden_Servicios IEOS ON      
 ESOS.EMPR_Codigo = IEOS.EMPR_Codigo                         
 AND ESOS.Numero = IEOS.ESOS_Numero      
                    
 LEFT JOIN Sitios_Cargue_Descargue AS SICA ON                    
 ESOS.EMPR_Codigo = SICA.EMPR_Codigo                    
 AND ESOS.SICD_Codigo_Cargue = SICA.Codigo                    
                    
 LEFT JOIN Sitios_Cargue_Descargue AS SIDE ON                    
 ESOS.EMPR_Codigo = SIDE.EMPR_Codigo                    
 AND ESOS.SICD_Codigo_Descargue = SIDE.Codigo                    
                    
 LEFT JOIN V_Tipo_Tarifa_Transporte_Carga TTTC ON                             
 DSOS.EMPR_Codigo = TTTC.EMPR_Codigo                             
 AND DSOS.TTTC_Codigo_Venta = TTTC.Codigo                             
 AND DSOS.TATC_Codigo_Venta = TTTC.TATC_Codigo      
      
 LEFT JOIN Tipo_Linea_Negocio_Carga TLNC ON                             
 ESOS.EMPR_Codigo = TLNC.EMPR_Codigo                             
 AND ESOS.TLNC_Codigo = TLNC.Codigo                             
                                 
 LEFT JOIN Detalle_Distribucion_Remesas DDRE ON                       
 ENRE.EMPR_Codigo = DDRE.EMPR_Codigo                       
 AND ENRE.Numero = DDRE.ENRE_Numero                      
                      
 LEFT JOIN Ciudades CIDD ON                       
 DDRE.EMPR_Codigo = CIDD.EMPR_Codigo                       
 AND DDRE.CIUD_Codigo_Destinatario = CIDD.Codigo                      
                      
 LEFT JOIN Producto_Transportados PTDR ON                       
 DDRE.EMPR_Codigo = PTDR.EMPR_Codigo                       
 AND DDRE.PRTR_Codigo = PTDR.Codigo                      
                      
 LEFT JOIN Semirremolques SEMI  ON             
 ENRE.EMPR_Codigo = SEMI.EMPR_Codigo            
 AND ENRE.SEMI_Codigo = SEMI.Codigo            
            
 WHERE                               
 ENRE.EMPR_Codigo = @par_EMPR_Codigo                              
 AND ENRE.Numero = @par_Numero_Documento          
 AND ENRE.TIDO_Codigo= 100          
END      
GO

CREATE OR ALTER  PROCEDURE [dbo].[gsp_reporte_zonificacion_guias]            
(                                                                            
@par_EMPR_Codigo SMALLINT,                                                                            
@par_Numero numeric = NULL,                                                                            
@par_Planilla  numeric = NULL,                                                                            
@par_FechaInicial Date = NULL,           
@par_FechaFinal Date = NULL,          
@par_ListaRemesas VARCHAR(100) = null          
)                                                                            
AS                                                                          
BEGIN              
 SELECT  ENRE.EMPR_Codigo          
 ,ENRP.ZOCI_Codigo_Entrega        
 ,ZONC.Nombre as Zona          
 ,ENPD.Numero_Documento as NumeroDocumentoPlanilla          
 ,ENRE.Fecha          
 ,ENRP.CATA_ESRP_Codigo,ENRE.Estado            
 ,ENRE.Numero                                                                            
 ,ENRE.Numero_Documento as Numero_Documento             
 ,ENRP.CIUD_Codigo_Origen                                                                            
 ,ENRP.CIUD_Codigo_Destino                                                                            
 ,CIOR.Nombre AS CiudadOrigen                                                                            
 ,CIDE.Nombre AS CiudadDestino           
 ,ENRE.Direccion_Destinatario 
 /*Modificación FP 2021/12/12*/
,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente
,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario
/*Fin Modificación*/ 
 ,ENRE.CATA_FOPR_Codigo                                                             
 ,FOPR.Campo1 AS FormaPago          
 ,ENRE.CATA_TIRE_Codigo          
 ,ENRE.Peso_Cliente                                   
 ,ENRP.Peso_Volumetrico                                  
 ,ENRP.Peso_A_Cobrar              
 ,ENRE.Cantidad_Cliente            
 ,ENRE.Total_Flete_Cliente         
 ,LNPA.Campo1 AS CATA_LNPA_Nombre          
 
 FROM Remesas_Paqueteria AS ENRP              
              
 INNER JOIN Encabezado_Remesas AS ENRE                                            
 ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                                                
 AND ENRP.ENRE_Numero = ENRE.Numero 
 
 LEFT JOIN Terceros AS CLIE                                                 
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                                
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                                
                                                                                
 LEFT JOIN Terceros AS REMI                                                                                
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                                
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                                
                                               
 LEFT JOIN Terceros AS DEST                                                                                
 ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                                
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                                                
                  
 LEFT JOIN Producto_Transportados AS PRTR                       
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                            
 AND ENRE.PRTR_Codigo = PRTR.Codigo                                                       
                               
 LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                                                
 ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                                      
 AND ENRE.ENPD_Numero = ENPD.Numero                                       
 AND ENPD.TIDO_Codigo IN(130,135)--Planilla Despacho Guias, Planilla Paqueteria                      
-- AND ENRE.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENRE.Fecha)                                                                       
 --AND ENRE.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENRE.Fecha)                         
                       
 LEFT JOIN Vehiculos AS VEHI                      
 ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                      
 AND ENRE.VEHI_Codigo = VEHI.Codigo                                                     
                                                                    
 LEFT JOIN Ciudades CIOR                                                                                
 ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                                
 AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                                
                                                                      
 LEFT JOIN Ciudades CIDE                                                                                
 ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                    
 AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo  
 
 LEFT JOIN Valor_Catalogos AS FOPR ON                                                        
 ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                                        
 AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                
                 
 LEFT JOIN Valor_Catalogos AS LNPA ON                                                        
 ENRP.EMPR_Codigo = LNPA.EMPR_Codigo                                                        
 AND ENRP.CATA_LNPA_Codigo = LNPA.Codigo                          
                                
 LEFT JOIN Encabezado_Facturas ENFA ON                              
 ENRE.EMPR_Codigo = ENFA.EMPR_Codigo AND                              
 ENRE.ENFA_Numero = ENFA.Numero           
     
 LEFT JOIN Zona_Ciudades AS ZONC                   
 ON ENRP.EMPR_Codigo = ZONC.EMPR_Codigo                                              
 AND ENRP.ZOCI_Codigo_Entrega = ZONC.Codigo  
 
WHERE               
ENRE.EMPR_Codigo =   @par_EMPR_Codigo              
AND ENRP.CATA_ESRP_Codigo  in (6005,6010)              
AND ENRE.Estado  = 1              
AND ENRE.Numero IN (SELECT * FROM STRING_SPLIT(@par_ListaRemesas, ',')) 
AND ENRP.ZOCI_Codigo_Entrega <> 0        
END        
GO
