USE [ENCOEXPRES]
GO

/****** Object: StoredProcedure [dbo].[gsp_consultar_remesas_pendientes_facturar] Script Date: 13/02/2022 10:42:14 p.�m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_remesas_pendientes_facturar]
(
@par_EMPR_Codigo SMALLINT,
@par_Numero_Documento NUMERIC = NULL,
@par_TIDO_Codigo NUMERIC = NULL,
@par_Fecha_Inicial DATETIME = NULL,
@par_Fecha_Final DATETIME = NULL,
@par_CLIE_Codigo NUMERIC = NULL,
@par_Documento_Cliente VARCHAR(30) = NULL,
@par_ENPD_Numero_Documento NUMERIC = NULL,
@par_ESOS_Numero_Documento NUMERIC = NULL,
@par_TEDI_Codigo INT = NULL,
@par_NumeroPagina INT = NULL,
@par_RegistrosPagina INT = NULL ,
@par_Facturar_A NUMERIC =NULL,
@par_CIUD_Origen NUMERIC = NULL,
@par_CIUD_Destino NUMERIC = NULL,
@par_PRTR_Codigo NUMERIC = NULL,
@par_OFIC_Codigo NUMERIC = NULL,
@par_CATA_ESRP_Codigo NUMERIC = NULL
)
AS
BEGIN
Declare @Remesa Numeric = 100;
Declare @RemesaPaqueteria Numeric = 110;
Declare @RemesaOtrasEmpresas Numeric = 118;
Declare @RemesaProveedores Numeric = 112;
DECLARE @TableTipoRemesas TABLE (Codigo NUMERIC)

IF (@par_TIDO_Codigo IS NULL) 
BEGIN 
INSERT @TableTipoRemesas (Codigo) values (@Remesa), (@RemesaPaqueteria), (@RemesaOtrasEmpresas), (@RemesaProveedores) 
END 
ELSE 
BEGIN 
INSERT @TableTipoRemesas (Codigo) values (@par_TIDO_Codigo) 
END

SELECT
ENRE.EMPR_Codigo,
ENRE.Numero,
ENRE.Numero_Documento,
ENRE.TIDO_Codigo,
ENRE.TERC_Codigo_Cliente,
ISNULL(ENRE.Documento_Cliente,'') AS Documento_Cliente,
ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'') As NombreCliente,
ENRE.Fecha,
ENRE.RUTA_Codigo,
RUTA.Nombre AS NombreRuta,
ENRE.PRTR_Codigo,
PRTR.Nombre AS NombreProducto,
ENRE.CATA_FOPR_Codigo,
ENRE.TERC_Codigo_Remitente,
ENRE.Observaciones,
ENRE.Cantidad_Cliente,
ENRE.Peso_Cliente,
ENRE.Peso_Volumetrico_Cliente,
ENRE.Valor_Flete_Cliente,
ENRE.Total_Flete_Cliente,
ENRE.Valor_FOB,
ENRE.Valor_Manejo_Cliente,
ENRE.Valor_Seguro_Cliente,
ENRE.Valor_Descuento_Cliente,
ENRE.Valor_Comercial_Cliente,
ENRE.Cantidad_Transportador,
ENRE.Peso_Transportador,
ENRE.Valor_Flete_Transportador,
ENRE.Total_Flete_Transportador,
ENRE.ETCC_Numero,
ENRE.ETCV_Numero,
ENRE.ESOS_Numero,
ESOS.Numero_Documento AS OrdenServicio,
ISNULL(ESOS.Facturar_Despachar, 0) AS Facturar_Despachar,
ISNULL(ESOS.Facturar_Cantidad_Peso_Cumplido, 0) AS Facturar_Cantidad_Peso_Cumplido,
ISNULL(CURE.Peso_Descargue, 0) AS Peso_Descargue,
ENRE.ENPD_Numero AS NumeroPlanilla,
ISNULL(ENPD.Numero_Documento,0) AS NumeroDocumentoPlanilla,
ENRE.ENMC_Numero AS NumeroManifiesto,
ISNULL(ENMC.Numero_Documento,0) AS NumeroDocumentoManifiesto,
ISNULL(ENRE.ECPD_Numero,0) AS ECPD_Numero,
ISNULL(ECPD.Numero_Documento,0) AS NumeroCumplido,
ENRE.VEHI_Codigo,
ISNULL(PRTR.UEPT_Codigo, 0) AS Unidad_Empaque,
ISNULL(ENRE.Distribucion,0) AS Distribucion,
ISNULL(TEDI.Nombre, '') As NombreSede,
VEHI.Placa,
IIF(ENRE.TIDO_Codigo = @RemesaPaqueteria, CIOP.Nombre , CIOR.Nombre) as CiudadRemitente,
IIF(ENRE.TIDO_Codigo = @RemesaPaqueteria, CIDP.Nombre , CIDE.Nombre) as CiudadDestinatario,
ENRE.DT_SAP,
ENRE.Entrega_SAP,
ENRE.TERC_Codigo_Facturara,
RTRIM(LTRIM(CONCAT(FACA.Razon_Social,' ',FACA.Nombre,' ',FACA.Apellido1,' ',FACA.Apellido2))) AS NombreFacturarA,
CASE ENRE.TIDO_Codigo
When @Remesa Then TIDE.Campo1
When @RemesaPaqueteria Then 'Paqueter�a'
When @RemesaOtrasEmpresas Then 'Manifiesto Otras Empresas'
When @RemesaProveedores Then 'Planilla Proveedores'
END AS TipoDespacho,
ESOS.CATA_TDOS_Codigo,
--FP 2022 02 13 a la fecha solo remesas paq manejan estado por catalogo
CASE WHEN @par_TIDO_Codigo = 110 THEN ISNULL(REPE.CATA_ESRP_Codigo,0) END as Estado

FROM
Encabezado_Remesas ENRE

LEFT JOIN Rutas RUTA
ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo
AND ENRE.RUTA_Codigo = RUTA.Codigo

LEFT JOIN Producto_Transportados PRTR
ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
AND ENRE.PRTR_Codigo = PRTR.Codigo

LEFT JOIN Terceros CLIE
ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo

LEFT JOIN Terceros FACA
ON ENRE.EMPR_Codigo = FACA.EMPR_Codigo
AND ENRE.TERC_Codigo_Facturara = FACA.Codigo

LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS
ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo
AND ENRE.ESOS_Numero = ESOS.Numero

LEFT JOIN Encabezado_Facturas ENFA
ON ENRE.EMPR_Codigo = ENFA.EMPR_Codigo
AND ENRE.ENFA_Numero = ENFA.Numero

LEFT JOIN Encabezado_Planilla_Despachos ENPD
ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo
AND ENRE.ENPD_Numero = ENPD.Numero

LEFT JOIN Encabezado_Manifiesto_Carga ENMC
ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo
AND ENRE.ENMC_Numero = ENMC.Numero

LEFT JOIN Encabezado_Cumplido_Planilla_Despachos ECPD
ON ENRE.EMPR_Codigo = ECPD.EMPR_Codigo
AND ENRE.ECPD_Numero = ECPD.Numero

LEFT JOIN Cumplido_Remesas CURE
ON ENRE.EMPR_Codigo = CURE.EMPR_Codigo
AND ENRE.Numero = CURE.ENRE_Numero

LEFT JOIN Detalle_Despacho_Orden_Servicios AS DDOS
ON ENRE.EMPR_Codigo = DDOS.EMPR_Codigo
AND ENRE.Numero = DDOS.ENRE_Numero

LEFT JOIN Tercero_Direcciones As TEDI
ON ENRE.EMPR_Codigo = TEDI.EMPR_Codigo
AND ENRE.TEDI_Codigo = TEDI.Codigo

LEFT JOIN Vehiculos AS VEHI
ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo
AND ENRE.VEHI_Codigo = VEHI.Codigo

LEFT JOIN Ciudades as CIOR
ON CIOR.EMPR_Codigo = RUTA.EMPR_Codigo
AND CIOR.Codigo = RUTA.CIUD_Codigo_Origen

LEFT JOIN Ciudades as CIDE
ON CIDE.EMPR_Codigo = RUTA.EMPR_Codigo
AND CIDE.Codigo = RUTA.CIUD_Codigo_Destino

LEFT JOIN Ciudades as CIOP
ON CIOP.EMPR_Codigo = ENRE.EMPR_Codigo
AND CIOP.Codigo = ENRE.CIUD_Codigo_Remitente

LEFT JOIN Ciudades as CIDP
ON CIDP.EMPR_Codigo = ENRE.EMPR_Codigo
AND CIDP.Codigo = ENRE.CIUD_Codigo_Destinatario

LEFT JOIN Valor_Catalogos AS TIDE
ON ESOS.EMPR_Codigo = TIDE.EMPR_Codigo
AND ESOS.CATA_TDOS_Codigo = TIDE.Codigo

LEFT JOIN Remesas_Paqueteria AS REPE
ON REPE.EMPR_Codigo = ENRE.EMPR_Codigo
AND REPE.ENRE_Numero = ENRE.Numero

INNER JOIN (SELECT Codigo FROM @TableTipoRemesas) TDRE ON
TDRE.Codigo = ENRE.TIDO_Codigo

WHERE
ENRE.EMPR_Codigo = @par_EMPR_Codigo
AND ENRE.Estado = 1
AND ENRE.Anulado = 0
AND ENRE.Numero_Documento = ISNULL(@par_Numero_Documento, ENRE.Numero_Documento)
AND ENRE.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ENRE.Fecha) AND ISNULL(@par_Fecha_Final, ENRE.Fecha)
AND (ESOS.Numero_Documento = @par_ESOS_Numero_Documento OR @par_ESOS_Numero_Documento IS NULL)
-- AND (ESOS.TERC_Codigo_Facturar = @par_Facturar_A OR @par_Facturar_A IS NULL)
AND (ENRE.TERC_Codigo_Facturara = @par_Facturar_A OR @par_Facturar_A IS NULL)
AND (ENPD.Numero_Documento = @par_ENPD_Numero_Documento OR @par_ENPD_Numero_Documento IS NULL)
AND ((ISNULL(ENRE.Cumplido, 0) = 1) OR (ESOS.Facturar_Despachar = 1 ))
AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_CLIE_Codigo, ENRE.TERC_Codigo_Cliente)
AND (ENRE.Documento_Cliente LIKE'%'+@par_Documento_Cliente+'%' OR @par_Documento_Cliente IS NULL)
AND (ENRE.ENFA_Numero = 0 OR ENRE.ENFA_Numero IS NULL)
AND CLIE.Numero_Identificacion = ISNULL(@par_Documento_Cliente,CLIE.Numero_Identificacion)
AND (ENRE.TEDI_Codigo = @par_TEDI_Codigo OR @par_TEDI_Codigo IS NULL)
AND (ENRE.Remesa_Cortesia <> 1 or ENRE.Remesa_Cortesia IS NULL)
AND (@par_CIUD_Origen = (CASE WHEN @par_TIDO_Codigo <> 110 THEN CIOR.Codigo ELSE ENRE.CIUD_Codigo_Remitente END)
OR @par_CIUD_Origen IS NULL) --Filtro Origen Para Masivo (tido <> 110)
AND (@par_CIUD_Destino = (CASE WHEN @par_TIDO_Codigo <> 110 THEN CIDE.Codigo ELSE ENRE.CIUD_Codigo_Destinatario END)
OR @par_CIUD_Destino IS NULL) --Filtro Destino Para Masivo (tido <> 110)
AND (ENRE.PRTR_Codigo = @par_PRTR_Codigo OR @par_PRTR_Codigo IS NULL)
AND (@par_OFIC_Codigo = (CASE WHEN (@par_TIDO_Codigo <> 110 OR REPE.OFIC_Codigo_Registro_Manual IS NULL) THEN ENRE.OFIC_Codigo ELSE REPE.OFIC_Codigo_Registro_Manual END)
OR @par_OFIC_Codigo IS NULL) --Filtro oficina registro con caso manual
AND (@par_TIDO_Codigo <> 110 OR REPE.CATA_ESRP_Codigo = @par_CATA_ESRP_Codigo OR @par_CATA_ESRP_Codigo IS NULL) --Filtro estado remesa
ORDER BY ENRE.Numero

END
GO


