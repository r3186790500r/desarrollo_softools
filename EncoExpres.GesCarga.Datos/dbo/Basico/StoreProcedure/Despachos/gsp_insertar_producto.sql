﻿PRINT 'gsp_insertar_producto'
go
DROP PROCEDURE gsp_insertar_producto
GO
create procedure gsp_insertar_producto
(
@par_EMPR_Codigo smallint
           ,@par_Codigo_Alterno varchar(20) = null
           ,@par_Nombre varchar(50) = null
           ,@par_Descripcion varchar(250) = null
           ,@par_Estado smallint = null
           ,@par_USUA_Codigo_Crea smallint = null
)
as
begin

INSERT INTO Producto_Transportados
           (EMPR_Codigo
           ,Codigo_Alterno
           ,Nombre
           ,Descripcion
           ,Estado
           ,Fecha_Crea
           ,USUA_Codigo_Crea
           ,Fecha_Modifica
           ,USUA_Codigo_Modifica)
     VALUES
           (@par_EMPR_Codigo 
           ,ISNULL(@par_Codigo_Alterno,'')
           ,ISNULL(@par_Nombre ,'')
           ,ISNULL(@par_Descripcion ,'')
           ,ISNULL(@par_Estado ,1)
           ,GETDATE()
           ,@par_USUA_Codigo_Crea 
           ,NULL
           ,NULL)

		   SELECT @@IDENTITY AS Codigo

end
go