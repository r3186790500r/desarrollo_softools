﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos

Namespace Despachos.Remesa
    <JsonObject>
    Public NotInheritable Class DetalleRemesas

        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Destinatario As Terceros
        <JsonProperty>
        Public Property Codigo As Integer
        <JsonProperty>
        Public Property Cumplido As Integer
        <JsonProperty>
        Public Property NombreRecibe As String
        <JsonProperty>
        Public Property IdentificacionRecibe As String
        <JsonProperty>
        Public Property TelefonoRecibe As String
        <JsonProperty>
        Public Property FechaRecibe As Date
        <JsonProperty>
        Public Property CantidadRecibida As Double
        <JsonProperty>
        Public Property PesoRecibido As Double
        <JsonProperty>
        Public Property CantidadFaltante As Double
        <JsonProperty>
        Public Property PesoFaltante As Double
        <JsonProperty>
        Public Property ValorFaltante As Double
    End Class
End Namespace
