﻿PRINT 'gsp_generar_manifiesto_transbordo'
GO
DROP PROCEDURE gsp_generar_manifiesto_transbordo
GO
CREATE PROCEDURE gsp_generar_manifiesto_transbordo   
(            
  @par_EMPR_Codigo NUMERIC,                      
  @par_ENPD_Numero NUMERIC,                          
  @par_ENMC_Numero NUMERIC,  
  @par_ECPD_Numero NUMERIC,  
  @par_OFIC_Codigo SMALLINT,            
  @par_VEHI_Codigo_Transbordo numeric  = NULL,             
  @par_TERC_Codigo_Tenedor_Transbordo numeric  = NULL,             
  @par_TERC_Codigo_Propietario_Transbordo numeric  = NULL,             
  @par_TERC_Codigo_Condcutor_Transbordo numeric  = NULL,             
  @par_SEMI_Codigo_Transbordo numeric  = NULL            
)            
AS            
BEGIN    
	DECLARE @NumeroDocumentoManifiesto numeric      
	DECLARE @NumeroInternoDocumentoManifiesto numeric  
	DECLARE @NumeroDocumentoPlanilla Numeric      
	DECLARE @NumeroInternoPlanilla Numeric  
	--actualiza en planilla y manifiesto  
	UPDATE Encabezado_Planilla_Despachos SET ECPD_Numero = @par_ECPD_Numero  
	WHERE EMPR_Codigo = @par_EMPR_Codigo  
	AND Numero = @par_ENPD_Numero  
  
	UPDATE Encabezado_Manifiesto_Carga SET ECPD_Numero = @par_ECPD_Numero  
	WHERE EMPR_Codigo = @par_EMPR_Codigo  
	AND Numero = @par_ENMC_Numero  
	--actualiza en planilla y manifiesto  
	--Crear Manifiesto          
	EXEC gsp_generar_consecutivo @par_EMPR_Codigo, 140, @par_OFIC_Codigo, @NumeroDocumentoManifiesto OUTPUT      
	INSERT INTO Encabezado_Manifiesto_Carga  
	(            
	EMPR_Codigo,            
	TIDO_Codigo,            
	Numero_Documento,            
	Fecha,            
	VEHI_Codigo,            
	TERC_Codigo_Propietario,            
	TERC_Codigo_Tenedor,            
	TERC_Codigo_Conductor,            
	TERC_Codigo_Segundo_Conductor,            
	TERC_Codigo_Afiliador,            
	SEMI_Codigo,            
	TIVE_Codigo,            
	RUTA_Codigo,            
	Observaciones,            
	Fecha_Cumplimiento,            
	Cantidad_Total,            
	Peso_Total,            
	Valor_Flete,            
	Valor_Retencion_Fuente,            
	Valor_ICA,            
	Valor_Otros_Descuentos,            
	Valor_Flete_Neto,            
	Valor_Anticipo,            
	Valor_Pagar,            
	TERC_Codigo_Aseguradora,            
	Numero_Poliza,            
	Fecha_Vigencia_Poliza,            
	ELPD_Numero,            
	ECPD_Numero,            
	Finalizo_Viaje,            
	Siniestro,            
	Estado,            
	Numeracion,            
	Fecha_Crea,            
	USUA_Codigo_Crea,            
	Fecha_Modifica,            
	USUA_Codigo_Modifica,            
	Fecha_Anula,            
	USUA_Codigo_Anula,            
	Causa_Anula,            
	OFIC_Codigo,            
	CATA_TIMA_Codigo,            
	Anulado,            
	CATA_MAMC_Codigo,            
	ENPD_Numero,            
	QRManifiesto,            
	CATA_TICM_Codigo,            
	CATA_MOSM_Codigo,            
	CATA_COSM_Codigo,            
	ENMC_Numero_Transbordo,            
	Numero_Manifiesto_Electronico,      
	Aceptacion_Electronica          
	)            
	SELECT TOP(1)          
	EMPR_Codigo,            
	TIDO_Codigo,            
	@NumeroDocumentoManifiesto,            
	GETDATE(),            
	ISNULL(@par_VEHI_Codigo_Transbordo,VEHI_Codigo),            
	ISNULL(@par_TERC_Codigo_Propietario_Transbordo,TERC_Codigo_Propietario),            
	ISNULL(@par_TERC_Codigo_Tenedor_Transbordo,TERC_Codigo_Tenedor),            
	ISNULL(@par_TERC_Codigo_Condcutor_Transbordo,TERC_Codigo_Conductor),            
	TERC_Codigo_Segundo_Conductor,            
	TERC_Codigo_Afiliador,            
	ISNULL(@par_SEMI_Codigo_Transbordo,SEMI_Codigo),            
	TIVE_Codigo,            
	RUTA_Codigo,            
	Observaciones,            
	Fecha_Cumplimiento,            
	Cantidad_Total,            
	Peso_Total,            
	Valor_Flete,            
	Valor_Retencion_Fuente,            
	Valor_ICA,            
	Valor_Otros_Descuentos,            
	Valor_Flete_Neto,            
	Valor_Anticipo,            
	Valor_Pagar,            
	TERC_Codigo_Aseguradora,            
	Numero_Poliza,            
	Fecha_Vigencia_Poliza,            
	0,            
	0,            
	Finalizo_Viaje,            
	Siniestro,      
	Estado,            
	Numeracion,            
	GETDATE(),            
	USUA_Codigo_Crea,            
	Fecha_Modifica,            
	USUA_Codigo_Modifica,            
	Fecha_Anula,        
	USUA_Codigo_Anula,            
	Causa_Anula,            
	OFIC_Codigo,            
	CATA_TIMA_Codigo,            
	Anulado,            
	CATA_MAMC_Codigo,            
	ENPD_Numero,            
	QRManifiesto,            
	CATA_TICM_Codigo,            
	CATA_MOSM_Codigo,            
	CATA_COSM_Codigo,            
	Numero,            
	0,      
	Aceptacion_Electronica          
	FROM Encabezado_Manifiesto_Carga WHERE EMPR_Codigo = @par_EMPR_Codigo and Numero = @par_ENMC_Numero      
	SELECT @NumeroInternoDocumentoManifiesto = @@IDENTITY        
	--Crear Manifiesto      
	--Crear Detalle Manifiesto      
	INSERT INTO Detalle_Manifiesto_Carga (EMPR_Codigo, ENMC_Numero, ENRE_Numero)      
	SELECT EMPR_Codigo, @NumeroInternoDocumentoManifiesto, ENRE_Numero FROM Detalle_Manifiesto_Carga       
	WHERE EMPR_Codigo = @par_EMPR_Codigo and ENMC_Numero = @par_ENMC_Numero      
	--Crear Detalle Manifiesto      
	--Crea Planilla      
	EXEC gsp_generar_consecutivo @par_EMPR_Codigo, 150, @par_OFIC_Codigo, @NumeroDocumentoPlanilla OUTPUT      
	INSERT INTO Encabezado_Planilla_Despachos (  
	EMPR_Codigo      
	,TIDO_Codigo      
	,Numero_Documento      
	,ENMC_Numero      
	,Fecha_Hora_Salida      
	,Fecha      
	,LNTC_Codigo_Compra      
	,TLNC_Codigo_Compra      
	,RUTA_Codigo      
	,TATC_Codigo_Compra      
	,TTTC_Codigo_Compra      
	,ETCC_Numero      
	,VEHI_Codigo      
	,SEMI_Codigo      
	,TERC_Codigo_Tenedor      
	,TERC_Codigo_Conductor      
	,Cantidad      
	,Peso      
	,Valor_Flete_Transportador      
	,Valor_Anticipo      
	,Valor_Impuestos      
	,Valor_Pagar_Transportador      
	,Valor_Flete_Cliente      
	,Valor_Seguro_Mercancia      
	,Valor_Otros_Cobros      
	,Valor_Total_Credito      
	,Valor_Total_Contado      
	,Valor_Total_Alcobro      
	,Valor_Auxiliares      
	,Observaciones      
	,Anulado      
	,Estado      
	,Fecha_Crea      
	,USUA_Codigo_Crea      
	,Fecha_Modifica      
	,USUA_Codigo_Modifica      
	,Fecha_Anula      
	,USUA_Codigo_Anula      
	,Causa_Anula      
	,OFIC_Codigo      
	,Numeracion      
	,ECPD_Numero      
	,ELPD_Numero      
	,ELGC_Numero      
	,Ultimo_Seguimiento_Vehicular      
	,Valor_Seguro_Poliza      
	,Anticipo_Pagado_A      
	,ENRE_Numero_Masivo      
	,ENRE_Numero_Documento_Masivo      
	,Valor_Remesas_Contra_Entrega      
	,Valor_Bruto      
	,Porcentaje      
	,Valor_Descuentos      
	,Flete_Sugerido      
	,Valor_Fondo_Ayuda_Mutua      
	,Valor_Neto_Pagar      
	,Valor_Utilidad      
	,Valor_Estampilla      
	,Gastos_Agencia      
	,Valor_Reexpedicion      
	,Valor_Planilla_Adicional      
	,Calcular_Contra_Entrega      
	,ENPD_Numero_Transbordo      
	)      
	SELECT       
	EMPR_Codigo      
	,TIDO_Codigo      
	,@NumeroDocumentoPlanilla      
	,@NumeroInternoDocumentoManifiesto      
	,Fecha_Hora_Salida      
	,GETDATE()      
	,LNTC_Codigo_Compra      
	,TLNC_Codigo_Compra      
	,RUTA_Codigo      
	,TATC_Codigo_Compra      
	,TTTC_Codigo_Compra      
	,ETCC_Numero      
	,ISNULL(@par_VEHI_Codigo_Transbordo,VEHI_Codigo)      
	,ISNULL(@par_SEMI_Codigo_Transbordo,SEMI_Codigo)            
	,ISNULL(@par_TERC_Codigo_Tenedor_Transbordo,TERC_Codigo_Tenedor)      
	,ISNULL(@par_TERC_Codigo_Condcutor_Transbordo,TERC_Codigo_Conductor)      
	,Cantidad      
	,Peso      
	,Valor_Flete_Transportador      
	,Valor_Anticipo      
	,Valor_Impuestos      
	,Valor_Pagar_Transportador      
	,Valor_Flete_Cliente      
	,Valor_Seguro_Mercancia      
	,Valor_Otros_Cobros      
	,Valor_Total_Credito      
	,Valor_Total_Contado      
	,Valor_Total_Alcobro      
	,Valor_Auxiliares      
	,Observaciones      
	,Anulado      
	,Estado      
	,Fecha_Crea      
	,USUA_Codigo_Crea      
	,Fecha_Modifica      
	,USUA_Codigo_Modifica      
	,Fecha_Anula      
	,USUA_Codigo_Anula      
	,Causa_Anula      
	,OFIC_Codigo      
	,Numeracion      
	,0      
	,0      
	,0      
	,Ultimo_Seguimiento_Vehicular      
	,Valor_Seguro_Poliza      
	,Anticipo_Pagado_A    
	,ENRE_Numero_Masivo      
	,ENRE_Numero_Documento_Masivo      
	,Valor_Remesas_Contra_Entrega      
	,Valor_Bruto      
	,Porcentaje      
	,Valor_Descuentos      
	,Flete_Sugerido      
	,Valor_Fondo_Ayuda_Mutua      
	,Valor_Neto_Pagar      
	,Valor_Utilidad      
	,Valor_Estampilla      
	,Gastos_Agencia      
	,Valor_Reexpedicion      
	,Valor_Planilla_Adicional      
	,Calcular_Contra_Entrega      
	,@par_ENPD_Numero      
	FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo and Numero = @par_ENPD_Numero      
	SELECT @NumeroInternoPlanilla = @@IDENTITY     
	--Crea Planilla    
	--Crea Detalle Planilla      
	INSERT INTO Detalle_Planilla_Despachos(EMPR_Codigo, ENPD_Numero, TIDO_Codigo, ENRE_Numero, Numero_Documento_Remesa)      
	SELECT EMPR_Codigo, @NumeroInternoPlanilla, TIDO_Codigo, ENRE_Numero, Numero_Documento_Remesa      
	FROM Detalle_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo and ENPD_Numero = @par_ENPD_Numero      
	--Crea Detalle Planilla      
	--Crea Seguimiento Planilla    
	INSERT INTO Detalle_Seguimiento_Vehiculos (  
	EMPR_Codigo,    
	ENPD_Numero,    
	ENOC_Numero,    
	ENMC_Numero,    
	Fecha_Reporte,    
	CATA_TOSV_Codigo,    
	Ubicacion,    
	Longitud,    
	Latitud,    
	PUCO_Codigo,    
	CATA_SRSV_Codigo,    
	CATA_NOSV_Codigo,    
	Observaciones,    
	Kilometros_Vehiculo,    
	Reportar_Cliente,    
	Envio_Reporte_Cliente,    
	Fecha_Reporte_Cliente,    
	USUA_Codigo_Crea,    
	Fecha_Crea,    
	Anulado,    
	Orden    
	)                                                
	VALUES                                                 
	(                                                
	@par_EMPR_Codigo,                                                
	@NumeroInternoPlanilla,                                                
	0,                                                
	@NumeroInternoDocumentoManifiesto,                                                
	getdate(),                                                
	8001,                                                
	'',                                                
	0,                                                
	0,                                                
	0,                                                
	8202,                                                
	8100,                      
	'PLANILLA DESPACHOS',                                                
	0,                                                
	0,                                                
	0,                                                
	'',                                                
	1,                                                
	GETDATE(),                                                
	0,                                                
	1                                                
	)    
	--Crea Seguimiento Planilla    
	--Asociar manifiesto y planilla a las remesas      
	UPDATE      
	Encabezado_Remesas  
	SET      
	ENMC_Numero = @NumeroInternoDocumentoManifiesto,      
	ENPD_Numero = @NumeroInternoPlanilla,      
	VEHI_Codigo = @par_VEHI_Codigo_Transbordo,      
	TERC_Codigo_Conductor = @par_TERC_Codigo_Condcutor_Transbordo,      
	SEMI_Codigo = ISNULL(@par_SEMI_Codigo_Transbordo, SEMI_Codigo)      
	WHERE      
	EMPR_Codigo = @par_EMPR_Codigo      
	AND ENPD_Numero = @par_ENPD_Numero      
	--Asociar manifiesto y planilla a las remesas    
	--Actualizar Seguimiento Remesas    
	Update Detalle_Seguimiento_Remesas SET ENPD_Numero = @NumeroInternoPlanilla    
	WHERE      
	EMPR_Codigo = @par_EMPR_Codigo      
	AND ENPD_Numero = @par_ENPD_Numero     
	--Actualizar Seguimiento Remesas    
	--Actualizar Planilla en Manifiesto      
	UPDATE                                  
	Encabezado_Manifiesto_Carga                                  
	SET                                  
	ENPD_Numero = @NumeroInternoPlanilla      
	WHERE                                  
	EMPR_Codigo = @par_EMPR_Codigo                               
	AND Numero = @NumeroInternoDocumentoManifiesto       
	--Actualizar Planilla en Manifiesto  
	select @NumeroInternoDocumentoManifiesto as Numero            
END 
GO