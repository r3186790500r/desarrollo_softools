﻿PRINT 'gsp_modificar_encabezado_documento_comprobantes'
GO
DROP PROCEDURE gsp_modificar_encabezado_documento_comprobantes
GO
CREATE PROCEDURE gsp_modificar_encabezado_documento_comprobantes
(
@par_EMPR_Codigo SMALLINT,
@par_Numero NUMERIC ,
@par_Codigo NUMERIC, 
@par_Codigo_Alterno VARCHAR (20),
@par_TIDO_Codigo NUMERIC,
@par_Fecha DATETIME,
@par_TERC_Codigo_Titular NUMERIC,
@par_TERC_Codigo_Beneficiario NUMERIC,
@par_Observaciones VARCHAR (1000) = NULL,
@par_CATA_DOOR_Codigo NUMERIC,
@par_CATA_FPDC_Codigo NUMERIC,
@par_CATA_DEIN_Codigo NUMERIC,
@par_Documento_Origen NUMERIC = NULL,
@par_CUBA_Codigo SMALLINT = NULL,
@par_CAJA_Codigo SMALLINT = NULL,
@par_Numero_Pago_Recaudo NUMERIC = NULL,
@par_Fecha_Pago_Recaudo DATETIME,
@par_Valor_Pago_Recaudo_Transferencia MONEY,
@par_Valor_Pago_Recaudo_Efectivo MONEY,
@par_Valor_Pago_Recaudo_Cheque MONEY,
@par_Valor_Pago_Recaudo_Total MONEY,
@par_Numeracion VARCHAR (20) = NULL,
@par_OFIC_Codigo_Creacion SMALLINT,
@par_OFIC_Codigo_Destino SMALLINT,
@par_Genero_Consignacion SMALLINT,
@par_Valor_Alterno MONEY,
@par_VEHI_Codigo NUMERIC = NULL,
@par_Estado SMALLINT,
@par_USUA_Codigo_Modifica SMALLINT,
@par_ECCO_Codigo NUMERIC = NULL
)
AS
BEGIN

DELETE Detalle_Documento_Comprobantes where EMPR_Codigo = @par_EMPR_Codigo AND EDCO_Codigo = @par_Codigo 

UPDATE Encabezado_Documento_Comprobantes SET 
Codigo_Alterno = @par_Codigo_Alterno,
Fecha = @par_Fecha,
TERC_Codigo_Titular = @par_TERC_Codigo_Titular,
TERC_Codigo_Beneficiario = @par_TERC_Codigo_Beneficiario,
Observaciones = ISNULL(@par_Observaciones,''),
CATA_DOOR_Codigo = @par_CATA_DOOR_Codigo,
CATA_FPDC_Codigo = @par_CATA_FPDC_Codigo,
CATA_DEIN_Codigo = @par_CATA_DEIN_Codigo,
Documento_Origen = ISNULL(@par_Documento_Origen,0),
CUBA_Codigo = ISNULL(@par_CUBA_Codigo,0) ,
CAJA_Codigo = ISNULL(@par_CAJA_Codigo,0),
Numero_Pago_Recaudo =ISNULL(@par_Numero_Pago_Recaudo,0),
Fecha_Pago_Recaudo =@par_Fecha_Pago_Recaudo,
Valor_Pago_Recaudo_Transferencia =@par_Valor_Pago_Recaudo_Transferencia,
Valor_Pago_Recaudo_Efectivo =@par_Valor_Pago_Recaudo_Efectivo,
Valor_Pago_Recaudo_Cheque =@par_Valor_Pago_Recaudo_Cheque ,
Valor_Pago_Recaudo_Total = @par_Valor_Pago_Recaudo_Total,
Numeracion =ISNULL(@par_Numeracion ,0),
OFIC_Codigo_Creacion =@par_OFIC_Codigo_Creacion ,
OFIC_Codigo_Destino = @par_OFIC_Codigo_Destino,
Genero_Consignacion = @par_Genero_Consignacion,
Valor_Alterno = @par_Valor_Alterno,
VEHI_Codigo = @par_VEHI_Codigo,
Estado = @par_Estado,
USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,
Fecha_Modifica = GETDATE(),
ECCO_Codigo= ISNULL(@par_ECCO_Codigo,0)

WHERE 
EMPR_Codigo = @par_EMPR_Codigo
AND Codigo = @par_Codigo
AND Numero = @par_Numero
AND TIDO_Codigo  = @par_TIDO_Codigo

SELECT @par_Numero AS Numero

END
GO