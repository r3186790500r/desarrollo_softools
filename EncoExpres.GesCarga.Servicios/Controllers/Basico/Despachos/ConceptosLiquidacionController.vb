﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="ConceptosLiquidacionController"/>
''' </summary>
<Authorize>
Public Class ConceptosLiquidacionController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ConceptoLiquidacionPlanillaDespacho) As Respuesta(Of IEnumerable(Of ConceptoLiquidacionPlanillaDespacho))
        Return New LogicaConceptoLiquidacion(CapaPersistenciaConceptosLiquidacion).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ConceptoLiquidacionPlanillaDespacho) As Respuesta(Of ConceptoLiquidacionPlanillaDespacho)
        Return New LogicaConceptoLiquidacion(CapaPersistenciaConceptosLiquidacion).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ConceptoLiquidacionPlanillaDespacho) As Respuesta(Of Long)
        Return New LogicaConceptoLiquidacion(CapaPersistenciaConceptosLiquidacion).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("GuardarImpuestos")>
    Public Function GuardarImpuestos(ByVal entidad As ConceptoLiquidacionPlanillaDespacho) As Respuesta(Of Long)
        Return New LogicaConceptoLiquidacion(CapaPersistenciaConceptosLiquidacion).GuardarImpuestos(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ConceptoLiquidacionPlanillaDespacho) As Respuesta(Of Boolean)
        Return New LogicaConceptoLiquidacion(CapaPersistenciaConceptosLiquidacion).Anular(entidad)
    End Function

End Class
