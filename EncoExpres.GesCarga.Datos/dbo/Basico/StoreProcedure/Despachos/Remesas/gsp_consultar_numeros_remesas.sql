﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 07/12/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_consultar_numeros_remesas'
GO
DROP PROCEDURE gsp_consultar_numeros_remesas
GO
CREATE PROCEDURE gsp_consultar_numeros_remesas 
(                    
 @par_EMPR_Codigo SMALLINT,                  
 @par_Codigo_Conductor NUMERIC = null        
)                    
AS                  
BEGIN                  
        
 SELECT                  
  2 AS Obtener                  
    ,ENRE.EMPR_Codigo                  
    ,ENRE.Numero                           
    ,ENRE.Numero_Documento           
    ,ENRE.TIDO_Codigo           
                 
 FROM                  
  Encabezado_Remesas ENRE                  
              
 LEFT JOIN Terceros AS COND              
 ON ENRE.EMPR_Codigo = COND.EMPR_Codigo             
 AND ENRE.TERC_Codigo_Conductor = COND.Codigo                                       
     
 LEFT JOIN Remesas_Paqueteria AS REPA    
 ON ENRE.EMPR_Codigo = REPA.EMPR_Codigo    
 AND ENRE.Numero = REPA.ENRE_Numero    
    
 WHERE                  
  ENRE.EMPR_Codigo = @par_EMPR_Codigo                  
  AND (COND.Codigo = @par_Codigo_Conductor OR @par_Codigo_Conductor IS NULL )        
  AND ((TIDO_Codigo = 100  AND ENRE.Distribucion = 1) OR (TIDO_Codigo = 110  and (ENRE.Entregado = 0 OR ENRE.Entregado IS NULL) AND REPA.CATA_ESRP_Codigo = 6025))  
  AND ISNULL(ENRE.Distribuido, 0) = 0         
  AND ENRE.Cumplido = 0             
  --AND ENRE.ENPD_Numero = 0             
END                  	
GO