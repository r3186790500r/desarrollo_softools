﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Repositorio.Gesphone

Namespace Gesphone
    Public NotInheritable Class PersistenciaNovedadTransito
        Implements IPersistenciaBase(Of NovedadTransito)

        Public Function Consultar(filtro As NovedadTransito) As IEnumerable(Of NovedadTransito) Implements IPersistenciaBase(Of NovedadTransito).Consultar
            Return New RepositorioNovedadTransito().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As NovedadTransito) As Long Implements IPersistenciaBase(Of NovedadTransito).Insertar
            Return New RepositorioNovedadTransito().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As NovedadTransito) As Long Implements IPersistenciaBase(Of NovedadTransito).Modificar
            Return New RepositorioNovedadTransito().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As NovedadTransito) As NovedadTransito Implements IPersistenciaBase(Of NovedadTransito).Obtener
            Return New RepositorioNovedadTransito().Obtener(filtro)
        End Function

        Public Function Anular(entidad As NovedadTransito) As Boolean
            Return New RepositorioNovedadTransito().Anular(entidad)
        End Function

    End Class

End Namespace

