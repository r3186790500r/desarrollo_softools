﻿PRINT 'gsp_consultar_estudio_seguridad_documentos'
GO
DROP PROCEDURE gsp_consultar_estudio_seguridad_documentos
GO
CREATE PROCEDURE gsp_consultar_estudio_seguridad_documentos 
(      
@par_EMPR_Codigo SMALLINT,  
@par_USUA_Codigo_Modifica NUMERIC = null,    
@par_Numero NUMERIC      
)      
AS      
BEGIN     
      
SELECT       
EMPR_Codigo,     
ENES_Numero,      
EOES_Codigo,      
TDES_Codigo,      
ISNULL(Referencia, 0) AS Referencia,      
ISNULL(Emisor, 0) AS Emisor,      
ISNULL(Fecha_Emision, '') AS Fecha_Emision,      
ISNULL(Fecha_Vence, '') AS Fecha_Vence,      
ISNULL(Documento, 0) AS Documento,      
ISNULL(Nombre_Documento, '') AS Nombre_Documento,      
ISNULL(Extension_Documento, '') AS Extension_Documento    
      
FROM      
Estudio_Seguridad_Documentos      
      
WHERE      
EMPR_Codigo = @par_EMPR_Codigo      
AND ENES_Numero = @par_Numero      
      
END   
GO  