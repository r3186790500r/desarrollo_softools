﻿Imports Newtonsoft.Json

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref=" ReferenciaAlmacenes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ReferenciaAlmacenes
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ReferenciaAlmacenes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ReferenciaAlmacenes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            ExistenciaActual = Read(lector, "Existencia_Actual")
            ExistenciaMaxima = Read(lector, "Existencia_Maxima")
            ExistenciaMinima = Read(lector, "Existencia_Minima")
            UltimoPrecio = Read(lector, "Ultimo_Precio")
            ValorPromedio = Read(lector, "Valor_Promedio")
            UbicacionAlmacen = Read(lector, "Ubicacion_Almacen")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")

            If Obtener = 0 Then
                Almacen = New Almacenes With {.Codigo = Read(lector, "ALMA_Codigo"), .Nombre = Read(lector, "Almacen")}
                Referencia = New Referencias With {.Codigo = Read(lector, "REAL_Codigo"), .Nombre = Read(lector, "NombreReferencia"), .CodigoReferencia = Read(lector, "Codigo_Referencia")}
                TotalPaginas = Read(lector, "TotalRegistros")
            ElseIf Obtener = 2 Then 'Obtener
                NombreCompleto = Read(lector, "NombreCompleto")
                Cantidad = Read(lector, "Cantidad")
            Else
                Almacen = New Almacenes With {.Codigo = Read(lector, "ALMA_Codigo")}
                Referencia = New Referencias With {.Codigo = Read(lector, "REAL_Codigo")}
            End If

        End Sub

        <JsonProperty>
        Public Property Almacen As Almacenes
        <JsonProperty>
        Public Property Referencia As Referencias
        <JsonProperty>
        Public Property ExistenciaMinima As Integer
        <JsonProperty>
        Public Property ExistenciaMaxima As Integer
        <JsonProperty>
        Public Property ExistenciaActual As Integer
        <JsonProperty>
        Public Property UltimoPrecio As Double
        <JsonProperty>
        Public Property ValorPromedio As Double
        <JsonProperty>
        Public Property UbicacionAlmacen As String


        <JsonProperty>
        Public Property NombreReferencia As String
        <JsonProperty>
        Public Property NombreAlmacen As String
        <JsonProperty>
        Public Property CodigoReferencia As String
        <JsonProperty>
        Public Property Cantidad As Integer

        <JsonProperty>
        Public Property NombreCompleto As String
    End Class
End Namespace
