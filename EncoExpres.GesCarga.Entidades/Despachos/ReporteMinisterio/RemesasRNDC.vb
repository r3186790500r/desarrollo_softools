﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Despachos.Procesos.General
Namespace Despachos.Procesos
    <JsonObject>
    Public NotInheritable Class RemesasRNDC
        Inherits BaseDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            OpcionReporte = Read(lector, "OpcionReporte")
            Select Case OpcionReporte
                Case OPCIONES_REPORTE_RNDC.REMESA
                    NUMNITEMPRESATRANSPORTEALTERNO = Read(lector, "NUMNITEMPRESATRANSPORTE_ALTERNO")
                    NUMNITEMPRESATRANSPORTE = Read(lector, "NUMNITEMPRESATRANSPORTE")
                    CONSECUTIVOREMESA = Read(lector, "CONSECUTIVOREMESA")
                    NUMMANIFIESTOCARGA = Read(lector, "NUMMANIFIESTOCARGA")
                    TIPOCUMPLIDOREMESA = Read(lector, "TIPOCUMPLIDOREMESA")
                    CONSECUTIVOINFORMACIONCARGA = Read(lector, "CONSECUTIVOINFORMACIONCARGA")
                    CONSECUTIVOCARGADIVIDIDA = Read(lector, "CONSECUTIVOCARGADIVIDIDA")
                    CODOPERACIONTRANSPORTE = Read(lector, "CODOPERACIONTRANSPORTE")
                    CODTIPOEMPAQUE = Read(lector, "CODTIPOEMPAQUE")
                    CODNATURALEZACARGA = Read(lector, "CODNATURALEZACARGA")
                    PERMISOCARGAEXTRA = Read(lector, "PERMISOCARGAEXTRA")
                    DESCRIPCIONCORTAPRODUCTO = Read(lector, "DESCRIPCIONCORTAPRODUCTO")
                    MERCANCIAREMESA = Read(lector, "MERCANCIAREMESA")
                    CANTIDADINFORMACIONCARGA = Read(lector, "CANTIDADINFORMACIONCARGA")
                    CANTIDADCARGADA = Read(lector, "CANTIDADCARGADA")
                    UNIDADMEDIDACAPACIDAD = Read(lector, "UNIDADMEDIDACAPACIDAD")
                    PESOCONTENEDORVACIO = Read(lector, "PESOCONTENEDORVACIO")
                    DUENOPOLIZA = Read(lector, "DUENOPOLIZA") 'Vacio
                    NUMPOLIZATRANSPORTE = Read(lector, "NUMPOLIZATRANSPORTE") 'Vacio
                    FECHAVENCIMIENTOPOLIZACARGA = Read(lector, "FECHAVENCIMIENTOPOLIZACARGA") 'Vacio
                    COMPANIASEGURO = Read(lector, "COMPANIASEGURO") 'Vacio
                    CANTIDADENTREGADA = Read(lector, "CANTIDADENTREGADA") 'Vacio
                    FECHALLEGADACARGUE = Read(lector, "FECHALLEGADACARGUE") 'Vacio
                    FECHAENTRADACARGUE = Read(lector, "FECHAENTRADACARGUE") 'Vacio
                    FECHASALIDACARGUE = Read(lector, "FECHASALIDACARGUE") 'Vacio
                    FECHALLEGADADESCARGUE = Read(lector, "FECHALLEGADADESCARGUE") 'Vacio
                    FECHAENTRADADESCARGUE = Read(lector, "FECHAENTRADADESCARGUE") 'Vacio
                    FECHASALIDADESCARGUE = Read(lector, "FECHASALIDADESCARGUE") 'Vacio
                    HORASALIDACARGUEREMESA = Read(lector, "HORASALIDACARGUEREMESA") 'Vacio
                    HORASPACTOCARGA = Read(lector, "HORASPACTOCARGA")
                    MINUTOSPACTOCARGA = Read(lector, "MINUTOSPACTOCARGA")
                    HORASPACTODESCARGUE = Read(lector, "HORASPACTODESCARGUE")
                    MINUTOSPACTODESCARGUE = Read(lector, "MINUTOSPACTODESCARGUE")
                    'FECHACITAPACTADACARGUE = Read(lector, "FECHACITAPACTADACARGUE")
                    'HORACITAPACTADACARGUE = Read(lector, "HORACITAPACTADACARGUE")
                    'FECHACITAPACTADADESCARGUE = Read(lector, "FECHACITAPACTADADESCARGUE")
                    'HORACITAPACTADADESCARGUEREMESA = Read(lector, "HORACITAPACTADADESCARGUEREMESA")
                    OBSERVACIONES = Read(lector, "OBSERVACIONES")
                    NUMPLACA = Read(lector, "NUMPLACA")
                    NUMIDGPS = Read(lector, "NUMIDGPS")
                    NUMERORADICADOREMESA = Read(lector, "Numero_Remesa_Electronico")
                    NUMERORADICADOMANIFIESTO = Read(lector, "Numero_Manifiesto_Electronico")
                    FechaRemesa = Read(lector, "FECHAREMESA")

                    Remitente = New TerceroRNDC With {
                        .NUMNITEMPRESATRANSPORTE = Read(lector, "NUMNITEMPRESATRANSPORTE"),
                        .CODTIPOIDTERCERO = Read(lector, "REMI_CODTIPOIDTERCERO"),
                        .DIGITOCHEQUEO = Read(lector, "REMI_DIGITOCHEQUEO"),
                        .NUMIDTERCERO = Read(lector, "REMI_NUMIDTERCERO"),
                        .NOMIDTERCERO = Read(lector, "REMI_NOMIDTERCERO"),
                        .PRIMERAPELLIDOIDTERCERO = Read(lector, "REMI_PRIMERAPELLIDOIDTERCERO"),
                        .SEGUNDOAPELLIDOIDTERCERO = Read(lector, "REMI_SEGUNDOAPELLIDOIDTERCERO"),
                        .CODSEDETERCERO = Read(lector, "REMI_CODSEDETERCERO"),
                        .NOMSEDETERCERO = Read(lector, "REMI_NOMSEDETERCERO"),
                        .NUMTELEFONOCONTACTO = Read(lector, "REMI_NUMTELEFONOCONTACTO"),
                        .NUMCELULARPERSONA = Read(lector, "REMI_NUMCELULARPERSONA"),
                        .NOMENCLATURADIRECCION = Read(lector, "REMI_NOMENCLATURADIRECCION"),
                        .CODMUNICIPIORNDC = Read(lector, "REMI_CODSEDETERCERO"),
                        .Reportar_RNDC = Read(lector, "REMI_Reportar_RNDC"),
                        .Codigo = Read(lector, "REMI_Codigo")
                    }

                    Destinatario = New TerceroRNDC With {
                        .NUMNITEMPRESATRANSPORTE = Read(lector, "NUMNITEMPRESATRANSPORTE"),
                        .CODTIPOIDTERCERO = Read(lector, "DEST_CODTIPOIDTERCERO"),
                        .DIGITOCHEQUEO = Read(lector, "DEST_DIGITOCHEQUEO"),
                        .NUMIDTERCERO = Read(lector, "DEST_NUMIDTERCERO"),
                        .NOMIDTERCERO = Read(lector, "DEST_NOMIDTERCERO"),
                        .PRIMERAPELLIDOIDTERCERO = Read(lector, "DEST_PRIMERAPELLIDOIDTERCERO"),
                        .SEGUNDOAPELLIDOIDTERCERO = Read(lector, "DEST_SEGUNDOAPELLIDOIDTERCERO"),
                        .CODSEDETERCERO = Read(lector, "DEST_CODSEDETERCERO"),
                        .NOMSEDETERCERO = Read(lector, "DEST_NOMSEDETERCERO"),
                        .NUMTELEFONOCONTACTO = Read(lector, "DEST_NUMTELEFONOCONTACTO"),
                        .NUMCELULARPERSONA = Read(lector, "DEST_NUMCELULARPERSONA"),
                        .NOMENCLATURADIRECCION = Read(lector, "DEST_NOMENCLATURADIRECCION"),
                        .CODMUNICIPIORNDC = Read(lector, "DEST_CODSEDETERCERO"),
                        .Reportar_RNDC = Read(lector, "DEST_Reportar_RNDC"),
                        .Codigo = Read(lector, "DEST_Codigo")
                    }
            End Select
        End Sub

        <JsonProperty>
        Public Property NUMNITEMPRESATRANSPORTE As String
        <JsonProperty>
        Public Property NUMNITEMPRESATRANSPORTEALTERNO As String
        <JsonProperty>
        Public Property CONSECUTIVOREMESA As String
        <JsonProperty>
        Public Property NUMPLACA As String
        <JsonProperty>
        Public Property NUMIDGPS As String
        <JsonProperty>
        Public Property CONSECUTIVOINFORMACIONCARGA As String
        <JsonProperty>
        Public Property CONSECUTIVOCARGADIVIDIDA As String
        <JsonProperty>
        Public Property CODOPERACIONTRANSPORTE As String
        <JsonProperty>
        Public Property CODTIPOEMPAQUE As String
        <JsonProperty>
        Public Property CODNATURALEZACARGA As String
        <JsonProperty>
        Public Property DESCRIPCIONCORTAPRODUCTO As String
        <JsonProperty>
        Public Property MERCANCIAREMESA As String
        <JsonProperty>
        Public Property CANTIDADINFORMACIONCARGA As String
        <JsonProperty>
        Public Property CANTIDADCARGADA As String
        <JsonProperty>
        Public Property UNIDADMEDIDACAPACIDAD As String
        <JsonProperty>
        Public Property PESOCONTENEDORVACIO As String
        <JsonProperty>
        Public Property CODTIPOIDREMITENTE As String
        <JsonProperty>
        Public Property NUMIDREMITENTE As String
        <JsonProperty>
        Public Property CODSEDEREMITENTE As String
        <JsonProperty>
        Public Property CODTIPOIDDESTINATARIO As String
        <JsonProperty>
        Public Property NUMIDDESTINATARIO As String
        <JsonProperty>
        Public Property CODSEDEDESTINATARIO As String
        <JsonProperty>
        Public Property CODTIPOIDPROPIETARIO As String
        <JsonProperty>
        Public Property NUMIDPROPIETARIO As String
        <JsonProperty>
        Public Property CODSEDEPROPIETARIO As String
        <JsonProperty>
        Public Property DUENOPOLIZA As String
        <JsonProperty>
        Public Property NUMPOLIZATRANSPORTE As String
        <JsonProperty>
        Public Property FECHAVENCIMIENTOPOLIZACARGA As String
        <JsonProperty>
        Public Property COMPANIASEGURO As String
        <JsonProperty>
        Public Property FECHACITAPACTADACARGUE As String
        <JsonProperty>
        Public Property HORACITAPACTADACARGUE As String
        <JsonProperty>
        Public Property FECHALLEGADACARGUE As String
        <JsonProperty>
        Public Property HORALLEGADACARGUEREMESA As String
        <JsonProperty>
        Public Property FECHAENTRADACARGUE As String
        <JsonProperty>
        Public Property HORAENTRADACARGUEREMESA As String
        <JsonProperty>
        Public Property FECHASALIDACARGUE As String
        <JsonProperty>
        Public Property HORASALIDACARGUEREMESA As String
        <JsonProperty>
        Public Property HORASPACTODESCARGUE As String
        <JsonProperty>
        Public Property MINUTOSPACTODESCARGUE As String
        <JsonProperty>
        Public Property FECHACITAPACTADADESCARGUE As String
        <JsonProperty>
        Public Property HORACITAPACTADADESCARGUEREMESA As String
        <JsonProperty>
        Public Overloads Property OBSERVACIONES As String
        <JsonProperty>
        Public Property HORASPACTOCARGA As String
        <JsonProperty>
        Public Property MINUTOSPACTOCARGA As String
        <JsonProperty>
        Public Property PERMISOCARGAEXTRA As String


        <JsonProperty>
        Public Property FechaRemesa As String

        ''CAMPOS CUMPLIDO
        <JsonProperty>
        Public Property TIPOCUMPLIDOREMESA As String
        <JsonProperty>
        Public Property MOTIVOSUSPENSIONREMESA As String
        <JsonProperty>
        Public Property CANTIDADENTREGADA As String
        <JsonProperty>
        Public Property HORACITAREALPACTADACARGUE As String
        <JsonProperty>
        Public Property FECHALLEGADADESCARGUE As String
        <JsonProperty>
        Public Property HORALLEGADADESCARGUECUMPLIDO As String
        <JsonProperty>
        Public Property FECHAENTRADADESCARGUE As String
        <JsonProperty>
        Public Property HORAENTRADADESCARGUECUMPLIDO As String
        <JsonProperty>
        Public Property FECHASALIDADESCARGUE As String
        <JsonProperty>
        Public Property HORASALIDADESCARGUECUMPLIDO As String
        <JsonProperty>
        Public Property VALORREMESAFACTURA As String

        'CAMPOS ANULACION
        <JsonProperty>
        Public Property MOTIVOREVERSAREMESA As String
        <JsonProperty>
        Public Property NUMMANIFIESTOCARGA As String
        <JsonProperty>
        Public Property CODMUNICIPIOTRANSBORDO As String
        <JsonProperty>
        Public Property MOTIVOANULACIONREMESA As String
        <JsonProperty>
        Public Property MOTIVOTRANSBORDOREMESA As String

        <JsonProperty>
        Public Property REMCIUDAD_DESTI_TRANSBORDO As String
        <JsonProperty>
        Public Property REMMOTIVOTRANSBORDO As String
        <JsonProperty>
        Public Property REMREMITENTE As String
        <JsonProperty>
        Public Property NUMERORADICADOREMESA As String
        <JsonProperty>
        Public Property NUMERORADICADOMANIFIESTO As String

        <JsonProperty>
        Public Property Remitente As TerceroRNDC
        <JsonProperty>
        Public Property Destinatario As TerceroRNDC
        <JsonProperty>
        Public Property OpcionReporte As Integer
    End Class
End Namespace
