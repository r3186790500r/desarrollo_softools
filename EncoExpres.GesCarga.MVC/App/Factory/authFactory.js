﻿(function () {
    'use strict';
    EncoExpresApp.factory('authFactory', ['$http', '$q',
    'localStorageService', function ($http, $q, localStorageService) {

        var serviceBase = URL_Servicios;
        
        var authServiceFactory = {};

        var _authentication = {
            isAuth: false,
            userName: ""
        };

        var _login = function (loginData) {

            var data = "grant_type=password&username=" +
            loginData.userName + "&password=" + loginData.password;

            var deferred = $q.defer();

            $http.post(serviceBase + 'encoExpresToken', data, {
                headers:
                { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(successCallback, errorCallback);

            function successCallback(response) {
                localStorageService.set('authorizationData',
                { token: response.data.access_token, userName: loginData.userName });

                _authentication.isAuth = true;
                _authentication.userName = loginData.userName;

                deferred.resolve(response);
            }
            function errorCallback(err, status) {
                _logOut();
                deferred.reject(err);
            }


            //$http(serviceBase + 'encoExpresToken', data, {
            //    method: 'GET',
            //    headers:
            //    { 'Content-Type': 'application/x-www-form-urlencoded' }
            //}).then(function successCallback(response){

            //    localStorageService.set('authorizationData',
            //    { token: response.access_token, userName: loginData.userName });

            //    _authentication.isAuth = true;
            //    _authentication.userName = loginData.userName;

            //    deferred.resolve(response);

            //}).error(function (err, status) {
            //    _logOut();
            //    deferred.reject(err);
            //});

            return deferred.promise;
        };

        var _logOut = function () {

            localStorageService.remove('authorizationData');

            _authentication.isAuth = false;
            _authentication.userName = "";
        };

        var _fillAuthData = function () {

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
            }
        }

        authServiceFactory.login = _login;
        authServiceFactory.logOut = _logOut;
        authServiceFactory.fillAuthData = _fillAuthData;
        authServiceFactory.authentication = _authentication;

        return authServiceFactory;
    }]);

})();