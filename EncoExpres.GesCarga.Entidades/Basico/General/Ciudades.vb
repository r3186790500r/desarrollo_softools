﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" Ciudades"/>
    ''' </summary>
    <JsonObject>
    Public Class Ciudades
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Ciudades"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Ciudades"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Ciudad")
            Paises = New Paises With {.Codigo = Read(lector, "PAIS_Codigo"), .Nombre = Read(lector, "Pais")}
            Departamento = New Departamentos With {.Codigo = Read(lector, "DEPA_Codigo"), .Nombre = Read(lector, "Departamento")}
            CodigoPostal = Read(lector, "Codigo_Postal")
            Estado = Read(lector, "Estado")
            NombreCompleto = Read(lector, "CiudadDepartamento")
            Pais = Read(lector, "Pais")
            Cantidad_Sitios = Read(lector, "Cantidad_Sitios")
            CantidadZonas = Read(lector, "Cantidad_Zonas")
            TotalRegistros = Read(lector, "TotalRegistros")
            CalcularICAOficina = Read(lector, "Calcular_ICA_Oficina")

        End Sub


        <JsonProperty>
        Public Property CalcularICAOficina As Short


        <JsonProperty>
        Public Property AplicaOficina As Boolean
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property CodigoAlterno As String
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Codigo As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property NombreCompleto As String
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Pais As String
        ''' <summary>
        ''' Obtiene o establece si el catalogo es actualizable o no
        ''' </summary>
        <JsonProperty>
        Public Property Paises As Paises
        ''' <summary>
        ''' Obtiene o establece si el catalogo es actualizable o no
        ''' </summary>
        <JsonProperty>
        Public Property Departamento As Departamentos

        ''' <summary>
        ''' Obtiene o establece el numero de columnas del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property TarifaImpuesto1 As Double
        ''' <summary>
        ''' Obtiene o establece el numero de columnas del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property TarifaImpuesto2 As Double

        ''' <summary>
        ''' Obtiene o establece el numero de columnas del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property UsuarioCrea As Integer

        ''' <summary>
        ''' Obtiene o establece el numero de columnas del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property UsuarioModifica As Integer

        ''' <summary>
        ''' Obtiene o establece el numero de columnas del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Estado As Short


        ''' <summary>
        ''' Obtiene o establece el numero de columnas del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property CodigoCiudad As Short

        <JsonProperty>
        Public Property Oficinas As IEnumerable(Of Oficinas)
        <JsonProperty>
        Public Property Regiones As IEnumerable(Of RegionesPaises)
        <JsonProperty>
        Public Property CiudadOficinas As Short
        <JsonProperty>
        Public Property CiudadRegiones As Short
        <JsonProperty>
        Public Property Cantidad_Sitios As Short
        <JsonProperty>
        Public Property CantidadZonas As Short
        <JsonProperty>
        Public Property Region As RegionesPaises
        ''' <summary>
        ''' Obtiene o establece el codigo postal de la ciudad
        ''' </summary>
        <JsonProperty>
        Public Property CodigoPostal As String
    End Class
End Namespace
