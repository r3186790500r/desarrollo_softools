﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.Facturacion

    ''' <summary>
    ''' Clase <see cref="ImpuestoConceptoFacturacion"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ImpuestoConceptoFacturacion
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImpuestoConceptoFacturacion"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImpuestoConceptoFacturacion"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            Codigo = Read(lector, "Codigo")

            Nombre = Read(lector, "Nombre")
            Operacion = Read(lector, "Operacion")
            Estado = Read(lector, "Estado")
            Valor_tarifa = Read(lector, "Valor_Tarifa")
            valor_base = Read(lector, "Valor_Base")
            PUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo")}

        End Sub
        <JsonProperty>
        Public Property Valor_tarifa As Double

        <JsonProperty>
        Public Property valor_base As Long

        <JsonProperty>
        Public Property Placa As String

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property ConceptoSistema As Integer

        <JsonProperty>
        Public Property AplicaImpuesto As Integer

        <JsonProperty>
        Public Property Operacion As Integer
        <JsonProperty>
        Public Property PUC As PlanUnicoCuentas

        <JsonProperty>
        Public Property ListadoImpuestos As IEnumerable(Of Impuestos)


    End Class
End Namespace
