﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="UnidadEmpaque"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class UnidadEmpaque
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="UnidadEmpaque"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="UnidadEmpaque"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre_Corto = Read(lector, "Nombre_Corto")
            Nombre = Read(lector, "Descripcion")
            Estado = Read(lector, "Estado")

        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Nombre_Corto As String

    End Class
End Namespace
