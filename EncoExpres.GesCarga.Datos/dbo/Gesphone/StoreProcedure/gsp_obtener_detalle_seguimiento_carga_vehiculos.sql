﻿PRINT 'gsp_obtener_detalle_seguimiento_carga_vehiculos'
GO
DROP PROCEDURE gsp_obtener_detalle_seguimiento_carga_vehiculos
GO
CREATE PROCEDURE gsp_obtener_detalle_seguimiento_carga_vehiculos
(
@par_EMPR_Codigo SMALLINT,
@par_Numero NUMERIC
)
AS
BEGIN 
	SELECT 
		DECV.EMPR_Codigo,
		DECV.ID,
		DECV.Numero_Documento,
		DECV.ENSV_Codigo,
		DECV.TIDO_Soporte,
		DECV.Numero_Documento_Soporte,
		DECV.Numero_Remesa,
		DECV.Fecha_Hora_Reporte,
		DECV.CATA_TIRS_Codigo,
		DECV.TERC_Codigo_Cliente,
		DECV.CATA_NOSC_Codigo,
		DECV.Observaciones,
		DECV.Identificacion_Recibido_Cliente,
		DECV.Nombre_Recibido_Cliente,
		DECV.CIUD_Recibido_Cliente,
		DECV.Direccion_Recibido_Cliente,
		DECV.Telefono_Recibido_Cliente,
		DECV.Cumplir,
		DECV.Reportar_Cliente,
		DECV.Estado,
		DECV.Anulado,
		DECV.Firma,
		TIDO.Nombre AS TipoDocumento,
		NOSC.Nombre AS NovedadSeguimiento,
		TIRS.Nombre AS TipoSeguimiento,
		CASE WHEN TERC.CATA_TINT_Codigo = 1202 THEN TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 ELSE TERC.Razon_Social END As NombreCliente,
		CIUD.Nombre AS Ciudad
	FROM 
		Detalle_Seguimiento_Carga_Vehiculos DECV,
		Tipo_Documentos TIDO,
		V_Novedad_Seguimiento_Carga NOSC,
		V_Tipo_Reporte_Seguimiento TIRS,
		Terceros TERC,
		Ciudades CIUD

	WHERE 
		DECV.EMPR_Codigo = @par_EMPR_Codigo
		AND DECV.ID = @par_Numero

		AND DECV.EMPR_Codigo = TIDO.EMPR_Codigo 
		AND DECV.TIDO_Soporte = TIDO.Codigo

		AND DECV.EMPR_Codigo = NOSC.EMPR_Codigo 
		AND DECV.CATA_NOSC_Codigo = NOSC.Codigo

		AND DECV.EMPR_Codigo = TIRS.EMPR_Codigo 
		AND DECV.CATA_TIRS_Codigo = TIRS.Codigo

		AND DECV.EMPR_Codigo = TERC.EMPR_Codigo 
		AND DECV.TERC_Codigo_Cliente = TERC.Codigo

		AND DECV.EMPR_Codigo = CIUD.EMPR_Codigo 
		AND DECV.CIUD_Recibido_Cliente = CIUD.Codigo
END
GO