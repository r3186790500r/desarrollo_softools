﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Mantenimiento

Namespace Mantenimiento
    Public NotInheritable Class RepositorioDetalleTareaMantenimiento
        Inherits RepositorioBase(Of DetalleTareaMantenimiento)

        Public Overrides Function Consultar(filtro As DetalleTareaMantenimiento) As IEnumerable(Of DetalleTareaMantenimiento)
            Dim lista As New List(Of DetalleTareaMantenimiento)
            Dim item As DetalleTareaMantenimiento
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_detalle_tarea_equipo_mantenimientos]")

                While resultado.Read
                    item = New DetalleTareaMantenimiento(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleTareaMantenimiento) As Long
            Throw New NotImplementedException()
        End Function

        Public Function InsertarDetalle(entidad As DetalleTareaMantenimiento, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleTareaMantenimiento) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleTareaMantenimiento) As DetalleTareaMantenimiento
            Throw New NotImplementedException()
        End Function

        Public Function Anular(entidad As DetalleTareaMantenimiento) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
