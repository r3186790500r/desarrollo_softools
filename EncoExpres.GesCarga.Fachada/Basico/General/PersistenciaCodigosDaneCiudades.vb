﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.General

    Public NotInheritable Class PersistenciaCodigosDaneCiudades
        Implements IPersistenciaBase(Of CodigoDaneCiudades)

        Public Function Consultar(filtro As CodigoDaneCiudades) As IEnumerable(Of CodigoDaneCiudades) Implements IPersistenciaBase(Of CodigoDaneCiudades).Consultar
            Return New RepositorioCodigosDaneCiudades().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As CodigoDaneCiudades) As Long Implements IPersistenciaBase(Of CodigoDaneCiudades).Insertar
            Return New RepositorioCodigosDaneCiudades().Insertar(entidad)

        End Function

        Public Function Modificar(entidad As CodigoDaneCiudades) As Long Implements IPersistenciaBase(Of CodigoDaneCiudades).Modificar
            Return New RepositorioCodigosDaneCiudades().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As CodigoDaneCiudades) As CodigoDaneCiudades Implements IPersistenciaBase(Of CodigoDaneCiudades).Obtener
            Return New RepositorioCodigosDaneCiudades().Obtener(filtro)
        End Function

        Public Function Anular(filtro As CodigoDaneCiudades) As Boolean
            Return New RepositorioCodigosDaneCiudades().Anular(filtro)
        End Function
    End Class
End Namespace
