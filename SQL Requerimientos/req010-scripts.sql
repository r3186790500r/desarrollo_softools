USE [GESCARGA50_DESA]
GO

DELETE FROM [dbo].[Permiso_Grupo_Usuarios]
      WHERE [EMPR_Codigo] = 10 AND [MEAP_Codigo] = CAST(470205 AS Numeric(18, 0))
GO

DELETE FROM [dbo].[Menu_Aplicaciones]
      WHERE [EMPR_Codigo] = 10 AND [Codigo] = CAST(470205 AS Numeric(18, 0))
GO

/*
FP 2021-12-22 Para restaurar datos existentes a la fecha eliminados en este comando

INSERT [dbo].[Menu_Aplicaciones] ([EMPR_Codigo], [Codigo], [MOAP_Codigo], [Nombre], [Padre_Menu], [Orden_Menu], [Mostrar_Menu], [Aplica_Habilitar], [Aplica_Consultar], [Aplica_Actualizar], [Aplica_Eliminar_Anular], [Aplica_Imprimir], [Aplica_Contabilidad], [Opcion_Permiso], [Opcion_Listado], [Aplica_Ayuda], [Url_Pagina], [Url_Ayuda], [Imagen]) VALUES (10, CAST(470205 AS Numeric(18, 0)), 47, N'Cumplir Gu�as', CAST(4702 AS Numeric(18, 0)), CAST(25 AS Numeric(18, 0)), 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, N'#!CumplirGuiasPaqueteria', N'#!', NULL)
GO

INSERT [dbo].[Permiso_Grupo_Usuarios] ([EMPR_Codigo], [MEAP_Codigo], [GRUS_Codigo], [Habilitar], [Consultar], [Actualizar], [Eliminar_Anular], [Imprimir], [Permiso], [Contabilidad]) VALUES (10, CAST(470205 AS Numeric(18, 0)), 1, 1, 1, 1, 1, 1, 0, 0)
INSERT [dbo].[Permiso_Grupo_Usuarios] ([EMPR_Codigo], [MEAP_Codigo], [GRUS_Codigo], [Habilitar], [Consultar], [Actualizar], [Eliminar_Anular], [Imprimir], [Permiso], [Contabilidad]) VALUES (10, CAST(470205 AS Numeric(18, 0)), 62, 1, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[Permiso_Grupo_Usuarios] ([EMPR_Codigo], [MEAP_Codigo], [GRUS_Codigo], [Habilitar], [Consultar], [Actualizar], [Eliminar_Anular], [Imprimir], [Permiso], [Contabilidad]) VALUES (10, CAST(470205 AS Numeric(18, 0)), 63, 0, 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Permiso_Grupo_Usuarios] ([EMPR_Codigo], [MEAP_Codigo], [GRUS_Codigo], [Habilitar], [Consultar], [Actualizar], [Eliminar_Anular], [Imprimir], [Permiso], [Contabilidad]) VALUES (10, CAST(470205 AS Numeric(18, 0)), 65, 0, 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Permiso_Grupo_Usuarios] ([EMPR_Codigo], [MEAP_Codigo], [GRUS_Codigo], [Habilitar], [Consultar], [Actualizar], [Eliminar_Anular], [Imprimir], [Permiso], [Contabilidad]) VALUES (10, CAST(470205 AS Numeric(18, 0)), 66, 0, 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Permiso_Grupo_Usuarios] ([EMPR_Codigo], [MEAP_Codigo], [GRUS_Codigo], [Habilitar], [Consultar], [Actualizar], [Eliminar_Anular], [Imprimir], [Permiso], [Contabilidad]) VALUES (10, CAST(470205 AS Numeric(18, 0)), 69, 0, 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Permiso_Grupo_Usuarios] ([EMPR_Codigo], [MEAP_Codigo], [GRUS_Codigo], [Habilitar], [Consultar], [Actualizar], [Eliminar_Anular], [Imprimir], [Permiso], [Contabilidad]) VALUES (10, CAST(470205 AS Numeric(18, 0)), 70, 0, 0, 0, 0, 0, 0, 0)
GO*/