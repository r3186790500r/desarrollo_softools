﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Repositorio

Namespace Basico.Tesoreria

    Public NotInheritable Class PersistenciaSucursalesSIESA
        Implements IPersistenciaBase(Of SucursalesSIESA)

        Public Function Consultar(filtro As SucursalesSIESA) As IEnumerable(Of SucursalesSIESA) Implements IPersistenciaBase(Of SucursalesSIESA).Consultar
            Return New RepositarioSucursalesSIESA().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As SucursalesSIESA) As Long Implements IPersistenciaBase(Of SucursalesSIESA).Insertar
            Return New RepositarioSucursalesSIESA().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As SucursalesSIESA) As Long Implements IPersistenciaBase(Of SucursalesSIESA).Modificar
            Return New RepositarioSucursalesSIESA().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As SucursalesSIESA) As SucursalesSIESA Implements IPersistenciaBase(Of SucursalesSIESA).Obtener
            Return New RepositarioSucursalesSIESA().Obtener(filtro)
        End Function

        Public Function Anular(entidad As SucursalesSIESA) As Boolean
            Return New RepositarioSucursalesSIESA().Anular(entidad)
        End Function

    End Class

End Namespace
