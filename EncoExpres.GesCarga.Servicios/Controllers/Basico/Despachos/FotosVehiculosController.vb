﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.Despachos


''' <summary>
''' Controlador <see cref="FotosVehiculosController"/>
''' </summary>
<Authorize>
Public Class FotosVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As FotosVehiculos) As Respuesta(Of IEnumerable(Of FotosVehiculos))
        Return New LogicaFotosVehiculos(CapaPersistenciaFotosVehiculos).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("InsertarTemporal")>
    Public Function Guardar(ByVal entidad As FotosVehiculos) As Respuesta(Of Long)
        Return New LogicaFotosVehiculos(CapaPersistenciaFotosVehiculos).InsertarTemporal(entidad)
    End Function
    <HttpPost>
    <ActionName("EliminarFoto")>
    Public Function EliminarFoto(ByVal entidad As FotosVehiculos) As Respuesta(Of Boolean)
        Return New LogicaFotosVehiculos(CapaPersistenciaFotosVehiculos).EliminarFoto(entidad)
    End Function
    <HttpPost>
    <ActionName("LimpiarTemporalUsuario")>
    Public Function LimpiarTemporalUsuario(ByVal entidad As FotosVehiculos) As Respuesta(Of Boolean)
        Return New LogicaFotosVehiculos(CapaPersistenciaFotosVehiculos).LimpiarTemporalUsuario(entidad)
    End Function

End Class
