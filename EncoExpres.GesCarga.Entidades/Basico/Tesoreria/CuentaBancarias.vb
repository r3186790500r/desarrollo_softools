﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="CuentaBancarias"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class CuentaBancarias
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="CuentaBancarias"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CuentaBancarias"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            Banco = New Bancos With {.Codigo = Read(lector, "BANC_Codigo"), .Nombre = Read(lector, "NombreBanco")}
            NumeroCuenta = Read(lector, "Numero_Cuenta")
            TipoCuentaBancaria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICB_Codigo"), .Nombre = Read(lector, "TipoCuentaBancaria")}
            Tercero = New Terceros With {.Codigo = Read(lector, "TERC_Codigo"), .NombreCompleto = Read(lector, "NombreTercero"), .NumeroIdentificacion = Read(lector, "Numero_Identificacion")}
            CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .Nombre = Read(lector, "NombreCuentaPUC")}
            SobregiroAutorizado = Read(lector, "Sobregiro_Autorizado")
            SaldoActual = Read(lector, "Saldo_Actual")


            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            Else

            End If
        End Sub

        <JsonProperty>
        Public Property Banco As Bancos
        <JsonProperty>
        Public Property NumeroCuenta As String
        <JsonProperty>
        Public Property TipoCuentaBancaria As ValorCatalogos
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Tercero As Terceros
        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property SobregiroAutorizado As Double
        <JsonProperty>
        Public Property SaldoActual As Double

    End Class

End Namespace
