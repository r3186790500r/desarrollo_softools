﻿Imports System.Data.OleDb
Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General

Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioVehiculos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioVehiculos
        Inherits RepositorioBase(Of Vehiculos)

        Public Overrides Function Consultar(filtro As Vehiculos) As IEnumerable(Of Vehiculos)
            Dim lista As New List(Of Vehiculos)
            Dim item As Vehiculos

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    If Not IsNothing(filtro.ValorAutocomplete) Or filtro.Sync = True Then
                        conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)
                        If filtro.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                        End If
                        If Not IsNothing(filtro.Conductor) Then
                            If filtro.Conductor.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.Codigo)
                            End If
                        End If
                        If Not IsNothing(filtro.Estado) Then 'FP 2021-12-22 Agrega opcion de autocomplete con parametro estado
                            conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                        End If
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_vehiculos_Autocomplete")
                        While resultado.Read
                            item = New Vehiculos(resultado)
                            lista.Add(item)
                        End While

                    Else
                        If filtro.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                        End If
                        If Not IsNothing(filtro.Placa) Then
                            If Len(filtro.Placa) > 0 Then
                                conexion.AgregarParametroSQL("@par_Placa", filtro.Placa)
                            End If
                        End If
                        If Not IsNothing(filtro.Semirremolque) Then
                            If Len(filtro.Semirremolque.Placa) > 0 Then
                                conexion.AgregarParametroSQL("@par_Semiremolque", filtro.Semirremolque.Placa)
                            End If
                        End If
                        If Not IsNothing(filtro.CodigoAlterno) Then
                            If Len(filtro.CodigoAlterno) > 0 Then
                                conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                            End If
                        End If
                        If Not IsNothing(filtro.Conductor) Then
                            If Len(filtro.Conductor.Nombre) > 0 Then
                                conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.Nombre)
                            End If
                        End If
                        If Not IsNothing(filtro.Tenedor) Then
                            If Len(filtro.Tenedor.Nombre) > 0 Then
                                conexion.AgregarParametroSQL("@par_Tenedor", filtro.Tenedor.Nombre)
                            End If
                        End If
                        If Not IsNothing(filtro.TipoVehiculo) Then
                            If filtro.TipoVehiculo.Codigo <> 2200 Then 'NO APLICA
                                conexion.AgregarParametroSQL("@par_Tipo_Vehiculo", filtro.TipoVehiculo.Codigo)
                            End If
                        End If
                        If Not IsNothing(filtro.Estado) Then
                            If filtro.Estado.Codigo > -1 Then 'NO APLICA
                                conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                            End If
                        End If
                        If Not IsNothing(filtro.Pagina) Then
                            If filtro.Pagina > 0 Then
                                conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                            End If
                        End If
                        If Not IsNothing(filtro.RegistrosPagina) Then
                            If filtro.RegistrosPagina > 0 Then
                                conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                            End If
                        End If
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_vehiculos]")

                        While resultado.Read
                            item = New Vehiculos(resultado)
                            lista.Add(item)
                        End While
                    End If
                    conexion.CloseConnection()

                End Using
            End Using

            Return lista
        End Function

        Public Function ConsultarPropietarios(filtro As Vehiculos) As IEnumerable(Of PropietarioVehiculo)
            Dim lista As New List(Of PropietarioVehiculo)
            Dim item As PropietarioVehiculo

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_propietarios_vehiculos")
                    While resultado.Read
                        item = New PropietarioVehiculo(resultado)
                        lista.Add(item)
                    End While

                End Using
            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Vehiculos) As Long
            Dim item = New Vehiculos
            Using conexionTemp = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexionTemp.CreateConnection()
                conexionTemp.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexionTemp.AgregarParametroSQL("@par_Filtro", entidad.Placa)
                Dim resultadoConsulta As IDataReader = conexionTemp.ExecuteReaderStoreProcedure("gsp_obtener_vehiculo_x_identificacion")
                While resultadoConsulta.Read
                    item = New Vehiculos(resultadoConsulta)
                End While
                resultadoConsulta.Close()
                conexionTemp.CleanParameters()
                conexionTemp.CloseConnection()
            End Using
            If item.Codigo > 0 Then
                entidad.Codigo = item.Codigo
                Return Modificar(entidad)
            Else
                Dim trasladoFotos As Boolean = False
                Dim limpioTemporal As Boolean = False
                Dim detalleFotos As New FotosVehiculos
                Dim lista As New List(Of Vehiculos)
                Dim TempEntidad As New Vehiculos
                Dim insertoDocumento As Boolean = True
                Dim inserto As Boolean = True
                Dim TrasladoDocumentos As Boolean = True
                Dim Documentos As New Repositorio.Basico.General.RepositorioDocumentos

                TempEntidad.CodigoEmpresa = entidad.CodigoEmpresa
                TempEntidad.Placa = entidad.Placa
                lista = Consultar(TempEntidad)
                If lista.Count > 0 Then
                    Return 1
                Else
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                        conexion.AgregarParametroSQL("@Codigo", entidad.Codigo) 'NUMERIC = 0,
                        conexion.AgregarParametroSQL("@par_Placa", entidad.Placa) 'varchar(20),
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno) 'varchar(20) = null,
                        If Not IsNothing(entidad.Semirremolque) Then
                            conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo) 'numeric = null,
                        End If
                        If Not IsNothing(entidad.Propietario) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Propietario", entidad.Propietario.Codigo) '	numeric = null,
                        End If
                        If Not IsNothing(entidad.Tenedor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor", entidad.Tenedor.Codigo) '	numeric = null,
                        End If
                        If Not IsNothing(entidad.Sucursal) Then
                            conexion.AgregarParametroSQL("@par_Sucursal", entidad.Sucursal) '	smallint,
                        End If
                        If Not IsNothing(entidad.Conductor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo) '	numeric = null,
                        End If
                        If Not IsNothing(entidad.Afiliador) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Afiliador", entidad.Afiliador.Codigo) '	numeric = null,
                        End If
                        If Not IsNothing(entidad.Color) Then
                            conexion.AgregarParametroSQL("@par_COVE_Codigo", entidad.Color.Codigo) '	numeric = null,
                        End If
                        If Not IsNothing(entidad.Marca) Then
                            conexion.AgregarParametroSQL("@par_MAVE_Codigo", entidad.Marca.Codigo) 'numeric = null,
                        End If
                        If Not IsNothing(entidad.Linea) Then
                            conexion.AgregarParametroSQL("@par_LIVE_Codigo", entidad.Linea.Codigo) '	numeric = null,
                        End If
                        conexion.AgregarParametroSQL("@par_Modelo", entidad.Modelo) 'smallint = null,
                        conexion.AgregarParametroSQL("@par_Modelo_Repotenciado", entidad.ModeloRepotenciado) '	smallint = null,
                        If Not IsNothing(entidad.TipoVehiculo) Then
                            conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo) '	numeric = null,
                        End If
                        conexion.AgregarParametroSQL("@par_Cilindraje", entidad.Cilindraje, SqlDbType.Decimal) 'numeric = null,
                        If Not IsNothing(entidad.TipoCarroceria) Then
                            conexion.AgregarParametroSQL("@par_CATA_TICA_Codigo", entidad.TipoCarroceria.Codigo) '	numeric = null,
                        End If
                        conexion.AgregarParametroSQL("@par_Numero_Ejes", entidad.NumeroEjes) '	smallint = null,
                        conexion.AgregarParametroSQL("@par_Numero_Motor", entidad.NumeroMotor) '	varchar(50) = null,
                        conexion.AgregarParametroSQL("@par_Numero_Serie", entidad.Chasis) '	varchar(50) = null,
                        conexion.AgregarParametroSQL("@par_Peso_Bruto", entidad.PesoBruto, SqlDbType.Decimal) 'numeric = null,
                        conexion.AgregarParametroSQL("@par_Kilometraje", entidad.Kilometraje) 'numeric = null,
                        If entidad.FechaActualizacionKilometraje > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Actuliza_Kilometraje", entidad.FechaActualizacionKilometraje, SqlDbType.DateTime) 'datetime = null,
                        End If
                        conexion.AgregarParametroSQL("@par_ESSE_Numero", 0) 'numeric = null,
                        If entidad.FechaUltimoCargue > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Ultimo_Cargue", entidad.FechaUltimoCargue, SqlDbType.DateTime) '	datetime = null,
                        End If
                        If Not IsNothing(entidad.Estado) Then
                            If entidad.Estado.Codigo > -1 Then 'NO APLICA
                                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                            End If
                        End If

                        'If Not IsNothing(entidad.EstadoVehiculos) Then
                        '    conexion.AgregarParametroSQL("@par_CATA_ESVE_Codigo", entidad.EstadoVehiculos.Codigo) 'numeric = null,
                        'End If

                        If Not IsNothing(entidad.UsuarioCrea) Then
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo) 'smallint = null,
                        End If
                        If Not IsNothing(entidad.TipoDueno) Then
                            conexion.AgregarParametroSQL("@par_CATA_TIDV_Codigo", entidad.TipoDueno.Codigo) 'numeric = null,
                        End If
                        conexion.AgregarParametroSQL("@par_Tarjeta_Propiedad", entidad.TarjetaPropiedad) 'numeric = null,
                        conexion.AgregarParametroSQL("@par_Capacidad", entidad.Capacidad, SqlDbType.Decimal) 'numeric = null,
                        conexion.AgregarParametroSQL("@par_Capacidad_Galones", entidad.CapacidadGalones, SqlDbType.Decimal) 'numeric = null,
                        conexion.AgregarParametroSQL("@par_Peso_Tara", entidad.PesoTara, SqlDbType.Decimal) 'numeric = null,
                        If Not IsNothing(entidad.ProveedorGPS) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor_GPS", entidad.ProveedorGPS.Codigo) '	numeric = null,
                        End If
                        conexion.AgregarParametroSQL("@par_URL_GPS", entidad.URLGPS) '	varchar = null,
                        conexion.AgregarParametroSQL("@par_Usuario_GPS", entidad.UsuarioGPS) '	varchar = null,
                        conexion.AgregarParametroSQL("@par_Clave_GPS", entidad.ClaveGPS) 'varchar = null,
                        conexion.AgregarParametroSQL("@par_TelefonoGPS", entidad.TelefonoGPS) '	numeric = null,
                        'If Not IsNothing(entidad.CapacidadVehiculo) Then
                        '    conexion.AgregarParametroSQL("@par_CATA_CAVE_Codigo", entidad.CapacidadVehiculo.Codigo) '	numeric = null,
                        'End If
                        If Not IsNothing(entidad.CausaInactividad) Then
                            conexion.AgregarParametroSQL("@par_CATA_CAIV_Codigo", entidad.CausaInactividad.Codigo) '	numeric = null,
                        End If
                        conexion.AgregarParametroSQL("@par_Justificacion_Bloqueo", entidad.JustificacionBloqueo) 'varchar (150) = null
                        If Not IsNothing(entidad.AseguradoraSOAT) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Aseguradora_SOAT", entidad.AseguradoraSOAT.Codigo) 'varchar (150) = null
                        End If
                        If entidad.FechaVencimientoSOAT > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Vencimiento_SOAT", entidad.FechaVencimientoSOAT, SqlDbType.DateTime) 'varchar (150) = null
                        End If
                        If entidad.ReferenciaSOAT <> String.Empty Then
                            conexion.AgregarParametroSQL("@par_Referencia_SOAT", entidad.ReferenciaSOAT) 'varchar (150) = null
                        End If
                        conexion.AgregarParametroSQL("@par_Identificador_GPS", entidad.IdentificadorGPS) '	numeric = null,

                        If Not IsNothing(entidad.CapacidadVolumen) Then
                            conexion.AgregarParametroSQL("@par_CapacidadVolumen", entidad.CapacidadVolumen)
                        End If

                        If entidad.FechaCrea > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Vinculacion", entidad.FechaCrea, SqlDbType.DateTime)
                        End If

                        If entidad.FechaModifica > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Inactivacion", entidad.FechaModifica, SqlDbType.DateTime)
                        End If

                        If Not IsNothing(entidad.TipoCombustible) Then
                            If entidad.TipoCombustible.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo", entidad.TipoCombustible.Codigo)
                            End If
                        End If

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_vehiculos")

                        While resultado.Read
                            entidad.Codigo = resultado.Item("Codigo").ToString()
                        End While

                        resultado.Close()
                        If entidad.Codigo.Equals(Cero) Then
                            Return Cero
                        Else
                            conexion.CloseConnection()
                            Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                            If Not IsNothing(entidad.Documentos) Then
                                For Each detalle In entidad.Documentos
                                    detalle.CodigoVehiculo = entidad.Codigo
                                    If Not Documento.InsertarDocumentoVehiculo(detalle) Then
                                        insertoDocumento = False
                                        Exit For
                                    End If
                                Next
                            End If

                            If Not IsNothing(entidad.DetalleFotos) Then
                                If entidad.DetalleFotos.Count() > 0 Then
                                    detalleFotos.CodigoEmpresa = entidad.CodigoEmpresa
                                    detalleFotos.CodigoVehiculo = entidad.Codigo
                                    detalleFotos.UsuarioCrea = entidad.UsuarioModifica
                                    Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                                        trasladoFotos = New RepositorioFotosVehiculos().TrasladarFotos(detalleFotos, conexionDocumentos)
                                        If trasladoFotos Then
                                            limpioTemporal = New RepositorioFotosVehiculos().LimpiarTemporalUsuario(detalleFotos)
                                        End If
                                    End Using
                                End If
                            End If

                        End If
                    End Using

                    Return entidad.Codigo
                End If
            End If
        End Function


        Public Function InsertarPropietarios(entidad As Vehiculos) As Long
            Dim item = New Vehiculos
            Using conexionTemp = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexionTemp.CreateConnection()
                conexionTemp.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexionTemp.AgregarParametroSQL("@par_Vehiculo", entidad.Codigo)
                Dim resultadoConsulta As IDataReader = conexionTemp.ExecuteReaderStoreProcedure("gsp_anular_propietarios_vehiculo")

                resultadoConsulta.Close()
                conexionTemp.CleanParameters()
                conexionTemp.CloseConnection()
            End Using

            For Each Propietario In entidad.Propietarios
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", Propietario.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", Propietario.Vehiculo.Codigo) 'NUMERIC = 0,
                    conexion.AgregarParametroSQL("@par_TERC_Codigo", Propietario.Propietario.Codigo) 'varchar(20),
                    conexion.AgregarParametroSQL("@par_Porcentaje_Participacion", Propietario.PorcentajeParticipacion, SqlDbType.Decimal) 'varchar(20) = null,


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_propietarios")

                    resultado.Close()

                End Using
            Next



            Return entidad.Codigo

        End Function
        Public Overrides Function Modificar(entidad As Vehiculos) As Long
            Dim trasladoFotos As Boolean = False
            Dim limpioTemporal As Boolean = False
            Dim detalleFotos As New FotosVehiculos
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim insertoDocumento As Boolean = True
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                conexion.AgregarParametroSQL("@Codigo", entidad.Codigo) 'NUMERIC = 0,
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno) 'varchar(20) = null,
                If Not IsNothing(entidad.Semirremolque) Then
                    conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo) 'numeric = null,
                End If
                If Not IsNothing(entidad.Propietario) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Propietario", entidad.Propietario.Codigo) '	numeric = null,
                End If
                If Not IsNothing(entidad.Tenedor) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor", entidad.Tenedor.Codigo) '	numeric = null,
                End If
                If Not IsNothing(entidad.Sucursal) Then
                    conexion.AgregarParametroSQL("@par_Sucursal", entidad.Sucursal) '	smallint,
                End If
                If Not IsNothing(entidad.Conductor) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo) '	numeric = null,
                End If
                If Not IsNothing(entidad.Afiliador) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Afiliador", entidad.Afiliador.Codigo) '	numeric = null,
                End If
                If Not IsNothing(entidad.Color) Then
                    conexion.AgregarParametroSQL("@par_COVE_Codigo", entidad.Color.Codigo) '	numeric = null,
                End If
                If Not IsNothing(entidad.Marca) Then
                    conexion.AgregarParametroSQL("@par_MAVE_Codigo", entidad.Marca.Codigo) 'numeric = null,
                End If
                If Not IsNothing(entidad.Linea) Then
                    conexion.AgregarParametroSQL("@par_LIVE_Codigo", entidad.Linea.Codigo) '	numeric = null,
                End If
                conexion.AgregarParametroSQL("@par_Modelo", entidad.Modelo) 'smallint = null,
                conexion.AgregarParametroSQL("@par_Modelo_Repotenciado", entidad.ModeloRepotenciado) '	smallint = null,
                If Not IsNothing(entidad.TipoVehiculo) Then
                    conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo) '	numeric = null,
                End If
                conexion.AgregarParametroSQL("@par_Cilindraje", entidad.Cilindraje, SqlDbType.Decimal) 'numeric = null,
                If Not IsNothing(entidad.TipoCarroceria) Then
                    conexion.AgregarParametroSQL("@par_CATA_TICA_Codigo", entidad.TipoCarroceria.Codigo) '	numeric = null,
                End If
                conexion.AgregarParametroSQL("@par_Numero_Ejes", entidad.NumeroEjes) '	smallint = null,
                conexion.AgregarParametroSQL("@par_Numero_Motor", entidad.NumeroMotor) '	varchar(50) = null,
                conexion.AgregarParametroSQL("@par_Numero_Serie", entidad.Chasis) '	varchar(50) = null,
                conexion.AgregarParametroSQL("@par_Peso_Bruto", entidad.PesoBruto, SqlDbType.Decimal) 'numeric = null,
                conexion.AgregarParametroSQL("@par_Kilometraje", entidad.Kilometraje) 'numeric = null,
                If entidad.FechaActualizacionKilometraje > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Actuliza_Kilometraje", entidad.FechaActualizacionKilometraje, SqlDbType.DateTime) 'datetime = null,
                End If
                conexion.AgregarParametroSQL("@par_ESSE_Numero", 0) 'numeric = null,
                If entidad.FechaUltimoCargue > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Ultimo_Cargue", entidad.FechaUltimoCargue, SqlDbType.DateTime) '	datetime = null,
                End If
                If Not IsNothing(entidad.Estado) Then
                    If entidad.Estado.Codigo > -1 Then 'NO APLICA
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                    End If
                End If
                'conexion.AgregarParametroSQL("@par_Estado", entidad.Estado) 'smallint = null,

                'If Not IsNothing(entidad.EstadoVehiculos) Then
                '    conexion.AgregarParametroSQL("@par_CATA_ESVE_Codigo", entidad.EstadoVehiculos.Codigo) 'numeric = null,
                'End If
                If Not IsNothing(entidad.UsuarioCrea) Then
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo) 'smallint = null,
                End If
                If Not IsNothing(entidad.TipoDueno) Then
                    conexion.AgregarParametroSQL("@par_CATA_TIDV_Codigo", entidad.TipoDueno.Codigo) 'numeric = null,
                End If
                conexion.AgregarParametroSQL("@par_Tarjeta_Propiedad", entidad.TarjetaPropiedad) 'numeric = null,
                conexion.AgregarParametroSQL("@par_Capacidad", entidad.Capacidad, SqlDbType.Decimal) 'numeric = null,
                conexion.AgregarParametroSQL("@par_Capacidad_Galones", entidad.CapacidadGalones, SqlDbType.Decimal) 'numeric = null,
                conexion.AgregarParametroSQL("@par_Peso_Tara", entidad.PesoTara, SqlDbType.Decimal) 'numeric = null,
                If Not IsNothing(entidad.ProveedorGPS) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor_GPS", entidad.ProveedorGPS.Codigo) '	numeric = null,
                End If
                conexion.AgregarParametroSQL("@par_URL_GPS", entidad.URLGPS) '	varchar = null,
                conexion.AgregarParametroSQL("@par_Usuario_GPS", entidad.UsuarioGPS) '	varchar = null,
                conexion.AgregarParametroSQL("@par_Clave_GPS", entidad.ClaveGPS) 'varchar = null,
                conexion.AgregarParametroSQL("@par_TelefonoGPS", entidad.TelefonoGPS) '	numeric = null,
                'If Not IsNothing(entidad.CapacidadVehiculo) Then
                '    conexion.AgregarParametroSQL("@par_CATA_CAVE_Codigo", entidad.CapacidadVehiculo.Codigo) '	numeric = null,
                'End If
                If Not IsNothing(entidad.CausaInactividad) Then
                    conexion.AgregarParametroSQL("@par_CATA_CAIV_Codigo", entidad.CausaInactividad.Codigo) '	numeric = null,
                End If
                conexion.AgregarParametroSQL("@par_Justificacion_Bloqueo", entidad.JustificacionBloqueo) 'varchar (150) = null
                If Not IsNothing(entidad.AseguradoraSOAT) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Aseguradora_SOAT", entidad.AseguradoraSOAT.Codigo) 'varchar (150) = null
                End If
                If entidad.FechaVencimientoSOAT > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Vencimiento_SOAT", entidad.FechaVencimientoSOAT, SqlDbType.DateTime) 'varchar (150) = null
                End If
                If entidad.ReferenciaSOAT <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Referencia_SOAT", entidad.ReferenciaSOAT) 'varchar (150) = null
                End If
                conexion.AgregarParametroSQL("@par_Identificador_GPS", entidad.IdentificadorGPS) '	numeric = null,
                If Not IsNothing(entidad.CapacidadVolumen) Then
                    conexion.AgregarParametroSQL("@par_CapacidadVolumen", entidad.CapacidadVolumen)
                End If
                If Not IsNothing(entidad.TipoCombustible) Then
                    If entidad.TipoCombustible.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo", entidad.TipoCombustible.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_vehiculos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While
                resultado.Close()
                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                Else
                    conexion.CloseConnection()
                    Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                    If Not IsNothing(entidad.Documentos) Then
                        For Each detalle In entidad.Documentos
                            detalle.CodigoVehiculo = entidad.Codigo
                            If Not Documento.InsertarDocumentoVehiculo(detalle) Then
                                insertoDocumento = False
                                Exit For
                            End If
                        Next
                    End If

                    If Not IsNothing(entidad.DetalleFotos) Then
                        If entidad.DetalleFotos.Count() > 0 Then
                            detalleFotos.CodigoEmpresa = entidad.CodigoEmpresa
                            detalleFotos.CodigoVehiculo = entidad.Codigo
                            detalleFotos.UsuarioCrea = entidad.UsuarioModifica
                            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                                trasladoFotos = New RepositorioFotosVehiculos().TrasladarFotos(detalleFotos, conexionDocumentos)
                                If trasladoFotos Then
                                    limpioTemporal = New RepositorioFotosVehiculos().LimpiarTemporalUsuario(detalleFotos)
                                End If
                            End Using
                        End If
                    End If

                End If
            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As Vehiculos) As Vehiculos
            Dim item As New Vehiculos
            Dim Doc As New Documentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo = 0 And IsNothing(filtro.Placa) And IsNothing(filtro.Sucursal) Then
                    Return item
                Else
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    If Not IsNothing(filtro.Placa) Then
                        conexion.AgregarParametroSQL("@par_Placa", filtro.Placa)
                    End If
                    If Not IsNothing(filtro.Sucursal) Then
                        conexion.AgregarParametroSQL("@par_Sucursal", filtro.Sucursal)
                    End If
                    If Not IsNothing(filtro.Estado) Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_vehiculo]")

                    While resultado.Read
                        item = New Vehiculos(resultado)
                    End While
                    resultado.Close()
                    item.ListaNovedades = ConsultarNovedadesVehiculo(filtro.CodigoEmpresa, filtro.Codigo, conexion, resultado)
                    conexion.CloseConnection()

                    Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                    Doc.CodigoEmpresa = filtro.CodigoEmpresa
                    Doc.Vehiculo = True
                    Doc.CodigoVehiculo = item.Codigo
                    item.Documentos = Documento.Consultar(Doc)


                End If

            End Using

            Return item
        End Function

        Public Function ConsultarNovedadesVehiculo(CodiEmpresa As Short, CodVehiculo As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of NovedadTercero)
            Dim lista As New List(Of NovedadTercero)
            Dim item As NovedadTercero
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", CodVehiculo)

            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_novedades_vehiculo")

            While resultado.Read
                item = New NovedadTercero(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function GuardarNovedad(entidad As Vehiculos) As Long
            Dim inserto As Short
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_NOVE_Codigo", entidad.Novedad.Codigo)
                conexion.AgregarParametroSQL("@par_Justificacion", entidad.JustificacionNovedad)
                conexion.AgregarParametroSQL("@par_Responsable", entidad.UsuarioCrea.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_novedad_vehiculo")

                While resultado.Read
                    inserto = resultado.Item("Numero")
                End While
            End Using
            Return inserto
        End Function

        Public Function Anular(entidad As Vehiculos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_Vehiculos]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function ConsultarVehiculoEstudioSeguridad(filtro As EstudioSeguridad) As EstudioSeguridad
            Dim item As New Vehiculos
            Dim objEstudio As New EstudioSeguridad
            Dim Reader As IDataReader
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero_Autorizacion", filtro.NumeroAutorizacion)

                    Reader = conexion.ExecuteReaderStoreProcedure("[dbo].[Obtener_vehiculo_estudio_seguridad]")
                    While Reader.Read
                        objEstudio = New EstudioSeguridad(Reader)
                    End While
                    Reader.Close()
                    If objEstudio.Numero > 0 Then
                        Dim lista As New List(Of EstudioSeguridadDocumentos)
                        Dim Seccion As EstudioSeguridadDocumentos
                        conexionDocumentos.CreateConnection()
                        conexionDocumentos.CleanParameters()
                        conexionDocumentos.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                        If objEstudio.Numero > 0 Then
                            conexionDocumentos.AgregarParametroSQL("@par_Numero", objEstudio.Numero)
                        End If

                        Reader = conexionDocumentos.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_estudio_seguridad_documentos]")
                        While Reader.Read
                            Seccion = New EstudioSeguridadDocumentos(Reader)
                            lista.Add(Seccion)
                        End While
                        objEstudio.ListadoDocumentos = lista
                    End If
                End Using
            End Using

            Return objEstudio
        End Function

        Public Function LimpiarTemporalUsuario(entidad As FotosVehiculos, ByRef contextoConexion As DataBaseFactory) As Boolean
            Dim inserto As Boolean = False
            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)
            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_limpiar_t_fotos_vehiculos]")

            While resultado.Read
                inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
            End While

            Return inserto
        End Function

        Public Function ConsultarDocumentosProximosVencer(entidad As VehiculosDocumentos) As IEnumerable(Of VehiculosDocumentos)
            Dim item As New VehiculosDocumentos
            Dim lista As New List(Of VehiculosDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                If Not IsNothing(entidad.Tercero) Then
                    If entidad.Tercero.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
                    End If
                End If
                If entidad.TipoAplicativo > 0 Then
                    conexion.AgregarParametroSQL("@par_CATA_APLI", entidad.TipoAplicativo)
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_listado_vehiculo_documentos_proximos_vencer]")

                While resultado.Read
                    item = New VehiculosDocumentos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
            End Using
            Return lista
        End Function

        Public Function ConsultarDocumentos(entidad As VehiculosDocumentos) As IEnumerable(Of VehiculosDocumentos)
            Dim item As New VehiculosDocumentos
            Dim lista As New List(Of VehiculosDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                If Not IsNothing(entidad.Vehiculo) Then
                    If entidad.Vehiculo.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    End If
                End If



                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_documentos_vehiculo")

                While resultado.Read
                    item = New VehiculosDocumentos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
            End Using
            Return lista
        End Function

        Public Function ConsultarDocumentosListaDocumentosPendientes(entidad As VehiculosDocumentos) As IEnumerable(Of VehiculosDocumentos)
            Dim item As New VehiculosDocumentos
            Dim lista As New List(Of VehiculosDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Detalle_EPOS_Codigo", entidad.CodigoDetalleEpos)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_documentos_proximos_vencer_vehiculos")

                While resultado.Read
                    item = New VehiculosDocumentos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
            End Using
            Return lista
        End Function

        Public Function ConsultarDocumentosSoatRTM(entidad As VehiculosDocumentos) As Boolean
            Dim VehiculoValido As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Detalle_EPOS_Codigo", entidad.CodigoDetalleEpos)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_validar_vehiculo_documentos_SOAT_RTM")

                While resultado.Read
                    VehiculoValido = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
                End While
                resultado.Close()
            End Using
            Return VehiculoValido
        End Function

        Public Function ConsultarEstadoListaNegra(entidad As VehiculosDocumentos) As Boolean
            Dim VehiculoValido As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_validar_vehiculo_lista_negra")

                While resultado.Read
                    VehiculoValido = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
                End While
                resultado.Close()
            End Using
            Return VehiculoValido
        End Function

        Public Function GenerarPlanitilla(filtro As Vehiculos) As Vehiculos
            If filtro.PlantillaDocumento > 0 Then
                Dim fullPath As String
                fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
                System.IO.File.Copy(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Documentos_Vehiculos.xlsx", fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Documentos_Vehiculos_Generado.xlsx", True)
                Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Documentos_Vehiculos_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"
                Dim Documentos As IEnumerable(Of ConfiguracionGestionDocumentos)
                Dim Documento As New ConfiguracionGestionDocumentos
                Dim ObjDocumento As New RepositorioGestionDocumentos
                Documento.CodigoEmpresa = filtro.CodigoEmpresa
                Documento.Estado = 1
                Documento.EntidadDocumento = New EntidadGestionDocumentos With {.Codigo = 1}
                Documentos = ObjDocumento.Consultar(Documento)
                Using cnn As New OleDbConnection(cn)
                    cnn.Open()
                    Documentos = Documentos.OrderBy(Function(a) a.Documento.Nombre)
                    For i = 1 To Documentos.Count
                        Dim row = i + 2
                        If Documentos(i - 1).Codigo <> 101 Then
                            Using cmd As OleDbCommand = cnn.CreateCommand()
                                cmd.CommandText = "INSERT INTO [Documentos$] (Codigo,Nombre) values(@Cod,@nom)"
                                cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Documentos(i - 1).Codigo
                                cmd.Parameters.AddWithValue("@nom", Documentos(i - 1).Documento.Nombre)
                                cmd.ExecuteNonQuery()
                            End Using
                        End If
                    Next

                    cnn.Close()
                End Using
                Dim RTA As New Vehiculos
                Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Documentos_Vehiculos_Generado.xlsx")
                RTA.Planitlla = Archivo
                Return RTA
            Else
                Dim fullPath As String
                fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
                System.IO.File.Copy(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Vehiculos.xlsx", fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Vehiculos_Generado.xlsx", True)
                Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Vehiculos_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"

                Dim Catalogos As IEnumerable(Of ValorCatalogos)
                Dim Catalogo As New ValorCatalogos
                Dim ObjValorCatalogos As New RepositorioValorCatalogos
                Catalogo.CodigoEmpresa = filtro.CodigoEmpresa
                Catalogo.Catalogo = New Catalogos With {.Codigo = 22}
                Catalogos = ObjValorCatalogos.Consultar(Catalogo)


                Dim Colores As IEnumerable(Of ColorVehiculos)
                Dim Color As New ColorVehiculos
                Dim ObjValorColores As New RepositorioColorVehiculos
                Color.CodigoEmpresa = filtro.CodigoEmpresa
                Color.Estado = 1
                Colores = ObjValorColores.Consultar(Color)


                Dim Marcas As IEnumerable(Of MarcaVehiculos)
                Dim Marca As New MarcaVehiculos
                Dim ObjValorMarcas As New RepositorioMarcaVehiculos
                Marca.CodigoEmpresa = filtro.CodigoEmpresa
                Marca.Estado = 1
                Marcas = ObjValorMarcas.Consultar(Marca)


                Dim Lineas As IEnumerable(Of LineaVehiculos)
                Dim Linea As New LineaVehiculos
                Dim ObjValorLineas As New RepositorioLineaVehiculos
                Linea.CodigoEmpresa = filtro.CodigoEmpresa
                Linea.Estado = 1
                Lineas = ObjValorLineas.Consultar(Linea)

                Using cnn As New OleDbConnection(cn)
                    cnn.Open()
                    Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [Tipo_Vehiculo$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                    Catalogo.CodigoEmpresa = filtro.CodigoEmpresa
                    Catalogo.Catalogo = New Catalogos With {.Codigo = 23}
                    Catalogos = ObjValorCatalogos.Consultar(Catalogo)
                    Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [Tipo_Carroceria$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                    Colores = Colores.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Colores.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [Color$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Colores(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Colores(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                    Marcas = Marcas.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Marcas.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [Marca$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Marcas(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Marcas(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                    Lineas = Lineas.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Lineas.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [Linea$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Lineas(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Lineas(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next

                    cnn.Close()
                End Using
                Dim RTA As New Vehiculos
                Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Vehiculos_Generado.xlsx")
                RTA.Planitlla = Archivo
                Return RTA
            End If
        End Function

        Public Function ConsultarVehiculosListaNegra(filtro As Vehiculos) As IEnumerable(Of Vehiculos)
            Dim lista As New List(Of Vehiculos)
            Dim item As Vehiculos

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    If Not IsNothing(filtro.Placa) Then
                        If Len(filtro.Placa) > 0 Then
                            conexion.AgregarParametroSQL("@par_Placa", filtro.Placa)
                        End If
                    End If

                    If Not IsNothing(filtro.CodigoAlterno) Then
                        If Len(filtro.CodigoAlterno) > 0 Then
                            conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                        End If
                    End If

                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_vehiculos_lista_negra]")

                    While resultado.Read
                        item = New Vehiculos(resultado)
                        lista.Add(item)
                    End While
                    conexion.CloseConnection()

                End Using
            End Using

            Return lista
        End Function

        Public Function ConsultarAuditoria(filtro As Vehiculos) As IEnumerable(Of Vehiculos)
            Dim lista As New List(Of Vehiculos)
            Dim item As Vehiculos

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()


                    If filtro.FechaInicial > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                    End If

                    If filtro.FechaFinal > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                    End If

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_VEHI_Codigo", filtro.Codigo)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Consultar_auditoria_vehiculos")

                    While resultado.Read
                        item = New Vehiculos(resultado)
                        lista.Add(item)
                    End While
                    conexion.CloseConnection()

                End Using
            End Using

            Return lista
        End Function

    End Class
End Namespace