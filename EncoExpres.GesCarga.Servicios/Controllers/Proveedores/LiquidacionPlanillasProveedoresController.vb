﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Proveedores
Imports EncoExpres.GesCarga.Negocio.Proveedores

<Authorize>
Public Class LiquidacionPlanillasProveedoresController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As LiquidacionPlanillasProveedores) As Respuesta(Of IEnumerable(Of LiquidacionPlanillasProveedores))
        Return New LogicaLiquidacionPlanillasProveedores(CapaPersistenciaLiquidacionPlanillasDespachosProveedores).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As LiquidacionPlanillasProveedores) As Respuesta(Of LiquidacionPlanillasProveedores)
        Return New LogicaLiquidacionPlanillasProveedores(CapaPersistenciaLiquidacionPlanillasDespachosProveedores).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As LiquidacionPlanillasProveedores) As Respuesta(Of Long)
        Return New LogicaLiquidacionPlanillasProveedores(CapaPersistenciaLiquidacionPlanillasDespachosProveedores).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As LiquidacionPlanillasProveedores) As Respuesta(Of Boolean)
        Return New LogicaLiquidacionPlanillasProveedores(CapaPersistenciaLiquidacionPlanillasDespachosProveedores).Anular(entidad)
    End Function
End Class
