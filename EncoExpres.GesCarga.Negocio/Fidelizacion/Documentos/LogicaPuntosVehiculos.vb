﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Fidelizacion.Documentos
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Fidelizacion.Documentos
    Public NotInheritable Class LogicaPuntosVehiculos
        Inherits LogicaBase(Of PuntosVehiculos)

        ReadOnly _persistencia As IPersistenciaBase(Of PuntosVehiculos)

        Public Sub New(capaPersistenciaPuntosVehiculos As IPersistenciaBase(Of PuntosVehiculos))
            _persistencia = capaPersistenciaPuntosVehiculos
        End Sub


        Public Overrides Function Consultar(filtro As PuntosVehiculos) As Respuesta(Of IEnumerable(Of PuntosVehiculos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PuntosVehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PuntosVehiculos))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As PuntosVehiculos) As Respuesta(Of PuntosVehiculos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PuntosVehiculos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of PuntosVehiculos)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As PuntosVehiculos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Vehiculo.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function
    End Class
End Namespace

