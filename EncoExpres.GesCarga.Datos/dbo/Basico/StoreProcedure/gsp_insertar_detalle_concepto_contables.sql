﻿PRINT 'gsp_insertar_detalle_concepto_contables'
GO
DROP PROCEDURE gsp_insertar_detalle_concepto_contables
GO
CREATE PROCEDURE gsp_insertar_detalle_concepto_contables
(
@par_EMPR_Codigo SMALLINT,
@par_ECCO_Codigo NUMERIC,
@par_PLUC_Codigo SMALLINT,
@par_CATA_NACC_Codigo NUMERIC,
@par_CATA_TCUC_Codigo NUMERIC,
@par_CATA_DOCR_Codigo NUMERIC,
@par_CATA_TEPC_Codigo NUMERIC,
@par_CATA_CCPC_Codigo NUMERIC,
@par_Valor_Base MONEY,
@par_Porcentaje_Tarifa NUMERIC (18,2),
@par_Genera_Cuenta SMALLINT,
@par_Prefijo VARCHAR(10),
@par_Codigo_Anexo VARCHAR(10),
@par_Sufijo_Codigo_Anexo VARCHAR(10),
@par_Campo_Auxiliar VARCHAR(10)
)
AS
BEGIN

	INSERT INTO Detalle_Concepto_Contables
	(
		EMPR_Codigo,
		ECCO_Codigo,
		PLUC_Codigo,
		CATA_NACC_Codigo,
		CATA_TCUC_Codigo,
		CATA_DOCR_Codigo,
		CATA_TEPC_Codigo,
		CATA_CCPC_Codigo,
		Valor_Base,
		Porcentaje_Tarifa,
		Genera_Cuenta,
		Prefijo,
		Codigo_Anexo,
		Sufijo_Codigo_Anexo,
		Campo_Auxiliar
	)
	VALUES 
	(
		@par_EMPR_Codigo,
		@par_ECCO_Codigo,
		@par_PLUC_Codigo,
		@par_CATA_NACC_Codigo,
		@par_CATA_TCUC_Codigo,
		@par_CATA_DOCR_Codigo,
		@par_CATA_TEPC_Codigo,
		@par_CATA_CCPC_Codigo,
		@par_Valor_Base,
		@par_Porcentaje_Tarifa,
		@par_Genera_Cuenta,
		@par_Prefijo,
		@par_Codigo_Anexo,
		@par_Sufijo_Codigo_Anexo,
		@par_Campo_Auxiliar
	)
	SELECT @@IDENTITY AS Codigo
END
GO