﻿Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Repositorio.ControlViajes

Namespace ControlViajes
    Public NotInheritable Class PersistenciaInspeccionPreoperacional
        Implements IPersistenciaBase(Of InspeccionPreoperacional)

        Public Function Consultar(filtro As InspeccionPreoperacional) As IEnumerable(Of InspeccionPreoperacional) Implements IPersistenciaBase(Of InspeccionPreoperacional).Consultar
            Return New RepositorioInspeccionPreoperacional().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As InspeccionPreoperacional) As Long Implements IPersistenciaBase(Of InspeccionPreoperacional).Insertar
            Return New RepositorioInspeccionPreoperacional().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As InspeccionPreoperacional) As Long Implements IPersistenciaBase(Of InspeccionPreoperacional).Modificar
            Return New RepositorioInspeccionPreoperacional().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As InspeccionPreoperacional) As InspeccionPreoperacional Implements IPersistenciaBase(Of InspeccionPreoperacional).Obtener
            Return New RepositorioInspeccionPreoperacional().Obtener(filtro)
        End Function

        Public Function Anular(entidad As InspeccionPreoperacional) As Boolean
            Return New RepositorioInspeccionPreoperacional().Anular(entidad)
        End Function


        Public Function ObtenerDocumentoActivo(filtro As InspeccionPreoperacional) As InspeccionPreoperacional
            Return New RepositorioInspeccionPreoperacional().ObtenerDocumentoActivo(filtro)
        End Function
        Public Function ObtenerUltimaInspeccion(filtro As InspeccionPreoperacional) As InspeccionPreoperacional
            Return New RepositorioInspeccionPreoperacional().ObtenerUltimaInspeccion(filtro)
        End Function

    End Class

End Namespace

