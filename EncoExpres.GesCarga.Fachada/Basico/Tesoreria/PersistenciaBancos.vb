﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Repositorio.Basico.Tesoreria

Namespace Basico.Tesoreria
    Public NotInheritable Class PersistenciaBancos
        Implements IPersistenciaBase(Of Bancos)

        Public Function Consultar(filtro As Bancos) As IEnumerable(Of Bancos) Implements IPersistenciaBase(Of Bancos).Consultar
            Return New RepositorioBancos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Bancos) As Long Implements IPersistenciaBase(Of Bancos).Insertar
            Return New RepositorioBancos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Bancos) As Long Implements IPersistenciaBase(Of Bancos).Modificar
            Return New RepositorioBancos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Bancos) As Bancos Implements IPersistenciaBase(Of Bancos).Obtener
            Return New RepositorioBancos().Obtener(filtro)
        End Function

    End Class

End Namespace

