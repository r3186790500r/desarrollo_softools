﻿PRINT 'gsp_modificar_novedades_despachos'
GO
DROP PROCEDURE gsp_modificar_novedades_despachos
GO
CREATE PROCEDURE gsp_modificar_novedades_despachos 
(      
@par_EMPR_Codigo SMALLINT,      
@par_Codigo NUMERIC,      
@par_Codigo_Alterno VARCHAR(50) = NULL,      
@par_Nombre VARCHAR(50),       
@par_Estado SMALLINT,        
@par_USUA_Codigo_Modifica SMALLINT      
)      
AS      
BEGIN      
UPDATE Novedades_Despacho      
SET      
Codigo_Alterno = @par_Codigo_Alterno,      
Nombre = @par_Nombre,         
Estado = @par_Estado,         
USUA_Codigo_Modifica =  @par_USUA_Codigo_Modifica,      
Fecha_Modifica = GETDATE()      
WHERE      
EMPR_Codigo = @par_EMPR_Codigo      
AND Codigo = @par_Codigo      
      
SELECT @par_Codigo AS Codigo      
      
END      
GO