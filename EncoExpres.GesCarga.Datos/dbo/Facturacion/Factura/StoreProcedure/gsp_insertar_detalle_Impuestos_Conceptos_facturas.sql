﻿
Print 'CREATE PROCEDURE gsp_insertar_detalle_Impuestos_Conceptos_facturas'
GO
DROP PROCEDURE gsp_insertar_detalle_Impuestos_Conceptos_facturas
GO

CREATE PROCEDURE gsp_insertar_detalle_Impuestos_Conceptos_facturas (  
@par_EMPR_Codigo	 smallint,  
@par_ENFA_Numero	 numeric, 
@par_DECF_Codigo 	 numeric, 
@par_ENIM_Codigo 	 numeric,  
@par_Valor_tarifa 	 MONEY, 
@par_Valor_base      MONEY,  
@par_Valor_impuesto  MONEY
)  
AS  
BEGIN  
  
 INSERT INTO Detalle_Impuestos_Conceptos_Facturas (  
 EMPR_Codigo,  
 ENFA_Numero,  
 DECF_Codigo, 
 ENIM_Codigo,  
 Valor_Tarifa,
 Valor_Base,
 Valor_Impuesto
 )  
 VALUES(  
 @par_EMPR_Codigo,  
 @par_ENFA_Numero, 
 @par_DECF_Codigo,
 @par_ENIM_Codigo,
 @par_Valor_tarifa,
 @par_Valor_base,
 @par_Valor_impuesto
 )
 
 SELECT ENFA_Numero
 FROM Detalle_Impuestos_Conceptos_Facturas
 WHERE EMPR_Codigo = @par_EMPR_Codigo  
 AND ENFA_Numero = @par_ENFA_Numero 
 AND DECF_Codigo = @par_DECF_Codigo
 AND ENIM_Codigo = @par_ENIM_Codigo
 
END  
GO
