﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento

    Public NotInheritable Class PersistenciaTareaMantenimiento
        Implements IPersistenciaBase(Of TareaMantenimiento)
        Public Function Consultar(filtro As TareaMantenimiento) As IEnumerable(Of TareaMantenimiento) Implements IPersistenciaBase(Of TareaMantenimiento).Consultar
            Return New RepositorioTareaMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TareaMantenimiento) As Long Implements IPersistenciaBase(Of TareaMantenimiento).Insertar
            Return New RepositorioTareaMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TareaMantenimiento) As Long Implements IPersistenciaBase(Of TareaMantenimiento).Modificar
            Return New RepositorioTareaMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TareaMantenimiento) As TareaMantenimiento Implements IPersistenciaBase(Of TareaMantenimiento).Obtener
            Return New RepositorioTareaMantenimiento().Obtener(filtro)
        End Function

        Public Function Anular(entidad As TareaMantenimiento) As Boolean
            Return New RepositorioTareaMantenimiento().Anular(entidad)
        End Function

    End Class
End Namespace
