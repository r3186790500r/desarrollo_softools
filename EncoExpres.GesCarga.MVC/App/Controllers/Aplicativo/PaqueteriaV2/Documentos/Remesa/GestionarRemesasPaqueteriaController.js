﻿EncoExpresApp.controller("GestionarRemesasPaqueteriaCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TarifarioVentasFactory',
    'ValorCatalogosFactory', 'TercerosFactory', 'ProductoTransportadosFactory', 'CiudadesFactory', 'RemesaGuiasFactory', 'OficinasFactory',
    'blockUIConfig', 'SitiosTerceroClienteFactory', 'SitiosCargueDescargueFactory', 'ServicioSIESAFactory', 'ZonasFactory', 'DocumentosFactory', 'PlanillaPaqueteriaFactory', 'GuiasPreimpresasFactory', 'CierreContableDocumentosFactory', 'EtiquetasPreimpresasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TarifarioVentasFactory,
        ValorCatalogosFactory, TercerosFactory, ProductoTransportadosFactory, CiudadesFactory, RemesaGuiasFactory, OficinasFactory,
        blockUIConfig, SitiosTerceroClienteFactory, SitiosCargueDescargueFactory, ServicioSIESAFactory, ZonasFactory, DocumentosFactory, PlanillaPaqueteriaFactory, GuiasPreimpresasFactory, CierreContableDocumentosFactory, EtiquetasPreimpresasFactory) {
        //--------------------------Variables-------------------------//
        console.clear();
        var MeapCodigo = 0;
        var OPCION_MENU = OPCION_MENU_PAQUETERIA.REMESAS;
        var RemesaGesphone = false;
        $scope.CountInt = 0
        $scope.PlanillaPaqueteria = {};
        $scope.ExistePlanilla = false;

        var CodigoCiudadOrigen = '';
        var CodigoCiudadDestino = '';
        $scope.PesoNormal = true;
        $scope.DeshabilitarOficinaDestino = false;
        $scope.ListadoDireccionesRemitente = [];
        $scope.ListadoFormaPago = [];
        $scope.TMPListadoFormaPago = [];
        $scope.Obtenida = false;
        $scope.FleteAnterior = 0;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,
            Remesa: {
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                Numero: 0,
                TipoRemesaDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },
                //TipoRemesa: {Codigo: }
                DetalleTarifaVenta: {},
                ProductoTransportado: '',
                Fecha: new Date(),
                Remitente: { Direccion: '' },
                Destinatario: { Direccion: '' },
                Numeracion: '',
                RemesaCortesia: false,
                Zona: { Codigo: -1 }
            }
        };
        //--Limite Fecha
        $scope.MaxFecha = new Date();
        $scope.MinFecha = new Date($scope.Modelo.Remesa.Fecha);
        $scope.MinFecha.setDate($scope.Modelo.Remesa.Fecha.getDate() - 4);
        //--Receptoria
        $scope.Comision = 0;
        $scope.Agencista = 0;
        var TMPValorFleteCliente = 0;
        var TMPPorcentajeAfiliado = 0;
        var TMPPorcentajeSeguro = 0;
        var OficinaObj;
        //--Receptoria
        var dataRemitente = 0;
        var TMPRemitente = "";
        var TMPDestinatario = "";

        $scope.ManejoDetalleUnidades = false;
        $scope.ManejoUnidadesMensajeria = false;

        $scope.ListaReexpedicionOficinas = [];
        $scope.ListadoDetalleUnidades = [];
        $scope.Modal = {};
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_DEFINITIVO);

        $scope.ListadoCliente = [];
        $scope.ListadoRemitente = [];
        $scope.ListadoDestinatario = [];
        $scope.ListadoProveedor = [];
        $scope.ListadoAforador = [];
        $scope.ListadoProductoTransportados = [];
        $scope.ListadoCiudades = [];
        $scope.ListadoSitiosCargueAlterno2 = [];
        $scope.ListadoSitiosDescargueAlterno2 = [];
        $scope.ListaOficinas = [];
        $scope.ListaOficinasRegistroManual = [];
        $scope.ListaCondicionesComerciales = [];
        $scope.ListadoOficinaRecibe = [];
        $scope.ListaGestionDocuCliente = [];
        $scope.ListadoConfirmaEntrega = [
            { Codigo: -1, Nombre: "(SELECCIONAR)" },
            { Codigo: 0, Nombre: "NO" },
            { Codigo: 1, Nombre: "SI" }
        ];
        $scope.MensajesErrorDocu = [];
        $scope.TarifarioInvalido = false;
        $scope.MensajesErrorTarifas = [];
        var FormasPagoCliente = [];
        //--Documental
        $scope.ListadoDocumentos = [];
        $scope.CadenaFormatos = ".BMP, .GIF, .JPG, .JPEG, .TIF, .TIFF, .PNG, .doc, .docx, .docm, .dotx, .dotm, .dot, .xps, .txt, .xml, .odt, .xlsx, .xlsm, .xlsb, .xltx, .xltm, .xls, .xlt, .xml, .xlam, .xla, .xlw, .xlr, .pdf"
        //--Guias Preimpresos
        var PreimpresoValido = true;
        $scope.MensajesErrorPreimpreso = [];
        var tarifaConReexpediccion = false;

        $scope.MostrarApellidoRemitente = true;
        $scope.MostrarApellidoDestinatario = true;
        //--Cambio Valores Totales
        $scope.DeshabilitarTotales = true;
        //--Multiples tarifarios
        $scope.TarifariosPaq = '';
        $scope.Tarifario = '';
        $scope.MultiplesTarifarios = false;
        $scope.ClienteSinTarifarioValido = false;
        //--Genericos
        $scope.RemitenteGenerico = false;
        $scope.DestinatarioGenerico = false;
        //--Tarifa Productto para detalle unidades
        $scope.AplicaTarifaProducto = -1;
        //--Etiquetas preimpresas vs unidades
        $scope.EtiquetasIncompletas = false;
        //--FP 2022 01 28 creación de guia por etapas
        $scope.EditarEncabezado = true;
        //--direccion principal remitente y destinatario
        var DireccionPrincipalRemitente = '';
        var DireccionPrincipalDestinatario = '';
        //--Identificar cambios ciudad
        var codigoPrevCiudadRemitente = -1;
        var codigoPrevCiudadDestinatario = -1;
        //--------------------------Variables-------------------------//
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'GESTIONAR GUÍA';
        $scope.MapaSitio = $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Guía' }, { Nombre: 'Gestionar' }];
        $scope.Master = "#!ConsultarRemesasPaqueteria";
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        if ($routeParams.MeapCodigo !== undefined && $routeParams.MeapCodigo !== null && $routeParams.MeapCodigo !== '' && $routeParams.MeapCodigo !== 0) {
            MeapCodigo = parseInt($routeParams.MeapCodigo);
            if (MeapCodigo > 0) {
                switch (MeapCodigo) {
                    case OPCION_MENU_PAQUETERIA.REMESAS:
                        OPCION_MENU = OPCION_MENU_PAQUETERIA.REMESAS;
                        break;
                    case OPCION_MENU_PAQUETERIA.REMESAS_GESPHONE:
                        OPCION_MENU = OPCION_MENU_PAQUETERIA.REMESAS_GESPHONE;
                        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Guías' }, { Nombre: 'Gestionar' }];
                        RemesaGesphone = true;
                        break;
                }
            }
        }

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
            $scope.DeshabilitarCrear = $scope.Permisos.DeshabilitarCrear;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }

        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        $scope.ManejoPesoVolumetrico = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria;
        $scope.ManejoReexpedicionOficina = $scope.Sesion.UsuarioAutenticado.ManejoReexpedicionPorOficina;
        $scope.ManejoSitiosCargueDescargueGeneral = $scope.Sesion.UsuarioAutenticado.ManejoSitioCargueDescargueGeneral;
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete
        //--Clientes
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //--Clientes
        //--Remitente
        $scope.AutocompleteRemitente = function (value) {
            if (!$scope.RemitenteGenerico) {
                if (value.length > 0) {
                    if (value.length >= MIN_LEN_AUTOCOMLETE) {
                        blockUIConfig.autoBlock = false;
                        var Response = TercerosFactory.Consultar({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            CadenaPerfiles: PERFIL_REMITENTE,
                            ValorAutocomplete: value,
                            Estado: { Codigo: ESTADO_DEFINITIVO },
                            Sync: true
                        });
                        $scope.ListadoRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoRemitente);
                    }
                }
                return $scope.ListadoRemitente;
            }
        };
        //--Remitente
        //--Destinatario
        $scope.AutocompleteDestinatario = function (value) {
            if (!$scope.DestinatarioGenerico) {
                if (value.length > 0) {
                    if (value.length >= MIN_LEN_AUTOCOMLETE) {
                        blockUIConfig.autoBlock = false;
                        var Response = TercerosFactory.Consultar({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            CadenaPerfiles: PERFIL_DESTINATARIO,
                            ValorAutocomplete: value,
                            Estado: { Codigo: ESTADO_DEFINITIVO },
                            Sync: true
                        });
                        $scope.ListadoDestinatario = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoDestinatario);
                    }
                }
                return $scope.ListadoDestinatario;
            }
        };
        //--Destinatario
        //--Proveedor
        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_PROVEEDOR,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoProveedor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedor);
                }
            }
            return $scope.ListadoProveedor;
        };
        //--Proveedor
        //--Aforador
        $scope.AutocompleteAforador = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_AFORADOR,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoAforador = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoAforador);
                }
            }
            return $scope.ListadoAforador;
        };
        //--Aforador
        //--Producto Transportado
        $scope.AutocompleteProductoTransportado = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoProductoTransportados = [];
                    blockUIConfig.autoBlock = false;
                    var Response = ProductoTransportadosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        AplicaTarifario: $scope.AplicaTarifaProducto,
                        Sync: true
                    });
                    $scope.ListadoProductoTransportados = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProductoTransportados);
                }
            }
            return $scope.ListadoProductoTransportados;
        };
        //--Producto Transportado
        //--Ciudades
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        };
        //--Ciudades
        //--Sitios Cargue Cliente
        $scope.AutocompleteSitiosClienteCargue = function (value, cliente, ciudad) {
            $scope.ListadoSitiosCargueAlterno2 = [];
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = "";
                    if ($scope.ManejoSitiosCargueDescargueGeneral == true) {
                        //--Sitios de cargue generales
                        Response = SitiosCargueDescargueFactory.Consultar({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Ciudad: { Codigo: ciudad.Codigo == undefined ? 0 : ciudad.Codigo },
                            ValorAutocomplete: value,
                            Estado: ESTADO_ACTIVO,
                            Sync: true
                        });
                    }
                    else {
                        //--Sitios de cargue asignados al cliente
                        Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Cliente: { Codigo: cliente.Codigo == undefined ? 0 : ciudad.Codigo },
                            CiudadCargue: { Codigo: ciudad.Codigo == undefined ? 0 : ciudad.Codigo },
                            ValorAutocomplete: value,
                            Sync: true
                        });
                        for (var i = 0; i < Response.Datos.length; i++) {
                            Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo;
                        }
                    }
                    $scope.ListadoSitiosCargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosCargueAlterno2);
                }
            }
            return $scope.ListadoSitiosCargueAlterno2;
        };
        //--Sitios Cargue Cliente
        //--Sitios DesCargue Cliente
        $scope.AutocompleteSitiosClienteDescargue = function (value, cliente, ciudad) {
            $scope.ListadoSitiosDescargueAlterno2 = [];
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = "";
                    if ($scope.ManejoSitiosCargueDescargueGeneral == true) {
                        //--Sitios descargue generales
                        Response = SitiosCargueDescargueFactory.Consultar({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Ciudad: { Codigo: ciudad.Codigo == undefined ? 0 : ciudad.Codigo },
                            ValorAutocomplete: value,
                            Estado: ESTADO_ACTIVO,
                            Sync: true
                        });
                    }
                    else {
                        //--Sitios descargue Cliente
                        Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Cliente: { Codigo: cliente.Codigo == undefined ? 0 : ciudad.Codigo },
                            CiudadCargue: { Codigo: ciudad.Codigo == undefined ? 0 : ciudad.Codigo },
                            ValorAutocomplete: value,
                            Sync: true
                        });
                        for (var i = 0; i < Response.Datos.length; i++) {
                            Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo;
                        }
                    }
                    $scope.ListadoSitiosDescargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosDescargueAlterno2);
                }
            }
            return $scope.ListadoSitiosDescargueAlterno2;
        };
        //--Sitios DesCargue Cliente
        //----------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //-- Oficina Receptoria
            OficinaObj = OficinasFactory.Obtener({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                Sync: true
            }).Datos;
            //--Forma de pago
            var ListaFormaPago = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS },
                Sync: true
            }).Datos;
            ListaFormaPago.splice(ListaFormaPago.findIndex(item => item.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA), 1);
            $scope.ListadoFormaPago.push({ Codigo: -1, Nombre: "(SELECCIONAR)" });
            $scope.ListadoFormaPago = $scope.ListadoFormaPago.concat(ListaFormaPago);
            $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==-1');
            //--Tipo rexpedición remesa paquetería
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_REEXPEDICION_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoReexpedicionRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoReexpedicionRemesaPaqueteria = response.data.Datos;
                            $scope.Modelo.Remesa.TipoReexpedicion = $scope.ListadoTipoReexpedicionRemesaPaqueteria[0];
                        }
                        else {
                            $scope.ListadoTipoReexpedicionRemesaPaqueteria = [];
                        }
                    }
                }, function (response) {
                });
            //--Oficinas
            $scope.ListaOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
            $scope.Modelo.OficinaDestino = $scope.ListaOficinas[0];
            $scope.ListaOficinasRegistroManual.push({ Codigo: 0, Nombre: "NO APLICA" });
            $scope.ListaOficinasRegistroManual = $scope.ListaOficinasRegistroManual.concat($scope.ListaOficinas);
            $scope.Modelo.OficinaRegistroManual = $linq.Enumerable().From($scope.ListaOficinasRegistroManual).First('$.Codigo ==0');
            //--Estado Remesa
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaEstadosGuia = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListaEstadosGuia = response.data.Datos;
                            if (!$scope.Modelo.Numero > 0) {
                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                                if ($scope.CodigoEstadoGuia !== undefined && $scope.CodigoEstadoGuia !== null) {
                                    $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + $scope.CodigoEstadoGuia);

                                } else {
                                    if (RemesaGesphone == true) {
                                        $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);
                                    }
                                    else {
                                        $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_OFICINA_ORIGEN);
                                    }
                                }
                            }
                        }
                    }
                }, function (response) {
                });
            //--Tipo entrega
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;

                            if ($scope.CodigoTipoEntregaRemesaPaqueteria !== undefined && $scope.CodigoTipoEntregaRemesaPaqueteria !== null) {
                                if ($scope.CodigoTipoEntregaRemesaPaqueteria > 0) {
                                    $scope.Modelo.TipoEntregaRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoEntregaRemesaPaqueteria).First('$.Codigo ==' + $scope.CodigoTipoEntregaRemesaPaqueteria);
                                } else {
                                    $scope.Modelo.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[1];
                                }
                            } else {
                                $scope.Modelo.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[1];
                            }

                        }

                    }
                }, function (response) {
                });
            //--Tipo transporte
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_TRANSPORTE_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTransporteRemsaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoTransporteRemsaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoTransporteRemsaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoTransporteRemsaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoTransporteRemsaPaqueteria = $scope.ListadoTipoTransporteRemsaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            //--Tipo despacho*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DESPACHO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoDespachoRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoDespachoRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoDespachoRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoDespachoRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoDespachoRemesaPaqueteria = $scope.ListadoTipoDespachoRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            //--Temperatura Producto
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TEMPERATURA_PRODUCTO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTemperaturaProductoRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTemperaturaProductoRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TemperaturaProductoRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTemperaturaProductoRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TemperaturaProductoRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TemperaturaProductoRemesaPaqueteria = $scope.ListadoTemperaturaProductoRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTemperaturaProductoRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            //--Tipo Servicio
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SERVICIO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoServicioRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoServicioRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoServicioRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoServicioRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoServicioRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoServicioRemesaPaqueteria = $scope.ListadoTipoServicioRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoServicioRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            //--Tipo interfaz
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 67 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoInterfazRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoInterfazRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoInterfazRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoInterfazRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoInterfazRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoInterfazRemesaPaqueteria = $scope.ListadoTipoInterfazRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoInterfazRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            //--tipo identificaciones Tercero
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoIdentificacion = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoIdentificacion = response.data.Datos;
                            $scope.ListadoTipoIdentificacion.splice(0, 1)
                            if ($scope.CodigoTipoIdentificacionRemitente !== undefined && $scope.CodigoTipoIdentificacionRemitente !== null) {
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRemitente);
                            } else {
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                            }
                            if ($scope.CodigoTipoIdentificacionDestinatario !== undefined && $scope.CodigoTipoIdentificacionDestinatario !== null) {
                                $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionDestinatario);
                            } else {
                                $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                            }

                        }
                        else {
                            $scope.ListadoTipoIdentificacion = []
                        }
                    }
                }, function (response) {
                });
            //--Catalogo Linea Negocio Paqueteria
            $scope.ListadoLineaNegocioPaqueteria = [];
            $scope.ListadoLineaNegocioPaqueteria = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CATALOGO_LINEA_NEGOCIO_PAQUETERIA.CODIGO },
                Sync: true
            }).Datos;
            $scope.Modelo.LineaNegocioPaqueteria = $linq.Enumerable().From($scope.ListadoLineaNegocioPaqueteria).First('$.Codigo ==' + CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA);
            //--Catalogo Linea Negocio Paqueteria
            //--Catalogo Horarios
            $scope.ListadoHorariosEntrega = [];
            $scope.ListadoHorariosEntrega = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_HORARIO_ENTREGA_REMESAS_PAQUETERIA },
                Sync: true
            }).Datos;
            $scope.Modelo.Remesa.HorarioEntrega = $scope.ListadoHorariosEntrega[0];
            //--Catalogo Horarios
            //--Producto Transportado Generico
            $scope.Modelo.Remesa.ProductoTransportado = ProductoTransportadosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                ValorAutocomplete: 'PRODUCTO GENERICO',
                AplicaTarifario: $scope.AplicaTarifaProducto,
                Sync: true
            }).Datos[0];
            //--Prodcuto Transportado Generico
            //--Obtener Planilla De Conductor Actual
            if (RemesaGesphone) {
                var responPlanilla = PlanillaPaqueteriaFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                    ConsultaPLanillasenRuta: 1,
                    Planilla: {
                        Conductor: { Codigo: $scope.Sesion.UsuarioAutenticado.Conductor.Codigo }
                    },
                    Sync: true
                });
                if (responPlanilla.ProcesoExitoso == true) {
                    if (responPlanilla.Datos.length > 0) {
                        $scope.PlanillaPaqueteria = responPlanilla.Datos[0];
                        var resOptPlanilla = PlanillaPaqueteriaFactory.Obtener({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Numero: $scope.PlanillaPaqueteria.Planilla.Numero,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                            Sync: true
                        });
                        try {
                            if (resOptPlanilla.Datos.Planilla.DetallesAuxiliares.length > 0) {
                                var detalleAuxiliar = resOptPlanilla.Datos.Planilla.DetallesAuxiliares;
                                for (var i = 0; i < detalleAuxiliar.length; i++) {
                                    if (detalleAuxiliar[i].Aforador > 0) {
                                        $scope.Modelo.Aforador = {
                                            Codigo: detalleAuxiliar[i].Funcionario.Codigo,
                                            NombreCompleto: detalleAuxiliar[i].Funcionario.Nombre
                                        };
                                        break;
                                    }
                                }
                            }
                        }
                        catch (e) {

                        }
                        $scope.ExistePlanilla = true;
                    }
                }
                else {
                    //--Error de planilla para el conductor
                    $scope.ExistePlanilla = false;
                }
            }
            else {
                //--Validar Perfil Tercero empleado usuario
                var terc = TercerosFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Sesion.UsuarioAutenticado.Empleado.Codigo,
                    Sync: true
                });
                if (terc.ProcesoExitoso) {
                    for (var i = 0; i < terc.Datos.Perfiles.length; i++) {
                        if (terc.Datos.Perfiles[i].Codigo == PERFIL_AFORADOR) {
                            $scope.Modelo.Aforador = {
                                Codigo: terc.Datos.Codigo,
                                NombreCompleto: terc.Datos.NombreCompleto,
                                NumeroIdentificacion: terc.Datos.NumeroIdentificacion
                            };
                            $scope.UsuarioAforador = $scope.Modelo.Aforador
                            break;
                        }
                    }
                }
            }
            //--Obtener Planilla De Conductor Actual
            //----Obtener parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== '0') {
                $scope.PermisoGuardar = !$scope.DeshabilitarActualizar;
                $scope.Modelo.Numero = $routeParams.Numero;
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Guía...");
                $timeout(function () { blockUI.message("Cargando Guía..."); Obtener(); }, 100);
            } else {
                $scope.PermisoGuardar = !$scope.DeshabilitarCrear;
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //--Zonas
        $scope.ObtenerZonas = function () {
            if ($scope.Modelo.Remesa.Destinatario.Ciudad != undefined && $scope.Modelo.Remesa.Destinatario.Ciudad != null && $scope.Modelo.Remesa.Destinatario.Ciudad != '') {
                $scope.ListadoZonas = [];
                $scope.ListadoZonas = ZonasFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Estado: ESTADO_DEFINITIVO,
                    Ciudad: { Codigo: $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo },
                    Sync: true
                }).Datos;
                $scope.ListadoZonas.push({ Codigo: -1, Nombre: "(NO APLICA)" });
                $scope.Modelo.Remesa.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo == -1');
            }
        };
        //-- Zonas
        $scope.ValidarTIIDRemitente = function () {
            if ($scope.Modelo.Remesa.Remitente.TipoIdentificacion.Codigo == 102) {
                //--NIT
                $scope.Modelo.Remesa.Remitente.TipoNaturaleza = { Codigo: 502 };
                $scope.MostrarApellidoRemitente = false;
                $scope.Modelo.Remesa.Remitente.PrimeroApellido = '';
            }
            else {
                //--CC, CE, PASSPORT
                $scope.Modelo.Remesa.Remitente.TipoNaturaleza = { Codigo: 501 };
                if ($scope.Modelo.Remesa.Remitente.Codigo <= 0 || $scope.Modelo.Remesa.Remitente.Codigo == undefined) {
                    $scope.MostrarApellidoRemitente = true;
                }
                else {
                    $scope.MostrarApellidoRemitente = false;
                    $scope.Modelo.Remesa.Remitente.PrimeroApellido = '';
                }
            }
        };
        $scope.ValidarTIIDDestinatario = function () {
            if ($scope.Modelo.Remesa.Destinatario.TipoIdentificacion.Codigo == 102) {
                //--NIT
                $scope.Modelo.Remesa.Destinatario.TipoNaturaleza = { Codigo: 502 };
                $scope.MostrarApellidoDestinatario = false;
                $scope.Modelo.Remesa.Destinatario.PrimeroApellido = '';
            }
            else {
                //--CC, CE, PASSPORT
                $scope.Modelo.Remesa.Destinatario.TipoNaturaleza = { Codigo: 501 };
                if ($scope.Modelo.Remesa.Destinatario.Codigo <= 0 || $scope.Modelo.Remesa.Destinatario.Codigo == undefined) {
                    $scope.MostrarApellidoDestinatario = true;
                }
                else {
                    $scope.MostrarApellidoDestinatario = false;
                    $scope.Modelo.Remesa.Destinatario.PrimeroApellido = '';
                }
            }
        };

        //--Limpiar AutoCompletes
        $scope.limpiarControl = function (Control) {
            switch (Control) {
                case "Cliente":
                    $scope.Modelo.Remesa.Cliente = "";
                    //$scope.Modelo.Remesa.Remitente.NumeroIdentificacion = "";
                    $scope.TarifariosPaq = '';
                    $scope.Tarifario = '';
                    $scope.ListaCondicionesComerciales = [];
                    $scope.ListaGestionDocuCliente = [];
                    $scope.MultiplesTarifarios = false;
                    $scope.ClienteSinTarifarioValido = false;
                    $scope.ValidarTarifarioCliente();
                    break;
                case "Remitente":
                    $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = "";
                    LimpiaCamposRemitente();
                    $scope.RemitenteDirecciones = [];
                    $scope.ValidarTIIDRemitente();
                    break;
                case "SitioCargue":
                    $scope.Modelo.Remesa.Remitente.SitioCargue = "";
                    $scope.Modelo.Remesa.ValorCargue = '';
                    if (TMPRemitente != undefined && TMPRemitente != null && TMPRemitente != "" && $scope.Modelo.Remesa.Remitente.Ciudad != ''
                        && $scope.Modelo.Remesa.Remitente.Ciudad != null && $scope.Modelo.Remesa.Remitente.Ciudad != undefined) {
                        $scope.Modelo.Remesa.Remitente.Direccion = TMPRemitente.Direccion;
                        $scope.Modelo.Remesa.Remitente.Telefonos = TMPRemitente.Telefonos;
                        $scope.Modelo.Remesa.CodigoPostalRemitente = TMPRemitente.CodigoPostalRemitente;
                    }
                    else {
                        $scope.Modelo.Remesa.Remitente.Direccion = "";
                        $scope.Modelo.Remesa.Remitente.Telefonos = "";
                    }
                    $scope.Calcular();
                    break;
                case "Destinatario":
                    $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion = "";
                    LimpiaCamposDestinatario();
                    $scope.DestinatarioDirecciones = [];
                    $scope.ValidarTIIDDestinatario();
                    break;
                case "SitioDescargue":
                    $scope.Modelo.Remesa.Destinatario.SitioDescargue = "";
                    $scope.Modelo.Remesa.ValorDescargue = '';
                    if (TMPDestinatario != undefined && TMPDestinatario != null && TMPDestinatario != "" && $scope.Modelo.Remesa.Destinatario.Ciudad != ''
                        && $scope.Modelo.Remesa.Destinatario.Ciudad != null && $scope.Modelo.Remesa.Destinatario.Ciudad != undefined) {
                        $scope.Modelo.Remesa.Destinatario.Direccion = TMPDestinatario.Direccion;
                        $scope.Modelo.Remesa.Destinatario.Telefonos = TMPDestinatario.Telefonos;
                    }
                    else {
                        $scope.Modelo.Remesa.Destinatario.Direccion = "";
                        $scope.Modelo.Remesa.Destinatario.Telefonos = "";
                    }
                    $scope.Calcular();
                    break;
                case "Producto":
                    $scope.Modelo.Remesa.ProductoTransportado = "";
                    break;
                case "ProductoDetalle":
                    $scope.DetalleUnidades.Producto = "";
                    $scope.DetalleUnidades.ValorFlete = "";
                    $scope.DetalleUnidades.DescripcionProducto = "";
                    break;
                case "OficinaRecibe":
                    $scope.Modelo.OficinaRecibe = '';
                    break;
                case "CiudadRemitente":
                    $scope.Modelo.Remesa.CodigoPostalRemitente = '';
                    $scope.Modelo.Remesa.Remitente.Direccion = '';
                    $scope.Modelo.Remesa.Remitente.Ciudad = '';
                    $scope.Modelo.Remesa.Remitente.Telefonos = '';
                    $scope.RemitenteDirecciones = [];
                    break;
                case "CiudadDestinatario":
                    $scope.Modelo.Remesa.CodigoPostalDestinatario = '';
                    $scope.Modelo.Remesa.Destinatario.Direccion = '';
                    $scope.Modelo.Remesa.Destinatario.Ciudad = '';
                    $scope.Modelo.Remesa.BarrioDestinatario = '';
                    $scope.Modelo.Remesa.Destinatario.Telefonos = '';
                    $scope.DestinatarioDirecciones = [];
                    break;
                case "NoPreimpreso":
                    $scope.Modelo.Remesa.Numeracion = '';
                    $scope.Modelo.Aforador = $scope.UsuarioAforador ? $scope.UsuarioAforador : "";
                    $scope.Modelo.OficinaRegistroManual = $linq.Enumerable().From($scope.ListaOficinasRegistroManual).First('$.Codigo ==0');
                    $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                    $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;
                    $scope.DetalleEtiquetasPreimpresas = [];
                    PreimpresoValido = true;
                    $scope.MensajesErrorPreimpreso = [];
                    break;
                case "Liquidacion":
                    $scope.Modelo.Remesa.RemesaCortesia = false;
                    $scope.ManejoDetalleUnidades = false;
                    $scope.ListadoDetalleUnidades = [];
                    $scope.Modelo.Remesa.CantidadCliente = '';
                    $scope.Modelo.Remesa.PesoCliente = '';
                    $scope.Modelo.PesoCobrar = '';
                    $scope.Modelo.Remesa.ValorComercialCliente = '';
                    $scope.Modelo.Largo = '';
                    $scope.Modelo.Alto = '';
                    $scope.Modelo.Ancho = '';
                    $scope.Modelo.PesoVolumetrico = '';
                    $scope.Modelo.DescripcionProductoTransportado = '';
                    limpiarModalDetalleUnidad();
                    $scope.ValorReexpedicion = '';
                    $scope.Modelo.ValorReexpedicion = '';
                    $scope.Modelo.Remesa.ValorFleteCliente = '';
                    $scope.Modelo.Remesa.ValorManejoCliente = '';
                    $scope.Modelo.Remesa.ValorSeguroCliente = '';
                    $scope.Modelo.Remesa.ValorFleteTransportador = '';
                    $scope.Modelo.Remesa.TotalFleteCliente = '';
                    $scope.Modelo.Remesa.ValorDescargue = '';
                    $scope.Modelo.Remesa.ValorCargue = '';
                    $scope.Modelo.FletePactado = '';
                    $scope.Comision = 0;
                    break;
            }
        };
        //--Limpiar Autocompletes
        //--Oficina Rebice
        $scope.AutocompleteOficinaRecibe = function (value) {
            $scope.ListadoOficinaRecibe = [];
            if ($scope.Modelo.RecogerOficinaDestino && $scope.Modelo.Remesa.Destinatario.Ciudad != undefined) {
                if ($scope.Modelo.Remesa.Destinatario.Ciudad.Codigo > 0) {
                    $scope.ListadoOficinaRecibe = OficinasFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CodigoCiudad: $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo,
                        Estado: ESTADO_ACTIVO,
                        Sync: true
                    }).Datos;
                    return $scope.ListadoOficinaRecibe;
                }
            }
        }
        //--Oficina Rebice
        //--Remitente Generico
        $scope.SeleccionaRemitenteGenerico = function () {
            if ($scope.RemitenteGenerico) {
                $scope.limpiarControl('Remitente')
                $scope.RemitenteGenerico = true;
                $scope.BloqueoRemitenteIdentificacion = true;
                $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = TERCERO_GENERICO_NUMERO_IDENTIFICACION
                $scope.ValidarTIIDRemitente()
            } else {
                $scope.limpiarControl('Remitente')
            }
        };
        //--Remitente Generico
        //--Destinatario Generico
        $scope.SeleccionaDestinatarioGenerico = function () {
            if ($scope.DestinatarioGenerico) {
                $scope.limpiarControl('Destinatario')
                $scope.DestinatarioGenerico = true;
                $scope.BloqueoDestinatario = true;
                $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion = TERCERO_GENERICO_NUMERO_IDENTIFICACION
                $scope.ValidarTIIDDestinatario()
            } else {
                $scope.limpiarControl('Destinatario')
            }
        };
        //--Destiantario Generico
        //--Direcciones Remitente
        $scope.ValidarDireccionRemitente = function () {
            if ($scope.Modelo.Remesa.Remitente.Direccion != '' && $scope.Modelo.Remesa.Remitente.Direccion.NombreCompleto != undefined && $scope.Modelo.Remesa.Remitente.Direccion.NombreCompleto != null) {
                $scope.Modelo.Remesa.Remitente.Telefonos = $scope.Modelo.Remesa.Remitente.Direccion.Telefonos
                $scope.Modelo.Remesa.CodigoPostalRemitente = $scope.Modelo.Remesa.Remitente.Direccion.CodigoPostal
                $scope.Modelo.Remesa.Remitente.Direccion = $scope.Modelo.Remesa.Remitente.Direccion.NombreCompleto
            }
        };
        //--Direcciones Remitente
        //--Direcciones Destinatario
        $scope.ValidarDireccionDestinatario = function () {
            if ($scope.Modelo.Remesa.Destinatario.Direccion != '' && $scope.Modelo.Remesa.Destinatario.Direccion.NombreCompleto != undefined && $scope.Modelo.Remesa.Destinatario.Direccion.NombreCompleto != null) {
                $scope.Modelo.Remesa.Destinatario.Telefonos = $scope.Modelo.Remesa.Destinatario.Direccion.Telefonos
                $scope.Modelo.Remesa.BarrioDestinatario = $scope.Modelo.Remesa.Destinatario.Direccion.Barrio
                $scope.Modelo.Remesa.CodigoPostalDestinatario = $scope.Modelo.Remesa.Destinatario.Direccion.CodigoPostal
                $scope.Modelo.Remesa.Destinatario.Direccion = $scope.Modelo.Remesa.Destinatario.Direccion.NombreCompleto
            }
        };
        //--Direcciones Destinatario
        //--Cliente o Remitente
        $scope.ObtenerCliente = function () {
            var response = null;
            if ($scope.Modelo.Remesa.Cliente != undefined && $scope.Modelo.Remesa.Cliente != '' && $scope.Modelo.Remesa.Cliente != null) {
                if ($scope.Modelo.Remesa.Cliente.Codigo > 0) {
                    dataRemitente = PERFIL_CLIENTE;
                    response = TercerosFactory.Obtener({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Modelo.Remesa.Cliente.Codigo,
                        Sync: true
                    });
                    $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = '';
                    LimpiaCamposRemitente();
                    ObtenerInformacionRemitente(response, PERFIL_CLIENTE);
                }
            }
            else {
                if (dataRemitente == PERFIL_CLIENTE) {
                    $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = '';
                    LimpiaCamposRemitente();
                }
            }
        };
        $scope.ObtenerRemitente = function () {
            var response = null;
            if ($scope.Modelo.Remesa.Remitente.NumeroIdentificacion != undefined && $scope.Modelo.Remesa.Remitente.NumeroIdentificacion != '' && $scope.Modelo.Remesa.Remitente.NumeroIdentificacion != null) {
                $scope.RemitenteNoRegistrado = true;
                dataRemitente = PERFIL_REMITENTE;
                $scope.RemitenteNoRegistrado = true;
                response = TercerosFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.Modelo.Remesa.Remitente.NumeroIdentificacion,
                    Sync: true
                });
                LimpiaCamposRemitente();
                ObtenerInformacionRemitente(response, PERFIL_REMITENTE);
            }
            else {
                if (dataRemitente == PERFIL_REMITENTE) {
                    $scope.RemitenteNoRegistrado = false;
                    LimpiaCamposRemitente();
                }
            }
        };
        $scope.ObtenerRemitenteNombre = function (Remitente) {
            var response = null;
            if (angular.isObject(Remitente)) {
                if (Remitente.Codigo > 0) {
                    $scope.RemitenteNoRegistrado = true;
                    dataRemitente = PERFIL_REMITENTE;
                    response = TercerosFactory.Obtener({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: Remitente.Codigo,
                        //CadenaPerfiles: PERFIL_REMITENTE,
                        Sync: true
                    });
                    $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = '';
                    LimpiaCamposRemitente();
                    ObtenerInformacionRemitente(response, PERFIL_REMITENTE);
                }
            }
        };
        function ObtenerInformacionRemitente(response, tipo) {
            //--Obtiene Informacion ya sea de cliente o remitente
            if (response != null) {
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.Codigo > 0) {
                        $scope.RemitenteNoRegistrado = false;
                        $scope.RemitenteSinPerfil = !response.Datos.Perfiles.some(item => item.Codigo == PERFIL_REMITENTE);
                        //--Carga Informacion Remitente en el Modelo
                        $scope.Modelo.Remesa.Remitente = {
                            Codigo: response.Datos.Codigo,
                            Direccion: response.Datos.Direccion,
                            Direcciones: response.Datos.Direcciones.filter(item => item.Estado == ESTADO_ACTIVO),
                            CentroCosto: response.Datos.CentroCosto,
                            LineaSerivicioCliente: response.Datos.LineaSerivicioCliente,
                            TipoIdentificacion: $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.Datos.TipoIdentificacion.Codigo),
                            NumeroIdentificacion: response.Datos.NumeroIdentificacion,
                            //Nombre: response.Datos.NombreCompleto,
                            Ciudad: $scope.CargarCiudad(response.Datos.Ciudad.Codigo),
                            //SitioCargue: response.Datos.SitioCargue,
                            Telefonos: response.Datos.Telefonos.replace(';', ' - '),
                            CorreoFacturacion: response.Datos.CorreoFacturacion
                            //Cliente: response.Datos.Cliente
                        };
                        $scope.Modelo.Remesa.CodigoPostalRemitente = response.Datos.CodigoPostal > 0 ? response.Datos.CodigoPostal : $scope.Modelo.Remesa.Remitente.Ciudad.CodigoPostal;
                        //$scope.Modelo.Remesa.CodigoPostalRemitente = response.Datos.CodigoPostal;
                        $scope.NombreRemitente = response.Datos.NombreCompleto;
                        $scope.ValidarTIIDRemitente();

                        if (tipo == PERFIL_CLIENTE) {
                            $scope.Modelo.Remesa.Remitente.Nombre = response.Datos.NombreCompleto;
                            $scope.Modelo.Remesa.Remitente.Cliente = response.Datos.Cliente;
                            FormasPagoCliente = response.Datos.FormasPago;
                            //--Información Cliente
                            if (response.Datos.Cliente != undefined && response.Datos.Cliente != '' && response.Datos.Cliente != null) {
                                //--Condiciones Comerciales
                                if (response.Datos.Cliente.CondicionesComercialesTarifas == ESTADO_ACTIVO) {
                                    $scope.ListaCondicionesComerciales = response.Datos.Cliente.CondicionesComerciales;
                                }
                                //--Condiciones Comerciales

                                //--Tarifarios Venta Paqueteria
                                $scope.TarifariosPaq = response.Datos.Cliente.TarifariosPaq.filter(tarifa => tarifa.Estado.Codigo == ESTADO_ACTIVO);
                                $scope.ClienteSinTarifarioValido = false
                                if ($scope.TarifariosPaq.length < 1) {
                                    $scope.ClienteSinTarifarioValido = true
                                    $scope.MultiplesTarifarios = false
                                    ShowError("El cliente seleccionado no cuenta con un tarifario activo válido");
                                } else if ($scope.TarifariosPaq.length > 1) {
                                    $scope.MultiplesTarifarios = true;
                                }
                                $scope.Tarifario = $scope.TarifariosPaq[0];
                                //--Tarifarios Venta Paqueteria

                                //--Gestion Documentos
                                if (response.Datos.Cliente.GestionDocumentos == ESTADO_ACTIVO) {
                                    $scope.ListaGestionDocuCliente = [];
                                    for (var i = 0; i < response.Datos.Cliente.ListaGestionDocumentos.length; i++) {
                                        var item = response.Datos.Cliente.ListaGestionDocumentos[i];
                                        if (item.Estado.Codigo == ESTADO_ACTIVO) {
                                            $scope.ListaGestionDocuCliente.push({
                                                Recibido: $linq.Enumerable().From($scope.ListadoConfirmaEntrega).First('$.Codigo ==-1'),
                                                NumeroDocumentoGestion: "",
                                                TipoDocumento: item.TipoDocumento,
                                                TipoGestion: item.TipoGestion,
                                                NumeroHojas: "",
                                                ValorRecaudar: "",
                                                GeneraCobro: item.GeneraCobro
                                            });
                                        }
                                    }
                                }
                                //--Gestion Documentos
                            }
                            //--Información Cliente
                        }
                        //--Linea Servicio Remitente
                        try {
                            $scope.ListadoLineaServicio = [];
                            $scope.ListadoLineaServicio = response.Datos.LineaServicio;
                            if (!($scope.ListadoLineaServicio == undefined)) {
                                $scope.ListadoLineaServicio.push({ Codigo: 19400, Nombre: '(NO APLICA)' });
                            }

                            if ($scope.ListadoLineaServicio.length > 0) {
                                $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $linq.Enumerable().From($scope.ListadoLineaServicio).First('$.Codigo ==' + 19400);
                            }
                        } catch (e) {
                            $scope.ListadoLineaServicio = [];
                        }
                        //--Linea Servicio Remitente
                        //--Verifica si es cliente o remitente para bloqueo de campos
                        //if (dataRemitente == PERFIL_CLIENTE) {
                        //    //$scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + 4901);
                        //    //$scope.BloqueoRemitente = true;
                        //    //$scope.BloqueoRemitenteNombre = true;
                        //    //$scope.BloqueoRemitenteIdentificacion = true;
                        //    $scope.Modelo.Remesa.Remitente.Nombre = response.Datos.NombreCompleto;
                        //}
                        //else {
                        //    //$scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO);
                        //    $scope.BloqueoRemitenteNombre = false;
                        //    $scope.BloqueoRemitenteIdentificacion = false;
                        //}
                        TMPRemitente = angular.copy($scope.Modelo.Remesa.Remitente);
                        TMPRemitente.CodigoPostalRemitente = response.Datos.CodigoPostal;
                        if (response.Datos.Direccion !== undefined && response.Datos.Direccion !== "") {
                            DireccionPrincipalRemitente = {
                                Barrio: response.Datos.Barrio ? response.Datos.Barrio : '',
                                Ciudad: response.Datos.Ciudad,
                                CodigoPostal: response.Datos.CodigoPostal > 0 ? response.Datos.CodigoPostal : $scope.Modelo.Remesa.Remitente.Ciudad.CodigoPostal,
                                Telefonos: response.Datos.Telefonos,
                                Direccion: response.Datos.Direccion,
                                NombreCompleto: 'PRINCIPAL ' + response.Datos.Direccion
                            };
                        }
                        $scope.ValidarTarifarioCliente();
                    }
                }
            }
        }
        function LimpiaCamposRemitente() {
            $scope.Modelo.Remesa.Remitente.Codigo = '';
            //$scope.Modelo.Remesa.Remitente.Nombre = '';
            $scope.NombreRemitente = '';
            $scope.Modelo.Remesa.Remitente.PrimeroApellido = '';
            $scope.Modelo.Remesa.CodigoPostalRemitente = '';
            //$scope.ListadoDireccionesRemitente = [];
            $scope.Modelo.Remesa.Remitente.Direccion = '';
            $scope.Modelo.Remesa.Remitente.Ciudad = '';
            $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0];
            $scope.Modelo.Remesa.Remitente.Telefonos = '';
            $scope.Modelo.Remesa.Remitente.CorreoFacturacion = '';
            $scope.Modelo.Remesa.Remitente.SitioCargue = '';
            $scope.Modelo.Remesa.ValorCargue = '';
            //$scope.ListaCondicionesComerciales = [];
            //$scope.ListaGestionDocuCliente = [];
            $scope.BloqueoRemitente = false;
            $scope.BloqueoRemitenteNombre = false;
            $scope.BloqueoRemitenteIdentificacion = false;
            $scope.MostrarApellidoRemitente = true;
            TMPRemitente = "";
            $scope.RemitenteGenerico = false;
            //$scope.RemitenteNoRegistrado = false;
        }
        function ObtenerRemitenteAlterno(Codigo) {
            var FiltroRemitente = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: Codigo,
                NumeroIdentificacion: $scope.Modelo.Remesa.Remitente.NumeroIdentificacion

            };
            TercerosFactory.Obtener(FiltroRemitente).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            try {
                                $scope.ListadoLineaServicio = []
                                $scope.ListadoLineaServicio = response.data.Datos.LineaServicio
                                if (!($scope.ListadoLineaServicio == undefined)) {
                                    $scope.ListadoLineaServicio.push({ Codigo: 19400, Nombre: '(NO APLICA)' });
                                }
                                if ($scope.ListadoLineaServicio.length > 0) {
                                    $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $linq.Enumerable().From($scope.ListadoLineaServicio).First('$.Codigo ==' + 19400);
                                }
                                try {
                                    $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $linq.Enumerable().From($scope.ListadoLineaServicio).First('$.Codigo ==' + $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente.Codigo);
                                } catch (e) {

                                }
                            } catch (e) {
                                $scope.ListadoLineaServicio = []

                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        }

        //--Cliente o Remitente
        //--Destinatario
        $scope.ObtenerDestinatario = function () {
            var response = null;
            if ($scope.Modelo.Remesa.Destinatario.NumeroIdentificacion != undefined && $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion != '' && $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion != null) {
                $scope.DestinatarioNoRegistrado = true;
                response = TercerosFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion,
                    //CadenaPerfiles: PERFIL_DESTINATARIO,
                    Sync: true
                });
                LimpiaCamposDestinatario();
                ObtenerInformacionDestinatario(response);
            }
            else {
                $scope.DestinatarioNoRegistrado = false;
                LimpiaCamposDestinatario();
            }
        };

        $scope.ObtenerDestinatarioNombre = function (Destinatario) {
            var response = null;
            if (angular.isObject(Destinatario)) {
                if (Destinatario.Codigo > 0) {
                    $scope.DestinatarioNoRegistrado = true;
                    response = TercerosFactory.Obtener({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: Destinatario.Codigo,
                        //CadenaPerfiles: PERFIL_DESTINATARIO,
                        Sync: true
                    });
                    LimpiaCamposDestinatario();
                    ObtenerInformacionDestinatario(response);
                }
            }
        };

        function ObtenerInformacionDestinatario(response) {
            //--Obtiene Informacion del destinatario
            if (response != null) {
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.Codigo > 0) {
                        $scope.DestinatarioNoRegistrado = false;
                        $scope.DestinatarioSinPerfil = !response.Datos.Perfiles.some(item => item.Codigo == PERFIL_DESTINATARIO);
                        $scope.Modelo.Remesa.Destinatario = response.Datos;
                        $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion = response.Datos.NumeroIdentificacion;
                        $scope.Modelo.Remesa.Destinatario.Nombre = response.Datos.NombreCompleto;
                        $scope.Modelo.Remesa.Destinatario.Ciudad = $scope.CargarCiudad(response.Datos.Ciudad.Codigo);
                        $scope.Modelo.Remesa.Destinatario.Direccion = response.Datos.Direccion;
                        $scope.Modelo.Remesa.Destinatario.Direcciones = response.Datos.Direcciones.filter(item => item.Estado == ESTADO_ACTIVO);
                        $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.Datos.TipoIdentificacion.Codigo);
                        //if ($scope.Modelo.Remesa.Destinatario.Direcciones.length > 0) {
                        //    $scope.ListadoDireccionesDestinatario = $scope.Modelo.Remesa.Destinatario.Direcciones;
                        //} else {
                        //    $scope.ListadoDireccionesDestinatario = [];
                        //}
                        //if ($scope.Modelo.Remesa.Destinatario.Direccion !== '' && $scope.Modelo.Remesa.Destinatario.Direccion !== undefined && $scope.Modelo.Remesa.Destinatario.Direccion !== null) {
                        //    $scope.ListadoDireccionesDestinatario.push({ Direccion: $scope.Modelo.Remesa.Destinatario.Direccion });
                        //    $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[0].Direccion;
                        //}
                        $scope.NombreDestinatario = response.Datos.NombreCompleto;
                        $scope.Modelo.Remesa.CodigoPostalDestinatario = response.Datos.CodigoPostal;
                        $scope.ValidarTIIDDestinatario();
                        $scope.Modelo.Remesa.Destinatario.Telefonos = $scope.Modelo.Remesa.Destinatario.Telefonos.replace(';', ' - ');
                        $scope.Modelo.Remesa.Destinatario.CorreoFacturacion = response.Datos.CorreoFacturacion;
                        $scope.Modelo.Remesa.BarrioDestinatario = response.Datos.Barrio;
                        $scope.Modelo.Remesa.CodigoPostalDestinatario = response.Datos.CodigoPostal > 0 ? response.Datos.CodigoPostal : $scope.Modelo.Remesa.Destinatario.Ciudad.CodigoPostal;
                        TMPDestinatario = angular.copy($scope.Modelo.Remesa.Destinatario);
                        //$scope.BloqueoDestinatario = true;
                        if (response.Datos.Direccion !== undefined && response.Datos.Direccion !== "") {
                            DireccionPrincipalDestinatario = {
                                Barrio: response.Datos.Barrio ? response.Datos.Barrio : '',
                                Ciudad: response.Datos.Ciudad,
                                CodigoPostal: response.Datos.CodigoPostal > 0 ? response.Datos.CodigoPostal : $scope.Modelo.Remesa.Remitente.Ciudad.CodigoPostal,
                                Telefonos: response.Datos.Telefonos,
                                Direccion: response.Datos.Direccion,
                                NombreCompleto: 'PRINCIPAL ' + response.Datos.Direccion
                            };
                        }
                        $scope.ValidarTarifarioCliente();
                    }
                }
            }
        }

        function LimpiaCamposDestinatario() {
            $scope.Modelo.Remesa.Destinatario.Codigo = '';
            //$scope.Modelo.Remesa.Destinatario.Nombre = '';
            $scope.NombreDestinatario = '';
            $scope.Modelo.Remesa.Destinatario.PrimeroApellido = '';
            $scope.Modelo.Remesa.CodigoPostalDestinatario = '';
            //$scope.ListadoDireccionesDestinatario = [];
            $scope.Modelo.Remesa.Destinatario.Direccion = '';
            $scope.Modelo.Remesa.Destinatario.Ciudad = '';
            $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0];
            $scope.Modelo.Remesa.Destinatario.Telefonos = '';
            $scope.Modelo.Remesa.Destinatario.CorreoFacturacion = '';
            $scope.Modelo.Remesa.Destinatario.SitioDescargue = '';
            $scope.Modelo.Remesa.ValorDescargue = '';
            $scope.Modelo.Remesa.BarrioDestinatario = '';
            TMPDestinatario = "";
            $scope.BloqueoDestinatario = false;
            $scope.MostrarApellidoDestinatario = true;
            $scope.DestinatarioGenerico = false;

        }
        //--Destinatario
        //--Validar Ciudad Origen
        $scope.ValidarCiudadOrigen = function () {
            if ($scope.Modelo.Remesa.Remitente.Ciudad != undefined && $scope.Modelo.Remesa.Remitente.Ciudad != null && $scope.Modelo.Remesa.Remitente.Ciudad != "") {
                if (codigoPrevCiudadRemitente === -1) {
                    codigoPrevCiudadRemitente = $scope.Modelo.Remesa.Remitente.Ciudad.Codigo
                } else if ($scope.Modelo.Remesa.Remitente.Ciudad.Codigo !== codigoPrevCiudadRemitente) {
                    var temp = $scope.Modelo.Remesa.Remitente.Ciudad;
                    $scope.limpiarControl('CiudadRemitente');
                    codigoPrevCiudadRemitente = temp.Codigo;
                    $scope.Modelo.Remesa.Remitente.Ciudad = temp;
                }
                if ($scope.Modelo.Remesa.CodigoPostalRemitente == '' || $scope.Modelo.Remesa.CodigoPostalRemitente == undefined || $scope.Modelo.Remesa.CodigoPostalRemitente == '0') {
                    var ciudadorigen = $scope.CargarCiudad($scope.Modelo.Remesa.Remitente.Ciudad.Codigo);
                    $scope.Modelo.Remesa.CodigoPostalRemitente = ciudadorigen.CodigoPostal == undefined ? "" : ciudadorigen.CodigoPostal;
                }
                $scope.RemitenteDirecciones = [];
                if ($scope.Modelo.Remesa.Remitente != undefined && $scope.Modelo.Remesa.Remitente != "" && !$scope.RemitenteGenerico) {
                    if ($scope.Modelo.Remesa.Remitente.Direcciones !== undefined && $scope.Modelo.Remesa.Remitente.Direcciones.length > 0) {
                        $scope.RemitenteDirecciones = $scope.Modelo.Remesa.Remitente.Direcciones.filter(item => item.Ciudad.Codigo === $scope.Modelo.Remesa.Remitente.Ciudad.Codigo)
                        $scope.RemitenteDirecciones.forEach(item => item.NombreCompleto = item.Nombre + ' ' + item.Direccion)

                    }
                    if (DireccionPrincipalRemitente !== '' && DireccionPrincipalRemitente.Ciudad.Codigo === $scope.Modelo.Remesa.Remitente.Ciudad.Codigo) {
                        $scope.RemitenteDirecciones.push(DireccionPrincipalRemitente)
                    }
                }
                $scope.EsMultipleRemitenteDirecciones = $scope.RemitenteDirecciones.length > 1;
            } else {
                $scope.limpiarControl('CiudadRemitente')
            }
        };
        //--Validar Ciudad Origen
        //--Validar Ciudad Destino
        $scope.ValidarCiudadDestino = function () {
            if ($scope.Modelo.Remesa.Destinatario.Ciudad != undefined && $scope.Modelo.Remesa.Destinatario.Ciudad != null && $scope.Modelo.Remesa.Destinatario.Ciudad != "") {
                if (codigoPrevCiudadDestinatario === -1) {
                    codigoPrevCiudadDestinatario = $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo
                } else if ($scope.Modelo.Remesa.Destinatario.Ciudad.Codigo !== codigoPrevCiudadDestinatario) {
                    var temp = $scope.Modelo.Remesa.Destinatario.Ciudad;
                    $scope.limpiarControl('CiudadDestinatario');
                    codigoPrevCiudadDestinatario = temp.Codigo;
                    $scope.Modelo.Remesa.Destinatario.Ciudad = temp;
                }
                if ($scope.Modelo.Remesa.CodigoPostalDestinatario == '' || $scope.Modelo.Remesa.CodigoPostalDestinatario == undefined || $scope.Modelo.Remesa.CodigoPostalDestinatario == '0') {
                    var ciudaddestino = $scope.CargarCiudad($scope.Modelo.Remesa.Destinatario.Ciudad.Codigo);
                    $scope.Modelo.Remesa.CodigoPostalDestinatario = ciudaddestino.CodigoPostal == undefined ? "" : ciudaddestino.CodigoPostal;
                }
                $scope.DestinatarioDirecciones = [];
                if ($scope.Modelo.Remesa.Destinatario != undefined && $scope.Modelo.Remesa.Destinatario != "" && !$scope.DestinatarioGenerico) {
                    if ($scope.Modelo.Remesa.Destinatario.Direcciones !== undefined && $scope.Modelo.Remesa.Destinatario.Direcciones.length > 0) {
                        $scope.DestinatarioDirecciones = $scope.Modelo.Remesa.Destinatario.Direcciones.filter(item => item.Ciudad.Codigo === $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo)
                        $scope.DestinatarioDirecciones.forEach(item => item.NombreCompleto = item.Nombre + ' ' + item.Direccion)
                    }
                    if (DireccionPrincipalDestinatario !== '' && DireccionPrincipalDestinatario.Ciudad.Codigo === $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo) {
                        $scope.DestinatarioDirecciones.push(DireccionPrincipalDestinatario)
                    }
                }
                $scope.EsMultipleDestinatarioDirecciones = $scope.DestinatarioDirecciones.length > 1;
            } else {
                $scope.limpiarControl('CiudadDestinatario')
            }
        };
        //--Validar Ciudad Destino
        //-- Sitios de Cargue y Descargue
        //--Limpia Sitio Cargue
        $scope.LimpiarSitioCargue = function (Ciudad) {
            if (CodigoCiudadOrigen !== Ciudad.Codigo) {
                $scope.Modelo.Remesa.Remitente.SitioCargue = '';
                $scope.Modelo.Remesa.ValorCargue = '';
            }
        };
        //--Limpia Sitio Cargue
        //--Limpia Sitio Descargue
        $scope.LimpiarSitioDescargue = function (Ciudad) {
            if (CodigoCiudadDestino !== Ciudad.Codigo) {
                $scope.Modelo.Remesa.Destinatario.SitioDescargue = '';
                $scope.Modelo.Remesa.ValorDescargue = '';
            }
        };
        //--Limpia Sitio Descargue
        //--Cargue
        $scope.CargarInformacionSitioCargue = function (SitioCargue) {
            if (SitioCargue != undefined && SitioCargue != null && SitioCargue != '') {
                CodigoCiudadOrigen = $scope.Modelo.Remesa.Remitente.Ciudad.Codigo;
                if ($scope.ManejoSitiosCargueDescargueGeneral) {//--Sitio Cargue general
                    if (SitioCargue.Direccion != '')
                        $scope.Modelo.Remesa.Remitente.Direccion = SitioCargue.Direccion;
                    if (SitioCargue.Telefono)
                        $scope.Modelo.Remesa.Remitente.Telefonos = SitioCargue.Telefono;
                    if (SitioCargue.ValorCargue) {
                        $scope.Modelo.Remesa.ValorCargue = SitioCargue.ValorCargue;
                        $scope.MaskValores();
                    }

                }
                else {//--Sitio Cargue cliente
                    if (SitioCargue.DireccionSitio != '')
                        $scope.Modelo.Remesa.Remitente.Direccion = SitioCargue.DireccionSitio;
                    if (SitioCargue.Telefono) {
                        $scope.Modelo.Remesa.Remitente.Telefonos = SitioCargue.Telefono;
                    }
                }
            }
            else {
                //if (TMPRemitente != undefined && TMPRemitente != null && TMPRemitente != "" && $scope.Modelo.Remesa.Remitente.Ciudad != ''
                //    && $scope.Modelo.Remesa.Remitente.Ciudad != null && $scope.Modelo.Remesa.Remitente.Ciudad != undefined) {
                //    $scope.Modelo.Remesa.Remitente.Direccion = TMPRemitente.Direccion != "" ? TMPRemitente.Direccion : $scope.Modelo.Remesa.Remitente.Direccion;
                //    $scope.Modelo.Remesa.Remitente.Telefonos = TMPRemitente.Telefonos != "" ? TMPRemitente.Telefonos : $scope.Modelo.Remesa.Remitente.Telefonos;
                //    $scope.Modelo.Remesa.CodigoPostalRemitente = TMPRemitente.CodigoPostalRemitente != "" ? TMPRemitente.CodigoPostalRemitente : $scope.Modelo.Remesa.CodigoPostalRemitente;
                //}
                //else {
                //    $scope.Modelo.Remesa.Remitente.Direccion = "";
                //    $scope.Modelo.Remesa.Remitente.Telefonos = "";
                //}
                $scope.Modelo.Remesa.Remitente.SitioCargue = '';
                $scope.Modelo.Remesa.ValorCargue = '';
            }
            $scope.Calcular();
        };
        //--Descargue
        $scope.CargarInformacionSitioDesCargue = function (SitioDesCargue) {
            if (SitioDesCargue != undefined && SitioDesCargue != null && SitioDesCargue != '') {
                CodigoCiudadDestino = $scope.Modelo.Remesa.Remitente.Ciudad.Codigo;
                if ($scope.ManejoSitiosCargueDescargueGeneral) {//--Sitio Descargue general
                    if (SitioDesCargue.Direccion != '')
                        $scope.Modelo.Remesa.Destinatario.Direccion = SitioDesCargue.Direccion;
                    if (SitioDesCargue.Telefono)
                        $scope.Modelo.Remesa.Destinatario.Telefonos = SitioDesCargue.Telefono;
                    if (SitioDesCargue.ValorDescargue) {
                        $scope.Modelo.Remesa.ValorDescargue = SitioDesCargue.ValorDescargue;
                        $scope.MaskValores();
                    }
                }
                else {//--Sitio Descargue Cliiente
                    if (SitioDesCargue.DireccionSitio != '')
                        $scope.Modelo.Remesa.Destinatario.Direccion = SitioDescargue.DireccionSitio;
                    if (SitioDesCargue.Telefono) {
                        $scope.Modelo.Remesa.Destinatario.Telefonos = SitioDescargue.Telefono;
                    }
                }
            }
            else {
                //if (TMPDestinatario != undefined && TMPDestinatario != null && TMPDestinatario != "" && $scope.Modelo.Remesa.Destinatario.Ciudad != ''
                //    && $scope.Modelo.Remesa.Destinatario.Ciudad != null && $scope.Modelo.Remesa.Destinatario.Ciudad != undefined) {
                //    $scope.Modelo.Remesa.Destinatario.Direccion = TMPDestinatario.Direccion;
                //    $scope.Modelo.Remesa.Destinatario.Telefonos = TMPDestinatario.Telefonos;
                //}
                //else {
                //    $scope.Modelo.Remesa.Destinatario.Direccion = "";
                //    $scope.Modelo.Remesa.Destinatario.Telefonos = "";
                //}
                $scope.Modelo.Remesa.Destinatario.SitioDescargue = '';
                $scope.Modelo.Remesa.ValorDescargue = '';
            }
            $scope.Calcular();
        };
        //-- Sitios de Cargue y Descargue
        //---------------------Gestion Tarifas------------------------------//
        function DatosRequeridosTarifas() {
            var Continuar = true;

            if ($scope.Modelo.Remesa.Remitente.Ciudad !== undefined && $scope.Modelo.Remesa.Remitente.Ciudad !== null) {
                if (!($scope.Modelo.Remesa.Remitente.Ciudad.Codigo > 0)) {
                    Continuar = false;
                }
            } else {
                Continuar = false;
            }
            if ($scope.Modelo.Remesa.Destinatario.Ciudad !== undefined && $scope.Modelo.Remesa.Destinatario.Ciudad !== null) {
                if (!($scope.Modelo.Remesa.Destinatario.Ciudad.Codigo > 0)) {
                    Continuar = false;
                }
            } else {
                Continuar = false;
            }
            if ($scope.ClienteSinTarifarioValido) {
                Continuar = false;
            }
            return Continuar;
        }
        $scope.ValidarTarifarioCliente = function () {
            if (DatosRequeridosTarifas() && !$scope.EditarEncabezado) {
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Tarifas...");
                $timeout(function () { blockUI.message("Cargando Tarifas..."); ObtenerTarifarioCliente(); }, 100);
            }
        };
        //--Obtiene detalle tarifas del cliente teniendo en cuenta origen y destino
        function ObtenerTarifarioCliente() {
            $scope.ListaAuxTipoTarifas = [];
            $scope.ListaTarifaCarga = [];
            $scope.ListaTipoTarifaCargaVenta = [];
            var TipoLineaNegocioAux = 0;

            if ($scope.Modelo.Remesa.Remitente.Ciudad.Codigo == $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo) {
                TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA;
                CodigoTipoRemesa = CODIGO_TIPO_REMESA_URBANA;
            }
            else {
                TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL;
                CodigoTipoRemesa = CODIGO_TIPO_REMESA_NACIONAL;
            }
            $scope.CodigoCliente = 0;
            if ($scope.Modelo.Remesa.Cliente !== undefined && $scope.Modelo.Remesa.Cliente !== null && $scope.Modelo.Remesa.Cliente !== '') {
                $scope.CodigoCliente = $scope.Modelo.Remesa.Cliente.Codigo;
            }

            if ($scope.Modelo.Remesa.Remitente.Ciudad != undefined && $scope.Modelo.Remesa.Remitente.Ciudad != null && $scope.Modelo.Remesa.Remitente.Ciudad != '' &&
                $scope.Modelo.Remesa.Destinatario.Ciudad != undefined && $scope.Modelo.Remesa.Destinatario.Ciudad != null && $scope.Modelo.Remesa.Destinatario.Ciudad != '') {
                $scope.TarifaCliente = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.CodigoCliente,
                    CodigoCiudadOrigen: $scope.Modelo.Remesa.Remitente.Ciudad.Codigo,
                    CodigoCiudadDestino: $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo,
                    CodigoLineaNegocio: CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA,
                    CodigoTipoLineaNegocio: TipoLineaNegocioAux,
                    TarifarioProveedores: false,
                    TarifarioClientes: true,
                    TarifarioPaqueteria: $scope.Tarifario ? $scope.Tarifario.Codigo : null,
                    Sync: true
                };

                var ResponsTercero = TercerosFactory.Consultar($scope.TarifaCliente);
                if (ResponsTercero.ProcesoExitoso == true) {
                    if (ResponsTercero.Datos.length > 0) {
                        $scope.TarifarioInvalido = false;
                        for (var i = 0; i < ResponsTercero.Datos.length; i++) {
                            var InsertaTarifa = true;
                            for (var j = 0; j < $scope.ListaTarifaCarga.length; j++) {
                                if ($scope.ListaTarifaCarga[j].Codigo == ResponsTercero.Datos[i].TarifaCarga.Codigo) {
                                    InsertaTarifa = false;
                                }
                            }
                            if (InsertaTarifa == true) {
                                $scope.ListaTarifaCarga.push(ResponsTercero.Datos[i].TarifaCarga);
                            }
                            var AuxTipoTarifa = {
                                CodigoDetalleTarifa: ResponsTercero.Datos[i].CodigoDetalleTarifa,
                                Codigo: ResponsTercero.Datos[i].TipoTarifaCarga.Codigo,
                                CodigoTarifa: ResponsTercero.Datos[i].TarifaCarga.Codigo,
                                Nombre: ResponsTercero.Datos[i].TipoTarifaCarga.Nombre,
                                ValorFlete: ResponsTercero.Datos[i].ValorFlete,
                                ValorManejo: ResponsTercero.Datos[i].ValorManejo,
                                ValorSeguro: ResponsTercero.Datos[i].ValorSeguro,
                                PorcentajeAfiliado: ResponsTercero.Datos[i].PorcentajeAfiliado,
                                PorcentajeSeguro: ResponsTercero.Datos[i].PorcentajeSeguro,
                                PesoMinimo: ResponsTercero.Datos[i].ValorCatalogoTipoTarifa.CampoAuxiliar2,
                                PesoMaximo: ResponsTercero.Datos[i].ValorCatalogoTipoTarifa.CampoAuxiliar3,
                                Reexpedicion: ResponsTercero.Datos[i].Reexpedicion,
                                PorcentajeReexpedicion: ResponsTercero.Datos[i].PorcentajeReexpedicion,
                                TiempoEntrega: ResponsTercero.Datos[i].TiempoEntrega,
                                CondicionesComerciales: false,
                                FleteMinimo: 0,
                                ManejoMinimo: 0,
                                PorcentajeValorDeclarado: 0,
                                PorcentajeFlete: 0,
                                PorcentajeVolumen: 0
                            };
                            //--Valida Condiciones Comerciales con Tarifario
                            for (var j = 0; j < $scope.ListaCondicionesComerciales.length; j++) {
                                if ($scope.ListaCondicionesComerciales[j].LineaNegocio.Codigo == CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA &&
                                    $scope.ListaCondicionesComerciales[j].TipoLineaNegocio.Codigo == TipoLineaNegocioAux &&
                                    $scope.ListaCondicionesComerciales[j].Tarifa.Codigo == ResponsTercero.Datos[i].TarifaCarga.Codigo &&
                                    $scope.ListaCondicionesComerciales[j].TipoTarifa.Codigo == ResponsTercero.Datos[i].TipoTarifaCarga.Codigo) {
                                    AuxTipoTarifa.FleteMinimo = $scope.ListaCondicionesComerciales[j].FleteMinimo;
                                    AuxTipoTarifa.ManejoMinimo = $scope.ListaCondicionesComerciales[j].ManejoMinimo;
                                    AuxTipoTarifa.PorcentajeValorDeclarado = $scope.ListaCondicionesComerciales[j].PorcentajeValorDeclarado;
                                    AuxTipoTarifa.PorcentajeFlete = $scope.ListaCondicionesComerciales[j].PorcentajeFlete;
                                    AuxTipoTarifa.PorcentajeVolumen = $scope.ListaCondicionesComerciales[j].PorcentajeVolumen;
                                    AuxTipoTarifa.CondicionesComerciales = true;
                                }
                            }
                            //--Valida Condiciones Comerciales con Tarifario
                            $scope.ListaAuxTipoTarifas.push(AuxTipoTarifa);

                            if (i == 0) {
                                $scope.Modelo.Remesa.NumeroTarifarioVenta = ResponsTercero.Datos[i].NumeroTarifarioVenta;
                            }
                        }
                        if ($scope.CodigoTarifaCarga !== undefined && $scope.CodigoTarifaCarga !== null) {
                            if ($scope.CodigoTarifaCarga > 0) {
                                $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + $scope.CodigoTarifaCarga);
                            }
                            else {
                                try {
                                    $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + TARIFA_RANGO_PESO_VALOR_KILO);
                                } catch (e) {
                                    $scope.Modelo.Remesa.TarifaCarga = $scope.ListaTarifaCarga[0];
                                }
                            }
                        }
                        else {
                            try {
                                $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + TARIFA_RANGO_PESO_VALOR_KILO);
                            } catch (e) {
                                $scope.Modelo.Remesa.TarifaCarga = $scope.ListaTarifaCarga[0];
                            }
                        }
                        try {
                            $scope.ObtenerValoresPaqueteria();

                        } catch (e) {

                        }
                        $scope.CargarTipoTarifa($scope.Modelo.Remesa.TarifaCarga.Codigo);
                    }
                    else {
                        $scope.TarifarioInvalido = true;
                    }
                }
                else {
                    $scope.TarifarioInvalido = true;
                }
            }
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
        }
        //--Carga Tipo Tarifa Inicial
        $scope.CargarTipoTarifa = function (CodigoTarifaCarga) {
            $scope.ListaTipoTarifaCargaVenta = [];
            $scope.ListaAuxTipoTarifas.forEach(function (item) {
                if (CodigoTarifaCarga == item.CodigoTarifa) {
                    $scope.ListaTipoTarifaCargaVenta.push(item);
                }
            });
            $scope.Modelo.Remesa.TipoTarifaCarga = $scope.ListaTipoTarifaCargaVenta[0];
            if ($scope.CodigoTipoTarifaTransporte !== undefined && $scope.CodigoTipoTarifaTransporte !== null) {
                if ($scope.CodigoTipoTarifaTransporte > 0) {
                    $scope.Modelo.Remesa.TipoTarifaCarga = $linq.Enumerable().From($scope.ListaTipoTarifaCargaVenta).First('$.Codigo ==' + $scope.CodigoTipoTarifaTransporte);
                }
            }

            //asignacion fecha estimada de entrega
            $scope.Modelo.Remesa.FechaEntrega = new Date();
            $scope.Modelo.Remesa.FechaEntrega.setDate($scope.Modelo.Remesa.FechaEntrega.getDate() + $scope.Modelo.Remesa.TipoTarifaCarga.TiempoEntrega);
            //asignacion fecha estimada de entrega

            if (CodigoTarifaCarga != TARIFA_PRODUCTO_PAQUETERIA) {
                $scope.AplicaTarifaProducto = -1;
                if (!$scope.Obtenida) {
                    $scope.ValidarDetalleUnidades(false)
                }
                $scope.ManejoDetalleUnidades = false
            } else {
                $scope.AplicaTarifaProducto = 1;
            }
            $scope.Calcular(true);
        };
        $scope.ObtenerValoresPaqueteria = function () {
            $scope.TarifasPaqueteria = false;
            try {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Modelo.Remesa.NumeroTarifarioVenta,
                    Sync: true
                };
                var response = TarifarioVentasFactory.ObtenerDetalleTarifarioPaqueteria(filtros);
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.Paqueteria !== undefined && response.Datos.Paqueteria !== '' && response.Datos.Paqueteria !== null) {
                        $scope.TarifasPaqueteria = true;
                        $scope.PaqueteriaMinimoValorSeguro = response.Datos.Paqueteria.MinimoValorSeguro;
                        $scope.PaqueteriaPorcentajeSeguro = response.Datos.Paqueteria.PorcentajeSeguro;
                        $scope.PaqueteriaMinimoValorManejo = response.Datos.Paqueteria.MinimoValorManejo;
                        $scope.PaqueteriaFleteMinimo = response.Datos.Paqueteria.MinimoValorUnidad;
                    }
                }
            } catch (e) {

            }
        };

        //---------------------Gestion Tarifas------------------------------//
        //--Valida Producto
        //$scope.ValidarProducto = function () {
        //    if ($scope.Modelo.Remesa.ProductoTransportado !== undefined) {
        //        if ($scope.Modelo.Remesa.ProductoTransportado.Codigo > 0) {
        //            $scope.Modelo.Remesa.DescripcionProductoTransportado = '';
        //        } else {
        //            $scope.Modelo.Remesa.ProductoTransportado.inserta = true;
        //            $scope.Modelo.Remesa.DescripcionProductoTransportado = '';

        //        }
        //    }
        //};
        //--Valida Producto
        //--Validar Peso
        $scope.ValidarPeso = function () {
            var peso = parseFloat(RevertirMV($scope.Modelo.PesoCobrar));
            if (peso > 0) {
                if ($scope.Modelo.LineaNegocioPaqueteria.Codigo == CATALOGO_LINEA_NEGOCIO_PAQUETERIA.COURIER) {

                    $scope.Modelo.Remesa.CantidadCliente = 1
                    $scope.ManejoUnidadesMensajeria = true

                    if (peso > 5) {
                        ShowError("El peso debe estar entre 1 y 5 kg cuando la línea de negocio es " + $scope.Modelo.LineaNegocioPaqueteria.Nombre);

                        if ($scope.PesoNormal) {
                            $scope.Modelo.Remesa.PesoCliente = 0;
                        }
                        else {
                            $scope.Modelo.Largo = 0;
                            $scope.Modelo.Alto = 0;
                            $scope.Modelo.Ancho = 0;
                            $scope.PesoVolumetrico = 0;
                        }
                        $scope.Modelo.PesoCobrar = 0;
                    }
                }
                if ($scope.Modelo.LineaNegocioPaqueteria.Codigo == CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA) {
                    $scope.ManejoUnidadesMensajeria = false
                    if (peso <= 5) {
                        ShowError("El peso debe ser mayor a 5 kg cuando la línea de negocio es " + $scope.Modelo.LineaNegocioPaqueteria.Nombre);
                        if ($scope.PesoNormal) {
                            $scope.Modelo.Remesa.PesoCliente = 0;
                        }
                        else {
                            $scope.Modelo.Largo = 0;
                            $scope.Modelo.Alto = 0;
                            $scope.Modelo.Ancho = 0;
                            $scope.PesoVolumetrico = 0;
                        }
                        $scope.Modelo.PesoCobrar = 0;
                    }
                }
            }
        };
        //--Validar Peso
        //--Validar Peso Volumetrico
        $scope.ValidarPesoVolumetrico = function () {
            var tmpPesoVol = 0;
            if ($scope.ManejoPesoVolumetrico) {
                if ($scope.Modelo.Alto !== undefined && $scope.Modelo.Alto !== '' && $scope.Modelo.Alto !== '' &&
                    $scope.Modelo.Ancho !== undefined && $scope.Modelo.Ancho !== '' && $scope.Modelo.Ancho !== '' &&
                    $scope.Modelo.Largo !== undefined && $scope.Modelo.Largo !== '' && $scope.Modelo.Largo !== '') {
                    if ($scope.Modelo.Alto > 0 && $scope.Modelo.Ancho > 0 && $scope.Modelo.Largo > 0) {
                        var resVal
                        resVal = RemesaGuiasFactory.ValidarPesoVolumetrico({
                            Alto: RevertirMV($scope.Modelo.Alto),
                            Ancho: RevertirMV($scope.Modelo.Ancho),
                            Largo: RevertirMV($scope.Modelo.Largo),
                            Sync: true
                        });

                        if (resVal.ProcesoExitoso) {
                            tmpPesoVol = resVal.Datos.PesoVolumetrico;
                            if (tmpPesoVol != Math.round($scope.Modelo.PesoVolumetrico)) {
                                $scope.Modelo.Alto = 0;
                                $scope.Modelo.Ancho = 0;
                                $scope.Modelo.Largo = 0;
                            }
                        }
                    }
                }
            }
        };
        //--Validar Peso Volumetrico
        //-----------Calcular----------//}
        //--Manejo Peso Volumetrico
        function ManejoPesoVolumen() {
            if ($scope.ManejoPesoVolumetrico) {
                if ($scope.Modelo.Alto !== undefined && $scope.Modelo.Alto !== '' && $scope.Modelo.Alto !== '' &&
                    $scope.Modelo.Ancho !== undefined && $scope.Modelo.Ancho !== '' && $scope.Modelo.Ancho !== '' &&
                    $scope.Modelo.Largo !== undefined && $scope.Modelo.Largo !== '' && $scope.Modelo.Largo !== '') {
                    if ($scope.Modelo.Alto > 0 && $scope.Modelo.Ancho > 0 && $scope.Modelo.Largo > 0) {
                        var resVal
                        resVal = RemesaGuiasFactory.ValidarPesoVolumetrico({
                            Alto: RevertirMV($scope.Modelo.Alto),
                            Ancho: RevertirMV($scope.Modelo.Ancho),
                            Largo: RevertirMV($scope.Modelo.Largo),
                            Sync: true
                        });

                        if (resVal.ProcesoExitoso) {
                            $scope.Modelo.PesoVolumetrico = resVal.Datos.PesoVolumetrico;
                        }
                    }
                }
                //--Compara Peso volumetrico con peso normal
                if (RevertirMV($scope.Modelo.Remesa.PesoCliente) > 0 || $scope.Modelo.PesoVolumetrico > 0) {
                    if (RevertirMV($scope.Modelo.Remesa.PesoCliente) > $scope.Modelo.PesoVolumetrico) {
                        $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente);
                        $scope.PesoNormal = true;
                    }
                    if ($scope.Modelo.PesoVolumetrico > RevertirMV($scope.Modelo.Remesa.PesoCliente)) {
                        $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.PesoVolumetrico);
                        $scope.PesoNormal = false;
                    }
                }
                else {
                    $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente);
                }
            }
            else {
                $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente);
            }
        }
        //--Manejo Peso Volumetrico
        $scope.Calcular = function (recalcula = false) {
            if (!$scope.Obtenida || recalcula) {
                if ($scope.Modelo.Remesa.CantidadCliente == undefined || $scope.Modelo.Remesa.CantidadCliente == 0 || $scope.Modelo.Remesa.CantidadCliente == "") {
                    $scope.Modelo.Remesa.CantidadCliente = 0;
                }
                tarifaConReexpediccion = false;
                $scope.MensajesErrorTarifas = [];

                $scope.ValorReexpedicion = 0;
                $scope.Modelo.Remesa.ValorFleteCliente = 0;
                $scope.Modelo.Remesa.ValorManejoCliente = 0;
                $scope.Modelo.Remesa.ValorSeguroCliente = 0;
                $scope.Modelo.Remesa.ValorFleteTransportador = 0;
                $scope.Modelo.Remesa.TotalFleteCliente = 0;
                $scope.Modelo.Remesa.ValorDescargue = $scope.Modelo.Remesa.ValorDescargue ? Math.round(RevertirMV($scope.Modelo.Remesa.ValorDescargue)) : 0;
                $scope.Modelo.Remesa.ValorCargue = $scope.Modelo.Remesa.ValorCargue ? Math.round(RevertirMV($scope.Modelo.Remesa.ValorCargue)) : 0;
                var Calculo = false;
                $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente);
                ManejoPesoVolumen();//--Valida Peso Volumetrico
                $scope.ValidarPeso();
                if ($scope.Modelo.Remesa.TarifaCarga != null && $scope.Modelo.Remesa.TarifaCarga != undefined &&
                    $scope.Modelo.PesoCobrar > 0 && $scope.Modelo.Remesa.ProductoTransportado != undefined && $scope.Modelo.Remesa.ProductoTransportado != "" &&
                    $scope.Modelo.Remesa.ProductoTransportado != null) {
                    var resVal
                    resVal = RemesaGuiasFactory.CalcularValores({
                        CodigoTarifaTransporte: $scope.Modelo.Remesa.TarifaCarga.Codigo,
                        ListaTipoTarifaCargaVenta: $scope.ListaTipoTarifaCargaVenta.map(a => {
                            return {
                                CodigoTarifa: a.CodigoTarifa,
                                Codigo: a.Codigo,
                                ValorFlete: a.ValorFlete,
                                PesoMinimo: a.PesoMinimo,
                                PesoMaximo: a.PesoMaximo,
                                CondicionesComerciales: a.CondicionesComerciales,
                                PorcentajeFlete: a.PorcentajeFlete,
                                PorcentajeVolumen: a.PorcentajeVolumen,
                                Reexpedicion: a.Reexpedicion,
                                FleteMinimo: a.FleteMinimo,
                                PorcentajeReexpedicion: a.PorcentajeReexpedicion,
                                PorcentajeSeguro: a.PorcentajeSeguro,
                                PorcentajeValorDeclarado: a.PorcentajeValorDeclarado,
                                ManejoMinimo: a.ManejoMinimo
                            };
                        }),
                        PesoVolumetrico: $scope.Modelo.PesoVolumetrico,
                        PesoCobrar: $scope.Modelo.PesoCobrar,
                        LineaNegocioPaqueteria: { Codigo: $scope.Modelo.LineaNegocioPaqueteria ? $scope.Modelo.LineaNegocioPaqueteria.Codigo : 0 },
                        ListaReexpedicionOficinas: $scope.ListaReexpedicionOficinas.map(a => {
                            return {
                                Valor: a.Valor,
                                TipoReexpedicion: { Codigo: a.TipoReexpedicion.Codigo }
                            };
                        }),
                        Remesa: {
                            PesoCliente: $scope.Modelo.Remesa.PesoCliente ? $scope.Modelo.Remesa.PesoCliente : 0,
                            ValorFleteCliente: $scope.Modelo.Remesa.ValorFleteCliente ? $scope.Modelo.Remesa.ValorFleteCliente : 0,
                            ValorFleteTransportador: $scope.Modelo.Remesa.ValorFleteTransportador ? $scope.Modelo.Remesa.ValorFleteTransportador : 0,
                            TotalFleteCliente: $scope.Modelo.Remesa.TotalFleteCliente ? $scope.Modelo.Remesa.TotalFleteCliente : 0,
                            ValorSeguroCliente: $scope.Modelo.Remesa.ValorSeguroCliente ? $scope.Modelo.Remesa.ValorSeguroCliente : 0,
                            ValorComisionOficina: $scope.Comision ? $scope.Comision : 0,
                            ProductoTransportado: {
                                Codigo: $scope.Modelo.Remesa.ProductoTransportado ? $scope.Modelo.Remesa.ProductoTransportado.Codigo : 0,
                                Peso: $scope.Modelo.Remesa.ProductoTransportado ? $scope.Modelo.Remesa.ProductoTransportado.Peso : 0
                            },
                            CantidadCliente: $scope.Modelo.Remesa.CantidadCliente ? $scope.Modelo.Remesa.CantidadCliente : 0,
                            ValorComercialCliente: $scope.Modelo.Remesa.ValorComercialCliente ? $scope.Modelo.Remesa.ValorComercialCliente : 0,
                            ValorDescargue: $scope.Modelo.Remesa.ValorDescargue ? $scope.Modelo.Remesa.ValorDescargue : 0,
                            ValorCargue: $scope.Modelo.Remesa.ValorCargue ? $scope.Modelo.Remesa.ValorCargue : 0,
                            RemesaCortesia: $scope.Modelo.Remesa.RemesaCortesia == true ? 1 : 0
                        },
                        parametrosCalculos: {
                            OficinaObjPorcentajeSeguroPaqueteria: OficinaObj.PorcentajeSeguroPaqueteria ? OficinaObj.PorcentajeSeguroPaqueteria : 0,
                            PaqueteriaMinimoValorManejo: $scope.PaqueteriaMinimoValorManejo,
                            PaqueteriaMinimoValorSeguro: $scope.PaqueteriaMinimoValorSeguro,
                            PaqueteriaFleteMinimo: $scope.PaqueteriaFleteMinimo,
                            calculo: false,
                            ListadoDetalleUnidadesValorFlete: $scope.ListadoDetalleUnidades.map(a => a.ValorFlete),
                            ManejoDetalleUnidades: $scope.ManejoDetalleUnidades,
                            ManejoReexpedicionOficina: $scope.ManejoReexpedicionOficina,
                            Tarifario: { Codigo: $scope.Modelo.Remesa.NumeroTarifarioVenta ? $scope.Modelo.Remesa.NumeroTarifarioVenta : 0, TarifarioBase: $scope.Tarifario.TarifarioBase ? $scope.Tarifario.TarifarioBase : 1 }
                        },
                        Sync: true
                    });

                    if (resVal.ProcesoExitoso) {
                        $scope.Modelo.Remesa.PesoCliente = resVal.Datos.Remesa.PesoCliente;
                        $scope.Modelo.PesoCobrar = resVal.Datos.PesoCobrar;
                        $scope.Modelo.PesoVolumetrico = resVal.Datos.PesoVolumetrico;
                        Calculo = resVal.Datos.parametrosCalculos.calculo;
                        $scope.Modelo.Remesa.ValorFleteCliente = resVal.Datos.Remesa.ValorFleteCliente;
                        $scope.Modelo.Remesa.ValorFleteTransportador = resVal.Datos.Remesa.ValorFleteTransportador;
                        $scope.Modelo.Remesa.TotalFleteCliente = resVal.Datos.Remesa.TotalFleteCliente;
                        $scope.Modelo.FletePactado = resVal.Datos.FletePactado;
                        $scope.ValorReexpedicion = resVal.Datos.ValorReexpedicion;
                        $scope.Modelo.ValorReexpedicion = resVal.Datos.ValorReexpedicion;
                        $scope.Modelo.Remesa.ValorSeguroCliente = resVal.Datos.Remesa.ValorSeguroCliente;
                        $scope.Comision = resVal.Datos.Remesa.ValorComisionOficina;
                    }
                    if ($scope.Modelo.Remesa.TarifaCarga.Codigo == TARIFA_PRODUCTO_PAQUETERIA) {
                        $scope.AplicaTarifaProducto = 1;
                    }
                    if ($scope.Modelo.PesoCobrar > 0) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == TARIFA_RANGO_PESO_VALOR_KILO) {
                            $scope.AplicaTarifaProducto = -1;
                        }
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == TARIFA_RANGO_PESO_VALOR_FIJO) {
                            $scope.AplicaTarifaProducto = -1;
                        }
                    }
                    if (Calculo == false) {
                        $scope.TarifarioInvalido = true;
                        switch ($scope.Modelo.Remesa.TarifaCarga.Codigo) {
                            case TARIFA_PRODUCTO_PAQUETERIA:
                                $scope.MensajesErrorTarifas.push("Debe seleccionar un producto que coincida con la tarifa");
                                break;
                            case TARIFA_RANGO_PESO_VALOR_FIJO:
                                $scope.MensajesErrorTarifas.push("Debe ingresar un peso que coincida con las tarifas");
                                break;
                            case TARIFA_RANGO_PESO_VALOR_KILO:
                                $scope.MensajesErrorTarifas.push("Debe ingresar un peso que coincida con las tarifas");
                                break;
                        }
                    }
                    else {
                        $scope.TarifarioInvalido = false;
                        $scope.CalcularAjusteTotal();
                    }
                }
            } else {
                $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente);
                ManejoPesoVolumen();//--Valida Peso Volumetrico
                $scope.ValidarPeso();
                $scope.Modelo.Remesa.TotalFleteCliente = RevertirMV($scope.Modelo.Remesa.ValorFleteCliente) + RevertirMV($scope.Modelo.Remesa.ValorSeguroCliente) +
                    RevertirMV($scope.ValorReexpedicion) + RevertirMV($scope.Modelo.Remesa.ValorCargue) + RevertirMV($scope.Modelo.Remesa.ValorDescargue)
            }
            $scope.MaskValores();
        };
        //-----------Calcular----------//
        //----Asignar Peso Producto
        $scope.AsignarPesoProducto = function () {
            if ($scope.ManejoDetalleUnidades == false) {
                if ($scope.Modelo.Remesa.ProductoTransportado.Peso > 0) {
                    $scope.Modelo.Remesa.PesoCliente = $scope.Modelo.Remesa.ProductoTransportado.Peso;
                }
            }
        };
        //----Asignar Peso Producto
        //-----------------------------Obtener------------------------------//
        function Obtener() {

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero
            };
            RemesaGuiasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = angular.copy(response.data.Datos);
                        $scope.Obtenida = true;

                        //--Estado
                        if ($scope.Modelo.Estado == ESTADO_DEFINITIVO || $scope.Modelo.Anulado == ESTADO_ANULADO) {
                            $scope.Deshabilitar = true;
                            $scope.DeshabilitarOficinaDestino = true;
                            $scope.BloqueoRemitenteNombre = true;
                            $scope.BloqueoRemitenteIdentificacion = true;
                            $scope.BloqueoDestinatario = true;
                            $scope.DeshabilitarTotales = true;

                        } else {
                            $scope.Deshabilitar = false;
                            $scope.DeshabilitarOficinaDestino = false;
                            $scope.BloqueoRemitenteNombre = false;
                            $scope.BloqueoRemitenteIdentificacion = false;
                            $scope.BloqueoDestinatario = false;
                            $scope.DeshabilitarTotales = false;
                        }
                        //--Obtiente tercero e informacion
                        var resDocu;
                        var remitente = response.data.Datos.Remesa.Cliente.Codigo > 0 ? response.data.Datos.Remesa.Cliente.Codigo : response.data.Datos.Remesa.Remitente.Codigo;
                        resDocu = TercerosFactory.Obtener({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: remitente,
                            Sync: true
                        });
                        if (resDocu.ProcesoExitoso) {
                            $scope.ListadoLineaServicio = resDocu.Datos.LineaServicio;
                            if (!($scope.ListadoLineaServicio == undefined)) {
                                $scope.ListadoLineaServicio.push({ Codigo: 19400, Nombre: '(NO APLICA)' });
                            }
                            FormasPagoCliente = resDocu.Datos.FormasPago;
                        }
                        //--Obtiente tercero e informacion

                        $scope.Modelo.Remesa.Numero = response.data.Datos.Remesa.Numero;
                        $scope.Modelo.Remesa.NumeroDocumento = response.data.Datos.Remesa.NumeroDocumento;
                        $scope.Modelo.Remesa.Fecha = new Date(response.data.Datos.Remesa.Fecha);
                        $scope.Modelo.Remesa.Numeracion = response.data.Datos.Numeracion;

                        $scope.Modelo.Remesa.RemesaCortesia = response.data.Datos.Remesa.RemesaCortesia == 1 ? true : false;

                        $scope.CodigoTipoEntregaRemesaPaqueteria = response.data.Datos.TipoEntregaRemesaPaqueteria.Codigo;
                        $scope.Modelo.LineaNegocioPaqueteria = $linq.Enumerable().From($scope.ListadoLineaNegocioPaqueteria).First('$.Codigo ==' + response.data.Datos.LineaNegocioPaqueteria.Codigo);
                        $scope.Modelo.RecogerOficinaDestino = response.data.Datos.RecogerOficinaDestino == 1 ? true : false;
                        $scope.ManejoDetalleUnidades = response.data.Datos.ManejaDetalleUnidades == 1 ? true : false;
                        $scope.ListadoDetalleUnidades = response.data.Datos.DetalleUnidades ? response.data.Datos.DetalleUnidades : [];
                        if ($scope.ManejoDetalleUnidades == true) {
                            if ($scope.ListadoDetalleUnidades != undefined && $scope.ListadoDetalleUnidades != '' && $scope.ListadoDetalleUnidades != null && $scope.ListadoDetalleUnidades.length > 0) {
                                for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                                    if ($scope.ManejoPesoVolumetrico) {
                                        if ($scope.ListadoDetalleUnidades[i].PesoVolumetrico > $scope.ListadoDetalleUnidades[i].Peso)
                                            $scope.ListadoDetalleUnidades[i].PesoCobrar = $scope.ListadoDetalleUnidades[i].PesoVolumetrico;
                                        else
                                            $scope.ListadoDetalleUnidades[i].PesoCobrar = $scope.ListadoDetalleUnidades[i].Peso;
                                    }
                                    else {
                                        $scope.ListadoDetalleUnidades[i].PesoCobrar = $scope.ListadoDetalleUnidades[i].Peso;
                                    }
                                    $scope.ListadoDetalleUnidades[i].ValorComercial = MascaraValores($scope.ListadoDetalleUnidades[i].ValorComercial);
                                    $scope.ListadoDetalleUnidades[i].Cantidad = 1;
                                }
                                IndiceDetalleUnidad();
                            }
                        }

                        if ($scope.ListadoTipoEntregaRemesaPaqueteria !== undefined && $scope.ListadoTipoEntregaRemesaPaqueteria !== null) {
                            if ($scope.ListadoTipoEntregaRemesaPaqueteria.length > 0) {
                                $scope.Modelo.TipoEntregaRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoEntregaRemesaPaqueteria).First('$.Codigo ==' + $scope.CodigoTipoEntregaRemesaPaqueteria);
                            }
                        }

                        if (response.data.Datos.Remesa.Cliente.Codigo > 0) {
                            $scope.Modelo.Remesa.Cliente = $scope.CargarTercero(response.data.Datos.Remesa.Cliente.Codigo);
                        }

                        $scope.Modelo.Remesa.NumeroDocumentoCliente = response.data.Datos.Remesa.NumeroDocumentoCliente;


                        ////-- REMITENTE
                        $scope.CodigoTipoIdentificacionRemitente = response.data.Datos.CodigoTipoIdentificacionRemitente;
                        if ($scope.ListadoTipoIdentificacion !== undefined && $scope.ListadoTipoIdentificacion !== null) {
                            if ($scope.ListadoTipoIdentificacion.length > 0) {
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRemitente);
                            }
                        }

                        $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = response.data.Datos.Remesa.Remitente.NumeroIdentificacion;
                        if (response.data.Datos.Remesa.Remitente.NumeroIdentificacion == TERCERO_GENERICO_NUMERO_IDENTIFICACION) $scope.RemitenteGenerico = true;
                        //$scope.Modelo.Remesa.Remitente.Nombre = response.data.Datos.Remesa.Remitente.Nombre;
                        $scope.NombreRemitente = response.data.Datos.Remesa.Remitente.Nombre;
                        $scope.Modelo.Remesa.Remitente.Ciudad = $scope.CargarCiudad(response.data.Datos.Remesa.Remitente.Ciudad.Codigo)
                        $scope.Modelo.Remesa.Destinatario.Ciudad = $scope.CargarCiudad(response.data.Datos.Remesa.Destinatario.Ciudad.Codigo)
                        $scope.Modelo.Remesa.BarrioRemitente = response.data.Datos.Remesa.BarrioRemitente;

                        try {
                            $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = response.data.Datos.LineaSerivicioCliente;
                            $scope.Modelo.Remesa.CentroCosto = response.data.Datos.CentroCosto;
                            if ($scope.ManejoSitiosCargueDescargueGeneral == true) {
                                $scope.Modelo.Remesa.Remitente.SitioCargue = response.data.Datos.SitioCargue;
                            }
                            else {
                                $scope.Modelo.Remesa.Remitente.SitioCargue = { SitioCliente: response.data.Datos.SitioCargue };
                            }

                            if ($scope.ManejoSitiosCargueDescargueGeneral == true) {
                                $scope.Modelo.Remesa.Destinatario.SitioDescargue = response.data.Datos.SitioDescargue;
                            }
                            else {
                                $scope.Modelo.Remesa.Destinatario.SitioDescargue = { SitioCliente: response.data.Datos.SitioDescargue };
                            }


                            $scope.Modelo.Remesa.FechaEntrega = new Date(response.data.Datos.FechaEntrega);
                            $scope.ObtenerRemitenteAlterno($scope.Modelo.Remesa.Remitente.Codigo);
                        } catch (e) {

                        }


                        //Para el remitente y destinatario por ahora se carga en el combo solo la dirección guardada
                        $scope.ListadoDireccionesRemitente = [];
                        $scope.ListadoDireccionesRemitente.push({ Direccion: response.data.Datos.Remesa.Remitente.Direccion });
                        $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[0].Direccion;

                        $scope.ListadoDireccionesDestinatario = [];
                        $scope.ListadoDireccionesDestinatario.push({ Direccion: response.data.Datos.Remesa.Destinatario.Direccion });
                        $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[0].Direccion;

                        $scope.Modelo.Remesa.CodigoPostalRemitente = response.data.Datos.Remesa.CodigoPostalRemitente;
                        $scope.Modelo.Remesa.Remitente.Telefonos = response.data.Datos.Remesa.Remitente.Telefonos;
                        $scope.Modelo.Remesa.ObservacionesRemitente = response.data.Datos.Remesa.ObservacionesRemitente;
                        if (!($scope.ListadoLineaServicio == undefined)) {
                            $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente.Codigo == 0 ? $scope.ListadoLineaServicio[0] : $linq.Enumerable().From($scope.ListadoLineaServicio).First('$.Codigo ==' + $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente.Codigo);
                        }

                        $scope.ValidarTIIDRemitente();

                        ////-- DESTINATARIO
                        $scope.AutocompleteOficinaRecibe();
                        if ($scope.Modelo.OficinaRecibe.Codigo > 0) {
                            try { $scope.Modelo.OficinaRecibe = $linq.Enumerable().From($scope.ListadoOficinaRecibe).First('$.Codigo ==' + response.data.Datos.OficinaRecibe.Codigo) } catch (e) { $scope.Modelo.OficinaRecibe = $scope.ListadoOficinaRecibe[0] };
                        }
                        try {
                            $scope.CodigoTipoIdentificacionDestinatario = response.data.Datos.CodigoTipoIdentificacionDestinatario;
                            if ($scope.ListadoTipoIdentificacion !== undefined && $scope.ListadoTipoIdentificacion !== null) {
                                if ($scope.ListadoTipoIdentificacion.length > 0) {
                                    $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionDestinatario);
                                }
                            }
                        } catch (e) {

                        }


                        $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion = response.data.Datos.Remesa.Destinatario.NumeroIdentificacion;
                        if (response.data.Datos.Remesa.Destinatario.NumeroIdentificacion == TERCERO_GENERICO_NUMERO_IDENTIFICACION) $scope.DestinatarioGenerico = true;
                        //$scope.Modelo.Remesa.Destinatario.Nombre = response.data.Datos.Remesa.Destinatario.Nombre;
                        $scope.NombreDestinatario = response.data.Datos.Remesa.Destinatario.Nombre;
                        $scope.Modelo.Remesa.BarrioDestinatario = response.data.Datos.Remesa.BarrioDestinatario;
                        $scope.ValidarTIIDDestinatario();

                        $scope.Modelo.Remesa.CodigoPostalDestinatario = response.data.Datos.Remesa.CodigoPostalDestinatario;
                        $scope.Modelo.Remesa.Destinatario.Telefonos = response.data.Datos.Remesa.Destinatario.Telefonos;
                        $scope.Modelo.Remesa.ObservacionesDestinatario = response.data.Datos.Remesa.ObservacionesDestinatario;
                        ////--DETALLE
                        $scope.Modelo.Remesa.FormaPago = response.data.Datos.Remesa.FormaPago;
                        if ($scope.ListadoFormaPago !== undefined && $scope.ListadoFormaPago !== null) {
                            if ($scope.ListadoFormaPago.length > 0) {
                                $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.Modelo.Remesa.FormaPago.Codigo);
                            }
                        }

                        $scope.Modelo.Remesa.PesoCliente = response.data.Datos.Remesa.PesoCliente;
                        $scope.Modelo.Remesa.CantidadCliente = response.data.Datos.Remesa.CantidadCliente;
                        $scope.Modelo.Remesa.ValorComercialCliente = response.data.Datos.Remesa.ValorComercialCliente;
                        $scope.Modelo.Remesa.ProductoTransportado = $scope.CargarProducto(response.data.Datos.Remesa.ProductoTransportado.Codigo);

                        $scope.CodigoTarifaCarga = response.data.Datos.CodigoTarifaTransporte;
                        $scope.CodigoTipoTarifaTransporte = response.data.Datos.CodigoTipoTarifaTransporte;
                        $scope.Modelo.DescripcionProductoTransportado = response.data.Datos.DescripcionProductoTransportado;


                        //Arma Tarifa consultada
                        $scope.ListaTarifaCarga = [{ Codigo: 0, Nombre: '(Seleccione Item)' },
                        { Codigo: response.data.Datos.CodigoTarifaTransporte, Nombre: response.data.Datos.NombreTarifa }];
                        $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + $scope.CodigoTarifaCarga);

                        //Arma Tipo de tarifa consultada
                        $scope.ListaTipoTarifaCargaVenta = [
                            { CodigoDetalleTarifa: 0, Codigo: 0, CodigoTarifa: 0, Nombre: '(Seleccione Item)' },
                            { CodigoDetalleTarifa: response.data.Datos.Remesa.DetalleTarifaVenta.Codigo, Codigo: $scope.CodigoTipoTarifaTransporte, CodigoTarifa: $scope.CodigoTarifaCarga, Nombre: response.data.Datos.NombreTipoTarifa },
                        ];
                        $scope.Modelo.Remesa.TipoTarifaCarga = $linq.Enumerable().From($scope.ListaTipoTarifaCargaVenta).First('$.Codigo ==' + $scope.CodigoTipoTarifaTransporte);

                        ////--INFORMACION GUIA
                        $scope.Modelo.OficinaOrigen = response.data.Datos.OficinaOrigen;
                        if ($scope.Modelo.AjusteFlete > 0) {
                            $scope.Modelo.AjusteFlete = true;
                        }
                        var fechaDevolucion = (new Date(response.data.Datos.FechaDevolucion)).withoutTime();
                        var fechaInit = (new Date(FORMATO_FECHA_MINIMA)).withoutTime();
                        $scope.Modelo.FechaDevolucion = fechaDevolucion.getTime() == fechaInit.getTime() ? '' : fechaDevolucion;
                        $scope.Modelo.Devolucion = $scope.Modelo.Devolucion == 0 ? false : true;

                        $scope.Modelo.Oficina = response.data.Datos.Oficina;

                        try {
                            $scope.CodigoOficina = response.data.Datos.OficinaDestino.Codigo;
                            if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.CodigoOficina);
                            }
                        } catch (e) {

                        }
                        $scope.Modelo.OficinaActual = response.data.Datos.OficinaActual;
                        $scope.Modelo.EstadoGuia = response.data.Datos.EstadoGuia;
                        $scope.Modelo.Remesa.NumeroPlanilla = response.data.Datos.Remesa.NumeroPlanilla;
                        $scope.Modelo.Remesa.Vehiculo = response.data.Datos.Remesa.Vehiculo;
                        $scope.Modelo.Remesa.Conductor = response.data.Datos.Remesa.Conductor;
                        // FP 2021-12-15 obtiene la ruta de la planilla de la guia
                        if (response.data.Datos.NumeroPlanilla > 0) {
                            PlanillaPaqueteriaFactory.Consultar({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Estado: -1,
                                Oficina: { Codigo: - 1 },
                                Planilla: { Numero: response.data.Datos.NumeroPlanilla },
                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                                Pagina: 1,
                                RegistrosPagina: 1
                            }).then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos != null && response.data.Datos != undefined && response.data.Datos.length > 0) {
                                        $scope.Modelo.Remesa.Ruta = response.data.Datos[0].Planilla.Ruta
                                    }
                                }
                            });
                        }
                        // FP 2021-12-15 obtiene la ruta de la planilla de la guia
                        var listaconductores = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: response.data.Datos.Remesa.Conductor.NombreCompleto, Sync: true }).Datos
                        if (listaconductores.length > 0) $scope.Modelo.Remesa.Conductor = listaconductores[0]
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                        ////-- ESTADO
                        if ($scope.ListadoEstados !== undefined && $scope.ListadoEstados !== null) {
                            if ($scope.ListadoEstados.length > 0) {
                                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                                if ($scope.Modelo.Estado.Codigo === ESTADO_BORRADOR) $scope.PermisoGuardar = !$scope.DeshabilitarCrear; //FP 2022 01 20 Req 013 modificar borrador se considera como crear
                            }
                        }

                        $scope.TipoRemesa = response.data.Datos.CodigoTipoRemesa;
                        $scope.AplicaReexpedicion = response.data.Datos.AplicaReexpedicion;
                        $scope.Modelo.EstadoGuia = response.data.Datos.EstadoGuia;
                        $scope.ListaReexpedicionOficinas = response.data.Datos.ListaReexpedicionOficinas
                        if ($scope.ManejoReexpedicionOficina) {
                            $scope.ValorReexpedicion = 0
                            try {
                                for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                                    $scope.ValorReexpedicion += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor))
                                }
                                $scope.ValorReexpedicion = MascaraValores($scope.ValorReexpedicion)
                            } catch (e) {
                            }
                        }

                        $scope.ObtenerZonas();
                        //--Carga de Zona, Posicion, Horarios Destinatario
                        if (response.data.Datos.CodigoZona > 0) {
                            try {
                                $scope.Modelo.Remesa.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==' + response.data.Datos.CodigoZona);
                            } catch (e) {
                                $scope.Modelo.Remesa.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==-1');
                            }
                        }
                        else {
                            $scope.Modelo.Remesa.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==-1');
                        }
                        $scope.Modelo.Remesa.Latitud = response.data.Datos.Latitud;
                        $scope.Modelo.Remesa.Longitud = response.data.Datos.Longitud;
                        if (response.data.Datos.HorarioEntregaRemesa.Codigo > 0) {
                            try {
                                $scope.Modelo.Remesa.HorarioEntrega = $linq.Enumerable().From($scope.ListadoHorariosEntrega).First('$.Codigo ==' + response.data.Datos.HorarioEntregaRemesa.Codigo);
                            } catch (e) {
                                $scope.Modelo.Remesa.HorarioEntrega = ListadoHorariosEntrega[0];
                            }

                        }

                        //--Gestion Documentos
                        if (response.data.Datos.Remesa.GestionDocumentosRemesa != undefined && response.data.Datos.Remesa.GestionDocumentosRemesa != '' && response.data.Datos.Remesa.GestionDocumentosRemesa != null) {
                            if (response.data.Datos.Estado == ESTADO_BORRADOR) {
                                var tmpGestionDocuReme = response.data.Datos.Remesa.GestionDocumentosRemesa;
                                var tmpGestionDocuTerc = [];
                                if (resDocu.ProcesoExitoso) {
                                    if (resDocu.Datos.Codigo > 0) {
                                        if (resDocu.Datos.Cliente != undefined && resDocu.Datos.Cliente != '' && resDocu.Datos.Cliente != null) {
                                            //--Condiciones Comerciales
                                            if (resDocu.Datos.Cliente.CondicionesComercialesTarifas == ESTADO_ACTIVO) {
                                                $scope.ListaCondicionesComerciales = resDocu.Datos.Cliente.CondicionesComerciales;
                                            }
                                            //--Condiciones Comerciales
                                            //--Gestion Documentos
                                            if (resDocu.Datos.Cliente.GestionDocumentos == ESTADO_ACTIVO) {
                                                for (var i = 0; i < resDocu.Datos.Cliente.ListaGestionDocumentos.length; i++) {
                                                    var item = resDocu.Datos.Cliente.ListaGestionDocumentos[i];
                                                    if (item.Estado.Codigo == ESTADO_ACTIVO) {
                                                        tmpGestionDocuTerc.push({
                                                            Recibido: $linq.Enumerable().From($scope.ListadoConfirmaEntrega).First('$.Codigo ==-1'),
                                                            NumeroDocumentoGestion: "",
                                                            TipoDocumento: item.TipoDocumento,
                                                            TipoGestion: item.TipoGestion,
                                                            NumeroHojas: "",
                                                            ValorRecaudar: "",
                                                            GeneraCobro: item.GeneraCobro
                                                        });
                                                    }
                                                }
                                                for (var i = 0; i < tmpGestionDocuTerc.length; i++) {
                                                    for (var j = 0; j < tmpGestionDocuReme.length; j++) {
                                                        if (tmpGestionDocuTerc[i].TipoDocumento.Codigo == tmpGestionDocuReme[j].TipoDocumento.Codigo &&
                                                            tmpGestionDocuTerc[i].TipoGestion.Codigo == tmpGestionDocuReme[j].TipoGestion.Codigo) {
                                                            tmpGestionDocuTerc[i].Recibido = $linq.Enumerable().From($scope.ListadoConfirmaEntrega).First('$.Codigo ==' + tmpGestionDocuReme[j].Recibido);
                                                            if (tmpGestionDocuTerc[i].Recibido.Codigo == ESTADO_ACTIVO) {
                                                                tmpGestionDocuTerc[i].NumeroDocumentoGestion = tmpGestionDocuReme[j].NumeroDocumentoGestion;
                                                                tmpGestionDocuTerc[i].NumeroHojas = tmpGestionDocuReme[j].NumeroHojas;
                                                                tmpGestionDocuTerc[i].ValorRecaudar = MascaraValores(tmpGestionDocuReme[j].ValorRecaudar);
                                                            }
                                                        }
                                                    }
                                                }
                                                $scope.ListaGestionDocuCliente = tmpGestionDocuTerc;
                                            }
                                            //--Gestion Documentos
                                        }
                                    }
                                    else {
                                        $scope.ListaGestionDocuCliente = response.data.Datos.Remesa.GestionDocumentosRemesa;
                                    }
                                }
                                else {
                                    $scope.ListaGestionDocuCliente = response.data.Datos.Remesa.GestionDocumentosRemesa;
                                }
                            }
                            else {
                                $scope.ListaGestionDocuCliente = response.data.Datos.Remesa.GestionDocumentosRemesa;
                                for (var j = 0; j < $scope.ListaGestionDocuCliente.length; j++) {
                                    var itm = $scope.ListaGestionDocuCliente[j];
                                    if (itm.TipoDocumento.Codigo == itm.TipoDocumento.Codigo &&
                                        itm.TipoGestion.Codigo == itm.TipoGestion.Codigo) {
                                        itm.NumeroHojas = MascaraValores(itm.NumeroHojas);
                                        itm.ValorRecaudar = MascaraValores(itm.ValorRecaudar);
                                        itm.Recibido = $linq.Enumerable().From($scope.ListadoConfirmaEntrega).First('$.Codigo ==' + itm.Recibido);
                                    }
                                }
                            }
                        }
                        //if (response.data.Datos.Estado == ESTADO_BORRADOR) {
                        //    ObtenerTarifarioCliente();
                        //}
                        //--Gestion Documentos
                        //--Documentos
                        $scope.ListadoDocumentos = response.data.Datos.ListadoDocumentos;
                        for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                            $scope.ListadoDocumentos[i].ValorDocumento = 1;
                            $scope.ListadoDocumentos[i].AplicaEliminar = 1;
                            $scope.ListadoDocumentos[i].temp = false;
                        }
                        //--Documentos
                        //--Detalle Etiquetas
                        $scope.DetalleEtiquetasPreimpresas = response.data.Datos.DetalleEtiquetasPreimpresas;
                        //--Detalle Etiquetas
                        if (response.data.Datos.Aforador.Codigo > 0) {
                            $scope.Modelo.Aforador = $scope.CargarTercero(response.data.Datos.Aforador.Codigo);
                        }
                        if (response.data.Datos.OficinaRegistroManual.Codigo > 0) {
                            $scope.Modelo.OficinaRegistroManual = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + response.data.Datos.OficinaRegistroManual.Codigo);
                        }

                        //--LIQUIDACION
                        $scope.Modelo.Remesa.ValorFleteTransportador = response.data.Datos.Remesa.ValorFleteTransportador;
                        $scope.Modelo.Remesa.ValorFleteCliente = response.data.Datos.Remesa.ValorFleteCliente;
                        $scope.FleteAnterior = response.data.Datos.Remesa.ValorFleteCliente;
                        $scope.Modelo.Remesa.ValorManejoCliente = response.data.Datos.Remesa.ValorManejoCliente;
                        $scope.Modelo.Remesa.ValorSeguroCliente = response.data.Datos.Remesa.ValorSeguroCliente;

                        $scope.ValorReexpedicion = response.data.Datos.ValorReexpedicion;
                        $scope.Modelo.ValorReexpedicion = $scope.ValorReexpedicion

                        $scope.Modelo.Remesa.TotalFleteCliente = response.data.Datos.Remesa.TotalFleteCliente;
                        $scope.Comision = response.data.Datos.Remesa.ValorComisionOficina;
                        $scope.Agencista = response.data.Datos.Remesa.ValorComisionAgencista;

                        $scope.Modelo.Remesa.Observaciones = response.data.Datos.Remesa.Observaciones;
                        //--LIQUIDACION
                        $scope.MaskValores();
                    }
                    else {
                        ShowError('No se logro consultar el tarifario ventas código ' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = $scope.Master + "/0/" + OPCION_MENU;
                    }
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = $scope.Master + "/0/" + OPCION_MENU;
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                });
        }
        //-----------------------------Obtener------------------------------//
        //----------------------------Guardar----------------------------//
        function DatosRequeridosEncabezado() {
            $scope.MensajesErrorEncabezado = [];
            var continuar = true;
            var modelo = $scope.Modelo;

            var dateEntMin = new Date();
            dateEntMin.setDate(dateEntMin.getDate() - 1);
            if ($scope.Modelo.Remesa.Fecha == undefined || $scope.Modelo.Remesa.Fecha == null || $scope.Modelo.Remesa.Fecha == '') {
                $scope.MensajesErrorEncabezado.push('Debe ingresar una fecha válida');
                continuar = false;
            } else if ($scope.Modelo.Remesa.Fecha > $scope.MaxFecha || $scope.Modelo.Remesa.Fecha < $scope.MinFecha) {
                $scope.MensajesErrorEncabezado.push('Debe ingresar una fecha dentro del rango permitido');
                continuar = false;
            }
            if (modelo.Remesa.FormaPago == undefined || modelo.Remesa.FormaPago == null || modelo.Remesa.FormaPago == '') {
                $scope.MensajesErrorEncabezado.push('Debe ingresar la forma de pago');
                continuar = false;
            }
            else if (modelo.Remesa.FormaPago.Codigo == 6100 || modelo.Remesa.FormaPago.Codigo == -1) {
                $scope.MensajesErrorEncabezado.push('Debe ingresar la forma de pago');
                continuar = false;
            }
            if ($scope.Obtenida !== true) {
                //--Forma Pago
                if (modelo.Remesa.Cliente != undefined && modelo.Remesa.Cliente != null && modelo.Remesa.Cliente != "") {
                    var existe = false;
                    if (FormasPagoCliente != undefined && FormasPagoCliente != null && FormasPagoCliente != "") {
                        for (var i = 0; i < FormasPagoCliente.length; i++) {
                            if (FormasPagoCliente[i].Codigo == modelo.Remesa.FormaPago.Codigo) {
                                existe = true;
                                break;
                            }
                        }
                        if (existe == false) {
                            $scope.MensajesErrorEncabezado.push('La forma de pago no esta asignada al cliente');
                            continuar = false;
                        }
                    }
                    if (modelo.Remesa.Remitente !== undefined || modelo.Remesa.Remitente !== null || modelo.Remesa.Remitente !== "") {
                        if (modelo.Remesa.Destinatario !== undefined || modelo.Remesa.Destinatario !== null || modelo.Remesa.Destinatario !== "") {
                            if (modelo.Remesa.Cliente.Codigo !== modelo.Remesa.Remitente.Codigo
                                && modelo.Remesa.Cliente.Codigo !== modelo.Remesa.Destinatario.Codigo) {
                                $scope.MensajesErrorEncabezado.push('El cliente debe ser remitente o destinatario');
                                continuar = false;
                            }
                        }
                    }
                }
                else {
                    if (modelo.Remesa.Remitente !== undefined || modelo.Remesa.Remitente !== null || modelo.Remesa.Remitente !== "") {
                        if (modelo.Remesa.Remitente.Codigo == 0) {
                            if (modelo.Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                                $scope.MensajesErrorEncabezado.push('No se puede asignar forma de pago crédito para un remitente nuevo');
                                continuar = false;
                            }
                        }
                        else {
                            var existe = false;
                            if (FormasPagoCliente != undefined && FormasPagoCliente != null && FormasPagoCliente != "") {
                                for (var i = 0; i < FormasPagoCliente.length; i++) {
                                    if (FormasPagoCliente[i].Codigo == modelo.Remesa.FormaPago.Codigo) {
                                        existe = true;
                                        break;
                                    }
                                }
                                if (existe == false) {
                                    $scope.MensajesErrorEncabezado.push('La forma de pago no esta asignada al remitente');
                                    continuar = false;
                                }
                            }
                            else {
                                if (modelo.Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                                    $scope.MensajesErrorEncabezado.push('No se puede asignar forma de pago crédito para un remitente');
                                    continuar = false;
                                }
                            }
                        }
                    }
                }

                //--Forma Pago
            }
            if ($scope.Modelo.Remesa.Remitente.Ciudad !== undefined && $scope.Modelo.Remesa.Remitente.Ciudad !== null && $scope.Modelo.Remesa.Destinatario.Ciudad !== undefined && $scope.Modelo.Remesa.Destinatario.Ciudad !== null) {
                if ($scope.Modelo.Remesa.Remitente.Ciudad.Codigo > 0 && $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo > 0) {
                    if ($scope.Modelo.Remesa.Remitente.Ciudad.Codigo !== $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo) {
                        $scope.TipoRemesa = CODIGO_TIPO_REMESA_NACIONAL;
                    } else {
                        $scope.TipoRemesa = CODIGO_TIPO_REMESA_URBANA;
                    }
                } else {
                    $scope.MensajesErrorEncabezado.push('Debe seleccionar la ciudad para el remitente y el destinatario');
                    continuar = false;
                }

            } else {
                $scope.MensajesErrorEncabezado.push('No hay ciudades seleccionadas para el remitente y/o destinatario');
                continuar = false;
            }

            if (modelo.Remesa.Remitente !== undefined || modelo.Remesa.Remitente !== null || modelo.Remesa.Remitente !== "") {
                if (ValidarCampo(modelo.Remesa.Remitente.NumeroIdentificacion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesErrorEncabezado.push('Debe ingresar el número de identificación del remitente');
                    continuar = false;
                }
                if (modelo.Remesa.Remitente.TipoIdentificacion.Codigo == 100) {
                    $scope.MensajesErrorEncabezado.push('Debe seleccionar el tipo de identificación del remitente');
                    continuar = false;
                }
                if (ValidarCampo($scope.NombreRemitente) == OBJETO_SIN_DATOS) {
                    $scope.MensajesErrorEncabezado.push('Debe ingresar el nombre del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Ciudad) == OBJETO_SIN_DATOS) {
                    $scope.MensajesErrorEncabezado.push('Debe seleccionar la ciudad del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Direccion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesErrorEncabezado.push('Debe seleccionar la dirección del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Telefonos) == OBJETO_SIN_DATOS) {
                    $scope.MensajesErrorEncabezado.push('Debe ingresar el teléfono del remitente');
                    continuar = false;
                }

            } else {
                $scope.MensajesErrorEncabezado.push('Debe ingresar la información del remitente');
                continuar = false;
            }
            if (modelo.Remesa.Destinatario !== undefined || modelo.Remesa.Destinatario !== null || modelo.Remesa.Destinatario !== "") {
                if (ValidarCampo(modelo.Remesa.Destinatario.NumeroIdentificacion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesErrorEncabezado.push('Debe ingresar el número de identificación del destinatario');
                    continuar = false;
                }
                if (modelo.Remesa.Destinatario.TipoIdentificacion.Codigo == 100) {
                    $scope.MensajesErrorEncabezado.push('Debe seleccionar el tipo de identificación del destinatario');
                    continuar = false;
                }
                if (ValidarCampo($scope.NombreDestinatario) == OBJETO_SIN_DATOS) {
                    $scope.MensajesErrorEncabezado.push('Debe ingresar el nombre del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Ciudad) == OBJETO_SIN_DATOS) {
                    $scope.MensajesErrorEncabezado.push('Debe seleccionar la ciudad del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Direccion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesErrorEncabezado.push('Debe seleccionar la dirección del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Telefonos) == OBJETO_SIN_DATOS) {
                    $scope.MensajesErrorEncabezado.push('Debe ingresar el teléfono del destinatario');
                    continuar = false;
                }
            } else {
                $scope.MensajesErrorEncabezado.push('Debe ingresar la información del destinatario');
                continuar = false;
            }

            if ($scope.ClienteSinTarifarioValido) {
                $scope.MensajesErrorEncabezado.push('El cliente seleccionado no cuenta con un tarifario activo válido')
                continuar = false;
            }

            return continuar;
        }
        function DatosRequeridosLiquidacion() {
            $scope.MensajesErrorLiquidacion = [];
            $scope.MensajesErrorDocu = [];
            var continuar = true;
            var modelo = $scope.Modelo;

            var dateEntMin = new Date();
            dateEntMin.setDate(dateEntMin.getDate() - 1);

            if ($scope.Obtenida == true) {
                if (modelo.Remesa.Observaciones == undefined || modelo.Remesa.Observaciones == null || modelo.Remesa.Observaciones == "") {
                    $scope.MensajesErrorLiquidacion.push('Debe ingresar una Observacion');

                    continuar = false;
                }
            }

            if (modelo.Remesa.FechaEntrega == undefined || modelo.Remesa.FechaEntrega == '' || modelo.Remesa.FechaEntrega == null) {
                $scope.MensajesErrorLiquidacion.push('Debe ingresar la fecha de entrega');
                continuar = false;
            }
            else if (modelo.Remesa.FechaEntrega < dateEntMin) {
                $scope.MensajesErrorLiquidacion.push('la fecha entrega debe ser posterior a la fecha actual');
                continuar = false;
            }

            if (modelo.RecogerOficinaDestino == true) {
                if (modelo.OficinaRecibe === undefined || modelo.OficinaRecibe === '' || modelo.OficinaRecibe === null) {
                    $scope.MensajesErrorLiquidacion.push('Debe ingresar la oficina que recibe');
                    continuar = false;
                }
            }

            if (ValidarCampo(modelo.Remesa.ProductoTransportado) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorLiquidacion.push('Debe ingresar el producto ');
                continuar = false;
            }

            if (ValidarCampo(modelo.DescripcionProductoTransportado) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorLiquidacion.push('Debe ingresar la descripción de la mercancia')
                continuar = false;
            }

            if (ValidarCampo(modelo.PesoCobrar) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorLiquidacion.push('Debe ingresar el peso o las dimensiones');
                continuar = false;
            }
            if (ValidarCampo(modelo.Remesa.ValorComercialCliente) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorLiquidacion.push('Debe ingresar el valor comercial');
                continuar = false;
            }
            if (ValidarCampo(modelo.Remesa.CantidadCliente) == OBJETO_SIN_DATOS || modelo.Remesa.CantidadCliente == 0) {
                $scope.MensajesErrorLiquidacion.push('Debe ingresar el No. Unidades');
                continuar = false;
            }

            if (modelo.TipoEntregaRemesaPaqueteria.Codigo == 6600) {
                $scope.MensajesErrorLiquidacion.push('Debe seleccionar el tipo de entrega');
                continuar = false;
            }

            if (ValidarCampo(modelo.Remesa.ValorFleteCliente) == OBJETO_SIN_DATOS && $scope.Modelo.Remesa.RemesaCortesia == false) {
                $scope.MensajesErrorLiquidacion.push('No hay un flete asignado con las condiciones especificadas');
                continuar = false;
            }

            if ($scope.ListaGestionDocuCliente.length > 0) {
                var contError = 0;
                for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                    if ($scope.ListaGestionDocuCliente[i].Recibido.Codigo == -1) {
                        $scope.MensajesErrorDocu.push('Se debe seleccionar en todos los documentos si fueron recibidos o no');
                        $scope.MensajesErrorLiquidacion.push('Campos mandatorios en la sección Gestión Documentos Cliente');
                        continuar = false;
                        break;
                    }
                    else if ($scope.ListaGestionDocuCliente[i].Recibido.Codigo == 1) {
                        if ($scope.ListaGestionDocuCliente[i].NumeroDocumentoGestion == undefined || $scope.ListaGestionDocuCliente[i].NumeroDocumentoGestion == "") {
                            $scope.MensajesErrorDocu.push('Digite No. Documento en ' + $scope.ListaGestionDocuCliente[i].TipoDocumento.Nombre);
                            continuar = false;
                            contError++;
                        }
                        if ($scope.ListaGestionDocuCliente[i].NumeroHojas == undefined || $scope.ListaGestionDocuCliente[i].NumeroHojas == "") {
                            $scope.MensajesErrorDocu.push('Digite No. Hojas en ' + $scope.ListaGestionDocuCliente[i].TipoDocumento.Nombre);
                            continuar = false;
                            contError++;
                        }
                        if ($scope.ListaGestionDocuCliente[i].NumeroHojas == undefined || $scope.ListaGestionDocuCliente[i].NumeroHojas == "") {
                            if ($scope.ListaGestionDocuCliente[i].GeneraCobro == true) {
                                $scope.MensajesErrorDocu.push('Digite valor a recaudar en ' + $scope.ListaGestionDocuCliente[i].TipoDocumento.Nombre);
                                continuar = false;
                                contError++;
                            }
                        }
                    }
                }
                if (contError > 0) {
                    $scope.MensajesErrorLiquidacion.push('Campos mandatorios en la sección Gestión Documentos Cliente');
                    continuar = false;
                }
            }

            if (tarifaConReexpediccion == true && $scope.Modelo.Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA) {
                $scope.MensajesErrorLiquidacion.push("La forma de pago no puede ser contra entrega cuando la tarifa tiene reexpedición");
                continuar = false;
            }

            if ($scope.Modelo.Remesa.Numeracion != undefined && $scope.Modelo.Remesa.Numeracion != null && $scope.Modelo.Remesa.Numeracion != "") {
                if (PreimpresoValido == false) {
                    $scope.MensajesErrorLiquidacion.push("El  No. Preimpreso no es válido");
                    continuar = false;
                }
                if ($scope.ListadoDocumentos == undefined || $scope.ListadoDocumentos == null || !($scope.ListadoDocumentos.length > 0)
                    || $scope.ListadoDocumentos[0].Id == null || $scope.ListadoDocumentos[0].Id == undefined) {
                    $scope.MensajesErrorLiquidacion.push("Debe adjuntar el documento correspondiente al consecutivo de guía preimpresa ingresado");
                    continuar = false;
                }
            }

            if ($scope.DetalleEtiquetasPreimpresas.length > 0) {
                if ($scope.ManejoDetalleUnidades == false) {
                    if ($scope.DetalleEtiquetasPreimpresas.length > $scope.Modelo.Remesa.CantidadCliente) {
                        $scope.MensajesErrorLiquidacion.push('El número de etiquetas no debe exceder el número de unidades')
                        continuar = false;
                    }
                } else {
                    if ($scope.DetalleEtiquetasPreimpresas.length > $scope.ListadoDetalleUnidades.length) {
                        $scope.MensajesErrorLiquidacion.push('El número de etiquetas no debe exceder el número de unidades')
                        continuar = false;
                    }
                }
            }

            if ($scope.Modelo.Aforador == undefined || $scope.Modelo.Aforador == null || $scope.Modelo.Aforador == "") {
                $scope.MensajesErrorLiquidacion.push('No cuenta con perfil aforador asignado, ingrese No. Preimpreso de la guía')
                continuar = false;
            }

            return continuar;
        }

        $scope.Confirmacion = function () {
            $scope.CountInt += 1
            if ($scope.CountInt > 0) {
                $scope.NumeroGuiaExterna = 'caja'
                $scope.ConfirmacionEtiquetas()
            } else {
                $scope.NumeroGuiaExterna = ''
                $scope.ConfirmacionEtiquetas()
            }

        };

        $scope.ConfirmacionEtiquetas = function () {
            if ($scope.PermisoGuardar) {
                if (DatosRequeridosEncabezado() && DatosRequeridosLiquidacion()) {
                    $scope.EtiquetasIncompletas = false;
                    if ($scope.DetalleEtiquetasPreimpresas.length > 0) {
                        if ($scope.ManejoDetalleUnidades == false) {
                            if ($scope.DetalleEtiquetasPreimpresas.length < $scope.Modelo.Remesa.CantidadCliente) {
                                showModal('modalConfirmacionEtiquetas');
                                $scope.EtiquetasIncompletas = true;
                            } else {
                                $scope.Guardar();
                            }
                        } else {
                            if ($scope.DetalleEtiquetasPreimpresas.length < $scope.ListadoDetalleUnidades.length) {
                                showModal('modalConfirmacionEtiquetas');
                                $scope.EtiquetasIncompletas = true;
                            } else {
                                $scope.Guardar();
                            }
                        }
                    } else {
                        showModal('modalConfirmacionEtiquetas');
                        $scope.EtiquetasIncompletas = true;
                    }
                }
            } else {
                ShowError('No tiene permisos para realizar esta acción');
            }

        };
        $scope.PeriodoValido = true
        function FindCierre() {
            var anoFecha = $scope.Modelo.Remesa.Fecha.getFullYear();
            var mesFecha = $scope.Modelo.Remesa.Fecha.getMonth();
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentos: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },
                Ano: anoFecha,
                Mes: { Codigo: ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MES }, CampoAuxiliar2: (mesFecha + 1), Sync: true }).Datos[0].Codigo },
                EstadoCierre: { Codigo: 18002 },
                Sync: true
            };
            var RCierreContable = CierreContableDocumentosFactory.Consultar(filtros)
            if (RCierreContable.ProcesoExitoso === true) {
                if (RCierreContable.Datos.length > 0) {
                    ShowError('La fecha ingresada no se encuentra en un periodo contable abierto');
                    $scope.Modelo.Remesa.Fecha = '';
                    $scope.PeriodoValido = false
                    //$scope.ListadoTipoDocumentos = RCierreContable.Datos;
                    //$scope.ValidarCierre();
                }
            }
        }

        $scope.ValidarCierre = function () {
            if ($scope.ListadoTipoDocumentos.length > 0) {
                var anoFecha = $scope.Modelo.Remesa.Fecha.getFullYear();
                var mesFecha = $scope.Modelo.Remesa.Fecha.getMonth();
                for (var k = 0; k < $scope.ListadoTipoDocumentos.length; k++) {
                    var item = $scope.ListadoTipoDocumentos[k];
                    var mes = MascaraNumero(item.Mes.CampoAuxiliar2);
                    if (item.Ano === anoFecha && mes === (mesFecha + 1) && item.EstadoCierre.Codigo === 18002) {
                        ShowError('La fecha se encuentra en un mes y año con cierre contable');
                        $scope.Modelo.Remesa.Fecha = '';
                        $scope.PeriodoValido = false
                        break;
                    }
                }
            }
        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            closeModal('modalConfirmacionEtiquetas');
            if (DatosRequeridosEncabezado() && DatosRequeridosLiquidacion()) {
                $scope.PeriodoValido = true
                FindCierre();
                if ($scope.PeriodoValido) {
                    //$scope.Modelo.Remesa.Fecha = RetornarFechaEspecificaSinTimeZone($scope.Modelo.Remesa.Fecha);
                    if ($scope.Modelo.Remesa.ProductoTransportado.Codigo == undefined || $scope.Modelo.Remesa.ProductoTransportado.Codigo == 0) {
                        $scope.Modelo.Remesa.ProductoTransportado = { Nombre: $scope.Modelo.Remesa.ProductoTransportado, Descripcion: $scope.Modelo.DescripcionProductoTransportado };
                    }
                    var GestionDocumentosRemesa = [];
                    if ($scope.ListaGestionDocuCliente != undefined && $scope.ListaGestionDocuCliente != null) {
                        if ($scope.ListaGestionDocuCliente.length > 0) {
                            for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                                var item = $scope.ListaGestionDocuCliente[i];
                                if (item.TipoDocumento.Codigo == CODIGO_TIPO_DOCUMENTO_CARTA_PORTE) {
                                    // FP 2021-12-14 si se recibe documento carta porte, se usa su No.Documento en campo Documento_Cliente
                                    if (item.Recibido.Codigo == 1) {
                                        $scope.Modelo.Remesa.NumeroDocumentoCliente = item.NumeroDocumentoGestion
                                    }
                                }
                                GestionDocumentosRemesa.push({
                                    NumeroDocumentoGestion: item.NumeroDocumentoGestion,
                                    TipoDocumento: item.TipoDocumento,
                                    TipoGestion: item.TipoGestion,
                                    NumeroHojas: item.NumeroHojas,
                                    ValorRecaudar: item.ValorRecaudar,
                                    Recibido: item.Recibido.Codigo
                                });
                            }
                        }
                    }

                    //--Documentos Remesas
                    var ListaDocuemntos = [];
                    if ($scope.ListadoDocumentos != undefined && $scope.ListadoDocumentos != null) {
                        if ($scope.ListadoDocumentos.length > 0) {
                            for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                                if ($scope.ListadoDocumentos[i].Id != undefined && $scope.ListadoDocumentos[i].Id != null && $scope.ListadoDocumentos[i].Id != '') {
                                    ListaDocuemntos.push({
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        Codigo: $scope.ListadoDocumentos[i].Id,
                                        Numero: $scope.Modelo.Numero,
                                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                                        Temporal: $scope.ListadoDocumentos[i].temp
                                    });
                                }
                            }
                        }
                    }
                    //--Documentos Remesas

                    //--Remitente Nombre
                    $scope.Modelo.Remesa.Remitente.Nombre = $scope.NombreRemitente;
                    //--Remitente Nombre

                    var ObtjetoRemesa = {

                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Numero: $scope.Modelo.Remesa.Numero,
                        TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },

                        TipoRemesa: { Codigo: $scope.TipoRemesa },
                        RemesaCortesia: $scope.Modelo.Remesa.RemesaCortesia == true ? 1 : 0,
                        NumeroDocumentoCliente: $scope.Modelo.Remesa.NumeroDocumentoCliente,
                        FechaDocumentoCliente: MIN_DATE,
                        Fecha: $scope.Modelo.Remesa.Fecha,
                        Ruta: { Codigo: 0 },
                        ProductoTransportado: $scope.Modelo.Remesa.ProductoTransportado,
                        FormaPago: $scope.Modelo.Remesa.FormaPago,
                        Cliente: $scope.Modelo.Remesa.Cliente,
                        Remitente: $scope.Modelo.Remesa.Remitente,

                        CiudadRemitente: { Codigo: $scope.Modelo.Remesa.Remitente.Ciudad.Codigo },
                        DireccionRemitente: $scope.Modelo.Remesa.Remitente.Direccion,
                        TelefonoRemitente: $scope.Modelo.Remesa.Remitente.Telefonos,
                        Observaciones: $scope.Modelo.Remesa.Observaciones,

                        CantidadCliente: $scope.Modelo.Remesa.CantidadCliente,
                        PesoCliente: RevertirMV($scope.Modelo.Remesa.PesoCliente),
                        PesoVolumetricoCliente: $scope.Modelo.Remesa.PesoVolumetricoCliente,

                        ValorFleteCliente: $scope.Modelo.Remesa.ValorFleteCliente,
                        ValorFleteTransportador: $scope.Modelo.Remesa.ValorFleteTransportador,
                        ValorManejoCliente: $scope.Modelo.Remesa.ValorManejoCliente,
                        ValorSeguroCliente: $scope.Modelo.Remesa.ValorSeguroCliente,
                        ValorDescuentoCliente: 0,
                        TotalFleteCliente: $scope.Modelo.Remesa.TotalFleteCliente,
                        ValorComercialCliente: $scope.Modelo.Remesa.ValorComercialCliente,

                        CantidadTransportador: $scope.Modelo.Remesa.CantidadTransportador,
                        PesoTransportador: $scope.Modelo.Remesa.PesoTransportador,
                        //ValorFleteTransportador: 0,
                        TotalFleteTransportador: $scope.Modelo.Remesa.TotalFleteTransportador,
                        ValorComisionOficina: $scope.Comision,
                        ValorComisionAgencista: $scope.Agencista,
                        ValorOtros: $scope.Modelo.Remesa.ValorOtros,
                        ValorCargue: $scope.Modelo.Remesa.ValorCargue,
                        ValorDescargue: $scope.Modelo.Remesa.ValorDescargue,

                        Destinatario: {
                            Codigo: $scope.Modelo.Remesa.Destinatario.Codigo,
                            Ciudad: $scope.Modelo.Remesa.Destinatario.Ciudad,
                            TipoIdentificacion: $scope.Modelo.Remesa.Destinatario.TipoIdentificacion,
                            Direccion: $scope.Modelo.Remesa.Destinatario.Direccion,
                            Telefonos: $scope.Modelo.Remesa.Destinatario.Telefonos,
                            NumeroIdentificacion: $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion,
                            Nombre: $scope.NombreDestinatario,
                            PrimeroApellido: $scope.Modelo.Remesa.Destinatario.PrimeroApellido,
                            Barrio: $scope.Modelo.Remesa.BarrioDestinatario,
                            CorreoFacturacion: $scope.Modelo.Remesa.Destinatario.CorreoFacturacion
                        },
                        CiudadDestinatario: { Codigo: $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo },
                        DireccionDestinatario: $scope.Modelo.Remesa.Destinatario.Direccion,
                        TelefonoDestinatario: $scope.Modelo.Remesa.Destinatario.Telefonos,

                        Estado: $scope.Modelo.Estado.Codigo,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },

                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },

                        TarifarioVenta: { Codigo: $scope.Modelo.Remesa.NumeroTarifarioVenta },
                        NumeroOrdenServicio: 0,

                        BarrioRemitente: $scope.Modelo.Remesa.BarrioRemitente,
                        CodigoPostalRemitente: $scope.Modelo.Remesa.CodigoPostalRemitente,
                        BarrioDestinatario: $scope.Modelo.Remesa.BarrioDestinatario,
                        CodigoPostalDestinatario: $scope.Modelo.Remesa.CodigoPostalDestinatario,
                        DetalleTarifaVenta: { Codigo: $scope.Modelo.Remesa.TipoTarifaCarga.CodigoDetalleTarifa }
                    };

                    $scope.AplicaReexpedicion = 0;
                    if ($scope.ListaReexpedicionOficinas !== undefined && $scope.ListaReexpedicionOficinas !== null) {
                        if ($scope.ListaReexpedicionOficinas.length > 0) {
                            $scope.AplicaReexpedicion = 1;
                        }
                    }

                    var ObtjetoFactura = {};

                    if (($scope.Modelo.Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO ||
                        $scope.Modelo.Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA) && $scope.Modelo.Remesa.RemesaCortesia == false) {
                        var ClienteFactura = 0;
                        if ($scope.Modelo.Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA) {
                            ClienteFactura = $scope.Modelo.Remesa.Destinatario.Codigo;
                        }
                        else {
                            if ($scope.Modelo.Remesa.Cliente == undefined || $scope.Modelo.Remesa.Cliente == '' || $scope.Modelo.Remesa.Cliente == null) {
                                ClienteFactura = $scope.Modelo.Remesa.Remitente.Codigo;
                            }
                            else {
                                ClienteFactura = $scope.Modelo.Remesa.Cliente.Codigo;
                            }
                        }
                        ObtjetoFactura = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_FACTURAS,
                            NumeroPreImpreso: CERO,
                            CataTipoFactura: CODIGO_CATALOGO_TIPO_FACTURA,
                            Fecha: $scope.Modelo.Remesa.Fecha,
                            OficinaFactura: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                            Cliente: { Codigo: ClienteFactura },
                            FacturarA: { Codigo: ClienteFactura },
                            FormaPago: { Codigo: $scope.Modelo.Remesa.FormaPago.Codigo },
                            DiasPlazo: CERO,
                            Observaciones: "",

                            ValorRemesas: $scope.ValorRemesas,
                            ValorOtrosConceptos: CERO,
                            ValorDescuentos: CERO,
                            Subtotal: $scope.ValorRemesas,
                            ValorImpuestos: 0,
                            ValorFactura: $scope.ValorRemesas,
                            ValorTRM: CERO,
                            Moneda: { Codigo: 1 },
                            ValorMonedaAlterna: CERO,
                            ValorAnticipo: CERO,
                            ResolucionFacturacion: $scope.Sesion.UsuarioAutenticado.Oficinas.ResolucionFacturacion,
                            ControlImpresion: 1,
                            Estado: $scope.Modelo.Estado.Codigo,
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            Anulado: CERO
                        };
                    }

                    //--Manejo Unidades
                    var ListaDetalleUnidades = [];
                    if ($scope.ListadoDetalleUnidades != undefined && $scope.ListadoDetalleUnidades != null) {
                        for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                            var detalle = $scope.ListadoDetalleUnidades[i];
                            ListaDetalleUnidades.push({
                                Producto: { Codigo: detalle.Producto.Codigo },
                                Referencia: detalle.Referencia,
                                Peso: detalle.Peso,
                                Largo: detalle.Largo,
                                Alto: detalle.Alto,
                                Ancho: detalle.Ancho,
                                PesoVolumetrico: detalle.PesoVolumetrico,
                                PesoCobrar: detalle.PesoCobrar,
                                ValorComercial: detalle.ValorComercial,
                                DescripcionProducto: detalle.DescripcionProducto,
                                ValorFlete: detalle.ValorFlete
                            });
                        }
                    }
                //--Manejo Unidades
                $scope.RemesaPaqueteria = {
                        CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                        Remesa: ObtjetoRemesa,
                        Factura: ObtjetoFactura,
                        TransportadorExterno: { Codigo: 0 },
                        NumeroGuiaExterna: $scope.NumeroGuiaExterna,
                        TipoTransporteRemsaPaqueteria: $scope.Modelo.TipoTransporteRemsaPaqueteria,
                        TipoDespachoRemesaPaqueteria: $scope.Modelo.TipoDespachoRemesaPaqueteria,
                        TemperaturaProductoRemesaPaqueteria: $scope.Modelo.TemperaturaProductoRemesaPaqueteria,
                        TipoServicioRemesaPaqueteria: $scope.Modelo.TipoServicioRemesaPaqueteria,
                        TipoEntregaRemesaPaqueteria: $scope.Modelo.TipoEntregaRemesaPaqueteria,

                        TipoInterfazRemesaPaqueteria: $scope.Modelo.TipoInterfazRemesaPaqueteria,
                        FechaInterfazTracking: $scope.Modelo.FechaInterfaz, /// ----> fecha interfaz
                        FechaInterfazWMS: $scope.Modelo.FechaInterfaz, /////
                        UsuarioCrea: $scope.Modelo.UsuarioCrea,
                        Oficina: $scope.Modelo.Oficina,
                        EstadoRemesaPaqueteria: $scope.Modelo.EstadoGuia,

                        ObservacionesDestinatario: $scope.Modelo.Remesa.ObservacionesDestinatario,
                        ObservacionesRemitente: $scope.Modelo.Remesa.ObservacionesRemitente,

                        OficinaOrigen: { Codigo: $scope.Modelo.OficinaOrigen.Codigo },
                        OficinaDestino: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                        OficinaActual: { Codigo: $scope.Modelo.OficinaActual.Codigo },
                        DesripcionMercancia: $scope.Modelo.DescripcionProductoTransportado,
                        Reexpedicion: $scope.AplicaReexpedicion,
                        ListaReexpedicionOficinas: $scope.ListaReexpedicionOficinas,
                        CentroCosto: $scope.Modelo.Remesa.CentroCosto,
                        FechaEntrega: $scope.Modelo.Remesa.FechaEntrega,
                        ValorReexpedicion: $scope.Modelo.ValorReexpedicion,
                        Largo: $scope.Modelo.Largo,
                        Alto: $scope.Modelo.Alto,
                        Ancho: $scope.Modelo.Ancho,
                        PesoVolumetrico: $scope.Modelo.PesoVolumetrico,
                        PesoCobrar: $scope.Modelo.PesoCobrar,
                        FletePactado: $scope.Modelo.FletePactado,
                        AjusteFlete: $scope.Modelo.AjusteFlete == true ? 1 : 0,
                        Numeracion: $scope.Modelo.Remesa.Numeracion,
                        LineaNegocioPaqueteria: { Codigo: $scope.Modelo.LineaNegocioPaqueteria.Codigo },
                        ManejaDetalleUnidades: $scope.ManejoDetalleUnidades == true ? 1 : 0,
                        RecogerOficinaDestino: $scope.Modelo.RecogerOficinaDestino == true ? 1 : 0,
                        OficinaRecibe: $scope.Modelo.RecogerOficinaDestino == true ? $scope.Modelo.OficinaRecibe : null,
                        DetalleUnidades: ListaDetalleUnidades,
                        CodigoZona: $scope.Modelo.Remesa.Zona.Codigo,
                        Latitud: $scope.Modelo.Remesa.Latitud,
                        Longitud: $scope.Modelo.Remesa.Longitud,
                        HorarioEntregaRemesa: { Codigo: $scope.Modelo.Remesa.HorarioEntrega.Codigo },
                        GestionDocumentosRemesa: GestionDocumentosRemesa,
                        Aforador: $scope.Modelo.Aforador,
                        OficinaRegistroManual: $scope.Modelo.OficinaRegistroManual,
                        ListadoDocumentos: ListaDocuemntos,
                        GuiaCreadaRutaConductor: RemesaGesphone == true ? 1 : 0,
                        DetalleEtiquetasPreimpresas: $scope.DetalleEtiquetasPreimpresas,

                        CodigoTarifaTransporte: $scope.Modelo.Remesa.TarifaCarga.Codigo,
                        ListaTipoTarifaCargaVenta: $scope.ListaTipoTarifaCargaVenta.map(a => {
                            return {
                                CodigoTarifa: a.CodigoTarifa,
                                Codigo: a.Codigo,
                                ValorFlete: a.ValorFlete,
                                PesoMinimo: a.PesoMinimo,
                                PesoMaximo: a.PesoMaximo,
                                CondicionesComerciales: a.CondicionesComerciales,
                                PorcentajeFlete: a.PorcentajeFlete,
                                PorcentajeVolumen: a.PorcentajeVolumen,
                                Reexpedicion: a.Reexpedicion,
                                FleteMinimo: a.FleteMinimo,
                                PorcentajeReexpedicion: a.PorcentajeReexpedicion,
                                PorcentajeSeguro: a.PorcentajeSeguro,
                                PorcentajeValorDeclarado: a.PorcentajeValorDeclarado,
                                ManejoMinimo: a.ManejoMinimo
                            };
                        }),
                        parametrosCalculos: {
                            OficinaObjPorcentajeSeguroPaqueteria: OficinaObj.PorcentajeSeguroPaqueteria ? OficinaObj.PorcentajeSeguroPaqueteria : 0,
                            PaqueteriaMinimoValorManejo: $scope.PaqueteriaMinimoValorManejo,
                            PaqueteriaMinimoValorSeguro: $scope.PaqueteriaMinimoValorSeguro,
                            PaqueteriaFleteMinimo: $scope.PaqueteriaFleteMinimo,
                            calculo: false,
                            ListadoDetalleUnidadesValorFlete: $scope.ListadoDetalleUnidades.map(a => a.ValorFlete),
                            ManejoDetalleUnidades: $scope.ManejoDetalleUnidades,
                            ManejoReexpedicionOficina: $scope.ManejoReexpedicionOficina,
                            Tarifario: { Codigo: $scope.Modelo.Remesa.NumeroTarifarioVenta ? $scope.Modelo.Remesa.NumeroTarifarioVenta : 0, TarifarioBase: $scope.Tarifario.TarifarioBase ? $scope.Tarifario.TarifarioBase : 1 }
                        },
                    };
                    try {
                        if ($scope.Modelo.Remesa.Remitente.LineaSerivicioCliente !== undefined && $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente !== '' && $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente !== null) {
                            $scope.RemesaPaqueteria.LineaSerivicioCliente = { Codigo: $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente.Codigo };
                        }
                    } catch (e) {

                    }
                    try {
                        if ($scope.Modelo.Remesa.Remitente.SitioCargue !== undefined && $scope.Modelo.Remesa.Remitente.SitioCargue !== '' && $scope.Modelo.Remesa.Remitente.SitioCargue !== null) {
                            $scope.RemesaPaqueteria.SitioCargue = { Codigo: $scope.Modelo.Remesa.Remitente.SitioCargue.Codigo };
                        }
                    } catch (e) {

                    }
                    try {
                        if ($scope.Modelo.Remesa.Destinatario.SitioDescargue !== undefined && $scope.Modelo.Remesa.Destinatario.SitioDescargue !== '' && $scope.Modelo.Remesa.Destinatario.SitioDescargue !== null) {
                            $scope.RemesaPaqueteria.SitioDescargue = { Codigo: $scope.Modelo.Remesa.Destinatario.SitioDescargue.Codigo };
                        }
                    } catch (e) {

                    }

                    if ($scope.Obtenida == true && $scope.Modelo.Estado.Codigo == ESTADO_DEFINITIVO) {
                        var Bitacora = RemesaGuiasFactory.GuardarBitacoraGuias({
                            Numero: $scope.Modelo.Remesa.Numero,
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            Observaciones: $scope.Modelo.Remesa.Observaciones

                        })
                    }

                    blockUIConfig.autoBlock = true;
                    blockUIConfig.delay = 0;
                    blockUI.start("Guardando Guía...");
                    $timeout(function () { blockUI.message("Guardando Guía..."); }, 100);
                    RemesaGuiasFactory.Guardar($scope.RemesaPaqueteria).
                        then(function (response) {
                            if (response.data.ProcesoExitoso == true) {
                                if (response.data.Datos > 0) {
                                    if (RemesaGesphone && $scope.ExistePlanilla && $scope.Modelo.Estado.Codigo == ESTADO_DEFINITIVO) {
                                        //--Proceso Adicionar Guia Planilla
                                        var responAsocioRemPlan = RemesaGuiasFactory.AsociarGuiaPlanilla({
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
                                            RemesaPaqueteria: [{
                                                Numero: response.data.Numero,

                                                NumeroDocumento: response.data.Datos
                                            }],
                                            NumeroPlanilla: $scope.PlanillaPaqueteria.Planilla.Numero,
                                            Sync: true
                                        });
                                        if (responAsocioRemPlan.ProcesoExitoso == true) {
                                            if ($scope.Modelo.Remesa.Numero == 0) {
                                                ShowSuccess('Se guardó la guía con el número ' + response.data.Datos + ' y se asoció a la planilla No. ' + $scope.PlanillaPaqueteria.Planilla.NumeroDocumento);
                                            }
                                            else {
                                                ShowSuccess('Se modificó la guía con el número ' + response.data.Datos + ' y se asoció a la planilla No. ' + $scope.PlanillaPaqueteria.Planilla.NumeroDocumento);
                                            }

                                        }
                                        else {
                                            if ($scope.Modelo.Remesa.Numero == 0) {
                                                ShowSuccess('Se guardó la guía con el número ' + response.data.Datos);
                                            }
                                            else {
                                                ShowSuccess('Se modificó la guía con el número ' + response.data.Datos);
                                            }
                                        }
                                    }
                                    else {
                                        if ($scope.Modelo.Remesa.Numero == 0) {
                                            ShowSuccess('Se guardó la guía con el número ' + response.data.Datos);
                                        }
                                        else {
                                            ShowSuccess('Se modificó la guía con el número ' + response.data.Datos);
                                        }
                                    }
                                    location.href = $scope.Master + "/" + response.data.Datos + "/" + OPCION_MENU;
                                }
                                else {
                                    if (response.data != undefined) {
                                        ShowError(response.data.MensajeOperacion);
                                    }
                                    else {
                                        ShowError(response.statusText);
                                    }
                                }
                            }
                            else {
                                if (response.data != undefined) {
                                    ShowError(response.data.MensajeOperacion);
                                }
                                else {
                                    ShowError(response.statusText);
                                }
                            }
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        }, function (response) {
                            if (response.statusText == 'Error: Usuario no cuenta con caja asignada a la oficina actual') {
                                showModal('modalConfirmacionGuardar');
                            } else {
                                ShowError(response.statusText);
                            }
                            
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        });
                    CrearTerceroSiesa()
                }
            }
        };
        $scope.ValidarRecibirGestionDocumento = function (item) {
            if (item.Recibido.Codigo == ESTADO_INACTIVO) {
                item.NumeroDocumentoGestion = "";
                item.NumeroHojas = "";
                item.ValorRecaudar = "";
            }
        };
        //----------------------------Guardar----------------------------//
        //--Direcciones Remitente
        $scope.GuardarDireccionRemitente = function () {
            if ($scope.Modal.Ciudad == undefined || $scope.Modal.Ciudad == null || $scope.Modal.Ciudad == '' || $scope.Modal.Direccion == undefined || $scope.Modal.Direccion == null || $scope.Modal.Direccion == '') {
                ShowError('Debe ingresar la cuidad y la dirección');
            } else {
                $scope.Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Tercero: { Codigo: $scope.Modelo.Remesa.Remitente.Codigo },
                    Direccion: $scope.Modal.Direccion,
                    Telefonos: $scope.Modal.Telefonos,
                    Ciudad: $scope.Modal.Ciudad,
                    Barrio: $scope.Modal.Barrio,
                    CodigoPostal: $scope.Modal.CodigoPostal
                };
                TercerosFactory.InsertarDirecciones($scope.Entidad).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDireccionesRemitente.push($scope.Entidad);
                        $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[($scope.ListadoDireccionesRemitente.length - 1)];
                        if ($scope.Modelo.Remesa.Destinatario.Codigo == $scope.Modelo.Remesa.Remitente.Codigo) {
                            $scope.ListadoDireccionesDestinatario.push($scope.Entidad);
                            $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[($scope.ListadoDireccionesDestinatario.length - 1)];
                        }
                        closeModal('modalAdicionarDireccionRemitente');
                    } else {
                        ShowError('No se pudo guardar la dirección');
                    }
                });

            }
        };
        //--Direcciones Remitente
        //--Direcciones Destinatario
        $scope.GuardarDireccionDestinatario = function () {
            if ($scope.Modal.Ciudad == undefined || $scope.Modal.Ciudad == null || $scope.Modal.Ciudad == '' || $scope.Modal.Direccion == undefined || $scope.Modal.Direccion == null || $scope.Modal.Direccion == '') {
                ShowError('Debe ingresar la cuidad y la dirección');
            } else {
                $scope.Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Tercero: { Codigo: $scope.Modelo.Remesa.Destinatario.Codigo },
                    Direccion: $scope.Modal.Direccion,
                    Telefonos: $scope.Modal.Telefonos,
                    Ciudad: $scope.Modal.Ciudad,
                    Barrio: $scope.Modal.Barrio,
                    CodigoPostal: $scope.Modal.CodigoPostal
                };
                TercerosFactory.InsertarDirecciones($scope.Entidad).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDireccionesDestinatario.push($scope.Entidad);
                        $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[($scope.ListadoDireccionesDestinatario.length - 1)];
                        if ($scope.Modelo.Remesa.Destinatario.Codigo == $scope.Modelo.Remesa.Remitente.Codigo) {
                            $scope.ListadoDireccionesRemitente.push($scope.Entidad);
                            $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[($scope.ListadoDireccionesRemitente.length - 1)];
                        }
                        closeModal('modalAdicionarDireccionDestinatario');
                    } else {
                        ShowError('No se pudo guardar la dirección');
                    }
                });

            }
        };
        //--Direcciones Destinatario
        //--Adicionar Oficina
        $scope.AdicionarOficinas = function () {
            if ($scope.Modelo.Remesa.Oficina == undefined || $scope.Modelo.Remesa.Oficina == null || $scope.Modelo.Remesa.Oficina == '' ||
                $scope.Modelo.Remesa.TipoReexpedicion == undefined || $scope.Modelo.Remesa.TipoReexpedicion == null || $scope.Modelo.Remesa.TipoReexpedicion == ''

            ) {
                ShowError('Debe ingresar la oficina, el tipo de reexpedición')
            } else {
                if ($scope.Modelo.Remesa.TipoReexpedicion.Codigo == 11102 && ($scope.Modelo.Remesa.ProveedorExterno == undefined || $scope.Modelo.Remesa.ProveedorExterno == null || $scope.Modelo.Remesa.ProveedorExterno == '')) {
                    ShowError('Debe ingresar el proveedor externo')
                }
                else {
                    if ($scope.Modelo.Remesa.Valor == undefined || $scope.Modelo.Remesa.Valor == null || $scope.Modelo.Remesa.Valor == '' || $scope.Modelo.Remesa.Valor == 0 || $scope.Modelo.Remesa.Valor == '0') {
                        $scope.Modelo.Remesa.Valor = 0;
                    }
                    try {
                        if ($scope.ListaReexpedicionOficinas.length > 0) {
                            var cont = 0;
                            for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                                if ($scope.ListaReexpedicionOficinas[i].Oficina.Codigo == $scope.Modelo.Remesa.Oficina.Codigo) {
                                    cont++;
                                    break;
                                }
                            }
                            if (cont > 0) {
                                ShowError('La oficina que intenta ingresar ya se encuentra registrada');
                                $scope.Modelo.Remesa.Oficina = '';
                                $scope.Modelo.Remesa.Valor = '';
                            } else {
                                if ($scope.Modelo.Remesa.RemesaCortesia == true) {
                                    $scope.Modelo.Remesa.Valor = 0;
                                }
                                $scope.ListaReexpedicionOficinas.push({
                                    Oficina: $scope.Modelo.Remesa.Oficina,
                                    TipoReexpedicion: $scope.Modelo.Remesa.TipoReexpedicion,
                                    Valor: $scope.Modelo.Remesa.Valor,
                                    ProveedorExterno: $scope.Modelo.Remesa.ProveedorExterno,
                                    Observaciones: $scope.Modelo.Remesa.ObservacionesReexpedicion,
                                    Estado: MascaraNumero($scope.Modelo.Remesa.Valor) > 0 ? 1 : 0
                                });
                                $scope.Modelo.Remesa.Oficina = '';
                                $scope.Modelo.Remesa.Valor = '';
                                $scope.Modelo.Remesa.ObservacionesReexpedicion = '';
                                $scope.Modelo.Remesa.ProveedorExterno = '';

                            }
                        } else {
                            $scope.ListaReexpedicionOficinas = []
                            if ($scope.Modelo.Remesa.RemesaCortesia == true) {
                                $scope.Modelo.Remesa.Valor = 0;
                            }
                            $scope.ListaReexpedicionOficinas.push({
                                Oficina: $scope.Modelo.Remesa.Oficina,
                                TipoReexpedicion: $scope.Modelo.Remesa.TipoReexpedicion,
                                Valor: $scope.Modelo.Remesa.Valor,
                                ProveedorExterno: $scope.Modelo.Remesa.ProveedorExterno,
                                Observaciones: $scope.Modelo.Remesa.ObservacionesReexpedicion,
                                Estado: MascaraNumero($scope.Modelo.Remesa.Valor) > 0 ? 1 : 0
                            });
                            $scope.Modelo.Remesa.Oficina = '';
                            $scope.Modelo.Remesa.Valor = '';
                            $scope.Modelo.Remesa.ObservacionesReexpedicion = '';
                            $scope.Modelo.Remesa.ProveedorExterno = '';
                        }
                    } catch (e) {
                        if ($scope.Modelo.Remesa.RemesaCortesia == true) {
                            $scope.Modelo.Remesa.Valor = 0;
                        }
                        $scope.ListaReexpedicionOficinas = [];
                        $scope.ListaReexpedicionOficinas.push({
                            Oficina: $scope.Modelo.Remesa.Oficina,
                            TipoReexpedicion: $scope.Modelo.Remesa.TipoReexpedicion,
                            Valor: $scope.Modelo.Remesa.Valor,
                            ProveedorExterno: $scope.Modelo.Remesa.ProveedorExterno,
                            Observaciones: $scope.Modelo.Remesa.ObservacionesReexpedicion,
                            Estado: MascaraNumero($scope.Modelo.Remesa.Valor) > 0 ? 1 : 0
                        });
                        $scope.Modelo.Remesa.Oficina = '';
                        $scope.Modelo.Remesa.Valor = '';
                        $scope.Modelo.Remesa.ObservacionesReexpedicion = '';
                        $scope.Modelo.Remesa.ProveedorExterno = '';
                    }
                }
            }
            $scope.Calcular();
        };
        //--Adicionar Oficina
        //--Reexpedicion
        $scope.EliminarOficinaReexpedicion = function (i) {
            $scope.ListaReexpedicionOficinas.splice(i, 1);
            $scope.Calcular();
        };
        //--Reexpedicion
        //--Calcular Ajuste Totales

        $scope.CalcularAjusteTotal = function () {
            if ($scope.FleteAnterior != 0) {
                if (MascaraNumero($scope.Modelo.Remesa.ValorFleteCliente) > $scope.FleteAnterior) {
                    $('#ValorFleteCliente').popover({
                        html: true,
                        placement: "bottom",
                        content: '<p>',
                        template: '<div class="popover" style="background-color:#f58300a1" role="tooltip"><div class="popover-body"> <p> <i class="fas fa-exclamation-triangle"></i> El valor ingresado exede el valor del flete</div></div>'
                    });
                    $('#ValorFleteCliente').popover('show');
                } else {
                    $('#ValorFleteCliente').popover('hide')
                }
            }
        };
        //--Calcular Ajuste Totales
        //-----------------------Manejo Detalle Unidades------------------------------//
        $scope.TmpEliminarDetalleUnidad = {};
        $scope.EsEditarDetalleUnidad = false;
        $scope.IndiceEditarDetalleUnidad = 0;
        $scope.ListadoDetalleUnidades = [];
        $scope.DetalleUnidades = {
            Producto: '',
            Referencia: '',
            Peso: '',
            Largo: '',
            Alto: '',
            Ancho: '',
            PesoVolumetrico: '',
            PesoCobrar: '',
            ValorComercial: '',
            DescripcionProducto: ''
        };
        //----Asignar Peso Producto
        $scope.AsignarDetallePesoProducto = function () {
            if ($scope.DetalleUnidades.Producto.Peso > 0) {
                $scope.DetalleUnidades.Peso = $scope.DetalleUnidades.Producto.Peso;
            }
            $scope.DetalleUnidades.DescripcionProducto = $scope.DetalleUnidades.Producto.Nombre;
        };
        //----Asignar Peso Producto
        //----Asignar Tarifa Detalle Unidades
        $scope.ValidarDetalleUnidadesTarifaProducto = function () {
            if ($scope.Modelo.Remesa.TarifaCarga.Codigo == TARIFA_PRODUCTO_PAQUETERIA) {
                for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                    if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa &&
                        ($scope.ListaTipoTarifaCargaVenta[i].Codigo == $scope.DetalleUnidades.Producto.Codigo)) {
                        $scope.DetalleUnidades.ValorFlete = parseFloat(($scope.ListaTipoTarifaCargaVenta[i].ValorFlete).toFixed(2));
                        $scope.DetalleUnidades.ValorFlete = MascaraValores($scope.DetalleUnidades.ValorFlete);
                        existe = true;
                        break;
                    }
                }
            }
        };
        //----Asignar Tarifa Detalle Unidades
        $scope.ValidarDetalleUnidades = function (valor) {
            if (valor == true) {
                $scope.ListadoDetalleUnidades = [];
                $scope.Modelo.Remesa.CantidadCliente = 0;
                $scope.Modelo.Remesa.PesoCliente = '';
                $scope.Modelo.PesoCobrar = '';
                $scope.Modelo.Remesa.ValorComercialCliente = '';
                /*$scope.Modelo.Remesa.CantidadCliente = $scope.ListadoDetalleUnidades.length;
                var tmpPeso = 0;
                var tmpValorComercial = 0;
                for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                    tmpPeso += RevertirMV($scope.ListadoDetalleUnidades[i].PesoCobrar);
                    tmpValorComercial += RevertirMV($scope.ListadoDetalleUnidades[i].ValorComercial);
                }
                $scope.Modelo.Remesa.PesoCliente = tmpPeso.toFixed(2);
                $scope.Modelo.Remesa.PesoCobrar = tmpPeso.toFixed(2);
                $scope.Modelo.Remesa.ValorComercialCliente = tmpValorComercial;
                IndiceDetalleUnidad();
                */
            }
            else {
                $scope.Modelo.Remesa.CantidadCliente = 0;
                $scope.Modelo.Remesa.PesoCliente = '';
                $scope.Modelo.PesoCobrar = '';
                $scope.Modelo.Remesa.ValorComercialCliente = '';
            }
            $scope.Modelo.Largo = '';
            $scope.Modelo.Alto = '';
            $scope.Modelo.Ancho = '';
            $scope.Modelo.PesoVolumetrico = '';
            $scope.Modelo.DescripcionProductoTransportado = '';
            $scope.Calcular();

        };
        function limpiarModalDetalleUnidad() {
            $scope.DetalleUnidades = {
                Producto: '',
                Referencia: '',
                Cantidad: 1,
                Peso: 0,
                Largo: 0,
                Alto: 0,
                Ancho: 0,
                PesoVolumetrico: 0,
                PesoCobrar: 0,
                ValorComercial: '',
                DescripcionProducto: '',
                ValorFlete: ''
            };
            $scope.IndiceEditarDetalleUnidad = 0;
            $scope.EsEditarDetalleUnidad = false;
            $scope.MostrarDetalleCantidad = true;
        }

        $scope.ModalDetalleUnidades = function () {
            if ($scope.Modelo.Remesa.TarifaCarga !== undefined && $scope.Modelo.Remesa.TarifaCarga !== null && $scope.Modelo.Remesa.TarifaCarga !== '' &&
                $scope.Modelo.Remesa.ProductoTransportado !== undefined && $scope.Modelo.Remesa.ProductoTransportado !== null && $scope.Modelo.Remesa.ProductoTransportado !== '') {
                var existe = false;
                var valorflete = 0;
                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == TARIFA_PRODUCTO_PAQUETERIA) {
                    for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa &&
                            ($scope.ListaTipoTarifaCargaVenta[i].Codigo == $scope.Modelo.Remesa.ProductoTransportado.Codigo)) {
                            valorflete = parseFloat(($scope.ListaTipoTarifaCargaVenta[i].ValorFlete).toFixed(2));
                            existe = true;
                            break;
                        }
                    }
                }
                else {
                    existe = true;
                }
                if (existe == true) {
                    limpiarModalDetalleUnidad();
                    $scope.DetalleUnidades.Producto = $scope.Modelo.Remesa.ProductoTransportado;
                    $scope.DetalleUnidades.Peso = $scope.DetalleUnidades.Producto.Peso;
                    $scope.DetalleUnidades.PesoCobrar = $scope.DetalleUnidades.Producto.Peso;
                    $scope.DetalleUnidades.ValorFlete = MascaraValores(valorflete);
                    $scope.DetalleUnidades.DescripcionProducto = $scope.Modelo.Remesa.ProductoTransportado.Nombre;
                    showModal('modalAdicionarDetalleUnidades');
                }
                else {
                    ShowError("Se debe seleccionar producto asignado a una tarifa");
                }
            }
            else {
                ShowError("Se debe seleccionar una tarifa y Producto");
            }
        };
        //--Validar Peso Volumetrico Detalle
        $scope.ValidarPesoVolumetricoDetalle = function () {
            var Detalle = $scope.DetalleUnidades;
            var tmpPesoVol = 0;
            var PesoVolumetrico = 0;
            if ($scope.ManejoPesoVolumetrico) {
                if (Detalle.Alto !== undefined && Detalle.Alto !== '' && Detalle.Alto !== '' &&
                    Detalle.Ancho !== undefined && Detalle.Ancho !== '' && Detalle.Ancho !== '' &&
                    Detalle.Largo !== undefined && Detalle.Largo !== '' && Detalle.Largo !== '') {
                    if (Detalle.Alto > 0 && Detalle.Ancho > 0 && Detalle.Largo > 0) {
                        tmpPesoVol = ((RevertirMV(Detalle.Alto) * RevertirMV(Detalle.Ancho) * RevertirMV(Detalle.Largo)) / 1000000) * 400;
                        tmpPesoVol = tmpPesoVol.toFixed(2);
                        if (tmpPesoVol != $scope.DetalleUnidades.PesoVolumetrico) {
                            Detalle.Alto = 0;
                            Detalle.Ancho = 0;
                            Detalle.Largo = 0;
                        }
                    }
                }
            }
        };
        //--Validar Peso Volumetrico Detalle
        $scope.CalcularDetalleUnidades = function () {
            var Detalle = $scope.DetalleUnidades;
            var PesoVolumetrico = 0;
            if ($scope.ManejoPesoVolumetrico) {
                if (Detalle.Alto !== undefined && Detalle.Alto !== '' && Detalle.Alto !== '' &&
                    Detalle.Ancho !== undefined && Detalle.Ancho !== '' && Detalle.Ancho !== '' &&
                    Detalle.Largo !== undefined && Detalle.Largo !== '' && Detalle.Largo !== '') {
                    if (Detalle.Alto > 0 && Detalle.Ancho > 0 && Detalle.Largo > 0) {
                        PesoVolumetrico = ((RevertirMV(Detalle.Alto) * RevertirMV(Detalle.Ancho) * RevertirMV(Detalle.Largo)) / 1000000) * 400;
                        PesoVolumetrico = PesoVolumetrico.toFixed(2);
                        $scope.DetalleUnidades.PesoVolumetrico = PesoVolumetrico;
                    }
                }
                //else {
                //    $scope.DetalleUnidades.PesoVolumetrico = '';
                //}
                if (RevertirMV($scope.DetalleUnidades.PesoVolumetrico) > 0 || RevertirMV(Detalle.Peso) > 0) {
                    if (RevertirMV(Detalle.Peso) > RevertirMV($scope.DetalleUnidades.PesoVolumetrico))
                        $scope.DetalleUnidades.PesoCobrar = RevertirMV(Detalle.Peso);
                    else
                        $scope.DetalleUnidades.PesoCobrar = RevertirMV($scope.DetalleUnidades.PesoVolumetrico);
                }
                else {
                    $scope.DetalleUnidades.PesoCobrar = RevertirMV(Detalle.Peso);
                }
            }
            else {
                $scope.DetalleUnidades.PesoCobrar = RevertirMV(Detalle.Peso);
            }
        };
        $scope.AgregarDetalleUnidades = function () {
            if (DatosRequeridosDetalleUnidades()) {
                $scope.ValidarDetalleUnidadesTarifaProducto();
                $scope.CalcularDetalleUnidades();
                if ($scope.EsEditarDetalleUnidad == false) {
                    //--Agrega Nuevo Detalle
                    //$scope.ListadoDetalleUnidades.push($scope.DetalleUnidades);
                    var cantidad = $scope.DetalleUnidades.Cantidad;
                    $scope.DetalleUnidades.Cantidad = 1;
                    for (var i = 0; i < cantidad; i++) {
                        $scope.ListadoDetalleUnidades.push(angular.copy($scope.DetalleUnidades));
                    }
                }
                else {
                    //--Edita Detalle Actual
                    $scope.ListadoDetalleUnidades[$scope.IndiceEditarDetalleUnidad] = $scope.DetalleUnidades;
                }
                $scope.Modelo.Remesa.CantidadCliente = $scope.ListadoDetalleUnidades.length;
                var tmpPeso = 0;
                var tmpValorComercial = 0;
                for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                    tmpPeso += RevertirMV($scope.ListadoDetalleUnidades[i].PesoCobrar);
                    tmpValorComercial += RevertirMV($scope.ListadoDetalleUnidades[i].ValorComercial);
                }
                $scope.Modelo.Remesa.PesoCliente = tmpPeso.toFixed(2);
                $scope.Modelo.Remesa.PesoCobrar = tmpPeso.toFixed(2);
                $scope.Modelo.Remesa.ValorComercialCliente = tmpValorComercial;
                $scope.Calcular();
                IndiceDetalleUnidad();
                closeModal('modalAdicionarDetalleUnidades');
            }
        };
        function DatosRequeridosDetalleUnidades() {
            $scope.MensajesErrorDetalleUnidades = [];
            var continuar = true;
            var detalle = $scope.DetalleUnidades;
            if (ValidarCampo(detalle.Producto) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorDetalleUnidades.push('Debe ingresar el producto ');
                continuar = false;
            }
            else {
                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == TARIFA_PRODUCTO_PAQUETERIA) {
                    var existe = false;
                    for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa &&
                            ($scope.ListaTipoTarifaCargaVenta[i].Codigo == detalle.Producto.Codigo)) {
                            existe = true;
                            break;
                        }
                    }
                    if (!existe) {
                        $scope.MensajesErrorDetalleUnidades.push('El producto no esta asignado a la tarifa seleccionada');
                        continuar = false;
                    }
                }
            }
            if (ValidarCampo(detalle.PesoCobrar) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorDetalleUnidades.push('Debe ingresar el peso o las dimensiones');
                continuar = false;
            }
            if (ValidarCampo(detalle.Cantidad) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorDetalleUnidades.push('Debe ingresar No. unidades');
                continuar = false;
            }
            if (ValidarCampo(detalle.DescripcionProducto) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorDetalleUnidades.push('Debe ingresar la descripción de la mercancia')
                continuar = false;
            }
            return continuar;
        }
        //--Editar Detalle Unidades
        $scope.EditarDetalleUnidades = function (indice) {
            $scope.DetalleUnidades = angular.copy($scope.ListadoDetalleUnidades[indice]);
            $scope.IndiceEditarDetalleUnidad = indice;
            $scope.EsEditarDetalleUnidad = true;
            $scope.MostrarDetalleCantidad = false;
            showModal('modalAdicionarDetalleUnidades');
        };
        //--Editar Detalle Unidades
        //---Eliminar Detalle Unidades
        $scope.ConfirmacionEliminarDetalleUnidades = function (Producto, indice) {
            $scope.TmpEliminarDetalleUnidad = { Producto, Indice: indice };
            showModal('modalEliminarDetalleUnidad');
        };
        $scope.EliminarDetalleUnidad = function (indice) {
            $scope.ListadoDetalleUnidades.splice(indice, 1);
            $scope.Modelo.Remesa.CantidadCliente = $scope.ListadoDetalleUnidades.length;
            var tmpPeso = 0;
            var tmpValorComercial = 0;
            for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                tmpPeso += RevertirMV($scope.ListadoDetalleUnidades[i].PesoCobrar);
                tmpValorComercial += RevertirMV($scope.ListadoDetalleUnidades[i].ValorComercial);
            }
            $scope.Modelo.Remesa.PesoCliente = tmpPeso.toFixed(2);
            $scope.Modelo.Remesa.PesoCobrar = tmpPeso.toFixed(2);
            $scope.Modelo.Remesa.ValorComercialCliente = tmpValorComercial;
            IndiceDetalleUnidad();
            $scope.Calcular();
            closeModal('modalEliminarDetalleUnidad');
        };
        //---Eliminar Detalle Unidades
        function IndiceDetalleUnidad() {
            for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                $scope.ListadoDetalleUnidades[i].Indice = i;
            }
        }
        //-----------------------Manejo Detalle Unidades------------------------------//
        //-------------------------Gestion Documental Cliente------------------------------//
        $scope.MarcarDocuGestionClie = function (chk) {
            for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                $scope.ListaGestionDocuCliente[i].Seleccionado = chk;
            }
        };
        //-------------------------Gestion Documental Cliente------------------------------//
        //-----------------------Mapa Google------------------------------//
        $scope.AbrirMapa = function (lat, lng) {
            showModal('modalMostrarMapa');
            iniciarMapaGoogle(lat, lng);
        };
        function iniciarMapaGoogle(lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.MapaLatitud = lat != '' ? lat : 0;
            $scope.MapaLongitud = lng != '' ? lng : 0;
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    };
                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };
                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.MapaLatitud = this.getPosition().lat();
                    $scope.MapaLongitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.MapaLatitud != undefined && $scope.MapaLongitud != undefined && $scope.MapaLatitud != 0 && $scope.MapaLongitud != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.MapaLatitud, $scope.MapaLongitud), 'Posición del usuario', 'Destino');
                } else {
                    $scope.MapaLatitud = posicion.coords.latitude;
                    $scope.MapaLongitud = posicion.coords.longitude;
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }
        $scope.AsignarPosicion = function () {
            $scope.Modelo.Remesa.Latitud = $scope.MapaLatitud;
            $scope.Modelo.Remesa.Longitud = $scope.MapaLongitud;
            closeModal('modalMostrarMapa');
        };
        //-----------------------Mapa Google------------------------------//
        //-----------------------Documental------------------------------//
        $scope.AgregarFilaDocumento = function () {
            if ($scope.ListadoDocumentos.length < 5) {
                var itmDoc = {
                    Descripcion: '',
                    AplicaDescargar: 0,
                    AplicaArchivo: 1,
                    ValorDocumento: 0,
                    TipoArchivoDocumento: $scope.CadenaFormatos
                };
                $scope.ListadoDocumentos.push(itmDoc);
            } else {
                ShowError("Máximo se permite cargar cinco (5) documentos");
            }
        };

        $scope.CargarArchivo = function (element) {
            $scope.IdPosiction = parseInt(element.id, 10);
            var continuar = true;
            if (element.files.length > 0) {
                $scope.PDF = element;
                $scope.TipoDocumento = element;
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento = angular.element(this.ItemFoto)[0];
                } else {
                    angular.element(this.item)[0].Temporal = true
                    $scope.Documento = angular.element(this.item)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos: 0");
                    continuar = false;
                }
                if (continuar == true) {
                    if (element.files[0].size >= 3000000) { //3 MB
                        ShowError("El archivo " + element.files[0].name + " supera los " + "3 MB" + ", intente con otro archivo", false);
                        continuar = false;
                    }
                    var Formatos = $scope.CadenaFormatos.split(',');
                    var cont = 0;
                    for (var i = 0; i < Formatos.length; i++) {
                        Formatos[i] = Formatos[i].replace(' ', '');
                        var Extencion = element.files[0].name.split('.');
                        //if (element.files[0].type == Formatos[i]) {
                        if ('.' + (Extencion[Extencion.length - 1].toUpperCase()) == Formatos[i].toUpperCase()) {
                            cont++;
                        }
                    }
                    if (cont == 0) {
                        ShowError("Archivo invalido, por favor verifique el formato del archivo.  Formatos permitidos:" + $scope.CadenaFormatos);
                        continuar = false;
                    }
                }
                if (continuar == true) {
                    if ($scope.Documento.Archivo != undefined && $scope.Documento.Archivo != '' && $scope.Documento.Archivo != null) {
                        $scope.Documento.ArchivoTemporal = element.files[0];
                        showModal('modalConfirmacionRemplazarDocumentoRemesa');
                    } else {
                        var reader = new FileReader();
                        $scope.Documento.ArchivoCargado = element.files[0];
                        reader.onload = $scope.AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }
            }
        };

        $scope.RemplazarDocumentoRemesa = function () {
            var reader = new FileReader();
            $scope.Documento.ArchivoCargado = $scope.Documento.ArchivoTemporal;
            reader.onload = $scope.AsignarArchivo;
            reader.readAsDataURL($scope.Documento.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumentoRemesa');
        };

        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                //imagenes, tienen un proceso aparte ya que se deben redimencionar para que no ocupen mucho espacio en la BD
                if ($scope.Documento.ArchivoCargado.type.includes("image")) {
                    var img = new Image();
                    img.src = e.target.result;
                    $timeout(function () {
                        $('#Foto' + $scope.IdPosiction).show();
                        $('#FotoCargada' + $scope.IdPosiction).hide();
                        if (img.height > img.width) {
                            RedimencionarImagen('CanvasVertical', img, 699, 499);
                            $scope.Documento.Archivo = document.getElementById('CanvasVertical').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                        }
                        //la imagen es horizontal
                        else {
                            RedimencionarImagen('CanvasHorizontal', img, 499, 699);
                            $scope.Documento.Archivo = document.getElementById('CanvasHorizontal').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                        }
                        $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type;
                        var Extencion = $scope.Documento.ArchivoCargado.name.split('.');
                        var tmpNombre = $scope.Documento.ArchivoCargado.name.split('.').slice(0, -1).join('.');
                        $scope.Documento.NombreDocumento = tmpNombre;
                        $scope.Documento.ExtensionDocumento = Extencion[Extencion.length - 1];
                        $scope.InsertarDocumento();
                    }, 100);
                }
                //Resto de archivos
                else {
                    $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                    $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type;
                    var Extencion = $scope.Documento.ArchivoCargado.name.split('.');
                    var tmpNombre = $scope.Documento.ArchivoCargado.name.split('.').slice(0, -1).join('.');
                    $scope.Documento.NombreDocumento = tmpNombre;
                    $scope.Documento.ExtensionDocumento = Extencion[Extencion.length - 1];
                    $scope.InsertarDocumento();
                }
            });
        };

        $scope.SeleccionarObjeto = function (item) {
            $scope.DocumentoSeleccionado = item;
        };

        $scope.InsertarDocumento = function () {
            $timeout(function () {
                // Guardar Archivo temporal
                var Documento = {};
                Documento = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Codigo: $scope.DocumentoSeleccionado.Id,
                    Numero: $scope.Modelo.Remesa.Numero,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                    Descripcion: $scope.Documento.Descripcion,
                    Nombre: $scope.Documento.NombreDocumento,
                    Extension: $scope.Documento.ExtensionDocumento,
                    Tipo: $scope.Documento.Tipo,
                    Archivo: $scope.Documento.Archivo,
                    Temporal: true
                };
                DocumentosFactory.InsertarDocumentoRemesa(Documento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            // Se guardó correctamente el pdf
                            if (response.data.Datos) {
                                $scope.ListadoDocumentos.forEach(function (item) {
                                    if (item.$$hashKey == $scope.DocumentoSeleccionado.$$hashKey) {
                                        item.Id = response.data.Datos;
                                        item.ValorDocumento = 1;
                                        item.AplicaEliminar = 1;
                                        item.temp = true;
                                    }
                                });
                                ShowSuccess('Se cargó el documento adjunto "' + $scope.Documento.NombreDocumento + '"');
                                $scope.Documento = '';
                            }
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }, 200);
        };

        $scope.ElimiarDocumento = function (EliminarDocumento) {
            var EliminarDocu = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: EliminarDocumento.Id,
                Numero: $scope.Modelo.Remesa.Numero,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                Temporal: EliminarDocumento.temp
            };

            DocumentosFactory.EliminarDocumentoRemesa(EliminarDocu).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                            var item = $scope.ListadoDocumentos[i];
                            if (item.Id == EliminarDocumento.Id) {
                                $scope.ListadoDocumentos.splice(i, 1);
                            }
                        }
                    }
                    else {
                        MensajeError("Error", response.data.MensajeError, false);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {
            if (item.temp) {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Remesa&ID=' + item.Id + '&Temp=1' + '&TIDO_Codigo=' + CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA);
            }
            else {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Remesa&ID=' + item.Id + '&ENRE_Numero=' + parseInt($scope.Modelo.Remesa.Numero) + '&Temp=0' + '&TIDO_Codigo=' + CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA);
            }
        };
        //-----------------------Documental------------------------------//
        //-------------------Preimpreso Guia------------------------//
        $scope.CargarInfoPreimpreso = function () {
            $scope.MensajesErrorPreimpreso = [];
            if ($scope.Modelo.Remesa.Numeracion != undefined && $scope.Modelo.Remesa.Numeracion != null && $scope.Modelo.Remesa.Numeracion != "") {
                var filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroInicial: $scope.Modelo.Remesa.Numeracion,
                    NumeroFinal: $scope.Modelo.Remesa.Numeracion,
                    Estado: 0,
                    Sync: true
                };
                //if ($scope.Modelo.OficinaRegistroManual != undefined && $scope.Modelo.OficinaRegistroManual != "" && $scope.Modelo.OficinaRegistroManual != null) {
                //    filtro.Oficina = { Codigo: $scope.Modelo.OficinaRegistroManual.Codigo };
                //}
                //if ($scope.Modelo.Aforador != undefined && $scope.Modelo.Aforador != "" && $scope.Modelo.Aforador != null) {
                //    filtro.Responsable = { Codigo: $scope.Modelo.Aforador.Codigo };
                //}
                var response = GuiasPreimpresasFactory.ConsultarPrecinto(filtro);
                if (response.ProcesoExitoso) {
                    if (response.Datos.length > 0) {
                        $scope.Modelo.Aforador = $scope.CargarTercero(response.Datos[0].Responsable.Codigo);
                        $scope.Modelo.OficinaRegistroManual = $linq.Enumerable().From($scope.ListaOficinasRegistroManual).First('$.Codigo ==' + response.Datos[0].Oficina.Codigo);
                        $scope.Modelo.OficinaOrigen = $linq.Enumerable().From($scope.ListaOficinasRegistroManual).First('$.Codigo ==' + response.Datos[0].Oficina.Codigo);
                        $scope.Modelo.OficinaActual = $linq.Enumerable().From($scope.ListaOficinasRegistroManual).First('$.Codigo ==' + response.Datos[0].Oficina.Codigo);
                    }
                    else {
                        $scope.Modelo.Remesa.Numeracion = "";
                        $scope.UsuarioAforador ? $scope.Modelo.Aforador = $scope.UsuarioAforador : $scope.Modelo.Aforador = "";
                        $scope.Modelo.OficinaRegistroManual = $linq.Enumerable().From($scope.ListaOficinasRegistroManual).First('$.Codigo ==0');
                        $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                        $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;
                    }
                    $scope.DetalleEtiquetasPreimpresas = [];
                }
            }
        };
        $scope.ValidarPreimpreso = function () {
            PreimpresoValido = true;
            $scope.MensajesErrorPreimpreso = [];
            if ($scope.Modelo.Remesa.Numeracion != undefined && $scope.Modelo.Remesa.Numeracion != null && $scope.Modelo.Remesa.Numeracion != "") {
                if ($scope.Modelo.Aforador != undefined && $scope.Modelo.Aforador != null && $scope.Modelo.Aforador != "" &&
                    $scope.Modelo.OficinaRegistroManual != undefined && $scope.Modelo.OficinaRegistroManual != null && $scope.Modelo.OficinaRegistroManual != "") {
                    var filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroInicial: $scope.Modelo.Remesa.Numeracion,
                        NumeroFinal: $scope.Modelo.Remesa.Numeracion,
                        Oficina: { Codigo: $scope.Modelo.OficinaRegistroManual.Codigo },
                        Responsable: { Codigo: $scope.Modelo.Aforador.Codigo },
                        Estado: 0,
                        Sync: true
                    };
                    var response = GuiasPreimpresasFactory.ConsultarPrecinto(filtros);
                    if (response.ProcesoExitoso) {
                        if (response.Datos.length == 0) {
                            //--Precinto Invalido
                            PreimpresoValido = false;
                            $scope.MensajesErrorPreimpreso.push("El No. Preimpreso no existe para el aforador y la oficina especificada");
                        } else if (response.Datos[0].Remesa.NumeroDocumento > 0) {
                            //--guia preimpresa ya asignada
                            PreimpresoValido = false;
                            $scope.MensajesErrorPreimpreso.push("El No. Preimpreso ingresado ya está asociado a una guía");
                        }
                    }
                    else {
                        //--Precinto Invalido
                        PreimpresoValido = false;
                        $scope.MensajesErrorPreimpreso.push("El No. Preimpreso no existe para el aforador y la oficina especificada");
                    }
                }
                else {
                    $scope.Modelo.Remesa.Numeracion = "";
                }
            }
        };
        //-------------------Preimpreso Guia------------------------//
        //-----------------------Etiquetas Preimpresas----------------------//
        $scope.DetalleEtiquetasPreimpresas = [];
        $scope.AgregarEtiqueta = function (etiqueta) {
            if (etiqueta != undefined && etiqueta != "") {
                if ($scope.DetalleEtiquetasPreimpresas.length > 0) {
                    var existe = false;
                    for (var i = 0; i < $scope.DetalleEtiquetasPreimpresas.length; i++) {
                        if (etiqueta == $scope.DetalleEtiquetasPreimpresas[i]) {
                            existe = true;
                            break;
                        }
                    }
                    if (!existe) {
                        if ($scope.ListadoDetalleUnidades != null) {
                            if ($scope.DetalleEtiquetasPreimpresas.length < $scope.ListadoDetalleUnidades.length) {
                                $scope.EtiquetaVsCantidad(etiqueta)
                            } else if ($scope.ManejoDetalleUnidades == false) {
                                if ($scope.DetalleEtiquetasPreimpresas.length < $scope.Modelo.Remesa.CantidadCliente) {
                                    $scope.EtiquetaVsCantidad(etiqueta)
                                } else {
                                    ShowError('El número de etiquetas no debe exceder el número de unidades')

                                }
                            } else {
                                ShowError('El número de etiquetas no debe exceder el número de unidades')

                            }
                        } else if ($scope.ManejoDetalleUnidades == false) {
                            if ($scope.DetalleEtiquetasPreimpresas.length < $scope.Modelo.Remesa.CantidadCliente) {
                                $scope.EtiquetaVsCantidad(etiqueta)
                            } else {
                                ShowError('El número de etiquetas no debe exceder el número de unidades')

                            }
                        } else {
                            ShowError('El número de etiquetas no debe exceder el número de unidades')

                        }
                    }
                }
                else {
                    if ($scope.DetalleEtiquetasPreimpresas.length < $scope.ListadoDetalleUnidades.length) {
                        $scope.EtiquetaVsCantidad(etiqueta)
                    }
                    else if ($scope.ManejoDetalleUnidades == false) {
                        if ($scope.DetalleEtiquetasPreimpresas.length < $scope.Modelo.Remesa.CantidadCliente) {
                            $scope.EtiquetaVsCantidad(etiqueta)
                        } else {
                            ShowError('El número de etiquetas no debe exceder el número de unidades')

                        }
                    }
                    else {
                        ShowError('El número de etiquetas no debe exceder el número de unidades')

                    }
                }



                $scope.EtiquetaPreimpresa = "";
            }
        };
        $scope.EliminarEtiqueta = function (item) {
            var index = 0;
            for (var i = 0; i < $scope.DetalleEtiquetasPreimpresas.length; i++) {
                if (item == $scope.DetalleEtiquetasPreimpresas[i]) {
                    index = i;
                    break;
                }
            }
            $scope.DetalleEtiquetasPreimpresas.splice(index, 1);
        };

        $scope.EtiquetaVsCantidad = function (Etiqueta) {
            if ($scope.Modelo.Aforador != undefined && $scope.Modelo.Aforador != null && $scope.Modelo.Aforador != '') {
                var ofi = ''
                if ($scope.Modelo.Remesa.Numeracion != undefined && $scope.Modelo.Remesa.Numeracion != null && $scope.Modelo.Remesa.Numeracion != "") {
                    ofi = $scope.Modelo.OficinaRegistroManual
                } else if ($scope.UsuarioAforador) {
                    ofi = $scope.Modelo.Oficina
                } else {
                    ShowError('Debe ingresar una guia preimpresa válida')
                }

                if (ofi !== '') {
                    var RListaEtiquetas = EtiquetasPreimpresasFactory.ConsultarPrecinto({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Responsable: { Codigo: $scope.Modelo.Aforador.Codigo }, Oficina: { Codigo: ofi.Codigo }, Estado: 0, Pagina: 1, RegistrosPagina: 1000, Sync: true }).Datos
                    if (RListaEtiquetas != undefined && RListaEtiquetas != null && RListaEtiquetas != "") {
                        if (RListaEtiquetas.length > 0) {
                            var coin = false
                            RListaEtiquetas.forEach(item => {
                                if (item.Numero_Precinto == Etiqueta) {
                                    $scope.DetalleEtiquetasPreimpresas.push(Etiqueta);
                                    coin = true
                                }
                            })
                            if (!coin) {
                                ShowError('El No. de Etiqueta no existe para el aforador y la oficina especificada')
                            }
                        } else {
                            ShowError('El No. de Etiqueta no existe para el aforador y la oficina especificada')
                        }
                    } else {
                        ShowError('El No. de Etiqueta no existe para el aforador y la oficina especificada')
                    }
                }
            } else {
                ShowError('No se encuentra un aforador válido para esta guía')
            }
        }
        //-----------------------Etiquetas Preimpresas----------------------//
        //-----------------------Mascaras - Volver------------------------------//
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Remesa.NumeroDocumento !== undefined)
                document.location.href = $scope.Master + "/" + $scope.Modelo.Remesa.NumeroDocumento + '/' + OPCION_MENU;
            else
                document.location.href = $scope.Master + "/0/" + OPCION_MENU;
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item);
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskNumeroGrid = function (item) {
            try {
                return parseInt(MascaraNumero(item));

            } catch (e) {
                return 0;
            }
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope);
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item);
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item);
        };

        CrearTerceroSiesa = function () {
            try {
                if ($scope.RemitenteNoRegistrado && !($scope.RemitenteGenerico)) {
                    remitente = {
                        Nit: $scope.Modelo.Remesa.Remitente.NumeroIdentificacion,
                        TipoCliente: $scope.Modelo.Remesa.Remitente.TipoIdentificacion.Nombre,
                        Nombres: $scope.NombreRemitente,
                        RazonSocial: $scope.NombreRemitente,
                        Apellido1: $scope.Modelo.Remesa.Remitente.PrimeroApellido,
                        CodigoCiudad: $scope.Modelo.Remesa.Remitente.Ciudad.CodigoAlterno,
                        CodigoPostal: $scope.Modelo.Remesa.CodigoPostalRemitente,
                        Direccion: $scope.Modelo.Remesa.Remitente.Direccion,
                        Telefono: $scope.Modelo.Remesa.Remitente.Telefonos,
                        Email: $scope.Modelo.Remesa.Remitente.CorreoFacturacion,
                        IdCia: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cupo: $scope.Modelo.Remesa.FormaPago.Nombre,
                        Sync: false
                    };

                    ServicioSIESAFactory.ImportarTerceroSIESA(remitente).then(function (response) {
                        console.log(response);

                    }, function (response) {
                        ShowError(response.statusText);

                    });

                }

                if ($scope.DestinatarioNoRegistrado && !($scope.DestinatarioGenerico)) {
                    destinatario = {
                        Nit: $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion,
                        TipoCliente: $scope.Modelo.Remesa.Destinatario.TipoIdentificacion.Nombre,
                        Nombres: $scope.NombreDestinatario,
                        RazonSocial: $scope.NombreDestinatario,
                        Apellido1: $scope.Modelo.Remesa.Destinatario.PrimeroApellido,
                        CodigoCiudad: $scope.Modelo.Remesa.Destinatario.Ciudad.CodigoAlterno,
                        CodigoPostal: $scope.Modelo.Remesa.CodigoPostalDestinatario,
                        Direccion: $scope.Modelo.Remesa.Destinatario.Direccion,
                        Telefono: $scope.Modelo.Remesa.Destinatario.Telefonos,
                        Email: $scope.Modelo.Remesa.Destinatario.CorreoFacturacion,
                        IdCia: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cupo: $scope.Modelo.Remesa.FormaPago.Nombre,
                        Sync: false
                    };

                    ServicioSIESAFactory.ImportarTerceroSIESA(destinatario).then(function (response) {
                        console.log(response);

                    }, function (response) {
                        ShowError(response.statusText);

                    });

                }
            } catch (e) { }

        };

        $scope.ConfirmacionEncabezadoContinuar = function () {
            if ($scope.PermisoGuardar) {
                showModal('modalEncabezadoContinuar');
            } else {
                ShowError('No tiene permisos para realizar esta acción');
            }
        };

        $scope.ConfirmacionEncabezadoVolver = function () {
            if ($scope.PermisoGuardar) {
                showModal('modalEncabezadoVolver');
            } else {
                ShowError('No tiene permisos para realizar esta acción');
            }
        };

        $scope.EncabezadoContinuar = function () {
            closeModal('modalEncabezadoContinuar');
            if (DatosRequeridosEncabezado()) {
                $scope.MensajesErrorEncabezado = [];
                $scope.MensajesErrorLiquidacion = [];
                $scope.MensajesErrorDetalleUnidades = [];
                $scope.MensajesErrorDocu = [];
                $scope.MensajesErrorPreimpreso = [];
                $scope.MensajesErrorTarifas = [];
                $scope.EditarEncabezado = false;
                if (!$scope.Obtenida) {
                    $scope.ValidarTarifarioCliente();
                }
            }
        };

        $scope.EncabezadoVolver = function () {
            closeModal('modalEncabezadoVolver');
            $scope.MensajesErrorEncabezado = [];
            $scope.MensajesErrorLiquidacion = [];
            $scope.MensajesErrorDetalleUnidades = [];
            $scope.MensajesErrorDocu = [];
            $scope.MensajesErrorPreimpreso = [];
            $scope.MensajesErrorTarifas = [];
            if (!$scope.Obtenida) {
                $scope.limpiarControl('Liquidacion');
            }
            $scope.EditarEncabezado = true;
        };

        //----------------------------Funciones Generales---------------------------------//
    }]);
