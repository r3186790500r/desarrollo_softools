﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Negocio.Paqueteria

''' <summary>
''' Controlador <see cref="RemesaPaqueteriaController"/>
''' </summary>
<Authorize>
Public Class CargueMasivoGuiasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarGuiasEtiquetasPreimpresas")>
    Public Function ConsultarGuiasEtiquetasPreimpresas(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarGuiasEtiquetasPreimpresas(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarGuiasPlanillaEntrega")>
    Public Function ConsultarGuiasPlanillaEntrega(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarGuiasPlanillaEntrega(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("CumplirGuias")>
    Public Function CumplirGuias(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).CumplirGuias(entidad)
    End Function
    <ActionName("GenerarPlantilla")>
    Public Function GenerarPlantilla(ByVal Filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaCargueMasivoGuias(CapaPersistenciaCargueMasivoGuias).GenerarPlantilla(Filtro)
    End Function
    <ActionName("GuardarEntrega")>
    Public Function GuardarEntrega(ByVal Entidad As Remesas) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).GuardarEntrega(Entidad)
    End Function
    <ActionName("ConsultarIndicadores")>
    Public Function ConsultarIndicadores(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarIndicadores(Entidad)
    End Function
    <HttpPost>
    <ActionName("AsociarGuiaPlanilla")>
    Public Function AsociarGuiaPlanilla(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).AsociarGuiaPlanilla(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarEstadosRemesa")>
    Public Function ConsultarEstadosRemesa(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarEstadosRemesa(Entidad)
    End Function
    <ActionName("ObtenerControlEntregas")>
    Public Function ObtenerControlEntregas(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ObtenerControlEntregas(Entidad)
    End Function
    <ActionName("ConsultarRemesasPendientesControlEntregas")>
    Public Function ConsultarRemesasPendientesControlEntregas(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasPendientesControlEntregas(Entidad)
    End Function
    <ActionName("ConsultarRemesasRecogerOficina")>
    Public Function ConsultarRemesasRecogerOficina(ByVal Entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasRecogerOficina(Entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasPorPlanillar")>
    Public Function ConsultarRemesasPorPlanillar(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasPorPlanillar(filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasRetornoContado")>
    Public Function ConsultarRemesasRetornoContado(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasRetornoContado(filtro)
    End Function
    <HttpPost>
    <ActionName("CambiarEstadoRemesasPaqueteria")>
    Public Function CambiarEstadoRemesasPaqueteria(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).CambiarEstadoRemesasPaqueteria(entidad)
    End Function
    <HttpPost>
    <ActionName("ValidarPreimpreso")>
    Public Function ValidarPreimpreso(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ValidarPreimpreso(entidad)
    End Function
    <HttpPost>
    <ActionName("RecibirRemesaPaqueteriaOficinaDestino")>
    Public Function RecibirRemesaPaqueteriaOficinaDestino(ByVal entidad As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).RecibirRemesaPaqueteriaOficinaDestino(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasPendientesRecibirOficina")>
    Public Function ConsultarRemesasPendientesRecibirOficina(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasPendientesRecibirOficina(filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasPorLegalizar")>
    Public Function ConsultarRemesasPorLegalizar(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasPorLegalizar(filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasPorLegalizarRecaudo")>
    Public Function ConsultarRemesasPorLegalizarRecaudo(ByVal filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaRemesaPaqueteria(CapaPersistenciaRemesaPaqueteria).ConsultarRemesasPorLegalizarRecaudo(filtro)
    End Function

    <HttpPost>
    <ActionName("InsertarMasivo")>
    Public Function InsertarMasivo(ByVal filtro As RemesaPaqueteria) As Respuesta(Of Long)
        Return New LogicaCargueMasivoGuias(CapaPersistenciaCargueMasivoGuias).InsertarMasivo(filtro)
    End Function


    <HttpPost>
    <ActionName("ConsultarTercerosIdentificacion")>
    Public Function ConsultarTercerosIdentificacion(ByVal filtro As Terceros) As Respuesta(Of IEnumerable(Of Terceros))
        Return New LogicaCargueMasivoGuias(CapaPersistenciaCargueMasivoGuias).ConsultarTercerosIdentificacion(filtro)
    End Function


    <HttpPost>
    <ActionName("ObtenerUltimoConsecutivoTercero")>
    Public Function ObtenerUltimoConsecutivoTercero(ByVal filtro As Terceros) As Respuesta(Of Terceros)
        Return New LogicaCargueMasivoGuias(CapaPersistenciaCargueMasivoGuias).ObtenerUltimoConsecutivoTercero(filtro)
    End Function


    <HttpPost>
    <ActionName("InsertarNuevosTerceros")>
    Public Function InsertarNuevosTerceros(ByVal filtro As Terceros) As Respuesta(Of Long)
        Return New LogicaCargueMasivoGuias(CapaPersistenciaCargueMasivoGuias).InsertarNuevosTerceros(filtro)
    End Function

    <HttpPost>
    <ActionName("ObtenerUltimaGuia")>
    Public Function ObtenerUltimaGuia(ByVal filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
        Return New LogicaCargueMasivoGuias(CapaPersistenciaCargueMasivoGuias).ObtenerUltimaGuia(filtro)
    End Function
End Class
