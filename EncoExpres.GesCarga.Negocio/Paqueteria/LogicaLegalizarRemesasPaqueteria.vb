﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Fachada.Paqueteria
Imports EncoExpres.GesCarga.Entidades

Namespace Paqueteria
    Public NotInheritable Class LogicaLegalizarRemesasPaqueteria
        Inherits LogicaBase(Of LegalizarRemesasPaqueteria)

        ReadOnly _persistencia As IPersistenciaBase(Of LegalizarRemesasPaqueteria)

        Sub New(persistencia As IPersistenciaBase(Of LegalizarRemesasPaqueteria))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As LegalizarRemesasPaqueteria) As Respuesta(Of IEnumerable(Of LegalizarRemesasPaqueteria))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of LegalizarRemesasPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of LegalizarRemesasPaqueteria))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As LegalizarRemesasPaqueteria) As Respuesta(Of LegalizarRemesasPaqueteria)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of LegalizarRemesasPaqueteria)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of LegalizarRemesasPaqueteria)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As LegalizarRemesasPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Private Function DatosRequeridos(entidad As LegalizarRemesasPaqueteria) As Respuesta(Of LegalizarRemesasPaqueteria)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of LegalizarRemesasPaqueteria) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of LegalizarRemesasPaqueteria) With {.ProcesoExitoso = True}

        End Function

        Public Function EliminarRemesa(entidad As LegalizarRemesasPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim BorrarRemesa As Boolean = False

            BorrarRemesa = CType(_persistencia, PersistenciaLegalizarRemesasPaqueteria).EliminarRemesa(entidad)

            If Not BorrarRemesa Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function ConsultarOtrasLegalizaciones(filtro As LegalizarRemesasPaqueteria) As Respuesta(Of IEnumerable(Of LegalizarRemesasPaqueteria))
            Dim consulta = CType(_persistencia, PersistenciaLegalizarRemesasPaqueteria).ConsultarOtrasLegalizaciones(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of LegalizarRemesasPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of LegalizarRemesasPaqueteria))(consulta)
        End Function

    End Class
End Namespace
