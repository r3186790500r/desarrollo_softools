﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="ValorCatalogosController"/>
''' </summary>
<Authorize>
Public Class ValorCatalogosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ValorCatalogos) As Respuesta(Of IEnumerable(Of ValorCatalogos))
        Return New LogicaValorCatalogos(CapaPersistenciaValorCatalogos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ValorCatalogos) As Respuesta(Of ValorCatalogos)
        Return New LogicaValorCatalogos(CapaPersistenciaValorCatalogos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ValorCatalogos) As Respuesta(Of Long)
        Return New LogicaValorCatalogos(CapaPersistenciaValorCatalogos).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ValorCatalogos) As Respuesta(Of Boolean)
        Return New LogicaValorCatalogos(CapaPersistenciaValorCatalogos).Anular(entidad)
    End Function
End Class
