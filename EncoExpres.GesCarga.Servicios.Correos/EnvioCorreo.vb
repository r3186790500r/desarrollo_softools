﻿Imports System.Threading
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Configuration.ConfigurationManager
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Net
Imports System.IO
Imports System.Text.RegularExpressions
Imports EncoExpres.GesCarga.Servicios
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Negocio.Aplicativo

Public Class EnvioCorreo

#Region "Variables"

    'Variables Bandeja Salida Correos
    Private intidCorre As Integer
    Private lonNumeDocu As Long
    Private strDest As String
    Private strDestCopi As String
    Private strCuerCorre As String
    Private intEstaCorr As Integer
    Private strAsunCorr As String
    Private intNumeInteEnvi As Integer

    'Variables Documento Envio Correos
    Private strDocumento As Byte()
    Private strExtensionDocumento As String

    'Variables Configuración Servidor Correo
    Private strRemi As String
    Private strClieSmtp As String
    Private intPuerSmtp As Integer
    Private bytRequAuteSmtp As Byte
    Private strUsuaCorre As String
    Private strClavCorreo As String
    Private bytAuteSSL As Byte
    Private strFirma As String

    'Variables Generales del Servicio
    Private intCodiEmpr As Integer
    Private intTiemSuspServ As Integer
    Private thrProceso As Thread
    Private strRutaLog As String
    Private strNombLog As String
    Private strArchLog As StreamWriter
    Private strMensajeError As String

    'Variables SQL
    Private dsDataSet As DataSet
    Private sqlComando As SqlCommand
    Private strCadenaDeConexionSQL As String
    Private strCadenaDeConexionSQLDocumental As String
    Private sqlConexion As SqlConnection
    Private sqlConexionDocumental As SqlConnection
    Private services As DetalleSeguimientoVehiculosController

#End Region

#Region "Constantes"

    Public Const ESTADO_ENVIADO As Integer = 1
    Public Const ESTADO_NO_ENVIADO As Integer = 0

#End Region

#Region "Propiedades"

    Public Property IdCorreo() As Integer
        Get
            Return intidCorre
        End Get
        Set(ByVal value As Integer)
            intidCorre = value
        End Set
    End Property

    Public Property NumeroDocumento() As Long
        Get
            Return lonNumeDocu
        End Get
        Set(ByVal value As Long)
            lonNumeDocu = value
        End Set
    End Property

    Public Property Destinatarios() As String
        Get
            Return strDest
        End Get
        Set(ByVal value As String)
            strDest = value
        End Set
    End Property

    Public Property DestinatariosCopia() As String
        Get
            Return strDestCopi
        End Get
        Set(ByVal value As String)
            strDestCopi = value
        End Set
    End Property
    Public Property Documento() As Byte()
        Get
            Return strDocumento
        End Get
        Set(ByVal value As Byte())
            strDocumento = value
        End Set
    End Property

    Public Property ExtensionDocumento() As String
        Get
            Return strExtensionDocumento
        End Get
        Set(ByVal value As String)
            strExtensionDocumento = value
        End Set
    End Property

    Public Property CuerpoCorreo() As String
        Get
            Return strCuerCorre
        End Get
        Set(ByVal value As String)
            strCuerCorre = value
        End Set
    End Property

    Public Property Estado() As Integer
        Get
            Return intEstaCorr
        End Get
        Set(ByVal value As Integer)
            intEstaCorr = value
        End Set
    End Property

    Public Property Asunto() As String
        Get
            Return strAsunCorr
        End Get
        Set(ByVal value As String)
            strAsunCorr = value
        End Set
    End Property

    Public Property NumeroIntentosEnvio() As Integer
        Get
            Return intNumeInteEnvi
        End Get
        Set(ByVal value As Integer)
            intNumeInteEnvi = value
        End Set
    End Property

    Public Property Remitente() As String
        Get
            Return strRemi
        End Get
        Set(ByVal value As String)
            strRemi = value
        End Set
    End Property

    Public Property ServidorSMTP() As String
        Get
            Return strClieSmtp
        End Get
        Set(ByVal value As String)
            strClieSmtp = value
        End Set
    End Property

    Public Property PuertoSMTP() As Integer
        Get
            Return intPuerSmtp
        End Get
        Set(ByVal value As Integer)
            intPuerSmtp = value
        End Set
    End Property

    Public Property RequiereAutenticacionSMTP() As Byte
        Get
            Return bytRequAuteSmtp
        End Get
        Set(ByVal value As Byte)
            bytRequAuteSmtp = value
        End Set
    End Property

    Public Property UsuarioCorreo() As String
        Get
            Return strUsuaCorre
        End Get
        Set(ByVal value As String)
            strUsuaCorre = value
        End Set
    End Property

    Public Property ClaveCorreo() As String
        Get
            Return strClavCorreo
        End Get
        Set(ByVal value As String)
            strClavCorreo = value
        End Set
    End Property

    Public Property AutenticacionSSL() As Byte
        Get
            Return bytAuteSSL
        End Get
        Set(ByVal value As Byte)
            bytAuteSSL = value
        End Set
    End Property

    Public Property Firma() As String
        Get
            Return strFirma
        End Get
        Set(ByVal value As String)
            strFirma = value
        End Set
    End Property

    Public Property CodigoEmpresa() As Integer
        Get
            Return intCodiEmpr
        End Get
        Set(ByVal value As Integer)
            intCodiEmpr = value
        End Set
    End Property

    Public Property TiempoSuspencionServicio() As Integer
        Get
            Return intTiemSuspServ
        End Get
        Set(ByVal value As Integer)
            intTiemSuspServ = value
        End Set
    End Property

    Public Property Proceso() As Thread
        Get
            Return thrProceso
        End Get
        Set(ByVal value As Thread)
            thrProceso = value
        End Set
    End Property

    Public Property RutaLog() As String
        Get
            Return strRutaLog
        End Get
        Set(ByVal value As String)
            strRutaLog = value
        End Set
    End Property

    Public Property NombreLog() As String
        Get
            Return strNombLog
        End Get
        Set(ByVal value As String)
            strNombLog = value
        End Set
    End Property

    Public Property ArchivoLog() As StreamWriter
        Get
            Return strArchLog
        End Get
        Set(ByVal value As StreamWriter)
            strArchLog = value
        End Set
    End Property

    Public Property MensajeError() As String
        Get
            Return strMensajeError
        End Get
        Set(ByVal value As String)
            strMensajeError = value
        End Set
    End Property

    Public Property ComandoSQL() As SqlCommand
        Get
            Return sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            sqlComando = value
        End Set
    End Property

    Public Property ConexionSQL() As SqlConnection
        Get
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ConexionSQLDocumental() As SqlConnection
        Get
            If IsNothing(Me.sqlConexionDocumental) Then
                Me.sqlConexionDocumental = New SqlConnection(Me.strCadenaDeConexionSQLDocumental)
            End If
            Return Me.sqlConexionDocumental
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexionDocumental) Then
                Me.sqlConexionDocumental = New SqlConnection(Me.strCadenaDeConexionSQLDocumental)
            End If
            Me.sqlConexionDocumental = value
        End Set
    End Property


#End Region

#Region "Constructor"

    Sub New()
        InitializeComponent()
        Me.thrProceso = New Thread(AddressOf Iniciar_Proceso)
        Me.thrProceso.Start()
    End Sub

#End Region

#Region "Funciones Privadas"
    Protected Overrides Sub OnStart(ByVal args() As String)
        Me.thrProceso = New Thread(AddressOf Iniciar_Proceso)
        thrProceso.Start()
    End Sub

    Protected Overrides Sub OnStop()
        Me.thrProceso.Abort()
    End Sub

#End Region

#Region "Funcionamiento del Servicio"

    Private Sub Iniciar_Proceso()
        Try

            While (True)

                Dim ArchivoLog As FileStream
                If Cargar_Valores_AppConfig() Then
                    If Not My.Computer.FileSystem.DirectoryExists(Me.RutaLog) Then
                        My.Computer.FileSystem.CreateDirectory(Me.RutaLog)
                        ArchivoLog = File.Create(Me.RutaLog & Me.NombreLog)
                        ArchivoLog.Close()
                    Else
                        If Not My.Computer.FileSystem.FileExists(Me.RutaLog & Me.NombreLog) Then
                            ArchivoLog = File.Create(Me.RutaLog & Me.NombreLog)
                            ArchivoLog.Close()
                        End If
                    End If
                    Try
                        Dim fecha1 As Date = DateTime.Now
                        If (fecha1.Hour = 6 Or fecha1.Hour = 14 Or fecha1.Hour = 18) And fecha1.Minute = 0 And Me.CodigoEmpresa = 4 Then 'Reporte Clientes IMPOCOMA
                            Dim strSQLclientes
                            Dim dsDataSetClientes As New DataSet
                            strSQLclientes = "SELECT distinct DESV.TERC_Cliente FROM Detalle_Seguimiento_Vehiculos DESV INNER JOIN Encabezado_Planilla_Despachos AS ENPD ON DESV.EMPR_Codigo = ENPD.EMPR_Codigo AND DESV.ENPD_Numero = ENPD.Numero WHERE DESV.EMPR_Codigo = " & Me.CodigoEmpresa
                            strSQLclientes += " AND ENPD.Anulado = 0 AND (ENPD.ECPD_Numero IS NULL OR ENPD.ECPD_Numero = 0) AND ISNULL(DESV.Fin_Viaje, 0) = 0 AND DESV.TERC_Cliente IS NOT NULL "
                            Me.ComandoSQL = New SqlCommand(strSQLclientes, Me.ConexionSQL)
                            If Me.ConexionSQL.State = ConnectionState.Open Then
                                Me.ConexionSQL.Close()
                            Else
                                Me.ConexionSQL.Open()
                            End If
                            dsDataSetClientes = Me.Retorna_Dataset(strSQLclientes)
                            Dim services2 = New DetalleSeguimientoVehiculosController
                            Dim Entidad As New DetalleSeguimientoVehiculos
                            If dsDataSetClientes.Tables(0).Rows.Count > 0 Then
                                For Each Registro As DataRow In dsDataSetClientes.Tables(0).Rows
                                    Entidad.Codigo = Val(Registro.Item("TERC_Cliente").ToString() + fecha1.Year.ToString() + fecha1.DayOfYear.ToString() + fecha1.Hour.ToString())
                                    Entidad.CodigoCliente = Val(Registro.Item("TERC_Cliente").ToString())
                                    Entidad.CodigoEmpresa = Me.CodigoEmpresa
                                    Entidad.UsuarioCrea = New SeguridadUsuarios.Usuarios With {.Codigo = 1}
                                    services2.EnviarCorreoAvanceVehiculosCliente(Entidad)
                                Next
                            End If
                        End If
                    Catch ex As Exception
                        Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                        Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: " & ex.Message)
                        Me.ArchivoLog.Close()
                    End Try
                    Call Consultar_Bandeja_Salida_Correos()


                End If
                Thread.Sleep(Val(Me.TiempoSuspencionServicio))
            End While
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: " & ex.Message)
            Me.ArchivoLog.Close()
        End Try
    End Sub

    Public Function Email_Valido(ByVal Email As String) As Boolean
        Try
            Dim rgxPatronCorreo As Regex = New Regex("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}$")
            Dim mthEvaluaCorreo As Match = rgxPatronCorreo.Match(Email)

            If mthEvaluaCorreo.Success Then
                Email_Valido = True
            Else
                Email_Valido = False
            End If
        Catch ex As Exception
            Email_Valido = False
        End Try
    End Function

    Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try
    End Function

    Function Retorna_Dataset_Documental(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset_Documental = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try
    End Function


    Function Cargar_Valores_AppConfig() As Boolean
        Try
            Me.CodigoEmpresa = AppSettings.Get("CodigoEmpresa")
            Me.strCadenaDeConexionSQL = AppSettings.Get("ConexionSQL")
            Me.TiempoSuspencionServicio = AppSettings.Get("TiempoSuspencionProceso")
            Me.NombreLog = AppSettings.Get("NombreLog")
            Me.RutaLog = AppSettings.Get("RutaArchivoLog")
            Cargar_Valores_AppConfig = True
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: " & ex.Message)
            Me.ArchivoLog.Close()
            Cargar_Valores_AppConfig = False
        End Try
    End Function

    Function Cargar_Datos_Servidor_Correo(CodigoEmpresa As Integer) As Boolean
        Try
            Dim strSQL
            Dim dsDataSet As New DataSet
            Dim drReader As SqlDataReader

            Cargar_Datos_Servidor_Correo = False

            strSQL = "SELECT *"
            strSQL += " FROM V_Configuracion_Servidor_Correos WHERE EMPR_Codigo = " & CodigoEmpresa

            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
            End If

            drReader = Me.ComandoSQL.ExecuteReader

            If drReader.Read Then
                Me.ServidorSMTP = drReader.Item("Smtp").ToString
                Me.RequiereAutenticacionSMTP = drReader.Item("RequiereAutenticacionSmtp").ToString
                Me.PuertoSMTP = drReader.Item("SmtpPuerto").ToString
                Me.UsuarioCorreo = drReader.Item("UsuarioCorreo").ToString
                Me.ClaveCorreo = drReader.Item("ContrasenaCorreo").ToString
                Me.AutenticacionSSL = drReader.Item("AutenticacionSSL").ToString
                Me.Remitente = drReader.Item("Remitente").ToString
                Cargar_Datos_Servidor_Correo = True
            End If
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se presentó el siguiente error en la función Cargar_Datos_Servidor_Correo: " & ex.Message)
            Me.ArchivoLog.Close()
            Cargar_Datos_Servidor_Correo = False
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Function

    Private Function Consultar_Bandeja_Salida_Correos() As Boolean
        Try
            Dim strSQL
            Dim dsDataSet As New DataSet

            Consultar_Bandeja_Salida_Correos = False

            strSQL = "SELECT EMPR_Codigo, ID, Numero_Documento, Cuenta_Correo_Para, Cuenta_Correo_Copia, Texto_Correo,Mensaje_Error_SMTP, Numero_Intentos_Envio, Estado, Asunto_Correo"
            strSQL += " FROM V_Bandeja_Salida_Correos"

            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
            End If


            dsDataSet = Me.Retorna_Dataset(strSQL)



            If dsDataSet.Tables(0).Rows.Count > 0 Then
                For Each Registro As DataRow In dsDataSet.Tables(0).Rows
                    Me.CodigoEmpresa = Val(Registro.Item("EMPR_Codigo").ToString())
                    Me.IdCorreo = Val(Registro.Item("ID").ToString())
                    Me.NumeroDocumento = Val(Registro.Item("Numero_Documento").ToString)
                    Me.Destinatarios = Registro.Item("Cuenta_Correo_Para").ToString()
                    Me.DestinatariosCopia = Registro.Item("Cuenta_Correo_Copia").ToString()
                    Me.CuerpoCorreo = Registro.Item("Texto_Correo").ToString()
                    Me.MensajeError = String.Empty
                    Me.NumeroIntentosEnvio = Val(Registro.Item("Numero_Intentos_Envio").ToString)
                    Me.Estado = Val(Registro.Item("Estado"))
                    Me.Asunto = Registro.Item("Asunto_Correo").ToString
                    If Cargar_Datos_Servidor_Correo(Me.CodigoEmpresa) Then
                        If Val(Registro.Item("Estado")) = ESTADO_NO_ENVIADO Then
                            Consultar_Documentos_Envio_Correos(Me.NumeroDocumento, Me.CodigoEmpresa)
                            Try
                                Enviar_Email()
                                Consultar_Bandeja_Salida_Correos = Insertar_Historico_SQL()

                            Catch ex As Exception
                                Consultar_Bandeja_Salida_Correos = Insertar_Historico_SQL()

                            End Try

                        End If
                    End If
                Next
            Else
                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "No hay correos pendientes por enviar")
                Me.ArchivoLog.Close()
            End If

        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se presentó el siguiente error en la función Consultar_Bandeja_Salida_Correos: " & ex.Message)
            Me.ArchivoLog.Close()
            Consultar_Bandeja_Salida_Correos = False
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Function
    Private Function Consultar_Documentos_Envio_Correos(ByVal Codigo As Long, CodigoEmpresa As Integer) As Boolean
        Try
            Dim strSQL
            Dim dsDataSet As New DataSet
            Dim RederDocumento As SqlDataReader

            Consultar_Documentos_Envio_Correos = False

            strSQL = "SELECT top(1) Documento, Extension_Documento"
            strSQL += " FROM V_Evento_Correo_Documentos WHERE Numero_Documento = " & Codigo & " AND  EMPR_Codigo = " & CodigoEmpresa & " ORDER BY Numero_Documento DESC"

            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
            End If

            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            dsDataSet = Me.Retorna_Dataset(strSQL)

            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
            End If

            RederDocumento = Me.ComandoSQL.ExecuteReader

            If RederDocumento.Read Then
                Me.ExtensionDocumento = RederDocumento.Item("Extension_Documento".ToString())
                Me.Documento = RederDocumento.Item("Documento")

            End If

        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se presentó el siguiente error en la función Consultar_Documentos_Envio_Correos: " & ex.Message)
            Me.ArchivoLog.Close()
            Consultar_Documentos_Envio_Correos = False
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Function
    Public Function Enviar_Email() As Boolean
        Try
            Dim Correo As MailMessage
            Dim Smtp As New SmtpClient

            If Len(Trim(Me.Destinatarios)).ToString = 0 Then
                Enviar_Email = False
            Else
                Enviar_Email = True
            End If

            If Enviar_Email Then
                Correo = New MailMessage
                Correo.From = New MailAddress(Me.UsuarioCorreo, Me.Remitente)

                Me.Destinatarios = Me.Destinatarios.Replace(" ", "")
                Me.DestinatariosCopia = Me.DestinatariosCopia.Replace(" ", "")

                Dim arrayTo = Split(Me.Destinatarios, ",")
                For Each destinatario In arrayTo
                    If Me.Email_Valido(destinatario) Then
                        Correo.To.Add(destinatario)
                    Else
                        Me.MensajeError += "El correo para " & destinatario & " no es válido"
                    End If
                Next

                If Me.DestinatariosCopia <> "" Then
                    Dim arrayBcc = Split(Me.DestinatariosCopia, ",")
                    For Each conCopia In arrayBcc
                        If Me.Email_Valido(conCopia) Then
                            Correo.Bcc.Add(conCopia)
                        Else
                            Me.MensajeError += "El correo con copia a " & conCopia & " no es válido"
                        End If
                    Next
                End If

                Dim strSaludo As String
                Dim strCuerpoCorreo As String

                If Date.Now.Hour >= 0 And Date.Now.Hour < 12 Then
                    strSaludo = "Buenos Días, "
                ElseIf Date.Now.Hour >= 12 And Date.Now.Hour < 19 Then
                    strSaludo = "Buenas Tardes, "
                Else
                    strSaludo = "Buenas Noches, "
                End If

                strCuerpoCorreo = strSaludo & Chr(13) & Chr(13)
                strCuerpoCorreo += Me.CuerpoCorreo

                Correo.Body = strCuerpoCorreo
                Correo.Priority = MailPriority.Normal
                Smtp.Credentials = New System.Net.NetworkCredential(Me.UsuarioCorreo, Me.ClaveCorreo)
                Smtp.Host = Me.ServidorSMTP
                Smtp.Port = Me.PuertoSMTP
                Smtp.EnableSsl = True

                Correo.SubjectEncoding = System.Text.Encoding.UTF8
                Correo.Subject = Me.Asunto
                Correo.BodyEncoding = System.Text.Encoding.UTF8
                Correo.IsBodyHtml = True


                If Not IsNothing(Me.Documento) Then
                    Correo.Attachments.Add(New Attachment(New MemoryStream(Me.Documento), "Documento" & Me.ExtensionDocumento))
                End If

                ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As X509Certificate, chain As X509Chain, sslPolicyErrors As SslPolicyErrors) True
                Smtp.Send(Correo)
                Smtp.Dispose()
                Enviar_Email = True
                Me.Estado = ESTADO_ENVIADO
                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Correo ID " & Me.IdCorreo & " enviado correctamente a " & Me.Destinatarios & " con copia a " & Me.DestinatariosCopia)
                Me.ArchivoLog.Close()
            Else
                Me.Estado = ESTADO_NO_ENVIADO
                Enviar_Email = False
                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "No hay destinatarios en el correo ID " & Me.IdCorreo)
                Me.ArchivoLog.Close()
            End If
        Catch ex_SmtpFailReciExcep As SmtpFailedRecipientsException
            If Me.MensajeError = String.Empty Then
                Me.MensajeError = ex_SmtpFailReciExcep.InnerException.Message
            Else
                Me.MensajeError += " - " & ex_SmtpFailReciExcep.InnerException.Message
            End If
            Enviar_Email = False
        Catch ex_ObjeDispExcep As ObjectDisposedException
            If Me.MensajeError = String.Empty Then
                Me.MensajeError = ex_ObjeDispExcep.InnerException.Message
            Else
                Me.MensajeError += " - " & ex_ObjeDispExcep.InnerException.Message
            End If
            Enviar_Email = False
        Catch ex_InvaOperExcep As InvalidOperationException
            If Me.MensajeError = String.Empty Then
                Me.MensajeError = ex_InvaOperExcep.InnerException.Message
            Else
                Me.MensajeError += " - " & ex_InvaOperExcep.InnerException.Message
            End If
            Enviar_Email = False
        Catch ex_ArguNullExcep As ArgumentNullException
            If Me.MensajeError = String.Empty Then
                Me.MensajeError = ex_ArguNullExcep.InnerException.Message
            Else
                Me.MensajeError += " - " & ex_ArguNullExcep.InnerException.Message
            End If
            Enviar_Email = False

        Catch ex_SmtpExcep As SmtpException
            If Me.MensajeError = String.Empty Then
                Me.MensajeError = ex_SmtpExcep.InnerException.Message
            Else
                Me.MensajeError += " - " & ex_SmtpExcep.InnerException.Message
            End If
            Enviar_Email = False
        Catch ex As Exception
            If Me.MensajeError = String.Empty Then
                Me.MensajeError = ex.InnerException.Message
            Else
                Me.MensajeError += " - " & ex.InnerException.Message
            End If
            Enviar_Email = False
        Finally
            If Me.MensajeError <> String.Empty Then
                Enviar_Email = False
                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se presentó el siguiente error en la función Enviar_Email: " & Me.MensajeError)
                Me.ArchivoLog.Close()
                Call Falla_Correo_Enviado()
            End If
        End Try
    End Function

    Private Function Insertar_Historico_SQL() As Boolean
        Try
            Dim lonNumeRegi As Long
            Insertar_Historico_SQL = False

            Me.ComandoSQL = New SqlCommand("gsp_modificar_bandeja_salida_correos", Me.ConexionSQL)
            ComandoSQL.CommandType = CommandType.StoredProcedure

            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.CodigoEmpresa
            ComandoSQL.Parameters.Add("@par_ID", SqlDbType.Int) : ComandoSQL.Parameters("@par_ID").Value = Me.IdCorreo
            ComandoSQL.Parameters.Add("@par_Numero_Intentos_Envio", SqlDbType.Int, 10) : ComandoSQL.Parameters("@par_Numero_Intentos_Envio").Value = Me.NumeroIntentosEnvio + 1
            ComandoSQL.Parameters.Add("@par_Mensaje_Error_SMTP", SqlDbType.VarChar, 500) : ComandoSQL.Parameters("@par_Mensaje_Error_SMTP").Value = Me.MensajeError
            ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Estado").Value = Me.Estado

            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
            End If

            lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

            If lonNumeRegi > 0 Then
                Insertar_Historico_SQL = True
                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se almacenó correctamente el correo ID " & Me.IdCorreo & " en el histórico")
                Me.ArchivoLog.Close()
            Else
                Insertar_Historico_SQL = False
                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se presentó un error al insertar el correo ID " & Me.IdCorreo & " en el histórico")
                Me.ArchivoLog.Close()
            End If
        Catch ex As Exception
            Insertar_Historico_SQL = False
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se presentó el siguiente error en la función Insertar_Historico_SQL: " & ex.Message)
            Me.ArchivoLog.Close()
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Function

    Private Sub Falla_Correo_Enviado()
        Try
            Dim lonNumeRegi As Long

            Me.ComandoSQL = New SqlCommand("gsp_modificar_numero_intento_envios_correo", Me.ConexionSQL)
            ComandoSQL.CommandType = CommandType.StoredProcedure

            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.CodigoEmpresa
            ComandoSQL.Parameters.Add("@par_ID", SqlDbType.Int) : ComandoSQL.Parameters("@par_ID").Value = Me.IdCorreo
            ComandoSQL.Parameters.Add("@par_Mensaje_Error_SMTP", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Mensaje_Error_SMTP").Value = Me.MensajeError
            ComandoSQL.Parameters.Add("@par_Numero_Intentos_Envio", SqlDbType.Int, 10) : ComandoSQL.Parameters("@par_Numero_Intentos_Envio").Value = Me.NumeroIntentosEnvio + 1

            If ConexionSQL.State = ConnectionState.Open Then
                ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
            End If

            lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

            If lonNumeRegi > 0 Then
                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "El correo ID " & Me.IdCorreo & " presenta el siguiente error: " & Me.MensajeError)
                Me.ArchivoLog.Close()
            End If
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se presentó el siguiente error en la función Falla_Correo_Enviado: " & ex.Message)
            Me.ArchivoLog.Close()
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Sub

#End Region

End Class

