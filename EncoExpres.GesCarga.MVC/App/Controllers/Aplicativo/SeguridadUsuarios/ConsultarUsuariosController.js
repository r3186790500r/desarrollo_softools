﻿EncoExpresApp.controller("ConsultarUsuariosCtrl", ['$scope', '$timeout', 'ValorCatalogosFactory', 'UsuariosFactory', 'GrupoUsuariosFactory', 'OficinasFactory','CajasFactory', '$linq', 'blockUI', '$routeParams',
    function ($scope, $timeout, ValorCatalogosFactory, UsuariosFactory, GrupoUsuariosFactory, OficinasFactory, CajasFactory, $linq, blockUI, $routeParams) {

        $scope.MapaSitio = [{ Nombre: 'Seguridad' }, { Nombre: 'Usuarios' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.Seleccionado = false;
        $scope.TipoAplicacion = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;

        $scope.cantidadRegistrosPorPaginasOfi = 10;
        $scope.paginaActualOfi = 1;

        $scope.CodigoUsuario = 0;
        IdentificadorModals = '';
        NombreModals = '';
        $scope.ListadoOficinas = [];
        $scope.ListadoCajas = [];
        $scope.ListadoGridOficinas = [];
        $scope.ListadoGridOficinasTemp = [];
        $scope.ListadoGridTaquillas = [];
        CodigoDia = 0;
        CodigoHoraInicial = 0;
        $scope.ListaHorarioUsuarios = [];
        $scope.CodigoUsuario = '';
        $scope.modalIdentificador = 0;
        $scope.modalNombre = '';
        $scope.CodigoUsuarioTemp = 0;
        $scope.MensajesErrorHorarios = []
        $scope.ListadoTipoAplicacion = []
        $scope.ModeloLoguueado = [];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_USUARIOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.ListadoEstadoLogueado = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'LOGUEADO', Codigo: CODIGO_UNO },
            { Nombre: 'DESLOGUEADO', Codigo: CERO }
        ]

        $scope.ModeloLoguueado = $linq.Enumerable().From($scope.ListadoEstadoLogueado).First('$.Codigo == -1 ');

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/


        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN E IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN E IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPaginaOfi = function () {
            $scope.paginaActualOfi = 1;
            $scope.PaginarOficinas()
        };

        $scope.SiguienteOfi = function () {
            if ($scope.paginaActualOfi < $scope.totalPaginasOfi) {
                $scope.paginaActualOfi = $scope.paginaActualOfi + 1;
                $scope.PaginarOficinas()
            }
        };

        $scope.AnteriorOfi = function () {
            if ($scope.paginaActualOfi > 1) {
                $scope.paginaActualOfi = $scope.paginaActualOfi - 1;
                $scope.PaginarOficinas()
            }
        };

        $scope.UltimaPaginaOfi = function () {
            $scope.paginaActualOfi = $scope.totalPaginasOfi;
            $scope.PaginarOficinas()
        };


        $scope.PaginarOficinas = function () {

            $scope.ListadoGridOficinasTemp = []

            $scope.totalRegistrosOfi = $scope.ListadoGridOficinas.length
            $scope.totalPaginasOfi = Math.ceil($scope.totalRegistrosOfi / $scope.cantidadRegistrosPorPaginasOfi);
            var total = $scope.paginaActualOfi * 10
            for (let i = total - 10; i < total; i++) {

                if ($scope.ListadoGridOficinas[i] != undefined) {
                    $scope.ListadoGridOficinasTemp.push($scope.ListadoGridOficinas[i])
                }                
            }

        }


        //----------------------------Cargar Combos----------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar el combo de Aplicacion*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_APLICACIONES_DE_GESTRANS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoAplicacion = []
                    $scope.ListadoTipoAplicacion = response.data.Datos
                    $scope.TipoAplicacion = response.data.Datos[0]
                }
                else {
                    $scope.ListadoTipoAplicacion = []
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        GrupoUsuariosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoGrupos = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoGrupos.push(item);
                    });
                    $scope.Grupos = response.data.Datos[0];
                }
            }, function (response) {
            });

        //    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 27 } }).
        //        then(function (response) {
        //            if (response.data.ProcesoExitoso === true) {
        //                $scope.ListadoHorarioInicial = [];
        //                $scope.ListadoHorarioFinal = [];
        //                response.data.Datos.forEach(function (item) {
        //                    $scope.ListadoHorarioInicial.push(item);
        //                    $scope.ListadoHorarioFinal.push(item);
        //                });
        //                $scope.HorarioInicialLunes = response.data.Datos[0];
        //                $scope.HorarioFinalLunes = response.data.Datos[0];

        //                $scope.HorarioInicialMartes = response.data.Datos[0];
        //                $scope.HorarioFinalMartes = response.data.Datos[0];

        //                $scope.HorarioInicialMiercoles = response.data.Datos[0];
        //                $scope.HorarioFinalMiercoles = response.data.Datos[0];

        //                $scope.HorarioInicialJueves = response.data.Datos[0];
        //                $scope.HorarioFinalJueves = response.data.Datos[0];

        //                $scope.HorarioInicialViernes = response.data.Datos[0];
        //                $scope.HorarioFinalViernes = response.data.Datos[0];

        //                $scope.HorarioInicialSabado = response.data.Datos[0];
        //                $scope.HorarioFinalSabado = response.data.Datos[0];

        //                $scope.HorarioInicialDomingo = response.data.Datos[0];
        //                $scope.HorarioFinalDomingo = response.data.Datos[0];
        //            }
        //        }, function (response) {
        //        });


 
      
       

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = [];
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo != 0) {
                            $scope.ListadoOficinas.push(item);
                        }
                    });
                    $scope.Oficinas = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            Find();
        };

        $scope.BuscarCajas = function () {

            console.log('llego oficina',$scope.Oficinas.Codigo)

            CajasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Oficina: { Codigo: $scope.Oficinas.Codigo }
            }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCajas = [];
                        response.data.Datos.forEach(function (item) {
                            if (item.Codigo != 0) {
                                $scope.ListadoCajas.push(item);
                            }
                        });
                        $scope.ListadoCajas.push({ Nombre: '(No Aplica)', Codigo: 0 });
                        $scope.Cajas = $scope.ListadoCajas[0];

                        console.log('listado actual Cajas', $scope.ListadoCajas)

                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarUsuarios';
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoUsuarios = [];

            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Codigo,
                        Nombre: $scope.Nombre,
                        CodigoUsuario: $scope.Identificador,
                        AplicacionUsuario: $scope.TipoAplicacion.Codigo,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        Login: $scope.ModeloLoguueado.Codigo
                    }
                    blockUI.delay = 1000;
                    UsuariosFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Codigo = undefined;
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoUsuarios = response.data.Datos
                                    $scope.ListadoUsuarios.forEach(function (item) {
                                        if (item.Login == CODIGO_UNO) {
                                            item.Login = true;
                                        } else {
                                            item.Login = false;
                                        }

                                    });
                                     

                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                    if ($scope.ListadoUsuarios.length == 0) {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }

        //--------------------------------------------------------------------------------------------------------
        //-------------------------------------------MODAL GRUPOS-------------------------------------------------
        //--------------------------------------------------------------------------------------------------------

        //#region CONSULTAR
        $scope.GrupoUsuarios = function (Codigo, Identificador, Nombre) {
            $scope.CodigoUsuarioTemp = Codigo;
            $scope.modalIdentificador = Identificador;
            $scope.modalNombre = Nombre;
            $scope.ListadoGridGrupos = [];
            $scope.Grupos = $scope.ListadoGrupos[0];

            console.log('llego', Identificador, Nombre)
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo,
            }
            blockUI.delay = 1000;
            UsuariosFactory.ConsultarUsuarioGrupoSeguridades(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                var registro = item;
                                $scope.ListadoGridGrupos.push(registro);
                            });
                        }
                    }
                });
            showModal('modalGrupoUsuarios');
        };
        //#endregion

        //#region ADICIONAR A LISTA
        $scope.AdicionarGrupo = function () {
            $scope.ListadoTempGrupos = [];
            $scope.GrupoAgregado = 0;
            $scope.ListadoTempGrupos.Codigo = $scope.Grupos.Codigo;
            $scope.ListadoTempGrupos.Nombre = $scope.Grupos.Nombre;
            $scope.ListadoGridGrupos.forEach(function (item) {
                if (item.GrupoUsuarios.Codigo == $scope.Grupos.Codigo) {
                    $scope.GrupoAgregado = 1;
                }
            });
            if ($scope.GrupoAgregado == 0) {
                $scope.ListadoGridGrupos.push({ GrupoUsuarios: { Codigo: $scope.ListadoTempGrupos.Codigo, Nombre: $scope.ListadoTempGrupos.Nombre } });
            }
            else {
                ShowError("El grupo ya se encuentra adicionado");
            }
        }
        //#endregion

        //#region GUARDAR
        $scope.AsignarGrupos = function () {
            $scope.listaGruposGuardar = [];
            $scope.ListadoGridGrupos.forEach(function (itmGrupo) {
                parametros =
                    {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: itmGrupo.GrupoUsuarios.Codigo,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        CodigoUsuario: $scope.CodigoUsuarioTemp,
                    };
                $scope.listaGruposGuardar.push(parametros);
            })
            if ($scope.listaGruposGuardar.length == 0) {
                $scope.listaGruposGuardar.push(
                    {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CodigoUsuario: $scope.CodigoUsuarioTemp
                    })
            }
            UsuariosFactory.GuardarUsuarioGrupoSeguridades($scope.listaGruposGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('Se asigno el grupo (s) correctamente');
                            closeModal('modalGrupoUsuarios');
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                });
        }
        //#endregion

        //#region ELIMINAR
        $scope.EliminarGrupo = function (indexGrupo) {
            $scope.ListadoGridGrupos.splice(indexGrupo, 1)
        }
        //#endregion

        //#region CERRAR MODAL
        $scope.CerrarModalGrupos = function () {
            closeModal('modalGrupoUsuarios');
        }
        //#endregion

        //--------------------------------------------------------------------------------------------------------
        //-------------------------------------------MODAL HORARIOS-----------------------------------------------
        //--------------------------------------------------------------------------------------------------------

        //#region CONSULTAR
        $scope.HorarioUsuarios = function (Codigo, Identificador, Nombre) {
            $scope.CodigoUsuarioTemp = Codigo;
            $scope.modalIdentificador = Identificador;
            $scope.modalNombre = Nombre;

            filtros = ({
                codigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                codigoUsuario: Codigo
            })
            blockUI.delay = 1000;
            UsuariosFactory.ConsultarUsuarioHorarios(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.DiaSemana.Id == 1) {
                                    $scope.HorarioInicialLunes = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarInicial.Codigo);
                                    $scope.HorarioFinalLunes = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarFinal.Codigo);
                                }
                                if (item.DiaSemana.Id == 2) {
                                    $scope.HorarioInicialMartes = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarInicial.Codigo);
                                    $scope.HorarioFinalMartes = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarFinal.Codigo);
                                }
                                if (item.DiaSemana.Id == 3) {
                                    $scope.HorarioInicialMiercoles = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarInicial.Codigo);
                                    $scope.HorarioFinalMiercoles = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarFinal.Codigo);
                                }
                                if (item.DiaSemana.Id == 4) {
                                    $scope.HorarioInicialJueves = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarInicial.Codigo);
                                    $scope.HorarioFinalJueves = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarFinal.Codigo);
                                }
                                if (item.DiaSemana.Id == 5) {
                                    $scope.HorarioInicialViernes = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarInicial.Codigo);
                                    $scope.HorarioFinalViernes = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarFinal.Codigo);
                                }
                                if (item.DiaSemana.Id == 6) {
                                    $scope.HorarioInicialSabado = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarInicial.Codigo);
                                    $scope.HorarioFinalSabado = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarFinal.Codigo);
                                }
                                if (item.DiaSemana.Id == 7) {
                                    $scope.HorarioInicialDomingo = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarInicial.Codigo);
                                    $scope.HorarioFinalDomingo = $linq.Enumerable().From($scope.ListadoHorarioInicial).First('$.Codigo == ' + item.HoraMilitarFinal.Codigo);
                                }
                            });
                        }
                    }
                });
            showModal('modalHorarioUsuarios');
        };
        //#endregion

        //#region CERRAR MODAL
        $scope.CerrarModalHorarioUsuarios = function () {
            closeModal('modalHorarioUsuarios');
        }
        //#endregion

        //#region GUARDAR
        $scope.GuardarHorarioUsuario = function () {
            $scope.MensajesErrorHorarios = []
            if (DatosRequeridosHorario()) {
                $scope.ListaHorarioUsuarios = [];
                $scope.ListaHorarioUsuarios.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoUsuario: $scope.CodigoUsuarioTemp,
                    DiaSemana: { Codigo: 2308 },
                    HoraMilitarInicial: { Codigo: $scope.HorarioInicialLunes.Codigo },
                    HoraMilitarFinal: { Codigo: $scope.HorarioFinalLunes.Codigo }
                })
                $scope.ListaHorarioUsuarios.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoUsuario: $scope.CodigoUsuarioTemp,
                    DiaSemana: { Codigo: 2309 },
                    HoraMilitarInicial: { Codigo: $scope.HorarioInicialMartes.Codigo },
                    HoraMilitarFinal: { Codigo: $scope.HorarioFinalMartes.Codigo }
                })
                $scope.ListaHorarioUsuarios.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoUsuario: $scope.CodigoUsuarioTemp,
                    DiaSemana: { Codigo: 2310 },
                    HoraMilitarInicial: { Codigo: $scope.HorarioInicialMiercoles.Codigo },
                    HoraMilitarFinal: { Codigo: $scope.HorarioFinalMiercoles.Codigo }
                })
                $scope.ListaHorarioUsuarios.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoUsuario: $scope.CodigoUsuarioTemp,
                    DiaSemana: { Codigo: 2311 },
                    HoraMilitarInicial: { Codigo: $scope.HorarioInicialJueves.Codigo },
                    HoraMilitarFinal: { Codigo: $scope.HorarioFinalJueves.Codigo }
                })
                $scope.ListaHorarioUsuarios.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoUsuario: $scope.CodigoUsuarioTemp,
                    DiaSemana: { Codigo: 2312 },
                    HoraMilitarInicial: { Codigo: $scope.HorarioInicialViernes.Codigo },
                    HoraMilitarFinal: { Codigo: $scope.HorarioFinalViernes.Codigo }
                })
                $scope.ListaHorarioUsuarios.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoUsuario: $scope.CodigoUsuarioTemp,
                    DiaSemana: { Codigo: 2313 },
                    HoraMilitarInicial: { Codigo: $scope.HorarioInicialSabado.Codigo },
                    HoraMilitarFinal: { Codigo: $scope.HorarioFinalSabado.Codigo }
                })
                $scope.ListaHorarioUsuarios.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoUsuario: $scope.CodigoUsuarioTemp,
                    DiaSemana: { Codigo: 2314 },
                    HoraMilitarInicial: { Codigo: $scope.HorarioInicialDomingo.Codigo },
                    HoraMilitarFinal: { Codigo: $scope.HorarioFinalDomingo.Codigo }
                })

                UsuariosFactory.GuardarUsuarioHorarios($scope.ListaHorarioUsuarios).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('Horario agregado correctamente');
                                closeModal('modalHorarioUsuarios')
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                    });
            }

        }

        function DatosRequeridosHorario() {
            guardar = true
            var fechainicio = new Date('1999', '01', '01', $scope.HorarioInicialLunes.Nombre[0] + $scope.HorarioInicialLunes.Nombre[1], $scope.HorarioInicialLunes.Nombre[3] + $scope.HorarioInicialLunes.Nombre[4])
            var fechafin = new Date('1999', '01', '01', $scope.HorarioFinalLunes.Nombre[0] + $scope.HorarioFinalLunes.Nombre[1], $scope.HorarioFinalLunes.Nombre[3] + $scope.HorarioFinalLunes.Nombre[4])
            if (fechainicio > fechafin) {
                $scope.MensajesErrorHorarios.push('La hora inicial no puede ser mayor a la hora final del día lunes')
                guardar = false
            }
            fechainicio = new Date('1999', '01', '01', $scope.HorarioInicialMartes.Nombre[0] + $scope.HorarioInicialMartes.Nombre[1], $scope.HorarioInicialMartes.Nombre[3] + $scope.HorarioInicialMartes.Nombre[4])
            fechafin = new Date('1999', '01', '01', $scope.HorarioFinalMartes.Nombre[0] + $scope.HorarioFinalMartes.Nombre[1], $scope.HorarioFinalMartes.Nombre[3] + $scope.HorarioFinalMartes.Nombre[4])
            if (fechainicio > fechafin) {

                $scope.MensajesErrorHorarios.push('La hora inicial no puede ser mayor a la hora final del día martes')
                guardar = false
            }
            fechainicio = new Date('1999', '01', '01', $scope.HorarioInicialMiercoles.Nombre[0] + $scope.HorarioInicialMiercoles.Nombre[1], $scope.HorarioInicialMiercoles.Nombre[3] + $scope.HorarioInicialMiercoles.Nombre[4])
            fechafin = new Date('1999', '01', '01', $scope.HorarioFinalMiercoles.Nombre[0] + $scope.HorarioFinalMiercoles.Nombre[1], $scope.HorarioFinalMiercoles.Nombre[3] + $scope.HorarioFinalMiercoles.Nombre[4])
            if (fechainicio > fechafin) {
                $scope.MensajesErrorHorarios.push('La hora inicial no puede ser mayor a la hora final del día miércoles')
                guardar = false
            }
            fechainicio = new Date('1999', '01', '01', $scope.HorarioInicialJueves.Nombre[0] + $scope.HorarioInicialJueves.Nombre[1], $scope.HorarioInicialJueves.Nombre[3] + $scope.HorarioInicialJueves.Nombre[4])
            fechafin = new Date('1999', '01', '01', $scope.HorarioFinalJueves.Nombre[0] + $scope.HorarioFinalJueves.Nombre[1], $scope.HorarioFinalJueves.Nombre[3] + $scope.HorarioFinalJueves.Nombre[4])
            if (fechainicio > fechafin) {
                $scope.MensajesErrorHorarios.push('La hora inicial no puede ser mayor a la hora final del día jueves')
                guardar = false
            }
            fechainicio = new Date('1999', '01', '01', $scope.HorarioInicialViernes.Nombre[0] + $scope.HorarioInicialViernes.Nombre[1], $scope.HorarioInicialViernes.Nombre[3] + $scope.HorarioInicialViernes.Nombre[4])
            fechafin = new Date('1999', '01', '01', $scope.HorarioFinalViernes.Nombre[0] + $scope.HorarioFinalViernes.Nombre[1], $scope.HorarioFinalViernes.Nombre[3] + $scope.HorarioFinalViernes.Nombre[4])
            if (fechainicio > fechafin) {
                $scope.MensajesErrorHorarios.push('La hora inicial no puede ser mayor a la hora final del día viernes')
                guardar = false
            }
            fechainicio = new Date('1999', '01', '01', $scope.HorarioInicialSabado.Nombre[0] + $scope.HorarioInicialSabado.Nombre[1], $scope.HorarioInicialSabado.Nombre[3] + $scope.HorarioInicialSabado.Nombre[4])
            fechafin = new Date('1999', '01', '01', $scope.HorarioFinalSabado.Nombre[0] + $scope.HorarioFinalSabado.Nombre[1], $scope.HorarioFinalSabado.Nombre[3] + $scope.HorarioFinalSabado.Nombre[4])
            if (fechainicio > fechafin) {
                $scope.MensajesErrorHorarios.push('La hora inicial no puede ser mayor a la hora final del día sábado')
                guardar = false
            }
            fechainicio = new Date('1999', '01', '01', $scope.HorarioInicialDomingo.Nombre[0] + $scope.HorarioInicialDomingo.Nombre[1], $scope.HorarioInicialDomingo.Nombre[3] + $scope.HorarioInicialDomingo.Nombre[4])
            fechafin = new Date('1999', '01', '01', $scope.HorarioFinalDomingo.Nombre[0] + $scope.HorarioFinalDomingo.Nombre[1], $scope.HorarioFinalDomingo.Nombre[3] + $scope.HorarioFinalDomingo.Nombre[4])
            if (fechainicio > fechafin) {
                $scope.MensajesErrorHorarios.push('La hora inicial no puede ser mayor a la hora final del día domingo')
                guardar = false
            }
            return guardar

        }
        //#endregion

        //--------------------------------------------------------------------------------------------------------
        //--------------------------------------------MODAL OFICINAS----------------------------------------------
        //--------------------------------------------------------------------------------------------------------

        //#region CONSULTAR
        $scope.OficinaUsuarios = function (Codigo, Identificador, Nombre) {
            $scope.BuscarCajas();
            $scope.CodigoUsuarioTemp = Codigo;
            $scope.modalIdentificador = Identificador;
            $scope.modalNombre = Nombre;
            $scope.Oficinas = $scope.ListadoOficinas[0];

            console.log('llego', Codigo, Nombre)

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo,
            }
            blockUI.delay = 1000;
            UsuariosFactory.ConsultarUsuarioOficinas(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoGridOficinas = [];
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                var registro = item;
                                $scope.ListadoGridOficinas.push(registro);
                            });
                        }
                    }
                    $scope.ListadoGridOficinas.forEach(function (item) {
                        var ind = $scope.ListadoOficinas.indexOf({ Codigo: item.Codigo })
                        if (ind > -1) {
                            $scope.ListadoOficinas.splice(ind, 1)
                            $scope.Oficinas = $scope.ListadoOficinas[0];
                        }
                    });
                    $scope.totalRegistrosOfi = response.data.Datos[0].TotalRegistros;
                    $scope.totalPaginasOfi = Math.ceil($scope.totalRegistrosOfi / $scope.cantidadRegistrosPorPaginasOfi);
                    $scope.PrimerPaginaOfi()
                });
            showModal('modalOficinaUsuarios');
        }
        //#endregion

        //#region ADICIONAR A LISTA
        $scope.AdicionarOficina = function (opcion) {
            if (opcion > 0) {
                for (var i = 0; i < $scope.ListadoOficinas.length; i++) {
                    var Oficina = $scope.ListadoOficinas[i]

                    var Caja = $linq.Enumerable().From($scope.ListadoCajas).First('$.Codigo == 0 ');
                    Caja.Codigo = 0

                    $scope.OficinaAgregada = 0;
                    $scope.ListadoGridOficinas.forEach(function (item) {
                        if (item.Codigo == Oficina.Codigo) { $scope.ListadoCajas
                        console.log('listado', $scope.ListadoGridOficinas)
                            $scope.OficinaAgregada = 1;
                        }
                    });
                    if ($scope.OficinaAgregada == 0) {
                        console.log('listado2', $scope.ListadoGridOficinas)
                        $scope.ListadoGridOficinas.push({
                            Codigo: Oficina.Codigo, Nombre: Oficina.Nombre,
                            CodigoCaja: Caja.Codigo, Caja:Caja.Nombre
                        });
                    }
                }
                $scope.PrimerPaginaOfi()
            } else {
                $scope.ListadoTempOficina = [];
                $scope.OficinaAgregada = 0;
                $scope.ListadoTempOficina.Codigo = $scope.Oficinas.Codigo;
                $scope.ListadoTempOficina.Nombre = $scope.Oficinas.Nombre;

                $scope.ListadoTempOficina.CodigoCaja = $scope.Cajas.Codigo;
                $scope.ListadoTempOficina.Caja = $scope.Cajas.Nombre;

                $scope.ListadoGridOficinas.forEach(function (item) {
                    if (item.Codigo == $scope.ListadoTempOficina.Codigo) {
                        $scope.OficinaAgregada = 1;
                    }
                });
                if ($scope.OficinaAgregada == 0) {
                    $scope.ListadoGridOficinas.push({
                        Codigo: $scope.ListadoTempOficina.Codigo,
                        Nombre: $scope.ListadoTempOficina.Nombre,
                        CodigoCaja: $scope.ListadoTempOficina.CodigoCaja,
                        Caja: $scope.ListadoTempOficina.Caja

                    });
                    $scope.PrimerPaginaOfi()
                }                
                else {
                    ShowError("La oficina ya se encuentra adicionada");
                }
            }
        }
        //#endregion

        //#region GUARDAR
        $scope.GuardarOficina = function () {
            $scope.listaOficinasGuardar = [];
            $scope.ListadoGridOficinas.forEach(function (itmOficinas) {
                parametros =
                    {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: itmOficinas.Codigo,
                        CodigoUsuario: $scope.CodigoUsuarioTemp,
                        CodigoCaja: itmOficinas.CodigoCaja
                    };
                $scope.listaOficinasGuardar.push(parametros);
            })
            UsuariosFactory.GuardarUsuarioOficinas($scope.listaOficinasGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('Se asignó la oficina (s) corretamente');
                            closeModal('modalOficinaUsuarios')
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                });
        }
        //#endregion

        //#region ELIMINAR
        $scope.EliminarOficina = function (OficinaCodigo) {

            let index = $scope.ListadoGridOficinas.findIndex(Oficina => Oficina.Codigo == OficinaCodigo);

            $scope.ListadoGridOficinas.splice(index, 1);
            $scope.PaginarOficinas()
        }
        //#endregion

        //#region CERRAR MODAL
        $scope.CerrarModalOficinaUsuarios = function () {
            closeModal('modalOficinaUsuarios');
        }
        //#endregion
        if ($routeParams.Codigo !== undefined) {
            $scope.Codigo = $routeParams.Codigo;
            $scope.Buscar();
        }
        else {
            $scope.Codigo = undefined;
        }
    }]);