﻿Print 'gsp_insertar_detalle_impuestos_planilla_despachos'
GO
DROP PROCEDURE gsp_insertar_detalle_impuestos_planilla_despachos
GO
CREATE PROCEDURE gsp_insertar_detalle_impuestos_planilla_despachos 
(
@par_EMPR_Codigo	smallint,
@par_ENPD_Numero	numeric,
@par_ENIM_Codigo	numeric,
@par_Valor_Tarifa	numeric,
@par_Valor_Base		money,
@par_Valor_Impuesto	money
)
AS
BEGIN
	
	INSERT INTO Detalle_Impuestos_Planilla_Despachos (
				EMPR_Codigo,
				ENPD_Numero,
				ENIM_Codigo,
				Valor_Tarifa,
				Valor_Base,
				Valor_Impuesto
				)
	VALUES (
				@par_EMPR_Codigo,
				@par_ENPD_Numero,
				@par_ENIM_Codigo,
				@par_Valor_Tarifa,
				@par_Valor_Base,
				@par_Valor_Impuesto
	)

	SELECT ENPD_Numero As Numero FROM Detalle_Impuestos_Planilla_Despachos
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND ENPD_Numero = @par_ENPD_Numero

END
GO