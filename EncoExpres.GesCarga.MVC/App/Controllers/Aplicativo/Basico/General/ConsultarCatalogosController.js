﻿EncoExpresApp.controller("ConsultarCatalogosCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', 'CatalogosFactory', 'ValorCatalogosFactory', 'ConfiguracionCatalogosFactory', '$linq',
    function ($scope, $timeout, $routeParams, blockUI, CatalogosFactory, ValorCatalogosFactory, ConfiguracionCatalogosFactory, $linq) {
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Catálogos' }];
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        var filtros = {};
        $scope.ListadoEstados = [];
        $scope.paginaActual = 1;
        $scope.CodigoCatalogo = 0

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CATALOGOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar


        // -------------------------- Constantes ---------------------------------------------------------------------//

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            if ($scope.ListadoValorCatalogosConsulta.length > 0) {
                $scope.paginaActual = 1;
                $scope.ListadoValorCatalogos = [];

                var i = 0;
                for (i = 0; i <= $scope.ListadoValorCatalogosConsulta.length - 1; i++) {
                    if (i < $scope.cantidadRegistrosPorPagina) {
                        $scope.ListadoValorCatalogos.push($scope.ListadoValorCatalogosConsulta[i])
                    }
                }
            }

        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.PaginaAuxiliar = $scope.paginaActual;
                $scope.paginaActual += 1;
                $scope.ListadoValorCatalogos = [];
                if ($scope.ListadoValorCatalogosConsulta.length > 0) {
                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoValorCatalogosConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoValorCatalogosConsulta[i].EliminarCatalogo = CODIGO_UNO
                            $scope.ListadoValorCatalogos.push($scope.ListadoValorCatalogosConsulta[i]);
                        }
                    }
                }
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {

                if ($scope.ListadoValorCatalogosConsulta.length > 0) {
                    $scope.ListadoValorCatalogos = [];
                    $scope.paginaActual -= 1;

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListadoValorCatalogosConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoValorCatalogos.push($scope.ListadoValorCatalogosConsulta[i]);
                        }
                    }
                }
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            if ($scope.ListadoValorCatalogosConsulta.length > 0) {
                $scope.paginaActual = $scope.totalPaginas;
                $scope.ListadoValorCatalogos = [];

                var i = 0;
                for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListadoValorCatalogosConsulta.length - 1; i++) {
                    if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                        $scope.ListadoValorCatalogosConsulta[i].EliminarCatalogo = CODIGO_UNO
                        $scope.ListadoValorCatalogos.push($scope.ListadoValorCatalogosConsulta[i])
                    }
                }
            }
        };

        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

        /*Cargar el combo de Catalogos*/
        CatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCatalogos = [];
                    $scope.ListadoCatalogos.push({ Nombre: 'Seleccione un Catálogo', Codigo: 0 })
                    $('#CmbCatalogos').selectpicker('destroy')
                    $timeout(function () {
                        $('#CmbCatalogos').selectpicker('refresh')
                    }, 200);
                    response.data.Datos.forEach(function (item) {
                        if (item.Actualizable == 1) {
                            $scope.ListadoCatalogos.push(item)
                        }
                    })
                    //En caso de espacio en blanco
                    $timeout(function () {
                        if ($('#CmbCatalogos')[0].options[0].value == "?") {
                            $('#CmbCatalogos')[0].options[0].remove()
                        }
                    }, 50);
                    $timeout(function () {
                        $('#CmbCatalogos').selectpicker()
                    }, 200);
                    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
                        $scope.CodigoCatalogo = $routeParams.Codigo
                        if ($scope.CodigoCatalogo.Codigo > 0) {
                            $scope.Catalogos = $linq.Enumerable().From($scope.ListadoCatalogos).First('$.Codigo ==' + $scope.CodigoCatalogo.Codigo);
                        }
                    } else {
                        $scope.Catalogos = $linq.Enumerable().From($scope.ListadoCatalogos).First('$.Codigo ==0');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ConsultarCatalogos = function (catalogo) {
            $scope.MensajesError = [];
            $scope.Catalogo = catalogo
            if (catalogo.Codigo > 0) {
                /*Cargar el combo de Catalogos*/
                ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: catalogo.Codigo } }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.cantidadRegistrosPorPagina = 10;
                            $scope.ListadoValorCatalogos = [];
                            $scope.ListadoValorCatalogosConsulta = [];
                            $scope.paginaActual = 1;
                            $scope.ListadoValorCatalogosConsulta = response.data.Datos;
                            if ($scope.ListadoValorCatalogosConsulta.length > 0) {
                                //Se muestran la cantidad de recorridos por pagina
                                //En este punto se muestra siempre desde la primera pagina
                                var i = 0;
                                for (i = 0; i <= $scope.ListadoValorCatalogosConsulta.length - 1; i++) {
                                    if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                        var Decimal = $scope.ListadoValorCatalogosConsulta[i].Codigo / 100

                                        if (Decimal - Math.floor(Decimal) > 0) {
                                            $scope.ListadoValorCatalogosConsulta[i].EliminarCatalogo = 1
                                            $scope.ListadoValorCatalogos.push($scope.ListadoValorCatalogosConsulta[i])
                                        }
                                        else {
                                            $scope.ListadoValorCatalogosConsulta[i].EliminarCatalogo = 0
                                            $scope.ListadoValorCatalogos.push($scope.ListadoValorCatalogosConsulta[i])
                                        }
                                    }
                                }


                                $scope.totalRegistros = $scope.ListadoValorCatalogosConsulta.length;
                                $scope.totalPaginas = Math.ceil($scope.ListadoValorCatalogosConsulta.length / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }


                            /* Obtener parametros*/
                            if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
                                $scope.CodigoCatalogo = $routeParams.Codigo
                                if ($scope.CodigoCatalogo.Codigo > 0) {
                                    $scope.Catalogos = $linq.Enumerable().From($scope.ListadoCatalogos).First('$.Codigo ==' + $scope.CodigoCatalogo.Codigo);
                                }
                            }

                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                /*Cargar el combo de Catalogos*/
                ConfiguracionCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoCatalogo: catalogo.Codigo }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoConfiguracionCatalogos = [];

                            $scope.ListadoConfiguracionCatalogos = response.data.Datos;
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });

            }
            else {
                $scope.ListadoConfiguracionCatalogos = [];
                $scope.ListadoValorCatalogos = [];
                $scope.ListadoValorCatalogosConsulta = [];
            }
        }
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            $scope.MensajesError = [];
            if ($scope.Catalogo == undefined || $scope.Catalogo == "" || $scope.Catalogo == null || $scope.Catalogo.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar un catálogo');
            }
            else if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {

                $scope.NumeroConsecutivoCatalogo = $scope.ListadoValorCatalogosConsulta[$scope.ListadoValorCatalogosConsulta.length - 1];
                $scope.NumeroConsecutivoCatalogo = $scope.NumeroConsecutivoCatalogo.Codigo + 1
                document.location.href = '#!GestionarCatalogos/' + $scope.Catalogo.Codigo + '/' + 0 + '/' + $scope.NumeroConsecutivoCatalogo;
            }
        };
        /*------------------------------------------------------------------------------------Eliminar Catalogos-----------------------------------------------------------------------*/
        $scope.EliminarValorCatalogo = function (codigo, catalogo, Nombre) {
            $scope.Catalogo = catalogo
            $scope.AuxiCodigo = codigo
            $scope.AuxiNombre = Nombre
            showModal('modalEliminarValorCatalogo');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.AuxiCodigo,
                Catalogo: $scope.Catalogo,
            };

            ValorCatalogosFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó el ítem  ' + $scope.AuxiNombre + ' del catálogo ' + $scope.Catalogo.Nombre);
                        closeModal('modalEliminarValorCatalogo');
                        $scope.ConsultarCatalogos($scope.Catalogo);
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.CodigoCatalogo = parseInt($routeParams.Codigo)
            if ($scope.CodigoCatalogo > 0) {
                $scope.paginaActual = 1;
                $scope.paginaActual = 1;
                $scope.ConsultarCatalogos($routeParams.Codigo = { Codigo: $scope.CodigoCatalogo });
            }
        }

    }]);