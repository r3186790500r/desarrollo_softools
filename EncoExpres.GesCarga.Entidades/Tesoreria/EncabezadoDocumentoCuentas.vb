﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Tesoreria

    ''' <summary>
    ''' Clase <see cref="EncabezadoDocumentoCuentas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoDocumentoCuentas
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="EncabezadoDocumentoCuentas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoDocumentoCuentas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Numero = Read(lector, "Numero")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Fecha = Read(lector, "Fecha")
            Tercero = New Terceros With {.Codigo = Read(lector, "TERC_Codigo"), .Nombre = Read(lector, "NombreTercero")}
            DocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOOR_Codigo")}
            CodigoDocumentoOrigen = Read(lector, "Codigo_Documento_Origen")
            NumeroDocumento = Read(lector, "Documento_Origen")
            NumeroManifiesto = Read(lector, "Numero_Manifiesto")
            FechaDocumento = Read(lector, "Fecha_Documento_Origen")
            FechaVenceDocumento = Read(lector, "Fecha_Vence_Documento_Origen")
            ValorTotal = Read(lector, "Valor_Total")
            Abono = Read(lector, "Abono")
            Saldo = Read(lector, "Saldo")
            Anulado = Read(lector, "Anulado")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
            Observaciones = Read(lector, "Observaciones")
            Banco = New Bancos With {.Codigo = Read(lector, "BANC_Codigo_Transferencia")}
            TipoCuentaBancaria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICB_Codigo_Transferencia")}
            NumeroCuentaBancaria = Read(lector, "Numero_Cuenta_Transferencia")
        End Sub

        <JsonProperty>
        Public Property Banco As Bancos

        <JsonProperty>
        Public Property TipoCuentaBancaria As ValorCatalogos
        <JsonProperty>
        Public Property NumeroCuentaBancaria As String
        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Tercero As Terceros
        <JsonProperty>
        Public Property CodigoDocumentoOrigen As Integer
        <JsonProperty>
        Public Property DocumentoOrigen As ValorCatalogos
        <JsonProperty>
        Public Property FechaDocumento As DateTime
        <JsonProperty>
        Public Property FechaVenceDocumento As DateTime
        <JsonProperty>
        Public Property ValorTotal As Double
        <JsonProperty>
        Public Property ValorPagar As Double
        <JsonProperty>
        Public Property Abono As Double
        <JsonProperty>
        Public Property Saldo As Double
        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleDocumentoCuentaComprobantes)
        <JsonProperty>
        Public Property CuentaPuc As PlanUnicoCuentas
        <JsonProperty>
        Public Property FechaCancelacionPago As Date
        <JsonProperty>
        Public Property Aprobado As Integer
        <JsonProperty>
        Public Property UsuarioAprobo As Usuarios
        <JsonProperty>
        Public Property FechaAprobo As Date
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Numeracion As String
        <JsonProperty>
        Public Property AplicaPSL As Integer
        <JsonProperty>
        Public Property NumeroManifiesto As Integer
        <JsonProperty>
        Public Property CreadaManual As Integer
    End Class

End Namespace
