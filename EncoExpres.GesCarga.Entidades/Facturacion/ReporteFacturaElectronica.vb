﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Facturacion
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Facturacion
    <JsonObject>
    Public NotInheritable Class ReporteFacturaElectronica
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Factura = New Facturas With {
                .Numero = Read(lector, "Numero"),
                .NumeroDocumento = Read(lector, "Numero_Documento"),
                .Fecha = Read(lector, "Fecha"),
                .NombreTipoFactura = Read(lector, "CATA_TIFA_Nombre"),
                .Cliente = New Terceros With {.NombreCompleto = Read(lector, "NombreCliente")},
                .OficinaFactura = New Oficinas With {.Nombre = Read(lector, "OficinaFactura")},
                .FormaPago = New ValorCatalogos With {.Nombre = Read(lector, "CATA_FPVE_Nombre")},
                .ValorFactura = Read(lector, "Valor_Factura"),
                .FacturaElectronica = New FacturaElectronica With {.MensajeError = Read(lector, "Mensaje_Error")}
            }
            NotaFactura = New NotasFacturas With {
                .Numero = Read(lector, "Numero"),
                .NumeroDocumento = Read(lector, "Numero_Documento"),
                .NombreTipoDocumento = Read(lector, "TIDO_Nombre"),
                .Fecha = Read(lector, "Fecha"),
                .Factura = New Facturas With {.NumeroDocumento = Read(lector, "ENFA_NumeroDocumento")},
                .Clientes = New Terceros With {.NombreCompleto = Read(lector, "NombreCliente")},
                .OficinaNota = New Oficinas With {.Nombre = Read(lector, "OficinaNota")},
                .ValorNota = Read(lector, "Valor_Nota"),
                .NotaElectronica = Read(lector, "Nota_Electronica"),
                .MensajeError = Read(lector, "Mensaje_Error")
            }
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub

        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property FormaPago As ValorCatalogos
        <JsonProperty>
        Public Property Factura As Facturas
        <JsonProperty>
        Public Property NotaFactura As NotasFacturas
        <JsonProperty>
        Public Property ListaDocumentos As IEnumerable(Of Integer)
        <JsonProperty>
        Public Property TotalFacturas As Integer
        <JsonProperty>
        Public Property TotalNotasDebito As Integer
        <JsonProperty>
        Public Property TotalNotasCredito As Integer
    End Class
End Namespace
