USE [GESCARGA50_DESA]
GO

/****** Object:  StoredProcedure [dbo].[gsp_insertar_encabezado_remesa_paqueteria]    Script Date: 11/01/2022 4:57:33 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_encabezado_remesa_paqueteria]    
(            
 @par_EMPR_Codigo smallint,            
 @par_ENRE_Numero numeric,            
 @par_TERC_Codigo_Transportador_Externo numeric = NULL,            
 @par_Numero_Guia_Externa varchar(50) = NULL,            
 @par_CATA_TTRP_Codigo numeric = NULL,            
 @par_CATA_TDRP_Codigo numeric = NULL,            
 @par_CATA_TPRP_Codigo numeric = NULL,            
 @par_CATA_TSRP_Codigo numeric = NULL,            
 @par_CATA_TERP_Codigo numeric = NULL,            
 @par_CATA_TIRP_Codigo numeric = NULL,            
 @par_Fecha_Interfaz_Tracking datetime = NULL,            
 @par_Fecha_Interfaz_WMS datetime  = NULL,          
            
 @par_CATA_ESRP_Codigo numeric = NULL,          
 @par_Observaciones_Remitente varchar(500) = NULL,          
 @par_Observaciones_Destinatario varchar(500) = NULL,          
 @par_CIUD_Codigo_Origen numeric = NULL,          
 @par_CIUD_Codigo_Destino numeric = NULL,          
          
 @par_OFIC_Codigo_Origen numeric = NULL,          
 @par_OFIC_Codigo_Destino numeric = NULL,          
 @par_OFIC_Codigo_Actual  numeric = NULL,          
 @par_Descripcion_Mercancia varchar(500) = NULL,          
 @par_Reexpedicion  smallint = NULL,            
             
 @par_Centro_Costo VARCHAR(20) = NULL,            
 @par_CATA_LISC_Codigo NUMERIC = NULL,            
 @par_SICD_Codigo_Cargue NUMERIC = NULL,            
 @par_SICD_Codigo_Descargue NUMERIC = NULL,            
 @par_Fecha_Entrega DATETIME = NULL,          
 @par_Largo numeric(18,5) = NULL,          
 @par_Alto numeric(18,5) = NULL,          
 @par_Ancho numeric(18,5) = NULL,          
 @par_Peso_Volumetrico numeric(18,5) = NULL,          
 @par_Peso_A_Cobrar numeric(18,5) = NULL,          
 @par_Flete_Pactado numeric = NULL,          
 @par_Ajuste_Flete smallint= NULL   ,            
 @par_Linea_Negocio_Paqueteria NUMERIC = NULL,            
 @par_Maneja_Detalle_Unidades NUMERIC = NULL,            
 @par_Recoger_Oficina_Destino NUMERIC = NULL,          
 @par_OFIC_Codigo_Recibe NUMERIC = NULL,          
 @par_ZONA_Codigo NUMERIC = NULL,          
 @par_Latitud NUMERIC(18, 10) = NULL,          
 @par_Longitud NUMERIC(18, 10) = NULL,          
 @par_HERP_Codigo NUMERIC = NULL  ,            
 @par_Liquidar_Unidad NUMERIC = NULL,        
 @par_TERC_Codigo_Aforador NUMERIC = NULL,        
 @par_OFIC_Codigo_Registro_Manual NUMERIC = NULL,        
 @par_Guia_Creada_Ruta_Conductor NUMERIC = NULL,     
 @par_USUA_Codigo_Crea NUMERIC = NULL,

 @par_CAJA_Codigo_Responsable_Venta NUMERIC = NULL,
 @par_OFIC_Codigo_Factura_Venta NUMERIC = NULL
)            
AS            
BEGIN        
 INSERT INTO Remesas_Paqueteria (        
 EMPR_Codigo,            
 ENRE_Numero,            
 TERC_Codigo_Transportador_Externo,            
 Numero_Guia_Externa,            
 CATA_TTRP_Codigo,            
 CATA_TDRP_Codigo,            
 CATA_TPRP_Codigo,            
 CATA_TSRP_Codigo,            
 CATA_TERP_Codigo,            
 CATA_TIRP_Codigo,            
 Fecha_Interfaz_Cargue_Archivo,            
 Fecha_Interfaz_Cargue_WMS,            
 CATA_ESRP_Codigo,            
 Observaciones_Remitente,            
 Observaciones_Destinatario,            
 CIUD_Codigo_Origen,            
 CIUD_Codigo_Destino,            
            
 OFIC_Codigo_Origen,            
 OFIC_Codigo_Destino,            
 OFIC_Codigo_Actual,            
 Descripcion_Mercancia,            
 Reexpedicion,            
 ZOCI_Codigo_Entrega,            
            
 Centro_Costo,            
 CATA_LISC_Codigo,            
 SICD_Codigo_Cargue,            
 SICD_Codigo_Descargue,            
 Fecha_Entrega,            
 Largo,            
 Alto,            
 Ancho,            
 Peso_Volumetrico,            
 Peso_A_Cobrar,            
 Flete_Pactado,            
 Ajuste_Flete,            
 CATA_LNPA_Codigo,            
 Recoger_Oficina_Destino,          
 OFIC_Codigo_Recibe,          
 Maneja_Detalle_Unidades,          
 Latitud,          
 Longitud,          
 CATA_HERP_Codigo,            
 Liquidar_Unidad,        
 TERC_Codigo_Aforador,        
 OFIC_Codigo_Registro_Manual,    
 Guia_Creada_Ruta_Conductor,

 CAJA_Codigo_Responsable_Venta,
 OFIC_Codigo_Factura_Venta
 )         
 VALUES (@par_EMPR_Codigo,            
 ISNULL(@par_ENRE_Numero, 0),             
 ISNULL(@par_TERC_Codigo_Transportador_Externo, 0),             
 ISNULL(@par_Numero_Guia_Externa, ''),             
 ISNULL(@par_CATA_TTRP_Codigo, 6200),             
 ISNULL(@par_CATA_TDRP_Codigo, 6300),           
 ISNULL(@par_CATA_TPRP_Codigo, 6400),             
 ISNULL(@par_CATA_TSRP_Codigo, 6500),             
 ISNULL(@par_CATA_TERP_Codigo, 6601),             
 ISNULL(@par_CATA_TIRP_Codigo, 6700),             
 ISNULL(@par_Fecha_Interfaz_Tracking, '01/01/1900'),             
 ISNULL(@par_Fecha_Interfaz_WMS, '01/01/1900'),             
 ISNULL(@par_CATA_ESRP_Codigo, 0),             
 ISNULL(@par_Observaciones_Remitente, ''),             
 ISNULL(@par_Observaciones_Destinatario, ''),            
 @par_CIUD_Codigo_Origen, @par_CIUD_Codigo_Destino,             
 ISNULL(@par_OFIC_Codigo_Origen, 0),             
 ISNULL(@par_OFIC_Codigo_Destino, 0),             
 ISNULL(@par_OFIC_Codigo_Actual, 0),             
 ISNULL(@par_Descripcion_Mercancia, ''),             
 ISNULL(@par_Reexpedicion, 0),             
 ISNULL(@par_ZONA_Codigo, 0),            
 @par_Centro_Costo,             
 @par_CATA_LISC_Codigo,             
 @par_SICD_Codigo_Cargue,             
 @par_SICD_Codigo_Descargue,             
 @par_Fecha_Entrega,             
 @par_Largo, @par_Alto,             
 @par_Ancho,             
 @par_Peso_Volumetrico,             
 @par_Peso_A_Cobrar,             
 @par_Flete_Pactado,             
 @par_Ajuste_Flete,            
 @par_Linea_Negocio_Paqueteria,            
 @par_Recoger_Oficina_Destino,            
 @par_OFIC_Codigo_Recibe,          
 @par_Maneja_Detalle_Unidades,          
 @par_Latitud,          
 @par_Longitud,          
 @par_HERP_Codigo,            
 @par_Liquidar_Unidad,        
 @par_TERC_Codigo_Aforador,        
 @par_OFIC_Codigo_Registro_Manual,    
 @par_Guia_Creada_Ruta_Conductor,
 
 @par_CAJA_Codigo_Responsable_Venta,
 @par_OFIC_Codigo_Factura_Venta
 )            
 SELECT            
 ENRE_Numero AS Numero            
 FROM Remesas_Paqueteria            
 WHERE EMPR_Codigo = @par_EMPR_Codigo            
 AND ENRE_Numero = @par_ENRE_Numero            
END    
GO


