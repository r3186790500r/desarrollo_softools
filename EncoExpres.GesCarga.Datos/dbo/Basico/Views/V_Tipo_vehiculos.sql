﻿Print 'V_Tipo_vehiculos'
GO
DROP VIEW V_Tipo_vehiculos
GO
CREATE VIEW V_Tipo_vehiculos 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 22
GO