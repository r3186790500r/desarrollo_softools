﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Paqueteria
    <JsonObject>
    Public Class LegalizarRecaudoRemesas
        Inherits BaseDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")
            ValorContado = Read(lector, "Valor_Contado")
            ValorContraEntregas = Read(lector, "Valor_Contra_Entrega")
            Observaciones = Read(lector, "Observaciones")
            InterfazContable = Read(lector, "Interfaz_Contable")
            FechaInterfazContable = Read(lector, "Fecha_Interfase_Contable")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            TotalRegistros = Read(lector, "TotalRegistros")
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "OFIC_Nombre")}
        End Sub
        <JsonProperty>
        Public Property NumeroDocumentoPlanilla As Integer
        <JsonProperty>
        Public Property NumeroDocumentoLegalizarRemesas As Integer
        <JsonProperty>
        Public Property ValorContado As Double
        <JsonProperty>
        Public Property ValorContraEntregas As Double
        <JsonProperty>
        Public Property ValorTotalLegalizar As Double
        <JsonProperty>
        Public Property InterfazContable As Integer
        <JsonProperty>
        Public Property IntentosInterfazContable As Integer
        <JsonProperty>
        Public Property FechaInterfazContable As DateTime
        <JsonProperty>
        Public Property MensajeInterfazContable As String
        <JsonProperty>
        Public Property DetalleRemesas As IEnumerable(Of DetalleLegalizarRecaudoRemesas)
    End Class
End Namespace