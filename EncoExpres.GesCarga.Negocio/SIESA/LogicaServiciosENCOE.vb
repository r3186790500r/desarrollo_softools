﻿Imports EncoExpres.GesCarga.Entidades
Imports System.Text.RegularExpressions
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Negocio.WS_ENCOE_SIESA_GENERICTRANSFER
Imports EncoExpres.GesCarga.Negocio.co.encoexpres.gti
Imports System.Xml
Imports EncoExpres.GesCarga.Entidades.SIESA
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Public NotInheritable Class LogicaServiciosENCOE

    ' Retorna la Lista Cajas o una Caja
    Dim objGenericTrasfer As WSUNOEE
    Dim objgenerarPlanoTrasfer As wsGenerarPlano
    Dim strXml As String
    Sub New()
        objGenericTrasfer = New WSUNOEE()
        objgenerarPlanoTrasfer = New wsGenerarPlano()
    End Sub


    Public Function Importar_Tercero_SIESA(tercero As TerceroSIESA) As String
        Dim xmlDocumento As XmlDocument
        Dim xmlElementoRaiz As XmlElement
        Dim xmlElementoRoot As XmlElement
        Dim xmlElemento As XmlElement
        Dim tipoIde As String
        Dim tipoTercero As String
        Dim departamento = ""
        Dim ciudad = ""
        Dim idCondPago = ""
        Dim idTipoCli = ""

        xmlDocumento = New XmlDocument

        xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
        xmlDocumento.AppendChild(xmlElementoRaiz)

        xmlElementoRoot = xmlDocumento.DocumentElement
        xmlElemento = xmlDocumento.CreateElement("Terceros")
        xmlElementoRoot.AppendChild(xmlElemento)
        xmlElementoRoot = xmlDocumento.DocumentElement

        If (tercero.Cupo.Equals("Contado") Or tercero.Cupo.Equals("Credito")) Then
            idCondPago = "00D"
            idTipoCli = "0001"
        Else
            idCondPago = "05D"
            idTipoCli = "0002"
        End If

        Select Case tercero.TipoCliente
                Case "CC"
                    tipoIde = "C"
                    tipoTercero = "1"
                Case "NIT"
                    tipoIde = "N"
                    tipoTercero = "2"
                Case Else
                    tipoIde = "0"
                    tipoTercero = "1"
            End Select

            If (tercero.CodigoCiudad.Length > 3) Then
            ciudad = Strings.Right(tercero.CodigoCiudad, 3)
            departamento = tercero.CodigoCiudad.Substring(0, tercero.CodigoCiudad.Length - 3)
            If (departamento.Length < 2) Then
                departamento = "0" + departamento
            End If
        End If


        Try
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_ID", tercero.Nit)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_NIT", tercero.Nit)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_ID_TIPO_IDENT", tipoIde)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_IND_TIPO_TERCERO", tipoTercero)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_RAZON_SOCIAL", tercero.RazonSocial)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_APELLIDO1", tercero.Apellido1)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_APELLIDO2", tercero.Apellido2)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_NOMBRES", tercero.Nombres)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_NOMBRE_EST", "")
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_CONTACTO", tercero.Nombres + " " + tercero.Apellido1 + " " + tercero.Apellido2)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_DIRECCION1", tercero.Direccion)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_DIRECCION2", "")
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_PAIS", "169")
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_DEPTO", departamento)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_CIUDAD", ciudad)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_BARRIO", "")
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_TELEFONO", tercero.Telefono)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_EMAIL", tercero.Email)
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_FECHA_NACIMIENTO", Year(Now).ToString + IIf(Month(Now) < 10, "0" + Month(Now).ToString, Month(Now).ToString) + IIf(Day(Now) < 10, "0" + Day(Now).ToString, Day(Now).ToString))
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_ID_CIIU", "0010")
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_IND_NO_DOMICILIADO", "0")
            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_CELULAR", tercero.Telefono)

            strXml = xmlDocumento.InnerXml

            ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
            objgenerarPlanoTrasfer = New wsGenerarPlano()
            Dim strPlanoXML = objgenerarPlanoTrasfer.ImportarDatosXML(109072, "TERCEROS_INTEGRACION", 2, "1", "gt", "gt", strXml, "C:\inetpub\wwwroot\GTIntegration\Planos\")

            If strPlanoXML = "Importacion exitosa" Then

                xmlDocumento = New XmlDocument

                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("Clientes")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_ID_TERCERO", tercero.Nit)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_ID_SUCURSAL", "001")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_DESCRIPCION_SUCURSAL", "CONTADO")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_ID_VENDEDOR", "ENCO")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_IND_CALIFICACION", "C")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_ID_COND_PAGO", idCondPago)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_CUPO_CREDITO", "0")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_ID_TIPO_CLI", idTipoCli)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_ID_CO_FACTURA", "")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_CONTACTO", tercero.Nombres + " " + tercero.Apellido1 + " " + tercero.Apellido2)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_DIRECCION1", tercero.Direccion)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_DIRECCION2", "")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_PAIS", "169")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_DEPTO", departamento)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_CIUDAD", ciudad)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_BARRIO", "")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_TELEFONO", tercero.Telefono)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_EMAIL", tercero.Email)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_FECHA_INGRESO", Year(Now).ToString + IIf(Month(Now) < 10, "0" + Month(Now).ToString, Month(Now).ToString) + IIf(Day(Now) < 10, "0" + Day(Now).ToString, Day(Now).ToString))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_ID_CO_MOVTO_FACTURA", "")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F201_ID_UN_MOVTO_FACTURA", "")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f201_id_cobrador", "ENCO")
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f015_celular", tercero.Telefono)

                strXml = xmlDocumento.InnerXml

                ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                objgenerarPlanoTrasfer = New wsGenerarPlano()
                strPlanoXML = objgenerarPlanoTrasfer.ImportarDatosXML(109073, "CLIENTES_INTEGRACION", 2, "1", "gt", "gt", strXml, "C:\inetpub\wwwroot\GTIntegration\Planos\")

                If strPlanoXML = "Importacion exitosa" Then
                    Return strPlanoXML
                Else
                    Return "Fallo la creacion de la sucursal del tercero en SIESA"
                End If

            Else
                    Return "Fallo la creacion del tercero en SIESA"

            End If

        Catch ex As Exception
            Return "Fallo tecnico en la creacion del tercero en SIESA"
        End Try



    End Function

    Private Sub AdicionarNodoXml(ByRef xmlDocumento As XmlDocument, ByRef xmlElementoRoot As XmlElement, strElemento As String, strElementoNodo As String)
        Try
            xmlElementoRoot.LastChild.AppendChild(xmlDocumento.CreateElement(strElemento))
            xmlElementoRoot.LastChild.LastChild.AppendChild(xmlDocumento.CreateTextNode(strElementoNodo))

        Catch ex As Exception

        End Try
    End Sub

    Public Function Consultar_Terceros_SIESA(strTerceroSIESA As TerceroSIESA) As List(Of TerceroSIESA)

        Dim strRespuestaXML As DataSet
        Dim lista As New List(Of TerceroSIESA)
        Dim objoficinasTrasfer = New WSUNOEE()
        Dim telefono As String
        Dim codigoCiudad As String
        strXml = "<?xml version='1.0' encoding='utf-8'?> <Consulta><NombreConexion>pruebas nomina 2015</NombreConexion><IdCia>1</IdCia><IdProveedor>IYS</IdProveedor><IdConsulta>INTEGRACION_TERCEROS_FIN</IdConsulta><Usuario>generict</Usuario><Clave>generic2021</Clave><Parametros><nit>" & strTerceroSIESA.Nit & "</nit></Parametros></Consulta>"
        strRespuestaXML = objoficinasTrasfer.EjecutarConsultaXML(strXml)

        Dim data = strRespuestaXML.Tables(0).DataSet().Tables(0)

        For Each item In data.Rows

            If (item("estado").Equals("ACTIVO")) Then
                If (String.IsNullOrEmpty(Convert.ToString(item("telefono")))) Then
                    telefono = Convert.ToString(item("celular"))
                Else
                    telefono = Convert.ToString(item("telefono"))
                End If

                If (item("codigo_ciudad") = Nothing) Then
                    codigoCiudad = ""
                Else
                    codigoCiudad = Convert.ToString(item("codigo_ciudad")).Substring(3)
                End If

                lista.Add(New TerceroSIESA() With {
                        .Sucursal = Convert.ToString(item("sucursal")),
                        .Nit = Convert.ToString(item("nit")),
                        .RazonSocial = Convert.ToString(item("razon_social")),
                        .NombreEstablecimiento = Convert.ToString(item("nombre_estable")),
                        .Nombres = Convert.ToString(item("nombres")),
                        .Apellido1 = Convert.ToString(item("apellido1")),
                        .Apellido2 = Convert.ToString(item("apellido2")),
                        .Direccion = Convert.ToString(item("direccion")),
                        .Contacto = Convert.ToString(item("contacto")),
                        .CodigoCiudad = codigoCiudad,
                        .Telefono = telefono,
                        .Email = Convert.ToString(item("email")),
                        .CedulaVendedor = Convert.ToString(item("cedula_vendedor")).Trim,
                        .NombreVendedor = Convert.ToString(item("nombre_vendedor"))
                })
            End If

        Next
        Return lista
    End Function

    Public Function Consultar_Cajas_SIESA(strCodCajSIESA As ServiciosENCOE) As List(Of ServiciosENCOE)

        Dim strRespuestaXML As DataSet
        Dim lista As New List(Of ServiciosENCOE)
        Dim params As String = ""

        If Not IsNothing(strCodCajSIESA.CodigoCaja) Then
            params = "<caja>" & strCodCajSIESA.CodigoCaja & "</caja>"
        End If

        If Not IsNothing(strCodCajSIESA.CentroOperacion) Then
            params = params & "<centro_operacion>" & strCodCajSIESA.CentroOperacion & "</centro_operacion>"
        End If

        strXml = "<?xml version='1.0' encoding='utf-8'?> <Consulta><NombreConexion>pruebas nomina 2015</NombreConexion><IdCia>1</IdCia><IdProveedor>IYS</IdProveedor><IdConsulta>INTEGRACION_MAESTROS_CAJAS</IdConsulta><Usuario>generict</Usuario><Clave>generic2021</Clave><Parametros>" & params & "</Parametros></Consulta>"
        strRespuestaXML = objGenericTrasfer.EjecutarConsultaXML(strXml)

        Dim data = strRespuestaXML.Tables(0).DataSet().Tables(0)

        For Each item In data.Rows
            lista.Add(New ServiciosENCOE() With {.CodigoCaja = Convert.ToString(item("id_caja")), .CentroOperacion = Convert.ToString(item("centro_operacion")), .Descripcion = Convert.ToString(item("descripcion"))})
        Next
        Return lista
    End Function

    Public Function Consultar_Sucursales_Tenedor_SIESA(strNitTene As SucursalTenedorSIESA) As List(Of SucursalTenedorSIESA)

        Dim strRespuestaXML As DataSet
        Dim lista As New List(Of SucursalTenedorSIESA)


        strXml = "<?xml version='1.0' encoding='utf-8'?> 
                <Consulta>
                    <NombreConexion>pruebas nomina 2015</NombreConexion>
				    <IdCia>1</IdCia>
				    <IdProveedor>IYS</IdProveedor>
				    <IdConsulta>INTEGRACION_PROVEEDOR_DUENO_DE_VEHICULOS</IdConsulta>
				    <Usuario>generict</Usuario>
				    <Clave>generic2021</Clave>
				    <Parametros>
					    <nit>" & strNitTene.Nit & "</nit>
				    </Parametros>
                </Consulta>"
        strRespuestaXML = objGenericTrasfer.EjecutarConsultaXML(strXml)

        Dim data = strRespuestaXML.Tables(0).DataSet().Tables(0)

        For Each item In data.Rows
            lista.Add(New SucursalTenedorSIESA() With {.Nit = Convert.ToString(item("nit")), .Sucursal = Regex.Replace(Convert.ToString(item("id_sucursal")), "[^\d]", "")})
        Next
        Return lista
    End Function



    Private Function Consultar_Lista_Precios_Cliente(strIdentClie As String) As String
        Try
            'Dim strRespuestaXML As String

            '' Invocar WEB SERVICE CONSULTA SELENE método ListaPrecios
            'Me.objSelene = New WebServicePlexaSoapClient
            'strRespuestaXML = objSelene.ListaPrecio(strIdentClie)

            ' Asignar la Respuesta a la Lista


        Catch ex As Exception

        End Try

        Return 1


    End Function

    Public Function Consultar_Oficinas_SIESA(strCodOficinaSIESA As OficinaSIESA) As List(Of OficinaSIESA)

        Dim strRespuestaXML As DataSet
        Dim lista As New List(Of OficinaSIESA)
        Dim objoficinasTrasfer = New WSUNOEE()
        Dim telefono As String
        strXml = "<?xml version='1.0' encoding='utf-8'?> <Consulta><NombreConexion>pruebas nomina 2015</NombreConexion><IdCia>1</IdCia><IdProveedor>IYS</IdProveedor><IdConsulta>INTEGRACION_CENTRO_DE_OPERACIONES</IdConsulta><Usuario>generict</Usuario><Clave>generic2021</Clave><Parametros><centro_operacion>" & strCodOficinaSIESA.CodigoSiesa & "</centro_operacion></Parametros></Consulta>"
        strRespuestaXML = objoficinasTrasfer.EjecutarConsultaXML(strXml)

        Dim data = strRespuestaXML.Tables(0).DataSet().Tables(0)

        For Each item In data.Rows

            If (item("estado").Equals("ACTIVO")) Then
                If (String.IsNullOrEmpty(Convert.ToString(item("telefono")))) Then
                    telefono = Convert.ToString(item("celular"))
                Else
                    telefono = Convert.ToString(item("telefono"))
                End If

                lista.Add(New OficinaSIESA() With {.CodigoSiesa = Convert.ToString(item("centro_operacion")), .Nombre = Convert.ToString(item("descripcion")), .Regional = Convert.ToString(item("regional")), .Contacto = Convert.ToString(item("contacto")), .Ciudad = Convert.ToString(item("codigo_ciudad")).Substring(3), .Telefono = telefono, .Direccion = Convert.ToString(item("direccion")), .Email = Convert.ToString(item("email"))})
            End If

        Next
        Return lista
    End Function





End Class





