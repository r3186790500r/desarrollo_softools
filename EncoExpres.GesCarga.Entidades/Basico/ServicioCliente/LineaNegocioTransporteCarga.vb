﻿Imports Newtonsoft.Json

Namespace Basico.ServicioCliente

    ''' <summary>
    ''' Clase <see cref="LineaNegocioTransporteCarga"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class LineaNegocioTransporteCarga
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="LineaNegocioTransporteCarga"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransporteCarga"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")

        End Sub

        <JsonProperty>
        Public Property Nombre As String

    End Class

End Namespace
