﻿PRINT 'gsp_consultar_cuenta_bancarias'
GO
DROP PROCEDURE gsp_consultar_cuenta_bancarias
GO  
CREATE PROCEDURE gsp_consultar_cuenta_bancarias  
(  
  
 @par_EMPR_Codigo SMALLINT,  
 @par_Codigo NUMERIC = NULL,  
 @par_Nombre VARCHAR(50) = NULL,  
 @par_Codigo_Tipo_Cuenta NUMERIC = NULL,  
 @par_Nombre_Banco VARCHAR(50) = NULL,  
 @par_Numero_Cuenta VARCHAR(30) = NULL,  
 @par_Estado SMALLINT = NULL,  
 @par_NumeroPagina INT = NULL,  
 @par_RegistrosPagina INT = NULL  
)  
AS  
BEGIN  
 SET NOCOUNT ON;  
  DECLARE  
   @CantidadRegistros INT  
  SELECT @CantidadRegistros = (  
    SELECT DISTINCT   
     COUNT(1)   
    FROM  
     Cuenta_Bancarias  CUBA,  
     Bancos BANC,  
     Valor_Catalogos TICB,  
     Plan_Unico_Cuentas PLUC,  
     Terceros TERC  
    WHERE  
  
     CUBA.EMPR_Codigo = BANC.EMPR_Codigo   
     AND CUBA.BANC_Codigo = BANC.Codigo  
  
     AND CUBA.EMPR_Codigo = TICB.EMPR_Codigo  
     AND CUBA.CATA_TICB_Codigo = TICB.Codigo   
  
     AND CUBA.EMPR_Codigo = PLUC.EMPR_Codigo  
     AND CUBA.PLUC_Codigo = PLUC.Codigo  
  
     AND CUBA.EMPR_Codigo = TERC.EMPR_Codigo  
     AND CUBA.TERC_Codigo = TERC.Codigo  
  
     AND CUBA.EMPR_Codigo = @par_EMPR_Codigo  
     AND CUBA.Codigo > 0  
     AND CUBA.Codigo = ISNULL(@par_Codigo, CUBA.Codigo)  
     AND CUBA.Estado = ISNULL(@par_Estado, CUBA.Estado)  
     AND ((CUBA.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
     AND ((CUBA.Numero_Cuenta LIKE '%' + @par_Numero_Cuenta + '%') OR (@par_Numero_Cuenta IS NULL))  
	 AND ((BANC.Nombre LIKE '%' + @par_Nombre_Banco + '%') OR (@par_Nombre IS NULL))    
	 AND CUBA.CATA_TICB_Codigo = ISNULL(@par_Codigo_Tipo_Cuenta, CUBA.CATA_TICB_Codigo)  

     );  
              
    WITH Pagina AS  
    (  
  
    SELECT  
     CUBA.EMPR_Codigo,  
     CUBA.Codigo,  
     CUBA.BANC_Codigo,  
     CUBA.Numero_Cuenta,  
     CUBA.CATA_TICB_Codigo,  
     CUBA.Nombre,  
     CUBA.TERC_Codigo,  
     CUBA.PLUC_Codigo,  
     CUBA.Sobregiro_Autorizado,  
     CUBA.Saldo_Actual,  
     CUBA.Estado,  
     TERC.Numero_Identificacion,  
     TERC.Razon_Social + TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 AS NombreTercero,  
     BANC.Nombre AS NombreBanco,  
     TICB.Campo1 AS TipoCuentaBancaria,  
     PLUC.Nombre AS NombreCuentaPUC,  
     ROW_NUMBER() OVER(ORDER BY CUBA.Nombre) AS RowNumber  
    FROM  
     Cuenta_Bancarias  CUBA,  
     Bancos BANC,  
     Valor_Catalogos TICB,  
     Plan_Unico_Cuentas PLUC,  
     Terceros TERC  
    WHERE  
  
     CUBA.EMPR_Codigo = BANC.EMPR_Codigo   
     AND CUBA.BANC_Codigo = BANC.Codigo  
  
     AND CUBA.EMPR_Codigo = TICB.EMPR_Codigo  
     AND CUBA.CATA_TICB_Codigo = TICB.Codigo   
  
     AND CUBA.EMPR_Codigo = PLUC.EMPR_Codigo  
     AND CUBA.PLUC_Codigo = PLUC.Codigo  
  
     AND CUBA.EMPR_Codigo = TERC.EMPR_Codigo  
     AND CUBA.TERC_Codigo = TERC.Codigo  
     AND CUBA.Codigo > 0  
     AND CUBA.EMPR_Codigo = @par_EMPR_Codigo  
     AND CUBA.Codigo = ISNULL(@par_Codigo, CUBA.Codigo)  
     AND CUBA.Estado = ISNULL(@par_Estado, CUBA.Estado)  
     AND ((CUBA.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
     AND ((CUBA.Numero_Cuenta LIKE '%' + @par_Numero_Cuenta + '%') OR (@par_Numero_Cuenta IS NULL))  
	 AND ((BANC.Nombre LIKE '%' + @par_Nombre_Banco + '%') OR (@par_Nombre IS NULL))   
	 AND CUBA.CATA_TICB_Codigo = ISNULL(@par_Codigo_Tipo_Cuenta, CUBA.CATA_TICB_Codigo)  
  
  )  
  SELECT DISTINCT  
  0 AS Obtener,  
  EMPR_Codigo,  
  Codigo,  
  BANC_Codigo,  
  Numero_Cuenta,  
  CATA_TICB_Codigo,  
  Nombre,  
  TERC_Codigo,  
  PLUC_Codigo,  
  Sobregiro_Autorizado,  
  Saldo_Actual,  
  Numero_Identificacion,  
  NombreTercero,  
  Estado,  
  NombreBanco,  
  TipoCuentaBancaria,  
  NombreCuentaPUC,  
  @CantidadRegistros AS TotalRegistros,  
  @par_NumeroPagina AS PaginaObtener,  
  @par_RegistrosPagina AS RegistrosPagina  
  FROM  
   Pagina  
  WHERE  
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
  order by Nombre  
END  
GO