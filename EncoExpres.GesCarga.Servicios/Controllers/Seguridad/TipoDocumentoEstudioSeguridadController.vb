﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Seguridad
Imports EncoExpres.GesCarga.Negocio.Seguridad

''' <summary>
''' Controlador <see cref="TipoDocumentoEstudioSeguridadController"/>
''' </summary>
<Authorize>
Public Class TipoDocumentoEstudioSeguridadController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TipoDocumentoEstudioSeguridad) As Respuesta(Of IEnumerable(Of TipoDocumentoEstudioSeguridad))
        Return New LogicaTipoDocumentoEstudioSeguridad(CapaPersistenciaTipoDocumentoEstudioSeguridad).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TipoDocumentoEstudioSeguridad) As Respuesta(Of TipoDocumentoEstudioSeguridad)
        Return New LogicaTipoDocumentoEstudioSeguridad(CapaPersistenciaTipoDocumentoEstudioSeguridad).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TipoDocumentoEstudioSeguridad) As Respuesta(Of Long)
        Return New LogicaTipoDocumentoEstudioSeguridad(CapaPersistenciaTipoDocumentoEstudioSeguridad).Guardar(entidad)
    End Function

End Class
