﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Negocio.Mantenimiento

<Authorize>
Public Class TareaMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TareaMantenimiento) As Respuesta(Of IEnumerable(Of TareaMantenimiento))
        Return New LogicaTareaMantenimiento(CapaPersistenciaTareaMantenimiento, CapaPersistenciaDetalleTareaMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TareaMantenimiento) As Respuesta(Of TareaMantenimiento)
        Return New LogicaTareaMantenimiento(CapaPersistenciaTareaMantenimiento, CapaPersistenciaDetalleTareaMantenimiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TareaMantenimiento) As Respuesta(Of Long)
        Return New LogicaTareaMantenimiento(CapaPersistenciaTareaMantenimiento, CapaPersistenciaDetalleTareaMantenimiento).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As TareaMantenimiento) As Respuesta(Of Boolean)
        Return New LogicaTareaMantenimiento(CapaPersistenciaTareaMantenimiento, CapaPersistenciaDetalleTareaMantenimiento).Anular(entidad)
    End Function
End Class