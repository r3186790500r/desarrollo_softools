﻿EncoExpresApp.controller("ConsultarListadoFacturaCarteraCtrl", ['$scope', '$timeout', 'TercerosFactory', '$linq', 'blockUI', '$routeParams', 'blockUIConfig',
    function ($scope, $timeout, TercerosFactory, $linq, blockUI, $routeParams, blockUIConfig) {

        $scope.MapaSitio = [{ Nombre: 'Facturación' }, { Nombre: 'Listados Cartera' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.ListadoCliente = [];
        $scope.FiltroCliente = true;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_LISTADOS_FACTURA_CARTERA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        // Cargar combos de módulo y listados 
        $scope.ListaFacturaCartera = [];
        for (var i = 0; i < $scope.ListadoMenuListados.length; i++) {
            if ($scope.ListadoMenuListados[i].Padre == OCION_MENU_LISTADOS_FACTURA_CARTERA) {
                $scope.ListaFacturaCartera.push({ NombreMenu: $scope.ListadoMenuListados[i].Nombre, NombreReporte: $scope.ListadoMenuListados[i].UrlPagina, Codigo: $scope.ListadoMenuListados[i].Codigo});
            }
        }

        $scope.ModeloDocumentosFactura = $scope.ListaFacturaCartera[0];

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }

        $scope.ListadoClientes = [];
        $scope.AutocompleteClientes = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoClientes = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoClientes);
                }
            }
            return $scope.ListadoClientes;
        };


        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.DatosRequeridos();
            if ($scope.DatosRequeridos() == true) {
                //Depende del listado seleccionado se enviará el nombre por parametro
                $scope.ArmarFiltro();
                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.ModeloDocumentosFactura.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.ModeloDocumentosFactura.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf);
                }
                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';

            }
        };

        $scope.DatosRequeridos = function () {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '')
                && ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '')
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                    if ($scope.ModeloFechaInicial < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.ModeloFechaInicial - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaInicial;
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaInicial;
                    }
                }
            }
            return continuar;
        };

        $scope.ArmarFiltro = function () {
            if ($scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '' && $scope.ModeloFechaInicial !== null) {
                var ModeloFechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaInicial);
                $scope.FiltroArmado += '&Fecha_Inicial=' + ModeloFechaInicial;
            }
            if ($scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '' && $scope.ModeloFechaFinal !== null) {
                var ModeloFechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaFinal);
                $scope.FiltroArmado += '&Fecha_Final=' + ModeloFechaFinal;
            }
            if ($scope.ModeloFechaCorte !== undefined && $scope.ModeloFechaCorte !== '' && $scope.ModeloFechaCorte !== null) {
                var ModeloFechaCorte = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaCorte);
                $scope.FiltroArmado += '&Fecha_Corte=' + ModeloFechaCorte;
            }
            if ($scope.ModeloCliente !== undefined && $scope.ModeloCliente !== '' && $scope.ModeloCliente !== null) {
                $scope.FiltroArmado += '&Cliente=' + $scope.ModeloCliente.Codigo;
            }
        };
    }]);