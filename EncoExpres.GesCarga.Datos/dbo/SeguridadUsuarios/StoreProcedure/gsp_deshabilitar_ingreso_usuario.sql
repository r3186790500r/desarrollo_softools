﻿PRINT 'gsp_deshabilitar_ingreso_usuario'
GO
DROP PROCEDURE gsp_deshabilitar_ingreso_usuario
GO
CREATE PROCEDURE gsp_deshabilitar_ingreso_usuario(
  @par_EMPR_Codigo SMALLINT,
  @par_USUA_Codigo SMALLINT 
  )
AS
BEGIN
  
    UPDATE Usuarios SET Habilitado = 0
    WHERE EMPR_Codigo = @par_EMPR_Codigo
    AND Codigo = @par_USUA_Codigo

    SELECT Habilitado AS Habilitado FROM Usuarios WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_USUA_Codigo
END
GO