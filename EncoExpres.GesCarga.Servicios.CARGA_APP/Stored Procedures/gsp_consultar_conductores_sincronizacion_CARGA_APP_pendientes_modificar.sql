PRINT 'gsp_consultar_conductores_sincronizacion_CARGA_APP_pendientes_modificar'
GO
DROP PROCEDURE gsp_consultar_conductores_sincronizacion_CARGA_APP_pendientes_modificar
GO
CREATE PROCEDURE gsp_consultar_conductores_sincronizacion_CARGA_APP_pendientes_modificar
(
@par_EMPR_Codigo smallint,
@par_Perfiles varchar(max)
)
AS
BEGIN
SELECT DISTINCT
LTRIM(RTRIM(CONCAT(TERC.Razon_Social,' ',TERC.Nombre))) AS Nombres,
LTRIM(RTRIM(CONCAT(TERC.Apellido1,' ',TERC.Apellido2))) AS Apellidos,
TERC.Codigo_Alterno,
TERC.Estado,
TERC.Codigo,
TERC.Numero_Identificacion,
TERC.Direccion,
TERC.CIUD_Codigo,
TERC.Emails,
IIF(ISNULL(CACO.Campo1,'SIN PARAMETRIZAR') = '','SIN PARAMETRIZAR',ISNULL(CACO.Campo1,'SIN PARAMETRIZAR')) AS DescripcionCategoria,
CACO.Codigo AS CATA_CCCV_Codigo,
ISNULL(ARL.Fecha_Vence,'') AS fechaExpiracionARL,
ISNULL(DIAC.Fecha_Vence,'') AS fechaExpiracionCarnetDIACO,
0 AS Reportes,
0 AS Comparendos,
'' AS ListadoClientesBloqueados,
0 As BloqueadosSeguimientoEspecial,
ISNULL(LICO.Fecha_Vence,'') AS fechaVencimientoLicencia,
ISNULL(SUPE.Fecha_Vence,'') AS fechaVencimientoCarnetMercanciaPeligrosa,
CIUD.Codigo_Alterno As Ciudad,
TERC.Telefonos,
ISNULL((SELECT TOP(1) Placa FROM Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND (TERC_Codigo_Conductor = TERC.Codigo OR TERC_Codigo_Tenedor = TERC.Codigo OR TERC_Codigo_Propietario = TERC.Codigo )),'(N/A).') AS PlacaCabezote,
ISNULL((SELECT TOP(1) Placa FROM Semirremolques WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo =( SELECT TOP(1) SEMI_Codigo FROM Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND (TERC_Codigo_Conductor = TERC.Codigo OR TERC_Codigo_Tenedor = TERC.Codigo OR TERC_Codigo_Propietario = TERC.Codigo ))),'(N/A).') AS PlacaTrailer
FROM Terceros TERC

LEFT JOIN Registro_Sincronizacion_APP_CARGA AS RSAC
ON TERC.EMPR_Codigo = RSAC.EMPR_Codigo  
AND TERC.Codigo = RSAC.Codigo
AND RSAC.CATA_OSCA_Codigo = 23805 -- Origen Sincronizacion CARGA APP Conductores

INNER JOIN Perfil_Terceros PETE ON
TERC.EMPR_Codigo = PETE.EMPR_Codigo AND
TERC.Codigo = PETE.TERC_Codigo
AND PETE.Codigo IN(SELECT * FROM Func_Dividir_String(@par_Perfiles,','))

LEFT JOIN Ciudades CIUD ON
TERC.EMPR_Codigo = CIUD.EMPR_Codigo AND
TERC.CIUD_Codigo = CIUD.Codigo

LEFT JOIN Tercero_Conductores TECO ON
TERC.EMPR_Codigo = TECO.EMPR_Codigo AND
TERC.Codigo = TECO.TERC_Codigo

LEFT JOIN Valor_Catalogos CACO ON
TERC.EMPR_Codigo = CACO.EMPR_Codigo AND
TECO.CATA_CCCV_Codigo = CACO.Codigo

LEFT JOIN GESCARGA50_DOCU_DESARROLLO.dbo.Tercero_Documentos ARL ON
TERC.EMPR_Codigo = ARL.EMPR_Codigo AND
TERC.Codigo = ARL.TERC_Codigo AND
ARL.CDGD_Codigo = 203 --ARL

LEFT JOIN GESCARGA50_DOCU_DESARROLLO.dbo.Tercero_Documentos DIAC ON
TERC.EMPR_Codigo = DIAC.EMPR_Codigo AND
TERC.Codigo = DIAC.TERC_Codigo AND
DIAC.CDGD_Codigo = 10004 --Carn� DIACO

LEFT JOIN GESCARGA50_DOCU_DESARROLLO.dbo.Tercero_Documentos LICO ON
TERC.EMPR_Codigo = LICO.EMPR_Codigo AND
TERC.Codigo = LICO.TERC_Codigo AND
LICO.CDGD_Codigo = 202 --Licencia Conducci�n

LEFT JOIN GESCARGA50_DOCU_DESARROLLO.dbo.Tercero_Documentos SUPE ON
TERC.EMPR_Codigo = SUPE.EMPR_Codigo AND
TERC.Codigo = SUPE.TERC_Codigo AND
SUPE.CDGD_Codigo = 305 --Carnet Sustancias Peligrosas

WHERE TERC.EMPR_Codigo = @par_EMPR_Codigo
AND ISNULL(RSAC.Sincronizado,0) = 1 AND
TERC.Fecha_Modifica > RSAC.Fecha_Sincronizacion
END
GO