﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.FlotaPropia

    ''' <summary>
    ''' Clase <see cref=" ConceptoGastos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ConceptoGastos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConceptoGastos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConceptoGastos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
            Operacion = Read(lector, "Operacion")
            CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .Nombre = Read(lector, "NombreCuenta")}
            ValorFijo = Read(lector, "Valor_Fijo")
            Porcentaje = Read(lector, "Valor_Porcentaje")
            ConceptoSistema = Read(lector, "Concepto_Sistema")
            FechaCrea = Read(lector, "Fecha_Crea")

        End Sub

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Operacion As Integer

        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas

        <JsonProperty>
        Public Property ValorFijo As Double

        <JsonProperty>
        Public Property Porcentaje As Double

        <JsonProperty>
        Public Property ConceptoSistema As Integer

        <JsonProperty>
        Public Property AplicaValorBaseImpuestos As Integer


    End Class
End Namespace
