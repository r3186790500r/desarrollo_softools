﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Tesoreria
    <JsonObject>
    Public NotInheritable Class SucursalesSIESA
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Perfil_Terceros = New ValorCatalogos With {.Codigo = Read(lector, "PETE_Codigo"), .Nombre = Read(lector, "PETE_Name")}
            Tipo_Conector = New ValorCatalogos With {.Codigo = Read(lector, "TICS_Codigo"), .Nombre = Read(lector, "TICS_Name")}
            Forma_Pago = New ValorCatalogos With {.Codigo = Read(lector, "FPVE_Codigo"), .Nombre = Read(lector, "FPVE_Name")}
            Codigo_SIESA = Read(lector, "Codigo_SIESA")
            UsuarioCrea = New SeguridadUsuarios.Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea")}
            FechaCrea = Read(lector, "Fecha_Crea")
            UsuarioModifica = New SeguridadUsuarios.Usuarios With {.Codigo = Read(lector, "USUA_Modifica")}
            FechaModifica = Read(lector, "Fecha_Modifica")
            TotalRegistros = Read(lector, "TotalRegistros")

        End Sub

        <JsonProperty>
        Public Property Perfil_Terceros As ValorCatalogos
        <JsonProperty>
        Public Property Tipo_Conector As ValorCatalogos
        <JsonProperty>
        Public Property Forma_Pago As ValorCatalogos
        <JsonProperty>
        Public Property Codigo_SIESA As Object
    End Class
End Namespace

