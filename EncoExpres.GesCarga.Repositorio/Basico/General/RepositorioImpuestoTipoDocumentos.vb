﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioImpuestoTipoDocumentos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioImpuestoTipoDocumentos
        Inherits RepositorioBase(Of ImpuestoTipoDocumentos)

        Public Overrides Function Consultar(filtro As ImpuestoTipoDocumentos) As IEnumerable(Of ImpuestoTipoDocumentos)
            Dim lista As New List(Of ImpuestoTipoDocumentos)
            Dim item As ImpuestoTipoDocumentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.CodigoTipoDocumento)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", filtro.CodigoRuta)
                conexion.AgregarParametroSQL("@par_Valor", filtro.Valor)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_generar_impuesto_tipo_documentos]")

                While resultado.Read
                    item = New ImpuestoTipoDocumentos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ImpuestoTipoDocumentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ImpuestoTipoDocumentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ImpuestoTipoDocumentos) As ImpuestoTipoDocumentos
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As ImpuestoTipoDocumentos) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace