﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Negocio.Despachos

''' <summary>
''' Controlador <see cref="RemesasController"/>
''' </summary>
<Authorize>
Public Class RemesasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Remesas) As Respuesta(Of IEnumerable(Of Remesas))
        Return New LogicaRemesas(CapaPersistenciaRemesas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Remesas) As Respuesta(Of Remesas)
        Return New LogicaRemesas(CapaPersistenciaRemesas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Remesas) As Respuesta(Of Long)
        Return New LogicaRemesas(CapaPersistenciaRemesas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Remesas) As Respuesta(Of Remesas)
        Return New LogicaRemesas(CapaPersistenciaRemesas).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarRemesasPendientesFacturar")>
    Public Function ConsultarRemesasPendientesFacturar(ByVal filtro As Remesas) As Respuesta(Of IEnumerable(Of Remesas))
        Return New LogicaRemesas(CapaPersistenciaRemesas).ConsultarRemesasPendientesFacturar(filtro)
    End Function

    <HttpPost>
    <ActionName("ActualizarDistribuido")>
    Public Function ActualizarDistribuido(ByVal entidad As Remesas) As Respuesta(Of Remesas)
        Return New LogicaRemesas(CapaPersistenciaRemesas).ActualizarDistribuido(entidad)
    End Function

    <HttpPost>
    <ActionName("MarcarRemesasFacturadas")>
    Public Function MarcarRemesasFacturadas(ByVal entidad As Remesas) As Respuesta(Of Remesas)
        Return New LogicaRemesas(CapaPersistenciaRemesas).MarcarRemesasFacturadas(entidad)
    End Function

    <HttpPost>
    <ActionName("CumplirRemesas")>
    Public Function CumplirGuias(ByVal entidad As Remesas) As Respuesta(Of Boolean)
        Return New LogicaRemesas(CapaPersistenciaRemesas).CumplirGuias(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarNumerosRemesas")>
    Public Function ConsultarNumerosRemesas(ByVal entidad As Remesas) As Respuesta(Of IEnumerable(Of Remesas))
        Return New LogicaRemesas(CapaPersistenciaRemesas).ConsultarNumerosRemesas(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerPrecintosAleatorios")>
    Public Function ObtenerPrecintosAleatorios(ByVal entidad As Remesas) As Respuesta(Of IEnumerable(Of DetallePrecintos))
        Return New LogicaRemesas(CapaPersistenciaRemesas).ObtenerPrecintosAleatorios(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarRemesasPlanillaDespachos")>
    Public Function ConsultarRemesasPlanillaDespachos(ByVal entidad As Remesas) As Respuesta(Of IEnumerable(Of Remesas))
        Return New LogicaRemesas(CapaPersistenciaRemesas).ConsultarRemesasPlanillaDespachos(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarCumplidoRemesas")>
    Public Function ConsultarCumplidoRemesas(ByVal entidad As Remesas) As Respuesta(Of IEnumerable(Of Remesas))
        Return New LogicaRemesas(CapaPersistenciaRemesas).ConsultarCumplidoRemesas(entidad)
    End Function
    <HttpPost>
    <ActionName("ActualizarRemesasExcel")>
    Public Function ActualizarRemesasExcel(ByVal entidad As Remesas) As Respuesta(Of Long)
        Return New LogicaRemesas(CapaPersistenciaRemesas).ActualizarRemesasExcel(entidad)
    End Function
End Class
