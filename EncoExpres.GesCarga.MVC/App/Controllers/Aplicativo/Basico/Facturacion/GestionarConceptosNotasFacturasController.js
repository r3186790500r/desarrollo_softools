﻿EncoExpresApp.controller('GestionarConceptosNotasFacturasCtrl', ['$scope', '$timeout', '$linq', 'blockUI', '$routeParams','ConceptoNotasFacturasFactory',
    function ($scope, $timeout, $linq, blockUI, $routeParams, ConceptoNotasFacturasFactory) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Facturación' }, { Nombre: 'Conceptos Notas Facturas' }, { Nombre: 'Gestionar' }];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONCEPTOS_NOTAS_FACTURA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.Codigo = '';
        $scope.Nombre = '';

        $scope.MensajesError = [];

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarConceptosNotasFacturas/';
        };

        $scope.ListaTiposNota = [
            { Nombre: 'CRÉDITO', Codigo: 190 },
            { Nombre: 'DÉBITO', Codigo: 195 },
        ];

        $scope.TipoNota = $linq.Enumerable().From($scope.ListaTiposNota).First('$.Codigo == 190');

        //Obtener Parametros:
        if ($routeParams.Codigo > 0) {
            $scope.Codigo = $routeParams.Codigo;
            Obtener();
        }

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Codigo,
                    Nombre: $scope.Nombre,
                    Estado: 1,
                    TipoDocumento: $scope.TipoNota.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }

                ConceptoNotasFacturasFactory.Guardar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Codigo == 0) {
                                    ShowSuccess('Se guardó el Concepto "' + $scope.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se Modificó el Concepto "' + $scope.Nombre + '"');
                                }
                                location.href = '#!ConsultarConceptosNotasFacturas/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
           
            if ($scope.Nombre == undefined || $scope.Nombre == '' || $scope.Nombre == null) {
                $scope.MensajesError.push('Debe ingresar un Nombre');
                continuar = false;
            }
           
            return continuar;
        }


        function Obtener() {
            blockUI.start('Cargando Concepto código No. ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Concepto Código No.' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            blockUI.delay = 1000;
            ConceptoNotasFacturasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Codigo = response.data.Datos.Codigo;                       
                        $scope.Nombre = response.data.Datos.Nombre;
                        $scope.TipoNota = $linq.Enumerable().From($scope.ListaTiposNota).First('$.Codigo ==' + response.data.Datos.TipoDocumento)
                        if (response.data.Datos.ConceptoSistema == true) {
                            $scope.DeshabilitarActualizar = true;
                        }
                    }
                    else {
                        ShowError('No se logro consultar el Concepto código ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarConceptosNotasFacturas';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarConceptosNotasFacturas';
                });

            blockUI.stop();
        };




        //Máscaras:
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };


    }

     
]);