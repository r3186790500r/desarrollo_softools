﻿PRINT 'gsp_obtener_remesa'
GO
DROP PROCEDURE gsp_obtener_remesa
GO 
CREATE PROCEDURE gsp_obtener_remesa 
(      
 @par_EMPR_Codigo SMALLINT,    
 @par_Numero INT      
)      
AS    
BEGIN    
 SELECT    
  1 AS Obtener    
    ,ENRE.EMPR_Codigo    
    ,ENRE.Numero    
    ,ENRE.TIDO_Codigo    
    ,TIDO.Nombre AS TipoDocumento    
    ,ENRE.Numero_Documento    
    ,ENRE.CATA_TIRE_Codigo    
    ,TIRE.Nombre AS TipoRemesa    
    ,ISNULL(ENRE.Documento_Cliente, '') AS Documento_Cliente    
    ,ISNULL(ENRE.Fecha_Documento_Cliente, '') AS Fecha_Documento_Cliente    
    ,ENRE.Fecha    
    ,ENRE.RUTA_Codigo    
    ,RUTA.Nombre AS NombreRuta    
    ,ENRE.PRTR_Codigo    
    ,PRTR.Nombre AS NombreProducto   
	,PRTR.UEPT_Codigo AS Unidad_Empaque 
    ,ENRE.CATA_FOPR_Codigo    
    ,ENRE.TERC_Codigo_Cliente    
    ,CONCAT(CLIE.Nombre, ' ', CLIE.Apellido1, ' ', CLIE.Apellido2, ' ', CLIE.Razon_Social) AS NombreCliente    
    ,ENRE.TERC_Codigo_Remitente    
    ,CONCAT(REMI.Nombre, ' ', REMI.Apellido1, ' ', REMI.Apellido2, ' ', REMI.Razon_Social) AS NombreRemitente    
    ,ENRE.CIUD_Codigo_Remitente    
    ,CIRE.Nombre AS CiudadRemitente    
    ,ENRE.Direccion_Remitente    
    ,ENRE.Telefonos_Remitente    
    ,ENRE.Observaciones    
    ,ENRE.Cantidad_Cliente    
    ,ENRE.Peso_Cliente    
    ,ENRE.Peso_Volumetrico_Cliente    
    ,ENRE.DTCV_Codigo    
    ,ENRE.Valor_Flete_Cliente    
    ,ENRE.Valor_Manejo_Cliente    
    ,ENRE.Valor_Seguro_Cliente    
    ,ENRE.Valor_Descuento_Cliente    
    ,ENRE.Total_Flete_Cliente    
    ,ENRE.Valor_Comercial_Cliente    
    ,ENRE.Cantidad_Transportador    
    ,ENRE.Peso_Transportador    
    ,ENRE.Valor_Flete_Transportador    
    ,ENRE.Total_Flete_Transportador    
    ,ENRE.TERC_Codigo_Destinatario    
    ,CONCAT(DEST.Nombre, ' ', DEST.Apellido1, ' ', DEST.Apellido2, ' ', DEST.Razon_Social) AS NombreDestinatario    
    ,ENRE.CIUD_Codigo_Destinatario    
    ,CIDE.Nombre AS CiudadDestinatario    
    ,ENRE.Direccion_Destinatario    
    ,ENRE.Telefonos_Destinatario    
    ,ENRE.Anulado    
    ,ENRE.Estado    
    ,ENRE.Fecha_Crea    
    ,ENRE.USUA_Codigo_Crea    
    ,USCR.Codigo_Usuario AS UsuarioCrea    
    ,ISNULL(ENRE.Fecha_Modifica, '') AS Fecha_Modifica    
    ,ISNULL(ENRE.USUA_Codigo_Modifica, '') AS USUA_Codigo_Modifica    
    ,ISNULL(ENRE.Fecha_Anula, '') AS Fecha_Anula    
    ,ISNULL(ENRE.USUA_Codigo_Anula, '') AS USUA_Codigo_Anula    
    ,ISNULL(ENRE.Causa_Anula, '') AS Causa_Anula    
    ,ENRE.OFIC_Codigo    
    ,OFIC.Nombre AS NombreOficina    
    ,ISNULL(ENRE.Numeracion, '') AS Numeracion    
    ,ENRE.ETCC_Numero AS NumeroTarifarioCompra    
    ,ENRE.ETCV_Numero AS NumeroTarifarioVenta    
    ,ENRE.ESOS_Numero    
    ,ESOS.Numero_Documento AS OrdenServicio    
    ,ISNULL(ENRE.ENOC_Numero,0) AS NumeroOrdenCargue    
    ,ISNULL(ENOC.Numero_Documento,0) AS NumeroDocumentoOrdenCargue    
    ,ENRE.ENPD_Numero AS NumeroPlanilla    
    ,ISNULL(ENPD.Numero_Documento,0) AS NumeroDocumentoPlanilla    
    ,ENRE.ENMC_Numero AS NumeroManifiesto    
    ,ISNULL(ENMC.Numero_Documento,0) AS NumeroDocumentoManifiesto    
    ,ENRE.ECPD_Numero AS NumeroCumplido    
    ,ENRE.ENFA_Numero AS NumeroFactura    
    ,ENRE.VEHI_Codigo    
    ,ISNULL(ENRE.SEMI_Codigo, 0) AS SEMI_Codigo    
    ,VEHI.Placa    
    ,ISNULL(SEMI.Placa, '') AS PlacaSemirremolque    
    ,ENRE.TERC_Codigo_Conductor    
    ,CONCAT(COND.Nombre, ' ', COND.Apellido1, ' ', COND.Apellido2, ' ', COND.Razon_Social) AS NombreConductor    
    ,VEHI.TERC_Codigo_Tenedor    
    ,CONCAT(TENE.Nombre, ' ', TENE.Apellido1, ' ', TENE.Apellido2, ' ', TENE.Razon_Social) AS NombreTenedor    
    ,COND.Telefonos AS TelefonoConductor    
    ,DTCV.TATC_Codigo_Venta As TATC_Codigo   
    ,TTTC.NombreTarifa AS TipoTarifa    
    ,TTTC.Codigo AS CodigoValorTipoTarifa    
    ,TTTC.Nombre AS NombreValorTipoTarifa    
    ,ISNULL(VEHI.CATA_TIVE_Codigo,0) As CATA_TIVE_Codigo  
    ,ISNULL(TIVE.Nombre,'') AS TipoVehiculo    
    ,ISNULL(DMCA.Numero_Remesa_Electronico,0) AS Numero_Remesa_Electronico    
   
 FROM    
  Encabezado_Remesas ENRE    
    
  INNER JOIN Tipo_Documentos TIDO    
  ON ENRE.EMPR_Codigo = TIDO.EMPR_Codigo AND ENRE.TIDO_Codigo = TIDO.Codigo    
    
  INNER JOIN V_Tipo_Remesas TIRE    
  ON ENRE.EMPR_Codigo = TIRE.EMPR_Codigo AND ENRE.CATA_TIRE_Codigo = TIRE.Codigo    
    
  INNER JOIN Rutas RUTA    
  ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo AND ENRE.RUTA_Codigo = RUTA.Codigo    
    
  INNER JOIN Producto_Transportados PRTR    
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo AND ENRE.PRTR_Codigo = PRTR.Codigo    
    
  INNER JOIN Terceros CLIE    
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo    
    
  INNER JOIN Terceros REMI    
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo AND ENRE.TERC_Codigo_Remitente = REMI.Codigo    
    
  INNER JOIN Ciudades CIRE    
  ON ENRE.EMPR_Codigo = CIRE.EMPR_Codigo AND ENRE.CIUD_Codigo_Remitente = CIRE.Codigo    
    
  INNER JOIN Terceros DEST    
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo    
    
  INNER JOIN Ciudades CIDE    
  ON ENRE.EMPR_Codigo = CIDE.EMPR_Codigo AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo    
    
  INNER JOIN Usuarios USCR    
  ON ENRE.EMPR_Codigo = USCR.EMPR_Codigo AND ENRE.USUA_Codigo_Crea = USCR.Codigo    
    
  INNER JOIN Oficinas OFIC    
  ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo AND ENRE.OFIC_Codigo = OFIC.Codigo    
    
     
  INNER JOIN Terceros COND    
  ON ENRE.EMPR_Codigo = COND.EMPR_Codigo AND ENRE.TERC_Codigo_Conductor = COND.Codigo    
    
    
  --INNER JOIN Detalle_Tarifario_Carga_Ventas DTCV    
  --ON ENRE.EMPR_Codigo = DTCV.EMPR_Codigo AND ENRE.DTCV_Codigo = DTCV.Codigo    
    
  INNER JOIN Detalle_Despacho_Orden_Servicios DTCV ON  
  ENRE.EMPR_Codigo = DTCV.EMPR_Codigo   
  AND ENRE.DTCV_Codigo = DTCV.ID  
    
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS    
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo AND ENRE.ESOS_Numero = ESOS.Numero    
   
 -----------  
  
  LEFT JOIN V_Tipo_Tarifa_Transporte_Carga TTTC    
  ON DTCV.EMPR_Codigo = TTTC.EMPR_Codigo AND DTCV.TTTC_Codigo_Venta = TTTC.Codigo    
  
  LEFT JOIN Vehiculos VEHI    
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo AND ENRE.VEHI_Codigo = VEHI.Codigo    
  
  --  
  LEFT JOIN Terceros TENE    
  ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo     
  
  LEFT JOIN V_Tipo_vehiculos TIVE    
  ON VEHI.EMPR_Codigo = TIVE.EMPR_Codigo AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo    
  --  
  
  LEFT JOIN Semirremolques SEMI    
  ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo AND ENRE.SEMI_Codigo = SEMI.Codigo    
  
  LEFT JOIN Detalle_Manifiesto_Carga DMCA    
  ON ENRE.EMPR_Codigo = DMCA.EMPR_Codigo AND ENRE.Numero = DMCA.ENRE_Numero    
    
  LEFT JOIN Encabezado_Manifiesto_Carga ENMC    
  ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo AND ENRE.ENMC_Numero = ENMC.Numero    
    
  LEFT JOIN Encabezado_Orden_Cargues ENOC    
  ON ENRE.EMPR_Codigo = ENOC.EMPR_Codigo AND ENRE.ENOC_Numero = ENOC.Numero    
      
  LEFT JOIN Encabezado_Planilla_Despachos ENPD    
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo AND ENRE.ENPD_Numero = ENPD.Numero    
    
 WHERE    
  ENRE.EMPR_Codigo = @par_EMPR_Codigo    
  AND ENRE.Numero = @par_Numero    
END    
GO