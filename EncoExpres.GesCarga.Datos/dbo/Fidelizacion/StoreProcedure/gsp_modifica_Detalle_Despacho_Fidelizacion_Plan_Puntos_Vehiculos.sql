﻿PRINT 'gsp_modifica_Detalle_Despacho_Fidelizacion_Plan_Puntos_Vehiculos'
GO
DROP PROCEDURE gsp_modifica_Detalle_Despacho_Fidelizacion_Plan_Puntos_Vehiculos 
GO
CREATE PROCEDURE gsp_modifica_Detalle_Despacho_Fidelizacion_Plan_Puntos_Vehiculos 
(
	@par_EMPR_Codigo smallint,
	@par_Codigo numeric,
	@par_Vehi_Codigo numeric,
	@par_Fecha_Inicio_Vigencia varchar(20),
	@par_Fecha_Fin_Vigencia varchar(20),
	@par_USUA_Codigo_Modifica numeric,
	@par_Estado smallint
)
AS
BEGIN

	Update Detalle_Despachos_Plan_Puntos_Vehiculos SET
	Fecha_Inicio = @par_Fecha_Inicio_Vigencia,
	Fecha_Vence = @par_Fecha_Fin_Vigencia,
	USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,
	Estado = @par_Estado,
	Fecha_Modifica = GETDATE()

	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND VEHI_Codigo = @par_Vehi_Codigo
	AND ID = @par_Codigo
	
	SELECT ID as Codigo FROM Detalle_Despachos_Plan_Puntos_Vehiculos
	WHERE EMPR_Codigo = @par_EMPR_Codigo      
	AND VEHI_Codigo = @par_Vehi_Codigo
	AND ID = @par_Codigo

END 
GO