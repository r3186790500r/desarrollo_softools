﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Negocio.Basico.Tesoreria

''' <summary>
''' Controlador <see cref="PlanUnicoCuentasController"/>
''' </summary>
<Authorize>
Public Class PlanUnicoCuentasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PlanUnicoCuentas) As Respuesta(Of IEnumerable(Of PlanUnicoCuentas))
        Return New LogicaPlanUnicoCuentas(CapaPersistenciaPlanUnicoCuentas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PlanUnicoCuentas) As Respuesta(Of PlanUnicoCuentas)
        Return New LogicaPlanUnicoCuentas(CapaPersistenciaPlanUnicoCuentas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PlanUnicoCuentas) As Respuesta(Of Long)
        Return New LogicaPlanUnicoCuentas(CapaPersistenciaPlanUnicoCuentas).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PlanUnicoCuentas) As Respuesta(Of Boolean)
        Return New LogicaPlanUnicoCuentas(CapaPersistenciaPlanUnicoCuentas).Anular(entidad)
    End Function
End Class
