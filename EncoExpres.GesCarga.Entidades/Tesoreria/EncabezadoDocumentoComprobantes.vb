﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos

Namespace Tesoreria

    ''' <summary>
    ''' Clase <see cref="EncabezadoDocumentoComprobantes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoDocumentoComprobantes
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="EncabezadoDocumentoComprobantes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoDocumentoComprobantes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Numero = Read(lector, "Numero")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Fecha = Read(lector, "Fecha")
            Observaciones = Read(lector, "Observaciones")
            NumeroDocumentoOrigen = Read(lector, "Documento_Origen")
            Anulado = Read(lector, "Anulado")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            CentroCostos = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CCCE_Codigo")}
            If Obtener = 0 Then
                DocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOOR_Codigo"), .Nombre = Read(lector, "DocumentoOrigen")}
                OficinaDestino = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Destino"), .Nombre = Read(lector, "Oficina")}
                Tercero = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Titular"), .Nombre = Read(lector, "NombreTercero")}
                Beneficiario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Beneficiario"), .Nombre = Read(lector, "NombreBeneficiario")}
                TotalRegistros = Read(lector, "TotalRegistros")
                ValorPagoTotal = Read(lector, "Valor_Pago_Recaudo_Total")
            Else
                DocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOOR_Codigo")}
                DestinoIngreso = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DEIN_Codigo")}
                FormaPagoDocumento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FPDC_Codigo")}
                OficinaDestino = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Destino")}
                Tercero = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Titular"), .NumeroIdentificacion = Read(lector, "IdentTercero"), .NombreCompleto = Read(lector, "NombreTercero")}
                Caja = New Cajas With {.Codigo = Read(lector, "CAJA_Codigo")}
                CuentaBancaria = New CuentaBancarias With {.Codigo = Read(lector, "CUBA_Codigo"), .Nombre = Read(lector, "NombreCuentaBancaria")}
                Beneficiario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Beneficiario"), .NumeroIdentificacion = Read(lector, "IdentBeneficiario"), .NombreCompleto = Read(lector, "NombreBeneficiario")}
                NumeroPagoRecaudo = Read(lector, "Numero_Pago_Recaudo")
                FechaPagoRecaudo = Read(lector, "Fecha_Pago_Recaudo")
                ValorPagocheque = Read(lector, "Valor_Pago_Recaudo_Cheque")
                ValorPagoEfectivo = Read(lector, "Valor_Pago_Recaudo_Efectivo")
                ValorPagoTransferencia = Read(lector, "Valor_Pago_Recaudo_Transferencia")
                ValorPagoTotal = Read(lector, "Valor_Pago_Recaudo_Total")
                Numeracion = Read(lector, "Numeracion")
                GeneraConsignacion = Read(lector, "Genero_Consignacion")
                ValorAlterno = Read(lector, "Valor_Alterno")
                ConceptoContable = Read(lector, "ECCO_Codigo")
                Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}

            End If
            ValorPagoTransferencia = Read(lector, "Valor_Pago_Recaudo_Transferencia")
            FechaPagoRecaudo = Read(lector, "Fecha_Pago_Recaudo")
            Banco = New Bancos With {.Codigo = Read(lector, "BANC_Codigo_Transferencia")}
            TipoCuentaBancaria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICB_Codigo_Transferencia")}
            NumeroCuentaBancaria = Read(lector, "Numero_Cuenta_Transferencia")
            CuentaBancaria = New CuentaBancarias With {.Codigo = Read(lector, "CUBA_Codigo"), .Nombre = Read(lector, "NombreCuentaBancaria")}
            MensajeProcesoTransferencia = Read(lector, "Mensaje_Proceso_Tranferencia")
        End Sub

        <JsonProperty>
        Public Property CadenaEgresos As String

        <JsonProperty>
        Public Property Comprobantes As IEnumerable(Of EncabezadoDocumentoComprobantes)

        <JsonProperty>
        Public Property MensajeProcesoTransferencia As String
        <JsonProperty>
        Public Property Banco As Bancos

        <JsonProperty>
        Public Property TipoCuentaBancaria As ValorCatalogos
        <JsonProperty>
        Public Property NumeroCuentaBancaria As String
        <JsonProperty>
        Public Property ValorAnticipoPlanilla As Long
        <JsonProperty>
        Public Property CodigoCuentaPUC As Integer
        Public Overloads Property UsuarioModifica As SeguridadUsuarios.Usuarios
        <JsonProperty>
        Public Overloads Property UsuarioCrea As SeguridadUsuarios.Usuarios

        <JsonProperty>
        Public Property Placa As Vehiculos
        <JsonProperty>
        Public Property Diferencia As Long
        <JsonProperty>
        Public Property Debito As Long

        <JsonProperty>
        Public Property Credito As Long
        <JsonProperty>
        Public Property CuentaBancariaCheque As CuentaBancarias
        <JsonProperty>
        Public Property CuentaBancariaConsignacion As CuentaBancarias
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Tercero As Terceros
        <JsonProperty>
        Public Property Beneficiario As Terceros
        <JsonProperty>
        Public Property NumeroConsignacion As Integer
        <JsonProperty>
        Public Property NumeroDocumentoOrigen As Integer
        <JsonProperty>
        Public Property Caja As Cajas
        <JsonProperty>
        Public Property CuentaBancaria As CuentaBancarias
        <JsonProperty>
        Public Property NumeroPagoRecaudo As Integer

        <JsonProperty>
        Public Property FechaConsignacion As DateTime
        <JsonProperty>
        Public Property FechaPagoRecaudo As DateTime
        <JsonProperty>
        Public Property ValorPagoRecaudo As Double
        <JsonProperty>
        Public Property ValorPagoTransferencia As Double
        <JsonProperty>
        Public Property ValorPagoEfectivo As Double
        <JsonProperty>
        Public Property ValorPagocheque As Double
        <JsonProperty>
        Public Property ValorPagoTotal As Double
        <JsonProperty>
        Public Property Numeracion As String
        <JsonProperty>
        Public Property Oficina As Oficinas
        <JsonProperty>
        Public Property OficinaCrea As Oficinas
        <JsonProperty>
        Public Property OficinaDestino As Oficinas
        <JsonProperty>
        Public Property GeneraConsignacion As Short
        <JsonProperty>
        Public Property DocumentoOrigen As ValorCatalogos
        <JsonProperty>
        Public Property FormaPagoRecaudo As ValorCatalogos
        <JsonProperty>
        Public Property FormaPagoDocumento As ValorCatalogos
        <JsonProperty>
        Public Property DestinoIngreso As ValorCatalogos
        <JsonProperty>
        Public Property ValorAlterno As Double
        <JsonProperty>
        Public Property Fuente As String
        <JsonProperty>
        Public Property FuenteAnula As String
        <JsonProperty>
        Public Property ConceptoContable As Integer

        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleDocumentoComprobantes)
        <JsonProperty>
        Public Property Cuentas As IEnumerable(Of EncabezadoDocumentoCuentas)
        <JsonProperty>
        Public Property DetalleCuentas As IEnumerable(Of DetalleDocumentoCuentaComprobantes)
        <JsonProperty>
        Public Property NombreTercero As String
        <JsonProperty>
        Public Property NombreBeneficiario As String
        <JsonProperty>
        Public Property Autorizacion As Short
        <JsonProperty>
        Public Property CodigoTercero As Integer
        <JsonProperty>
        Public Property CodigoLegalizacionGastos As Integer
        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)

        <JsonProperty>
        Public Property CentroCostos As ValorCatalogos
        <JsonProperty>
        Public Property Ruta As String

    End Class

End Namespace
