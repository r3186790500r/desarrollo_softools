﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Negocio.Facturacion

''' <summary>
''' Controlador <see cref="ImpuestoFacturasController"/>
''' </summary>

<Authorize>
Public Class ImpuestoFacturasController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ImpuestoFacturas) As Respuesta(Of IEnumerable(Of ImpuestoFacturas))
        Return New LogicaImpuestoFacturas(CapaPersistenciaImpuestoFacturas).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ImpuestoFacturas) As Respuesta(Of ImpuestoFacturas)
        Return New LogicaImpuestoFacturas(CapaPersistenciaImpuestoFacturas).Obtener(filtro)
    End Function
End Class
