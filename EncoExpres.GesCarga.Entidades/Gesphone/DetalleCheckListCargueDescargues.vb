﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General


Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="DetalleCheckListCargueDescargues"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleCheckListCargueDescargues
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleCheckListCargueDescargues"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleCheckListCargueDescargues"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "ID")
            NumeroDocumento = Read(lector, "ECCD_Numero")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            Nombre = Read(lector, "Nombre")
            OrdenFormulario = Read(lector, "Orden_Formulario")
            TipoPregunta = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TPCL_Codigo")}
            Valor = Read(lector, "Valor")
        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property OrdenFormulario As Integer
        <JsonProperty>
        Public Property Valor As String
        <JsonProperty>
        Public Property TipoPregunta As ValorCatalogos
    End Class
End Namespace
