﻿Imports Newtonsoft.Json

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="GrupoUsuarios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class GrupoUsuarios
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="GrupoUsuarios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="GrupoUsuarios"/>
        ''' </summary>
        ''' <param name="lector">Objeto datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Obtener = Read(lector, "Obtener")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoGrupo = Read(lector, "Codigo_Grupo")


            If Obtener = 0 Then 'Consultar
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
        End Sub


        ''' <summary>
        ''' Obtiene o establece el codigo del grupo seguridad
        ''' </summary>
        <JsonProperty>
        Public Property CodigoGrupo As String

        ''' <summary>
        ''' Obtiene o establece el nombre del grupo seguridad
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String


        <JsonProperty>
        Public Property CodigoUsuario As Integer

    End Class
End Namespace
