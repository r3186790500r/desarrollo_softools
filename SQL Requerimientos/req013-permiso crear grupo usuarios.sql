USE [GESCARGA50_DESA]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Permiso_Grupo_Usuarios]
ADD Crear smallint not null default 0;
GO

update [dbo].[Permiso_Grupo_Usuarios] set Crear = 1 where Actualizar = 1;
GO 

ALTER TABLE [dbo].[Menu_Aplicaciones]
ADD Aplica_Crear smallint not null default 0;
GO

update [dbo].[Menu_Aplicaciones] set Aplica_Crear = 1 where Aplica_Actualizar = 1;
GO 

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_menu_aplicaciones]  
(  
@par_EMPR_Codigo SMALLINT,  
@par_USUA_Codigo INT   
)  
AS  
BEGIN  
SELECT   
MOAP.EMPR_Codigo,  
MEAP.Codigo AS CodigoMenu,  
MEAP.MOAP_Codigo,  
MEAP.Nombre AS NombreMenu,  
MEAP.Url_Pagina,  
MEAP.Mostrar_Menu,  
MEAP.Padre_Menu,  
MEAP.Aplica_Habilitar,  
MEAP.Aplica_Consultar,  
MEAP.Aplica_Actualizar,  
MEAP.Aplica_Crear,  
MEAP.Aplica_Eliminar_Anular,  
MEAP.Aplica_Imprimir,  
MEAP.Aplica_Contabilidad,  
MEAP.Opcion_Permiso AS OpcionPermiso,  
MEAP.Opcion_Listado AS OpcionListado,  
MOAP.Estado AS EstadoModulo,   
MOAP.Nombre AS NombreModulo,  
MOAP.CATA_APLI_Codigo,  
CASE WHEN SUM(PGUS.Consultar) >= 1 THEN 1 ELSE 0 END AS Consultar,   
CASE WHEN SUM(PGUS.Actualizar) >= 1 THEN 1 ELSE 0 END AS Actualizar,   
CASE WHEN SUM(PGUS.Crear) >= 1 THEN 1 ELSE 0 END AS Crear,   
CASE WHEN SUM(PGUS.Eliminar_Anular) >= 1 THEN 1 ELSE 0 END AS Eliminar_Anular,   
CASE WHEN SUM(PGUS.Imprimir) >= 1 THEN 1 ELSE 0 END AS Imprimir,   
CASE WHEN SUM(PGUS.Habilitar) >= 1 THEN 1 ELSE 0 END AS Habilitar,  
CASE WHEN SUM(PGUS.Contabilidad) >= 1 THEN 1 ELSE 0 END AS Contabilidad  
  
  
FROM  Menu_Aplicaciones AS MEAP,  
Modulo_Aplicaciones AS MOAP,  
Usuario_Grupo_Usuarios AS USGU,  
Permiso_Grupo_Usuarios AS PGUS  
  
WHERE   
MEAP.EMPR_Codigo =@par_EMPR_Codigo  
AND USGU.USUA_Codigo = @par_USUA_Codigo  
AND MEAP.Aplica_Habilitar = 1  
AND MOAP.Estado = 1  
AND PGUS.Habilitar = 1  

AND MEAP.EMPR_Codigo = MOAP.EMPR_Codigo  
AND MEAP.MOAP_Codigo = MOAP.Codigo   
  
AND USGU.EMPR_Codigo = PGUS.EMPR_Codigo  
AND USGU.GRUS_Codigo = PGUS.GRUS_Codigo  
  
AND PGUS.EMPR_Codigo =MEAP.EMPR_Codigo   
AND PGUS.MEAP_Codigo = MEAP.Codigo   
  
GROUP BY MOAP.EMPR_Codigo,  
MEAP.Codigo,  
MEAP.MOAP_Codigo,  
MEAP.Nombre,  
MEAP.Url_Pagina,  
MEAP.Mostrar_Menu,  
MEAP.Padre_Menu,  
MEAP.Aplica_Habilitar,  
MEAP.Aplica_Consultar,  
MEAP.Aplica_Actualizar, 
MEAP.Aplica_Crear,  
MEAP.Aplica_Eliminar_Anular,  
MEAP.Aplica_Imprimir,  
MEAP.Aplica_Contabilidad,  
MEAP.Opcion_Permiso,  
MEAP.Opcion_Listado,  
MEAP.Orden_Menu,  
MOAP.Estado,   
MOAP.CATA_APLI_Codigo,  
MOAP.Nombre  
  
ORDER BY MEAP.Orden_Menu  
END  
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_permiso_grupo_usuarios]
(
@par_EMPR_Codigo SMALLINT,
@par_MEAP_Codigo NUMERIC = NULL,
@par_GRUS_Codigo NUMERIC = NULL
)
AS
BEGIN

SELECT 
EMPR_Codigo,
MEAP_Codigo,
GRUS_Codigo,
Habilitar,
Consultar,
Actualizar,
Crear,
Eliminar_Anular,
Imprimir,
Permiso,
Contabilidad
FROM
Permiso_Grupo_Usuarios
WHERE 

EMPR_Codigo = @par_EMPR_Codigo
AND MEAP_Codigo = ISNULL(@par_MEAP_Codigo,MEAP_Codigo)
AND GRUS_Codigo = ISNULL(@par_GRUS_Codigo,GRUS_Codigo)

END 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_permiso_grupos]
(            
	@par_EMPR_Codigo SMALLINT,            
	@par_GRUS_Codigo SMALLINT = NULL,            
	@par_MEAP_Codigo INT = NULL,            
	@par_Padre_Menu NUMERIC = NULL            
)            
AS            
BEGIN            
 SELECT PEGU.EMPR_Codigo       
    ,PEGU.MEAP_Codigo            
    ,MEAP.Nombre As NombreMenu            
    ,MEAP.Padre_Menu            
    ,PEGU.GRUS_Codigo            
    ,MEAP.MOAP_Codigo            
	,MEAP.Mostrar_Menu
	,MEAP.Opcion_Listado
    ,PEGU.Habilitar            
    ,PEGU.Consultar            
    ,PEGU.Actualizar
	,PEGU.Crear            
    ,PEGU.Eliminar_Anular            
    ,PEGU.Imprimir            
    ,PEGU.Permiso            
    ,PEGU.Contabilidad           
 ,CASE WHEN (select COUNT(*) from Menu_Aplicaciones where Padre_Menu = @par_MEAP_Codigo AND Mostrar_Menu = 1) > 0 THEN 1 ELSE 0 END AS Padre          
   FROM Permiso_Grupo_Usuarios AS PEGU            
       ,Menu_Aplicaciones AS MEAP            
 WHERE        
     PEGU.EMPR_Codigo = @par_EMPR_Codigo      
    and   PEGU.EMPR_Codigo = MEAP.EMPR_Codigo            
   AND PEGU.MEAP_Codigo = MEAP.Codigo            
            
   AND PEGU.GRUS_Codigo = ISNULL(@par_GRUS_Codigo, PEGU.GRUS_Codigo)            
   AND PEGU.MEAP_Codigo = ISNULL(@par_MEAP_Codigo, PEGU.MEAP_Codigo)            
   AND MEAP.Padre_Menu = ISNULL(@par_Padre_Menu, MEAP.Padre_Menu)            
   --AND CASE WHEN MEAP.Opcion_Permiso = 1 THEN 1 ELSE MEAP.Mostrar_Menu END = 1    
   --AND CASE WHEN MEAP.Opcion_Listado = 1 THEN 1 ELSE MEAP.Mostrar_Menu END = 1    
            
 ORDER BY MEAP.Padre_Menu, MEAP.Orden_Menu      
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_grupo_usuarios]  
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo_Grupo VARCHAR(50),  
@par_Nombre VARCHAR (20),  
@par_USUA_Codigo_Crea SMALLINT  
)  
AS  
BEGIN  
INSERT INTO Grupo_Usuarios  
(  
EMPR_Codigo,  
Codigo_Grupo,  
Nombre,  
USUA_Codigo_Crea,  
Fecha_Crea  
)  
VALUES  
(  
@par_EMPR_Codigo,  
@par_Codigo_Grupo,  
@par_Nombre,  
@par_USUA_Codigo_Crea,  
GETDATE()  
)



INSERT INTO 
 Permiso_Grupo_Usuarios  
 SELECT EMPR_Codigo  
 ,Codigo  
 ,@@IDENTITY  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0
 ,0
 FROM Menu_Aplicaciones   
 WHERE EMPR_Codigo = @par_EMPR_Codigo  

SELECT @@ROWCOUNT AS Codigo  
  
END  
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_permiso_grupo_usuarios]
(
@par_EMPR_Codigo SMALLINT,
@par_MEAP_Codigo NUMERIC,
@par_GRUS_Codigo SMALLINT,
@par_Habilitar SMALLINT,
@par_Consultar SMALLINT,
@par_Actualizar SMALLINT,
@par_Crear SMALLINT,
@par_Eliminar_Anular SMALLINT,
@par_Imprimir SMALLINT,
@par_Permiso SMALLINT,
@par_Contabilidad SMALLINT
)
AS
BEGIN

INSERT INTO Permiso_Grupo_Usuarios
(
EMPR_Codigo,
MEAP_Codigo,
GRUS_Codigo,
Habilitar,
Consultar,
Actualizar,
Crear,
Eliminar_Anular,
Imprimir,
Permiso,
Contabilidad
)
VALUES 
(
@par_EMPR_Codigo,
@par_MEAP_Codigo,
@par_GRUS_Codigo,
@par_Habilitar,
@par_Consultar,
@par_Actualizar,
@par_Crear,
@par_Eliminar_Anular,
@par_Imprimir,
@par_Permiso,
@par_Contabilidad
)

SELECT @@ROWCOUNT AS Codigo 

END 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_modificar_permiso_grupo_usuarios]
(
@par_EMPR_Codigo SMALLINT,
@par_MEAP_Codigo NUMERIC,
@par_GRUS_Codigo SMALLINT,
@par_Habilitar SMALLINT,
@par_Consultar SMALLINT,
@par_Actualizar SMALLINT,
@par_Crear SMALLINT,
@par_Eliminar_Anular SMALLINT,
@par_Imprimir SMALLINT,
@par_Permiso SMALLINT,
@par_Contabilidad SMALLINT
)
AS
BEGIN
UPDATE Permiso_Grupo_Usuarios SET 
Habilitar = @par_Habilitar,
Consultar = @par_Consultar,
Actualizar = @par_Actualizar,
Crear = @par_Crear,
Eliminar_Anular = @par_Eliminar_Anular,
Imprimir = @par_Imprimir,
Permiso = @par_Permiso,
Contabilidad = @par_Contabilidad
WHERE 
EMPR_Codigo = @par_EMPR_Codigo
AND MEAP_Codigo = @par_MEAP_Codigo
AND GRUS_Codigo = @par_GRUS_Codigo

SELECT @@ROWCOUNT AS Codigo

END 
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_modificar_permiso_grupos]
(  
	@par_EMPR_Codigo SMALLINT,  
	@par_GRUS_Codigo SMALLINT,  
	@par_MEAP_Codigo INT,  
	@par_Habilitar SMALLINT,  
	@par_Consultar SMALLINT,  
	@par_Actualizar SMALLINT,  
	@par_Crear SMALLINT, 
	@par_Eliminar_Anular SMALLINT,  
	@par_Imprimir SMALLINT  
)  
AS  
BEGIN
	IF EXISTS(
		SELECT MEAP_Codigo from Permiso_Grupo_Usuarios
		WHERE EMPR_Codigo = @par_EMPR_Codigo  
		AND MEAP_Codigo = @par_MEAP_Codigo  
		AND GRUS_Codigo = @par_GRUS_Codigo 
	)
	BEGIN
		UPDATE Permiso_Grupo_Usuarios  
		SET Habilitar = @par_Habilitar  
		,Consultar = @par_Consultar  
		,Actualizar = @par_Actualizar  
		,Crear = @par_Crear  
		,Eliminar_Anular = @par_Eliminar_Anular  
		,Imprimir = @par_Imprimir  
		WHERE EMPR_Codigo = @par_EMPR_Codigo  
		AND MEAP_Codigo = @par_MEAP_Codigo  
		AND GRUS_Codigo = @par_GRUS_Codigo  
	END
	ELSE
	BEGIN
		INSERT INTO Permiso_Grupo_Usuarios(
		EMPR_Codigo,
		MEAP_Codigo,
		GRUS_Codigo,
		Habilitar,
		Consultar,
		Actualizar,
		Crear,
		Eliminar_Anular,
		Imprimir,
		Permiso,
		Contabilidad
		)
		VALUES(
			@par_EMPR_Codigo,
			@par_MEAP_Codigo,
			@par_GRUS_Codigo,
			@par_Habilitar,
			@par_Consultar,
			@par_Actualizar,
			@par_Crear,
			@par_Eliminar_Anular,
			@par_Imprimir,
			0,
			0
		)
	END
END
GO

