﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="ProductosMinisterioTransporteController"/>
''' </summary>
<Authorize>
Public Class ProductosMinisterioTransporteController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ProductosMinisterioTransporte) As Respuesta(Of IEnumerable(Of ProductosMinisterioTransporte))
        Return New LogicaProductosMinisterioTransporte(CapaPersistenciaProductosMinisterioTransporte).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ProductosMinisterioTransporte) As Respuesta(Of ProductosMinisterioTransporte)
        Return New LogicaProductosMinisterioTransporte(CapaPersistenciaProductosMinisterioTransporte).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ProductosMinisterioTransporte) As Respuesta(Of Long)
        Return New LogicaProductosMinisterioTransporte(CapaPersistenciaProductosMinisterioTransporte).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ProductosMinisterioTransporte) As Respuesta(Of Boolean)
        Return New LogicaProductosMinisterioTransporte(CapaPersistenciaProductosMinisterioTransporte).Anular(entidad)
    End Function
End Class
