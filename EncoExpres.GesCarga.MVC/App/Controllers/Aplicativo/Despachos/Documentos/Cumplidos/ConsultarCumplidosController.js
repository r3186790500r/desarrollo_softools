﻿EncoExpresApp.controller("ConsultarCumplidosCtrl", ['$scope', '$timeout', 'CumplidosFactory', 'TercerosFactory', 'EmpresasFactory', '$linq', 'blockUI', '$routeParams', 'OficinasFactory', 'NovedadesDespachosFactory', 'DetallePlanillaDespachosFactory', 'DetalleNovedadesDespachosFactory', 'ValorCatalogosFactory', 'LiquidacionesFactory',
    function ($scope, $timeout, CumplidosFactory, TercerosFactory, EmpresasFactory, $linq, blockUI, $routeParams, OficinasFactory, NovedadesDespachosFactory, DetallePlanillaDespachosFactory, DetalleNovedadesDespachosFactory, ValorCatalogosFactory, LiquidacionesFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Documentos' }, { Nombre: 'Cumplido Planilla Despacho' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.MostrarMensajeError = false
        $scope.NumeroAnular;
        $scope.NumeroDocumentoAnular;
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoOficinas = [];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CUMPLIDOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarAgregarNovedad = false;

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Novedades = {
            NumeroPlanilla: 0,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaFinal = new Date();
        }
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarCumplido';
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                if (DatosRequeridos()) {
                    $scope.Codigo = 0
                    Find()
                }
            }
        };


        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        //Cargar combo de novedades despacho
        NovedadesDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListadoNovedadesDespachos = response.data.Datos;
                        $scope.Novedades.Novedad = $scope.ListadoNovedadesDespachos[$scope.ListadoNovedadesDespachos.length - 1];
                    } else {
                        $scope.ListadoNovedadesDespachos = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de Proveedores*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_PROVEEDOR }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaProveedores = response.data.Datos;
                        $scope.Novedades.Proveedor = $scope.ListaProveedores[-1];
                    } else {
                        $scope.ListaProveedores = '';
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO_LUGAR } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRecaudo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoRecaudo = response.data.Datos;
                        $scope.ModalRecaudo = $scope.ListadoTipoRecaudo[0];
                    }
                    else {
                        $scope.ListadoTipoRecaudo = []
                    }
                }
            }, function (response) {
            });
        $scope.ListadoOficinas = []
        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.ModeloOficina = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '')
                && ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '')
                && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
            }
            return continuar
        }
        function Find() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.ModeloNumero,
                FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaInicial,
                ModeloFechaFinalal: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloModeloFechaFinalal,
                NumeroPlanilla: $scope.ModeloPlanilla,
                NumeroPlanillaFinal: $scope.ModeloPlanillaFinal,
                NumeroManifiesto: $scope.ModeloManifiesto,
                Vehiculo: { Placa: $scope.ModeloPlaca },
                Conductor: { Nombre: $scope.ModeloConductor },
                Tenedor: { Nombre: $scope.ModeloTenedor },
                Oficina: $scope.ModeloOficina,
                Estado: $scope.ModeloEstado.Codigo,
                Pagina: $scope.paginaActual,
                Numero: 0,
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas
            };
            //console.log("filtro: ", filtros);
            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoCumplidos = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    blockUI.delay = 1000;
                    $scope.Pagina = $scope.paginaActual
                    $scope.RegistrosPagina = 10
                    CumplidosFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Numero = 0
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoCumplidos = response.data.Datos
                                    //console.log("Listado Cumplidos: ", $scope.ListadoCumplidos);
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                /*var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });*/
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };

        $scope.Anular = function (Numero, NumeroDocumento) {
            $scope.MensajesErrorAnula = [];
            $scope.ModeloCausaAnula = '';
            $scope.NumeroAnular = Numero;
            $scope.NumeroDocumentoAnular = NumeroDocumento;
            showModal('modalAnular')
        }
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        $scope.ConfirmaAnular = function () {
            if ($scope.ModeloCausaAnula != undefined && $scope.ModeloCausaAnula != null && $scope.ModeloCausaAnula != '') {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.NumeroAnular,
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnula: $scope.ModeloCausaAnula
                }
                CumplidosFactory.Anular(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló el cumplido' + $scope.NumeroDocumentoAnular);
                                closeModal('modalAnular');
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular el cumplido ya que se encuentra relacionado con los siguientes documentos: '

                                var Liquidaciones = '\nLiquidaciones: '
                                for (var i = 0; i < response.data.Datos.DocumentosRelacionados.length; i++) {
                                    if (response.data.Datos.DocumentosRelacionados[i].Liquidacion.Numero > 0) {
                                        Liquidaciones += response.data.Datos.DocumentosRelacionados[i].Liquidacion.Numero + ','
                                        aplicaLiquidaciones = true
                                    }
                                }

                                if (aplicaLiquidaciones) {
                                    mensaje += Liquidaciones
                                }
                                ShowError(mensaje)
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.MensajesErrorAnula.push('Debe ingresar la causa de anulación');
            }
        }

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_CUMPLIDO;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }

        }
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        };
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            if ($routeParams.Numero > 0) {
                $scope.ModeloNumero = $routeParams.Numero;
                Find();
            }
        }

        //funcion que muestra la modal novedades 
        $scope.AbrirNovedades = function (item) {
            //$scope.DeshabilitarGuardar(item);
            $scope.DesabilitarBotonGuardarNovedad = false;

            $scope.DeshabilitarNumeroRemesa = true;
            $scope.DeshabilitarValorCliente = true;
            $scope.DeshabilitarValorCompra = true;
            $scope.NumeroDocumentoPlanilla = item.NumeroPlanilla
            $scope.Novedades.NumeroPlanilla = item.CodigoPlanilla
            $scope.ResultadoSinRegistrosNovedad = '';

            //Cargar combo de numeros remesas de planilla
            $scope.Remesa = [];
            $scope.ListadoNumerosRemesas = [];
            DetallePlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroPlanilla: $scope.Novedades.NumeroPlanilla }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNumerosRemesas = response.data.Datos;
                            $scope.Remesa = $scope.ListadoNumerosRemesas[-1];
                            $scope.CodigoLiquidacionPlanillaDetalle = $scope.ListadoNumerosRemesas[0].NumeroLiquidaPlanilla;
                            //console.log("Liquidación: ", $scope.CodigoLiquidacionPlanillaDetalle);
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            //funcion que consulta nueva novedad
            $scope.ListaNovedades = [];
            var filtroNovedades = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroPlanilla: $scope.Novedades.NumeroPlanilla
            }
            DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaNovedades = response.data.Datos
                        } else {
                            $scope.ResultadoSinRegistrosNovedad = 'No hay datos para mostrar';
                        }
                    }
                }, function (response) {
                });
            showModal('modalNovedades')
            $('#NuevaNovedad').hide(); $('#BotonNuevo').show()
        }

        //funcion que validad si la remesa ingresada se encuentra facturada, para permitir o no el ingreso del campo valor cliente
        $scope.VerificarNumeroFactura = function (remesa) {
            if (remesa !== "") {
                if (remesa.NumeroRemesa == undefined || remesa.NumeroRemesa == null || remesa.NumeroRemesa == '') {
                    $scope.Remesa = ''
                } else {
                    if (remesa.NumeroFacturaRemesa > 0) {
                        $scope.Remesa = '';
                        $scope.Novedades.ValorVenta = '';
                        $scope.DeshabilitarValorCliente = true;
                        ShowError('El número de remesa ingresado ya se encuentra facturado')
                    }
                }
            }
        }

        //funcion que validad la remesa ingresada
        $scope.CargarValidacionesNovedad = function (novedad) {
            if (novedad.Codigo !== undefined && novedad.Codigo !== 0) {
                $scope.DeshabilitarNumeroRemesa = false;
                $scope.DeshabilitarValorCompra = false;
                $scope.DeshabilitarValorCliente = false;
                $scope.CodigoFacturacion = '';
                $scope.CodigoLiquidacionPlanilla = '';
                filtrosObtener = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: novedad.Codigo,
                };

                blockUI.delay = 1000;
                NovedadesDespachosFactory.Obtener(filtrosObtener).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.CodigoNovedadConsultada = response.data.Datos.Codigo;
                            $scope.CodigoFacturacion = response.data.Datos.Conceptos.COVECodigo
                            $scope.CodigoLiquidacionPlanilla = response.data.Datos.Conceptos.CLPDCodigo
                        }
                        //--Transportador
                        if ($scope.CodigoLiquidacionPlanilla != 0) {
                            $scope.DeshabilitarValorCompra = false;
                            if ($scope.CodigoLiquidacionPlanillaDetalle > 0) {
                                $scope.DeshabilitarValorCompra = true;
                            }
                        }
                        else {
                            $scope.DeshabilitarValorCompra = true;
                        }
                        //--Cliente
                        if ($scope.CodigoFacturacion != 0) {
                            $scope.DeshabilitarValorCliente = false;
                        }
                        else {
                            $scope.DeshabilitarValorCliente = true;
                        }
                        /*if ($scope.CodigoLiquidacionPlanilla != 0 && $scope.CodigoFacturacion != 0) {
                            $scope.DeshabilitarNumeroRemesa = false;
                            $scope.DeshabilitarValorCompra = false;
                            $scope.DeshabilitarValorCliente = false;
                        } else {
                            if ($scope.CodigoLiquidacionPlanilla != 0) {
                                $scope.DeshabilitarValorCompra = false;
                            } else {
                                $scope.DeshabilitarValorCompra = true;
                            }
                            if ($scope.CodigoFacturacion != 0) {
                                $scope.DeshabilitarValorCliente = false;
                            } else {
                                $scope.DeshabilitarValorCliente = true;
                            }
                            $scope.DeshabilitarNumeroRemesa = false;
                        }*/
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                blockUI.stop();
            } else {
                $scope.DeshabilitarNumeroRemesa = true;
                $scope.DeshabilitarValorCompra = true;
                $scope.DeshabilitarValorCliente = true;
            }
        }

        //funcion que inserta nueva novedad 
        $scope.InsertarNovedad = function () {
            $scope.ResultadoSinRegistrosNovedad = 'No hay datos para mostrar';
            if (DatosRequeridosModal()) {
                if ($scope.Remesa !== undefined && $scope.Remesa !== '' && $scope.Remesa !== null) {
                    $scope.Novedades.NumeroRemesa = $scope.Remesa.NumeroRemesa
                }
                DetalleNovedadesDespachosFactory.Guardar($scope.Novedades).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $('#NuevaNovedad').hide(500); $('#BotonNuevo').show(500)
                            ShowSuccess('Novedad agregada satisfactoriamente')
                            $scope.Remesa = $scope.ListadoNumerosRemesas[-1];
                            $scope.Novedades.Novedad = $scope.ListadoNovedadesDespachos[$scope.ListadoNovedadesDespachos.length - 1];
                            //funcion que consulta nueva novedad
                            $scope.ListaNovedades = [];
                            var filtroNovedades = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroPlanilla: $scope.Novedades.NumeroPlanilla
                            }
                            DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            $scope.ListaNovedades = response.data.Datos
                                        } else {
                                            $scope.ResultadoSinRegistrosNovedad = 'No hay datos para mostrar';
                                        }
                                    }
                                }, function (response) {
                                });
                            showModal('modalNovedades')
                            $('#NuevaNovedad').hide(); $('#BotonNuevo').show()
                        }
                    }, function (response) {
                    });
            }
        }

        function DatosRequeridosModal() {
            $scope.MensajesErrorModal = [];
            var continuar = true;
            if ($scope.Novedades.Novedad === undefined || $scope.Novedades.Novedad === '' || $scope.Novedades.Novedad === null || $scope.Novedades.Novedad.Codigo === 0) {
                $scope.MensajesErrorModal.push('Debe seleccionar la novedad');
                continuar = false;
            } else {
                if ($scope.CodigoLiquidacionPlanilla > 0 && $scope.CodigoFacturacion > 0) {
                    if (($scope.Novedades.ValorCompra === undefined || $scope.Novedades.ValorCompra === '' || $scope.Novedades.ValorCompra === null || $scope.Novedades.ValorCompra === 0) &&
                        ($scope.Novedades.ValorVenta === undefined || $scope.Novedades.ValorVenta === '' || $scope.Novedades.ValorVenta === null || $scope.Novedades.ValorVenta === 0)) {
                        $scope.MensajesErrorModal.push('Debe ingresar el valor transportador o el valor cliente');
                        continuar = false;
                    }
                } else {
                    if ($scope.CodigoLiquidacionPlanillaDetalle === 0) {
                        if ($scope.CodigoLiquidacionPlanilla > 0) {
                            if ($scope.Novedades.ValorCompra === undefined || $scope.Novedades.ValorCompra === '' || $scope.Novedades.ValorCompra === null || $scope.Novedades.ValorCompra === 0) {
                                $scope.MensajesErrorModal.push('Debe ingresar el valor transportador');
                                continuar = false;
                            }
                        }
                    }
                    if ($scope.CodigoFacturacion > 0) {
                        if ($scope.Remesa.NumeroFacturaRemesa === 0 || $scope.Remesa.NumeroFacturaRemesa === undefined) {
                            if ($scope.Remesa === undefined || $scope.Remesa === '' || $scope.Remesa === null || $scope.Remesa.NumeroRemesa === 0 || $scope.Remesa.NumeroRemesa === undefined || $scope.Remesa.NumeroRemesa === null) {
                                $scope.MensajesErrorModal.push('Debe ingresar el número de la remesa');
                                continuar = false;
                            }
                            if ($scope.Novedades.ValorVenta === undefined || $scope.Novedades.ValorVenta === '' || $scope.Novedades.ValorVenta === null || $scope.Novedades.ValorVenta === 0) {
                                $scope.MensajesErrorModal.push('Debe ingresar el valor cliente');
                                continuar = false;
                            }
                            if ($scope.Novedades.Proveedor.Codigo > 0) {
                                if ($scope.Novedades.ValorCosto > $scope.Novedades.ValorVenta) {
                                    $scope.MensajesErrorModal.push('El valor costo debe ser menor o igual al valor cliente');
                                    continuar = false;
                                }
                            }
                        }
                    }
                }
                if ($scope.Novedades.Proveedor.Codigo > 0) {
                    if ($scope.Novedades.ValorCosto === undefined || $scope.Novedades.ValorCosto === '' || $scope.Novedades.ValorCosto === null || $scope.Novedades.ValorCosto === 0) {
                        $scope.MensajesErrorModal.push('Debe ingresar el valor costo');
                        continuar = false;
                    }
                }
                if ($scope.DeshabilitarValorCompra === true && $scope.DeshabilitarValorCliente === true) {
                    $scope.MensajesErrorModal.push('No se pueden ingresar novedades');
                    continuar = false;
                }
            }
            return continuar;
        }

        //funcion que anula nueva novedad
        $scope.AnularNovedad = function (item) {
            $scope.CausaAnulacion = '';
            $scope.nombrenovedad = item.Novedad.Nombre
            $scope.numeroplanilla = item.NumeroPlanilla;
            $scope.Modal = {};
            $scope.Modal.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            $scope.Modal.Codigo = item.Codigo
            $scope.FechaAnulacion = new Date()
            $scope.Modal.UsuarioAnula = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            showModal('modalDatosAnular')
        }

        $scope.AnularNovedades = function (causa) {
            if (causa === '' || causa === undefined || causa === null) {
                ShowError('Debe ingresar la causa de anulación de la novedad')
            }
            else {
                $scope.Modal.CausaAnulacion = causa
                DetalleNovedadesDespachosFactory.Anular($scope.Modal).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anulo la novedad.' + $scope.Modal.Codigo + ' - ' + $scope.nombrenovedad);
                            closeModal('modalDatosAnular');
                            Find();
                            $scope.ListaNovedades = [];
                            var filtroNovedades = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroPlanilla: $scope.numeroplanilla
                            }
                            DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            $scope.ListaNovedades = response.data.Datos
                                        }
                                    }
                                }, function (response) {
                                });
                            showModal('modalNovedades')
                            $('#NuevaNovedad').hide(); $('#BotonNuevo').show()
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        /*$scope.DeshabilitarGuardar = function (item) {
            //var DesabilitarBotonGuardar = false;
            $scope.DeshabilitarAgregarNovedad = false;

            //if ($scope.DeshabilitarActualizar) {
            //    DesabilitarBotonGuardar = true;
            //}

            //if (item.Cumplido.Numero > 0) {
            //    DesabilitarBotonGuardar = true;
            //}

            var FiltroPlanillafiltros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroPlanilla: item.NumeroPlanilla,
                Estado: ESTADO_DEFINITIVO,
                Aprobado: -1,
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO }

            };
            LiquidacionesFactory.Consultar(FiltroPlanillafiltros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            //--Liquidaciones Asociadas
                            $scope.DeshabilitarAgregarNovedad = true;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            //return DesabilitarBotonGuardar;
        }*/
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
    }]);