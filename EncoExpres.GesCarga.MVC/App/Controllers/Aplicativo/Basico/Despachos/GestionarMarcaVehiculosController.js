﻿EncoExpresApp.controller("GestionarMarcaVehiculosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'MarcaVehiculosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, MarcaVehiculosFactory) {

        $scope.Titulo = 'GESTIONAR MARCAS VEHÍCULOS';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Marcas Vehículos' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_MARCA_VEHICULOS);

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_MARCA_VEHICULOS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVA" },
            { Codigo: 1, Nombre: "ACTIVA" },
        ]
       
        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;      
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando marca vehículo código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando marca vehículo Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            MarcaVehiculosFactory.Obtener(filtros).
               then(function (response) {
                   if (response.data.ProcesoExitoso === true) {

                       $scope.Modelo.Codigo = response.data.Datos.Codigo;
                       $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                       $scope.Modelo.Nombre = response.data.Datos.Nombre;
                       $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                   }
                   else {
                       ShowError('No se logro consultar la marca vehículo código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                       document.location.href = '#!ConsultarMarcaVehiculos';
                   }
               }, function (response) {
                   ShowError(response.statusText);
                   document.location.href = '#!ConsultarMarcaVehiculos';
               });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

                MarcaVehiculosFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó la marca vehículos "' + $scope.Modelo.Nombre + '"');
                                    location.href = '#!ConsultarMarcaVehiculos/' + response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó la marca vehículos "' + $scope.Modelo.Nombre + '"');
                                    location.href = '#!ConsultarMarcaVehiculos/' + $scope.Modelo.Codigo;
                                }                          
                            }                          
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        if (response.statusText.includes('IX_Marca_Vehiculos_Nombre')) {
                            ShowError('No se pudo guardar, el nombre ingresado ya está siendo utilizado')
                        } else if (response.statusText.includes('IX_Marca_Vehiculos_Codigo_Alterno')) {
                            ShowError('No se pudo guardar, el código alterno ingresado ya está siendo utilizado')
                        } else {
                            ShowError(response.statusText);
                        }
                    });
            }
        };
        
        
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
                $scope.MensajesError.push('Debe ingresar el nombre de la marca vehículos');
                continuar = false;
            }
            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarMarcaVehiculos/' + $scope.Modelo.Codigo;
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
}]);