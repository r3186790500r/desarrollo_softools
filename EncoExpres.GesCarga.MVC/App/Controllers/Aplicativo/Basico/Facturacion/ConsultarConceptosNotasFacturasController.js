﻿EncoExpresApp.controller('ConsultarConceptosNotasFacturasCtrl', ['$scope', 'blockUI', '$linq', '$routeParams', '$timeout','ConceptoNotasFacturasFactory',
    function ($scope, blockUI, $linq, $routeParams, $timeout, ConceptoNotasFacturasFactory){
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Facturación' }, { Nombre: 'Conceptos Notas Facturas' }];


        $scope.Nombre = '';
        $scope.Codigo = '';
        $scope.TipoNota = '';
        $scope.MensajesError = [];
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONCEPTOS_NOTAS_FACTURA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.ListaTiposNota = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'CRÉDITO', Codigo: CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO },
            { Nombre: 'DÉBITO', Codigo: CODIGO_TIPO_DOCUMENTO_NOTA_DEBITO }
        ];

        $scope.TipoNota = $linq.Enumerable().From($scope.ListaTiposNota).First('$.Codigo == -1');

        //Funcion Nuevo Documento:
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarConceptosNotasFacturas';
            }
        };


        $scope.Buscar = function () {
                        
            $scope.Codigo = 0;
            Find();               
            
        }

        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }


        //Buscar:


        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                //NumeroDocumento
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.MensajesError = [];
            $scope.ListadoConceptos = [];

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo : $scope.Codigo,
                TipoDocumento: $scope.TipoNota.Codigo,
                Nombre: $scope.Nombre
            }

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;

                ConceptoNotasFacturasFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.Buscando = true;
                            $scope.ResultadoSinRegistros = '';
                            $scope.ListadoConceptos = response.data.Datos;
                            for (var i = 0; i < $scope.ListadoConceptos.length; i++) {
                                if ($scope.ListadoConceptos[i].ConceptoSistema == 0) {
                                    $scope.ListadoConceptos[i].Sistema = 'NO';
                                } else {
                                    $scope.ListadoConceptos[i].Sistema = 'SI';
                                }

                                if ($scope.ListadoConceptos[i].TipoDocumento == 190) {
                                    $scope.ListadoConceptos[i].TipoNota = 'NOTA CRÉDITO';
                                } else {
                                    $scope.ListadoConceptos[i].TipoNota = 'NOTA DÉBITO';
                                }

                            }

                        } else {
                            $scope.ListadoConceptos = [];
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                      
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }else{
                $scope.Buscando = false;
            }
            blockUI.stop();

        };



        //Anular:

        $scope.EliminarConcepto = function (concepto) {

            $scope.CodigoConcepto = concepto.Codigo;
            $scope.NombreConcepto = concepto.Nombre


            showModal('modalEliminarConcepto');


        };


        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoConcepto
            };

            ConceptoNotasFacturasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó el Concepto ' + $scope.NombreConcepto);
                        closeModal('modalEliminarConcepto');                        
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    console.log(response);
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarConceptos');
                    $scope.ModalError = 'No se puede eliminar el Concepto  ' + $scope.NombreConcepto + ' ya que se encuentra relacionado con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };

        $scope.CerrarModal = function () {
            closeModal('modalEliminarConcepto');
            closeModal('modalMensajeEliminarConceptos');
        }


        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        //Máscaras:


        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        }

        
    }
]);