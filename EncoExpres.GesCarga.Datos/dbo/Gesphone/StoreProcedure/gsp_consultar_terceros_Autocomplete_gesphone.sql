﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 05/06/2021 
-- Módulo: Gesphone

PRINT 'gsp_consultar_terceros_Autocomplete_gesphone'
GO
DROP PROCEDURE gsp_consultar_terceros_Autocomplete_gesphone
GO
CREATE PROCEDURE dbo.gsp_consultar_terceros_Autocomplete_gesphone    
(                               
 @par_EMPR_Codigo SMALLINT,                                              
 @par_Filtro VARCHAR(100) = NULL,                                              
 @par_Perfil_Tercero SMALLINT = NULL,                      
 @par_Codigo NUMERIC = null,    
 @par_Estado NUMERIC = NULL    
)                                              
AS                                              
BEGIN                                              
 IF @par_Codigo > 0              
 BEGIN                                              
  IF @par_Perfil_Tercero IS NULL    
  BEGIN    
   SELECT     
   0 As Obtener                                              
   ,TERC.EMPR_Codigo                                              
   ,TERC.Codigo                                              
   ,TERC.Numero_Identificacion                                              
   ,TERC.Digito_Chequeo                                         
   ,TERC.CATA_TINT_Codigo                                      
   ,TERC.CATA_TIID_Codigo                                      
   ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                                            
   ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                                            
   ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2                
   ,CONCAT(TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2) As NombreCompleto                                        
   ,ISNULL(TERC.Emails,'')AS Emails                                            
   ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                                              
   ,CIUD.Nombre AS NombreCuidadDireccion                                  
   ,ISNULL(TERC.Barrio,'') AS Barrio                                            
   ,ISNULL(TERC.Direccion,'')AS Direccion                               
   ,TERC.Codigo_Postal AS Codigo_Postal                                                  
   ,ISNULL(TERC.Telefonos,'')AS Telefonos                                              
   ,ISNULL(TERC.Celulares,'')AS Celulares                                              
   ,TERC.Estado                                           
   ,TERC.Justificacion_Bloqueo            
   ,TERC.TERC_Codigo_Beneficiario                                    
   ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                                              
   ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno                                  
   ,ROW_NUMBER() OVER(ORDER BY TERC.Codigo) AS RowNumber                                              
                                                     
   FROM                                               
   Terceros TERC LEFT JOIN Ciudades CIEX ON                                            
   TERC.EMPR_Codigo = CIEX.EMPR_Codigo                                            
   AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                            
                      
   LEFT JOIN Ciudades CIUD  ON                         
   TERC.EMPR_Codigo = CIUD.EMPR_Codigo                                              
   AND TERC.CIUD_Codigo = CIUD.Codigo                      
   WHERE                                               
   TERC.Codigo > 0                                   
   AND (TERC.Codigo = @par_Codigo OR @par_Codigo IS NULL)                      
   AND TERC.EMPR_Codigo = @par_EMPR_Codigo                                              
   AND (                        
   (TERC.Numero_Identificacion LIKE  '%'+@par_Filtro+ '%' )                                            
   OR ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%') OR (@par_Filtro IS NULL))                    
   )                                              
   AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)    
   order by TERC.Nombre                                             
  END                                              
  ELSE                                              
  BEGIN    
   SELECT    
   0 As Obtener                                              
   ,TERC.EMPR_Codigo                                              
   ,TERC.Codigo                                          
   ,TERC.Numero_Identificacion                                              
   ,TERC.Digito_Chequeo                                        
   ,TERC.CATA_TINT_Codigo                                      
   ,TERC.CATA_TIID_Codigo                                       
   ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                                           
   ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2               
   ,CONCAT(TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2) As NombreCompleto                                         
   ,ISNULL(TERC.Emails,'') AS Emails                                              
   ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                                              
   ,CIUD.Nombre AS NombreCuidadDireccion                                              
   ,ISNULL(TERC.Barrio,'') AS Barrio                                            
   ,ISNULL(TERC.Direccion,'')AS Direccion                               
   ,TERC.Codigo_Postal AS Codigo_Postal                                 
   ,ISNULL(TERC.Telefonos,'') AS Telefonos                                       
   ,ISNULL(TERC.Celulares ,'') AS Celulares                                           
   ,TERC.Estado                                       
   ,TERC.TERC_Codigo_Beneficiario                                           
   ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                                              
   ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno                              
   ,TERC.Justificacion_Bloqueo            
   ,ROW_NUMBER() OVER(ORDER BY TERC.Nombre,TERC.Razon_Social) AS RowNumber                                              
   FROM                                               
                                                   
   Terceros TERC LEFT JOIN Ciudades CIEX ON                                            
   TERC.EMPR_Codigo = CIEX.EMPR_Codigo                                            
   AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                                               
                          
   LEFT JOIN Ciudades CIUD  ON                         
   TERC.EMPR_Codigo = CIUD.EMPR_Codigo                                              
   AND TERC.CIUD_Codigo = CIUD.Codigo                      
                                                 
   ,Perfil_Terceros PETE                                              
                                              
   WHERE                                                  
   TERC.Codigo > 0                                   
   AND (TERC.Codigo = @par_Codigo OR @par_Codigo IS NULL)                      
   AND TERC.EMPR_Codigo = @par_EMPR_Codigo                                              
   AND (                        
   (TERC.Numero_Identificacion LIKE  '%'+@par_Filtro+ '%' )                                            
   OR ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE RTRIM(LTRIM(@par_Filtro)) + '%' OR TERC.Razon_Social LIKE RTRIM(LTRIM(@par_Filtro)) + '%') OR (@par_Filtro IS NULL))                        
   )            
   AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)    
   AND TERC.EMPR_Codigo = PETE.EMPR_Codigo    
   AND TERC.Codigo = PETE.TERC_Codigo                                              
   AND PETE.Codigo = @par_Perfil_Tercero    
   ORDER BY TERC.Nombre ASC             
  END                 
 END    
 ELSE              
 BEGIN    
  IF @par_Perfil_Tercero IS NULL                                              
  BEGIN    
   SELECT     
   0 As Obtener                                              
   ,TERC.EMPR_Codigo                                              
   ,TERC.Codigo                                              
   ,TERC.Numero_Identificacion                                              
   ,TERC.Digito_Chequeo                                         
   ,TERC.CATA_TINT_Codigo                                      
   ,TERC.CATA_TIID_Codigo                                      
   ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                                            
   ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                                            
   ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2            
   ,CONCAT(TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2) As NombreCompleto                                            
   ,ISNULL(TERC.Emails,'')AS Emails                                            
   ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                                              
   ,CIUD.Nombre AS NombreCuidadDireccion                                  
   ,ISNULL(TERC.Barrio,'') AS Barrio                                            
   ,ISNULL(TERC.Direccion,'')AS Direccion                               
   ,TERC.Codigo_Postal AS Codigo_Postal                                                  
   ,ISNULL(TERC.Telefonos,'')AS Telefonos                                              
   ,ISNULL(TERC.Celulares,'')AS Celulares                                              
   ,TERC.Estado                                           
   ,TERC.TERC_Codigo_Beneficiario                                    
   ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                                              
   ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno                                  
   ,TERC.Justificacion_Bloqueo            
   ,ROW_NUMBER() OVER(ORDER BY TERC.Codigo) AS RowNumber                                              
                                                     
   FROM                                               
   Terceros TERC LEFT JOIN Ciudades CIEX ON                                            
   TERC.EMPR_Codigo = CIEX.EMPR_Codigo                                            
   AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                            
                      
   LEFT JOIN Ciudades CIUD  ON                         
   TERC.EMPR_Codigo = CIUD.EMPR_Codigo                                              
   AND TERC.CIUD_Codigo = CIUD.Codigo                      
   WHERE                                               
   TERC.Codigo > 0                                   
   AND (TERC.Codigo = @par_Codigo OR @par_Codigo IS NULL)                      
   AND TERC.EMPR_Codigo = @par_EMPR_Codigo                                              
   AND (                        
   (TERC.Numero_Identificacion LIKE  '%'+@par_Filtro+ '%' )                                            
   OR ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%') OR (@par_Filtro IS NULL))                        
   )                                              
   AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)                               
   order by TERC.Nombre                                             
  END                                              
  ELSE                                      
  BEGIN    
   SELECT    
   0 As Obtener                                              
   ,TERC.EMPR_Codigo                                              
   ,TERC.Codigo                                              
   ,TERC.Numero_Identificacion                                              
   ,TERC.Digito_Chequeo                                        
   ,TERC.CATA_TINT_Codigo                                      
   ,TERC.CATA_TIID_Codigo                                       
   ,ISNULL(TERC.CIUD_Codigo_Identificacion,0) AS CIUD_Codigo_Expedicion                                           
   ,ISNULL(CIEX.Nombre,'') AS Ciudad_Expedicion                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Razon_Social,''))) AS Razon_Social                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Nombre, ''))) AS Nombre                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido1, ''))) AS Apellido1                                              
   ,LTRIM(RTRIM(ISNULL(TERC.Apellido2,''))) AS Apellido2                   
   ,CONCAT(TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2) As NombreCompleto                                     
   ,ISNULL(TERC.Emails,'') AS Emails                                              
   ,TERC.CIUD_Codigo AS CIUD_Codigo_Direccion                                              
   ,CIUD.Nombre AS NombreCuidadDireccion                                              
   ,ISNULL(TERC.Barrio,'') AS Barrio                                 
   ,ISNULL(TERC.Direccion,'')AS Direccion                               
   ,TERC.Codigo_Postal AS Codigo_Postal                                 
   ,ISNULL(TERC.Telefonos,'') AS Telefonos                                       
   ,ISNULL(TERC.Celulares ,'') AS Celulares                                           
   ,TERC.Estado                                       
   ,TERC.TERC_Codigo_Beneficiario                                           
   ,CASE WHEN TERC.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado                                              
   ,ISNULL(TERC.Codigo_Alterno,'') AS Codigo_Alterno                                  
   ,TERC.Justificacion_Bloqueo            
   ,ROW_NUMBER() OVER(ORDER BY TERC.Nombre,TERC.Razon_Social) AS RowNumber                                              
   FROM                                               
                                                   
   Terceros TERC LEFT JOIN Ciudades CIEX ON                                            
   TERC.EMPR_Codigo = CIEX.EMPR_Codigo                                            
   AND TERC.CIUD_Codigo_Identificacion = CIEX.Codigo                                               
                          
   LEFT JOIN Ciudades CIUD  ON                         
   TERC.EMPR_Codigo = CIUD.EMPR_Codigo                                              
   AND TERC.CIUD_Codigo = CIUD.Codigo                      
                                                 
   ,Perfil_Terceros PETE                                              
                                              
   WHERE                                                  
   TERC.Codigo > 0                                   
   AND (TERC.Codigo = @par_Codigo OR @par_Codigo IS NULL)                      
   AND TERC.EMPR_Codigo = @par_EMPR_Codigo                                              
   AND (                        
   (TERC.Numero_Identificacion LIKE  '%'+@par_Filtro+ '%' )                                            
   OR ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Filtro)) + '%') OR (@par_Filtro IS NULL))                        
   )                                              
   AND (TERC.Estado = @par_Estado OR @par_Estado IS NULL)                                            
   AND TERC.EMPR_Codigo = PETE.EMPR_Codigo              
   AND TERC.Codigo = PETE.TERC_Codigo                                              
   AND PETE.Codigo = @par_Perfil_Tercero                                              
                      
   ORDER BY TERC.Nombre ASC                                       
  END              
 END                                              
END    
GO