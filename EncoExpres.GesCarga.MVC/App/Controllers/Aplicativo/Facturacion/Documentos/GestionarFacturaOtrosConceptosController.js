﻿EncoExpresApp.controller("GestionarFacturaOtrosConceptosCtrl", ['$scope', '$routeParams', '$linq', 'blockUI', '$timeout', 'ValorCatalogosFactory', 'TercerosFactory', 'FacturasOtrosConceptosFactory',
    'ConceptosFacturacionFactory', 'blockUIConfig', 'CierreContableDocumentosFactory',
    function ($scope, $routeParams, $linq, blockUI, $timeout, ValorCatalogosFactory, TercerosFactory, FacturasOtrosConceptosFactory,
        ConceptosFacturacionFactory, blockUIConfig, CierreContableDocumentosFactory) {

        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'NUEVA FACTURA OTROS CONCEPTOS';
        $scope.MapaSitio = [{ Nombre: 'Facturación' }, { Nombre: 'Documentos' }, { Nombre: 'Facturas Otros Conceptos' }, { Nombre: 'Gestionar' }];
        $scope.Master = "#!ConsultarFacturaOtrosConceptos";
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_FACTURA_OTROS_CONCEPTOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        //--Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoNoPermitirFacturarAFacturacion == true) {
            $scope.DeshabilitarFacturarA = true;
        }
        //--------------------------Validaciones Proceso-------------------------//
        //--------------------------Variables-------------------------//
        $scope.MensajesErrorConslta = [];
        var TMPCodigoCliente = 0;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: {
                Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre,
                Ciudad: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo },
                ResolucionFacturacion: $scope.Sesion.UsuarioAutenticado.Oficinas.ResolucionFacturacion
            },
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Fecha: new Date()
        };
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_BORRADOR);
        //Valores Iniciales de Factura
        $scope.Modelo.ValorOtrosConceptos = 0;
        $scope.Modelo.ValorFactura = 0;
        //Valores Iniciales de Factura
        //--Cargar Listado Otros Conceptos
        $scope.ListadoConceptosFactura = [];//Carga Complete para agregar conceptos
        $scope.ListadoOtrosConceptosFactura = [];//Lista de Conceptos Agregados
        $scope.ListadoImpuestosConceptosFactura = [];//Impuestos Agrupados de los Conceptos agregados
        $scope.ListadoCliente = [];
        $scope.ListadoTerceros = [];
        //--------------------------Variables-------------------------//
        //--------------------Controles---------------------//
        $('.CriteriosBusqueda').hide();
        $('#DiasPlazo').hide();
        //--------------------Controles---------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //---- Clientes
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //---- Facturar A
        $scope.AutocompleteTercero = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoTerceros = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTerceros);
                }
            }
            return $scope.ListadoTerceros;
        };
        //----Carga Inicial
        $scope.InitLoad = function () {
            //--Combo de forma pago
            $scope.ListadoFormaPago = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS },
                Sync: true
            }).Datos;
            $scope.ListadoFormaPago.splice($scope.ListadoFormaPago.findIndex(item => item.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA), 1);
            $scope.Modelo.FormaPago = $scope.ListadoFormaPago[0];
            $scope.AsignarFormaPago($scope.Modelo.FormaPago.Codigo);
            $scope.ConsultarConceptosImpuestos();
            //----Obtener parametros
            if ($routeParams.Numero > 0) {
                $scope.Numero = $routeParams.Numero;
                Obtener();
            }
            else {
                $scope.Numero = 0;
                FindCierre();
            }
        };
        //----Carga Inicial
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //------------------------------------------------Funciones Generales------------------------------------------------//
        //--Conceptos Factura e impuestos por concepto
        $scope.ConsultarConceptosImpuestos = function () {
            var RespConcetosFactura = ConceptosFacturacionFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: -1,
                Ciudad: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo },
                Sync: true
            });
            if (RespConcetosFactura.ProcesoExitoso == true) {
                if (RespConcetosFactura.Datos.length > CERO) {
                    RespConcetosFactura.Datos.forEach(function (item) {
                        if (item.Estado != ESTADO_BORRADOR) {
                            if (item.Codigo != CODIGO_INICIAL_NO_APLICA) {
                                $scope.ListadoConceptosFactura.push(item);
                            }
                        }
                    });
                    for (var i = 0; i < $scope.ListadoConceptosFactura.length; i++) {
                        var ListadoImpuestosConceptos = $scope.ListadoConceptosFactura[i].ListadoImpuestos;
                        for (var j = 0; j < ListadoImpuestosConceptos.length; j++) {
                            if ($scope.ListadoImpuestosConceptosFactura.length > 0) {
                                var existeItem = false;
                                for (var k = 0; k < $scope.ListadoImpuestosConceptosFactura.length; k++) {
                                    if ($scope.ListadoImpuestosConceptosFactura[k].Codigo == ListadoImpuestosConceptos[j].Codigo) {
                                        existeItem = true;
                                    }
                                }
                                if (existeItem == false) {
                                    $scope.ListadoImpuestosConceptosFactura.push({
                                        Codigo: ListadoImpuestosConceptos[j].Codigo,
                                        Nombre: ListadoImpuestosConceptos[j].Nombre,
                                        Operacion: ListadoImpuestosConceptos[j].Operacion,
                                        valor_base: CERO,
                                        Valor_tarifa: ListadoImpuestosConceptos[j].Valor_tarifa,
                                        Valor_impuesto: CERO
                                    });
                                }
                            }
                            else {
                                $scope.ListadoImpuestosConceptosFactura.push({
                                    Codigo: ListadoImpuestosConceptos[j].Codigo,
                                    Nombre: ListadoImpuestosConceptos[j].Nombre,
                                    Operacion: ListadoImpuestosConceptos[j].Operacion,
                                    valor_base: CERO,
                                    Valor_tarifa: ListadoImpuestosConceptos[j].Valor_tarifa,
                                    Valor_impuesto: CERO
                                });
                            }
                        }
                    }
                    $scope.ListaImpuestosAgregadosConceptos = angular.copy($scope.ListadoImpuestosConceptosFactura);
                }
            }
            else {
                ShowError(RespConcetosFactura.statusText);
            }
        };
        //Asignar Cliente
        $scope.AsignarCliente = function (Cliente) {
            if (Cliente !== undefined && Cliente !== null && Cliente !== '') {
                //valida si el dato ingresado hace parte del objeto de la lista
                if (angular.isObject(Cliente)) {
                    if (TMPCodigoCliente !== Cliente.Codigo) {
                        $scope.Modelo.Cliente = angular.copy(Cliente);
                        $scope.Modelo.FacturarA = angular.copy(Cliente);
                        TMPCodigoCliente = Cliente.Codigo;
                        AsignarFormaPagoCliente($scope.Modelo.Cliente);
                    }
                }
            }
        };
        //Asignar Cliente
        //Asignar Forma Pago Cliente Predeterminado
        function AsignarFormaPagoCliente(Cliente) {
            var response = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Cliente.Codigo, Sync: true });
            if (response.ProcesoExitoso === true) {
                try {
                    var formaPagoCliente = response.Datos.Cliente;
                    $scope.Modelo.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + formaPagoCliente.FormaPago.Codigo);
                    $scope.Modelo.DiasPlazo = formaPagoCliente.DiasPlazo;
                    $scope.AsignarFormaPago($scope.Modelo.FormaPago.Codigo);
                } catch (e) {
                    //--No tiene asignado forma de pago
                }
            }
        }
        //----Obtener
        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                Estado: -1,
                Anulado: -1
            };
            $scope.DeshabilitarCliente = true;
            FacturasOtrosConceptosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Titulo = "FACTURA OTROS CONCEPTOS";
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        $scope.Modelo.NumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.Modelo.Fecha = new Date(response.data.Datos.Fecha);
                        $scope.Modelo.Cliente = $scope.CargarTercero(response.data.Datos.Cliente.Codigo);
                        $scope.Modelo.FacturarA = $scope.CargarTercero(response.data.Datos.FacturarA.Codigo);

                        $scope.CodigoFormaPago = response.data.Datos.FormaPago.Codigo;
                        if ($scope.ListadoFormaPago !== undefined && $scope.ListadoFormaPago !== null) {
                            if ($scope.ListadoFormaPago.length > 0) {
                                $scope.Modelo.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.CodigoFormaPago);
                                $scope.AsignarFormaPago($scope.CodigoFormaPago);
                            }
                        }

                        $scope.Modelo.DiasPlazo = response.data.Datos.DiasPlazo;

                        if (response.data.Datos.Anulado == 1) {
                            $scope.ListadoEstados.push({ Nombre: 'ANULADO', Codigo: 2 });
                            $scope.Modelo.Estado = $scope.ListadoEstados[2];
                            $scope.Deshabilitar = true;
                            $scope.DeshabilitarFacturarA = true;
                        } else {
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                            if (response.data.Datos.Estado == ESTADO_ACTIVO) {
                                $scope.Deshabilitar = true;
                                $scope.DeshabilitarFacturarA = true;
                            } else {
                                $scope.Deshabilitar = false;
                                $scope.MostrarCriteriosBusqueda();
                            }
                        }

                        $scope.Modelo.Observaciones = response.data.Datos.Observaciones;
                        $scope.Modelo.ValorImpuestos = $scope.MaskValoresGrid(response.data.Datos.ValorImpuestos);
                        $scope.Modelo.ValorOtrosConceptos = $scope.MaskValoresGrid(response.data.Datos.ValorOtrosConceptos);
                        $scope.Modelo.ValorFactura = $scope.MaskValoresGrid(response.data.Datos.ValorFactura);

                        $scope.ListadoOtrosConceptosFactura = [];
                        response.data.Datos.OtrosConceptos.forEach(function (itmOtroConcepto) {
                            var auxOtr = {
                                DetalleNODE: { Codigo: itmOtroConcepto.DetalleNODE.Codigo },
                                NumeroRemesa: { Numero: itmOtroConcepto.Remesas.Numero, NumeroDocumento: itmOtroConcepto.Remesas.NumeroDocumento },
                                ConceptoVenta: {
                                    Codigo: itmOtroConcepto.ConceptosVentas.Codigo,
                                    Nombre: itmOtroConcepto.ConceptosVentas.Nombre,
                                    Operacion: itmOtroConcepto.ConceptosVentas.Operacion
                                },
                                Descripcion: itmOtroConcepto.Descripcion,
                                ValorVenta: itmOtroConcepto.Valor_Concepto
                            };
                            $scope.ListadoOtrosConceptosFactura.push(auxOtr);
                        });
                        OrdenarIndicesOtrosConceptos();

                        var tmpImpuestoConc = [];
                        var DetalleImpuestos = response.data.Datos.DetalleImpuestosFacturas;
                        for (var i = 0; i < DetalleImpuestos.length; i++) {
                            switch (DetalleImpuestos[i].CATA_TVFI_Codigo) {
                                case CODIGO_CATALOGO_TIPO_VALOR_FACTURA_IMPUESTOS_OTROS_CONCEPTOS:
                                    for (var j = 0; j < $scope.ListaImpuestosAgregadosConceptos.length; j++) {
                                        if (DetalleImpuestos[i].ImpuestoFacturas.Codigo == $scope.ListaImpuestosAgregadosConceptos[j].Codigo) {
                                            tmpImpuestoConc.push({
                                                Codigo: DetalleImpuestos[i].ImpuestoFacturas.Codigo,
                                                Nombre: DetalleImpuestos[i].ImpuestoFacturas.Nombre,
                                                Operacion: $scope.ListaImpuestosAgregadosConceptos[j].Operacion,
                                                valor_base: DetalleImpuestos[i].Valor_base,
                                                Valor_tarifa: DetalleImpuestos[i].Valor_tarifa,
                                                Valor_impuesto: DetalleImpuestos[i].Valor_impuesto
                                            });
                                        }
                                    }
                                    break;
                            }
                        }
                        $scope.ListaImpuestosAgregadosConceptos = tmpImpuestoConc;

                        if (response.data.Datos.Estado == ESTADO_BORRADOR) {
                            $scope.Calcular();
                        }

                    }
                    else {
                        ShowError('No se logro consultar la factura No. ' + $scope.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = $scope.Master;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = $scope.Master;
                });

            blockUI.stop();
        }
        //----Obtener
        //----Guardar
        $scope.ConfirmacionGuardar = function () {
            if ($scope.DeshabilitarActualizar) {
                location.href = '#!ConsultarPlanillaGuias/' + $scope.Modelo.Planilla.Numero;
            } else {
                showModal('modalConfirmacionGuardar');
            }
        };
        $scope.PeriodoValido = true
        function FindCierre() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentos: { Codigo: 175 },
                Sync: true
            };
            var RCierreContable = CierreContableDocumentosFactory.Consultar(filtros)
            if (RCierreContable.ProcesoExitoso === true) {
                if (RCierreContable.Datos.length > 0) {
                    $scope.ListadoTipoDocumentos = RCierreContable.Datos;
                    $scope.ValidarCierre();
                }
            }
        }
        $scope.ValidarCierre = function () {
            ValidarCierre();
        }

        function ValidarCierre() {
            if ($scope.ListadoTipoDocumentos.length > 0) {
                var anoFecha = $scope.Modelo.Fecha.getFullYear();
                var mesFecha = $scope.Modelo.Fecha.getMonth();
                for (var k = 0; k < $scope.ListadoTipoDocumentos.length; k++) {
                    var item = $scope.ListadoTipoDocumentos[k];
                    var mes = MascaraNumero(item.Mes.CampoAuxiliar2);
                    if (item.Ano === anoFecha && mes === (mesFecha + 1) && item.EstadoCierre.Codigo === 18002) {
                        ShowError('La fecha se encuentra en un mes y año con cierre contable');
                        $scope.Modelo.Fecha = '';
                        $scope.PeriodoValido = false
                        break;
                    }
                }
            }
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                $scope.PeriodoValido = true
                FindCierre()
                if ($scope.PeriodoValido) {
                    $scope.ListaAuxOtrosConceptosFactura = [];
                    for (i = 0; i < $scope.ListadoOtrosConceptosFactura.length; i++) {
                        var TmpListadoImpuestosConceptos = [];
                        var SumImpuestoConcepto = 0;
                        var listaImpuestos = $scope.ListadoOtrosConceptosFactura[i].ListadoImpuestos;
                        if (listaImpuestos !== undefined && listaImpuestos !== null && listaImpuestos !== []) {
                            for (j = 0; j < listaImpuestos.length; j++) {
                                var existe = false;
                                for (k = 0; k < $scope.ListaImpuestosAgregadosConceptos.length; k++) {
                                    if (listaImpuestos[j].Codigo == $scope.ListaImpuestosAgregadosConceptos[k].Codigo) {
                                        existe = true;
                                        break;
                                    }
                                }
                                if (existe) {
                                    var objImpuesto = {
                                        ENIM_Codigo: listaImpuestos[j].Codigo,
                                        Valor_tarifa: listaImpuestos[j].Valor_tarifa,
                                        Valor_base: listaImpuestos[j].valor_base,
                                        Valor_impuesto: listaImpuestos[j].Valor_impuesto,
                                    };
                                    switch (listaImpuestos[j].Operacion) {
                                        case 1:
                                            SumImpuestoConcepto += listaImpuestos[j].Valor_impuesto;
                                            break;
                                        case 2:
                                            SumImpuestoConcepto -= listaImpuestos[j].Valor_impuesto;
                                            break;
                                    }
                                    TmpListadoImpuestosConceptos.push(objImpuesto);
                                }
                            }
                        }
                        var otroConcepto = {
                            DetalleNODE: $scope.ListadoOtrosConceptosFactura[i].DetalleNODE,
                            ConceptosVentas: { Codigo: $scope.ListadoOtrosConceptosFactura[i].ConceptoVenta.Codigo },
                            Descripcion: $scope.ListadoOtrosConceptosFactura[i].Descripcion,
                            Valor_Concepto: $scope.ListadoOtrosConceptosFactura[i].ValorVenta,
                            Valor_Impuestos: SumImpuestoConcepto,
                            Remesas: { Numero: $scope.ListadoOtrosConceptosFactura[i].NumeroRemesa.Numero },
                            NumeroOrdenServicio: CERO,
                            Emoe_numero: CERO,
                            DetalleImpuestoConceptosFacturas: TmpListadoImpuestosConceptos
                        };
                        $scope.ListaAuxOtrosConceptosFactura.push(otroConcepto);
                    }

                    var TmpDetalleImpuestosFacturas = [];
                    $scope.ListaImpuestosAgregadosConceptos.forEach(function (item) {
                        if (item.Valor_impuesto > CERO) {
                            var impuesto = {
                                ImpuestoFacturas: { Codigo: item.Codigo },
                                CATA_TVFI_Codigo: CODIGO_CATALOGO_TIPO_VALOR_FACTURA_IMPUESTOS_OTROS_CONCEPTOS,
                                Valor_tarifa: item.Valor_tarifa,
                                Valor_base: item.valor_base,
                                Valor_impuesto: item.Valor_impuesto
                            };
                            TmpDetalleImpuestosFacturas.push(impuesto);
                        }
                    });
                    var CuentaPorCobrar = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                        CodigoAlterno: '',
                        Fecha: $scope.Modelo.Fecha,
                        Tercero: { Codigo: $scope.Modelo.Cliente.Codigo },
                        DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_FACTURA },
                        CodigoDocumentoOrigen: CERO,
                        NumeroDocumento: CERO,
                        Numeracion: '',
                        CuentaPuc: { Codigo: CERO },
                        ValorTotal: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.ValorFactura)),
                        Abono: CERO,
                        Saldo: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.ValorFactura)),
                        FechaCancelacionPago: $scope.Modelo.Fecha,
                        Aprobado: 1,
                        UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        FechaAprobo: $scope.Modelo.Fecha,
                        Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                        Vehiculo: { Codigo: CERO },
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    };

                    var ingresarFacturarA = false;
                    if (($scope.Modelo.Cliente == undefined || $scope.Modelo.Cliente == '' || $scope.Modelo.Cliente == null) && $scope.Sesion.UsuarioAutenticado.ManejoFacturarA == true) {
                        ingresarFacturarA = true;
                    }

                    var Factura = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_FACTURAS,
                        Numero: $scope.Numero,
                        NumeroPreImpreso: CERO,
                        CataTipoFactura: CODIGO_CATALOGO_TIPO_FACTURA_OTROS_CONCEPTOS,
                        Fecha: $scope.Modelo.Fecha,
                        OficinaFactura: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },

                        Cliente: { Codigo: ingresarFacturarA == true ? $scope.Modelo.FacturarA.Codigo : $scope.Modelo.Cliente.Codigo },
                        FacturarA: { Codigo: $scope.Modelo.FacturarA.Codigo },
                        FormaPago: { Codigo: $scope.Modelo.FormaPago.Codigo },
                        DiasPlazo: $scope.Modelo.DiasPlazo,
                        Observaciones: $scope.Modelo.Observaciones,

                        ValorRemesas: CERO,
                        ValorOtrosConceptos: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.ValorOtrosConceptos)),
                        ValorDescuentos: CERO,
                        Subtotal: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.ValorOtrosConceptos)),
                        ValorImpuestos: $scope.Modelo.ValorImpuestos,

                        ValorFactura: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.ValorFactura)),
                        ValorTRM: CERO,
                        Moneda: { Codigo: 1 },
                        ValorMonedaAlterna: CERO,
                        ValorAnticipo: CERO,

                        ResolucionFacturacion: $scope.Sesion.UsuarioAutenticado.Oficinas.ResolucionFacturacion,
                        ControlImpresion: 1,
                        Estado: $scope.Modelo.Estado.Codigo,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Anulado: CERO,
                        OtrosConceptos: $scope.ListaAuxOtrosConceptosFactura,
                        DetalleImpuestosFacturas: TmpDetalleImpuestosFacturas,
                        CuentaPorCobrar: CuentaPorCobrar
                    };
                    FacturasOtrosConceptosFactory.Guardar(Factura).
                        then(function (response) {
                            if (response.data.ProcesoExitoso == true) {
                                if (response.data.Datos > CERO) {
                                    if ($scope.Numero == CERO) {
                                        ShowSuccess('Se guardó la factura número ' + response.data.Datos + ' correctamente');
                                    }
                                    else {
                                        ShowSuccess('Se modificó la factura número ' + response.data.Datos + ' correctamente');
                                    }
                                    location.href = $scope.Master + '/' + response.data.Numero;
                                }
                                else {
                                    if (response.data.Datos == -1) {
                                        ShowError("La oficina no cuenta con consecutivo de facturación asignado");
                                    }
                                    else {
                                        ShowError(response.statusText);
                                    }
                                }
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo;

            if (Modelo.Fecha == undefined || Modelo.Fecha == '' || Modelo.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }
            if (Modelo.Cliente == undefined || Modelo.Cliente == '' || Modelo.Cliente == null) {
                $scope.MensajesError.push('Debe ingresar el cliente');
                continuar = false;
            }
            if (Modelo.FacturarA == undefined || Modelo.FacturarA == '' || Modelo.FacturarA == null) {
                $scope.MensajesError.push('Debe ingresar el tercero a facturar');
                continuar = false;
            }

            if (Modelo.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA) {//No Aplica
                $scope.MensajesError.push('Debe ingresar la forma de pago');
                continuar = false;
            }
            if (Modelo.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {//Credito
                if ($scope.Modelo.DiasPlazo <= CERO || $scope.Modelo.DiasPlazo == undefined || $scope.Modelo.DiasPlazo == '' || $scope.Modelo.DiasPlazo == null) {
                    $scope.MensajesError.push('Debe ingresar los días de plazo');
                    continuar = false;
                }
            }
            if ($scope.ListadoOtrosConceptosFactura.length == 0) {
                $scope.MensajesError.push('Debe ingresar al menos un concepto');
                continuar = false;
            }

            return continuar;
        }
        //----Guardar
        $scope.Calcular = function () {
            $scope.Modelo.ValorOtrosConceptos = 0;
            $scope.Modelo.ValorFactura = 0;
            $scope.Modelo.ValorImpuestos = 0;
            resetVistasImpuestos();

            //---------------------- Otros Conceptos ----------------------//
            for (var i = 0; i < $scope.ListadoOtrosConceptosFactura.length; i++) {
                if ($scope.ListadoOtrosConceptosFactura[i].ConceptoVenta.Operacion == 2) {
                    $scope.Modelo.ValorOtrosConceptos -= $scope.ListadoOtrosConceptosFactura[i].ValorVenta;
                } else {
                    $scope.Modelo.ValorOtrosConceptos += $scope.ListadoOtrosConceptosFactura[i].ValorVenta;
                }
            }

            var ImpuestosOtrosConceptos = [];
            for (var i = 0; i < $scope.ListadoOtrosConceptosFactura.length; i++) {
                for (var j = 0; j < $scope.ListadoConceptosFactura.length; j++) {
                    if ($scope.ListadoOtrosConceptosFactura[i].ConceptoVenta.Codigo == $scope.ListadoConceptosFactura[j].Codigo) {
                        $scope.ListadoOtrosConceptosFactura[i].ListadoImpuestos = angular.copy($scope.ListadoConceptosFactura[j].ListadoImpuestos);
                        ImpuestosOtrosConceptos = $scope.ListadoOtrosConceptosFactura[i].ListadoImpuestos;
                        for (var k = 0; k < ImpuestosOtrosConceptos.length; k++) {
                            if ($scope.ListadoOtrosConceptosFactura[i].ValorVenta >= ImpuestosOtrosConceptos[k].valor_base) {
                                ImpuestosOtrosConceptos[k].valor_base = $scope.ListadoOtrosConceptosFactura[i].ValorVenta;
                            }
                            else {
                                ImpuestosOtrosConceptos[k].valor_base = CERO;
                            }
                            ImpuestosOtrosConceptos[k].Valor_impuesto = Math.round(ImpuestosOtrosConceptos[k].valor_base * ImpuestosOtrosConceptos[k].Valor_tarifa);
                        }
                    }
                }
            }

            for (var i = 0; i < $scope.ListaImpuestosAgregadosConceptos.length; i++) {
                for (var j = 0; j < $scope.ListadoOtrosConceptosFactura.length; j++) {
                    ImpuestosOtrosConceptos = $scope.ListadoOtrosConceptosFactura[j].ListadoImpuestos;
                    if (ImpuestosOtrosConceptos != undefined) {
                        for (var k = 0; k < ImpuestosOtrosConceptos.length; k++) {
                            if ($scope.ListaImpuestosAgregadosConceptos[i].Codigo == ImpuestosOtrosConceptos[k].Codigo) {
                                $scope.ListaImpuestosAgregadosConceptos[i].valor_base += ImpuestosOtrosConceptos[k].valor_base;
                                $scope.ListaImpuestosAgregadosConceptos[i].Valor_impuesto += ImpuestosOtrosConceptos[k].Valor_impuesto;
                            }
                        }
                    }
                }
            }
            //---------------------- Otros Conceptos ----------------------//
            for (var i = 0; i < $scope.ListaImpuestosAgregadosConceptos.length; i++) {
                switch ($scope.ListaImpuestosAgregadosConceptos[i].Operacion) {
                    case 1:
                        $scope.Modelo.ValorImpuestos += $scope.ListaImpuestosAgregadosConceptos[i].Valor_impuesto;
                        break;
                    case 2:
                        $scope.Modelo.ValorImpuestos -= $scope.ListaImpuestosAgregadosConceptos[i].Valor_impuesto;
                        break;
                }
            }

            $scope.Modelo.ValorFactura = $scope.Modelo.ValorOtrosConceptos + $scope.Modelo.ValorImpuestos;
            /*Agrega Mascaras*/
            $scope.Modelo.ValorFactura = $scope.MaskValoresGrid($scope.Modelo.ValorFactura);
            $scope.Modelo.ValorOtrosConceptos = $scope.MaskValoresGrid($scope.Modelo.ValorOtrosConceptos);
            $scope.Modelo.ValorImpuestos = $scope.MaskValoresGrid($scope.Modelo.ValorImpuestos);
        };
        function resetVistasImpuestos() {
            //---Inicializa valor impuesto Concepto vista Factura
            $scope.ListaImpuestosAgregadosConceptos.forEach(function (item) {
                item.valor_base = CERO;
                item.Valor_impuesto = CERO;
            })
            //---Inicializa valor impuesto Concepto vista Factura
        }
        $scope.AsignarFormaPago = function (cod) {
            if (cod == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                $('#DiasPlazo').show()
            } else {
                $('#DiasPlazo').hide()
                $scope.Modelo.DiasPlazo = 0
            }
        }
        $scope.MostrarCriteriosBusqueda = function () {
            $('.CriteriosBusqueda').show();
        };

        $scope.VolverMaster = function () {
            document.location.href = $scope.Master + '/' + $scope.Numero;
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };
        $scope.MaskDecimalesGrid = function (item) {
            return MascaraDecimales(item)
        };
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope)
        }

       
        //---Modal Conceptos
        $scope.TmpEliminarConcepto = {};
        $scope.EsEditarConcepto = false;
        $scope.IndiceEditar = 0;
        function limpiarModalConceptoFactura() {
            $scope.Concepto = '';
            $scope.DescripcionConcepto = '';
            $scope.ValorConcepto = '';
            $scope.IndiceEditar = 0;
            $scope.EsEditarConcepto = false;
        }
        //---Restaura Todo los Valores
        $scope.InicializarVistaModalConceptoFactura = function () {
            $scope.MensajesErrorConceptos = [];
            showModal('modalConceptoFactura');
            limpiarModalConceptoFactura();
        };
        // Metodo para cerrar la modal de conceptos
        $scope.AsignarListadoConcepto = function (value) {
            return RetornarListaAutocomplete(value, $scope.ListadoConceptosFactura, 'Nombre', 'CodigoAlterno');
        };
        // Metodo para cerrar la modal de conceptos
        $scope.CerrarModalConcepto = function () {
            $scope.MensajesErrorRecorrido = [];
            closeModal('modalConceptoFactura');
        };

        //Inserta los conceptos en el grid de conceptos
        $scope.CargarConceptos = function () {
            if (DatosRequeridosConceptos()) {
                closeModal('modalConceptoFactura');
                if ($scope.EsEditarConcepto == true) {
                    $scope.ListadoOtrosConceptosFactura[$scope.IndiceEditar].NumeroRemesa = $scope.ENRE_Numero_OtrosConceptos;
                    $scope.ListadoOtrosConceptosFactura[$scope.IndiceEditar].ConceptoVenta = $scope.Concepto;
                    $scope.ListadoOtrosConceptosFactura[$scope.IndiceEditar].Descripcion = $scope.DescripcionConcepto;
                    $scope.ListadoOtrosConceptosFactura[$scope.IndiceEditar].ValorVenta = MascaraNumero(MascaraValores($scope.ValorConcepto));
                }
                else {
                    $scope.ListadoOtrosConceptosFactura.push({
                        DetalleNODE: { Codigo: CERO },
                        NumeroRemesa: { Numero: CERO, NumeroDocumento: CERO },
                        ConceptoVenta: $scope.Concepto,
                        Descripcion: $scope.DescripcionConcepto,
                        ValorVenta: MascaraNumero(MascaraValores($scope.ValorConcepto))
                    });
                }
                OrdenarIndicesOtrosConceptos();
                $scope.Calcular();
            }
        }
        //Inserta los conceptos en el grid de conceptos
        //Se establecen los datos requeridos de la modal de conceptos
        function DatosRequeridosConceptos() {
            $scope.MensajesErrorConceptos = [];
            var continuar = true;

            if ($scope.Concepto == undefined || $scope.Concepto == "" || $scope.Concepto == null) {
                $scope.MensajesErrorConceptos.push('Debe ingresar un concepto');
                continuar = false;
            }

            if ($scope.DescripcionConcepto == undefined || $scope.DescripcionConcepto == "" || $scope.DescripcionConcepto == null) {
                $scope.MensajesErrorConceptos.push('Debe ingresar una descripcion del concepto');
                continuar = false;
            }
            if ($scope.ValorConcepto == undefined || $scope.ValorConcepto == "" || $scope.ValorConcepto == null) {
                $scope.MensajesErrorConceptos.push('Debe ingresar  el valor del concepto');
                continuar = false;
            }

            return continuar;
        }
        //Se establecen los datos requeridos de la modal de conceptos
        //Eliminar conceptos facturas
        $scope.ConfirmacionEliminarConcepto = function (Concepto, indice) {
            $scope.TmpEliminarConcepto = {
                Concepto: Concepto,
                Indice: indice
            };
            showModal('modalEliminarConcepto');
        };

        $scope.EliminarConcepto = function (indice) {
            $scope.ListadoOtrosConceptosFactura.splice(indice, 1);
            OrdenarIndicesOtrosConceptos();
            $scope.Calcular();
            closeModal('modalEliminarConcepto');
        };
        $scope.EditarConcepto = function (indice) {
            $scope.ENRE_Numero_OtrosConceptos = $scope.ListadoOtrosConceptosFactura[indice].NumeroRemesa;
            $scope.Concepto = $scope.ListadoOtrosConceptosFactura[indice].ConceptoVenta;
            $scope.DescripcionConcepto = $scope.ListadoOtrosConceptosFactura[indice].Descripcion;
            $scope.ValorConcepto = $scope.ListadoOtrosConceptosFactura[indice].ValorVenta;
            $scope.MaskNumero(); $scope.MaskValores();
            $scope.IndiceEditar = indice;
            $scope.EsEditarConcepto = true;
            showModal('modalConceptoFactura');
        };
        function OrdenarIndicesOtrosConceptos() {
            for (var i = 0; i < $scope.ListadoOtrosConceptosFactura.length; i++) {
                $scope.ListadoOtrosConceptosFactura[i].indice = i;
            }
        }
        //Eliminar conceptos facturas
        //Modal Conceptos
        //---------------------------------Modal Eliminar Impuestos
        $scope.RestablecerImpuestos = function () {
            $scope.ListaImpuestosAgregadosConceptos = angular.copy($scope.ListadoImpuestosConceptosFactura);
            $scope.Calcular();
        };
        $scope.ConfirmacionEliminarImpuesto = function (item) {
            $scope.TmpEliminarImpuesto = item;
            showModal("modalEliminarImpuesto");
        };
        $scope.EliminarImpuesto = function (item) {
            closeModal("modalEliminarImpuesto");
            var tmp = [];
            //--Impuesto concepto
            for (i = 0; i < $scope.ListaImpuestosAgregadosConceptos.length; i++) {
                if (item.Codigo != $scope.ListaImpuestosAgregadosConceptos[i].Codigo) {
                    tmp.push($scope.ListaImpuestosAgregadosConceptos[i]);
                }
            }
            $scope.ListaImpuestosAgregadosConceptos = tmp;
            $scope.Calcular();
        };
        //---------------------------------Modal Eliminar Impuestos
    }]);
