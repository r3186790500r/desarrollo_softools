﻿Print 'gsp_consultar_detalle_despacho_fidelizacion_puntos_vehiculos'
GO
DROP PROCEDURE gsp_consultar_detalle_despacho_fidelizacion_puntos_vehiculos
GO
CREATE PROCEDURE gsp_consultar_detalle_despacho_fidelizacion_puntos_vehiculos
(
	@par_EMPR_Codigo smallint,
	@par_Codigo smallint = NULL,
	@par_Vehi_Codigo numeric = NULL,
	@par_TENE_Codigo numeric = NULL,
	@par_COND_Codigo numeric = NULL,
	@par_Planilla numeric = NULL,
	@par_Manifiesto numeric = NULL,
	@par_Fecha_Inicio_Vigencia_Desde datetime = NULL,
	@par_Fecha_Inicio_Vigencia_Hasta datetime = NULL,
	@par_Fecha_Fin_Vigencia_Desde datetime = NULL,
	@par_Fecha_Fin_Vigencia_Hasta datetime = NULL,
	@par_Estado smallint = NULL,
	@par_Anulado smallint = Null,
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL  
)
AS                                 
BEGIN                        
	SET NOCOUNT ON;                              
	DECLARE @CantidadRegistros INT                        
	SELECT                        
	@CantidadRegistros = (SELECT COUNT(1)

	FROM Detalle_Despachos_Plan_Puntos_Vehiculos DDPV

	INNER JOIN Vehiculos AS VEHI
	ON DDPV.EMPR_Codigo = VEHI.EMPR_Codigo
	AND DDPV.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS TENE
	ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Terceros AS COND
	ON VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo

	INNER JOIN Rutas AS RUTA
	ON DDPV.EMPR_Codigo = RUTA.EMPR_Codigo
	AND DDPV.RUTA_Codigo = RUTA.Codigo

	INNER JOIN Encabezado_Planilla_Despachos ENPD
	ON DDPV.EMPR_Codigo = ENPD.EMPR_Codigo
	AND DDPV.ENPD_Numero = ENPD.Numero

	INNER JOIN Encabezado_Manifiesto_Carga ENMC
	ON DDPV.EMPR_Codigo = ENMC.EMPR_Codigo
	AND DDPV.ENMC_Numero = ENMC.Numero

	LEFT JOIN Usuarios AS USUA
	ON DDPV.EMPR_Codigo = USUA.EMPR_Codigo
	AND DDPV.USUA_Codigo_Crea = USUA.Codigo
	
	WHERE DDPV.EMPR_Codigo = @par_EMPR_Codigo
	AND DDPV.ID = ISNULL(@par_Codigo, DDPV.ID)
	AND DDPV.VEHI_Codigo = ISNULL(@par_Vehi_Codigo, DDPV.VEHI_Codigo)
	AND TENE.Codigo = ISNULL(@par_TENE_Codigo, TENE.Codigo)
	AND COND.Codigo = ISNULL(@par_COND_Codigo, COND.Codigo)
	AND ENPD.Numero_Documento = ISNULL(@par_Planilla, ENPD.Numero_Documento)
	AND ENMC.Numero_Documento = ISNULL(@par_Manifiesto, ENMC.Numero_Documento)
	AND DDPV.Estado = ISNULL(@par_Estado, DDPV.Estado)
	AND DDPV.Anulado = ISNULL(@par_Anulado, DDPV.Anulado)
	AND DDPV.Fecha_Inicio >= ISNULL(@par_Fecha_Inicio_Vigencia_Desde, DDPV.Fecha_Inicio)
	AND DDPV.Fecha_Inicio <= ISNULL(@par_Fecha_Inicio_Vigencia_Hasta, DDPV.Fecha_Inicio)
	AND DDPV.Fecha_Vence >= ISNULL(@par_Fecha_Fin_Vigencia_Desde, DDPV.Fecha_Vence)
	AND DDPV.Fecha_Vence <= ISNULL(@par_Fecha_Fin_Vigencia_Hasta, DDPV.Fecha_Vence)
);                        
WITH Pagina
AS
(
	SELECT
	DDPV.EMPR_Codigo,
	DDPV.ID AS Codigo,
	VEHI.Codigo AS VEHI_Codigo,
	VEHI.Placa AS VEHI_Placa,
	ENPD.Numero AS ENPD_Numero,
	ENPD.Numero_Documento AS ENPD_NumeroDocumento,
	ENMC.Numero AS ENMC_Numero,
	ENMC.Numero_Documento AS ENMC_NumeroDocumento,
	RUTA.Codigo AS RUTA_Codigo,
	RUTA.Nombre AS RUTA_Nombre,
	ISNULL(TENE.Codigo, 0) AS TENE_Codigo,
	IIF(TENE.Nombre is NULL OR TENE.Nombre = '', TENE.Razon_Social, CONCAT(TENE.Nombre,' ', TENE.Apellido1)) AS TENE_Nombre,
	ISNULL(COND.Codigo, 0) AS COND_Codigo,
	IIF(COND.Nombre is NULL OR COND.Nombre = '', COND.Razon_Social, CONCAT(COND.Nombre,' ', COND.Apellido1)) AS COND_Nombre,
	ISNULL(DDPV.Fecha_Inicio,'') AS FechaInicio,
	ISNULL(DDPV.Fecha_Vence,'') AS FechaFin,
	ISNULL(DDPV.Puntos, 0) AS Puntos,
	DDPV.Estado,
	DDPV.Anulado,
	USUA.Codigo_Usuario as USUA_CodigoUsuario,
	DDPV.Fecha_Crea,
	ROW_NUMBER() OVER (ORDER BY DDPV.ID) AS RowNumber
	        
	FROM Detalle_Despachos_Plan_Puntos_Vehiculos DDPV

	INNER JOIN Vehiculos AS VEHI
	ON DDPV.EMPR_Codigo = VEHI.EMPR_Codigo
	AND DDPV.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS TENE
	ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Terceros AS COND
	ON VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo

	INNER JOIN Rutas AS RUTA
	ON DDPV.EMPR_Codigo = RUTA.EMPR_Codigo
	AND DDPV.RUTA_Codigo = RUTA.Codigo

	INNER JOIN Encabezado_Planilla_Despachos ENPD
	ON DDPV.EMPR_Codigo = ENPD.EMPR_Codigo
	AND DDPV.ENPD_Numero = ENPD.Numero

	INNER JOIN Encabezado_Manifiesto_Carga ENMC
	ON DDPV.EMPR_Codigo = ENMC.EMPR_Codigo
	AND DDPV.ENMC_Numero = ENMC.Numero

	LEFT JOIN Usuarios AS USUA
	ON DDPV.EMPR_Codigo = USUA.EMPR_Codigo
	AND DDPV.USUA_Codigo_Crea = USUA.Codigo

	WHERE DDPV.EMPR_Codigo = @par_EMPR_Codigo
	AND DDPV.ID = ISNULL(@par_Codigo, DDPV.ID)
	AND DDPV.VEHI_Codigo = ISNULL(@par_Vehi_Codigo, DDPV.VEHI_Codigo)
	AND TENE.Codigo = ISNULL(@par_TENE_Codigo, TENE.Codigo)
	AND COND.Codigo = ISNULL(@par_COND_Codigo, COND.Codigo)
	AND ENPD.Numero_Documento = ISNULL(@par_Planilla, ENPD.Numero_Documento)
	AND ENMC.Numero_Documento = ISNULL(@par_Manifiesto, ENMC.Numero_Documento)
	AND DDPV.Estado = ISNULL(@par_Estado, DDPV.Estado)
	AND DDPV.Anulado = ISNULL(@par_Anulado, DDPV.Anulado)
	AND DDPV.Fecha_Inicio >= ISNULL(@par_Fecha_Inicio_Vigencia_Desde, DDPV.Fecha_Inicio)
	AND DDPV.Fecha_Inicio <= ISNULL(@par_Fecha_Inicio_Vigencia_Hasta, DDPV.Fecha_Inicio)
	AND DDPV.Fecha_Vence >= ISNULL(@par_Fecha_Fin_Vigencia_Desde, DDPV.Fecha_Vence)
	AND DDPV.Fecha_Vence <= ISNULL(@par_Fecha_Fin_Vigencia_Hasta, DDPV.Fecha_Vence)
)                        
	SELECT                     
	EMPR_Codigo,
	Codigo,
	VEHI_Codigo,
	VEHI_Placa,
	ENPD_Numero,
	ENPD_NumeroDocumento,
	ENMC_Numero,
	ENMC_NumeroDocumento,
	RUTA_Codigo,
	RUTA_Nombre,
	TENE_Codigo,
	TENE_Nombre,
	COND_Codigo,
	COND_Nombre,
	FechaInicio,
	FechaFin,
	Puntos,
	Estado,
	Anulado,
	USUA_CodigoUsuario,
	Fecha_Crea,
	@CantidadRegistros AS TotalRegistros,
	@par_NumeroPagina AS PaginaObtener,
	@par_RegistrosPagina AS RegistrosPagina

	FROM Pagina                        
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                        
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                        
	--ORDER BY FechaInicio DESC, Codigo ASC                        
END
GO