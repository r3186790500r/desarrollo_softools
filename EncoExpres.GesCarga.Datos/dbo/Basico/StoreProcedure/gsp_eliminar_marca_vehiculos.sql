﻿PRINT 'gsp_eliminar_marca_vehiculos'
GO
DROP PROCEDURE gsp_eliminar_marca_vehiculos
GO
CREATE PROCEDURE gsp_eliminar_marca_vehiculos(
  @par_EMPR_Codigo SMALLINT,
  @par_Codigo NUMERIC
  )
AS
BEGIN
  
  DELETE Marca_Vehiculos 
  WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND Codigo = @par_Codigo

    SELECT @@ROWCOUNT AS Numero

END
GO