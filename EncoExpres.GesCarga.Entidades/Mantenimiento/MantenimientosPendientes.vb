﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Namespace MantenimientosPendientes

    ''' <summary>
    ''' Clase <see cref="MantenimientosPendientes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class MantenimientosPendientes
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="MantenimientosPendientes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="MantenimientosPendientes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Equipo = New EquipoMantenimiento With {.Codigo = Read(lector, "CodigoEquipo"), .Nombre = Read(lector, "Equipo")}
            PlanMantenimiento = New PlanEquipoMantenimiento With {.Codigo = Read(lector, "CodigoPlan"), .Nombre = Read(lector, "PlanMantenimiento")}
            Sistema = New Sistemas With {.Codigo = Read(lector, "CodigoSistema"), .Nombre = Read(lector, "Sistema")}
            Subsistema = New SubSistemas With {.Codigo = Read(lector, "CodigoSubsistema"), .Nombre = Read(lector, "Subsistema")}
            TipoSubsistema = New TipoSubsistemas With {.Codigo = Read(lector, "CodigoTipoSubsistema"), .Nombre = Read(lector, "TipoSubsistema")}
            Cantidad = Read(lector, "Cantidad")
            FechaUnitmoMantenimiento = Read(lector, "FechaUltimoMantenimiento")
            TotalRegistros = Read(lector, "TotalRegistros")

        End Sub

        <JsonProperty>
        Public Property Equipo As EquipoMantenimiento

        <JsonProperty>
        Public Property PlanMantenimiento As PlanEquipoMantenimiento

        <JsonProperty>
        Public Property Sistema As Sistemas

        <JsonProperty>
        Public Property Subsistema As SubSistemas

        <JsonProperty>
        Public Property TipoSubsistema As TipoSubsistemas

        <JsonProperty>
        Public Property Cantidad As Integer

        <JsonProperty>
        Public Property FechaUnitmoMantenimiento As Date



    End Class
End Namespace
