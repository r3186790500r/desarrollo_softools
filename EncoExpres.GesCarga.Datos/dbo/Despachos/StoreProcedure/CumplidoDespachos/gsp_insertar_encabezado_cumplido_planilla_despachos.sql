﻿PRINT 'gsp_insertar_encabezado_cumplido_planilla_despachos'
GO
DROP PROCEDURE gsp_insertar_encabezado_cumplido_planilla_despachos
GO
CREATE PROCEDURE gsp_insertar_encabezado_cumplido_planilla_despachos
(                  
	@par_EMPR_Codigo NUMERIC,                  
	@par_TIDO_Codigo NUMERIC,                  
	@par_Fecha DATE,                  
	@par_Fecha_Entrega DATETIME,                  
	@par_ENPD_Numero NUMERIC,                  
	@par_ENMC_Numero NUMERIC = NULL,                  
	@par_Fecha_Inicio_Cargue DATETIME,                  
	@par_Fecha_Fin_Cargue DATETIME,                  
	@par_Fecha_Inicio_Descargue DATETIME,                  
	@par_Fecha_Fin_Descargue DATETIME,                  
	@par_Fecha_Llegada_Cargue DATETIME = NULL,                  
	@par_Fecha_Llegada_Descargue DATETIME  = NULL,                  
	@par_Valor_Multa_Extemporaneo MONEY,                  
	@par_Nombre_Entrego_Cumplido VARCHAR(50),                  
	@par_Observaciones VARCHAR(500),                  
	@par_Estado SMALLINT,                  
	@par_USUA_Codigo_Crea SMALLINT,                  
	@par_OFIC_Codigo SMALLINT,    
	@par_VEHI_Codigo_Transbordo numeric  = NULL,     
	@par_TERC_Codigo_Tenedor_Transbordo numeric  = NULL,     
	@par_TERC_Codigo_Propietario_Transbordo numeric  = NULL,     
	@par_TERC_Codigo_Condcutor_Transbordo numeric  = NULL,     
	@par_SEMI_Codigo_Transbordo numeric  = NULL,     
	@par_CATA_TICM_Codigo numeric  = NULL,     
	@par_CATA_MOSM_Codigo numeric  = NULL,     
	@par_CATA_COSM_Codigo numeric  = NULL    
)                  
AS                  
BEGIN
	if (@par_ENMC_Numero is null)
	begin
		set @par_ENMC_Numero = 0
	end
	IF (
		SELECT COUNT (*) 
		FROM Encabezado_Cumplido_Planilla_Despachos 
		WHERE ENMC_Numero = ISNULL(@par_ENMC_Numero, ENMC_Numero) 
		AND ENPD_Numero = @par_ENPD_Numero 
		AND Anulado = 0 
		AND EMPR_Codigo = @par_EMPR_Codigo
		) > 0          
	BEGIN          
		SELECT -1 AS Numero, -1 as NumeroDocumento    --Planilla Cumplida      
	END          
	ELSE          
	BEGIN      
		IF(SELECT COUNT(*) FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENPD_Numero AND Anulado = 0 AND Estado = 1) <= 0        
		BEGIN        
			SELECT -2 AS Numero, -2 as NumeroDocumento   -- Planilla Anulada        
		END        
		ELSE        
		BEGIN      
			IF(SELECT COUNT(*) FROM Detalle_Seguimiento_Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_ENPD_Numero AND Fin_Viaje > 0) <= 0      
			BEGIN      
				SELECT -3 AS Numero, -3 as NumeroDocumento    -- Planilla Sin Viaje Finalizado      
			END      
			ELSE      
			BEGIN      
				DECLARE @NumeroDocumentoGenerado NUMERIC                  
				DECLARE @NumeroCumplido NUMERIC                  
                   
				EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, @par_OFIC_Codigo, @NumeroDocumentoGenerado OUTPUT                  
                  
				INSERT INTO        
				Encabezado_Cumplido_Planilla_Despachos        
				(                  
				EMPR_Codigo,                  
				TIDO_Codigo,                  
				Numero_Documento,                  
				Fecha,                  
				Fecha_Entrega,                  
				ENPD_Numero,                  
				ENMC_Numero,                  
				Fecha_Inicio_Cargue,                  
				Fecha_Fin_Cargue,                  
				Fecha_Inicio_Descargue,                  
				Fecha_Fin_Descargue,                  
				Valor_Multa_Extemporaneo,                  
				Nombre_Entrego_Cumplido,                  
				Observaciones,                  
				Anulado,                  
				Estado,                  
				Fecha_Crea,                  
				USUA_Codigo_Crea,                  
				OFIC_Codigo,                
				Fecha_Llegada_Cargue,                
				Fecha_Llegada_Descargue   ,    
				VEHI_Codigo_Transbordo,    
				TERC_Codigo_Tenedor_Transbordo,    
				TERC_Codigo_Propietario_Transbordo,    
				TERC_Codigo_Condcutor_Transbordo,    
				SEMI_Codigo_Transbordo,    
				CATA_TICM_Codigo,    
				CATA_MOSM_Codigo,    
				CATA_COSM_Codigo    
				)                  
				VALUES                  
				(                  
				@par_EMPR_Codigo,                  
				@par_TIDO_Codigo,                  
				@NumeroDocumentoGenerado,                  
				@par_Fecha,                  
				@par_Fecha_Entrega,                  
				@par_ENPD_Numero,                  
				@par_ENMC_Numero,            
				@par_Fecha_Inicio_Cargue,                  
				@par_Fecha_Fin_Cargue,                  
				@par_Fecha_Inicio_Descargue,                  
				@par_Fecha_Fin_Descargue,                  
				@par_Valor_Multa_Extemporaneo,                  
				@par_Nombre_Entrego_Cumplido,                  
				@par_Observaciones,                  
				0,--Anulado                  
				@par_Estado ,                  
				GETDATE(),                  
				@par_USUA_Codigo_Crea,                  
				@par_OFIC_Codigo,                
				@par_Fecha_Llegada_Cargue ,                  
				@par_Fecha_Llegada_Descargue,    
				@par_VEHI_Codigo_Transbordo,    
				@par_TERC_Codigo_Tenedor_Transbordo,    
				@par_TERC_Codigo_Propietario_Transbordo,    
				@par_TERC_Codigo_Condcutor_Transbordo,    
				@par_SEMI_Codigo_Transbordo,    
				@par_CATA_TICM_Codigo,    
				@par_CATA_MOSM_Codigo,    
				@par_CATA_COSM_Codigo    
				)                  
                  
				SELECT        
				@NumeroCumplido = @@identity              
                
				IF EXISTS (select * from Plan_Puntos_Vehiculos where EMPR_Codigo = @par_EMPR_Codigo and VEHI_Codigo = (SELECT VEHI_Codigo FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENPD_Numero ))              
				AND EXISTS(SELECT * FROM Validacion_Procesos WHERE  EMPR_Codigo = @par_EMPR_Codigo AND  PROC_Codigo = 5)              
				BEGIN
					INSERT INTO Detalle_Despachos_Plan_Puntos_Vehiculos        
					(              
					EMPR_Codigo,              
					VEHI_Codigo,              
					ENPD_Numero,              
					RUTA_Codigo,              
					ENMC_Numero,              
					Puntos,              
					Fecha_Inicio,              
					Fecha_Vence,              
					Estado,              
					USUA_Codigo_Crea,              
					Fecha_Crea,              
					USUA_Codigo_Modifica,              
					Fecha_Modifica,              
					Anulado,              
					USUA_Codigo_Anula,              
					Fecha_Anula,              
					Causa_Anula)              
					SELECT EMPR_Codigo,              
					VEHI_Codigo,              
					Numero,              
					RUTA_Codigo,              
					ENMC_Numero,              
					Peso/1000,              
					GETDATE(),              
					DATEADD(DAY,30,GETDATE()) ,              
					1,              
					@par_USUA_Codigo_Crea,              
					GETDATE(),              
					NULL,              
					NULL,              
					0,              
					NULL,              
					NULL,              
					NULL               
					FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENPD_Numero               
				END              
              
				select @NumeroCumplido as Numero,              
				@NumeroDocumentoGenerado AS NumeroDocumento       
			END          
		END               
	END                
END
GO