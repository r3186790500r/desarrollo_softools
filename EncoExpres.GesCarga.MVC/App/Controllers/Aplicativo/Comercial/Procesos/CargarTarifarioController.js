﻿EncoExpresApp.controller("CargarTarifarioCtrl", ['$scope', '$timeout', '$linq', 'TercerosFactory', 'blockUI', 'RemesaGuiasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'ProductoTransportadosFactory', 'CiudadesFactory', 'TarifarioVentasFactory', 'LineaNegocioTransportesFactory', 'TarifaTransportesFactory', 'TipoLineaNegocioTransportesFactory', 'TipoTarifaTransportesFactory', 'RutasFactory', 'TarifarioComprasFactory','blockUIConfig',
    function ($scope, $timeout, $linq, TercerosFactory, blockUI, RemesaGuiasFactory, ValorCatalogosFactory, TercerosFactory, ProductoTransportadosFactory, CiudadesFactory, TarifarioVentasFactory, LineaNegocioTransportesFactory, TarifaTransportesFactory, TipoLineaNegocioTransportesFactory, TipoTarifaTransportesFactory, RutasFactory, TarifarioComprasFactory,blockUIConfig) {
        console.clear()



        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.continuar = false
        $scope.VerErrores = false;
        $scope.DeshabilitarCliente = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.ListadoRutasPuntosGestion = []
        $scope.Modelo = {
            Fecha: new Date(),
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,

        }
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.MapaSitio = [{ Nombre: 'Comercial' }, { Nombre: 'Procesos' }, { Nombre: 'Cargue Tarifarios' }];
        $scope.ListadoTipoTarifario = [{ Nombre: 'TARIFARIO COMPRA', Codigo: 1 }, { Nombre: 'TARIFARIO VENTA', Codigo: 2 }]
        $scope.Tarifario = $scope.ListadoTipoTarifario[1]
        $scope.ListaReexpedicion = [];
        $scope.ListaReexpedicion = [
            { Codigo: 0, Nombre: 'NO' },
            { Codigo: 1, Nombre: 'SI' }
        ];
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaFormaPagoVentas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaFormaPagoVentas = response.data.Datos;
                    }
                    else {
                        $scope.ListaFormaPagoVentas = []
                    }
                }
            }, function (response) {
            });
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, AplicaTarifario: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoProductosTransportados = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoProductosTransportados = response.data.Datos;
                    }
                    else {
                        $scope.ListadoProductosTransportados = []
                    }
                }
            }, function (response) {
            });
        $scope.CargarTarifarios = function () {
            if ($scope.Tarifario.Codigo == 1) {//Compras
                TarifarioComprasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1 }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length >= 0) {
                                $scope.ListadoTarifarioVentas = [];

                                $scope.ListadoTarifarioVentas.push({ Nombre: '(NUEVO TARIFARIO)', Codigo: 0 })
                                for (var i = 0; i < response.data.Datos.length; i++) {
                                    response.data.Datos[i].Nombre = response.data.Datos[i].Nombre + ' (' + response.data.Datos[i].Codigo + ')'
                                    if (response.data.Datos[i].Codigo > 0) {
                                        $scope.ListadoTarifarioVentas.push(response.data.Datos[i])
                                    }
                                }
                                $scope.Tarifarios = $scope.ListadoTarifarioVentas[0]

                                LineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                                    then(function (response) {
                                        if (response.data.ProcesoExitoso === true) {
                                            $scope.ListadoLineaNegocioTransportes = [];
                                            if (response.data.Datos.length > 0) {
                                                for (var i = 0; i < response.data.Datos.length; i++) {
                                                    if (response.data.Datos[i].Codigo != 3) {
                                                        $scope.ListadoLineaNegocioTransportes.push(response.data.Datos[i])
                                                    }
                                                }
                                                $scope.ListadoLineaNegocioTransportesFiltrado = $scope.ListadoLineaNegocioTransportes
                                                $scope.LineaNegocioTransportes = $scope.ListadoLineaNegocioTransportesFiltrado[0]
                                                $scope.CargarTipoLineaCabecera($scope.LineaNegocioTransportes.Codigo)
                                            }
                                            else {
                                                $scope.ListadoLineaNegocioTransportes = []
                                            }
                                        }
                                    }, function (response) {
                                    });

                            }
                        }
                    })
            }
            if ($scope.Tarifario.Codigo == 2) {//Ventas
                TarifarioVentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1 }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length >= 0) {
                                $scope.ListadoTarifarioVentas = [];

                                $scope.ListadoTarifarioVentas.push({ Nombre: '(NUEVO TARIFARIO)', Codigo: 0 })
                                for (var i = 0; i < response.data.Datos.length; i++) {
                                    response.data.Datos[i].Nombre = response.data.Datos[i].Nombre + ' (' + response.data.Datos[i].Codigo + ')'
                                    if (response.data.Datos[i].Codigo > 0) {
                                        $scope.ListadoTarifarioVentas.push(response.data.Datos[i])
                                    }
                                }
                                $scope.Tarifarios = $scope.ListadoTarifarioVentas[0]
                                LineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                                    then(function (response) {
                                        if (response.data.ProcesoExitoso === true) {
                                            $scope.ListadoLineaNegocioTransportes = [];
                                            if (response.data.Datos.length > 0) {
                                                $scope.ListadoLineaNegocioTransportes = response.data.Datos
                                                $scope.ListadoLineaNegocioTransportesFiltrado = $scope.ListadoLineaNegocioTransportes
                                                $scope.LineaNegocioTransportes = $scope.ListadoLineaNegocioTransportesFiltrado[0]
                                                $scope.CargarTipoLineaCabecera($scope.LineaNegocioTransportes.Codigo)
                                            }
                                            else {
                                                $scope.ListadoLineaNegocioTransportes = []
                                            }
                                        }
                                    }, function (response) {
                                    });

                            }
                        }
                    })
            }

        }
        $scope.AsignarFechasTarifario = function () {
            if ($scope.Tarifarios.Codigo > 0) {
                $scope.Modelo.FechaInicio = new Date($scope.Tarifarios.FechaInicio)
                $scope.Modelo.FechaFin = new Date($scope.Tarifarios.FechaFin)
            }
        }
        $scope.CargarTarifarios()
        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVA" },
            { Codigo: 1, Nombre: "ACTIVA" },
        ]
        try {
            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado.Codigo);
            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                var tarifa = $scope.Modelo.Tarifas[i]
                for (var j = 0; j < $scope.ListadoEstados.length; j++) {
                    var estado = $scope.ListadoEstados[j]
                }
            }
        } catch (e) {
            $scope.Modelo.Estado = $scope.ListadoEstados[1]
        }


        //Ciudades
        CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaCiudadOrigen = response.data.Datos;
                        $scope.ListaCiudadDestino = response.data.Datos;
                    }
                    else {
                        $scope.ListaCiudadDescargue = [];
                        $scope.ListaCiudadCargue = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });



        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*Cargar el combo de TipoLineaNegocioTransportes*/
        TipoLineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoLineaNegocioTransportes = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoLineaNegocioTransportes = response.data.Datos;

                        $scope.CargarTipoLineaCabecera($scope.LineaNegocioTransportes.Codigo)
                    }
                    else {
                        $scope.ListadoTipoLineaNegocioTransportes = []
                    }
                }
            }, function (response) {
            });
        $scope.CargarTipoLineaCabecera = function (Codigo) {
            $scope.ListadoTipoLineaNegocioTransportesFiltrado = []
            $scope.ListadoTipoTarifaTransportesFiltrado = []
            $scope.ListadoTipoLineaNegocioTransportesFiltrado.push({ Nombre: '(NO APLICA)' });
            for (var i = 0; i < $scope.ListadoTipoLineaNegocioTransportes.length; i++) {
                var item = $scope.ListadoTipoLineaNegocioTransportes[i]
                if (item.LineaNegocioTransporte.Codigo == Codigo) {
                    $scope.ListadoTipoLineaNegocioTransportesFiltrado.push(item)
                }
            }
            $scope.TipoLineaNegocioTransportes = $scope.ListadoTipoLineaNegocioTransportesFiltrado[0]
        }
        /*Cargar el combo de TarifaTransportes*/
        TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTarifaTransportes = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTarifaTransportes = response.data.Datos;

                    }
                    else {
                        $scope.ListadoTarifaTransportes = []
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de TipoTarifaTransportes*/
        TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoTarifaTransportes = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoTarifaTransportes = response.data.Datos;

                    }
                    else {
                        $scope.ListadoTipoTarifaTransportes = []
                    }
                }
            }, function (response) {
            });
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoRutas = response.data.Datos
                }
            }, function (response) {
            });

        $scope.CargarTipoLinea = function (item) {
            item.ListadoTipoLineaNegocioTransportes = []
            item.ListadoTarifaTransportes = []
            item.ListadoTipoTarifaTransportes = []
            for (var j = 0; j < $scope.ListadoTipoLineaNegocioTransportes.length; j++) {
                var item2 = $scope.ListadoTipoLineaNegocioTransportes[j]
                if (item2.LineaNegocioTransporte.Codigo == item.LineaNegocioTransportes.Codigo) {
                    item.ListadoTipoLineaNegocioTransportes.push(item2)
                }
            }
            item.TipoLineaNegocioTransportes = item.ListadoTipoLineaNegocioTransportes[0]
            $scope.CargarTarifa(item)
        }
        $scope.CargarTarifa = function (item) {
            item.ListadoTarifaTransportes = []
            item.ListadoTipoTarifaTransportes = []
            for (var j = 0; j < $scope.ListadoTarifaTransportes.length; j++) {
                var item2 = $scope.ListadoTarifaTransportes[j]
                if (item2.TipoLineaNegocioTransporte.Codigo == item.TipoLineaNegocioTransportes.Codigo) {
                    item.ListadoTarifaTransportes.push(item2)
                }
            }
            item.TarifaTransportes = item.ListadoTarifaTransportes[0]
            $scope.CargarTipoTarifa(item)
        }
        $scope.CargarTipoTarifa = function (item) {
            item.ListadoTipoTarifaTransportes = []
            if (item.TarifaTransportes.Codigo == 300 || item.TarifaTransportes.Codigo == 6 || item.TarifaTransportes.Codigo == 302 || item.TarifaTransportes.Codigo == 5) { //BARRIL GALON TONELADA PRODUCTO
                item.ListadoTipoTarifaTransportes = angular.copy($scope.ListadoProductosTransportados)
            } else {
                for (var j = 0; j < $scope.ListadoTipoTarifaTransportes.length; j++) {
                    var item2 = $scope.ListadoTipoTarifaTransportes[j]
                    if (item2.TarifaTransporte.Codigo == item.TarifaTransportes.Codigo) {
                        item.ListadoTipoTarifaTransportes.push(item2)
                    }
                }
            }
            item.TipoTarifaTransportes = item.ListadoTipoTarifaTransportes[0]
        }

        $scope.FiltrarRutas = function () {
            $scope.ListadoRutasFiltrado = []
            var cteTipoRuta
            if ($scope.Modal.TipoLineaNegocioTransportes.Nombre.toUpperCase() == 'Urbana'.toUpperCase() || $scope.Modal.TipoLineaNegocioTransportes.Nombre.toUpperCase() == 'Urbano'.toUpperCase()) {
                cteTipoRuta = 4402
            } else if ($scope.Modal.TipoLineaNegocioTransportes.Nombre.toUpperCase() != '(NO APLICA)'.toUpperCase()) {
                cteTipoRuta = 4401
            }
            for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                var ruta = $scope.ListadoRutas[i]
                if (ruta.TipoRuta.Codigo == cteTipoRuta) {
                    $scope.ListadoRutasFiltrado.push(ruta)
                }
            }
            if ($scope.Modal.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL) {
                $scope.HabilitarCiudadOrigenDestino = true
                $scope.HabilitarCiudadUrbana = false
            }
            else if ($scope.Modal.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA) {
                $scope.HabilitarCiudadOrigenDestino = false
                $scope.HabilitarCiudadUrbana = true
            }
            else {
                $scope.HabilitarCiudadOrigenDestino = false
                $scope.HabilitarCiudadUrbana = false
            }
        }


        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.DataActual = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.DataActual = []
                for (var i = a - 10; i < $scope.DataArchivo.length; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }

        //Paginacion MOdal de errores

        $scope.PrimerPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10) {
                $scope.DataErrorActual = []
                $scope.paginaActualError = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.AnteriorModalError = function () {
            if ($scope.paginaActualError > 1) {
                $scope.paginaActualError -= 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteModalError = function () {
            if ($scope.paginaActualError < $scope.totalPaginasErrores) {
                $scope.paginaActualError += 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    $scope.UltimaPaginaModalError()

                }
            } else if ($scope.paginaActualError == $scope.totalPaginasErrores) {
                $scope.UltimaPaginaModalError()
            }
        }
        $scope.UltimaPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10 && $scope.totalPaginasErrores > 1) {
                $scope.paginaActualError = $scope.totalPaginasErrores
                var a = $scope.paginaActualError * 10
                $scope.DataErrorActual = []
                for (var i = a - 10; i < $scope.ListaErrores.length; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10) {
                $scope.ProgramacionesNoGuardadasActual = []
                $scope.paginaActualNoGuardadas = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }
        $scope.AnteriorNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas -= 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas < $scope.totalPaginasNoGuardadas) {
                $scope.paginaActualNoGuardadas += 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardados()

                }
            } else if ($scope.paginaActualNoGuardadas == $scope.totalPaginasNoGuardadas) {
                $scope.UltimaPaginaNoGuardados()
            }
        }
        $scope.UltimaPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10 && $scope.totalPaginasNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas = $scope.totalPaginasNoGuardadas
                var a = $scope.paginaActualNoGuardadas * 10
                $scope.ProgramacionesNoGuardadasActual = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10) {
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                $scope.paginaActualNoGuardadasRemplazo = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }
        $scope.AnteriorNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo -= 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo < $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.paginaActualNoGuardadasRemplazo += 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardadosRemplazo()

                }
            } else if ($scope.paginaActualNoGuardadasRemplazo == $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.UltimaPaginaNoGuardadosRemplazo()
            }
        }
        $scope.UltimaPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10 && $scope.totalPaginasNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo = $scope.totalPaginasNoGuardadasRemplazo
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }


        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardaras = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardadasRemplazo = function (item) {
            if ($scope.Option.name == 'Omitido') {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Omitido'
                }
            } else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Remplazado'
                }
            }

        }
        $scope.LimpiarData = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            $scope.DataVerificada = []
            $scope.DataActual = []
            $scope.DataErrorActual = []
            $scope.DataArchivo = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            $scope.VerErrores = false
        }

        /*-----------------------------------------------------------------------funciones de lectura y apertura de archivos----------------------------------------------------------------------------*/

        $scope.ListaProgramaciones = []
        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined) {
                return true
            } else {
                return false
            }
        }
        function AsignarValoresTabla() {
            $scope.NumeroCargue = 0

            if ($scope.DataArchivo.Valores.length > 0) {
                var DataTemporal = []
                for (var i = 0; i < $scope.DataArchivo.Valores.length; i++) {
                    var item = $scope.DataArchivo.Valores[i]

                    if (!Eval(item.Tarifa) || !Eval(item.Tarifa) || !Eval(item.Tarifa)
                    ) {
                        DataTemporal.push(item)
                    }
                }
                $scope.DataArchivo = DataTemporal
                $scope.totalRegistros = ($scope.DataArchivo.length)
                $scope.paginaActual = 1
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                blockUI.start();
                blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');
                $timeout(function () {
                    try {
                        var data = []
                        for (var i = 0; i < $scope.DataArchivo.length; i++) {
                            var DataItem = $scope.DataArchivo[i]
                            DataItem.Tipo_Linea_Negocio = $scope.TipoLineaNegocioTransportes.Codigo
                            DataItem.Linea_Negocio = $scope.LineaNegocioTransportes.Codigo
                            if (DataItem.Linea_Negocio !== undefined && DataItem.Linea_Negocio !== '' && DataItem.Linea_Negocio !== '0') {
                                try { DataItem.LineaNegocioTransportes = $linq.Enumerable().From($scope.ListadoLineaNegocioTransportes).First('$.Codigo ==' + $scope.LineaNegocioTransportes.Codigo); } catch (e) { }
                            }

                            if (DataItem.Tipo_Linea_Negocio !== undefined && DataItem.Tipo_Linea_Negocio !== '' && DataItem.Tipo_Linea_Negocio !== '0') {
                                DataItem.ListadoTipoLineaNegocioTransportes = []
                                try {
                                    for (var j = 0; j < $scope.ListadoTipoLineaNegocioTransportes.length; j++) {
                                        var item = $scope.ListadoTipoLineaNegocioTransportes[j]
                                        if (item.LineaNegocioTransporte.Codigo == DataItem.LineaNegocioTransportes.Codigo) {
                                            DataItem.ListadoTipoLineaNegocioTransportes.push(item)
                                        }
                                    }
                                    try { DataItem.TipoLineaNegocioTransportes = $linq.Enumerable().From(DataItem.ListadoTipoLineaNegocioTransportes).First('$.Codigo == "' + DataItem.Tipo_Linea_Negocio + '"'); } catch (e) { }
                                } catch (e) {

                                }
                            }
                            if (DataItem.LineaNegocioTransportes.Codigo == 3) {
                                if (DataItem.Ciudad_Origen !== undefined && DataItem.Ciudad_Origen !== '' && DataItem.Ciudad_Origen !== '0') {
                                    try { DataItem.CiudadOrigen = $linq.Enumerable().From($scope.ListaCiudadOrigen).First('$.Codigo ==' + DataItem.Ciudad_Origen); } catch (e) { }
                                }
                                try {
                                    if (DataItem.TipoLineaNegocioTransportes.Codigo == 301) {
                                        if (DataItem.Ciudad_Destino !== undefined && DataItem.Ciudad_Destino !== '' && DataItem.Ciudad_Destino !== '0') {
                                            try { DataItem.CiudadDestino = $linq.Enumerable().From($scope.ListaCiudadDestino).First('$.Codigo ==' + DataItem.Ciudad_Destino); } catch (e) { }
                                        }
                                    }
                                } catch (e) {
                                    try { DataItem.CiudadDestino = $linq.Enumerable().From($scope.ListaCiudadDestino).First('$.Codigo ==' + DataItem.Ciudad_Destino); } catch (e) { }
                                }
                                if (DataItem.Forma_Pago !== undefined && DataItem.Forma_Pago !== '' && DataItem.Forma_Pago !== '0') {
                                    try { DataItem.FormaPagoVentas = $linq.Enumerable().From($scope.ListaFormaPagoVentas).First('$.Codigo ==' + DataItem.Forma_Pago); } catch (e) { }
                                }
                            }
                            if (DataItem.LineaNegocioTransportes.Codigo != 3) {
                                if (DataItem.Ruta !== undefined && DataItem.Ruta !== '' && DataItem.Ruta !== '0') {
                                    try { DataItem.Ruta = $linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + DataItem.Ruta); } catch (e) { }
                                }
                            }
                            if (DataItem.Tarifa !== undefined && DataItem.Tarifa !== '' && DataItem.Tarifa !== '0') {
                                DataItem.ListadoTarifaTransportes = []
                                try {
                                    for (var j = 0; j < $scope.ListadoTarifaTransportes.length; j++) {
                                        var item = $scope.ListadoTarifaTransportes[j]
                                        if (item.TipoLineaNegocioTransporte.Codigo == DataItem.TipoLineaNegocioTransportes.Codigo) {
                                            DataItem.ListadoTarifaTransportes.push(item)
                                        }
                                    }
                                    try { DataItem.TarifaTransportes = $linq.Enumerable().From(DataItem.ListadoTarifaTransportes).First('$.Nombre == "' + DataItem.Tarifa + '"'); } catch (e) { }
                                } catch (e) {

                                }

                            }
                            if (DataItem.Tipo_Tarifa !== undefined && DataItem.Tipo_Tarifa !== '' && DataItem.Tipo_Tarifa !== '0') {
                                DataItem.ListadoTipoTarifaTransportes = []
                                try {
                                    if (DataItem.TarifaTransportes.Codigo == 300 || DataItem.TarifaTransportes.Codigo == 6 || DataItem.TarifaTransportes.Codigo == 302 || DataItem.TarifaTransportes.Codigo == 5) {
                                        DataItem.ListadoTipoTarifaTransportes = angular.copy($scope.ListadoProductosTransportados)
                                    } else {
                                        for (var j = 0; j < $scope.ListadoTipoTarifaTransportes.length; j++) {
                                            var item = $scope.ListadoTipoTarifaTransportes[j]
                                            if (item.TarifaTransporte.Codigo == DataItem.TarifaTransportes.Codigo) {
                                                DataItem.ListadoTipoTarifaTransportes.push(item)
                                            }
                                        }
                                    }
                                    try { DataItem.TipoTarifaTransportes = $linq.Enumerable().From(DataItem.ListadoTipoTarifaTransportes).First('$.Codigo == ' + parseInt(DataItem.Tipo_Tarifa)); } catch (e) { }
                                } catch (e) {

                                }
                            }
                            if (DataItem.Valor_flete !== undefined && DataItem.Valor_flete !== '') {
                                DataItem.ValorFlete = $scope.MaskValoresGrid(DataItem.Valor_flete)
                            }
                            if (DataItem.Valor_flete_transportador !== undefined && DataItem.Valor_flete_transportador !== '') {
                                try {
                                    DataItem.ValorFleteTransportador = $scope.MaskValoresGrid(DataItem.Valor_flete_transportador)
                                } catch (e) {
                                }
                            }
                            if (DataItem.Reexpedicion !== undefined && DataItem.Reexpedicion !== '') {
                                try {
                                    DataItem.Reexpedicion = $linq.Enumerable().From($scope.ListaReexpedicion).First('$.Codigo==' + DataItem.Reexpedicion)
                                } catch (e) {
                                }
                            } else {
                                DataItem.Reexpedicion = $linq.Enumerable().From($scope.ListaReexpedicion).First('$.Codigo==0')
                            }
                            if (DataItem.Porcentaje_Reexpedicion !== undefined && DataItem.Porcentaje_Reexpedicion !== '') {
                                try {
                                    DataItem.PorcentajeReexpedicion = $scope.MaskValoresGrid(DataItem.Porcentaje_Reexpedicion)
                                } catch (e) {
                                }
                            }
                            if (DataItem.Porcentaje_Reexpedicion !== undefined && DataItem.Porcentaje_Reexpedicion !== '') {
                                try {
                                    DataItem.PorcentajeReexpedicion = $scope.MaskValoresGrid(DataItem.Porcentaje_Reexpedicion)
                                } catch (e) {
                                }
                            }
                            if (DataItem.Porcentaje_Manejo !== undefined && DataItem.Porcentaje_Manejo !== '') {
                                try {
                                    DataItem.PorcentajeManejo = DataItem.Porcentaje_Manejo
                                } catch (e) {
                                }
                            }
                            try {
                                if (DataItem.LineaNegocioTransportes.Codigo != 3) {
                                    if (DataItem.ValorEscolta !== undefined && DataItem.Valor_Escolta !== '') {
                                        DataItem.ValorEscolta = $scope.MaskValoresGrid(DataItem.Valor_Escolta)
                                    }
                                }
                            } catch (e) {
                            }
                            $scope.DataArchivo[i] = DataItem
                        }
                        if ($scope.totalRegistros > 10) {
                            for (var i = 0; i < 10; i++) {
                                $scope.DataActual.push($scope.DataArchivo[i])
                            }
                        } else {
                            $scope.DataActual = $scope.DataArchivo
                        }
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    } catch (e) {
                        ShowError('Error en la lectura del archivo por favor intente nuevamente')
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    }


                }, 1000)
            }

        }
        var conterroresprevalidar = 0

        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            if ($scope.DataArchivo.Valores != undefined) {
                $('#TablaCarge').show();
                $('#btnValida').show();
                if (parseInt($scope.DataArchivo.Lineas_Negocio[0].Codigo) !== $scope.LineaNegocioTransportes.Codigo || parseInt($scope.DataArchivo.Tipo_Linea_Negocio[0].Codigo) !== $scope.TipoLineaNegocioTransportes.Codigo) {
                    ShowError('El archivo seleccionado no coincide con la linea y/o tipo linea ingresado')
                    blockUI.stop()
                } else {
                    $scope.DataArchivo = { Valores: $scope.DataArchivo.Valores }
                    AsignarValoresTabla()
                }



            }
            else {
                ShowError('El arhivo cargado no corresponde al formato de carge masivo del tarifario')
            }
            blockUI.stop()

        }

        $scope.handleFile = function (e) {
            var files = e.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = e.target.result;
                $scope.arr = fixdata(data);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(btoa($scope.arr), { type: 'base64' });
                        process_wb($scope.wb);
                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);
            });
        }

        function restrow(item) {
            item.stRow = ''
            item.stGeneral = ''
            item.msGeneral = ''
            item.stLineaNegocioTransportes = ''
            item.msLineaNegocioTransportes = ''
            item.stTipoLineaNegocioTransportes = ''
            item.msTipoLineaNegocioTransportes = ''
            item.stCiudadOrigen = ''
            item.msCiudadOrigen = ''
            item.stCiudadDestino = ''
            item.msCiudadDestino = ''
            item.stRuta = ''
            item.msRuta = ''
            item.stTarifaTransportes = ''
            item.msTarifaTransportes = ''
            item.stTipoTarifaTransportes = ''
            item.msTipoTarifaTransportes = ''
            item.stFormaPagoVentas = ''
            item.msFormaPagoVentas = ''

        }

        $scope.ValidarDocumento = function () {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Validando Información...");
            $timeout(function () { blockUI.message("Validando Información..."); $scope.ValidarDocumento1(); }, 100);
        }

        $scope.ValidarDocumento1 = function () {
            $scope.ListaErrores = []
            $scope.MensajesError = []
            /*
                        blockUI.start();
                        $timeout(function() {
                            blockUI.message('Validando Archivo');
                        }, 1000);*/
            var contadorgeneral = 0
            var contadorRegistros = 0
            $scope.DataVerificada = []
            $scope.DataVerificadatmp = []
            for (var i = 0; i < $scope.DataArchivo.length; i++) {
                var item = $scope.DataArchivo[i]
                item.ID = i
                //resetea todos los estilos de la fila que se va a evaluar
                restrow(item);
                var errores = 0
                //Condicion para saber si aplican las validaciones (Si el registro no tiene una ruta seleccionada no validara el resto de campos)
                if (item.Omitido !== true) {
                    contadorRegistros++
                    if (item.LineaNegocioTransportes == undefined || item.LineaNegocioTransportes == '') {
                        item.stLineaNegocioTransportes = 'background:red'
                        item.msLineaNegocioTransportes = 'Ingrese la linea de transporte'
                        errores++
                    }
                    if (item.TipoLineaNegocioTransportes == undefined || item.TipoLineaNegocioTransportes == '') {
                        item.stTipoLineaNegocioTransportes = 'background:red'
                        item.msTipoLineaNegocioTransportes = 'Ingrese el tipo linea de transporte'
                        errores++
                    }
                    try {
                        if (item.LineaNegocioTransportes.Codigo == 3) {
                            item.Ruta = undefined
                            if (item.CiudadOrigen == undefined || item.CiudadOrigen == '') {
                                item.stCiudadOrigen = 'background:red'
                                item.msCiudadOrigen = 'Ingrese la ciudad origen'
                                errores++
                            }
                            try {
                                if (item.TipoLineaNegocioTransportes.Codigo == 301) {
                                    if (item.CiudadDestino == undefined || item.CiudadDestino == '') {
                                        item.stCiudadDestino = 'background:red'
                                        item.msCiudadDestino = 'Ingrese la ciudad destino'
                                        errores++
                                    }
                                }
                            } catch (e) {
                                item.CiudadDestino = undefined
                            }
                            if (item.FormaPagoVentas == undefined || item.FormaPagoVentas == '') {
                                item.stFormaPagoVentas = 'background:red'
                                item.msFormaPagoVentas = 'Ingrese la forma de pago'
                                errores++
                            }
                        } else {
                            item.CiudadOrigen = undefined
                            item.CiudadDestino = undefined
                        }

                    } catch (e) {

                    }
                    if (item.LineaNegocioTransportes.Codigo != 3) {
                        item.FormaPagoVentas == undefined
                        item.CiudadOrigen = undefined
                        item.CiudadDestino = undefined
                        if (item.Ruta == undefined || item.Ruta == '') {
                            item.stRuta = 'background:red'
                            item.msRuta = 'Ingrese la ruta'
                            errores++
                        }
                    }
                    if (item.TarifaTransportes == undefined || item.TarifaTransportes == '') {
                        item.stTarifaTransportes = 'background:red'
                        item.msTarifaTransportes = 'Ingrese la tarifa transportes'
                        errores++
                    }
                    if (item.TipoTarifaTransportes == undefined || item.TipoTarifaTransportes == '') {
                        if (item.TarifaTransportes.Codigo == 301) {
                            item.TipoTarifaTransportes = { Codigo: 0, TarifaTransporte: item.TarifaTransportes }
                        } else {
                            item.stTipoTarifaTransportes = 'background:red'
                            item.msTipoTarifaTransportes = 'Ingrese el tipo tarifa transportes'
                            errores++
                        }
                    }

                    if (errores > 0) {
                        item.stRow = 'background:yellow'
                        $scope.ListaErrores.push(item)
                        contadorgeneral++
                    }
                    else {
                        item.stRow = ''
                        var cont = 0
                        //for (var j = 0; j < $scope.DataVerificada.length; j++) {
                        //    item2 = $scope.DataVerificada[j]
                        //    if (j != i) {
                        //        if (item2.LineaNegocioTransportes.Codigo == item.LineaNegocioTransportes.Codigo && item2.TipoLineaNegocioTransportes.Codigo == item.TipoLineaNegocioTransportes.Codigo && item2.TarifaTransportes.Codigo == item.TarifaTransportes.Codigo && item2.TipoTarifaTransportes.Codigo == item.TipoTarifaTransportes.Codigo) {
                        //            try {
                        //                if (item2.Ruta.Codigo == item.Ruta.Codigo) {
                        //                    cont++
                        //                    item.stRow = 'background:yellow'
                        //                    item.stGeneral = 'background:red'
                        //                    item.msGeneral = 'Este registro se encuentra duplicado'
                        //                    $scope.ListaErrores.push(item)
                        //                    contadorgeneral++
                        //                    break;
                        //                }
                        //            } catch (e) {
                        //                try {
                        //                    if (item2.CiudadOrigen.Codigo == item.CiudadOrigen.Codigo && item2.CiudadDestino.Codigo == item.CiudadDestino.Codigo) {
                        //                        cont++
                        //                        item.stRow = 'background:yellow'
                        //                        item.stGeneral = 'background:red'
                        //                        item.msGeneral = 'Este registro se encuentra duplicado'
                        //                        $scope.ListaErrores.push(item)
                        //                        contadorgeneral++
                        //                        break;
                        //                    }
                        //                } catch (e) {
                        //                    try {
                        //                        if (item2.CiudadOrigen.Codigo == item.CiudadOrigen.Codigo && item2.CiudadDestino == undefined && item.CiudadDestino == undefined) {
                        //                            cont++
                        //                            item.stRow = 'background:yellow'
                        //                            item.stGeneral = 'background:red'
                        //                            item.msGeneral = 'Este registro se encuentra duplicado'
                        //                            $scope.ListaErrores.push(item)
                        //                            contadorgeneral++
                        //                            break;
                        //                        }
                        //                    } catch (e) {

                        //                    }
                        //                }

                        //            }
                        //        }
                        //    }
                        //}
                        if (cont == 0) {
                            $scope.DataVerificada.push(item)
                        }
                    }
                }
                else {
                    restrow(item);
                }
            }
            blockUI.stop();

            if (contadorgeneral > 0) {
                ShowError('Se encontraron inconsistencias en los datos ingresados, por favor corrija los datos marcados o seleccione omitirlos.');
                $scope.paginaActualError = 1
                $scope.DataErrorActual = []
                $scope.totalRegistrosErrores = $scope.ListaErrores.length
                $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 10);
                if ($scope.totalRegistrosErrores > 10) {
                    for (var i = 0; i < 10; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    for (var i = 0; i < $scope.ListaErrores.length; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                if (DatosRequeridos()) {
                    if (contadorRegistros == 0) {
                        ShowError('No se encontraron registros para validar, por favor revise que la casilla de omitir no se encuentre marcada en al menos un registro')
                    } else {
                        showModal('modalConfirmacionGuardarPorgramacion')
                        $scope.MSJ = 'Los datos se verificaron correctamente. \n  ¿Desea continuar con el cargue de la información?'
                    }
                }
            }


        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var modelo = $scope.Modelo
            if ($scope.Tarifarios.Codigo == 0) {

                if (ValidarCampo(modelo.Nombre) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el nombre ');
                    continuar = false;
                }
                if (ValidarFecha(modelo.FechaInicio) == FECHA_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar la fecha de inicio ');
                    continuar = false;
                }
                if (ValidarFecha(modelo.FechaFin) == FECHA_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar la fecha fin ');
                    continuar = false;
                } else if (ValidarFecha(modelo.FechaFin, false, true) == FECHA_ES_MENOR) {
                    $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha actual');
                    continuar = false;
                }
                if (ValidarFecha(modelo.FechaInicio) == FECHA_VALIDA && ValidarFecha(modelo.FechaFin, false, true) == FECHA_VALIDA) {
                    if (modelo.FechaInicio > modelo.FechaFin) {
                        $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha de inicio');
                        continuar = false;
                    }
                }
            } else {
                modelo.Nombre = $scope.Tarifarios.Nombre
                modelo.Codigo = $scope.Tarifarios.Codigo
                modelo.FechaInicio = $scope.Tarifarios.FechaInicio
                modelo.FechaInicio = $scope.Tarifarios.FechaInicio
            }
            return continuar;
        }
        $scope.AsignarTarifario = function () {
            if ($scope.Tarifarios.Codigo > 0) {
                $scope.Modelo.Nombre = $scope.Tarifarios.Nombre
                $scope.Modelo.Codigo = $scope.Tarifarios.Codigo
                $scope.Modelo.FechaInicio = $scope.Tarifarios.FechaInicio
                $scope.Modelo.FechaInicio = $scope.Tarifarios.FechaInicio
            }
        }

        $scope.Guardar = function () {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Guardando Tarifas...");
            $timeout(function () { blockUI.message("Guardando Tarifas..."); $scope.Guardar1(); }, 100);
        }
        $scope.Guardar1 = function () {
            closeModal('modalConfirmacionGuardarPorgramacion')

            $scope.Modelo.TarifarioBase
            $scope.Modelo.Codigo = $scope.Tarifarios.Codigo
            $scope.Modelo.OpcionCargue = 1
            $scope.Modelo.Tarifas = []
            for (var i = 0; i < $scope.DataVerificada.length; i++) {
                var item = $scope.DataVerificada[i]
                item.TipoTarifaTransportes.TarifaTransporte = item.TarifaTransportes
                $scope.Modelo.Tarifas.push(
                    {
                        TipoLineaNegocioTransportes: {
                            Codigo: item.TipoLineaNegocioTransportes.Codigo,
                            LineaNegocioTransporte: { Codigo: item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo }
                        },
                        Ruta: item.Ruta,
                        CiudadOrigen: { Codigo: item.Ruta == undefined ? item.CiudadOrigen.Codigo : item.Ruta.CiudadOrigen.Codigo },
                        CiudadDestino: { Codigo: item.Ruta == undefined ? item.CiudadDestino.Codigo : item.Ruta.CiudadDestino.Codigo},
                        TipoTarifaTransportes: {
                            Codigo: item.TipoTarifaTransportes.Codigo,
                            TarifaTransporte: { Codigo: item.TipoTarifaTransportes.TarifaTransporte.Codigo }
                        },
                        ValorFlete: item.ValorFlete,
                        ValorFleteTransportador: item.ValorFleteTransportador,
                        ValorEscolta: item.ValorEscolta,                      
                        FormaPagoVentas: { Codigo: item.FormaPagoVentas == undefined || item.FormaPagoVentas == null ? 4901 : item.FormaPagoVentas.Codigo },
                       
                        FechaInicioVigencia: $scope.Modelo.FechaInicio,
                        FechaFinVigencia: $scope.Modelo.FechaFin,
                        Estado: { Codigo: 1 },
                        Reexpedicion: item.Reexpedicion.Codigo,
                        PorcentajeAfiliado: item.PorcentajeReexpedicion == undefined ? 0 : parseFloat(item.PorcentajeReexpedicion),
                        PorcentajeSeguro: item.PorcentajeManejo == undefined ? 0 : parseFloat(item.PorcentajeManejo)
                        //ValorCargue: item.AcarreoLocal,
                        //ValorDescargue: item.AcarreoDestino
                    }
                )
            }
            //Estado
            if ($scope.Tarifario.Codigo == 1) {//Compras
                TarifarioComprasFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el tarifario compras "' + $scope.Modelo.Nombre + '"');
                                    blockUI.stop()
                                }
                                else {
                                    ShowSuccess('Se modificó el tarifario ventas "' + $scope.Modelo.Nombre + '"');
                                    blockUI.stop()
                                }
                                location.href = '#!ConsultarTarifarioCompras/' + $scope.Modelo.Codigo;
                            }
                            else {
                                ShowError(response.statusText);
                                blockUI.stop()
                            }
                        }
                        else {
                            ShowError(response.statusText);
                            blockUI.stop()
                        }
                    }, function (response) {
                            ShowError(response.statusText);
                            blockUI.stop()
                    });
                blockUI.stop()
            }
            if ($scope.Tarifario.Codigo == 2) {//Ventas
                TarifarioVentasFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el tarifario ventas "' + $scope.Modelo.Nombre + '"');
                                    blockUI.stop()
                                }
                                else {
                                    ShowSuccess('Se modificó el tarifario ventas "' + $scope.Modelo.Nombre + '"');
                                    blockUI.stop()
                                }
                                blockUI.stop()
                                location.href = '#!ConsultarTarifarioVentas/' + $scope.Modelo.Codigo;
                            }
                            else {
                                ShowError(response.statusText);
                                blockUI.stop()
                            }
                        }
                        else {
                            ShowError(response.statusText);
                            blockUI.stop()
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        blockUI.stop()
                    });
            }
            blockUI.stop();
        }
        $scope.GuardarErrados = function () {
            //closeModal('ModalErrores')
            data = []
            for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                if (!$scope.ProgramacionesNoGuardadas[i].Omitido && $scope.ProgramacionesNoGuardadas[i].Option != 'Omitido') {
                    if ($scope.ProgramacionesNoGuardadas[i].Option == 'Remplazado') {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 1
                    } else {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 0
                    }
                    data.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
            $scope.DataVerificada = data
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        var contGuard = 0
        var contGuardGeneral = 0
        var contGeneral = 0
        var Guardaros = 0
        function GuardarCompleto(inicio, opt) {
            contGuard++
            if (contGuard > $scope.DataVerificada.length) {

                ShowSuccess('Se guardaron ' + contGuardGeneral + ' de ' + contGeneral + ' remesas')
                closeModal('modalConfirmacionRemplazarProgramacion');
                closeModal('ModalErrores');
                $scope.LimpiarData()

            } else {

                blockUI.start();
                $timeout(function () {
                    blockUI.message('Guardando Tarifario');
                }, 1000);
                RemesaGuiasFactory.Guardar($scope.DataVerificada[inicio].Remesa).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        Guardaros++
                        contGuardGeneral++
                        GuardarCompleto(contGuard)
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $scope.ProgramacionesNoGuardadas.push($scope.DataVerificada[inicio])
                    GuardarCompleto(contGuard)
                    blockUI.stop();
                })
            }
        }


        $scope.ModalConfirmacionNuevoProceso = function () {
            showModal('modalConfirmacionCancelarProceso');
        }

        $scope.CerrarModalNuevoProceso = function (e) {
            var fileVal = document.getElementById("fileModal");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
            closeModal('modalConfirmacionCancelarProceso');


        }

        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (o) {
            return MascaraNumero(o)
        }
        $scope.MaskValoresGrid = function (o) {
            return MascaraValores(o)
        }
        $scope.MaskMayusGrid = function (o) {
            return MascaraMayus(o)
        }
        $scope.MaskplacaGrid = function (o) {
            return MascaraPlaca(o)
        }
        $scope.MaskplacaSemiremolqueGrid = function (o) {
            return MascaraPlacaSemirremolque(o)
        }

        $scope.GenerarPlanilla = function () {

            var entidad = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, LineaNegocioTransporte: $scope.LineaNegocioTransportes, TipoLineaNegocioTransportes: $scope.TipoLineaNegocioTransportes }
            blockUI.start();
            $timeout(function () {
                blockUI.message('Generando Plantilla..');
            }, 1000);
            TarifarioVentasFactory.GenerarPlanitilla(entidad).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var pdfAsDataUri = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data.Datos.Planitlla;
                    var link = document.createElement('a');
                    link.href = pdfAsDataUri;
                    link.download = "Plantilla_Cargue_Masivo_Tarifario.xlsx";
                    link.click();
                    //window.open(pdfAsDataUri);
                    blockUI.stop();
                    ShowSuccess('La plantilla se generó correctamente')
                }
            }, function (response) {
                blockUI.stop();
                ShowError(response.statusText)
            })

        }


        $scope.MaskPorcentaje = function (value, item) {
            var numeroarray = [];
            for (var i = 0; i < value.length; i++) {
                patron = /[0123456789.]/
                if (i == 0 && value[i] == '-') {
                    numeroarray.push(value[i])
                }
                else if (patron.test(value[i])) {
                    if (patron.test(value[i]) == ' ') {
                        option[i] = '';
                    } else {
                        numeroarray.push(value[i])
                    }
                }
            }

            var val = numeroarray.join('');
            item = val;
            var nval = [];
            if (val[1] != '.') {
                if (val.length == 3) {
                    if (val[0] != 1 && val[0] != "1" && val[2] != ".") {
                        var MaxLength = 3;
                        nval.push(val[0]);
                        nval.push(val[1]);
                        item = nval.join('');
                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        }
                    } else if (val[2] == ".") {
                        MaxLength = 5;
                    }
                    else if ((val[0] == 1 || val[0] == "1") && val[2] != ".") {
                        MaxLength = 3;

                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        }
                    }

                } else if (val.length > 3) {
                    if (val[2] == ".") {
                        if (val.length == 4) {
                            nval.push(val[0]);
                            nval.push(val[1]);
                            nval.push(val[2]);
                            if (val[3] != ".") {
                                nval.push(val[3]);
                            }
                            item = nval.join('');
                        } else if (val.length >= 5) {
                            nval.push(val[0]);
                            nval.push(val[1]);
                            nval.push(val[2]);
                            if (val[3] != ".") {
                                nval.push(val[3]);
                                if (val[4] != ".") {
                                    nval.push(val[4]);
                                }
                            }

                            item = nval.join('');
                        }
                    } else {
                        nval.push(val[0]);
                        nval.push(val[1]);
                        nval.push(val[2]);
                        item = nval.join('');
                    }

                }
            } else {
                nval.push(val[0]);
                nval.push(val[1]);
                if (val[2] != ".") {
                    nval.push(val[2]);
                    if (val[3] != ".") {
                        nval.push(val[3]);
                    }
                }


                item = nval.join('');
                MaxLength = 4;
            }
            return item;
        }


    }]);

