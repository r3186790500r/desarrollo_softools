﻿PRINT 'gsp_obtener_puesto_controles'
GO
DROP PROCEDURE gsp_obtener_puesto_controles
GO
CREATE PROCEDURE gsp_obtener_puesto_controles    
(    
	@par_EMPR_Codigo SMALLINT,    
	@par_Codigo numeric = NULL
)    
AS    
BEGIN    

    
	SELECT    
	1 AS Obtener,
		EMPR_Codigo,    
		Codigo,
		ISNULL(Codigo_Alterno,'') AS Codigo_Alterno, 
		ISNULL(Nombre,'')AS Nombre,     
		ISNULL(Contacto,'')AS Contacto,     
		ISNULL(Telefono,'')AS Telefono,     
		ISNULL(Celular,'')AS Celular,
		TERC_Codigo_Proveedor,     
		ISNULL(Ubicacion,'') AS Ubicacion, 
		ISNULL(Estado,'') AS Estado
	FROM    
		Puesto_Controles   
	WHERE    
		EMPR_Codigo = @par_EMPR_Codigo    
		AND Codigo = ISNULL(@par_Codigo,Codigo)    
END    
go