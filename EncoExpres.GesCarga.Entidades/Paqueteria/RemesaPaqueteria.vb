﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Paqueteria
    <JsonObject>
    Public NotInheritable Class RemesaPaqueteria
        Inherits BaseDocumento


        Sub New()

        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Remesa = New Remesas With {.Numero = Read(lector, "Numero")}
            Remesa.NumeroDocumento = Read(lector, "Numero_Documento")
            Remesa.Fecha = Read(lector, "Fecha")
            Remesa.Observaciones = Read(lector, "Observaciones")
            Remesa.NumeroDocumentoCliente = Read(lector, "Documento_Cliente")
            Estado = Read(lector, "Estado")
            Remesa.FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FOPR_Codigo")}
            Remesa.CiudadRemitente = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Origen"), .Nombre = Read(lector, "CiudadOrigen")}
            Remesa.CiudadDestinatario = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino"), .Nombre = Read(lector, "CiudadDestino")}
            OficinaActual = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Actual"), .Nombre = Read(lector, "NombreOficinaActual")}
            OficinaEntrega = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Entrega"), .Nombre = Read(lector, "OFIC_Nombre_Entrega")}
            UsuarioEntrega = Read(lector, "Usuario_Entrega")
            EstadoGuia = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESRP_Codigo"), .Nombre = Read(lector, "NombreEstadoRemesa")}
            Secuencia = Read(lector, "Secuencia")
            Remesa.Remitente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Remitente")}
            Numeracion = Read(lector, "Numeracion")
            If Read(lector, "Obtener") = 0 Then
                Remesa.TotalFleteCliente = Read(lector, "Total_Flete_Cliente")
                Remesa.ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
                Remesa.ValorSeguroCliente = Read(lector, "Valor_Seguro_Cliente")
                Remesa.CantidadCliente = Read(lector, "Cantidad_Cliente")
                Remesa.PesoCliente = Read(lector, "Peso_Cliente")
                NumeroPlanilla = Read(lector, "NumeroDocumentoPlanilla")
                NumeroPlanillaRecoleccion = Read(lector, "PlanillaRecoleccion")
                'Remesa.Detalle = New DetalleRemesas With {.Destinatario = New Terceros With {.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino")}}}
                'Remesa.Detalle = New DetalleRemesas With {.Cumplido = Read(lector, "Cumplido")}
                TotalRegistros = Read(lector, "TotalRegistros")
                TipoDocumento = Read(lector, "TipoDocumento")
                OficinaOrigen = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Origen"), .Nombre = Read(lector, "NombreOficinaOrigen")}
                PesoVolumetrico = Read(lector, "Peso_Volumetrico")
                PesoCobrar = Read(lector, "Peso_A_Cobrar")
            Else
                Remesa.FechaDocumentoCliente = Read(lector, "Fecha_Documento_Cliente")
                Remesa.DetalleTarifaVenta = New DetalleTarifarioVentas With {.Codigo = Read(lector, "DTCV_Codigo")}
                Remesa.DetalleTarifaVenta.TipoLineaNegocioTransportes = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo")}


                Remesa.PesoCliente = Read(lector, "Peso_Cliente")
                Remesa.CantidadCliente = Read(lector, "Cantidad_Cliente")
                Remesa.ValorComercialCliente = Read(lector, "Valor_Comercial_Cliente")
                Remesa.PesoVolumetricoCliente = Read(lector, "Peso_Volumetrico_Cliente")
                TipoEntregaRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TERP_Codigo")}
                TipoTransporteRemsaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TTRP_Codigo")}
                TipoDespachoRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TDRP_Codigo")}
                TemperaturaProductoRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TPRP_Codigo")}
                TipoServicioRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TSRP_Codigo")}
                TipoInterfazRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIRP_Codigo")}
                FechaInterfazTracking = Read(lector, "Fecha_Interfaz_Cargue_Archivo")
                FechaInterfazWMS = Read(lector, "Fecha_Interfaz_Cargue_WMS")
                Remesa.Remitente.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Remitente")}
                Remesa.Remitente.Direccion = Read(lector, "Direccion_Remitente")


                'Remesa.Detalle = New DetalleRemesas

                Remesa.ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
                Remesa.ValorManejoCliente = Read(lector, "Valor_Manejo_Cliente")
                Remesa.ValorSeguroCliente = Read(lector, "Valor_Seguro_Cliente")
                Remesa.ValorDescuentoCliente = Read(lector, "Valor_Descuento_Cliente")
                Remesa.TotalFleteCliente = Read(lector, "Total_Flete_Cliente")
                Remesa.TotalFleteTransportador = Read(lector, "Total_Flete_Transportador")
                Remesa.DetalleTarifaVenta.NumeroTarifario = Read(lector, "ETCV_Numero")
                CodigoTipoLineaNegocio = Read(lector, "TLNC_Codigo")
                CodigoLineaNegocio = Read(lector, "LNTC_Codigo")

                Remesa.Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo")}
                CodigoTipoTarifaTransporte = Read(lector, "TTTC_Codigo")
                NombreTipoTarifa = Read(lector, "NombreTipoTarifa")
                CodigoTarifaTransporte = Read(lector, "TATC_Codigo")
                NombreTarifa = Read(lector, "NombreTarifa")


                CodigoTipoRemesa = Read(lector, "CATA_TIRE_Codigo")
                AplicaReexpedicion = Read(lector, "Reexpedicion")

                CodigoTipoIdentificacionRemitente = Read(lector, "TIID_Remitente")

                Remesa.BarrioRemitente = Read(lector, "Barrio_Remitente")
                Remesa.CodigoPostalRemitente = Read(lector, "Codigo_Postal_Remitente")
                Remesa.ObservacionesRemitente = Read(lector, "Observaciones_Remitente")


                CodigoTipoIdentificacionDestinatario = Read(lector, "TIID_Destinatario")

                DescripcionProductoTransportado = Read(lector, "Descripcion_Mercancia")


                OficinaOrigen = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Origen"), .Nombre = Read(lector, "NombreOficinaOrigen")}
                OficinaDestino = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Destino"), .Nombre = Read(lector, "NombreOficinaDestino")}



            End If
            OficinaOrigen = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Origen"), .Nombre = Read(lector, "NombreOficinaOrigen")}
            Remesa.Remitente.Telefonos = Read(lector, "Telefonos_Remitente")
            Remesa.Remitente.TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "Tipo_Identificacion_Remitente")}
            Remesa.Remitente.NumeroIdentificacion = Read(lector, "Identificacion_Remitente")
            Remesa.Remitente.CorreoFacturacion = Read(lector, "Correo_Remitente")
            PlanillaEntrega = Read(lector, "PlanillaEntrega")
            Remesa.RemesaCortesia = Read(lector, "Remesa_Cortesia")
            Remesa.Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .NombreCompleto = Read(lector, "NombreCliente")}
            Remesa.ProductoTransportado = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "NombreProducto")}
            Remesa.Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "VEHI_Placa"), .CodigoAlterno = Read(lector, "VEHI_CodigoAlterno")}
            Remesa.Conductor = New Terceros With {.NombreCompleto = Read(lector, "NombreConductor")}
            Remesa.FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FOPR_Codigo"), .Nombre = Read(lector, "FormaPago")}
            Remesa.ValorFleteTransportador = Read(lector, "Valor_Flete_Transportador")
            LineaSerivicioCliente = New ValorCatalogos With {.Codigo = Read(lector, "CATA_LISC_Codigo")}
            SitioCargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Cargue"), .Nombre = Read(lector, "SitioCargue")}
            SitioDescargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Descargue"), .Nombre = Read(lector, "SitioDescargue")}
            CentroCosto = Read(lector, "Centro_Costo")
            FechaEntrega = Read(lector, "Fecha_Entrega")
            Remesa.Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario"), .Nombre = Read(lector, "NombreDestinatario")}
            'Remesa.Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario")}
            Remesa.Destinatario.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destinatario")}
            Remesa.Destinatario.Direccion = Read(lector, "Direccion_Destinatario")
            Remesa.Destinatario.Telefonos = Read(lector, "Telefonos_Destinatario")
            Remesa.Destinatario.NumeroIdentificacion = Read(lector, "Identificacion_Destinatario")
            Remesa.Destinatario.Codigo = Read(lector, "TERC_Codigo_Destinatario")
            Remesa.Destinatario.Nombre = Read(lector, "NombreDestinatario")
            Remesa.Destinatario.NombreCompleto = Read(lector, "NombreDestinatario")
            Remesa.Destinatario.TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "Tipo_Identificacion_Destinatario")}
            Remesa.Destinatario.CorreoFacturacion = Read(lector, "Correo_Destinatario")
            Remesa.Remitente.Codigo = Read(lector, "TERC_Codigo_Remitente")
            Remesa.Remitente.Nombre = Read(lector, "NombreRemitente")
            Remesa.Remitente.NombreCompleto = Read(lector, "NombreRemitente")



            Remesa.BarrioDestinatario = Read(lector, "Barrio_Destinatario")
            Remesa.Cumplido = Read(lector, "Cumplido")

            Remesa.CodigoPostalDestinatario = Read(lector, "Codigo_Postal_Destinatario")
            Remesa.ObservacionesDestinatario = Read(lector, "Observaciones_Destinatario")
            Remesa.FechaRecibe = Read(lector, "Fecha_Recibe")
            Remesa.CantidadRecibe = Read(lector, "Cantidad_Recibe")
            Remesa.PesoRecibe = Read(lector, "Peso_Recibe")
            Remesa.NumeroIdentificacionRecibe = Read(lector, "Numero_Identificacion_Recibe")
            Remesa.NombreRecibe = Read(lector, "Nombre_Recibe")
            Remesa.TelefonoRecibe = Read(lector, "Telefonos_Recibe")
            Remesa.Firma = Read(lector, "Firma_Recibe")
            Remesa.ObservacionesRecibe = Read(lector, "Observaciones_Recibe")
            Remesa.ValorComisionOficina = Read(lector, "Valor_Comision_Oficina")
            Remesa.ValorComisionAgencista = Read(lector, "Valor_Comision_Agencista")
            Remesa.NovedadEntrega = New ValorCatalogos With {.Nombre = Read(lector, "NovedadRecibe"), .Codigo = Read(lector, "CATA_NERP_Codigo")}

            Remesa.UsuarioModifica = New Usuarios With {.Nombre = Read(lector, "Usuario_Modifica")}

            Nombre = Read(lector, "Nombre")
            Cantidad = Read(lector, "Cantidad")
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "NombreOficina")}
            Anulado = Read(lector, "Anulado")
            ValorReexpedicion = Read(lector, "Valor_Reexpedicion")
            Largo = Read(lector, "Largo")
            Alto = Read(lector, "Alto")
            Ancho = Read(lector, "Ancho")
            PesoVolumetrico = Read(lector, "Peso_Volumetrico")
            PesoCobrar = Read(lector, "Peso_A_Cobrar")
            FletePactado = Read(lector, "Flete_Pactado")
            AjusteFlete = Read(lector, "Ajuste_Flete")
            Reexpedicion = Read(lector, "Reexpedicion")
            LineaNegocioPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_LNPA_Codigo"), .Nombre = Read(lector, "CATA_LNPA_Nombre")}
            RecogerOficinaDestino = Read(lector, "Recoger_Oficina_Destino")
            ManejaDetalleUnidades = Read(lector, "Maneja_Detalle_Unidades")
            Zona = New Zonas With {.Nombre = Read(lector, "Zona")}
            CodigoZona = Read(lector, "ZOCI_Codigo_Entrega")
            Latitud = Read(lector, "Latitud")
            Longitud = Read(lector, "Longitud")
            HorarioEntregaRemesa = New ValorCatalogos With {.Codigo = Read(lector, "CATA_HERP_Codigo")}
            LiquidarUnidad = Read(lector, "Liquidar_Unidad")
            Remesa.ValorOtros = Read(lector, "Valor_Otros")
            Remesa.ValorCargue = Read(lector, "Valor_Cargue")
            Remesa.ValorDescargue = Read(lector, "Valor_Descargue")
            OficinaRecibe = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Recibe"), .Nombre = Read(lector, "OFIC_Nombre_Recibe")}
            Factura = New Facturas With {.Numero = Read(lector, "ENFA_Numero"), .NumeroDocumento = Read(lector, "ENFA_Numero_Documento")}
            Devolucion = Read(lector, "Devolucion")
            FechaDevolucion = Read(lector, "Fecha_Devolucion")
            NumeroPlanilla = Read(lector, "NumeroDocumentoPlanilla")
            FechaPlanillaInicial = Read(lector, "FechaPlanilla")
            FechaPlanillaFinal = Read(lector, "FechaPlanillaActual")
            NumeroDocumentoLegalizarGuias = Read(lector, "ELGU_NumeroDocumento")
            NumeroDocumentoLegalizarRecaudo = Read(lector, "ELRG_NumeroDocumento")
            Aforador = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Aforador")}
            OficinaRegistroManual = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Registro_Manual")}
            FechaEstadoRemesaPaqueteria = Read(lector, "Fecha_Estado_Remesa")
            OficinaEstadoRemesa = New Oficinas With {.Nombre = Read(lector, "Oficina_Nombre_Estado_Remesa")}
            GuiaCreadaRutaConductor = Read(lector, "Guia_Creada_Ruta_Conductor")
            GestionDocumentosLegalizar = Read(lector, "Gestion_Documentos_Legalizar")
            EntregaRecaudoLegalizar = Read(lector, "Entrega_Recaudo_Legalizar")
            OficinaDestino = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Destino"), .Nombre = Read(lector, "NombreOficinaDestino")}
            listaDeZonas = Read(lector, "Lista_Zonas")
            TipoProcesoGuia = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TPGP_Codigo")}
            ResponsableLegalizarRecaudo = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Responsable"), .NombreCompleto = Read(lector, "TERC_Nombre_Responsable")}
            NumeroEtiquetasAutomaticas = Read(lector, "Numero_Etiquetas_Automaticas")
        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        ''' 
        <JsonProperty>
        Public Property Planilla As PlanillaPaqueteria
        <JsonProperty>
        Public Property listaDeZonas As ArrayList
        <JsonProperty>
        Public Property Numeracion As String
        <JsonProperty>
        Public Property Remesa As Remesas
        <JsonProperty>
        Public Property TransportadorExterno As Terceros
        <JsonProperty>
        Public Property BarrioDestinatario As String
        <JsonProperty>
        Public Property NumeroGuiaExterna As String
        <JsonProperty>
        Public Property TipoTransporteRemsaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property TipoDespachoRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property TemperaturaProductoRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property TipoServicioRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property TipoEntregaRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property EstadoRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property TipoInterfazRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property FechaInterfazTracking As Date
        <JsonProperty>
        Public Property FechaInterfazWMS As Date
        <JsonProperty>
        Public Property NumeroPlanillaInicial As Long
        <JsonProperty>
        Public Property NumeroPlanillaFinal As Long
        <JsonProperty>
        Public Property FechaPlanillaInicial As Date
        <JsonProperty>
        Public Property FechaPlanillaFinal As Date
        <JsonProperty>
        Public Property NumeroPlanilla As Long
        <JsonProperty>
        Public Property NumeroPlanillaRecoleccion As Long
        <JsonProperty>
        Public Property CodigoTipoLineaNegocio As Integer
        <JsonProperty>
        Public Property CodigoLineaNegocio As Integer
        <JsonProperty>
        Public Property CodigoTipoTarifaTransporte As Integer
        <JsonProperty>
        Public Property CodigoTarifaTransporte As Integer
        <JsonProperty>
        Public Property NombreTipoTarifa As String
        <JsonProperty>
        Public Property NombreTarifa As String
        <JsonProperty>
        Public Property CumplidoGuiasPaqueteria As IEnumerable(Of CumplidoRemesa)
        <JsonProperty>
        Public Property Cumplido As String
        <JsonProperty>
        Public Property ObservacionesRemitente As String
        <JsonProperty>
        Public Property ObservacionesDestinatario As String
        <JsonProperty>
        Public Property CodigoOficinaActual As Double
        <JsonProperty>
        Public Property RemesaPaqueteria As IEnumerable(Of RemesaPaqueteria)
        <JsonProperty>
        Public Property actualizaestado As Short
        <JsonProperty>
        Public Property AsociarRemesasPlanilla As Short
        <JsonProperty>
        Public Property AsociarRemesasRecolecciones As Short
        <JsonProperty>
        Public Property AsociarRemesasEntrega As Short
        <JsonProperty>
        Public Property ConsultarControlEntregas As Short
        <JsonProperty>
        Public Property PlanillaEntrega As Long
        <JsonProperty>
        Public Property OficinaOrigen As Oficinas
        <JsonProperty>
        Public Property OficinaDestino As Oficinas
        <JsonProperty>
        Public Property OficinaActual As Oficinas
        <JsonProperty>
        Public Property OficinaEntrega As Oficinas
        <JsonProperty>
        Public Property DesripcionMercancia As String
        <JsonProperty>
        Public Property UsuarioEntrega As String
        <JsonProperty>
        Public Property CadenaGuias As String
        <JsonProperty>
        Public Property Reexpedicion As Integer
        <JsonProperty>
        Public Property CodigoOficina As Integer
        <JsonProperty>
        Public Property CodigoTipoRemesa As Integer
        <JsonProperty>
        Public Property CodigoZona As Integer
        <JsonProperty>
        Public Property CodigoCiudad As Integer
        <JsonProperty>
        Public Property CodigoCiudadActual As Integer
        <JsonProperty>
        Public Property CodigoOficinaActualUsuario As Integer
        <JsonProperty>
        Public Property CodigoCiudadOrigen As Integer
        <JsonProperty>
        Public Property AplicaReexpedicion As Byte
        <JsonProperty>
        Public Property EstadoGuia As ValorCatalogos
        <JsonProperty>
        Public Property CodigoTipoIdentificacionDestinatario As Integer
        <JsonProperty>
        Public Property CodigoTipoIdentificacionRemitente As Integer
        <JsonProperty>
        Public Property DescripcionProductoTransportado As String
        <JsonProperty>
        Public Property ListaReexpedicionOficinas As IEnumerable(Of ReexpedicionRemesasPaqueteria)
        <JsonProperty>
        Public Property Planitlla As Byte()
        <JsonProperty>
        Public Property CodigoCiudadPlanillaRecoleccion As Integer
        <JsonProperty>
        Public Property CodigoOficinaPlanillaRecoleccion As Integer



        <JsonProperty>
        Public Property LineaSerivicioCliente As ValorCatalogos
        <JsonProperty>
        Public Property CentroCosto As String
        <JsonProperty>
        Public Property SitioCargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property SitioDescargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property FechaEntrega As DateTime
        <JsonProperty>
        Public Property ConsultaNovedades As Short
        <JsonProperty>
        Public Property InidicadorPedidoNovedades As Short
        <JsonProperty>
        Public Property InidicadorEntregas As Short
        <JsonProperty>
        Public Property InidicadorErroresAtribuibles As Short
        <JsonProperty>
        Public Property InidicadorPuntosEntregados As Short
        <JsonProperty>
        Public Property InidicadorCajasEntregadas As Short

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Cantidad As Long

        <JsonProperty>
        Public Property Factura As Facturas
        <JsonProperty>
        Public Property ValorReexpedicion As Double
        <JsonProperty>
        Public Property Largo As Double
        <JsonProperty>
        Public Property Alto As Double
        <JsonProperty>
        Public Property Ancho As Double
        <JsonProperty>
        Public Property PesoVolumetrico As Double
        <JsonProperty>
        Public Property PesoCobrar As Double
        <JsonProperty>
        Public Property FletePactado As Double
        <JsonProperty>
        Public Property AjusteFlete As Short
        <JsonProperty>
        Public Property LineaNegocioPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property RecogerOficinaDestino As Integer
        <JsonProperty>
        Public Property OficinaRecibe As Oficinas
        <JsonProperty>
        Public Property ManejaDetalleUnidades As Integer
        <JsonProperty>
        Public Property DetalleUnidades As IEnumerable(Of DetalleUnidades)
        <JsonProperty>
        Public Property Latitud As Double
        <JsonProperty>
        Public Property Longitud As Double
        <JsonProperty>
        Public Property HorarioEntregaRemesa As ValorCatalogos
        <JsonProperty>
        Public Property Secuencia As Long
        <JsonProperty>
        Public Property Zona As Zonas

        <JsonProperty>
        Public Property DocumentosCumplidoRemsa As IEnumerable(Of Documentos)
        <JsonProperty>
        Public Property IndicadorPaqueteria As Integer
        <JsonProperty>
        Public Property LiquidarUnidad As Integer
        <JsonProperty>
        Public Property Devolucion As String
        <JsonProperty>
        Public Property FechaDevolucion As Date
        <JsonProperty>
        Public Property GestionDocumentosRemesa As IEnumerable(Of GestionDocumentosRemesa)
        <JsonProperty>
        Public Property NumeroDocumentoLegalizarRecaudo As Integer
        <JsonProperty>
        Public Property Aforador As Terceros
        <JsonProperty>
        Public Property OficinaRegistroManual As Oficinas
        <JsonProperty>
        Public Property ListadoDocumentos As IEnumerable(Of Documentos)
        <JsonProperty>
        Public Property NumeroDocumentoPlanillaCargue As Integer
        <JsonProperty>
        Public Property FechaEstadoRemesaPaqueteria As DateTime
        <JsonProperty>
        Public Property OficinaEstadoRemesa As Oficinas
        <JsonProperty>
        Public Property GuiaCreadaRutaConductor As Integer
        <JsonProperty>
        Public Property NumeroDocumentoLegalizarGuias As Integer
        <JsonProperty>
        Public Property ModificarLegalizarGuias As Integer
        <JsonProperty>
        Public Property NumeroEtiquetasAutomaticas As Integer
        <JsonProperty>
        Public Property DetalleEtiquetasPreimpresas As IEnumerable(Of Integer)
        <JsonProperty>
        Public Property TipoProcesoGuia As ValorCatalogos
        <JsonProperty>
        Public Property ResponsableLegalizarRecaudo As Terceros
#Region "legalizacion Guias"
        <JsonProperty>
        Public Property GestionDocumentosLegalizar As Integer
        <JsonProperty>
        Public Property EntregaRecaudoLegalizar As Integer
#End Region

        <JsonProperty>
        Public Property ListaTipoTarifaCargaVenta As IEnumerable(Of DetalleTarifaRemesa)
        <JsonProperty>
        Public Property detalleTarifa As DetalleTarifaRemesa
        <JsonProperty>
        Public Property parametrosCalculos As ParametrosCalculosRemesa

    End Class

    Public Class DetalleTarifaRemesa
        Inherits Base

        Sub New()
        End Sub

        <JsonProperty>
        Public Property Codigo As Integer
        <JsonProperty>
        Public Property CodigoDetalleTarifa As Integer
        <JsonProperty>
        Public Property CodigoTarifa As Integer
        <JsonProperty>
        Public Property CondicionesComerciales As Boolean
        <JsonProperty>
        Public Property FleteMinimo As Double
        <JsonProperty>
        Public Property ManejoMinimo As Double
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property PesoMaximo As Double
        <JsonProperty>
        Public Property PesoMinimo As Double
        <JsonProperty>
        Public Property PorcentajeAfiliado As Double
        <JsonProperty>
        Public Property PorcentajeFlete As Double
        <JsonProperty>
        Public Property PorcentajeReexpedicion As Double
        <JsonProperty>
        Public Property PorcentajeSeguro As Double
        <JsonProperty>
        Public Property PorcentajeValorDeclarado As Double
        <JsonProperty>
        Public Property PorcentajeVolumen As Double
        <JsonProperty>
        Public Property Reexpedicion As Boolean
        <JsonProperty>
        Public Property ValorFlete As Double
        <JsonProperty>
        Public Property ValorManejo As Double
        <JsonProperty>
        Public Property ValorSeguro As Double

    End Class
    Public Class ParametrosCalculosRemesa
        Inherits Base

        Sub New()
        End Sub

        <JsonProperty>
        Public Property OficinaObjPorcentajeSeguroPaqueteria As Double
        <JsonProperty>
        Public Property ListadoDetalleUnidadesValorFlete As IEnumerable(Of Double)
        <JsonProperty>
        Public Property PaqueteriaMinimoValorManejo As Double
        <JsonProperty>
        Public Property PaqueteriaMinimoValorSeguro As Double
        <JsonProperty>
        Public Property PaqueteriaFleteMinimo As Double
        <JsonProperty>
        Public Property ManejoDetalleUnidades As Boolean
        <JsonProperty>
        Public Property ManejoReexpedicionOficina As Boolean
        <JsonProperty>
        Public Property Tarifario As TarifarioVentas
        <JsonProperty>
        Public Property calculo As Boolean
    End Class

End Namespace
