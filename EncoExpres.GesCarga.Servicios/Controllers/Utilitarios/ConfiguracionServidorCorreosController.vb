﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Utilitarios
Imports EncoExpres.GesCarga.Negocio.Utilitarios
<Authorize>
Public Class ConfiguracionServidorCorreosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ConfiguracionServidorCorreos) As Respuesta(Of IEnumerable(Of ConfiguracionServidorCorreos))
        Return New LogicaConfiguracionServidorCorreos(CapaPersistenciaConfiguracionServidorCorreos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ConfiguracionServidorCorreos) As Respuesta(Of ConfiguracionServidorCorreos)
        Return New LogicaConfiguracionServidorCorreos(CapaPersistenciaConfiguracionServidorCorreos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ConfiguracionServidorCorreos) As Respuesta(Of Long)
        Return New LogicaConfiguracionServidorCorreos(CapaPersistenciaConfiguracionServidorCorreos).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("EnviarCorreoPrueba")>
    Public Function EnviarCorreoPrueba(ByVal entidad As ConfiguracionServidorCorreos) As Respuesta(Of String)
        Return New LogicaConfiguracionServidorCorreos(CapaPersistenciaConfiguracionServidorCorreos).EnviarCorreoPrueba(entidad)
    End Function

End Class
