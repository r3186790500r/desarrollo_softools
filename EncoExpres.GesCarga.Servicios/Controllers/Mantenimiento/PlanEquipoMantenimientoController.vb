﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Negocio.Mantenimiento
Imports EncoExpres.GesCarga.Entidades.MantenimientosPendientes

<Authorize>
Public Class PlanEquipoMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PlanEquipoMantenimiento) As Respuesta(Of IEnumerable(Of PlanEquipoMantenimiento))
        Return New LogicaPlanEquipoMantenimiento(CapaPersistenciaPlanEquipoMantenimiento, CapaPersistenciaDetallePlanEquipoMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PlanEquipoMantenimiento) As Respuesta(Of PlanEquipoMantenimiento)
        Return New LogicaPlanEquipoMantenimiento(CapaPersistenciaPlanEquipoMantenimiento, CapaPersistenciaDetallePlanEquipoMantenimiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PlanEquipoMantenimiento) As Respuesta(Of Long)
        Return New LogicaPlanEquipoMantenimiento(CapaPersistenciaPlanEquipoMantenimiento, CapaPersistenciaDetallePlanEquipoMantenimiento).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PlanEquipoMantenimiento) As Respuesta(Of Boolean)
        Return New LogicaPlanEquipoMantenimiento(CapaPersistenciaPlanEquipoMantenimiento, CapaPersistenciaDetallePlanEquipoMantenimiento).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("InsertarEquipos")>
    Public Function InsertarEquipos(ByVal entidad As IEnumerable(Of EquipoPlanEquipoMantenimiento)) As Respuesta(Of Boolean)
        Return New LogicaPlanEquipoMantenimiento(CapaPersistenciaPlanEquipoMantenimiento, CapaPersistenciaDetallePlanEquipoMantenimiento).InsertarEquipos(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarEquipos")>
    Public Function ConsultarEquipos(ByVal entidad As PlanEquipoMantenimiento) As Respuesta(Of IEnumerable(Of EquipoPlanEquipoMantenimiento))
        Return New LogicaPlanEquipoMantenimiento(CapaPersistenciaPlanEquipoMantenimiento, CapaPersistenciaDetallePlanEquipoMantenimiento).ConsultarEquipos(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarMantenimientosPendientes")>
    Public Function ConsultarMantenimientosPendientes(ByVal entidad As MantenimientosPendientes) As Respuesta(Of IEnumerable(Of MantenimientosPendientes))
        Return New LogicaPlanEquipoMantenimiento(CapaPersistenciaPlanEquipoMantenimiento, CapaPersistenciaDetallePlanEquipoMantenimiento).ConsultarMantenimientosPendientes(entidad)
    End Function
    <HttpPost>
    <ActionName("CambiarEstado")>
    Public Function CambiarEstado(ByVal entidad As PlanEquipoMantenimiento) As Respuesta(Of Boolean)
        Return New LogicaPlanEquipoMantenimiento(CapaPersistenciaPlanEquipoMantenimiento, CapaPersistenciaDetallePlanEquipoMantenimiento).CambiarEstado(entidad)
    End Function
End Class