﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="LineaVehiculosController"/>
''' </summary>
<Authorize>
Public Class LineaVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As LineaVehiculos) As Respuesta(Of IEnumerable(Of LineaVehiculos))
        Return New LogicaLineaVehiculos(CapaPersistenciaLineaVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As LineaVehiculos) As Respuesta(Of LineaVehiculos)
        Return New LogicaLineaVehiculos(CapaPersistenciaLineaVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As LineaVehiculos) As Respuesta(Of Long)
        Return New LogicaLineaVehiculos(CapaPersistenciaLineaVehiculos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As LineaVehiculos) As Respuesta(Of Boolean)
        Return New LogicaLineaVehiculos(CapaPersistenciaLineaVehiculos).Anular(entidad)
    End Function
End Class
