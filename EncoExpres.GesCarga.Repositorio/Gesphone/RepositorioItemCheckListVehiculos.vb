﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Gesphone

Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="RepositorioItemCheckListVehiculos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioItemCheckListVehiculos
        Inherits RepositorioBase(Of ItemCheckListVehiculos)

        Public Overrides Function Consultar(filtro As ItemCheckListVehiculos) As IEnumerable(Of ItemCheckListVehiculos)
            Dim lista As New List(Of ItemCheckListVehiculos)
            Dim item As ItemCheckListVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.CodigoChecklist) Then
                    conexion.AgregarParametroSQL("@par_CHLV_Codigo", filtro.CodigoChecklist)
                End If
                If filtro.Estado >= Cero Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_item_check_list_vehiculos]")

                While resultado.Read
                    item = New ItemCheckListVehiculos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ItemCheckListVehiculos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ItemCheckListVehiculos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ItemCheckListVehiculos) As ItemCheckListVehiculos
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As ItemCheckListVehiculos) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace