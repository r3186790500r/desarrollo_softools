﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports System.Transactions

Namespace Mantenimiento
    Public NotInheritable Class RepositorioOrdenTrabajoMantenimiento
        Inherits RepositorioBase(Of OrdenTrabajoMantenimiento)

        Public Overrides Function Consultar(filtro As OrdenTrabajoMantenimiento) As IEnumerable(Of OrdenTrabajoMantenimiento)
            Dim lista As New List(Of OrdenTrabajoMantenimiento)
            Dim item As OrdenTrabajoMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Pagina) Then
                    If (filtro.Pagina) Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If (filtro.RegistrosPagina) Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If (filtro.FechaInicial > Date.MinValue) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicio", filtro.FechaInicial, SqlDbType.Date)
                End If
                If (filtro.FechaFinal >= filtro.FechaInicial) And filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Fin", filtro.FechaFinal, SqlDbType.Date)
                End If
                If Not IsNothing(filtro.NumeroInicial) Then
                    If filtro.NumeroInicial > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Inicio", filtro.NumeroInicial)
                    End If
                End If
                If Not IsNothing(filtro.NumeroFinal) Then
                    If filtro.NumeroFinal > 0 And filtro.NumeroFinal >= filtro.NumeroInicial Then
                        conexion.AgregarParametroSQL("@par_Numero_Fin", filtro.NumeroFinal)
                    End If
                End If
                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado <> -1 Then 'NO APLICA
                        conexion.AgregarParametroSQL("@par_CATA_ESDO_Codigo", filtro.Estado)
                    End If
                End If
                If Not IsNothing(filtro.Equipo) Then
                    If filtro.Equipo.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_EQMA_Codigo", filtro.Equipo.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Proveedor) Then
                    If filtro.Proveedor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor", filtro.Proveedor.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_encabezado_orden_trabajo_mantenimientos]")

                While resultado.Read
                    item = New OrdenTrabajoMantenimiento(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As OrdenTrabajoMantenimiento) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_EQMA_Codigo", entidad.Equipo.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Entrega", entidad.FechaEntrega, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_CATA_ESDO_Codigo", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor", entidad.Proveedor.Codigo)
                    conexion.AgregarParametroSQL("@par_Contacto_Entrega", entidad.ContactoEntrega)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", 290)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_encabezado_orden_trabajo_mantenimientos]")

                    While resultado.Read
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While

                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalles
                        detalle.NumeroOrden = entidad.Numero
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto And insertoRecorrido Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using
            Return entidad.Numero
        End Function

        Public Function InsertarDetalle(entidad As DetalleOrdenTrabajoMantenimiento, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_EOTM_Numero", entidad.NumeroOrden)
            contextoConexion.AgregarParametroSQL("@par_SUMA_SIMA_Codigo", entidad.SistemaMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_SUMA_Codigo", entidad.SubsistemaMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_UNFM_Codigo", entidad.UnidadReferenciaMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
            contextoConexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)


            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_detalle_orden_trabajo_mantenimientos]")

            While resultado.Read
                inserto = IIf(resultado.Item("ID") > Cero, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As OrdenTrabajoMantenimiento) As Long

            Dim Modificodetalle As Boolean = True
            Dim Modifico As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_EQMA_Codigo", entidad.Equipo.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Entrega", entidad.FechaEntrega, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_CATA_ESDO_Codigo", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor", entidad.Proveedor.Codigo)
                    conexion.AgregarParametroSQL("@par_Contacto_Entrega", entidad.ContactoEntrega)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_modificar_encabezado_orden_trabajo_mantenimientos]")

                    While resultado.Read
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While

                    resultado.Close()

                    If resultado.RecordsAffected > 0 Then
                        Modificar = resultado.RecordsAffected
                        resultado.Close()
                    End If

                    For Each detalle In entidad.Detalles
                        detalle.NumeroOrden = entidad.Numero
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        If Not InsertarDetalle(detalle, conexion) Then
                            Modificodetalle = False
                            Modifico = False
                            Exit For
                        End If
                    Next

                    If Modificodetalle Then
                        transaccion.Complete()
                    End If

                End Using
            End Using
            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As OrdenTrabajoMantenimiento) As OrdenTrabajoMantenimiento
            Dim item As New OrdenTrabajoMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_encabezado_orden_trabajo_mantenimientos]")

                While resultado.Read
                    item = New OrdenTrabajoMantenimiento(resultado, True)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As OrdenTrabajoMantenimiento) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_anular_encabezado_tarea_equipo_mantenimiento]")

                While resultado.Read
                    anulo = IIf(Convert.ToInt16(resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class
End Namespace
