﻿PRINT 'gsp_consultar_empresas'
GO
DROP PROCEDURE gsp_consultar_empresas
GO
CREATE PROCEDURE gsp_consultar_empresas          
(          
  @par_Codigo SMALLINT = NULL,              
  @par_Codigo_Alterno VARCHAR(20) = NULL,              
  @par_Razon_Social VARCHAR(100) = NULL,                 
  @par_NumeroIdentificacion VARCHAR(30) = NULL,            
  @par_Nombre_Ciudad VARCHAR(100) = NULL          
)        
AS        
BEGIN        
 SELECT        
  0 AS Obtener        
    ,EMPR.Codigo        
    ,ISNULL(EMPR.Codigo_Alterno, '') AS Codigo_Alterno        
    ,EMPR.Nombre_Razon_Social        
    ,EMPR.Nombre_Corto_Razon_Social        
    ,EMPR.Numero_Identificacion        
    ,EMPR.CIUD_Codigo        
    ,CIUD.Nombre AS Nombre_Ciudad        
    ,ISNULL(EMPR.PAIS_Codigo    ,0) AS PAIS_Codigo    
    ,ISNULL(EMPR.Telefonos , '')   AS  Telefonos    
    ,ISNULL(EMPR.Direccion  , '')   AS Direccion    
    ,ISNULL(EMPR.Codigo_Postal, '')     AS Codigo_Postal    
    ,ISNULL(EMPR.Emails   , '')  AS Emails    
    ,ISNULL(EMPR.Pagina_Web   , '')  AS Pagina_Web    
    ,ISNULL(EMPR.Mensaje_Banner    , '') AS Mensaje_Banner    
    ,ISNULL(Prefijo, '') AS Prefijo        
    ,ISNULL(EMPR.Genera_Manifiesto,0)     AS Genera_Manifiesto    
    ,ISNULL(EMPR.Reportar_RNDC    ,0) AS Reporta_RNDC    
    ,ISNULL(EMPR.Aprobar_Liquidacion_Despachos,0) as Aprobar_Liquidacion_Despachos  
	,ISNULL(EMPR.Dias_Vigencia_Plan_Puntos, 0) as Dias_Vigencia_Plan_Puntos
 FROM        
  Empresas AS EMPR        
  INNER JOIN Ciudades AS CIUD        
   ON EMPR.Codigo = CIUD.EMPR_Codigo        
    AND EMPR.CIUD_Codigo = CIUD.Codigo        
 WHERE        
  EMPR.Codigo = ISNULL(@par_Codigo, EMPR.Codigo)        
  AND ((EMPR.Codigo_Alterno LIKE '%' + RTRIM(LTRIM(@par_Codigo_Alterno)) + '%'        
  OR (@par_Codigo_Alterno IS NULL)))        
  AND ((EMPR.Nombre_Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Razon_Social)) + '%'        
  OR (@par_Razon_Social IS NULL)))        
  AND ((EMPR.Numero_Identificacion LIKE '%' + RTRIM(LTRIM(@par_NumeroIdentificacion)) + '%'        
  OR (@par_NumeroIdentificacion IS NULL)))        
  AND ((CIUD.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Ciudad)) + '%'        
  OR (@par_Nombre_Ciudad IS NULL)))        
END 