﻿Imports Newtonsoft.Json

Namespace Despachos.Procesos
    <JsonObject>
    Public NotInheritable Class ResultReporteMinisterio
        Sub New()

        End Sub
        <JsonProperty>
        Public Property XmlEnvio As String
        <JsonProperty>
        Public Property StrError As String
        <JsonProperty>
        Public Property LstVariables As IEnumerable(Of String)
        <JsonProperty>
        Public Property NumConfirmacion As Double
        <JsonProperty>
        Public Property seguridadqr As String
        <JsonProperty>
        Public Property observacionesqr As String
    End Class
End Namespace

