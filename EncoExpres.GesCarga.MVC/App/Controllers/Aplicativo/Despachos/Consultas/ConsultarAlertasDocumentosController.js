﻿EncoExpresApp.controller('ConsultarAlertasDocumentosCtrl', ['$scope', '$linq', '$timeout', '$routeParams', 'blockUI', 'blockUIConfig', 'TercerosFactory', 'VehiculosFactory', 'ValorCatalogosFactory','SemirremolquesFactory',
    function ($scope, $linq, $timeout, $routeParams, blockUI, blockUIConfig, TercerosFactory, VehiculosFactory, ValorCatalogosFactory, SemirremolquesFactory) {
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Consultas' }, { Nombre: 'Alertas Documentos' }];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo==' + OPCION_MENU_ALERTAS_DOCUMENTOS);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta página');
            document.location.href = '#'
        }

        $scope.MensajesError = [];
        $scope.ListadoDocumentosVehiculo = [];
       
        $scope.Modelo = {
            Vehiculo : '',
            Conductor : ''
        };

        $scope.InitLoad = function () {

            $scope.ListadoCausasInactividadVehiculo = []
            $scope.ListadoCausasInactividadVehiculo = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CAUSA_INACTIVIDAD_VEHICULOS }, Sync: true }).Datos;
            

        }

        $scope.AutocompleteVehiculos = function (value) {
            return VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value,/* Estado: { Codigo: ESTADO_ACTIVO },*/ Sync: true }).Datos;
        }

        $scope.AutocompleteConductores = function (value) {
            return TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, CadenaPerfiles: PERFIL_CONDUCTOR, /*Estado: ESTADO_ACTIVO,*/ Sync: true }).Datos;
        }


        $scope.Buscar = function () {
            $scope.MensajesError = [];

            if (($scope.Modelo.Vehiculo != undefined && $scope.Modelo.Vehiculo != '') || ($scope.Modelo.Conductor != undefined || $scope.Modelo.Conductor != '')) {
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Obteniendo Documentos...");
                $timeout(function () { blockUI.message("Obteniendo Documentos..."); Find(); }, 100);
            } else {
                $scope.MensajesError.push('Debe ingresar un vehículo o un conductor')
            }
        }

        function Find() {
            $scope.MensajesError = [];

            if ($scope.Modelo.Vehiculo != undefined && $scope.Modelo.Vehiculo != '') {
                $scope.ListadoDocumentosVehiculo = [];
                $scope.ListadoDocumentosConductor = [];
                $scope.ListadoDocumentosTenedor = [];
                var fechaActual = new Date().getTime();

                var ResponseVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.Vehiculo.Codigo, Sync: true }).Datos;
                $scope.Vehiculo = ResponseVehiculo;
                $scope.Vehiculo.CausaInactividad = $linq.Enumerable().From($scope.ListadoCausasInactividadVehiculo).First('$.Codigo==' + $scope.Vehiculo.CausaInactividad.Codigo)
                var ResponseDocumentosVehiculo = VehiculosFactory.ConsultarDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo }, Sync: true }).Datos;
                if (ResponseDocumentosVehiculo != undefined) {

                    $scope.ListadoDocumentosVehiculo = ResponseDocumentosVehiculo;
                    $scope.ListadoDocumentosVehiculo.forEach(item => {
                        var fechaVence = new Date(item.FechaVenceDocumento).getTime();
                        item.DiasDiferencia = Math.ceil((fechaVence - fechaActual) / (24 * 3600 * 1000))
                    });

                }

                if (ResponseVehiculo.Semirremolque.Codigo > 0) {
                    $scope.Semirremolque = $scope.CargarSemirremolqueCodigo(ResponseVehiculo.Semirremolque.Codigo);

                    var ResponseDocumentosSemirremolque = SemirremolquesFactory.ConsultarDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Semirremolque: { Codigo: ResponseVehiculo.Semirremolque.Codigo }, Sync: true }).Datos;
                    if (ResponseDocumentosSemirremolque != undefined) {
                        $scope.ListadoDocumentosSemirremolque = ResponseDocumentosSemirremolque;
                        $scope.ListadoDocumentosSemirremolque.forEach(item => {
                            var fechaVenceSemirremolque = new Date(item.FechaVenceDocumento).getTime()
                            item.DiasDiferencia = Math.ceil((fechaVenceSemirremolque - fechaActual) / (24 * 3600 * 1000))
                        });
                    }
                }

                $scope.Conductor = $scope.CargarTercero(ResponseVehiculo.Conductor.Codigo);

                var ResponseDocumentosConductor = TercerosFactory.ConsultarDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: ResponseVehiculo.Conductor.Codigo }, Sync: true }).Datos;
                if (ResponseDocumentosConductor != undefined) {
                    $scope.ListadoDocumentosConductor = ResponseDocumentosConductor;
                    $scope.ListadoDocumentosConductor.forEach(item => {
                        var fechaVenceConductor = new Date(item.FechaVenceDocumento).getTime()
                        item.DiasDiferencia = Math.ceil((fechaVenceConductor - fechaActual) / (24 * 3600 * 1000))
                    });
                }

                $scope.Tenedor = $scope.CargarTercero(ResponseVehiculo.Tenedor.Codigo);

                var ResponseDocumentosTenedor = TercerosFactory.ConsultarDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: ResponseVehiculo.Tenedor.Codigo }, Sync: true }).Datos;
                if (ResponseDocumentosTenedor != undefined) {
                    $scope.ListadoDocumentosTenedor = ResponseDocumentosTenedor;
                    $scope.ListadoDocumentosTenedor.forEach(item => {
                        var fechaVenceTenedor = new Date(item.FechaVenceDocumento).getTime()
                        item.DiasDiferencia = Math.ceil((fechaVenceTenedor - fechaActual) / (24 * 3600 * 1000))
                    });
                }

            } else if ($scope.Modelo.Conductor != undefined && $scope.Modelo.Conductor != '') {
                $scope.ListadoDocumentosVehiculo = [];
                $scope.ListadoDocumentosConductor = [];
                $scope.ListadoDocumentosTenedor = [];
                var fechaActual = new Date().getTime();

                var ResponseVehiculo = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Conductor: { Codigo: $scope.Modelo.Conductor.Codigo }, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true }).Datos[0];
                if (ResponseVehiculo != undefined) {
                    $scope.Vehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: ResponseVehiculo.Codigo, Sync: true }).Datos;
                    $scope.Vehiculo.CausaInactividad = $linq.Enumerable().From($scope.ListadoCausasInactividadVehiculo).First('$.Codigo==' + $scope.Vehiculo.CausaInactividad.Codigo)
                    var ResponseDocumentosVehiculo = VehiculosFactory.ConsultarDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Vehiculo: { Codigo: $scope.Vehiculo.Codigo }, Sync: true }).Datos;
                    if (ResponseDocumentosVehiculo != undefined) {

                        $scope.ListadoDocumentosVehiculo = ResponseDocumentosVehiculo;
                        $scope.ListadoDocumentosVehiculo.forEach(item => {
                            var fechaVence = new Date(item.FechaVenceDocumento).getTime();
                            item.DiasDiferencia = parseInt((fechaVence - fechaActual) / (24 * 3600 * 1000))
                        });

                    }

                    if ($scope.Vehiculo.Semirremolque.Codigo > 0) {
                        $scope.Semirremolque = $scope.CargarSemirremolqueCodigo($scope.Vehiculo.Semirremolque.Codigo);

                        var ResponseDocumentosSemirremolque = SemirremolquesFactory.ConsultarDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Semirremolque: { Codigo: $scope.Vehiculo.Semirremolque.Codigo }, Sync: true }).Datos;
                        if (ResponseDocumentosSemirremolque != undefined) {
                            $scope.ListadoDocumentosSemirremolque = ResponseDocumentosSemirremolque;
                            $scope.ListadoDocumentosSemirremolque.forEach(item => {
                                var fechaVenceSemirremolque = new Date(item.FechaVenceDocumento).getTime()
                                item.DiasDiferencia = Math.ceil((fechaVenceSemirremolque - fechaActual) / (24 * 3600 * 1000))
                            });
                        }
                    }

                    $scope.Conductor = $scope.CargarTercero($scope.Modelo.Conductor.Codigo);

                    var ResponseDocumentosConductor = TercerosFactory.ConsultarDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.Conductor.Codigo }, Sync: true }).Datos;
                    if (ResponseDocumentosConductor != undefined) {
                        $scope.ListadoDocumentosConductor = ResponseDocumentosConductor;
                        $scope.ListadoDocumentosConductor.forEach(item => {
                            var fechaVenceConductor = new Date(item.FechaVenceDocumento).getTime()
                            item.DiasDiferencia = parseInt((fechaVenceConductor - fechaActual) / (24 * 3600 * 1000))
                        });
                    }

                    $scope.Tenedor = $scope.CargarTercero($scope.Vehiculo.Tenedor.Codigo);

                    var ResponseDocumentosTenedor = TercerosFactory.ConsultarDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.Vehiculo.Tenedor.Codigo }, Sync: true }).Datos;
                    if (ResponseDocumentosTenedor != undefined) {
                        $scope.ListadoDocumentosTenedor = ResponseDocumentosTenedor;
                        $scope.ListadoDocumentosTenedor.forEach(item => {
                            var fechaVenceTenedor = new Date(item.FechaVenceDocumento).getTime()
                            item.DiasDiferencia = parseInt((fechaVenceTenedor - fechaActual) / (24 * 3600 * 1000))
                        });
                    }
                } else {
                    $scope.Conductor = $scope.CargarTercero($scope.Modelo.Conductor.Codigo);

                    var ResponseDocumentosConductor = TercerosFactory.ConsultarDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.Conductor.Codigo }, Sync: true }).Datos;
                    if (ResponseDocumentosConductor != undefined) {
                        $scope.ListadoDocumentosConductor = ResponseDocumentosConductor;
                        $scope.ListadoDocumentosConductor.forEach(item => {
                            var fechaVenceConductor = new Date(item.FechaVenceDocumento).getTime()
                            item.DiasDiferencia = parseInt((fechaVenceConductor - fechaActual) / (24 * 3600 * 1000))
                        });
                    }
                }

            } else {
                $scope.MensajesError.push('Debe Ingresar un Vehículo o un Conductor.')
            }

            blockUI.stop();
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        }
        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope)
        }

    }
    
]);