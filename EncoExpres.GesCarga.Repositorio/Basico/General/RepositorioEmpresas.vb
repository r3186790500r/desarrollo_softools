﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports System.Transactions
Imports System.Net.Mail
Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioEmpresas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEmpresas
        Inherits RepositorioBase(Of Empresas)

        ''' <summary>
        ''' Metodo para consultar un listado de empresas
        ''' </summary>
        ''' <param name="filtro">filtros de busqueda</param>
        ''' <returns>Listado de empresas</returns>
        Public Overrides Function Consultar(filtro As Empresas) As IEnumerable(Of Empresas)
            Dim lista As New List(Of Empresas)
            Dim item As Empresas

            Dim correo As New MailMessage
            Dim envios As New SmtpClient

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()





                'conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.Codigo)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.RazonSocial) Then
                    conexion.AgregarParametroSQL("@par_Razon_Social", filtro.RazonSocial)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.NumeroIdentificacion) Then
                    conexion.AgregarParametroSQL("@par_NumeroIdentificacion", filtro.NumeroIdentificacion)
                End If

                If Not IsNothing(filtro.Ciudad) Then
                    If Not String.IsNullOrWhiteSpace(filtro.Ciudad.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Nombre_Ciudad", filtro.Ciudad.Nombre)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_empresas]")

                While resultado.Read
                    item = New Empresas(resultado)
                    item.EmpresaFacturaElectronica = ConsultarListaFactura(item.Codigo)
                    item.Validaciones = ConsultarValidacionesa(item.Codigo)
                    item.Controles = ConsultarControles(item.Codigo)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarMaster(filtro As Empresas) As IEnumerable(Of Empresas)
            Dim lista As New List(Of Empresas)
            Dim item As Empresas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_empresas_master]")

                While resultado.Read
                    item = New Empresas(resultado)
                    item.EmpresaFacturaElectronica = ConsultarListaFactura(item.Codigo)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        ''' <summary>
        ''' Metodo para insertar una nueva empresa en el repositorio de datos
        ''' </summary>
        ''' <param name="entidad">Objeto para insertar</param>
        ''' <returns>Codigo unico de la nueva empresa</returns>
        Public Overrides Function Insertar(entidad As Empresas) As Long

            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Nombre_Razon_Social", entidad.RazonSocial)
                    conexion.AgregarParametroSQL("@par_Nombre_Corto_Razon_Social", entidad.NombreCortoRazonSocial)
                    conexion.AgregarParametroSQL("@par_CATA_TIID_Codigo", entidad.CodigoTipoDocumento)
                    conexion.AgregarParametroSQL("@par_Numero_Identificacion", entidad.NumeroIdentificacion)
                    conexion.AgregarParametroSQL("@par_Digito_Chequeo", entidad.DigitoChequeo)
                    conexion.AgregarParametroSQL("@par_PAIS_Codigo", entidad.Pais.Codigo)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo", entidad.Ciudad.Codigo)
                    conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                    conexion.AgregarParametroSQL("@par_Telefonos", entidad.Telefono)
                    conexion.AgregarParametroSQL("@par_Codigo_Postal", entidad.CodigoPostal)
                    conexion.AgregarParametroSQL("@par_Emails", entidad.Email)
                    conexion.AgregarParametroSQL("@par_Pagina_Web", entidad.PaginaWeb)
                    conexion.AgregarParametroSQL("@par_Mensaje_Banner", entidad.MensajeBanner)
                    conexion.AgregarParametroSQL("@par_Aceptacion_Electronica", entidad.AceptacionElectronica)
                    conexion.AgregarParametroSQL("@par_Actividad_Economica", entidad.ActividadEconomica)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gps_insertar_empresas")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("@intCodigo").ToString()
                    End While
                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    Else

                        Call BorrarListaRNDC(entidad.Codigo, conexion)
                        If Not IsNothing(entidad.ListadoRNDC) Then

                            entidad.ListadoRNDC.CodigoEmpresa = entidad.Codigo

                            If Not InsertarListaRNDC(entidad.ListadoRNDC, conexion) Then
                            End If
                        End If

                        Call BorrarListaFactura(entidad.Codigo, conexion)
                        If Not IsNothing(entidad.EmpresaFacturaElectronica) Then

                            entidad.EmpresaFacturaElectronica.CodigoEmpresa = entidad.Codigo

                            If Not InsertarListaFactura(entidad.EmpresaFacturaElectronica, conexion) Then
                            End If
                        End If

                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo

        End Function

        ''' <summary>
        ''' Metodo para modificar la informacion de una empresa existente en el repositorio de datos
        ''' </summary>
        ''' <param name="entidad">Objeto para modificar</param>
        ''' <returns>Codigo unico de la empresa modificada</returns>
        Public Overrides Function Modificar(entidad As Empresas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Nombre_Razon_Social", entidad.RazonSocial)
                conexion.AgregarParametroSQL("@par_Nombre_Corto_Razon_Social", entidad.NombreCortoRazonSocial)
                conexion.AgregarParametroSQL("@par_CATA_TIID_Codigo", entidad.CodigoTipoDocumento)
                conexion.AgregarParametroSQL("@par_Numero_Identificacion", entidad.NumeroIdentificacion)
                conexion.AgregarParametroSQL("@par_Digito_Chequeo", entidad.DigitoChequeo)
                conexion.AgregarParametroSQL("@par_PAIS_Codigo", entidad.Pais.Codigo)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo", entidad.Ciudad.Codigo)
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Telefonos", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Codigo_Postal", entidad.CodigoPostal)
                conexion.AgregarParametroSQL("@par_Emails", entidad.Email)
                conexion.AgregarParametroSQL("@par_Pagina_Web", entidad.PaginaWeb)
                conexion.AgregarParametroSQL("@par_Mensaje_Banner", entidad.MensajeBanner)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                If Not IsNothing(entidad.Aseguradora) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Aseguradora", entidad.Aseguradora.Codigo)
                End If
                If entidad.NumeroPoliza <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Numero_Poliza", entidad.NumeroPoliza)
                End If
                If entidad.FechaVencimientoPoliza > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Vencimiento_Poliza", entidad.FechaVencimientoPoliza, SqlDbType.Date)
                End If
                conexion.AgregarParametroSQL("@par_Valor_Poliza", entidad.ValorPoliza)
                conexion.AgregarParametroSQL("@par_Valor_Combustible", entidad.ValorCombustible)
                conexion.AgregarParametroSQL("@par_CodigoUIAF", entidad.CodigoUIAF)

                conexion.AgregarParametroSQL("@par_Aceptacion_Electronica", entidad.AceptacionElectronica)
                conexion.AgregarParametroSQL("@par_Actividad_Economica", entidad.ActividadEconomica)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_empresas")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                Else

                    Call BorrarListaRNDC(entidad.Codigo, conexion)
                    If Not IsNothing(entidad.ListadoRNDC) Then

                        entidad.ListadoRNDC.CodigoEmpresa = entidad.Codigo

                        If Not InsertarListaRNDC(entidad.ListadoRNDC, conexion) Then
                        End If
                    End If

                    Call BorrarListaFactura(entidad.Codigo, conexion)
                    If Not IsNothing(entidad.EmpresaFacturaElectronica) Then

                        entidad.EmpresaFacturaElectronica.CodigoEmpresa = entidad.Codigo

                        If Not InsertarListaFactura(entidad.EmpresaFacturaElectronica, conexion) Then
                        End If
                    End If

                End If

            End Using

            Return entidad.Codigo

        End Function

        ''' <summary>
        ''' Metodo para obtener la informacion de una empresa
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Overrides Function Obtener(filtro As Empresas) As Empresas
            Dim item As New Empresas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gps_obtener_empresas")

                While resultado.Read
                    item = New Empresas(resultado)
                End While
                resultado.Close()
                If item.Codigo > 0 Then
                    item.ListadoRNDC = ConsultarListaRNDC(filtro.Codigo, conexion, resultado)
                End If

                If item.Codigo > 0 Then
                    item.EmpresaFacturaElectronica = ConsultarListaFactura(filtro.Codigo)
                End If

            End Using

            Return item
        End Function

        Public Function ObtenerSW(filtro As Empresas) As Empresas
            Dim item As New Empresas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gps_obtener_empresas")

                While resultado.Read
                    item = New Empresas(resultado)
                End While
                resultado.Close()
                If item.Codigo > 0 Then
                    item.ListadoRNDC = ConsultarListaRNDC(filtro.Codigo, conexion, resultado)
                End If
            End Using

            Return item
        End Function

        Public Function ObtenerNumeroMaximoUsuarios(codigo As Short) As Short
            Dim cantidad As Short = 0
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_Codigo", codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_numero_maximo_usuarios]")

                While resultado.Read
                    cantidad = resultado.Item("Numero_Maximo_Usuarios")
                End While

            End Using

            Return cantidad

        End Function

        Public Function BorrarListaRNDC(CodiEmpresa As Short, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_lista_rndc_empresa]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarListaRNDC(entidad As EmpresaManifiestoElectronico, ByRef conexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_Codigo_Regional_Ministerio", entidad.CodigoRegionalMinisterio)
            conexion.AgregarParametroSQL("@par_Codigo_Empresa_Ministerio", entidad.CodigoEmpresaMinisterio)
            conexion.AgregarParametroSQL("@par_Usuario_Manifiesto_Electronico", entidad.UsuarioManifiestoElectronico)
            conexion.AgregarParametroSQL("@par_Numero_Resolucion_Habilitacion_Empresa", entidad.NumeroResolucion)
            conexion.AgregarParametroSQL("@par_Clave_Manifiesto_Electronico", entidad.ClaveManifiestoElectronico)
            conexion.AgregarParametroSQL("@par_Enlace_WS_Ministerio", entidad.EnlaceWSMinisterio)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_lista_rndc_empresa]")

            If resultado.RecordsAffected = 0 Then
                inserto = False
            End If
            resultado.Close()

            Return inserto
        End Function

        Public Function ConsultarListaRNDC(CodiEmpresa As Short, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As EmpresaManifiestoElectronico
            Dim item As EmpresaManifiestoElectronico
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_rndc_empresa]")

            While resultado.Read
                item = New EmpresaManifiestoElectronico(resultado)
            End While
            resultado.Close()
            Return item
        End Function

        Public Function BorrarListaFactura(CodiEmpresa As Short, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_lista_factura_empresa]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarListaFactura(entidad As EmpresaFacturaElectonica, ByRef conexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_Proveedor", entidad.Proveedor.Codigo)
            conexion.AgregarParametroSQL("@par_Usuario", entidad.Usuario)
            conexion.AgregarParametroSQL("@par_Clave", entidad.Clave)
            conexion.AgregarParametroSQL("@par_Operador_Virtual", entidad.OperadorVirtual)
            conexion.AgregarParametroSQL("@par_Prefijo_Factura", entidad.PrefijoFactura)
            conexion.AgregarParametroSQL("@par_Clave_Tecnica_Factura", entidad.ClaveTecnicaFactura)
            conexion.AgregarParametroSQL("@par_Clave_Externa_Factura", entidad.ClaveExternaFactura)
            conexion.AgregarParametroSQL("@par_Prefijo_Nota_Credito", entidad.PrefijoNotaCredito)
            conexion.AgregarParametroSQL("@par_Clave_Externa_Nota_Credito", entidad.ClaveExternaNotaCredito)
            conexion.AgregarParametroSQL("@par_Prefijo_Nota_Debito", entidad.PrefijoNotaDebito)
            conexion.AgregarParametroSQL("@par_Clave_Externa_Nota_Debito", entidad.ClaveExternaNotaDebito)
            conexion.AgregarParametroSQL("@par_CATA_TAFE_Codigo", entidad.Ambiente.Codigo)
            conexion.AgregarParametroSQL("@par_ID_Empresa", entidad.ID_Empresa)
            conexion.AgregarParametroSQL("@par_Firma_Digital", entidad.FirmaDigital)


            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_lista_factura_empresa]")

            If resultado.RecordsAffected = 0 Then
                inserto = False
            End If
            resultado.Close()

            Return inserto
        End Function

        Public Function ConsultarListaFactura(CodiEmpresa As Short) As EmpresaFacturaElectonica
            Dim item As EmpresaFacturaElectonica

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_factura_empresa]")

                While resultado.Read
                    item = New EmpresaFacturaElectonica(resultado)
                End While
            End Using
            Return item
        End Function
        Public Function ConsultarValidacionesa(CodiEmpresa As Short) As IEnumerable(Of EmpresaValidaciones)
            Dim item As EmpresaValidaciones
            Dim lista As New List(Of EmpresaValidaciones)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_validaciones_empresa]")

                While resultado.Read
                    item = New EmpresaValidaciones(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function
        Public Function ConsultarControles(CodiEmpresa As Short) As IEnumerable(Of EmpresaControles)
            Dim item As EmpresaControles
            Dim lista As New List(Of EmpresaControles)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_controles_empresa]")

                While resultado.Read
                    item = New EmpresaControles(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function



        Public Function CambioClave(filtro As Empresas) As IEnumerable(Of Empresas)
            Dim lista As New List(Of Empresas)
            Dim item As Empresas
            Dim RepoCorreos As New Utilitarios.RepositorioConfiguracionServidorCorreos
            Dim correo As New MailMessage
            Dim envios As New SmtpClient

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.Codigo)

                If Not IsNothing(filtro.Usuario.Clave) Then
                    conexion.AgregarParametroSQL("@par_Clave", filtro.Usuario.Clave)
                    conexion.AgregarParametroSQL("@par_UsuaCodigo", filtro.Usuario.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_clave_usuario")

                    While resultado.Read
                        item = New Empresas(resultado)
                        lista.Add(item)
                    End While

                    RepoCorreos.EnviarCorreoConfirmacionClave(filtro.Usuario.Correo, filtro.Usuario.CorreoManager, filtro.Codigo)
                Else
                    conexion.AgregarParametroSQL("@par_Correo", filtro.Usuario.Correo)
                    conexion.AgregarParametroSQL("@par_Codigo_Usuario", filtro.Usuario.CodigoUsuario)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_usuario_correo")

                    While resultado.Read
                        item = New Empresas(resultado)
                        lista.Add(item)
                    End While

                    If lista.Count > 0 Then
                        If lista(0).Usuario.ExisteUsuario > 0 Then
                            RepoCorreos.EnviarCorreoCodigoVerificacion(lista(0).Usuario.CodigoUsuario, lista(0).Usuario.CodigoCambioClave, filtro.Usuario.Correo, filtro.Usuario.CorreoManager, filtro.Codigo)
                        End If
                    End If
                End If


            End Using
            Return lista
        End Function

    End Class



End Namespace