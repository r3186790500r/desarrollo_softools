﻿EncoExpresApp.factory('LiquidacionesFactory', ['$http', function ($http) {

    var urlService = GetUrl('Liquidaciones');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ObtenerPlanilla = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerPlanilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Rechazar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Rechazar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Aprobar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Aprobar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.CalcularValores = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CalcularValores';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.CargarCombinacionesCvt = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CargarCombinacionesCvt';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    return factory;
}]);