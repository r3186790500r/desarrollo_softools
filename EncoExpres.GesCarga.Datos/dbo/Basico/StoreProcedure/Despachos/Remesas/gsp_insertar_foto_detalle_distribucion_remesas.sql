﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 07/12/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_insertar_foto_detalle_distribucion_remesas'
GO
DROP PROCEDURE gsp_insertar_foto_detalle_distribucion_remesas
GO
CREATE PROCEDURE gsp_insertar_foto_detalle_distribucion_remesas 
(      
@par_EMPR_Codigo SMALLINT,      
@par_ENRE_Numero NUMERIC,       
@par_DEDR_ID NUMERIC,            
@par_Foto VARBINARY(max), 
@par_Nombre_Foto VARCHAR(50), 
@par_Extension_Foto VARCHAR(50),      
@par_Tipo_Foto VARCHAR(50),      
@par_USUA_Codigo_Crea SMALLINT      
)      
AS      
BEGIN      
      
 IF EXISTS(SELECT * FROM      
Foto_Detalle_Distribucion_Remesas      
WHERE       
EMPR_Codigo = @par_EMPR_Codigo      
AND ENRE_Numero = @par_ENRE_Numero      
AND DEDR_ID = @par_DEDR_ID
)  
 BEGIN      
      
  UPDATE  Foto_Detalle_Distribucion_Remesas      
  SET       
   Nombre_Foto = @par_Nombre_Foto,      
   Foto = @par_Foto,      
   Extension = @par_Extension_Foto,      
   Tipo = @par_Tipo_Foto,      
   USUA_Codigo_Crea = @par_USUA_Codigo_Crea,      
   Fecha_Crea = GETDATE()      
  WHERE       
   EMPR_Codigo = @par_EMPR_Codigo      
   AND ENRE_Numero = @par_ENRE_Numero      
   AND DEDR_ID = @par_DEDR_ID     
      
   SELECT @@ROWCOUNT AS Codigo       
      
 END      
 ELSE       
 BEGIN       
      
  INSERT INTO Foto_Detalle_Distribucion_Remesas      
  (   
   EMPR_Codigo,       
   ENRE_Numero ,
   DEDR_ID,   
   Foto, 
   Nombre_Foto,     
   Extension,      
   Tipo,      
   USUA_Codigo_Crea,      
   Fecha_Crea 
  )      
   VALUES        
  (      
@par_EMPR_Codigo,      
@par_ENRE_Numero,       
@par_DEDR_ID,             
@par_Foto, 
@par_Nombre_Foto, 
@par_Extension_Foto,      
@par_Tipo_Foto,      
@par_USUA_Codigo_Crea,      
GETDATE()      
  )      
  SELECT @@ROWCOUNT AS Codigo       
 END       
END 
GO  