﻿Imports Newtonsoft.Json

Namespace Basico.Despachos
    <JsonObject>
    Public NotInheritable Class PeajeRutas
        Inherits BaseBasico

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Peaje = New Peajes With {.Codigo = Read(lector, "PEAJ_Codigo"), .Nombre = Read(lector, "PEAJ_Nombre"), .Estado = Read(lector, "EstadoPeaje")}
            Orden = Read(lector, "Orden")
            TiempoEstimado = Read(lector, "Horas_Estimadas_Arribo")
        End Sub

        <JsonProperty>
        Public Property Peaje As Peajes
        <JsonProperty>
        Public Property Orden As Integer
        <JsonProperty>
        Public Property TiempoEstimado As Double
    End Class
End Namespace



