﻿Print 'gsp_Listado_OrdenCargue'       
GO
DROP PROCEDURE gsp_Listado_OrdenCargue
GO
CREATE PROCEDURE gsp_Listado_OrdenCargue(          
@par_EMPR_Codigo NUMERIC,          
@par_Numero_Documento_Inicial NUMERIC = null,          
@par_Numero_Documento_Final NUMERIC = null,          
@par_Fecha_Incial DATE = NULL,           
@par_Fecha_Final DATE = NULL,           
@par_Cliente VARCHAR(50) = NULL,           
@par_OFIC_Codigo NUMERIC = NULL ,          
@par_Estado NUMERIC = NULL ,          
@par_Ruta VARCHAR(50) = NULL           
)          
AS BEGIN      

	DECLARE @intAnulado SMALLINT = NULL
	SET @intAnulado = CASE @par_Estado WHEN NULL THEN NULL WHEN 2 THEN 1 ELSE 0 END
    SELECT @par_Estado = IIF (@par_Estado = 2,NULL,@par_Estado)
    
SELECT           
EMPR.Nombre_Razon_Social          
, ENOC.Numero_Documento           
, ENOC.FECHA           
, VEHI.Placa           
, RUTA.Nombre AS NombreRuta          
, ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')            
+ ' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS NombreConductor          
, CICA.Nombre AS CiudadCargue           
, ENOC.Direccion_Cargue           
, ENOC.Contacto_Cargue          
, OFIC.Nombre as NombreOficina          
,  ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'')            
+ ' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS NombreCliente          
,ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial          
,ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal          
,ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial          
,ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal         

,CASE ENOC.Anulado WHEN 1 THEN 'A' ELSE           
CASE ENOC.ESTADO WHEN 1 THEN 'D' ELSE 'B' 
END    
END As Estado    
      
   FROM Encabezado_Orden_Cargues ENOC          
 inner join  Empresas EMPR  on        
 ENOC.EMPR_Codigo = EMPR.Codigo           
left join  VEHICULOS VEHI  on        
  ENOC.EMPR_Codigo = VEHI.EMPR_Codigo          
 AND ENOC.VEHI_Codigo = VEHI.Codigo           
        
left join  Rutas RUTA   on        
 ENOC.EMPR_Codigo = RUTA.EMPR_Codigo          
 AND ENOC.RUTA_Codigo = RUTA.Codigo          
         
left join  Terceros TECO  on        
 ENOC.EMPR_Codigo = TECO.EMPR_Codigo          
 AND ENOC.TERC_Codigo_Conductor = TECO.Codigo           
        
left join  terceros TECL  on        
  ENOC.EMPR_Codigo = TECL.EMPR_Codigo           
 AND ENOC.TERC_Codigo_Cliente = TECL.Codigo          
          
left join  Ciudades CICA  on        
  ENOC.EMPR_Codigo = CICA.EMPR_Codigo          
 AND ENOC.CIUD_Codigo_Cargue = CICA.Codigo          
          
left join  Oficinas OFIC  on        
  ENOC.EMPR_Codigo = OFIC.EMPR_Codigo          
 AND ENOC.OFIC_Codigo = OFIC.Codigo           
          
 WHERE         
  ENOC.EMPR_Codigo =  @par_EMPR_Codigo        
 and  ENOC.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENOC.Numero_Documento)          
 AND ENOC.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENOC.Numero_Documento)           
          
 AND ENOC.Fecha >= ISNULL( @par_Fecha_Incial,ENOC.Fecha)           
 AND ENOC.Fecha <= ISNULL( @par_Fecha_Final,ENOC.Fecha)         
 AND (TECL.Nombre + ' ' +TECL.Apellido1 + ' ' + TECL.Apellido2 + ' ' + TECL.Razon_Social LIKE '%'+@par_Cliente+'%' or @par_Cliente is null)      
 AND ENOC.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENOC.OFIC_Codigo)
 AND ENOC.Estado = ISNULL (@par_Estado,ENOC.Estado)         
 AND ENOC.Anulado = ISNULL(@intAnulado, ENOC.Anulado)
 AND (RUTA.Nombre LIKE '%'+@par_Ruta+'%' or @par_Ruta is null)    
        
 END         

GO 