﻿PRINT 'gsp_insertar_encabezado_formularios_inspecciones'
GO
DROP PROCEDURE gsp_insertar_encabezado_formularios_inspecciones
GO
CREATE PROCEDURE gsp_insertar_encabezado_formularios_inspecciones 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo_Alterno VARCHAR(20),  
@par_Nombre VARCHAR(50),  
@par_Estado SMALLINT,  
@par_Version VARCHAR(50),  
@par_TIDO_Codigo_Origen NUMERIC,  
@par_USUA_Codigo_Crea SMALLINT
)  
AS  
BEGIN  
  
INSERT INTO Encabezado_Formularios_Inspecciones  
(  
EMPR_Codigo,  
Codigo_Alterno,  
Nombre,  
Estado,  
Version,  
TIDO_Codigo_Origen,  
Fecha_Crea,  
USUA_Codigo_Crea 
)  
VALUES   
(  
@par_EMPR_Codigo ,  
@par_Codigo_Alterno,  
@par_Nombre,  
@par_Estado,  
@par_Version,  
@par_TIDO_Codigo_Origen,  
GETDATE(),  
@par_USUA_Codigo_Crea   
)  
SELECT @@IDENTITY AS Codigo   
END  

GO