﻿PRINT 'gsp_modificar_detalle_seguimiento_carga_vehiculos'
GO
DROP PROCEDURE gsp_modificar_detalle_seguimiento_carga_vehiculos
GO
CREATE PROCEDURE gsp_modificar_detalle_seguimiento_carga_vehiculos
(
@par_EMPR_Codigo SMALLINT,
@par_Numero NUMERIC,
@par_Numero_Documento VARCHAR(20),
@par_ENSV_Codigo NUMERIC,
@par_TIDO_Soporte NUMERIC,
@par_Numero_Documento_Soporte NUMERIC,
@par_Numero_Remesa NUMERIC,
@par_CATA_TIRS_Codigo NUMERIC,
@par_TERC_Codigo_Cliente NUMERIC,
@par_CATA_NOSC_Codigo NUMERIC,
@par_Observaciones VARCHAR (200) = NULL,
@par_Identificacion_Recibido_Cliente VARCHAR (15) = NULL,
@par_Nombre_Recibido_Cliente  VARCHAR (50)= NULL,
@par_CIUD_Recibido_Cliente NUMERIC,
@par_Direccion_Recibido_Cliente VARCHAR (100) = NULL,
@par_Telefono_Recibido_Cliente VARCHAR (20)= NULL,
@par_Cumplir SMALLINT = NULL,
@par_Reportar_Cliente SMALLINT = NULL,
@par_Estado SMALLINT,
@par_USUA_Codigo_Modifica NUMERIC,
@par_Fecha_Modifica DATETIME, 
@par_Firma IMAGE = NULL
)
AS
BEGIN 
	UPDATE Detalle_Seguimiento_Carga_Vehiculos
	SET 
		TIDO_Soporte = @par_TIDO_Soporte,
		Numero_Documento_Soporte = @par_Numero_Documento_Soporte,
		Numero_Remesa = @par_Numero_Remesa,
		CATA_TIRS_Codigo = @par_CATA_TIRS_Codigo,
		TERC_Codigo_Cliente = @par_TERC_Codigo_Cliente,
		CATA_NOSC_Codigo = @par_CATA_NOSC_Codigo,
		Observaciones = ISNULL(@par_Observaciones,''),
		Identificacion_Recibido_Cliente =  ISNULL(@par_Identificacion_Recibido_Cliente,''),
		Nombre_Recibido_Cliente =  ISNULL(@par_Nombre_Recibido_Cliente,''),
		CIUD_Recibido_Cliente = @par_CIUD_Recibido_Cliente,
		Direccion_Recibido_Cliente = ISNULL(@par_Direccion_Recibido_Cliente,''),
		Telefono_Recibido_Cliente = ISNULL(@par_Telefono_Recibido_Cliente,''),
		Cumplir = @par_Cumplir,
		Reportar_Cliente = @par_Reportar_Cliente,
		Estado = @par_Estado,
		USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,
		Fecha_Modifica = GETDATE(),
		Firma = @par_Firma
	WHERE 
		EMPR_Codigo = @par_EMPR_Codigo
		AND ID = @par_Numero
		AND Numero_Documento = @par_Numero_Documento
		AND ENSV_Codigo = @par_ENSV_Codigo
END
GO