﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Almacen
Imports EncoExpres.GesCarga.Negocio.Almacen

<Authorize>
Public Class DetalleOrdenCompraController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleOrdenCompra) As Respuesta(Of IEnumerable(Of DetalleOrdenCompra))
        Return New LogicaDetalleOrdenCompra(CapaPersistenciaDetalleOrdenCompra).Consultar(filtro)
    End Function

End Class
