﻿EncoExpresApp.controller("GenerarComprobanteIngresosCtrl", ['$scope', '$routeParams', '$linq', 'blockUI', '$timeout', 'TercerosFactory', 'ValorCatalogosFactory', 'OficinasFactory', 'DocumentoComprobantesFactory', 'CajasFactory', 'CuentaBancariasFactory', 'PlanUnicoCuentasFactory', 'VehiculosFactory', 'ConceptoContablesFactory', 'DetalleConceptoContablesFactory', 'DocumentoCuentasFactory','CierreContableDocumentosFactory',
    function ($scope, $routeParams, $linq, blockUI, $timeout, TercerosFactory, ValorCatalogosFactory, OficinasFactory, DocumentoComprobantesFactory, CajasFactory, CuentaBancariasFactory, PlanUnicoCuentasFactory, VehiculosFactory, ConceptoContablesFactory, DetalleConceptoContablesFactory, DocumentoCuentasFactory, CierreContableDocumentosFactory) {
        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */
        $scope.Titulo = 'GENERAR COMPROBANTES INGRESO';
        $scope.MapaSitio = [{ Nombre: 'Tesorería' }, { Nombre: 'Procesos' }, { Nombre: 'Generar Comprobantes Ingreso' }];
        console.clear();

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GENERAR_COMPROBANTE_INGRESOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 }
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');
        $scope.cantidadRegistrosPorPagina = 10
        $scope.paginaActual = 1
        $scope.ListaCXC = [];
        $scope.ListadoCuentasPUC = [];
        $scope.ListadoOficinas = [];
        $scope.ListadoDocumentosOrigen = [];
        $scope.ListadoCuentaBancariaOficinas = [];
        $scope.ListadoConceptoContable = [];
        $scope.ListadoVehiculos = [];
        $scope.ListaFormaPago = [];
        $scope.MensajesError = [];
        $scope.ListadoCuentaBancarias = [];
        $scope.ListadoMovimiento = [];
        $scope.ListadoTodaCajaOficinas = [];
        $scope.ListadoCajaOficinas = [];
        $scope.ListaCXCSeleccionadas = [];

        $scope.ActivarEfectivo = false
        $scope.ActivarCheque = false
        $scope.ActivarConsignacion = false
        $scope.PlacaValida = true;
        $scope.DeshabilitaDocumentoOrigen = true;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            Numero: 0,
            Fecha: new Date(),
            Debito: 0,
            Credito: 0,
            Diferencia: 0,
            OficinaCrea: $scope.Sesion.UsuarioAutenticado.Oficinas[0],
        }
        $scope.Modal = {

        }

        /* Obtener parametros del enrutador*/
        if ($routeParams.Numero !== undefined) {
            $scope.Modelo.Numero = $routeParams.Numero;
        }
        else {
            $scope.Modelo.Numero = 0;
        };
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            $scope.ConsultarCXC();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                $scope.ConsultarCXC();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                $scope.ConsultarCXC();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            $scope.ConsultarCXC();
        };
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar el autocomplete de cuentas PUC*/
        PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCuentasPUC = response.data.Datos

                        if ($scope.CodigoCuentaPUC !== undefined && $scope.CodigoCuentaPUC !== '' && $scope.CodigoCuentaPUC !== null) {
                            $scope.Modal.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                        }
                    }
                    else {
                        $scope.ListadoCuentasPUC = [];
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Oficinas*/
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoOficinas = response.data.Datos
                        if ($scope.CodigoOficinaDestino !== undefined && $scope.CodigoOficinaDestino !== '' && $scope.CodigoOficinaDestino !== null) {
                            $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficinaDestino)
                        }
                        else {
                            $scope.Modelo.OficinaDestino = $scope.ListadoOficinas[0];
                        }

                        try {
                            $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                        } catch (e) {
                            $scope.Oficina = $scope.ListadoOficina[$scope.ListadoOficinas.length - 1];
                        }
                    }
                    else {
                        $scope.ListadoOficinas = [];
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Documenteos Origen*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDocumentosOrigen.push({ Codigo: 2600, Nombre: "Seleccione un documento origen" });
                    response.data.Datos.forEach(function (item) {
                        if (item.CampoAuxiliar2.match(/IGR/) && item.CampoAuxiliar2.match(/GEN/)) {
                            $scope.ListadoDocumentosOrigen.push(item);
                        }
                    });
                    if ($scope.CodigoDocumentoOrigen !== undefined && $scope.CodigoDocumentoOrigen !== '' && $scope.CodigoDocumentoOrigen !== null) {
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + $scope.CodigoDocumentoOrigen);
                    }
                    else {
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == 2600');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
    /*Cargar el combo de Forma de pago*/
        
       // ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MEDIO_PAGO } }).
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_DOCUMENTO_COMPROBANTES } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaFormaPago = response.data.Datos

                    if ($scope.CodigoFormaPago !== undefined && $scope.CodigoFormaPago !== '' && $scope.CodigoFormaPago !== null) {
                        $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo ==' + $scope.CodigoFormaPago);
                    }
                    else {
                        $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo == 5100');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        function CargarCuentasBancarias() {
            /*Cargar el combo Cuentas Bancarias por oficina*/
            CuentaBancariasFactory.CuentaBancariaOficinas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCuentaBancariaOficinas = [];
                        $scope.ListadoCuentaBancariaOficinas = response.data.Datos;

                        if ($scope.CodigoCuentaBancaria !== undefined && $scope.CodigoCuentaBancaria !== null && $scope.CodigoCuentaBancaria !== '' && $scope.CodigoCuentaBancaria !== 0) {
                            $scope.ListadoCuentaBancariaOficinas.forEach(function (item) {
                                if (item.CodigoCuenta > 0) {
                                    if (item.CodigoOficina == $scope.CodigoOficinaDestino) {
                                        $scope.ListadoCuentaBancarias.push(item);
                                    }
                                }
                            });

                            $scope.Modelo.CuentaBancariaCheque = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + $scope.CodigoCuentaBancaria);
                            $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + $scope.CodigoCuentaBancaria);
                        }
                        else {
                            $scope.ListadoCuentaBancarias = [];
                            $scope.ListadoCuentaBancariaOficinas.forEach(function (item) {
                                if (item.Oficina.Codigo == $scope.Modelo.OficinaDestino.Codigo) {
                                    $scope.ListadoCuentaBancarias.push(item);
                                }
                            });
                            if ($scope.ListadoCuentaBancarias.length > 0) {
                                $scope.Modelo.CuentaBancariaConsignacion = $scope.ListadoCuentaBancarias[0];
                                $scope.Modelo.CuentaBancariaCheque = $scope.ListadoCuentaBancarias[0];
                            }
                            else {
                                $scope.MensajesError.push('La oficina seleccionada no tiene ninguna cuenta bancaria asociada');
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        function CargarCajas() {
            $scope.ListadoCajaOficinas = [];
            $scope.MensajesError = [];
            CajasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTodaCajaOficinas = [];
                        $scope.ListadoTodaCajaOficinas = response.data.Datos;

                        if ($scope.CodigoCaja !== undefined && $scope.CodigoCaja !== null && $scope.CodigoCaja !== '' && $scope.CodigoCaja !== 0) {
                            $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                                if (itmOfic.Codigo > 0) {
                                    if (itmOfic.Oficina.Codigo == $scope.CodigoOficinaDestino) {
                                        $scope.ListadoCajaOficinas.push(itmOfic);
                                    }
                                }
                            });

                            $scope.Modelo.Caja = $linq.Enumerable().From($scope.ListadoCajaOficinas).First('$.Codigo == ' + $scope.CodigoCaja);

                        }
                        else {
                            $scope.ListadoCajaOficinas = [];
                            $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                                if (itmOfic.Codigo > 0) {
                                    if (itmOfic.Oficina.Codigo == $scope.Modelo.OficinaDestino.Codigo) {
                                        $scope.ListadoCajaOficinas.push(itmOfic);
                                    }
                                }
                            });
                            if ($scope.ListadoCajaOficinas.length > 0) {
                                $scope.Modelo.Caja = $scope.ListadoCajaOficinas[0];
                            }
                            else {
                                $scope.MensajesError.push('La oficina seleccionada no tiene ninguna caja asociada');
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        /*Cargar Combo Conceptos Contables*/
        ConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoConceptoContable = response.data.Datos;

                        if ($scope.CodigoConceptoContable !== undefined && $scope.CodigoConceptoContable !== '' && $scope.CodigoConceptoContable !== null) {
                            if ($scope.CodigoConceptoContable > 0) {
                                $scope.Modelo.ConceptoContable = $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo ==' + $scope.CodigoConceptoContable);
                            }
                        }
                    }
                    else {
                        $scope.ListadoConceptoContable = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.AsignarPlaca = function (Placa) {
            if (Placa != undefined || Placa != null) {
                if (angular.isObject(Placa)) {
                    $scope.LongitudPlaca = Placa.length;
                    $scope.PlacaValida = true;
                    $scope.ValidarTercero(Placa.Tenedor.Identificacion)
                    $scope.ValidarBeneficiario(Placa.Tenedor.Identificacion)
                }
                else if (angular.isString(Placa)) {
                    $scope.LongitudPlaca = Placa.length;
                    $scope.PlacaValida = false;
                }
            }
        };


        /*cargar AutoComplete de los terceros */
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTerceros = response.data.Datos;

                        //if ($scope.CodigoPlaca !== undefined && $scope.CodigoPlaca !== '' && $scope.CodigoPlaca !== null) {
                        //    if ($scope.CodigoPlaca > 0) {
                        //        $scope.Modelo.Placa = $linq.Enumerable().From($scope.ListadoVehiculos).First('$.Codigo == ' + $scope.CodigoPlaca);
                        //    }
                        //}

                    }
                    else {
                        $scope.ListadoTerceros = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.ValidarTercero = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Tercero = response.data.Datos
                            }
                            else {
                                $scope.Modelo.Tercero = undefined
                                ShowError('La identificación Ingresada no es válida o el tercero no se encuentra registrado')
                            }
                        }
                    });
            }
        };
        $scope.ValidarTerceroModal = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modal.TerceroModal = response.data.Datos
                            }
                            else {
                                $scope.Modal.TerceroModal = undefined
                                ShowError('La identificación Ingresada no es válida o el tercero no se encuentra registrado')
                            }
                        }
                    });
            }
        };

        //Marcar y demarcar todas las cuentas
        $scope.MarcarCuentas = function () {
            if ($scope.CuentasSeleccionados == true) {
                $scope.ListaCXC.forEach(function (item) {
                    item.Seleccionado = true;
                });
                $scope.ValidarValores();
            } else {
                $scope.ListaCXC.forEach(function (item) {
                    item.Seleccionado = false;
                });
            }
            Calcular();
        }

        $scope.ValidarValores = function () {
            $scope.ListaCXC.forEach(function (item) {
                if (item.Seleccionado = true) {
                    if (Math.ceil(MascaraNumero(item.ValorPagar)) > Math.ceil(MascaraNumero(item.Saldo))) {
                        item.Seleccionado = false;
                        ShowInfo('El valor a pagar de algunos documentos no puede ser mayor a su saldo, por lo tanto no se tendran en cuenta');
                    }
                }
            })
        }

        /*------------------------------------------------------------------------------------Funcion Calcular--------------------------------------------------------------*/
        $scope.Calcular = function (item) {

            // Si el item fue seleccionado lo inserto en la lista de seleccionados
            if (item.Seleccionado == true) {
                if (Math.ceil(MascaraNumero(item.ValorPagar)) > Math.ceil(MascaraNumero(item.Saldo))) {
                    ShowInfo('El valor a pagar del documento no.' + item.NumeroDocumento + ' no puede ser mayor a su saldo, por lo tanto no se tiene en cuenta');
                    item.Seleccionado = false;
                }
                // Si el item fue des-seleccionado, se busca en la lista de seleccionados para eliminarlo
            }
            else if (item.Seleccionado == null || item.Seleccionado == undefined) {
                $scope.ListaCXC.forEach(function (itemList) {
                    if (item.Codigo == itemList.Codigo) {
                        if (Math.ceil(MascaraNumero(item.ValorPagar)) > Math.ceil(MascaraNumero(item.Saldo))) {
                            itemList.ValorPagar = 0
                            ShowInfo('El valor a pagar del documento no.' + item.NumeroDocumento + ' no puede ser mayor a su saldo, por lo tanto no se tiene en cuenta');
                        }
                    }
                });
            }

            Calcular();
        }
        function Calcular() {
            var ValorTotalCuentas = 0;
            $scope.Modelo.ValorPagoTransferencia = 0;
            $scope.Modelo.ValorPagoCheque = 0;
            $scope.Modelo.ValorPagoEfectivo = 0;

            if ($scope.ListaCXC.length > 0) {
                $scope.ListaCXC.forEach(function (item) {
                    if (item.Seleccionado == true) {
                        ValorTotalCuentas = Math.ceil(MascaraNumero(ValorTotalCuentas)) + Math.ceil(MascaraNumero(item.ValorPagar));
                    }
                })
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                    $scope.Modelo.ValorPagoEfectivo = Math.ceil(MascaraNumero(ValorTotalCuentas));
                } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                    $scope.Modelo.ValorPagoCheque = Math.ceil(MascaraNumero(ValorTotalCuentas));
                } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                    $scope.Modelo.ValorPagoTransferencia = Math.ceil(MascaraNumero(ValorTotalCuentas));
                }

            }

            $scope.MaskValores();

        };
        $scope.CalcularGeneral = function () {
            Calcular()
        }
        $scope.ValidarBeneficiario = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Beneficiario = response.data.Datos
                            }
                            else {
                                $scope.Modelo.Beneficiario = undefined
                                ShowError('La identificación Ingresada no es válida o el beneficiario no se encuentra registrado')
                            }
                        }
                    });
            }
        };
        /*Consultar Cuentas Pendientes por pagar*/

        $scope.ConsultarCXC = function () {
            if (DatosRequeridosCXC()) {
                var Filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR, //CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR
                    Tercero: $scope.Modelo.Tercero,
                    DocumentoOrigen: $scope.Modelo.DocumentoOrigen,
                    NumeroDocumento: $scope.NumeroDocumentoOrigen,
                    Oficina: $scope.Oficina,
                    FechaInicial: $scope.FechaInicio,
                    FechaFinal: $scope.FechaFin,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                    CreadaManual: $scope.Modelo.DocumentoOrigen == undefined || $scope.Modelo.DocumentoOrigen.Codigo == 2600 ? 1 : - 1
                };

                DocumentoCuentasFactory.Consultar(Filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.ListaCXC = response.data.Datos
                                $scope.ListaCXC.forEach(function (item) {
                                    item.ValorPagar = item.Saldo
                                });

                                //if ($scope.ListaCXCSeleccionadas.length > 0) {
                                //    $scope.ListaCXC.forEach(function (item) {
                                //        item.ValorPagar = item.Saldo
                                //        $scope.ListaCXCSeleccionadas.forEach(function (itemSeleccionado) {
                                //            if (item.Codigo == itemSeleccionado.Codigo) {
                                //                item.Seleccionado = true;
                                //            }

                                //        });
                                //    });
                                //}
                                $scope.MaskValores()

                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';

                            }
                            else {
                                $scope.ListaCXC = [];
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                    });
            }
        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        function DatosRequeridosCXC() {
            $scope.MensajesErrorCXC = [];
            var continuar = true

            if ($scope.Modelo.Tercero == undefined || $scope.Modelo.Tercero == null || $scope.Modelo.Tercero == "" || $scope.Modelo.Tercero.Codigo == 0) {
                $scope.MensajesErrorCXC.push('Debe ingresar un tercero');
                continuar = false;
            }
            //if ($scope.Modelo.DocumentoOrigen == undefined || $scope.Modelo.DocumentoOrigen == null || $scope.Modelo.DocumentoOrigen.Codigo == 2600) {
            //    $scope.MensajesErrorCXC.push('Debe seleccionar el documento origen');
            //    continuar = false;
            //}
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')

            ) {

            } else if (
                ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesErrorCXC.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorCXC.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar
        }

        $scope.validarFormaPago = function () {
           // if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = true

                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.FechaConsignacion = new Date()
                $scope.Modelo.ValorPagoCheque = ''

                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''


            }
            else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = true
                $scope.ActivarConsignacion = false

                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''

                $scope.Modelo.CuentaBancariaConsignacion = ''
                $scope.Modelo.FechaCheque = new Date()
                $scope.Modelo.FechaConsignacion = null
                $scope.Modelo.ValorPagoTransferencia = ''
                $scope.Modelo.NumeroConsignacion = ''

            }
            else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                $scope.ActivarEfectivo = true
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = false

                $scope.Modelo.CuentaBancariaConsignacion = ''
                $scope.Modelo.FechaConsignacion = null
                $scope.Modelo.ValorPagoTransferencia = ''
                $scope.Modelo.NumeroConsignacion = ''


                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.ValorPagoCheque = ''
            }
            else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_NO_APLICA) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = false

                $scope.Modelo.CuentaBancariaConsignacion = ''
                $scope.Modelo.FechaConsignacion = null
                $scope.Modelo.ValorPagoTransferencia = ''
                $scope.Modelo.NumeroConsignacion = ''


                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.ValorPagoCheque = ''


                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''
            }
        }

        $scope.AsignarOficinaDestino = function (Oficina) {
            $scope.MensajesError = [];
            if (Oficina.Codigo !== 0 && Oficina.Codigo !== undefined && Oficina.Codigo !== null) {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                    CargarCajas()
                    Calcular()
                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE || $scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                    CargarCuentasBancarias()
                    Calcular()
                }
            }
        }
        $scope.ValidarFormaPago = function (Oficina) {
            if ($scope.Modelo.FormaPagoRecaudo.Codigo !== CODIGO_FORMA_PAGO_NO_APLICA) {
                $scope.AsignarOficinaDestino(Oficina)
            }
        }
        /*Funcion Limpiar Modal Movimiento*/
        function limpiarModalMovimiento() {
            $scope.Modal.TerceroModal = {};
            //$scope.ListadoCuentasPUC = [];
            $scope.Modal.ValorBase = '';
            $scope.Modal.ValorDebito = '';
            $scope.Modal.ValorCredito = '';
            $scope.Modal.CuentaPUC = '';
            $scope.Modal.CodigoCuentaPUC = '';
        }

        $scope.ModalMovimiento = function () {
            if ($scope.Modelo.FormaPagoRecaudo.Codigo !== CODIGO_FORMA_PAGO_NO_APLICA) {
                $scope.MensajesError = [];
                $scope.MensajesErrorMovimiento = [];
                if (($scope.Modelo.ValorPagoCheque !== undefined && $scope.Modelo.ValorPagoCheque !== '' && $scope.Modelo.ValorPagoCheque !== null) || ($scope.Modelo.ValorPagoEfectivo !== undefined && $scope.Modelo.ValorPagoEfectivo !== '' && $scope.Modelo.ValorPagoEfectivo !== null) || ($scope.Modelo.ValorPagoTransferencia !== undefined && $scope.Modelo.ValorPagoTransferencia !== '' && $scope.Modelo.ValorPagoTransferencia !== null)) {
                    $scope.MensajesError = [];
                    limpiarModalMovimiento();
                    showModal('modalMovimiento');
                    if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                        $scope.Modal.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                    }
                    if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                        $scope.Modal.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                    }
                    if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                        $scope.Modal.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                    }
                }
                else {
                    $scope.MensajesError.push('Debe Ingresar el valor de la forma de pago seleccionada ');
                    Ventana.scrollTop = 0
                }
            }
            else {
                $scope.MensajesError.push('Debe seleccionar una forma de pago');
                Ventana.scrollTop = 0
            }
        };
        $scope.GuardarMovimiento = function () {
            $scope.MensajesErrorMovimiento = [];
            if (DatosRequeridosMovimiento()) {
                CargarlistadoMovimiento()
            }
        }
        function CargarlistadoMovimiento() {
            closeModal('modalMovimiento');
            $scope.MaskNumero();
            $scope.ListadoMovimiento.push({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CuentaPUC: $scope.Modal.CuentaPUC,
                //CentroCosto: $scope.Modelo.CentroCosto,
                ValorBase: MascaraNumero($scope.Modal.ValorBase),
                ValorDebito: MascaraNumero($scope.Modal.ValorDebito),
                ValorCredito: MascaraNumero($scope.Modal.ValorCredito),
                Tercero: $scope.Modal.TerceroModal,
                //GeneraCuenta: 0,
                //Observaciones: '',
                //Prefijo: '',
                //CodigoAnexo: '',
                //SufijoCodigoAnexo: '',
                //CampoAuxiliar: '',
                TerceroParametrizacion: { Codigo: 3100 },
                DocumentoCruce: { Codigo: 2800 },
                CentroCostoParametrizacion: { Codigo: 3200 }
            });
            CalcularMovimiento()
            $scope.MaskValores();
        }
        /*Funcion Calcular Totales*/
        function CalcularMovimiento() {
            $scope.Modelo.Debito = 0
            $scope.Modelo.Credito = 0
            $scope.Modelo.Diferencia = 0
            /*realiza el recorrido para  obtener  los totales */
            if ($scope.ListadoMovimiento.length > 0) {
                $scope.ListadoMovimiento.forEach(function (item) {
                    $scope.Modelo.Debito = Math.ceil($scope.Modelo.Debito) + Math.ceil(item.ValorDebito)
                    $scope.Modelo.Credito = Math.ceil($scope.Modelo.Credito) + Math.ceil(item.ValorCredito)
                })
            }
            $scope.Modelo.Diferencia = Math.ceil(($scope.Modelo.Debito) - parseInt($scope.Modelo.Credito))
        }

        function DatosRequeridosMovimiento() {
            $scope.MensajesErrorMovimiento = [];
            var continuar = true;
            if ($scope.Modal.CuentaPUC == undefined || $scope.Modal.CuentaPUC == null || $scope.Modal.CuentaPUC == "") {
                $scope.MensajesErrorMovimiento.push('Debe Seleccionar una Cuenta Puc');
                continuar = false;
            }
            if ($scope.Modal.CuentaPUC.ExigeValorBase == 1) {
                if ($scope.Modal.ValorBase == 0 || $scope.Modal.ValorBase == "") {
                    $scope.MensajesErrorMovimiento.push('Debe ingresar el  valor base ');
                    continuar = false;
                }
            }
            if ($scope.Modal.CuentaPUC.ExigeTercero == 1) {
                if ($scope.Modal.TerceroModal == undefined || $scope.Modal.TerceroModal == "") {
                    $scope.MensajesErrorMovimiento.push('Debe ingresar el tercero');
                    continuar = false;
                }
            }
            else {
                $scope.Modal.TerceroModal = { Codigo: 0 };
            }
            if ($scope.Modal.ValorDebito > 0 && $scope.Modal.ValorCredito > 0) {
                $scope.MensajesErrorMovimiento.push('Debe ingresar solo un valor ya sea Crédito o Débito');
                continuar = false;
            }
            if ($scope.Modal.ValorDebito == undefined || $scope.Modal.ValorDebito == null || $scope.Modal.ValorDebito == "") {
                $scope.Modal.ValorDebito = 0;
            }
            if ($scope.Modal.ValorCredito == undefined || $scope.Modal.ValorCredito == null || $scope.Modal.ValorCredito == "") {
                $scope.Modal.ValorCredito = 0;
            }
            if ($scope.Modal.ValorBase == undefined || $scope.Modal.ValorBase == null || $scope.Modal.ValorBase == "") {
                $scope.Modal.ValorBase = 0
            }

            if (continuar == false) {
                Ventana.scrollTop = 0
            }

            return continuar;
        }
        $scope.LimpiarMovimiento = function () {
            $scope.ListadoMovimiento = [];
            $scope.Modelo.Debito = 0;
            $scope.Modelo.Credito = 0;
            $scope.Modelo.Diferencia = 0;
        };

        /*Eliminar Movimiento*/
        $scope.ConfirmacionEliminarMovimiento = function (indice) {
            $scope.MensajeEliminar = { indice };
            $scope.VarAuxi = indice
            showModal('modalEliminarMovimiento');
        };

        $scope.EliminarMovimiento = function (indice) {
            $scope.ListadoMovimiento.splice($scope.VarAuxi, 1);
            closeModal('modalEliminarMovimiento');
            CalcularMovimiento()
        };

        $scope.ConsultarMovimiento = function () {
            if (DatosRequeridosConsutarMovimiento()) {
                if ($scope.Modelo.ConceptoContable !== undefined && $scope.Modelo.ConceptoContable !== null && $scope.Modelo.ConceptoContable !== '') {
                    if ($scope.Modelo.ConceptoContable.Codigo == 0) {
                        //Crédito
                        var item = {};
                        if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                            item.CuentaPUC = { Nombre: $scope.Caja.PlanUnicoCuentas.Nombre, Codigo: $scope.Caja.PlanUnicoCuentas.Codigo }

                        } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                            item.CuentaPUC = { Nombre: $scope.CuentaBancariaCheque.PlanUnicoCuentas.Nombre, Codigo: $scope.CuentaBancariaCheque.PlanUnicoCuentas.Codigo }

                        } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                            item.CuentaPUC = { Nombre: $scope.CuentaBancariaConsignacion.PlanUnicoCuentas.Nombre, Codigo: $scope.CuentaBancariaConsignacion.PlanUnicoCuentas.Codigo }
                        }
                        //item.ValorDebito = 0;
                        //item.GeneraCuenta = 0;
                        //item.Observaciones = '';
                        //item.Prefijo = 0;
                        //item.CodigoAnexo = 0;
                        //item.CampoAuxiliar = '';

                        item.Tercero = $scope.Modelo.Beneficiario;
                        item.TerceroParametrizacion = { Codigo: 3100 };
                        item.DocumentoCruce = { Codigo: 2800 };
                        item.CentroCostoParametrizacion = { Codigo: 3200 };
                        $scope.ListadoMovimiento.push(item);

                        CalcularMovimiento();
                    } else {
                        DetalleConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.ConceptoContable.Codigo }).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) {
                                        response.data.Datos.forEach(function (item) {
                                            var Concepto = {}
                                            if (item.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_DEBITO) {
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                                                    Concepto.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                                                    Concepto.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                                                    Concepto.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia)
                                                }
                                                Concepto.ValorCredito = 0;
                                                if (item.CuentaPUC.AplicarMedioPago > 0) {
                                                    if ($scope.ActivarCambista) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarCheque) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaCheque.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarEfectivo) {
                                                        Concepto.CuentaPUC = $scope.Modelo.Caja.CuentaPUC
                                                    }
                                                    if ($scope.ActivarConsignacion) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }

                                                } else {
                                                    Concepto.CuentaPUC = item.CuentaPUC;
                                                }
                                            }
                                            if (item.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_CREDITO) {
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                                                    Concepto.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                                                    Concepto.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                                                    Concepto.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                }
                                                Concepto.ValorDebito = 0;
                                                if (item.CuentaPUC.AplicarMedioPago > 0) {
                                                    if ($scope.ActivarCambista) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarCheque) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaCheque.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarEfectivo) {
                                                        Concepto.CuentaPUC = $scope.Modelo.Caja.CuentaPUC
                                                    }
                                                    if ($scope.ActivarConsignacion) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }

                                                } else {
                                                    Concepto.CuentaPUC = item.CuentaPUC;
                                                }
                                            }

                                            Concepto.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                                            //Concepto.CuentaPUC = item.CuentaPUC;
                                            Concepto.Tercero = { Codigo: $scope.Modelo.Beneficiario.Codigo };
                                            Concepto.TerceroParametrizacion = { Codigo: 3100 }
                                            Concepto.DocumentoCruce = { Codigo: 2800 }
                                            Concepto.CentroCostoParametrizacion = { Codigo: 3200 }
                                            $scope.ListadoMovimiento.push(Concepto)
                                            CalcularMovimiento()
                                        });
                                    } else {
                                        ShowError('El Concepto contable no se encuentra parametrizado');
                                    }
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }

            }
        };

        function DatosRequeridosConsutarMovimiento() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.ConceptoContable == undefined || $scope.Modelo.ConceptoContable == null || $scope.Modelo.ConceptoContable == "" || $scope.Modelo.ConceptoContable.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar un concepto contable');
                continuar = false;
            }

            if ($scope.Modelo.Beneficiario == undefined || $scope.Modelo.Beneficiario == null || $scope.Modelo.Beneficiario == "") {
                $scope.MensajesError.push('Debe ingresar un beneficiario');
                continuar = false;
            }
            if ($scope.Modelo.FormaPagoRecaudo.Codigo == 4700) {
                $scope.MensajesError.push('Debe seleccionar una forma de pago');
                continuar = false;
            }
            else {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                    if ($scope.Modelo.ValorPagoEfectivo == 0) {
                        $scope.MensajesError.push('El valor ingresado debe ser mayor a 0');
                        continuar = false;
                    }
                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {

                    if ($scope.Modelo.ValorPagoCheque == 0) {
                        $scope.MensajesError.push('El valor ingresado debe ser mayor a 0');
                        continuar = false;
                    }
                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                    if ($scope.Modelo.ValorPagoTransferencia == 0) {
                        $scope.MensajesError.push('El valor ingresado debe ser mayor a 0');
                        continuar = false;
                    }
                }
                else if (($scope.Modelo.ValorPagoCheque !== undefined && $scope.Modelo.ValorPagoCheque !== '' && $scope.Modelo.ValorPagoCheque !== null && $scope.Modelo.ValorPagoEfectivo !== 0) ||
                    ($scope.Modelo.ValorPagoEfectivo !== undefined && $scope.Modelo.ValorPagoEfectivo !== '' && $scope.Modelo.ValorPagoEfectivo !== null && $scope.Modelo.ValorPagoCheque !== 0) ||
                    ($scope.Modelo.ValorPagoTransferencia !== undefined && $scope.Modelo.ValorPagoTransferencia !== '' && $scope.Modelo.ValorPagoTransferencia !== null && $scope.Modelo.ValorPagoTransferencia !== 0)) {
                }
                else {
                    $scope.MensajesError.push('Debe Ingresar el valor de la forma de pago seleccionada');
                    continuar = false;
                }
            }

            if (continuar == false) {
                Ventana.scrollTop = 0
            }


            return continuar;
        }
    /*----------------------------------------------------------------------------------------------------*/
        function FindCierre() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentos: { Codigo: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_INGRESO },
                Sync: true
            };
            var RCierreContable = CierreContableDocumentosFactory.Consultar(filtros)
            if (RCierreContable.ProcesoExitoso === true) {
                if (RCierreContable.Datos.length > 0) {
                    $scope.ListadoTipoDocumentos = RCierreContable.Datos;
                    $scope.ValidarCierre();
                }
            }
        }

        $scope.ValidarCierre = function () {
            if ($scope.ListadoTipoDocumentos.length > 0) {
                var anoFecha = $scope.Modelo.Fecha.getFullYear();
                var mesFecha = $scope.Modelo.Fecha.getMonth();
                for (var k = 0; k < $scope.ListadoTipoDocumentos.length; k++) {
                    var item = $scope.ListadoTipoDocumentos[k];
                    var mes = MascaraNumero(item.Mes.CampoAuxiliar2);
                    if (item.Ano === anoFecha && mes === (mesFecha + 1) && item.EstadoCierre.Codigo === 18002) {
                        ShowError('La fecha se encuentra en un mes y año con cierre contable');
                        $scope.Modelo.Fecha = '';
                        $scope.PeriodoValido = false
                        break;
                    }
                }
            }
        };
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                $scope.PeriodoValido = true
                FindCierre()
                if ($scope.PeriodoValido) {
                    showModal('modalConfirmacionGuardar');
                }
            }
            else {
                Ventana.scrollTop = 0
            }
        };


        /*Guardar/Modificar Comprobante De ingreso*/
        $scope.Guardar = function () {
            if (DatosRequeridos()) {
                $scope.Modelo.CodigoAlterno = 0;
                $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_COMPROBANTE_INGRESO;//CODIGO_TIPO_DOCUMENTO_COMPROBANTE_INGRESO
                $scope.Modelo.Vehiculo = $scope.Modelo.Placa;
                $scope.Modelo.FormaPagoDocumento = { Codigo: $scope.Modelo.FormaPagoRecaudo.Codigo };
                $scope.Modelo.Estado = $scope.Estado.Codigo;
                $scope.Modelo.ValorAlterno = 0;
                $scope.Modelo.ConceptoContable = $scope.Modelo.ConceptoContable.Codigo;
                $scope.Modelo.Tercero = { Codigo: $scope.Modelo.Tercero.Codigo };

                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                    $scope.Modelo.Caja = $scope.Modelo.Caja;
                    $scope.Modelo.ValorPagoEfectivo = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                    $scope.Modelo.CuentaBancaria = { Codigo: 0 };
                    $scope.Modelo.FechaPagoRecaudo = new Date();
                    $scope.Modelo.DestinoIngreso = { Codigo: 4801 }; //Codigo Destino ingreso Caja
                    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                    $scope.Modelo.CuentaBancaria = { Codigo: $scope.Modelo.CuentaBancariaCheque.CodigoCuenta };
                    $scope.Modelo.FechaPagoRecaudo = $scope.Modelo.FechaCheque;
                    $scope.Modelo.ValorPagoCheque = MascaraNumero($scope.Modelo.ValorPagoCheque);
                    $scope.Modelo.Numeracion = $scope.Modelo.NumeroCheque
                    $scope.Modelo.Caja = { Codigo: 0 };
                    $scope.Modelo.DestinoIngreso = { Codigo: 4802 };//Codigo Destino ingreso Cuenta Bancaria
                    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoCheque);
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                    $scope.Modelo.CuentaBancaria = { Codigo: $scope.Modelo.CuentaBancariaConsignacion.CodigoCuenta };
                    $scope.Modelo.FechaPagoRecaudo = $scope.Modelo.FechaConsignacion;
                    $scope.Modelo.NumeroPagoRecaudo = $scope.Modelo.NumeroConsignacion;
                    $scope.Modelo.ValorPagoTransferencia = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                    $scope.Modelo.Caja = { Codigo: 0 };
                    $scope.Modelo.DestinoIngreso = { Codigo: 4802 };//Codigo Destino ingreso Cuenta Bancaria
                    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                }

                $scope.Modelo.GeneraConsignacion = "";
                $scope.Modelo.Detalle = $scope.ListadoMovimiento;
                $scope.Modelo.Detalle.forEach(item => {
                    item.Tercero = {Codigo: item.Tercero.Codigo}
                });

                $scope.ListaCuentas = [];

                $scope.ListaCXC.forEach(function (item) {
                    if (item.Seleccionado == true) {
                        $scope.ListaCuentas.push(item)
                    }
                })


                $scope.Modelo.Cuentas = $scope.ListaCuentas;

                $scope.ListaDetalleCuentas = [];


                $scope.ListaCXC.forEach(function (item) {
                    if (item.Seleccionado == true) {
                        $scope.ListaDetalleCuentas.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoDocumentoCuenta: item.Codigo, ValorPago: item.ValorPagar })
                    }
                })

                $scope.Modelo.DetalleCuentas = $scope.ListaDetalleCuentas;


                DocumentoComprobantesFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if ($scope.Modelo.Numero == 0) {
                                ShowSuccess('Se guardó el comprobante de ingreso N.' + response.data.Datos); // ShowSuccess('Se guardó el comprobante de ingreso N.' + response.data.Datos);
                            }
                            else {
                                ShowSuccess('Se modificó el comprobante de ingreso N.' + response.data.Datos); //ShowSuccess('Se modificó el comprobante de ingreso N.' + response.data.Datos);
                            }
                            closeModal('modalConfirmacionGuardar');
                            document.location.href = '#!ConsultarComprobanteIngresos/' + response.data.Datos; // document.location.href = '#!ConsultarComprobanteIngresos/' + response.data.Datos;
                            $scope.MaskValores();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                Ventana.scrollTop = 0
                closeModal('modalConfirmacionGuardarComprobanteEgreso');
                $scope.MaskValores();
            }
        }
        /*Datos Requeridos Guardar o Modificar Comprobante*/
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == null || $scope.Modelo.Fecha == "") {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }

            if ($scope.Modelo.Tercero == undefined || $scope.Modelo.Tercero == null || $scope.Modelo.Tercero == "") {
                $scope.MensajesError.push('Debe ingresar un tercero ');
                continuar = false;
            }
            if ($scope.Modelo.Placa == undefined || $scope.Modelo.Placa == null || $scope.Modelo.Placa == "") {
                $scope.Modelo.Placa = { Codigo: 0, Placa: '' };
            }
            else if ($scope.PlacaValida == false) {
                $scope.MensajesError.push('La placa ingresada no es valida');
                continuar = false;
            }
            if ($scope.Modelo.Beneficiario == undefined || $scope.Modelo.Beneficiario == null || $scope.Modelo.Beneficiario == "") {
                $scope.MensajesError.push('Debe ingresar un beneficiario');
                continuar = false;
            }
            if ($scope.Modelo.DocumentoOrigen == undefined || $scope.Modelo.DocumentoOrigen == null || $scope.Modelo.DocumentoOrigen.Codigo == 2600) {
                $scope.MensajesError.push('Debe seleccionar el documento origen');
                Continuar = false;
            }
            if ($scope.Modelo.FormaPagoRecaudo.Codigo == 4700) {
                $scope.MensajesError.push('Debe seleccionar una forma de pago');
                continuar = false;
            }
            if ($scope.Modelo.OficinaDestino.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar una oficina');
                continuar = false;
            }
            else {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_TRANSFERENCIA) {
                    if ($scope.Modelo.FechaConsignacion == undefined || $scope.Modelo.FechaConsignacion == null || $scope.Modelo.FechaConsignacion == "") {
                        $scope.MensajesError.push('Debe seleccionar la fecha de la Transferencia ');
                        continuar = false;
                    }
                    if ($scope.Modelo.CuentaBancariaConsignacion == undefined || $scope.Modelo.CuentaBancariaConsignacion == null || $scope.Modelo.CuentaBancariaConsignacion == "") {
                        $scope.MensajesError.push('Debe seleccionar la cuenta bancaria de la Transferencia ');
                        continuar = false;
                    }
                    if ($scope.Modelo.ValorPagoTransferencia == undefined || $scope.Modelo.ValorPagoTransferencia == null || $scope.Modelo.ValorPagoTransferencia == "") {
                        $scope.MensajesError.push('Debe ingresar el valor de la Transferencia ');
                        continuar = false;
                    }
                    if ($scope.Modelo.NumeroConsignacion == undefined || $scope.Modelo.NumeroConsignacion == null || $scope.Modelo.NumeroConsignacion == "") {
                        $scope.Modelo.NumeroConsignacion = 0
                    }
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_CHEQUE) {
                    if ($scope.Modelo.FechaCheque == undefined || $scope.Modelo.FechaCheque == null || $scope.Modelo.FechaCheque == "") {
                        $scope.MensajesError.push('Debe seleccionar la fecha del cheque ');
                        continuar = false;
                    }
                    if ($scope.Modelo.NumeroCheque == undefined || $scope.Modelo.NumeroCheque == null || $scope.Modelo.NumeroCheque == "") {
                        $scope.MensajesError.push('Debe ingresar el cheque ');
                        continuar = false;
                    }
                    if ($scope.Modelo.CuentaBancariaCheque == undefined || $scope.Modelo.CuentaBancariaCheque == null || $scope.Modelo.CuentaBancariaCheque == "") {
                        $scope.MensajesError.push('Debe sngresar  la cuenta bancaria del cheque ');
                        continuar = false;
                    }
                    if ($scope.Modelo.ValorPagoCheque == undefined || $scope.Modelo.ValorPagoCheque == null || $scope.Modelo.ValorPagoCheque == "") {
                        $scope.MensajesError.push('Debe ingresar el valor del Cheque  ');
                        continuar = false;
                    }
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_FORMA_PAGO_EFECTIVO) {
                    if ($scope.Modelo.Caja == undefined || $scope.Modelo.Caja == null || $scope.Modelo.Caja == "") {
                        $scope.MensajesError.push('Debe ingresar la caja del efectivo');
                        continuar = false;
                    }
                    if ($scope.Modelo.ValorPagoEfectivo == undefined || $scope.Modelo.ValorPagoEfectivo == null || $scope.Modelo.ValorPagoEfectivo == "") {
                        $scope.MensajesError.push('Debe ingresar el valor del efectivo');
                        continuar = false;
                    }

                }
            }

            if ($scope.Modelo.Debito == 0 && $scope.Modelo.Credito == 0 && $scope.Modelo.Diferencia == 0) {
                $scope.MensajesError.push('Debe ingresar minimo un movimiento en el comprobante de ingreso'); //$scope.MensajesError.push('Debe ingresar minimo un movimiento en el comprobante de ingreso');
                continuar = false;
            }
            if ($scope.Modelo.Diferencia !== 0) {
                $scope.MensajesError.push('El comprobante de ingreso se encuentra desbalancedado');
                continuar = false;
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar;
        }



        $scope.VolverMaster = function () {
            if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarComprobanteIngresos/' + $scope.Modelo.Numero; //document.location.href = '#!ConsultarComprobanteIngresos/' + $scope.Modelo.Numero;
            } else {
                document.location.href = '#!ConsultarComprobanteIngresos'; //document.location.href = '#!ConsultarComprobanteIngresos';
            }
        };

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskPlaca = function () {
            try { $scope.Vehiculo.Placa = MascaraPlaca($scope.Vehiculo.Placa) } catch (e) { };
        };

        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
            for (var i = 0; i < $scope.ListaCXC.length; i++) {
                $scope.ListaCXC[i].ValorPagar = MascaraValores($scope.ListaCXC[i].ValorPagar) //$scope.ListaCXC[i].ValorPagar = MascaraValores($scope.ListaCXC[i].ValorPagar)
            }
        };
        $scope.ValidarSeleccion = function (item) {
            if (item.Seleccionado == true) {
                if ($scope.Modelo.DocumentoOrigen.Codigo == 2604) { // anticipo
                    var cont = 0
                    for (var i = 0; i < $scope.ListaCXC.length; i++) {
                        if ($scope.ListaCXC[i].Codigo !== item.Codigo && $scope.ListaCXC[i].Seleccionado == true) {
                            cont++
                        }
                    }
                    if (cont > 0) {
                        item.Seleccionado = false
                        ShowInfo('Solo puede escojer una cuenta por pagar cuando el documento origen es anticipo')
                    } else {
                        $scope.Modelo.NumeroDocumentoOrigen = item.NumeroDocumento
                        $scope.Calcular(item)
                    }
                }
                // Si el item fue des-seleccionado, se busca en la lista de seleccionados para eliminarlo
            }
            $scope.CalcularGeneral()

        }
        $scope.AsignarBeneficiario = function () {
            try {
                if ($scope.Modelo.Tercero.Codigo > 0) {
                    $scope.Modelo.Beneficiario = $scope.Modelo.Tercero

                }
            } catch (e) {
                $scope.Modelo.Beneficiario = ''
            }
        }
    }]);