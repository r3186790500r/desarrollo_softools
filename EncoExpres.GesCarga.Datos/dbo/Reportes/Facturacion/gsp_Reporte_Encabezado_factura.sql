﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */



	
PRINT 'gsp_Reporte_Encabezado_factura'

DROP PROCEDURE gsp_Reporte_Encabezado_factura
GO

CREATE PROCEDURE gsp_Reporte_Encabezado_factura (
@par_Empr_Codigo NUMERIC  
, @par_Numero_Factura  NUMERIC
)
AS BEGIN
SELECT DISTINCT
ENFA.Numero Numero_Factura
, EMPR.Nombre_Razon_Social
, CONCAT(EMPR.Numero_Identificacion,'-',EMPR.Digito_Chequeo) IdentificacionEmpresa
, EMPR.Direccion
, EMPR.Telefonos
, CIOF.Nombre CiudadOficina
, ENFA.Fecha
,FPVE.Campo1 FormaPago
,ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'')  
+' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS Cliente
, TECL.Direccion
, TECL.Numero_Identificacion IdentificacionCliente
, TECL.Telefonos 
, dbo.funcCantidadConLetra(ENFA.Valor_Factura) valorLetra
, ENFA.Observaciones ObservacionesFactura
, ENFA.Subtotal 
, ENFA.Valor_Otros_Conceptos
, ENFA.Valor_Descuentos
, ENFA.Valor_Impuestos
, ENFA.Valor_Factura
, ENFA.Resolucion_Facturacion
FROM Encabezado_Facturas ENFA
, Empresas EMPR 
, Terceros TECL
, Oficinas OFIC
, Ciudades CIOF
, Valor_Catalogos FPVE
WHERE ENFA.EMPR_Codigo = EMPR.Codigo 

AND ENFA.EMPR_Codigo = TECL.EMPR_Codigo 
AND ENFA.TERC_Cliente = TECL.Codigo

AND OFIC.EMPR_Codigo = CIOF.EMPR_Codigo
AND OFIC.CIUD_Codigo = CIOF.Codigo

AND ENFA.EMPR_Codigo = FPVE.EMPR_Codigo
AND ENFA.CATA_FPVE_Codigo = FPVE.Codigo

AND ENFA.EMPR_Codigo = @par_Empr_Codigo
AND ENFA.Numero = @par_Numero_Factura
END 
GO