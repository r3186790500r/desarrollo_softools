﻿Imports Newtonsoft.Json

Namespace Gesphone

    ''' <summary>
    ''' Clase encargada de definir la informacion de la estructura de una tabla de base de datos OffLine.
    ''' </summary>
    Public Class Schema

        ''' <summary>
        ''' Obtiene o establece el nombre para una columna.
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty("column")>
        Public Property Column As String

        ''' <summary>
        ''' Obtiene o establece la informacion de tipo de dato de la columna o valor o configuracion de llave foranea. 
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty("value")>
        Public Property Value As String

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Schema"/>
        ''' </summary>
        ''' <param name="column"></param>
        ''' <param name="value"></param>
        Sub New(column As String, value As String)
            Me.Column = column
            Me.Value = value
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Schema"/>
        ''' </summary>
        ''' <param name="config"></param>
        Sub New(config As SchemaOffLine)
            Me.Column = config.Column
            Me.Value = config.Value
        End Sub

    End Class

End Namespace

