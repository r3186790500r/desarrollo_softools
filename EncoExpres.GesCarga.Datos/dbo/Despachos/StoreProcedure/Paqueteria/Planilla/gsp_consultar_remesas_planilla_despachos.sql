﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 30/09/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_consultar_remesas_planilla_despachos'
GO
DROP PROCEDURE gsp_consultar_remesas_planilla_despachos
GO     
CREATE PROCEDURE gsp_consultar_remesas_planilla_despachos 
(        
 @par_EMPR_Codigo SMALLINT,   
 @par_ENPD_Numero NUMERIC = NULL      
)        
AS        
BEGIN            
 SELECT        
  EMPR_Codigo,        
  ISNULL(ENPD_Numero, 0) AS ENPD_Numero,      
  ISNULL(ENRE_Numero, 0) AS ENRE_Numero,   
  ISNULL(Numero_Documento_Remesa, 0) AS Numero_Documento_Remesa  
   
 FROM        
 Detalle_Planilla_Despachos

  WHERE        
   EMPR_Codigo = @par_EMPR_Codigo        
   AND ENPD_Numero = ISNULL(@par_ENPD_Numero , ENPD_Numero)   
    
END  
GO   