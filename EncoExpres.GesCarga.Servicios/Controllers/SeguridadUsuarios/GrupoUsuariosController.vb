﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Negocio.Seguridad

''' <summary>
''' Controlador <see cref="GrupoUsuariosController"/>
''' </summary>
<Authorize>
Public Class GrupoUsuariosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As GrupoUsuarios) As Respuesta(Of IEnumerable(Of GrupoUsuarios))
        Return New LogicaGrupoUsuarios(CapaPersistenciaGrupoUsuarios).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As GrupoUsuarios) As Respuesta(Of GrupoUsuarios)
        Return New LogicaGrupoUsuarios(CapaPersistenciaGrupoUsuarios).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As GrupoUsuarios) As Respuesta(Of Long)
        Return New LogicaGrupoUsuarios(CapaPersistenciaGrupoUsuarios).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As GrupoUsuarios) As Respuesta(Of Boolean)
        Return New LogicaGrupoUsuarios(CapaPersistenciaGrupoUsuarios).Anular(entidad)
    End Function


End Class
