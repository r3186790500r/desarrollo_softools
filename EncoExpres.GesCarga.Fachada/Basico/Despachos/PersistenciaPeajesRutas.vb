﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaPeajesRutas
        Implements IPersistenciaBase(Of PeajesRutas)

        Public Function Consultar(filtro As PeajesRutas) As IEnumerable(Of PeajesRutas) Implements IPersistenciaBase(Of PeajesRutas).Consultar
            Return New RepositorioPeajesRutas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PeajesRutas) As Long Implements IPersistenciaBase(Of PeajesRutas).Insertar
            Return New RepositorioPeajesRutas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PeajesRutas) As Long Implements IPersistenciaBase(Of PeajesRutas).Modificar
            Return New RepositorioPeajesRutas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PeajesRutas) As PeajesRutas Implements IPersistenciaBase(Of PeajesRutas).Obtener
            Return New RepositorioPeajesRutas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As PeajesRutas) As Boolean
            Return New RepositorioPeajesRutas().Anular(entidad)
        End Function
        Public Function EliminarPeajesRutas(entidad As PeajesRutas) As Boolean
            Return New RepositorioPeajesRutas().EliminarPeajesRutas(entidad)
        End Function


    End Class

End Namespace

