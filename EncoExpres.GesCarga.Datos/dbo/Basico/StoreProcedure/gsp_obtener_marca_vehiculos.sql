﻿PRINT 'gsp_obtener_marca_vehiculos'
GO
DROP PROCEDURE gsp_obtener_marca_vehiculos
GO
CREATE PROCEDURE gsp_obtener_marca_vehiculos
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC
)
AS 
BEGIN
	SELECT
		1 AS Obtener,
		MAVE.EMPR_Codigo,
		MAVE.Codigo,
		MAVE.Codigo_Alterno,
		MAVE.Nombre,
		MAVE.Estado,
		ROW_NUMBER() OVER(ORDER BY MAVE.Nombre) AS RowNumber
	FROM
		Marca_Vehiculos MAVE
	WHERE

		MAVE.EMPR_Codigo = @par_EMPR_Codigo
		AND MAVE.Codigo = @par_Codigo
END
GO