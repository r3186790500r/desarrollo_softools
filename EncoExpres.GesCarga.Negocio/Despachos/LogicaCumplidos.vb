﻿Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Fachada.Despachos

Namespace Despachos
    Public NotInheritable Class LogicaCumplidos
        Inherits LogicaBase(Of Cumplido)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Cumplido)

        Sub New(persistencia As IPersistenciaBase(Of Cumplido))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Cumplido) As Respuesta(Of IEnumerable(Of Cumplido))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Cumplido))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Cumplido))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Cumplido) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Cumplido) As Respuesta(Of Cumplido)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Cumplido)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Cumplido)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>

        Public Function Anular(entidad As Cumplido) As Respuesta(Of Cumplido)
            Return New Respuesta(Of Cumplido)(CType(_persistencia, PersistenciaCumplidos).Anular(entidad))
        End Function

        Public Function ConsultarTiempos(filtro As Cumplido) As Respuesta(Of Cumplido)
            Dim consulta = (CType(_persistencia, PersistenciaCumplidos).ConsultarTiempos(filtro))

            If IsNothing(consulta) Then
                Return New Respuesta(Of Cumplido)(CType(_persistencia, PersistenciaCumplidos).ConsultarTiempos(filtro))
            End If

            Return New Respuesta(Of Cumplido)(consulta)
        End Function

    End Class

End Namespace