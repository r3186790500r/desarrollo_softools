﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 22/09/2021 
-- Módulo: Gesphone

PRINT 'gsp_consultar_detalle_etiquetas_preimpresas_sincronizacion'
GO
DROP PROCEDURE gsp_consultar_detalle_etiquetas_preimpresas_sincronizacion
GO
CREATE PROCEDURE dbo.gsp_consultar_detalle_etiquetas_preimpresas_sincronizacion      
(                      
  @par_EMPR_Codigo SMALLINT,
  @par_OFIC_Codigo SMALLINT,
  @par_TERC_Codigo_Responsable INTEGER
)                      
AS                       
BEGIN
  
    SELECT * FROM Detalle_Asignacion_Etiquetas_Preimpresas 
    WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND OFIC_Codigo = @par_OFIC_Codigo 
    AND TERC_Codigo_Responsable = @par_TERC_Codigo_Responsable
    AND Anulado = 0
    AND (ENRE_Numero IS NULL OR ENRE_Numero <= 0)  
                  
END    
GO