﻿PRINT 'gsp_consultar_lista_distribucion_correos'
GO
DROP PROCEDURE gsp_consultar_lista_distribucion_correos
GO
CREATE PROCEDURE gsp_consultar_lista_distribucion_correos  
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC = NULL,  
@par_EVCO_Codigo NUMERIC = NULL  
)  
AS  
BEGIN  
SELECT   
 LDCO.EMPR_Codigo,  
 LDCO.EVCO_Codigo,  
 LDCO.ID,  
 LDCO.Email,  
 LDCO.TERC_Codigo_Empleado,  
 LDCO.Estado,  
 ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1,'') + ' ' + ISNULL(TERC.Apellido2,'') + ' ' + ISNULL(TERC.Razon_Social,'')  AS Tercero  
FROM  
 Lista_Distribucion_Correos LDCO,  
 Terceros TERC  
WHERE   
 LDCO.EMPR_Codigo = TERC.EMPR_Codigo  
 AND LDCO.TERC_Codigo_Empleado = TERC.Codigo  
 AND LDCO.EMPR_Codigo = @par_EMPR_Codigo  
 AND LDCO.ID = ISNULL(@par_Codigo,LDCO.ID)  
 AND LDCO.EVCO_Codigo = ISNULL(@par_EVCO_Codigo, LDCO.EVCO_Codigo)  
END  
GO  