﻿EncoExpresApp.factory('ReporteMinisterioFactory', ['$http', function ($http) {

    var urlService = GetUrl('ReporteMinisterio');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ReporteMinisterio';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ReportarRNDC = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ReportarRNDC';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ReporteMinisterio';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarRemesasSinManifiesto = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasSinManifiesto';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    return factory;
}]);