﻿EncoExpresApp.controller("GestionarSucursalSIESACtrl", ['$scope', '$timeout', 'SucursalesSIESAFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', function ($scope, $timeout, SucursalesSIESAFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory) {

    $scope.Titulo = 'GESTIONAR SUCURSALES SIESA';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Sucursales siesa' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SUCURSALES_SIESA);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.ListadoPerfil = [];
    $scope.ListadoTipoConector = [];
    $scope.ListadoTipoConectores = [];
    $scope.ListadoFormaPago = [];
    $scope.FormasPago = [];
    $scope.verFormasDePago = false
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoSIESA: ''        
    }
    $scope.Modal = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        Estado: 1
    }

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
    }

    /*Cargar el combo de Perfiles Terceros*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_PERFIL_TERCEROS } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoPerfil = []
                    $scope.ListadoPerfil = response.data.Datos
                    $scope.ListadoPerfil = $scope.ListadoPerfil.filter(perfil => perfil.Codigo != 1400);
                    if ($scope.Perfil !== undefined && $scope.Perfil !== '' && $scope.Perfil !== null) {
                        $scope.Modelo.Perfil = $linq.Enumerable().From($scope.ListadoPerfil).First('$.Codigo ==' + $scope.Perfil)
                    }
                    else {
                        
                    }
                }
                else {
                    $scope.ListadoPerfil = []
                }
            }
        }, function (response) {
        });

    /*Cargar el combo de Tipo conectores*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPOS_CONECTOR_SIESA } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoTipoConector = []
                    $scope.ListadoTipoConectores = response.data.Datos
                    $scope.ListadoTipoConector = $scope.ListadoTipoConectores.filter(conector => conector.Codigo != 23815);
                    if ($scope.TipoConector !== undefined && $scope.TipoConector !== '' && $scope.TipoConector !== null) {
                        $scope.Modelo.TipoConector = $linq.Enumerable().From($scope.ListadoTipoConector).First('$.Codigo ==' + $scope.TipoConector)
                    }
                    else {
                        
                    }
                }
                else {
                    $scope.ListadoTipoConector = []
                }
            }
        }, function (response) {
        });

    /*Cargar el combo de Formas de Pago*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoFormaPago = []
                    $scope.FormasPago = response.data.Datos
                    $scope.ListadoFormaPago = $scope.FormasPago.filter(pago => pago.Codigo != 4900);
                    if ($scope.FormaPago !== undefined && $scope.FormaPago !== '' && $scope.FormaPago !== null) {
                        $scope.Modelo.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.FormaPago)
                    }
                    else {
                        
                    }
                }
                else {
                    $scope.ListadoFormasDePago = []
                }
            }
        }, function (response) {
        });

    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando Sucursal código No. ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando  Sucursal No.' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        SucursalesSIESAFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.Modelo.Codigo = response.data.Datos.Codigo
                    $scope.Perfil = response.data.Datos.Perfil_Terceros.Codigo
                    $scope.TipoConector = response.data.Datos.Tipo_Conector.Codigo
                    $scope.FormaPago = response.data.Datos.Forma_Pago.Codigo
                    $scope.Modelo.CodigoSIESA = response.data.Datos.Codigo_SIESA

                    if ($scope.ListadoPerfil.length > 0) {
                        $scope.Modelo.Perfil = $linq.Enumerable().From($scope.ListadoPerfil).First('$.Codigo ==' + $scope.Perfil)
                    }

                    if ($scope.ListadoTipoConector.length > 0) {
                        $scope.Modelo.TipoConector = $linq.Enumerable().From($scope.ListadoTipoConector).First('$.Codigo ==' + $scope.TipoConector)
                        $scope.FormasDePago()
                    }

                    if ($scope.ListadoFormaPago.length > 0) {
                        $scope.Modelo.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.FormaPago)
                    }

                }
                else {
                    ShowError('No se logro consultar la sucursal código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarConceptoContables';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarConceptoContables';
            });

        blockUI.stop();
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;
        if ($scope.Modelo.Perfil == undefined || $scope.Modelo.Perfil == '' || $scope.Modelo.Perfil == '(NO APLICA)') {
            $scope.MensajesError.push('Debe ingresar el perfil de la sucursal');
            continuar = false;
        }

        if ($scope.Modelo.TipoConector == undefined || $scope.Modelo.TipoConector == '' || $scope.Modelo.Perfil == '(NO APLICA)') {
            $scope.MensajesError.push('Debe ingresar el tipo de conector de la sucursal');
            continuar = false;
        }

        if ($scope.Modelo.FormaPago == undefined || $scope.Modelo.FormaPago == '') {
            $scope.MensajesError.push('Debe ingresar el la forma de pago de la sucursal');
            continuar = false;
        }

        if ($scope.Modelo.CodigoSIESA == undefined || $scope.Modelo.CodigoSIESA == '' || $scope.Modelo.Perfil == '(NO APLICA)') {
            $scope.MensajesError.push('Debe ingresar el codigo de la sucursal');
            continuar = false;
        }

        return continuar;
    }

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarSucursalesSIESA/' + $scope.Modelo.Codigo;
    };

    $scope.MaskMayus = function (option) {
        MascaraMayusGeneral($scope)
    };

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.FormasDePago = function () {

        if ($scope.Modelo.TipoConector.Codigo == 23807) {

            $scope.ListadoFormaPago = $scope.FormasPago.filter(pago => pago.Codigo == 4901);
            if ($scope.FormaPago !== undefined && $scope.FormaPago !== '' && $scope.FormaPago !== null) {
                $scope.Modelo.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + 4901)
            }
            $scope.verFormasDePago = true
        } else if ($scope.Modelo.TipoConector.Codigo == 23806) {
            $scope.ListadoFormaPago = $scope.FormasPago.filter(pago => pago.Codigo != 4900);
            if ($scope.FormaPago !== undefined && $scope.FormaPago !== '' && $scope.FormaPago !== null) {
                $scope.Modelo.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.FormaPago)
            }

            $scope.verFormasDePago = true
        } else {
            $scope.ListadoFormaPago = $scope.FormasPago.filter(pago => pago.Codigo == 4900);
            $scope.Modelo.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + 4900)
            $scope.verFormasDePago = false
        }
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {
            var guardar = true
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
                Perfil_Terceros: { Codigo: $scope.Modelo.Perfil.Codigo },
                Tipo_Conector: { Codigo: $scope.Modelo.TipoConector.Codigo },
                Forma_Pago: { Codigo: $scope.Modelo.FormaPago.Codigo },
                Codigo_SIESA: $scope.Modelo.CodigoSIESA,
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            };

            SucursalesSIESAFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos.length > 0) {

                            response.data.Datos.forEach(function (item) {
                                if (item.Perfil_Terceros.Codigo == $scope.Modelo.Perfil.Codigo && item.Tipo_Conector.Codigo == $scope.Modelo.TipoConector.Codigo && item.Codigo_SIESA == $scope.Modelo.CodigoSIESA && item.Forma_Pago.Codigo == $scope.Modelo.FormaPago.Codigo) {
                                    ShowError('Ya existe una sucursal con estos datos');
                                    $scope.Modelo.CodigoSIESA = ''
                                    guardar = false
                                }                             

                            });

                            if (guardar) {
                                SucursalesSIESAFactory.Guardar(filtros).
                                    then(function (response) {
                                        if (response.data.ProcesoExitoso == true) {
                                            if (response.data.Datos > 0) {
                                                ShowSuccess('Se guardó la sucursal "' + $scope.Modelo.CodigoSIESA + '"');
                                                location.href = '#!ConsultarSucursalesSIESA/' + response.data.Datos;
                                            }
                                        }
                                        $scope.MaskValores();
                                    }, function (response) {
                                        ShowError('No se pudo guardar la sucursal, porfavor contacte al administrador del sistema');
                                    });
                            }
                           
                        } else {
                            SucursalesSIESAFactory.Guardar(filtros).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso == true) {
                                        if (response.data.Datos > 0) {
                                            ShowSuccess('Se guardó la sucursal "' + $scope.Modelo.CodigoSIESA + '"');
                                            location.href = '#!ConsultarSucursalesSIESA/' + response.data.Datos;
                                        }
                                    }
                                    $scope.MaskValores();
                                }, function (response) {
                                    ShowError('No se pudo guardar la sucursal, porfavor contacte al administrador del sistema');
                                });

                        }
                    }
                    $scope.MaskValores();
                }, function (response) {
                    ShowError('No se pudo consultar la sucursal, porfavor contacte al administrador del sistema');
                });


        }
    };
}]);