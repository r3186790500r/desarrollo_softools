﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref="RepositorioTarifarioCompras"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTarifarioCompras
        Inherits RepositorioBase(Of TarifarioCompras)

        Public Overrides Function Consultar(filtro As TarifarioCompras) As IEnumerable(Of TarifarioCompras)
            Dim lista As New List(Of TarifarioCompras)
            Dim item As TarifarioCompras

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado.Codigo > -1 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If Not IsNothing(filtro.FechaInicio) Then
                    If filtro.FechaInicio > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Inicio", filtro.FechaInicio, SqlDbType.DateTime) '	datetime = null,
                    End If
                End If
                If Not IsNothing(filtro.FechaFin) Then
                    If filtro.FechaFin > Date.MinValue And filtro.FechaFin >= filtro.FechaInicio Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Fin", filtro.FechaFin, SqlDbType.DateTime) '	datetime = null,
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_tarifario_Compras]")

                While resultado.Read
                    item = New TarifarioCompras(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As TarifarioCompras) As Long
            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 70) 'NUMERIC = 0,
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre) 'VARCHAR ,
                conexion.AgregarParametroSQL("@par_Tarifario_Base", entidad.TarifarioBase) 'VARCHAR ,
                If entidad.FechaInicio > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Inicio", entidad.FechaInicio, SqlDbType.DateTime) '	datetime = null,
                End If
                If entidad.FechaFin > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Fin", entidad.FechaFin, SqlDbType.DateTime) '	datetime = null,
                End If
                If Not IsNothing(entidad.Estado) Then
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo) 'numeric = null,
                End If
                If Not IsNothing(entidad.UsuarioCrea) Then
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo) 'smallint = null,
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_tarifario_Compras")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()
                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                Else
                    For Each detalle In entidad.Tarifas
                        detalle.NumeroTarifario = entidad.Codigo
                        detalle.UsuarioCrea = entidad.UsuarioCrea
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        If Not InsertarDetalle(detalle, conexion) Then
                            inserto = False
                            Exit For
                        End If '
                    Next

                End If
            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As TarifarioCompras) As Long
            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                If entidad.OpcionCargue = 0 Then
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Codigo) 'NUMERIC = 0,
                    conexion.AgregarParametroSQL("@par_Tarifario_Base", entidad.TarifarioBase) 'VARCHAR ,
                    If entidad.FechaInicio > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Inicio", entidad.FechaInicio, SqlDbType.DateTime) '	datetime = null,
                    End If
                    If entidad.FechaFin > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Fin", entidad.FechaFin, SqlDbType.DateTime) '	datetime = null,
                    End If
                    If Not IsNothing(entidad.Estado) Then
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo) 'numeric = null,
                    End If
                    If Not IsNothing(entidad.UsuarioCrea) Then
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo) 'smallint = null,
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_tarifario_Compras")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()
                End If
                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                Else
                    For Each detalle In entidad.Tarifas
                        detalle.NumeroTarifario = entidad.Codigo
                        detalle.UsuarioCrea = entidad.UsuarioCrea
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        If Not InsertarDetalle(detalle, conexion) Then
                            inserto = False
                            Exit For
                        End If '
                    Next

                End If
            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As TarifarioCompras) As TarifarioCompras
            Dim item As New TarifarioCompras
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo = 0 And IsNothing(filtro.Codigo) Then
                    Return item
                Else
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Codigo)
                    End If
                    If Len(Trim(filtro.Nombre)) > Cero Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_tarifario_compras]")

                    While resultado.Read
                        item = New TarifarioCompras(resultado)
                    End While
                    conexion.CloseConnection()
                    resultado.Close()
                    Dim Detalle As DetalleTarifarioCompras
                    Dim ListaDetalle As New List(Of DetalleTarifarioCompras)
                    conexion.CleanParameters()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ETCC_Numero", filtro.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_tarifario_compras]")

                    While resultado.Read
                        Detalle = New DetalleTarifarioCompras(resultado)
                        ListaDetalle.Add(Detalle)
                    End While
                    item.Tarifas = ListaDetalle
                End If

            End Using

            Return item
        End Function

        Public Function Anular(entidad As TarifarioCompras) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_usuario_anula", entidad.UsuarioAnula)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_Anular_Tarifario_Compras_Precintos]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function InsertarDetalle(entidad As DetalleTarifarioCompras, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ETCC_Numero", entidad.NumeroTarifario)
            contextoConexion.AgregarParametroSQL("@par_LNTC_Codigo", entidad.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TLNC_Codigo", entidad.TipoLineaNegocioTransportes.Codigo)
            contextoConexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TATC_Codigo", entidad.TipoTarifaTransportes.TarifaTransporte.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TTTC_Codigo", entidad.TipoTarifaTransportes.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Porcentaje_Afiliado", entidad.PorcentajeAfiliado, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Flete", entidad.ValorFlete, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Escolta", entidad.ValorEscolta, SqlDbType.Money)
            'contextoConexion.AgregarParametroSQL("@par_Valor_Kilo_Adicional", entidad.ValorKgAdicional, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Otros1", 0)
            contextoConexion.AgregarParametroSQL("@par_Valor_Otros2", 0)
            contextoConexion.AgregarParametroSQL("@par_Valor_Otros3", 0)
            contextoConexion.AgregarParametroSQL("@par_Valor_Otros4", 0)
            If entidad.FechaInicioVigencia > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Vigencia_Inicio", entidad.FechaInicioVigencia, SqlDbType.DateTime) '	datetime = null,
            End If
            If entidad.FechaFinVigencia > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Vigencia_Fin", entidad.FechaFinVigencia, SqlDbType.DateTime) '	datetime = null,
            End If
            contextoConexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_tarifario_compras]")

            While resultado.Read
                inserto = IIf(resultado.Item("Codigo") > Cero, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function

    End Class

End Namespace