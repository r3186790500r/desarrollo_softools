﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Repositorio.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class PersistenciaCerrarOrdenServicio
        Implements IPersistenciaBase(Of CerrarOrdenServicio)

        Public Function Consultar(filtro As CerrarOrdenServicio) As IEnumerable(Of CerrarOrdenServicio) Implements IPersistenciaBase(Of CerrarOrdenServicio).Consultar
            Return New RepositorioCerrarOrdenServicio().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As CerrarOrdenServicio) As Long Implements IPersistenciaBase(Of CerrarOrdenServicio).Insertar
            Return New RepositorioCerrarOrdenServicio().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As CerrarOrdenServicio) As Long Implements IPersistenciaBase(Of CerrarOrdenServicio).Modificar
            Return New RepositorioCerrarOrdenServicio().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As CerrarOrdenServicio) As CerrarOrdenServicio Implements IPersistenciaBase(Of CerrarOrdenServicio).Obtener
            Return New RepositorioCerrarOrdenServicio().Obtener(filtro)
        End Function

        Public Function CerrarOrdenServicio(entidad As CerrarOrdenServicio) As Boolean
            Return New RepositorioCerrarOrdenServicio().CerrarOrdenServicio(entidad)
        End Function
    End Class
End Namespace
