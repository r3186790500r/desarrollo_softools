﻿EncoExpresApp.controller("GestionarCuentasPorCobrarCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'EmpresasFactory', 'ValorCatalogosFactory',
    'TercerosFactory', 'CuentasPorCobrarFactory', 'ConceptoLiquidacionFactory', 'ConceptoGastosFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, EmpresasFactory, ValorCatalogosFactory,
        TercerosFactory, CuentasPorCobrarFactory, ConceptoLiquidacionFactory, ConceptoGastosFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'GESTIONAR CUENTAS POR COBRAR';
        $scope.Master = "#!ConsultarCuentasPorCobrar";
        $scope.MapaSitio = [{ Nombre: 'Tesorería' }, { Nombre: 'Documentos' }, { Nombre: 'Cuentas Por Cobrar' }, { Nombre: 'Gestionar' }];
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ===' + OPCION_MENU_TESORERIA.CUENTAS_POR_COBRAR); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.Buscando = false;
        $scope.Deshabilitar = false;
        $scope.MensajesError = [];
        $scope.Modelo = {
            Codigo: 0,
            Numero: 0,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas
        };

        $scope.ListadoDocumentoOrigen = [];

        $scope.ListadoEstados = [
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR },
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_BORRADOR);
        $scope.ListadoTerceros = [];
        $scope.ListadoConceptos = [];
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy === true) {
            $scope.FechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete
        //--Tercero
        $scope.AutocompleteTercero = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoTerceros = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTerceros);
                }
            }
            return $scope.ListadoTerceros;
        };
        //--Tercero
        //----------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //--Documento Origen
            $scope.ListadoDocumentoOrigen = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN },
                Estado: ESTADO_ACTIVO,
                CampoAuxiliar4: 'CXC',
                Sync: true
            }).Datos;
            $scope.ListadoDocumentoOrigen.push({ Nombre: '(NO APLICA)', Codigo: 2600 })
            $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentoOrigen).First('$.Codigo==2600')
            $scope.CargarConceptos($scope.Modelo.DocumentoOrigen)
            //--Obtener Parametros Url
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== '0') {
                $scope.Modelo.Codigo = $routeParams.Numero;
                PantallaBloqueo(Obtener);
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        //----Obtener
        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo
            };
            CuentasPorCobrarFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = angular.copy(response.data.Datos);
                        $scope.Modelo.Fecha = new Date(response.data.Datos.Fecha);
                        $scope.Modelo.FechaVenceDocumento = new Date(response.data.Datos.FechaVenceDocumento);
                        if (response.data.Datos.DocumentoOrigen != undefined && response.data.Datos.DocumentoOrigen != '' && response.data.Datos.DocumentoOrigen != null) {
                            $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentoOrigen).First('$.Codigo ==' + response.data.Datos.DocumentoOrigen.Codigo);
                        }
                        $scope.Modelo.Tercero = $scope.CargarTercero($scope.Modelo.Tercero.Codigo);
                        $scope.CargarConceptos($scope.Modelo.DocumentoOrigen);
                        if (response.data.Datos.CodigoConcepto > 0) {
                            $scope.Modelo.Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + response.data.Datos.CodigoConcepto);
                        }
                        if (response.data.Datos.Anulado == 1) {
                            $scope.ListadoEstados.push({ Nombre: 'ANULADO', Codigo: 2 });
                            $scope.Modelo.Estado = $scope.ListadoEstados[2];
                            $scope.Deshabilitar = true;
                        }
                        else {
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.Deshabilitar = true;
                            }
                            else {
                                $scope.Deshabilitar = false;
                            }
                        }
                        $scope.MaskValores();
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    }
                    else {
                        ShowError('No se logro consultar la cuenta por cobrar' + response.data.MensajeOperacion);
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); }; document.location.href = $scope.Master; }, 500);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); }; document.location.href = $scope.Master; }, 500);
                });
        }
        //----Obtener
        //----Guardar
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var modelo = $scope.Modelo;
            if (modelo.Fecha == undefined || modelo.Fecha == '' || modelo.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar fecha');
                continuar = false;
            }
            if (modelo.FechaVenceDocumento == undefined || modelo.FechaVenceDocumento == '' || modelo.FechaVenceDocumento == null) {
                $scope.MensajesError.push('Debe ingresar fecha vencimiento');
                continuar = false;
            }
            else if (ComparaFecha(modelo.Fecha, modelo.FechaVenceDocumento) == FECHA_ES_MAYOR) {
                $scope.MensajesError.push('La fecha debe ser mayor a la fecha de vencimiento');
            }
            if (modelo.Tercero == undefined || modelo.Tercero == '' || modelo.Tercero == null) {
                $scope.MensajesError.push('Debe ingresar tercero');
                continuar = false;
            }
            if (modelo.ValorTotal == undefined || modelo.ValorTotal == '' || modelo.ValorTotal == null) {
                $scope.MensajesError.push('Debe ingresar valor');
                continuar = false;
            }
            return continuar;
        }
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            var cuentaporcobrar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
                Numero: $scope.Modelo.Numero,
                Fecha: $scope.Modelo.Fecha,
                DiasPlazo: $scope.Modelo.DiasPlazo,
                FechaVenceDocumento: $scope.Modelo.FechaVenceDocumento,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                Oficina: $scope.Modelo.Oficina,
                DocumentoOrigen: $scope.Modelo.DocumentoOrigen,
                NumeroDocumentoOrigen: $scope.Modelo.NumeroDocumentoOrigen,
                Tercero: $scope.Modelo.Tercero,
                Observaciones: $scope.Modelo.Observaciones,
                ValorTotal: $scope.Modelo.ValorTotal,
                Saldo: $scope.Modelo.Saldo,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Estado: $scope.Modelo.Estado.Codigo,
                CreadaManual: CODIGO_UNO
            };
            if ($scope.Modelo.Concepto != undefined && $scope.Modelo.Concepto != '' && $scope.Modelo.Concepto != null) {
                if ($scope.Modelo.Concepto.Codigo > 0) {
                    cuentaporcobrar.CodigoConcepto = $scope.Modelo.Concepto.Codigo;
                }
            }
            CuentasPorCobrarFactory.Guardar(cuentaporcobrar).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó la cuenta por cobrar número ' + response.data.Datos);
                            }
                            else {
                                ShowSuccess('Se modificó la guía con el número ' + response.data.Datos);
                            }
                            location.href = $scope.Master + "/" + response.data.Datos;
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        //----Guardar
        //--Limpiar AutoCompletes
        $scope.limpiarControl = function (Control) {
            switch (Control) {
                case "Tercero":
                    $scope.Modelo.Tercero = "";
                    break;
            }
        };
        //--Limpiar Autocompletes
        //--Cargar Conceptos
        $scope.CargarConceptos = function (documento) {
            var filtros = {};
            $scope.ListadoConceptos = [];
            switch (documento.Codigo) {
                case CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION:
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Estado: ESTADO_ACTIVO,
                        AplicaConsultaMaster: CODIGO_UNO,
                        ConceptoSistema: CERO,
                        Sync: true
                    };
                    var response = ConceptoLiquidacionFactory.Consultar(filtros);
                    if (response.ProcesoExitoso) {
                        if (response.Datos.length > 0) {
                            $scope.ListadoConceptos.push({ Codigo: 0, Nombre: '(NO APLICA)' });
                            $scope.ListadoConceptos = $scope.ListadoConceptos.concat(response.Datos);
                            $scope.Modelo.Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==0');
                        }
                        else {
                            ShowError("No se pudieron consultar los conceptos de la liquidación");
                        }
                    }
                    else {
                        ShowError("No se pudieron consultar los conceptos de la liquidación");
                    }
                    break;
                case CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_MASIVO:
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Estado: ESTADO_ACTIVO,
                        AplicaConsultaMaster: CODIGO_UNO,
                        ConceptoSistema: CERO,
                        Sync: true
                    };
                    var response = ConceptoGastosFactory.Consultar(filtros);
                    if (response.ProcesoExitoso) {
                        if (response.Datos.length > 0) {
                            $scope.ListadoConceptos.push({ Codigo: 0, Nombre: '(NO APLICA)' });
                            $scope.ListadoConceptos = $scope.ListadoConceptos.concat(response.Datos);
                            $scope.Modelo.Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==0');
                        }
                        else {
                            ShowError("No se pudieron consultar los conceptos de la legalización");
                        }
                    }
                    else {
                        ShowError("No se pudieron consultar los conceptos de la legalización");
                    }
                    break;
                case 2600:
                    $scope.ListadoConceptos.push({ Codigo: 1, Nombre: 'DESCUENTO FALTANTE' });
                    $scope.ListadoConceptos.push({ Codigo: 2, Nombre: 'MAYOR VALOR CANCELADO' });
                    $scope.ListadoConceptos.push({ Codigo: 3, Nombre: 'PRESTAMO OTORGADO A TERCERO' });
                    $scope.ListadoConceptos.push({ Codigo: 4, Nombre: 'RECUPERACION GASTOS GERENCIA' });

                    $scope.Modelo.Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo==1')
                    break;
            }
        };
        //--Cargar Conceptos
        //---Actualizar Fecha Vence
        $scope.ActualizarFechaVence = function () {
            if ($scope.Modelo.Fecha != undefined && $scope.Modelo.Fecha != '' && $scope.Modelo.Fecha != null) {
                if ($scope.Modelo.DiasPlazo != undefined && $scope.Modelo.DiasPlazo != '' && $scope.Modelo.DiasPlazo != null) {
                    $scope.Modelo.FechaVenceDocumento = angular.copy($scope.Modelo.Fecha);
                    $scope.Modelo.FechaVenceDocumento.addDays($scope.Modelo.DiasPlazo);
                }
                else {
                    $scope.Modelo.FechaVenceDocumento = angular.copy($scope.Modelo.Fecha);
                    $scope.Modelo.DiasPlazo = 0;
                }
            }
        };
        //---Actualizar Fecha Vence
        //--Volver
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Numero !== undefined)
                document.location.href = $scope.Master + "/" + $scope.Modelo.Numero;
            else
                document.location.href = $scope.Master;
        };
        //--Mascaras
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        //----------------------------Funciones Generales---------------------------------//
    }]);
