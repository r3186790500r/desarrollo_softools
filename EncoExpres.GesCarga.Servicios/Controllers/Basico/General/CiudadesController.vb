﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="CiudadesController"/>
''' </summary>
<Authorize>
Public Class CiudadesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Ciudades) As Respuesta(Of IEnumerable(Of Ciudades))
        Return New LogicaCiudades(CapaPersistenciaCiudades).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarAlterno")>
    Public Function ConsultarAlterno(ByVal filtro As Ciudades) As Respuesta(Of IEnumerable(Of Ciudades))
        Return New LogicaCiudades(CapaPersistenciaCiudades).ConsultarAlterno(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Ciudades) As Respuesta(Of Ciudades)
        Return New LogicaCiudades(CapaPersistenciaCiudades).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Ciudades) As Respuesta(Of Long)
        Return New LogicaCiudades(CapaPersistenciaCiudades).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Ciudades) As Respuesta(Of Boolean)
        Return New LogicaCiudades(CapaPersistenciaCiudades).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarCodigoDane")>
    Public Function ConsultarCodigoDane(ByVal entidad As CodigoDaneCiudades) As Respuesta(Of IEnumerable(Of CodigoDaneCiudades))
        Return New LogicaCiudades(CapaPersistenciaCiudades).ConsultarCodigoDane(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarOficinas")>
    Public Function ConsultarOficinas(ByVal filtro As Ciudades) As Respuesta(Of IEnumerable(Of Oficinas))
        Return New LogicaCiudades(CapaPersistenciaCiudades).ConsultarOficinas(filtro)
    End Function

    <HttpPost>
    <ActionName("GuardarOficinas")>
    Public Function GuardarOficinas(ByVal entidad As Ciudades) As Respuesta(Of Long)
        Return New LogicaCiudades(CapaPersistenciaCiudades).GuardarOficinas(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarRegiones")>
    Public Function ConsultarRegiones(ByVal filtro As Ciudades) As Respuesta(Of IEnumerable(Of RegionesPaises))
        Return New LogicaCiudades(CapaPersistenciaCiudades).ConsultarRegiones(filtro)
    End Function

    <HttpPost>
    <ActionName("GuardarRegiones")>
    Public Function GuardarRegiones(ByVal entidad As Ciudades) As Respuesta(Of Long)
        Return New LogicaCiudades(CapaPersistenciaCiudades).GuardarRegiones(entidad)
    End Function
End Class
