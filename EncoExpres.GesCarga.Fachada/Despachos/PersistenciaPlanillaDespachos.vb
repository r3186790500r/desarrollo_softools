﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo
Imports EncoExpres.GesCarga.Repositorio.Despachos.Masivo
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla

Namespace Despachos.Masivo
    Public NotInheritable Class PersistenciaPlanillaDespachos
        Implements IPersistenciaBase(Of PlanillaDespachos)

        Public Function Consultar(filtro As PlanillaDespachos) As IEnumerable(Of PlanillaDespachos) Implements IPersistenciaBase(Of PlanillaDespachos).Consultar
            Return New RepositorioPlanillaDespachos().Consultar(filtro)
        End Function
        Public Function ConsultarListaPlanillas(filtro As PlanillaDespachos) As IEnumerable(Of PlanillaDespachos)
            Return New RepositorioPlanillaDespachos().ConsultarListaPlanillas(filtro)
        End Function
        Public Function Insertar(entidad As PlanillaDespachos) As Long Implements IPersistenciaBase(Of PlanillaDespachos).Insertar
            Return New RepositorioPlanillaDespachos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PlanillaDespachos) As Long Implements IPersistenciaBase(Of PlanillaDespachos).Modificar
            Return New RepositorioPlanillaDespachos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PlanillaDespachos) As PlanillaDespachos Implements IPersistenciaBase(Of PlanillaDespachos).Obtener
            Return New RepositorioPlanillaDespachos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As PlanillaDespachos) As PlanillaDespachos
            Return New RepositorioPlanillaDespachos().Anular(entidad)
        End Function
        Public Function Obtener_Detalle_Tiempos(entidad As PlanillaDespachos) As IEnumerable(Of DetalleTiemposPlanillaDespachos)
            Return New RepositorioPlanillaDespachos().Obtener_Detalle_Tiempos(entidad)
        End Function
        Public Function Obtener_Detalle_Tiempos_Remesas(entidad As PlanillaDespachos) As IEnumerable(Of DetalleTiemposPlanillaDespachos)
            Return New RepositorioPlanillaDespachos().Obtener_Detalle_Tiempos_Remesas(entidad)
        End Function
        Public Function Insertar_Detalle_Tiempos(entidad As PlanillaDespachos) As Long
            Return New RepositorioPlanillaDespachos().Insertar_Detalle_Tiempos(entidad)
        End Function
        Public Function ActualizarEntregayDTRemesasporPlanilla(entidad As PlanillaDespachos) As Long
            Return New RepositorioPlanillaDespachos().ActualizarEntregayDTRemesasporPlanilla(entidad)
        End Function
    End Class

End Namespace

