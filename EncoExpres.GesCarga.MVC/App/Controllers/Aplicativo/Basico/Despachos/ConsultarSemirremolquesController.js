﻿EncoExpresApp.controller("ConsultarSemirremolquesCtrl", ['$scope', '$timeout', 'SemirremolquesFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', function ($scope, $timeout, SemirremolquesFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory) {

    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Semirremolques' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

    $scope.cantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.ModalErrorCompleto = ''
    $scope.ModalError = ''
    $scope.MostrarMensajeError = false
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SEMIRREMOLQUES);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }
    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        Propietario: {},
        Marca: {},
        Estado: 1
    }
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };
    /*Cargar el combo de estado*/
    $scope.ListadoEstados = [
        { Codigo: -1, Nombre: "(NO APLICA)" },
        { Codigo: 0, Nombre: "INACTIVO" },
        { Codigo: 1, Nombre: "ACTIVO" },
    ]
    $scope.Modelo.Estado = $scope.ListadoEstados[0]

    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarSemirremolques';
        }
    };

    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = 0
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoSemirremolques = [];
        if ($scope.Buscando) {
            if ($scope.MensajesError.length == 0) {
                $scope.Modelo.Pagina = $scope.paginaActual
                $scope.Modelo.RegistrosPagina = $scope.cantidadRegistrosPorPaginas
                blockUI.delay = 1000;
                SemirremolquesFactory.Consultar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoSemirremolques = response.data.Datos

                                $scope.ListadoSemirremolques.forEach(function (item) {
                                    $scope.codigo = item.Codigo;
                                });
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                                $scope.ListadoSemirremolques.Asc = true
                                OrderBy('Placa', undefined, $scope.ListadoSemirremolques);
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                            var listado = [];
                            response.data.Datos.forEach(function (item) {
                                listado.push(item);
                            });
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        blockUI.stop();
    }
    /*------------------------------------------------------------------------------------Eliminar Semirremolques-----------------------------------------------------------------------*/
    $scope.EliminarSemirremolques = function (codigo, Nombre) {
        $scope.AuxiCodigo = codigo
        $scope.AuxiNombre = Nombre
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        showModal('modalEliminarSemirremolques');
    };

    $scope.Eliminar = function () {
        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.AuxiCodigo,
        };

        SemirremolquesFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se elimino la ruta ' + $scope.AuxiNombre);
                    closeModal('modalEliminarSemirremolques');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                var result = ''
                result = InternalServerError(response.statusText)
                showModal('modalMensajeEliminarSemirremolques');
                $scope.ModalError = 'No se puede eliminar la ruta ' + $scope.AuxiNombre + ' ya que se encuentra relacionada con ' + result;
                $scope.ModalErrorCompleto = response.statusText

            });

    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /* Obtener parametros*/
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }

    $scope.CerrarModal = function () {
        closeModal('modalEliminarSemirremolques');
        closeModal('modalMensajeEliminarSemirremolques');
    }

    $scope.MaskPlaca = function () {
        try { $scope.Modelo.Placa = MascaraPlaca($scope.Modelo.Placa) } catch (e) { }
    };
    $scope.MaskNumero = function (option) {
        MascaraNumeroGeneral($scope)
    };
    $scope.MaskMayus = function (option) {
        MascaraMayusGeneral($scope)
    };

}]);