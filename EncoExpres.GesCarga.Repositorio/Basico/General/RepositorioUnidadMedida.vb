﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioUnidadMedida"/>
    ''' </summary>
    Public NotInheritable Class RepositorioUnidadMedida
        Inherits RepositorioBase(Of UnidadMedida)

        Public Overrides Function Consultar(filtro As UnidadMedida) As IEnumerable(Of UnidadMedida)
            Dim lista As New List(Of UnidadMedida)
            Dim item As UnidadMedida

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_Unidad_Medida]")

                While resultado.Read
                    item = New UnidadMedida(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As UnidadMedida) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As UnidadMedida) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As UnidadMedida) As UnidadMedida
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace