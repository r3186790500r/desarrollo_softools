﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.FlotaPropia
Imports EncoExpres.GesCarga.Negocio.Despachos

''' <summary>
''' Controlador <see cref="LegalizacionGastosController"/>
''' </summary>
<Authorize>
Public Class LegalizacionGastosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As LegalizacionGastos) As Respuesta(Of IEnumerable(Of LegalizacionGastos))
        Return New LogicaLegalizacionGastos(CapaPersistenciaLegalizacionGastos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As LegalizacionGastos) As Respuesta(Of LegalizacionGastos)
        Return New LogicaLegalizacionGastos(CapaPersistenciaLegalizacionGastos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As LegalizacionGastos) As Respuesta(Of Long)
        Return New LogicaLegalizacionGastos(CapaPersistenciaLegalizacionGastos).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As LegalizacionGastos) As Respuesta(Of Boolean)
        Return New LogicaLegalizacionGastos(CapaPersistenciaLegalizacionGastos).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("GuardarFacturaTerpel")>
    Public Function GuardarFacturaTerpel(ByVal entidad As DetalleLegalizacionGastos) As Respuesta(Of Boolean)
        Return New LogicaLegalizacionGastos(CapaPersistenciaLegalizacionGastos).GuardarFacturaTerpel(entidad)
    End Function

    <HttpPost>
    <ActionName("CalcularValores")>
    Public Function CalcularValores(ByVal entidad As LegalizacionGastos) As Respuesta(Of LegalizacionGastos)
        Return New LogicaLegalizacionGastos(CapaPersistenciaLegalizacionGastos).CalcularValores(entidad)
    End Function

End Class
