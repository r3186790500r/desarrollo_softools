﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" RendimientoGalonCombustible"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class RendimientoGalonCombustible
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="RendimientoGalonCombustible"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="RendimientoGalonCombustible"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "ID")
            TipoVehiculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIVE_Codigo"), .Nombre = Read(lector, "Tipo_Vehiculo")}
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Ruta")}
            PesoDesde = Read(lector, "Peso_Desde")
            PesoHasta = Read(lector, "Peso_Hasta")
            RendimientoGalon = Read(lector, "Rendimiento_Galon")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub
        <JsonProperty>
        Public Property TipoVehiculo As ValorCatalogos
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property PesoDesde As Double
        <JsonProperty>
        Public Property PesoHasta As Double
        <JsonProperty>
        Public Property RendimientoGalon As Double

    End Class
End Namespace
