﻿PRINT 'gsp_modificar_encabezado_planilla_despachos'
GO
DROP PROCEDURE gsp_modificar_encabezado_planilla_despachos
GO
CREATE PROCEDURE gsp_modificar_encabezado_planilla_despachos
(
@par_EMPR_Codigo smallint 
,@par_Numero numeric(18,0) 
,@par_Fecha datetime  = NULL
,@par_Fecha_Hora_Salida datetime  = NULL
,@par_RUTA_Codigo numeric(18,0) = NULL
,@par_VEHI_Codigo numeric(18,0) = NULL
,@par_SEMI_Codigo numeric(18,0) = NULL
,@par_TERC_Codigo_Tenedor numeric(18,0) = NULL
,@par_TERC_Codigo_Conductor numeric(18,0) = NULL
,@par_Cantidad numeric(18,2) = NULL
,@par_Peso numeric(18,2) = NULL
,@par_TATC_Codigo smallint = NULL
,@par_TTTC_Codigo smallint = NULL
,@par_Valor_Flete_Transportador money = NULL
,@par_Valor_Anticipo money = NULL
,@par_Valor_Retencion_Fuente money = NULL
,@par_Valor_Retencion_ICA money = NULL
,@par_Valor_Pagar_Transportador money = NULL
,@par_Valor_Flete_Cliente money = NULL
,@par_Valor_Seguro_Mercancia money = NULL
,@par_Valor_Otros_Cobros money = NULL
,@par_Valor_Total_Credito money = NULL
,@par_Valor_Total_Contado money = NULL
,@par_Valor_Total_Alcobro money = NULL
,@par_Observaciones varchar(200) = NULL
,@par_Estado smallint = NULL
,@par_USUA_Codigo_Crea smallint 
)
AS
BEGIN

UPDATE Encabezado_Remesas SET ENPD_Numero = 0, VEHI_Codigo = 0 where ENPD_Numero = @par_Numero
DELETE Detalle_Planilla_Despachos where ENPD_Numero = @par_Numero
DELETE Detalle_Auxiliares_Planilla_Despachos where ENPD_Numero = @par_Numero
UPDATE Encabezado_Planilla_Despachos
           set Fecha_Hora_Salida = ISNULL(@par_Fecha_Hora_Salida ,'')
           ,Fecha = ISNULL(@par_Fecha,'')
           ,RUTA_Codigo = ISNULL(@par_RUTA_Codigo ,0)
           ,VEHI_Codigo = ISNULL(@par_VEHI_Codigo ,0)
           ,SEMI_Codigo = ISNULL(@par_SEMI_Codigo ,0)
           ,TERC_Codigo_Tenedor =  ISNULL(@par_TERC_Codigo_Tenedor ,0)
           ,TERC_Codigo_Conductor = ISNULL(@par_TERC_Codigo_Conductor ,0)
           ,Cantidad = ISNULL(@par_Cantidad ,0)
           ,Peso = ISNULL(@par_Peso ,0)
           ,TATC_Codigo = @par_TATC_Codigo
           ,TTTC_Codigo = @par_TTTC_Codigo
           ,Valor_Flete_Transportador =  ISNULL(@par_Valor_Flete_Transportador ,0)
           ,Valor_Anticipo = ISNULL(@par_Valor_Anticipo ,0)
           ,Valor_Retencion_Fuente = ISNULL(@par_Valor_Retencion_Fuente ,0)
           ,Valor_Retencion_ICA = ISNULL(@par_Valor_Retencion_ICA ,0)
           ,Valor_Pagar_Transportador = ISNULL(@par_Valor_Pagar_Transportador ,0)
           ,Valor_Flete_Cliente = ISNULL(@par_Valor_Flete_Cliente ,0)
           ,Valor_Seguro_Mercancia =  ISNULL(@par_Valor_Seguro_Mercancia ,0)
           ,Valor_Otros_Cobros = ISNULL(@par_Valor_Otros_Cobros ,0)
           ,Valor_Total_Credito = ISNULL(@par_Valor_Total_Credito ,0)
           ,Valor_Total_Contado = ISNULL(@par_Valor_Total_Contado ,0)
           ,Valor_Total_Alcobro = ISNULL(@par_Valor_Total_Alcobro ,0)
           ,Observaciones = ISNULL(@par_Observaciones,'')
           ,Estado =   ISNULL(@par_Estado ,0)
           ,Fecha_Modifica = GETDATE() 
           ,USUA_Codigo_Modifica = @par_USUA_Codigo_Crea
   
select @par_Numero as Numero
END
GO