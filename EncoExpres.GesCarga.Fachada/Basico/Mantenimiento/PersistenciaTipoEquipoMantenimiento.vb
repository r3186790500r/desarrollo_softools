﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento

    Public NotInheritable Class PersistenciaTipoEquipoMantenimiento
        Implements IPersistenciaBase(Of TipoEquipoMantenimiento)
        Public Function Consultar(filtro As TipoEquipoMantenimiento) As IEnumerable(Of TipoEquipoMantenimiento) Implements IPersistenciaBase(Of TipoEquipoMantenimiento).Consultar
            Return New RepositorioTipoEquipoMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TipoEquipoMantenimiento) As Long Implements IPersistenciaBase(Of TipoEquipoMantenimiento).Insertar
            Return New RepositorioTipoEquipoMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TipoEquipoMantenimiento) As Long Implements IPersistenciaBase(Of TipoEquipoMantenimiento).Modificar
            Return New RepositorioTipoEquipoMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TipoEquipoMantenimiento) As TipoEquipoMantenimiento Implements IPersistenciaBase(Of TipoEquipoMantenimiento).Obtener
            Return New RepositorioTipoEquipoMantenimiento().Obtener(filtro)
        End Function

    End Class
End Namespace
