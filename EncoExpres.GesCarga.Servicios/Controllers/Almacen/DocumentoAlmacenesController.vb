﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Almacen
Imports EncoExpres.GesCarga.Negocio.Almacen

<Authorize>
Public Class DocumentoAlmacenesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DocumentoAlmacenes) As Respuesta(Of IEnumerable(Of DocumentoAlmacenes))
        Return New LogicaDocumentoAlmacenes(CapaPersistenciaDocumentoAlmacenes, CapaPersistenciaDetalleDocumentoAlmacenes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DocumentoAlmacenes) As Respuesta(Of DocumentoAlmacenes)
        Return New LogicaDocumentoAlmacenes(CapaPersistenciaDocumentoAlmacenes, CapaPersistenciaDetalleDocumentoAlmacenes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DocumentoAlmacenes) As Respuesta(Of Long)
        Return New LogicaDocumentoAlmacenes(CapaPersistenciaDocumentoAlmacenes, CapaPersistenciaDetalleDocumentoAlmacenes).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DocumentoAlmacenes) As Respuesta(Of Boolean)
        Return New LogicaDocumentoAlmacenes(CapaPersistenciaDocumentoAlmacenes, CapaPersistenciaDetalleDocumentoAlmacenes).Anular(entidad)
    End Function
End Class
