﻿Print 'gsp_insertar_encabezado_solicitud_orden_servicios'
GO
DROP PROCEDURE gsp_insertar_encabezado_solicitud_orden_servicios
GO
CREATE PROCEDURE gsp_insertar_encabezado_solicitud_orden_servicios        
(        
@par_EMPR_Codigo SMALLINT,        
@par_TIDO_Codigo NUMERIC,        
@par_Fecha DATE,        
@par_ETCV_Numero NUMERIC,        
@par_LNTC_Codigo SMALLINT,       
@par_TLNC_Codigo SMALLINT,   
@par_TERC_Codigo_Cliente NUMERIC,        
@par_TERC_Codigo_Facturar NUMERIC,        
@par_Documento_Cliente VARCHAR(20) = NULL,        
@par_TERC_Codigo_Comercial NUMERIC,        
@par_Facturar_Despachar SMALLINT,        
@par_Facturar_Cantidad_Peso_Cumplido SMALLINT,        
@par_TERC_Codigo_Destinatario NUMERIC,        
@par_Fecha_Cargue DATETIME = NULL,        
@par_CIUD_Codigo_Cargue NUMERIC,        
@par_Direccion_Cargue VARCHAR(100) = NULL,        
@par_Telefono_Cargue VARCHAR(50) = NULL,        
@par_Contacto_Cargue VARCHAR(50) = NULL,        
@par_OFIC_Codigo_Depacha SMALLINT,        
@par_Observaciones VARCHAR(250) = NULL,        
@par_Aplica_Poliza SMALLINT,        
@par_Estado SMALLINT,        
@par_USUA_Codigo_Crea SMALLINT        
)        
AS        
BEGIN        
DECLARE @par_Numero_Documento NUMERIC      
EXEC gsp_generar_consecutivo  @par_EMPR_Codigo, @par_TIDO_Codigo, 0, @par_Numero_Documento OUTPUT          
        
INSERT INTO  Encabezado_Solicitud_Orden_Servicios (        
EMPR_Codigo,        
TIDO_Codigo,        
Numero_Documento,        
Fecha,        
ETCV_Numero,        
LNTC_Codigo,    
TLNC_Codigo,         
TERC_Codigo_Cliente,        
TERC_Codigo_Facturar,        
Documento_Cliente,        
TERC_Codigo_Comercial,        
Facturar_Despachar,        
Facturar_Cantidad_Peso_Cumplido,        
TERC_Codigo_Destinatario,        
Fecha_Cargue,        
CIUD_Codigo_Cargue,        
Direccion_Cargue,        
Telefono_Cargue,        
Contacto_Cargue,        
OFIC_Codigo_Despacha,        
Observaciones,        
Aplica_Poliza,        
Estado,        
Anulado,        
USUA_Codigo_Crea,        
Fecha_Crea,
CATA_ESSO_Codigo        
)        
VALUES (        
@par_EMPR_Codigo,        
@par_TIDO_Codigo,        
@par_Numero_Documento,        
@par_Fecha,        
@par_ETCV_Numero,        
@par_LNTC_Codigo,  
@par_TLNC_Codigo,           
@par_TERC_Codigo_Cliente,        
@par_TERC_Codigo_Facturar,        
ISNULL(@par_Documento_Cliente,''),        
@par_TERC_Codigo_Comercial,        
@par_Facturar_Despachar,        
@par_Facturar_Cantidad_Peso_Cumplido,        
@par_TERC_Codigo_Destinatario,        
@par_Fecha_Cargue,        
@par_CIUD_Codigo_Cargue,        
ISNULL(@par_Direccion_Cargue,''),        
ISNULL(@par_Telefono_Cargue,''),        
ISNULL(@par_Contacto_Cargue,''),        
@par_OFIC_Codigo_Depacha,        
ISNULL(@par_Observaciones,''),        
@par_Aplica_Poliza,        
@par_Estado,        
0,        
@par_USUA_Codigo_Crea,        
GETDATE(),
9201 --> Solicitud Estado Pendiente
)        
        
SELECT @par_Numero_Documento AS Numero_Documento, @@IDENTITY AS Numero        
        
END        
GO