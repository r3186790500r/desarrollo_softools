﻿Imports System.Web.Http
Imports Microsoft.Owin
Imports Microsoft.Owin.Security.OAuth
Imports Owin

<Assembly: OwinStartup(GetType(EncoExpres.GesCarga.Servicios.Startup))>
Namespace EncoExpres.GesCarga.Servicios

    ''' <summary>
    ''' Clase de inicialización de Owin
    ''' </summary>
    Public Class Startup
        Public Sub Configuration(app As IAppBuilder)
            Dim config = New HttpConfiguration()
            Register(config)
            ConfigureOAuth(app)
            app.UseWebApi(config)
        End Sub

        Public Sub ConfigureOAuth(app As IAppBuilder)

            Dim oAuthServerOptions = New OAuthAuthorizationServerOptions() With {
                .AllowInsecureHttp = True,
                .TokenEndpointPath = New PathString("/encoExpresToken"),
                .AccessTokenExpireTimeSpan = TimeSpan.FromSeconds(28800),
                .Provider = New ProveedorTokenAutorizacion()
            }
            app.UseOAuthAuthorizationServer(oAuthServerOptions)

            Dim authOptions = New OAuthBearerAuthenticationOptions() With {
                .AuthenticationMode = Security.AuthenticationMode.Active
            }
            app.UseOAuthBearerAuthentication(authOptions)

        End Sub

    End Class


End Namespace
