﻿Print 'Catalogo 35 tipo Equipo'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 35
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 35
GO
DELETE Catalogos WHERE Codigo = 35
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,35,'TIEQ','Tipo Equipo',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,35,3500,UPPER('(NO APLICA)'),1,GETDATE()),
(1,35,3501,UPPER('AUTOMOVIL'),1,GETDATE()),
(1,35,3502,UPPER('CAMION'),1,GETDATE()),
(1,35,3503,UPPER('Camiones Grua'),1,GETDATE()),
(1,35,3504,UPPER('Carromacho'),1,GETDATE()),
(1,35,3505,UPPER('Camioneta'),1,GETDATE()),
(1,35,3506,UPPER('Cargador'),1,GETDATE()),
(1,35,3507,UPPER('CAMION'),1,GETDATE()),
(1,35,3508,UPPER('FRAC TANK'),1,GETDATE()),
(1,35,3509,UPPER('Grua'),1,GETDATE()),
(1,35,3510,UPPER('Manilift'),1,GETDATE()),
(1,35,3511,UPPER('Modular'),1,GETDATE()),
(1,35,3512,UPPER('Montacarga'),1,GETDATE()),
(1,35,3513,UPPER('TRACTOCAMION'),1,GETDATE()),
(1,35,3514,UPPER('Trailer Cama Alta'),1,GETDATE()),
(1,35,3515,UPPER('Trailer Cama Baja'),1,GETDATE()),
(1,35,3516,UPPER('Tractocamión'),1,GETDATE())

GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,35,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,35,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 