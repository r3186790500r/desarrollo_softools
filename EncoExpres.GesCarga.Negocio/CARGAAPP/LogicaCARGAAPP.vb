﻿
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Fachada

Public NotInheritable Class LogicaCARGAAPP
    Inherits LogicaBase(Of TercerosSincronizacionCARGAAPP)

    ReadOnly _persistencia As IPersistenciaBase(Of TercerosSincronizacionCARGAAPP)

    Sub New(persistencia As IPersistenciaBase(Of TercerosSincronizacionCARGAAPP))
        _persistencia = persistencia
    End Sub


    Public Overrides Function Consultar(filtro As TercerosSincronizacionCARGAAPP) As Respuesta(Of IEnumerable(Of TercerosSincronizacionCARGAAPP))
        Throw New NotImplementedException()
    End Function

    Public Function ConsultarTerceros(filtro As TercerosSincronizacionCARGAAPP) As TercerosSincronizacionCARGAAPP
        Dim consulta = _persistencia.Obtener(filtro)


        Return consulta
    End Function

    Public Function ModificarTerceros(entidad As TercerosModificacionCARGAAPP)
        Dim consulta = CType(_persistencia, PersistenciaTercerosSincronizacionCARGAAPP).ModificarTerceros(entidad)
    End Function

    Public Function ModificarVehiculos(entidad As VehiculosModificacionCARGAAPP)
        Dim consulta = CType(_persistencia, PersistenciaTercerosSincronizacionCARGAAPP).ModificarVehiculos(entidad)
    End Function
    Public Overrides Function Obtener(filtro As TercerosSincronizacionCARGAAPP) As Respuesta(Of TercerosSincronizacionCARGAAPP)
        Throw New NotImplementedException()
    End Function

    Public Overrides Function Guardar(entidad As TercerosSincronizacionCARGAAPP) As Respuesta(Of Long)
        Throw New NotImplementedException()
    End Function
End Class
