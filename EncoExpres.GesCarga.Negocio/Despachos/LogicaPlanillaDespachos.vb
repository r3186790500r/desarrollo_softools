﻿
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo
Imports EncoExpres.GesCarga.Fachada.Despachos.Masivo
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla

Namespace Despachos.Masivo
    Public NotInheritable Class LogicaPlanillaDespachos
        Inherits LogicaBase(Of PlanillaDespachos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of PlanillaDespachos)

        Sub New(persistencia As IPersistenciaBase(Of PlanillaDespachos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As PlanillaDespachos) As Respuesta(Of IEnumerable(Of PlanillaDespachos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanillaDespachos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanillaDespachos))(consulta)
        End Function


        Public Function ConsultarListaPlanillas(filtro As PlanillaDespachos) As Respuesta(Of IEnumerable(Of PlanillaDespachos))
            Dim consulta = CType(_persistencia, PersistenciaPlanillaDespachos).ConsultarListaPlanillas(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanillaDespachos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanillaDespachos))(consulta)
        End Function
        Public Function Obtener_Detalle_Tiempos(entidad As PlanillaDespachos) As Respuesta(Of IEnumerable(Of DetalleTiemposPlanillaDespachos))

            Return New Respuesta(Of IEnumerable(Of DetalleTiemposPlanillaDespachos))(CType(_persistencia, PersistenciaPlanillaDespachos).Obtener_Detalle_Tiempos(entidad))

        End Function
        Public Function Obtener_Detalle_Tiempos_Remesas(entidad As PlanillaDespachos) As Respuesta(Of IEnumerable(Of DetalleTiemposPlanillaDespachos))

            Return New Respuesta(Of IEnumerable(Of DetalleTiemposPlanillaDespachos))(CType(_persistencia, PersistenciaPlanillaDespachos).Obtener_Detalle_Tiempos_Remesas(entidad))

        End Function

        Public Function ActualizarEntregayDTRemesasporPlanilla(entidad As PlanillaDespachos) As Respuesta(Of Long)

            Return New Respuesta(Of Long)(CType(_persistencia, PersistenciaPlanillaDespachos).ActualizarEntregayDTRemesasporPlanilla(entidad))

        End Function

        Public Function Insertar_Detalle_Tiempos(entidad As PlanillaDespachos) As Respuesta(Of Long)

            Return New Respuesta(Of Long)(CType(_persistencia, PersistenciaPlanillaDespachos).Insertar_Detalle_Tiempos(entidad))

        End Function
        Public Overrides Function Guardar(entidad As PlanillaDespachos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.NumeroDocumento, .MensajeOperacion = IIf(entidad.NumeroDocumento > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As PlanillaDespachos) As Respuesta(Of PlanillaDespachos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PlanillaDespachos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of PlanillaDespachos)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As PlanillaDespachos) As Respuesta(Of PlanillaDespachos)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of PlanillaDespachos) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of PlanillaDespachos) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As PlanillaDespachos) As Respuesta(Of PlanillaDespachos)
            Return New Respuesta(Of PlanillaDespachos)(CType(_persistencia, PersistenciaPlanillaDespachos).Anular(entidad))
        End Function
    End Class

End Namespace