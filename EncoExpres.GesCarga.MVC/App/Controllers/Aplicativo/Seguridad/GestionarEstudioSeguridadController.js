﻿EncoExpresApp.controller("GestionarEstudioSeguridadCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'MarcaSemirremolquesFactory', 'EstudioSeguridadDocumentosFactory', 'TipoDocumentoEstudioSeguridadFactory', 'EstudioSeguridadFactory', 'RutasFactory', 'ColorVehiculosFactory', 'MarcaVehiculosFactory', 'CiudadesFactory', 'TercerosFactory', 'ValorCatalogosFactory', 'blockUIConfig',
    function ($scope, $routeParams, $timeout, $linq, blockUI, MarcaSemirremolquesFactory, EstudioSeguridadDocumentosFactory, TipoDocumentoEstudioSeguridadFactory, EstudioSeguridadFactory, RutasFactory, ColorVehiculosFactory, MarcaVehiculosFactory, CiudadesFactory, TercerosFactory, ValorCatalogosFactory, blockUIConfig) {
        console.clear()
        $scope.Titulo = 'GESTIONAR ESTUDIO SEGURIDAD';
        $scope.MapaSitio = [{ Nombre: 'Seguridad' }, { Nombre: 'Estudio Seguridad' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ESTUDIO_SEGURIDAD); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ModeloFecha = new Date();
        $scope.Deshabilitar = true;
        $scope.DeshabilitarDefinitivo = false;
        $scope.DeshabilitarBotonGuardar = true;
        $scope.AplicaMostrarAutorizaciones = false;
        $scope.DeshabilitarAplicaTenedor = false;
        $scope.DeshabilitarAplicaConductor = false;
        $scope.DeshabilitarAplicaPropietarioSemirremolque = false;
        $scope.CheckTenedor = false;
        $scope.CheckConductor = false;
        $scope.CheckPropietarioSemirremolque = false;
        $scope.Aprobacion = 0;
        $scope.Numero = 0;
        $scope.ModeloNumeroDocumento = '';
        $scope.TipoImagen = '';
        $scope.DetalleDocumentos = [];
        $scope.ListadoMarcasSemirremolques = [];
        $scope.ListadoTipoSemirremolques = [];
        $scope.ListadoTipoCarroceria = [];

        //---------------------------------------------------Validar existencia de estudio de seguridad------------------------------------------------------------------------
        $scope.ValidarExistenciaEstudio = function () {
            if ($scope.ModeloPlacaVehiculo !== '' && $scope.ModeloPlacaVehiculo !== undefined && $scope.ModeloPlacaVehiculo !== null) {
                var Filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Placa: $scope.ModeloPlacaVehiculo,
                    EstadoAutorizacion: -1,
                    Estado: -1
                };
                EstudioSeguridadFactory.Consultar(Filtro).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                ShowError('Ya existe un estudio de seguridad para la  placa ' + $scope.ModeloPlacaVehiculo);
                                $scope.ModeloPlacaVehiculo = '';
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
        };

        /* Funcion validar que fecha vencimiento tecno-mecanica sea mayor a fecha actual*/
        $scope.FechaVencimientoSoat = function (fecha) {
            var fechaActual = new Date();
            if (fecha < fechaActual) {
                ShowError('La fecha de vencimiento del SOAT debe ser mayor a la fecha actual');
                $scope.ModeloFechaVencimientoSoat = '';
                continuar = false;
            }
        }

        /* Funcion validar que fecha vencimiento tecno-mecanica sea mayor a fecha actual*/
        $scope.FechaVencimientoTecno = function (fecha) {
            var fechaActual = new Date();
            if (fecha < fechaActual) {
                ShowError('La fecha de vencimiento de la tecno-mecánica debe ser mayor a la fecha actual');
                $scope.ModeloFechaVencimientoTecno = '';
                continuar = false;
            }
        }

        /* Limpiar temporal*/
        EstudioSeguridadDocumentosFactory.LimpiarDocumentoTemporalUsuario({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 5;
        $scope.totalPaginas = 0;
        $scope.DeshabilitarEstado = false
        $scope.ModeloPropietario = [];
        $scope.ModeloPropietarioSemirremolque = [];
        $scope.ModeloTenedor = [];
        $scope.ModeloConductor = [];
        $scope.EntidadesConsultadas = [];
        $scope.ListadoAutorizacion = [];
        $scope.ListadoTipoVehiculo = [];
        $scope.ListadoNaturalezas = [];
        $scope.ListadoTipoIdentificacion = [];
        $scope.ListadoMarcas = [];
        $scope.ListadoClaseRiesgo = [];
        $scope.ListadoColores = [];
        $scope.listaEntidades = [];
        $scope.ListaReferencias = [];
        $scope.ListaCiudadExpedicion = [];
        $scope.ListaCiudadResidencia = [];
        $scope.ListaTipoReferencia = [];
        $scope.ListaRutas = [];
        $scope.ListadoTiposDocumentos = [];

        /*--------COMBOS Y AUTOCOMPLETE------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        TipoDocumentoEstudioSeguridadFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTiposDocumentos = response.data.Datos
                        $scope.ListadoTiposDocumentos.forEach(function (item) {
                            item.CodigoDocumentos = 0;
                        })
                    }
                }
            }, function (response) {
            });

        /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ListadoEstados = [];
        $scope.ListadoEstados = [
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.ModeloEstadoDocumento = $scope.ListadoEstados[0];

        /*Cargar Autocomplete de marca*/
        MarcaSemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoMarcasSemirremolques = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoMarcasSemirremolques = response.data.Datos;
                        if ($scope.CodigoMarcaSemirremolque !== undefined && $scope.CodigoMarcaSemirremolque !== '' && $scope.CodigoMarcaSemirremolque !== null && $scope.CodigoMarcaSemirremolque !== 0) {
                            $scope.ModeloMarcaSemirremolque = $linq.Enumerable().From($scope.ListadoMarcasSemirremolques).First('$.Codigo ==' + $scope.CodigoMarcaSemirremolque);
                        } else {
                            $scope.ModeloMarcaSemirremolque = '';
                        }
                    }
                    else {
                        $scope.ListadoMarcasSemirremolques = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de tipo semirremolque*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SEMIRREMOLQUES } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoSemirremolques = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoSemirremolques = response.data.Datos;
                        if ($scope.CodigoTipoSemirremolque !== undefined && $scope.CodigoTipoSemirremolque !== '' && $scope.CodigoTipoSemirremolque !== null) {
                            $scope.ModeloTipoSemirremolque = $linq.Enumerable().From($scope.ListadoTipoSemirremolques).First('$.Codigo ==' + $scope.CodigoTipoSemirremolque);
                        } else {
                            $scope.ModeloTipoSemirremolque = $scope.ListadoTipoSemirremolques[0];
                        }
                    }
                    else {
                        $scope.ListadoTipoSemirremolques = [];
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de tipo carroceria*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CARROCERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoCarroceria = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoCarroceria = response.data.Datos;
                        if ($scope.CodigoTipoCarroceriaSemirremolque !== undefined && $scope.CodigoTipoCarroceriaSemirremolque !== '' && $scope.CodigoTipoCarroceriaSemirremolque !== null) {
                            $scope.ModeloTipoCarroceriaSemirremolque = $linq.Enumerable().From($scope.ListadoTipoCarroceria).First('$.Codigo ==' + $scope.CodigoTipoCarroceriaSemirremolque);
                        } else {
                            $scope.ModeloTipoCarroceriaSemirremolque = $scope.ListadoTipoCarroceria[0];
                        }
                    }
                    else {
                        $scope.ListadoTipoCarroceria = [];
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de estado solicitud*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_SOLICITUD } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoAutorizacion = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoAutorizacion = response.data.Datos
                        if ($scope.CodigoEstadoAutorizacion !== undefined && $scope.CodigoEstadoAutorizacion !== '' && $scope.CodigoEstadoAutorizacion !== null) {
                            $scope.ModeloEstadoAutorizacion = $linq.Enumerable().From($scope.ListadoAutorizacion).First('$.Codigo ==' + $scope.CodigoEstadoAutorizacion);
                        } else {
                            $scope.ModeloEstadoAutorizacion = $scope.ListadoAutorizacion[0];
                        }
                    }
                    else {
                        $scope.ListadoAutorizacion = []
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de clase de riesgo*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CLASE_RIESGO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoClaseRiesgo = []
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: 'Seleccione clase de riesgo', Codigo: 0 })
                        $scope.ListadoClaseRiesgo = response.data.Datos
                        if ($scope.CodigoClaseRiesgo !== undefined && $scope.CodigoClaseRiesgo !== '' && $scope.CodigoClaseRiesgo !== null) {
                            $scope.ModeloClaseRiesgo = $linq.Enumerable().From($scope.ListadoClaseRiesgo).First('$.Codigo ==' + $scope.CodigoClaseRiesgo);
                        } else {
                            $scope.ModeloClaseRiesgo = $linq.Enumerable().From($scope.ListadoClaseRiesgo).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListadoClaseRiesgo = []
                    }
                }
            }, function (response) {
            });

        /*Cargar Autocomplete de marca*/
        MarcaVehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoMarcas = []
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListadoMarcas = response.data.Datos;
                        if ($scope.CodigoMarca !== undefined && $scope.CodigoMarca !== '' && $scope.CodigoMarca !== null) {
                            $scope.ModeloMarca = $linq.Enumerable().From($scope.ListadoMarcas).First('$.Codigo ==' + $scope.CodigoMarca);
                        } else {
                            $scope.ModeloMarca = $linq.Enumerable().From($scope.ListadoMarcas).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListadoMarcas = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de tipo vehiculo*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoVehiculo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoVehiculo = response.data.Datos;
                        if ($scope.CodigoTipoVehiculo !== undefined && $scope.CodigoTipoVehiculo !== '' && $scope.CodigoTipoVehiculo !== null) {
                            $scope.ModeloTipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo ==' + $scope.CodigoTipoVehiculo);
                        } else {
                            $scope.ModeloTipoVehiculo = $scope.ListadoTipoVehiculo[0]
                        }
                    }
                    else {
                        $scope.ListadoTipoVehiculo = []
                    }
                }
            }, function (response) {
            });

        /*Cargar Autocomplete de colores*/
        $scope.ListadoColores = [];
        $scope.AutocompleteColores = function (value) {
            if (value.length > 0) {
                if (value.length % 3 === 0 || value.length === 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = ColorVehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoColores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoColores);
                }
            }
            return $scope.ListadoColores;
        };

        /*Cargar el combo de aseguradora*/
        $scope.ListaAseguradora = [];
        $scope.AutocompleteAseguradora = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_ASEGURADORA,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaAseguradora = ValidarListadoAutocomplete(Response.Datos, $scope.ListaAseguradora)
                }
            }
            return $scope.ListaAseguradora
        }
        /*Cargar el combo de tipo naturalezas*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_NATURALEZA_TERCERO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoNaturalezas = [];
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.forEach(function (item) {
                            if (item.Codigo !== 500) {
                                $scope.ListadoNaturalezas.push(item);
                            }
                        });
                        if ($scope.CodigoTipoNaturalezaPropietario !== undefined && $scope.CodigoTipoNaturalezaPropietario !== '' && $scope.CodigoTipoNaturalezaPropietario !== null) {
                            $scope.ModeloTipoNaturalezaPropietario = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.CodigoTipoNaturalezaPropietario);
                        } else {
                            $scope.ModeloTipoNaturalezaPropietario = $scope.ListadoNaturalezas[0];
                        }
                        if ($scope.CodigoTipoNaturalezasSemirremolque !== undefined && $scope.CodigoTipoNaturalezasSemirremolque !== '' && $scope.CodigoTipoNaturalezasSemirremolque !== null) {
                            $scope.ModeloTipoNaturalezaSemirremolque = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.CodigoTipoNaturalezasSemirremolque);
                        } else {
                            $scope.ModeloTipoNaturalezaSemirremolque = $scope.ListadoNaturalezas[0];
                        }
                        if ($scope.CodigoTipoNaturalezasTenedor !== undefined && $scope.CodigoTipoNaturalezasTenedor !== '' && $scope.CodigoTipoNaturalezasTenedor !== null) {
                            $scope.ModeloTipoNaturalezaTenedor = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.CodigoTipoNaturalezasTenedor);
                        } else {
                            $scope.ModeloTipoNaturalezaTenedor = $scope.ListadoNaturalezas[0];
                        }
                        if ($scope.CodigoTipoNaturalezasConductor !== undefined && $scope.CodigoTipoNaturalezasConductor !== '' && $scope.CodigoTipoNaturalezasConductor !== null) {
                            $scope.ModeloTipoNaturalezaConductor = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.CodigoTipoNaturalezasConductor);
                        } else {
                            $scope.ModeloTipoNaturalezaConductor = $scope.ListadoNaturalezas[0];
                        }
                    }
                    else {
                        $scope.ListadoNaturalezas = [];
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de tipo identificaciones*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoIdentificacion = [];
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.forEach(function (item) {
                            if (item.Codigo !== 100) {
                                $scope.ListadoTipoIdentificacion.push(item);
                            }
                        });
                        if ($scope.CodigoTipoIdentificacionPropietario !== undefined && $scope.CodigoTipoIdentificacionPropietario !== '' && $scope.CodigoTipoIdentificacionPropietario !== null) {
                            $scope.ModeloTipoIdentificacionPropietario = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionPropietario);
                        } else {
                            $scope.ModeloTipoIdentificacionPropietario = $scope.ListadoTipoIdentificacion[0]
                        }
                        if ($scope.CodigoTipoIdentificacionSemirremolque !== undefined && $scope.CodigoTipoIdentificacionSemirremolque !== '' && $scope.CodigoTipoIdentificacionSemirremolque !== null) {
                            $scope.ModeloTipoIdentificacionSemirremolque = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionSemirremolque);
                        } else {
                            $scope.ModeloTipoIdentificacionSemirremolque = $scope.ListadoTipoIdentificacion[0]
                        }
                        if ($scope.CodigoTipoIdentificacionTenedor !== undefined && $scope.CodigoTipoIdentificacionTenedor !== '' && $scope.CodigoTipoIdentificacionTenedor !== null) {
                            $scope.ModeloTipoIdentificacionTenedor = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionTenedor);
                        } else {
                            $scope.ModeloTipoIdentificacionTenedor = $scope.ListadoTipoIdentificacion[0]
                        }
                        if ($scope.CodigoTipoIdentificacionConductor !== undefined && $scope.CodigoTipoIdentificacionConductor !== '' && $scope.CodigoTipoIdentificacionConductor !== null) {
                            $scope.ModeloTipoIdentificacionConductor = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionConductor);
                        } else {
                            $scope.ModeloTipoIdentificacionConductor = $scope.ListadoTipoIdentificacion[0]
                        }
                    } else {
                        $scope.ListadoTipoIdentificacion = []
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de ciudades*/
        $scope.ListaCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListaCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCiudades)
                }
            }
            return $scope.ListaCiudades
        }

        /*Cargar el combo de ruta*/
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListaRutas = response.data.Datos;
                        if ($scope.CodigoRuta !== undefined && $scope.CodigoRuta !== '' && $scope.CodigoRuta !== null) {
                            $scope.ModeloRuta = $linq.Enumerable().From($scope.ListaRutas).First('$.Codigo ==' + $scope.CodigoRuta);
                        } else {
                            $scope.ModeloRuta = '';
                        }
                    } else {
                        $scope.ListaRutas = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo lista tipo referencia*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_REFERENCIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaTipoReferencia = [];
                    $scope.ListaTipoReferencia = response.data.Datos;
                    for (var i = 0; i < $scope.ListaTipoReferencia.length; i++) {
                        $scope.ListaTipoReferencia[i].Nombre = $scope.ListaTipoReferencia[i].Nombre.toUpperCase()
                    }
                    $scope.ListaTipoReferencia.push({ Nombre: 'Seleccione un tipo de referencia', Codigo: 0 })
                    $scope.Referencia = $scope.ListaTipoReferencia[$scope.ListaTipoReferencia.length - 1]
                }
            }, function (response) {
                $scope.Buscando = false;
            });

        //------------------------------------------------------------------------------Funciones aplica Tenedor Conductor y Propietario Semirremolque-------------------------------------------------------------
        $scope.AplicaTenedor = function (check) {
            if (check) {
                $scope.ModeloTipoNaturalezaTenedor = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.ModeloTipoNaturalezaPropietario.Codigo);
                $scope.ModeloTipoIdentificacionTenedor = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.ModeloTipoIdentificacionPropietario.Codigo);
                $scope.ModeloNumeroIdentificacionTenedor = $scope.ModeloNumeroIdentificacionPropietario;
                $scope.ModeloDigitoChequeoTenedor = $scope.ModeloDigitoChequeoPropietario;
                $scope.ModeloCiudadExpedicionTenedor = $scope.ModeloCiudadExpedicionPropietario;
                $scope.ModeloTenedor = $scope.ModeloPropietario;
                $scope.ModeloResidenciaTenedor = $scope.ModeloResidenciaPropietario;
                $scope.ModeloDireccionTenedor = $scope.ModeloDireccionPropietario;
                $scope.ModeloTelefonoTenedor = $scope.ModeloTelefonoPropietario;
                $scope.ModeloCelularTenedor = $scope.ModeloCelularPropietario;
                $scope.DeshabilitarAplicaTenedor = true;
            } else {
                $scope.DeshabilitarAplicaTenedor = false;
            }
        };

        $scope.AplicaConductor = function (check) {
            if (check) {
                $scope.ModeloTipoNaturalezaConductor = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.ModeloTipoNaturalezaPropietario.Codigo);
                $scope.ModeloTipoIdentificacionConductor = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.ModeloTipoIdentificacionPropietario.Codigo);
                $scope.ModeloNumeroIdentificacionConductor = $scope.ModeloNumeroIdentificacionPropietario;
                $scope.ModeloDigitoChequeoConductor = $scope.ModeloDigitoChequeoPropietario;
                $scope.ModeloCiudadExpedicionConductor = $scope.ModeloCiudadExpedicionPropietario;
                $scope.ModeloConductor = $scope.ModeloPropietario;
                $scope.ModeloResidenciaConductor = $scope.ModeloResidenciaPropietario;
                $scope.ModeloDireccionConductor = $scope.ModeloDireccionPropietario;
                $scope.ModeloTelefonoConductor = $scope.ModeloTelefonoPropietario;
                $scope.ModeloCelularConductor = $scope.ModeloCelularPropietario;
                $scope.DeshabilitarAplicaConductor = true;
            } else {
                $scope.DeshabilitarAplicaConductor = false;
            } 
        };

        $scope.AplicaPropietarioSemirremolque = function (check) {
            if (check) {
                $scope.ModeloTipoNaturalezaSemirremolque = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.ModeloTipoNaturalezaPropietario.Codigo);
                $scope.ModeloTipoIdentificacionSemirremolque = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.ModeloTipoIdentificacionPropietario.Codigo);
                $scope.ModeloNumeroIdentificacionSemirremolque = $scope.ModeloNumeroIdentificacionPropietario;
                $scope.ModeloDigitoChequeoSemirremolque = $scope.ModeloDigitoChequeoPropietario;
                $scope.ModeloCiudadExpedicionSemirremolque = $scope.ModeloCiudadExpedicionPropietario;
                $scope.ModeloPropietarioSemirremolque = $scope.ModeloPropietario;
                $scope.ModeloResidenciaSemirremolque = $scope.ModeloResidenciaPropietario;
                $scope.ModeloDireccionSemirremolque = $scope.ModeloDireccionPropietario;
                $scope.ModeloTelefonoSemirremolque = $scope.ModeloTelefonoPropietario;
                $scope.ModeloCelularSemirremolque = $scope.ModeloCelularPropietario;
                $scope.DeshabilitarAplicaPropietarioSemirremolque = true;
            } else {
                $scope.DeshabilitarAplicaPropietarioSemirremolque = false;
            } 
        };

        //Funcion agregar Obsevacion a la referencia-------------------------------------------------------------
        $scope.AgregarReferencia = function () {
            if ($scope.Referencia != '' && $scope.Referencia != undefined && $scope.Referencia.Codigo != 0) {
                if ($scope.ObservacionReferencia != '' && $scope.ObservacionReferencia != undefined) {

                    $scope.ListaReferencias.push({ ObservacionReferencia: $scope.ObservacionReferencia, Referencia: $scope.Referencia, ModificarReferencia: false });
                    $scope.ObservacionReferencia = '';
                    $scope.AgruparListado()

                    $scope.ListaReferenciasAgrupado.forEach(function (Recorrido) {
                        Recorrido.Data.forEach(function (Detalle) {
                            if ($scope.Referencia.Codigo == Detalle.Referencia.Codigo) {
                                Recorrido.items = false;
                            } else {
                                Recorrido.items = true;
                            }
                        })
                    })
                } else {
                    ShowError('Debe ingresar la observación de la referencia');
                }
            } else {
                ShowError('Debe ingresar el tipo de referencia');
            }
        }

        //Funcion agrupar listados por evento--------------------------------------------------------------
        $scope.AgruparListado = function () {
            $scope.ListaReferenciasAgrupado = [];
            $scope.ListaReferenciasAgrupado = AgruparListados($scope.ListaReferencias, 'Referencia');
        };

        $scope.ListTitem = function (item) {
            if (item.items) {
                item.items = false;
            } else {
                item.items = true;
            }
        };

        //Funcion eliminar referencia---------------------------------------------------------------------------
        $scope.EliminarReferencia = function (referencia, obsevacion) {
            if ($scope.ListaReferencias.length > 0) {
                for (var i = 0; i < $scope.ListaReferencias.length; i++) {
                    if (obsevacion === $scope.ListaReferencias[i].ObservacionReferencia) {
                        if (referencia === $scope.ListaReferencias[i].Referencia.Codigo) {
                            $scope.ListaReferencias.splice(i, 1);
                            $scope.AgruparListado();
                        }
                    }
                }
            }
        };

        //Funciones validar identificacion propietario-------------------------------------------------------        
        $scope.VerificarPropietario = function (propietario) {
            $scope.ValidarIdentificacionPropietario = '';
            $scope.ValidarIdentificacionPropietario = propietario;
            if ($scope.ValidarIdentificacionPropietario == undefined || isNaN($scope.ValidarIdentificacionPropietario) === true) {
                ShowInfo('El propietario no se encuentra registrado, por favor diligenciar todos los campos.');
                $scope.ModeloTipoNaturalezaPropietario = $scope.ListadoNaturalezas[0];
                $scope.ModeloTipoIdentificacionPropietario = $scope.ListadoTipoIdentificacion[0];
                $scope.ModeloDigitoChequeoPropietario = '';
                $scope.ModeloCiudadExpedicionPropietario = '';
                $scope.ModeloPropietario = { Nombre: '' };
                $scope.ModeloPropietario = { PrimeroApellido: '' };
                $scope.ModeloPropietario = { SegundoApellido: '' };
                $scope.ModeloPropietario = { RazonSocial: '' };
                $scope.ModeloResidenciaPropietario = '';
                $scope.ModeloDireccionPropietario = '';
                $scope.ModeloTelefonoPropietario = '';
                $scope.ModeloCelularPropietario = '';

                $scope.AplicaTenedor($scope.CheckTenedor);
                $scope.AplicaConductor($scope.CheckConductor);
                $scope.AplicaPropietarioSemirremolque($scope.CheckPropietarioSemirremolque);
            } else {
                TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_PROPIETARIO, NumeroIdentificacion: $scope.ValidarIdentificacionPropietario }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {

                                item = response.data.Datos[0];

                                $scope.ModeloTipoNaturalezaPropietario = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + item.TipoNaturaleza.Codigo);
                                $scope.ModeloTipoIdentificacionPropietario = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + item.TipoIdentificacion.Codigo);
                                $scope.ModeloNumeroIdentificacionPropietario = item.NumeroIdentificacion;
                                $scope.ModeloDigitoChequeoPropietario = item.DigitoChequeo;
                                $scope.ModeloCiudadExpedicionPropietario = item.CiudadExpedicionIdent;
                                $scope.ModeloPropietario = item;
                                $scope.ModeloResidenciaPropietario = item.Ciudad;
                                $scope.ModeloDireccionPropietario = item.Direccion;
                                $scope.ModeloTelefonoPropietario = item.Telefonos;
                                $scope.ModeloCelularPropietario = item.Celular;

                                $scope.AplicaTenedor($scope.CheckTenedor);
                                $scope.AplicaConductor($scope.CheckConductor);
                                $scope.AplicaPropietarioSemirremolque($scope.CheckPropietarioSemirremolque);

                            } else {
                                ShowInfo('El propietario no se encuentra registrado, por favor diligenciar todos los campos.');
                                $scope.ModeloTipoNaturalezaPropietario = $scope.ListadoNaturalezas[0];
                                $scope.ModeloTipoIdentificacionPropietario = $scope.ListadoTipoIdentificacion[0];
                                $scope.ModeloDigitoChequeoPropietario = '';
                                $scope.ModeloCiudadExpedicionPropietario = '';
                                $scope.ModeloPropietario = { Nombre: '' };
                                $scope.ModeloPropietario = { PrimeroApellido: '' };
                                $scope.ModeloPropietario = { SegundoApellido: '' };
                                $scope.ModeloPropietario = { RazonSocial: '' };
                                $scope.ModeloResidenciaPropietario = '';
                                $scope.ModeloDireccionPropietario = '';
                                $scope.ModeloTelefonoPropietario = '';
                                $scope.ModeloCelularPropietario = '';

                                $scope.AplicaTenedor($scope.CheckTenedor);
                                $scope.AplicaConductor($scope.CheckConductor);
                                $scope.AplicaPropietarioSemirremolque($scope.CheckPropietarioSemirremolque);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        //Funciones validar identificacion propietario semirremolque-------------------------------------------------------
        $scope.VerificarPropietarioSemirremolque = function (propietariosemirremolque) {
            $scope.ValidarIdentificacionPropietarioSemirremolque = '';
            $scope.ValidarIdentificacionPropietarioSemirremolque = propietariosemirremolque;
            if ($scope.ValidarIdentificacionPropietarioSemirremolque === undefined || isNaN($scope.ValidarIdentificacionPropietarioSemirremolque) === true) {
                ShowInfo('El propietario del semirremolque no se encuentra registrado, por favor diligenciar todos los campos.');
                $scope.ModeloTipoNaturalezaSemirremolque = $scope.ListadoNaturalezas[0];
                $scope.ModeloTipoIdentificacionSemirremolque = $scope.ListadoTipoIdentificacion[0];
                $scope.ModeloDigitoChequeoSemirremolque = '';
                $scope.ModeloCiudadExpedicionSemirremolque = '';
                $scope.ModeloPropietarioSemirremolque = { Nombre: '' };
                $scope.ModeloPropietarioSemirremolque = { PrimeroApellido: '' };
                $scope.ModeloPropietarioSemirremolque = { SegundoApellido: '' };
                $scope.ModeloPropietarioSemirremolque = { RazonSocial: '' };
                $scope.ModeloResidenciaSemirremolque = '';
                $scope.ModeloDireccionSemirremolque = '';
                $scope.ModeloTelefonoSemirremolque = '';
                $scope.ModeloCelularSemirremolque = '';
            } else {
                TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_PROPIETARIO, NumeroIdentificacion: $scope.ValidarIdentificacionPropietarioSemirremolque }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {

                                item = response.data.Datos[0];

                                $scope.ModeloTipoNaturalezaSemirremolque = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + item.TipoNaturaleza.Codigo);
                                $scope.ModeloTipoIdentificacionSemirremolque = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + item.TipoIdentificacion.Codigo);
                                $scope.ModeloNumeroIdentificacionSemirremolque = item.NumeroIdentificacion;
                                $scope.ModeloDigitoChequeoSemirremolque = item.DigitoChequeo;
                                $scope.ModeloCiudadExpedicionSemirremolque = item.CiudadExpedicionIdent;
                                $scope.ModeloPropietarioSemirremolque = item;
                                $scope.ModeloResidenciaSemirremolque = item.Ciudad;
                                $scope.ModeloDireccionSemirremolque = item.Direccion;
                                $scope.ModeloTelefonoSemirremolque = item.Telefonos;
                                $scope.ModeloCelularSemirremolque = item.Celular;
                            } else {
                                ShowInfo('El propietario del semirremolque no se encuentra registrado, por favor diligenciar todos los campos.');
                                $scope.ModeloTipoNaturalezaSemirremolque = $scope.ListadoNaturalezas[0];
                                $scope.ModeloTipoIdentificacionSemirremolque = $scope.ListadoTipoIdentificacion[0];
                                $scope.ModeloDigitoChequeoSemirremolque = '';
                                $scope.ModeloCiudadExpedicionSemirremolque = '';
                                $scope.ModeloPropietarioSemirremolque = { Nombre: '' };
                                $scope.ModeloPropietarioSemirremolque = { PrimeroApellido: '' };
                                $scope.ModeloPropietarioSemirremolque = { SegundoApellido: '' };
                                $scope.ModeloPropietarioSemirremolque = { RazonSocial: '' };
                                $scope.ModeloResidenciaSemirremolque = '';
                                $scope.ModeloDireccionSemirremolque = '';
                                $scope.ModeloTelefonoSemirremolque = '';
                                $scope.ModeloCelularSemirremolque = '';
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        //Funciones validar identificacion tenedor-------------------------------------------------------
        $scope.VerificarTenedor = function (tenedor) {
            $scope.ValidarIdentificacionTenedor = '';
            $scope.ValidarIdentificacionTenedor = tenedor;
            if ($scope.ValidarIdentificacionTenedor === undefined || isNaN($scope.ValidarIdentificacionTenedor) === true) {
                ShowInfo('El tenedor no se encuentra registrado, por favor diligenciar todos los campos.');
                //$scope.ModeloTipoNaturalezaTenedor = $scope.ListadoNaturalezas[0];
                //$scope.ModeloTipoIdentificacionTenedor = $scope.ListadoTipoIdentificacion[0];
                //$scope.ModeloDigitoChequeoTenedor = '';
                //$scope.ModeloCiudadExpedicionTenedor = '';
                //$scope.ModeloTenedor = { Nombre: '' };
                //$scope.ModeloTenedor = { PrimeroApellido: '' };
                //$scope.ModeloTenedor = { SegundoApellido: '' };
                //$scope.ModeloTenedor = { RazonSocial: '' };
                //$scope.ModeloResidenciaTenedor = '';
                //$scope.ModeloDireccionTenedor = '';
                //$scope.ModeloTelefonoTenedor = '';
                //$scope.ModeloCelularTenedor = '';
            } else {
                TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_TENEDOR, NumeroIdentificacion: $scope.ValidarIdentificacionTenedor }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {

                                item = response.data.Datos[0];

                                $scope.ModeloTipoNaturalezaTenedor = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + item.TipoNaturaleza.Codigo);
                                $scope.ModeloTipoIdentificacionTenedor = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + item.TipoIdentificacion.Codigo);
                                $scope.ModeloNumeroIdentificacionTenedor = item.NumeroIdentificacion;
                                $scope.ModeloDigitoChequeoTenedor = item.DigitoChequeo;
                                $scope.ModeloCiudadExpedicionTenedor = item.CiudadExpedicionIdent;
                                $scope.ModeloTenedor = item;
                                $scope.ModeloResidenciaTenedor = item.Ciudad;
                                $scope.ModeloDireccionTenedor = item.Direccion;
                                $scope.ModeloTelefonoTenedor = item.Telefonos;
                                $scope.ModeloCelularTenedor = item.Celular;
                            } else {
                                ShowInfo('El tenedor no se encuentra registrado, por favor diligenciar todos los campos.');
                                $scope.ModeloTipoNaturalezaTenedor = $scope.ListadoNaturalezas[0];
                                $scope.ModeloTipoIdentificacionTenedor = $scope.ListadoTipoIdentificacion[0];
                                //$scope.ModeloDigitoChequeoTenedor = '';
                                //$scope.ModeloCiudadExpedicionTenedor = '';
                                //$scope.ModeloTenedor = { Nombre: '' };
                                //$scope.ModeloTenedor = { PrimeroApellido: '' };
                                //$scope.ModeloTenedor = { SegundoApellido: '' };
                                //$scope.ModeloTenedor = { RazonSocial: '' };
                                //$scope.ModeloResidenciaTenedor = '';
                                //$scope.ModeloDireccionTenedor = '';
                                //$scope.ModeloTelefonoTenedor = '';
                                //$scope.ModeloCelularTenedor = '';
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        //Funciones validar identificacion conductor-------------------------------------------------------
        $scope.VerificarConductor = function (conductor) {
            $scope.ValidarIdentificacionConductor = '';
            $scope.ValidarIdentificacionConductor = conductor;
            if ($scope.ValidarIdentificacionConductor == undefined || isNaN($scope.ValidarIdentificacionConductor) === true) {
                ShowInfo('El conductor no se encuentra registrado, por favor diligenciar todos los campos.');
                $scope.ModeloTipoNaturalezaConductor = $scope.ListadoNaturalezas[0];
                $scope.ModeloTipoIdentificacionConductor = $scope.ListadoTipoIdentificacion[0];
                $scope.ModeloDigitoChequeoConductor = '';
                $scope.ModeloCiudadExpedicionConductor = '';
                $scope.ModeloConductor = { Nombre: '' };
                $scope.ModeloConductor = { PrimeroApellido: '' };
                $scope.ModeloConductor = { SegundoApellido: '' };
                $scope.ModeloConductor = { RazonSocial: '' };
                $scope.ModeloResidenciaConductor = '';
                $scope.ModeloDireccionConductor = '';
                $scope.ModeloTelefonoConductor = '';
                $scope.ModeloCelularConductor = '';
            } else {
                TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_CONDUCTOR, NumeroIdentificacion: $scope.ValidarIdentificacionConductor }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {

                                item = response.data.Datos[0];

                                $scope.ModeloTipoNaturalezaConductor = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + item.TipoNaturaleza.Codigo);
                                $scope.ModeloTipoIdentificacionConductor = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + item.TipoIdentificacion.Codigo);
                                $scope.ModeloNumeroIdentificacionConductor = item.NumeroIdentificacion;
                                $scope.ModeloDigitoChequeoConductor = item.DigitoChequeo;
                                $scope.ModeloCiudadExpedicionConductor = item.CiudadExpedicionIdent;
                                $scope.ModeloConductor = item;
                                $scope.ModeloResidenciaConductor = item.Ciudad;
                                $scope.ModeloDireccionConductor = item.Direccion;
                                $scope.ModeloTelefonoConductor = item.Telefonos;
                                $scope.ModeloCelularConductor = item.Celular;
                            } else {
                                ShowInfo('El conductor no se encuentra registrado, por favor diligenciar todos los campos.');
                                $scope.ModeloTipoNaturalezaConductor = $scope.ListadoNaturalezas[0];
                                $scope.ModeloTipoIdentificacionConductor = $scope.ListadoTipoIdentificacion[0];
                                $scope.ModeloDigitoChequeoConductor = '';
                                $scope.ModeloCiudadExpedicionConductor = '';
                                $scope.ModeloConductor = { Nombre: '' };
                                $scope.ModeloConductor = { PrimeroApellido: '' };
                                $scope.ModeloConductor = { SegundoApellido: '' };
                                $scope.ModeloConductor = { RazonSocial: '' };
                                $scope.ModeloResidenciaConductor = '';
                                $scope.ModeloDireccionConductor = '';
                                $scope.ModeloTelefonoConductor = '';
                                $scope.ModeloCelularConductor = '';
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        /*Cargar los check de autorizaciones entidades consultadas*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ENTIDAD_CONSULTADA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.EntidadesConsultadas = response.data.Datos;

                        $scope.EntidadesConsultadas.Asc = true
                        OrderBy('Nombre', undefined, $scope.EntidadesConsultadas);
                    }
                    else {
                        $scope.EntidadesConsultadas = []
                    }
                }
            }, function (response) {
            });

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            BloqueoPantalla.start('Cargando estudio seguridad...');

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Numero: $scope.Numero,
                ConsularDocumentos: 1
            };

            EstudioSeguridadFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.ModeloEstadoDocumento = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        if ($scope.ModeloEstadoDocumento.Codigo === 1) {
                            $scope.DeshabilitarDefinitivo = true;
                            $scope.DeshabilitarBotonGuardar = true;
                            $scope.CodigoEstadoAutorizacion = response.data.Datos.EstadoAutorizacion
                            if ($scope.ListadoAutorizacion.length > 0 && $scope.CodigoEstadoAutorizacion > 0) {
                                $scope.ModeloEstadoAutorizacion = $linq.Enumerable().From($scope.ListadoAutorizacion).First('$.Codigo == ' + response.data.Datos.EstadoAutorizacion);
                            }
                        } else {
                            $scope.DeshabilitarDefinitivo = false;
                            $scope.DeshabilitarBotonGuardar = false;
                            $scope.ModeloEstadoAutorizacion = $scope.ListadoAutorizacion[0];
                        }

                        $scope.ModeloNumero = response.data.Datos.Numero;
                        $scope.ModeloNumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.ModeloPlacaVehiculo = response.data.Datos.Placa;
                        $scope.ModeloFecha = new Date(response.data.Datos.Fecha);

                        $scope.ModeloNumeroAutorizacion = response.data.Datos.NumeroAutorizacion;

                        $scope.CodigoClaseRiesgo = response.data.Datos.ClaseRiesgo
                        if ($scope.ListadoClaseRiesgo.length > 0 && $scope.CodigoClaseRiesgo > 0) {
                            $scope.ModeloClaseRiesgo = $linq.Enumerable().From($scope.ListadoClaseRiesgo).First('$.Codigo == ' + response.data.Datos.ClaseRiesgo);
                        }

                        $scope.ModeloObservaciones = response.data.Datos.Observaciones;

                        $scope.CodigoMarca = response.data.Datos.MarcaVehiculo
                        if ($scope.ListadoMarcas.length > 0 && $scope.CodigoMarca > 0) {
                            $scope.ModeloMarca = $linq.Enumerable().From($scope.ListadoMarcas).First('$.Codigo == ' + response.data.Datos.MarcaVehiculo);
                        }

                        $scope.CodigoTipoVehiculo = response.data.Datos.TipoVehiculo
                        if ($scope.ListadoTipoVehiculo.length > 0 && $scope.CodigoTipoVehiculo > 0) {
                            $scope.ModeloTipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == ' + response.data.Datos.TipoVehiculo);
                        }

                        $scope.ModeloVehiculo = response.data.Datos.Modelo;
                        $scope.ModeloRepotenciado = response.data.Datos.ModeloRepotenciado;
                        $scope.ModeloColor = $scope.CargarColor(response.data.Datos.ColorVehiculo);
                        $scope.ModeloAseguradora = $scope.CargarTercero(response.data.Datos.Aseguradora.Codigo);

                        $scope.ModeloSoat = response.data.Datos.NumeroSeguro;
                        $scope.ModeloFechaEmisionSoat = new Date(response.data.Datos.FechaEmisionSeguro);
                        $scope.ModeloFechaVencimientoSoat = new Date(response.data.Datos.FechaVenceSeguro);
                        $scope.ModeloRevision = response.data.Datos.NumeroTecnomecanica;
                        $scope.ModeloFechaEmisionTecno = new Date(response.data.Datos.FechaEmisionTecnomecanica);
                        $scope.ModeloFechaVencimientoTecno = new Date(response.data.Datos.FechaVenceTecnomecanica);

                        $scope.ModeloEmpresaGPS = response.data.Datos.NombreEmpresaGPS;
                        $scope.ModeloTelefonoGPS = response.data.Datos.TelefonoEmpresaGPS;
                        $scope.ModeloUsuarioGPS = response.data.Datos.UsuarioGPS;
                        $scope.ModeloClaveGPS = response.data.Datos.ClaveGPS;
                        $scope.ModeloAfiliadora = response.data.Datos.EmpresaAfiliadora;
                        $scope.ModeloTelefonoAfiliadora = response.data.Datos.TelefonoAfiliadora;
                        $scope.ModeloNumeroCarnet = response.data.Datos.NumeroCarnetAfiliadora;
                        $scope.ModeloFechaVencimientoAfiliadora = new Date(response.data.Datos.FechaVenceAfiliadora);

                        $scope.ModeloSemirremolque = response.data.Datos.PlacaSemirremolque;
                        $scope.CodigoMarcaSemirremolque = response.data.Datos.MarcaSemirremolque.Codigo;
                        if ($scope.ListadoMarcasSemirremolques.length > 0 && $scope.CodigoMarcaSemirremolque > 0) {
                            $scope.ModeloMarcaSemirremolque = $linq.Enumerable().From($scope.ListadoMarcasSemirremolques).First('$.Codigo == ' + response.data.Datos.MarcaSemirremolque.Codigo);
                        }
                        $scope.CodigoTipoSemirremolque = response.data.Datos.TipoSemirremolque.Codigo;
                        if ($scope.ListadoTipoSemirremolques.length > 0 && $scope.CodigoTipoSemirremolque > 0) {
                            $scope.ModeloTipoSemirremolque = $linq.Enumerable().From($scope.ListadoTipoSemirremolques).First('$.Codigo == ' + response.data.Datos.TipoSemirremolque.Codigo);
                        }
                        $scope.CodigoTipoCarroceriaSemirremolque = response.data.Datos.TipoCarroceriaSemirremolque.Codigo;
                        if ($scope.ListadoTipoCarroceria.length > 0 && $scope.CodigoTipoCarroceriaSemirremolque > 0) {
                            $scope.ModeloTipoCarroceriaSemirremolque = $linq.Enumerable().From($scope.ListadoTipoCarroceria).First('$.Codigo == ' + response.data.Datos.TipoCarroceriaSemirremolque.Codigo);
                        }
                        $scope.ModeloModeloSemirremolque = response.data.Datos.ModeloSemirremolque;
                        $scope.ModeloNumeroEjesSemirremolque = response.data.Datos.NumeroEjes;

                        $scope.CodigoTipoNaturalezaPropietario = response.data.Datos.Propietario.TipoNaturaleza.Codigo
                        $scope.CodigoTipoNaturalezasSemirremolque = response.data.Datos.PropietarioRemolque.TipoNaturaleza.Codigo
                        $scope.CodigoTipoNaturalezaTenedor = response.data.Datos.Tenedor.TipoNaturaleza.Codigo
                        $scope.CodigoTipoNaturalezaConductor = response.data.Datos.Conductor.TipoNaturaleza.Codigo
                        if ($scope.ListadoNaturalezas.length > 0) {
                            if ($scope.CodigoTipoNaturalezaPropietario > 0) {
                                $scope.ModeloTipoNaturalezaPropietario = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo == ' + response.data.Datos.Propietario.TipoNaturaleza.Codigo);
                            }
                            if ($scope.CodigoTipoNaturalezasSemirremolque > 0) {
                                $scope.ModeloTipoNaturalezaSemirremolque = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo == ' + response.data.Datos.PropietarioRemolque.TipoNaturaleza.Codigo);
                            }
                            if ($scope.CodigoTipoNaturalezaTenedor > 0) {
                                $scope.ModeloTipoNaturalezaTenedor = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo == ' + response.data.Datos.Tenedor.TipoNaturaleza.Codigo);
                            }
                            if ($scope.CodigoTipoNaturalezaConductor > 0) {
                                $scope.ModeloTipoNaturalezaConductor = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo == ' + response.data.Datos.Conductor.TipoNaturaleza.Codigo);
                            }
                        }

                        $scope.CodigoTipoIdentificacionPropietario = response.data.Datos.Propietario.TipoIdentificacion.Codigo
                        $scope.CodigoTipoIdentificacionSemirremolque = response.data.Datos.PropietarioRemolque.TipoIdentificacion.Codigo
                        $scope.CodigoTipoIdentificacionTenedor = response.data.Datos.Tenedor.TipoIdentificacion.Codigo
                        $scope.CodigoTipoIdentificacionConductor = response.data.Datos.Conductor.TipoIdentificacion.Codigo
                        if ($scope.ListadoTipoIdentificacion.length > 0) {
                            if ($scope.CodigoTipoIdentificacionPropietario > 0) {
                                $scope.ModeloTipoIdentificacionPropietario = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.Propietario.TipoIdentificacion.Codigo);
                            }
                            if ($scope.CodigoTipoIdentificacionSemirremolque > 0) {
                                $scope.ModeloTipoIdentificacionSemirremolque = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.PropietarioRemolque.TipoIdentificacion.Codigo);
                            }
                            if ($scope.CodigoTipoIdentificacionTenedor > 0) {
                                $scope.ModeloTipoIdentificacionTenedor = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.Tenedor.TipoIdentificacion.Codigo);
                            }
                            if ($scope.CodigoTipoIdentificacionConductor > 0) {
                                $scope.ModeloTipoIdentificacionConductor = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.Conductor.TipoIdentificacion.Codigo);
                            }
                        }

                        $scope.ModeloNumeroIdentificacionPropietario = response.data.Datos.Propietario.NumeroIdentificacion;
                        $scope.ModeloDigitoChequeoPropietario = response.data.Datos.Propietario.DigitoChequeo;
                        $scope.ModeloPropietario.Nombre = response.data.Datos.Propietario.Nombre;
                        $scope.ModeloPropietario.PrimeroApellido = response.data.Datos.Propietario.PrimeroApellido;
                        $scope.ModeloPropietario.SegundoApellido = response.data.Datos.Propietario.SegundoApellido;
                        $scope.ModeloPropietario.RazonSocial = response.data.Datos.Propietario.RazonSocial;
                        $scope.ModeloDireccionPropietario = response.data.Datos.Propietario.Direccion;
                        $scope.ModeloTelefonoPropietario = response.data.Datos.Propietario.Telefonos;
                        $scope.ModeloCelularPropietario = response.data.Datos.Propietario.Celular;

                        if (response.data.Datos.PropietarioRemolque.NumeroIdentificacion > 0) {
                            $scope.ModeloNumeroIdentificacionSemirremolque = response.data.Datos.PropietarioRemolque.NumeroIdentificacion;
                            $scope.ModeloDigitoChequeoSemirremolque = response.data.Datos.PropietarioRemolque.DigitoChequeo;
                            $scope.ModeloPropietarioSemirremolque.Nombre = response.data.Datos.PropietarioRemolque.Nombre;
                            $scope.ModeloPropietarioSemirremolque.PrimeroApellido = response.data.Datos.PropietarioRemolque.PrimeroApellido;
                            $scope.ModeloPropietarioSemirremolque.SegundoApellido = response.data.Datos.PropietarioRemolque.SegundoApellido;
                            $scope.ModeloPropietarioSemirremolque.RazonSocial = response.data.Datos.PropietarioRemolque.RazonSocial;
                            $scope.ModeloDireccionSemirremolque = response.data.Datos.PropietarioRemolque.Direccion;
                            $scope.ModeloTelefonoSemirremolque = response.data.Datos.PropietarioRemolque.Telefonos;
                            $scope.ModeloCelularSemirremolque = response.data.Datos.PropietarioRemolque.Celular;
                            $scope.ModeloCiudadExpedicionSemirremolque = $scope.CargarCiudad(response.data.Datos.PropietarioRemolque.CiudadExpedicionIdent.Codigo);
                            $scope.ModeloResidenciaSemirremolque = $scope.CargarCiudad(response.data.Datos.PropietarioRemolque.Ciudad.Codigo);
                        }

                        $scope.ModeloNumeroIdentificacionTenedor = response.data.Datos.Tenedor.NumeroIdentificacion;
                        $scope.ModeloDigitoChequeoTenedor = response.data.Datos.Tenedor.DigitoChequeo;
                        $scope.ModeloTenedor.Nombre = response.data.Datos.Tenedor.Nombre;
                        $scope.ModeloTenedor.PrimeroApellido = response.data.Datos.Tenedor.PrimeroApellido;
                        $scope.ModeloTenedor.SegundoApellido = response.data.Datos.Tenedor.SegundoApellido;
                        $scope.ModeloTenedor.RazonSocial = response.data.Datos.Tenedor.RazonSocial;
                        $scope.ModeloDireccionTenedor = response.data.Datos.Tenedor.Direccion;
                        $scope.ModeloTelefonoTenedor = response.data.Datos.Tenedor.Telefonos;
                        $scope.ModeloCelularTenedor = response.data.Datos.Tenedor.Celular;

                        $scope.ModeloNumeroIdentificacionConductor = response.data.Datos.Conductor.NumeroIdentificacion;
                        $scope.ModeloDigitoChequeoConductor = response.data.Datos.Conductor.DigitoChequeo;
                        $scope.ModeloConductor.Nombre = response.data.Datos.Conductor.Nombre;
                        $scope.ModeloConductor.PrimeroApellido = response.data.Datos.Conductor.PrimeroApellido;
                        $scope.ModeloConductor.SegundoApellido = response.data.Datos.Conductor.SegundoApellido;
                        $scope.ModeloConductor.RazonSocial = response.data.Datos.Conductor.RazonSocial;
                        $scope.ModeloDireccionConductor = response.data.Datos.Conductor.Direccion;
                        $scope.ModeloTelefonoConductor = response.data.Datos.Conductor.Telefonos;
                        $scope.ModeloCelularConductor = response.data.Datos.Conductor.Celular;

                        if (response.data.Datos.Propietario.NumeroIdentificacion === response.data.Datos.Tenedor.NumeroIdentificacion) {
                            $scope.CheckTenedor = true;
                            $scope.DeshabilitarAplicaTenedor = true;
                        }
                        if (response.data.Datos.Propietario.NumeroIdentificacion === response.data.Datos.Conductor.NumeroIdentificacion) {
                            $scope.CheckConductor = true;
                            $scope.DeshabilitarAplicaConductor = true;
                        }
                        if (response.data.Datos.Propietario.NumeroIdentificacion === response.data.Datos.PropietarioRemolque.NumeroIdentificacion) {
                            $scope.CheckPropietarioSemirremolque = true;
                            $scope.DeshabilitarAplicaPropietarioSemirremolque = true;
                        }

                        $scope.ModeloCiudadExpedicionPropietario = $scope.CargarCiudad(response.data.Datos.Propietario.CiudadExpedicionIdent.Codigo);

                        $scope.ModeloCiudadExpedicionTenedor = $scope.CargarCiudad(response.data.Datos.Tenedor.CiudadExpedicionIdent.Codigo);
                        $scope.ModeloCiudadExpedicionConductor = $scope.CargarCiudad(response.data.Datos.Conductor.CiudadExpedicionIdent.Codigo);

                        $scope.ModeloResidenciaPropietario = $scope.CargarCiudad(response.data.Datos.Propietario.Ciudad.Codigo);

                        $scope.ModeloResidenciaTenedor = $scope.CargarCiudad(response.data.Datos.Tenedor.Ciudad.Codigo);
                        $scope.ModeloResidenciaConductor = $scope.CargarCiudad(response.data.Datos.Conductor.Ciudad.Codigo);

                        $scope.ModeloCliente = response.data.Datos.Cliente
                        $scope.ModeloMercanciaTransportar = response.data.Datos.MercanciaCliente;

                        $scope.CodigoRuta = response.data.Datos.Ruta.Codigo
                        if ($scope.ListaRutas.length > 0 && $scope.CodigoRuta > 0) {
                            $scope.ModeloRuta = $linq.Enumerable().From($scope.ListaRutas).First('$.Codigo == ' + response.data.Datos.Ruta.Codigo);
                        }
                        $scope.ModeloValorDeclarado = MascaraValores(response.data.Datos.ValorMercancia);
                        $scope.ModeloObservacionesCliente = response.data.Datos.ObservacionesCliente;

                        if (response.data.Datos.EstadoAutorizacion === 9701) {
                            $scope.DeshabilitarAproboRechazo = false;
                        } else {
                            $scope.DeshabilitarAproboRechazo = true;
                            $scope.AplicaMostrarAutorizaciones = true;
                        }

                        if ($scope.Aprobacion > 0) {
                            $scope.DeshabilitarBotonGuardar = false;
                            $scope.DeshabilitarDefinitivo = false
                        } else {
                            $scope.DeshabilitarBotonGuardar = true;
                        }

                        if ($scope.ListaTipoReferencia.length > 0) {
                            $scope.Referencia = $linq.Enumerable().From($scope.ListaTipoReferencia).First('$.Codigo == 0')
                        }
                        $scope.ListaReferencias = response.data.Datos.ListadoReferencias;
                        $scope.AgruparListado();

                        $scope.EntidadesConsultadas.forEach(function (lista) {
                            response.data.Datos.ListadoEntidades.forEach(function (entidad) {
                                if (entidad.Estado == 1) {
                                    if (entidad.Codigo == lista.Codigo) {
                                        lista.Estado = true
                                        lista.ObservacionEntidades = entidad.ObservacionEntidades
                                    }
                                } else {
                                    lista.Estado = false
                                }
                            })
                        })

                        $scope.ListadoTiposDocumentos.forEach(function (item) {
                            response.data.Datos.ListadoDocumentos.forEach(function (entidad) {
                                if (item.Codigo === entidad.TDESCodigo && entidad.ExtensionDocumento !== '') {
                                    item.CodigoDocumentos = 1
                                    $scope.ENESNumero = entidad.ENESNumero
                                    if (entidad.Referencia == 0) {
                                        item.Referencia = '';
                                    } else {
                                        item.Referencia = entidad.Referencia
                                    }
                                    if (entidad.Emisor == 0) {
                                        item.Emisor = '';
                                    } else {
                                        item.Emisor = entidad.Emisor
                                    }
                                    if (new Date(entidad.FechaEmision) < MIN_DATE) {
                                        item.FechaEmision = ''
                                    } else {
                                        item.FechaEmision = new Date(entidad.FechaEmision)
                                    }
                                    if (new Date(entidad.FechaVence) < MIN_DATE) {
                                        item.FechaVence = ''
                                    } else {
                                        item.FechaVence = new Date(entidad.FechaVence)
                                    }
                                } else if (item.Codigo == entidad.TDESCodigo) {
                                    $scope.ENESNumero = entidad.ENESNumero
                                    if (entidad.Referencia == 0) {
                                        item.Referencia = '';
                                    } else {
                                        item.Referencia = entidad.Referencia
                                    }
                                    if (entidad.Emisor == 0) {
                                        item.Emisor = '';
                                    } else {
                                        item.Emisor = entidad.Emisor
                                    }
                                    if (new Date(entidad.FechaEmision) < MIN_DATE) {
                                        item.FechaEmision = ''
                                    } else {
                                        item.FechaEmision = new Date(entidad.FechaEmision)
                                    }
                                    if (new Date(entidad.FechaVence) < MIN_DATE) {
                                        item.FechaVence = ''
                                    } else {
                                        item.FechaVence = new Date(entidad.FechaVence)
                                    }
                                }
                            })
                        })
                        BloqueoPantalla.stop();

                    }
                    else {
                        ShowError('No se logro consultar el estudio de seguridad No.' + $scope.NumeroDocumento);
                        document.location.href = '#!consultarEstudioSeguridad';
                        BloqueoPantalla.stop();
                    }
                }, function (response) {
                    ShowError('No se logro consultar el estudio de seguridad No.' + $scope.NumeroDocumento + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!consultarEstudioSeguridad';
                    BloqueoPantalla.stop();
                });
            //BloqueoPantalla.stop();
        };

        /* Obtener parametros*/
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            $scope.Numero = $routeParams.Numero;
            if ($scope.Numero > 0) {
                Obtener();
            }
            if ($routeParams.Aprobacion !== undefined && $routeParams.Aprobacion !== null && $routeParams.Aprobacion !== '' && $routeParams.Aprobacion !== 0) {
                $scope.Aprobacion = $routeParams.Aprobacion;
                $scope.AplicaMostrarAutorizaciones = true;
            }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*----------------------------------------------------------------------------------Funcion guardar datos-------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                if ($scope.AplicaMostrarAutorizaciones) {
                    showModal('modalConfirmacionGuardarAutorizacion');
                } else {
                    showModal('modalConfirmacionGuardar');
                }
            }
        };

        $scope.Guardar = function (aprobar) {
            $scope.Aprobar = aprobar;
            closeModal('modalConfirmacionGuardar');

            if ($scope.ListaReferencias.length > 0) {
                $scope.ListaReferencias = $scope.ListaReferencias;
            }

            $scope.EntidadesConsultadas.forEach(function (entidad) {
                if (entidad.Estado) {
                    entidad.Estado = 1;
                    $scope.listaEntidades.push(entidad);
                } else {
                    entidad.Estado = 0;
                }
            });

            if ($scope.ModeloColor === undefined) {
                $scope.ModeloColor = { Codigo: 0 };
            }
            //if ($scope.ModeloValorDeclarado === '' || $scope.ModeloValorDeclarado === undefined) {
            //    $scope.ModeloValorDeclarado = 0;
            //} else {
            //    MascaraNumero($scope.ModeloValorDeclarado);
            //}

            $scope.DatosGuardar = {

                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                NumeroDocumento: $scope.ModeloNumeroDocumento,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_ESTUDIO_SEGURIDAD,
                Fecha: $scope.ModeloFecha,
                Placa: $scope.ModeloPlacaVehiculo,
                Oficinas: $scope.Sesion.UsuarioAutenticado.Oficinas,
                EstadoAutorizacion: $scope.ModeloEstadoAutorizacion.Codigo,
                NumeroAutorizacion: $scope.ModeloNumeroAutorizacion,
                ClaseRiesgo: $scope.ModeloClaseRiesgo.Codigo,
                Estado: $scope.ModeloEstadoDocumento.Codigo,
                Observaciones: $scope.ModeloObservaciones,

                MarcaVehiculo: $scope.ModeloMarca.Codigo,
                TipoVehiculo: $scope.ModeloTipoVehiculo.Codigo,
                Modelo: $scope.ModeloVehiculo,
                ModeloRepotenciado: $scope.ModeloRepotenciado,
                ColorVehiculo: $scope.ModeloColor.Codigo,

                Aseguradora: $scope.ModeloAseguradora,
                NumeroSeguro: $scope.ModeloSoat,
                FechaEmisionSeguro: $scope.ModeloFechaEmisionSoat,
                FechaVenceSeguro: $scope.ModeloFechaVencimientoSoat,
                NumeroTecnomecanica: $scope.ModeloRevision,
                FechaEmisionTecnomecanica: $scope.ModeloFechaEmisionTecno,
                FechaVenceTecnomecanica: $scope.ModeloFechaVencimientoTecno,

                NombreEmpresaGPS: $scope.ModeloEmpresaGPS,
                TelefonoEmpresaGPS: $scope.ModeloTelefonoGPS,
                UsuarioGPS: $scope.ModeloUsuarioGPS,
                ClaveGPS: $scope.ModeloClaveGPS,

                EmpresaAfiliadora: $scope.ModeloAfiliadora,
                TelefonoAfiliadora: $scope.ModeloTelefonoAfiliadora,
                NumeroCarnetAfiliadora: $scope.ModeloNumeroCarnet,
                FechaVenceAfiliadora: $scope.ModeloFechaVencimientoAfiliadora,

                PlacaSemirremolque: $scope.ModeloSemirremolque,
                MarcaSemirremolque: $scope.ModeloMarcaSemirremolque,
                TipoSemirremolque: $scope.ModeloTipoSemirremolque,
                TipoCarroceriaSemirremolque: $scope.ModeloTipoCarroceriaSemirremolque,
                ModeloSemirremolque: $scope.ModeloModeloSemirremolque,
                NumeroEjes: $scope.ModeloNumeroEjesSemirremolque,

                Propietario: {
                    TipoNaturaleza: $scope.ModeloTipoNaturalezaPropietario,
                    TipoIdentificacion: $scope.ModeloTipoIdentificacionPropietario,
                    NumeroIdentificacion: $scope.ModeloNumeroIdentificacionPropietario,
                    DigitoChequeo: $scope.ModeloDigitoChequeoPropietario,
                    Nombre: $scope.ModeloPropietario.Nombre,
                    PrimeroApellido: $scope.ModeloPropietario.PrimeroApellido,
                    SegundoApellido: $scope.ModeloPropietario.SegundoApellido,
                    RazonSocial: $scope.ModeloPropietario.RazonSocial,
                    CiudadExpedicionIdent: $scope.ModeloCiudadExpedicionPropietario,
                    Ciudad: $scope.ModeloResidenciaPropietario,
                    Direccion: $scope.ModeloDireccionPropietario,
                    Telefonos: $scope.ModeloTelefonoPropietario,
                    Celular: $scope.ModeloCelularPropietario
                },

                PropietarioRemolque: {
                    TipoNaturaleza: $scope.ModeloTipoNaturalezaSemirremolque,
                    TipoIdentificacion: $scope.ModeloTipoIdentificacionSemirremolque,
                    NumeroIdentificacion: $scope.ModeloNumeroIdentificacionSemirremolque,
                    DigitoChequeo: $scope.ModeloDigitoChequeoSemirremolque,
                    Nombre: $scope.ModeloPropietarioSemirremolque.Nombre,
                    PrimeroApellido: $scope.ModeloPropietarioSemirremolque.PrimeroApellido,
                    SegundoApellido: $scope.ModeloPropietarioSemirremolque.SegundoApellido,
                    RazonSocial: $scope.ModeloPropietarioSemirremolque.RazonSocial,
                    CiudadExpedicionIdent: $scope.ModeloCiudadExpedicionSemirremolque,
                    Ciudad: $scope.ModeloResidenciaSemirremolque,
                    Direccion: $scope.ModeloDireccionSemirremolque,
                    Telefonos: $scope.ModeloTelefonoSemirremolque,
                    Celular: $scope.ModeloCelularSemirremolque
                },

                Tenedor: {
                    TipoNaturaleza: $scope.ModeloTipoNaturalezaTenedor,
                    TipoIdentificacion: $scope.ModeloTipoIdentificacionTenedor,
                    NumeroIdentificacion: $scope.ModeloNumeroIdentificacionTenedor,
                    DigitoChequeo: $scope.ModeloDigitoChequeoTenedor,
                    Nombre: $scope.ModeloTenedor.Nombre,
                    PrimeroApellido: $scope.ModeloTenedor.PrimeroApellido,
                    SegundoApellido: $scope.ModeloTenedor.SegundoApellido,
                    RazonSocial: $scope.ModeloTenedor.RazonSocial,
                    CiudadExpedicionIdent: $scope.ModeloCiudadExpedicionTenedor,
                    Ciudad: $scope.ModeloResidenciaTenedor,
                    Direccion: $scope.ModeloDireccionTenedor,
                    Telefonos: $scope.ModeloTelefonoTenedor,
                    Celular: $scope.ModeloCelularTenedor
                },

                Conductor: {
                    TipoNaturaleza: $scope.ModeloTipoNaturalezaConductor,
                    TipoIdentificacion: $scope.ModeloTipoIdentificacionConductor,
                    NumeroIdentificacion: $scope.ModeloNumeroIdentificacionConductor,
                    DigitoChequeo: $scope.ModeloDigitoChequeoConductor,
                    Nombre: $scope.ModeloConductor.Nombre,
                    PrimeroApellido: $scope.ModeloConductor.PrimeroApellido,
                    SegundoApellido: $scope.ModeloConductor.SegundoApellido,
                    RazonSocial: $scope.ModeloConductor.RazonSocial,
                    CiudadExpedicionIdent: $scope.ModeloCiudadExpedicionConductor,
                    Ciudad: $scope.ModeloResidenciaConductor,
                    Direccion: $scope.ModeloDireccionConductor,
                    Telefonos: $scope.ModeloTelefonoConductor,
                    Celular: $scope.ModeloCelularConductor
                },

                Cliente: $scope.ModeloCliente,
                MercanciaCliente: $scope.ModeloMercanciaTransportar,
                Ruta: $scope.ModeloRuta,
                ValorMercancia: $scope.ModeloValorDeclarado,
                ObservacionesCliente: $scope.ModeloObservacionesCliente,
                ListadoReferencias: $scope.ListaReferencias,
                ListadoEntidades: $scope.listaEntidades,
                Documentos: $scope.DetalleDocumentos,

                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },

            };

            EstudioSeguridadFactory.Guardar($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Aprobar !== undefined) {
                                if ($scope.Aprobar === 9703) {
                                    $scope.ConfirmarRechazo();
                                } else if ($scope.Aprobar === 9702) {
                                    $scope.ConfirmarAutorizar();
                                }
                            } else {
                                if ($scope.Numero === 0) {
                                    ShowSuccess('Se guardó el estudio seguridad No. ' + response.data.Datos);
                                }
                                else {
                                    ShowSuccess('Se modificó el estudio seguridad No. ' + response.data.Datos);
                                }
                                location.href = '#!ConsultarEstudioSeguridad/' + response.data.Datos;
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        //Funcion mostrar digito de chequeo----------------------------------------------------------------
        $scope.MostrarDigitoChequeo = function () {
            if ($scope.ModeloTipoNaturalezaPropietario.Codigo === 502) {
                $scope.DigitoChequeoPropietario = true
            }
            if ($scope.ModeloTipoNaturalezaPropietario.Codigo === 500 || $scope.ModeloTipoNaturalezaPropietario.Codigo === 501) {
                $scope.DigitoChequeoPropietario = false
            }
            if ($scope.ModeloTipoNaturalezaSemirremolque.Codigo === 502) {
                $scope.DigitoChequeoPropietarioSemirremolque = true
            }
            if ($scope.ModeloTipoNaturalezaSemirremolque.Codigo === 500 || $scope.ModeloTipoNaturalezaSemirremolque.Codigo === 501) {
                $scope.DigitoChequeoPropietarioSemirremolque = false
            }
            if ($scope.ModeloTipoNaturalezaTenedor.Codigo === 502) {
                $scope.DigitoChequeoTenedor = true
            }
            if ($scope.ModeloTipoNaturalezaTenedor.Codigo === 500 || $scope.ModeloTipoNaturalezaTenedor.Codigo === 501) {
                $scope.DigitoChequeoTenedor = false
            }
            if ($scope.ModeloTipoNaturalezaConductor.Codigo === 502) {
                $scope.DigitoChequeoConductor = true
            }
            if ($scope.ModeloTipoNaturalezaConductor.Codigo === 500 || $scope.ModeloTipoNaturalezaConductor.Codigo === 501) {
                $scope.DigitoChequeoConductor = false
            }
        }

        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridos() {
            $scope.MensajesError = [];
            window.scrollTo(top, top);
            var continuar = true;
            if ($scope.ModeloCiudadExpedicionPropietario === undefined || $scope.ModeloCiudadExpedicionPropietario === '' || $scope.ModeloCiudadExpedicionPropietario === null) {
                $scope.MensajesError.push('Debe digitar la ciudad de expedición del propietario');
                continuar = false;
            }
            if ($scope.ModeloPlacaVehiculo === undefined || $scope.ModeloPlacaVehiculo === '' || $scope.ModeloPlacaVehiculo === null) {
                $scope.MensajesError.push('Debe digitar la placa del vehículo');
                continuar = false;
            }
            if ($scope.ModeloClaseRiesgo === undefined || $scope.ModeloClaseRiesgo === '' || $scope.ModeloClaseRiesgo === null || $scope.ModeloClaseRiesgo.Codigo === 0) {
                $scope.MensajesError.push('Debe seleccionar la clase de riesgo');
                continuar = false;
            }
            if ($scope.ModeloVehiculo === undefined || $scope.ModeloVehiculo === '' || $scope.ModeloVehiculo === null || $scope.ModeloVehiculo === 0) {
                $scope.MensajesError.push('Debe ingresar el modelo del vehículo');
                continuar = false;
            }
            if ($scope.ModeloTipoNaturalezaPropietario.Codigo === 502) {
                if ($scope.ModeloDigitoChequeoPropietario === undefined || $scope.ModeloDigitoChequeoPropietario === '' || $scope.ModeloDigitoChequeoPropietario === null || $scope.ModeloDigitoChequeoPropietario === 0) {
                    $scope.MensajesError.push('Debe ingresar el digito de chequeo del propietario');
                    continuar = false;
                }
                if ($scope.ModeloNumeroIdentificacionPropietario === undefined || $scope.ModeloNumeroIdentificacionPropietario === '' || $scope.ModeloNumeroIdentificacionPropietario === null || $scope.ModeloNumeroIdentificacionPropietario === 0) {
                    $scope.MensajesError.push('Debe ingresar la identificación del propietario');
                    continuar = false;
                }
                if ($scope.ModeloPropietario.RazonSocial === undefined || $scope.ModeloPropietario.RazonSocial === '' || $scope.ModeloPropietario.RazonSocial === null) {
                    $scope.MensajesError.push('Debe ingresar el nombre del propietario');
                    continuar = false;
                }
            } else {
                if ($scope.ModeloNumeroIdentificacionPropietario === undefined || $scope.ModeloNumeroIdentificacionPropietario === '' || $scope.ModeloNumeroIdentificacionPropietario === null || $scope.ModeloNumeroIdentificacionPropietario === 0) {
                    $scope.MensajesError.push('Debe ingresar la identificación del propietario');
                    continuar = false;
                }
                if ($scope.ModeloPropietario.Nombre === undefined || $scope.ModeloPropietario.Nombre === '' || $scope.ModeloPropietario.Nombre === null || $scope.ModeloPropietario.PrimeroApellido === undefined || $scope.ModeloPropietario.PrimeroApellido === '' || $scope.ModeloPropietario.PrimeroApellido === null) {
                    $scope.MensajesError.push('Debe ingresar el nombre y el primer apellido del propietario');
                    continuar = false;
                }
            }
            if ($scope.ModeloTipoNaturalezaTenedor.Codigo === 502) {
                if ($scope.ModeloDigitoChequeoTenedor === undefined || $scope.ModeloDigitoChequeoTenedor === '' || $scope.ModeloDigitoChequeoTenedor === null || $scope.ModeloDigitoChequeoTenedor === 0) {
                    $scope.MensajesError.push('Debe ingresar el digito de chequeo del tenedor');
                    continuar = false;
                }
                if ($scope.ModeloNumeroIdentificacionTenedor === undefined || $scope.ModeloNumeroIdentificacionTenedor === '' || $scope.ModeloNumeroIdentificacionTenedor === null || $scope.ModeloNumeroIdentificacionTenedor === 0) {
                    $scope.MensajesError.push('Debe ingresar la identificación del tenedor');
                    continuar = false;
                }
                if ($scope.ModeloTenedor.RazonSocial === undefined || $scope.ModeloTenedor.RazonSocial === '' || $scope.ModeloTenedor.RazonSocial === null) {
                    $scope.MensajesError.push('Debe ingresar el nombre del tenedor');
                    continuar = false;
                }
            } else {
                if ($scope.ModeloNumeroIdentificacionTenedor === undefined || $scope.ModeloNumeroIdentificacionTenedor === '' || $scope.ModeloNumeroIdentificacionTenedor === null || $scope.ModeloNumeroIdentificacionTenedor === 0) {
                    $scope.MensajesError.push('Debe ingresar la identificación del tenedor');
                    continuar = false;
                }
                if ($scope.ModeloTenedor.Nombre === undefined || $scope.ModeloTenedor.Nombre === '' || $scope.ModeloTenedor.Nombre === null || $scope.ModeloTenedor.PrimeroApellido === undefined || $scope.ModeloTenedor.PrimeroApellido === '' || $scope.ModeloTenedor.PrimeroApellido === null) {
                    $scope.MensajesError.push('Debe ingresar el nombre y el primer apellido del tenedor');
                    continuar = false;
                }
            }
            if ($scope.ModeloTipoNaturalezaConductor.Codigo === 502) {
                if ($scope.ModeloDigitoChequeoConductor === undefined || $scope.ModeloDigitoChequeoConductor === '' || $scope.ModeloDigitoChequeoConductor === null || $scope.ModeloDigitoChequeoConductor === 0) {
                    $scope.MensajesError.push('Debe ingresar el digito de chequeo del conductor');
                    continuar = false;
                }
                if ($scope.ModeloNumeroIdentificacionConductor === undefined || $scope.ModeloNumeroIdentificacionConductor === '' || $scope.ModeloNumeroIdentificacionConductor === null || $scope.ModeloNumeroIdentificacionConductor === 0) {
                    $scope.MensajesError.push('Debe ingresar la identificación del conductor');
                    continuar = false;
                }
                if ($scope.ModeloConductor.RazonSocial === undefined || $scope.ModeloConductor.RazonSocial === '' || $scope.ModeloConductor.RazonSocial === null) {
                    $scope.MensajesError.push('Debe ingresar el nombre del conductor');
                    continuar = false;
                }
            } else {
                if ($scope.ModeloNumeroIdentificacionTenedor === undefined || $scope.ModeloNumeroIdentificacionTenedor === '' || $scope.ModeloNumeroIdentificacionTenedor === null || $scope.ModeloNumeroIdentificacionTenedor === 0) {
                    $scope.MensajesError.push('Debe ingresar la identificación del conductor');
                    continuar = false;
                }
                if ($scope.ModeloConductor.Nombre === undefined || $scope.ModeloConductor.Nombre === '' || $scope.ModeloConductor.Nombre === null || $scope.ModeloConductor.PrimeroApellido === undefined || $scope.ModeloConductor.PrimeroApellido === '' || $scope.ModeloConductor.PrimeroApellido === null) {
                    $scope.MensajesError.push('Debe ingresar el nombre y el primer apellido del conductor');
                    continuar = false;
                }
            } if ($scope.ModeloTipoNaturalezaSemirremolque.Codigo === 502) {
                if ($scope.ModeloDigitoChequeoSemirremolque === undefined || $scope.ModeloDigitoChequeoSemirremolque === '' || $scope.ModeloDigitoChequeoSemirremolque === null || $scope.ModeloDigitoChequeoSemirremolque === 0) {
                    $scope.MensajesError.push('Debe ingresar el digito de chequeo del propietario del semirremolque');
                    continuar = false;
                }
            }

            //Documentos
            $scope.DetalleDocumentos = []
            for (var i = 0; i < $scope.ListadoTiposDocumentos.length; i++) {
                var Cont = 0
                var documento = $scope.ListadoTiposDocumentos[i]

                if (documento.EOESCodigo == 1) {
                    if (documento.AplicaReferencia == 2) {
                        if (documento.Referencia === undefined) {
                            $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaEmisor == 2) {
                        if (documento.Emisor === undefined) {
                            $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaEmision === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Nombre + ')');
                            continuar = false;
                            var f = new Date();
                            if (documento.FechaEmision > f) {
                                $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaVence === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Nombre + ')');
                            continuar = false;
                        } else {
                            var f = new Date();
                            if (documento.FechaVence < f) {
                                $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.CodigoDocumentos == 1 || documento.Referencia != undefined || documento.Emisor != undefined || documento.FechaEmision != undefined || documento.FechaVence != undefined) {
                        $scope.DetalleDocumentos.push(
                            {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                EOESCodigo: documento.EOESCodigo,
                                TDESCodigo: documento.Codigo,
                                Referencia: documento.Referencia,
                                Emisor: documento.Emisor,
                                FechaEmision: documento.FechaEmision,
                                FechaVence: documento.FechaVence,
                            }
                        )
                    }
                }
                if (documento.EOESCodigo == 2) {
                    if (documento.AplicaReferencia == 2) {
                        if (documento.Referencia === undefined) {
                            $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaEmisor == 2) {
                        if (documento.Emisor === undefined) {
                            $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaEmision === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Nombre + ')');
                            continuar = false;
                            var f = new Date();
                            if (documento.FechaEmision > f) {
                                $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaVence === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Nombre + ')');
                            continuar = false;
                        } else {
                            var f = new Date();
                            if (documento.FechaVence < f) {
                                $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.CodigoDocumentos == 1 || documento.Referencia != undefined || documento.Emisor != undefined || documento.FechaEmision != undefined || documento.FechaVence != undefined) {
                        $scope.DetalleDocumentos.push(
                            {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                EOESCodigo: documento.EOESCodigo,
                                TDESCodigo: documento.Codigo,
                                Referencia: documento.Referencia,
                                Emisor: documento.Emisor,
                                FechaEmision: documento.FechaEmision,
                                FechaVence: documento.FechaVence,
                            }
                        )
                    }
                }
                if (documento.EOESCodigo == 3) {
                    if (documento.AplicaReferencia == 2) {
                        if (documento.Referencia === undefined) {
                            $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaEmisor == 2) {
                        if (documento.Emisor === undefined) {
                            $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaEmision === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Nombre + ')');
                            continuar = false;
                            var f = new Date();
                            if (documento.FechaEmision > f) {
                                $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaVence === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Nombre + ')');
                            continuar = false;
                        } else {
                            var f = new Date();
                            if (documento.FechaVence < f) {
                                $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.CodigoDocumentos == 1 || documento.Referencia != undefined || documento.Emisor != undefined || documento.FechaEmision != undefined || documento.FechaVence != undefined) {
                        $scope.DetalleDocumentos.push(
                            {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                EOESCodigo: documento.EOESCodigo,
                                TDESCodigo: documento.Codigo,
                                Referencia: documento.Referencia,
                                Emisor: documento.Emisor,
                                FechaEmision: documento.FechaEmision,
                                FechaVence: documento.FechaVence,
                            }
                        )
                    }
                }
                if (documento.EOESCodigo == 4) {
                    if (documento.AplicaReferencia == 2) {
                        if (documento.Referencia === undefined) {
                            $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaEmisor == 2) {
                        if (documento.Emisor === undefined) {
                            $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaEmision === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Nombre + ')');
                            continuar = false;
                            var f = new Date();
                            if (documento.FechaEmision > f) {
                                $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaVence === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Nombre + ')');
                            continuar = false;
                        } else {
                            var f = new Date();
                            if (documento.FechaVence < f) {
                                $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.CodigoDocumentos == 1 || documento.Referencia != undefined || documento.Emisor != undefined || documento.FechaEmision != undefined || documento.FechaVence != undefined) {
                        $scope.DetalleDocumentos.push(
                            {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                EOESCodigo: documento.EOESCodigo,
                                TDESCodigo: documento.Codigo,
                                Referencia: documento.Referencia,
                                Emisor: documento.Emisor,
                                FechaEmision: documento.FechaEmision,
                                FechaVence: documento.FechaVence,
                            }
                        )
                    }
                }
                if (documento.EOESCodigo == 5) {
                    if (documento.AplicaReferencia == 2) {
                        if (documento.Referencia === undefined) {
                            $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaEmisor == 2) {
                        if (documento.Emisor === undefined) {
                            $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaEmision === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Nombre + ')');
                            continuar = false;
                            var f = new Date();
                            if (documento.FechaEmision > f) {
                                $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaVence === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Nombre + ')');
                            continuar = false;
                        } else {
                            var f = new Date();
                            if (documento.FechaVence < f) {
                                $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.CodigoDocumentos == 1 || documento.Referencia != undefined || documento.Emisor != undefined || documento.FechaEmision != undefined || documento.FechaVence != undefined) {
                        $scope.DetalleDocumentos.push(
                            {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                EOESCodigo: documento.EOESCodigo,
                                TDESCodigo: documento.Codigo,
                                Referencia: documento.Referencia,
                                Emisor: documento.Emisor,
                                FechaEmision: documento.FechaEmision,
                                FechaVence: documento.FechaVence,
                            }
                        )
                    }
                }
                if (documento.EOESCodigo == 6) {
                    if (documento.AplicaReferencia == 2) {
                        if (documento.Referencia === undefined) {
                            $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaEmisor == 2) {
                        if (documento.Emisor === undefined) {
                            $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaEmision === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Nombre + ')');
                            continuar = false;
                            var f = new Date();
                            if (documento.FechaEmision > f) {
                                $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaVence === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Nombre + ')');
                            continuar = false;
                        } else {
                            var f = new Date();
                            if (documento.FechaVence < f) {
                                $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.CodigoDocumentos == 1 || documento.Referencia != undefined || documento.Emisor != undefined || documento.FechaEmision != undefined || documento.FechaVence != undefined) {
                        $scope.DetalleDocumentos.push(
                            {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                EOESCodigo: documento.EOESCodigo,
                                TDESCodigo: documento.Codigo,
                                Referencia: documento.Referencia,
                                Emisor: documento.Emisor,
                                FechaEmision: documento.FechaEmision,
                                FechaVence: documento.FechaVence,
                            }
                        )
                    }
                }
                if (documento.EOESCodigo == 7) {
                    if (documento.AplicaReferencia == 2) {
                        if (documento.Referencia === undefined) {
                            $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaEmisor == 2) {
                        if (documento.Emisor === undefined) {
                            $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Nombre + ')');
                            continuar = false;
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaEmision === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Nombre + ')');
                            continuar = false;
                            var f = new Date();
                            if (documento.FechaEmision > f) {
                                $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.AplicaFechaEmision == 2) {
                        if (documento.FechaVence === undefined) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Nombre + ')');
                            continuar = false;
                        } else {
                            var f = new Date();
                            if (documento.FechaVence < f) {
                                $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                    }
                    if (documento.CodigoDocumentos == 1 || documento.Referencia != undefined || documento.Emisor != undefined || documento.FechaEmision != undefined || documento.FechaVence != undefined) {
                        $scope.DetalleDocumentos.push(
                            {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                EOESCodigo: documento.EOESCodigo,
                                TDESCodigo: documento.Codigo,
                                Referencia: documento.Referencia,
                                Emisor: documento.Emisor,
                                FechaEmision: documento.FechaEmision,
                                FechaVence: documento.FechaVence,
                            }
                        )
                    }
                }
            }
            return continuar;
        }


        /*---------------------------------------------------------------------------------------Funciones Guardar Documentos------------------------------------------------------------------------------------------------*/
        $scope.ElimiarDocumentoReemplazado = function (Eliminar) {
            $scope.EliminarDocumento = Eliminar.CodigoDocumentos
            $scope.LimpiarBotones = Eliminar.Codigo
        }

        $scope.DatosDocumentos = function (item) {
            $scope.CodigoDocumento = item.CodigoDocumentos;
            $scope.EOESCodigo = item.EOESCodigo;
            $scope.TDESCodigo = item.Codigo;
            $scope.Referencia = item.Referencia;
            $scope.Emisor = item.Emisor;
            $scope.FechaEmision = item.FechaEmision;
            $scope.FechaVence = item.FechaVence;
            $scope.NombreDocumento = item.Nombre;
        }

        $scope.ValidarReemplazo = function (item) {
            $scope.valor = item;
            closeModal('modalConfirmacionReemplazarDocumento');

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.EliminarDocumento,
                ENESNumero: $scope.ENESNumero,
                TDESCodigo: $scope.LimpiarBotones
            };
            EstudioSeguridadDocumentosFactory.EliminarDocumentoDefinitivo(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            $scope.reader = new FileReader();
            $scope.reader.onload = $scope.ReemplazarDocumento;
            $scope.reader.readAsDataURL($scope.element.files[0]);
        };

        $scope.CargarDocumento = function (element) {

            var continuar = true;
            $scope.TipoImagen = element
            blockUI.start();
            $scope.NombreDocumento = element.files[0].name
            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato jpeg, png  o pdf", false);
                continuar = false;
            }
            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png' && element.files[0].type !== 'application/pdf') {
                    ShowError("Solo se pueden cargar archivos en formato jpeg, png  o pdf", false);
                    continuar = false;
                }
                if (element.files[0].type == 'application/pdf') {

                    if (element.files[0].size >= 4000000) { //4 MB
                        ShowError("El documento " + element.files[0].name + " supera los 4 MB, intente con otro documento", false);
                        continuar = false;
                    }
                }
            }
            if (continuar == true) {
                if ($scope.CodigoDocumento != undefined && $scope.CodigoDocumento != '' && $scope.CodigoDocumento != null) {
                    showModal('modalConfirmacionReemplazarDocumento');
                    document.getElementById('mensajearchivoreemplazar').innerHTML = '¿Está seguro de reemplazar el documento que se encuentra guardado?'
                    if ($scope.valor == 1) {
                        $scope.reader = new FileReader();
                        $scope.reader.onload = $scope.ReemplazarDocumento;
                        $scope.reader.readAsDataURL(element.files[0]);
                    } else {
                        $scope.element = element;
                    }
                } else {
                    var reader = new FileReader();
                    reader.onload = $scope.AsignarDocumento;
                    reader.readAsDataURL(element.files[0]);
                }
            }
            blockUI.stop();
        }

        $scope.ReemplazarDocumento = function (e) {
            $scope.$apply(function () {
                $scope.ConvertirDocumento = null
                var string = ''
                var TipoImagen = ''
                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        AsignarDocumentoListado()
                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        AsignarDocumentoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.ConvertirDocumento = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoImagen = TipoImagen
                    AsignarDocumentoListado()
                }
            });
        }

        $scope.AsignarDocumento = function (e) {
            $scope.$apply(function () {
                $scope.ConvertirDocumento = null
                var string = ''
                var TipoImagen = ''
                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        AsignarDocumentoListado()
                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        AsignarDocumentoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.ConvertirDocumento = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoImagen = TipoImagen
                    AsignarDocumentoListado()
                }
            });
        }

        function AsignarDocumentoListado() {

            var documento = {
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoDocumento,
                ENESNumero: $scope.Numero,
                EOESCodigo: $scope.EOESCodigo,
                TDESCodigo: $scope.TDESCodigo,
                Referencia: $scope.Referencia,
                Emisor: $scope.Emisor,
                FechaEmision: $scope.FechaEmision,
                FechaVence: $scope.FechaVence,
                Documento: $scope.ConvertirDocumento,
                NombreDocumento: $scope.NombreDocumento,
                ExtensionDocumento: $scope.TipoImagen
            }
            // Guardar documento temporal
            EstudioSeguridadDocumentosFactory.InsertarTemporal(documento).
                then(function (response) {
                    if (response.data.Datos > 0) {
                        // Se guardó correctamente el documento
                        $scope.ListadoTiposDocumentos.forEach(function (item) {
                            if (item.Codigo == $scope.TDESCodigo) {
                                item.CodigoDocumentos = response.data.Datos;
                                item.temp = true;
                            }
                        })
                    } else {
                        ShowError(response.data.MensajeError);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.AsignarDocumentoListadoDetalles = function (item) {
            if (item.Referencia !== undefined || item.Referencia !== undefined || item.FechaEmision !== undefined || item.FechaVence !== undefined) {
                var documento = {
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.CodigoDocumento,
                    ENESNumero: $scope.Numero,
                    EOESCodigo: item.EOESCodigo,
                    TDESCodigo: item.Codigo,
                    Referencia: item.Referencia,
                    Emisor: item.Emisor,
                    FechaEmision: item.FechaEmision,
                    FechaVence: item.FechaVence
                };
                // Guardar documento temporal
                EstudioSeguridadDocumentosFactory.InsertarTemporal(documento).
                    then(function (response) {
                        if (response.data.Datos > 0) {
                            response.data.Datos;
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        $scope.ElimiarDocumento = function (EliminarDocumento) {
            $scope.EliminarDocumento = EliminarDocumento.CodigoDocumentos;
            $scope.LimpiarBotones = EliminarDocumento.Codigo;
            showModal('modalConfirmacionEliminarDocumento');
            document.getElementById('mensajearchivo').innerHTML = '¿Está seguro de eliminar el documento?';
        };

        $scope.BorrarDocumento = function () {
            closeModal('modalConfirmacionEliminarDocumento');
            var entidad = {
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.EliminarDocumento,
                ENESNumero: $scope.ENESNumero,
                TDESCodigo: $scope.LimpiarBotones
            };

            EstudioSeguridadDocumentosFactory.EliminarDocumento(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTiposDocumentos.forEach(function (item) {
                            if (item.Codigo === $scope.LimpiarBotones) {
                                item.CodigoDocumentos = 0;
                            }
                        });
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            EstudioSeguridadDocumentosFactory.EliminarDocumentoDefinitivo(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos;
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };

        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarDocumento = function (item) {
            if (item.temp) {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Estudio_Seguridad&Codigo=' + item.Codigo + '&EMPR_Codigo=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Temp=1' + '&Usuariotemp=' + $scope.Sesion.UsuarioAutenticado.Codigo);
            }
            else {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Estudio_Seguridad&Codigo=' + item.Codigo + '&EMPR_Codigo=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Temp=0' + '&ENES_Numero=' + $scope.ModeloNumero);
            }
        };

        $scope.ConfirmarAutorizar = function () {
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ModeloNumero,
                NumeroAutorizacion: $scope.ModeloNumeroAutorizacion,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            };
            EstudioSeguridadFactory.Autorizar(filtros).
                then(function (response) {
                    closeModal('modalConfirmacionGuardarAutorizacion');
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.ProcesoExitoso === true) {
                            location.href = '#!ConsultarEstudioSeguridad/' + $scope.ModeloNumeroDocumento + '/1';
                        } else {
                            ShowError('Se presentó un error al autorizar el estudio seguridad');
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.Rechazar = function () {
            showModal('modalRechazar', 1);
        };

        $scope.ConfirmarRechazo = function () {
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ModeloNumero,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            };
            EstudioSeguridadFactory.Rechazar(filtros).
                then(function (response) {
                    closeModal('modalRechazar');
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.ProcesoExitoso === true) {
                            location.href = '#!ConsultarEstudioSeguridad/' + $scope.ModeloNumeroDocumento + '/0';
                        } else {
                            ShowError('Se presentó un error al rechazadar el estudio seguridad');
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarEstudioSeguridad/' + $scope.ModeloNumeroDocumento;
        };

        $scope.VolverMasterAprobar = function () {
            document.location.href = '#!AprobarEstudioSeguridad/' + $scope.ModeloNumeroDocumento;
        };

        //inicializacion de divisiones
        if ($scope.Aprobacion > 0) {
            $('#Vehiculo').hide(); $('#Propietario').hide(); $('#Tenedor').hide(); $('#Conductor').hide(); $('#Cliente').hide(); $('#Referencias').hide(); $('#Autorizaciones').show(); $('#Semirremolque').hide();
        } else {
            $('#Vehiculo').show(); $('#Propietario').hide(); $('#Tenedor').hide(); $('#Conductor').hide(); $('#Cliente').hide(); $('#Referencias').hide(); $('#Autorizaciones').hide(); $('#Semirremolque').hide();
        }

        $scope.MostrarVehiculo = function () {
            $('#Vehiculo').show();
            $('#Propietario').hide();
            $('#Tenedor').hide();
            $('#Conductor').hide();
            $('#Semirremolque').hide();
            $('#Cliente').hide();
            $('#Referencias').hide();
            $('#Autorizaciones').hide();
        };
        $scope.MostrarPropietario = function () {
            $('#Vehiculo').hide();
            $('#Propietario').show();
            $('#Tenedor').hide();
            $('#Conductor').hide();
            $('#Semirremolque').hide();
            $('#Cliente').hide();
            $('#Referencias').hide();
            $('#Autorizaciones').hide();
        };
        $scope.MostrarTenedor = function () {
            $('#Vehiculo').hide();
            $('#Propietario').hide();
            $('#Tenedor').show();
            $('#Conductor').hide();
            $('#Semirremolque').hide();
            $('#Cliente').hide();
            $('#Referencias').hide();
            $('#Autorizaciones').hide();
        };
        $scope.MostrarConductor = function () {
            $('#Vehiculo').hide();
            $('#Propietario').hide();
            $('#Tenedor').hide();
            $('#Conductor').show();
            $('#Semirremolque').hide();
            $('#Cliente').hide();
            $('#Referencias').hide();
            $('#Autorizaciones').hide();
        };
        $scope.MostrarSemirremolque = function () {
            $('#Vehiculo').hide();
            $('#Propietario').hide();
            $('#Tenedor').hide();
            $('#Conductor').hide();
            $('#Semirremolque').show();
            $('#Cliente').hide();
            $('#Referencias').hide();
            $('#Autorizaciones').hide();
        };
        $scope.MostrarCliente = function () {
            $('#Vehiculo').hide();
            $('#Propietario').hide();
            $('#Tenedor').hide();
            $('#Conductor').hide();
            $('#Semirremolque').hide();
            $('#Cliente').show();
            $('#Referencias').hide();
            $('#Autorizaciones').hide();
        };
        $scope.MostrarReferencias = function () {
            $('#Vehiculo').hide();
            $('#Propietario').hide();
            $('#Tenedor').hide();
            $('#Conductor').hide();
            $('#Semirremolque').hide();
            $('#Cliente').hide();
            $('#Referencias').show();
            $('#Autorizaciones').hide();
        };
        $scope.MostrarAutorizaciones = function () {
            $('#Vehiculo').hide();
            $('#Propietario').hide();
            $('#Tenedor').hide();
            $('#Conductor').hide();
            $('#Semirremolque').hide();
            $('#Cliente').hide();
            $('#Referencias').hide();
            $('#Autorizaciones').show();
        };

        if ($scope.Numero > 0) {
            $scope.Titulo = 'CONSULTAR ESTUDIO SEGURIDAD';
            Obtener();
        }

        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskDireccion = function (option) {
            try { $scope.ModeloDireccionPropietario = MascaraDireccion($scope.ModeloDireccionPropietario) } catch (e) { }
            try { $scope.ModeloDireccionSemirremolque = MascaraDireccion($scope.ModeloDireccionSemirremolque) } catch (e) { }
            try { $scope.ModeloDireccionTenedor = MascaraDireccion($scope.ModeloDireccionTenedor) } catch (e) { }
            try { $scope.ModeloDireccionConductor = MascaraDireccion($scope.ModeloDireccionConductor) } catch (e) { }
        };
        $scope.MaskTelefono = function (option) {
            try { $scope.ModeloTelefonoConductor = MascaraTelefono($scope.ModeloTelefonoConductor) } catch (e) { }
            try { $scope.ModeloTelefonoTenedor = MascaraTelefono($scope.ModeloTelefonoTenedor) } catch (e) { }
            try { $scope.ModeloTelefonoSemirremolque = MascaraTelefono($scope.ModeloTelefonoSemirremolque) } catch (e) { }
            try { $scope.ModeloTelefonoPropietario = MascaraTelefono($scope.ModeloTelefonoPropietario) } catch (e) { }
        };
        $scope.MaskCelular = function (option) {
            try { $scope.ModeloCelularPropietario = MascaraCelular($scope.ModeloCelularPropietario) } catch (e) { }
            try { $scope.ModeloCelularSemirremolque = MascaraCelular($scope.ModeloCelularSemirremolque) } catch (e) { }
            try { $scope.ModeloCelularTenedor = MascaraCelular($scope.ModeloCelularTenedor) } catch (e) { }
            try { $scope.ModeloCelularConductor = MascaraCelular($scope.ModeloCelularConductor) } catch (e) { }
        };
        $scope.MaskPlaca = function () {
            try { $scope.ModeloPlacaVehiculo = MascaraPlaca($scope.ModeloPlacaVehiculo) } catch (e) { }
        };

        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }
        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }

    }]); 