﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="UnidadMedidaController"/>
''' </summary>
<Authorize>
Public Class UnidadMedidaController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As UnidadMedida) As Respuesta(Of IEnumerable(Of UnidadMedida))
        Return New LogicaUnidadMedida(CapaPersistenciaUnidadMedida).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As UnidadMedida) As Respuesta(Of UnidadMedida)
        Return New LogicaUnidadMedida(CapaPersistenciaUnidadMedida).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As UnidadMedida) As Respuesta(Of Long)
        Return New LogicaUnidadMedida(CapaPersistenciaUnidadMedida).Guardar(entidad)
    End Function
End Class
