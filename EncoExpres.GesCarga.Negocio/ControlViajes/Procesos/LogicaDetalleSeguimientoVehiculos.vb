﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Entidades.Utilitarios
Imports EncoExpres.GesCarga.Fachada.ControlViajes
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace ControlViajes
    Public NotInheritable Class LogicaDetalleSeguimientoVehiculos
        Inherits LogicaBase(Of DetalleSeguimientoVehiculos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of DetalleSeguimientoVehiculos)

        Public Sub New(capaPersistenciaDetalleSeguimientoVehiculos As IPersistenciaBase(Of DetalleSeguimientoVehiculos))
            _persistencia = capaPersistenciaDetalleSeguimientoVehiculos
        End Sub

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos de la notificacion de correo.
        ''' </summary>

        Public Overrides Function Consultar(filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As DetalleSeguimientoVehiculos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function
        Public Function Anular(entidad As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function CambiarRutaSeguimiento(entidad As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim cambio As Boolean = False

            cambio = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).CambiarRutaSeguimiento(entidad)

            If Not cambio Then
                Return New Respuesta(Of Boolean)("Fallo al Cambiar Ruta")
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Overrides Function Obtener(filtro As DetalleSeguimientoVehiculos) As Respuesta(Of DetalleSeguimientoVehiculos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of DetalleSeguimientoVehiculos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of DetalleSeguimientoVehiculos)(consulta)
        End Function
        Public Function ConsultarRemesas(filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
            Dim consulta = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).ConsultarRemesas(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))(consulta)
        End Function
        Public Function ConsultarNovedadesSitiosReporte(filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
            Dim consulta = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).ConsultarNovedadesSitiosReporte(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))(consulta)
        End Function
        Public Function ConsultarPuestoControlRutas(filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
            Dim consulta = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).ConsultarPuestoControlRutas(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))(consulta)
        End Function
        Public Function ConsultarMasterRemesa(filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
            Dim consulta = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).ConsultarMasterRemesa(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))(consulta)
        End Function
        Public Function ConsultarPuntosControl(filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
            Dim consulta = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).ConsultarPuntosControl(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))(consulta)
        End Function
        Private Function DatosRequeridos(entidad As DetalleSeguimientoVehiculos) As Respuesta(Of DetalleSeguimientoVehiculos)
            Throw New NotImplementedException()
        End Function
        Public Function EnviarCorreoCliente(entidad As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Envio As Boolean = False

            Envio = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).EnviarCorreoCliente(entidad)

            If Not Envio Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function EnviarCorreoSeguimiento(entidad As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Envio As Boolean = False

            Envio = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).EnviarCorreoSeguimiento(entidad)

            If Not Envio Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function EnviarCorreoAvanceVehiculosCliente(entidad As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Envio As Boolean = False

            Envio = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).EnviarCorreoAvanceVehiculosCliente(entidad)

            If Not Envio Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function EnviarDocumentosConductor(entidad As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Envio As Boolean = False

            Envio = CType(_persistencia, PersistenciaDetalleSeguimientoVehiculos).EnviarDocumentosConductor(entidad)

            If Not Envio Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

#Region "Envio Correos"
        Private Function PrepararCorreo(entidad As DetalleSeguimientoVehiculos) As BandejaSalidaCorreos
            Dim TextoCorreo As New BandejaSalidaCorreos With {
                .CodigoEmpresa = entidad.CodigoEmpresa,
                .TipoDocumento = 180,'New ValorCatalogo With {.Codigo = entidad.TipoDocumento},
                .NumeroDocumeto = entidad.Numero,
                .CuentaCorreoDe = "supportlogistic@soscontingencias.com.co",
                .CuentaCorreoCopia = String.Empty,
                .CodigoUsuarioCrea = entidad.UsuarioCrea.Codigo,
                .Estado = 0
            }
            Dim Con = 0
            Dim CantidadCorreos = entidad.EventoCorreo.ListaDistrubicionCorreos.Count

            For Each CuentaCorreos In entidad.EventoCorreo.ListaDistrubicionCorreos
                If Con < (CantidadCorreos - 1) Then
                    If CuentaCorreos.Email <> "" Then
                        TextoCorreo.CuentaCorreoPara += CuentaCorreos.Email + ","
                        Con = Con + 1
                    Else
                        Con = Con + 1
                        TextoCorreo.CuentaCorreoPara += CuentaCorreos.Email
                    End If
                Else
                    TextoCorreo.CuentaCorreoPara += CuentaCorreos.Email
                End If
            Next
            TextoCorreo.Contenido += vbNewLine
            TextoCorreo.Contenido += vbNewLine
            TextoCorreo.Contenido = Replace(entidad.EventoCorreo.EncabezadoCorreo, EventoCorreos.CAMPO_TEXTO_CORREO_PLACA, entidad.Vehiculo.Placa)
            TextoCorreo.Contenido += vbNewLine
            TextoCorreo.Contenido += vbNewLine
            TextoCorreo.Contenido += entidad.EventoCorreo.MensajeCorreo
            TextoCorreo.Contenido += vbNewLine
            TextoCorreo.Contenido += vbNewLine

            TextoCorreo.AsuntoCorreo = Replace(Replace(entidad.EventoCorreo.AsuntoCorreo, EventoCorreos.CAMPO_TEXTO_CORREO_PLACA, entidad.Vehiculo.Placa), EventoCorreos.CAMPO_TEXTO_CORREO_FECHA, entidad.Fecha)
            TextoCorreo.Contenido += vbNewLine
            TextoCorreo.Contenido += vbNewLine
            TextoCorreo.Contenido += entidad.EventoCorreo.MensajeFirma
            TextoCorreo.Contenido += vbNewLine
            TextoCorreo.Contenido += vbNewLine

            Return TextoCorreo
        End Function

        'Public Function EncolarCorreo(Seguimiento As DetalleSeguimientoVehiculos) As Respuesta(Of Long)
        '    Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
        '    Dim Codigo As Long = 0
        '    Try

        '        Dim bandejaSalida As New LogicaBandejaSalidaCorreos(_persistenciaBandejaSalidaCorreos)
        '        Dim correo = PrepararCorreo(Seguimiento)

        '        Codigo = _persistenciaBandejaSalidaCorreos.Insertar(correo)

        '    Catch ex As Exception
        '        Debug.WriteLine(ex.Message.ToString())
        '        Codigo = 0
        '    End Try


        '    If Codigo.Equals(0) Then
        '        Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
        '    End If
        '    Return New Respuesta(Of Long)(Codigo) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(Seguimiento.Codigo > 0, String.Format(mensajeGuardo, Codigo, Recursos.OperacionInserto), String.Format(mensajeGuardo, Codigo, Recursos.OperacionModifico))}

        'End Function

#End Region

    End Class

End Namespace