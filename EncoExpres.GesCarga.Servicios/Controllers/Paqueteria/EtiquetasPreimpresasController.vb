﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="PrecintosController"/>
''' </summary>
<Authorize>
Public Class EtiquetasPreimpresasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EtiquetaPreimpresa) As Respuesta(Of IEnumerable(Of EtiquetaPreimpresa))
        Return New LogicaEtiquetasPreimpresas(CapaPersistenciaEtiquetasPreimpresas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EtiquetaPreimpresa) As Respuesta(Of EtiquetaPreimpresa)
        Return New LogicaEtiquetasPreimpresas(CapaPersistenciaEtiquetasPreimpresas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EtiquetaPreimpresa) As Respuesta(Of Long)
        Return New LogicaEtiquetasPreimpresas(CapaPersistenciaEtiquetasPreimpresas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("GuardarPrecintos")>
    Public Function GuardarPrecintos(ByVal entidad As EtiquetaPreimpresa) As Respuesta(Of EtiquetaPreimpresa)
        Return New LogicaEtiquetasPreimpresas(CapaPersistenciaEtiquetasPreimpresas).GuardarPrecintos(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EtiquetaPreimpresa) As Respuesta(Of Boolean)
        Return New LogicaEtiquetasPreimpresas(CapaPersistenciaEtiquetasPreimpresas).Anular(entidad)
    End Function
    <ActionName("ConsultarUnicoPrecinto")>
    Public Function ConsultarUnicoPrecinto(ByVal filtro As DetalleEtiquetaPreimpresa) As Respuesta(Of IEnumerable(Of DetalleEtiquetaPreimpresa))
        Return New LogicaEtiquetasPreimpresas(CapaPersistenciaEtiquetasPreimpresas).ConsultarUnicoPrecinto(filtro)
    End Function

    <HttpPost>
    <ActionName("LiberarPrecinto")>
    Public Function LiberarPrecinto(ByVal entidad As EtiquetaPreimpresa) As Respuesta(Of Boolean)
        'Return New LogicaPrecintos(CapaPersistenciaPrecintos).LiberarPrecinto(entidad)
        Dim retorno As Respuesta(Of Boolean) = New LogicaEtiquetasPreimpresas(CapaPersistenciaEtiquetasPreimpresas).LiberarPrecinto(entidad)
        Return retorno
    End Function
    <HttpPost>
    <ActionName("ValidarPrecintoPlanillaPaqueteria")>
    Public Function ValidarPrecintoPlanillaPaqueteria(ByVal entidad As EtiquetaPreimpresa) As Respuesta(Of EtiquetaPreimpresa)
        Return New LogicaEtiquetasPreimpresas(CapaPersistenciaEtiquetasPreimpresas).ValidarPrecintoPlanillaPaqueteria(entidad)
    End Function


    <HttpPost>
    <ActionName("ActualizarResponsable")>
    Public Function ActualizarResponsable(ByVal entidad As EtiquetaPreimpresa) As Respuesta(Of Long)
        Return New LogicaEtiquetasPreimpresas(CapaPersistenciaEtiquetasPreimpresas).ActualizarResponsable(entidad)
    End Function
End Class
