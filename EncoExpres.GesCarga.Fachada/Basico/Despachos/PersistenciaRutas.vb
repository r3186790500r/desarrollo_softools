﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaRutas
        Implements IPersistenciaBase(Of Rutas)

        Public Function Consultar(filtro As Rutas) As IEnumerable(Of Rutas) Implements IPersistenciaBase(Of Rutas).Consultar
            Return New RepositorioRutas().Consultar(filtro)
        End Function
        Public Function ConsultarTrayectos(filtro As Rutas) As IEnumerable(Of TrayectoRuta)
            Return New RepositorioRutas().ConsultarTrayectos(filtro)
        End Function
        Public Function Insertar(entidad As Rutas) As Long Implements IPersistenciaBase(Of Rutas).Insertar
            Return New RepositorioRutas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Rutas) As Long Implements IPersistenciaBase(Of Rutas).Modificar
            Return New RepositorioRutas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Rutas) As Rutas Implements IPersistenciaBase(Of Rutas).Obtener
            Return New RepositorioRutas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Rutas) As Boolean
            Return New RepositorioRutas().Anular(entidad)
        End Function
        Public Function InsertarPuestosControl(entidad As Rutas) As Boolean
            Return New RepositorioRutas().InsertarPuestosControl(entidad)
        End Function
        Public Function InsertarLegalizacionGastosRuta(entidad As Rutas) As Boolean
            Return New RepositorioRutas().InsertarLegalizacionGastosRuta(entidad)
        End Function
        Public Function InsertarPeaje(entidad As Rutas) As Boolean
            Return New RepositorioRutas().InsertarPeaje(entidad)
        End Function
        Public Function GenerarPlanitilla(filtro As Rutas) As Rutas
            Return New RepositorioRutas().GenerarPlanitilla(filtro)
        End Function
        Public Function InsertarTrayectos(entidad As Rutas) As Boolean
            Return New RepositorioRutas().InsertarTrayectos(entidad)
        End Function
    End Class

End Namespace

