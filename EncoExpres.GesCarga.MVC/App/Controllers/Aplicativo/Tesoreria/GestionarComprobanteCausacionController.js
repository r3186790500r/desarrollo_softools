﻿EncoExpresApp.controller("GestionarComprobanteCausacionCtrl", ['$scope', '$routeParams', '$linq', 'blockUI', '$timeout', 'TercerosFactory', 'ValorCatalogosFactory', 'OficinasFactory', 'blockUIConfig',
    'DocumentoComprobantesFactory', 'CajasFactory', 'CuentaBancariasFactory', 'PlanUnicoCuentasFactory', 'VehiculosFactory', 'ConceptoContablesFactory', 'DetalleConceptoContablesFactory',
    'blockUIConfig', 'CierreContableDocumentosFactory', 'PlanillaDespachosFactory', 'LiquidacionesFactory', 'DocumentoCausacionesFactory',
    function ($scope, $routeParams, $linq, blockUI, $timeout, TercerosFactory, ValorCatalogosFactory, OficinasFactory, blockUIConfig,
        DocumentoComprobantesFactory, CajasFactory, CuentaBancariasFactory, PlanUnicoCuentasFactory, VehiculosFactory, ConceptoContablesFactory, DetalleConceptoContablesFactory,
        blockUIConfig, CierreContableDocumentosFactory, PlanillaDespachosFactory, LiquidacionesFactory, DocumentoCausacionesFactory) {
        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */
        $scope.Titulo = 'GESTIONAR COMPROBANTES EGRESO';
        $scope.MapaSitio = [{ Nombre: 'Tesorería' }, { Nombre: 'Documentos' }, { Nombre: 'Comprobantes Causación' }, { Nombre: 'Gestionar' }];

        //console.clear()
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_COMPROBANTE_EGRESOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/

        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 }
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');

        $scope.ListadoNaturalezasTercero = [];
        $scope.ListadoProveedores = [];
        $scope.ListadoTipoComprobantes = [];
        $scope.ListadoCuentasPUC = [];
        $scope.ListadoOficinas = [];
        $scope.ListadoDocumentosOrigen = [];
        $scope.ListadoCuentaBancariaOficinas = [];
        $scope.ListadoConceptoContable = [];
        $scope.ListadoConceptosEgreso = [];
        $scope.ListadoConceptosIngreso = [];
        $scope.ListadoVehiculos = [];
        $scope.ListaFormaPago = [];
        $scope.MensajesError = [];
        $scope.ListadoCuentaBancarias = [];
        $scope.ListadoMovimiento = [];
        $scope.ListadoTodaCajaOficinas = [];
        $scope.ListadoCajaOficinas = [];
        $scope.ListadoNumeroDocumentosOrigen = [];

        
        $scope.ActivarEfectivo = false
        $scope.ActivarCheque = false
        $scope.ActivarConsignacion = false
        $scope.PlacaValida = true;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Numero: 0,
            Fecha: new Date(),
            FechaConsignacion: new Date(),
            Debito: 0,
            Credito: 0,
            Diferencia: 0,
            OficinaCrea: $scope.Sesion.UsuarioAutenticado.Oficinas[0],
            TipoComprobante: '',
            Proveedor: ''
        }

        $scope.Modelo.FechaFactura = new Date();
        $scope.Modelo.FechaVencimientoFactura = new Date();

        $scope.Modal = {
            TerceroModal: { NumeroIdentificacion: 0 },
            TerceroModal: { NombreCompleto: '' }
        }

        /* Obtener parametros del enrutador*/
        if ($routeParams.Numero !== undefined) {
            $scope.Modelo.Numero = $routeParams.Numero;
            
        }
        else {
            $scope.Modelo.Numero = 0;
            Find();
        }

        function Find() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentos: { Codigo: 30 }
            };
            CierreContableDocumentosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoDocumentos = response.data.Datos;
                            $scope.ValidarCierre();
                        }
                    }
                }, function (response) {
                });
        }

        $scope.ValidarCierre = function () {
            if ($scope.ListadoTipoDocumentos.length > 0) {
                var anoFecha = $scope.Modelo.Fecha.getFullYear();
                var mesFecha = $scope.Modelo.Fecha.getMonth();
                for (var k = 0; k < $scope.ListadoTipoDocumentos.length; k++) {
                    var item = $scope.ListadoTipoDocumentos[k];
                    var mes = MascaraNumero(item.Mes.CampoAuxiliar2);
                    if (item.Ano === anoFecha && mes === (mesFecha + 1) && item.EstadoCierre.Codigo === 18002) {
                        ShowError('La fecha se encuentra en un mes y año con cierre contable');
                        $scope.ModeloFecha = '';
                        break;
                    }
                }
            }
        };

      

        //Listado Comprobantes:
        $scope.ListadoTipoComprobantes = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONCEPTO_CONTABLE }, Sync: true }).Datos;
        $scope.ListadoTipoComprobantes.splice(0, 1);
        $scope.Modelo.TipoComprobante = $scope.ListadoTipoComprobantes[0];
        

        
        

        //ListadoNaturalezasTerceros:
        $scope.ListadoNaturalezasTercero = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_NATURALEZA_TERCERO }, Sync: true }).Datos;


        $scope.AutocompleteProveedores = function (value) {
            if (value.length > 3) {
                $scope.ListadoProveedores = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR, Estado: 1, ValorAutocomplete: value, Sync: true }).Datos;


                return $scope.ListadoProveedores;
            }
        }
        $scope.AsignarDatosProveedor = function () {
            if ($scope.Modelo.Proveedor != undefined && $scope.Modelo.Proveedor != '') {
                $scope.Modelo.NaturalezaProveedor = $linq.Enumerable().From($scope.ListadoNaturalezasTercero).First('$.Codigo==' + $scope.Modelo.Proveedor.TipoNaturaleza.Codigo)
                $scope.Modelo.CiudadProveedor = $scope.Modelo.Proveedor.Ciudad.Nombre;
                $scope.Modelo.DireccionProveedor = $scope.Modelo.Proveedor.Direccion;
                $scope.Modelo.TelefonoProveedor = $scope.Modelo.Proveedor.Telefonos;
                $scope.Modelo.IdentificacionProveedor = $scope.Modelo.Proveedor.NumeroIdentificacion;
                $scope.Modelo.RegimenProveedor = '';
                $scope.Modelo.CelularProveedor = $scope.Modelo.Proveedor.Celular;
            }
        }

        $scope.CalcularTotal = function () {
          //  $scope.Modelo.ValorTotal = parseInt($scope.Modelo.Subtotal) - parseInt($scope.Modelo.ValorIva);
            var subtotal = $scope.Modelo.Subtotal != undefined ? parseInt($scope.Modelo.Subtotal.replace(/,/gi,'')) : 0;
            var valorIva = $scope.Modelo.ValorIva != undefined ? parseInt($scope.Modelo.ValorIva.replace(/,/gi, '')) : 0;            
            
            $scope.Modelo.ValorTotal = MascaraValores(subtotal + valorIva);            
        }
        /*Cargar ciudad oficina de la planilla*/
        $scope.ListadoOficinasPlanilla = [];
        OficinasFactory.Consultar({ CodigoCiudad: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinasPlanilla = response.data.Datos;
                    $scope.Oficina = $scope.ListadoOficinasPlanilla[$scope.ListadoOficinasPlanilla.length - 1];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el autocomplete de cuentas PUC*/
        PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCuentasPUC = response.data.Datos

                    if ($scope.CodigoCuentaPUC !== undefined && $scope.CodigoCuentaPUC !== '' && $scope.CodigoCuentaPUC !== null) {
                        $scope.Modal.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                    }
                }
                else {
                    $scope.ListadoCuentasPUC = [];
                }
            }, function (response) {
            });
        /*Cargar el combo de Oficinas*/
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoOficinas = response.data.Datos
                        if ($scope.CodigoOficinaDestino !== undefined && $scope.CodigoOficinaDestino !== '' && $scope.CodigoOficinaDestino !== null) {
                            $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficinaDestino)
                        }
                        else {
                            $scope.Modelo.OficinaDestino = $scope.ListadoOficinas[0];
                        }
                    }
                    else {
                        $scope.ListadoOficinas = [];
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Documenteos Origen*/
        //ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
        //    then(function (response) {
        //        if (response.data.ProcesoExitoso === true) {
        //            response.data.Datos.forEach(function (item) {
        //                if (item.CampoAuxiliar2.match(/EGR/)) {
        //                    $scope.ListadoDocumentosOrigen.push(item);
        //                }
        //            });
        //            $scope.ListadoDocumentosOrigen.push({ Codigo: 2600, Nombre: "Seleccione un documento origen" });

        //            if ($scope.CodigoDocumentoOrigen !== undefined && $scope.CodigoDocumentoOrigen !== '' && $scope.CodigoDocumentoOrigen !== null) {
        //                $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + $scope.CodigoDocumentoOrigen);
        //            }
        //            else {
        //                $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == 2600');
        //            }
        //        }
        //    }, function (response) {
        //        ShowError(response.statusText);
        //    });

        $scope.Modelo.DocumentoOrigen = {Codigo: 2603}
        /*Cargar el combo de Forma de pago*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MEDIO_PAGO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaFormaPago = response.data.Datos

                    if ($scope.CodigoFormaPago !== undefined && $scope.CodigoFormaPago !== '' && $scope.CodigoFormaPago !== null) {
                        $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo ==' + $scope.CodigoFormaPago);
                    }
                    else {
                        $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo ==' + CODIGO_MEDIO_PAGO_NO_APLICA);
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        CuentaBancariasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoCuentaBancaria: { Codigo: 403 }, Estado: 1, }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCuentaCambistas = response.data.Datos
                        if ($scope.CodigoCuentaBancaria !== undefined && $scope.CodigoCuentaBancaria !== null && $scope.CodigoCuentaBancaria !== '' && $scope.CodigoCuentaBancaria !== 0) {
                            $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaCambistas).First('$.Codigo == ' + $scope.CodigoCuentaBancaria);
                        }
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ListaCentrosCostos = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CENTROS_COSTOS_COMPROBANTE_EGRESO }, Sync: true }).Datos;

        $scope.Modelo.CentroCostos = $linq.Enumerable().From($scope.ListaCentrosCostos).First('$.Codigo == 21500');
        function CargarCuentasBancarias() {
            /*Cargar el combo Cuentas Bancarias por oficina*/
            CuentaBancariasFactory.CuentaBancariaOficinas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.OficinaDestino.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCuentaBancariaOficinas = [];
                        $scope.ListadoCuentaBancariaOficinas = response.data.Datos;

                        if ($scope.CodigoCuentaBancaria !== undefined && $scope.CodigoCuentaBancaria !== null && $scope.CodigoCuentaBancaria !== '' && $scope.CodigoCuentaBancaria !== 0) {
                            $scope.ListadoCuentaBancariaOficinas.forEach(function (item) {
                                if (item.CuentaBancaria.Codigo > 0) {
                                    if (item.Oficina.Codigo == $scope.CodigoOficinaDestino) {
                                        $scope.ListadoCuentaBancarias.push(item);
                                    }
                                }
                            });

                            $scope.Modelo.CuentaBancariaCheque = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + $scope.CodigoCuentaBancaria);
                            $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + $scope.CodigoCuentaBancaria);
                        }
                        else {
                            $scope.ListadoCuentaBancarias = [];
                            $scope.ListadoCuentaBancariaOficinas.forEach(function (item) {
                                if (item.Oficina.Codigo == $scope.Modelo.OficinaDestino.Codigo) {
                                    $scope.ListadoCuentaBancarias.push(item);
                                }
                            });
                            if ($scope.ListadoCuentaBancarias.length > 0) {
                                $scope.Modelo.CuentaBancariaConsignacion = $scope.ListadoCuentaBancarias[0];
                                $scope.Modelo.CuentaBancariaCheque = $scope.ListadoCuentaBancarias[0];
                            }
                            else {
                                $scope.MensajesError.push('La oficina seleccionada no tiene ninguna cuenta bancaria asociada');
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        function CargarCajas() {
            $scope.ListadoCajaOficinas = [];
            $scope.MensajesError = [];
            CajasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTodaCajaOficinas = [];
                        $scope.ListadoTodaCajaOficinas = response.data.Datos;

                        if ($scope.CodigoCaja !== undefined && $scope.CodigoCaja !== null && $scope.CodigoCaja !== '' && $scope.CodigoCaja !== 0) {
                            $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                                if (itmOfic.Codigo > 0) {
                                    if (itmOfic.Oficina.Codigo == $scope.CodigoOficinaDestino) {
                                        $scope.ListadoCajaOficinas.push(itmOfic);
                                    }
                                }
                            });

                            $scope.Modelo.Caja = $linq.Enumerable().From($scope.ListadoCajaOficinas).First('$.Codigo == ' + $scope.CodigoCaja);

                        }
                        else {
                            $scope.ListadoCajaOficinas = [];
                            $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                                if (itmOfic.Codigo > 0) {
                                    if (itmOfic.Oficina.Codigo == $scope.Modelo.OficinaDestino.Codigo) {
                                        $scope.ListadoCajaOficinas.push(itmOfic);
                                    }
                                }
                            });
                            if ($scope.ListadoCajaOficinas.length > 0) {
                                $scope.Modelo.Caja = $scope.ListadoCajaOficinas[0];
                            }
                            else {
                                if ($scope.ValidarCajasOficina == true) {
                                    $scope.MensajesError.push('La oficina seleccionada no tiene ninguna caja asociada');
                                }
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.ValidarConceptos = function (TipoComprobante) {
            if (TipoComprobante != undefined && TipoComprobante != null && TipoComprobante != '') {
                if (TipoComprobante.Codigo == 2701) {
                    $scope.ListadoConceptoContable = $scope.ListadoConceptosEgreso;
                } else if (TipoComprobante.Codigo == 2702) {
                    $scope.ListadoConceptoContable = $scope.ListadoConceptosIngreso;
                }
            }
        }
        /*Cargar Combo Conceptos Contables*/
        ConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoConceptoContable = response.data.Datos;

                        response.data.Datos.forEach(item => {
                            if (item.TipoConceptoContable.Codigo == 2701) {
                                $scope.ListadoConceptosEgreso.push(item);
                            } else if (item.TipoConceptoContable.Codigo == 2702) {
                                $scope.ListadoConceptosIngreso.push(item);
                            }
                        });
                        $scope.ValidarConceptos($scope.Modelo.TipoComprobante);
                        if ($scope.CodigoConceptoContable !== undefined && $scope.CodigoConceptoContable !== '' && $scope.CodigoConceptoContable !== null) {
                            if ($scope.CodigoConceptoContable > 0) {
                                $scope.Modelo.ConceptoContable = $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo ==' + $scope.CodigoConceptoContable);
                            }
                        }
                    }
                    else {
                        $scope.ListadoConceptoContable = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        
        $scope.ValidarConceptos($scope.Modelo.TipoComprobante);
       
        $scope.AsignarPlaca = function (Placa) {
            if (Placa != undefined || Placa != null) {
                if (angular.isObject(Placa)) {
                    $scope.LongitudPlaca = Placa.length;
                    $scope.PlacaValida = true;
                    $scope.ListaBeneficiariotercero = []
                    $scope.Modelo.Tercero = $scope.CargarTercero(Placa.Tenedor.Codigo)
                    $scope.Modelo.Beneficiario = $scope.CargarTercero(Placa.Tenedor.Codigo)
                    $scope.ListaBeneficiariotercero.push($scope.CargarTercero(Placa.Tenedor.Codigo))
                    var cont = 0
                    for (var i = 0; i < $scope.ListaBeneficiariotercero.length; i++) {
                        if ($scope.ListaBeneficiariotercero[i].Codigo == Placa.Conductor.Codigo) {
                            cont++
                            break
                        }
                    }
                    if (cont == 0) {
                        $scope.ListaBeneficiariotercero.push($scope.CargarTercero(Placa.Conductor.Codigo))
                    }
                }
                else if (angular.isString(Placa)) {
                    $scope.LongitudPlaca = Placa.length;
                    $scope.PlacaValida = false;
                }
            }
        };
        $scope.AsignarBeneficiario = function () {
            try {
                if ($scope.Modelo.Tercero.Codigo > 0) {
                    $scope.Modelo.Beneficiario = $scope.Modelo.Tercero
                }
            } catch (e) {
                $scope.Modelo.Beneficiario = ''
            }
        }
        $scope.ListadoTerceros = []
        $scope.AutocompleteTerceros = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            //CadenaPerfiles: PERFIL_TENEDOR,
                            ValorAutocomplete: value, Sync: true
                        })
                    $scope.ListadoTerceros = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTerceros)
                }
            }
            return $scope.ListadoTerceros
        }
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 2) == 0 || value.length == 1) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                }
            }
            return $scope.ListadoVehiculos
        }
        $scope.ValidarTercero = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Tercero = response.data.Datos
                            }
                            else {
                                $scope.Modelo.Tercero = undefined
                                ShowError('La identificación Ingresada no es válida o el tercero no se encuentra registrado')
                            }
                        }
                    });
            }
        };

        $scope.ConsultarTerceros = function (Nombre, opcion) {
            $scope.Opcion = opcion

            if (Nombre !== undefined && Nombre !== '' && Nombre !== null) {
                TercerosFactory.ConsultarTerceroGeneral({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Nombre: Nombre }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoTerceros = response.data.Datos
                                showModal('ConsultaTerceros')
                            }
                            else {
                                showDialog('El tercero ingresado se encuentra inactivo o no existe en el sistema')
                            }
                        }
                    });
            }
        }


        $scope.AsignarTercero = function (item) {
            $scope.ListadoTerceros.forEach(function (itemTercero) {
                if (itemTercero.Codigo == item.Codigo) {
                    if ($scope.Opcion == 1) {
                        $scope.Modelo.Tercero = itemTercero
                    }
                    if ($scope.Opcion == 2) {
                        $scope.Modelo.Beneficiario = itemTercero
                    }
                    if ($scope.Opcion == 3) {
                        $scope.Modelo.Tercero = itemTercero
                    }
                    closeModal('ConsultaTerceros')
                }
            })
        }



        $scope.ValidarTerceroModal = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modal.TerceroModal = response.data.Datos
                            }
                            else {
                                $scope.Modal.TerceroModal = undefined
                                ShowError('La identificación Ingresada no es válida o el tercero no se encuentra registrado')
                            }
                        }
                    });
            }
        };
        $scope.ValidarBeneficiario = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Beneficiario = response.data.Datos
                            }
                            else {
                                $scope.Modelo.Beneficiario = undefined
                                ShowError('La identificación Ingresada no es válida o el beneficiario no se encuentra registrado')
                            }
                        }
                    });
            }
        };

        $scope.validarFormaPago = function () {
            if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = true

                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.ValorPagoCheque = ''


                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''


            }
            if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = false
                $scope.ActivarCambista = true

                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.FechaConsignacion = new Date()
                $scope.Modelo.ValorPagoCheque = ''

                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''


            }
            else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = true
                $scope.ActivarConsignacion = false

                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''

                $scope.Modelo.CuentaBancariaConsignacion = ''
                $scope.Modelo.FechaConsignacion = null
                $scope.Modelo.ValorPagoTransferencia = ''
                $scope.Modelo.NumeroConsignacion = ''

            }
            else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                $scope.ActivarEfectivo = true
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = false

                $scope.Modelo.CuentaBancariaConsignacion = ''
                $scope.Modelo.FechaConsignacion = null
                $scope.Modelo.ValorPagoTransferencia = ''
                $scope.Modelo.NumeroConsignacion = ''


                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.ValorPagoCheque = ''
            }
            else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_NO_APLICA) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = false

                $scope.Modelo.CuentaBancariaConsignacion = ''
                $scope.Modelo.FechaConsignacion = null
                $scope.Modelo.ValorPagoTransferencia = ''
                $scope.Modelo.NumeroConsignacion = ''


                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.ValorPagoCheque = ''


                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''
            }
        }

        $scope.AsignarOficinaDestino = function (Oficina) {
            $scope.MensajesError = [];
            if (Oficina.Codigo !== 0 && Oficina.Codigo !== undefined && Oficina.Codigo !== null) {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                    CargarCajas()
                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE || $scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                    CargarCuentasBancarias()
                }
            }
        }
        $scope.ValidarFormaPago = function (Oficina) {
            if ($scope.Modelo.FormaPagoRecaudo.Codigo !== CODIGO_MEDIO_PAGO_NO_APLICA) {
                $scope.AsignarOficinaDestino(Oficina)
            }
        }
        /*Funcion Limpiar Modal Movimiento*/
        function limpiarModalMovimiento() {
            $scope.Modal.ValorBase = '';
            $scope.Modal.ValorDebito = '';
            $scope.Modal.ValorCredito = '';
            $scope.Modal.CuentaPUC = '';
            $scope.Modal.CodigoCuentaPUC = '';
            $scope.Modal.TerceroModal = '';
            $scope.Modal.GeneraCuenta = false;
            $scope.Modal.Prefijo = '';
            $scope.Modal.CodigoAnexo = '';
            $scope.Modal.SufijoCodigoAnexo = '';
            $scope.Modal.CampoAuxiliar = '';
            $scope.Modal.Vehiculo = '';
            $scope.Modal.CentroCosto = '';
            $scope.Modal.NotaDetalle = '';
            $scope.Modal.indice = '';
        }

        $scope.ModalMovimiento = function () {
            limpiarModalMovimiento();
            $scope.TituloModalCuenta = 'Nueva Cuenta';
            $scope.MensajesError = [];           
            $scope.MensajesErrorMovimiento = [];
            
            showModal('modalMovimiento');
            if ($scope.Modelo.Proveedor != undefined && $scope.Modelo.Proveedor != null && $scope.Modelo.Proveedor != '') {
                $scope.Modal.TerceroModal = $scope.CargarTercero($scope.Modelo.Proveedor.Codigo);
            }
            
            
        };

        $scope.ValidarValorBase = function (cuentaPUC) {
            if (cuentaPUC.ExigeValorBase == 1) {
                $scope.Modal.ValorBase = MascaraValores(cuentaPUC.ValorBase);
            }
        }
        $scope.EditarMovimiento = function (item, indice) {
            limpiarModalMovimiento();
            item.Editando = true;
            $scope.Modal.indice = indice
            $scope.TituloModalCuenta = 'Editar Cuenta';
            $scope.MensajesError = [];
            $scope.MensajesErrorMovimiento = [];
            
            $scope.Modal.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + item.CuentaPUC.Codigo);
            if (item.Tercero == undefined || item.Tercero == '' || item.Tercero == null) {
                if ($scope.Deshabilitar == false || $scope.Deshabilitar == undefined) {
                    if ($scope.Modelo.Proveedor != undefined && $scope.Modelo.Proveedor != null && $scope.Modelo.Proveedor != '') {
                        $scope.Modal.TerceroModal = $scope.CargarTercero($scope.Modelo.Proveedor.Codigo);
                    }
                }
                
            } else {
                $scope.Modal.TerceroModal = $scope.CargarTercero(item.Tercero.Codigo);
            }
            
            $scope.Modal.ValorBase = item.ValorBase == undefined ? '' : MascaraValores(item.ValorBase);
            $scope.Modal.ValorDebito = item.ValorDebito == undefined || item.ValorDebito == 0 ? '' : MascaraValores(item.ValorDebito);
            $scope.Modal.ValorCredito = item.ValorCredito == undefined || item.ValorCredito == 0 ? '' : MascaraValores(item.ValorCredito);
            $scope.Modal.GeneraCuenta = item.GeneraCuenta == 1 || item.GeneraCuenta == true ? true : false;
            $scope.Modal.Vehiculo = item.Vehiculo == undefined ? '' : $scope.CargarVehiculos(item.Vehiculo.Codigo);
            $scope.Modal.CentroCosto = item.CentroCosto;
            $scope.Modal.NotaDetalle = item.NotaDetalle;
            $scope.Modal.Prefijo = item.Prefijo;
            $scope.Modal.SufijoCodigoAnexo = item.SufijoCodigoAnexo;
            $scope.Modal.CodigoAnexo = item.CodigoAnexo;
            $scope.Modal.CampoAuxiliar = item.CampoAuxiliar;
            if ($scope.Modal.CuentaPUC != undefined && $scope.Modal.CuentaPUC != null && $scope.Modal.CuentaPUC != '') {
                $scope.Modal.CodigoCuenta = $scope.Modal.CuentaPUC.CodigoCuenta;
                if ($scope.Modal.ValorBase == undefined || $scope.Modal.ValorBase == null || $scope.Modal.ValorBase == '') {
                    $scope.ValidarValorBase($scope.Modal.CuentaPUC);
                }
            }
            

            showModal('modalMovimiento');
        }
        $scope.GuardarMovimiento = function () {
            $scope.MensajesErrorMovimiento = [];
            if (DatosRequeridosMovimiento()) {
                CargarlistadoMovimiento()
            }
        }
        function CargarlistadoMovimiento() {
            var Coincidencia = false;
            closeModal('modalMovimiento');
            $scope.ListadoMovimiento.forEach(item =>{
                if (item.CuentaPUC.CodigoCuenta == $scope.Modal.CuentaPUC.CodigoCuenta) {
                    Coincidencia = true;
                    item.CuentaPUC = $scope.Modal.CuentaPUC;
                    item.ValorBase = MascaraNumero($scope.Modal.ValorBase);
                    item.ValorDebito = MascaraNumero($scope.Modal.ValorDebito);
                    item.ValorCredito = MascaraNumero($scope.Modal.ValorCredito);
                    item.Tercero = { Codigo: $scope.Modal.TerceroModal == undefined || $scope.Modal.TerceroModal == null || $scope.Modal.TerceroModal == '' ? 0 : $scope.Modal.TerceroModal.Codigo };
                    item.TerceroParametrizacion = { Codigo: 3100 };
                    item.DocumentoCruce = { Codigo: 2800 };
                    item.CentroCostoParametrizacion = { Codigo: 3200 };
                    item.GeneraCuenta = $scope.Modal.GeneraCuenta == true ? 1 : 0;
                    item.Vehiculo = { Codigo: $scope.Modal.Vehiculo == undefined || $scope.Modal.Vehiculo == null || $scope.Modal.Vehiculo == ''? 0: $scope.Modal.Vehiculo.Codigo };
                    item.CentroCosto = $scope.Modal.CentroCosto;
                    item.NotaDetalle = $scope.Modal.NotaDetalle;
                    item.Prefijo = $scope.Modal.Prefijo;
                    item.CodigoAnexo = $scope.Modal.CodigoAnexo;
                    item.SufijoCodigoAnexo = $scope.Modal.SufijoCodigoAnexo;
                    item.CampoAuxiliar = $scope.Modal.CampoAuxiliar;
                }
            }); 
                
            if (!Coincidencia) {
                $scope.ListadoMovimiento.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CuentaPUC: $scope.Modal.CuentaPUC,
                    ValorBase: MascaraNumero($scope.Modal.ValorBase),
                    ValorDebito: MascaraNumero($scope.Modal.ValorDebito),
                    ValorCredito: MascaraNumero($scope.Modal.ValorCredito),
                    Tercero: { Codigo: $scope.Modal.TerceroModal == undefined || $scope.Modal.TerceroModal == null || $scope.Modal.TerceroModal == '' ? 0 : $scope.Modal.TerceroModal.Codigo },
                    TerceroParametrizacion: { Codigo: 3100 },
                    DocumentoCruce: { Codigo: 2800 },
                    CentroCostoParametrizacion: { Codigo: 3200 },
                    GeneraCuenta: $scope.Modal.GeneraCuenta == true ? 1 : 0,
                    Vehiculo: { Codigo: $scope.Modal.Vehiculo == undefined || $scope.Modal.Vehiculo == null || $scope.Modal.Vehiculo == '' ? 0 : $scope.Modal.Vehiculo.Codigo },
                    CentroCosto: $scope.Modal.CentroCosto,
                    NotaDetalle : $scope.Modal.NotaDetalle,
                    Prefijo : $scope.Modal.Prefijo,
                    CodigoAnexo : $scope.Modal.CodigoAnexo,
                    SufijoCodigoAnexo : $scope.Modal.SufijoCodigoAnexo,
                    CampoAuxiliar : $scope.Modal.CampoAuxiliar
                });
            }
            CalcularMovimiento()
        }
        /*Funcion Calcular Totales*/
        function CalcularMovimiento() {
            $scope.Modelo.Debito = 0
            $scope.Modelo.Credito = 0
            $scope.Modelo.Diferencia = 0
        /*realiza el recorrido para  obtener  los totales */
            if ($scope.ListadoMovimiento != undefined && $scope.ListadoMovimiento != null) {
                if ($scope.ListadoMovimiento.length > 0) {
                    $scope.ListadoMovimiento.forEach(function (item) {
                        $scope.Modelo.Debito = Math.ceil($scope.Modelo.Debito) + Math.ceil(item.ValorDebito == undefined || isNaN(item.ValorDebito) ? 0 : item.ValorDebito)
                        $scope.Modelo.Credito = Math.ceil($scope.Modelo.Credito) + Math.ceil(item.ValorCredito == undefined || isNaN(item.ValorCredito) ? 0 : item.ValorCredito)
                    })

                }
            }
                
            $scope.Modelo.Diferencia = Math.ceil(($scope.Modelo.Debito) - parseInt($scope.Modelo.Credito))
        }

        function DatosRequeridosMovimiento() {
            $scope.MensajesErrorMovimiento = [];
            var con = 0
            if ($scope.ListadoMovimiento.length > 0) {
                $scope.ListadoMovimiento.forEach(function (item) {
                    if (item.CuentaPUC.Codigo == $scope.Modal.CuentaPUC.Codigo) {
                        if (!item.Editando) {
                            con++
                        }
                    }
                })
            }
            if (con == 0) {
                var continuar = true;
                if ($scope.Modal.CuentaPUC == undefined || $scope.Modal.CuentaPUC == null || $scope.Modal.CuentaPUC == "") {
                    $scope.MensajesErrorMovimiento.push('Debe Seleccionar una Cuenta Puc');
                    continuar = false;
                }
                if ($scope.Modal.CuentaPUC.ExigeValorBase == 1) {
                    if ($scope.Modal.ValorBase == 0 || $scope.Modal.ValorBase == "") {
                        $scope.MensajesErrorMovimiento.push('Debe ingresar el  valor base ');
                        continuar = false;
                    }
                }
                else if ($scope.Modal.ValorBase == undefined || $scope.Modal.ValorBase == null || $scope.Modal.ValorBase == "") {
                    $scope.Modal.ValorBase = 0
                }
                if ($scope.Modal.CuentaPUC.ExigeTercero == 1) {
                    if ($scope.Modal.TerceroModal == undefined || $scope.Modal.TerceroModal == "" || $scope.Modal.TerceroModal== null) {
                        $scope.MensajesErrorMovimiento.push('Debe ingresar el tercero');
                        continuar = false;
                    }
                }
                if ($scope.Modal.CuentaPUC.ExigeCentroCosto == 1) {
                    if ($scope.Modal.CentroCosto == undefined || $scope.Modal.CentroCosto == "" || $scope.Modal.CentroCosto == null) {
                        $scope.MensajesErrorMovimiento.push('Debe ingresar el Centro de Costo');
                        continuar = false;
                    }
                }
                if ($scope.Modal.ValorDebito > 0 && $scope.Modal.ValorCredito > 0) {
                    $scope.MensajesErrorMovimiento.push('Debe ingresar solo un valor ya sea Crédito o Débito');
                    continuar = false;
                }
                if (($scope.Modal.ValorDebito == undefined || $scope.Modal.ValorDebito == null || $scope.Modal.ValorDebito == "")
                    && ($scope.Modal.ValorCredito !== undefined && $scope.Modal.ValorCredito !== null && $scope.Modal.ValorCredito !== "")) {
                    $scope.Modal.ValorDebito = '';
                    //if (MascaraNumero($scope.Modal.ValorBase) < MascaraNumero($scope.Modal.ValorCredito)) {
                    //    $scope.MensajesErrorMovimiento.push('el valor crédito no debe ser mayor al valor base');
                    //    continuar = false;
                    //}
                } else if (($scope.Modal.ValorDebito !== undefined && $scope.Modal.ValorDebito !== null && $scope.Modal.ValorDebito !== "") &&
                    ($scope.Modal.ValorCredito == undefined || $scope.Modal.ValorCredito == null || $scope.Modal.ValorCredito == "")) {
                    $scope.Modal.ValorCredito = '';
                    //if (MascaraNumero($scope.Modal.ValorBase) < MascaraNumero($scope.Modal.ValorDebito)) {
                    //    $scope.MensajesErrorMovimiento.push('el valor débito no debe ser mayor al valor base');
                    //    continuar = false;
                    //}
                }
                else {
                    $scope.MensajesErrorMovimiento.push('Debe ingresar solo un valor ya sea Crédito o Débito');
                    continuar = false;
                }
                if ($scope.Modal.GeneraCuenta) {
                    for (var i = 0; i < $scope.ListadoMovimiento.length; i++) {
                        if ($scope.ListadoMovimiento[i].GeneraCuenta == 1 && (i !== $scope.Modal.indice)) {
                            continuar = false;
                            $scope.MensajesErrorMovimiento.push('ya existe otro movimiento que genera cuenta');
                            break;
                        }
                    }
                }
            }
            else {
                $scope.MensajesErrorMovimiento.push('La cuenta PUC ' + $scope.Modal.CuentaPUC.Nombre + ' ya se encuentra ingresada');
                continuar = false;
            }
            if (continuar) {
                $scope.ListadoMovimiento.forEach(item => {
                    item.Editando = false;
                });
            }
            return continuar;
        }
        $scope.LimpiarMovimiento = function () {
            $scope.ListadoMovimiento = [];
            $scope.Modelo.Debito = 0;
            $scope.Modelo.Credito = 0;
            $scope.Modelo.Diferencia = 0;
        };

        /*Eliminar Movimiento*/
        $scope.ConfirmacionEliminarMovimiento = function (indice) {
            $scope.MensajeEliminar = { indice };
            $scope.VarAuxi = indice
            showModal('modalEliminarMovimiento');
        };

        $scope.EliminarMovimiento = function (indice) {
            $scope.ListadoMovimiento.splice($scope.VarAuxi, 1);
            closeModal('modalEliminarMovimiento');
            CalcularMovimiento()
        };

        $scope.ConsultarMovimiento = function () {
            $scope.LimpiarMovimiento();
            if (DatosRequeridosConsutarMovimiento()) {
                if ($scope.Modelo.ConceptoContable !== undefined && $scope.Modelo.ConceptoContable !== null && $scope.Modelo.ConceptoContable !== '') {
                    if ($scope.Modelo.ConceptoContable.Codigo == 0) {
                        //Crédito
                        var item = {};
                        if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                            item.CuentaPUC = { Nombre: $scope.Caja.PlanUnicoCuentas.Nombre, Codigo: $scope.Caja.PlanUnicoCuentas.Codigo }

                        } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                            item.CuentaPUC = { Nombre: $scope.CuentaBancariaCheque.PlanUnicoCuentas.Nombre, Codigo: $scope.CuentaBancariaCheque.PlanUnicoCuentas.Codigo }

                        } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA || $scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                            item.CuentaPUC = { Nombre: $scope.CuentaBancariaConsignacion.PlanUnicoCuentas.Nombre, Codigo: $scope.CuentaBancariaConsignacion.PlanUnicoCuentas.Codigo }
                        }
                      
                        item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        item.Tercero = { Codigo: $scope.Modelo.Beneficiario.Codigo }
                        item.TerceroParametrizacion = { Codigo: 3100 };
                        item.DocumentoCruce = { Codigo: 2800 };
                        item.CentroCostoParametrizacion = { Codigo: 3200 };
                        $scope.ListadoMovimiento.push(item);

                        CalcularMovimiento();
                    } else {
                        DetalleConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.ConceptoContable.Codigo }).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) {
                                        response.data.Datos.forEach(function (item) {
                                            var Concepto = {}
                                            if (item.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_DEBITO) {
                                                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                                                //    Concepto.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                //    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                //}
                                                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                                                //    Concepto.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                //    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                //}
                                                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA || $scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                                                //    Concepto.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                //    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia)
                                                //}
                                                Concepto.ValorCredito = 0;
                                                //if (item.CuentaPUC.AplicarMedioPago > 0) {
                                                //    if ($scope.ActivarCambista) {
                                                //        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                //    }
                                                //    if ($scope.ActivarCheque) {
                                                //        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaCheque.CuentaBancaria.CuentaPUC
                                                //    }
                                                //    if ($scope.ActivarEfectivo) {
                                                //        Concepto.CuentaPUC = $scope.Modelo.Caja.CuentaPUC
                                                //    }
                                                //    if ($scope.ActivarConsignacion) {
                                                //        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                //    }

                                                //} else {
                                                    Concepto.CuentaPUC = item.CuentaPUC;
                                               // }
                                            }
                                            if (item.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_CREDITO) {
                                                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                                                //    Concepto.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                //    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                //}
                                                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                                                //    Concepto.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                //    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                //}
                                                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA || $scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                                                //    Concepto.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                //    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                //}
                                                Concepto.ValorDebito = 0;
                                                //if (item.CuentaPUC.AplicarMedioPago > 0) {
                                                //    if ($scope.ActivarCambista) {
                                                //        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                //    }
                                                //    if ($scope.ActivarCheque) {
                                                //        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaCheque.CuentaBancaria.CuentaPUC
                                                //    }
                                                //    if ($scope.ActivarEfectivo) {
                                                //        Concepto.CuentaPUC = $scope.Modelo.Caja.CuentaPUC
                                                //    }
                                                //    if ($scope.ActivarConsignacion) {
                                                //        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                //    }

                                               // } else {
                                                    Concepto.CuentaPUC = item.CuentaPUC;
                                                //}
                                            }

                                            Concepto.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                                            //Concepto.Tercero = { Codigo: $scope.Modelo.Beneficiario.Codigo };
                                            Concepto.TerceroParametrizacion = { Codigo: 3100 }
                                            Concepto.DocumentoCruce = { Codigo: 2800 }
                                            Concepto.CentroCostoParametrizacion = { Codigo: 3200 }
                                            Concepto.GeneraCuenta = parseInt(item.GeneraCuenta);
                                            $scope.ListadoMovimiento.push(Concepto)
                                            CalcularMovimiento()
                                        });
                                    } else {
                                        ShowError('El Concepto contable no se encuentra parametrizado');
                                    }
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }

            }
        };

        function DatosRequeridosConsutarMovimiento() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.ConceptoContable == undefined || $scope.Modelo.ConceptoContable == null || $scope.Modelo.ConceptoContable == "" || $scope.Modelo.ConceptoContable.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar un concepto contable');
                continuar = false;
            }
           
           

            if (continuar == false) {
                Ventana.scrollTop = 0
            }


            return continuar;
        }
        /*----------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
            else {
                Ventana.scrollTop = 0
            }
        };
        

        /*Guardar/Modificar Comprobante De Egreso*/
        $scope.Guardar = function () {
            if (DatosRequeridos()) {
                $scope.Modelo = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo : $scope.Modelo.Codigo,
                    Fecha: $scope.Modelo.Fecha,
                    Numero: $scope.Modelo.Numero,
                    TipoComprobante: $scope.Modelo.TipoComprobante,
                    OficinaDestino: $scope.Modelo.OficinaDestino,
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                    Observaciones: $scope.Modelo.Observaciones,
                    Proveedor: { Codigo: $scope.Modelo.Proveedor.Codigo },
                    NumeroFactura: $scope.Modelo.NumeroFactura,
                    FechaFactura: new Date($scope.Modelo.FechaFactura),
                    FechaVencimientoFactura: new Date($scope.Modelo.FechaVencimientoFactura),
                    Subtotal: MascaraNumero($scope.Modelo.Subtotal),
                    ValorIva: MascaraNumero($scope.Modelo.ValorIva),
                    ValorTotal: MascaraNumero($scope.Modelo.ValorTotal),
                    DocumentoOrigen: { Codigo: 2603 },
                    Debito: $scope.Modelo.Debito,
                    Credito: $scope.Modelo.Credito,
                    Diferencia: $scope.Modelo.Diferencia,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }
               // $scope.Modelo.CodigoAlterno = 0;
               // $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO;
                //$scope.Modelo.Vehiculo = $scope.Modelo.Placa;
                //$scope.Modelo.FormaPagoDocumento = $scope.Modelo.FormaPagoRecaudo;
                $scope.Modelo.Estado = $scope.Estado.Codigo;
                //$scope.Modelo.ValorAlterno = 0;

                if ($scope.Modelo.ConceptoContable == undefined || $scope.Modelo.ConceptoContable == "" || $scope.Modelo.ConceptoContable == null) {
                    $scope.Modelo.ConceptoContable = { Codigo: 0 }
                }
                $scope.Modelo.ConceptoContable = $scope.Modelo.ConceptoContable.Codigo
                //$scope.Modelo.Proveedor = { Codigo: $scope.Modelo.Proveedor.Codigo }
                
                
                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                //    $scope.Modelo.Caja = $scope.Modelo.Caja;
                //    $scope.Modelo.ValorPagoEfectivo = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                //    $scope.Modelo.CuentaBancaria = { Codigo: 0 };
                //    $scope.Modelo.FechaPagoRecaudo = new Date();
                //    $scope.Modelo.DestinoIngreso = { Codigo: 4801 }; //Codigo Destino ingreso Caja
                //    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                //}
                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                //    $scope.Modelo.CuentaBancaria = { Codigo: $scope.Modelo.CuentaBancariaCheque.CodigoCuenta };
                //    $scope.Modelo.FechaPagoRecaudo = $scope.Modelo.FechaCheque;
                //    $scope.Modelo.ValorPagoCheque = MascaraNumero($scope.Modelo.ValorPagoCheque);
                //    //$scope.Modelo.Numeracion = $scope.Modelo.NumeroCheque
                //    $scope.Modelo.Caja = { Codigo: 0 };
                //    $scope.Modelo.DestinoIngreso = { Codigo: 4802 };//Codigo Destino ingreso Cuenta Bancaria
                //    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoCheque);
                //}
                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                //    $scope.Modelo.CuentaBancaria = { Codigo: $scope.Modelo.CuentaBancariaConsignacion.CodigoCuenta };
                //    $scope.Modelo.FechaPagoRecaudo = $scope.Modelo.FechaConsignacion;
                //    $scope.Modelo.NumeroPagoRecaudo = $scope.Modelo.NumeroConsignacion;
                //    $scope.Modelo.ValorPagoTransferencia = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                //    $scope.Modelo.Caja = { Codigo: 0 };
                //    $scope.Modelo.DestinoIngreso = { Codigo: 4802 };//Codigo Destino ingreso Cuenta Bancaria
                //    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                //}
                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                //    $scope.Modelo.CuentaBancaria = { Codigo: $scope.Modelo.CuentaBancariaConsignacion.Codigo };
                //    $scope.Modelo.FechaPagoRecaudo = $scope.Modelo.FechaConsignacion;
                //    $scope.Modelo.NumeroPagoRecaudo = $scope.Modelo.NumeroConsignacion;
                //    $scope.Modelo.ValorPagoTransferencia = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                //    $scope.Modelo.Caja = { Codigo: 0 };
                //    $scope.Modelo.DestinoIngreso = { Codigo: 4802 };//Codigo Destino ingreso Cuenta Bancaria
                //    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                //}
                //$scope.Modelo.GeneraConsignacion = "";
                $scope.Modelo.Detalle = $scope.ListadoMovimiento;
                //if ($scope.Modelo.DocumentoOrigen.Codigo == 2609) {
                //    $scope.Modelo.NumeroDocumentoOrigen = '';
                //} else {
                    $scope.Modelo.NumeroDocumentoOrigen = $scope.Modelo.NumeroFactura;
                //}
                $scope.Modelo.Detalle.forEach(item => {
                    if (item.GeneraCuenta == 1 || item.GeneraCuenta == true) {
                        $scope.Modelo.CuentaPorPagar = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: $scope.Modelo.TipoComprobante.Codigo == 2701 ? CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR : CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                            CodigoAlterno: '',
                            Fecha: $scope.Modelo.Fecha,
                            Tercero: { Codigo: $scope.Modelo.Proveedor == undefined ? 0 : $scope.Modelo.Proveedor.Codigo },
                            DocumentoOrigen: { Codigo: $scope.Modelo.DocumentoOrigen.Codigo },
                            CodigoDocumentoOrigen: $scope.Modelo.DocumentoOrigen.Codigo,
                            //Numero: $scope.ModeloNumero,
                            FechaDocumento: $scope.Modelo.FechaFactura,
                            FechaVenceDocumento: $scope.Modelo.FechaVencimientoFactura,
                            Numeracion: '',
                            CuentaPuc: { Codigo: item.CuentaPUC.Codigo },
                            ValorTotal: item.ValorDebito == undefined || item.ValorDebito == null || item.ValorDebito == '' || isNaN(item.ValorDebito) ? item.ValorCredito : item.ValorDebito,
                            Abono: 0,
                            Saldo: item.ValorDebito == undefined || item.ValorDebito == null || item.ValorDebito == '' || isNaN(item.ValorDebito) ? item.ValorCredito : item.ValorDebito,
                            FechaCancelacionPago: $scope.Modelo.Fecha,
                            Aprobado: 1,
                            UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            FechaAprobo: $scope.Modelo.Fecha,
                            Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                            Vehiculo: { Codigo: 0 },
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        }
                        item.GeneraCuenta = item.GeneraCuenta == true || item.GeneraCuenta == 1 ? 1 : 0;
                    }
                });
                


                var continuar = true
                //if ($scope.Modelo.DocumentoOrigen.Codigo == 2605) {
                //    for (var i = 0; i < $scope.ListadoNumeroDocumentosOrigen.length; i++) {
                //        if ($scope.Modelo.NumeroDocumentoOrigen == $scope.ListadoNumeroDocumentosOrigen[i].Codigo) {
                //            if ($scope.ListadoNumeroDocumentosOrigen[i].ValorFleteTransportador > CERO) {
                //                if (($scope.ListadoNumeroDocumentosOrigen[i].ValorAnticipo + $scope.Modelo.ValorPagoTotal) > $scope.ListadoNumeroDocumentosOrigen[i].ValorFleteTransportador && $scope.Sesion.UsuarioAutenticado.CodigoEmpresa != 5) {
                //                    ShowError('El sobreanticipo ingresado sumado al anticipo ya creado supera el valor del flete')
                //                    continuar = false;
                //                    closeModal('modalConfirmacionGuardar');

                //                    $scope.Modelo.NumeroDocumentoOrigen = '';
                //                }
                //            }
                //        }
                //    }
                //    $scope.Modelo.Autorizacion = 1
                //}
                if (continuar) {
                    DocumentoCausacionesFactory.Guardar($scope.Modelo).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if ($scope.Modelo.Numero == 0 || $scope.Modelo.Numero == undefined) {
                                    ShowSuccess('Se guardó el comprobante de causación N.' + response.data.Datos);
                                }
                                else {
                                    ShowSuccess('Se modificó el comprobante de causación N.' + response.data.Datos);
                                }
                                closeModal('modalConfirmacionGuardar');
                                document.location.href = '#!ConsultarComprobanteCausacion/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            else {
                Ventana.scrollTop = 0
                closeModal('modalConfirmacionGuardarComprobanteEgreso');
            }
        }
        /*Datos Requeridos Guardar o Modificar Comprobante*/
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == null || $scope.Modelo.Fecha == "") {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }
            if ($scope.Modelo.OficinaDestino.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar una oficina');
                continuar = false;
            }
            if ($scope.Modelo.Proveedor == undefined || $scope.Modelo.Proveedor == null || $scope.Modelo.Proveedor == "") {
                $scope.MensajesError.push('Debe ingresar un proveedor');
                continuar = false;
            }
            if ($scope.Modelo.NumeroFactura == undefined || $scope.Modelo.NumeroFactura == null || $scope.Modelo.NumeroFactura == "") {
                $scope.MensajesError.push('Debe ingresar un número de factura');
                continuar = false;
            }
            if ($scope.Modelo.FechaFactura == undefined || $scope.Modelo.FechaFactura == null || $scope.Modelo.FechaFactura == "") {
                $scope.MensajesError.push('Debe ingresar un fecha de factura');
                continuar = false;
            }
            if ($scope.Modelo.FechaVencimientoFactura == undefined || $scope.Modelo.FechaVencimientoFactura == null || $scope.Modelo.FechaVencimientoFactura == "") {
                $scope.MensajesError.push('Debe ingresar un fecha de vencimiento factura');
                continuar = false;
            }
            if ($scope.Modelo.Subtotal == undefined || $scope.Modelo.Subtotal == null || $scope.Modelo.Subtotal == "") {
                $scope.MensajesError.push('Debe ingresar un subtotal factura');
                continuar = false;
            }
            //if ($scope.Modelo.ValorIva == undefined || $scope.Modelo.ValorIva == null || $scope.Modelo.ValorIva == "") {
            //    $scope.MensajesError.push('Debe ingresar un valor IVA factura');
            //    continuar = false;
            //}

            var contadorGeneraCuenta = 0;
            $scope.ListadoMovimiento.forEach(item => {
                if (item.GeneraCuenta == 1 || item.GeneraCuenta == true ) {
                    contadorGeneraCuenta++;
                }
            });

            if (contadorGeneraCuenta > 1) {
                $scope.MensajesError.push('solo puede existir un movimiento que genere cuenta');
                continuar = false;
            } else if (contadorGeneraCuenta < 1) {
                $scope.MensajesError.push('debe existir un movimiento que genere cuenta');
                continuar = false;
            }
            //if ($scope.Modelo.Tercero == undefined || $scope.Modelo.Tercero == null || $scope.Modelo.Tercero == "") {
            //    $scope.MensajesError.push('Debe ingresar un tercero ');
            //    continuar = false;
            //}
            //if (($scope.Modelo.Placa == undefined || $scope.Modelo.Placa == null || $scope.Modelo.Placa == "") && $scope.Modelo.DocumentoOrigen.Codigo != 2609) {
            //    $scope.Modelo.Placa = { Codigo: 0, Placa: '' };
            //    continuar = false;
            //    $scope.MensajesError.push('Debe Ingresar una placa');
            //}
            //else if ($scope.PlacaValida == false && $scope.Modelo.DocumentoOrigen.Codigo != 2609) {
            //    $scope.MensajesError.push('La placa ingresada no es valida');
            //    continuar = false;
            //}
            //if ($scope.Modelo.Beneficiario == undefined || $scope.Modelo.Beneficiario == null || $scope.Modelo.Beneficiario == "") {
            //    $scope.MensajesError.push('Debe ingresar un beneficiario');
            //    continuar = false;
            //}
            //if (($scope.Modelo.DocumentoOrigen !== undefined && $scope.Modelo.DocumentoOrigen !== null && $scope.Modelo.DocumentoOrigen.Codigo !== 2600) && $scope.Modelo.DocumentoOrigen.Codigo != 2609) {
            //    if ($scope.Modelo.NumeroDocumentoOrigen == "" || $scope.Modelo.NumeroDocumentoOrigen == null || $scope.Modelo.NumeroDocumentoOrigen == undefined) {
            //        $scope.MensajesError.push('Debe ingresar el número origen');
            //        continuar = false;
            //    }
            //}
            //if ($scope.Modelo.FormaPagoRecaudo.Codigo == 4700) {
            //    $scope.MensajesError.push('Debe seleccionar una forma de pago');
            //    continuar = false;
            //}
            if ($scope.Modelo.OficinaDestino.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar una oficina');
                continuar = false;
            }
            //else {
                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                //    if ($scope.Modelo.FechaConsignacion == undefined || $scope.Modelo.FechaConsignacion == null || $scope.Modelo.FechaConsignacion == "") {
                //        $scope.MensajesError.push('Debe seleccionar la fecha de la Transferencia ');
                //        continuar = false;
                //    }
                //    if ($scope.Modelo.CuentaBancariaConsignacion == undefined || $scope.Modelo.CuentaBancariaConsignacion == null || $scope.Modelo.CuentaBancariaConsignacion == "") {
                //        $scope.MensajesError.push('Debe seleccionar la cuenta bancaria de la Transferencia ');
                //        continuar = false;
                //    }
                //    if ($scope.Modelo.ValorPagoTransferencia == undefined || $scope.Modelo.ValorPagoTransferencia == null || $scope.Modelo.ValorPagoTransferencia == "") {
                //        $scope.MensajesError.push('Debe ingresar el valor de la Transferencia ');
                //        continuar = false;
                //    }
                //    if ($scope.Modelo.NumeroConsignacion == undefined || $scope.Modelo.NumeroConsignacion == null || $scope.Modelo.NumeroConsignacion == "") {
                //        $scope.Modelo.NumeroConsignacion = 0
                //    }
                //}
                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                //    if ($scope.Modelo.FechaCheque == undefined || $scope.Modelo.FechaCheque == null || $scope.Modelo.FechaCheque == "") {
                //        $scope.MensajesError.push('Debe seleccionar la fecha del cheque ');
                //        continuar = false;
                //    }
                //    if ($scope.Modelo.NumeroCheque == undefined || $scope.Modelo.NumeroCheque == null || $scope.Modelo.NumeroCheque == "") {
                //        $scope.MensajesError.push('Debe ingresar el cheque ');
                //        continuar = false;
                //    }
                //    if ($scope.Modelo.CuentaBancariaCheque == undefined || $scope.Modelo.CuentaBancariaCheque == null || $scope.Modelo.CuentaBancariaCheque == "") {
                //        $scope.MensajesError.push('Debe sngresar  la cuenta bancaria del cheque ');
                //        continuar = false;
                //    }
                //    if ($scope.Modelo.ValorPagoCheque == undefined || $scope.Modelo.ValorPagoCheque == null || $scope.Modelo.ValorPagoCheque == "") {
                //        $scope.MensajesError.push('Debe ingresar el valor del Cheque  ');
                //        continuar = false;
                //    }
                //}
                //if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                //    if ($scope.Modelo.Caja == undefined || $scope.Modelo.Caja == null || $scope.Modelo.Caja == "") {
                //        $scope.MensajesError.push('Debe ingresar la caja del efectivo');
                //        continuar = false;
                //    }
                //    if ($scope.Modelo.ValorPagoEfectivo == undefined || $scope.Modelo.ValorPagoEfectivo == null || $scope.Modelo.ValorPagoEfectivo == "") {
                //        $scope.MensajesError.push('Debe ingresar el valor del efectivo');
                //        continuar = false;
                //    }

                //}
            //}

            if ($scope.Modelo.Debito == 0 && $scope.Modelo.Credito == 0 && $scope.Modelo.Diferencia == 0) {
                $scope.MensajesError.push('Debe ingresar minimo un movimiento en el comprobante de egreso');
                continuar = false;
            }
            if ($scope.Modelo.Diferencia !== 0) {
                $scope.MensajesError.push('El comprobante de egreso se encuentra desbalancedado');
                continuar = false;
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar;
        }

        // Metodo Obtener Comprobantes de egreso  

        if ($scope.Modelo.Numero > 0) {
            $scope.Titulo = 'EDITAR COMPROBANTE CAUSACIÓN';
            $scope.Deshabilitar = true;
            $scope.Bloquear = true;
            Obtener();
        }
        $scope.ValidarCajasOficina = true;
        function Obtener() {
            $scope.ValidarCajasOficina = false;
            blockUI.start('Cargando Comprobante causación Número ' + $scope.Modelo.Numero);

            $timeout(function () {
                blockUI.message('Cargando Comprobante causación  Número ' + $scope.Modelo.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Numero,
                //TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
            };

            blockUI.delay = 1000;
            DocumentoCausacionesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = response.data.Datos.Codigo
                        $scope.Modelo.Numero = response.data.Datos.Numero;
                        $scope.Modelo.Observaciones = response.data.Datos.Observaciones;
                        $scope.ListadoMovimiento = response.data.Datos.Detalle;
                        $scope.ListadoMovimiento.forEach(item => {
                            item.Tercero = $scope.CargarTercero(item.Tercero.Codigo);
                            item.CuentaPUC = item.CuentaPUC;
                            item.ValorBase = MascaraNumero(item.ValorBase);
                            item.ValorDebito = MascaraNumero(item.ValorDebito);
                            item.ValorCredito = MascaraNumero(item.ValorCredito);
                            item.Tercero = { Codigo: item.Tercero == undefined ? 0: item.Tercero.Codigo };
                            item.TerceroParametrizacion = { Codigo: 3100 };
                            item.DocumentoCruce = { Codigo: 2800 };
                            item.CentroCostoParametrizacion = { Codigo: 3200 };
                            item.GeneraCuenta = item.GeneraCuenta;
                            item.Vehiculo = { Codigo: item.Vehiculo.Codigo };
                            item.CentroCosto = item.CentroCosto;
                            item.NotaDetalle = item.NotaDetalle;
                            item.Prefijo = item.Prefijo;
                            item.CodigoAnexo = item.CodigoAnexo;
                            item.SufijoCodigoAnexo = item.SufijoCodigoAnexo;
                            item.CampoAuxiliar = item.CampoAuxiliar;
                        });
                        if (response.data.Datos.CentroCostos != undefined && response.data.Datos.CentroCostos.Codigo > 0) {
                            $scope.Modelo.CentroCostos = $linq.Enumerable().From($scope.ListaCentrosCostos).First('$.Codigo ==' + response.data.Datos.CentroCostos.Codigo);
                        }
                        var fecha = new Date(response.data.Datos.Fecha);
                        fecha.setHours(0);
                        fecha.setMinutes(0);
                        fecha.setMilliseconds(0);
                        $scope.Modelo.Fecha = fecha;
                        $scope.Modelo.Proveedor = $scope.CargarTercero(response.data.Datos.Proveedor.Codigo);
                        $scope.AsignarDatosProveedor();
                        $scope.Modelo.NumeroFactura = response.data.Datos.NumeroFactura;
                        $scope.Modelo.FechaFactura = new Date(response.data.Datos.FechaFactura);
                        $scope.Modelo.FechaVencimientoFactura = new Date(response.data.Datos.FechaVencimientoFactura);
                        $scope.Modelo.Subtotal = response.data.Datos.Subtotal;
                        $scope.Modelo.ValorIva = response.data.Datos.ValorIva;
                        $scope.Modelo.ValorTotal = response.data.Datos.ValorTotal;
                        //$scope.Modelo.Beneficiario = response.data.Datos.Beneficiario;
                        $scope.Modelo.TipoComprobante = $linq.Enumerable().From($scope.ListadoTipoComprobantes).First('$.Codigo==' + response.data.Datos.TipoComprobante.Codigo);
                       // $scope.Modelo.Vehiculo = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo)
                        //$scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);

                        if (response.data.Datos.Anulado == 1) {
                            $scope.Deshabilitar = true;
                            $scope.ListadoEstados.push({ Nombre: "ANULADO", Codigo: -1 })
                            $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

                        }
                        else {
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.Deshabilitar = true;
                            }
                            else if (response.data.Datos.Estado == 2) { //Pendiente aprobacion
                                $scope.ListadoEstados.push({ Nombre: "PENDIENTE APROBACIÓN", Codigo: 2 })
                                $scope.Deshabilitar = true;
                            }
                            else {
                                $scope.Deshabilitar = false;
                            }
                            $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                        }


                       // $scope.CodigoDocumentoOrigen = response.data.Datos.DocumentoOrigen.Codigo
                        //if ($scope.ListadoDocumentosOrigen.length > 0) {
                        //    $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + response.data.Datos.DocumentoOrigen.Codigo);
                        //}

                        //if ($scope.Modelo.DocumentoOrigen != null && $scope.Modelo.DocumentoOrigen != undefined && $scope.Modelo.DocumentoOrigen != '' && $scope.Modelo.DocumentoOrigen.Codigo != 2600 &&
                        //    $scope.Modelo.Placa != null && $scope.Modelo.Placa != undefined && $scope.Modelo.Placa != '' && $scope.Modelo.Placa.Placa.length > 3) {
                        //    switch ($scope.Modelo.DocumentoOrigen.Nombre) {
                        //        case "Liquidación":
                        //            CargarLiquidaciones();
                        //            break;
                        //        case "Anticipo":
                        //        case "Sobreanticipo":
                        //            CargarPlanillas();
                        //            break;
                        //    }
                        //}
                       //$scope.Modelo.NumeroDocumentoOrigen = { Codigo: response.data.Datos.NumeroDocumentoOrigen, NumeroDocumento: response.data.Datos.NumeroDocumentoOrigen };

                        //$scope.CodigoOficinaDestino = response.data.Datos.OficinaDestino.Codigo;
                        //if ($scope.ListadoOficinas.length > 0) {
                        //    $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + response.data.Datos.OficinaDestino.Codigo);
                        //}

                        //$scope.valorAlterno = response.data.Datos.ValorAlterno;
                        //$scope.Observaciones = response.data.Datos.Observaciones;
                        //$scope.CodigoFormaPago = response.data.Datos.FormaPagoDocumento.Codigo;
                        //if ($scope.ListaFormaPago.length > 0) {
                        //    $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo == ' + response.data.Datos.FormaPagoDocumento.Codigo);
                        //}
                        //$scope.CodigoConceptoContable = response.data.Datos.ConceptoContable
                        //if ($scope.ListadoConceptoContable.length > 0) {
                        //    if ($scope.CodigoConceptoContable > 0) {
                        //        $scope.Modelo.ConceptoContable = $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo == ' + response.data.Datos.ConceptoContable);
                        //    }
                        //}
                        //$scope.ValidarCajasOficina = false;
                        //CargarCajas()
                        ////Forma Pago Efectivo
                        //if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                        //    $scope.ActivarEfectivo = true
                        //    $scope.ActivarCheque = false
                        //    $scope.ActivarConsignacion = false
                        //    $scope.Modelo.ValorPagoEfectivo = response.data.Datos.ValorPagoEfectivo
                        //    $scope.CodigoCaja = response.data.Datos.Caja.Codigo;
                        //    if ($scope.ListadoTodaCajaOficinas.length > 0) {
                        //        $scope.ListadoCajaOficinas = [];
                        //        $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                        //            if (itmOfic.CodigoOficina == $scope.CodigoOficinaDestino) {
                        //                $scope.ListadoCajaOficinas.push(itmOfic);
                        //            }
                        //        });
                        //        $scope.Caja = $linq.Enumerable().From($scope.ListadoCajaOficinas).First('$.Codigo == ' + response.data.Datos.Caja.Codigo);
                        //    }
                        //}

                        ////Forma Pago Cheque
                        //if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                        //    CargarCuentasBancarias()
                        //    $scope.ActivarEfectivo = false
                        //    $scope.ActivarCheque = true
                        //    $scope.ActivarConsignacion = false
                        //    $scope.Modelo.ValorPagoCheque = response.data.Datos.ValorPagocheque
                        //    $scope.CodigoCuentaBancaria = response.data.Datos.CuentaBancaria.Codigo
                        //    if ($scope.ListadoCuentaBancarias.length > 0) {
                        //        $scope.Modelo.CuentaBancariaCheque = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + response.data.Datos.CuentaBancaria.Codigo);
                        //    }
                        //    var fechaCheque = new Date(response.data.Datos.FechaPagoRecaudo);
                        //    fechaCheque.setHours(0);
                        //    fechaCheque.setMinutes(0);
                        //    fechaCheque.setMilliseconds(0);

                        //    $scope.Modelo.FechaCheque = fechaCheque;
                        //    $scope.Modelo.NumeroCheque = response.data.Datos.Numeracion
                        //}
                        ////Forma Pago Cosnignacion
                        //if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                        //    CargarCuentasBancarias()
                        //    $scope.ActivarEfectivo = false
                        //    $scope.ActivarCheque = false
                        //    $scope.ActivarConsignacion = true
                        //    $scope.CodigoCuentaBancaria = response.data.Datos.CuentaBancaria.Codigo
                        //    $scope.Modelo.NumeroConsignacion = response.data.Datos.NumeroPagoRecaudo
                        //    var fechaConsignacion = new Date(response.data.Datos.FechaPagoRecaudo);
                        //    fechaConsignacion.setHours(0);
                        //    fechaConsignacion.setMinutes(0);
                        //    fechaConsignacion.setMilliseconds(0);
                        //    $scope.Modelo.FechaConsignacion = fechaConsignacion
                        //    $scope.Modelo.ValorPagoTransferencia = response.data.Datos.ValorPagoTransferencia
                        //    if ($scope.ListadoCuentaBancarias.length > 0) {
                        //        $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + response.data.Datos.CuentaBancaria.Codigo);
                        //    }
                        //}
                        //if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                        //    $scope.ActivarEfectivo = false
                        //    $scope.ActivarCheque = false
                        //    $scope.ActivarCambista = true
                        //    $scope.CodigoCuentaBancaria = response.data.Datos.CuentaBancaria.Codigo
                        //    $scope.Modelo.NumeroConsignacion = response.data.Datos.NumeroPagoRecaudo
                        //    var fechaConsignacion = new Date(response.data.Datos.FechaPagoRecaudo);
                        //    fechaConsignacion.setHours(0);
                        //    fechaConsignacion.setMinutes(0);
                        //    fechaConsignacion.setMilliseconds(0);
                        //    $scope.Modelo.FechaConsignacion = fechaConsignacion
                        //    $scope.Modelo.ValorPagoTransferencia = response.data.Datos.ValorPagoTransferencia
                        //    if ($scope.ListadoCuentaCambistas.length > 0) {
                        //        $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaCambistas).First('$.Codigo == ' + response.data.Datos.CuentaBancaria.Codigo);
                        //    }
                        //    $scope.ValorPagoTransferenciaCambista = response.data.Datos.ValorPagoTransferencia

                        //}
                        $scope.Modelo.Proveedor = $scope.CargarTercero($scope.Modelo.Proveedor.Codigo)
                        //$scope.Modelo.Beneficiario = $scope.CargarTercero($scope.Modelo.Beneficiario.Codigo)
                        //$scope.ListaBeneficiariotercero = []
                        //$scope.ListaBeneficiariotercero.push($scope.CargarTercero($scope.Modelo.Tercero.Codigo))
                        //var cont = 0
                        //for (var i = 0; i < $scope.ListaBeneficiariotercero.length; i++) {
                        //    if ($scope.ListaBeneficiariotercero[i].Codigo == $scope.Modelo.Beneficiario.Codigo) {
                        //        cont++
                        //        break
                        //    }
                        //}
                        //if (cont == 0) {
                        //    $scope.ListaBeneficiariotercero.push($scope.CargarTercero($scope.Modelo.Beneficiario.Codigo))
                        //}
                        //$scope.Modelo.Beneficiario = $linq.Enumerable().From($scope.ListaBeneficiariotercero).First('$.Codigo ==' + $scope.Modelo.Beneficiario.Codigo);
                        CalcularMovimiento()
                        $scope.MaskValores()
                    }
                    else {
                        ShowError('No se logro consultar el Comprobante de egreso No.' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarComprobanteCausacion';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar el Comprobante de egreso No.' + $scope.Modelo.Numero + '. Por favor contacte el administrador del sistema.');
                        document.location.href = '#!ConsultarComprobanteCausacion';
                });

            blockUI.stop();
        };
        //--Asginar AutoComplete Numero Documento Origen
        $scope.AsignarListaNumeroDocumento = function () {

            if ($scope.Modelo.DocumentoOrigen != null && $scope.Modelo.DocumentoOrigen != undefined && $scope.Modelo.DocumentoOrigen != '' && $scope.Modelo.DocumentoOrigen.Codigo != 2600 &&
                $scope.Modelo.Placa != null && $scope.Modelo.Placa != undefined && $scope.Modelo.Placa != '' && $scope.Modelo.Placa.Placa.length > 3) {
                switch ($scope.Modelo.DocumentoOrigen.Nombre) {
                    case "Liquidación":
                        CargarLiquidaciones();
                        break;
                    case "Anticipo":
                    case "Sobreanticipo":
                        CargarPlanillas();
                        break;

                }
            }
            if ($scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2605) {
                $scope.Modelo.Tercero = $scope.ListaBeneficiariotercero[0]
                $scope.Modelo.Beneficiario = $scope.ListaBeneficiariotercero[0]
            }
        }


        function CargarPlanillas() {
            $scope.ListadoNumeroDocumentosOrigen = [];
            var FiltroPlanilla = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Vehiculo: { Placa: $scope.Modelo.Placa.Placa },
                Estado: { Codigo: 1 },
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
            };
            blockUI.delay = 1000;
            PlanillaDespachosFactory.Consultar(FiltroPlanilla).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListadoPlanillas = response.data.Datos
                                $scope.ListadoNumeroDocumentosOrigen.push({
                                    Codigo: response.data.Datos[i].NumeroDocumento,
                                    NumeroDocumento: response.data.Datos[i].NumeroDocumento,
                                    ValorAnticipo: response.data.Datos[i].ValorAnticipo,
                                    ValorFleteTransportador: response.data.Datos[i].ValorFleteTransportador

                                });
                            }
                        }
                    }
                }, function (response) {
                });
        }

        function CargarLiquidaciones() {
            $scope.ListadoNumeroDocumentosOrigen = [];
            var FiltroLiquidacion = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                PlacaVehiculo: $scope.Modelo.Placa.Placa,
                Estado: 1,
                Aprobado: -1,
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO }
            };
            LiquidacionesFactory.Consultar(FiltroLiquidacion).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListadoNumeroDocumentosOrigen.push({
                                    Codigo: response.data.Datos[i].NumeroDocumento,
                                    NumeroDocumento: response.data.Datos[i].NumeroDocumento
                                });
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        //--Asginar AutoComplete Numero Documento Origen
        $scope.VolverMaster = function () {
            if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarComprobanteCausacion/' + $scope.Modelo.Numero;
            } else {
                document.location.href = '#!ConsultarComprobanteCausacion';
            }
        };

        $scope.MaskMayus = function () {
            //try { $scope.Descripcion = $scope.Descripcion.toUpperCase() } catch (e) { };
            MascaraMayusGeneral($scope);
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskPlaca = function () {
            try { $scope.Vehiculo.Placa = MascaraPlaca($scope.Vehiculo.Placa) } catch (e) { };
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };

    }]);