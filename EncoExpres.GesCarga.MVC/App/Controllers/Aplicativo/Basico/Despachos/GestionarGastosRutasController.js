﻿EncoExpresApp.controller("GestionarGastosRutasCtrl", ['$scope', '$timeout', 'RutasFactory', 'LegalizacionGastosRutaFactory', '$linq', 'blockUI', '$routeParams', 'PuestoControlesFactory', 'ValorCatalogosFactory', 'ConceptoGastosFactory',
    function ($scope, $timeout, RutasFactory, LegalizacionGastosRutaFactory, $linq, blockUI, $routeParams, PuestoControlesFactory, ValorCatalogosFactory, ConceptoGastosFactory,
    ) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Gestionar Gastos Rutas' }];
        $scope.Titulo = 'GESTIONAR GASTOS RUTAS'
        $scope.listadosRutas = [];
        $scope.ListaGastosRuta = [];
        $scope.ListadoTipoVehiculo = [];
        $scope.ListadoConceptosGastos = [];
        $scope.Master = "#!ConsultarGastosRutas";
        $scope.Modelo = {
           

            Valor: ""
        };

        //---------------------Validacion de permisos --------------------///
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GASTOS_RUTAS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);



        $scope.InitLoad = function () {
            //Listado Tipos Vehiculos 
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoVehiculo.push({ Codigo: -1, Nombre: '(TODOS)' });
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if (response.data.Datos[i].Codigo != CODIGO_TIPO_VEHICULO_NO_APLICA) {
                                    $scope.ListadoTipoVehiculo.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                                }
                            }
                        }
                        else {
                            $scope.ListadoTipoVehiculo = [];
                        }
                    }
                    $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == -1');
  
                }, function (response) {
                });
            //Listados Conceptos  
            ConceptoGastosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, ConceptoSistema: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoConceptosGastos.push({ Codigo: -1, Nombre: '(TODOS)' });
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                for (var i = 0; i < response.data.Datos.length; i++) {
                                    if (response.data.Datos[i].ConceptoSistema == 1) {
                                        if (response.data.Datos[i].Codigo == CODIGO_GASTO_COMBUSTIBLE_RENDIMIENTO_GALON) {
                                            $scope.ListadoConceptosGastos.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                                        }
                                    } else {
                                        $scope.ListadoConceptosGastos.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                                    }
                                }
                            }
                            else {
                                $scope.ListadoConceptosGastos = [];
                            }
                        }
                    }
                    $scope.Modelo.Concepto = $linq.Enumerable().From($scope.ListadoConceptosGastos).First('$.Codigo == -1');

                }, function (response) {
                    ShowError(response.statusText);
                });

            //Listado de rutas 
            RutasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: 1
            }).then(function (response) {
                $scope.listadosRutas = response.data.Datos;
                console.log('rutas', $scope.listadosRutas)
            }, function (response) {
                ShowError(response.statusText);
            });
           
        } 

        $scope.ConfirmarGuardar = function () { 
            showModal('modaConfirmar'); 
        };

        
        $scope.GuardarGastosRutas = function () {

            if (DatosRequeridos()) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Ruta: $scope.Modelo.Ruta,
                    Concepto: $scope.Modelo.Concepto,
                    TipoVehiculo: $scope.Modelo.TipoVehiculo,
                    Valor: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.Valor,)),
                    AplicaAnticipo: $scope.Modelo.AplicaAnticipo == true ? 1 : 0

                }  

                console.log('llegando', $scope.Modelo.Valor)
                console.log('enviando',filtros)
               
                LegalizacionGastosRutaFactory.Guardar(filtros).
                    then(function (response) {
                        console.log(response)
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se guardo el Gasto de la Ruta ' + $scope.Modelo.Ruta.Nombre)
                            location.href = $scope.Master
                                + '/' + $scope.Modelo.Ruta.Codigo
                                + '/' + $scope.Modelo.Concepto.Codigo
                                + '/' +  $scope.Modelo.TipoVehiculo.Codigo; 

                        } else {
                            ShowSuccess(' el gasto de  la ruta  ya se encuentra registrado')
                        
                        }
                    }, function (response) {
                            ShowError(' el gasto de  la ruta  ya se encuentra registrado')
                        //ShowError(response.statusText);
                    }); 

                closeModal('modaConfirmar'); 
            } else {
                console.log('No guardar')

            }

        }
         
        function DatosRequeridos() {
            var continuar = true;
            $scope.MensajesError = [];
            window.scrollTo(top, top); 

          
            if ($scope.Modelo.Ruta == undefined || $scope.Modelo.Ruta == "") {
                closeModal('modaConfirmar');
                $scope.MensajesError.push('Debe  Seleccionar Una Ruta');
                continuar = false;
            } 
            if ($scope.Modelo.TipoVehiculo.Codigo == -1) {
                closeModal('modaConfirmar');
                $scope.MensajesError.push('Debe Seleccionar un Vehiculo');
                continuar = false;
            }
            if ($scope.Modelo.Concepto.Codigo == -1) {
                closeModal('modaConfirmar');
                $scope.MensajesError.push('Debe  Seleccionar Concepto');
                continuar = false;
            } 

            return continuar;

        }

        $scope.Regresar = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!ConsultarGastosRutas';
            }
        }; 

        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
           
        }


        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };

        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
    }]);