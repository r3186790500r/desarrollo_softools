﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente
Imports EncoExpres.GesCarga.Negocio.Basico.ServicioCliente

''' <summary>
''' Controlador <see cref="TarifaTransporteCargaController"/>
''' </summary>
<Authorize>
Public Class TarifaTransporteCargaController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TarifaTransporteCarga) As Respuesta(Of IEnumerable(Of TarifaTransporteCarga))
        Return New LogicaTarifaTransporteCarga(CapaPersistenciaTarifaTransporteCarga).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TarifaTransporteCarga) As Respuesta(Of TarifaTransporteCarga)
        Return New LogicaTarifaTransporteCarga(CapaPersistenciaTarifaTransporteCarga).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TarifaTransporteCarga) As Respuesta(Of Long)
        Return New LogicaTarifaTransporteCarga(CapaPersistenciaTarifaTransporteCarga).Guardar(entidad)
    End Function

End Class
