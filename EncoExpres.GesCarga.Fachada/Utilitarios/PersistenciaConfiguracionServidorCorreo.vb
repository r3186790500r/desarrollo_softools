﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Utilitarios
Imports EncoExpres.GesCarga.Repositorio.Utilitarios

Namespace Utilitarios
    Public NotInheritable Class PersistenciaConfiguracionServidorCorreos
        Implements IPersistenciaBase(Of ConfiguracionServidorCorreos)
        Public Function Consultar(filtro As ConfiguracionServidorCorreos) As IEnumerable(Of ConfiguracionServidorCorreos) Implements IPersistenciaBase(Of ConfiguracionServidorCorreos).Consultar
            Return New RepositorioConfiguracionServidorCorreos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ConfiguracionServidorCorreos) As Long Implements IPersistenciaBase(Of ConfiguracionServidorCorreos).Insertar
            Return New RepositorioConfiguracionServidorCorreos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ConfiguracionServidorCorreos) As Long Implements IPersistenciaBase(Of ConfiguracionServidorCorreos).Modificar
            Return New RepositorioConfiguracionServidorCorreos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ConfiguracionServidorCorreos) As ConfiguracionServidorCorreos Implements IPersistenciaBase(Of ConfiguracionServidorCorreos).Obtener
            Return New RepositorioConfiguracionServidorCorreos().Obtener(filtro)
        End Function

        Public Function EnviarCorreoPrueba(entidad As ConfiguracionServidorCorreos) As String
            Return New RepositorioConfiguracionServidorCorreos().EnviarCorreoPrueba(entidad)
        End Function
    End Class
End Namespace
