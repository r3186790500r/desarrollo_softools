﻿
Print 'CREATE PROCEDURE gsp_obtener_notas_facturas'
GO
DROP PROCEDURE gsp_obtener_notas_facturas
GO
  
CREATE PROCEDURE gsp_obtener_notas_facturas (    
@par_EMPR_Codigo SMALLINT,    
@par_Numero NUMERIC = NULL,
@par_NumeroDocumento NUMERIC = NULL,
@par_TERC_Cliente NUMERIC = NULL,
@par_Estado NUMERIC = NULL,
@par_Anulado NUMERIC = NULL
)    
AS    
BEGIN    

	SELECT   
	1 As Obtener,        
	ENNF.EMPR_Codigo,        
	ENNF.Numero,
	ENNF.Numero_Documento AS ENNF_NumeroDocumento,
	ENNF.Numero_Preimpreso AS ENNF_NumeroPreimpreso,
	ENNF.TIDO_Codigo,        
	ENNF.Fecha AS ENNF_Fecha,

	ENFA.Numero AS ENFA_Numero,
	ENFA.Numero_Documento AS ENFA_NumeroDocumento,
	ENFA.Valor_Factura,
	ENFA.Fecha AS ENFA_Fecha,
	
	ENNF.OFIC_Nota,
	OFIC.Nombre As OFIC_NombreNota,
	
	ENNF.TERC_Cliente,
	ENNF.TERC_Facturar,
	
	CONF.Codigo AS CONF_Codigo,
	
	ENNF.Valor_Nota AS ENNF_Valor_Nota, 
	ISNULL(ENNF.Nota_Electronica,0) As Estado_NOEL,
	
	ENNF.Observaciones AS ENNF_Observaciones,
	ENNF.Estado AS ENNF_Estado,
	ENNF.Anulado AS ENNF_Anulado
	
	FROM Encabezado_Notas_Facturas ENNF
	
	INNER JOIN        
	Oficinas OFIC ON        
	ENNF.EMPR_Codigo = OFIC.EMPR_Codigo        
	AND ENNF.OFIC_Nota = OFIC.Codigo
	
	INNER JOIN
	Conceptos_Notas_Facturas AS CONF ON
	ENNF.EMPR_Codigo = CONF.EMPR_Codigo
	AND ENNF.CONF_Codigo = CONF.Codigo
	
	INNER JOIN
	Encabezado_Facturas AS ENFA ON
	ENNF.EMPR_Codigo = ENFA.EMPR_Codigo
	AND ENNF.ENFA_Numero = ENFA.Numero
	
	WHERE ENNF.EMPR_Codigo = @par_EMPR_Codigo   
	AND (ENNF.Numero = @par_Numero OR @par_Numero IS NULL)
	AND (ENNF.Numero_Documento = @par_NumeroDocumento OR @par_NumeroDocumento IS NULL)
	AND (ENNF.TERC_Cliente = @par_TERC_Cliente OR @par_TERC_Cliente IS NULL)
	AND (ENNF.Estado = @par_Estado OR @par_Estado IS NULL)
	AND (ENNF.Anulado = @par_Anulado OR @par_Anulado IS NULL)
END    
GO