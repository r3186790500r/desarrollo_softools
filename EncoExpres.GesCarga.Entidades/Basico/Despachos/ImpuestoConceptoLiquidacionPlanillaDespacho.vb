﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="ImpuestoConceptoLiquidacionPlanillaDespacho"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ImpuestoConceptoLiquidacionPlanillaDespacho
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImpuestoConceptoLiquidacionPlanillaDespacho"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImpuestoConceptoLiquidacionPlanillaDespacho"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            Codigo = Read(lector, "Codigo")

            Nombre = Read(lector, "Nombre")
            Operacion = Read(lector, "Operacion")
            Estado = Read(lector, "Estado")
            Valor_tarifa = Read(lector, "Valor_Tarifa")
            Valor_base = Read(lector, "Valor_Base")
            ValorImpuesto = Read(lector, "Valor_Impuesto")
            PUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo")}
            Try
                ConceptoLiquidacion = New ConceptoLiquidacionPlanillaDespacho With {.Codigo = Read(lector, "CLPD_Codigo")}
            Catch ex As Exception
            End Try

        End Sub
        <JsonProperty>
        Public Property Valor_tarifa As Double

        <JsonProperty>
        Public Property Valor_base As Double

        <JsonProperty>
        Public Property ValorImpuesto As Double

        <JsonProperty>
        Public Property Placa As String

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property ConceptoSistema As Integer

        <JsonProperty>
        Public Property AplicaImpuesto As Integer

        <JsonProperty>
        Public Property Operacion As Integer
        <JsonProperty>
        Public Property PUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property ConceptoLiquidacion As ConceptoLiquidacionPlanillaDespacho
        <JsonProperty>
        Public Property ListadoImpuestos As IEnumerable(Of Impuestos)


    End Class
End Namespace
