﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Seguridad

''' <summary>
''' Clase <see cref="Respuesta(Of T)"/>
''' </summary>
''' <typeparam name="T">Tipo de dato asociado a la respuesta.</typeparam>
<JsonObject>
Public NotInheritable Class Respuesta(Of T)

    ''' <summary>
    ''' Inicializa una nueva instancia de la clase <see cref="Respuesta(Of T)"/>
    ''' </summary>
    Sub New()
    End Sub

    ''' <summary>
    ''' Inicializa una nueva instancia de la clase <see cref="Respuesta(Of T)"/>
    ''' </summary>
    ''' <param name="mensajeOperacion">Mensaje error generado por el proceso ejecutado.</param>
    Sub New(mensajeOperacion As String)
        Me.MensajeOperacion = mensajeOperacion
    End Sub

    ''' <summary>
    ''' Inicializa una nueva instancia de la clase <see cref="Respuesta(Of T)"/>
    ''' </summary>
    ''' <param name="datos">Información resultante de la ejecución del proceso.</param>
    Sub New(datos As T)
        Me.Datos = datos
        ProcesoExitoso = True
    End Sub

    ''' <summary>
    ''' Inicializa una nueva instancia de la clase <see cref="Respuesta(Of T)"/>
    ''' </summary>
    ''' <param name="procesoExitoso">if set to <c>true</c> [proceso exitoso].</param>
    Sub New(procesoExitoso As Boolean)
        Me.ProcesoExitoso = procesoExitoso
    End Sub

    ''' <summary>
    ''' Inicializa una nueva instancia de la clase <see cref="Respuesta(Of T)"/>
    ''' </summary>
    ''' <param name="procesoExitoso">if set to <c>true</c> [proceso exitoso].</param>
    ''' <param name="mensajeOperacion">Mensaje error generado por el proceso ejecutado.</param>
    Sub New(procesoExitoso As Boolean, mensajeOperacion As String)
        Me.ProcesoExitoso = procesoExitoso
        Me.MensajeOperacion = mensajeOperacion
    End Sub

    ''' <summary>
    ''' Obtiene o establece el mensaje error generado por el proceso ejecutado.
    ''' </summary>
    <JsonProperty>
    Public Property MensajeOperacion As String

    ''' <summary>
    ''' Obtiene o establece la información retornada por el proceso ejecutado.
    ''' </summary>
    <JsonProperty>
    Public Property Datos As T

    ''' <summary>
    ''' Obtiene o establece si el porceso ejecutado fianlizó exitosamente.
    ''' </summary>
    <JsonProperty>
    Public Property ProcesoExitoso As Boolean
    <JsonProperty>
    Public Property Numero As Long

    Public Property Cantidad_Detalles As Object

    Public Shared Widening Operator CType(v As Respuesta(Of EntidadOrigenEstudioSeguridad)) As Respuesta(Of T)
        Throw New NotImplementedException()
    End Operator
End Class