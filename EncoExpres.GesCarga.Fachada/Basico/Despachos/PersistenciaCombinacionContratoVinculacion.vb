﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaCombinacionContratoVinculacion
        Implements IPersistenciaBase(Of CombinacionContratoVinculacion)

        Public Function Consultar(filtro As CombinacionContratoVinculacion) As IEnumerable(Of CombinacionContratoVinculacion) Implements IPersistenciaBase(Of CombinacionContratoVinculacion).Consultar
            Return New RepositorioCombinacionContratoVinculacion().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As CombinacionContratoVinculacion) As Long Implements IPersistenciaBase(Of CombinacionContratoVinculacion).Insertar
            Return New RepositorioCombinacionContratoVinculacion().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As CombinacionContratoVinculacion) As Long Implements IPersistenciaBase(Of CombinacionContratoVinculacion).Modificar
            Return New RepositorioCombinacionContratoVinculacion().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As CombinacionContratoVinculacion) As CombinacionContratoVinculacion Implements IPersistenciaBase(Of CombinacionContratoVinculacion).Obtener
            Return New RepositorioCombinacionContratoVinculacion().Obtener(filtro)
        End Function
        Public Function Anular(entidad As CombinacionContratoVinculacion) As Boolean
            Return New RepositorioCombinacionContratoVinculacion().Anular(entidad)
        End Function
    End Class
End Namespace