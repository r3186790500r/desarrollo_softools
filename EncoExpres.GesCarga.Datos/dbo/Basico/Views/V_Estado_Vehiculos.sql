﻿Print 'V_Estado_Vehiculos'
GO
DROP VIEW V_Estado_Vehiculos
GO
CREATE VIEW V_Estado_Vehiculos 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 25
GO
