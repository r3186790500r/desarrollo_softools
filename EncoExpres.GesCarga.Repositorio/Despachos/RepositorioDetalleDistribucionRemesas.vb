﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa

Namespace Despachos.Remesa

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleDistribucionRemesas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleDistribucionRemesas
        Inherits RepositorioBase(Of DetalleDistribucionRemesas)

        Public Overrides Function Consultar(filtro As DetalleDistribucionRemesas) As IEnumerable(Of DetalleDistribucionRemesas)
            Dim lista As New List(Of DetalleDistribucionRemesas)
            Dim item As DetalleDistribucionRemesas
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    If filtro.NumeroRemesa > 0 Then
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", filtro.NumeroRemesa)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_distribucion_remesas]")

                    While resultado.Read
                        item = New DetalleDistribucionRemesas(resultado)
                        lista.Add(item)
                    End While

                    conexion.CloseConnection()

                End Using
            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleDistribucionRemesas) As Long
            Dim inserto As Boolean = True
            'Insertar Destinatario
            Dim tercero = New Repositorio.Basico.General.RepositorioTerceros
            If Not IsNothing(entidad.Destinatario) Then
                If entidad.Destinatario.Codigo = 0 Then
                    entidad.Destinatario.CodigoEmpresa = entidad.CodigoEmpresa
                    If entidad.Destinatario.TipoIdentificacion.Codigo = 102 Then
                        entidad.Destinatario.TipoNaturaleza = New Entidades.Basico.General.ValorCatalogos With {.Codigo = 502}
                    Else
                        entidad.Destinatario.TipoNaturaleza = New Entidades.Basico.General.ValorCatalogos With {.Codigo = 501}
                    End If
                    entidad.Destinatario.Nombre = entidad.Destinatario.NombreCompleto
                    entidad.Destinatario.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                    entidad.Destinatario.CadenaPerfiles = "1404,1410," ' Destinatario y Remitente
                    entidad.Destinatario.Estado = New Entidades.Comercial.Documentos.Estado With {.Codigo = 1}
                    entidad.Destinatario.Codigo = tercero.Insertar(entidad.Destinatario)
                End If
            End If
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
                If Not IsNothing(entidad.Producto) Then
                    If entidad.Producto.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.UnidadEmpaque) Then
                    If entidad.UnidadEmpaque.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_UEPT_Codigo", entidad.UnidadEmpaque.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Peso", entidad.Peso, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Documento_Cliente", entidad.DocumentoCliente)
                conexion.AgregarParametroSQL("@par_Fecha_Documento_Cliente", entidad.FechaDocumentoCliente, SqlDbType.Date)
                If Not IsNothing(entidad.Destinatario) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Destinatario", entidad.Destinatario.Codigo)
                    If Not IsNothing(entidad.Destinatario.TipoIdentificacion) Then
                        conexion.AgregarParametroSQL("@par_CATA_TIID_Codigo", entidad.Destinatario.TipoIdentificacion.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_Numero_Identificacion_Destinatario", entidad.Destinatario.NumeroIdentificacion)
                    conexion.AgregarParametroSQL("@par_Nombre_Destinatario", entidad.Destinatario.NombreCompleto)
                    If Not IsNothing(entidad.Destinatario.Ciudad) Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destinatario", entidad.Destinatario.Ciudad.Codigo)
                    End If
                    If Not IsNothing(entidad.SitioEntrega) Then
                        conexion.AgregarParametroSQL("@par_SICD_Codigo_Entrega", entidad.SitioEntrega.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_Barrio_Destinatario", entidad.Destinatario.Barrio)
                    conexion.AgregarParametroSQL("@par_Direccion_Destinatario", entidad.Destinatario.Direccion)
                    conexion.AgregarParametroSQL("@par_Codigo_Postal_Destinatario", entidad.Destinatario.CodigoPostal)
                    conexion.AgregarParametroSQL("@par_Telefonos_Destinatario", entidad.Destinatario.Telefonos)

                End If
                If Not IsNothing(entidad.ZonaDestinatario) Then
                    conexion.AgregarParametroSQL("@par_ZOCI_Codigo_Destinatario", entidad.ZonaDestinatario.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Observaciones_Entrega", entidad.Observaciones)
                If Not IsNothing(entidad.EstadoDistribucionRemesas) Then
                    If entidad.EstadoDistribucionRemesas.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_ESDR_Codigo", entidad.EstadoDistribucionRemesas.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_distribucion_remesas")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()
                conexion.CloseConnection()

            End Using

            Return entidad.Codigo

        End Function

        Public Function Anular(entidad As DetalleDistribucionRemesas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_detalle_distribucion_remesas]")


                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > Cero, True, False)
                End While
                resultado.Close()

            End Using

            Return anulo
        End Function

        Public Overrides Function Modificar(entidad As DetalleDistribucionRemesas) As Long

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad_Recibe", entidad.CantidadRecibe, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso_Recibe", entidad.PesoRecibe, SqlDbType.Money)
                    'conexion.AgregarParametroSQL("@par_Fecha_Recibe", entidad.FechaRecibe, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_CATA_TIID_Codigo_Recibe", entidad.CodigoTipoIdentificacionRecibe)
                    conexion.AgregarParametroSQL("@par_Numero_Identificacion_Recibe", entidad.NumeroIdentificacionRecibe)
                    conexion.AgregarParametroSQL("@par_Nombre_Recibe", entidad.NombreRecibe)
                    conexion.AgregarParametroSQL("@par_Telefonos_Recibe", entidad.TelefonoRecibe)
                    conexion.AgregarParametroSQL("@par_Observaciones_Recibe", entidad.ObservacionesRecibe)
                    conexion.AgregarParametroSQL("@par_Firma", entidad.Firma, SqlDbType.Image)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                    If Not IsNothing(entidad.EstadoDistribucionRemesas) Then
                        conexion.AgregarParametroSQL("@par_CATA_ESDR_Codigo", entidad.EstadoDistribucionRemesas.Codigo)
                    End If


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_detalle_distribucion_remesas")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()
                    conexion.CloseConnection()

                    If entidad.Codigo.Equals(Cero) Then
                        transaccion.Dispose()
                        Return Cero
                    Else
                        transaccion.Complete()
                    End If

                End Using

            End Using

            If entidad.Codigo > 0 Then
                If Not IsNothing(entidad.Fotografia) Then

                    entidad.Fotografia.CodigoEmpresa = entidad.CodigoEmpresa
                    entidad.Fotografia.NumeroRemesa = entidad.NumeroRemesa
                    entidad.Fotografia.Codigo = entidad.Codigo
                    entidad.Fotografia.CodigoUsuarioCrea = entidad.UsuarioModifica.Codigo

                    InsertarFoto(entidad.Fotografia)

                End If
            End If

            Return entidad.Codigo

        End Function

        Public Overrides Function Obtener(filtro As DetalleDistribucionRemesas) As DetalleDistribucionRemesas
            Dim item As New DetalleDistribucionRemesas

            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", filtro.NumeroRemesa)
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_distribucion_remesas")

                    While resultado.Read
                        item = New DetalleDistribucionRemesas(resultado)
                    End While
                    resultado.Close()
                    conexion.CloseConnection()

                    If item.Codigo > 0 Then
                        item.Fotografia = ConsultarFoto(filtro.CodigoEmpresa, filtro.NumeroRemesa, filtro.Codigo, conexionDocumentos, resultado)
                    End If

                End Using

            End Using
            Return item
        End Function

        Public Function InsertarFoto(entidad As FotoDetalleDistribucionRemesas) As Boolean
            Dim Inserto As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)

                conexion.AgregarParametroSQL("@par_Foto", entidad.FotoBits, SqlDbType.VarBinary)
                conexion.AgregarParametroSQL("@par_Nombre_Foto", entidad.NombreFoto)
                conexion.AgregarParametroSQL("@par_Extension_Foto", entidad.ExtensionFoto)
                conexion.AgregarParametroSQL("@par_Tipo_Foto", entidad.TipoFoto)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea)
                conexion.AgregarParametroSQL("@par_Entrega_Devolucion", entidad.EntregaDevolucion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_foto_detalle_distribucion_remesas]")

                While resultado.Read
                    Inserto = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

                resultado.Close()
                conexion.CloseConnection()

            End Using

            Return Inserto
        End Function

        Public Function ConsultarFoto(CodiEmpresa As Short, NumeRemesa As Long, CodiDistribucion As Long, ByRef conexionDocumentos As Conexion.DataBaseFactory, resultado As IDataReader) As FotoDetalleDistribucionRemesas
            Dim item As New FotoDetalleDistribucionRemesas

            conexionDocumentos.CreateConnection()
            conexionDocumentos.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexionDocumentos.AgregarParametroSQL("@par_ENRE_Numero", NumeRemesa)
            conexionDocumentos.AgregarParametroSQL("@par_DEDR_Codigo", CodiDistribucion)

            resultado = conexionDocumentos.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_foto_detalle_distribucion_remesas]")

            While resultado.Read
                item = New FotoDetalleDistribucionRemesas(resultado)
            End While
            resultado.Close()
            conexionDocumentos.CloseConnection()

            Return item
        End Function

    End Class

End Namespace