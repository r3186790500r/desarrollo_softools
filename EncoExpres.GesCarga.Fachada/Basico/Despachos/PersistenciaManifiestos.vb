﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Repositorio.Despachos

Namespace Basico.General
    Public NotInheritable Class PersistenciaManifiestos
        Implements IPersistenciaBase(Of Manifiesto)

        Public Function Consultar(filtro As Manifiesto) As IEnumerable(Of Manifiesto) Implements IPersistenciaBase(Of Manifiesto).Consultar
            Return New RepositorioManifiesto().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Manifiesto) As Long Implements IPersistenciaBase(Of Manifiesto).Insertar
            Return New RepositorioManifiesto().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Manifiesto) As Long Implements IPersistenciaBase(Of Manifiesto).Modificar
            Return New RepositorioManifiesto().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Manifiesto) As Manifiesto Implements IPersistenciaBase(Of Manifiesto).Obtener
            Return New RepositorioManifiesto().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Manifiesto) As Manifiesto
            Return New RepositorioManifiesto().Anular(entidad)
        End Function
    End Class
End Namespace

