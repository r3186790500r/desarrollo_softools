﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref="TareaMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TareaMantenimiento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TareaMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TareaMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            Fecha = Read(lector, "Fecha")
            Anulado = Read(lector, "Anulado")
            EquipoMantenimiento = New EquipoMantenimiento With {.Codigo = Read(lector, "EQMA_Codigo"), .Nombre = Read(lector, "EquipoMantenimiento")}
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Placa")}
            Estado = Read(lector, "Estado")
            Descripcion = Read(lector, "Descripcion")
            TotalRegistros = Read(lector, "TotalRegistros")
            Nombre = Read(lector, "Nombre")



        End Sub
        ''' <summary>
        ''' Obtiene o establece el equipo  de la tarea de mantenimiento
        ''' </summary>
        <JsonProperty>
        Public Property EquipoMantenimiento As EquipoMantenimiento

        ''' <summary>
        ''' Obtiene o establece el vehiculo de la tarea de mantenimiento
        ''' </summary>
        <JsonProperty>
        Public Property Vehiculo As Vehiculos

        ''' <summary>
        ''' Obtiene o establece la descripcion de la tarea de mantenimiento
        ''' </summary>
        <JsonProperty>
        Public Property Descripcion As String

        ''' <summary>
        ''' Obtiene o establece el detalle de la tarea de mantenimiento
        ''' </summary>
        <JsonProperty>
        Public Property Detalles As IEnumerable(Of DetalleTareaMantenimiento)

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Placa As String


    End Class
End Namespace
