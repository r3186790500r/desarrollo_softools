﻿Print 'gsp_Listado_Remesa_pendientes_facturar'
GO
DROP PROCEDURE gsp_Listado_Remesa_pendientes_facturar
GO
CREATE PROCEDURE gsp_Listado_Remesa_pendientes_facturar
(
	@par_EMPR_Codigo NUMERIC,              
	@par_Numero_Documento_Inicial NUMERIC = null,              
	@par_Numero_Documento_Final NUMERIC = null,              
	@par_Fecha_Incial DATE = NULL,               
	@par_Fecha_Final DATE = NULL,               
	@par_Cliente VARCHAR(MAX) = NULL,                   
	@par_OFIC_Codigo NUMERIC = NULL ,                  
	@par_Estado NUMERIC = NULL ,                  
	@par_Ruta VARCHAR(50) = NULL 
)              
AS BEGIN

	DECLARE @intAnulado SMALLINT = NULL
	SET @intAnulado = CASE @par_Estado WHEN NULL THEN NULL WHEN 2 THEN 1 ELSE 0 END
    SELECT @par_Estado = IIF (@par_Estado = 2,NULL,@par_Estado)
               
	SELECT               
	EMPR.Nombre_Razon_Social              
	,ENRE.Numero_Documento AS Remesa              
	, ENRE.FECHA               
	, ESOS.Numero_Documento AS OrdenServicio              
	, ENRE.Documento_Cliente AS  Documento_Cliente              
	, ENFA.Numero_Documento AS NumeroFactura              
	,ENMA.Numero_Documento AS Manifiesto           
	, ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'')                
	+' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS NombreCliente              
	, ETCV.Nombre AS TarifarioCompra              
	, RUTA.Nombre AS NombreRuta              
	, VEHI.Placa              
	, ENRE.Peso_Cliente              
	, ENRE.Cantidad_Cliente              
	, ENRE.Valor_Flete_Cliente               
	,ENRE.Valor_Flete_Transportador                
	,OFIC.Nombre AS OFICINA              
	,ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial              
	,ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal              
	,ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial              
	,ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal              
	,ENRE.Anulado           
	,ENMA.Numero_Documento NumeroManifiesto         
	,CASE ENRE.Anulado WHEN 1 THEN 'A' ELSE           
	  CASE ENRE.ESTADO WHEN 1 THEN 'D' ELSE 'B'          
          
	  END           
	END as Estado          
	 ,LNTC.Nombre LineaNegocio        
	 ,TLNC.Nombre TipoLineaNegocio        
          
	FROM Encabezado_Remesas   ENRE              
	 LEFT JOIN Encabezado_Facturas ENFA   on            
	 ENRE.EMPR_Codigo = ENFA.EMPR_Codigo              
	AND ENRE.ENFA_Numero = ENFA.Numero              
            
	 LEFT JOIN Terceros TECL   on            
	  ENRE.EMPR_Codigo = TECL.EMPR_Codigo              
	AND ENRE.TERC_Codigo_Cliente = TECL.Codigo             
         
	 LEFT JOIN Empresas EMPR   on            
	   ENRE.EMPR_Codigo = EMPR.Codigo            
              
	 LEFT JOIN Rutas RUTA    on            
	 ENRE.EMPR_Codigo = RUTA.EMPR_Codigo              
	AND ENRE.RUTA_Codigo = RUTA.Codigo               
              
	 LEFT JOIN Vehiculos VEHI   on            
	 ENRE.EMPR_Codigo = VEHI.EMPR_Codigo              
	AND ENRE.VEHI_Codigo = VEHI.Codigo              
        
	 LEFT JOIN Encabezado_Tarifario_Carga_Ventas ETCV    on            
	 ENRE.EMPR_Codigo = ETCV.EMPR_Codigo               
	AND ENRE.ETCV_Numero = ETCV.Numero               
        
	 LEFT JOIN Oficinas OFIC   on            
	  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo              
	AND ENRE.OFIC_Codigo = OFIC.Codigo               
          
	LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS           
	ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo           
	AND ENRE.ESOS_Numero = ESOS.Numero          
          
	LEFT JOIN Encabezado_Manifiesto_Carga ENMA ON           
	ENRE.EMPR_Codigo = ENMA.EMPR_Codigo          
	AND ENRE.ENMC_Numero = ENMA.Numero          
        
	LEFT JOIN Detalle_Despacho_Orden_Servicios DDOS ON         
	DDOS.EMPR_Codigo = ENRE.EMPR_Codigo         
	AND DDOS.ENRE_Numero = ENRE.Numero         
        
	LEFT JOIN Linea_Negocio_Transporte_Carga LNTC ON         
	DDOS.EMPR_Codigo = LNTC.EMPR_Codigo         
	AND DDOS.LNTC_Codigo = LNTC.Codigo        
        
	LEFT JOIN Tipo_Linea_Negocio_Carga TLNC ON         
	DDOS.EMPR_Codigo = TLNC.EMPR_Codigo         
	AND DDOS.TLNC_Codigo = TLNC.Codigo         
        
          
	WHERE        
	ENRE.EMPR_Codigo = @par_EMPR_Codigo              
	AND ENRE.TIDO_Codigo=100               
	AND (ENRE.ENFA_Numero = 0         
	OR  ENRE.ENFA_Numero = null )  
	AND ENRE.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENRE.Numero_Documento)              
	AND ENRE.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENRE.Numero_Documento)               
	AND ENRE.EMPR_Codigo =  @par_EMPR_Codigo               
	AND ENRE.Fecha >= ISNULL( @par_Fecha_Incial,ENRE.Fecha)               
	AND ENRE.Fecha <= ISNULL( @par_Fecha_Final,ENRE.Fecha)               
	AND (TECL.Nombre + ' ' +TECL.Apellido1 + ' ' + TECL.Apellido2 + ' ' + TECL.Razon_Social LIKE '%'+@par_Cliente+'%' or @par_Cliente is null)               
	AND  ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)
	AND ENRE.Estado = ISNULL (@par_Estado,ENRE.Estado)                 
	AND ENRE.Anulado = ISNULL(@intAnulado, ENRE.Anulado)
	AND  (RUTA.Nombre LIKE '%'+@par_Ruta+'%' or @par_Ruta is null)           
                   
END               
GO