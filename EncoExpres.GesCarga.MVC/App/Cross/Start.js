﻿$(document).ready(function () {
    //$(".navbar-nav li a").click(function (event) {
    //    $(".navbar-collapse").collapse('hide');
    //});
});

$(function () {
    var form = $(".login-form");

    form.css({
        opacity: 1,
        "-webkit-transform": "scale(1)",
        "transform": "scale(1)",
        "-webkit-transition": ".5s",
        "transition": ".5s"
    });

    $(".select2").select2();
});

function showDialog(id) {
    var dialog = $("#" + id).data('dialog');
    if (!dialog.element.data('opened')) {
        dialog.open();
    } else {
        dialog.close();
    }
}

function showModal(id) {
    $("#" + id).modal('show');
}

function closeModal(id, opt) {
    $("#" + id).hide();
    $("#" + id).modal('hide');
    if (opt != undefined && opt == 1) {
        setTimeout("$('body').addClass('modal-open')", 1000)
    }
    else {
        setTimeout("$('body').removeClass('modal-open')", 1000)

    }
}
function QuitarModals(e) {
    var a
    a
}
function backToTop() {
    $('html,body,div').animate({ scrollTop: 0 }, 'slow');
}

function HideMenu() {
    $(".navbar-collapse").collapse('hide');
}

function ObtenerMinutosUTC(date) {
    var minutos = date.getTimezoneOffset();
    return minutos;
}

function ObtenerIntervaloTiempo(date1, date2, interval) {
    var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
    date1 = new Date(date1);
    date2 = new Date(date2);
    var timediff = date2 - date1;
    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years": return date2.getFullYear() - date1.getFullYear();
        case "months": return (
            (date2.getFullYear() * 12 + date2.getMonth())
            -
            (date1.getFullYear() * 12 + date1.getMonth())
        );
        case "weeks": return Math.floor(timediff / week);
        case "days": return Math.floor(timediff / day);
        case "hours": return Math.floor(timediff / hour);
        case "minutes": return Math.floor(timediff / minute);
        case "seconds": return Math.floor(timediff / second);
        default: return undefined;
    }
}

function Notificacion(htmlElement) {

    this.htmlElement = htmlElement;
    this.icon = htmlElement.querySelector('.icon > i');
    this.text = htmlElement.querySelector('.text');
    this.close = htmlElement.querySelector('.close');
    this.isRunning = false;
    this.timeout;

    this.bindEvents();
};

Notificacion.prototype.bindEvents = function () {
    var self = this;
    this.close.addEventListener('click', function () {
        window.clearTimeout(self.timeout);
        self.reset();
    });
}

Notificacion.prototype.info = function (message) {
    this.reset();
    if (this.isRunning) return false;

    this.text.innerHTML = message;
    this.htmlElement.className = 'notification info';
    this.icon.className = 'fa fa-2x fa-info-circle';

    this.show();
}

Notificacion.prototype.warning = function (message) {
    this.reset();
    if (this.isRunning) return false;

    this.text.innerHTML = message;
    this.htmlElement.className = 'notification warning';
    this.icon.className = 'fa fa-2x fa-exclamation-triangle';

    this.show();
}

Notificacion.prototype.error = function (message) {
    this.reset();
    if (this.isRunning) return false;

    this.text.innerHTML = message;
    this.htmlElement.className = 'notification error';
    this.icon.className = 'fa fa-2x fa-exclamation-circle';

    this.show();
}

Notificacion.prototype.show = function () {
    if (!this.htmlElement.classList.contains('visible'))
        this.htmlElement.classList.add('visible');

    this.isRunning = true;
    this.autoReset();
};

Notificacion.prototype.autoReset = function () {
    var self = this;
    this.timeout = window.setTimeout(function () {
        self.reset();
    }, 2000);
}

Notificacion.prototype.reset = function () {
    this.htmlElement.className = "notification";
    this.icon.className = "";
    this.isRunning = false;
};

//document.addEventListener('DOMContentLoaded', init);

function init() {
    var info = document.getElementById('info');
    var warn = document.getElementById('warn');
    var error = document.getElementById('error');

    var notificator = new Notificacion(document.querySelector('.notification'));

    info.onclick = function () {
        notificator.info('Esta es una información');
    }

    warn.onclick = function () {
        notificator.warning('Te te te advieeeerto!');
    }

    error.onclick = function () {
        notificator.error('Le causaste derrame al sistema');
    }
}

