﻿EncoExpresApp.controller("ConsultarListadoBasicoCtrl", ['$scope', '$timeout', 'TercerosFactory', 'RutasFactory', 'OficinasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'EmpresasFactory', 'VehiculosFactory', 'SemirremolquesFactory', 'CiudadesFactory', 'PaisesFactory',
    function ($scope, $timeout, TercerosFactory, RutasFactory, OficinasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, EmpresasFactory, VehiculosFactory, SemirremolquesFactory, CiudadesFactory, PaisesFactory) {

        $scope.MapaSitio = [{ Nombre: 'Basico' }, { Nombre: 'Listados' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.ListadoCliente = [];
        $scope.FiltroTercero = true;
        $scope.FiltroConductor = false;
        $scope.FiltroPlaca = false;
        $scope.FiltroSemirremolque = false;
        $scope.FiltroRuta = false;
        $scope.FiltroSitios = false;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ModeloSemirremolque = [];
        $scope.ModeloPlaca = [];
        $scope.ModeloRuta = [];
        $scope.ModeloPais = [];
        $scope.ModeloCiudad = [];
        $scope.ModeloTipoSitio = [];
        $scope.ListadoModuloAplicaciones = [];
        $scope.ListaDespacho = [];
        $scope.ListadoConductor = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} }
        }

        $scope.ListadoModuloAplicaciones.push({ Nombre: '(Seleccione Modulo)', Codigo: -1 })
        $scope.ListaDespacho.push({ Nombre: '(Seleccione Modulo)', Codigo: -1 })
        $scope.ListadoMenu.forEach(function (item) {
            if (item.Padre == OPCION_MENU_BASICO) {
                if (item.Codigo != 1007) {
                    $scope.ListadoModuloAplicaciones.push(item)
                }
            }
        });
        $scope.ModeloModuloBasico = $linq.Enumerable().From($scope.ListadoModuloAplicaciones).First('$.Codigo == -1 ');
        $scope.ModeloDocumentosDespachos = $scope.ListaDespacho[0]



        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_LISTADOS_BASICO);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }





        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;


        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });


        /*Cargar el combo de ESTADO DE REMESAS PAQUETERIA*/

        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 1, Nombre: 'ACTIVO' }, { Codigo: 0, Nombre: 'INACTIVO' }];
        $scope.ModeloEstado = $scope.ListadoEstadoRemesaPaqueteria[0];


        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)

        };




        /*Cargar el combo de tipo sitios*/
        ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SITIO }
        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: 'Seleccione tipo sitio', Codigo: 0 })
                        $scope.ListadoSitios = response.data.Datos
                        if ($scope.CodigoSitio !== undefined && $scope.CodigoSitio !== '' && $scope.CodigoSitio !== null) {
                            $scope.ModeloTipoSitio = $linq.Enumerable().From($scope.ListadoSitios).First('$.Codigo ==' + $scope.CodigoSitio);
                        } else {
                            $scope.ModeloTipoSitio = $linq.Enumerable().From($scope.ListadoSitios).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListadoSitios = []
                    }
                }
            }, function (response) {
            });


        $scope.asignarlistado = function (CodigoLista) {

            $scope.ListaDespacho = [];
            $scope.ListaDespacho.push({ Nombre: '(Seleccione Listado)', Codigo: -1 })
            $scope.ListadoMenuListados.forEach(function (item) {
                if (item.Padre == CodigoLista) {
                    $scope.ListaDespacho.push(item)
                }
            });
            $scope.ModeloDocumentosDespachos = $scope.ListaDespacho[0]
        }

        $scope.AsignarFiltro = function (CodigoLista) {

            if (CodigoLista == CODIGO_LITADO_TERCEROS) {
                $scope.FiltroTercero = true;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = false;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = false;
                $scope.FiltroNombre = false;
            }
            if (CodigoLista == CODIGO_LITADO_VEHICULO) {
                $scope.FiltroTercero = false;
                $scope.FiltroConductor = true;
                $scope.FiltroPlaca = true;
                $scope.FiltroSemirremolque = true;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = false;
                $scope.FiltroNombre = false;
            }
            if (CodigoLista == CODIGO_LITADO_SEMIRREMOLQUE) {
                $scope.FiltroTercero = true;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = true;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = false;
                $scope.FiltroNombre = false;
            }
            if (CodigoLista == CODIGO_LITADO_SITIO_CARGUE) {
                $scope.FiltroTercero = false;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = false;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = true;
                $scope.FiltroNombre = false;
            }
            if (CodigoLista == CODIGO_LITADO_RUTA) {
                $scope.FiltroTercero = false;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = false;
                $scope.FiltroRuta = true;
                $scope.FiltroSitios = false;
                $scope.FiltroNombre = false;
            }
            if (CodigoLista == CODIGO_LITADO_IMPUESTOS) {
                $scope.FiltroTercero = false;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = false;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = false;
                $scope.FiltroCiudad = true;
                $scope.FiltroNombre = true;
            }
            if (CodigoLista == CODIGO_LITADO_NOVEDADES_DESPACHO) {
                $scope.FiltroTercero = false;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = false;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = false;
                $scope.FiltroCiudad = false;
                $scope.FiltroNombre = true;
            }
            if (CodigoLista == CODIGO_LITADO_CONCEPTOS_LIQUIDACION) {
                $scope.FiltroTercero = false;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = false;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = false;
                $scope.FiltroCiudad = false;
                $scope.FiltroNombre = true;
            }
            if (CodigoLista == CODIGO_LITADO_FACTURACION) {
                $scope.FiltroTercero = false;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = false;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = false;
                $scope.FiltroCiudad = false;
                $scope.FiltroNombre = true;
            }

        }


        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);




            //Depende del listado seleccionado se enviará el nombre por parametro

            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_TERCEROS) {
                $scope.NombreReporte = NOMBRE_LITADO_TERCERO
            }

            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_VEHICULO) {
                $scope.NombreReporte = NOMBRE_LITADO_VEHICULO
            }

            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_SEMIRREMOLQUE) {
                $scope.NombreReporte = NOMBRE_LITADO_SEMIRREMOLQUES
            }

            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_SITIO_CARGUE) {
                $scope.NombreReporte = NOMBRE_LITADO_SITIO_CARGUE_DESCARGUE
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_RUTA) {
                $scope.NombreReporte = NOMBRE_LITADO_RUTAS
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_PRODUCTOS_TRANSPORTADOS) {
                $scope.NombreReporte = NOMBRE_LITADO_PRODUCTOS_TRANSPORTADOS
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_COLORES_VEHICULOS) {
                $scope.NombreReporte = NOMBRE_LITADO_COLORES_VEHICULOS
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_MARCAS_VEHICULOS) {
                $scope.NombreReporte = NOMBRE_LITADO_MARCAS_VEHICULOS
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_LINEAS_VEHICULOS) {
                $scope.NombreReporte = NOMBRE_LITADO_LINEAS_VEHICULOS
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_MARCAS_SEMIRREMOLQUES) {
                $scope.NombreReporte = NOMBRE_LITADO_MARCAS_SEMIRREMOLQUES
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_PUESTOS_CONTROL) {
                $scope.NombreReporte = NOMBRE_LITADO_PUESTOS_CONTROL
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_IMPUESTOS) {
                $scope.NombreReporte = NOMBRE_LITADO_IMPUESTOS
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_NOVEDADES_DESPACHO) {
                $scope.NombreReporte = NOMBRE_LITADO_NOVEDADES_DESPACHO
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_CONCEPTOS_LIQUIDACION) {
                $scope.NombreReporte = NOMBRE_LITADO_CONCEPTOS_LIQUIDACION
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_FACTURACION) {
                $scope.NombreReporte = NOMBRE_LITADO_CONCEPTOS_FACTURACION
            }

            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf);
            }
            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';
        };

        $scope.ArmarFiltro = function () {
            if ($scope.ModeloTercero !== undefined && $scope.ModeloTercero !== '' && $scope.ModeloTercero !== null) {
                $scope.FiltroArmado += '&Terceros=' + $scope.ModeloTercero;
            }
            if ($scope.ModeloConductor !== undefined && $scope.ModeloConductor !== '' && $scope.ModeloConductor !== null) {
                $scope.FiltroArmado += '&Conductor=' + $scope.ModeloConductor;
            }
            if ($scope.ModeloPlaca !== undefined && $scope.ModeloPlaca !== '' && $scope.ModeloPlaca !== null) {
                $scope.FiltroArmado += '&Placa=' + $scope.ModeloPlaca;
            }
            if ($scope.ModeloSemirremolque !== undefined && $scope.ModeloSemirremolque !== '' && $scope.ModeloSemirremolque !== null) {
                $scope.FiltroArmado += '&Semirromolque=' + $scope.ModeloSemirremolque;
            }
            if ($scope.ModeloRuta !== undefined && $scope.ModeloRuta !== '' && $scope.ModeloRuta !== null) {
                $scope.FiltroArmado += '&Ruta=' + $scope.ModeloRuta;
            }
            if ($scope.ModeloEstado.Codigo !== undefined && $scope.ModeloEstado.Codigo !== -1 && $scope.ModeloEstado.Codigo !== '' && $scope.ModeloEstado.Codigo !== null) {
                $scope.FiltroArmado += '&Estado=' + $scope.ModeloEstado.Codigo;
            }
            if ($scope.ModeloPais !== undefined && $scope.ModeloPais !== '' && $scope.ModeloPais !== null) {
                $scope.FiltroArmado += '&Pais=' + $scope.ModeloPais;
            }
            if ($scope.ModeloCiudad !== undefined && $scope.ModeloCiudad !== '' && $scope.ModeloCiudad !== null) {
                $scope.FiltroArmado += '&Ciudad=' + $scope.ModeloCiudad;
            }
            if ($scope.ModeloNombre !== undefined && $scope.ModeloNombre !== '' && $scope.ModeloNombre !== null) {
                $scope.FiltroArmado += '&Nombre=' + $scope.ModeloNombre;
            }
            if ($scope.ModeloTipoSitio.Codigo !== undefined && $scope.ModeloTipoSitio.Codigo !== 0 && $scope.ModeloTipoSitio.Codigo !== '' && $scope.ModeloTipoSitio.Codigo !== null) {
                $scope.FiltroArmado += '&TipoSitio=' + $scope.ModeloTipoSitio.Codigo;
            }
        }

    }]);