﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente

Namespace Despachos
    <JsonObject>
    Public NotInheritable Class Cumplido
        Inherits Base
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Cumplido"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            If Read(lector, "ConsultaTiempos").Equals(1) Then
                Me.FechaLlegadaCargue = Read(lector, "Fecha_Llegada_Cargue")
                Me.FechaInicioCargue = Read(lector, "Fecha_Inicio_Cargue")
                Me.FechaFinCargue = Read(lector, "Fecha_Fin_Cargue")
                Me.FechaLlegadaDescargue = Read(lector, "Fecha_Llegada_Descargue")
                Me.FechaInicioDescargue = Read(lector, "Fecha_Inicio_Descargue")
                Me.FechaFinDescargue = Read(lector, "Fecha_Fin_Descargue")
            Else
                If Read(lector, "Obtener").Equals(0) Then
                    Me.NumeroDocumento = Read(lector, "Numero_Documento")
                    Me.FechaInicial = Read(lector, "Fecha")
                    Me.FechaFinal = Read(lector, "Fecha")
                    Me.NumeroPlanillaInicial = Read(lector, "NumeroPlanilla")
                    Me.NumeroPlanillaFinal = Read(lector, "NumeroPlanilla")
                    Me.TotalRegistros = Read(lector, "TotalRegistros")
                    Me.NombreRuta = Read(lector, "NombreRuta")
                    Me.Anulado = Read(lector, "Anulado")
                    Me.Tenedor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .NombreCompleto = Read(lector, "NombreTenedor")}
                Else
                    Me.FechaEntrega = Read(lector, "Fecha_Entrega")
                    Me.FechaInicioCargue = Read(lector, "Fecha_Inicio_Cargue")
                    Me.FechaFinCargue = Read(lector, "Fecha_Fin_Cargue")
                    Me.FechaInicioDescargue = Read(lector, "Fecha_Inicio_Descargue")
                    Me.FechaFinDescargue = Read(lector, "Fecha_Fin_Descargue")
                    Me.ValorMultaExtemporaneo = Read(lector, "Valor_Multa_Extemporaneo")
                    Me.NombreEntregoCumplido = Read(lector, "Nombre_Entrego_Cumplido")
                    Me.Observaciones = Read(lector, "Observaciones")
                    Me.Tenedor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .NombreCompleto = Read(lector, "NombreTenedor")}
                End If
                Me.FechaLlegadaCargue = Read(lector, "Fecha_Llegada_Cargue")
                Me.FechaLlegadaDescargue = Read(lector, "Fecha_Llegada_Descargue")
                Me.CodigoEmpresa = Read(lector, "EMPR_Codigo")
                Me.Fecha = Read(lector, "Fecha")
                Me.Conductor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .NombreCompleto = Read(lector, "NombreConductor")}
                Me.Numero = Read(lector, "Numero")
                Me.NumeroDocumento = Read(lector, "Numero_Documento")
                Me.Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "NombreOficina")}
                Me.Estado = Read(lector, "Estado")
                Me.NumeroPlanilla = Read(lector, "NumeroPlanilla")
                Me.NumeroInternoPlanilla = Read(lector, "ENPD_Numero")
                Me.NumeroManifiesto = Read(lector, "NumeroManifiesto")
                Me.NumeroInternoManifiesto = Read(lector, "ENMC_Numero")

            End If
            Me.Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "PlacaVehiculo")}
            CantidadNovedades = Read(lector, "Novedades")
            Me.CodigoPlanilla = Read(lector, "ENPD_Numero")
            TipoCumplidoManifiesto = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICM_Codigo")}
            MotivoSuspensionManifiesto = New ValorCatalogos With {.Codigo = Read(lector, "CATA_MOSM_Codigo")}
            ConsecuenciaSuspencionManifiesto = New ValorCatalogos With {.Codigo = Read(lector, "CATA_COSM_Codigo")}
            VehiculoTransbordo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo_Transbordo")}
            SemirremolqueTransbordo = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo_Transbordo")}
            TenedorTransbordo = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Tenedor_Transbordo")}
            ConductorTransbordo = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Condcutor_Transbordo")}
            ManifiestoTransbordo = Read(lector, "ManifiestoTransbordo")
            PlanillaTransbordo = Read(lector, "PlanillaTransbordo")
            PlacaSemirremolque = Read(lector, "PlacaSemirremolque")
            RutaTransbordo = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo_Transbordo")}
            TarifaTransbordo = New TarifaTransporteCarga With {.Codigo = Read(lector, "TATC_Codigo_Transbordo")}
            TipoTarifaTransbordo = New TipoTarifaTransporteCarga With {.Codigo = Read(lector, "TTTC_Codigo_Transbordo")}
            CantidadTransbordo = Read(lector, "Cantidad_Transbordo")
            PesoTransbordo = Read(lector, "Peso_Transbordo")
            ValorFleteTransportadorTransbordo = Read(lector, "Valor_Flete_Transportador_Transbordo")
            'NumeroManifiesto = Read(lector, "PlacaSemirremolque")
        End Sub

#Region "Propiedades"
        <JsonProperty>
        Public Property CantidadNovedades As Double
        <JsonProperty>
        Public Property Numero As Long

        <JsonProperty>
        Public Property NumeroDocumento As Long

        <JsonProperty>
        Public Property NumeroPlanilla As Long
        <JsonProperty>
        Public Property CodigoPlanilla As Long
        <JsonProperty>
        Public Property NumeroManifiesto As Long
        <JsonProperty>
        Public Property NumeroInternoPlanilla As Long
        <JsonProperty>
        Public Property NumeroInternoManifiesto As Long
        <JsonProperty>
        Public Property FechaInicial As Date

        <JsonProperty>
        Public Property FechaFinal As Date

        <JsonProperty>
        Public Property NumeroPlanillaInicial As Long

        <JsonProperty>
        Public Property NumeroPlanillaFinal As Long

        <JsonProperty>
        Public Property Fecha As Date

        <JsonProperty>
        Public Property Vehiculo As Vehiculos

        <JsonProperty>
        Public Property PlacaSemirremolque As String
        <JsonProperty>
        Public Property NombreRuta As String

        <JsonProperty>
        Public Property Tenedor As Tercero

        <JsonProperty>
        Public Property Conductor As Tercero

        <JsonProperty>
        Public Property Anulado As Integer

        <JsonProperty>
        Public Property Estado As Integer

        <JsonProperty>
        Public Property Numeracion As String

        <JsonProperty>
        Public Property TipoDocumento As TipoDocumentos

        <JsonProperty>
        Public Property UsuarioCrea As Usuarios

        <JsonProperty>
        Public Property FechaCrea As BaseBasico

        <JsonProperty>
        Public Property UsuarioModifica As Usuarios

        <JsonProperty>
        Public Property FechaModifica As BaseBasico

        <JsonProperty>
        Public Property FechaAnula As Date

        <JsonProperty>
        Public Property UsuarioAnula As Usuarios

        <JsonProperty>
        Public Property CausaAnula As String

        <JsonProperty>
        Public Property Oficina As Oficinas

        <JsonProperty>
        Public Property FechaEntrega As Date

        <JsonProperty>
        Public Property FechaLlegadaCargue As Date

        <JsonProperty>
        Public Property FechaInicioCargue As Date

        <JsonProperty>
        Public Property FechaFinCargue As Date

        <JsonProperty>
        Public Property FechaLlegadaDescargue As Date

        <JsonProperty>
        Public Property FechaInicioDescargue As Date

        <JsonProperty>
        Public Property FechaFinDescargue As Date

        <JsonProperty>
        Public Property ValorMultaExtemporaneo As Double

        <JsonProperty>
        Public Property NombreEntregoCumplido As String

        <JsonProperty>
        Public Property Observaciones As String

        <JsonProperty>
        Public Property CumplidoRemesas As CumplidoRemesa

        <JsonProperty>
        Public Property ListaCumplidoRemesas As IEnumerable(Of CumplidoRemesa)

        <JsonProperty>
        Public Property ListaRemesasManifiesto As IEnumerable(Of CumplidoRemesa)

        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)
        <JsonProperty>
        Public Property Documentos As Documentos
        <JsonProperty>
        Public Property ListaDocumentos As IEnumerable(Of Documentos)
        <JsonProperty>
        Public Property ConteoDocumentos As Integer
        <JsonProperty>
        Public Property TipoCumplidoManifiesto As ValorCatalogos
        <JsonProperty>
        Public Property MotivoSuspensionManifiesto As ValorCatalogos
        <JsonProperty>
        Public Property ConsecuenciaSuspencionManifiesto As ValorCatalogos
#Region "Transbordo"
        <JsonProperty>
        Public Property ManifiestoTransbordo As Integer
        <JsonProperty>
        Public Property PlanillaTransbordo As Integer
        <JsonProperty>
        Public Property VehiculoTransbordo As Vehiculos
        <JsonProperty>
        Public Property SemirremolqueTransbordo As Semirremolques
        <JsonProperty>
        Public Property TenedorTransbordo As Terceros
        <JsonProperty>
        Public Property PropietarioTransbordo As Terceros
        <JsonProperty>
        Public Property ConductorTransbordo As Terceros
        <JsonProperty>
        Public Property RutaTransbordo As Rutas
        <JsonProperty>
        Public Property TarifaTransbordo As TarifaTransporteCarga
        <JsonProperty>
        Public Property TipoTarifaTransbordo As TipoTarifaTransporteCarga
        <JsonProperty>
        Public Property CantidadTransbordo As Integer
        <JsonProperty>
        Public Property PesoTransbordo As Integer
        <JsonProperty>
        Public Property ValorFleteTransportadorTransbordo As Double
#End Region
#End Region

    End Class
End Namespace
