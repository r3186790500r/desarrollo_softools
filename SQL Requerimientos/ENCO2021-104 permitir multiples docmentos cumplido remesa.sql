USE [ENCOEXPRES_DOCU]
GO

/****** Object:  StoredProcedure [dbo].[gsp_trasladar_documento_cumplido_remesa]    Script Date: 7/02/2022 11:20:21 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_trasladar_documento_cumplido_remesa]
(  
	@par_EMPR_Codigo SMALLINT,  
	@par_ENRE_Numero NUMERIC,
	@par_ID NUMERIC = NULL
)  
AS  
BEGIN  
	Declare @ID Numeric = 0

	--DELETE Documentos_Cumplido_Remesa WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero
	INSERT INTO Documentos_Cumplido_Remesa (
		EMPR_codigo,
		ENRE_Numero,
		Fecha_Crea,
		NombreArchivo,
		Tipo,
		ExtensionArchivo,
		Archivo,
		Descripcion
	)
	SELECT 
	EMPR_codigo,
	ENRE_Numero,
	Fecha_Crea,
	NombreArchivo,
	Tipo,
	ExtensionArchivo,
	Archivo,
	Descripcion  
	FROM T_Documentos_Cumplido_Remesa  
	WHERE EMPR_Codigo = @par_EMPR_Codigo  
	AND ENRE_Numero = @par_ENRE_Numero
	AND Codigo = @par_ID

	SET @ID = @@IDENTITY


	DELETE T_Documentos_Cumplido_Remesa  
	WHERE EMPR_Codigo = @par_EMPR_Codigo  
	AND ENRE_Numero = @par_ENRE_Numero  
	AND Codigo = @par_ID

	SELECT @ID As Codigo

END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_t_documento_cumplido_remesa]
(
	@par_EMPR_Codigo SMALLINT,
	@par_ENRE_Numero NUMERIC, 
	@par_NombreArchivo VARCHAR(100),  
	@par_Archivo VARBINARY(MAX) = NULL,  
	@par_Tipo VARCHAR(100) = NULL,
	@par_ExtensionArchivo VARCHAR(20) = NULL,
	@par_Descripcion VARCHAR(100) = NULL,  
	@par_ID NUMERIC = NULL
)  
AS  
BEGIN
	DECLARE @ID Numeric = 0
	IF(EXISTS(
		SELECT ENRE_Numero from T_Documentos_Cumplido_Remesa 
		WHERE EMPR_codigo = @par_EMPR_Codigo 
		AND ENRE_Numero = @par_ENRE_Numero
		AND Codigo = @par_ID
	))
	BEGIN
		--Actualiza
		UPDATE T_Documentos_Cumplido_Remesa SET
		Fecha_Crea = GETDATE(),
		NombreArchivo = ISNULL(@par_NombreArchivo, NombreArchivo),
		Tipo = ISNULL(@par_Tipo, Tipo),
		ExtensionArchivo = ISNULL(@par_ExtensionArchivo, ExtensionArchivo),
		Archivo = ISNULL(@par_Archivo, Archivo),
		Descripcion = ISNULL(@par_Descripcion, Descripcion)
		WHERE EMPR_codigo = @par_EMPR_Codigo 
		AND ENRE_Numero = @par_ENRE_Numero		
		AND Codigo = @par_ID

		SELECT @ID = Codigo from T_Documentos_Cumplido_Remesa  
		WHERE EMPR_codigo = @par_EMPR_Codigo 
		AND ENRE_Numero = @par_ENRE_Numero 
		AND Codigo = @par_ID
	END
	ELSE
	BEGIN
		--Inserta
		INSERT INTO T_Documentos_Cumplido_Remesa (  
			EMPR_codigo,
			ENRE_Numero,
			Fecha_Crea,
			NombreArchivo,
			Tipo,
			ExtensionArchivo,
			Archivo,
			Descripcion
		)  
		VALUES (  
			@par_EMPR_Codigo,
			@par_ENRE_Numero,
			GETDATE(),
			@par_NombreArchivo,
			@par_Tipo,
			@par_ExtensionArchivo,
			@par_Archivo,
			@par_Descripcion
		)
		SET @ID = @@IDENTITY
	END
	select @ID AS Codigo
END
GO

CREATE OR ALTER   PROCEDURE [dbo].[gsp_eliminar_t_documento_cumplido_remesa] (

	@par_EMPR_Codigo SMALLINT,
	@par_ENRE_Numero NUMERIC,
	@par_ID NUMERIC = NULL
)

AS
BEGIN
	
	IF EXISTS(SELECT EMPR_Codigo FROM T_Documentos_Cumplido_Remesa 
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND ENRE_Numero = @par_ENRE_Numero
	AND (Codigo = @par_ID or @par_ID is null))	BEGIN

			DELETE T_Documentos_Cumplido_Remesa  
			WHERE EMPR_Codigo = @par_EMPR_Codigo  
			AND ENRE_Numero = @par_ENRE_Numero 
			AND (Codigo = @par_ID or @par_ID is null)

			SELECT @@ROWCOUNT As RegistrosAfectados
	END ELSE
	BEGIN
		SELECT 1 As RegistrosAfectados
	END

END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_eliminar_documento_cumplido_remesa]  
(    
 @par_EMPR_Codigo SMALLINT,    
 @par_ENRE_Numero NUMERIC,
 @par_ID NUMERIC = NULL
)    
AS    
BEGIN    
 
  
 DELETE Documentos_Cumplido_Remesa 
 WHERE EMPR_Codigo = @par_EMPR_Codigo 
 AND ENRE_Numero = @par_ENRE_Numero  
 AND Codigo = @par_ID
  
  
 --DELETE T_Documentos_Cumplido_Remesa    
 --WHERE EMPR_Codigo = @par_EMPR_Codigo    
 --AND ENRE_Numero = @par_ENRE_Numero    
  
 SELECT @@rowcount As RegistrosAfectados  
  
END  
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_documento_cumplido_remesa_despachos]   
(      
 @par_EMPR_Codigo SMALLINT,   
 @par_ENRE_Numero NUMERIC = NULL    
)      
AS      
BEGIN    
 SELECT      
 EMPR_codigo,    
 ENRE_Numero AS Numero,    
 Codigo  AS ID,    
 NombreArchivo,    
 ExtensionArchivo,    
 Descripcion    
    
 FROM Documentos_Cumplido_Remesa    
 WHERE EMPR_codigo = @par_EMPR_Codigo    
 AND ENRE_Numero = @par_ENRE_Numero 
END    
GO