﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos
    <JsonObject>
    Public NotInheritable Class TarifasPeajes
        Inherits BaseBasico

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Categoria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CAPE_Codigo"), .Nombre = Read(lector, "CATA_TIVE_Nombre"), .CampoAuxiliar2 = Read(lector, "CATA_TIVE_Campo2")}
            Valor = Read(lector, "Valor")
            ValorRemolque1Eje = Read(lector, "Valor_Remolque_Un_Eje")
            ValorRemolque2Ejes = Read(lector, "Valor_Remolque_Dos_Ejes")
            ValorRemolque3Ejes = Read(lector, "Valor_Remolque_Tres_Ejes")
            ValorRemolque4Ejes = Read(lector, "Valor_Remolque_Cuatro_Ejes")
            TipoVehiculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIVE_Codigo"), .Nombre = Read(lector, "TipoVehiculo")}
        End Sub

        <JsonProperty>
        Public Property CodigoPeaje As Integer
        <JsonProperty>
        Public Property Categoria As ValorCatalogos
        <JsonProperty>
        Public Property Valor As Integer

        <JsonProperty>
        Public Property ValorRemolque1Eje As Integer
        <JsonProperty>
        Public Property ValorRemolque2Ejes As Integer
        <JsonProperty>
        Public Property ValorRemolque3Ejes As Integer
        <JsonProperty>
        Public Property ValorRemolque4Ejes As Integer

        <JsonProperty>
        Public Property TipoVehiculo As ValorCatalogos
    End Class
End Namespace
