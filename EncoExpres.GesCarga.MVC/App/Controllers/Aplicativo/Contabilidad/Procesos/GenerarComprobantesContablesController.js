﻿EncoExpresApp.controller("GenerarComprobantesContablesCtrl", ['$scope', '$timeout', 'LiquidacionesFactory', 'TercerosFactory', '$linq', 'blockUI', '$routeParams', 'OficinasFactory', 'EncabezadoComprobantesContablesFactory', 'FacturasFactory', 'ValorCatalogosFactory', 'DocumentoComprobantesFactory', 'EncabezadoComprobantesContablesFactory', 'FacturasOtrosConceptosFactory','LegalizacionGastosFactory',
    function ($scope, $timeout, LiquidacionesFactory, TercerosFactory, $linq, blockUI, $routeParams, OficinasFactory, EncabezadoComprobantesContablesFactory, FacturasFactory, ValorCatalogosFactory, DocumentoComprobantesFactory, EncabezadoComprobantesContablesFactory, FacturasOtrosConceptosFactory, LegalizacionGastosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Contabilidad' }, { Nombre: 'Procesos' }, { Nombre: 'Generar Comprobantes Contables' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;

        $scope.MostrarMensajeError = false
        $scope.NumeroAnular;
        $scope.NumeroDocumentoAnular;
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoOficinas = [];
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GENERAR_COMPROBANTES_CONTABLES); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar


        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaFinal = new Date();
        }
        $('#SeccionLiquidaciones').show()
        $('#SeccionFancturas').hide()
        $('#SeccionComprobantesEgreso').hide()
        $('#SeccionComprobantesIngreso').hide()
        $('#SeccionFancturasOtrosConceptos').hide()
        $('#Seccionlegalizaciones').hide()
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        $scope.Liquidacion = {}
        $scope.legalizacion = {}
        $scope.Facturacion = {}
        $scope.FacturacionOtrosConceptos = {}
        $scope.ComprobantesEgreso = {}
        $scope.ComprobantesIngreso = {}
        $scope.ModeloOficina = {}
        $scope.ModeloEstado = {}
        //-------------------------------------------------FUNCIONES--------------------------------------------------------


        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: CERO },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
              { Nombre: 'ANULADO', Codigo: 2 }
        ]

        $scope.Liquidacion.ModeloEstado = $scope.ListadoEstados[2]
        $scope.legalizacion.ModeloEstado = $scope.ListadoEstados[2]
        $scope.Facturacion.Estado = $scope.ListadoEstados[2]
        $scope.FacturacionOtrosConceptos.Estado = $scope.ListadoEstados[2]
        $scope.ComprobantesEgreso.Estado = $scope.ListadoEstados[2]
        $scope.ComprobantesIngreso.Estado = $scope.ListadoEstados[2]

        $scope.ListadoCliente = []
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente)
                }
            }
            return $scope.ListadoCliente
        }

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item)
                    });
                    if ($scope.Sesion.UsuarioAutenticado.Prefijo == PREFIJO_INTEGRA) {
                        $scope.Liquidacion.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 1');
                        $scope.legalizacion.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 1');
                        $scope.Facturacion.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 1');
                        $scope.ComprobantesEgreso.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 1');
                        $scope.ComprobantesIngreso.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 1');
                    } else {
                        $scope.Liquidacion.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                        $scope.legalizacion.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                        $scope.Facturacion.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                        $scope.ComprobantesEgreso.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 1');
                        $scope.ComprobantesIngreso.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 1');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDocumentosOrigen = response.data.Datos;
                    $scope.ComprobantesEgreso.DocumentoOrigen = response.data.Datos[0];
                    $scope.ComprobantesIngreso.DocumentoOrigen = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //Liquidaciones

        $scope.BuscarLiquidaciones = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                if (DatosRequeridosLquidaciones()) {
                    FindLiquidaciones()
                }
            }
        };
        function DatosRequeridosLquidaciones() {
            $scope.MensajesErrorLiquidaciones = [];
            var continuar = true;
            if (($scope.Liquidacion.ModeloFechaInicial === null || $scope.Liquidacion.ModeloFechaInicial === undefined || $scope.Liquidacion.ModeloFechaInicial === '')
                && ($scope.Liquidacion.ModeloFechaFinal === null || $scope.Liquidacion.ModeloFechaFinal === undefined || $scope.Liquidacion.ModeloFechaFinal === '')
                && ($scope.Liquidacion.ModeloNumero === null || $scope.Liquidacion.ModeloNumero === undefined || $scope.Liquidacion.ModeloNumero === '' || $scope.Liquidacion.ModeloNumero === 0 || isNaN($scope.Liquidacion.ModeloNumero) === true)

            ) {
                $scope.MensajesErrorLiquidaciones.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.Liquidacion.ModeloNumero !== null && $scope.Liquidacion.ModeloNumero !== undefined && $scope.Liquidacion.ModeloNumero !== '' && $scope.Liquidacion.ModeloNumero !== 0)
                || ($scope.Liquidacion.ModeloFechaInicial !== null && $scope.Liquidacion.ModeloFechaInicial !== undefined && $scope.Liquidacion.ModeloFechaInicial !== '')
                || ($scope.Liquidacion.ModeloFechaFinal !== null && $scope.Liquidacion.ModeloFechaFinal !== undefined && $scope.Liquidacion.ModeloFechaFinal !== '')

            ) {
                if (($scope.Liquidacion.ModeloFechaInicial !== null && $scope.Liquidacion.ModeloFechaInicial !== undefined && $scope.Liquidacion.ModeloFechaInicial !== '')
                    && ($scope.Liquidacion.ModeloFechaFinal !== null && $scope.Liquidacion.ModeloFechaFinal !== undefined && $scope.Liquidacion.ModeloFechaFinal !== '')) {
                    if ($scope.Liquidacion.ModeloFechaFinal < $scope.Liquidacion.ModeloFechaInicial) {
                        $scope.Liquidacion.MensajesErrorLiquidaciones.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.Liquidacion.ModeloFechaFinal - $scope.Liquidacion.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorLiquidaciones.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.Liquidacion.ModeloFechaInicial !== null && $scope.Liquidacion.ModeloFechaInicial !== undefined && $scope.Liquidacion.ModeloFechaInicial !== '')) {
                        $scope.Liquidacion.ModeloFechaFinal = $scope.Liquidacion.ModeloFechaInicial
                    } else {
                        $scope.Liquidacion.ModeloFechaInicial = $scope.Liquidacion.ModeloFechaFinal
                    }
                }
            }
            return continuar
        }
        function FindLiquidaciones() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.Liquidacion.ModeloNumero,
                FechaInicial: $scope.Liquidacion.ModeloNumero > 0 ? undefined : $scope.Liquidacion.ModeloFechaInicial,
                FechaFinal: $scope.Liquidacion.ModeloNumero > 0 ? undefined : $scope.Liquidacion.ModeloFechaFinal,
                NumeroPlanilla: $scope.Liquidacion.ModeloPlanilla,
                NumeroManifiesto: $scope.Liquidacion.ModeloManifiesto,
                PlacaVehiculo: $scope.Liquidacion.ModeloPlaca,
                Conductor: { Nombre: $scope.Liquidacion.ModeloConductor },
                Tenedor: { Nombre: $scope.Liquidacion.ModeloTenedor },
                Oficina: $scope.Liquidacion.ModeloOficina,
                Estado: $scope.Liquidacion.ModeloEstado.Codigo,
                //Pagina: $scope.paginaActual,
                Numero: 0,
                //RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                Aprobado: -1,
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO }
            };

            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoLiquidaciones = [];
            $scope.ListadoLiquidacionesFiltrado = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    blockUI.delay = 1000;
                    $scope.Pagina = $scope.paginaActual
                    $scope.RegistrosPagina = 10
                    LiquidacionesFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Numero = 0
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoLiquidaciones = response.data.Datos
                                    $scope.totalRegistrosLiquidaciones = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginasLiquidaciones = Math.ceil($scope.totalRegistrosLiquidaciones / $scope.cantidadRegistrosPorPaginas);
                                    $scope.BuscandoLiquidaciones = false;
                                    $scope.ResultadoSinRegistrosLiquidaciones = '';
                                    $scope.PrimerPaginaLiquidaciones();
                                }
                                else {
                                    $scope.totalRegistrosLiquidaciones = 0;
                                    $scope.totalPaginasLiquidaciones = 0;
                                    $scope.paginaActualLiquidaciones = 1;
                                    $scope.ResultadoSinRegistrosLiquidaciones = 'No hay datos para mostrar';
                                    $scope.BuscandoLiquidaciones = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }

        $scope.PrimerPaginaLiquidaciones = function () {
            if ($scope.totalRegistrosLiquidaciones > 10) {
                $scope.ListadoLiquidacionesFiltrado = []
                $scope.paginaActualLiquidaciones = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListadoLiquidacionesFiltrado.push($scope.ListadoLiquidaciones[i])
                }
            } else {
                $scope.ListadoLiquidacionesFiltrado = $scope.ListadoLiquidaciones
            }
        }
        $scope.AnteriorLiquidaciones = function () {
            if ($scope.paginaActualLiquidaciones > 1) {
                $scope.paginaActualLiquidaciones -= 1
                var a = $scope.paginaActualLiquidaciones * 10
                if (a < $scope.totalRegistrosLiquidaciones) {
                    $scope.ListadoLiquidacionesFiltrado = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoLiquidacionesFiltrado.push($scope.ListadoLiquidaciones[i])
                    }
                }
            }
            else {
                $scope.PrimerPaginaLiquidaciones()
            }
        }
        $scope.SiguienteLiquidaciones = function () {
            if ($scope.paginaActualLiquidaciones < $scope.totalPaginasLiquidaciones) {
                $scope.paginaActualLiquidaciones += 1
                var a = $scope.paginaActualLiquidaciones * 10
                if (a < $scope.totalRegistrosLiquidaciones) {
                    $scope.ListadoLiquidacionesFiltrado = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoLiquidacionesFiltrado.push($scope.ListadoLiquidaciones[i])
                    }
                } else {
                    $scope.UltimaPaginaLiquidaciones()
                }
            } else if ($scope.paginaActualLiquidaciones == $scope.totalPaginasLiquidaciones) {
                $scope.UltimaPaginaLiquidaciones()
            }
        }
        $scope.UltimaPaginaLiquidaciones = function () {
            if ($scope.totalRegistrosLiquidaciones > 10 && $scope.totalPaginasLiquidaciones > 1) {
                $scope.paginaActualLiquidaciones = $scope.totalPaginasLiquidaciones
                var a = $scope.paginaActualLiquidaciones * 10
                $scope.ListadoLiquidacionesFiltrado = []
                for (var i = a - 10; i < $scope.ListadoLiquidaciones.length; i++) {
                    $scope.ListadoLiquidacionesFiltrado.push($scope.ListadoLiquidaciones[i])
                }
            }
        }

        $scope.GenerarMovimientoContableLiquidaciones = function (NumeroDocumento) {
            $scope.ListadoDocumentos = []
            for (var i = 0; i < $scope.ListadoLiquidaciones.length; i++) {
                if ($scope.ListadoLiquidaciones[i].Seleccionado) {
                    $scope.ListadoDocumentos.push({ NumeroDocumento: $scope.ListadoLiquidaciones[i].Numero })
                }
            }
            if ($scope.ListadoDocumentos.length > 0) {
                Response = EncabezadoComprobantesContablesFactory.Guardar({CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ListaDocumentos: $scope.ListadoDocumentos, Masivo: 1, TipoDocumentoOrigen: { Codigo: 160 },Sync: true})
                if (Response.ProcesoExitoso === true) {
                    ShowSuccess('Los movimientos contables se generaron correctamente')
                    $scope.MensajesError = [];
                    $scope.ListadoLiquidaciones = [];
                    $scope.ListadoLiquidacionesFiltrado = [];
                } else {
                    ShowError('No se generó el movimiento contable, por favor verifique la parametrización ')
                }

            } else {
                ShowError('Debe seleccionar al menos un documento')
            }
        }
        $scope.MarcarTodoiquidacion = function (check) {
            for (var i = 0; i < $scope.ListadoLiquidaciones.length; i++) {
                $scope.ListadoLiquidaciones[i].Seleccionado = check
            }
        }
        //Legalizacion

        $scope.BuscarLegalizacion = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                if (DatosRequeridosLegalizacion()) {
                    FindLegalizacion()
                }
            }
        };
        function DatosRequeridosLegalizacion() {
            $scope.MensajesErrorlegalizaciones = [];
            var continuar = true;
            if (($scope.legalizacion.ModeloFechaInicial === null || $scope.legalizacion.ModeloFechaInicial === undefined || $scope.legalizacion.ModeloFechaInicial === '')
                && ($scope.legalizacion.ModeloFechaFinal === null || $scope.legalizacion.ModeloFechaFinal === undefined || $scope.legalizacion.ModeloFechaFinal === '')
                && ($scope.legalizacion.ModeloNumero === null || $scope.legalizacion.ModeloNumero === undefined || $scope.legalizacion.ModeloNumero === '' || $scope.legalizacion.ModeloNumero === 0 || isNaN($scope.legalizacion.ModeloNumero) === true)

            ) {
                $scope.MensajesErrorlegalizaciones.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.legalizacion.ModeloNumero !== null && $scope.legalizacion.ModeloNumero !== undefined && $scope.legalizacion.ModeloNumero !== '' && $scope.legalizacion.ModeloNumero !== 0)
                || ($scope.legalizacion.ModeloFechaInicial !== null && $scope.legalizacion.ModeloFechaInicial !== undefined && $scope.legalizacion.ModeloFechaInicial !== '')
                || ($scope.legalizacion.ModeloFechaFinal !== null && $scope.legalizacion.ModeloFechaFinal !== undefined && $scope.legalizacion.ModeloFechaFinal !== '')

            ) {
                if (($scope.legalizacion.ModeloFechaInicial !== null && $scope.legalizacion.ModeloFechaInicial !== undefined && $scope.legalizacion.ModeloFechaInicial !== '')
                    && ($scope.legalizacion.ModeloFechaFinal !== null && $scope.legalizacion.ModeloFechaFinal !== undefined && $scope.legalizacion.ModeloFechaFinal !== '')) {
                    if ($scope.legalizacion.ModeloFechaFinal < $scope.legalizacion.ModeloFechaInicial) {
                        $scope.legalizacion.MensajesErrorLiquidaciones.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.legalizacion.ModeloFechaFinal - $scope.legalizacion.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorlegalizaciones.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.legalizacion.ModeloFechaInicial !== null && $scope.legalizacion.ModeloFechaInicial !== undefined && $scope.legalizacion.ModeloFechaInicial !== '')) {
                        $scope.legalizacion.ModeloFechaFinal = $scope.legalizacion.ModeloFechaInicial
                    } else {
                        $scope.legalizacion.ModeloFechaInicial = $scope.legalizacion.ModeloFechaFinal
                    }
                }
            }
            return continuar
        }
        function FindLegalizacion() { 
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.legalizacion.ModeloNumero,
                FechaInicial: $scope.legalizacion.ModeloNumero > 0 ? undefined : $scope.legalizacion.ModeloFechaInicial,
                FechaFinal: $scope.legalizacion.ModeloNumero > 0 ? undefined : $scope.legalizacion.ModeloFechaFinal,
                Conductor: { Nombre: $scope.ModeloConductor == undefined ? '' : $scope.ModeloConductor.NombreCompleto },
                Estado: $scope.legalizacion.ModeloEstado.Codigo,
                Pagina: $scope.paginaActual,
                Oficina: $scope.legalizacion.ModeloOficina,
                Numero: $scope.ModeloNumeros,
                Planilla: { NumeroDocumento: $scope.legalizacion.ModeloPlanilla },
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
                Aprobado: -1
            };


            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.Listadolegalizacion = [];
            $scope.ListadoLegalizacionFiltrado = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    blockUI.delay = 1000;
                    $scope.Pagina = $scope.paginaActual
                    $scope.RegistrosPagina = 10
                    LegalizacionGastosFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Numero = 0
                                if (response.data.Datos.length > 0) {
                                    $scope.Listadolegalizacion = response.data.Datos
                                    $scope.totalRegistrosLegalizacion = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginasLegalizacion = Math.ceil($scope.totalRegistrosLegalizacion /    $scope.cantidadRegistrosPorPaginas);
                                    $scope.BuscandoLegalizacion = false;
                                    $scope.ResultadoSinRegistrosLegalizacion = '';
                                    $scope.PrimerPaginaLegalizacion();
                                }
                                else {
                                    $scope.totalRegistrosLLegalizacion = 0;
                                    $scope.totalPaginasLegalizacion = 0;
                                    $scope.paginaActualLegalizacion = 1;
                                    $scope.ResultadoSinRegistrosLegalizacion = 'No hay datos para mostrar';
                                    $scope.BuscandoLegalizacion = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }

        $scope.PrimerPaginaLegalizacion = function () {
            if ($scope.totalRegistrosLegalizacion > 10) {
                $scope.ListadolegalizacionFiltrado = []
                $scope.paginaActuallegalizacion = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListadoLegalizacionFiltrado.push($scope.Listadolegalizacion[i])
                }
            } else {
                $scope.ListadolegalizacionFiltrado = $scope.Listadolegalizacion
            }
        }
        $scope.Anteriorlegalizacion = function () {
            if ($scope.paginaActualLegalizacion > 1) {
                $scope.paginaActualLegalizacion -= 1
                var a = $scope.paginaActualLegalizacion * 10
                if (a < $scope.totalRegistrosLegalizacion) {
                    $scope.ListadoLegalizacionFiltrado = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoLegalizacionFiltrado.push($scope.ListadoLegalizacion[i])
                    }
                }
            }
            else {
                $scope.PrimerPaginaLegalizacion()
            }
        }
        $scope.Siguientelegalizacion = function () {
            if ($scope.paginaActualLegalizacion < $scope.totalPaginasLegalizacion) {
                $scope.paginaActuallegalizacion += 1
                var a = $scope.paginaActuallegalizacion * 10
                if (a < $scope.totalRegistroslegalizacion) {
                    $scope.ListadolegalizacionFiltrado = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadolegalizacionFiltrado.push($scope.Listadolegalizacion[i])
                    }
                } else {
                    $scope.UltimaPaginalegalizacion()
                }
            } else if ($scope.paginaActuallegalizacion == $scope.totalPaginaslegalizacion) {
                $scope.UltimaPaginalegalizacion()
            }
        }
        $scope.UltimaPaginalegalizacion = function () {
            if ($scope.totalRegistroslegalizacion > 10 && $scope.totalPaginaslegalizacion > 1) {
                $scope.paginaActuallegalizacion = $scope.totalPaginaslegalizacion
                var a = $scope.paginaActuallegalizacion * 10
                $scope.ListadolegalizacionFiltrado = []
                for (var i = a - 10; i < $scope.Listadolegalizacion.length; i++) {
                    $scope.ListadolegalizacionFiltrado.push($scope.Listadolegalizacion[i])
                }
            }
        }

        $scope.GenerarMovimientoContableLegalizacion = function (NumeroDocumento) {
            $scope.ListadoDocumentos = []
            for (var i = 0; i < $scope.Listadolegalizacion.length; i++) {
                if ($scope.Listadolegalizacion[i].Seleccionado) {
                    $scope.ListadoDocumentos.push({ NumeroDocumento: $scope.Listadolegalizacion[i].Numero })
                }
            }
            if ($scope.ListadoDocumentos.length > 0) {
                Response = EncabezadoComprobantesContablesFactory.Guardar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ListaDocumentos: $scope.ListadoDocumentos, Masivo: 1, TipoDocumentoOrigen: { Codigo: 230 }, Sync: true })
                if (Response.ProcesoExitoso === true) {
                    ShowSuccess('Los movimientos contables se generaron correctamente')
                    $scope.MensajesError = [];
                    $scope.Listadolegalizacion = [];
                    $scope.ListadolegalizacionFiltrado = [];
                } else {
                    ShowError('No se generó el movimiento contable, por favor verifique la parametrización ')
                }

            } else {
                ShowError('Debe seleccionar al menos un documento')
            }
        }

        $scope.MarcarTodolegalizacion = function (check) {
            for (var i = 0; i < $scope.Listadolegalizacion.length; i++) {
                $scope.Listadolegalizacion[i].Seleccionado = check
            }
        }

        //Facturas

        $scope.BuscarFacturacion = function () {
            if (DatosRequeridosFacturacion()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    FindFacturacion();
                }
            }
        };

        function DatosRequeridosFacturacion() {
            $scope.MensajesErrorFacturacion = [];
            var continuar = true;
            if (($scope.Facturacion.FechaInicial === null || $scope.Facturacion.FechaInicial === undefined || $scope.Facturacion.FechaInicial === '')
                && ($scope.Facturacion.FechaFinal === null || $scope.Facturacion.FechaFinal === undefined || $scope.Facturacion.FechaFinal === '')
                && ($scope.Facturacion.NumeroInicial === null || $scope.Facturacion.NumeroInicial === undefined || $scope.Facturacion.NumeroInicial === '' || $scope.Facturacion.NumeroInicial === 0 || isNaN($scope.Facturacion.NumeroInicial) === true)

            ) {
                $scope.MensajesErrorFacturacion.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.Facturacion.NumeroInicial !== null && $scope.Facturacion.NumeroInicial !== undefined && $scope.Facturacion.NumeroInicial !== '' && $scope.Facturacion.NumeroInicial !== 0)
                || ($scope.Facturacion.FechaInicial !== null && $scope.Facturacion.FechaInicial !== undefined && $scope.Facturacion.FechaInicial !== '')
                || ($scope.Facturacion.FechaFinal !== null && $scope.Facturacion.FechaFinal !== undefined && $scope.Facturacion.FechaFinal !== '')

            ) {
                if (($scope.Facturacion.FechaInicial !== null && $scope.Facturacion.FechaInicial !== undefined && $scope.Facturacion.FechaInicial !== '')
                    && ($scope.Facturacion.FechaFinal !== null && $scope.Facturacion.FechaFinal !== undefined && $scope.Facturacion.FechaFinal !== '')) {
                    if ($scope.Facturacion.FechaFinal < $scope.Facturacion.FechaInicial) {
                        $scope.MensajesErrorFacturacion.push('La fecha final debe ser mayor a la fecha inicial');
                        continuar = false;
                    } else if ((($scope.Facturacion.FechaFinal - $scope.Facturacion.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorFacturacion.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.Facturacion.FechaInicial !== null && $scope.Facturacion.FechaInicial !== undefined && $scope.Facturacion.FechaInicial !== '')) {
                        $scope.Facturacion.FechaFinal = $scope.Facturacion.FechaInicial;
                    }
                    else {
                        $scope.Facturacion.FechaInicial = $scope.Facturacion.FechaFinal;
                    }
                }
            }
            if ($scope.Facturacion.NumeroInicial === undefined || $scope.Facturacion.NumeroInicial === '' || $scope.Facturacion.NumeroInicial === null)
                $scope.Facturacion.NumeroInicial = 0;

            return continuar;
        }
        function FindFacturacion() {
            $scope.ListaFacturas = [];
            $scope.ListaFacturasFiltrado = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.Facturacion.NumeroInicial,

                FechaInicial: $scope.Facturacion.NumeroInicial > 0 ? undefined : $scope.Facturacion.FechaInicial,
                FechaFinal: $scope.Facturacion.NumeroInicial > 0 ? undefined : $scope.Facturacion.FechaFinal,
                CataTipoFactura: CODIGO_CATALOGO_TIPO_FACTURA,
                OficinaFactura: { Codigo: $scope.Facturacion.Oficina.Codigo },

                Cliente: $scope.Facturacion.Cliente,

                Estado: $scope.Facturacion.Estado.Codigo,

            };
            blockUI.delay = 1000;
            FacturasFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {

                            $scope.ListaFacturas = response.data.Datos;
                            $scope.totalRegistrosFacturacion = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasFacturacion = Math.ceil($scope.totalRegistrosFacturacion / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoFacturacion = false;
                            $scope.ResultadoSinRegistrosFacturacion = '';
                            $scope.PrimerPaginaFacturacion();
                        }
                        else {
                            $scope.totalRegistrosFacturacion = 0;
                            $scope.totalPaginasFacturacion = 0;
                            $scope.paginaActualFacturacion = 1;
                            $scope.ResultadoSinRegistrosFacturacion = 'No hay datos para mostrar';
                            $scope.BuscandoFacturacion = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        };
        $scope.PrimerPaginaFacturacion = function () {
            if ($scope.totalRegistrosFacturacion > 10) {
                $scope.ListaFacturasFiltrado = []
                $scope.paginaActualFacturacion = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListaFacturasFiltrado.push($scope.ListaFacturas[i])
                }
            } else {
                $scope.ListaFacturasFiltrado = $scope.ListaFacturas
            }
        }
        $scope.AnteriorFacturacion = function () {
            if ($scope.paginaActualFacturacion > 1) {
                $scope.paginaActualFacturacion -= 1
                var a = $scope.paginaActualFacturacion * 10
                if (a < $scope.totalRegistrosFacturacion) {
                    $scope.ListaFacturasFiltrado = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListaFacturasFiltrado.push($scope.ListaFacturas[i])
                    }
                }
            }
            else {
                $scope.PrimerPaginaFacturacion()
            }
        }
        $scope.SiguienteFacturacion = function () {
            if ($scope.paginaActualFacturacion < $scope.totalPaginasFacturacion) {
                $scope.paginaActualFacturacion += 1
                var a = $scope.paginaActualFacturacion * 10
                if (a < $scope.totalRegistrosFacturacion) {
                    $scope.ListaFacturasFiltrado = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListaFacturasFiltrado.push($scope.ListaFacturas[i])
                    }
                } else {
                    $scope.UltimaPaginaFacturacion()
                }
            } else if ($scope.paginaActualFacturacion == $scope.totalPaginasFacturacion) {
                $scope.UltimaPaginaFacturacion()
            }
        }
        $scope.UltimaPaginaFacturacion = function () {
            if ($scope.totalRegistrosFacturacion > 10 && $scope.totalPaginasFacturacion > 1) {
                $scope.paginaActualFacturacion = $scope.totalPaginasFacturacion
                var a = $scope.paginaActualFacturacion * 10
                $scope.ListaFacturasFiltrado = []
                for (var i = a - 10; i < $scope.ListaFacturas.length; i++) {
                    $scope.ListaFacturasFiltrado.push($scope.ListaFacturas[i])
                }
            }
        }
        $scope.GenerarMovimientoContableFacturacion = function (NumeroDocumento) {
            $scope.ListadoDocumentos = []
            for (var i = 0; i < $scope.ListaFacturas.length; i++) {
                if ($scope.ListaFacturas[i].Seleccionado) {
                    $scope.ListadoDocumentos.push({ NumeroDocumento: $scope.ListaFacturas[i].Numero })
                }
            }
            if ($scope.ListadoDocumentos.length > 0) {
                Response = EncabezadoComprobantesContablesFactory.Guardar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ListaDocumentos: $scope.ListadoDocumentos, Masivo: 1, TipoDocumentoOrigen: { Codigo: 170 }, Sync: true })
                if (Response.ProcesoExitoso === true) {
                    ShowSuccess('Los movimientos contables se generaron correctamente')
                    $scope.ListaFacturas = [];
                    $scope.ListaFacturasFiltrado = [];
                } else {
                    ShowError('No se generó el movimiento contable, por favor verifique la parametrización ')
                }
            } else {
                ShowError('Debe seleccionar al menos un documento')
            }
        }
        $scope.MarcarTodoFacturacion = function (check) {
            for (var i = 0; i < $scope.ListaFacturas.length; i++) {
                $scope.ListaFacturas[i].Seleccionado = check
            }
        }

        //Facturas Otros Conceptos

        $scope.BuscarFacturacionOtrosConceptos = function () {
            if (DatosRequeridosFacturacionOtrosConceptos()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    FindFacturacionOtrosConceptos();
                }
            }
        };
        function DatosRequeridosFacturacionOtrosConceptos() {
            $scope.MensajesErrorFacturacionOtrosConceptos = [];
            var continuar = true;
            if (($scope.FacturacionOtrosConceptos.FechaInicial === null || $scope.FacturacionOtrosConceptos.FechaInicial === undefined || $scope.FacturacionOtrosConceptos.FechaInicial === '')
                && ($scope.FacturacionOtrosConceptos.FechaFinal === null || $scope.FacturacionOtrosConceptos.FechaFinal === undefined || $scope.FacturacionOtrosConceptos.FechaFinal === '')
                && ($scope.FacturacionOtrosConceptos.NumeroInicial === null || $scope.FacturacionOtrosConceptos.NumeroInicial === undefined || $scope.FacturacionOtrosConceptos.NumeroInicial === '' || $scope.FacturacionOtrosConceptos.NumeroInicial === 0 || isNaN($scope.FacturacionOtrosConceptos.NumeroInicial) === true)

            ) {
                $scope.MensajesErrorFacturacionOtrosConceptos.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.FacturacionOtrosConceptos.NumeroInicial !== null && $scope.FacturacionOtrosConceptos.NumeroInicial !== undefined && $scope.FacturacionOtrosConceptos.NumeroInicial !== '' && $scope.FacturacionOtrosConceptos.NumeroInicial !== 0)
                || ($scope.FacturacionOtrosConceptos.FechaInicial !== null && $scope.FacturacionOtrosConceptos.FechaInicial !== undefined && $scope.FacturacionOtrosConceptos.FechaInicial !== '')
                || ($scope.FacturacionOtrosConceptos.FechaFinal !== null && $scope.FacturacionOtrosConceptos.FechaFinal !== undefined && $scope.FacturacionOtrosConceptos.FechaFinal !== '')

            ) {
                if (($scope.FacturacionOtrosConceptos.FechaInicial !== null && $scope.FacturacionOtrosConceptos.FechaInicial !== undefined && $scope.FacturacionOtrosConceptos.FechaInicial !== '')
                    && ($scope.FacturacionOtrosConceptos.FechaFinal !== null && $scope.FacturacionOtrosConceptos.FechaFinal !== undefined && $scope.FacturacionOtrosConceptos.FechaFinal !== '')) {
                    if ($scope.FacturacionOtrosConceptos.FechaFinal < $scope.FacturacionOtrosConceptos.FechaInicial) {
                        $scope.MensajesErrorFacturacionOtrosConceptos.push('La fecha final debe ser mayor a la fecha inicial');
                        continuar = false;
                    } else if ((($scope.FacturacionOtrosConceptos.FechaFinal - $scope.FacturacionOtrosConceptos.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorFacturacionOtrosConceptos.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.FacturacionOtrosConceptos.FechaInicial !== null && $scope.FacturacionOtrosConceptos.FechaInicial !== undefined && $scope.FacturacionOtrosConceptos.FechaInicial !== '')) {
                        $scope.FacturacionOtrosConceptos.FechaFinal = $scope.FacturacionOtrosConceptos.FechaInicial;
                    }
                    else {
                        $scope.FacturacionOtrosConceptos.FechaInicial = $scope.FacturacionOtrosConceptos.FechaFinal;
                    }
                }
            }
            if ($scope.FacturacionOtrosConceptos.NumeroInicial === undefined || $scope.FacturacionOtrosConceptos.NumeroInicial === '' || $scope.FacturacionOtrosConceptos.NumeroInicial === null)
                $scope.FacturacionOtrosConceptos.NumeroInicial = 0;

            return continuar;
        }
        function FindFacturacionOtrosConceptos() {
            $scope.ListaFacturasOtrosConceptos = [];
            $scope.ListaFacturasFiltradoOtrosConceptos = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.FacturacionOtrosConceptos.NumeroInicial,
                FechaInicial: $scope.FacturacionOtrosConceptos.NumeroInicial > 0 ? undefined : $scope.FacturacionOtrosConceptos.FechaInicial,
                FechaFinal: $scope.FacturacionOtrosConceptos.NumeroInicial > 0 ? undefined : $scope.FacturacionOtrosConceptos.FechaFinal,
                CataTipoFactura: CODIGO_CATALOGO_TIPO_FACTURA_OTROS_CONCEPTOS,
                OficinaFactura: { Codigo: $scope.FacturacionOtrosConceptos.Oficina.Codigo },
                Cliente: $scope.FacturacionOtrosConceptos.Cliente,
                Estado: $scope.FacturacionOtrosConceptos.Estado.Codigo,

            };
            blockUI.delay = 1000;
            FacturasOtrosConceptosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {

                            $scope.ListaFacturasOtrosConceptos = response.data.Datos;
                            $scope.totalRegistrosFacturacionOtrosConceptos = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasFacturacionOtrosConceptos = Math.ceil($scope.totalRegistrosFacturacionOtrosConceptos / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoFacturacionOtrosConceptos = false;
                            $scope.ResultadoSinRegistrosFacturacionOtrosConceptos = '';
                            $scope.PrimerPaginaFacturacionOtrosConceptos();
                        }
                        else {
                            $scope.totalRegistrosFacturacionOtrosConceptos = 0;
                            $scope.totalPaginasFacturacionOtrosConceptos = 0;
                            $scope.paginaActualFacturacionOtrosConceptos = 1;
                            $scope.ResultadoSinRegistrosFacturacionOtrosConceptos = 'No hay datos para mostrar';
                            $scope.BuscandoFacturacionOtrosConceptos = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        };
        $scope.PrimerPaginaFacturacionOtrosConceptos = function () {
            if ($scope.totalRegistrosFacturacionOtrosConceptos > 10) {
                $scope.ListaFacturasFiltradoOtrosConceptos = []
                $scope.paginaActualFacturacionOtrosConceptos = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListaFacturasFiltradoOtrosConceptos.push($scope.ListaFacturasOtrosConceptos[i])
                }
            } else {
                $scope.ListaFacturasFiltradoOtrosConceptos = $scope.ListaFacturasOtrosConceptos
            }
        }
        $scope.AnteriorFacturacionOtrosConceptos = function () {
            if ($scope.paginaActualFacturacionOtrosConceptos > 1) {
                $scope.paginaActualFacturacionOtrosConceptos -= 1
                var a = $scope.paginaActualFacturacionOtrosConceptos * 10
                if (a < $scope.totalRegistrosFacturacionOtrosConceptos) {
                    $scope.ListaFacturasFiltradoOtrosConceptos = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListaFacturasFiltradoOtrosConceptos.push($scope.ListaFacturasOtrosConceptos[i])
                    }
                }
            }
            else {
                $scope.PrimerPaginaFacturacionOtrosConceptos()
            }
        }
        $scope.SiguienteFacturacionOtrosConceptos = function () {
            if ($scope.paginaActualFacturacionOtrosConceptos < $scope.totalPaginasFacturacionOtrosConceptos) {
                $scope.paginaActualFacturacionOtrosConceptos += 1
                var a = $scope.paginaActualFacturacionOtrosConceptos * 10
                if (a < $scope.totalRegistrosFacturacionOtrosConceptos) {
                    $scope.ListaFacturasFiltradoOtrosConceptos = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListaFacturasFiltradoOtrosConceptos.push($scope.ListaFacturasOtrosConceptos[i])
                    }
                } else {
                    $scope.UltimaPaginaFacturacionOtrosConceptos()
                }
            } else if ($scope.paginaActualFacturacionOtrosConceptos == $scope.totalPaginasFacturacionOtrosConceptos) {
                $scope.UltimaPaginaFacturacionOtrosConceptos()
            }
        }
        $scope.UltimaPaginaFacturacionOtrosConceptos = function () {
            if ($scope.totalRegistrosFacturacionOtrosConceptos > 10 && $scope.totalPaginasFacturacionOtrosConceptos > 1) {
                $scope.paginaActualFacturacionOtrosConceptos = $scope.totalPaginasFacturacionOtrosConceptos
                var a = $scope.paginaActualFacturacionOtrosConceptos * 10
                $scope.ListaFacturasFiltradoOtrosConceptos = []
                for (var i = a - 10; i < $scope.ListaFacturasOtrosConceptos.length; i++) {
                    $scope.ListaFacturasFiltradoOtrosConceptos.push($scope.ListaFacturasOtrosConceptos[i])
                }
            }
        }
        $scope.GenerarMovimientoContableFacturacionOtrosConceptos = function (NumeroDocumento) {
            $scope.ListadoDocumentos = []
            for (var i = 0; i < $scope.ListaFacturasOtrosConceptos.length; i++) {
                if ($scope.ListaFacturasOtrosConceptos[i].Seleccionado) {
                    $scope.ListadoDocumentos.push({ NumeroDocumento: $scope.ListaFacturasOtrosConceptos[i].Numero })
                }
            }
            if ($scope.ListadoDocumentos.length > 0) {
                Response = EncabezadoComprobantesContablesFactory.Guardar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ListaDocumentos: $scope.ListadoDocumentos, Masivo: 1, TipoDocumentoOrigen: { Codigo: 170 }, Sync: true })
                if (Response.ProcesoExitoso === true) {
                    ShowSuccess('Los movimientos contables se generaron correctamente')
                    $scope.ListaFacturasOtrosConceptos = [];
                    $scope.ListaFacturasFiltradoOtrosConceptos = [];
                } else {
                    ShowError('No se generó el movimiento contable, por favor verifique la parametrización ')
                }
            } else {
                ShowError('Debe seleccionar al menos un documento')
            }
        }
        $scope.MarcarTodoFacturacionOtrosConceptos = function (check) {
            for (var i = 0; i < $scope.ListaFacturasOtrosConceptos.length; i++) {
                $scope.ListaFacturasOtrosConceptos[i].Seleccionado = check
            }
        }
        //Egresos

        $scope.BuscarComprobantesEgreso = function () {
            if (DatosRequeridosComprobantesEgreso()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    FindComprobantesEgreso();
                }
            }
        };
        function DatosRequeridosComprobantesEgreso() {
            $scope.MensajesErrorComprobantesEgreso = [];
            var continuar = true;
            if (($scope.ComprobantesEgreso.FechaInicio === null || $scope.ComprobantesEgreso.FechaInicio === undefined || $scope.ComprobantesEgreso.FechaInicio === '')
                && ($scope.ComprobantesEgreso.FechaFin === null || $scope.ComprobantesEgreso.FechaFin === undefined || $scope.ComprobantesEgreso.FechaFin === '')
                && ($scope.ComprobantesEgreso.NumeroDocumento === null || $scope.ComprobantesEgreso.NumeroDocumento === undefined || $scope.ComprobantesEgreso.NumeroDocumento === '' || $scope.ComprobantesEgreso.NumeroDocumento === 0 || isNaN($scope.ComprobantesEgreso.NumeroDocumento) === true)

            ) {
                $scope.MensajesErrorComprobantesEgreso.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.ComprobantesEgreso.NumeroDocumento !== null && $scope.ComprobantesEgreso.NumeroDocumento !== undefined && $scope.ComprobantesEgreso.NumeroDocumento !== '' && $scope.ComprobantesEgreso.NumeroDocumento !== 0)
                || ($scope.ComprobantesEgreso.FechaInicio !== null && $scope.ComprobantesEgreso.FechaInicio !== undefined && $scope.ComprobantesEgreso.FechaInicio !== '')
                || ($scope.ComprobantesEgreso.FechaFin !== null && $scope.ComprobantesEgreso.FechaFin !== undefined && $scope.ComprobantesEgreso.FechaFin !== '')

            ) {
                if (($scope.ComprobantesEgreso.FechaInicio !== null && $scope.ComprobantesEgreso.FechaInicio !== undefined && $scope.ComprobantesEgreso.FechaInicio !== '')
                    && ($scope.ComprobantesEgreso.FechaFin !== null && $scope.ComprobantesEgreso.FechaFin !== undefined && $scope.ComprobantesEgreso.FechaFin !== '')) {
                    if ($scope.ComprobantesEgreso.FechaFin < $scope.ComprobantesEgreso.FechaInicio) {
                        $scope.MensajesErrorComprobantesEgreso.push('La fecha final debe ser mayor a la fecha inicial');
                        continuar = false;
                    } else if ((($scope.ComprobantesEgreso.FechaFin - $scope.ComprobantesEgreso.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorComprobantesEgreso.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.ComprobantesEgreso.FechaInicio !== null && $scope.ComprobantesEgreso.FechaInicio !== undefined && $scope.ComprobantesEgreso.FechaInicio !== '')) {
                        $scope.ComprobantesEgreso.FechaFin = $scope.ComprobantesEgreso.FechaInicio;
                    }
                    else {
                        $scope.ComprobantesEgreso.FechaInicio = $scope.ComprobantesEgreso.FechaFin;
                    }
                }
            }
            if ($scope.ComprobantesEgreso.NumeroDocumento === undefined || $scope.ComprobantesEgreso.NumeroDocumento === '' || $scope.ComprobantesEgreso.NumeroDocumento === null)
                $scope.ComprobantesEgreso.NumeroDocumento = 0;

            return continuar;
        }
        function FindComprobantesEgreso() {
            $scope.ListaComprobantesEgreso = [];
            $scope.ListaComprobantesEgresoFiltrado = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            var filtros = {
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ComprobantesEgreso.NumeroDocumento,
                FechaInicial: $scope.ComprobantesEgreso.NumeroDocumento > 0 ? undefined : $scope.ComprobantesEgreso.FechaInicio,
                FechaFinal: $scope.ComprobantesEgreso.NumeroDocumento > 0 ? undefined : $scope.ComprobantesEgreso.FechaFin,
                Estado: $scope.ComprobantesEgreso.Estado.Codigo,
                NombreBeneficiario: $scope.ComprobantesEgreso.Beneficiario,
                NombreTercero: $scope.ComprobantesEgreso.Tercero,
                NumeroDocumentoOrigen: $scope.ComprobantesEgreso.NumeroDocumentoOrigen,
                DocumentoOrigen: $scope.ComprobantesEgreso.DocumentoOrigen,
                OficinaDestino: $scope.ComprobantesEgreso.Oficina,
            };
            blockUI.delay = 1000;
            DocumentoComprobantesFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaComprobantesEgreso = response.data.Datos;
                            $scope.totalRegistrosComprobantesEgreso = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasComprobantesEgreso = Math.ceil($scope.totalRegistrosComprobantesEgreso / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoComprobantesEgreso = false;
                            $scope.ResultadoSinRegistrosComprobantesEgreso = '';
                            $scope.PrimerPaginaComprobantesEgreso();
                        }
                        else {
                            $scope.totalRegistrosComprobantesEgreso = 0;
                            $scope.totalPaginasComprobantesEgreso = 0;
                            $scope.paginaActualComprobantesEgreso = 1;
                            $scope.ResultadoSinRegistrosComprobantesEgreso = 'No hay datos para mostrar';
                            $scope.BuscandoComprobantesEgreso = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        };
        $scope.PrimerPaginaComprobantesEgreso = function () {
            if ($scope.totalRegistrosComprobantesEgreso > 10) {
                $scope.ListaComprobantesEgresoFiltrado = []
                $scope.paginaActualComprobantesEgreso = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListaComprobantesEgresoFiltrado.push($scope.ListaComprobantesEgreso[i])
                }
            } else {
                $scope.ListaComprobantesEgresoFiltrado = $scope.ListaComprobantesEgreso
            }
        }
        $scope.AnteriorComprobantesEgreso = function () {
            if ($scope.paginaActualComprobantesEgreso > 1) {
                $scope.paginaActualComprobantesEgreso -= 1
                var a = $scope.paginaActualComprobantesEgreso * 10
                if (a < $scope.totalRegistrosComprobantesEgreso) {
                    $scope.ListaComprobantesEgresoFiltrado = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListaComprobantesEgresoFiltrado.push($scope.ListaComprobantesEgreso[i])
                    }
                }
            }
            else {
                $scope.PrimerPaginaComprobantesEgreso()
            }
        }
        $scope.SiguienteComprobantesEgreso = function () {
            if ($scope.paginaActualComprobantesEgreso < $scope.totalPaginasComprobantesEgreso) {
                $scope.paginaActualComprobantesEgreso += 1
                var a = $scope.paginaActualComprobantesEgreso * 10
                if (a < $scope.totalRegistrosComprobantesEgreso) {
                    $scope.ListaComprobantesEgresoFiltrado = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListaComprobantesEgresoFiltrado.push($scope.ListaComprobantesEgreso[i])
                    }
                } else {
                    $scope.UltimaPaginaComprobantesEgreso()
                }
            } else if ($scope.paginaActualComprobantesEgreso == $scope.totalPaginasComprobantesEgreso) {
                $scope.UltimaPaginaComprobantesEgreso()
            }
        }
        $scope.UltimaPaginaComprobantesEgreso = function () {
            if ($scope.totalRegistrosComprobantesEgreso > 10 && $scope.totalPaginasComprobantesEgreso > 1) {
                $scope.paginaActualComprobantesEgreso = $scope.totalPaginasComprobantesEgreso
                var a = $scope.paginaActualComprobantesEgreso * 10
                $scope.ListaComprobantesEgresoFiltrado = []
                for (var i = a - 10; i < $scope.ListaComprobantesEgreso.length; i++) {
                    $scope.ListaComprobantesEgresoFiltrado.push($scope.ListaComprobantesEgreso[i])
                }
            }
        }
        $scope.GenerarMovimientoContableComprobantesEgreso = function (NumeroDocumento) {
            $scope.ListadoDocumentos = []
            for (var i = 0; i < $scope.ListaComprobantesEgreso.length; i++) {
                if ($scope.ListaComprobantesEgreso[i].Seleccionado) {
                    $scope.ListadoDocumentos.push({ NumeroDocumento: $scope.ListaComprobantesEgreso[i].Codigo })
                }
            }
            if ($scope.ListadoDocumentos.length > 0) {
                Response = EncabezadoComprobantesContablesFactory.Guardar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ListaDocumentos: $scope.ListadoDocumentos, Masivo: 1, TipoDocumentoOrigen: { Codigo: 30 }, Sync: true })
                if (Response.ProcesoExitoso === true) {
                    $scope.ListaComprobantesEgreso = [];
                    $scope.ListaComprobantesEgresoFiltrado = [];
                    ShowSuccess('Los movimientos contables se generaron correctamente')
                } else {
                    ShowError('No se generó el movimiento contable, por favor verifique la parametrización ')
                }
            } else {
                ShowError('Debe seleccionar al menos un documento')
            }
        }
        $scope.MarcarTodoComprobantesEgreso = function (check) {
            for (var i = 0; i < $scope.ListaComprobantesEgreso.length; i++) {
                $scope.ListaComprobantesEgreso[i].Seleccionado = check
            }
        }
        //Ingresos

        $scope.BuscarComprobantesIngreso = function () {
            if (DatosRequeridosComprobantesIngreso()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    FindComprobantesIngreso();
                }
            }
        };
        function DatosRequeridosComprobantesIngreso() {
            $scope.MensajesErrorComprobantesIngreso = [];
            var continuar = true;
            if (($scope.ComprobantesIngreso.FechaInicio === null || $scope.ComprobantesIngreso.FechaInicio === undefined || $scope.ComprobantesIngreso.FechaInicio === '')
                && ($scope.ComprobantesIngreso.FechaFin === null || $scope.ComprobantesIngreso.FechaFin === undefined || $scope.ComprobantesIngreso.FechaFin === '')
                && ($scope.ComprobantesIngreso.NumeroDocumento === null || $scope.ComprobantesIngreso.NumeroDocumento === undefined || $scope.ComprobantesIngreso.NumeroDocumento === '' || $scope.ComprobantesIngreso.NumeroDocumento === 0 || isNaN($scope.ComprobantesIngreso.NumeroDocumento) === true)

            ) {
                $scope.MensajesErrorComprobantesIngreso.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.ComprobantesIngreso.NumeroDocumento !== null && $scope.ComprobantesIngreso.NumeroDocumento !== undefined && $scope.ComprobantesIngreso.NumeroDocumento !== '' && $scope.ComprobantesIngreso.NumeroDocumento !== 0)
                || ($scope.ComprobantesIngreso.FechaInicio !== null && $scope.ComprobantesIngreso.FechaInicio !== undefined && $scope.ComprobantesIngreso.FechaInicio !== '')
                || ($scope.ComprobantesIngreso.FechaFin !== null && $scope.ComprobantesIngreso.FechaFin !== undefined && $scope.ComprobantesIngreso.FechaFin !== '')

            ) {
                if (($scope.ComprobantesIngreso.FechaInicio !== null && $scope.ComprobantesIngreso.FechaInicio !== undefined && $scope.ComprobantesIngreso.FechaInicio !== '')
                    && ($scope.ComprobantesIngreso.FechaFin !== null && $scope.ComprobantesIngreso.FechaFin !== undefined && $scope.ComprobantesIngreso.FechaFin !== '')) {
                    if ($scope.ComprobantesIngreso.FechaFin < $scope.ComprobantesIngreso.FechaInicio) {
                        $scope.MensajesErrorComprobantesIngreso.push('La fecha final debe ser mayor a la fecha inicial');
                        continuar = false;
                    } else if ((($scope.ComprobantesIngreso.FechaFin - $scope.ComprobantesIngreso.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorComprobantesIngreso.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.ComprobantesIngreso.FechaInicio !== null && $scope.ComprobantesIngreso.FechaInicio !== undefined && $scope.ComprobantesIngreso.FechaInicio !== '')) {
                        $scope.ComprobantesIngreso.FechaFin = $scope.ComprobantesIngreso.FechaInicio;
                    }
                    else {
                        $scope.ComprobantesIngreso.FechaInicio = $scope.ComprobantesIngreso.FechaFin;
                    }
                }
            }
            if ($scope.ComprobantesIngreso.NumeroDocumento === undefined || $scope.ComprobantesIngreso.NumeroDocumento === '' || $scope.ComprobantesIngreso.NumeroDocumento === null)
                $scope.ComprobantesIngreso.NumeroDocumento = 0;

            return continuar;
        }
        function FindComprobantesIngreso() {
            $scope.ListaComprobantesIngreso = [];
            $scope.ListaComprobantesIngresoFiltrado = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            var filtros = {
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_INGRESO,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ComprobantesIngreso.NumeroDocumento,
                FechaInicial: $scope.ComprobantesIngreso.NumeroDocumento > 0 ? undefined : $scope.ComprobantesIngreso.FechaInicio,
                FechaFinal: $scope.ComprobantesIngreso.NumeroDocumento > 0 ? undefined : $scope.ComprobantesIngreso.FechaFin,
                Estado: $scope.ComprobantesIngreso.Estado.Codigo,
                NombreBeneficiario: $scope.ComprobantesIngreso.Beneficiario,
                NombreTercero: $scope.ComprobantesIngreso.Tercero,
                NumeroDocumentoOrigen: $scope.ComprobantesIngreso.NumeroDocumentoOrigen,
                DocumentoOrigen: $scope.ComprobantesIngreso.DocumentoOrigen,
                OficinaDestino: $scope.ComprobantesIngreso.Oficina,
            };
            blockUI.delay = 1000;
            DocumentoComprobantesFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaComprobantesIngreso = response.data.Datos;
                            $scope.totalRegistrosComprobantesIngreso = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasComprobantesIngreso = Math.ceil($scope.totalRegistrosComprobantesIngreso / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoComprobantesIngreso = false;
                            $scope.ResultadoSinRegistrosComprobantesIngreso = '';
                            $scope.PrimerPaginaComprobantesIngreso();
                        }
                        else {
                            $scope.totalRegistrosComprobantesIngreso = 0;
                            $scope.totalPaginasComprobantesIngreso = 0;
                            $scope.paginaActualComprobantesIngreso = 1;
                            $scope.ResultadoSinRegistrosComprobantesIngreso = 'No hay datos para mostrar';
                            $scope.BuscandoComprobantesIngreso = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        };
        $scope.PrimerPaginaComprobantesIngreso = function () {
            if ($scope.totalRegistrosComprobantesIngreso > 10) {
                $scope.ListaComprobantesIngresoFiltrado = []
                $scope.paginaActualComprobantesIngreso = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListaComprobantesIngresoFiltrado.push($scope.ListaComprobantesIngreso[i])
                }
            } else {
                $scope.ListaComprobantesIngresoFiltrado = $scope.ListaComprobantesIngreso
            }
        }
        $scope.AnteriorComprobantesIngreso = function () {
            if ($scope.paginaActualComprobantesIngreso > 1) {
                $scope.paginaActualComprobantesIngreso -= 1
                var a = $scope.paginaActualComprobantesIngreso * 10
                if (a < $scope.totalRegistrosComprobantesIngreso) {
                    $scope.ListaComprobantesIngresoFiltrado = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListaComprobantesIngresoFiltrado.push($scope.ListaComprobantesIngreso[i])
                    }
                }
            }
            else {
                $scope.PrimerPaginaComprobantesIngreso()
            }
        }
        $scope.SiguienteComprobantesIngreso = function () {
            if ($scope.paginaActualComprobantesIngreso < $scope.totalPaginasComprobantesIngreso) {
                $scope.paginaActualComprobantesIngreso += 1
                var a = $scope.paginaActualComprobantesIngreso * 10
                if (a < $scope.totalRegistrosComprobantesIngreso) {
                    $scope.ListaComprobantesIngresoFiltrado = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListaComprobantesIngresoFiltrado.push($scope.ListaComprobantesIngreso[i])
                    }
                } else {
                    $scope.UltimaPaginaComprobantesIngreso()
                }
            } else if ($scope.paginaActualComprobantesIngreso == $scope.totalPaginasComprobantesIngreso) {
                $scope.UltimaPaginaComprobantesIngreso()
            }
        }
        $scope.UltimaPaginaComprobantesIngreso = function () {
            if ($scope.totalRegistrosComprobantesIngreso > 10 && $scope.totalPaginasComprobantesIngreso > 1) {
                $scope.paginaActualComprobantesIngreso = $scope.totalPaginasComprobantesIngreso
                var a = $scope.paginaActualComprobantesIngreso * 10
                $scope.ListaComprobantesIngresoFiltrado = []
                for (var i = a - 10; i < $scope.ListaComprobantesIngreso.length; i++) {
                    $scope.ListaComprobantesIngresoFiltrado.push($scope.ListaComprobantesIngreso[i])
                }
            }
        }
        $scope.GenerarMovimientoContableComprobantesIngreso = function (NumeroDocumento) {
            $scope.ListadoDocumentos = []
            for (var i = 0; i < $scope.ListaComprobantesIngreso.length; i++) {
                if ($scope.ListaComprobantesIngreso[i].Seleccionado) {
                    $scope.ListadoDocumentos.push({ NumeroDocumento: $scope.ListaComprobantesIngreso[i].Codigo })
                }
            }
            if ($scope.ListadoDocumentos.length > 0) {
                Response = EncabezadoComprobantesContablesFactory.Guardar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ListaDocumentos: $scope.ListadoDocumentos, Masivo: 1, TipoDocumentoOrigen: { Codigo: 40 }, Sync: true })
                if (Response.ProcesoExitoso === true) {
                    ShowSuccess('Los movimientos contables se generaron correctamente')
                    $scope.ListaComprobantesIngreso = [];
                    $scope.ListaComprobantesIngresoFiltrado = [];
                } else {
                    ShowError('No se generó el movimiento contable, por favor verifique la parametrización ')
                }
            } else {
                ShowError('Debe seleccionar al menos un documento')
            }
        }
        $scope.MarcarTodoComprobantesIngreso = function (check) {
            for (var i = 0; i < $scope.ListaComprobantesIngreso.length; i++) {
                $scope.ListaComprobantesIngreso[i].Seleccionado = check
            }
        }
        $scope.MostrarLiquidaciones = function () {
            $('#SeccionLiquidaciones').show()
            $('#SeccionFancturas').hide()
            $('#SeccionComprobantesEgreso').hide()
            $('#SeccionComprobantesIngreso').hide()
            $('#SeccionFancturasOtrosConceptos').hide()
            $('#Seccionlegalizaciones').hide()

        }
        $scope.MostrarFacturas = function () {
            $('#SeccionLiquidaciones').hide()
            $('#SeccionFancturas').show()
            $('#SeccionComprobantesEgreso').hide()
            $('#SeccionComprobantesIngreso').hide()
            $('#SeccionFancturasOtrosConceptos').hide()
            $('#Seccionlegalizaciones').hide()
        }
        $scope.MostrarFacturasOtrosConceptos = function () {
            $('#SeccionLiquidaciones').hide()
            $('#SeccionFancturas').hide()
            $('#SeccionComprobantesEgreso').hide()
            $('#SeccionComprobantesIngreso').hide()
            $('#SeccionFancturasOtrosConceptos').show()
            $('#Seccionlegalizaciones').hide()
        }
        $scope.MostrarComprobantesEgreso = function () {
            $('#SeccionLiquidaciones').hide()
            $('#SeccionFancturas').hide()
            $('#SeccionComprobantesEgreso').show()
            $('#SeccionComprobantesIngreso').hide()
            $('#SeccionFancturasOtrosConceptos').hide()
            $('#Seccionlegalizaciones').hide()
        }
        $scope.MostrarComprobantesIngreso = function () {
            $('#SeccionLiquidaciones').hide()
            $('#SeccionFancturas').hide()
            $('#SeccionComprobantesEgreso').hide()
            $('#SeccionComprobantesIngreso').show()
            $('#SeccionFancturasOtrosConceptos').hide()
            $('#Seccionlegalizaciones').hide()
        }
        $scope.Mostrarlegalizaciones = function () {
            $('#Seccionlegalizaciones').show()
            $('#SeccionLiquidaciones').hide()
            $('#SeccionFancturas').hide()
            $('#SeccionComprobantesEgreso').hide()
            $('#SeccionComprobantesIngreso').hide()
            $('#SeccionFancturasOtrosConceptos').hide()

        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskValoresGrid = function (valor) {
            return MascaraValores(valor)
        };


    }]);