﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Tesoreria

Namespace Despachos
    <JsonObject>
    Public NotInheritable Class DetalleLiquidacion
        Inherits Base
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Liquidacion"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            Me.CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Me.Liquidacion = New Liquidacion With {.Numero = Read(lector, "ELPD_Numero"), .NumeroDocumento = Read(lector, "Numero_Documento")}
            Me.ConceptoLiquidacion = New ConceptoLiquidacionPlanillaDespacho With {.Codigo = Read(lector, "CLPD_Codigo"), .Nombre = Read(lector, "Nombre"), .DocumentoCuenta = New EncabezadoDocumentoCuentas With {.NumeroDocumento = Read(lector, "Documento_Origen")}}
            Me.Observaciones = Read(lector, "Observaciones")
            Me.Valor = Read(lector, "Valor")
            Me.Operacion = Read(lector, "Operacion")
            Me.EsConceptoSistema = Read(lector, "Concepto_Sistema")
            DocumentoCuentaCodigo = Read(lector, "ENDC_Codigo")
            Me.AplicaValorBase = Read(lector, "Aplica_Valor_Base_Impuestos")
        End Sub

#Region "Propiedades"

        <JsonProperty>
        Public Property Liquidacion As Liquidacion

        <JsonProperty>
        Public Property ConceptoLiquidacion As ConceptoLiquidacionPlanillaDespacho

        <JsonProperty>
        Public Property Observaciones As String

        <JsonProperty>
        Public Property Valor As Double

        <JsonProperty>
        Public Property Operacion As Integer

        <JsonProperty>
        Public Property EsConceptoSistema As Integer

        <JsonProperty>
        Public Property ID As Integer

        <JsonProperty>
        Public Property AplicaValorBase As Integer

        <JsonProperty>
        Public Property DocumentoCuentaCodigo As Integer

#End Region

    End Class
End Namespace
