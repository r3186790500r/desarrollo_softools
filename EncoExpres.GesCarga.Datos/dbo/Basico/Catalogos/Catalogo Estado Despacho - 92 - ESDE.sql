﻿PRINT 'Catalogo Estado Despacho - 92 - ESDE'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 92
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 92
GO
DELETE Catalogos WHERE Codigo = 92
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos (EMPR_Codigo, Codigo, Nombre_Corto, Nombre, Actualizable, Numero_Columnas, Numero_Campos_Llave)
SELECT Codigo, 92, 'ESDE','Estado Despacho', 1, 2, 1 FROM Empresas
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 92, 9201, 'Pendiente', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 92, 9202, 'Incompleta', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 92, 9203, 'Despachada', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 92, 9204, 'Finalizada', '', '', '', 1, 1, GETDATE() FROM Empresas
GO