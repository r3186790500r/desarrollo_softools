﻿EncoExpresApp.controller("ConsultarOficinasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'OficinasFactory', 'TipoDocumentosFactory', 'blockUI', function ($scope, $routeParams, $timeout, $linq, OficinasFactory, TipoDocumentosFactory, blockUI) {
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Oficinas' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
    $scope.Buscando = false;
    $scope.ResultadoSinRegistros = '';
    $scope.MensajesError = [];
    var filtros = {};
    $scope.ListadoEstados = [];
    $scope.MostrarMensajeError = false
    $scope.paginaActual = 1;
    $scope.cantidadRegistrosPorPagina = 10;
    $scope.TMPOficina;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_OFICINAS); 
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    $scope.ListadoEstados = [
        { Nombre: '(TODAS)', Codigo: -1 },
        { Nombre: 'ACTIVA', Codigo: 1 },
        { Nombre: 'INACTIVA', Codigo: 0 }
    ];
    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
    /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
        
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();


        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
            
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
        
    };
    /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

    /*Metodo buscar*/
    $scope.Buscar = function () {

        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = 0;
            Find();
           
        }

    };
    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarOficinas';
        }
    };
    /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
    function Find() {
       
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);

        $scope.Buscando = true;
        $scope.MensajesError = [];

        $scope.ListaOficinas = [];

        DatosRequeridos();
        filtros = {
            Pagina: $scope.paginaActual,
            RegistrosPagina: $scope.cantidadRegistrosPorPagina,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Codigo,
            Nombre: $scope.Nombre,
            NombreCiudad: $scope.Ciudad,
            Estado: $scope.ModalEstado.Codigo,
            CodigoAlterno: $scope.CodigoAlterno,
            AplicaConsultaMaster: 1,
        };

        if ($scope.MensajesError.length == 0) {
            blockUI.delay = 1000;
            OficinasFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Codigo > 0) {
                                    $scope.ListaOficinas.push(item);
                                }

                            });

                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = true;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.ListaOficinas = "";
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    $scope.Buscando = false;
                });
        }
        else {
            $scope.Buscando = false;
        }
        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
    function DatosRequeridos() {
        if (filtros.Codigo == undefined) {
            filtros.Codigo = 0;
        }
    }
    /*------------------------------------------------------------------------------------Eliminar Oficinas-----------------------------------------------------------------------*/
    $scope.EliminarOficina = function (codigo, Nombre, CodigoAlterno) {
        $scope.Codigo = codigo
        $scope.NombreOficinaEliminada = Nombre
        $scope.ModalErrorCompleto = ''
        $scope.CodigoAlternoOficinaEliminada = CodigoAlterno
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        showModal('modalEliminarOficinas');
    };

    $scope.Eliminar = function () {
        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Codigo,
        };

        OficinasFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se eliminó la oficina ' + $scope.NombreOficinaEliminada);
                    closeModal('modalEliminarOficinas');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                var result = ''
                showModal('modalMensajeEliminarOficinas');
                result = InternalServerError(response.statusText)
                $scope.ModalError = 'No se puede eliminar la oficina ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                $scope.ModalErrorCompleto = response.statusText

            });

    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    /*Consecutivos*/
    $scope.TMPConsecutivos = {};
    $scope.isEditConsecutivo = false;
    $scope.ModalConsecutivos = {
        Oficina: '',
        TipoDocumento: '',
        Consecutivo: '',
        ConsecutivoFin: '',
        AvisoConRestantes: ''
    }

    /*FinConsecutivos*/

    /*Combo TipoDocumento*/
    $scope.ListadoTiposDocumento = TipoDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos;
   
    /*Fin Combo TipoDocumento*/

    /* Obtener parametros*/
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Codigo = $routeParams.Codigo;
        
        Find();
    }

    $scope.CerrarModal = function () {
        closeModal('modalEliminarOficinas');
        closeModal('modalMensajeEliminarOficinas');
    }

    $scope.MaskNumero = function () {
        $scope.CodigoInicial = MascaraNumero($scope.CodigoInicial)
        $scope.CodigoFinal = MascaraNumero($scope.CodigoFinal)
        $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
    };

    // Modal Consecutivos

    $scope.AbrirListaDocumentos = function (item) {
        $scope.TMPConsecutivos = item;
        showModal('modalConsecutivos');
        $scope.LimpiarmodalConsecutivos();
        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: item.Codigo,
            ConsultarConsecutivosTDOficina: true
        }
        blockUI.delay = 1000;
        OficinasFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos !== undefined) {
                        $scope.ListaConsecutivos = response.data.Datos.Consecutivos;
                        $scope.TMPOficina = {Codigo:item.Codigo, Nombre: item.Nombre}
                        OrdenarIndicesConsecutivoTido();
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
    }

    // --fin Modal Consecutivos

    $scope.AdicionarConsecutivo = function () {
        if (DatosRequeridosConsecutivo()) {
            if ($scope.isEditConsecutivo == false) {
                $scope.ListaConsecutivos.push($scope.ModalConsecutivos);
                OrdenarIndicesConsecutivoTido();
            }
            else {
                $scope.ListaConsecutivos[$scope.ModalConsecutivos.indice] = $scope.ModalConsecutivos;
            }
            $scope.LimpiarmodalConsecutivos();
        }
    }
    function DatosRequeridosConsecutivo() {
        $scope.MensajesErrorConsecutivos = [];
        var continuar = true;

        if ($scope.ModalConsecutivos.TipoDocumento == undefined || $scope.ModalConsecutivos.TipoDocumento == "" || $scope.ModalConsecutivos.TipoDocumento == null) {
            $scope.MensajesErrorConsecutivos.push('Debe seleccionar un tipo de documento');
            continuar = false;
        }

        if ($scope.ModalConsecutivos.Consecutivo == undefined || $scope.ModalConsecutivos.Consecutivo == "" || $scope.ModalConsecutivos.Consecutivo == null) {
            $scope.MensajesErrorConsecutivos.push('Debe ingresar un consecutivo');
            continuar = false;
        }

        if ($scope.ModalConsecutivos.ConsecutivoFin == undefined || $scope.ModalConsecutivos.ConsecutivoFin == "" || $scope.ModalConsecutivos.ConsecutivoFin == null) {
            $scope.MensajesErrorConsecutivos.push('Debe ingresar un límite de consecutivo');
            continuar = false;
        }

        if ($scope.ModalConsecutivos.ConsecutivoFin <= $scope.ModalConsecutivos.Consecutivo) {
            $scope.MensajesErrorConsecutivos.push('El consecutivo Hasta debe ser mayor al consecutivo Desde');
            continuar = false;
        }

        if ($scope.ModalConsecutivos.AvisoConRestantes > ($scope.ModalConsecutivos.ConsecutivoFin - $scope.ModalConsecutivos.Consecutivo)) {
            $scope.MensajesErrorConsecutivos.push('El valor de aviso documentos faltantes no debe ser mayor a : ' + ($scope.ModalConsecutivos.ConsecutivoFin - $scope.ModalConsecutivos.Consecutivo) +' (Consecutivo Hasta - Consecutivo) ');
            continuar = false;
        }

        if ($scope.ModalConsecutivos.AvisoConRestantes == undefined || $scope.ModalConsecutivos.AvisoConRestantes == "" || $scope.ModalConsecutivos.AvisoConRestantes == null) {
            $scope.MensajesErrorConsecutivos.push('Debe ingresar un valor de aviso');
            continuar = false;
        }

        var indice = $scope.ModalConsecutivos.indice != undefined ? $scope.ModalConsecutivos.indice : -1;

        if (continuar == true) {
            for (var i = 0; i < $scope.ListaConsecutivos.length; i++) {
                var item = $scope.ListaConsecutivos[i];
                if ($scope.isEditConsecutivo == true) {
                    /*if (indice != i) {
                         if (item.TipoDocumento.Codigo == $scope.ModalConsecutivos.TipoDocumento.Codigo) {
                             $scope.MensajesErrorConsecutivos.push('El tipo de documento "' + item.TipoDocumento.Nombre+'" ya esta agregado');
                             continuar = false;
                             break;
                         }
                    }*/
                }
                else {
                    if (item.TipoDocumento.Codigo == $scope.ModalConsecutivos.TipoDocumento.Codigo) {
                        $scope.MensajesErrorConsecutivos.push('El tipo de documento "' + item.TipoDocumento.Nombre + '" ya esta agregado');
                        continuar = false;
                        break;
                    }
                }
            }
        }

        return continuar;
    }

    $scope.LimpiarmodalConsecutivos = function () {
        $scope.ModalConsecutivos = {
            TipoDocumento: '',
            Consecutivo: '',
            ConsecutivoFin: '',
            AvisoConRestantes: ''
        };
        $scope.ModalConsecutivos.TipoDocumento = $linq.Enumerable().From($scope.ListadoTiposDocumento).First('$.Codigo == ' + $scope.ListadoTiposDocumento[0].Codigo);
        $scope.MensajesErrorConsecutivos = [];
        $scope.isEditConsecutivo = false;
    }

    // -- Editar
    $scope.EditarConsecutivo = function (item) {
        $scope.LimpiarmodalConsecutivos();
        $scope.ModalConsecutivos = angular.copy(item);
        $scope.ModalConsecutivos.TipoDocumento = $linq.Enumerable().From($scope.ListadoTiposDocumento).First('$.Codigo ==' + item.TipoDocumento.Codigo);
        $scope.ModalConsecutivos.Consecutivo = item.Consecutivo;
        $scope.ModalConsecutivos.ConsecutivoFin = item.ConsecutivoFin;
        $scope.ModalConsecutivos.AvisoConRestantes = item.AvisoConRestantes;
        $scope.isEditConsecutivo = true;
        $scope.MaskNumero(); $scope.MaskValores();
    }
    //-- Fin Editar

    //--Eliminar
    $scope.TmpEliminarConsecutivo = {};
    $scope.ConfirmarEliminarConsecutivo = function (item) {
        $scope.TmpEliminarConsecutivo = { Indice: item.indice };
        showModal('modalEliminarConsecutivo');
    };

    $scope.EliminarConsecutivo = function (indice) {
        $scope.ListaConsecutivos.splice(indice, 1);
        OrdenarIndicesConsecutivoTido();
        closeModal('modalEliminarConsecutivo');
    };


    $scope.GuardarConsecutivos = function () {
        if ($scope.ListaConsecutivos.length > 0) {
            var tmpConsecutivos = [];
            for (var i = 0; i < $scope.ListaConsecutivos.length; i++) {
                tmpConsecutivos.push({
                    Codigo: $scope.ListaConsecutivos[i].Codigo == undefined ? 0 : $scope.ListaConsecutivos[i].Codigo,
                    TipoDocumento: { Codigo: $scope.ListaConsecutivos[i].TipoDocumento.Codigo },
                    Consecutivo: $scope.ListaConsecutivos[i].Consecutivo,
                    ConsecutivoFin: $scope.ListaConsecutivos[i].ConsecutivoFin,
                    AvisoConRestantes: $scope.ListaConsecutivos[i].AvisoConRestantes
                });
            }
            var Consecutivos = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.TMPConsecutivos.Codigo,
                Consecutivos: tmpConsecutivos,

            };

            OficinasFactory.InsertarConsecutivos(Consecutivos).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Los consecutivos se agregaron satisfactoriamente')
                        closeModal('modalConsecutivos')
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {

            ShowError('Debe agregar al menos un consecutivo');
        }
    }

    function OrdenarIndicesConsecutivoTido() {
        for (var i = 0; i < $scope.ListaConsecutivos.length; i++) {
            $scope.ListaConsecutivos[i].indice = i;
        }
    }

    $scope.MaskNumero = function (option) {
        MascaraNumeroGeneral($scope);
    };

    $scope.MaskValores = function (option) {
        MascaraValoresGeneral($scope);
    };

}]);