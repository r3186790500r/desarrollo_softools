﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 04/10/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_eliminar_puertos_paises'
GO
DROP PROCEDURE gsp_eliminar_puertos_paises
GO
CREATE PROCEDURE gsp_eliminar_puertos_paises 
(  
  @par_EMPR_Codigo SMALLINT,  
  @par_Codigo NUMERIC  
  )  
AS  
BEGIN  
    
  DELETE Puertos_Paises   
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
    AND Codigo = @par_Codigo  
  
    SELECT @@ROWCOUNT AS Numero    
END  
GO