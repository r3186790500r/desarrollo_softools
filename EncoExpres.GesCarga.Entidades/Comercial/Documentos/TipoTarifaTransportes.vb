﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref=" TipoTarifaTransportes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TipoTarifaTransportes
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoTarifaTransportes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoTarifaTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            ValorRangoInicial = Read(lector, "Rango_Inicial")
            ValorRangoFinal = Read(lector, "Rango_Final")
            ValorCatalogo = New ValorCatalogos With {.Codigo = Read(lector, "VACA_Codigo"), .Catalogo = New Catalogos With {.Codigo = Read(lector, "CATA_Codigo")}}
            TarifaTransporte = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo")}
            Try
                If Not IsDBNull(Read(lector, "Nombre_Alterno")) Then
                    NombreAlterno = Read(lector, "Nombre_Alterno")
                End If
            Catch ex As Exception
            End Try

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        ''' 
        <JsonProperty>
        Public Property ValorRangoInicial As Double
        <JsonProperty>
        Public Property ValorRangoFinal As Double

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property NombreAlterno As String
        <JsonProperty>
        Public Property TarifaTransporte As TarifaTransportes
        <JsonProperty>
        Public Property OpcionPlantilla As Integer
        <JsonProperty>
        Public Property TipoLineaNegocioTransporte As TipoLineaNegocioTransportes
        <JsonProperty>
        Public Property ValorFlete As Double
        <JsonProperty>
        Public Property ValorFleteTransportador As Double
        <JsonProperty>
        Public Property ValorCatalogo As ValorCatalogos
    End Class
End Namespace
