﻿EncoExpresApp.controller("CambiarEstadoDefinitivoRemesasCtrl", ['$scope', '$timeout', '$linq', 'blockUI', '$routeParams', 'RemesaGuiasFactory', 'EmpresasFactory',
    'OficinasFactory', 'blockUIConfig',
    function ($scope, $timeout, $linq, blockUI, $routeParams, RemesaGuiasFactory, EmpresasFactory,
        OficinasFactory, blockUIConfig) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CAMBIAR ESTADO DEFINITIVO REMESAS';
        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Procesos' }, { Nombre: 'Cambiar Estado Definitivo Remesas' }];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CAMBIAR_ESTADO_REMESAS);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.pref = '';

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas
        };
        $scope.ListadoGuias = [];
        $scope.ListadoOficinasCrea = [
            { Codigo: 0, Nombre: '(TODAS)' }
        ];
        $scope.ListadoOficinasActual = [
            { Codigo: 0, Nombre: '(TODAS)' }
        ];
        $scope.PagRemesas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Init
        $scope.InitLoad = function () {
            //--Oficinas
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinasCrea.push(item);
                            $scope.ListadoOficinasActual.push(item);
                        });
                        $scope.Modelo.OficinaCrea = $scope.ListadoOficinasCrea[0];
                        $scope.Modelo.OficinaActual = $scope.ListadoOficinasActual[0];
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN
        function ResetPaginacion(obj) {
            obj.totalRegistros = obj.array.length;
            obj.totalPaginas = Math.ceil(obj.totalRegistros / 10);
            $scope.PrimerPagina(obj);
        }
        $scope.PrimerPagina = function (obj) {
            if (obj.totalRegistros > 10) {
                obj.PagArray = [];
                obj.paginaActual = 1;
                for (var i = 0; i < 10; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            } else {
                obj.PagArray = obj.array;
            }
        };
        $scope.Anterior = function (obj) {
            if (obj.paginaActual > 1) {
                obj.paginaActual -= 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                }
            }
            else {
                $scope.PrimerPagina(obj);
            }
        };
        $scope.Siguiente = function (obj) {
            if (obj.paginaActual < obj.totalPaginas) {
                obj.paginaActual += 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                } else {
                    $scope.UltimaPagina(obj);
                }
            } else if (obj.paginaActual == obj.totalPaginas) {
                $scope.UltimaPagina(obj);
            }
        };
        $scope.UltimaPagina = function (obj) {
            if (obj.totalRegistros > 10 && obj.totalPaginas > 1) {
                obj.paginaActual = obj.totalPaginas;
                var a = obj.paginaActual * 10;
                obj.PagArray = [];
                for (var i = a - 10; i < obj.array.length; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            }
        };
        //----------------------------Funciones Generales---------------------------------//
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO && DatosRequeridos()) {
                PantallaBloqueo(Find);
            }
        };

        function Find() {
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroInicial: $scope.Modelo.NumeroRemesa,
                FechaInicial: $scope.Modelo.FechaInicial,
                FechaFinal: $scope.Modelo.FechaFinal,
                Numeracion: $scope.Modelo.Preimpreso,
                Oficina: { Codigo: $scope.Modelo.OficinaCrea.Codigo },
                OficinaActual: { Codigo: $scope.Modelo.OficinaActual.Codigo },
                Estado: ESTADO_BORRADOR,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA
            };
            console.log(filtro)
            RemesaGuiasFactory.Consultar(filtro).
                then(function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoGuias = response.data.Datos;
                            $scope.PagRemesas.array = $scope.ListadoGuias;
                            $scope.PagRemesas.ResultadoSinRegistros = '';
                            ResetPaginacion($scope.PagRemesas);
                        }
                        else {
                            $scope.PagRemesas.totalRegistros = 0;
                            $scope.PagRemesas.totalPaginas = 0;
                            $scope.PagRemesas.paginaActual = 1;
                            $scope.PagRemesas.array = [];
                            ResetPaginacion($scope.PagRemesas);
                            $scope.PagRemesas.ResultadoSinRegistros = 'No hay datos para mostrar';
                        }
                    }
                }, function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    ShowError(response.statusText);
                });
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroRemesa === null || $scope.Modelo.NumeroRemesa === undefined || $scope.Modelo.NumeroRemesa === '' || $scope.Modelo.NumeroRemesa === 0 || isNaN($scope.Modelo.NumeroRemesa))
                && ($scope.Modelo.Preimpreso === null || $scope.Modelo.Preimpreso === undefined || $scope.Modelo.Preimpreso === '' || $scope.Modelo.Preimpreso === 0)) {
                $scope.MensajesError.push('Debe ingresar los filtros de fecha, número o Preimpreso');
                continuar = false;

            } else if (($scope.Modelo.NumeroRemesa !== null && $scope.Modelo.NumeroRemesa !== undefined && $scope.Modelo.NumeroRemesa !== '' && $scope.Modelo.NumeroRemesa !== 0)
                || ($scope.Modelo.Preimpreso !== null || $scope.Modelo.Preimpreso !== undefined || $scope.Modelo.Preimpreso !== '' || $scope.Modelo.Preimpreso !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {


                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    }

                    else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                } else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial;
                    }
                    else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal;
                    }
                }
                if (($scope.Modelo.Preimpreso !== null && $scope.Modelo.Preimpreso !== undefined && $scope.Modelo.Preimpreso !== '' && $scope.Modelo.Preimpreso !== 0 && isNaN($scope.Modelo.Preimpreso) === true)) {
                    $scope.Modelo.Preimpreso = $scope.Modelo.Preimpreso;
                }
            }
            return continuar;
        }

        // Marcar o Desmarcar
        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListadoGuias.length; i++) {
                    $scope.ListadoGuias[i].Marcado = true;
                }
            }
            else {
                for (var i = 0; i < $scope.ListadoGuias.length; i++) {
                    $scope.ListadoGuias[i].Marcado = false;
                }
            }
        };

        $scope.CambiarEstado = function (item) {
            var countmarcadas = 0;
            if ($scope.ListadoGuias.length > 0) {
                for (var i = 0; i < $scope.ListadoGuias.length; i++) {
                    if ($scope.ListadoGuias[i].Marcado) {
                        countmarcadas++;
                    }
                }
                if (countmarcadas > 0) {
                    showModal('modalConfirmacionCarbiarEstado');
                }
                else {
                    ShowError('Debe seleccionar al menos una remesa');
                }
            }
            else {
                ShowError('Debe seleccionar al menos una remesa');
            }
        };

        $scope.CambiarEstadoRemesas = function () {
            closeModal('modalConfirmacionCarbiarEstado');
            blockUI.start();
            blockUI.message('Procesando Remesas...');
            $timeout(function () {
                var Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

                    RemesaPaqueteria: []
                };

                for (var obj = 0; obj < $scope.ListadoGuias.length; obj++) {
                    if ($scope.ListadoGuias[obj].Marcado) {
                        Entidad.RemesaPaqueteria.push({
                            Numero: $scope.ListadoGuias[obj].Remesa.Numero,
                            NumeroDocumento: $scope.ListadoGuias[obj].Remesa.NumeroDocumento,
                            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
                            Estado: ESTADO_DEFINITIVO
                        });
                    }
                }

                RemesaGuiasFactory.CambiarEstadoRemesas(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('El estado de las remesas se modificó correctamente');
                            Find();
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                blockUI.stop();
                $timeout(blockUI.stop(), 1000);
            }, 1000);
        };
        //----------------------------Funciones Generales---------------------------------//
    }]);