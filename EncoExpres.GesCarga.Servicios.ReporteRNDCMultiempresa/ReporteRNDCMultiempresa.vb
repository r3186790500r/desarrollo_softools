﻿Imports System.Threading
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System.Text.RegularExpressions
Imports EncoExpres.GesCarga.Servicios
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Despachos.Procesos
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Despachos

Public Class ReporteRNDCMultiempresa

#Region "Variables"

    'Variables Bandeja Salida Correos
    Private lonNumeDocu As Long
    Private strDest As String
    Private strDestCopi As String
    Private strCuerCorre As String
    Private intEstaCorr As Integer
    Private strAsunCorr As String
    Private intNumeInteEnvi As Integer

    'Variables Documento Envio Correos
    Private strDocumento As Byte()
    Private strExtensionDocumento As String

    'Variables Configuración Servidor Correo
    Private strRemi As String
    Private strClieSmtp As String
    Private intPuerSmtp As Integer
    Private bytRequAuteSmtp As Byte
    Private strUsuaCorre As String
    Private strClavCorreo As String
    Private bytAuteSSL As Byte
    Private strFirma As String

    'Variables Generales del Servicio
    Private intCodiEmpr As Integer
    Private intTiemSuspServ As Integer
    Private strRutaLog As String
    Private strNombLog As String
    Private strMensajeError As String

    'Variables SQL
    Private dsDataSet As DataSet
    Private sqlComando As SqlCommand
    Private strCadenaDeConexionSQL As String
    Private strCadenaDeConexionSQLDocumental As String
    Private sqlConexion As SqlConnection
    Private sqlConexionDocumental As SqlConnection
    Private services As DetalleSeguimientoVehiculosController
    Private thrProceso As Thread
    Private stmStreamW As Stream
    Private stwStreamWriter As StreamWriter

#End Region

#Region "Constantes"

    Public Const ESTADO_ENVIADO As Integer = 1
    Public Const ESTADO_NO_ENVIADO As Integer = 0
    Public Enum OPCIONES_REPORTE_RNDC
        REMESA = 1
        REMESA_CUMPLIDA = 2
        REMESA_ANULADA = 3
        REMESA_CUMLIDA_ANULADA = 4
        MANIFIESTO = 5
        MANIFIESTO_CUMPLIDO = 6
        MANIFIESTO_ANULADO = 7
        MANIFIESTO_CUMPLIDO_ANULADO = 8
        REMESA_CUMPLIDO_INICIAL = 9
        ACEPTACION_ELECTRONICA = 10
    End Enum
#End Region

#Region "Propiedades"

    Public Property IdCorreo() As Integer

    Public Property NumeroDocumento() As Long
        Get
            Return lonNumeDocu
        End Get
        Set(ByVal value As Long)
            lonNumeDocu = value
        End Set
    End Property

    Public Property Destinatarios() As String
        Get
            Return strDest
        End Get
        Set(ByVal value As String)
            strDest = value
        End Set
    End Property

    Public Property DestinatariosCopia() As String
        Get
            Return strDestCopi
        End Get
        Set(ByVal value As String)
            strDestCopi = value
        End Set
    End Property
    Public Property Documento() As Byte()
        Get
            Return strDocumento
        End Get
        Set(ByVal value As Byte())
            strDocumento = value
        End Set
    End Property

    Public Property ExtensionDocumento() As String
        Get
            Return strExtensionDocumento
        End Get
        Set(ByVal value As String)
            strExtensionDocumento = value
        End Set
    End Property

    Public Property CuerpoCorreo() As String
        Get
            Return strCuerCorre
        End Get
        Set(ByVal value As String)
            strCuerCorre = value
        End Set
    End Property

    Public Property Estado() As Integer
        Get
            Return intEstaCorr
        End Get
        Set(ByVal value As Integer)
            intEstaCorr = value
        End Set
    End Property

    Public Property Asunto() As String
        Get
            Return strAsunCorr
        End Get
        Set(ByVal value As String)
            strAsunCorr = value
        End Set
    End Property

    Public Property NumeroIntentosEnvio() As Integer
        Get
            Return intNumeInteEnvi
        End Get
        Set(ByVal value As Integer)
            intNumeInteEnvi = value
        End Set
    End Property

    Public Property Remitente() As String
        Get
            Return strRemi
        End Get
        Set(ByVal value As String)
            strRemi = value
        End Set
    End Property

    Public Property ServidorSMTP() As String
        Get
            Return strClieSmtp
        End Get
        Set(ByVal value As String)
            strClieSmtp = value
        End Set
    End Property

    Public Property PuertoSMTP() As Integer
        Get
            Return intPuerSmtp
        End Get
        Set(ByVal value As Integer)
            intPuerSmtp = value
        End Set
    End Property

    Public Property RequiereAutenticacionSMTP() As Byte
        Get
            Return bytRequAuteSmtp
        End Get
        Set(ByVal value As Byte)
            bytRequAuteSmtp = value
        End Set
    End Property

    Public Property UsuarioCorreo() As String
        Get
            Return strUsuaCorre
        End Get
        Set(ByVal value As String)
            strUsuaCorre = value
        End Set
    End Property

    Public Property ClaveCorreo() As String
        Get
            Return strClavCorreo
        End Get
        Set(ByVal value As String)
            strClavCorreo = value
        End Set
    End Property

    Public Property AutenticacionSSL() As Byte
        Get
            Return bytAuteSSL
        End Get
        Set(ByVal value As Byte)
            bytAuteSSL = value
        End Set
    End Property

    Public Property Firma() As String
        Get
            Return strFirma
        End Get
        Set(ByVal value As String)
            strFirma = value
        End Set
    End Property

    Public Property CodigoEmpresa() As Integer
        Get
            Return intCodiEmpr
        End Get
        Set(ByVal value As Integer)
            intCodiEmpr = value
        End Set
    End Property

    Public Property TiempoSuspencionServicio() As Integer
        Get
            Return intTiemSuspServ
        End Get
        Set(ByVal value As Integer)
            intTiemSuspServ = value
        End Set
    End Property

    Public Property Proceso() As Thread
        Get
            Return thrProceso
        End Get
        Set(ByVal value As Thread)
            thrProceso = value
        End Set
    End Property

    Public Property RutaLog() As String
        Get
            Return strRutaLog
        End Get
        Set(ByVal value As String)
            strRutaLog = value
        End Set
    End Property

    Public Property NombreLog() As String
        Get
            Return strNombLog
        End Get
        Set(ByVal value As String)
            strNombLog = value
        End Set
    End Property

    'Public Property ArchivoLog() As StreamWriter
    '    Get
    '        Return strArchLog
    '    End Get
    '    Set(ByVal value As StreamWriter)
    '        strArchLog = value
    '    End Set
    'End Property

    Public Property MensajeError() As String
        Get
            Return strMensajeError
        End Get
        Set(ByVal value As String)
            strMensajeError = value
        End Set
    End Property

    Public Property ComandoSQL() As SqlCommand
        Get
            Return sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            sqlComando = value
        End Set
    End Property

    Public Property ConexionSQL() As SqlConnection
        Get
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ConexionSQLDocumental() As SqlConnection
        Get
            If IsNothing(Me.sqlConexionDocumental) Then
                Me.sqlConexionDocumental = New SqlConnection(Me.strCadenaDeConexionSQLDocumental)
            End If
            Return Me.sqlConexionDocumental
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexionDocumental) Then
                Me.sqlConexionDocumental = New SqlConnection(Me.strCadenaDeConexionSQLDocumental)
            End If
            Me.sqlConexionDocumental = value
        End Set
    End Property


#End Region

#Region "Constructor"

    Public Sub New()
        InitializeComponent()
        'Iniciar_Proceso()
        Me.thrProceso = New Thread(AddressOf Iniciar_Proceso)
        Me.thrProceso.Start()
    End Sub

#End Region

#Region "Funciones Privadas"
    Protected Overrides Sub OnStart(ByVal args() As String)
        'Me.thrProceso = New Thread(AddressOf Iniciar_Proceso)
        'thrProceso.Start()
    End Sub

    Protected Overrides Sub OnStop()
        'Me.thrProceso.Abort()
    End Sub

#End Region

#Region "Funcionamiento del Servicio"

    Private Sub Iniciar_Proceso()
        Try

            While (True)
                'Dim ArchivoLog As FileStream
                If Cargar_Valores_AppConfig() Then
                    Guardar_Mensaje_Log("----------Inicia Proceso Reporte RNDC---------")
                    'Call ReportarDocumentosPruebas()
                    'Quitar Comentario SC
                    Call ReportarDocumentos()
                End If
                Thread.Sleep(Val(Me.TiempoSuspencionServicio))
            End While
        Catch ex As Exception
            Guardar_Mensaje_Log("Error Iniciar_Proceso: " & ex.Message)
            'Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            'Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: " & ex.Message)
            'Me.ArchivoLog.Close()
        End Try
    End Sub

    Public Function Email_Valido(ByVal Email As String) As Boolean
        Try
            Dim rgxPatronCorreo As Regex = New Regex("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
            Dim mthEvaluaCorreo As Match = rgxPatronCorreo.Match(Email)

            If mthEvaluaCorreo.Success Then
                Email_Valido = True
            Else
                Email_Valido = False
            End If
        Catch ex As Exception
            Email_Valido = False
        End Try
    End Function

    Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try
    End Function

    Function Retorna_Dataset_Documental(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQLDocumental)
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset_Documental = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try
    End Function


    Function Cargar_Valores_AppConfig() As Boolean
        Try
            Me.CodigoEmpresa = AppSettings.Get("CodigoEmpresa")
            Me.strCadenaDeConexionSQL = AppSettings.Get("ConexionSQL")
            Me.strCadenaDeConexionSQLDocumental = AppSettings.Get("ConexionSQLDocumental")
            Me.TiempoSuspencionServicio = AppSettings.Get("TiempoSuspencionProceso")
            Me.NombreLog = AppSettings.Get("NombreLog")
            Me.RutaLog = AppSettings.Get("RutaArchivoLog")
            Cargar_Valores_AppConfig = True
        Catch ex As Exception
            'Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            'Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: " & ex.Message)
            'Me.ArchivoLog.Close()
            Guardar_Mensaje_Log("Error Cargar_Valores_AppConfig: " & ex.Message)
            Cargar_Valores_AppConfig = False
        End Try
    End Function

    Private Sub ReportarDocumentosPruebas()
        Dim ServicioRNDC = New ReporteMinisterioController
        Dim Entidad As New ReporteMinisterio
        Dim Manifiesto As New Manifiesto
        Dim DetalleReporteRemesas As New List(Of Remesas)
        Dim DetalleReporteManifiesto As New List(Of Manifiesto)
        Dim Reslut As Respuesta(Of IEnumerable(Of ReporteMinisterio))
        If Me.CodigoEmpresa > 0 Then
            Entidad.ConsultaServicioAutomatico = 0
            Entidad.CodigoEmpresa = Me.CodigoEmpresa
        Else
            Entidad.ConsultaServicioAutomatico = 1
        End If
#Region "Consulta Aceptacion Electronica"
        'Consulta Aceptacion Electronica 
        Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.ACEPTACION_ELECTRONICA
        Reslut = ServicioRNDC.Consultar(Entidad)
        DetalleReporteManifiesto.Clear()
        For Each DetalleManifeisto In Reslut.Datos
            'If DetalleManifeisto.Mensaje = String.Empty Or IsNothing(DetalleManifeisto.Mensaje) Then
            DetalleReporteManifiesto.Add(New Manifiesto With {.Numero = DetalleManifeisto.Remesa.Numero, .CodigoEmpresa = DetalleManifeisto.CodigoEmpresa})
            'End If
        Next
        Entidad.DetalleReporteManifiesto = DetalleReporteManifiesto
        ServicioRNDC.ReportarRNDC(Entidad)
#End Region
    End Sub

    Private Sub ReportarDocumentos()
        Try
            Dim ServicioRNDC = New ReporteMinisterioController
            Dim Entidad As New ReporteMinisterio
            Dim Manifiesto As New Manifiesto
            Dim DetalleReporteRemesas As New List(Of Remesas)
            Dim DetalleReporteManifiesto As New List(Of Manifiesto)
            Dim Reslut As Respuesta(Of IEnumerable(Of ReporteMinisterio))
            If Me.CodigoEmpresa > 0 Then
                Entidad.ConsultaServicioAutomatico = 0
                Entidad.CodigoEmpresa = Me.CodigoEmpresa
                Guardar_Mensaje_Log("Proceso Empresa: " & Me.CodigoEmpresa)
            Else
                Entidad.ConsultaServicioAutomatico = 1
                Guardar_Mensaje_Log("Proceso MultiEmpresa")
            End If

#Region "Reporta Remesas"
            'reporta las remesas"
            Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.REMESA
            Reslut = ServicioRNDC.Consultar(Entidad)
            Guardar_Mensaje_Log("Consulta Remesas a Reportar: " & Reslut.Datos.Count)
            DetalleReporteRemesas.Clear()
            For Each DetalleRemesa In Reslut.Datos
                If DetalleRemesa.Mensaje = String.Empty Or IsNothing(DetalleRemesa.Mensaje) Then
                    DetalleRemesa.Remesa.CodigoEmpresa = DetalleRemesa.CodigoEmpresa
                    DetalleReporteRemesas.Add(DetalleRemesa.Remesa)
                End If
            Next
            Entidad.DetalleReporteRemesas = DetalleReporteRemesas
            Guardar_Mensaje_Log("Remesas a Reportar Sin Errores: " & DetalleReporteRemesas.Count)
            Guardar_Mensaje_Log("Inicio Reporte Remesas")
            ServicioRNDC.ReportarRNDC(Entidad)
            Guardar_Mensaje_Log("Fin Reporte Remesas")
#End Region

#Region "Reporta Manifiestos"
            ' reporta los Manifiestos
            Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.MANIFIESTO
            Reslut = ServicioRNDC.Consultar(Entidad)
            Guardar_Mensaje_Log("Consulta Manifiestos a Reportar: " & Reslut.Datos.Count)
            DetalleReporteManifiesto.Clear()
            For Each DetalleManifeisto In Reslut.Datos
                If DetalleManifeisto.Mensaje = String.Empty Or IsNothing(DetalleManifeisto.Mensaje) Then
                    DetalleReporteManifiesto.Add(New Manifiesto With {.Numero = DetalleManifeisto.Remesa.Numero, .CodigoEmpresa = DetalleManifeisto.CodigoEmpresa})
                End If
            Next
            Guardar_Mensaje_Log("Manifiestos a Reportar Sin Errores: " & DetalleReporteManifiesto.Count)
            Entidad.DetalleReporteManifiesto = DetalleReporteManifiesto
            Guardar_Mensaje_Log("Inicio Reporte Manifiesto")
            ServicioRNDC.ReportarRNDC(Entidad)
            Guardar_Mensaje_Log("Fin Reporte Manifiesto")
#End Region

#Region "Reporta Cumplido Inicial Remesas"

            'reporta el cumplido Inicial Remesas
            Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.REMESA_CUMPLIDO_INICIAL
            Reslut = ServicioRNDC.Consultar(Entidad)
            Guardar_Mensaje_Log("Consulta Cumplido Inicial Remesas a Reportar: " & Reslut.Datos.Count)
            DetalleReporteRemesas.Clear()
            For Each DetalleRemesa In Reslut.Datos
                If DetalleRemesa.Mensaje = String.Empty Or IsNothing(DetalleRemesa.Mensaje) Then
                    DetalleRemesa.Remesa.CodigoEmpresa = DetalleRemesa.CodigoEmpresa
                    DetalleReporteRemesas.Add(DetalleRemesa.Remesa)
                End If
            Next
            Guardar_Mensaje_Log("Cumplido Inicial Remesas a Reportar Sin Errores: " & DetalleReporteRemesas.Count)
            Entidad.DetalleReporteRemesas = DetalleReporteRemesas
            Guardar_Mensaje_Log("Inicio Reporte Cumplido Inicial Remesas")
            ServicioRNDC.ReportarRNDC(Entidad)
            Guardar_Mensaje_Log("Fin Reporte Cumplido Inicial Remesas")
#End Region

#Region "Reporta Cumplido Remesas"

            'reporta el cumplido Remesas
            Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.REMESA_CUMPLIDA
            Reslut = ServicioRNDC.Consultar(Entidad)
            Guardar_Mensaje_Log("Consulta Cumplido Remesas a Reportar: " & Reslut.Datos.Count)
            DetalleReporteRemesas.Clear()
            For Each DetalleRemesa In Reslut.Datos
                If DetalleRemesa.Mensaje = String.Empty Or IsNothing(DetalleRemesa.Mensaje) Then
                    DetalleRemesa.Remesa.CodigoEmpresa = DetalleRemesa.CodigoEmpresa
                    DetalleReporteRemesas.Add(DetalleRemesa.Remesa)
                End If
            Next
            Guardar_Mensaje_Log("Cumplido Remesas a Reportar Sin Errores: " & DetalleReporteRemesas.Count)
            Entidad.DetalleReporteRemesas = DetalleReporteRemesas
            Guardar_Mensaje_Log("Inicio Reporte Cumplido Remesas")
            ServicioRNDC.ReportarRNDC(Entidad)
            Guardar_Mensaje_Log("Fin Reporte Cumplido Remesas")
#End Region

#Region "Reporta Cumplido Manifiestos"

            ' reporta el cumpldio de los Manifiestos
            Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.MANIFIESTO_CUMPLIDO
            Reslut = ServicioRNDC.Consultar(Entidad)
            Guardar_Mensaje_Log("Consulta Cumplido Manifiestos a Reportar: " & Reslut.Datos.Count)
            DetalleReporteManifiesto.Clear()
            For Each DetalleManifeisto In Reslut.Datos
                If DetalleManifeisto.Mensaje = String.Empty Or IsNothing(DetalleManifeisto.Mensaje) Then
                    DetalleReporteManifiesto.Add(New Manifiesto With {.Numero = DetalleManifeisto.Remesa.Numero, .CodigoEmpresa = DetalleManifeisto.CodigoEmpresa})
                End If
            Next
            Guardar_Mensaje_Log("Cumplido Manifiestos a Reportar Sin Errores: " & DetalleReporteManifiesto.Count)
            Entidad.DetalleReporteManifiesto = DetalleReporteManifiesto
            Guardar_Mensaje_Log("Inicio Reporte Cumplido Manifiestos")
            ServicioRNDC.ReportarRNDC(Entidad)
            Guardar_Mensaje_Log("Fin Reporte Cumplido Manifiestos")
#End Region

#Region "Reporta Cumplido Manifiestos Anulados"

            ' reporta el cumpldio anulado de los Manifiestos
            Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.MANIFIESTO_CUMPLIDO_ANULADO
            Reslut = ServicioRNDC.Consultar(Entidad)
            Guardar_Mensaje_Log("Consulta Cumplido Anulado Manifiestos a Reportar: " & Reslut.Datos.Count)
            DetalleReporteManifiesto.Clear()
            For Each DetalleManifeisto In Reslut.Datos
                If DetalleManifeisto.Mensaje = String.Empty Or IsNothing(DetalleManifeisto.Mensaje) Then
                    DetalleReporteManifiesto.Add(New Manifiesto With {.Numero = DetalleManifeisto.Remesa.Numero, .CodigoEmpresa = DetalleManifeisto.CodigoEmpresa})
                End If
            Next
            Guardar_Mensaje_Log("Cumplido Anulado Manifiestos a Reportar Sin Errores: " & DetalleReporteManifiesto.Count)
            Entidad.DetalleReporteManifiesto = DetalleReporteManifiesto
            Guardar_Mensaje_Log("Inicio Reporte Cumplido Anulado Manifiestos")
            ServicioRNDC.ReportarRNDC(Entidad)
            Guardar_Mensaje_Log("Fin Reporte Cumplido Anulado Manifiestos")
#End Region

#Region "Reporta Cumplido Remesas Anuladas"

            'reporta el cumplido anulado Remesas
            Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.REMESA_CUMLIDA_ANULADA
            Reslut = ServicioRNDC.Consultar(Entidad)
            Guardar_Mensaje_Log("Consulta Cumplido Anulado Remesas a Reportar: " & Reslut.Datos.Count)
            DetalleReporteRemesas.Clear()
            For Each DetalleRemesa In Reslut.Datos
                If DetalleRemesa.Mensaje = String.Empty Or IsNothing(DetalleRemesa.Mensaje) Then
                    DetalleRemesa.Remesa.CodigoEmpresa = DetalleRemesa.CodigoEmpresa
                    DetalleReporteRemesas.Add(DetalleRemesa.Remesa)
                End If
            Next
            Guardar_Mensaje_Log("Cumplido Anulado Remesas a Reportar Sin Errores: " & DetalleReporteRemesas.Count)
            Entidad.DetalleReporteRemesas = DetalleReporteRemesas
            Guardar_Mensaje_Log("Inicio Reporte Cumplido Anulado Remesas")
            ServicioRNDC.ReportarRNDC(Entidad)
            Guardar_Mensaje_Log("Fin Reporte Cumplido Anulado Remesas")
#End Region

#Region "Reporta Manifiestos Anulados"
            ' reporta el anulado de los Manifiestos
            Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.MANIFIESTO_ANULADO
            Reslut = ServicioRNDC.Consultar(Entidad)
            Guardar_Mensaje_Log("Consulta Anulado Manifiesto a Reportar: " & Reslut.Datos.Count)
            DetalleReporteManifiesto.Clear()
            For Each DetalleManifeisto In Reslut.Datos
                If DetalleManifeisto.Mensaje = String.Empty Or IsNothing(DetalleManifeisto.Mensaje) Then
                    DetalleReporteManifiesto.Add(New Manifiesto With {.Numero = DetalleManifeisto.Remesa.Numero, .CodigoEmpresa = DetalleManifeisto.CodigoEmpresa})
                End If
            Next
            Guardar_Mensaje_Log("Anulado Manifiestos a Reportar Sin Errores: " & DetalleReporteManifiesto.Count)
            Entidad.DetalleReporteManifiesto = DetalleReporteManifiesto
            Guardar_Mensaje_Log("Inicio Reporte Anulado Manifiestos")
            ServicioRNDC.ReportarRNDC(Entidad)
            Guardar_Mensaje_Log("Fin Reporte Anulado Manifiestos")
#End Region

#Region "Reporta Remesas Anuladas"
            'reporta el anulado Remesas
            Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.REMESA_ANULADA
            Reslut = ServicioRNDC.Consultar(Entidad)
            Guardar_Mensaje_Log("Consulta Anulado Remesas a Reportar: " & Reslut.Datos.Count)
            DetalleReporteRemesas.Clear()
            For Each DetalleRemesa In Reslut.Datos
                If DetalleRemesa.Mensaje = String.Empty Or IsNothing(DetalleRemesa.Mensaje) Then
                    DetalleRemesa.Remesa.CodigoEmpresa = DetalleRemesa.CodigoEmpresa
                    DetalleReporteRemesas.Add(DetalleRemesa.Remesa)
                End If
            Next
            Guardar_Mensaje_Log("Anulado Remesas a Reportar Sin Errores: " & DetalleReporteRemesas.Count)
            Entidad.DetalleReporteRemesas = DetalleReporteRemesas
            Guardar_Mensaje_Log("Inicio Reporte Anulado Remesas")
            ServicioRNDC.ReportarRNDC(Entidad)
            Guardar_Mensaje_Log("Fin Reporte Anulado Remesas")
#End Region

#Region "Consulta Aceptacion Electronica"
            ''Consulta Aceptacion Electronica 
            'Entidad.OpcionReporte = OPCIONES_REPORTE_RNDC.ACEPTACION_ELECTRONICA
            'Reslut = ServicioRNDC.Consultar(Entidad)
            'Guardar_Mensaje_Log("Consulta Aceptacion Electronica a Reportar: " & Reslut.Datos.Count)
            'DetalleReporteManifiesto.Clear()
            'For Each DetalleManifeisto In Reslut.Datos
            '    If DetalleManifeisto.Mensaje = String.Empty Or IsNothing(DetalleManifeisto.Mensaje) Then
            '        DetalleReporteManifiesto.Add(New Manifiesto With {.Numero = DetalleManifeisto.Remesa.Numero, .CodigoEmpresa = DetalleManifeisto.CodigoEmpresa})
            '    End If
            'Next
            'Guardar_Mensaje_Log("Aceptacion Electronica a Reportar Sin Errores: " & DetalleReporteManifiesto.Count)
            'Entidad.DetalleReporteManifiesto = DetalleReporteManifiesto
            'Guardar_Mensaje_Log("Inicio Reporte Aceptacion Electronica")
            'ServicioRNDC.ReportarRNDC(Entidad)
            'Guardar_Mensaje_Log("Fin Reporte Aceptacion Electronica")
#End Region

#Region "Cumplido Remesa Paqueteria"
            ' La Remesa Tipo Paqueteria del Manifiesto la toma el proceso Cumplido Remesas
#End Region



#End Region

        Catch ex As Exception
            Guardar_Mensaje_Log("Error ReportarDocumentos: " & ex.Message)
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Sub



#Region "Archivos Planos"
    Public Function Guardar_Mensaje_Log(ByVal strMensaje As String)
        Try

            Dim strContenido As String, strFechaHora As String = Date.Now.ToString
            Dim strNombreArchivoLog As String = Me.RutaLog & Me.NombreLog

            If Not File.Exists(strNombreArchivoLog) Then
                File.Create(strNombreArchivoLog).Dispose()
                strContenido = String.Empty
            Else
                strContenido = File.ReadAllText(strNombreArchivoLog)
            End If
            Me.stmStreamW = File.OpenWrite(strNombreArchivoLog)
            Me.stwStreamWriter = New StreamWriter(Me.stmStreamW, System.Text.Encoding.Default)
            Me.stwStreamWriter.Flush()
            Me.stwStreamWriter.Write(strContenido & vbNewLine & strFechaHora & " : " & strMensaje)
            Me.stwStreamWriter.Close()
            Me.stmStreamW.Close()

            Return True
        Catch ex As Exception
            '' FALTA
            ' Escribir en log eventos windows
            Return False
            Exit Try
        End Try
    End Function

#End Region

End Class

