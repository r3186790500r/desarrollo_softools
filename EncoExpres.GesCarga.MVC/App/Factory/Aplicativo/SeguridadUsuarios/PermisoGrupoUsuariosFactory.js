﻿EncoExpresApp.factory('PermisoGrupoUsuariosFactory', ['$http', function ($http) {

    var urlService = GetUrl('PermisoGrupoUsuarios');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarModulo = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarModulo';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.GenerarPlanitilla = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/GenerarPlanitilla';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.GuardarAsigancionPerfilesNotificaciones = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/GuardarAsigancionPerfilesNotificaciones';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };    
    factory.ConsultarAsigancionPerfilesNotificaciones = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarAsigancionPerfilesNotificaciones';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    return factory;
}]);