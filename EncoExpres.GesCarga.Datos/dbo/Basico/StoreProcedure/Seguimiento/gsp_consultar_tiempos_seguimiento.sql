﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 17/06/2019
	Descripción_Crea: 
	Modifica: Geferson Latorre
	Fecha_Modifica: 27/06/2019
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


PRINT 'gsp_consultar_tiempos_seguimiento'
GO
DROP PROCEDURE gsp_consultar_tiempos_seguimiento
GO 
CREATE PROCEDURE gsp_consultar_tiempos_seguimiento 
(      
 @par_EMPR_Codigo NUMERIC,      
 @par_Numero_Documento NUMERIC      
)      
AS      
BEGIN      

DECLARE @Numero INT
SELECT @Numero = (SELECT Numero FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero_Documento = @par_Numero_Documento)

 SELECT TOP 1
  1 AS ConsultaTiempos, 
  (SELECT Fecha_Reporte FROM Detalle_Seguimiento_Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @Numero AND CATA_SRSV_Codigo = 8203) AS Fecha_Llegada_Cargue,     
  (SELECT Fecha_Reporte FROM Detalle_Seguimiento_Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @Numero AND CATA_SRSV_Codigo = 8204) AS Fecha_Inicio_Cargue,      
  (SELECT Fecha_Reporte FROM Detalle_Seguimiento_Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @Numero AND CATA_SRSV_Codigo = 8205) AS Fecha_Fin_Cargue, 
  (SELECT Fecha_Reporte FROM Detalle_Seguimiento_Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @Numero AND CATA_SRSV_Codigo = 8207) AS Fecha_Llegada_Descargue,      
  (SELECT Fecha_Reporte FROM Detalle_Seguimiento_Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @Numero AND CATA_SRSV_Codigo = 8208) AS Fecha_Inicio_Descargue,      
  (SELECT Fecha_Reporte FROM Detalle_Seguimiento_Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @Numero AND CATA_SRSV_Codigo = 8209) AS Fecha_Fin_Descargue     
    
  FROM
  Detalle_Seguimiento_Vehiculos
  WHERE
  EMPR_Codigo = @par_EMPR_Codigo 
  AND ENPD_Numero = @Numero
      
END      
GO