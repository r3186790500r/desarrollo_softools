﻿DROP PROCEDURE gsp_insertar_detalle_planilla_despachos                   
GO
CREATE PROCEDURE gsp_insertar_detalle_planilla_despachos                   
(                                      
 @par_EMPR_Codigo smallint                                      
 ,@par_ENPD_Numero numeric(18,0)                                      
 ,@par_ENRE_Numero numeric(18,0)                                      
 ,@par_Numero_Documento_Remesa numeric(18,0)                                      
 ,@par_USUA_Codigo_Crea numeric                                      
 ,@par_Actualiza_Despacho numeric = null                                      
)                                      
AS                                      
BEGIN                            
IF (SELECT ENPD_Numero FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENRE_Numero) > 0  
BEGIN  
 SELECT 0 AS Numero  
END  
ELSE  
BEGIN  
INSERT INTO Detalle_Planilla_Despachos                                      
 (EMPR_Codigo                                      
 ,ENPD_Numero                                      
 ,ENRE_Numero                                      
 ,Numero_Documento_Remesa                
 ,TIDO_Codigo)                                      
VALUES                                      
 (@par_EMPR_Codigo                                       
 ,@par_ENPD_Numero                                       
 ,@par_ENRE_Numero                                       
 ,@par_Numero_Documento_Remesa                
 ,(SELECT TIDO_Codigo FROM Encabezado_Planilla_Despachos   WHERE Numero = @par_ENPD_Numero ))        
 --Inserta el seguimiento de la planilla despachos      
 IF (SELECT TIDO_Codigo FROM Encabezado_Planilla_Despachos   WHERE Numero = @par_ENPD_Numero ) = 150  --planilla despachos                        
 BEGIN                     
  INSERT INTO Detalle_Seguimiento_Remesas                   
  (                  
  EMPR_Codigo,                  
  ENPD_Numero,                  
  ENRE_Numero,                  
  Fecha_Reporte,                  
  CATA_TOSV_Codigo,                  
  Ubicacion,                  
  Longitud,                  
  Latitud,                  
  PUCO_Codigo,                  
  CATA_SRSV_Codigo,                  
  CATA_NOSV_Codigo,                  
  Observaciones,                  
  Reportar_Cliente,                  
  Envio_Reporte_Cliente,                  
  Fecha_Reporte_Cliente,                  
  USUA_Codigo_Crea,                  
  Fecha_Crea,                  
  Anulado,                  
  USUA_Codigo_Anula,                  
  Fecha_Anula,                  
  Causa_Anula,                  
  Orden                  
  )                  
  VALUES                  
  (                  
  @par_EMPR_Codigo,                  
  @par_ENPD_Numero,                  
  @par_ENRE_Numero,                  
  GETDATE(),                    
  8001,                  
  '',                                
  0,                                
  0,                   
  0,                                
  8202,                                
  8100,                  
  'REMESA',                  
  0,                  
  0,                  
  GETDATE( ),                  
  @par_USUA_Codigo_Crea,                  
  GETDATE(),                    
  0,                  
  NULL,                  
  NULL,                  
  NULL,                  
  1                         
  )       
  --Actualiza el cliente y el producto con base en la remesa a los seguimientos de la planilla      
  UPDATE Detalle_Seguimiento_Vehiculos SET TERC_Cliente =       
   (SELECT TERC_Codigo_Cliente FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENRE_Numero   ),        
  PRTR_Codigo =       
   (SELECT PRTR_Codigo FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENRE_Numero)           
  WHERE EMPR_Codigo = @par_EMPR_Codigo          
  AND ENPD_Numero = @par_ENPD_Numero          
 END                    
 IF (SELECT TIDO_Codigo FROM Encabezado_Planilla_Despachos   WHERE Numero = @par_ENPD_Numero  ) IN (130,150) --planilla despachos                   
 BEGIN                        
  UPDATE Encabezado_Remesas SET ENPD_Numero = @par_ENPD_Numero WHERE Numero = @par_ENRE_Numero AND EMPR_Codigo = @par_EMPR_Codigo                                
  update Detalle_Novedades_Despacho set ENPD_Numero = @par_ENPD_Numero where ENRE_Numero = @par_ENRE_Numero and EMPR_Codigo = @par_EMPR_Codigo            
  update Detalle_Seguimiento_Remesas set ENPD_Numero = @par_ENPD_Numero where ENRE_Numero = @par_ENRE_Numero and EMPR_Codigo = @par_EMPR_Codigo            
  update Remesas_Paqueteria                                 
  set CATA_ESRP_Codigo = 6010                   
  where                                 
  ENRE_Numero = @par_ENRE_Numero  AND EMPR_Codigo = @par_EMPR_Codigo                                    
    
  if @par_Actualiza_Despacho > 0 begin  
   update Detalle_Despacho_Orden_Servicios set                                 
   ENPD_Numero = @par_ENPD_Numero,                                
   ENPD_Numero_Documento = (SELECT Numero_Documento FROM Encabezado_Planilla_Despachos WHERE Numero = @par_ENPD_Numero AND EMPR_Codigo = @par_EMPR_Codigo) ,                                
   VEHI_Codigo = ISNULL(( select VEHI_Codigo from Encabezado_Planilla_Despachos where Numero = @par_ENPD_Numero AND EMPR_Codigo = @par_EMPR_Codigo),0)                                 
   where ENRE_Numero = @par_ENRE_Numero  AND EMPR_Codigo = @par_EMPR_Codigo  AND ENRE_Numero <> 0 AND ENRE_Numero IS NOT NULL                              
  end      
  update Encabezado_Orden_Cargues set                                 
  ENPD_Numero = @par_ENPD_Numero,                                
  VEHI_Codigo = ISNULL(( select VEHI_Codigo from Encabezado_Planilla_Despachos where Numero = @par_ENPD_Numero AND EMPR_Codigo = @par_EMPR_Codigo),0)                                 
  where ESOS_Numero = ( SELECT ESOS_Numero FROM Detalle_Despacho_Orden_Servicios  WHERE  ENRE_Numero = @par_ENRE_Numero AND EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero <> 0 AND ENRE_Numero IS NOT NULL  )     
  AND Numero = ( SELECT ENOC_Numero FROM Detalle_Despacho_Orden_Servicios  WHERE  ENRE_Numero = @par_ENRE_Numero AND EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero <> 0 AND ENRE_Numero IS NOT NULL  )     
  AND EMPR_Codigo = @par_EMPR_Codigo                            
                            
  IF NOT (                                
  (SELECT COUNT(*) FROM Detalle_Despacho_Orden_Servicios                                 
  WHERE ESOS_Numero = (SELECT ESOS_Numero FROM Detalle_Despacho_Orden_Servicios                                 
  WHERE  ENRE_Numero = @par_ENRE_Numero AND EMPR_Codigo = @par_EMPR_Codigo )                                
  AND EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = 0) > 0                                
  )                                
  BEGIN                                
  UPDATE Encabezado_Solicitud_Orden_Servicios SET CATA_ESSO_Codigo = 9202 WHERE Numero = (SELECT ESOS_Numero FROM Detalle_Despacho_Orden_Servicios WHERE  ENRE_Numero = @par_ENRE_Numero AND EMPR_Codigo = @par_EMPR_Codigo )AND EMPR_Codigo = @par_EMPR_Codig
o  
    
        
 END                                
 declare @par_Numero numeric                                     
 IF @par_ENPD_Numero > 0                              
 BEGIN                              
  update Detalle_Seguimiento_Vehiculos set ENPD_Numero = @par_ENPD_Numero WHERE ENOC_Numero in (    
  select  ENOC_Numero  from Detalle_Despacho_Orden_Servicios where ENRE_Numero = @par_ENRE_Numero AND EMPR_Codigo = @par_EMPR_Codigo        
  ) AND EMPR_Codigo = @par_EMPR_Codigo                              
 END                              
                          
 EXEC gsp_actualizar_totales_fletes_cliente @par_EMPR_Codigo,@par_ENPD_Numero                          
 END                             
 IF (SELECT TIDO_Codigo FROM Encabezado_Planilla_Despachos   WHERE Numero = @par_ENPD_Numero  ) = 210 -- Planilla entrega                        
 BEGIN                        
  UPDATE Encabezado_Remesas SET ENPE_Numero = @par_ENPD_Numero WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENRE_Numero AND TIDO_Codigo = 110                              
  UPDATE Remesas_Paqueteria SET CATA_ESRP_Codigo = 6025 WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero                           
 END                            
 IF (SELECT TIDO_Codigo FROM Encabezado_Planilla_Despachos   WHERE Numero = @par_ENPD_Numero  ) = 205 -- Planilla recoleccion                        
 BEGIN              
  UPDATE Encabezado_Remesas SET ENPR_Numero = @par_ENPD_Numero WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENRE_Numero AND TIDO_Codigo = 110                              
  UPDATE Remesas_Paqueteria SET CATA_ESRP_Codigo = 6001 WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero                           
 END      
UPDATE Encabezado_Remesas SET                                
VEHI_Codigo = ISNULL(( select VEHI_Codigo from Encabezado_Planilla_Despachos where Numero = @par_ENPD_Numero AND EMPR_Codigo = @par_EMPR_Codigo),0),      
TERC_Codigo_Conductor = ISNULL(( select TERC_Codigo_Conductor from Encabezado_Planilla_Despachos where Numero = @par_ENPD_Numero AND EMPR_Codigo = @par_EMPR_Codigo),0)                                 
WHERE                                 
Numero = @par_ENRE_Numero AND EMPR_Codigo = @par_EMPR_Codigo         
      
SELECT @par_ENPD_Numero AS Numero                                      
END      
END      
GO