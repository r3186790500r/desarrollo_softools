USE [ENCOEXPRES]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_encabezado_documento_comprobantes]
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo_Alterno VARCHAR (20),
@par_TIDO_Codigo NUMERIC,
@par_Fecha DATETIME,
@par_TERC_Codigo_Titular NUMERIC,
@par_TERC_Codigo_Beneficiario NUMERIC,
@par_Observaciones VARCHAR (1000) = NULL,
@par_CATA_DOOR_Codigo NUMERIC,
@par_CATA_FPDC_Codigo NUMERIC,
@par_CATA_DEIN_Codigo NUMERIC,
@par_Documento_Origen NUMERIC = NULL,
@par_CUBA_Codigo SMALLINT = NULL,
@par_CAJA_Codigo SMALLINT = NULL,
@par_Numero_Pago_Recaudo NUMERIC = NULL,
@par_Fecha_Pago_Recaudo DATETIME,
@par_Valor_Pago_Recaudo_Transferencia MONEY,
@par_Valor_Pago_Recaudo_Efectivo MONEY,
@par_Valor_Pago_Recaudo_Cheque MONEY,
@par_Valor_Pago_Recaudo_Total MONEY,
@par_Numeracion VARCHAR (20) = NULL,
@par_OFIC_Codigo_Creacion SMALLINT,
@par_OFIC_Codigo_Destino SMALLINT,
@par_Genero_Consignacion SMALLINT,
@par_Valor_Alterno MONEY,
@par_VEHI_Codigo NUMERIC = NULL,
@par_Estado SMALLINT,
@par_USUA_Codigo_Crea SMALLINT,
@par_ECCO_Codigo NUMERIC = NULL ,
@par_Centro_Costos NUMERIC= NULL ,
@par_Valor_Anticipo_Planilla numeric(18,0) = null
)
AS
BEGIN
DECLARE @ProcesoAnticipoComprobante SMALLINT = 0

SET @ProcesoAnticipoComprobante = (SELECT Estado FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = 56) --Sugerir Anticipo Comprobante Egreso


DECLARE @intNumero NUMERIC

EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, 0, @intNumero OUTPUT

INSERT INTO Encabezado_Documento_Comprobantes (
EMPR_Codigo,
Numero,
Codigo_Alterno,
TIDO_Codigo,
Fecha,
TERC_Codigo_Titular,
TERC_Codigo_Beneficiario,
Observaciones,
CATA_DOOR_Codigo,
CATA_FPDC_Codigo,
CATA_DEIN_Codigo,
Documento_Origen,
CUBA_Codigo,
CAJA_Codigo,
Numero_Pago_Recaudo,
Fecha_Pago_Recaudo,
Valor_Pago_Recaudo_Transferencia,
Valor_Pago_Recaudo_Efectivo,
Valor_Pago_Recaudo_Cheque,
Valor_Pago_Recaudo_Total,
Numeracion,
OFIC_Codigo_Creacion,
OFIC_Codigo_Destino,
Genero_Consignacion,
Valor_Alterno,
Anulado,
VEHI_Codigo,
Estado,
USUA_Codigo_Crea,
Fecha_Crea,
ECCO_Codigo ,
CATA_CCCE_Codigo ,
BANC_Codigo_Transferencia,
Numero_Cuenta_Transferencia,
CATA_TICB_Codigo_Transferencia
)
VALUES (
@par_EMPR_Codigo,
@intNumero,
@par_Codigo_Alterno,
@par_TIDO_Codigo,
@par_Fecha,
@par_TERC_Codigo_Titular,
@par_TERC_Codigo_Beneficiario,
ISNULL(@par_Observaciones,''),
@par_CATA_DOOR_Codigo,
@par_CATA_FPDC_Codigo,
@par_CATA_DEIN_Codigo,
ISNULL(@par_Documento_Origen,0),
ISNULL(@par_CUBA_Codigo,0),
--ISNULL(@par_CAJA_Codigo,0),
@par_CAJA_Codigo,
ISNULL(@par_Numero_Pago_Recaudo,0),
@par_Fecha_Pago_Recaudo,
@par_Valor_Pago_Recaudo_Transferencia,
@par_Valor_Pago_Recaudo_Efectivo,
@par_Valor_Pago_Recaudo_Cheque,
@par_Valor_Pago_Recaudo_Total,
ISNULL(@par_Numeracion,''),
@par_OFIC_Codigo_Creacion,
@par_OFIC_Codigo_Destino,
@par_Genero_Consignacion,
@par_Valor_Alterno,
0,
ISNULL(@par_VEHI_Codigo,0),
@par_Estado,
@par_USUA_Codigo_Crea,
GETDATE(),
ISNULL(@par_ECCO_Codigo,0) ,
@par_Centro_Costos,
IIF(@par_CATA_FPDC_Codigo = 5102,CONVERT(smallint, (SELECT BANC_Codigo From Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Titular)),0 ), --5101: Transferencia
IIF(@par_CATA_FPDC_Codigo = 5102,(SELECT CONVERT(VARCHAR(30),Numero_Cuenta_Bancaria) From Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Titular),'0' ),
IIF(@par_CATA_FPDC_Codigo = 5102,(SELECT CATA_TICB_Codigo From Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_TERC_Codigo_Titular),0 )
)

IF (@par_Estado = 1 AND @ProcesoAnticipoComprobante = 1)
BEGIN
IF (@par_CATA_DOOR_Codigo = 2604 OR @par_CATA_DOOR_Codigo = 2605) --(Anticipos o Sobreanticipos Planilla Masivo)
BEGIN
UPDATE Encabezado_Planilla_Despachos SET Valor_Anticipo = Valor_Anticipo + @par_Valor_Pago_Recaudo_Total WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero_Documento = @par_Documento_Origen AND TIDO_Codigo = 150 --Planilla Masivo
END
ELSE IF (@par_CATA_DOOR_Codigo = 2613 OR @par_CATA_DOOR_Codigo = 2617) --(Anticipos o Sobreanticipos Planilla Paquetería)
BEGIN
UPDATE Encabezado_Planilla_Despachos SET Valor_Anticipo = Valor_Anticipo + @par_Valor_Pago_Recaudo_Total WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero_Documento = @par_Documento_Origen AND TIDO_Codigo = 135 --Planilla Paquetería
END
END

SELECT SCOPE_IDENTITY() AS Codigo, @intNumero AS Numero

END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_modificar_encabezado_documento_comprobantes]
(
@par_EMPR_Codigo SMALLINT,
@par_Numero NUMERIC ,
@par_Codigo NUMERIC,
@par_Codigo_Alterno VARCHAR (20),
@par_TIDO_Codigo NUMERIC,
@par_Fecha DATETIME,
@par_TERC_Codigo_Titular NUMERIC,
@par_TERC_Codigo_Beneficiario NUMERIC,
@par_Observaciones VARCHAR (1000) = NULL,
@par_CATA_DOOR_Codigo NUMERIC,
@par_CATA_FPDC_Codigo NUMERIC,
@par_CATA_DEIN_Codigo NUMERIC,
@par_Documento_Origen NUMERIC = NULL,
@par_CUBA_Codigo SMALLINT = NULL,
@par_CAJA_Codigo SMALLINT = NULL,
@par_Numero_Pago_Recaudo NUMERIC = NULL,
@par_Fecha_Pago_Recaudo DATETIME,
@par_Valor_Pago_Recaudo_Transferencia MONEY,
@par_Valor_Pago_Recaudo_Efectivo MONEY,
@par_Valor_Pago_Recaudo_Cheque MONEY,
@par_Valor_Pago_Recaudo_Total MONEY,
@par_Numeracion VARCHAR (20) = NULL,
@par_OFIC_Codigo_Creacion SMALLINT,
@par_OFIC_Codigo_Destino SMALLINT,
@par_Genero_Consignacion SMALLINT,
@par_Valor_Alterno MONEY,
@par_VEHI_Codigo NUMERIC = NULL,
@par_Estado SMALLINT,
@par_USUA_Codigo_Modifica SMALLINT,
@par_ECCO_Codigo NUMERIC = NULL,
@par_Centro_Costos NUMERIC = NULL ,
@par_Valor_Anticipo_Planilla numeric(18,0) = null
)
AS
BEGIN

DECLARE @ProcesoAnticipoComprobante SMALLINT = 0

SET @ProcesoAnticipoComprobante = (SELECT Estado FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = 56) --Sugerir Anticipo Comprobante Egreso


DELETE Detalle_Documento_Comprobantes where EMPR_Codigo = @par_EMPR_Codigo AND EDCO_Codigo = @par_Codigo

UPDATE Encabezado_Documento_Comprobantes SET
Codigo_Alterno = @par_Codigo_Alterno,
Fecha = @par_Fecha,
TERC_Codigo_Titular = @par_TERC_Codigo_Titular,
TERC_Codigo_Beneficiario = @par_TERC_Codigo_Beneficiario,
Observaciones = ISNULL(@par_Observaciones,''),
CATA_DOOR_Codigo = @par_CATA_DOOR_Codigo,
CATA_FPDC_Codigo = @par_CATA_FPDC_Codigo,
CATA_DEIN_Codigo = @par_CATA_DEIN_Codigo,
Documento_Origen = ISNULL(@par_Documento_Origen,0),
CUBA_Codigo = ISNULL(@par_CUBA_Codigo,0) ,
CAJA_Codigo = ISNULL(@par_CAJA_Codigo,0),
Numero_Pago_Recaudo =ISNULL(@par_Numero_Pago_Recaudo,0),
Fecha_Pago_Recaudo =@par_Fecha_Pago_Recaudo,
Valor_Pago_Recaudo_Transferencia =@par_Valor_Pago_Recaudo_Transferencia,
Valor_Pago_Recaudo_Efectivo =@par_Valor_Pago_Recaudo_Efectivo,
Valor_Pago_Recaudo_Cheque =@par_Valor_Pago_Recaudo_Cheque ,
Valor_Pago_Recaudo_Total = @par_Valor_Pago_Recaudo_Total,
Numeracion =ISNULL(@par_Numeracion ,0),
OFIC_Codigo_Creacion =@par_OFIC_Codigo_Creacion ,
OFIC_Codigo_Destino = @par_OFIC_Codigo_Destino,
Genero_Consignacion = @par_Genero_Consignacion,
Valor_Alterno = @par_Valor_Alterno,
VEHI_Codigo = @par_VEHI_Codigo,
Estado = @par_Estado,
USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,
Fecha_Modifica = GETDATE(),
ECCO_Codigo= ISNULL(@par_ECCO_Codigo,0),
CATA_CCCE_Codigo = @par_Centro_Costos

WHERE
EMPR_Codigo = @par_EMPR_Codigo
AND Codigo = @par_Codigo
AND Numero = @par_Numero
AND TIDO_Codigo = @par_TIDO_Codigo

IF (@par_Estado = 1 AND @ProcesoAnticipoComprobante = 1)
BEGIN
IF (@par_CATA_DOOR_Codigo = 2604 OR @par_CATA_DOOR_Codigo = 2605) --(Anticipos o Sobreanticipos Planilla Masivo)
BEGIN
UPDATE Encabezado_Planilla_Despachos SET Valor_Anticipo = Valor_Anticipo + @par_Valor_Pago_Recaudo_Total WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero_Documento = @par_Documento_Origen AND TIDO_Codigo = 150 --Planilla Masivo
END
ELSE IF (@par_CATA_DOOR_Codigo = 2613 OR @par_CATA_DOOR_Codigo = 2617) --(Anticipos o Sobreanticipos Planilla Paquetería)
BEGIN
UPDATE Encabezado_Planilla_Despachos SET Valor_Anticipo = Valor_Anticipo + @par_Valor_Pago_Recaudo_Total WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero_Documento = @par_Documento_Origen AND TIDO_Codigo = 135 --Planilla Paquetería
END
END

SELECT @par_Numero AS Numero

END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_anular_encabezado_documento_comprobantes]
(
@par_EMPR_Codigo SMALLINT ,
@par_Codigo NUMERIC,
@par_Numero NUMERIC,
@par_TIDO_Codigo NUMERIC,
@par_USUA_Anula SMALLINT,
@par_Causa_Anulacion VARCHAR(150),
@par_OFIC_Anula SMALLINT )
AS
BEGIN
IF (SELECT ELGC_Numero FROM Encabezado_Documento_Comprobantes WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_Codigo AND TIDO_Codigo = @par_TIDO_Codigo AND Numero = @par_Numero ) > 0
begin
SELECT 0 AS Numero
end
else
begin
------------restar valor de anticipo planilla-----------------------
DECLARE @ProcesoAnticipoComprobante SMALLINT = 0
SET @ProcesoAnticipoComprobante = (SELECT Estado FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = 56) --Sugerir Anticipo Comprobante Egreso

DECLARE @par_CATA_DOOR_Codigo NUMERIC(18,0) = 0
DECLARE @par_Valor_Pago_Recaudo_Total MONEY = 0
DECLARE @par_Documento_Origen NUMERIC(18,0) = null
DECLARE @par_Estado SMALLINT = 0

SELECT
@par_CATA_DOOR_Codigo = CATA_DOOR_Codigo,
@par_Valor_Pago_Recaudo_Total= Valor_Pago_Recaudo_Total,
@par_Documento_Origen = Documento_Origen,
@par_Estado = Estado
FROM Encabezado_Documento_Comprobantes
WHERE EMPR_Codigo = @par_EMPR_Codigo
AND Codigo = @par_Codigo
AND TIDO_Codigo = @par_TIDO_Codigo
AND Numero = @par_Numero

IF (@par_Estado = 1 AND @ProcesoAnticipoComprobante = 1)
BEGIN
IF (@par_CATA_DOOR_Codigo = 2604 OR @par_CATA_DOOR_Codigo = 2605) --(Anticipos o Sobreanticipos Planilla Masivo)
BEGIN
UPDATE Encabezado_Planilla_Despachos SET Valor_Anticipo = Valor_Anticipo - @par_Valor_Pago_Recaudo_Total WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero_Documento = @par_Documento_Origen AND TIDO_Codigo = 150 --Planilla Masivo
END
ELSE IF (@par_CATA_DOOR_Codigo = 2613 OR @par_CATA_DOOR_Codigo = 2617) --(Anticipos o Sobreanticipos Planilla Paquetería)
BEGIN
UPDATE Encabezado_Planilla_Despachos SET Valor_Anticipo = Valor_Anticipo - @par_Valor_Pago_Recaudo_Total WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero_Documento = @par_Documento_Origen AND TIDO_Codigo = 135 --Planilla Paquetería
END
END
------------restar valor de anticipo planilla-----------------------

UPDATE Encabezado_Documento_Comprobantes
SET
Anulado = 1,
USUA_Codigo_Anula = @par_USUA_Anula,
Fecha_Anula = GETDATE(),
OFIC_Codigo_Anula = @par_OFIC_Anula,
Causa_Anulacion = @par_Causa_Anulacion
WHERE
EMPR_Codigo = @par_EMPR_Codigo
AND Codigo = @par_Codigo
AND TIDO_Codigo = @par_TIDO_Codigo
AND Numero = @par_Numero
-- JCB: 10-NOV-2020 -- ESTE UPDATE SOLO CRUZA UNA CUENTA CON UN COMPROBANTE, POR LO QUE NO SIRVE PARA EGRESOS QUE CANCELAN VARIAS CXP
--UPDATE ENDC set ENDC.Abono = ENDC.Abono-EDCO.Valor_Pago_Recaudo_Total,
--ENDC.Saldo = ENDC.Saldo + EDCO.Valor_Pago_Recaudo_Total
--FROM Encabezado_Documento_Cuentas as ENDC
--INNER JOIN Encabezado_Documento_Comprobantes as EDCO
--ON ENDC.EMPR_Codigo = EDCO.EMPR_Codigo
--AND ENDC.Documento_Origen = EDCO.Documento_Origen
--AND ENDC.CATA_DOOR_Codigo = EDCO.CATA_DOOR_Codigo
--WHERE ENDC.EMPR_Codigo = @par_EMPR_Codigo
--AND EDCO.Codigo = @par_Codigo
--PARA ANULAR LAS CxP o CxC ASOCIADAS AL COMPROBANTE EGRESO O COMPROBANTE INGRESO, SE CRUZA Encabezado_Documento_Cuentas y Detalle_Documento_Cuenta_Comprobantes
UPDATE ENDC SET ENDC.Abono = ENDC.Abono - DDCC.Valor_Pago_Recaudo,
ENDC.Saldo = ENDC.Saldo + DDCC.Valor_Pago_Recaudo
FROM Encabezado_Documento_Cuentas as ENDC
INNER JOIN Detalle_Documento_Cuenta_Comprobantes as DDCC
ON ENDC.EMPR_Codigo = DDCC.EMPR_Codigo
AND ENDC.Codigo = DDCC.ENDC_Codigo
WHERE ENDC.EMPR_Codigo = @par_EMPR_Codigo
AND DDCC.EDCO_Codigo = @par_Codigo
-- GENERAR MOVIMIENTO CONTABLE ANULACION COMPROBANTE
EXEC gsp_anular_movimiento_contable @par_EMPR_Codigo,@par_TIDO_Codigo, @par_Codigo


SELECT @par_Numero AS Numero
END
END
GO

INSERT INTO [dbo].[Valor_Catalogos]
([EMPR_Codigo]
,[Codigo]
,[CATA_Codigo]
,[Campo1]
,[Campo2]
,[Campo3]
,[Campo4]
,[Campo5]
,[Estado]
,[USUA_Codigo_Crea]
,[Fecha_Crea]
,[USUA_Modifica]
,[Fecha_Modifica])
VALUES
(1
,2617
,26
,'Sobreanticipo Planilla Paquetería'
,'EGR'
,'0'
,''
,null
,1
,1
,CURRENT_TIMESTAMP
,null
,null)
GO

UPDATE [dbo].[Valor_Catalogos] SET [Campo1] = 'Sobreanticipo Planilla Masivo' where [Codigo] = 2605
GO
