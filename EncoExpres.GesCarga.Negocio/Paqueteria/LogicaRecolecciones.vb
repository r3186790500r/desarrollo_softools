﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Fachada.Despachos.Paqueteria
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria

Namespace Despachos.Paqueteria
    Public NotInheritable Class LogicaRecolecciones
        Inherits LogicaBase(Of Recolecciones)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Recolecciones)

        Sub New(persistencia As IPersistenciaBase(Of Recolecciones))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Recolecciones) As Respuesta(Of IEnumerable(Of Recolecciones))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Recolecciones))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Recolecciones))(consulta)
        End Function

        Public Function ConsultarENCOE(filtro As Recolecciones) As Respuesta(Of IEnumerable(Of Recolecciones))
            Dim consulta = CType(_persistencia, PersistenciaRecolecciones).ConsultarENCOE(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Recolecciones))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Recolecciones))(consulta)
        End Function

        Public Function ConsultarRecoleccionesPorPlanillar(filtro As Recolecciones) As Respuesta(Of IEnumerable(Of Recolecciones))
            Dim consulta = CType(_persistencia, PersistenciaRecolecciones).ConsultarRecoleccionesPorPlanillar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Recolecciones))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Recolecciones))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Recolecciones) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long = 0

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If Not IsNothing(entidad.parametrosCalculos) Then
                validarDatos = ValidarPesoVolumetrico(entidad)
                If validarDatos.ProcesoExitoso Then
                    entidad = validarDatos.Datos
                    validarDatos = CalcularValores(entidad)
                    If validarDatos.ProcesoExitoso Then
                        entidad = validarDatos.Datos
                    End If
                End If
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Recolecciones) As Respuesta(Of Recolecciones)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Recolecciones)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Recolecciones)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As Recolecciones) As Respuesta(Of Recolecciones)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Recolecciones) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Recolecciones) With {.ProcesoExitoso = True}

        End Function

        Public Function Anular(entidad As Recolecciones) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaRecolecciones).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function Cancelar(entidad As Recolecciones) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaRecolecciones).Cancelar(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function AsociarRecoleccionesPlanilla(entidad As Recolecciones) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaRecolecciones).AsociarRecoleccionesPlanilla(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function


        Public Function GuardarEntrega(entidad As Recolecciones) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Numero As Long = 0

            Numero = CType(_persistencia, PersistenciaRecolecciones).GuardarEntrega(entidad)

            Return New Respuesta(Of Long)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad, Recursos.OperacionModifico), .Datos = Numero}
        End Function

        Public Function ValidarPesoVolumetrico(entidad As Recolecciones) As Respuesta(Of Recolecciones)
            Dim strError As String = String.Empty

            If IsNothing(entidad) Then
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
                Return New Respuesta(Of Recolecciones)(entidad) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            entidad.PesoVolumetrico = ((entidad.Alto * entidad.Ancho * entidad.Largo) / 1000000) * 400
            entidad.PesoVolumetrico = Math.Round(entidad.PesoVolumetrico, 2)

            Return New Respuesta(Of Recolecciones)(entidad) With {.ProcesoExitoso = True}

        End Function

        Public Function CalcularValores(entidad As Recolecciones) As Respuesta(Of Recolecciones)
            Dim strError As String = String.Empty

            If IsNothing(entidad) Then
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
                Return New Respuesta(Of Recolecciones)(entidad) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            entidad.ValorFleteCliente = 0

            entidad.parametrosCalculos.calculo = False
            If entidad.CodigoTarifaTransporte = 302 Then 'TARIFA_PRODUCTO_PAQUETERIA
                For Each item In entidad.ListaTarifaCargaVenta
                    If entidad.CodigoTarifaTransporte = item.CodigoTarifa AndAlso item.Codigo = entidad.ProductoTransportado.Codigo Then
                        entidad.ValorFleteCliente = Math.Round(entidad.Cantidad * item.ValorFlete, 2)
                        entidad.detalleTarifa = item
                        If entidad.ProductoTransportado.Peso > 0 Then
                            entidad.Peso = entidad.ProductoTransportado.Peso * entidad.Cantidad
                        End If
                        entidad.parametrosCalculos.calculo = True
                        Exit For
                    End If
                Next
            End If
            If entidad.PesoCobrar > 0 Then
                If entidad.CodigoTarifaTransporte = 200 Then 'TARIFA_RANGO_PESO_VALOR_KILO
                    For Each item In entidad.ListaTarifaCargaVenta
                        If entidad.CodigoTarifaTransporte = item.CodigoTarifa AndAlso
                            entidad.PesoCobrar >= item.PesoMinimo AndAlso
                            entidad.PesoCobrar <= item.PesoMaximo Then
                            entidad.ValorFleteCliente = Math.Round(entidad.PesoCobrar * item.ValorFlete, 2)
                            entidad.detalleTarifa = item
                            entidad.parametrosCalculos.calculo = True
                            Exit For
                        End If
                    Next
                End If
                If entidad.CodigoTarifaTransporte = 201 Then 'TARIFA_RANGO_PESO_VALOR_FIJO
                    For Each item In entidad.ListaTarifaCargaVenta
                        If entidad.CodigoTarifaTransporte = item.CodigoTarifa AndAlso
                            entidad.PesoCobrar >= item.PesoMinimo AndAlso
                            entidad.PesoCobrar <= item.PesoMaximo Then
                            entidad.ValorFleteCliente = item.ValorFlete
                            entidad.detalleTarifa = item
                            entidad.parametrosCalculos.calculo = True
                            Exit For
                        End If
                    Next
                End If
            End If
            If entidad.parametrosCalculos.calculo = True Then
                entidad.TotalFleteCliente = Math.Round(entidad.ValorFleteCliente, 0)
                If entidad.detalleTarifa.CondicionesComerciales Then
                    Dim descuento As Double = 0.0
                    If entidad.detalleTarifa.Reexpedicion = 0 Then
                        If entidad.parametrosCalculos.PesoNormal = True Then
                            descuento = Math.Round(entidad.ValorFleteCliente * (entidad.detalleTarifa.PorcentajeFlete / 100), 0)
                        Else
                            descuento = Math.Round(entidad.ValorFleteCliente * (entidad.detalleTarifa.PorcentajeVolumen / 100), 0)
                        End If
                        entidad.ValorFleteCliente -= descuento
                        If entidad.ValorFleteCliente < entidad.detalleTarifa.FleteMinimo Then
                            entidad.ValorFleteCliente = entidad.detalleTarifa.FleteMinimo
                        End If
                    End If
                End If
                If entidad.ValorComercial > 0 Then
                    If entidad.detalleTarifa.PorcentajeSeguro > 0 Then
                        entidad.ValorSeguroCliente = Math.Round(entidad.ValorComercial * (entidad.detalleTarifa.PorcentajeSeguro / 100), 0)
                    End If
                    If entidad.parametrosCalculos.OficinaObjPorcentajeSeguroPaqueteria > 0 AndAlso entidad.detalleTarifa.PorcentajeSeguro = 0 Then
                        entidad.ValorSeguroCliente = Math.Round(entidad.ValorComercial * (entidad.parametrosCalculos.OficinaObjPorcentajeSeguroPaqueteria / 100), 0)
                    End If
                    If entidad.LineaNegocioPaqueteria.Codigo = 21601 Then 'CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA
                        If entidad.ValorSeguroCliente < entidad.parametrosCalculos.PaqueteriaMinimoValorManejo Then
                            entidad.ValorSeguroCliente = entidad.parametrosCalculos.PaqueteriaMinimoValorManejo
                        End If
                    ElseIf entidad.LineaNegocioPaqueteria.Codigo = 21602 Then 'CATALOGO_LINEA_NEGOCIO_PAQUETERIA.COURIER
                        If entidad.ValorSeguroCliente < entidad.parametrosCalculos.PaqueteriaMinimoValorSeguro Then
                            entidad.ValorSeguroCliente = entidad.parametrosCalculos.PaqueteriaMinimoValorSeguro
                        End If
                    End If

                    If entidad.detalleTarifa.CondicionesComerciales Then
                        Dim tmpValorSeguro As Double = Math.Round(entidad.ValorComercial * (entidad.detalleTarifa.PorcentajeValorDeclarado / 100), 0)
                        If tmpValorSeguro > entidad.detalleTarifa.ManejoMinimo Then
                            entidad.ValorSeguroCliente = tmpValorSeguro
                        Else
                            entidad.ValorSeguroCliente = entidad.detalleTarifa.ManejoMinimo
                        End If
                    End If
                Else
                    If entidad.LineaNegocioPaqueteria.Codigo = 21601 Then 'CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA
                        entidad.ValorSeguroCliente = entidad.parametrosCalculos.PaqueteriaMinimoValorManejo
                    ElseIf entidad.LineaNegocioPaqueteria.Codigo = 21602 Then 'CATALOGO_LINEA_NEGOCIO_PAQUETERIA.COURIER
                        entidad.ValorSeguroCliente = entidad.parametrosCalculos.PaqueteriaMinimoValorSeguro
                    End If
                End If
                entidad.TotalFleteCliente = entidad.ValorFleteCliente + entidad.ValorSeguroCliente
                Return New Respuesta(Of Recolecciones)(entidad) With {.ProcesoExitoso = True}

            End If

            Return New Respuesta(Of Recolecciones)(entidad) With {.ProcesoExitoso = False}

        End Function

    End Class

End Namespace