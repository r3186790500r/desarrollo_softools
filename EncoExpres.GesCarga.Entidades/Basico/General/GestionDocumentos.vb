﻿Imports Newtonsoft.Json

Namespace Basico.General
    ''' <summary>
    ''' Clase <see cref="EntidadGestionDocumentos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EntidadGestionDocumentos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EntidadGestionDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EntidadGestionDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property TipoDocumento As TipoDocumentos

    End Class
    ''' <summary>
    ''' Clase <see cref="GestionDocumentos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class GestionDocumentos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="GestionDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>s
        ''' Inicializa una nueva instancia de la clase <see cref="GestionDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


        End Sub

        <JsonProperty>
        Public Property Nombre As String

    End Class
    ''' <summary>
    ''' Clase <see cref="ConfiguracionGestionDocumentos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ConfiguracionGestionDocumentos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConfiguracionGestionDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>s
        ''' Inicializa una nueva instancia de la clase <see cref="ConfiguracionGestionDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            EntidadDocumento = New EntidadGestionDocumentos With {.Codigo = Read(lector, "EDGD_Codigo"), .Nombre = Read(lector, "Nombre_Entidad")}
            Documento = New GestionDocumentos With {.Codigo = Read(lector, "DOGD_Codigo"), .Nombre = Read(lector, "Nombre_Documento")}
            AplicaReferencia = Read(lector, "Aplica_Referencia")
            AplicaEmisor = Read(lector, "Aplica_Emisor")
            AplicaFechaEmision = Read(lector, "Aplica_Fecha_Emision")
            AplicaFechaVencimiento = Read(lector, "Aplica_Fecha_Vencimiento")
            AplicaArchivo = Read(lector, "Aplica_Archivo")
            AplicaEliminar = Read(lector, "Aplica_Eliminar")
            AplicaDescargar = Read(lector, "Aplica_Descargar")
            Size = Read(lector, "Tamano")
            NombreTamano = Read(lector, "Nombre_Tamano")
            Habilitado = Read(lector, "Habilitado")
            TipoArchivoDocumento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIAD_Codigo"), .Nombre = Read(lector, "Tipo_Archivo"), .CampoAuxiliar2 = Read(lector, "Formato_Aplicacion")}
            Estado = Read(lector, "Estado")
            Cantidad_repeticiones = Read(lector, "Cantidad_Repeticiones")

            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub
        <JsonProperty>
        Public Property EntidadDocumento As EntidadGestionDocumentos
        <JsonProperty>
        Public Property Documento As GestionDocumentos
        <JsonProperty>
        Public Property AplicaReferencia As Integer
        <JsonProperty>
        Public Property AplicaEmisor As Integer
        <JsonProperty>
        Public Property AplicaFechaEmision As Integer
        <JsonProperty>
        Public Property AplicaFechaVencimiento As Integer
        <JsonProperty>
        Public Property AplicaArchivo As Integer
        <JsonProperty>
        Public Property AplicaEliminar As Integer
        <JsonProperty>
        Public Property AplicaDescargar As Integer
        <JsonProperty>
        Public Property Size As Integer
        <JsonProperty>
        Public Property NombreTamano As String
        <JsonProperty>
        Public Property Habilitado As Integer
        <JsonProperty>
        Public Property Cantidad_repeticiones As Integer
        <JsonProperty>
        Public Property TipoArchivoDocumento As ValorCatalogos

    End Class
End Namespace

