﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Utilitarios
Imports System.Net.Mail
Imports System.Net
Namespace Utilitarios
    Public NotInheritable Class RepositorioConfiguracionServidorCorreos
        Inherits RepositorioBase(Of ConfiguracionServidorCorreos)
        Public Overrides Function Consultar(filtro As ConfiguracionServidorCorreos) As IEnumerable(Of ConfiguracionServidorCorreos)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Insertar(entidad As ConfiguracionServidorCorreos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ConfiguracionServidorCorreos) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ID", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Smtp", entidad.Smtp)
                conexion.AgregarParametroSQL("@par_RequiereAutenticacionSmtp", entidad.AutenticacionRequerida)
                conexion.AgregarParametroSQL("@par_SmtpPuerto", entidad.SmtpPuerto)
                conexion.AgregarParametroSQL("@par_UsuarioCorreo", entidad.Usuario)
                conexion.AgregarParametroSQL("@par_ContrasenaCorreo", entidad.Contraseña)
                conexion.AgregarParametroSQL("@par_AutenticacionSSL", entidad.AutenticacionSSL)
                conexion.AgregarParametroSQL("@par_Remitente", entidad.Remitente)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_configuracion_servidor_correo")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As ConfiguracionServidorCorreos) As ConfiguracionServidorCorreos
            Dim item As New ConfiguracionServidorCorreos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_configuracion_servidor_correo")

                While resultado.Read
                    item = New ConfiguracionServidorCorreos(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function EnviarCorreoPrueba(Entidad As ConfiguracionServidorCorreos) As String
            Try
                Dim Mail As New MailMessage
                Dim Conexion As New SmtpClient
                Dim Remitente As New MailAddress(Entidad.Usuario, Entidad.Remitente)
                Mail.From = Remitente
                Mail.To.Add(Entidad.Receptor)
                Mail.Subject = "Prueba Servidor de correo"
                Mail.Body = "Prueba servidor de correo"
                Conexion.EnableSsl = Entidad.AutenticacionSSL
                If Entidad.AutenticacionRequerida > 0 Then
                    Conexion.Credentials = New Net.NetworkCredential(Entidad.Usuario, Entidad.Contraseña)
                End If
                Conexion.Port = Entidad.SmtpPuerto
                Conexion.Host = Entidad.Smtp
                Conexion.Send(Mail)
                Return ""
            Catch ex As Exception
                Return ex.InnerException.ToString()
            End Try
        End Function


        Public Function EnviarCorreoConfirmacionClave(CorreoUsuario As String, CorreoManager As String, CodigoEmpresa As Integer)
            Dim correo As New MailMessage
            Dim envios As New SmtpClient
            Dim item As New ConfiguracionServidorCorreos


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)



                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_correo_servidor")

                While resultado.Read
                    item = New ConfiguracionServidorCorreos(resultado)
                End While

                correo.To.Clear()
                correo.Body = ""
                correo.Subject = ""
                correo.Body = "Su Clave se ha modificado con &Eacute;xito"
                correo.Subject = "Confirmación Cambio Contraseña GESTRANS.NET"
                correo.IsBodyHtml = True
                correo.To.Add(Trim(CorreoUsuario))
                If CorreoManager <> "" Then
                    correo.To.Add(Trim(CorreoManager))
                End If
                correo.From = New MailAddress(item.Usuario)
                envios.Credentials = New NetworkCredential(item.Usuario, item.Contraseña)

                'Datos importantes no modificables para tener acceso a las cuentas

                envios.Host = item.Smtp
                envios.Port = item.SmtpPuerto
                envios.EnableSsl = IIf(item.AutenticacionSSL = 1, True, False)

                envios.Send(correo)
            End Using

        End Function

        Public Function EnviarCorreoCodigoVerificacion(NombreUsuario As String, CodigoCambioClave As String, CorreoUsuario As String, CorreoManager As String, CodigoEmpresa As Integer)
            Dim correo As New MailMessage
            Dim envios As New SmtpClient
            Dim item As New ConfiguracionServidorCorreos


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)



                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_correo_servidor")

                While resultado.Read
                    item = New ConfiguracionServidorCorreos(resultado)
                End While

                correo.To.Clear()
                correo.Body = ""
                correo.Subject = ""
                correo.Body = "El c&oacute;digo de verificaci&oacute;n para el cambio de contrase&ntilde;a del usuario " & NombreUsuario & " es  <strong>" & CodigoCambioClave &
                            "</strong> Este C&oacute;digo tiene una validez de 5 minutos"
                correo.Subject = "Recuperar Contraseña GESTRANS.NET"
                correo.IsBodyHtml = True


                correo.To.Add(Trim(CorreoUsuario))
                If CorreoManager <> "" Then
                    correo.To.Add(Trim(CorreoManager))
                End If
                correo.From = New MailAddress(item.Usuario)
                envios.Credentials = New NetworkCredential(item.Usuario, item.Contraseña)

                'Datos importantes no modificables para tener acceso a las cuentas

                envios.Host = item.Smtp
                envios.Port = item.SmtpPuerto
                envios.EnableSsl = IIf(item.AutenticacionSSL = 1, True, False)

                envios.Send(correo)
            End Using



        End Function
    End Class
End Namespace
