﻿PRINT 'gsp_modificar_encabezado_concepto_contables'
GO  
DROP PROCEDURE gsp_modificar_encabezado_concepto_contables
GO
CREATE PROCEDURE gsp_modificar_encabezado_concepto_contables
(
@par_EMPR_Codigo SMALLINT, 
@par_Codigo NUMERIC,
@par_Codigo_Alterno VARCHAR(20),
@par_Nombre VARCHAR (50),
@par_TIDO_Codigo SMALLINT, 
@par_CATA_DOOR_Codigo NUMERIC,
@par_CATA_TICC_Codigo NUMERIC,
@par_CATA_TINA_Codigo NUMERIC,
@par_OFIC_Codigo_Aplica SMALLINT, 
@par_Observaciones  VARCHAR (250) = NULL,
@par_OFIC_Codigo SMALLINT, 
@par_Fuente VARCHAR (10) = NULL,
@par_Fuente_Anulacion VARCHAR (10) = NULL,
@par_Estado SMALLINT, 
@par_USUA_Codigo_Modifica SMALLINT
)
AS
BEGIN

DELETE Detalle_Concepto_Contables WHERE  EMPR_Codigo = @par_EMPR_Codigo AND ECCO_Codigo = @par_Codigo

	UPDATE Encabezado_Concepto_Contables
	SET 
	Codigo_Alterno = @par_Codigo_Alterno,
	Nombre = @par_Nombre,
	TIDO_Codigo = @par_TIDO_Codigo,
	CATA_DOOR_Codigo = @par_CATA_DOOR_Codigo,
	CATA_TICC_Codigo = @par_CATA_TICC_Codigo,
	CATA_TINA_Codigo = @par_CATA_TINA_Codigo,
	OFIC_Codigo_Aplica = @par_OFIC_Codigo_Aplica,
	Observaciones = ISNULL(@par_Observaciones,''),
	OFIC_Codigo = @par_OFIC_Codigo,
	Fuente = ISNULL(@par_Fuente ,''),
	Fuente_Anulacion = ISNULL(@par_Fuente_Anulacion,''),
	Estado = @par_Estado,
	USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,
	Fecha_Modifica = GETDATE()
	
	WHERE 
	EMPR_Codigo = @par_EMPR_Codigo
	AND Codigo = @par_Codigo

	SELECT @par_Codigo AS Codigo
END
GO