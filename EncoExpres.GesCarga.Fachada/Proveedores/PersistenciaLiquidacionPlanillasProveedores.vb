﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Proveedores
Imports EncoExpres.GesCarga.Repositorio.Proveedores

Namespace Proveedores

    Public NotInheritable Class PersistenciaLiquidacionPlanillasProveedores
        Implements IPersistenciaBase(Of LiquidacionPlanillasProveedores)

        Public Function Consultar(filtro As LiquidacionPlanillasProveedores) As IEnumerable(Of LiquidacionPlanillasProveedores) Implements IPersistenciaBase(Of LiquidacionPlanillasProveedores).Consultar
            Return New RepositorioLiquidacionPlanillasProveedores().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As LiquidacionPlanillasProveedores) As Long Implements IPersistenciaBase(Of LiquidacionPlanillasProveedores).Insertar
            Return New RepositorioLiquidacionPlanillasProveedores().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As LiquidacionPlanillasProveedores) As Long Implements IPersistenciaBase(Of LiquidacionPlanillasProveedores).Modificar
            Return New RepositorioLiquidacionPlanillasProveedores().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As LiquidacionPlanillasProveedores) As LiquidacionPlanillasProveedores Implements IPersistenciaBase(Of LiquidacionPlanillasProveedores).Obtener
            Return New RepositorioLiquidacionPlanillasProveedores().Obtener(filtro)
        End Function
        Public Function Anular(entidad As LiquidacionPlanillasProveedores) As Boolean
            Return New RepositorioLiquidacionPlanillasProveedores().Anular(entidad)
        End Function

    End Class
End Namespace