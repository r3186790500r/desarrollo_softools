﻿PRINT 'gsp_obtener_productos_ministerio_transporte'
GO
DROP PROCEDURE gsp_obtener_productos_ministerio_transporte
GO
CREATE PROCEDURE gsp_obtener_productos_ministerio_transporte  
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo SMALLINT = NULL,
@par_Nombre VARCHAR(100) = NULL
)  
AS  
BEGIN  
 SELECT  
  PMTR.EMPR_Codigo,  
  PMTR.Codigo,  
  ISNULL(PMTR.Codigo_Alterno,0) AS Codigo_Alterno,  
  ISNULL(PMTR.Nombre,'') AS Nombre,  
  ISNULL(PMTR.Descripcion,'') AS Descripcion,  
  ISNULL(PMTR.Estado,'') AS Estado
 FROM  
  Productos_Ministerio_Transporte AS PMTR

 WHERE  
  PMTR.EMPR_Codigo = @par_EMPR_Codigo
  AND PMTR.Codigo = ISNULL(@par_Codigo, PMTR.Codigo)  
  AND PMTR.Nombre = ISNULL(@par_Nombre, PMTR.Nombre) 
END  
GO