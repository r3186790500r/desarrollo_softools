﻿Imports System.Reflection
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos

Namespace Gesphone

    ''' <summary>
    ''' 
    ''' </summary>
    Public Class LogicaSincronizacionApp

        ReadOnly TipoDocumentoPlanillaRecolecciones As Integer = 135
        ReadOnly TipoDocumentoPlanillaEntregas As Integer = 210

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaSincronizacionApp

        Sub New(persistencia As IPersistenciaSincronizacionApp)
            _persistencia = persistencia
        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        Public Function GenerarEstructuraBaseOffLine() As DataBaseOffLine

            Dim res = GenerarEsquema()

            Return res
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function SincronizarTarifario(filtro As FiltroSyncApp) As Respuesta(Of SyncEncabezadoTarifarioVenta)

            If IsNothing(filtro) Then
                Return New Respuesta(Of SyncEncabezadoTarifarioVenta)() With {.MensajeOperacion = "Sin parametros de búsqueda", .ProcesoExitoso = False}
            End If

            If filtro.CodigoEmpresa <= 0 Then
                Return New Respuesta(Of SyncEncabezadoTarifarioVenta)() With {.MensajeOperacion = "Código de empresa invalido", .ProcesoExitoso = False}
            End If

            Dim encabezadoTarifario = _persistencia.ObtenerEncabezadoTarifario(filtro.CodigoEmpresa, 0)

            If IsNothing(encabezadoTarifario) Or encabezadoTarifario.Numero < 1 Then
                Return New Respuesta(Of SyncEncabezadoTarifarioVenta)() With {.MensajeOperacion = "No existe un tarifario base vigente o habilitado.", .ProcesoExitoso = False}
            End If

            filtro.NumeroTarifario = encabezadoTarifario.Numero

            Dim detalle = _persistencia.Consultar(filtro)

            If IsNothing(detalle) Then
                Return New Respuesta(Of SyncEncabezadoTarifarioVenta)() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            encabezadoTarifario.DetalleTarifario = New List(Of Object)

            For Each item As SyncDetalleTarifarioVenta In detalle
                encabezadoTarifario.DetalleTarifario.Add(ObtenerValoresDetalleTarifario(item))
            Next
            '#If DEBUG Then
            '            If IsNothing(encabezadoTarifario) = False AndAlso IsNothing(encabezadoTarifario.DetalleTarifario) = False Then
            '                encabezadoTarifario.DetalleTarifario = encabezadoTarifario.DetalleTarifario.Take(20000).ToList()
            '            End If
            '#End If
            Return New Respuesta(Of SyncEncabezadoTarifarioVenta)(encabezadoTarifario) With {.Cantidad_Detalles = encabezadoTarifario.DetalleTarifario.Count, .Numero = encabezadoTarifario.Numero}
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ConsultarTarifarioCliente(filtro As FiltroSyncApp) As Respuesta(Of SyncEncabezadoTarifarioVenta)

            If IsNothing(filtro) Then
                Return New Respuesta(Of SyncEncabezadoTarifarioVenta)() With {.MensajeOperacion = "Sin parametros de búsqueda", .ProcesoExitoso = False}
            End If

            If filtro.CodigoEmpresa <= 0 Then
                Return New Respuesta(Of SyncEncabezadoTarifarioVenta)() With {.MensajeOperacion = "Código de empresa invalido", .ProcesoExitoso = False}
            End If

            Dim tarifarioCliente = _persistencia.ObtenerEncabezadoTarifario(filtro.CodigoEmpresa, filtro.CodigoCliente)

            If IsNothing(tarifarioCliente) Or tarifarioCliente.Numero < 1 Then
                Return New Respuesta(Of SyncEncabezadoTarifarioVenta)() With {.MensajeOperacion = "No existe un tarifario para el cliente vigente o habilitado.", .ProcesoExitoso = False}
            End If

            filtro.NumeroTarifario = tarifarioCliente.Numero

            Dim detalle = _persistencia.Consultar(filtro)

            If IsNothing(detalle) Then
                Return New Respuesta(Of SyncEncabezadoTarifarioVenta)() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            ' Validar si existen condiciones comerciales y asignar prioridad sobre el tarifario.
            If (IsNothing(tarifarioCliente.CondicionesComerciales) = False And tarifarioCliente.CondicionesComerciales.Count > 0) Then

                ' Buscar en el detalle del tarifario las tarifas que cumplan con las condiciones comerciales y asignar los valores de las condiciones.
                For Each condicion In tarifarioCliente.CondicionesComerciales
                    For Each tarifa In detalle
                        If condicion.LineaNegocio.Codigo = tarifa.Ln And condicion.Tarifa.Codigo = tarifa.Tt And condicion.TipoTarifa.Codigo = tarifa.Ttt Then
                            ' Asignar valores
                            tarifa.FleteMinimo = condicion.FleteMinimo
                            tarifa.ManejoMinimo = condicion.ManejoMinimo
                            tarifa.PorcentajeValorDeclarado = condicion.PorcentajeValorDeclarado
                            tarifa.PorcentajeFlete = condicion.PorcentajeFlete
                            tarifa.PorcentajeVolumen = condicion.PorcentajeVolumen
                            tarifa.CondicionComercial = True
                        End If
                    Next
                Next
            End If

            tarifarioCliente.DetalleTarifario = New List(Of Object)

            For Each item As SyncDetalleTarifarioVenta In detalle
                tarifarioCliente.DetalleTarifario.Add(ObtenerValoresDetalleTarifario(item))
            Next

            tarifarioCliente.CondicionesComerciales = New List(Of CondicionComercialTarifaTercero)

            Return New Respuesta(Of SyncEncabezadoTarifarioVenta)(tarifarioCliente) With {.Cantidad_Detalles = tarifarioCliente.DetalleTarifario.Count, .Numero = tarifarioCliente.Numero}
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ConsultarPlanillasPendienteEntrega(filtro As FiltroSyncApp) As Respuesta(Of List(Of SyncEncabezadoPlanilla))

            Dim listadoPlanillaSync = New List(Of SyncEncabezadoPlanilla)

            If IsNothing(filtro) Then
                Return New Respuesta(Of List(Of SyncEncabezadoPlanilla))() With {.MensajeOperacion = "Sin parametros de búsqueda", .ProcesoExitoso = False}
            End If

            If filtro.CodigoEmpresa <= 0 Then
                Return New Respuesta(Of List(Of SyncEncabezadoPlanilla))() With {.MensajeOperacion = "Código de empresa invalido", .ProcesoExitoso = False}
            End If

            If filtro.CodigoConductor <= 0 Then
                Return New Respuesta(Of List(Of SyncEncabezadoPlanilla))() With {.MensajeOperacion = "Código de conductor invalido", .ProcesoExitoso = False}
            End If

            Dim filtroPlanilla As PlanillaGuias = ArmarFiltro(filtro)

            '' Consultar las recolecciones
            Dim listadoPlanillas = _persistencia.ObtenerEncabezadoPlanilla(filtroPlanilla).ToList()

            '' Mapeo al objeto de sincronizacion
            listadoPlanillas.Reverse()

            For Each planilla In listadoPlanillas
                listadoPlanillaSync.Add(New SyncEncabezadoPlanilla(planilla, TipoDocumentoPlanillaRecolecciones))
            Next

            '' Consultar las entregas
            filtroPlanilla.TipoDocumento = TipoDocumentoPlanillaEntregas
            listadoPlanillas = _persistencia.ObtenerEncabezadoPlanilla(filtroPlanilla)

            '' Mapeo al objeto de sincronizacion
            For Each planilla In listadoPlanillas
                listadoPlanillaSync.Add(New SyncEncabezadoPlanilla(planilla, TipoDocumentoPlanillaEntregas))
            Next

            If IsNothing(listadoPlanillaSync) Or listadoPlanillaSync.Count = 0 Then
                Return New Respuesta(Of List(Of SyncEncabezadoPlanilla))() With {.MensajeOperacion = "El conductor no tiene planilla asignadas.", .ProcesoExitoso = False}
            End If

            ' Luego recupero la información de las guias asociadas a la planilla.
            For Each planilla In listadoPlanillaSync

                If (planilla.Numero > 0) Then

                    Dim filtroRemesas As New RemesaPaqueteria With {
                    .CodigoEmpresa = filtro.CodigoEmpresa,
                    .Remesa = New Remesas() With {.NumeroplanillaDespacho = planilla.Numero},
                    .Estado = 1,
                    .NumeroPlanilla = -1,
                    .Planilla = New PlanillaPaqueteria With {.TipoDocumento = 135},
                    .ConsultarControlEntregas = 1
                }
                    '' Obtener el detalle de la planilla.
                    Dim remesasPlanilla = _persistencia.ObtenerRemesasPaqueteria(filtroRemesas)
                    Dim detallePlanilla = _persistencia.ObtenerDetallePlanilla(planilla.CodigoEmpresa, planilla.Numero)

                    For Each detalle In detallePlanilla
                        If (remesasPlanilla.Count(Function(x) x.Remesa.Numero = Convert.ToInt64(detalle.NumeroRemesa)) > 0) Then
                            planilla.DetallePlanilla.Add(detalle)
                        ElseIf (detalle.TipDoc = 200 Or detalle.TipDoc = 135) Then 'Recolección
                            planilla.DetallePlanilla.Add(detalle)
                        End If
                    Next

                    '' Buscar el Codigo del Aforador
                    Dim listaAuxiliares = _persistencia.ConsultarDetalleAxiliares(filtro.CodigoEmpresa, planilla.Numero)

                    If (IsNothing(listaAuxiliares) = False And listaAuxiliares.Count(Function(x) x.Aforador = 1) > 0) Then
                        Dim objAforador = listaAuxiliares.Where(Function(x) x.Aforador = 1).FirstOrDefault()

                        planilla.CodigoAforador = objAforador.Funcionario.Codigo
                        planilla.NombreAforador = objAforador.Funcionario.Nombre
                    End If

                    '' Obtener la lista de etiques preimpresas.
                    Dim listaEtiquetas = _persistencia.ConsultarDetalleAsignacionEtiquetasPreimpresas(filtro.CodigoEmpresa, filtro.CodigoOficina, planilla.CodigoAforador)
                    planilla.DetalleAsignacionEtiquetas = listaEtiquetas

                    For Each detalle In planilla.DetallePlanilla

                        detalle.NumeroDocumentoPlanilla = planilla.NumeroDocumento
                        detalle.Tipo = 1 'Guia Entrega
                        detalle.Off = 0

                        If (detalle.TipDoc = 200) Then
                            detalle.Tipo = 2 'Recolección
                        End If

                        If (detalle.EsGuiaRecogida = 1) Then
                            detalle.Tipo = 3 'Guia Recogida
                        End If

                        If (detalle.Tipo = 1 And detalle.Cum = 1) Then
                            detalle.Est = 6030
                        End If

                        'Validar la gestion documental de cada guia.
                        If (detalle.TieneDoc > 0) Then
                            Dim listaDocumentos = _persistencia.ConsultarDetalleGestionDocumentos(filtro.CodigoEmpresa, detalle.NumeroRemesa)
                            detalle.GesDoc = New List(Of SyncGestionDocumentos)
                            For Each documento In listaDocumentos
                                detalle.GesDoc.Add(New SyncGestionDocumentos(documento))
                            Next
                        End If

                        'Validar los intentos de entrega generados.
                        Dim listaIntentosEntrega = _persistencia.ConsultarIntentosEntregaGuia(filtro.CodigoEmpresa, detalle.NumeroRemesa)
                        detalle.Intentos = New List(Of SyncIntentoEntrega)
                        For Each intento In listaIntentosEntrega
                            detalle.Intentos.Add(New SyncIntentoEntrega(intento))
                        Next
                    Next

                    If (IsNothing(planilla) = False) Then
                        planilla.DetallePlanilla.Reverse()
                    End If
                End If
            Next

            Return New Respuesta(Of List(Of SyncEncabezadoPlanilla))(listadoPlanillaSync) With {.Cantidad_Detalles = listadoPlanillaSync.Count}
        End Function

        Private Function ArmarFiltro(filtro As FiltroSyncApp) As PlanillaGuias
            '' Crear filtro planillas
            Dim filtroPlanilla As New PlanillaGuias() With {
                .FechaFinal = DateTime.Today.AddDays(1),
                .FechaInicial = DateTime.Today.AddDays(-8),
                .Conductor = New Terceros() With {.Codigo = filtro.CodigoConductor},
                .CodigoEmpresa = filtro.CodigoEmpresa,
                .Planilla = New Planillas() With {.Estado = New Estado() With {.Codigo = 1}},
                .TipoDocumento = TipoDocumentoPlanillaRecolecciones
                }
            Return filtroPlanilla
        End Function

        Public Function ConsultarImagenesRemesaPaqueteria(codigoEmpresa As Short, numeroRemesa As Long) As Respuesta(Of IEnumerable(Of SyncDetallePlanillaImagenes))
            Dim lista = _persistencia.ConsultarImagenesRemesaPaqueteria(codigoEmpresa, numeroRemesa)

            If IsNothing(lista) Then
                Return New Respuesta(Of IEnumerable(Of SyncDetallePlanillaImagenes))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            Else
                Return New Respuesta(Of IEnumerable(Of SyncDetallePlanillaImagenes))(lista) With {.Cantidad_Detalles = lista.Count}
            End If
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ConsultarTerceros(filtro As Terceros) As Respuesta(Of IEnumerable(Of SyncTerceros))

            Dim lista = _persistencia.ConsultarTerceros(filtro)

            If IsNothing(lista) Then
                Return New Respuesta(Of IEnumerable(Of SyncTerceros))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            Else

                Dim resultado = New List(Of SyncTerceros)

                For Each item In lista

                    Dim objTercero = New SyncTerceros(item) With {
                        .GesDoc = New List(Of SyncGestionDocumentos)
                    }

                    If (IsNothing(item) = False And IsNothing(item.Cliente) = False) Then

                        If (item.Cliente.GestionDocumentos = 1) Then
                            ' Consultar la informacion de gestion documentos si aplica para el tercero.
                            Dim listaGestionDocumental = _persistencia.ConsultarGestionDocumentosTercero(filtro.CodigoEmpresa, item.Codigo)

                            If (IsNothing(listaGestionDocumental) = False) Then

                                For Each documento In listaGestionDocumental
                                    objTercero.GesDoc.Add(New SyncGestionDocumentos(documento))
                                Next
                            End If
                        End If

                        objTercero.Fp = _persistencia.ConsultarFormasPagoTercero(filtro.CodigoEmpresa, item.Codigo).Select(Function(x) x.Codigo).ToList()

                    End If

                    resultado.Add(objTercero)
                Next

                Return New Respuesta(Of IEnumerable(Of SyncTerceros))(resultado) With {.Cantidad_Detalles = lista.Count}
            End If

        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ConsultarCiudades(filtro As Ciudades) As Respuesta(Of IEnumerable(Of SyncCiudades))

            Dim lista = _persistencia.ConsultarCiudades(filtro)

            If IsNothing(lista) Then
                Return New Respuesta(Of IEnumerable(Of SyncCiudades))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            Else

                Dim resultado = New List(Of SyncCiudades)

                For Each item In lista
                    resultado.Add(New SyncCiudades(item))
                Next

                Return New Respuesta(Of IEnumerable(Of SyncCiudades))(resultado) With {.Cantidad_Detalles = lista.Count}
            End If

        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ConsultarSitiosCargueDescargue(filtro As SitiosCargueDescargue) As Respuesta(Of IEnumerable(Of SyncSitiosCargueDescargue))

            Dim lista = _persistencia.ConsultarSitiosCargueDescargue(filtro)

            If IsNothing(lista) Then
                Return New Respuesta(Of IEnumerable(Of SyncSitiosCargueDescargue))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            Else

                Dim resultado = New List(Of SyncSitiosCargueDescargue)

                For Each item In lista
                    resultado.Add(New SyncSitiosCargueDescargue(item))
                Next

                Return New Respuesta(Of IEnumerable(Of SyncSitiosCargueDescargue))(resultado) With {.Cantidad_Detalles = lista.Count}
            End If

        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ConsultarProductosTransportador(filtro As ProductoTransportados) As Respuesta(Of IEnumerable(Of SyncProductosTransportados))

            Dim lista = _persistencia.ConsultarProductosTransportados(filtro)

            If IsNothing(lista) Then
                Return New Respuesta(Of IEnumerable(Of SyncProductosTransportados))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            Else

                Dim resultado = New List(Of SyncProductosTransportados)

                For Each item In lista
                    resultado.Add(New SyncProductosTransportados(item))
                Next

                Return New Respuesta(Of IEnumerable(Of SyncProductosTransportados))(resultado) With {.Cantidad_Detalles = lista.Count}
            End If

        End Function

        Public Function InsertarDetalleEtiquetasPreimpresas(registro As IEnumerable(Of SyncEtiquetaPreimpresa)) As Respuesta(Of Boolean)

            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim resultado As Boolean = False

            resultado = _persistencia.InsertarDetalleEtiquetasPreimpresas(registro)

            If Not resultado Then
                Return New Respuesta(Of Boolean)(False)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True}

        End Function

        Public Function LiberarEtiquetas(entidad As EtiquetaPreimpresa) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = _persistencia.LiberarEtiquetas(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, "liberó"), .Datos = anulo}
        End Function

        Public Function InsertarDetalleIntentoEntregaGuia(registro As DetalleIntentoEntregaGuia) As Respuesta(Of Boolean)

            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim resultado = _persistencia.InsertarDetalleIntentoEntregaGuia(registro)

            If resultado <= 0 Then
                Return New Respuesta(Of Boolean)(False) With {.ProcesoExitoso = False, .MensajeOperacion = "El registro NO se guardó correctamente, intentelo nuevamente."}
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, registro.ID, Recursos.OperacionInserto)}

        End Function

        Public Function ModificarEstadoRecoleccion(codigoEmpresa As Integer, numeroRecoleccion As Integer, codigoEstado As Integer) As Respuesta(Of Boolean)

            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim resultado = _persistencia.ModificarEstadoRecoleccion(codigoEmpresa, numeroRecoleccion, codigoEstado)

            If resultado <= 0 Then
                Return New Respuesta(Of Boolean)(False) With {.ProcesoExitoso = False, .MensajeOperacion = "El registro NO se guardó correctamente, intentelo nuevamente."}
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, resultado, Recursos.OperacionModifico)}

        End Function

        Public Function GuardarEntrega(entidad As Remesas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Numero = _persistencia.GuardarEntrega(entidad)
            Return New Respuesta(Of Long)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad, Recursos.OperacionModifico), .Datos = Numero}
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="datosTarifario"></param>
        ''' <returns></returns>
        Private Function GenerarEsquema(datosTarifario As SyncEncabezadoTarifarioVenta, datosPlanilla As SyncEncabezadoPlanilla) As DataBaseOffLine

            Dim baseDatos As New DataBaseOffLine With {
                .Tables = New List(Of Table)
            }

            'CrearTablaEncabezadoTarifario(datosTarifario, baseDatos)
            'CrearTablaDetalleTarifario(datosTarifario.DetalleTarifario, baseDatos)
            'CrearTablaEncabezadoPlanilla(datosPlanilla, baseDatos)
            'CrearTablaDetallePlanilla(datosPlanilla.DetallePlanilla, baseDatos)

            Return baseDatos

        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        Private Function GenerarEsquema() As DataBaseOffLine

            Dim baseDatos As New DataBaseOffLine With {
                .Tables = New List(Of Table)
            }

            CrearTablaFotosRemesas(Nothing, baseDatos)

            Return baseDatos

        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="datos"></param>
        ''' <param name="baseDatos"></param>
        Private Sub CrearTablaFotosRemesas(datos As SyncFotosRemesa, baseDatos As DataBaseOffLine)
            Dim tabla = New Table("FotosRemesa") With {
                .Schema = CrearEstructuraTabla(New SyncFotosRemesa()),
                .Values = New List(Of List(Of Object))
            }

            baseDatos.Tables.Add(tabla)
        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="datos"></param>
        ''' <param name="baseDatos"></param>
        Private Sub CrearTablaDetalleTarifario(datos As List(Of SyncDetalleTarifarioVenta), baseDatos As DataBaseOffLine)
            Dim tabla = New Table("DetalleTarifario") With {
                .Schema = CrearEstructuraTabla(Of SyncDetalleTarifarioVenta)(New SyncDetalleTarifarioVenta()),
                .Values = New List(Of List(Of Object))
            }

            If (IsNothing(datos) = False) Then
                For Each detalle As SyncDetalleTarifarioVenta In datos
                    tabla.Values.Add(ObtenerValoresDetalleTarifario(detalle))
                Next
            End If

            baseDatos.Tables.Add(tabla)
        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="datos"></param>
        ''' <param name="baseDatos"></param>
        Private Sub CrearTablaEncabezadoPlanilla(datos As SyncEncabezadoPlanilla, baseDatos As DataBaseOffLine)
            Dim tabla = New Table("EncabezadoPlanilla") With {
                .Schema = CrearEstructuraTabla(Of SyncEncabezadoPlanilla)(New SyncEncabezadoPlanilla()),
                .Values = New List(Of List(Of Object))
            }

            If (IsNothing(datos) = False) Then
                tabla.Values.Add(ObtenerValoresEncabezadoPlanilla(datos))
            End If

            baseDatos.Tables.Add(tabla)
        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="datos"></param>
        ''' <param name="baseDatos"></param>
        Private Sub CrearTablaDetallePlanilla(datos As List(Of SyncDetallePlanilla), baseDatos As DataBaseOffLine)
            Dim tabla = New Table("DetallePlanilla") With {
                .Schema = CrearEstructuraTabla(Of SyncDetallePlanilla)(New SyncDetallePlanilla()),
                .Values = New List(Of List(Of Object))
            }

            If (IsNothing(datos) = False) Then
                For Each detalle As SyncDetallePlanilla In datos
                    tabla.Values.Add(ObtenerValoresDetalllePlanilla(detalle))
                Next
            End If

            baseDatos.Tables.Add(tabla)
        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="registro"></param>
        ''' <returns></returns>
        Private Function CrearEstructuraTabla(Of T)(registro As T) As List(Of Schema)
            Dim listaCampos As List(Of Schema) = New List(Of Schema)

            For Each propiedad As PropertyInfo In registro.GetType().GetProperties()
                Dim config = propiedad.GetCustomAttribute(Of SchemaOffLine)

                If (IsNothing(config) = False) Then
                    listaCampos.Add(New Schema(config))
                End If
            Next

            Return listaCampos
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="registro"></param>
        ''' <returns></returns>
        Private Function ObtenerValoresEncabezadoTarifario(registro As SyncEncabezadoTarifarioVenta) As List(Of Object)
            Dim listaValores As List(Of Object) = New List(Of Object) From {
                registro.CodigoEmpresa,
                registro.Numero,
                registro.Nombre,
                registro.GestionDocumentos,
                registro.Estado
            }

            Return listaValores
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="registro"></param>
        ''' <returns></returns>
        Private Function ObtenerValoresDetalleTarifario(registro As SyncDetalleTarifarioVenta) As List(Of Object)
            Dim listaValores As List(Of Object) = New List(Of Object) From {
                 registro.Id,
                 registro.Ln,
                 registro.Tln,
                 registro.Tt,
                 registro.Ttt,
                 registro.Vf,
                 registro.Ve,
                 registro.O1,
                 registro.O2,
                 registro.O3,
                 registro.O4,
                 registro.Nttc,
                 registro.Ntc,
                 registro.Fpv,
                 registro.Ca,
                 registro.Vc1,
                 registro.Vc2,
                 registro.Vc3,
                 registro.Vm,
                 registro.Vs,
                 registro.Ps,
                 registro.Re,
                 registro.Pr,
                 registro.Vc,
                 registro.Vd,
                 registro.Co,
                 registro.Cd,
                 registro.CondicionComercial,
                 registro.FleteMinimo,
                 registro.ManejoMinimo,
                 registro.PorcentajeValorDeclarado,
                 registro.PorcentajeFlete,
                 registro.PorcentajeVolumen
             }

            Return listaValores
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="registro"></param>
        ''' <returns></returns>
        Private Function ObtenerValoresEncabezadoPlanilla(registro As SyncEncabezadoPlanilla) As List(Of Object)
            Dim listaValores As List(Of Object) = New List(Of Object) From {
                registro.CodigoEmpresa,
                registro.Numero,
                registro.NumeroDocumento,
                registro.TipoDocumento,
                registro.Estado
            }

            Return listaValores
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="registro"></param>
        ''' <returns></returns>
        Private Function ObtenerValoresDetalllePlanilla(registro As SyncDetallePlanilla) As List(Of Object)
            Dim listaValores As List(Of Object) = New List(Of Object) From {
                registro.CodEmp,
                registro.NumeroPlanilla,
                registro.TipDoc,
                registro.NumeroRemesa
            }

            Return listaValores
        End Function

    End Class
End Namespace
