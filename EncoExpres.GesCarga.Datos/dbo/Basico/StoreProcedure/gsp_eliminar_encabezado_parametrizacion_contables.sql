﻿PRINT 'gsp_eliminar_encabezado_parametrizacion_contables'
GO
DROP PROCEDURE gsp_eliminar_encabezado_parametrizacion_contables
GO
CREATE PROCEDURE gsp_eliminar_encabezado_parametrizacion_contables
(
  @par_EMPR_Codigo SMALLINT,
  @par_Codigo NUMERIC
  )
AS
BEGIN
  DELETE Encabezado_Parametrizacion_Contables 
  WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND Codigo = @par_Codigo

	DELETE Detalle_Parametrizacion_Contables
	WHERE 
	EMPR_Codigo = @par_EMPR_Codigo
	AND ENPC_Codigo  = @par_Codigo

    SELECT @@ROWCOUNT AS Numero
END
GO