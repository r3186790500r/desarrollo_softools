﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 07/12/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_modificar_cierre_contable'
GO
DROP PROCEDURE gsp_modificar_cierre_contable
GO 
CREATE PROCEDURE gsp_modificar_cierre_contable 
(      
 @par_EMPR_Codigo SMALLINT,      
 @par_Ano NUMERIC,      
 @par_Mes NUMERIC,      
 @par_Tipo_Documento NUMERIC,       
 @par_Estado_Cierre NUMERIC,  
 @par_USUA_Codigo_Modifica SMALLINT      
)      
AS      
BEGIN      
 UPDATE      
  Cierre_Contable_Documentos      
 SET      
  EMPR_Codigo = @par_EMPR_Codigo,    
  Ano = @par_Ano,    
  CATA_MESE_Codigo = @par_Mes,    
  TIDO_Codigo = @par_Tipo_Documento,    
  CATA_ESPC_Codigo = @par_Estado_Cierre,     
  Fecha_Modifica = GETDATE(),    
  USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica    
 WHERE      
  EMPR_Codigo = @par_EMPR_codigo     
  AND Ano = @par_Ano   
  AND CATA_MESE_Codigo = @par_Mes  
  AND TIDO_Codigo = @par_Tipo_Documento       
 
 SELECT Codigo = @@ROWCOUNT        
END      
GO