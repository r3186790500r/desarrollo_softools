﻿print ('gsp_consultar_direcciones_terceros') 
go
drop procedure gsp_consultar_direcciones_terceros
go
create procedure gsp_consultar_direcciones_terceros
(@par_EMPR_Codigo SMALLINT,
@par_TERC_Codigo numeric)
as
begin
select  
TEDI.EMPR_Codigo
,TEDI.TERC_Codigo
,TEDI.Direccion
,ISNULL(TEDI.Telefonos,'')  as Telefonos
,TEDI.CIUD_Codigo AS CIUD_Codigo_Direccion
from Tercero_Direcciones TEDI
left join Terceros TERC ON
TERC.EMPR_Codigo = TEDI.EMPR_Codigo
AND TERC.Codigo = TEDI.TERC_Codigo

left join Ciudades CIUD ON
CIUD.EMPR_Codigo = TEDI.EMPR_Codigo
AND CIUD.Codigo = TEDI.CIUD_Codigo

where 
TEDI.EMPR_Codigo = @par_EMPR_Codigo
AND TEDI.TERC_Codigo = @par_TERC_Codigo
end
go
