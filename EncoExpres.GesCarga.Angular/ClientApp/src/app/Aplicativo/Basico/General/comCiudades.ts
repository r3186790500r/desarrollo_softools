import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
// Services
import { serCiudades } from '../../../services/serCiudades';
// Interfaces
import { Filtro, Respuesta, EstadoRegistro, Ciudad, Departamento } from '../../../interfaces/ifaGeneral';

@Component({
  selector: 'app-ciudades',
  templateUrl: './comCiudades.html'
})
export class comCiudades implements OnInit{
  form: FormGroup;

  // Variables Consulta
  public numEmpresa = 6; numEstado = 1; numPagina = 1; numRegPag = 10; objFiltro: Filtro;
  public numTotaRegi: number; numTotalPaginas: number; bolProcesoExitoso: boolean;

  // Variables Campos Pagina
  public Codigo: number; CodigoAlterno: string; Nombre: string; Estado = 1; lstEstados: EstadoRegistro[]; lstMensajesError: string[]; strMensajeSinRegistros: string;
  // Variables apiConsultar
  public objRespuesta: Observable<Respuesta>; lstCiudades: Ciudad[]; strJson: string; strMensaje: string;
  // Manejo Detalle
  selectedCiudad: Ciudad; objCiudad: Ciudad; objDepartamento: Departamento;

  constructor(public http: HttpClient, protected serCiudades: serCiudades) {
    this.lstEstados = [{ Codigo: 1, Nombre: "ACTIVO" }, { Codigo: 0, Nombre: "INACTIVO" }];
    this.FormControles();
  }

  ngOnInit() {
    this.lstEstados = [{ Codigo: 1, Nombre: "ACTIVO" }, { Codigo: 0, Nombre: "INACTIVO" }];
  }

  onSelect(ciudad: Ciudad): void {
    this.selectedCiudad = ciudad;
  }

  public funBuscar() {
    try {
      this.numPagina = 1;
      this.funConsultar();
    }
    catch (error) {
      this.lstMensajesError.push(String(error));
    }
  }

  public funPrimerPagina() {
    this.numPagina = 1;
    this.funConsultar();
  }

  public funSiguientePagina() {
    this.numPagina ++;
    this.funConsultar();
  }

  public funAnteriorPagina() {
    this.numPagina --;
    this.funConsultar();
  }

  public funUltimaPagina() {
    this.numPagina = this.numTotalPaginas;
    this.funConsultar();
  }

  public funConsultar() {
    try {
      this.numEstado = this.form.controls.cmbEstado.value;
      
      this.objFiltro = {
        CodigoEmpresa: this.numEmpresa,
        Codigo: this.Codigo,
        CodigoAlterno: this.form.controls.txtBuscCodi.value,
        Nombre: this.form.controls.txtBuscNomb.value,
        Estado: this.numEstado,
        Pagina: this.numPagina,
        RegistrosPagina: this.numRegPag
      };

      this.serCiudades.apiConsultar(this.objFiltro).subscribe(resp => {
        if (resp.ProcesoExitoso === true) {

          // El objeto objPost.Datos se convierte en un string JSON
          this.strJson = JSON.stringify(resp.Datos);
          // El string strJson se convierte en arreglo
          this.lstCiudades = JSON.parse(this.strJson);

          if (this.lstCiudades.length > 0) {
            this.numTotaRegi = resp.Datos[0].TotalRegistros;
            this.numTotalPaginas = Math.round(this.numTotaRegi / this.numRegPag);
            this.strMensajeSinRegistros = "";
          } else {
            this.numTotaRegi = 0;
            this.numTotalPaginas = 0;
            this.strMensajeSinRegistros = "No se encontró información para los filtros ingresados"
          }
          
        }else {
          this.lstMensajesError.push(resp.MensajeOperacion);
        }
      }
      );

    }
    catch (error) {
      this.lstMensajesError.push(String(error));
    }
  }

  public funEliminar(CodiCiud: number) {
    try {

      this.objCiudad = {
        CodigoEmpresa: this.numEmpresa,
        Codigo: CodiCiud,
        CodigoCiudad: 0,
        CodigoAlterno: "",
        Nombre: "",
        Departamento: this.objDepartamento,
        Estado: 0,
        UsuarioCrea: 0,
        UsuarioModifica: 0
      };

      this.serCiudades.apiEliminar(this.objCiudad).subscribe(resp => {
        if (resp.ProcesoExitoso === true) {

          this.funConsultar();

        } else {
          this.lstMensajesError.push(resp.MensajeOperacion);
        }
      }
      );

    }
    catch (error) {
      this.lstMensajesError.push(String(error));
    }
  }

  private FormControles() {
    this.form = new FormGroup({
      txtBuscCodi: new FormControl('', []),
      txtBuscNomb: new FormControl('', []),
      txtBuscDepa: new FormControl('', []),
      cmbEstado: new FormControl('ACTIVO', []),
    });
  }

 }
