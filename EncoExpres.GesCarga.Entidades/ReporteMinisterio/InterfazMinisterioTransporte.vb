﻿'Imports EncoExpres.GesCarga.Conexion
'Imports EncoExpres.GesCarga.Entidades.Despachos.Procesos
'Imports EncoExpres.GesCarga.Entidades.Entidades.ReporteMinisterio
'Imports EncoExpres.GesCarga.Entidades
'Imports Microsoft.VisualBasic
'Imports System.Xml
'Imports System.Xml.Serialization
'Imports System.IO
'Imports System
'Imports System.Data
'Imports System.Transactions
'Imports System.Data.SqlClient
'Imports System.Configuration
'Imports System.Collections.Generic
'Imports System.Threading



'Namespace Entidades.ReporteMinisterio
'    Public Class InterfazMinisterioTransporte

'#Region "Variables"
'        Dim strSQL As String
'        Dim objGeneral As General
'        'Dim objEmpresa As Empresas
'        Dim dteFechaInicial As Date
'        Dim dteFechaFinal As Date

'        Dim solicitudConsulta As Solicitud
'        Dim wsManifiestoElectronico As ManifiestoElectronico.IBPMServicesservice
'        Dim acceso As Acceso
'        Dim strError As String
'        Dim dtsRemesa As DataSet
'        Dim EnuAmbiente As Ambiantes_Desarrollos
'        'Dim objUsuario As Usuario

'        Private strTercDupl As String
'        Private strVehiDupl As String
'        Private strRemeDupl As String
'        Private strManiDupl As String
'        Private strCumpRemeDupli As String
'        Private strCumpManiDupli As String

'        Dim lonTiempoSegundos As Long
'        Dim intActivaEnvioMultiProceso As Integer
'        Dim thrControProceso As Thread
'        Dim thrEnvioMinisterio As Thread
'        Private lonCodigoConfirmacion As String

'        Private strTextoXmlRemitente As String
'        Private strTextoXmlDestinatario As String
'        Private strTextXmlRemesa As String

'        Private strTextXmlRemesaAnulada As String
'        Private strTextXmlAnulacionCarga As String

'        Private strTextXmlManifiestoAnulado As String
'        Private strTextXmlCumplidoAnulado As String
'        Private strTextXmlAnulacionViaje As String

'        Private strTextoXmlTenedor As String
'        Private strTextoXmlPropietario As String
'        Private strTextXmlConductor As String
'        Private strTextXmlVehiculo As String
'        Private strTextXmlRemolque As String
'        Private strTextXmlManifiesto As String

'        Private strMensaje As String
'        Private intEstadoReporteDocumento As EnumEstadoReporteDocumentos
'        Private TipoDocumento As EnumTipoDocumentoGenera
'        Private TipoReporteDocumento As EnumTipoReporteDocumento
'        Private enumEnlacesMinisterio As EnumEnlaceServicioMinisterio
'        Private enumTemporalEnlacesMinisterio As EnumEnlaceServicioMinisterio

'        Private strPrimerEnlaceWebServiceMinisterio As String
'        Private strSegundoEnlaceWebServiceMinisterio As String
'        'Private objvalidacion As Validacion
'        Private lonCantidadRepeticionesProceso As Integer
'        Private dblCantidadSegundosPrimerReporte As Double

'        Private strPeriodoConsultaSiceTac As String
'#End Region

'#Region "Constantes"

'        Const REMITENTE As String = "REMITENTE"
'        Const DESTINATARIO As String = "DESTINATARIO DETALLE REMESA"
'        Const PROPIETARIO As String = "PROPIETARIO"
'        Const TENEDOR As String = "TENEDOR"
'        Const CONDUCTOR As String = "CONDUCTOR"
'        Const VEHICULO As String = "VEHICULO"
'        Const SEMIREMOLQUE As String = "SEMIREMOLQUE"

'        ' Constantes de Tipos de Campo
'        Public Const CAMPO_NUMERICO As Integer = 1
'        Public Const CAMPO_CARACTER As Integer = 2
'        Public Const CAMPO_DECIMAL As Integer = 3
'        Public Const CAMPO_FECHA As Integer = 4
'        Public Const CAMPO_CARACTER_CEROS As Integer = 5
'        Public Const CAMPO_IDENTIFICACION As Integer = 6
'        Public Const CAMPO_CEROS_IZQUIERDA As Integer = 7
'        Private Const CAMPO_DECIMAL_CON_PUNTO_UNOE As Integer = 8
'        Const CAMPO_ESPACIOS_IZQUIERDA As Integer = 9
'        Const CAMPO_DECIMAL_CON_PUNTO As Integer = 10
'        Public Const CAMPO_CEROS_DERECHA As Integer = 11
'        Const SOLO_CAMPO_DECIMAL_CON_PUNTO As Integer = 12
'        Private Const CAMPO_FECHA_UNOE As Short = 15
'        Private Const CAMPO_DECIMAL_UNOE_85 As Short = 16

'        Const CARACTER_ENTER As String = Chr(13)
'        'Public Const SEPARADOR_CNT As String = vbTab
'        Public Const SEPARADOR_CNT As String = " "
'        Public Const FORMATO_FECHA_CNT As String = "ddMMyyyy"
'        Private Const CARACTERES_EXTRANOS_CNT As String = " ï»¿"
'        Private Const INICIAL_CONSIGNACION As String = "CONS"

'        Private Const CONFIGURACION_REMOLQUE_X_DEFECTO As Integer = 63
'        Private Const CONFIGURACION_VEHICULO_X_DEFECTO As Integer = 51
'        Private Const CAPACIDAD_UNIDAD_MARCA_X_DEFECTO As Integer = 2000
'        Private Const NUMERO_EJES_X_DEFECTO As Integer = 4
'        Private Const VALIDACION_CAMBIO_ENLACE_MINISTERIO As String = "clsInterMinisterioTransVali1"
'        Private Const TIEMPO_DEMORA_UN_SEGUNDO As Integer = 1000
'        Private Const TIPO_REMESA_URBANA As Integer = 81
'#End Region

'#Region "Propiedades"

'        Public Property General As General
'            Get
'                Return Me.objGeneral
'            End Get
'            Set(value As General)
'                Me.objGeneral = value
'            End Set
'        End Property
'        Public Property Empresa As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property
'        Public Property Ambiente As Ambiantes_Desarrollos
'            Get
'                Return Me.EnuAmbiente
'            End Get
'            Set(value As Ambiantes_Desarrollos)
'                Me.EnuAmbiente = value
'            End Set
'        End Property
'        Public Property Usuario As Usuario
'            Get
'                Return Me.objUsuario
'            End Get
'            Set(value As Usuario)
'                Me.objUsuario = value
'            End Set
'        End Property

'        Public ReadOnly Property XmlRemitente As String
'            Get
'                Return Me.strTextoXmlRemitente
'            End Get
'        End Property
'        Public ReadOnly Property XmlDestinatario As String
'            Get
'                Return Me.strTextoXmlDestinatario
'            End Get
'        End Property
'        Public ReadOnly Property XmlRemesa As String
'            Get
'                Return Me.strTextXmlRemesa
'            End Get
'        End Property

'        Public ReadOnly Property XmlRemesaAnulada As String
'            Get
'                Return Me.strTextXmlRemesaAnulada
'            End Get
'        End Property

'        Public ReadOnly Property XmlAnulacionCarga As String
'            Get
'                Return Me.strTextXmlAnulacionCarga
'            End Get
'        End Property

'        Public ReadOnly Property XmlManifiestoAnulado As String
'            Get
'                Return Me.strTextXmlManifiestoAnulado
'            End Get
'        End Property

'        Public ReadOnly Property XmlAnulacionViaje As String
'            Get
'                Return Me.strTextXmlAnulacionViaje
'            End Get
'        End Property

'        Public ReadOnly Property XmlCumplidoAnulado As String
'            Get
'                Return Me.strTextXmlCumplidoAnulado
'            End Get
'        End Property
'        Public ReadOnly Property CodigoConfirmacion As Long
'            Get
'                Return Me.lonCodigoConfirmacion
'            End Get
'        End Property

'        Public ReadOnly Property XmlPropietario As String
'            Get
'                Return Me.strTextoXmlPropietario
'            End Get
'        End Property
'        Public ReadOnly Property XmlTenedor As String
'            Get
'                Return Me.strTextoXmlTenedor
'            End Get
'        End Property
'        Public ReadOnly Property XmlConductor As String
'            Get
'                Return Me.strTextXmlConductor
'            End Get
'        End Property
'        Public ReadOnly Property XmlVehiculo As String
'            Get
'                Return Me.strTextXmlVehiculo
'            End Get
'        End Property
'        Public ReadOnly Property XmlSemiremolque As String
'            Get
'                Return Me.strTextXmlRemolque
'            End Get
'        End Property
'        Public ReadOnly Property XmlManifiesto As String
'            Get
'                Return Me.strTextXmlManifiesto
'            End Get
'        End Property
'        Public ReadOnly Property Mensaje As String
'            Get
'                Return Me.strMensaje
'            End Get
'        End Property

'        Public Property TipoReporte As EnumTipoReporteDocumento
'            Get
'                Return Me.TipoReporteDocumento
'            End Get
'            Set(value As EnumTipoReporteDocumento)
'                Me.TipoReporteDocumento = value
'            End Set
'        End Property
'        Public Property EnlacesMinisterio As EnumEnlaceServicioMinisterio
'            Get
'                Return Me.enumEnlacesMinisterio
'            End Get
'            Set(value As EnumEnlaceServicioMinisterio)
'                Me.enumEnlacesMinisterio = value
'            End Set
'        End Property

'#End Region

'#Region "Constructor"
'        Sub New(ByVal lonCodigoEmpresa As Long, Optional ByVal ObjGeneral_Parametro As General = Nothing)
'            objEmpresa = New Empresas(lonCodigoEmpresa)
'            solicitudConsulta = New Solicitud
'            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
'            objGeneral = New General
'            If Not ObjGeneral_Parametro Is Nothing Then
'                objGeneral.CadenaDeConexionSQL = ObjGeneral_Parametro.CadenaDeConexionSQL
'            End If
'            Me.dtsRemesa = New DataSet
'            Dim arrCampos(3), arrValoresCampos(3) As String
'            arrCampos(0) = "Nit_Manifiesto_Electronico"
'            arrCampos(1) = "Usuario_Manifiesto_Electronico"
'            arrCampos(2) = "Clave_Manifiesto_Electronico"

'            If Me.objEmpresa.Retorna_Campos(Me.objEmpresa.Codigo, arrCampos, arrValoresCampos, 3, "", strError) Then
'                Me.objEmpresa.NumeroIdentificacion = Replace(Replace(arrValoresCampos(0), ".", ""), "-", "")
'                Me.objEmpresa.UsuarioManifiestoElectronico = arrValoresCampos(1)
'                Me.objEmpresa.ClaveManifiestoElectronico = arrValoresCampos(2)
'            End If
'            Me.objUsuario = New Usuario
'            Me.acceso = New Acceso(Me.objEmpresa.UsuarioManifiestoElectronico, Me.objEmpresa.ClaveManifiestoElectronico, Acceso.AMBIENTE_WS_MINISTERIO_R)
'            Me.strTercDupl = ConfigurationManager.AppSettings("SiglaTerceroDuplicadoRNDC").ToString()
'            Me.strVehiDupl = ConfigurationManager.AppSettings("SiglaVehiculoDuplicadoRNDC").ToString()
'            Me.strRemeDupl = ConfigurationManager.AppSettings("SiglaRemesaDuplicadoRNDC").ToString()
'            Me.strManiDupl = ConfigurationManager.AppSettings("SiglaManifiestoDuplicadoRNDC").ToString()
'            Me.strCumpRemeDupli = ConfigurationManager.AppSettings("SiglaCumplidoRemesaDuplicadoRNDC").ToString()
'            Me.strCumpManiDupli = ConfigurationManager.AppSettings("SiglaCumplidoManifiestoDuplicadoRNDC").ToString()
'            Me.intActivaEnvioMultiProceso = ConfigurationManager.AppSettings("ActivarEnvioMultiproceso").ToString()
'            Me.lonCodigoConfirmacion = General.CERO
'            Me.TipoReporteDocumento = EnumTipoReporteDocumento.DESDE_CREACION_DOCUMENTO
'            Me.objvalidacion = New Validacion
'            If Not Me.objvalidacion.Consultar(objEmpresa, VALIDACION_CAMBIO_ENLACE_MINISTERIO) Then
'                Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'            Else
'                Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'            End If
'            Me.strPrimerEnlaceWebServiceMinisterio = ConfigurationManager.AppSettings("ManifiestoElectronico.IBPMServicesservice").ToString()
'            Me.strSegundoEnlaceWebServiceMinisterio = ConfigurationManager.AppSettings("ManifiestoElectronico.IBPMServicesservice2").ToString()

'            Me.lonCantidadRepeticionesProceso = General.CERO
'            Me.dblCantidadSegundosPrimerReporte = General.CERO

'            Me.strPeriodoConsultaSiceTac = ConfigurationManager.AppSettings("PeriodoConsultaSICETAC").ToString()
'        End Sub
'        Sub New()
'            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
'            strSQL = String.Empty
'            Me.objGeneral = New General
'            Me.objEmpresa = New Empresas
'            Me.dteFechaInicial = Date.Now
'            Me.dteFechaFinal = Date.Now
'            Me.solicitudConsulta = New Solicitud
'            Me.acceso = New Acceso
'            Me.strError = String.Empty
'            Me.dtsRemesa = New DataSet
'            Me.EnuAmbiente = Ambiantes_Desarrollos.GESTRANS45
'            Me.objUsuario = New Usuario
'            Me.intActivaEnvioMultiProceso = ConfigurationManager.AppSettings("ActivarEnvioMultiproceso").ToString()
'            Me.strTercDupl = ConfigurationManager.AppSettings("SiglaTerceroDuplicadoRNDC").ToString()
'            Me.strVehiDupl = ConfigurationManager.AppSettings("SiglaVehiculoDuplicadoRNDC").ToString()
'            Me.strRemeDupl = ConfigurationManager.AppSettings("SiglaRemesaDuplicadoRNDC").ToString()
'            Me.strManiDupl = ConfigurationManager.AppSettings("SiglaManifiestoDuplicadoRNDC").ToString()
'            Me.strCumpRemeDupli = ConfigurationManager.AppSettings("SiglaCumplidoRemesaDuplicadoRNDC").ToString()
'            Me.strCumpManiDupli = ConfigurationManager.AppSettings("SiglaCumplidoManifiestoDuplicadoRNDC").ToString()
'            Me.lonCodigoConfirmacion = General.CERO
'            Me.TipoReporteDocumento = EnumTipoReporteDocumento.DESDE_CREACION_DOCUMENTO
'            Me.objvalidacion = New Validacion
'            If Not Me.objvalidacion.Consultar(objEmpresa, VALIDACION_CAMBIO_ENLACE_MINISTERIO) Then
'                Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'            Else
'                Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'            End If
'            Me.strPrimerEnlaceWebServiceMinisterio = ConfigurationManager.AppSettings("ManifiestoElectronico.IBPMServicesservice").ToString()
'            Me.strSegundoEnlaceWebServiceMinisterio = ConfigurationManager.AppSettings("ManifiestoElectronico.IBPMServicesservice2").ToString()
'            Me.lonCantidadRepeticionesProceso = General.CERO
'            Me.dblCantidadSegundosPrimerReporte = General.CERO

'            Me.strPeriodoConsultaSiceTac = ConfigurationManager.AppSettings("PeriodoConsultaSICETAC").ToString()
'        End Sub
'#End Region

'#Region "Enumeradores"
'        Public Enum EnumEstadoReporteDocumentos As Integer
'            REPORTADO = 1
'            NOREPORTADO = 2
'        End Enum
'        Public Enum EnumTipoDocumentoGenera As Integer
'            REMESA = 1
'            MANIFIESTO = 2
'            CUMPLIRREMESA = 3
'            CUMPLIRMANIFIESTO = 4
'        End Enum
'        Public Enum EnumTipoReporteDocumento As Integer
'            DESDE_CREACION_DOCUMENTO = 1
'            REPORTE_DOCUMENTO_MINISTERIO = 2
'        End Enum
'        Public Enum EnumEnlaceServicioMinisterio As Integer
'            PRIMER_ENLACE_MINISTERIO_TRANSPORTE = 1
'            SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE = 2
'        End Enum
'#End Region

'#Region "Atributos"

'#End Region

'#Region "Funciones Privadas"
'        Private Sub Retornar_Remesas(ByVal lonNumeroManifiesto As Long, ByRef arrRemesas As String())
'            Try
'                REM: CONSULTA LOS NUMEROS DE LAS REMESAS PARA ASOCIARLAS AL MANIFIESTO EN EL MINISTERIO DE TRANSPORTE
'                Dim dtsDetalleManifiesto As New DataSet
'                Dim ComandoSQL As SqlCommand
'                Dim intCont As Short = 1

'                strSQL = "SELECT ENRE.Numero_Documento"
'                strSQL += " FROM Detalle_Manifiestos DEMA, Encabezado_Remesas ENRE"
'                strSQL += " WHERE DEMA.EMPR_Codigo = ENRE.EMPR_Codigo"
'                strSQL += " AND DEMA.ENRE_Numero = ENRE.Numero"
'                strSQL += " AND DEMA.EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND DEMA.ENMA_Numero = " & lonNumeroManifiesto
'                strSQL += "AND ENRE.TIRE_Codigo <> " & TIPO_REMESA_URBANA

'                strSQL += " GROUP BY ENRE.Numero_Documento"

'                ComandoSQL = New SqlCommand(strSQL, objGeneral.ConexionSQL)
'                objGeneral.ConexionSQL.Open()
'                dtsDetalleManifiesto = objGeneral.Retorna_Dataset(strSQL)

'                ReDim arrRemesas(dtsDetalleManifiesto.Tables(Lista.PRIMERA_TABLA).Rows.Count - 1)

'                For intCont = 0 To dtsDetalleManifiesto.Tables(Lista.PRIMERA_TABLA).Rows.Count - 1
'                    arrRemesas(intCont) = dtsDetalleManifiesto.Tables(Lista.PRIMERA_TABLA).Rows(intCont).Item("Numero_Documento").ToString()
'                Next

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Sub

'        Private Function Tercero_Reportado(ByVal CodigoTercero As Long) As Boolean
'            Dim objGeneralAuxiliar As New General
'            Try
'                strSQL = "UPDATE Terceros SET Reportado=1"
'                strSQL += " WHERE EMPR_Codigo =" & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo =" & CodigoTercero

'                strError = ""

'                If objGeneralAuxiliar.Ejecutar_SQL(strSQL, Me.strError) Then
'                    Tercero_Reportado = True
'                Else
'                    Tercero_Reportado = False
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                Tercero_Reportado = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Vehiculo_Reportado(ByVal Placa As String) As Boolean
'            Dim objGeneralAuxiliar As New General
'            Try
'                strSQL = "UPDATE Vehiculos SET Reportado=1"
'                strSQL += " WHERE EMPR_Codigo =" & Me.objEmpresa.Codigo
'                strSQL += " AND Placa ='" & Placa & "'"

'                strError = ""

'                If objGeneralAuxiliar.Ejecutar_SQL(strSQL, Me.strError) Then
'                    Vehiculo_Reportado = True
'                Else
'                    Vehiculo_Reportado = False
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                Vehiculo_Reportado = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Remolque_Reportado(ByVal Placa As String) As Boolean
'            Dim objGeneralAuxiliar As New General
'            Try
'                strSQL = "UPDATE Semiremolques SET Reportado=1"
'                strSQL += " WHERE EMPR_Codigo =" & Me.objEmpresa.Codigo
'                strSQL += " AND Placa ='" & Placa & "'"

'                strError = ""

'                If objGeneralAuxiliar.Ejecutar_SQL(strSQL, Me.strError) Then
'                    Remolque_Reportado = True
'                Else
'                    Remolque_Reportado = False
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                Remolque_Reportado = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function
'#End Region

'#Region "Funcion General de envio de Documnentos"

'        ''' <summary>
'        ''' Controla demora de proceso
'        ''' </summary>
'        ''' <param name="lonNumeroDocumento">Numero Documento</param>
'        ''' <remarks></remarks>
'        Public Sub Control_Proceso_Reporte_Informacion(ByVal lonNumeroDocumento As Long)
'            Try
'                Select Case TipoDocumento
'                    Case EnumTipoDocumentoGenera.REMESA
'                        Me.thrEnvioMinisterio = New Thread(AddressOf Proceso_Reportar_Remesa)
'                        Me.thrEnvioMinisterio.Start(lonNumeroDocumento)
'                        Me.lonTiempoSegundos = ConfigurationManager.AppSettings("TiempoMaximoEnvioSegundosRemesas").ToString()
'                    Case EnumTipoDocumentoGenera.MANIFIESTO
'                        Me.thrEnvioMinisterio = New Thread(AddressOf Proceso_Reportar_Manifiesto)
'                        Me.thrEnvioMinisterio.Start(lonNumeroDocumento)
'                        Me.lonTiempoSegundos = ConfigurationManager.AppSettings("TiempoMaximoEnvioSegundosManifiesto").ToString()
'                End Select

'                Dim bolCierreFormazadoDeEnvio As Boolean = False
'                Dim bolNoCierraProceso As Boolean = True
'                Dim lonSegundoConteo As Long = General.CERO

'                While bolNoCierraProceso

'                    If Me.thrEnvioMinisterio.ThreadState = ThreadState.Running Or Me.thrEnvioMinisterio.ThreadState = ThreadState.WaitSleepJoin Then
'                        System.Threading.Thread.Sleep(TIEMPO_DEMORA_UN_SEGUNDO)
'                        lonSegundoConteo += 1
'                        bolNoCierraProceso = lonSegundoConteo <= Me.lonTiempoSegundos
'                        If Not bolNoCierraProceso Then
'                            Me.thrEnvioMinisterio.Abort()
'                            Select Case TipoDocumento
'                                Case EnumTipoDocumentoGenera.REMESA
'                                    Call Actualizar_Numero_Confirmacion_Remesa_Ministerio(lonNumeroDocumento, "El envío tardo mas de lo esperado, el proceso se ha cancelado.")
'                                Case EnumTipoDocumentoGenera.MANIFIESTO
'                                    Call Actualizar_Numero_Confirmacion_Manifiesto_Ministerio(lonNumeroDocumento, "El envío tardo mas de lo esperado, el proceso se ha cancelado.")
'                            End Select
'                            bolCierreFormazadoDeEnvio = True
'                        End If
'                    Else
'                        bolNoCierraProceso = False
'                    End If

'                End While

'                If bolCierreFormazadoDeEnvio Then

'                    If Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE Then
'                        Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                    Else
'                        Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                    End If
'                    Select Case TipoDocumento
'                        Case EnumTipoDocumentoGenera.REMESA
'                            Me.thrEnvioMinisterio = New Thread(AddressOf Proceso_Reportar_Remesa)
'                            Me.thrEnvioMinisterio.Start(lonNumeroDocumento)
'                            Me.lonTiempoSegundos = ConfigurationManager.AppSettings("TiempoMaximoEnvioSegundosRemesas").ToString()
'                        Case EnumTipoDocumentoGenera.MANIFIESTO
'                            Me.thrEnvioMinisterio = New Thread(AddressOf Proceso_Reportar_Manifiesto)
'                            Me.thrEnvioMinisterio.Start(lonNumeroDocumento)
'                            Me.lonTiempoSegundos = ConfigurationManager.AppSettings("TiempoMaximoEnvioSegundosManifiesto").ToString()
'                    End Select
'                    bolNoCierraProceso = True
'                    lonSegundoConteo = General.CERO
'                    While bolNoCierraProceso

'                        If Me.thrEnvioMinisterio.ThreadState = ThreadState.Running Or Me.thrEnvioMinisterio.ThreadState = ThreadState.WaitSleepJoin Then
'                            System.Threading.Thread.Sleep(TIEMPO_DEMORA_UN_SEGUNDO)
'                            lonSegundoConteo += 1
'                            bolNoCierraProceso = lonSegundoConteo <= Me.lonTiempoSegundos
'                            If Not bolNoCierraProceso Then
'                                Me.thrEnvioMinisterio.Abort()
'                                Select Case TipoDocumento
'                                    Case EnumTipoDocumentoGenera.REMESA
'                                        Call Actualizar_Numero_Confirmacion_Remesa_Ministerio(lonNumeroDocumento, "El envío tardo más de lo esperado, el proceso se ha cancelado.")
'                                    Case EnumTipoDocumentoGenera.MANIFIESTO
'                                        Call Actualizar_Numero_Confirmacion_Manifiesto_Ministerio(lonNumeroDocumento, "El envío tardo más de lo esperado, el proceso se ha cancelado.")
'                                End Select
'                            End If
'                        Else
'                            bolNoCierraProceso = False
'                        End If

'                    End While
'                    Actualiza_Validacion_Cambio_Enlace_Ministerio()

'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Sub

'        ''' <summary>
'        ''' Realiza Actualizacion a validacion para indicar el enlace a utilizar para reportar informacion
'        ''' </summary>
'        ''' <returns>Boolean</returns>
'        ''' <remarks></remarks>
'        Public Function Actualiza_Validacion_Cambio_Enlace_Ministerio() As Boolean
'            Try

'                If Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE Then
'                    Me.strSQL = "UPDATE VALIDACIONES SET Estado_Validacion = 0 WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo & " AND Validacion = '" & VALIDACION_CAMBIO_ENLACE_MINISTERIO & "'"
'                Else
'                    Me.strSQL = "UPDATE VALIDACIONES SET Estado_Validacion = 1 WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo & " AND Validacion = '" & VALIDACION_CAMBIO_ENLACE_MINISTERIO & "'"
'                End If
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.Ejecutar_SQL_Misma_Transaccion(Me.strSQL, Me.strError)
'                    Actualiza_Validacion_Cambio_Enlace_Ministerio = True
'                Else
'                    Me.objGeneral.Ejecutar_SQL(Me.strSQL, Me.strError)
'                    Actualiza_Validacion_Cambio_Enlace_Ministerio = True
'                End If

'            Catch ex As Exception
'                Actualiza_Validacion_Cambio_Enlace_Ministerio = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'#End Region

'#Region "Envío de Remesas"

'        ''' <summary>
'        ''' Realiza envio de Remesas al Ministerio de Transporte de Acuerdo a las configuraciones dadas.
'        ''' </summary>
'        ''' <param name="lonNumeroRemesa">Numero Interno de la Remesa</param>
'        ''' <returns>Boolean</returns>
'        ''' <remarks></remarks>
'        Public Function Reportar_Remesa(ByVal lonNumeroRemesa As Long) As Boolean
'            Try
'                Me.lonTiempoSegundos = ConfigurationManager.AppSettings("TiempoMaximoEnvioSegundosRemesas").ToString()
'                If Me.intActivaEnvioMultiProceso = General.SELECCIONO_SI Then
'                    Me.TipoDocumento = EnumTipoDocumentoGenera.REMESA
'                    Me.thrControProceso = New Thread(AddressOf Control_Proceso_Reporte_Informacion)
'                    Me.thrControProceso.Start(lonNumeroRemesa)
'                    Reportar_Remesa = True
'                Else
'                    Dim dteFechaInicial As Date = Date.Now
'                    If Me.lonCantidadRepeticionesProceso = General.CERO Then
'                        enumTemporalEnlacesMinisterio = enumEnlacesMinisterio
'                        Proceso_Reportar_Remesa(lonNumeroRemesa)
'                        Me.dblCantidadSegundosPrimerReporte = DateDiff(DateInterval.Second, dteFechaInicial, Date.Now)

'                        If (Me.lonTiempoSegundos * 2) < Me.dblCantidadSegundosPrimerReporte And Me.lonCodigoConfirmacion = General.CERO Then

'                            If Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE Then
'                                Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                            Else
'                                Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                            End If
'                            Proceso_Reportar_Remesa(lonNumeroRemesa)

'                            If (Me.lonTiempoSegundos * 2) < Me.dblCantidadSegundosPrimerReporte And Me.lonCodigoConfirmacion = General.CERO Then

'                                If Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE Then
'                                    Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                                Else
'                                    Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                                End If
'                                dteFechaInicial = Date.Now
'                                Proceso_Reportar_Remesa(lonNumeroRemesa)

'                                If (Me.lonTiempoSegundos * 2) > DateDiff(DateInterval.Second, dteFechaInicial, Date.Now) And Me.lonCodigoConfirmacion <> General.CERO Then
'                                    If Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE Then
'                                        Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                                    Else
'                                        Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                                    End If
'                                    Actualiza_Validacion_Cambio_Enlace_Ministerio()
'                                End If

'                            End If

'                        End If
'                    Else
'                        If Me.lonCantidadRepeticionesProceso = General.UNO Then
'                            If enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE Then
'                                enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                            Else
'                                enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                            End If
'                            Proceso_Reportar_Remesa(lonNumeroRemesa)
'                            If Me.dblCantidadSegundosPrimerReporte > DateDiff(DateInterval.Second, dteFechaInicial, Date.Now) Then
'                                Actualiza_Validacion_Cambio_Enlace_Ministerio()
'                            Else
'                                enumEnlacesMinisterio = enumTemporalEnlacesMinisterio
'                            End If
'                        Else
'                            Proceso_Reportar_Remesa(lonNumeroRemesa)
'                        End If
'                    End If
'                    Me.lonCantidadRepeticionesProceso += General.UNO
'                    Reportar_Remesa = True
'                End If
'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                strMensaje &= "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                Reportar_Remesa = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        ''' <summary>
'        ''' Realiza envio de informacion al Ministerio de Transporte en un proceso continuo o multi proceso de acuerdo a como se le indique.
'        ''' </summary>
'        ''' <param name="lonNumeroRemesa">Numero Interno de la remesa</param>
'        ''' <remarks></remarks>
'        Private Sub Proceso_Reportar_Remesa(ByVal lonNumeroRemesa As Long)
'            Try
'                Dim objRemesa As New RemesaRNDC(acceso, solicitudConsulta)
'                Dim objRemitente As New TerceroRNDC(acceso, solicitudConsulta)
'                Dim objDestinatario As New TerceroRNDC(acceso, solicitudConsulta)
'                Dim dteAxiliar As Date = Date.MinValue
'                Dim lonCodigoRemitente As Long = 0
'                Dim bolReporto As Boolean = False
'                Dim bolExisteDatos As Boolean = False
'                Dim sdrRemesa As SqlDataReader
'                strMensaje = String.Empty

'                Me.objGeneral.ComandoSQL = New SqlCommand
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
'                Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.CadenaDeConexionSQL)

'                Me.dtsRemesa = New DataSet

'                strSQL = "SELECT *"
'                strSQL += " FROM V_Informacion_Carga"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & lonNumeroRemesa

'                If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
'                    Me.objGeneral.ComandoSQL.Connection.Open()
'                End If
'                Me.objGeneral.ComandoSQL.CommandText = strSQL
'                sdrRemesa = Me.objGeneral.ComandoSQL.ExecuteReader

'                If sdrRemesa.Read Then

'                    lonCodigoRemitente = sdrRemesa("CodigoRemitente")
'                    REM: Cargar Informacion de destinatario
'                    objDestinatario.Codigo = sdrRemesa("CodigoDestinatario")
'                    objDestinatario.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion
'                    objDestinatario.CODTIPOIDTERCERO = sdrRemesa("TipoIdDestinatario")
'                    If Val(sdrRemesa("IdentificacionDestinatario")) = General.CERO Or Len(sdrRemesa("IdentificacionDestinatario")) < 7 Then
'                        objDestinatario.NUMIDTERCERO = TerceroRNDC.TELEFONO_X_DEFECTO
'                    Else
'                        objDestinatario.NUMIDTERCERO = Replace(sdrRemesa("IdentificacionDestinatario"), " ", "")
'                        objDestinatario.NUMIDTERCERO = Mid(objDestinatario.NUMIDTERCERO, General.UNO, TerceroRNDC.DIGITOS_TELEFONO)
'                    End If
'                    objDestinatario.NOMIDTERCERO = sdrRemesa("NombreDestinatario")
'                    objDestinatario.PRIMERAPELLIDOIDTERCERO = Mid(sdrRemesa("NombreDestinatario"), General.UNO, 22)
'                    If Val(sdrRemesa("TelefonoDestinatario")) = General.CERO Or Len(sdrRemesa("TelefonoDestinatario")) < TerceroRNDC.DIGITOS_TELEFONO Then
'                        objDestinatario.NUMTELEFONOCONTACTO = TerceroRNDC.TELEFONO_X_DEFECTO
'                    Else
'                        objDestinatario.NUMTELEFONOCONTACTO = Replace(sdrRemesa("TelefonoDestinatario"), " ", "")
'                        objDestinatario.NUMTELEFONOCONTACTO = Mid(objDestinatario.NUMTELEFONOCONTACTO, General.UNO, TerceroRNDC.DIGITOS_TELEFONO)
'                    End If
'                    If sdrRemesa("DireccionDestinatario") = String.Empty Then
'                        objDestinatario.NOMENCLATURADIRECCION = TerceroRNDC.DIRECCION_X_DEFECTO
'                    Else
'                        If Len(sdrRemesa("DireccionDestinatario")) > 9 Then
'                            objDestinatario.NOMENCLATURADIRECCION = Replace(sdrRemesa("DireccionDestinatario"), """", "")
'                        Else
'                            objDestinatario.NOMENCLATURADIRECCION = Replace(sdrRemesa("DireccionDestinatario"), """", "") & TerceroRNDC.DIRECCION_X_DEFECTO
'                        End If
'                    End If
'                    objDestinatario.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(sdrRemesa("CIUD_Destino"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                    objDestinatario.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(objDestinatario.CODMUNICIPIORNDC, 8, General.CAMPO_CEROS_DERECHA)
'                    objDestinatario.CODSEDETERCERO = objGeneral.FormCampMiniTran(Mid(sdrRemesa("CIUD_Destino"), 1, 6), 5, General.CAMPO_CEROS_IZQUIERDA)



'                    REM: Cargar informacion de Remitente
'                    objRemitente.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion
'                    objRemitente.Codigo = sdrRemesa("CodigoRemitente")
'                    objRemitente.CODTIPOIDTERCERO = sdrRemesa("TipoIdRemitente").trim
'                    objRemitente.NUMIDTERCERO = sdrRemesa("IdentificacionRemitente").trim
'                    objRemitente.NOMIDTERCERO = sdrRemesa("NombreRemitente").trim
'                    If sdrRemesa("PrimerApellidoRemitente") = String.Empty Then
'                        objRemitente.PRIMERAPELLIDOIDTERCERO = Mid(sdrRemesa("NombreRemitente"), General.UNO, 22)
'                    Else
'                        objRemitente.PRIMERAPELLIDOIDTERCERO = Mid(sdrRemesa("PrimerApellidoRemitente"), General.UNO, 22)
'                    End If
'                    objRemitente.SEGUNDOAPELLIDOIDTERCERO = sdrRemesa("SegundoApellidoRemitente")
'                    If Val(sdrRemesa("TelefonoRemitente")) = General.CERO Or Len(sdrRemesa("TelefonoRemitente")) < TerceroRNDC.DIGITOS_TELEFONO Then
'                        objRemitente.NUMTELEFONOCONTACTO = TerceroRNDC.TELEFONO_X_DEFECTO
'                    Else
'                        objRemitente.NUMTELEFONOCONTACTO = Replace(sdrRemesa("TelefonoRemitente"), " ", "")
'                        objRemitente.NUMTELEFONOCONTACTO = Mid(objRemitente.NUMTELEFONOCONTACTO, General.UNO, TerceroRNDC.DIGITOS_TELEFONO)
'                    End If
'                    If sdrRemesa("DireccionRemitente") = String.Empty Then
'                        objRemitente.NOMENCLATURADIRECCION = TerceroRNDC.DIRECCION_X_DEFECTO
'                    Else
'                        objRemitente.NOMENCLATURADIRECCION = sdrRemesa("DireccionRemitente")
'                    End If
'                    objRemitente.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(sdrRemesa("CIUD_Origen"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                    objRemitente.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(objRemitente.CODMUNICIPIORNDC, 8, General.CAMPO_CEROS_DERECHA)
'                    objRemitente.CODSEDETERCERO = objGeneral.FormCampMiniTran(Mid(sdrRemesa("CIUD_Destino"), 1, 6), 5, General.CAMPO_CEROS_IZQUIERDA)
'                    objRemitente.NOMSEDETERCERO = sdrRemesa("NombreSedeRemitente")



'                    REM: Cargar informacion de remesa
'                    objRemesa.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion
'                    objRemesa.CONSECUTIVOREMESA = sdrRemesa("Numero_Documento")
'                    'objRemesa.CONSECUTIVOINFORMACIONCARGA = objEmpresa.CodigoMinisterio & sdrRemesa("Numero")
'                    objRemesa.CONSECUTIVOCARGADIVIDIDA = ""
'                    objRemesa.CODOPERACIONTRANSPORTE = sdrRemesa("TipoRemesa")
'                    objRemesa.CODNATURALEZACARGA = sdrRemesa("CodigoNaturalezaProducto")
'                    If Val(sdrRemesa("Cantidad")) > 0 Then
'                        objRemesa.CANTIDADCARGADA = Val(sdrRemesa("Cantidad"))
'                    Else
'                        objRemesa.CANTIDADCARGADA = General.UNO
'                    End If
'                    objRemesa.UNIDADMEDIDACAPACIDAD = sdrRemesa("CodigoUnidadMedida")
'                    objRemesa.CODTIPOEMPAQUE = sdrRemesa("UnidadEmpaque")
'                    objRemesa.PESOCONTENEDORVACIO = Val(sdrRemesa("PesoContenedor"))
'                    If sdrRemesa("CodigoProducto") Is DBNull.Value Then
'                        strMensaje = "Debe revisar el codigo del producto, en el ministerio." & sdrRemesa("NombreProducto")
'                        objRemesa.MERCANCIAREMESA = sdrRemesa("CodigoProducto")
'                    End If
'                    objRemesa.MERCANCIAREMESA = sdrRemesa("CodigoProducto")
'                    objRemesa.DESCRIPCIONCORTAPRODUCTO = Replace(sdrRemesa("NombreProducto"), Chr(34), "")
'                    objRemesa.CODTIPOIDREMITENTE = sdrRemesa("TipoIdRemitente")
'                    objRemesa.NUMIDREMITENTE = sdrRemesa("IdentificacionRemitente")
'                    objRemesa.CODSEDEREMITENTE = objRemitente.CODSEDETERCERO
'                    objRemesa.CODTIPOIDDESTINATARIO = sdrRemesa("TipoIdDestinatario")
'                    If Val(sdrRemesa("IdentificacionDestinatario")) = General.CERO Or Len(sdrRemesa("IdentificacionDestinatario")) < 7 Then
'                        objRemesa.NUMIDDESTINATARIO = TerceroRNDC.TELEFONO_X_DEFECTO
'                    Else
'                        objRemesa.NUMIDDESTINATARIO = Replace(sdrRemesa("IdentificacionDestinatario"), " ", "")
'                        objRemesa.NUMIDDESTINATARIO = Mid(objRemesa.NUMIDDESTINATARIO, General.UNO, TerceroRNDC.DIGITOS_TELEFONO)
'                    End If
'                    objRemesa.CODSEDEDESTINATARIO = objGeneral.FormCampMiniTran(Mid(sdrRemesa("CIUD_Destino"), 1, 6), 5, General.CAMPO_CEROS_IZQUIERDA)
'                    objRemesa.DUENOPOLIZA = Replace(sdrRemesa("DuenoPolliza"), Chr(13), "")
'                    objRemesa.NUMPOLIZATRANSPORTE = sdrRemesa("Numero_Poliza")
'                    objRemesa.COMPANIASEGURO = sdrRemesa("IdentificacionAseguradora")
'                    objRemesa.FECHAVENCIMIENTOPOLIZACARGA = sdrRemesa("Fecha_Poliza")
'                    objRemesa.FECHALLEGADACARGUE = ""
'                    objRemesa.HORALLEGADACARGUEREMESA = ""
'                    objRemesa.FECHAENTRADACARGUE = ""
'                    objRemesa.HORAENTRADACARGUEREMESA = ""
'                    objRemesa.FECHASALIDACARGUE = ""
'                    objRemesa.HORASALIDACARGUEREMESA = ""



'                    Dim dteAxiliarInicial As Date = Date.Now
'                    Dim dteAxiliarFinal As Date = Date.Now
'                    Date.TryParse(sdrRemesa("Fecha_Inicio_Cargue"), dteAxiliarInicial)
'                    Date.TryParse(sdrRemesa("Fecha_Inicio_Descargue"), dteAxiliarFinal)
'                    If Ambiente = Ambiantes_Desarrollos.LOCAL Then
'                        Dim dteAuxiliaFechaRemesa As Date
'                        Date.TryParse(sdrRemesa("Fecha_Inicio_Cargue"), dteAuxiliaFechaRemesa)
'                        If dteAxiliarInicial < dteAuxiliaFechaRemesa Then
'                            Date.TryParse(dteAuxiliaFechaRemesa, dteAxiliarInicial)
'                        End If
'                        If dteAxiliarFinal < dteAuxiliaFechaRemesa Then
'                            Date.TryParse(dteAuxiliaFechaRemesa, dteAxiliarFinal)
'                        End If
'                    End If
'                    If dteAxiliarInicial > dteAxiliarFinal Or dteAxiliarInicial = dteAxiliarFinal Or (dteAxiliarInicial = Date.MinValue Or dteAxiliarFinal = Date.MinValue) Then
'                        If dteAxiliarInicial > dteAxiliarFinal Then
'                            Dim auxiliarCambio As Date = Date.Now
'                            auxiliarCambio = dteAxiliarInicial
'                            dteAxiliarInicial = dteAxiliarFinal
'                            dteAxiliarFinal = auxiliarCambio
'                        ElseIf dteAxiliarInicial = dteAxiliarFinal Or (dteAxiliarInicial.Date = Date.MinValue.Date Or dteAxiliarFinal.Date = Date.MinValue.Date) Then
'                            dteAxiliarInicial = DateAdd(DateInterval.Day, -3, Date.Parse(Date.Parse(sdrRemesa("FechaRemesa"))))
'                            dteAxiliarFinal = DateAdd(DateInterval.Day, -1, Date.Parse(Date.Parse(sdrRemesa("FechaRemesa"))))
'                        End If
'                    End If
'                    objRemesa.FECHACITAPACTADACARGUE = dteAxiliarInicial.Date
'                    objRemesa.HORACITAPACTADACARGUE = dteAxiliarInicial.ToString("HH:mm")
'                    objRemesa.FECHACITAPACTADADESCARGUE = dteAxiliarFinal.Date
'                    objRemesa.HORACITAPACTADADESCARGUEREMESA = dteAxiliarFinal.ToString("HH:mm")
'                    objRemesa.HORASPACTODESCARGUE = sdrRemesa("HorasPactoDescarga")
'                    objRemesa.HORASPACTOCARGA = sdrRemesa("HorasPactoCargue")
'                    objRemesa.MINUTOSPACTOCARGA = sdrRemesa("MinutosPactoCargue")
'                    objRemesa.CODTIPOIDPROPIETARIO = sdrRemesa("TipoIdRemitente")
'                    objRemesa.NUMIDPROPIETARIO = sdrRemesa("IdentificacionRemitente")
'                    objRemesa.CODSEDEPROPIETARIO = objRemitente.CODSEDETERCERO  'objGeneral.FormCampMiniTran(sdrRemesa("CIUD_Origen"), 5, General.CAMPO_CEROS_IZQUIERDA)


'                    REM: Variables para almacenar Errores y Listas de variables de consultas
'                    Dim strErroresRemitente As String = String.Empty
'                    Dim strErroresDestinatario As String = String.Empty
'                    Dim strErroresRemesa As String = String.Empty
'                    Dim lstVariablesRemitente As New List(Of String)
'                    Dim dblRemitente As Double = General.CERO
'                    Dim lstVariablesDestinatario As New List(Of String)
'                    Dim dblDestinatario As Double = General.CERO
'                    Dim lstVariablesRemesas As New List(Of String)
'                    Dim dblRemesa As Double = General.CERO

'                    REM: Campos de consulta para el remitente
'                    lstVariablesRemitente.Add("CODMUNICIPIORNDC".ToLower)
'                    lstVariablesRemitente.Add("CODSEDETERCERO".ToLower)
'                    lstVariablesRemitente.Add("NOMENCLATURADIRECCION".ToLower)

'                    REM: Campos de consulta para el Destinatario
'                    lstVariablesDestinatario.Add("CODMUNICIPIORNDC".ToLower)
'                    lstVariablesDestinatario.Add("CODSEDETERCERO".ToLower)
'                    lstVariablesDestinatario.Add("NOMENCLATURADIRECCION".ToLower)

'                    REM: realiza Cambio de Enlace para reportar información al Ministerio
'                    Select Case enumEnlacesMinisterio
'                        Case EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                            objRemitente.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                            objDestinatario.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                            objRemesa.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                        Case EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                            objRemitente.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                            objDestinatario.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                            objRemesa.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                    End Select

'                    REM: Ingresa el remitente como tercero en el ministerio
'                    strErroresRemitente = String.Empty
'                    dblRemitente = objRemitente.Reportar_Tercero(strTextoXmlRemitente, Tipos_Procesos.FORMATO_INGRESO, strErroresRemitente, lstVariablesRemitente, Tipos_Roles.REMITENTE)
'                    bolReporto = False
'                    If (dblRemitente > General.CERO And strErroresRemitente = String.Empty) Or strErroresRemitente.Contains(strTercDupl) Then
'                        If dblRemitente = General.CERO Then Double.TryParse(Val(strErroresRemitente.Split(" ")(0).Split(":")(1)), dblRemitente)
'                        objRemitente.Guardar_Tercero_Confirmacion(dblRemitente, objEmpresa.Codigo, Me.objUsuario.Codigo, strErroresRemitente)
'                        bolReporto = True
'                    End If


'                    REM: Si el remitente se reporto correctamente la variable bolReporto llega en TRUE de lo contrario retorn error
'                    If bolReporto = True Then

'                        REM: Ingresa el Destinatario como tercero en el ministerio
'                        strErroresDestinatario = String.Empty
'                        dblDestinatario = objDestinatario.Reportar_Tercero(strTextoXmlDestinatario, Tipos_Procesos.FORMATO_INGRESO, strErroresDestinatario, lstVariablesDestinatario, Tipos_Roles.DESTINATARIO)
'                        bolReporto = False
'                        If (dblDestinatario > General.CERO And strErroresDestinatario = String.Empty) Or strErroresDestinatario.Contains(strTercDupl) Then
'                            If dblDestinatario = General.CERO Then Double.TryParse(Val(strErroresDestinatario.Split(" ")(0).Split(":")(1)), dblDestinatario)
'                            objDestinatario.Guardar_Tercero_Confirmacion(dblDestinatario, objEmpresa.Codigo, Me.objUsuario.Codigo, strErroresDestinatario)
'                            bolReporto = True
'                        End If

'                        REM: si el destinatario se reporto correctamente la variable bolReporto llegara en TRUE
'                        If bolReporto Then
'                            strErroresRemesa = String.Empty
'                            REM: envia la remesa para reportarla
'                            dblRemesa = objRemesa.Reportar_Remesa(strTextXmlRemesa, Tipos_Procesos.FORMATO_INGRESO, strErroresRemesa, lstVariablesRemesas)
'                            If (dblRemesa > General.CERO And strErroresRemesa = String.Empty) Or strErroresRemesa.Contains(strRemeDupl) Then
'                                If strErroresRemesa.Contains(strRemeDupl) Then Double.TryParse(strErroresRemesa.Split(" ")(0).Split(":")(1), dblRemesa)
'                                lonCodigoConfirmacion = dblRemesa
'                                strMensaje = lonCodigoConfirmacion
'                                intEstadoReporteDocumento = EnumEstadoReporteDocumentos.REPORTADO
'                            Else
'                                REM: se le asigna un valor cero a la function Reportar_Remesa y asiga el mensaje de error
'                                strMensaje = strErroresRemesa
'                                lonCodigoConfirmacion = General.CERO
'                                intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                            End If
'                        Else
'                            strMensaje = "Destinatario: " & strErroresDestinatario
'                            lonCodigoConfirmacion = General.CERO
'                            strTextXmlRemesa = "Primero debe reportar el destinatario."
'                            intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                        End If
'                    Else
'                        strTextoXmlRemitente =
'                    lonCodigoConfirmacion = General.CERO
'                        strMensaje = "Remitente: " & strErroresRemitente
'                        strTextoXmlDestinatario = "Primero debe reportar el remitente."
'                        strTextXmlRemesa = "Primero debe reportar el remitente y el destinatario."
'                        intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                    End If
'                Else
'                    lonCodigoConfirmacion = General.CERO
'                    strMensaje = "GESTRANS: No existe información con este número de remesa."
'                    intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                End If
'            Catch ex As Exception
'                intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                lonCodigoConfirmacion = General.CERO
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                Call Actualizar_Numero_Confirmacion_Remesa_Ministerio(lonNumeroRemesa, strMensaje)
'            End Try
'        End Sub

'        ''' <summary>
'        ''' Actualiza el numero de confirmacion o el error presentado al momento de reportar documentos al MInisterio de transporte.
'        ''' </summary>
'        ''' <param name="lonNumero">Codigo interno de la Remesa</param>
'        ''' <param name="strMensaje">Mensaje de error/Numero confirmacion Ministerio</param>
'        ''' <returns>Boolean</returns>
'        ''' <remarks></remarks>
'        Public Function Actualizar_Numero_Confirmacion_Remesa_Ministerio(ByVal lonNumero As Long, ByVal strMensaje As String) As Boolean
'            Try
'                If IsNumeric(strMensaje) Then
'                    Me.strSQL = "UPDATE Encabezado_Remesas SET Numero_Confirmacion_Ministerio = '" & Val(strMensaje) & "'"
'                    Me.strSQL += ", Mensaje_Error_Reporte_Ministerio = '" & Mid(strMensaje, 1, 500) & "'"
'                    Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    Me.strSQL += " AND Numero = " & lonNumero
'                Else
'                    Me.strSQL = "UPDATE Encabezado_Remesas SET Numero_Confirmacion_Ministerio = '" & Val(0) & "'"
'                    Me.strSQL += ", Mensaje_Error_Reporte_Ministerio = '" & Mid(strMensaje, 1, 500) & "'"
'                    Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    Me.strSQL += " AND Numero = " & lonNumero
'                End If

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.Ejecutar_SQL_Misma_Transaccion(Me.strSQL, Me.strError)
'                    Actualizar_Numero_Confirmacion_Remesa_Ministerio = True
'                Else
'                    Me.objGeneral.Ejecutar_SQL(Me.strSQL, Me.strError)
'                    Actualizar_Numero_Confirmacion_Remesa_Ministerio = True
'                End If

'            Catch ex As Exception
'                Actualizar_Numero_Confirmacion_Remesa_Ministerio = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'#End Region

'#Region "Envío de Manifiestos"

'        Public Function Verificar_Acceso_Enlace_Uno() As Boolean
'            Dim Peticion As System.Net.WebRequest
'            Dim Respuesta As System.Net.HttpWebResponse
'            Try
'                Peticion = System.Net.WebRequest.Create(ConfigurationManager.AppSettings("ManifiestoElectronico.IBPMServicesservice").ToString())
'                Peticion.Timeout = Val(ConfigurationManager.AppSettings("TimeOutEsperaRNDC").ToString())
'                Respuesta = Peticion.GetResponse()
'                Verificar_Acceso_Enlace_Uno = True
'            Catch ex As System.Net.WebException
'                If ex.Status = Net.WebExceptionStatus.NameResolutionFailure Then
'                    Verificar_Acceso_Enlace_Uno = False
'                End If
'                If ex.Status = Net.WebExceptionStatus.Timeout Then
'                    Verificar_Acceso_Enlace_Uno = False
'                End If
'            End Try
'        End Function

'        Public Function Verificar_Acceso_Enlace_Dos() As Boolean
'            Dim Peticion As System.Net.WebRequest
'            Dim Respuesta As System.Net.HttpWebResponse
'            Try
'                Peticion = System.Net.WebRequest.Create(ConfigurationManager.AppSettings("ManifiestoElectronico.IBPMServicesservice2").ToString())
'                Peticion.Timeout = Val(ConfigurationManager.AppSettings("TimeOutEsperaRNDC").ToString())
'                Respuesta = Peticion.GetResponse()
'                Verificar_Acceso_Enlace_Dos = True
'            Catch ex As System.Net.WebException
'                If ex.Status = Net.WebExceptionStatus.NameResolutionFailure Then
'                    Verificar_Acceso_Enlace_Dos = False
'                End If
'                If ex.Status = Net.WebExceptionStatus.Timeout Then
'                    Verificar_Acceso_Enlace_Dos = False
'                End If
'            End Try
'        End Function


'        ''' <summary>
'        ''' Realiza envio de Manifiesto al Ministerio de Transporte de Acuerdo a las configuraciones dadas.
'        ''' </summary>
'        ''' <param name="lonNumeroManifiesto">Numero del manifiesto</param>
'        ''' <returns>Boolean</returns>
'        ''' <remarks></remarks>
'        ''' 
'        Public Function Reportar_Manifiesto(ByVal lonNumeroManifiesto As Long) As Boolean
'            Try
'                'Me.lonTiempoSegundos = ConfigurationManager.AppSettings("TiempoMaximoEnvioSegundosManifiesto").ToString()
'                Dim bolAccesoDisponible As Boolean
'                If Me.intActivaEnvioMultiProceso = General.SELECCIONO_SI Then
'                    Me.TipoDocumento = EnumTipoDocumentoGenera.MANIFIESTO
'                    Me.thrControProceso = New Thread(AddressOf Control_Proceso_Reporte_Informacion)
'                    Me.thrControProceso.Start(lonNumeroManifiesto)
'                    Reportar_Manifiesto = True
'                Else
'                    If Verificar_Acceso_Enlace_Uno() Then
'                        Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                        bolAccesoDisponible = True
'                    ElseIf Verificar_Acceso_Enlace_Dos() Then
'                        Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                        bolAccesoDisponible = True
'                    Else
'                        bolAccesoDisponible = False
'                    End If
'                    Actualiza_Validacion_Cambio_Enlace_Ministerio()

'                    If bolAccesoDisponible Then
'                        Proceso_Reportar_Manifiesto(lonNumeroManifiesto)
'                    Else
'                        Actualizar_Numero_Confirmacion_Manifiesto_Ministerio(lonNumeroManifiesto, "No se encuentra disponible la plataforma del ministerio de transporte, por favor intentar mas tarde")
'                    End If
'                End If
'            Catch ex As Exception
'                Reportar_Manifiesto = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try

'        End Function


'        Public Function Consultar_Flete_Minimo_Sicetac(ByVal lonNumeroManifiesto As Long) As Boolean
'            Try
'                Dim bolAccesoDisponible As Boolean

'                If Verificar_Acceso_Enlace_Uno() Then
'                    Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                    bolAccesoDisponible = True
'                ElseIf Verificar_Acceso_Enlace_Dos() Then
'                    Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                    bolAccesoDisponible = True
'                Else
'                    bolAccesoDisponible = False
'                End If
'                Actualiza_Validacion_Cambio_Enlace_Ministerio()

'                If bolAccesoDisponible Then
'                    Proceso_Consultar_Flete_Minimo_Sicetac(lonNumeroManifiesto)
'                Else
'                    Actualizar_Numero_Confirmacion_Manifiesto_Ministerio(lonNumeroManifiesto, "No se encuentra disponible la plataforma del ministerio de transporte, por favor intentar mas tarde")
'                End If

'            Catch ex As Exception
'                Consultar_Flete_Minimo_Sicetac = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'        Public Function Reportar_Anulacion_Cumplido(ByVal lonNumeroCumplido As Long) As Boolean
'            Try
'                Dim bolAccesoDisponible As Boolean

'                If Verificar_Acceso_Enlace_Uno() Then
'                    Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                    bolAccesoDisponible = True
'                ElseIf Verificar_Acceso_Enlace_Dos() Then
'                    Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                    bolAccesoDisponible = True
'                Else
'                    bolAccesoDisponible = False
'                End If
'                Actualiza_Validacion_Cambio_Enlace_Ministerio()

'                If bolAccesoDisponible Then
'                    Proceso_Reportar_Anulacion_Cumplido(lonNumeroCumplido)
'                Else
'                    Actualizar_Numero_Confirmacion_Manifiesto_Ministerio(lonNumeroCumplido, "No se encuentra disponible la plataforma del ministerio de transporte, por favor intentar mas tarde")
'                End If

'            Catch ex As Exception
'                Reportar_Anulacion_Cumplido = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'        Public Function Reportar_Manifiesto(ByVal lonNumeroManifiesto As Long) As Boolean
'            Try
'                Me.lonTiempoSegundos = ConfigurationManager.AppSettings("TiempoMaximoEnvioSegundosManifiesto").ToString()
'                If Me.intActivaEnvioMultiProceso = General.SELECCIONO_SI And Me.TipoReporteDocumento = EnumTipoReporteDocumento.DESDE_CREACION_DOCUMENTO Then
'                    Me.TipoDocumento = EnumTipoDocumentoGenera.MANIFIESTO
'                    Me.thrControProceso = New Thread(AddressOf Control_Proceso_Reporte_Informacion)
'                    Me.thrControProceso.Start(lonNumeroManifiesto)
'                    Reportar_Manifiesto = True
'                Else

'                    Dim dteFechaInicial As Date = Date.Now
'                    If Me.lonCantidadRepeticionesProceso = General.CERO Then
'                        enumTemporalEnlacesMinisterio = enumEnlacesMinisterio
'                        Proceso_Reportar_Manifiesto(lonNumeroManifiesto)
'                        Me.dblCantidadSegundosPrimerReporte = DateDiff(DateInterval.Second, dteFechaInicial, Date.Now)

'                        If (Me.lonTiempoSegundos * 2) < Me.dblCantidadSegundosPrimerReporte And Me.lonCodigoConfirmacion = General.CERO Then

'                            If Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE Then
'                                Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                            Else
'                                Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                            End If
'                            dteFechaInicial = Date.Now
'                            Proceso_Reportar_Manifiesto(lonNumeroManifiesto)
'                            If (Me.lonTiempoSegundos * 2) > DateDiff(DateInterval.Second, dteFechaInicial, Date.Now) And Me.lonCodigoConfirmacion <> General.CERO Then
'                                If Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE Then
'                                    Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                                Else
'                                    Me.enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                                End If
'                                Actualiza_Validacion_Cambio_Enlace_Ministerio()
'                            End If

'                        End If

'                    Else
'                        If Me.lonCantidadRepeticionesProceso = General.UNO Then
'                            If enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE Then
'                                enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                            Else
'                                enumEnlacesMinisterio = EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                            End If
'                            Proceso_Reportar_Manifiesto(lonNumeroManifiesto)
'                            If Me.dblCantidadSegundosPrimerReporte > DateDiff(DateInterval.Second, dteFechaInicial, Date.Now) Then
'                                Actualiza_Validacion_Cambio_Enlace_Ministerio()
'                            Else
'                                enumEnlacesMinisterio = enumTemporalEnlacesMinisterio
'                            End If
'                        Else
'                            Proceso_Reportar_Manifiesto(lonNumeroManifiesto)
'                        End If
'                    End If
'                    Me.lonCantidadRepeticionesProceso += General.UNO
'                    Reportar_Manifiesto = True

'                End If
'            Catch ex As Exception
'                Reportar_Manifiesto = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'        ''' <summary>
'        ''' Realiza envio de informacion al Ministerio de Transporte en un proceso continuo o multi proceso de acuerdo a como se le indique.
'        ''' </summary>
'        ''' <param name="lonNumeroManifiesto">Numero del Manifiesto</param>
'        ''' <remarks></remarks>
'        Public Sub Proceso_Reportar_Manifiesto(ByVal lonNumeroManifiesto As Long)
'            Try

'                Dim objManifiestoMEC As New ManifiestoRNDC(acceso, solicitudConsulta)
'                Dim objConductor As New TerceroRNDC(acceso, solicitudConsulta)
'                Dim objTenedor As New TerceroRNDC(acceso, solicitudConsulta)
'                Dim objPropietario As New TerceroRNDC(acceso, solicitudConsulta)
'                Dim objVehiculo As New VehiculoRNDC(acceso, solicitudConsulta)
'                Dim objSemirremolque As New VehiculoRNDC(acceso, solicitudConsulta)
'                Dim dteAuxiliar As Date = Date.MinValue
'                Dim strAuxiliar As String = String.Empty
'                Dim dblAuxiliar As Double = General.CERO
'                Dim bolReporto As Boolean = True
'                Dim auxiliarFechaVencimiento As Date = Date.Now

'                Dim strRemesasPendientes As String = String.Empty

'                Me.objGeneral.ComandoSQL = New SqlCommand

'                Me.strTextoXmlTenedor = String.Empty
'                Me.strTextoXmlPropietario = String.Empty
'                Me.strTextXmlConductor = String.Empty
'                Me.strTextXmlVehiculo = String.Empty
'                Me.strTextXmlRemolque = String.Empty
'                Me.strTextXmlManifiesto = String.Empty

'                Me.strTextoXmlPropietario = String.Empty
'                Me.strTextXmlConductor = String.Empty
'                Me.strTextXmlVehiculo = String.Empty
'                Me.strTextXmlRemolque = String.Empty
'                Me.strTextXmlManifiesto = String.Empty

'                If Not Remesas_Pendientes_X_Reporar_Manifiesto(TipoDocumentos.TIDO_MANIFIESTO, lonNumeroManifiesto, strRemesasPendientes) Then



'                    strSQL = "SELECT *"
'                    strSQL += " FROM V_Informacion_Viaje"
'                    strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    strSQL += " AND Numero = " & lonNumeroManifiesto

'                    Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
'                    Me.objGeneral.ComandoSQL.CommandText = strSQL
'                    Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.CadenaDeConexionSQL)
'                    If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
'                        Me.objGeneral.ComandoSQL.Connection.Open()
'                    End If
'                    Dim sdrManifiesto As SqlDataReader = Me.objGeneral.ComandoSQL.ExecuteReader

'                    While sdrManifiesto.Read

'                        objTenedor.Codigo = Val(sdrManifiesto("CodigoTenedor"))
'                        objPropietario.Codigo = Val(sdrManifiesto("CodigoPropietario"))
'                        objConductor.Codigo = Val(sdrManifiesto("CodigoConductor"))
'                        objManifiestoMEC.NUMNITEMPRESATRANSPORTE = Me.objEmpresa.NumeroIdentificacion
'                        objManifiestoMEC.NUMMANIFIESTOCARGA = sdrManifiesto("Numero")

'                        objManifiestoMEC.CODOPERACIONTRANSPORTE = sdrManifiesto("TipoManifiesto")

'                        objManifiestoMEC.FECHAEXPEDICIONMANIFIESTO = Format(Date.Parse(sdrManifiesto("FechaExpedicion")).Date, General.FORMATO_FECHA_DIA_MES_ANO)
'                        objManifiestoMEC.CODMUNICIPIOORIGENMANIFIESTO = objGeneral.FormCampMiniTran(sdrManifiesto("CIUD_Origen"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                        objManifiestoMEC.CODMUNICIPIOORIGENMANIFIESTO = objGeneral.FormCampMiniTran(objManifiestoMEC.CODMUNICIPIOORIGENMANIFIESTO, 8, General.CAMPO_CEROS_DERECHA)
'                        objManifiestoMEC.CODMUNICIPIODESTINOMANIFIESTO = objGeneral.FormCampMiniTran(sdrManifiesto("CIUD_Destino"), 5, General.CAMPO_CEROS_IZQUIERDA)

'                        objManifiestoMEC.CODMUNICIPIODESTINOMANIFIESTO = objGeneral.FormCampMiniTran(objManifiestoMEC.CODMUNICIPIODESTINOMANIFIESTO, 8, General.CAMPO_CEROS_DERECHA)
'                        objManifiestoMEC.CODIDTITULARMANIFIESTO = sdrManifiesto("TipoIdTenedor")
'                        objManifiestoMEC.NUMIDTITULARMANIFIESTO = sdrManifiesto("NumeroIdentificacionTenedor")
'                        objManifiestoMEC.NUMPLACA = sdrManifiesto("Placa")
'                        If Replace(sdrManifiesto("SEMI_Placa"), " ", "") <> String.Empty Then
'                            objManifiestoMEC.NUMPLACAREMOLQUE = sdrManifiesto("SEMI_Placa")
'                        Else
'                            objManifiestoMEC.NUMPLACAREMOLQUE = String.Empty
'                        End If

'                        objManifiestoMEC.CODIDCONDUCTOR = sdrManifiesto("TipoIdConductor")
'                        objManifiestoMEC.NUMIDCONDUCTOR = sdrManifiesto("NumeroIdentificacionConductor")
'                        If Double.Parse(sdrManifiesto("Valor_Pagar")) < General.CERO Then
'                            objManifiestoMEC.VALORFLETEPACTADOVIAJE = Double.Parse(sdrManifiesto("Valor_Flete")) + -Double.Parse(sdrManifiesto("Valor_Pagar"))
'                        Else
'                            If Ambiente = Ambiantes_Desarrollos.LOCAL Then
'                                If objManifiestoMEC.VALORFLETEPACTADOVIAJE = General.CERO Then
'                                    objManifiestoMEC.VALORFLETEPACTADOVIAJE = General.VALOR_DEFECTO_ENVIO
'                                End If
'                            Else
'                                objManifiestoMEC.VALORFLETEPACTADOVIAJE = sdrManifiesto("Valor_Flete")
'                            End If
'                        End If

'                        objManifiestoMEC.RETENCIONFUENTEMANIFIESTO = sdrManifiesto("Valor_Retencion_Fuente")
'                        Double.TryParse(sdrManifiesto("DescuentosLey"), dblAuxiliar)
'                        objManifiestoMEC.RETENCIONICAMANIFIESTOCARGA = dblAuxiliar / 100
'                        If objManifiestoMEC.VALORFLETEPACTADOVIAJE < sdrManifiesto("Valor_Anticipo") Then
'                            objManifiestoMEC.VALORFLETEPACTADOVIAJE = Val(sdrManifiesto("Valor_Anticipo")) + objManifiestoMEC.VALORANTICIPOMANIFIESTO
'                            objManifiestoMEC.VALORANTICIPOMANIFIESTO = sdrManifiesto("Valor_Anticipo")
'                        Else
'                            objManifiestoMEC.VALORANTICIPOMANIFIESTO = sdrManifiesto("Valor_Anticipo")
'                        End If

'                        objManifiestoMEC.CODMUNICIPIOPAGOSALDO = objGeneral.FormCampMiniTran(sdrManifiesto("CIUD_Destino"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                        objManifiestoMEC.CODMUNICIPIOPAGOSALDO = objGeneral.FormCampMiniTran(objManifiestoMEC.CODMUNICIPIOPAGOSALDO, 8, General.CAMPO_CEROS_DERECHA)
'                        objManifiestoMEC.CODRESPONSABLEPAGOCARGUE = objManifiestoMEC.CODMUNICIPIODESTINOMANIFIESTO
'                        objManifiestoMEC.CODRESPONSABLEPAGOCARGUE = sdrManifiesto("ResponsableCargue")
'                        objManifiestoMEC.CODRESPONSABLEPAGODESCARGUE = sdrManifiesto("ResponsableDescargue")

'                        If Date.Parse(objManifiestoMEC.FECHAEXPEDICIONMANIFIESTO) < Date.Parse(sdrManifiesto("Fecha_Cumplimiento")) Then
'                            objManifiestoMEC.FECHAPAGOSALDOMANIFIESTO = sdrManifiesto("Fecha_Cumplimiento")
'                        Else
'                            objManifiestoMEC.FECHAPAGOSALDOMANIFIESTO = DateAdd(DateInterval.Day, 1, Date.Parse(objManifiestoMEC.FECHAEXPEDICIONMANIFIESTO))
'                        End If
'                        objManifiestoMEC.OBSERVACIONES = Mid(Replace(Replace(Mid(sdrManifiesto("Observaciones"), 1, 199), "'", ""), Chr(34), ""), 1, 100)

'                        REM: Carga INFORMACION DEL TENEDOR
'                        objTenedor.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion
'                        objTenedor.CODTIPOIDTERCERO = sdrManifiesto("TipoIdTenedor")
'                        objTenedor.NUMIDTERCERO = sdrManifiesto("NumeroIdentificacionTenedor")
'                        If sdrManifiesto("PrimerApellidoTenedor") = String.Empty Or Len(sdrManifiesto("PrimerApellidoTenedor")) < General.TRES Then
'                            objTenedor.PRIMERAPELLIDOIDTERCERO = TerceroRNDC.APELLIDO_X_DEFECTO
'                        Else
'                            objTenedor.PRIMERAPELLIDOIDTERCERO = sdrManifiesto("PrimerApellidoTenedor")
'                        End If
'                        If Len(sdrManifiesto("NombreTenedor")) < 3 Then
'                            objTenedor.NOMIDTERCERO = sdrManifiesto("NombreTenedor") & objTenedor.PRIMERAPELLIDOIDTERCERO
'                        Else
'                            objTenedor.NOMIDTERCERO = sdrManifiesto("NombreTenedor")
'                        End If

'                        objTenedor.SEGUNDOAPELLIDOIDTERCERO = sdrManifiesto("SegundoApellidoTenedor")
'                        If Val(sdrManifiesto("TelefonoTenedor")) = General.CERO Or Len(sdrManifiesto("TelefonoTenedor")) < TerceroRNDC.DIGITOS_TELEFONO Then
'                            objTenedor.NUMTELEFONOCONTACTO = TerceroRNDC.TELEFONO_X_DEFECTO
'                        Else
'                            objTenedor.NUMTELEFONOCONTACTO = Replace(sdrManifiesto("TelefonoTenedor"), " ", "")
'                            objTenedor.NUMTELEFONOCONTACTO = Mid(objTenedor.NUMTELEFONOCONTACTO, General.UNO, TerceroRNDC.DIGITOS_TELEFONO)
'                        End If

'                        If Len(sdrManifiesto("DireccionTenedor")) > General.SEIS Then
'                            objTenedor.NOMENCLATURADIRECCION = Mid(sdrManifiesto("DireccionTenedor"), 1, 60)
'                        Else
'                            objTenedor.NOMENCLATURADIRECCION = TerceroRNDC.DIRECCION_X_DEFECTO
'                        End If
'                        objTenedor.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(sdrManifiesto("CiudadTenedor"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                        objTenedor.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(objTenedor.CODMUNICIPIORNDC, 8, General.CAMPO_CEROS_DERECHA)
'                        If objTenedor.CODTIPOIDTERCERO = TerceroRNDC.TIPO_IDENTIFICACION_NIT Then
'                            objTenedor.CODSEDETERCERO = objGeneral.FormCampMiniTran(sdrManifiesto("CiudadTenedor"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                            objTenedor.NOMSEDETERCERO = sdrManifiesto("NombreSedeTenedor")
'                        Else
'                            objTenedor.CODSEDETERCERO = objGeneral.FormCampMiniTran(sdrManifiesto("CiudadTenedor"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                        End If


'                        REM: Carga INFORMACION DEL CONDUCTOR
'                        objConductor.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion
'                        objConductor.CODTIPOIDTERCERO = sdrManifiesto("TipoIdConductor")
'                        objConductor.NUMIDTERCERO = sdrManifiesto("NumeroIdentificacionConductor").trim
'                        objConductor.NOMIDTERCERO = sdrManifiesto("NombreConductor")
'                        If sdrManifiesto("PrimerApellidoConductor") = String.Empty Or Len(sdrManifiesto("PrimerApellidoConductor")) < General.TRES Then
'                            objConductor.PRIMERAPELLIDOIDTERCERO = TerceroRNDC.APELLIDO_X_DEFECTO
'                        Else
'                            objConductor.PRIMERAPELLIDOIDTERCERO = sdrManifiesto("PrimerApellidoConductor")
'                        End If
'                        objConductor.SEGUNDOAPELLIDOIDTERCERO = sdrManifiesto("SegundoApellidoConductor")
'                        If Val(sdrManifiesto("TelefonoConductor")) = General.CERO Or Len(sdrManifiesto("TelefonoConductor")) < TerceroRNDC.DIGITOS_TELEFONO Then
'                            objConductor.NUMTELEFONOCONTACTO = TerceroRNDC.TELEFONO_X_DEFECTO
'                        Else
'                            objConductor.NUMTELEFONOCONTACTO = Replace(sdrManifiesto("TelefonoConductor"), " ", "")
'                            objConductor.NUMTELEFONOCONTACTO = Mid(objConductor.NUMTELEFONOCONTACTO, General.UNO, TerceroRNDC.DIGITOS_TELEFONO)
'                        End If

'                        If Len(sdrManifiesto("DireccionConductor")) > General.SEIS Then
'                            objConductor.NOMENCLATURADIRECCION = Mid(sdrManifiesto("DireccionConductor"), 1, 50)
'                        Else
'                            objConductor.NOMENCLATURADIRECCION = TerceroRNDC.DIRECCION_X_DEFECTO
'                        End If
'                        objConductor.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(sdrManifiesto("CiudadConductor"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                        objConductor.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(objConductor.CODMUNICIPIORNDC, 8, General.CAMPO_CEROS_DERECHA)
'                        If objConductor.CODTIPOIDTERCERO = TerceroRNDC.TIPO_IDENTIFICACION_NIT Then
'                            objConductor.CODSEDETERCERO = objGeneral.FormCampMiniTran(sdrManifiesto("CiudadConductor"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                            objConductor.NOMSEDETERCERO = sdrManifiesto("NombreSedeConductor")
'                        Else
'                            objConductor.CODSEDETERCERO = objGeneral.FormCampMiniTran(sdrManifiesto("CiudadConductor"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                        End If

'                        If Not IsDBNull(sdrManifiesto("CategoriaLicenciaConductor")) Then
'                            objConductor.CODCATEGORIALICENCIACONDUCCION = sdrManifiesto("CategoriaLicenciaConductor")
'                        Else
'                            objConductor.CODCATEGORIALICENCIACONDUCCION = ""
'                        End If

'                        objConductor.NUMLICENCIACONDUCCION = sdrManifiesto("NumeroLicenciaConductor")
'                        Date.TryParse(sdrManifiesto("FechaLicenciaConductor"), dteAuxiliar)
'                        objConductor.FECHAVENCIMIENTOLICENCIA = Format(dteAuxiliar, General.FORMATO_FECHA_DIA_MES_ANO)

'                        REM: Carga INFORMACION DEL PROPIETARIO
'                        objPropietario.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion
'                        objPropietario.CODTIPOIDTERCERO = sdrManifiesto("TipoIdPropietario")
'                        objPropietario.NUMIDTERCERO = sdrManifiesto("NumeroIdentificacionPropietario")
'                        objPropietario.NOMIDTERCERO = sdrManifiesto("NombrePropietario")
'                        If sdrManifiesto("PrimerApellidoPropietario") = String.Empty Or Len(sdrManifiesto("PrimerApellidoPropietario")) < General.TRES Then
'                            objPropietario.PRIMERAPELLIDOIDTERCERO = TerceroRNDC.APELLIDO_X_DEFECTO
'                        Else
'                            objPropietario.PRIMERAPELLIDOIDTERCERO = sdrManifiesto("PrimerApellidoPropietario")
'                        End If
'                        objPropietario.SEGUNDOAPELLIDOIDTERCERO = sdrManifiesto("SegundoApellidoPropietario")
'                        If Val(sdrManifiesto("TelefonoPropietario")) = General.CERO Or Len(sdrManifiesto("TelefonoPropietario")) < TerceroRNDC.DIGITOS_TELEFONO Then
'                            objPropietario.NUMTELEFONOCONTACTO = TerceroRNDC.TELEFONO_X_DEFECTO
'                        Else
'                            objPropietario.NUMTELEFONOCONTACTO = Replace(sdrManifiesto("TelefonoPropietario"), " ", "")
'                            objPropietario.NUMTELEFONOCONTACTO = Mid(objPropietario.NUMTELEFONOCONTACTO, General.UNO, TerceroRNDC.DIGITOS_TELEFONO)
'                        End If
'                        If Len(sdrManifiesto("DireccionPropietario")) > General.SEIS Then
'                            objPropietario.NOMENCLATURADIRECCION = Mid(sdrManifiesto("DireccionPropietario"), 1, 60)
'                        Else
'                            objPropietario.NOMENCLATURADIRECCION = TerceroRNDC.DIRECCION_X_DEFECTO
'                        End If

'                        objPropietario.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(sdrManifiesto("CiudadPropietario"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                        objPropietario.CODMUNICIPIORNDC = objGeneral.FormCampMiniTran(objPropietario.CODMUNICIPIORNDC, 8, General.CAMPO_CEROS_DERECHA)
'                        If objPropietario.CODTIPOIDTERCERO = TerceroRNDC.TIPO_IDENTIFICACION_NIT Then
'                            objPropietario.CODSEDETERCERO = objGeneral.FormCampMiniTran(sdrManifiesto("CiudadPropietario"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                            objPropietario.NOMSEDETERCERO = sdrManifiesto("NombreSedePropietario")
'                        Else
'                            objPropietario.CODSEDETERCERO = objGeneral.FormCampMiniTran(sdrManifiesto("CiudadPropietario"), 5, General.CAMPO_CEROS_IZQUIERDA)
'                        End If

'                        If Not IsDBNull(sdrManifiesto("CategoriaLicenciaPropietario")) Then
'                            If sdrManifiesto("CategoriaLicenciaPropietario") <> String.Empty And sdrManifiesto("CategoriaLicenciaPropietario") <> "0" Then
'                                objPropietario.CODCATEGORIALICENCIACONDUCCION = sdrManifiesto("CategoriaLicenciaPropietario")
'                                objPropietario.NUMLICENCIACONDUCCION = sdrManifiesto("NumeroLicenciaPropietario")
'                                Date.TryParse((sdrManifiesto("FechaLicenciaPropietario")), dteAuxiliar)
'                                objPropietario.FECHAVENCIMIENTOLICENCIA = Format(dteAuxiliar, General.FORMATO_FECHA_DIA_MES_ANO)
'                            End If
'                        End If



'                        Dim arrCampos(16), arrValoresCampos(16) As String
'                        arrCampos(0) = "ConfiguracionTipoVehiculo"
'                        arrCampos(1) = "CodigoMarcaMinisterio"
'                        arrCampos(2) = "CodigoLineaMinisterio"
'                        arrCampos(3) = "Modelo"
'                        arrCampos(4) = "Peso_Bruto"
'                        arrCampos(5) = "CodigoColorMinisterio"
'                        arrCampos(6) = "CodigoCarroceriaMinisterio"
'                        arrCampos(7) = "TipoIdentificacionPropietario"
'                        arrCampos(8) = "IdenPropietario"
'                        arrCampos(9) = "Poliza_SOAT"
'                        arrCampos(10) = "Fecha_Vence_SOAT"
'                        arrCampos(11) = "IdenAseguradoraConDigitoCheque"
'                        arrCampos(12) = "Numero_Serie"
'                        arrCampos(13) = "TipoCombustible"
'                        arrCampos(14) = "UnidadMedida"
'                        arrCampos(15) = "Capacidad"
'                        arrCampos(16) = "Propio"

'                        REM: CARGA LA INFORMACION DEL VEHICULO
'                        If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "V_Vehiculos", arrCampos, arrValoresCampos, arrCampos.Length, General.CAMPO_ALFANUMERICO, "Placa", objManifiestoMEC.NUMPLACA, "", Me.strError) Then

'                            objVehiculo.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion
'                            objVehiculo.NUMPLACA = objManifiestoMEC.NUMPLACA.Trim

'                            If objTenedor.NUMIDTERCERO = objVehiculo.NUMNITEMPRESATRANSPORTE Then
'                                objManifiestoMEC.bolVehiculoPropio = True
'                            Else
'                                objManifiestoMEC.bolVehiculoPropio = False
'                            End If

'                            objVehiculo.CODCONFIGURACIONUNIDADCARGA = Trim(arrValoresCampos(0))
'                            If Ambiente = Ambiantes_Desarrollos.LOCAL Then
'                                If Val(arrValoresCampos(1)) = General.CERO Then
'                                    objVehiculo.CODMARCAVEHICULOCARGA = General.VALOR_MARCA_DEFECTO_ENVIO
'                                Else
'                                    objVehiculo.CODMARCAVEHICULOCARGA = arrValoresCampos(1)
'                                End If
'                            Else
'                                objVehiculo.CODMARCAVEHICULOCARGA = arrValoresCampos(1)
'                            End If

'                            objVehiculo.CODLINEAVEHICULOCARGA = arrValoresCampos(2)
'                            If Val(arrValoresCampos(3)) > 1900 And Val(arrValoresCampos(3)) <= Date.Now.Year Then
'                                objVehiculo.ANOFABRICACIONVEHICULOCARGA = arrValoresCampos(3)
'                            Else
'                                objVehiculo.ANOFABRICACIONVEHICULOCARGA = Date.Now.Year
'                            End If

'                            objVehiculo.PESOVEHICULOVACIO = Val(arrValoresCampos(4)) * General.FACTOR_CONVERSION_X_MIL
'                            objVehiculo.UNIDADMEDIDACAPACIDAD = General.UNO
'                            objVehiculo.CODCOLORVEHICULOCARGA = arrValoresCampos(5)
'                            objVehiculo.CODTIPOCARROCERIA = arrValoresCampos(6)
'                            objVehiculo.CODTIPOIDPROPIETARIO = Mid(arrValoresCampos(TerceroRNDC.DIGITOS_TELEFONO), General.UNO, General.UNO)
'                            objVehiculo.NUMIDPROPIETARIO = arrValoresCampos(8)
'                            objVehiculo.CODTIPOIDTENEDOR = objTenedor.CODTIPOIDTERCERO
'                            objVehiculo.NUMIDTENEDOR = objTenedor.NUMIDTERCERO
'                            Date.TryParse(arrValoresCampos(10), dteAuxiliar)
'                            objVehiculo.FECHAVENCIMIENTOSOAT = Format(dteAuxiliar, General.FORMATO_FECHA_DIA_MES_ANO)
'                            objVehiculo.NUMSEGUROSOAT = Replace(Replace(Replace(arrValoresCampos(9), " ", ""), "-", ""), ".", "")
'                            objVehiculo.NUMNITASEGURADORASOAT = arrValoresCampos(11)
'                            objVehiculo.NUMCHASIS = arrValoresCampos(12)
'                            objVehiculo.CODTIPOCOMBUSTIBLE = arrValoresCampos(13)
'                            If (Val(objVehiculo.CODCONFIGURACIONUNIDADCARGA) <> VehiculoRNDC.CABEZOTE_TRES_EJES Or
'                            Val(objVehiculo.CODCONFIGURACIONUNIDADCARGA) <> VehiculoRNDC.CABEZOTE_DOS_EJES And
'                            objManifiestoMEC.NUMPLACAREMOLQUE.Trim = String.Empty) Then
'                                objVehiculo.CAPACIDADUNIDADCARGA = Val(arrValoresCampos(15)) * General.FACTOR_CONVERSION_X_MIL
'                            Else
'                                objVehiculo.CAPACIDADUNIDADCARGA = General.CERO
'                            End If
'                        End If

'                        REM: SI EXISTE LA PLACA DEL SEMI REMOLQUE LO CONSULTA, SI NO EXISTE SE LE ASIGNAN VALOR POR DEFECTO.

'                        If objManifiestoMEC.NUMPLACAREMOLQUE <> String.Empty Then

'                            ReDim arrCampos(5), arrValoresCampos(5)
'                            arrCampos(0) = "CodigoMarcaMinisterio"
'                            arrCampos(1) = "Modelo"
'                            arrCampos(2) = "Peso_Bruto"
'                            arrCampos(3) = "Capacidad_Kilogramos"
'                            arrCampos(4) = "CodigoTipoSemirremolqueMinisterio"
'                            arrCampos(5) = "Numero_Ejes"
'                            Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "V_Semiremolques", arrCampos, arrValoresCampos, arrCampos.Length, General.CAMPO_ALFANUMERICO, "Placa", objManifiestoMEC.NUMPLACAREMOLQUE, "", Me.strError)
'                            If arrValoresCampos(0) <> String.Empty Then

'                                REM: EXISTE EL SEMIREMOLQUE
'                                objSemirremolque.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion
'                                objSemirremolque.NUMPLACA = objManifiestoMEC.NUMPLACAREMOLQUE.Trim
'                                objSemirremolque.CODMARCAVEHICULOCARGA = arrValoresCampos(0)
'                                objSemirremolque.ANOFABRICACIONVEHICULOCARGA = objVehiculo.ANOFABRICACIONVEHICULOCARGA
'                                If Val(arrValoresCampos(2)) = General.CERO Then
'                                    objSemirremolque.PESOVEHICULOVACIO = General.PESO_VALOR_DEFECTO
'                                Else
'                                    If Val(arrValoresCampos(2)) <= 42 Then
'                                        objSemirremolque.PESOVEHICULOVACIO = Val(arrValoresCampos(2)) * General.FACTOR_CONVERSION_KILO_TONELADAS
'                                    Else
'                                        objSemirremolque.PESOVEHICULOVACIO = Val(arrValoresCampos(2))
'                                    End If
'                                End If
'                                objSemirremolque.UNIDADMEDIDACAPACIDAD = objVehiculo.UNIDADMEDIDACAPACIDAD
'                                objSemirremolque.CODTIPOCARROCERIA = objVehiculo.CODTIPOCARROCERIA
'                                objSemirremolque.CODCONFIGURACIONUNIDADCARGA = Trim(arrValoresCampos(4))
'                                objSemirremolque.CODTIPOIDPROPIETARIO = objVehiculo.CODTIPOIDPROPIETARIO
'                                objSemirremolque.NUMIDPROPIETARIO = objVehiculo.NUMIDPROPIETARIO
'                                objSemirremolque.CODTIPOIDTENEDOR = objTenedor.CODTIPOIDTERCERO
'                                objSemirremolque.NUMIDTENEDOR = objTenedor.NUMIDTERCERO
'                                objSemirremolque.CAPACIDADUNIDADCARGA = CAPACIDAD_UNIDAD_MARCA_X_DEFECTO
'                                If arrValoresCampos(5) >= General.CUATRO Then
'                                    objSemirremolque.NUMEJES = arrValoresCampos(5)
'                                End If

'                            Else
'                                REM: ASIGNAR VALORES POR DEFECTO AL SEMIRREMOLQUE
'                                objSemirremolque.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion
'                                objSemirremolque.NUMPLACA = objManifiestoMEC.NUMPLACAREMOLQUE
'                                objSemirremolque.CODMARCAVEHICULOCARGA = objVehiculo.CODMARCAVEHICULOCARGA
'                                objSemirremolque.CODLINEAVEHICULOCARGA = objVehiculo.CODLINEAVEHICULOCARGA
'                                objSemirremolque.ANOFABRICACIONVEHICULOCARGA = objVehiculo.ANOFABRICACIONVEHICULOCARGA
'                                If Val(arrValoresCampos(2)) = General.CERO Then
'                                    objSemirremolque.PESOVEHICULOVACIO = General.PESO_VALOR_DEFECTO
'                                Else
'                                    If Val(arrValoresCampos(2)) <= 42 Then
'                                        objSemirremolque.PESOVEHICULOVACIO = Val(arrValoresCampos(2)) * General.FACTOR_CONVERSION_KILO_TONELADAS
'                                    Else
'                                        objSemirremolque.PESOVEHICULOVACIO = Val(arrValoresCampos(2))
'                                    End If
'                                End If
'                                objSemirremolque.UNIDADMEDIDACAPACIDAD = objVehiculo.UNIDADMEDIDACAPACIDAD
'                                objSemirremolque.CODTIPOCARROCERIA = objVehiculo.CODTIPOCARROCERIA
'                                objSemirremolque.CODCONFIGURACIONUNIDADCARGA = CONFIGURACION_REMOLQUE_X_DEFECTO ' Se pone una configuracion por defecto, para garantizar que se envíe toda la información
'                                objSemirremolque.CODTIPOIDPROPIETARIO = objVehiculo.CODTIPOIDPROPIETARIO
'                                objSemirremolque.NUMIDPROPIETARIO = objVehiculo.NUMIDPROPIETARIO
'                                objSemirremolque.CODTIPOIDTENEDOR = objTenedor.CODTIPOIDTERCERO
'                                objSemirremolque.NUMIDTENEDOR = objTenedor.NUMIDTERCERO
'                                objSemirremolque.CAPACIDADUNIDADCARGA = CAPACIDAD_UNIDAD_MARCA_X_DEFECTO

'                            End If
'                        End If

'                    End While
'                    If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                        Me.objGeneral.ConexionSQL.Close()
'                    End If

'                    REM: realiza Cambio de Enlace para reportar información al Ministerio
'                    Select Case enumEnlacesMinisterio
'                        Case EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                            objTenedor.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                            objPropietario.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                            objTenedor.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                            objConductor.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                            objVehiculo.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                            objSemirremolque.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                            objManifiestoMEC.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                        Case EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                            objTenedor.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                            objPropietario.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                            objTenedor.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                            objConductor.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                            objVehiculo.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                            objSemirremolque.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                            objManifiestoMEC.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                    End Select

'                    Dim strErroresTenedor As String = String.Empty
'                    Dim lstVariablesTenedor As New List(Of String)
'                    Dim dblTenedor As Double = General.CERO

'                    REM:VARIABLES PARA RETORNAR VALOR DE TENEDOR DEL MINISTERIO
'                    lstVariablesTenedor.Add("NOMENCLATURADIRECCION".ToLower)
'                    lstVariablesTenedor.Add("CODSEDETERCERO".ToLower)
'                    LimpiaCarateresEspeciales(objTenedor.NOMENCLATURADIRECCION)

'                    REM: SE ENVIA EL TENEDOR AL MINISTERIO
'                    LimpiaCarateresEspeciales(objTenedor.NOMENCLATURADIRECCION)
'                    strErroresTenedor = String.Empty
'                    dblTenedor = objTenedor.Reportar_Tercero(strTextoXmlTenedor, Tipos_Procesos.FORMATO_INGRESO, strErroresTenedor, lstVariablesTenedor, Tipos_Roles.TENEDOR)
'                    bolReporto = False
'                    If (dblTenedor > General.CERO And strErroresTenedor = String.Empty) Or strErroresTenedor.Contains(strTercDupl) Then
'                        If dblTenedor = General.CERO Then Double.TryParse(strErroresTenedor.Split(" ")(0).Split(":")(1), dblTenedor)
'                        objTenedor.Guardar_Tercero_Confirmacion(dblTenedor, objEmpresa.Codigo, Me.objUsuario.Codigo, strErroresTenedor)
'                        bolReporto = True
'                    End If


'                    Dim strErroresPropietario As String = String.Empty
'                    Dim lstVariablesPropietario As New List(Of String)
'                    Dim dblPropietario As Double = General.CERO

'                    REM:VARIABLES PARA RETORNAR VALOR DE PROPIETARIO DEL MINISTERIO
'                    lstVariablesPropietario.Add("NOMENCLATURADIRECCION".ToLower)
'                    lstVariablesPropietario.Add("CODSEDETERCERO".ToLower)


'                    If bolReporto Then
'                        REM: LA VARIABLES FECHAVENCIMIENTOLICENCIA,CODCATEGORIALICENCIACONDUCCION Y NUMLICENCIACONDUCCION PARA QUE RETORNE INFORMACION DELICENCIA CON EL PROPIETARIO
'                        objPropietario.FECHAVENCIMIENTOLICENCIA = Nothing
'                        objPropietario.CODCATEGORIALICENCIACONDUCCION = Nothing
'                        objPropietario.NUMLICENCIACONDUCCION = Nothing
'                        REM: SE ENVIA EL PROPIETARIO AL MINISTERIO
'                        strErroresPropietario = String.Empty
'                        dblPropietario = objPropietario.Reportar_Tercero(strTextoXmlPropietario, Tipos_Procesos.FORMATO_INGRESO, strErroresPropietario, lstVariablesPropietario, Tipos_Roles.PROPIETARIO)
'                        bolReporto = False
'                        If (dblPropietario > General.CERO And strErroresPropietario = String.Empty) Or strErroresPropietario.Contains(strTercDupl) Then
'                            If dblPropietario = General.CERO Then Double.TryParse(strErroresPropietario.Split(" ")(0).Split(":")(1), dblPropietario)
'                            objPropietario.Guardar_Tercero_Confirmacion(dblPropietario, objEmpresa.Codigo, Me.objUsuario.Codigo, strErroresPropietario)
'                            bolReporto = True
'                        End If


'                        If bolReporto Then

'                            REM: VARIABLES DE OBJETO DE CONDUCTOR
'                            Dim strErroresConductor As String = String.Empty
'                            Dim lstVariablesConductor As New List(Of String)
'                            Dim dblConductor As Double = General.CERO
'                            lstVariablesConductor.Add("CODSEDETERCERO".ToLower)
'                            lstVariablesConductor.Add("FECHAVENCIMIENTOLICENCIA".ToLower)
'                            lstVariablesConductor.Add("CODCATEGORIALICENCIACONDUCCION".ToLower)
'                            lstVariablesConductor.Add("NUMLICENCIACONDUCCION".ToLower)
'                            lstVariablesConductor.Add("NOMENCLATURADIRECCION".ToLower)

'                            If bolReporto Then
'                                REM: SE CONSULTA EL TERCERO CONDUCTOR
'                                dblConductor = objConductor.Reportar_Tercero(strTextXmlConductor, Tipos_Procesos.FORTMATO_CONSULTA, strErroresConductor, lstVariablesConductor, Tipos_Roles.CONDUCTOR)
'                                If dblConductor > General.CERO And strErroresConductor = String.Empty Then

'                                    Date.TryParse(lstVariablesConductor(1), auxiliarFechaVencimiento)

'                                    If (objConductor.FECHAVENCIMIENTOLICENCIA <= auxiliarFechaVencimiento And auxiliarFechaVencimiento < Date.Now And Ambiente = Ambiantes_Desarrollos.LOCAL) Or
'                                        (objConductor.FECHAVENCIMIENTOLICENCIA > auxiliarFechaVencimiento And objConductor.FECHAVENCIMIENTOLICENCIA >= Date.Now) Or
'                                        (objConductor.FECHAVENCIMIENTOLICENCIA <= auxiliarFechaVencimiento And auxiliarFechaVencimiento > Date.Now And objConductor.CODSEDETERCERO <> lstVariablesConductor(0)) Or
'                                        objConductor.CODCATEGORIALICENCIACONDUCCION.Trim <> lstVariablesConductor(2).Trim Or
'                                        objConductor.NUMLICENCIACONDUCCION.Trim <> lstVariablesConductor(3).Trim Or
'                                        objConductor.CODSEDETERCERO <> lstVariablesConductor(0) Or
'                                        Len(lstVariablesConductor(4)) > General.CARACTERES_MAXIMO_DIRECCION_RNDC_TERCERO Then

'                                        If (objConductor.FECHAVENCIMIENTOLICENCIA <= auxiliarFechaVencimiento And auxiliarFechaVencimiento < Date.Now And Ambiente = Ambiantes_Desarrollos.LOCAL) Then
'                                            REM: SI LA FECHA DE GESTRANS ES MENOR ALA DEL MINISTERIO Y LA FECHA DEL MINISTERIO ASU VEZ ES MENOR ALA ACTUAL Y EL AMBIENTE ES LOCAL SE ENVIA UN VALOR QUEMADO
'                                            objConductor.FECHAVENCIMIENTOLICENCIA = Format(Date.Now, General.FORMATO_FECHA_DIA_MES_ANO)
'                                        ElseIf (objConductor.FECHAVENCIMIENTOLICENCIA <= auxiliarFechaVencimiento And auxiliarFechaVencimiento > Date.Now And objConductor.CODSEDETERCERO <> lstVariablesConductor(0)) Then
'                                            REM: SI LA FECHA DE GESTRANS ES MENOR ALA DEL MINISTERIO Y LA FECHA DEL MINISTERIO ASU VEXZ ES MAYOR ALA ACTUAL Y LA SEDE ES DISTINTA ALA QUE SE VA A REPORTAR SE ENVIA DE NUEVO LA FECHA DEL MINISTERIO ALA SEDE CORRECTA
'                                            objConductor.FECHAVENCIMIENTOLICENCIA = auxiliarFechaVencimiento
'                                        End If
'                                        If Len(lstVariablesConductor(4)) > General.CARACTERES_MAXIMO_DIRECCION_RNDC_TERCERO Then
'                                            REM: SI LA DIRECCION SUPERA EL NUMERO DE CARACTERES MAXIMO, SE REMUEVE LOS CARACTERES ESPECIALES Y SE RECORTA A 50 PARA NUEVAMENTE ENVIAR EL CONDUCTOR
'                                            objConductor.NOMENCLATURADIRECCION = Mid(LimpiaCarateresEspeciales(lstVariablesConductor(4)), 1, General.CARACTERES_MAXIMO_DIRECCION_RNDC_TERCERO)
'                                        End If
'                                        objConductor.CODSEDETERCERO = lstVariablesConductor(0)
'                                        dblConductor = objConductor.Reportar_Tercero(strTextXmlConductor, Tipos_Procesos.FORMATO_INGRESO, strErroresConductor, lstVariablesConductor, Tipos_Roles.CONDUCTOR)
'                                        bolReporto = False
'                                        If (dblConductor > General.CERO And strErroresConductor = String.Empty) Or strErroresConductor.Contains(strTercDupl) Then
'                                            If dblConductor = General.CERO Then Double.TryParse(strErroresConductor.Split(" ")(0).Split(":")(1), dblConductor)
'                                            bolReporto = True
'                                            bolReporto = True
'                                        End If
'                                    Else
'                                        REM: YA SE REPORTO EL TERCERO CONDUCTOR
'                                        bolReporto = True
'                                    End If
'                                Else
'                                    REM: SE ENVIA EL TERCERO CONDUCTOR AL MINISTERIO
'                                    strErroresConductor = String.Empty
'                                    If Ambiente = Ambiantes_Desarrollos.LOCAL Then
'                                        If objConductor.FECHAVENCIMIENTOLICENCIA < Date.Now Then
'                                            objConductor.FECHAVENCIMIENTOLICENCIA = Format(Date.Now, General.FORMATO_FECHA_DIA_MES_ANO)
'                                        End If
'                                    End If
'                                    dblConductor = objConductor.Reportar_Tercero(strTextXmlConductor, Tipos_Procesos.FORMATO_INGRESO, strErroresConductor, lstVariablesConductor, Tipos_Roles.CONDUCTOR)
'                                    bolReporto = False
'                                    If (dblConductor > General.CERO And strErroresConductor = String.Empty) Or strErroresConductor.Contains(strTercDupl) Then
'                                        If dblConductor = General.CERO Then Double.TryParse(strErroresConductor.Split(" ")(0).Split(":")(1), dblConductor)
'                                        bolReporto = True
'                                    End If
'                                End If
'                            End If



'                            REM: ASIGNA UNA CONFIGURACION POR DEFECTO AL SEMIRREMOLQUE DE ACUERDO AL VEHICULO
'                            If objManifiestoMEC.NUMPLACAREMOLQUE <> String.Empty Then
'                                Select Case objVehiculo.CODCONFIGURACIONUNIDADCARGA
'                                    Case Tipos_Configuracion_Vehiculo.CAMIONRIGIDODE2EJES
'                                        objSemirremolque.CODCONFIGURACIONUNIDADCARGA = Tipos_Configuracion_Vehiculo.REMOLQUEDE3EJES
'                                    Case Tipos_Configuracion_Vehiculo.CAMIONRIGIDODE3EJES
'                                        objSemirremolque.CODCONFIGURACIONUNIDADCARGA = Tipos_Configuracion_Vehiculo.REMOLQUEBALANCEADODE3EJE
'                                    Case Tipos_Configuracion_Vehiculo.CAMIONRIGIDODE4EJES
'                                        objSemirremolque.CODCONFIGURACIONUNIDADCARGA = Tipos_Configuracion_Vehiculo.REMOLQUEDE3EJES
'                                    Case Tipos_Configuracion_Vehiculo.TRACTOCAMIONDE2EJES
'                                        objSemirremolque.CODCONFIGURACIONUNIDADCARGA = Tipos_Configuracion_Vehiculo.SEMIREMOLQUEDE3EJE
'                                    Case Tipos_Configuracion_Vehiculo.TRACTOCAMIONDE3EJES
'                                        objSemirremolque.CODCONFIGURACIONUNIDADCARGA = Tipos_Configuracion_Vehiculo.SEMIREMOLQUEDE3EJE
'                                End Select
'                            End If

'                            If bolReporto Then

'                                REM: VARIABLES DE VEHICULO
'                                Dim strErroresVehiculo As String = String.Empty
'                                Dim lstVariablesVehiculo As New List(Of String)
'                                Dim dblVehiculo As Double = General.CERO

'                                REM: CAMPOS DE CONSULTA DE EN EL VEHICULO
'                                lstVariablesVehiculo.Add("FECHAVENCIMIENTOSOAT".ToLower)
'                                lstVariablesVehiculo.Add("CODCONFIGURACIONUNIDADCARGA".ToLower)
'                                lstVariablesVehiculo.Add("PESOVEHICULOVACIO".ToLower)
'                                lstVariablesVehiculo.Add("CAPACIDADUNIDADCARGA".ToLower)

'                                REM: CONSULTAR VEHICULO EN EL MINISTERIO
'                                strErroresVehiculo = String.Empty
'                                Date.TryParse(objVehiculo.FECHAVENCIMIENTOSOAT, auxiliarFechaVencimiento)
'                                If auxiliarFechaVencimiento > DateAdd(DateInterval.Year, General.UNO, Date.Now) Then
'                                    REM: SI LA FECHA DE VENCIMINETO DEL SOAT ES MAYOR A UN AÑO ALA FECHA ACTUAL SE LE ASIGNA EL VALOR DE LA FECHA ACTUAL MAS UN AÑO
'                                    objVehiculo.FECHAVENCIMIENTOSOAT = Format(DateAdd(DateInterval.Year, General.UNO, Date.Now), General.FORMATO_FECHA_DIA_MES_ANO)
'                                End If
'                                dblVehiculo = objVehiculo.Reportar_Vehiculo(strTextXmlVehiculo, Tipos_Procesos.FORMATO_INGRESO, strErroresVehiculo, lstVariablesVehiculo)
'                                bolReporto = False
'                                If (dblVehiculo > General.CERO And strErroresVehiculo = String.Empty) Or strErroresVehiculo.Contains(strVehiDupl) Then
'                                    If dblVehiculo = General.CERO Then Double.TryParse(strErroresVehiculo.Split(" ")(0).Split(":")(1), dblVehiculo)
'                                    objVehiculo.Guardar_Vehiculo_Confirmacion(dblVehiculo, Me.objEmpresa.Codigo, objUsuario.Codigo, strErroresVehiculo)
'                                    bolReporto = True
'                                End If


'                                If bolReporto Then

'                                    REM: VARIABLES DE SEMIRREMOLQUE
'                                    Dim strErroresSemiRemo As String = String.Empty
'                                    Dim lstVariablesSemiRemo As New List(Of String)
'                                    Dim dblSemiRemo As Double = General.CERO

'                                    REM: VARIABLES PARA CONSULTAR EN EL MINISTERIO

'                                    lstVariablesSemiRemo.Add("CODCONFIGURACIONUNIDADCARGA".ToLower)
'                                    lstVariablesSemiRemo.Add("PESOVEHICULOVACIO".ToLower)


'                                    If IsNothing(objSemirremolque.NUMPLACA) Or objSemirremolque.NUMPLACA = String.Empty Then
'                                        REM: VERIFICA SI EL SEMIREMOLQUE TIENE PLACA, SI NO ES DEBIDO A QUE EL VEHICULO NO POSEE SEMIRREMOLQUE
'                                        bolReporto = True
'                                    Else
'                                        REM: ENVIAR SEMIRREMOLQUE AL MINISTERIO
'                                        strErroresSemiRemo = String.Empty
'                                        dblSemiRemo = objSemirremolque.Reportar_Semirremolque(strTextXmlRemolque, Tipos_Procesos.FORMATO_INGRESO, strErroresSemiRemo, lstVariablesSemiRemo)
'                                        bolReporto = False
'                                        If (dblSemiRemo > General.CERO And strErroresSemiRemo = String.Empty) Or strErroresSemiRemo.Contains(strVehiDupl) Then
'                                            If dblSemiRemo = General.CERO Then Double.TryParse(strErroresSemiRemo.Split(" ")(0).Split(":")(1), dblSemiRemo)
'                                            objSemirremolque.Guardar_Vehiculo_Confirmacion(dblSemiRemo, Me.objEmpresa.Codigo, objUsuario.Codigo, strErroresSemiRemo, VehiculoRNDC.enumTipoVehiculo.REMOLQUE)
'                                            bolReporto = True
'                                        End If
'                                    End If

'                                    If bolReporto Then
'                                        REM: VARIABLES DEL MANIFIESTO
'                                        Dim strErroresManifiesto As String = String.Empty
'                                        Dim lstVariablesManifiesto As New List(Of String)
'                                        Dim dblManifiesto As Double = General.CERO

'                                        Me.Retornar_Remesas(lonNumeroManifiesto, objManifiestoMEC.CONSECUTIVOREMESA)
'                                        dblManifiesto = objManifiestoMEC.Reportar_Manifiesto(strTextXmlManifiesto, Tipos_Procesos.FORMATO_INGRESO, strErroresManifiesto, lstVariablesManifiesto)
'                                        If (dblManifiesto > General.CERO And strErroresManifiesto = String.Empty) Or strErroresManifiesto.Contains(strManiDupl) Then
'                                            If dblManifiesto = General.CERO Then Double.TryParse(strErroresManifiesto.Split(" ")(0).Split(":")(1), dblManifiesto)
'                                            lonCodigoConfirmacion = dblManifiesto
'                                            intEstadoReporteDocumento = EnumEstadoReporteDocumentos.REPORTADO
'                                            strMensaje = lonCodigoConfirmacion
'                                        Else
'                                            REM: sI EL MANIFIESTO NO REPORTA INFORMACION RETORNA CERO LA FUNCTION Y EL MENSAJE DE ERROR
'                                            lonCodigoConfirmacion = General.CERO
'                                            intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                                            strMensaje = strErroresManifiesto
'                                        End If

'                                    Else
'                                        lonCodigoConfirmacion = General.CERO
'                                        intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                                        strMensaje = strErroresSemiRemo
'                                        strTextXmlManifiesto = "Debe reportar  primero el Semiremolque para continuar."
'                                    End If
'                                Else
'                                    lonCodigoConfirmacion = General.CERO
'                                    intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                                    strMensaje = strErroresVehiculo
'                                    strTextXmlRemolque = "Debe reportar primero el Vehiculo para continuar."
'                                    strTextXmlManifiesto = "Debe reportar  primero el Vehiculo/Semiremolque para continuar."
'                                End If
'                            Else
'                                lonCodigoConfirmacion = General.CERO
'                                intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                                strMensaje = "Conductor: " & strErroresConductor
'                                strTextXmlVehiculo = "Debe reportar primero el Conductor para continuar."
'                                strTextXmlRemolque = "Debe reportar primero el Conductor/Vehiculo para continuar."
'                                strTextXmlManifiesto = "Debe reportar  primero el Conductor/Vehiculo/Semiremolque para continuar."
'                            End If
'                        Else
'                            lonCodigoConfirmacion = General.CERO
'                            intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                            strMensaje = "Propietario: " & strErroresPropietario
'                            strTextXmlConductor = "Debe reportar Primero el propietario para continuar."
'                            strTextXmlVehiculo = "Debe reportar primero el propietario/Conductor para continuar."
'                            strTextXmlRemolque = "Debe reportar primero el propietario/Conductor/Vehiculo para continuar."
'                            strTextXmlManifiesto = "Debe reportar  primero el propietario/Conductor/Vehiculo/Semiremolque para continuar."
'                        End If
'                    Else
'                        lonCodigoConfirmacion = General.CERO
'                        intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                        strMensaje = "Tenedor: " & strErroresTenedor
'                        strTextoXmlPropietario = "Debre reportar primero el Tenedor."
'                        strTextXmlConductor = "Debe reportar Primero el tenedor/propietario para continuar."
'                        strTextXmlVehiculo = "Debe reportar primero el tenedor/propietario/Conductor para continuar."
'                        strTextXmlRemolque = "Debe reportar primero el tenedor/propietario/Conductor/Vehiculo para continuar."
'                        strTextXmlManifiesto = "Debe reportar  primero el tenedor/propietario/Conductor/Vehiculo/Semiremolque para continuar."
'                    End If
'                Else
'                    strMensaje = "Por favor reportar las remesas asociadas al manifiesto (Remesa/s Número " & strRemesasPendientes & "), para proceder a reportar el mismo"
'                End If
'            Catch ex As Exception
'                intEstadoReporteDocumento = EnumEstadoReporteDocumentos.NOREPORTADO
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

'            Finally
'                Actualizar_Numero_Confirmacion_Manifiesto_Ministerio(lonNumeroManifiesto, strMensaje)
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Sub

'        Public Sub Proceso_Consultar_Flete_Minimo_Sicetac(ByVal lonNumeroManifiesto As Long)
'            Try
'                Dim objManifiestoMEC As New ManifiestoRNDC(acceso, solicitudConsulta)

'                Dim dteAuxiliar As Date = Date.MinValue
'                Dim strAuxiliar As String = String.Empty
'                Dim dblAuxiliar As Double = General.CERO
'                Dim dblToneldaManfifiesto As Double = General.CERO
'                Dim bolReporto As Boolean = True
'                Dim auxiliarFechaVencimiento As Date = Date.Now
'                Dim strCodigoMinisterioCiudadOrigen As String = String.Empty
'                Dim strCodigoMinisterioCiudadDestino As String = String.Empty

'                Dim strCodigoVehiculo As String = String.Empty
'                Dim strConfiguracionVehiculo As String = String.Empty
'                Dim strNumeroEjesVehiculo As String = String.Empty

'                Me.objGeneral.ComandoSQL = New SqlCommand


'                Dim strTextoXmlSICETAC = String.Empty

'                strSQL = "SELECT *"
'                strSQL += " FROM Encabezado_Manifiestos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & lonNumeroManifiesto

'                Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
'                Me.objGeneral.ComandoSQL.CommandText = strSQL
'                Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.CadenaDeConexionSQL)
'                If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
'                    Me.objGeneral.ComandoSQL.Connection.Open()
'                End If

'                Dim sdrManifiesto As SqlDataReader = Me.objGeneral.ComandoSQL.ExecuteReader

'                While sdrManifiesto.Read

'                    dblToneldaManfifiesto = sdrManifiesto("Peso_Total")
'                    dblToneldaManfifiesto = dblToneldaManfifiesto / 1000

'                    Dim arrCampos(1), arrValoresCampos(1) As String
'                    arrCampos(0) = "CIUD_Origen"
'                    arrCampos(1) = "CIUD_Destino"

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Rutas", arrCampos, arrValoresCampos, arrCampos.Length, General.CAMPO_NUMERICO, "Codigo", sdrManifiesto("RUTA_Codigo"), "", Me.strError) Then
'                        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Ciudades", "Codigo_Alterno", "Codigo", General.CAMPO_NUMERICO, arrValoresCampos(0).ToString, strCodigoMinisterioCiudadOrigen)
'                        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Ciudades", "Codigo_Alterno", "Codigo", General.CAMPO_NUMERICO, arrValoresCampos(1).ToString, strCodigoMinisterioCiudadDestino)
'                    End If

'                    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Valor_Catalogos", "Campo5", "Campo1", General.CAMPO_NUMERICO, sdrManifiesto("TIVE_Codigo").ToString, strCodigoVehiculo, "CATA_Codigo = 'TIVE'")

'                    If strCodigoVehiculo = VehiculoRNDC.CABEZOTE_DOS_EJES Or strCodigoVehiculo = VehiculoRNDC.CABEZOTE_TRES_EJES Then
'                        strConfiguracionVehiculo = "CS"
'                    Else
'                        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Valor_Catalogos", "Campo6", "Campo2", General.CAMPO_NUMERICO, strCodigoVehiculo, strNumeroEjesVehiculo, "CATA_Codigo = 'PCMT'")
'                        strNumeroEjesVehiculo = Mid(strNumeroEjesVehiculo, 1, 1)
'                        If Val(strNumeroEjesVehiculo) = General.DOS Then
'                            strConfiguracionVehiculo = "2"
'                        ElseIf Val(strNumeroEjesVehiculo) > General.DOS Then
'                            strConfiguracionVehiculo = "3"
'                        End If
'                    End If

'                End While

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'                REM: realiza Cambio de Enlace para reportar información al Ministerio
'                Select Case enumEnlacesMinisterio
'                    Case EnumEnlaceServicioMinisterio.PRIMER_ENLACE_MINISTERIO_TRANSPORTE
'                        objManifiestoMEC.EnlaceMinisterio = strPrimerEnlaceWebServiceMinisterio
'                    Case EnumEnlaceServicioMinisterio.SEGUNDO_ENLACE_MINISTERIO_TRANSPORTE
'                        objManifiestoMEC.EnlaceMinisterio = strSegundoEnlaceWebServiceMinisterio
'                End Select

'                Dim strErrores As String = String.Empty
'                Dim lstVariablesSiceTac As New List(Of String)
'                Dim dblValorMinimoSicetac As Double = General.CERO

'                objManifiestoMEC.NUMNITEMPRESATRANSPORTE = Me.objEmpresa.NumeroIdentificacion

'                lstVariablesSiceTac.Add("VALOR".ToLower)
'                lstVariablesSiceTac.Add("VALORTONELADA".ToLower)
'                lstVariablesSiceTac.Add("DISTANCIA".ToLower)

'                REM:VARIABLES PARA RETORNAR VALORES DE SICETAC
'                objManifiestoMEC.PERIODO = "'" & Me.strPeriodoConsultaSiceTac & "'"
'                objManifiestoMEC.MODELO = "'" & "2" & "'"
'                objManifiestoMEC.ORIGEN = strCodigoMinisterioCiudadOrigen & "000"
'                objManifiestoMEC.DESTINO = strCodigoMinisterioCiudadDestino & "000"

'                objManifiestoMEC.CONFIGURACION = "'" & strConfiguracionVehiculo & "'"


'                strErrores = String.Empty
'                dblValorMinimoSicetac = objManifiestoMEC.Consultar_Flete_Minimo_Sicetac(strTextoXmlSICETAC, Tipos_Procesos.FORTMATO_CONSULTA_SICETAC, strErrores, lstVariablesSiceTac, Tipos_Roles.TENEDOR)
'                If dblValorMinimoSicetac > General.CERO And strErrores = String.Empty Then
'                    dblValorMinimoSicetac = Val(lstVariablesSiceTac(1).ToString) * dblToneldaManfifiesto
'                    strMensaje = dblValorMinimoSicetac
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

'            Finally
'                Actualizar_Flete_Minimo_Manifiesto_Ministerio(lonNumeroManifiesto, strMensaje)
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Sub



'        Public Function Remesas_Pendientes_X_Reporar_Manifiesto(ByVal TipoDocumentoOrigen As Integer, ByVal lonNumeroDocumento As Long, ByRef strRemesasPendientes As String) As Boolean
'            Try

'                Dim dtsRemesas As New DataSet
'                Dim intCont As Integer
'                Remesas_Pendientes_X_Reporar_Manifiesto = True
'                strRemesasPendientes = String.Empty

'                'Guias de la planilla que se va a liquidar
'                strSQL = "SELECT ENRE.Numero_Documento "
'                strSQL += " FROM Encabezado_Remesas ENRE, Rutas RUTA"
'                strSQL += " WHERE ENRE.EMPR_Codigo = RUTA.EMPR_codigo"
'                strSQL += " AND ENRE.RUTA_Codigo = RUTA.Codigo"

'                strSQL += " AND ENRE.EMPR_Codigo = " & Me.objEmpresa.Codigo

'                If TipoDocumentoOrigen = TipoDocumentos.TIDO_MANIFIESTO Then
'                    strSQL += " AND ENRE.ENMA_Numero = " & lonNumeroDocumento
'                End If

'                If TipoDocumentoOrigen = TipoDocumentos.TIDO_CUMPLIDO Then
'                    strSQL += " AND ENRE.ENCU_Numero = " & lonNumeroDocumento
'                End If

'                strSQL += " AND ENRE.TIRE_Codigo <> " & TIPO_REMESA_URBANA 'JV: Se excluyen las remesas urbanas
'                strSQL += " AND ENRE.Numero_Confirmacion_Ministerio = 0"
'                strSQL += " AND RUTA.CIUD_Origen <> RUTA.CIUD_Destino"

'                dtsRemesas = Me.objGeneral.Retorna_Dataset(strSQL)

'                If dtsRemesas.Tables(Lista.PRIMERA_TABLA).Rows.Count > General.CERO Then
'                    For intCont = 0 To dtsRemesas.Tables(Lista.PRIMERA_TABLA).Rows.Count - 1
'                        If intCont = General.CERO Then
'                            Remesas_Pendientes_X_Reporar_Manifiesto = True
'                            strRemesasPendientes = dtsRemesas.Tables(Lista.PRIMERA_TABLA).Rows(intCont).Item("Numero_Documento").ToString()
'                        Else
'                            strRemesasPendientes += ", " & dtsRemesas.Tables(Lista.PRIMERA_TABLA).Rows(intCont).Item("Numero_Documento").ToString()
'                        End If
'                    Next
'                Else
'                    Remesas_Pendientes_X_Reporar_Manifiesto = False
'                End If

'                dtsRemesas.Dispose()

'            Catch ex As Exception
'                Remesas_Pendientes_X_Reporar_Manifiesto = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function


'        Public Function Manifiestos_Pendientes_X_Reportar_Manifiesto(ByVal TipoDocumentoOrigen As Integer, ByVal lonNumeroDocumento As Long, ByRef strManifiestosPendientes As String) As Boolean
'            Try

'                Dim dtsRemesas As New DataSet
'                Dim intCont As Integer
'                Manifiestos_Pendientes_X_Reportar_Manifiesto = True
'                strManifiestosPendientes = String.Empty

'                'Guias de la planilla que se va a liquidar
'                strSQL = "SELECT Numero "
'                strSQL += " FROM Encabezado_Manifiestos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo

'                If TipoDocumentoOrigen = TipoDocumentos.TIDO_CUMPLIDO Then
'                    strSQL += " AND ENCU_Numero = " & lonNumeroDocumento
'                End If

'                strSQL += " AND Numero_Manifiesto_Electronico = 0"


'                dtsRemesas = Me.objGeneral.Retorna_Dataset(strSQL)

'                If dtsRemesas.Tables(Lista.PRIMERA_TABLA).Rows.Count > General.CERO Then
'                    For intCont = 0 To dtsRemesas.Tables(Lista.PRIMERA_TABLA).Rows.Count - 1
'                        If intCont = General.CERO Then
'                            Manifiestos_Pendientes_X_Reportar_Manifiesto = True
'                            strManifiestosPendientes = dtsRemesas.Tables(Lista.PRIMERA_TABLA).Rows(intCont).Item("Numero").ToString()
'                        Else
'                            strManifiestosPendientes += ", " & dtsRemesas.Tables(Lista.PRIMERA_TABLA).Rows(intCont).Item("Numero").ToString()
'                        End If
'                    Next
'                Else
'                    Manifiestos_Pendientes_X_Reportar_Manifiesto = False
'                End If

'                dtsRemesas.Dispose()

'            Catch ex As Exception
'                Manifiestos_Pendientes_X_Reportar_Manifiesto = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        ''' <summary>
'        ''' Actualiza el numero de confirmacion o el error presentado al momento de reportar documentos al MInisterio de transporte.
'        ''' </summary>
'        ''' <param name="lonNumero">Codigo del Manifiesto</param>
'        ''' <param name="strMensaje">Mensaje de error/Numero confirmacion Ministerio</param>
'        ''' <returns>Boolean</returns>
'        ''' <remarks></remarks>
'        Public Function Actualizar_Numero_Confirmacion_Manifiesto_Ministerio(ByVal lonNumero As Long, ByVal strMensaje As String) As Boolean
'            Try
'                If IsNumeric(strMensaje) Then
'                    Me.strSQL = "UPDATE Encabezado_Manifiestos SET Numero_Manifiesto_Electronico = " & Val(strMensaje)
'                    Me.strSQL += ", Mensaje_Manifiesto_Electronico = '" & Mid(strMensaje, 1, 500) & "'"
'                    Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    Me.strSQL += " AND Numero = " & lonNumero
'                Else
'                    Me.strSQL = "UPDATE Encabezado_Manifiestos SET Numero_Manifiesto_Electronico = " & Val(0)
'                    Me.strSQL += ", Mensaje_Manifiesto_Electronico = '" & Mid(strMensaje, 1, 500) & "'"
'                    Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    Me.strSQL += " AND Numero = " & lonNumero
'                End If

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.Ejecutar_SQL_Misma_Transaccion(Me.strSQL, Me.strError)
'                    Actualizar_Numero_Confirmacion_Manifiesto_Ministerio = True
'                Else
'                    Me.objGeneral.Ejecutar_SQL(Me.strSQL, Me.strError)
'                    Actualizar_Numero_Confirmacion_Manifiesto_Ministerio = True
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

'                Actualizar_Numero_Confirmacion_Manifiesto_Ministerio = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Actualizar_Numero_Confirmacion_Anulacion_Cumplido(ByVal lonNumeroCumplido As Long, ByVal strMensaje As String) As Boolean
'            Try
'                If IsNumeric(strMensaje) Then
'                    Me.strSQL = "UPDATE Encabezado_Cumplidos SET Numero_Confirmacion_Anulacion_Ministerio = " & Val(strMensaje)
'                    Me.strSQL += ", Mensaje_Error_Reporte_Ministerio = '" & Mid(strMensaje, 1, 500) & "'"
'                    Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    Me.strSQL += " AND Numero = " & lonNumeroCumplido
'                Else
'                    Me.strSQL = "UPDATE Encabezado_Cumplidos SET Numero_Confirmacion_Anulacion_Ministerio = " & Val(0)
'                    Me.strSQL += ", Mensaje_Error_Reporte_Ministerio = '" & Mid(strMensaje, 1, 500) & "'"
'                    Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    Me.strSQL += " AND Numero = " & lonNumeroCumplido
'                End If

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.Ejecutar_SQL_Misma_Transaccion(Me.strSQL, Me.strError)
'                    Actualizar_Numero_Confirmacion_Anulacion_Cumplido = True
'                Else
'                    Me.objGeneral.Ejecutar_SQL(Me.strSQL, Me.strError)
'                    Actualizar_Numero_Confirmacion_Anulacion_Cumplido = True
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

'                Actualizar_Numero_Confirmacion_Anulacion_Cumplido = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Actualizar_Flete_Minimo_Manifiesto_Ministerio(ByVal lonNumero As Long, ByVal strMensaje As String) As Boolean
'            Try
'                If IsNumeric(strMensaje) Then
'                    Me.strSQL = "UPDATE Encabezado_Manifiestos SET Valor_Flete_Neto_Mini_Tran = " & Val(strMensaje)
'                    Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    Me.strSQL += " AND Numero = " & lonNumero
'                Else
'                    Me.strSQL = "UPDATE Encabezado_Manifiestos SET Valor_Flete_Neto_Mini_Tran = " & Val(0)
'                    Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    Me.strSQL += " AND Numero = " & lonNumero
'                End If

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.Ejecutar_SQL_Misma_Transaccion(Me.strSQL, Me.strError)
'                    Actualizar_Flete_Minimo_Manifiesto_Ministerio = True
'                Else
'                    Me.objGeneral.Ejecutar_SQL(Me.strSQL, Me.strError)
'                    Actualizar_Flete_Minimo_Manifiesto_Ministerio = True
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

'                Actualizar_Flete_Minimo_Manifiesto_Ministerio = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Actualizar_Numero_Confirmacion_Manifiesto_Ministerio_Detalle_Despacho(ByVal lonNumero As Long, ByVal strMensaje As String) As Boolean
'            Try
'                If IsNumeric(strMensaje) Then
'                    Me.strSQL = "UPDATE Detalle_Despacho_Contrato_Clientes SET Numero_Manifiesto_Ministerio = " & Val(strMensaje)
'                    Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    Me.strSQL += " AND Numero = " & lonNumero
'                Else
'                    Me.strSQL = "UPDATE Detalle_Despacho_Contrato_Clientes SET Numero_Manifiesto_Ministerio = " & Val(0)
'                    Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    Me.strSQL += " AND Numero = " & lonNumero
'                End If


'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.Ejecutar_SQL_Misma_Transaccion(Me.strSQL, Me.strError)
'                    Actualizar_Numero_Confirmacion_Manifiesto_Ministerio_Detalle_Despacho = True
'                Else
'                    Me.objGeneral.Ejecutar_SQL(Me.strSQL, Me.strError)
'                    Actualizar_Numero_Confirmacion_Manifiesto_Ministerio_Detalle_Despacho = True
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

'                Actualizar_Numero_Confirmacion_Manifiesto_Ministerio_Detalle_Despacho = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'#End Region

'#Region "Funciones Publicas"

'        Public Function LimpiaCarateresEspeciales(ByVal strCadenaLimpiar As String) As String
'            Try
'                Dim strCadenaCoreguida As String = String.Empty
'                For i As Integer = 1 To strCadenaLimpiar.Length
'                    Dim AuxiliarChar As Char = Mid(strCadenaLimpiar, i, i + 1)
'                    If (Convert.ToInt32(AuxiliarChar) >= 65 And Convert.ToInt32(AuxiliarChar) <= 90) Or
'                    (Convert.ToInt32(AuxiliarChar) >= 97 And Convert.ToInt32(AuxiliarChar) <= 122) Or
'                    Convert.ToInt32(AuxiliarChar) = 32 Or (Convert.ToInt32(AuxiliarChar) >= 48 And Convert.ToInt32(AuxiliarChar) <= 57) Or
'                    Convert.ToInt32(AuxiliarChar) = 35 Then
'                        strCadenaCoreguida &= AuxiliarChar
'                    End If
'                Next
'                strCadenaLimpiar = strCadenaCoreguida
'                Return strCadenaLimpiar
'            Catch ex As Exception
'                Return strCadenaLimpiar
'                Exit Try
'            End Try
'        End Function

'        Public Function Reportar_Cumplido_Remesa(ByVal lonNumeroCumplido As Long, ByRef strError As String, ByRef TextoXmlCumplidoRemesa As String, ByRef intNumRemesas As Integer) As Double

'            Dim strMensajeMinisterio As String = ""

'            Try

'                Reportar_Cumplido_Remesa = General.CERO
'                Dim objCumplidoRemesa As New RemesaRNDC(acceso, solicitudConsulta)
'                Dim dteAxiliar As Date = Date.MinValue
'                Dim bolConsultaDatos As Boolean = False
'                Dim lonNumeroRemesa As Long = 0
'                Dim strRemesasPendientes As String = String.Empty
'                Dim strSQL As String = String.Empty

'                Dim dtsDataset As New DataSet

'                If Not Remesas_Pendientes_X_Reporar_Manifiesto(TipoDocumentos.TIDO_CUMPLIDO, lonNumeroCumplido, strRemesasPendientes) Then
'                    strSQL = "SELECT *"
'                    strSQL += " FROM V_Informacion_Carga"
'                    strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    strSQL += " AND ENCU_Numero = " & lonNumeroCumplido

'                    dtsDataset = Me.objGeneral.Retorna_Dataset(strSQL, strError)

'                    If Not IsNothing(dtsDataset) Then
'                        If dtsDataset.Tables(0).Rows.Count > General.CERO Then
'                            For i = 0 To (dtsDataset.Tables(0).Rows.Count - 1)
'                                If Val(dtsDataset.Tables(0).Rows(i).Item("Numero_Confirmacion_Cumplido_Ministerio")) = General.CERO Then

'                                    objCumplidoRemesa.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion

'                                    objCumplidoRemesa.CONSECUTIVOREMESA = dtsDataset.Tables(0).Rows(i).Item("Numero_Documento").ToString

'                                    objCumplidoRemesa.NUMMANIFIESTOCARGA = dtsDataset.Tables(0).Rows(i).Item("ENMA_Numero").ToString
'                                    objCumplidoRemesa.TIPOCUMPLIDOREMESA = dtsDataset.Tables(0).Rows(i).Item("TipoCumplido").ToString
'                                    objCumplidoRemesa.CANTIDADCARGADA = Val(dtsDataset.Tables(0).Rows(i).Item("Cantidad")).ToString
'                                    objCumplidoRemesa.CANTIDADENTREGADA = Val(dtsDataset.Tables(0).Rows(i).Item("Cantidad")).ToString
'                                    objCumplidoRemesa.UNIDADMEDIDACAPACIDAD = dtsDataset.Tables(0).Rows(i).Item("CodigoUnidadMedida").ToString
'                                    Date.TryParse(dtsDataset.Tables(0).Rows(i).Item("Fecha_Inicio_Cargue").ToString, dteAxiliar)
'                                    objCumplidoRemesa.FECHALLEGADACARGUE = dteAxiliar.Date
'                                    objCumplidoRemesa.HORALLEGADACARGUEREMESA = General.HORA_DEFECTO
'                                    objCumplidoRemesa.FECHAENTRADACARGUE = dteAxiliar.Date
'                                    objCumplidoRemesa.HORAENTRADACARGUEREMESA = General.HORA_DEFECTO
'                                    Date.TryParse(dtsDataset.Tables(0).Rows(i).Item("Fecha_Fin_Cargue").ToString, dteAxiliar)
'                                    objCumplidoRemesa.FECHASALIDACARGUE = dteAxiliar.Date
'                                    objCumplidoRemesa.HORASALIDACARGUEREMESA = General.HORA_DEFECTO


'                                    Dim arrCampos(3), arrValoresCampos(3) As String
'                                    arrCampos(0) = "Fecha_Entrega"
'                                    arrCampos(1) = "Hora_Entrada_Descargue"
'                                    arrCampos(2) = "Hora_Salida_Descargue"
'                                    arrCampos(3) = "Observaciones"
'                                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Encabezado_cumplidos", arrCampos, arrValoresCampos, 4, General.CAMPO_NUMERICO, "Numero", dtsDataset.Tables(0).Rows(i).Item("ENCU_Numero").ToString, "", Me.strError) Then
'                                        objCumplidoRemesa.FECHALLEGADADESCARGUE = arrValoresCampos(0)
'                                        If Replace(arrValoresCampos(1), " ", "") <> String.Empty Then
'                                            objCumplidoRemesa.HORALLEGADADESCARGUECUMPLIDO = General.HORA_DEFECTO_30_MIN
'                                        Else
'                                            objCumplidoRemesa.HORALLEGADADESCARGUECUMPLIDO = General.HORA_DEFECTO_30_MIN
'                                        End If

'                                        Date.TryParse(arrValoresCampos(0), dteAxiliar)
'                                        objCumplidoRemesa.FECHAENTRADADESCARGUE = dteAxiliar.Date

'                                        If Replace(arrValoresCampos(1), " ", "") <> String.Empty Then
'                                            objCumplidoRemesa.HORAENTRADADESCARGUECUMPLIDO = General.HORA_DEFECTO_60_MIN
'                                        Else
'                                            objCumplidoRemesa.HORAENTRADADESCARGUECUMPLIDO = General.HORA_DEFECTO_60_MIN
'                                        End If

'                                        objCumplidoRemesa.FECHASALIDADESCARGUE = dteAxiliar.Date
'                                        If Replace(arrValoresCampos(2), " ", "") <> String.Empty Then
'                                            objCumplidoRemesa.HORASALIDADESCARGUECUMPLIDO = General.HORA_DEFECTO_90_MIN
'                                        Else
'                                            objCumplidoRemesa.HORASALIDADESCARGUECUMPLIDO = General.HORA_DEFECTO_90_MIN
'                                        End If

'                                        'Se eliminan las comillas dobles de las observaciones para evitar errores en el reporte
'                                        arrValoresCampos(3) = Replace(arrValoresCampos(3), """", "")
'                                        objCumplidoRemesa.OBSERVACIONES = Mid(arrValoresCampos(3), 1, 199)
'                                    End If

'                                    If lonNumeroRemesa = Val(dtsDataset.Tables(0).Rows(i).Item("Numero_Documento").ToString) Then
'                                        REM: La remesa ya fue reportada
'                                    Else
'                                        lonNumeroRemesa = Val(dtsDataset.Tables(0).Rows(i).Item("Numero_Documento").ToString)

'                                        strMensajeMinisterio = String.Empty
'                                        Dim dblNumeroConfirmacion As Double = General.CERO

'                                        dblNumeroConfirmacion = objCumplidoRemesa.Reportar_Cumplido_Remesa(TextoXmlCumplidoRemesa, strMensajeMinisterio, Tipos_Procesos.FORMATO_INGRESO)
'                                        'If (dblNumeroConfirmacion > General.CERO And strError <> String.Empty) Or strError.Contains(strCumpRemeDupli) Then
'                                        '    If dblNumeroConfirmacion = General.CERO Then Double.TryParse(strError.Split(" ")(0).Split(":")(1), dblNumeroConfirmacion)
'                                        'End If



'                                        If strMensajeMinisterio.Contains("DUPLICADO") Then

'                                            Double.TryParse(strMensajeMinisterio.Split(" ")(0).Split(":")(1), dblNumeroConfirmacion)
'                                        ElseIf strMensajeMinisterio.Contains(strCumpRemeDupli) Then
'                                            dblNumeroConfirmacion = 1
'                                        End If

'                                        If dblNumeroConfirmacion > General.CERO Then
'                                            strSQL = " UPDATE Encabezado_Remesas SET Numero_Confirmacion_Cumplido_Ministerio = " & dblNumeroConfirmacion & ","
'                                            strSQL &= " Mensaje_Error_Reporte_Ministerio = '" & dblNumeroConfirmacion & "' WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo & " AND Numero = " & Val(dtsDataset.Tables(0).Rows(i).Item("Numero").ToString) & ";"
'                                            Reportar_Cumplido_Remesa = dblNumeroConfirmacion
'                                        Else
'                                            strSQL = " UPDATE Encabezado_Remesas SET Numero_Confirmacion_Cumplido_Ministerio = 0 ,"
'                                            strSQL &= " Mensaje_Error_Reporte_Ministerio = '" & Mid(strMensajeMinisterio, 1, 499) & "' WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo & " AND Numero = " & Val(dtsDataset.Tables(0).Rows(i).Item("Numero").ToString) & ";"
'                                        End If
'                                    End If

'                                Else
'                                    Reportar_Cumplido_Remesa = Val(dtsDataset.Tables(0).Rows(i).Item("Numero_Confirmacion_Cumplido_Ministerio"))
'                                    'strError &= "La remesa ya cuenta con número de confirmación cumplido del ministerio en GESTRANS"

'                                End If
'                            Next
'                            bolConsultaDatos = True

'                        End If
'                    End If
'                Else
'                    strError = "Las remesas asociadas al cumplido (Remesa/s Número " & strRemesasPendientes & "), no se encuentran reportadas, por favor reportarlas."
'                End If



'                If strSQL <> String.Empty Then
'                    Me.objGeneral.Ejecutar_SQL(strSQL, strError)
'                End If

'                If Not bolConsultaDatos Then
'                    strMensajeMinisterio = "GESTRANS: No se puede enlazar informacion para reportar.(Enviar a soporte)"
'                End If

'            Catch ex As Exception
'                Reportar_Cumplido_Remesa = General.CERO

'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                strError += " - En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'                strError = strMensajeMinisterio + strError
'            End Try
'        End Function

'        Public Function Reportar_Cumplido_Remesa_Desde_Remesa(ByVal lonNumeroRemesa As Long, ByRef strError As String, ByRef TextoXmlCumplidoRemesa As String) As Double

'            Dim strMensajeMinisterio As String = ""

'            Try

'                Reportar_Cumplido_Remesa_Desde_Remesa = General.CERO
'                Dim objCumplidoRemesa As New RemesaRNDC(acceso, solicitudConsulta)
'                Dim dteAxiliar As Date = Date.MinValue
'                Dim bolConsultaDatos As Boolean = False
'                Dim strSQL As String = String.Empty

'                Dim dtsDataset As New DataSet

'                strSQL = "SELECT *"
'                strSQL += " FROM V_Informacion_Carga"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & lonNumeroRemesa

'                dtsDataset = Me.objGeneral.Retorna_Dataset(strSQL, strError)

'                If Not IsNothing(dtsDataset) Then
'                    If dtsDataset.Tables(0).Rows.Count > General.CERO Then

'                        If Val(dtsDataset.Tables(0).Rows(0).Item("Numero_Confirmacion_Cumplido_Ministerio")) = General.CERO Then
'                            objCumplidoRemesa.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion

'                            objCumplidoRemesa.CONSECUTIVOREMESA = dtsDataset.Tables(0).Rows(0).Item("Numero_Documento").ToString

'                            objCumplidoRemesa.NUMMANIFIESTOCARGA = dtsDataset.Tables(0).Rows(0).Item("ENMA_Numero").ToString
'                            objCumplidoRemesa.TIPOCUMPLIDOREMESA = dtsDataset.Tables(0).Rows(0).Item("TipoCumplido").ToString
'                            objCumplidoRemesa.CANTIDADCARGADA = Val(dtsDataset.Tables(0).Rows(0).Item("Cantidad")).ToString
'                            objCumplidoRemesa.CANTIDADENTREGADA = Val(dtsDataset.Tables(0).Rows(0).Item("Cantidad")).ToString
'                            objCumplidoRemesa.UNIDADMEDIDACAPACIDAD = dtsDataset.Tables(0).Rows(0).Item("CodigoUnidadMedida").ToString
'                            Date.TryParse(dtsDataset.Tables(0).Rows(0).Item("Fecha_Inicio_Cargue").ToString, dteAxiliar)
'                            objCumplidoRemesa.FECHALLEGADACARGUE = dteAxiliar.Date
'                            objCumplidoRemesa.HORALLEGADACARGUEREMESA = dteAxiliar.ToString("HH:mm")
'                            objCumplidoRemesa.FECHAENTRADACARGUE = dteAxiliar.Date
'                            objCumplidoRemesa.HORAENTRADACARGUEREMESA = dteAxiliar.ToString("HH:mm")
'                            Date.TryParse(dtsDataset.Tables(0).Rows(0).Item("Fecha_Fin_Cargue").ToString, dteAxiliar)
'                            objCumplidoRemesa.FECHASALIDACARGUE = dteAxiliar.Date
'                            objCumplidoRemesa.HORASALIDACARGUEREMESA = dteAxiliar.ToString("HH:mm")


'                            Dim arrCampos(3), arrValoresCampos(3) As String
'                            arrCampos(0) = "Fecha_Entrega"
'                            arrCampos(1) = "Hora_Entrada_Descargue"
'                            arrCampos(2) = "Hora_Salida_Descargue"
'                            arrCampos(3) = "Observaciones"
'                            If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Encabezado_cumplidos", arrCampos, arrValoresCampos, 4, General.CAMPO_NUMERICO, "Numero", dtsDataset.Tables(0).Rows(0).Item("ENCU_Numero").ToString, "", Me.strError) Then
'                                objCumplidoRemesa.FECHALLEGADADESCARGUE = arrValoresCampos(0)
'                                If Replace(arrValoresCampos(1), " ", "") <> String.Empty Then
'                                    objCumplidoRemesa.HORALLEGADADESCARGUECUMPLIDO = Mid(arrValoresCampos(1), General.UNO, 5)
'                                Else
'                                    objCumplidoRemesa.HORALLEGADADESCARGUECUMPLIDO = General.HORA_DEFECTO
'                                End If

'                                Date.TryParse(arrValoresCampos(0), dteAxiliar)
'                                objCumplidoRemesa.FECHAENTRADADESCARGUE = dteAxiliar.Date

'                                If Replace(arrValoresCampos(1), " ", "") <> String.Empty Then
'                                    objCumplidoRemesa.HORAENTRADADESCARGUECUMPLIDO = Mid(arrValoresCampos(1), General.UNO, 5)
'                                Else
'                                    objCumplidoRemesa.HORAENTRADADESCARGUECUMPLIDO = General.HORA_DEFECTO
'                                End If

'                                objCumplidoRemesa.FECHASALIDADESCARGUE = dteAxiliar.Date
'                                If Replace(arrValoresCampos(2), " ", "") <> String.Empty Then
'                                    objCumplidoRemesa.HORASALIDADESCARGUECUMPLIDO = Mid(arrValoresCampos(2), General.UNO, 5)
'                                Else
'                                    objCumplidoRemesa.HORASALIDADESCARGUECUMPLIDO = General.HORA_DEFECTO
'                                End If

'                                objCumplidoRemesa.OBSERVACIONES = Mid(arrValoresCampos(3), 1, 199)
'                            End If

'                            strMensajeMinisterio = String.Empty
'                            Dim dblNumeroConfirmacion As Double = General.CERO

'                            dblNumeroConfirmacion = objCumplidoRemesa.Reportar_Cumplido_Remesa(TextoXmlCumplidoRemesa, strMensajeMinisterio, Tipos_Procesos.FORMATO_INGRESO)
'                            'If (dblNumeroConfirmacion > General.CERO And strError <> String.Empty) Or strError.Contains(strCumpRemeDupli) Then
'                            '    If dblNumeroConfirmacion = General.CERO Then Double.TryParse(strError.Split(" ")(0).Split(":")(1), dblNumeroConfirmacion)
'                            'End If
'                            If strMensajeMinisterio.Contains("DUPLICADO") Then

'                                Double.TryParse(strMensajeMinisterio.Split(" ")(0).Split(":")(1), dblNumeroConfirmacion)
'                            ElseIf strMensajeMinisterio.Contains(strCumpRemeDupli) Then
'                                dblNumeroConfirmacion = 1
'                            End If

'                            If dblNumeroConfirmacion > General.CERO Then
'                                strSQL = " UPDATE Encabezado_Remesas SET Numero_Confirmacion_Cumplido_Ministerio = " & dblNumeroConfirmacion & ","
'                                strSQL &= " Mensaje_Error_Reporte_Ministerio = '" & dblNumeroConfirmacion & "' WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo & " AND Numero = " & Val(dtsDataset.Tables(0).Rows(0).Item("Numero").ToString) & ";"
'                                Reportar_Cumplido_Remesa_Desde_Remesa = dblNumeroConfirmacion
'                            Else
'                                strSQL = " UPDATE Encabezado_Remesas SET Numero_Confirmacion_Cumplido_Ministerio = 0 ,"
'                                strSQL &= " Mensaje_Error_Reporte_Ministerio = '" & Mid(strMensajeMinisterio, 1, 499) & "' WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo & " AND Numero = " & Val(dtsDataset.Tables(0).Rows(0).Item("Numero").ToString) & ";"
'                            End If
'                        Else
'                            Reportar_Cumplido_Remesa_Desde_Remesa = General.CERO
'                        End If
'                        bolConsultaDatos = True

'                    End If
'                End If

'                If strSQL <> String.Empty Then
'                    Me.objGeneral.Ejecutar_SQL(strSQL, strError)
'                End If

'                If Not bolConsultaDatos Then
'                    strMensajeMinisterio = "GESTRANS: No se puede enlazar informacion para reportar.(Enviar a soporte)"
'                End If

'            Catch ex As Exception
'                Reportar_Cumplido_Remesa_Desde_Remesa = General.CERO

'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                strError += " - En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'                strError = strMensajeMinisterio + strError
'            End Try
'        End Function

'        Public Function Reportar_Cumplido_Manifiesto(ByVal lonNumeroCumplido As Long, ByRef strMensaje As String, ByRef TextoXmlCumplidoManifisto As String) As Double
'            Try
'                strMensaje = String.Empty
'                Reportar_Cumplido_Manifiesto = General.CERO
'                Dim objManifiestoCumplidoMEC As New ManifiestoRNDC(acceso, solicitudConsulta)
'                Dim dteAxiliar As Date = Date.MinValue
'                Dim strManifiestosPendientes As String = String.Empty
'                Dim bolCargaDatos As Boolean = False
'                Dim dtrDatos As SqlDataReader

'                If Not Manifiestos_Pendientes_X_Reportar_Manifiesto(TipoDocumentos.TIDO_CUMPLIDO, lonNumeroCumplido, strManifiestosPendientes) Then
'                    strSQL = "SELECT Numero,TipoCumplido,Fecha_Cumplimiento,ValorAdicionalHorasCargue,ValorAdicionalHorasDescargue "
'                    strSQL += " FROM V_Informacion_Viaje"
'                    strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    strSQL += " AND ENCU_Numero = " & lonNumeroCumplido

'                    Me.objGeneral.ComandoSQL = New SqlCommand
'                    Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
'                    Me.objGeneral.ComandoSQL.CommandText = strSQL
'                    Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.CadenaDeConexionSQL)
'                    If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
'                        Me.objGeneral.ComandoSQL.Connection.Open()
'                    End If

'                    dtrDatos = Me.objGeneral.ComandoSQL.ExecuteReader
'                    strSQL = String.Empty

'                    While dtrDatos.Read

'                        objManifiestoCumplidoMEC.NUMNITEMPRESATRANSPORTE = Me.objEmpresa.NumeroIdentificacion
'                        objManifiestoCumplidoMEC.NUMMANIFIESTOCARGA = dtrDatos("Numero")
'                        objManifiestoCumplidoMEC.TIPOCUMPLIDOMANIFIESTO = dtrDatos("TipoCumplido")
'                        objManifiestoCumplidoMEC.FECHAENTREGADOCUMENTOS = dtrDatos("Fecha_Cumplimiento")
'                        objManifiestoCumplidoMEC.VALORADICIONALHORASCARGUE = dtrDatos("ValorAdicionalHorasCargue")
'                        objManifiestoCumplidoMEC.VALORADICIONALHORASDESCARGUE = dtrDatos("ValorAdicionalHorasDescargue")

'                        Dim douNumeroConfirmacion As Double = General.CERO
'                        douNumeroConfirmacion = objManifiestoCumplidoMEC.Reportar_Cumplido_Manifiesto(TextoXmlCumplidoManifisto, Tipos_Procesos.FORMATO_INGRESO, strError)
'                        If (douNumeroConfirmacion > General.CERO And strError = String.Empty) Or strError.Contains(strCumpManiDupli) Then
'                            If douNumeroConfirmacion = General.CERO Then Double.TryParse(strError.Split(" ")(0).Split(":")(1), douNumeroConfirmacion)
'                            Reportar_Cumplido_Manifiesto = douNumeroConfirmacion
'                            strSQL &= "UPDATE Encabezado_Manifiestos SET Numero_Confirmacion_Cumplido_Ministerio = " & douNumeroConfirmacion & ",Mensaje_Manifiesto_Electronico='" & douNumeroConfirmacion & "' WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo & " AND Numero = " & dtrDatos("Numero") & ";"
'                        Else
'                            strSQL &= "UPDATE Encabezado_Manifiestos SET Numero_Confirmacion_Cumplido_Ministerio = 0,Mensaje_Manifiesto_Electronico = '" & Mid(strError, 1, 499) & "' WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo & " AND Numero = " & dtrDatos("Numero") & ";"
'                            Reportar_Cumplido_Manifiesto = General.CERO
'                            strMensaje = strError
'                        End If
'                        bolCargaDatos = True
'                    End While
'                    If strSQL <> String.Empty Then
'                        Me.objGeneral.Ejecutar_SQL(strSQL, strError)
'                    End If
'                    If Not bolCargaDatos Then
'                        strMensaje = "GESTRANS: No se puede enlazar informacion para reportar.(Enviar a soporte)"
'                    End If

'                    If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                        Me.objGeneral.ConexionSQL.Close()
'                    End If

'                    dtrDatos.Close()
'                    dtrDatos.Dispose()
'                    Me.objGeneral.ComandoSQL.Dispose()

'                Else
'                    strError = "El manifiesto asociado al cumplido (Manifiesto Número " & strManifiestosPendientes & "), no se encuentra reportado, por favor reportarlo."
'                End If




'            Catch ex As Exception
'                Reportar_Cumplido_Manifiesto = General.CERO
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Reportar_Anulacion_Remesa(ByVal lonNumeroRemesa As Long, ByRef strMensaje As String) As Double
'            Try
'                Dim objRemesa As New RemesaRNDC(acceso, solicitudConsulta)
'                Dim dteAxiliar As Date = Date.MinValue
'                Dim dblNumeroAnulacioninformacionCargue As Double = General.CERO
'                Dim dblNumeroAnulacionRemesa As Double = General.CERO

'                Dim sdaDatos As SqlDataAdapter
'                Dim dtsDatos As New DataSet

'                strSQL = "SELECT *"
'                strSQL += " FROM V_Informacion_Carga"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & lonNumeroRemesa

'                sdaDatos = New SqlDataAdapter(strSQL, Me.objGeneral.CadenaDeConexionSQL)
'                sdaDatos.Fill(dtsDatos)

'                If dtsDatos.Tables(General.CERO).Rows.Count > General.CERO Then
'                    Dim dtrauxiliar As DataRow = dtsDatos.Tables(General.CERO).Rows(General.CERO)
'                    objRemesa.NUMNITEMPRESATRANSPORTE = objEmpresa.NumeroIdentificacion
'                    objRemesa.MOTIVOANULACIONREMESA = dtrauxiliar.Item("CodigoAnula")

'                    objRemesa.CONSECUTIVOREMESA = dtrauxiliar.Item("Numero_Documento")

'                    objRemesa.OBSERVACIONES = dtrauxiliar.Item("Causa_Anula")
'                End If
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If


'                dblNumeroAnulacioninformacionCargue = objRemesa.Reportar_Anulacion_Informacion_Carga(strTextXmlAnulacionCarga, strMensaje)
'                strMensaje = String.Empty
'                Reportar_Anulacion_Remesa = objRemesa.Reportar_Anulacion_Remesa(strTextXmlRemesaAnulada, strMensaje)

'                sdaDatos.Dispose()
'                dtsDatos.Dispose()

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

'                Reportar_Anulacion_Remesa = General.CERO
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Reportar_Anulacion_Manifiesto(ByVal lonNumeroManifiesto As Long, ByRef strMensaje As String) As Double
'            Try
'                Dim objManifiesto As New ManifiestoRNDC(acceso, solicitudConsulta)
'                Dim dteAxiliar As Date = Date.MinValue
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT *"
'                strSQL += " FROM V_Informacion_Viaje"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & lonNumeroManifiesto

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
'                Me.objGeneral.ConexionSQL.Open()
'                Dim sdrManifiesto As SqlDataReader = ComandoSQL.ExecuteReader

'                While sdrManifiesto.Read
'                    objManifiesto.NUMNITEMPRESATRANSPORTE = Me.objEmpresa.NumeroIdentificacion
'                    objManifiesto.NUMMANIFIESTOCARGA = sdrManifiesto("Numero")
'                    objManifiesto.MOTIVOANULACIONMANIFIESTO = sdrManifiesto("CodigoAnulacion")
'                    objManifiesto.OBSERVACIONES = sdrManifiesto("Causa_Anula")
'                End While

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'                objManifiesto.Reportar_Anulacion_Informacion_Viaje(strTextXmlAnulacionViaje, strMensaje)
'                Reportar_Anulacion_Manifiesto = objManifiesto.Reportar_Anulacion_Manifiesto(strTextXmlManifiestoAnulado, strMensaje)

'                sdrManifiesto.Close()
'                sdrManifiesto.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

'                Reportar_Anulacion_Manifiesto = General.CERO
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Formatear_Campo(ByVal strValor As String, ByVal intLongitud As Integer, ByVal intTipoCampo As Integer, Optional ByVal intPresicion As Integer = 0) As String
'            On Error Resume Next

'            Dim intCon%, strFormato$, intPos%, strValorEntero$, strValorDecimal$, intLon%, strSignoNegativo$, intLongSignoNegativo%
'            Dim strCampo$

'            strFormato = ""
'            Formatear_Campo = ""
'            strValorDecimal = 0
'            strSignoNegativo = ""
'            strCampo = ""

'            strValor = strValor.Replace(Chr(10).ToString, "")
'            strValor = strValor.Replace(CARACTER_ENTER, "")
'            strValor = strValor.Replace(CARACTERES_EXTRANOS_CNT, "")

'            strValor = Trim$(strValor)

'            If intTipoCampo = CAMPO_CEROS_IZQUIERDA Then
'                strValor = Trim$(strValor)
'                intLon = Len(strValor)

'                For intCon = intLon To intLongitud - 1
'                    strValor = "0" & strValor
'                Next
'                Formatear_Campo = strValor

'            ElseIf intTipoCampo = CAMPO_CEROS_DERECHA Then
'                strValor = Trim$(strValor)
'                intLon = Len(strValor)

'                For intCon = intLon + 1 To intLongitud
'                    strValor = strValor & "0"
'                Next
'                Formatear_Campo = strValor


'            ElseIf intTipoCampo = CAMPO_NUMERICO Then

'                ' Si es vacio
'                If strValor = "" Then
'                    For intCon = 1 To intLongitud
'                        strFormato = strFormato & "0"
'                    Next
'                    Formatear_Campo = strFormato

'                Else
'                    ' Buscar signo negativo
'                    intPos = InStr(strValor, "-")
'                    If intPos > 0 Then
'                        strSignoNegativo = "-"
'                        intLongSignoNegativo = 1
'                        strValor = Mid$(strValor, 2)
'                    End If

'                    For intCon = 1 To intLongitud - intLongSignoNegativo
'                        strFormato = strFormato & "0"
'                    Next

'                    Formatear_Campo = strSignoNegativo & Format$(strValor, strFormato)

'                End If

'            ElseIf intTipoCampo = CAMPO_DECIMAL_CON_PUNTO_UNOE Then

'                ' Si es vacio
'                If strValor = "" Then
'                    For intCon = 1 To intLongitud
'                        strFormato = strFormato & "0"
'                    Next
'                    Formatear_Campo = strFormato

'                Else
'                    ' Buscar signo negativo
'                    intPos = InStr(strValor, "-")
'                    If intPos > 0 Then
'                        strSignoNegativo = "-"
'                        intLongSignoNegativo = 1
'                        strValor = Mid$(strValor, 2)
'                    End If

'                    ' Buscar signo decimal
'                    intPos = InStr(strValor, ".")
'                    If intPos > 0 Then
'                        strValorEntero = Mid$(strValor, 1, intPos - 1)
'                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
'                    Else
'                        strValorEntero = strValor
'                        strValorDecimal = "0000"
'                    End If
'                    strValorEntero = Trim$(strValorEntero)
'                    strValorDecimal = Trim$(strValorDecimal)

'                    ' Formatear Entero
'                    For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
'                        strFormato = strFormato & "0"
'                    Next
'                    If strValorEntero = "" Then
'                        strValorEntero = strFormato
'                    Else
'                        strValorEntero = Format(Val(strValorEntero), strFormato)
'                    End If

'                    ' Formatear Decimal
'                    strFormato = ""
'                    intLon = Len(strValorDecimal)
'                    If intLon < intPresicion Then
'                        ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
'                        For intCon = intLon To intPresicion - 1
'                            strValorDecimal = strValorDecimal & "0"
'                        Next

'                    ElseIf intLon = 0 Then
'                        ' Si la longitud es 0 se llena el valor decimal con ceros
'                        For intCon = 0 To intPresicion - 1
'                            strValorDecimal = strValorDecimal & "0"
'                        Next

'                    End If

'                    Formatear_Campo = "+" & strSignoNegativo & strValorEntero & "." & strValorDecimal

'                End If

'            ElseIf intTipoCampo = CAMPO_DECIMAL_UNOE_85 Then
'                ' Buscar signo negativo
'                intPos = InStr(strValor, "-")
'                If intPos > 0 Then
'                    strSignoNegativo = "-"
'                    intLongSignoNegativo = 1
'                    strValor = Mid$(strValor, 2)
'                End If

'                ' Buscar signo decimal
'                intPos = InStr(strValor, ".")
'                If intPos > 0 Then
'                    strValorEntero = Mid$(strValor, 1, intPos - 1)
'                    strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
'                Else
'                    strValorEntero = strValor
'                    If strValorEntero = "0" Then
'                        strValorDecimal = ""
'                    Else
'                        strValorDecimal = "0000"
'                    End If
'                End If
'                strValorEntero = Trim$(strValorEntero)
'                strValorDecimal = Trim$(strValorDecimal)

'                ' Formatear Entero
'                For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
'                    strFormato = strFormato & "0"
'                Next
'                If strValorEntero = "" Then
'                    strValorEntero = strFormato
'                Else
'                    strValorEntero = Format(Val(strValorEntero), strFormato)
'                End If

'                ' Formatear Decimal
'                strFormato = ""
'                intLon = Len(strValorDecimal)
'                If intLon < intPresicion Then
'                    ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
'                    For intCon = intLon To intPresicion - 1
'                        strValorDecimal = strValorDecimal & "0"
'                    Next

'                ElseIf intLon = 0 Then
'                    ' Si la longitud es 0 se llena el valor decimal con ceros
'                    For intCon = 0 To intPresicion - 1
'                        strValorDecimal = strValorDecimal & "0"
'                    Next

'                End If

'                Formatear_Campo = strValorEntero & strValorDecimal & "+"


'            ElseIf intTipoCampo = CAMPO_CARACTER Then ' Blancos a la derecha

'                For intCon = 1 To intLongitud
'                    If Mid$(strValor, intCon, 1) <> Chr(13) Then
'                        strCampo = strCampo & Mid$(strValor, intCon, 1)
'                    End If
'                Next

'                If Len(strCampo) > intLongitud Then
'                    strValor = Mid$(strCampo, 1, intLongitud)
'                Else
'                    strValor = strCampo
'                End If

'                Formatear_Campo = strValor & Space$(intLongitud - Len(strValor))

'            ElseIf intTipoCampo = CAMPO_DECIMAL Then ' Ceros a la izquierda, con el número de decimales, sin punto ni comas

'                ' Si es vacio
'                If strValor = "" Then
'                    For intCon = 1 To intLongitud
'                        strFormato = strFormato & "0"
'                    Next
'                    Formatear_Campo = strFormato

'                Else

'                    ' Buscar signo negativo
'                    intPos = InStr(strValor, "-")
'                    If intPos > 0 Then
'                        strSignoNegativo = "-"
'                        intLongSignoNegativo = 1
'                        strValor = Mid$(strValor, 2)
'                    End If

'                    ' Buscar signo decimal
'                    intPos = InStr(strValor, ".")
'                    If intPos > 0 Then
'                        strValorEntero = Mid$(strValor, 1, intPos - 1)
'                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
'                    Else
'                        strValorEntero = strValor
'                        strValorDecimal = "00"
'                    End If
'                    strValorEntero = Trim$(strValorEntero)
'                    strValorDecimal = Trim$(strValorDecimal)

'                    ' Formatear Entero
'                    For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
'                        strFormato = strFormato & "0"
'                    Next
'                    If strValorEntero = "" Then
'                        strValorEntero = strFormato
'                    Else
'                        strValorEntero = Format(Val(strValorEntero), strFormato)
'                    End If

'                    ' Formatear Decimal
'                    strFormato = ""
'                    intLon = Len(strValorDecimal)
'                    If intLon < intPresicion Then
'                        ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
'                        For intCon = intLon To intPresicion - 1
'                            strValorDecimal = strValorDecimal & "0"
'                        Next

'                    ElseIf intLon = 0 Then
'                        ' Si la longitud es 0 se llena el valor decimal con ceros
'                        For intCon = 0 To intPresicion - 1
'                            strValorDecimal = strValorDecimal & "0"
'                        Next

'                    End If

'                    'Formatear_Campo = strSignoNegativo & strValorEntero & strValorDecimal
'                    Formatear_Campo = strSignoNegativo & strValorEntero

'                End If

'            ElseIf intTipoCampo = CAMPO_FECHA Then
'                Formatear_Campo = Format$(strValor, "DD/MM/YYYY")

'            ElseIf intTipoCampo = CAMPO_FECHA_UNOE Then
'                Formatear_Campo = Format$(strValor, "YYYY/MM/DD")


'            ElseIf intTipoCampo = CAMPO_IDENTIFICACION Then ' Espacios a la Derecha. (1-2) dependiendo del digito de chequeo
'                Dim intCerosAgregar As Short
'                intCerosAgregar = intLongitud - Len(strValor)
'                For intCon = 1 To intCerosAgregar
'                    Formatear_Campo += " "
'                Next
'                Formatear_Campo += strValor

'            ElseIf intTipoCampo = CAMPO_ESPACIOS_IZQUIERDA Then
'                Dim intEspacioAgregar As Short
'                intEspacioAgregar = intLongitud - Len(strValor)
'                For intCon = 1 To intEspacioAgregar
'                    Formatear_Campo += " "
'                Next
'                Formatear_Campo += strValor

'            ElseIf intTipoCampo = CAMPO_DECIMAL_CON_PUNTO Then

'                ' Si es vacio
'                If strValor = "" Then
'                    For intCon = 1 To intLongitud
'                        strFormato = strFormato & "0"
'                    Next
'                    Formatear_Campo = strFormato

'                Else
'                    ' Buscar signo negativo
'                    intPos = InStr(strValor, "-")
'                    If intPos > 0 Then
'                        strSignoNegativo = "-"
'                        intLongSignoNegativo = 1
'                        strValor = Mid$(strValor, 2)
'                    End If

'                    ' Buscar signo decimal
'                    intPos = InStr(strValor, ".")
'                    If intPos > 0 Then
'                        strValorEntero = Mid$(strValor, 1, intPos - 1)
'                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
'                    Else
'                        strValorEntero = strValor
'                        strValorDecimal = Trim$(strValorDecimal)
'                    End If
'                    strValorEntero = Trim$(strValorEntero)
'                    strValorDecimal = Trim$(strValorDecimal)

'                    ' Formatear Entero
'                    For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
'                        strFormato = strFormato & "0"
'                    Next
'                    If strValorEntero = "" Then
'                        strValorEntero = strFormato
'                    Else
'                        strValorEntero = Format(Val(strValorEntero), strFormato)
'                    End If

'                    ' Formatear Decimal
'                    strFormato = ""
'                    intLon = Len(strValorDecimal)
'                    If intLon < intPresicion Then
'                        ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
'                        For intCon = intLon To intPresicion - 1
'                            strValorDecimal = strValorDecimal & "0"
'                        Next

'                    ElseIf intLon = 0 Then
'                        ' Si la longitud es 0 se llena el valor decimal con ceros
'                        For intCon = 0 To intPresicion - 1
'                            strValorDecimal = strValorDecimal & "0"
'                        Next

'                    End If

'                    Formatear_Campo = strSignoNegativo & strValorEntero & "." & strValorDecimal
'                    Formatear_Campo = Trim(Formatear_Campo)

'                End If

'            ElseIf intTipoCampo = SOLO_CAMPO_DECIMAL_CON_PUNTO Then ' Formatea el campo con decimales con punto y con espacios a la izquierda

'                ' Si es vacio
'                If strValor = "" Then
'                    For intCon = 1 To intLongitud
'                        strFormato = strFormato & " "
'                    Next
'                    Formatear_Campo = strFormato

'                Else
'                    ' Buscar signo negativo
'                    intPos = InStr(strValor, "-")
'                    If intPos > 0 Then
'                        strSignoNegativo = "-"
'                        intLongSignoNegativo = 1
'                        strValor = Mid$(strValor, 2)
'                    End If

'                    ' Buscar signo decimal
'                    intPos = InStr(strValor, ".")
'                    If intPos > 0 Then
'                        strValorEntero = Mid$(strValor, 1, intPos - 1)
'                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
'                    Else
'                        strValorEntero = strValor
'                        strValorDecimal = Trim$(strValorDecimal)
'                    End If
'                    strValorEntero = Trim$(strValorEntero)
'                    strValorDecimal = Trim$(strValorDecimal)

'                    ' Formatear Entero
'                    Dim intEspacioAgregar As Short
'                    intEspacioAgregar = intLongitud - Len(strValorEntero)
'                    For intCon = 1 To intEspacioAgregar - intPresicion - intLongSignoNegativo - 1 '-1 POR EL PUNTO
'                        strFormato = strFormato & " "
'                    Next
'                    If strValorEntero = "" Then
'                        strValorEntero = strFormato
'                    Else
'                        strValorEntero = strFormato & strValorEntero
'                        ' strValorEntero = Format(Val(strValorEntero), strFormato)
'                    End If

'                    ' Formatear Decimal
'                    strFormato = ""
'                    intLon = Len(strValorDecimal)
'                    If intLon < intPresicion Then
'                        ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
'                        For intCon = intLon To intPresicion - 1
'                            strValorDecimal = strValorDecimal & "0"
'                        Next

'                    ElseIf intLon = 0 Then
'                        ' Si la longitud es 0 se llena el valor decimal con ceros
'                        For intCon = 0 To intPresicion - 1
'                            strValorDecimal = strValorDecimal & "0"
'                        Next

'                    End If

'                    Formatear_Campo = strSignoNegativo & strValorEntero & "." & strValorDecimal

'                End If

'            End If

'        End Function

'        Public Function Proceso_Reportar_Anulacion_Cumplido(ByVal lonNumeroCumplido As Long) As Double
'            Try
'                Dim objManifiesto As New ManifiestoRNDC(acceso, solicitudConsulta)
'                Dim dteAxiliar As Date = Date.MinValue
'                Dim intAuxiliar As Integer
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Numero, ENMA_Numero, Causa_Anula,CATA_MACU_Codigo"
'                strSQL += " FROM Encabezado_Cumplidos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & lonNumeroCumplido

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
'                Me.objGeneral.ConexionSQL.Open()
'                Dim sdrCumplido As SqlDataReader = ComandoSQL.ExecuteReader

'                While sdrCumplido.Read
'                    objManifiesto.NUMNITEMPRESATRANSPORTE = Me.objEmpresa.NumeroIdentificacion
'                    objManifiesto.NUMMANIFIESTOCARGA = sdrCumplido("ENMA_Numero")
'                    intAuxiliar = sdrCumplido("CATA_MACU_Codigo")
'                    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Motivos_Anulacion_Cumplidos", "Cod_ministerio_anulacion_cumplido", "CATA_MACU_Codigo", General.CAMPO_NUMERICO, intAuxiliar, objManifiesto.CODMOTIVOANULACIONCUMPLIDO)
'                    objManifiesto.OBSERVACIONES_ANULACION_CUMPLIDO = sdrCumplido("Causa_Anula")
'                End While

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'                Proceso_Reportar_Anulacion_Cumplido = objManifiesto.Reportar_Anulacion_Cumplido(strTextXmlCumplidoAnulado, strMensaje)

'                sdrCumplido.Close()
'                sdrCumplido.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                strMensaje = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

'                Proceso_Reportar_Anulacion_Cumplido = General.CERO
'            Finally
'                Actualizar_Numero_Confirmacion_Anulacion_Cumplido(lonNumeroCumplido, strMensaje)
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'#End Region
'    End Class
'End Namespace

