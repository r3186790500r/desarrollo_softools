﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 07/12/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica:  30/03/2019
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_modificar_detalle_distribucion_remesas'
GO
DROP PROCEDURE gsp_modificar_detalle_distribucion_remesas
GO
CREATE PROCEDURE gsp_modificar_detalle_distribucion_remesas 
(      
@par_EMPR_Codigo SMALLINT ,      
@par_Codigo NUMERIC,      
@par_Cantidad_Recibe NUMERIC (18,2) = NULL,        
@par_Peso_Recibe NUMERIC (18,2) = NULL,      
@par_Fecha_Recibe DATE = NULL,      
@par_CATA_TIID_Codigo_Recibe NUMERIC,      
@par_Numero_Identificacion_Recibe VARCHAR(30),      
@par_Nombre_Recibe VARCHAR(50) = NULL,                        
@par_Telefonos_Recibe VARCHAR(100) = NULL,     
@par_Firma IMAGE = NULL,             
@par_Observaciones_Recibe VARCHAR(500) = NULL,         
@par_USUA_Codigo_Modifica SMALLINT      
)      
AS       
BEGIN      
UPDATE Detalle_Distribucion_Remesas      
SET       
Cantidad_Recibe = @par_Cantidad_Recibe,      
Peso_Recibe = @par_Peso_Recibe,      
Fecha_Recibe = @par_Fecha_Recibe,      
CATA_TIID_Codigo_Recibe = @par_CATA_TIID_Codigo_Recibe,      
Numero_Identificacion_Recibe = @par_Numero_Identificacion_Recibe,     
Nombre_Recibe = @par_Nombre_Recibe,      
Telefonos_Recibe = @par_Telefonos_Recibe,    
Firma_Recibe = @par_Firma,    
Observaciones_Recibe = @par_Observaciones_Recibe,     
USUA_Codigo_Modifica =  @par_USUA_Codigo_Modifica,      
Fecha_Modifica = GETDATE(),      
CATA_ESDR_Codigo = 11402    
      
WHERE       
EMPR_Codigo =  @par_EMPR_Codigo      
AND ID =  @par_Codigo      
      
SELECT @par_Codigo As Codigo      
      
END      
GO