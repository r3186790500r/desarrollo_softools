﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Tesoreria
    Public NotInheritable Class DetalleObservacionesCuentas
        Inherits Base
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            ENDC_Numero = Read(lector, "ENDC_Numero")
            FechaCrea = Read(lector, "Fecha_Crea")
            UsuarioCrea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea"), .Nombre = Read(lector, "USUA_Nombre_Crea")}
            Observaciones = Read(lector, "Observaciones")
        End Sub
        <JsonProperty>
        Public Property ENDC_Numero As Integer
        <JsonProperty>
        Public Property FechaCrea As DateTime
        <JsonProperty>
        Public Property UsuarioCrea As Usuarios
        <JsonProperty>
        Public Property Observaciones As String
    End Class
End Namespace
