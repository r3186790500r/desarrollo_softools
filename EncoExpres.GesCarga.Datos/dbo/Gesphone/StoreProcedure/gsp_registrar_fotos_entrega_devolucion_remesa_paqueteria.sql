﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 19/10/2021 
-- Módulo: Gesphone

PRINT 'gsp_registrar_fotos_entrega_devolucion_remesa_paqueteria'
GO
DROP PROCEDURE gsp_registrar_fotos_entrega_devolucion_remesa_paqueteria
GO
CREATE PROCEDURE dbo.gsp_registrar_fotos_entrega_devolucion_remesa_paqueteria               
(                
@par_EMPR_Codigo SMALLINT ,                
@par_ENRE_Numero NUMERIC,                         
@par_Firma IMAGE = NULL,
@par_Tipo NUMERIC
)                
AS                 
BEGIN                
  
IF @par_Tipo = 0
  BEGIN
    -- Es una entrega.
    UPDATE Cumplido_Remesas SET Firma_Recibe = @par_Firma
    WHERE EMPR_Codigo =  @par_EMPR_Codigo                
    AND ENRE_Numero =  @par_ENRE_Numero
  END

  UPDATE Remesas_Paqueteria                
    SET Firma_Recibe = @par_Firma                
    WHERE EMPR_Codigo =  @par_EMPR_Codigo                
    AND ENRE_Numero =  @par_ENRE_Numero   
                
    SELECT @par_ENRE_Numero As Codigo
                
END 
GO