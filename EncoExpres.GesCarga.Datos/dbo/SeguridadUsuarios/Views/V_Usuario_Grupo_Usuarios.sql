﻿PRINT 'V_Usuario_Grupo_Usuarios'
GO
DROP VIEW V_Usuario_Grupo_Usuarios
GO
CREATE VIEW V_Usuario_Grupo_Usuarios
AS
SELECT 
USGU.EMPR_Codigo, 
USGU.GRUS_Codigo,
USGU.USUA_Codigo,
GRUS.Nombre AS NombreGrupo, 
USUA.Nombre AS NombreUsuario
FROM 
Usuario_Grupo_Usuarios USGU,
Grupo_Usuarios GRUS,
Usuarios USUA

WHERE USGU.EMPR_Codigo = GRUS.EMPR_Codigo 
AND USGU.GRUS_Codigo = GRUS.Codigo 

AND USGU.EMPR_Codigo = USUA.EMPR_Codigo 
AND USGU.USUA_Codigo = USUA .Codigo 
GO