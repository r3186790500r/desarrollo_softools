﻿PRINT 'gsp_proceso_cerrar_orden_servicio'
GO
DROP PROCEDURE gsp_proceso_cerrar_orden_servicio
GO
CREATE PROCEDURE gsp_proceso_cerrar_orden_servicio(
  @par_EMPR_Codigo SMALLINT,
  @par_ESOS_Numero NUMERIC,
  @par_ENOS_TIDO_Codigo NUMERIC,
  @par_CATA_CCOS NUMERIC,
  @par_USUA_Codigo_Cierre NUMERIC,
  @par_Observaciones_Cierre VARCHAR (500)
  )
AS
BEGIN
	Declare @ESOS_Finalizada numeric = 9204

	UPDATE Encabezado_Solicitud_Orden_Servicios  SET
	CATA_CCOS_Codigo = @par_CATA_CCOS,
	Observaciones_Cierre = @par_Observaciones_Cierre,
	USUA_Codigo_Cierre = @par_USUA_Codigo_Cierre,
	CATA_ESSO_Codigo = @ESOS_Finalizada,
	Fecha_Cierre = GETDATE()
	
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND Numero  = @par_ESOS_Numero
	AND TIDO_Codigo  = @par_ENOS_TIDO_Codigo

	SELECT Numero 
	FROM Encabezado_Solicitud_Orden_Servicios
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND Numero  = @par_ESOS_Numero
	AND TIDO_Codigo  = @par_ENOS_TIDO_Codigo
	
END
GO