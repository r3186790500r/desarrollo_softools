﻿Imports System.Configuration.ConfigurationManager
Imports System.Threading
Imports System.Data.SqlClient
Imports System.Net
Imports Newtonsoft.Json
Imports System.Configuration
Imports System.Text
Imports System.IO
Imports System.Reflection

Public Class svCARGA_APP

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            Me.objGeneral = New clsGeneral
            Call Cargar_Datos_App_Config()
            Me.objGeneral.Guardar_Mensaje_Log("Inició servicio Sincronización CARGA APP")
            Me.ProcesoSincronizacionCARGA_APP_Oficinas = New Thread(AddressOf Proceso_Sincronizacion_Oficinas)
            Me.ProcesoSincronizacionCARGA_APP_Oficinas.Start()
            Me.ProcesoSincronizacionCARGA_APP_Empresas_GPS = New Thread(AddressOf Proceso_Sincronizacion_Empresas_GPS)
            Me.ProcesoSincronizacionCARGA_APP_Empresas_GPS.Start()
            Me.ProcesoSincronizacionCARGA_APP_Vehiculos = New Thread(AddressOf Proceso_Sincronizacion_Vehiculos)
            Me.ProcesoSincronizacionCARGA_APP_Vehiculos.Start()
            Me.ProcesoSincronizacionCARGA_APP_Terceros = New Thread(AddressOf Proceso_Sincronizacion_Terceros)
            Me.ProcesoSincronizacionCARGA_APP_Terceros.Start()
            Me.ProcesoSincronizacionCARGA_APP_Conductores = New Thread(AddressOf Proceso_Sincronizacion_Conductores)
            Me.ProcesoSincronizacionCARGA_APP_Conductores.Start()
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error OnStart: " & ex.Message)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        Try
            Me.thrProcSync.Abort()
            Me.thrProcSync_Empresas_GPS.Abort()
            Me.thrProcSync_Vehiculos.Abort()
            Me.thrProcSync_Terceros.Abort()
            Me.thrProcSync_Conductores.Abort()
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error OnStop: " & ex.Message)
        End Try
    End Sub

#Region "Variables"
    Private thrProcSync As Thread
    Private thrProcSync_Empresas_GPS As Thread
    Private thrProcSync_Vehiculos As Thread
    Private thrProcSync_Terceros As Thread
    Private thrProcSync_Conductores As Thread
    Private objGeneral As clsGeneral
    Public Usuario As String
    Private TiempoSuspencionServicio As Long
    Private Token_CARGA_APP As String
    Private thrProceso As Thread
#End Region

#Region "Constantes"
    'EndPoints CARGA_APP:
    Dim GEN_URL_CARGA_APP As String
    Dim API_GET_TOKEN As String = "api/v1/security/login"
    Dim API_CREAR_OFICINA As String = "api/v1/agencias"
    Dim API_CREAR_EMPRESA_GPS As String = "api/v2/empresas-gps"
    Dim API_CREAR_VEHICULO As String = "api/v2/vehiculos"
    Dim API_CREAR_TERCERO As String = "api/v1/terceros"
    Dim API_CREAR_CONDUCTOR As String = "api/v1/conductores"
    Dim Empresa As String
    Dim URL_CARGA_APP As String
    Dim Clave As String = "Ls%4neW7E&ZU$?X4"
    Dim ORIGEN_SINCRONIZACION_OFICINAS = 23801
    Dim ORIGEN_SINCRONIZACION_EMPRESAS_GPS = 23802
    Dim ORIGEN_SINCRONIZACION_VEHICULOS = 23803
    Dim ORIGEN_SINCRONIZACION_TERCEROS = 23804
    Dim ORIGEN_SINCRONIZACION_CONDUCTORES = 23805
#End Region


#Region "Propiedades"
    Public Property ProcesoSincronizacionCARGA_APP_Oficinas() As Thread
        Get
            Return thrProcSync
        End Get
        Set(ByVal value As Thread)
            thrProcSync = value
        End Set
    End Property

    Public Property ProcesoSincronizacionCARGA_APP_Empresas_GPS() As Thread
        Get
            Return thrProcSync_Empresas_GPS
        End Get
        Set(ByVal value As Thread)
            thrProcSync_Empresas_GPS = value
        End Set
    End Property

    Public Property ProcesoSincronizacionCARGA_APP_Vehiculos() As Thread
        Get
            Return thrProcSync_Vehiculos
        End Get
        Set(ByVal value As Thread)
            thrProcSync_Vehiculos = value
        End Set
    End Property
    Public Property ProcesoSincronizacionCARGA_APP_Terceros() As Thread
        Get
            Return thrProcSync_Terceros
        End Get
        Set(ByVal value As Thread)
            thrProcSync_Terceros = value
        End Set
    End Property
    Public Property ProcesoSincronizacionCARGA_APP_Conductores() As Thread
        Get
            Return thrProcSync_Conductores
        End Get
        Set(ByVal value As Thread)
            thrProcSync_Conductores = value
        End Set
    End Property
#End Region


#Region "Constructor"

    Sub New()
        InitializeComponent()
        'Me.objGeneral = New clsGeneral
        'Call Cargar_Datos_App_Config()
        'Me.objGeneral.Guardar_Mensaje_Log("Inició servicio Sincronización CARGA APP")
        'Me.ProcesoSincronizacionCARGA_APP_Oficinas = New Thread(AddressOf Proceso_Sincronizacion_Oficinas)
        'Me.ProcesoSincronizacionCARGA_APP_Oficinas.Start()
        'Me.ProcesoSincronizacionCARGA_APP_Empresas_GPS = New Thread(AddressOf Proceso_Sincronizacion_Empresas_GPS)
        'Me.ProcesoSincronizacionCARGA_APP_Empresas_GPS.Start()
        'Me.ProcesoSincronizacionCARGA_APP_Vehiculos = New Thread(AddressOf Proceso_Sincronizacion_Vehiculos)
        'Me.ProcesoSincronizacionCARGA_APP_Vehiculos.Start()
        'Me.ProcesoSincronizacionCARGA_APP_Terceros = New Thread(AddressOf Proceso_Sincronizacion_Terceros)
        ''Me.ProcesoSincronizacionCARGA_APP_Terceros.Start()
        'Me.ProcesoSincronizacionCARGA_APP_Conductores = New Thread(AddressOf Proceso_Sincronizacion_Conductores)
        'Me.ProcesoSincronizacionCARGA_APP_Conductores.Start()
    End Sub
#End Region

#Region "Funciones Generales"
    Private Sub Cargar_Datos_App_Config()
        Try
            ' Generales
            Empresa = ConfigurationSettings.AppSettings.Get("CodigoEmpresa")
            Usuario = ConfigurationSettings.AppSettings.Get("UsuarioWS")
            GEN_URL_CARGA_APP = ConfigurationSettings.AppSettings.Get("UrlAPICARGA")
            TiempoSuspencionServicio = ConfigurationSettings.AppSettings.Get("FrecuenciaActualizacion")
            Me.objGeneral.RutaArchivoLog = ConfigurationSettings.AppSettings("RutaArchivoLog").ToString()
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Error Cargar_Datos_App_Config: " & ex.Message)
        End Try
    End Sub

    Private Function ObtenerToken_CARGA_APP() As String
        Try
            Dim webClient As New WebClient()
            Dim urlToken As String = GEN_URL_CARGA_APP & API_GET_TOKEN
            Dim objResult As TokenCargaAPP
            'Headers
            webClient.Headers("content-type") = "application/json"
            Dim EntidadAcceso = New Dictionary(Of String, Object)
            EntidadAcceso.Add("username", Usuario)
            EntidadAcceso.Add("password", Clave)

            'Se convierte el dictionary que representa los datos en un String
            Dim strJSON As String = JsonConvert.SerializeObject(EntidadAcceso, Newtonsoft.Json.Formatting.Indented)
            'El String con el JSON se convierte a un arreglo de Bytes
            Dim reqString() As Byte = Encoding.Default.GetBytes(strJSON)
            'Se realiza la solicitud por el verbo POST enviando el JSON representado en Bytes
            Dim resByte As Byte() = webClient.UploadData(urlToken, "post", reqString)
            'Se convierte la respuesta que es un arreglo de Bytes a un String
            Dim resString As String = Encoding.Default.GetString(resByte)
            'Se libera el Objeto de la conexión
            webClient.Dispose()
            'Se retorna la respuestaString en formato JObject
            objResult = JsonConvert.DeserializeObject(Of TokenCargaAPP)(resString)
            Return objResult.token
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error en ObtenerToken_CARGA_APP : " & ex.Message)
        End Try



    End Function
#End Region

#Region "Sincronizacion"



    Private Sub Proceso_Sincronizacion_Oficinas()
        Try
            While (True)
                Call Iniciar_Proceso_Sincronizacion_Oficinas()
                Thread.Sleep(Val(AppSettings.Get("FrecuenciaActualizacion")))
            End While
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Proceso_Sincronizacion_Oficinas : ")
        End Try
    End Sub

    Private Sub Proceso_Sincronizacion_Empresas_GPS()
        Try
            While (True)
                Call Iniciar_Proceso_Sincronizacion_Empresas_GPS()
                Thread.Sleep(Val(AppSettings.Get("FrecuenciaActualizacion")))
            End While
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Proceso_Sincronizacion_Empresas_GPS : ")
        End Try
    End Sub

    Private Sub Proceso_Sincronizacion_Vehiculos()
        Try
            While (True)
                Call Iniciar_Proceso_Sincronizacion_Vehiculos()
                Thread.Sleep(Val(AppSettings.Get("FrecuenciaActualizacion")))
            End While
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Proceso_Sincronizacion_Vehiculos : ")
        End Try
    End Sub

    Private Sub Proceso_Sincronizacion_Terceros()
        Try
            While (True)
                Call Iniciar_Proceso_Sincronizacion_Terceros()
                Thread.Sleep(Val(AppSettings.Get("FrecuenciaActualizacion")))
            End While
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Proceso_Sincronizacion_Terceros : ")
        End Try
    End Sub

    Private Sub Proceso_Sincronizacion_Conductores()
        Try
            While (True)
                Call Iniciar_Proceso_Sincronizacion_Conductores()
                Thread.Sleep(Val(AppSettings.Get("FrecuenciaActualizacion")))
            End While
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Proceso_Sincronizacion_Conductores : ")
        End Try
    End Sub
    Private Sub Iniciar_Proceso_Sincronizacion_Oficinas()

        Dim strSql As String
        Dim strSqlConsulta As String
        Dim urlCrearOficinas As String = GEN_URL_CARGA_APP & API_CREAR_OFICINA
        Dim objResultCrearOficinas As objResponseCrearOficina
        Dim webClient As New WebClient()
        Dim dtsSync As DataSet
        Dim dtsConsulta As DataSet
        strSqlConsulta = "gsp_consultar_oficinas_sincronizacion_CARGA_APP " & Empresa
        dtsConsulta = objGeneral.Retorna_Dataset(strSqlConsulta)

        If dtsConsulta.Tables.Count > 0 Then
            If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                For Each oficina In dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows
                    Try
                        Dim statusDescription As String
                        Dim statusCode As Long

                        Me.Token_CARGA_APP = ObtenerToken_CARGA_APP()
                        'Headers Crear Oficina:
                        webClient.Headers("content-type") = "application/json"
                        webClient.Headers("Authorization") = "Bearer " & Me.Token_CARGA_APP

                        'Se crea un objeto Json con el cuerpo de la solicitud : 
                        Dim EntidadCrearOficinas = New Dictionary(Of String, Object)
                        EntidadCrearOficinas.Add("codigoAlterno", oficina.Item("Codigo_Alterno"))
                        EntidadCrearOficinas.Add("nombreAgencia", oficina.Item("Nombre"))
                        Dim strJSONCrearOficinas As String = JsonConvert.SerializeObject(EntidadCrearOficinas, Newtonsoft.Json.Formatting.Indented)
                        Dim reqStringCrearOficinas() As Byte = Encoding.Default.GetBytes(strJSONCrearOficinas)
                        'Fin creación Json

                        'Se envía la solicitud:
                        Dim resByteCrearOficinas As Byte() = webClient.UploadData(urlCrearOficinas, "post", reqStringCrearOficinas)

                        'Para obtener el código y mensaje de status del response de la solicitud, se extraen los atributos con ayuda de System.Reflectioon.BindingFlags , ya que son atributos no públicos de la clase WebClient:
                        Dim status As FieldInfo = webClient.GetType().GetField("m_WebResponse", BindingFlags.Instance Or BindingFlags.NonPublic)
                        If Not IsNothing(status) Then
                            Dim response As HttpWebResponse = status.GetValue(webClient)
                            If Not IsNothing(response) Then
                                statusDescription = response.StatusDescription
                                statusCode = Long.Parse(response.StatusCode)
                            End If

                        End If
                        'Fin Obtención Código y mensaje del response

                        'Dim resStringCrearOficinas As String = Encoding.Default.GetString(resByteCrearOficinas)
                        webClient.Dispose()
                        'objResultCrearOficinas = JsonConvert.DeserializeObject(Of objResponseCrearOficina)(resStringCrearOficinas)


                        'Se inserta en la tabla Registro_Sincronización_CARGA_APP la oficina sincronizada junto con el mensaje de respuesta:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & "," & ORIGEN_SINCRONIZACION_OFICINAS & ", " & oficina.Item("Codigo") & ", '" & statusCode & " " & statusDescription & "', 1"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)



                    Catch ex As Exception
                        'En caso de generarse error, se inserta igualmente en la tabla de sincronización, con el mensaje respectivo, y con el campo "Sincronizado" = 0:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", " & ORIGEN_SINCRONIZACION_OFICINAS & ", " & oficina.Item("Codigo") & ", '" & ex.Message & "', 0"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)

                        'Se deja registro en el archivo log del resultado:
                        If dtsSync.Tables.Count > 0 Then

                            If dtsSync.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                Me.objGeneral.Guardar_Mensaje_Log("Error en la Oficina " & oficina.Item("Nombre") & " : " & oficina.Item("Codigo_Alterno") & ". Error : " & ex.Message)

                            End If

                        End If
                        Me.objGeneral.Guardar_Mensaje_Log("Error funcion Iniciar_Proceso_Sincronizacion_Oficinas: " & ex.Message)
                    End Try
                Next
                'Se deja registro en el archivo log el total de registros procesados:
                Me.objGeneral.Guardar_Mensaje_Log("Número Oficinas Sincronizadas: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)
            End If

        End If



    End Sub

    Private Sub Iniciar_Proceso_Sincronizacion_Empresas_GPS()

        Dim strSql As String
        Dim strSqlConsulta As String
        Dim urlCrearEmpresasGPS As String = GEN_URL_CARGA_APP & API_CREAR_EMPRESA_GPS
        Dim objResultCrearEmpresasGPS As objResponseCrearEmpresasGPS
        Dim webClient As New WebClient()
        Dim dtsSync As DataSet
        Dim dtsConsulta As DataSet
        strSqlConsulta = "gsp_consultar_proveedores_GPS_sincronizacion_CARGA_APP " & Empresa & ",'1414,'"
        dtsConsulta = objGeneral.Retorna_Dataset(strSqlConsulta)

        If dtsConsulta.Tables.Count > 0 Then
            If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                For Each tercero In dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows
                    Try
                        Dim statusDescription As String
                        Dim statusCode As Long

                        Me.Token_CARGA_APP = ObtenerToken_CARGA_APP()
                        'Headers Crear Tercero:
                        webClient.Headers("content-type") = "application/json"
                        webClient.Headers("Authorization") = "Bearer " & Me.Token_CARGA_APP

                        'Se crea un objeto Json con el cuerpo de la solicitud : 
                        Dim EntidadCrearTerceros = New Dictionary(Of String, Object)
                        EntidadCrearTerceros.Add("codigoAlterno", tercero.Item("Codigo_Alterno"))
                        EntidadCrearTerceros.Add("nombre", tercero.Item("NombreCompleto"))
                        Dim strJSONCrearTerceros As String = JsonConvert.SerializeObject(EntidadCrearTerceros, Newtonsoft.Json.Formatting.Indented)
                        Dim reqStringCrearTerceros() As Byte = Encoding.Default.GetBytes(strJSONCrearTerceros)
                        'Fin creación Json

                        'Se envía la solicitud:
                        Dim resByteCrearTerceros As Byte() = webClient.UploadData(urlCrearEmpresasGPS, "post", reqStringCrearTerceros)

                        'Para obtener el código y mensaje de status del response de la solicitud, se extraen los atributos con ayuda de System.Reflectioon.BindingFlags , ya que son atributos no públicos de la clase WebClient:
                        Dim status As FieldInfo = webClient.GetType().GetField("m_WebResponse", BindingFlags.Instance Or BindingFlags.NonPublic)
                        If Not IsNothing(status) Then
                            Dim response As HttpWebResponse = status.GetValue(webClient)
                            If Not IsNothing(response) Then
                                statusDescription = response.StatusDescription
                                statusCode = Long.Parse(response.StatusCode)
                            End If

                        End If
                        'Fin Obtención Código y mensaje del response

                        Dim resStringCrearTerceros As String = Encoding.Default.GetString(resByteCrearTerceros)
                        webClient.Dispose()
                        objResultCrearEmpresasGPS = JsonConvert.DeserializeObject(Of objResponseCrearEmpresasGPS)(resStringCrearTerceros)


                        'Se inserta en la tabla Registro_Sincronización_CARGA_APP el tercero sincronizado junto con el mensaje de respuesta:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", " & ORIGEN_SINCRONIZACION_EMPRESAS_GPS & ", " & tercero.Item("Codigo") & ", '" & statusCode & " " & statusDescription & "', 1"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)


                    Catch ex As Exception
                        'En caso de generarse error, se inserta igualmente en la tabla de sincronización, con el mensaje respectivo, y con el campo "Sincronizado" = 0:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & "," & ORIGEN_SINCRONIZACION_EMPRESAS_GPS & ",  " & tercero.Item("Codigo") & ", '" & ex.Message & "', 0"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)

                        'Se deja registro en el archivo log del resultado:
                        If dtsSync.Tables.Count > 0 Then

                            If dtsSync.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                Me.objGeneral.Guardar_Mensaje_Log("Error en el tercero " & tercero.Item("NombreCompleto") & " : " & tercero.Item("Codigo_Alterno") & ". Error : " & ex.Message)

                            End If

                        End If
                        Me.objGeneral.Guardar_Mensaje_Log("Error funcion Sincronizacion Empresas GPS: " & ex.Message)
                    End Try
                Next
                'Se deja registro en el archivo log el total de registros procesados:
                Me.objGeneral.Guardar_Mensaje_Log("Número Terceros Sincronizados: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)
            End If

        End If



    End Sub

    Private Sub Iniciar_Proceso_Sincronizacion_Vehiculos()

        Dim strSql As String
        Dim strSqlConsulta As String
        Dim urlCrearVehiculos As String = GEN_URL_CARGA_APP & API_CREAR_VEHICULO
        Dim objResultCrearVehiculos As objResponseCrearVehiculos
        Dim webClient As New WebClient()
        Dim dtsSync As DataSet
        Dim dtsConsulta As DataSet
        strSqlConsulta = "gsp_consultar_vehiculos_sincronizacion_CARGA_APP " & Empresa
        dtsConsulta = objGeneral.Retorna_Dataset(strSqlConsulta)

        If dtsConsulta.Tables.Count > 0 Then
            If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                For Each vehiculo In dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows
                    Try
                        Dim statusDescription As String
                        Dim statusCode As Long

                        Me.Token_CARGA_APP = ObtenerToken_CARGA_APP()
                        'Headers Crear Vehículos:
                        webClient.Headers("content-type") = "application/json"
                        webClient.Headers("Authorization") = "Bearer " & Me.Token_CARGA_APP

                        'Se crea un objeto Json con el cuerpo de la solicitud : 
                        Dim EntidadCrearVehiculos = New Dictionary(Of String, Object)
                        EntidadCrearVehiculos.Add("placa", vehiculo.Item("Placa"))
                        EntidadCrearVehiculos.Add("activo", IIf(vehiculo.Item("Estado") = 1, True, False))
                        EntidadCrearVehiculos.Add("modelo", vehiculo.Item("Modelo"))
                        EntidadCrearVehiculos.Add("tipoVehiculo", Integer.Parse(vehiculo.Item("CATA_TIVE_Codigo")))
                        EntidadCrearVehiculos.Add("descripcionTipoVehiculo", vehiculo.Item("Tipo_Vehiculo"))
                        EntidadCrearVehiculos.Add("ejesCabezote", vehiculo.Item("Numero_Ejes"))
                        EntidadCrearVehiculos.Add("placaTrailer", vehiculo.Item("SEMI_Placa"))
                        EntidadCrearVehiculos.Add("tipoCarroceria", Integer.Parse(vehiculo.Item("SEMI_CATA_TICA_Codigo")))
                        EntidadCrearVehiculos.Add("descripcionTipoCarroceria", vehiculo.Item("TipoCarroceriaSemirremolque"))
                        EntidadCrearVehiculos.Add("ejesTrailer", vehiculo.Item("EjesSemirremolque"))
                        EntidadCrearVehiculos.Add("fechaExpiracionTecnicoMecanica", vehiculo.Item("fechaExpiracionTecnicoMecanica"))
                        EntidadCrearVehiculos.Add("fechaExpiracionSOAT", vehiculo.Item("fechaExpiracionSOAT"))
                        EntidadCrearVehiculos.Add("tieneReporteExterno", IIf(vehiculo.Item("ReporteExterno") = 1, True, False))
                        EntidadCrearVehiculos.Add("tieneReporteInterno", IIf(vehiculo.Item("ReporteInterno") = 1, True, False))
                        EntidadCrearVehiculos.Add("tieneSatelital", IIf(vehiculo.Item("Satelital") = 1, True, False))
                        EntidadCrearVehiculos.Add("codigoTipoCombustible", Integer.Parse(vehiculo.Item("CATA_TICO_Codigo")))
                        EntidadCrearVehiculos.Add("descripcionTipoCombustible", vehiculo.Item("TipoCombustible"))
                        EntidadCrearVehiculos.Add("codigoAlternoEmpresaGPS", vehiculo.Item("CodigoAlternoProveedorGPS"))
                        EntidadCrearVehiculos.Add("nombreEmpresaGPS", vehiculo.Item("NombreEmpresaGPS"))
                        EntidadCrearVehiculos.Add("usuarioGPS", vehiculo.Item("usuarioGPS"))
                        EntidadCrearVehiculos.Add("passwordGPS", vehiculo.Item("passwordGPS"))
                        EntidadCrearVehiculos.Add("codigoAlternoConductor", vehiculo.Item("CodigoAlternoConductor"))
                        EntidadCrearVehiculos.Add("codigoAlternoTenedor", vehiculo.Item("CodigoAlternoTenedor"))
                        EntidadCrearVehiculos.Add("codigoAlternoPropietario", vehiculo.Item("CodigoAlternoPropietario"))
                        EntidadCrearVehiculos.Add("codigoAlternoAdministrador", vehiculo.Item("CodigoAlternoAdministrador"))
                        EntidadCrearVehiculos.Add("esArticulado", IIf(vehiculo.Item("Articulado") = 1, True, False))

                        Dim strJSONCrearVehiculos As String = JsonConvert.SerializeObject(EntidadCrearVehiculos, Newtonsoft.Json.Formatting.Indented)
                        Dim reqStringCrearVehiculos() As Byte = Encoding.Default.GetBytes(strJSONCrearVehiculos)
                        'Fin creación Json

                        'Se envía la solicitud:
                        Dim resByteCrearVehiculos As Byte() = webClient.UploadData(urlCrearVehiculos, "post", reqStringCrearVehiculos)

                        'Para obtener el código y mensaje de status del response de la solicitud, se extraen los atributos con ayuda de System.Reflectioon.BindingFlags , ya que son atributos no públicos de la clase WebClient:
                        Dim status As FieldInfo = webClient.GetType().GetField("m_WebResponse", BindingFlags.Instance Or BindingFlags.NonPublic)
                        If Not IsNothing(status) Then
                            Dim response As HttpWebResponse = status.GetValue(webClient)
                            If Not IsNothing(response) Then
                                statusDescription = response.StatusDescription
                                statusCode = Long.Parse(response.StatusCode)
                            End If

                        End If
                        'Fin Obtención Código y mensaje del response

                        webClient.Dispose()


                        'Se inserta en la tabla Registro_Sincronización_CARGA_APP el vehiculo sincronizado junto con el mensaje de respuesta:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", 23803, " & vehiculo.Item("Codigo") & ", '" & statusCode & " " & statusDescription & "', 1"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)




                    Catch ex As Exception
                        'En caso de generarse error, se inserta igualmente en la tabla de sincronización, con el mensaje respectivo, y con el campo "Sincronizado" = 0:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", 23803,  " & vehiculo.Item("Codigo") & ", '" & ex.Message & "', 0"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)

                        'Se deja registro en el archivo log del resultado:
                        If dtsSync.Tables.Count > 0 Then

                            If dtsSync.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                Me.objGeneral.Guardar_Mensaje_Log("Error en el vehiculo " & vehiculo.Item("Placa") & ". Error : " & ex.Message)

                            End If

                        End If
                        Me.objGeneral.Guardar_Mensaje_Log("Error funcion Sincronizacion Vehículos: " & ex.Message)
                    End Try
                Next
                'Se deja registro en el archivo log el total de registros procesados:
                Me.objGeneral.Guardar_Mensaje_Log("Número Vehículos Sincronizados: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)
            End If

        End If

        'Sincronización Vehículos Modificados:

        strSqlConsulta = "gsp_consultar_vehiculos_sincronizacion_CARGA_APP_pendientes_modificar " & Empresa
        dtsConsulta = objGeneral.Retorna_Dataset(strSqlConsulta)

        If dtsConsulta.Tables.Count > 0 Then
            If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                For Each vehiculo In dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows
                    Try
                        Dim statusDescription As String
                        Dim statusCode As Long

                        Me.Token_CARGA_APP = ObtenerToken_CARGA_APP()
                        'Headers Crear Vehículos:
                        webClient.Headers("content-type") = "application/json"
                        webClient.Headers("Authorization") = "Bearer " & Me.Token_CARGA_APP

                        'Se crea un objeto Json con el cuerpo de la solicitud : 
                        Dim EntidadCrearVehiculos = New Dictionary(Of String, Object)
                        EntidadCrearVehiculos.Add("placa", vehiculo.Item("Placa"))
                        EntidadCrearVehiculos.Add("activo", IIf(vehiculo.Item("Estado") = 1, True, False))
                        EntidadCrearVehiculos.Add("modelo", vehiculo.Item("Modelo"))
                        EntidadCrearVehiculos.Add("tipoVehiculo", Integer.Parse(vehiculo.Item("CATA_TIVE_Codigo")))
                        EntidadCrearVehiculos.Add("descripcionTipoVehiculo", vehiculo.Item("Tipo_Vehiculo"))
                        EntidadCrearVehiculos.Add("ejesCabezote", vehiculo.Item("Numero_Ejes"))
                        EntidadCrearVehiculos.Add("placaTrailer", vehiculo.Item("SEMI_Placa"))
                        EntidadCrearVehiculos.Add("tipoCarroceria", Integer.Parse(vehiculo.Item("SEMI_CATA_TICA_Codigo")))
                        EntidadCrearVehiculos.Add("descripcionTipoCarroceria", vehiculo.Item("TipoCarroceriaSemirremolque"))
                        EntidadCrearVehiculos.Add("ejesTrailer", vehiculo.Item("EjesSemirremolque"))
                        EntidadCrearVehiculos.Add("fechaExpiracionTecnicoMecanica", vehiculo.Item("fechaExpiracionTecnicoMecanica"))
                        EntidadCrearVehiculos.Add("fechaExpiracionSOAT", vehiculo.Item("fechaExpiracionSOAT"))
                        EntidadCrearVehiculos.Add("tieneReporteExterno", IIf(vehiculo.Item("ReporteExterno") = 1, True, False))
                        EntidadCrearVehiculos.Add("tieneReporteInterno", IIf(vehiculo.Item("ReporteInterno") = 1, True, False))
                        EntidadCrearVehiculos.Add("tieneSatelital", IIf(vehiculo.Item("Satelital") = 1, True, False))
                        EntidadCrearVehiculos.Add("codigoTipoCombustible", Integer.Parse(vehiculo.Item("CATA_TICO_Codigo")))
                        EntidadCrearVehiculos.Add("descripcionTipoCombustible", vehiculo.Item("TipoCombustible"))
                        EntidadCrearVehiculos.Add("codigoAlternoEmpresaGPS", vehiculo.Item("CodigoAlternoProveedorGPS"))
                        EntidadCrearVehiculos.Add("nombreEmpresaGPS", vehiculo.Item("NombreEmpresaGPS"))
                        EntidadCrearVehiculos.Add("usuarioGPS", vehiculo.Item("usuarioGPS"))
                        EntidadCrearVehiculos.Add("passwordGPS", vehiculo.Item("passwordGPS"))
                        EntidadCrearVehiculos.Add("codigoAlternoConductor", vehiculo.Item("CodigoAlternoConductor"))
                        EntidadCrearVehiculos.Add("codigoAlternoTenedor", vehiculo.Item("CodigoAlternoTenedor"))
                        EntidadCrearVehiculos.Add("codigoAlternoPropietario", vehiculo.Item("CodigoAlternoPropietario"))
                        EntidadCrearVehiculos.Add("codigoAlternoAdministrador", vehiculo.Item("CodigoAlternoAdministrador"))
                        EntidadCrearVehiculos.Add("esArticulado", IIf(vehiculo.Item("Articulado") = 1, True, False))


                        Dim strJSONCrearVehiculos As String = JsonConvert.SerializeObject(EntidadCrearVehiculos, Newtonsoft.Json.Formatting.Indented)
                        Dim reqStringCrearVehiculos() As Byte = Encoding.Default.GetBytes(strJSONCrearVehiculos)
                        'Fin creación Json

                        'Se envía la solicitud:
                        Dim resByteCrearVehiculos As Byte() = webClient.UploadData(urlCrearVehiculos, "put", reqStringCrearVehiculos)

                        'Para obtener el código y mensaje de status del response de la solicitud, se extraen los atributos con ayuda de System.Reflectioon.BindingFlags , ya que son atributos no públicos de la clase WebClient:
                        Dim status As FieldInfo = webClient.GetType().GetField("m_WebResponse", BindingFlags.Instance Or BindingFlags.NonPublic)
                        If Not IsNothing(status) Then
                            Dim response As HttpWebResponse = status.GetValue(webClient)
                            If Not IsNothing(response) Then
                                statusDescription = response.StatusDescription
                                statusCode = Long.Parse(response.StatusCode)
                            End If

                        End If
                        'Fin Obtención Código y mensaje del response
                        '
                        webClient.Dispose() '


                        'Se inserta en la tabla Registro_Sincronización_CARGA_APP el vehiculo sincronizado junto con el mensaje de respuesta:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", 23803, " & vehiculo.Item("Codigo") & ", '" & statusCode & " " & statusDescription & "', 1"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)



                    Catch ex As Exception
                        'En caso de generarse error, se inserta igualmente en la tabla de sincronización, con el mensaje respectivo, y con el campo "Sincronizado" = 0:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", 23803,  " & vehiculo.Item("Codigo") & ", '" & ex.Message & "', 0"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)

                        'Se deja registro en el archivo log del resultado:
                        If dtsSync.Tables.Count > 0 Then

                            If dtsSync.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                Me.objGeneral.Guardar_Mensaje_Log("Error en el vehículo " & vehiculo.Item("Placa") & ". Error : " & ex.Message)

                            End If

                        End If
                        Me.objGeneral.Guardar_Mensaje_Log("Error función Sincronizacion Vehículos: " & ex.Message)
                    End Try
                Next
                'Se deja registro en el archivo log el total de registros procesados:
                Me.objGeneral.Guardar_Mensaje_Log("Número Vehículos Modificados: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)
            End If

        End If


    End Sub

    Private Sub Iniciar_Proceso_Sincronizacion_Terceros()

        Dim strSql As String
        Dim strSqlConsulta As String
        Dim urlCrearTerceros As String = GEN_URL_CARGA_APP & API_CREAR_TERCERO
        Dim objResultCrearTerceros As objResponseCrearTerceros
        Dim webClient As New WebClient()
        Dim dtsSync As DataSet
        Dim dtsConsulta As DataSet
        strSqlConsulta = "gsp_consultar_terceros_sincronizacion_CARGA_APP " & Empresa & ",'1412,1408,'"
        dtsConsulta = objGeneral.Retorna_Dataset(strSqlConsulta)

        If dtsConsulta.Tables.Count > 0 Then
            If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                For Each tercero In dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows
                    Try
                        Dim statusDescription As String
                        Dim statusCode As Long

                        Me.Token_CARGA_APP = ObtenerToken_CARGA_APP()
                        'Headers Crear Vehículos:
                        webClient.Headers("content-type") = "application/json"
                        webClient.Headers("Authorization") = "Bearer " & Me.Token_CARGA_APP

                        'Se crea un objeto Json con el cuerpo de la solicitud : 
                        Dim EntidadCrearTerceros = New Dictionary(Of String, Object)
                        EntidadCrearTerceros.Add("codigoAlternoTercero", tercero.Item("Codigo_Alterno"))
                        EntidadCrearTerceros.Add("activo", IIf(tercero.Item("Estado") = 1, True, False))
                        EntidadCrearTerceros.Add("identificacion", tercero.Item("Numero_Identificacion"))
                        EntidadCrearTerceros.Add("nombres", tercero.Item("Nombres"))
                        EntidadCrearTerceros.Add("apellidos", tercero.Item("Apellidos"))
                        EntidadCrearTerceros.Add("direccion", tercero.Item("Direccion"))
                        EntidadCrearTerceros.Add("ciudad", tercero.Item("Ciudad"))
                        EntidadCrearTerceros.Add("celular", tercero.Item("Telefonos"))
                        EntidadCrearTerceros.Add("esTenedor", IIf(tercero.Item("Tenedor") = 1, True, False))
                        EntidadCrearTerceros.Add("esPropietario", IIf(tercero.Item("Propietario") = 1, True, False))
                        EntidadCrearTerceros.Add("esAdministrador", IIf(tercero.Item("Administrador") = 1, True, False))
                        EntidadCrearTerceros.Add("placaCabezote", tercero.Item("PlacaCabezote"))
                        EntidadCrearTerceros.Add("placaTrailer", tercero.Item("PlacaTrailer"))


                        Dim strJSONCreaTerceros As String = JsonConvert.SerializeObject(EntidadCrearTerceros, Newtonsoft.Json.Formatting.Indented)
                        Dim reqStringCrearTerceros() As Byte = Encoding.Default.GetBytes(strJSONCreaTerceros)
                        'Fin creación Json

                        'Se envía la solicitud:
                        Dim resByteCrearTerceros As Byte() = webClient.UploadData(urlCrearTerceros, "post", reqStringCrearTerceros)

                        'Para obtener el código y mensaje de status del response de la solicitud, se extraen los atributos con ayuda de System.Reflectioon.BindingFlags , ya que son atributos no públicos de la clase WebClient:
                        Dim status As FieldInfo = webClient.GetType().GetField("m_WebResponse", BindingFlags.Instance Or BindingFlags.NonPublic)
                        If Not IsNothing(status) Then
                            Dim response As HttpWebResponse = status.GetValue(webClient)
                            If Not IsNothing(response) Then
                                statusDescription = response.StatusDescription
                                statusCode = Long.Parse(response.StatusCode)
                            End If

                        End If
                        'Fin Obtención Código y mensaje del response

                        webClient.Dispose()


                        'Se inserta en la tabla Registro_Sincronización_CARGA_APP el tercero sincronizado junto con el mensaje de respuesta:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & "," & ORIGEN_SINCRONIZACION_TERCEROS & ", " & tercero.Item("Codigo") & ", '" & statusCode & " " & statusDescription & "', 1"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)

                    Catch ex As Exception
                        'En caso de generarse error, se inserta igualmente en la tabla de sincronización, con el mensaje respectivo, y con el campo "Sincronizado" = 0:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", " & ORIGEN_SINCRONIZACION_TERCEROS & ",  " & tercero.Item("Codigo") & ", '" & ex.Message & "', 0"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)

                        'Se deja registro en el archivo log del resultado:
                        If dtsSync.Tables.Count > 0 Then

                            If dtsSync.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                Me.objGeneral.Guardar_Mensaje_Log("Error en el tercero " & tercero.Item("Nombres") & ". Error : " & ex.Message)

                            End If

                        End If
                        Me.objGeneral.Guardar_Mensaje_Log("Error funcion Sincronizacion Terceros: " & ex.Message)
                    End Try
                Next
                'Se deja registro en el archivo log el total de registros procesados:
                Me.objGeneral.Guardar_Mensaje_Log("Número Terceros Sincronizados: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)
            End If

        End If

        'Sincronización Terceros Modificados:
        strSqlConsulta = "gsp_consultar_terceros_sincronizacion_CARGA_APP_pendientes_sincronizar " & Empresa & ",'1412,1408,'"
        dtsConsulta = objGeneral.Retorna_Dataset(strSqlConsulta)

        If dtsConsulta.Tables.Count > 0 Then
            If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                For Each tercero In dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows
                    Try
                        Dim statusDescription As String
                        Dim statusCode As Long

                        Me.Token_CARGA_APP = ObtenerToken_CARGA_APP()
                        'Headers Crear Vehículos:
                        webClient.Headers("content-type") = "application/json"
                        webClient.Headers("Authorization") = "Bearer " & Me.Token_CARGA_APP

                        'Se crea un objeto Json con el cuerpo de la solicitud : 
                        Dim EntidadCrearTerceros = New Dictionary(Of String, Object)
                        EntidadCrearTerceros.Add("codigoAlternoTercero", tercero.Item("Codigo_Alterno"))
                        EntidadCrearTerceros.Add("activo", IIf(tercero.Item("Estado") = 1, True, False))
                        EntidadCrearTerceros.Add("identificacion", tercero.Item("Numero_Identificacion"))
                        EntidadCrearTerceros.Add("nombres", tercero.Item("Nombres"))
                        EntidadCrearTerceros.Add("apellidos", tercero.Item("Apellidos"))
                        EntidadCrearTerceros.Add("direccion", tercero.Item("Direccion"))
                        EntidadCrearTerceros.Add("ciudad", tercero.Item("Ciudad"))
                        EntidadCrearTerceros.Add("celular", tercero.Item("Telefonos"))
                        EntidadCrearTerceros.Add("esTenedor", IIf(tercero.Item("Tenedor") = 1, True, False))
                        EntidadCrearTerceros.Add("esPropietario", IIf(tercero.Item("Propietario") = 1, True, False))
                        EntidadCrearTerceros.Add("esAdministrador", IIf(tercero.Item("Administrador") = 1, True, False))
                        EntidadCrearTerceros.Add("placaCabezote", tercero.Item("PlacaCabezote"))
                        EntidadCrearTerceros.Add("placaTrailer", tercero.Item("PlacaTrailer"))


                        Dim strJSONCreaTerceros As String = JsonConvert.SerializeObject(EntidadCrearTerceros, Newtonsoft.Json.Formatting.Indented)
                        Dim reqStringCrearTerceros() As Byte = Encoding.Default.GetBytes(strJSONCreaTerceros)
                        'Fin creación Json

                        'Se envía la solicitud:
                        Dim resByteCrearTerceros As Byte() = webClient.UploadData(urlCrearTerceros, "put", reqStringCrearTerceros)

                        'Para obtener el código y mensaje de status del response de la solicitud, se extraen los atributos con ayuda de System.Reflectioon.BindingFlags , ya que son atributos no públicos de la clase WebClient:
                        Dim status As FieldInfo = webClient.GetType().GetField("m_WebResponse", BindingFlags.Instance Or BindingFlags.NonPublic)
                        If Not IsNothing(status) Then
                            Dim response As HttpWebResponse = status.GetValue(webClient)
                            If Not IsNothing(response) Then
                                statusDescription = response.StatusDescription
                                statusCode = Long.Parse(response.StatusCode)
                            End If

                        End If
                        'Fin Obtención Código y mensaje del response

                        webClient.Dispose()


                        'Se inserta en la tabla Registro_Sincronización_CARGA_APP el tercero sincronizado junto con el mensaje de respuesta:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", " & ORIGEN_SINCRONIZACION_TERCEROS & ", " & tercero.Item("Codigo") & ", '" & statusCode & " " & statusDescription & "', 1"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)



                    Catch ex As Exception
                        'En caso de generarse error, se inserta igualmente en la tabla de sincronización, con el mensaje respectivo, y con el campo "Sincronizado" = 0:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", " & ORIGEN_SINCRONIZACION_TERCEROS & ",  " & tercero.Item("Codigo") & ", '" & ex.Message & "', 0"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)

                        'Se deja registro en el archivo log del resultado:
                        If dtsSync.Tables.Count > 0 Then

                            If dtsSync.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                Me.objGeneral.Guardar_Mensaje_Log("Error en el tercero " & tercero.Item("Nombres") & ". Error : " & ex.Message)

                            End If

                        End If
                        Me.objGeneral.Guardar_Mensaje_Log("Error funcion Modificacion Terceros: " & ex.Message)
                    End Try
                Next
                'Se deja registro en el archivo log el total de registros procesados:
                Me.objGeneral.Guardar_Mensaje_Log("Número Terceros Modificados: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)
            End If

        End If


    End Sub

    Private Sub Iniciar_Proceso_Sincronizacion_Conductores()

        Dim strSql As String
        Dim strSqlConsulta As String
        Dim urlCrearConductor As String = GEN_URL_CARGA_APP & API_CREAR_CONDUCTOR
        Dim objResultCrearConductor As objResponseCrearTerceros
        Dim webClient As New WebClient()
        Dim dtsSync As DataSet
        Dim dtsConsulta As DataSet
        strSqlConsulta = "gsp_consultar_conductores_sincronizacion_CARGA_APP " & Empresa & ",'1403,'"
        dtsConsulta = objGeneral.Retorna_Dataset(strSqlConsulta)

        If dtsConsulta.Tables.Count > 0 Then
            If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                For Each conductor In dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows
                    Try
                        Dim statusDescription As String
                        Dim statusCode As Long

                        Me.Token_CARGA_APP = ObtenerToken_CARGA_APP()
                        'Headers Crear Conductores:
                        webClient.Headers("content-type") = "application/json"
                        webClient.Headers("Authorization") = "Bearer " & Me.Token_CARGA_APP

                        'Se crea un objeto Json con el cuerpo de la solicitud : 
                        Dim EntidadCrearConductores = New Dictionary(Of String, Object)
                        EntidadCrearConductores.Add("codigoAlternoTercero", conductor.Item("Codigo_Alterno"))
                        EntidadCrearConductores.Add("activo", IIf(conductor.Item("Estado") = 1, True, False))
                        EntidadCrearConductores.Add("identificacion", conductor.Item("Numero_Identificacion"))
                        EntidadCrearConductores.Add("nombres", conductor.Item("Nombres"))
                        EntidadCrearConductores.Add("apellidos", conductor.Item("Apellidos"))
                        EntidadCrearConductores.Add("direccion", conductor.Item("Direccion"))
                        EntidadCrearConductores.Add("ciudad", conductor.Item("Ciudad"))
                        EntidadCrearConductores.Add("email", conductor.Item("Emails"))
                        EntidadCrearConductores.Add("celular", conductor.Item("Telefonos"))
                        EntidadCrearConductores.Add("categoria", Integer.Parse(conductor.Item("CATA_CCCV_Codigo")))
                        EntidadCrearConductores.Add("descripcionCategoria", conductor.Item("DescripcionCategoria"))
                        EntidadCrearConductores.Add("fechaExpiracionCarnetDIACO", conductor.Item("fechaExpiracionCarnetDIACO"))
                        EntidadCrearConductores.Add("fechaExpiracionARL", conductor.Item("fechaExpiracionARL"))
                        EntidadCrearConductores.Add("tieneReportes", IIf(conductor.Item("Reportes") = 1, True, False))
                        EntidadCrearConductores.Add("tieneComparendos", IIf(conductor.Item("Comparendos") = 1, True, False))
                        EntidadCrearConductores.Add("listadoClientesBloqueados", conductor.Item("ListadoClientesBloqueados"))
                        EntidadCrearConductores.Add("bloqueadoParaSeguimientoEspecial", IIf(conductor.Item("BloqueadosSeguimientoEspecial") = 1, True, False))
                        EntidadCrearConductores.Add("fechaVencimientoLicencia", conductor.Item("fechaVencimientoLicencia"))
                        EntidadCrearConductores.Add("fechaVencimientoCarnetMercanciaPeligrosa", conductor.Item("fechaVencimientoCarnetMercanciaPeligrosa"))
                        EntidadCrearConductores.Add("placaCabezote", conductor.Item("PlacaCabezote"))
                        EntidadCrearConductores.Add("placaTrailer", conductor.Item("PlacaTrailer"))


                        Dim strJSONCreaConductores As String = JsonConvert.SerializeObject(EntidadCrearConductores, Newtonsoft.Json.Formatting.Indented)
                        Dim reqStringCrearConductores() As Byte = Encoding.Default.GetBytes(strJSONCreaConductores)
                        'Fin creación Json

                        'Se envía la solicitud:
                        Dim resByteCrearConductores As Byte() = webClient.UploadData(urlCrearConductor, "post", reqStringCrearConductores)

                        'Para obtener el código y mensaje de status del response de la solicitud, se extraen los atributos con ayuda de System.Reflectioon.BindingFlags , ya que son atributos no públicos de la clase WebClient:
                        Dim status As FieldInfo = webClient.GetType().GetField("m_WebResponse", BindingFlags.Instance Or BindingFlags.NonPublic)
                        If Not IsNothing(status) Then
                            Dim response As HttpWebResponse = status.GetValue(webClient)
                            If Not IsNothing(response) Then
                                statusDescription = response.StatusDescription
                                statusCode = Long.Parse(response.StatusCode)
                            End If

                        End If
                        'Fin Obtención Código y mensaje del response
                        webClient.Dispose()


                        'Se inserta en la tabla Registro_Sincronización_CARGA_APP el conductor sincronizado junto con el mensaje de respuesta:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", 23805, " & conductor.Item("Codigo") & ", '" & statusCode & " " & statusDescription & "', 1"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)


                    Catch ex As Exception
                        'En caso de generarse error, se inserta igualmente en la tabla de sincronización, con el mensaje respectivo, y con el campo "Sincronizado" = 0:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", 23805,  " & conductor.Item("Codigo") & ", '" & ex.Message & "', 0"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)

                        'Se deja registro en el archivo log del resultado:
                        If dtsSync.Tables.Count > 0 Then

                            If dtsSync.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                Me.objGeneral.Guardar_Mensaje_Log("Error en el conductor " & conductor.Item("Nombres") & ". Error : " & ex.Message)

                            End If

                        End If
                        Me.objGeneral.Guardar_Mensaje_Log("Error funcion Sincronizacion conductores: " & ex.Message)
                    End Try
                Next
                'Se deja registro en el archivo log el total de registros procesados:
                Me.objGeneral.Guardar_Mensaje_Log("Número conductores Sincronizados: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)
            End If

        End If

        'Sincronización Conductores Modificados:
        strSqlConsulta = "gsp_consultar_conductores_sincronizacion_CARGA_APP_pendientes_modificar " & Empresa & ",'1403,'"
        dtsConsulta = objGeneral.Retorna_Dataset(strSqlConsulta)

        If dtsConsulta.Tables.Count > 0 Then
            If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                For Each conductor In dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows
                    Try
                        Dim statusDescription As String
                        Dim statusCode As Long

                        Me.Token_CARGA_APP = ObtenerToken_CARGA_APP()
                        'Headers Modificar Conductores:
                        webClient.Headers("content-type") = "application/json"
                        webClient.Headers("Authorization") = "Bearer " & Me.Token_CARGA_APP

                        'Se crea un objeto Json con el cuerpo de la solicitud : 
                        Dim EntidadCrearConductores = New Dictionary(Of String, Object)
                        EntidadCrearConductores.Add("codigoAlternoTercero", conductor.Item("Codigo_Alterno"))
                        EntidadCrearConductores.Add("activo", IIf(conductor.Item("Estado") = 1, True, False))
                        EntidadCrearConductores.Add("identificacion", conductor.Item("Numero_Identificacion"))
                        EntidadCrearConductores.Add("nombres", conductor.Item("Nombres"))
                        EntidadCrearConductores.Add("apellidos", conductor.Item("Apellidos"))
                        EntidadCrearConductores.Add("direccion", conductor.Item("Direccion"))
                        EntidadCrearConductores.Add("ciudad", conductor.Item("Ciudad"))
                        EntidadCrearConductores.Add("email", conductor.Item("Emails"))
                        EntidadCrearConductores.Add("celular", conductor.Item("Telefonos"))
                        EntidadCrearConductores.Add("categoria", Integer.Parse(conductor.Item("CATA_CCCV_Codigo")))
                        EntidadCrearConductores.Add("descripcionCategoria", conductor.Item("DescripcionCategoria"))
                        EntidadCrearConductores.Add("fechaExpiracionCarnetDIACO", conductor.Item("fechaExpiracionCarnetDIACO"))
                        EntidadCrearConductores.Add("fechaExpiracionARL", conductor.Item("fechaExpiracionARL"))
                        EntidadCrearConductores.Add("tieneReportes", IIf(conductor.Item("Reportes") = 1, True, False))
                        EntidadCrearConductores.Add("tieneComparendos", IIf(conductor.Item("Comparendos") = 1, True, False))
                        EntidadCrearConductores.Add("listadoClientesBloqueados", conductor.Item("ListadoClientesBloqueados"))
                        EntidadCrearConductores.Add("bloqueadoParaSeguimientoEspecial", IIf(conductor.Item("BloqueadosSeguimientoEspecial") = 1, True, False))
                        EntidadCrearConductores.Add("fechaVencimientoLicencia", conductor.Item("fechaVencimientoLicencia"))
                        EntidadCrearConductores.Add("fechaVencimientoCarnetMercanciaPeligrosa", conductor.Item("fechaVencimientoCarnetMercanciaPeligrosa"))
                        EntidadCrearConductores.Add("placaCabezote", conductor.Item("PlacaCabezote"))
                        EntidadCrearConductores.Add("placaTrailer", conductor.Item("PlacaTrailer"))


                        Dim strJSONCreaConductores As String = JsonConvert.SerializeObject(EntidadCrearConductores, Newtonsoft.Json.Formatting.Indented)
                        Dim reqStringCrearConductores() As Byte = Encoding.Default.GetBytes(strJSONCreaConductores)
                        'Fin creación Json

                        'Se envía la solicitud:
                        Dim resByteCrearConductores As Byte() = webClient.UploadData(urlCrearConductor, "put", reqStringCrearConductores)

                        'Para obtener el código y mensaje de status del response de la solicitud, se extraen los atributos con ayuda de System.Reflectioon.BindingFlags , ya que son atributos no públicos de la clase WebClient:
                        Dim status As FieldInfo = webClient.GetType().GetField("m_WebResponse", BindingFlags.Instance Or BindingFlags.NonPublic)
                        If Not IsNothing(status) Then
                            Dim response As HttpWebResponse = status.GetValue(webClient)
                            If Not IsNothing(response) Then
                                statusDescription = response.StatusDescription
                                statusCode = Long.Parse(response.StatusCode)
                            End If

                        End If
                        'Fin Obtención Código y mensaje del response

                        webClient.Dispose()


                        'Se inserta en la tabla Registro_Sincronización_CARGA_APP el conductor modificado junto con el mensaje de respuesta:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", 23805, " & conductor.Item("Codigo") & ", '" & statusCode & " " & statusDescription & "', 1"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)


                    Catch ex As Exception
                        'En caso de generarse error, se inserta igualmente en la tabla de sincronización, con el mensaje respectivo, y con el campo "Sincronizado" = 0:
                        strSql = "gsp_insertar_sincronizacion_CARGA_APP " & Empresa & ", 23805,  " & conductor.Item("Codigo") & ", '" & ex.Message & "', 0"
                        dtsSync = objGeneral.Retorna_Dataset(strSql)

                        'Se deja registro en el archivo log del resultado:
                        If dtsSync.Tables.Count > 0 Then

                            If dtsSync.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                Me.objGeneral.Guardar_Mensaje_Log("Error en el conductor " & conductor.Item("Nombres") & ". Error : " & ex.Message)

                            End If

                        End If
                        Me.objGeneral.Guardar_Mensaje_Log("Error función Modificación conductores: " & ex.Message)
                    End Try
                Next
                'Se deja registro en el archivo log el total de registros procesados:
                Me.objGeneral.Guardar_Mensaje_Log("Número conductores Modificados: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)
            End If

        End If


    End Sub
#End Region


End Class
