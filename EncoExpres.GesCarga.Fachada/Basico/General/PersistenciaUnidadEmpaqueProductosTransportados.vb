﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaUnidadEmpaqueProductosTransportados
        Implements IPersistenciaBase(Of UnidadEmpaqueProductosTransportados)

        Public Function Consultar(filtro As UnidadEmpaqueProductosTransportados) As IEnumerable(Of UnidadEmpaqueProductosTransportados) Implements IPersistenciaBase(Of UnidadEmpaqueProductosTransportados).Consultar
            Return New RepositorioUnidadEmpaqueProductosTransportados().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As UnidadEmpaqueProductosTransportados) As Long Implements IPersistenciaBase(Of UnidadEmpaqueProductosTransportados).Insertar
            Return New RepositorioUnidadEmpaqueProductosTransportados().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As UnidadEmpaqueProductosTransportados) As Long Implements IPersistenciaBase(Of UnidadEmpaqueProductosTransportados).Modificar
            Return New RepositorioUnidadEmpaqueProductosTransportados().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As UnidadEmpaqueProductosTransportados) As UnidadEmpaqueProductosTransportados Implements IPersistenciaBase(Of UnidadEmpaqueProductosTransportados).Obtener
            Return New RepositorioUnidadEmpaqueProductosTransportados().Obtener(filtro)
        End Function

        Public Function Anular(entidad As UnidadEmpaqueProductosTransportados) As Boolean
            Return New RepositorioUnidadEmpaqueProductosTransportados().Anular(entidad)
        End Function
    End Class
End Namespace