﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Paqueteria

Namespace Gesphone

    ''' <summary>
    ''' Clase que tiene la informacion de gestion documentos de terceros a sincronizar.
    ''' </summary>
    <JsonObject>
    Public Class SyncGestionDocumentos

        Sub New(gestion As GestionDocumentoCliente)
            IdDo = gestion.TipoDocumento.Codigo
            NomDo = gestion.TipoDocumento.Nombre
            IdGe = gestion.TipoGestion.Codigo
            NomGe = gestion.TipoGestion.Nombre
            Gc = gestion.GeneraCobro
            Es = gestion.Estado.Codigo
            En = 0
        End Sub

        Sub New(gestion As GestionDocumentosRemesa)
            IdDo = gestion.TipoDocumento.Codigo
            NomDo = gestion.TipoDocumento.Nombre
            IdGe = gestion.TipoGestion.Codigo
            NomGe = gestion.TipoGestion.Nombre
            Gc = 0
            Es = 0
            En = gestion.Entregado
        End Sub

        Public Property IdDo As Integer

        Public Property NomDo As String

        Public Property IdGe As Integer

        Public Property NomGe As String

        Public Property Gc As Short

        Public Property Es As Integer

        Public Property En As Integer

    End Class

    <JsonObject>
    Public Class SyncIntentoEntrega

        Sub New(item As DetalleIntentoEntregaGuia)
            Id = item.ID
            Obs = item.Observaciones
            Nov = item.NovedadIntentoEntrega
            FechaCrea = ""

            If (IsNothing(item.FechaCrea) = False) Then
                FechaCrea = $"{item.FechaCrea.Month}/{item.FechaCrea.Day}/{item.FechaCrea.Year} {item.FechaCrea.Hour}:{item.FechaCrea.Minute}"
            End If

        End Sub

        Public Property Id As Integer

        Public Property Obs As String

        Public Property Nov As String

        Public Property FechaCrea As String

    End Class

End Namespace
