﻿	PRINT 'gsp_consultar_cajas'
	GO
	DROP PROCEDURE gsp_consultar_cajas
	GO 
	CREATE PROCEDURE gsp_consultar_cajas
	(

		@par_EMPR_Codigo SMALLINT,
		@par_Codigo NUMERIC = NULL,
		@par_Nombre VARCHAR(50) = NULL,
		@par_Codigo_Alterno VARCHAR(30) = NULL,
		@par_Estado SMALLINT = NULL,
		@par_NumeroPagina INT = NULL,
		@par_RegistrosPagina INT = NULL
	)
	AS
	BEGIN
		SET NOCOUNT ON;
			DECLARE
				@CantidadRegistros	INT
			SELECT @CantidadRegistros = (
					SELECT DISTINCT 
						COUNT(1) 
						FROM
						Cajas CAJA,
						Plan_Unico_Cuentas PLUC,
						Oficinas OFIC

						WHERE

						CAJA.EMPR_Codigo = PLUC.EMPR_Codigo
						AND CAJA.PLUC_Codigo = PLUC.Codigo

						AND CAJA.EMPR_Codigo = OFIC.EMPR_Codigo 
						AND CAJA.OFIC_Codigo = OFIC.Codigo
						AND CAJA.Codigo > 0
						AND CAJA.EMPR_Codigo = @par_EMPR_Codigo
						AND CAJA.Codigo = ISNULL(@par_Codigo, CAJA.Codigo)
						AND CAJA.Estado = ISNULL(@par_Estado, CAJA.Estado)
						AND ((CAJA.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))
						AND ((CAJA.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))
						);
					       
					WITH Pagina AS
					(

						SELECT 
						CAJA.EMPR_Codigo,
						CAJA.Codigo,
						CAJA.Codigo_Alterno,
						CAJA.Nombre,
						CAJA.PLUC_Codigo,
						CAJA.OFIC_Codigo,
						CAJA.Estado,
						PLUC.Nombre AS CuentaPUC,
						OFIC.Nombre AS Oficina,
						ROW_NUMBER() OVER(ORDER BY CAJA.Nombre) AS RowNumber
						FROM
						Cajas CAJA,
						Plan_Unico_Cuentas PLUC,
						Oficinas OFIC

						WHERE

						CAJA.EMPR_Codigo = PLUC.EMPR_Codigo
						AND CAJA.PLUC_Codigo = PLUC.Codigo

						AND CAJA.EMPR_Codigo = OFIC.EMPR_Codigo 
						AND CAJA.OFIC_Codigo = OFIC.Codigo
						AND CAJA.Codigo > 0
						AND CAJA.EMPR_Codigo = @par_EMPR_Codigo
						AND CAJA.Codigo = ISNULL(@par_Codigo, CAJA.Codigo)
						AND CAJA.Estado = ISNULL(@par_Estado, CAJA.Estado)
						AND ((CAJA.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))
						AND ((CAJA.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))

			)
			SELECT DISTINCT
			0 AS Obtener,
			EMPR_Codigo,
			Codigo,
			Codigo_Alterno,
			Nombre,
			PLUC_Codigo,
			OFIC_Codigo,
			Estado,
			CuentaPUC,
			Oficina,
			@CantidadRegistros AS TotalRegistros,
			@par_NumeroPagina AS PaginaObtener,
			@par_RegistrosPagina AS RegistrosPagina
			FROM
				Pagina
			WHERE
				RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
				AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
			order by Nombre
	END
	GO