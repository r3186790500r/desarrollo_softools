﻿print 'gsp_consultar_tipo_tarifa_transporte'
go
drop procedure gsp_consultar_tipo_tarifa_transporte
go
create procedure gsp_consultar_tipo_tarifa_transporte
(
@par_EMPR_Codigo smallint,
@par_Estado smallint
)
as begin
select 
TTTC.EMPR_Codigo,
CONT.Campo1 +DECO.Campo1 +TIVE.Campo1 +CAVE.Campo1  AS Nombre,
TTTC.Codigo,
TTTC.Estado,
TTTC.TATC_Codigo
from Tipo_Tarifa_Transporte_Carga TTTC

LEFT JOIN Valor_Catalogos AS CONT
ON TTTC.EMPR_Codigo = CONT.EMPR_Codigo
AND TTTC.CATA_CONT_Codigo = CONT.Codigo

LEFT JOIN Valor_Catalogos AS DECO
ON TTTC.EMPR_Codigo = DECO.EMPR_Codigo
AND TTTC.CATA_DECO_Codigo = DECO.Codigo

LEFT JOIN Valor_Catalogos AS TIVE
ON TTTC.EMPR_Codigo = TIVE.EMPR_Codigo
AND TTTC.CATA_TIVE_Codigo = TIVE.Codigo

LEFT JOIN Valor_Catalogos AS CAVE
ON TTTC.EMPR_Codigo = CAVE.EMPR_Codigo
AND TTTC.CATA_CAVE_Codigo = CAVE.Codigo

where TTTC.EMPR_Codigo = @par_EMPR_Codigo
and TTTC.Estado = @par_Estado
end
go