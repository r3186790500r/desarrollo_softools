﻿Imports Newtonsoft.Json

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref=" EncabezadoUbicacionAlmacenes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoUbicacionAlmacenes
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoUbicacionAlmacenes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoUbicacionAlmacenes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Almacen = New Almacenes With {.Codigo = Read(lector, "ALMA_Codigo"), .Nombre = Read(lector, "Almacen")}
            NumeroFilas = Read(lector, "Numero_Filas")
            NumeroColumnas = Read(lector, "Numero_Columnas")
            NumeroNiveles = Read(lector, "Numero_Niveles")
            Estado = Read(lector, "Estado")
            Try
                TotalRegistros = Read(lector, "TotalRegistros")
            Catch
            End Try

        End Sub

        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Almacen As Almacenes

        ''' <summary>
        ''' Obtiene o establece el contacto del almacen
        ''' </summary>
        <JsonProperty>
        Public Property NumeroFilas As Integer

        ''' <summary>
        ''' Obtiene o establece el codigo alterno
        ''' </summary>
        <JsonProperty>
        Public Property NumeroColumnas As Integer
        ''' <summary>
        ''' Obtiene o establece el codigo alterno
        ''' </summary>
        <JsonProperty>
        Public Property NumeroNiveles As Integer

        ''' <summary>
        ''' Obtiene o establece
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty>
        Public Property Detalles As IEnumerable(Of DetalleUbicacionAlmacenes)

#Region "Filtros de Búsqueda"

#End Region

    End Class
End Namespace
