﻿EncoExpresApp.controller("GestionarPuestoControlesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'PuestoControlesFactory', 'TercerosFactory', 'blockUIConfig', 'ValorCatalogosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, PuestoControlesFactory, TercerosFactory, blockUIConfig, ValorCatalogosFactory) {

        $scope.Titulo = 'GESTIONAR PUESTOS DE CONTROL';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Puestos de Control' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PUESTO_CONTROLES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Deshabilitar = true;
        $scope.ListadoCiudadesDestino = [];
        $scope.ListadoCiudadesOrigen = [];
        $scope.ListadoEstados = [];
        $scope.ListadoProveedor = [];
        var codigotipopuestocontrol = 0;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,
            TipoPuestoControl : {}
        }
        /*Cargar el combo de tipo naturalezas*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PUESTO_CONTROL } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (codigotipopuestocontrol == CERO) {
                        $scope.ListadotipoPuestoControl = [];
                        $scope.ListadotipoPuestoControl.push({ Codigo: -1, Nombre: '(Seleccione Item)' });
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListadotipoPuestoControl.push(response.data.Datos[i]);

                            }

                            try {
                                $scope.Modelo.TipoPuestoControl = $linq.Enumerable().From($scope.ListadotipoPuestoControl).First('$.Codigo == -1');
                            } catch (e) {
                                $scope.Modelo.TipoPuestoControl = $scope.ListadotipoPuestoControl[0]
                            }
                        }
                        else {
                            $scope.ListadotipoPuestoControl = []
                        }
                    }

                }
            }, function (response) {
            });

        $scope.iniciarMapa = function (lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }

                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };

                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }
        /* Obtener parametros*/

        $scope.iniciarMapa()

        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0]

        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;

        }


        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando puesto de control código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando puesto de control Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            PuestoControlesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.Proveedor = $scope.CargarTercero($scope.Modelo.Proveedor.Codigo)
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado);
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        codigotipopuestocontrol = response.data.Datos.TipoPuestoControl.Codigo
                        if (codigotipopuestocontrol != '' && codigotipopuestocontrol != undefined && codigotipopuestocontrol != null) {
                            $scope.Modelo.TipoPuestoControl = $linq.Enumerable().From($scope.ListadotipoPuestoControl).First('$.Codigo ==' + codigotipopuestocontrol);
                        }
                        $scope.Modelo.Latitud = response.data.Datos.Latitud
                        $scope.Modelo.Longitud = response.data.Datos.Longitud
                        $scope.iniciarMapa($scope.Modelo.Latitud, $scope.Modelo.Longitud)


                    }
                    else {
                        ShowError('No se logro consultar la ruta código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarPuestosControl';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarPuestosControl';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_PROVEEDOR,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoProveedor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedor)
                }
            }
            return $scope.ListadoProveedor
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                $scope.Modelo.Latitud = parseFloat($scope.Modelo.Latitud)
                $scope.Modelo.Longitud = parseFloat($scope.Modelo.Longitud)


                ObjEnviar = {
                    Codigo: $scope.Modelo.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Nombre: $scope.Modelo.Nombre,
                    CodigoAlterno: $scope.Modelo.CodigoAlterno,
                    Proveedor: { Codigo: $scope.Modelo.Proveedor.Codigo },
                    Contacto: $scope.Modelo.Contacto,
                    Telefono: $scope.Modelo.Telefono,
                    Celular: $scope.Modelo.Celular,
                    Ubicacion: $scope.Modelo.Ubicacion,
                    Estado: $scope.Modelo.Estado.Codigo,
                    Longitud: $scope.Modelo.Longitud,
                    Latitud: $scope.Modelo.Latitud,
                    TipoPuestoControl: { Codigo: $scope.Modelo.TipoPuestoControl.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }



                PuestoControlesFactory.Guardar(ObjEnviar).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el puesto de control "' + $scope.Modelo.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se modificó el puesto de control "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = '#!ConsultarPuestosControl/' +response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        if (response.statusText.includes('IX_Puesto_Controles_Nombre')) {
                            ShowError('No se pudo guardar, el nombre ingresado ya está siendo utilizado')
                        } else if (response.statusText.includes('IX_Puesto_Controles_Codigo_Alterno')) {
                            ShowError('No se pudo guardar, el código alterno ingresado ya está siendo utilizado')
                        } else {
                            ShowError(response.statusText);
                        }
                    });
            }
        };


        function DatosRequeridos() {
            $scope.MensajesError = [];
            window.scrollTo(top, top);
            var modelo = $scope.Modelo
            var continuar = true;
            if (ValidarCampo(modelo.Nombre) == 1) {
                $scope.MensajesError.push('Debe ingresar el nombre');
                continuar = false;
            }
            if ($scope.Modelo.TipoPuestoControl.Codigo == -1) {
                $scope.MensajesError.push('Debe seleccionar tipo de puesto control');
                continuar = false;
            }
            if (ValidarCampo(modelo.Contacto) == 1) {
                $scope.MensajesError.push('Debe ingresar el contacto');
                continuar = false;
            }
            if (ValidarCampo(modelo.Telefono) == 1) {
                $scope.MensajesError.push('Debe ingresar el teléfono');
                continuar = false;
            }
            if (ValidarCampo(modelo.Celular) == 1) {
                $scope.MensajesError.push('Debe ingresar el celular');
                continuar = false;
            }
            if (ValidarCampo(modelo.Ubicacion) == 1) {
                $scope.MensajesError.push('Debe ingresar la ubicación');
                continuar = false;
            }

            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarPuestosControl'
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MasckDecimal = function (option) {
            return MascaraDecimales(option)
        }



    }]);