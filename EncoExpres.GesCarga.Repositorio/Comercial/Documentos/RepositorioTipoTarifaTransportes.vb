﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioTipoTarifaTransportes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTipoTarifaTransportes
        Inherits RepositorioBase(Of TipoTarifaTransportes)

        Public Overrides Function Consultar(filtro As TipoTarifaTransportes) As IEnumerable(Of TipoTarifaTransportes)
            Dim lista As New List(Of TipoTarifaTransportes)
            Dim item As TipoTarifaTransportes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)

                If Not IsNothing(filtro.TarifaTransporte) Then
                    If filtro.TarifaTransporte.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TATC_Codigo", filtro.TarifaTransporte.Codigo)
                    End If
                End If

                If filtro.OpcionPlantilla > 0 Then
                    conexion.AgregarParametroSQL("@par_LNTC_Codigo", filtro.TipoLineaNegocioTransporte.LineaNegocioTransporte.Codigo)
                    conexion.AgregarParametroSQL("@par_TLNC_Codigo", filtro.TipoLineaNegocioTransporte.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tipo_tarifa_transporte_plantilla]")
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tipo_tarifa_transporte]")
                End If
                While resultado.Read
                    item = New TipoTarifaTransportes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As TipoTarifaTransportes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As TipoTarifaTransportes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As TipoTarifaTransportes) As TipoTarifaTransportes
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As TipoTarifaTransportes) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace