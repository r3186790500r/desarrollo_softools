﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Repositorio.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class PersistenciaEncabezadoSolicitudOrdenServicios
        Implements IPersistenciaBase(Of EncabezadoSolicitudOrdenServicios)

        Public Function Consultar(filtro As EncabezadoSolicitudOrdenServicios) As IEnumerable(Of EncabezadoSolicitudOrdenServicios) Implements IPersistenciaBase(Of EncabezadoSolicitudOrdenServicios).Consultar
            Return New RepositorioEncabezadoSolicitudOrdenServicios().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of EncabezadoSolicitudOrdenServicios).Insertar
            Return New RepositorioEncabezadoSolicitudOrdenServicios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of EncabezadoSolicitudOrdenServicios).Modificar
            Return New RepositorioEncabezadoSolicitudOrdenServicios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoSolicitudOrdenServicios) As EncabezadoSolicitudOrdenServicios Implements IPersistenciaBase(Of EncabezadoSolicitudOrdenServicios).Obtener
            Return New RepositorioEncabezadoSolicitudOrdenServicios().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EncabezadoSolicitudOrdenServicios) As EncabezadoSolicitudOrdenServicios
            Return New RepositorioEncabezadoSolicitudOrdenServicios().Anular(entidad)
        End Function

        Public Function ObtenerInformacionControlCupo(filtro As EncabezadoSolicitudOrdenServicios) As EncabezadoSolicitudOrdenServicios
            Return New RepositorioEncabezadoSolicitudOrdenServicios().ObtenerInformacionControlCupo(filtro)
        End Function

        Public Function ObtenerOrdenServicioProgramacion(filtro As EncabezadoSolicitudOrdenServicios) As EncabezadoSolicitudOrdenServicios
            Return New RepositorioEncabezadoSolicitudOrdenServicios().ObtenerOrdenServicioProgramacion(filtro)
        End Function
    End Class

End Namespace

