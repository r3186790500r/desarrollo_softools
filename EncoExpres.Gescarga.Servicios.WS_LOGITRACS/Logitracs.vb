﻿Imports System.Threading
Imports System.Data.SqlClient
Imports System.Net
Imports System.Text
Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports Newtonsoft.Json
Imports Softtools.Gescarga.Servicios.WS_LOGITRACS.ReferenciaServicioLogitracs
Imports System.Xml.Serialization
Imports System.Xml

Public Class Logitracs

#Region "Declaración de Variables"
    Private archLog As StreamWriter
    Dim objLogitracsEvents As LgIntegrationSoap
    Dim thrReporte As Thread
    Dim intCodiEmpr As Byte
    Dim idCompany As String
    Dim strUsuaWebServ As String
    Dim strClavWebServ As String
    Dim strLogWebServ As String
    Dim strRegWebServ As String
    Dim strTraWebServ As String
    Dim intDiasConsDesp As Integer
    Dim intDiasConsNove As Integer
    Dim TERCCodigoProsegur As Integer
    Dim strCadenaDeConexionSQL As String
    Dim sqlConexion As SqlConnection
    Dim sqlComando As SqlCommand
    Dim strRutaArchLog As String
    Dim stwrLog As StreamWriter
    Dim intNumeMani As Integer
    Dim strPlacVehi As String
    Dim intCodiRuta As Integer
    Dim XML As String
    Dim intEstaRepo As Integer
    Dim strMensSatr As String
    Dim intTiemSuspServ As Integer
    Private strRutaLog As String
    Private strNombLog As String


#End Region

    Private urlToPost As String = ""
    Private urlToTrack As String = ""
    Private autToken As String = ""

#Region "Propiedades"

    Public Property ArchivoLog() As StreamWriter
        Get
            Return Me.archLog
        End Get
        Set(ByVal value As StreamWriter)
            Me.archLog = value
        End Set
    End Property
    Public Property logicAPI As LgIntegrationSoap
        Get
            Return Me.objLogitracsEvents
        End Get
        Set(ByVal value As LgIntegrationSoap)
            Me.objLogitracsEvents = value
        End Set
    End Property
    Public Property IdComp As String
        Get
            Return Me.idCompany
        End Get
        Set(ByVal value As String)
            Me.idCompany = value
        End Set
    End Property

    Public Property CodigoEmpresa As Integer
        Get
            Return Me.intCodiEmpr
        End Get
        Set(ByVal value As Integer)
            Me.intCodiEmpr = value
        End Set
    End Property

    Public Property UsuarioWebServices As String
        Get
            Return Me.strUsuaWebServ
        End Get
        Set(ByVal value As String)
            Me.strUsuaWebServ = value
        End Set
    End Property

    Public Property ClaveWebServices As String
        Get
            Return Me.strClavWebServ
        End Get
        Set(ByVal value As String)
            Me.strClavWebServ = value
        End Set
    End Property

    Public Property UrlLoginWebServices As String
        Get
            Return Me.strLogWebServ
        End Get
        Set(ByVal value As String)
            Me.strLogWebServ = value
        End Set
    End Property

    Public Property UrlRegistroWebServices As String
        Get
            Return Me.strRegWebServ
        End Get
        Set(ByVal value As String)
            Me.strRegWebServ = value
        End Set
    End Property

    Public Property UrlTrackWebServices As String
        Get
            Return Me.strTraWebServ
        End Get
        Set(ByVal value As String)
            Me.strTraWebServ = value
        End Set
    End Property

    Public Property CadenaConexionSQL As String
        Get
            Return Me.strCadenaDeConexionSQL
        End Get
        Set(ByVal value As String)
            Me.strCadenaDeConexionSQL = value
        End Set
    End Property

    Public Property ConexionSQL As SqlConnection
        Get
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ComandoSQL As SqlCommand
        Get
            Return Me.sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComando = value
        End Set
    End Property

    Public Property RutaLog() As String
        Get
            Return Me.strRutaLog
        End Get
        Set(ByVal value As String)
            Me.strRutaLog = value
        End Set
    End Property

    Public Property ArchiveLog() As String
        Get
            Return Me.strNombLog
        End Get
        Set(ByVal value As String)
            Me.strNombLog = value
        End Set
    End Property


#End Region

    Public Sub New()
        InitializeComponent()
        'Cargar_Valores_AppConfig()
        'Consultar_Seguimientos()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        'INICIO HILO DE CONSULTA DE SEGUIMIENTOS GPS VEHICULOS EN RUTA
        Me.thrReporte = New Thread(AddressOf Iniciar_Proceso_Reporte)
        Me.thrReporte.Start()
    End Sub

    Protected Overrides Sub OnStop()
        'SUSPENCION HILO DE CONSULTA DE SEGUIMIENTOS GPS VEHICULOS EN RUTA
        Me.thrReporte.Suspend()
    End Sub

    Private Sub Iniciar_Proceso_Reporte()
        Try
            While (True)
                'OBTENCION VALORES APP CONFIG
                Cargar_Valores_AppConfig()
                ' INICIO CONSULTA SEGUIMIENTOS
                Consultar_Seguimientos()
                Thread.Sleep(Val(AppSettings.Get("TiempoSuspension")))
            End While
        Catch ex As Exception
        End Try
    End Sub

    Function Cargar_Valores_AppConfig() As Boolean
        Try
            ' VALORES APP CONFIG FUNCIONAMIENTO SERVICIO Y CONSUMO WEB SERVICE
            Me.CodigoEmpresa = AppSettings.Get("Empresa")
            Me.IdComp = AppSettings.Get("idCompany")
            Me.UsuarioWebServices = AppSettings.Get("Usuario")
            Me.ClaveWebServices = AppSettings.Get("Clave")
            Me.CadenaConexionSQL = AppSettings.Get("ConexionSQLGestrans")
            Me.UrlLoginWebServices = AppSettings.Get("ClientUriLogin")
            Me.RutaLog = AppSettings.Get("RutaLog")
            Me.ArchiveLog = AppSettings.Get("NombreLog")

            Cargar_Valores_AppConfig = True
        Catch ex As Exception

            Cargar_Valores_AppConfig = False
        End Try
    End Function


    Public Sub Consultar_Seguimientos()
        ' DECLARACION DE VARIABLES
        Dim lista As List(Of GetLastPositionResponse) = New List(Of GetLastPositionResponse)
        Dim strSQL
        Dim dsDataSet As New DataSet
        Dim dicRetorno As New Dictionary(Of String, Object)
        Dim strPlaca As String = String.Empty
        Dim lonManifiesto As Long = 0
        Dim lonPlanilla As Long = 0
        Dim lonRuta As Long = 0
        Dim lonVehiculo As Long = 0
        Dim Position As LgIntegrationSoapClient = New LgIntegrationSoapClient
        ' OBJETO LOGITRACS
        Me.objLogitracsEvents = New LgIntegrationSoapClient
        ' EJECUCION SP OBTENCION VEHICULOS EN RUTA
        strSQL = "EXEC gsp_consultar_vehiculos_seguimiento_reporte_logitracs "
        strSQL += Me.CodigoEmpresa.ToString()

        Me.ConexionSQL = New SqlConnection(Me.CadenaConexionSQL)
        Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)

        ' CREACION DATA SET RESPUESTA EJECUCION SP
        dsDataSet = Me.Retorna_Dataset(strSQL)

        ' REGISTROS OBTENIDOS DE VEHICULOS EN RUTA
        If dsDataSet.Tables(0).Rows.Count > 0 Then
            ' RECORRIDO DE CADA UNO DE LOS VEHICULOS EN RUTA
            For Each Registro As DataRow In dsDataSet.Tables(0).Rows
                ' SET VARIABLES CONSUMO SERVICIO
                strPlaca = Registro.Item(2).ToString
                lonPlanilla = Registro.Item(3)
                lonManifiesto = Registro.Item(0)
                lonRuta = Registro.Item(1)
                lonVehiculo = Registro.Item(5)

                ' CREACION PETICION A LOGITRACS
                Dim RequestBody As GetLastPositionRequestBody = New GetLastPositionRequestBody(Me.IdComp, Me.UsuarioWebServices, Me.ClaveWebServices, strPlaca)
                Dim Request As GetLastPositionRequest = New GetLastPositionRequest(RequestBody)

                ' RESPUESTA LOGITRACS
                Dim xmlResponse As GetLastPositionResponse = Me.objLogitracsEvents.GetLastPosition(Request)

                ' INSERCION INFORMACION SEGUIMIENTO VEHICULAR GPS
                InsertarPositionLogitracs(xmlResponse, lonPlanilla, strPlaca, lonManifiesto, lonRuta, Me.CodigoEmpresa, lonVehiculo)
            Next
        End If
    End Sub


    Private Sub InsertarPositionLogitracs(dicRetorno As GetLastPositionResponse, Planilla As Long, strPlaca As String, Manifiesto As Long, Ruta As Long, Empresa As Integer, CodigoVehiculo As Long)
        Try
            ' Conversion GetLastPositionResponse a XML
            Dim element As XElement = XElement.Parse(dicRetorno.Body.GetLastPositionResult.ToString)
            ' Captura Valores XML Response Logitracs
            Dim motivo As String = CStr(element.Element("motivo"))
            Dim velocidad As String = CStr(element.Element("velocidad"))
            Dim tiempoGps As String = CStr(element.Element("tiempo_gps"))
            Dim posicion As String = CStr(element.Element("posicion"))
            Dim latitud As String = CStr(element.Element("latitud"))
            Dim longitud As String = CStr(element.Element("longitud"))
            Dim kilometraje As String = CStr(element.Element("kilometraje"))
            Dim kilometrajeDiario As String = CStr(element.Element("kilometrajediario"))

            Dim Evento As String = String.Empty
            Dim Placa As String = String.Empty

            ' INSERCION REGISTROS
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
                Me.ComandoSQL = New SqlCommand("gsp_insertar_seguimientos_vehiculo_gps_logitracs", Me.ConexionSQL)
                Me.ComandoSQL.CommandType = CommandType.StoredProcedure

                Me.ComandoSQL.Parameters.Add("@par_Empresa", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_Empresa").Value = Empresa
                Me.ComandoSQL.Parameters.Add("@par_Fecha", SqlDbType.DateTime) : Me.ComandoSQL.Parameters("@par_Fecha").Value = Convert.ToDateTime(tiempoGps)
                Me.ComandoSQL.Parameters.Add("@par_Placa", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_Placa").Value = strPlaca
                Me.ComandoSQL.Parameters.Add("@par_motivo", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_motivo").Value = motivo
                Me.ComandoSQL.Parameters.Add("@par_Latitud", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_Latitud").Value = latitud
                Me.ComandoSQL.Parameters.Add("@par_Longitud", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_Longitud").Value = longitud
                Me.ComandoSQL.Parameters.Add("@par_Velocidad", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_Velocidad").Value = velocidad
                Me.ComandoSQL.Parameters.Add("@par_posicion", SqlDbType.VarChar, 255) : Me.ComandoSQL.Parameters("@par_posicion").Value = posicion
                Me.ComandoSQL.Parameters.Add("@par_Planilla", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_Planilla").Value = Planilla

                If Me.ComandoSQL.ExecuteNonQuery = 1 Then
                    My.Computer.FileSystem.CreateDirectory(Me.RutaLog)
                    Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.strNombLog, True)
                    Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: Ubicacion del vehiculo: " & strPlaca & " , Evento: " & motivo)
                    Me.ArchivoLog.Close()
                Else
                    Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.strNombLog, True)
                    Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: La ubicacion del vehiculo: " & strPlaca & " , No se pudo guardar ")
                    Me.ArchivoLog.Close()
                End If
                Me.ConexionSQL.Close()
            End If
            'End If
        Catch ex As Exception
            Console.WriteLine(ex)
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Sub

    Private Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                ConexionSQL.Open()
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try

    End Function

End Class
