﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos

Namespace Despachos.Planilla
    <JsonObject>
    Public Class Planillas
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property FechaHoraSalida As DateTime
        <JsonProperty>
        Public Property FechaSalida As Date
        <JsonProperty>
        Public Property HoraSalida As String
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Semirremolque As Semirremolques
        <JsonProperty>
        Public Property Tenedor As Tercero
        <JsonProperty>
        Public Property Conductor As Tercero
        <JsonProperty>
        Public Overloads Property Estado As Estado
        <JsonProperty>
        Public Property Detalles As IEnumerable(Of DetallePlanillas)
        <JsonProperty>
        Public Property DetallesAuxiliares As IEnumerable(Of DetalleAuxiliaresPlanillas)
        <JsonProperty>
        Public Property TipoTarifaTransportes As TipoTarifaTransportes
        <JsonProperty>
        Public Property LineaNegocioTransporte As LineaNegocioTransportes
        <JsonProperty>
        Public Property TipoLineaNegocioTransportes As TipoLineaNegocioTransportes
        <JsonProperty>
        Public Property TarifarioCompras As Integer
        <JsonProperty>
        Public Property DetalleTarifarioCompra As DetalleTarifarioCompras
        <JsonProperty>
        Public Property TarifaTransportes As TarifaTransportes
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property PesoCumplido As Double
        <JsonProperty>
        Public Property ValorFleteTransportador As Double
        <JsonProperty>
        Public Property strValorFleteTransportador As String
        <JsonProperty>
        Public Property strValorFleteAutorizacion As String
        <JsonProperty>
        Public Property ValorAnticipo As Double
        <JsonProperty>
        Public Property ValorImpuestos As Double
        <JsonProperty>
        Public Property ValorPagarTransportador As Double
        <JsonProperty>
        Public Property ValorFleteCliente As Double
        <JsonProperty>
        Public Property ValorSeguroMercancia As Double
        <JsonProperty>
        Public Property ValorOtrosCobros As Double
        <JsonProperty>
        Public Property ValorTotalCredito As Double
        <JsonProperty>
        Public Property ValorTotalContado As Double
        <JsonProperty>
        Public Property ValorTotalAlcobro As Double
        <JsonProperty>
        Public Property ValorOtrosFletes As Double
        <JsonProperty>
        Public Property NumeroGuia As Integer
        <JsonProperty>
        Public Property strVehiculo As String
        <JsonProperty>
        Public Property strConductor As String
        <JsonProperty>
        Public Property strTenedor As String
        <JsonProperty>
        Public Property DetalleImpuesto As IEnumerable(Of DetalleImpuestosPlanillas)
        <JsonProperty>
        Public Property NumeroManifiesto As Integer
        <JsonProperty>
        Public Property AutorizacionAnticipo As Short
        <JsonProperty>
        Public Property ValorSeguroPoliza As Double
        <JsonProperty>
        Public Property AnticipoPagadoA As String
        <JsonProperty>
        Public Property FechaEntrega As Date
        <JsonProperty>
        Public Property PagoAnticipo As Integer
        <JsonProperty>
        Public Property RemesaMasivo As Integer
        <JsonProperty>
        Public Property RemesaPadre As Integer

        <JsonProperty>
        Public Property PesoCargue As Double

        <JsonProperty>
        Public Property UsuarioCumplido As String
        <JsonProperty>
        Public Property CiudadOrigen As String
        <JsonProperty>
        Public Property CiudadDestino As String
        <JsonProperty>
        Public Property FechaCumplido As Date

        <JsonProperty>
        Public Property FleteUnitario As Double
        <JsonProperty>
        Public Property TipoDespacho As String
        <JsonProperty>
        Public Property AutorizacionFlete As Short
        <JsonProperty>
        Public Property ValorFleteAutorizacion As Double
        <JsonProperty>
        Public Property NumeroCumplido As Integer
        <JsonProperty>
        Public Property NumeroLiquidacion As Integer
        <JsonProperty>
        Public Property NumeroLegalizacionGastos As Integer
        <JsonProperty>
        Public Property NumeroLegalizarRecaudo As Integer
        <JsonProperty>
        Public Property ValorContraEntrega As Double
        <JsonProperty>
        Public Property Conductores As IEnumerable(Of Terceros)

        <JsonProperty>
        Public Property ValorBruto As Double
        <JsonProperty>
        Public Property Porcentaje As Double
        <JsonProperty>
        Public Property ValorDescuentos As Double
        <JsonProperty>
        Public Property FleteSugerido As Double
        <JsonProperty>
        Public Property ValorFondoAyuda As Double

        <JsonProperty>
        Public Property ValorNetoPagar As Double
        <JsonProperty>
        Public Property ValorUtilidad As Double
        <JsonProperty>
        Public Property ValorEstampilla As Double
        <JsonProperty>
        Public Property GastosAgencia As Double
        <JsonProperty>
        Public Property ValorReexpedicion As Double
        <JsonProperty>
        Public Property ValorPlanillaAdicional As Double
        <JsonProperty>
        Public Property CalcularContraentregas As Double

    End Class
End Namespace
