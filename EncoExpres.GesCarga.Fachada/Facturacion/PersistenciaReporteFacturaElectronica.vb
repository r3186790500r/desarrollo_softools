﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Repositorio.Facturacion

Namespace Facturacion
    Public NotInheritable Class PersistenciaReporteFacturaElectronica
        Implements IPersistenciaBase(Of ReporteFacturaElectronica)

        Public Function Consultar(filtro As ReporteFacturaElectronica) As IEnumerable(Of ReporteFacturaElectronica) Implements IPersistenciaBase(Of ReporteFacturaElectronica).Consultar
            Return New RepositorioReporteFacturaElectronica().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ReporteFacturaElectronica) As Long Implements IPersistenciaBase(Of ReporteFacturaElectronica).Insertar
            Return New RepositorioReporteFacturaElectronica().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ReporteFacturaElectronica) As Long Implements IPersistenciaBase(Of ReporteFacturaElectronica).Modificar
            Return New RepositorioReporteFacturaElectronica().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ReporteFacturaElectronica) As ReporteFacturaElectronica Implements IPersistenciaBase(Of ReporteFacturaElectronica).Obtener
            Return New RepositorioReporteFacturaElectronica().Obtener(filtro)
        End Function

        Public Function ReportarDocumentosElectronicos(filtro As ReporteFacturaElectronica) As ReporteFacturaElectronica
            Return New RepositorioReporteFacturaElectronica().ReportarDocumentosElectronicos(filtro)
        End Function

        Public Function ReportarDocumentosSaphetyServicioWindows(filtro As ReporteFacturaElectronica) As ReporteFacturaElectronica
            Return New RepositorioReporteFacturaElectronica().ReportarDocumentosSaphetyServicioWindows(filtro)
        End Function
    End Class
End Namespace
