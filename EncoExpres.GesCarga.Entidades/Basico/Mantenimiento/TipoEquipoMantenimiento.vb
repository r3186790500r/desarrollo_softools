﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref="TipoEquipoMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TipoEquipoMantenimiento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoEquipoMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoEquipoMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = New ValorCatalogos With {.Codigo = Read(lector, "CodigoEstado"), .Nombre = Read(lector, "Estado")}
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Try
                TotalRegistros = Read(lector, "TotalRegistros")
            Catch
                TotalRegistros = 0
            End Try
        End Sub

        <JsonProperty>
        Public Property CodigoAlterno As String
        <JsonProperty>
        Public Property Nombre As String


        <JsonProperty>
        Public Property Estado As ValorCatalogos
    End Class
End Namespace
