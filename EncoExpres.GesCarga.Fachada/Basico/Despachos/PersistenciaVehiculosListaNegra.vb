﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaVehiculosListaNegra
        Implements IPersistenciaBase(Of Vehiculos)


        Public Function Consultar(filtro As Vehiculos) As IEnumerable(Of Vehiculos) Implements IPersistenciaBase(Of Vehiculos).Consultar
            Throw New NotImplementedException()
        End Function

        Public Function Insertar(entidad As Vehiculos) As Long Implements IPersistenciaBase(Of Vehiculos).Insertar
            Return New RepositorioVehiculosListaNegra().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Vehiculos) As Long Implements IPersistenciaBase(Of Vehiculos).Modificar
            Return New RepositorioVehiculosListaNegra().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Vehiculos) As Vehiculos Implements IPersistenciaBase(Of Vehiculos).Obtener
            Throw New NotImplementedException()
        End Function

        Public Function Anular(entidad As Vehiculos) As Boolean
            Return New RepositorioVehiculosListaNegra().Anular(entidad)
        End Function

    End Class

End Namespace
