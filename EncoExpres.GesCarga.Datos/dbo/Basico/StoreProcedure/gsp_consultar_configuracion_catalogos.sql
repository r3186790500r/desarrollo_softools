﻿PRINT 'gsp_consultar_configuracion_catalogos'
GO
DROP PROCEDURE gsp_consultar_configuracion_catalogos
GO
CREATE PROCEDURE gsp_consultar_configuracion_catalogos
(
@par_EMPR_Codigo SMALLINT,
@par_CATA_Codigo SMALLINT = NULL
)
AS
BEGIN
	SELECT
		EMPR_Codigo,
		CATA_Codigo,
		Secuencia,
		Nombre,
		Obligatorio,
		Parte_Llave,
		Tipo,
		Longitud_Campo,
		Numero_Decimales,
		Utilizar_Combo,
		Codigo_Catalogo_Combo,
		Campo_Codigo_Combo,
		Campo_Nombre_Combo,
		Visible,
		Sugerir_Codigo
	FROM
		configuracion_catalogos
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND CATA_Codigo = ISNULL(@par_CATA_Codigo, CATA_Codigo)

END
GO