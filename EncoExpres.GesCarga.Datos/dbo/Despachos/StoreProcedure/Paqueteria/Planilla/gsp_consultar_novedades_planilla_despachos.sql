﻿
Print 'CREATE PROCEDURE gsp_consultar_novedades_planilla_despachos'
GO
DROP PROCEDURE gsp_consultar_novedades_planilla_despachos
GO

CREATE PROCEDURE gsp_consultar_novedades_planilla_despachos     
(            
	@par_EMPR_Codigo SMALLINT,       
	@par_ENPD_Numero NUMERIC = NULL,
	@par_ENRE_Numero NUMERIC = NULL
)            
AS            
BEGIN                
	SELECT DISTINCT           
	DEND.EMPR_Codigo,            
	DEND.ID,            
	DEND.ENPD_Numero,      
	ENPD.Numero_Documento AS ENPD_Numero_Documento,    
	ISNULL(DEND.ENRE_Numero, 0) AS ENRE_Numero,    
	ISNULL(ENRE.Numero_Documento, 0) AS ENRE_Numero_Documento,
	ISNULL(COVE.Codigo, 0) AS COVE_Codigo,
	ISNULL(COVE.Nombre, '') AS COVE_Nombre,
	DEND.NODE_Codigo,    
	ISNULL(NDES.Nombre, '') As Nombre_Novedad,      
	DEND.Observaciones,       
	ISNULL(DEND.Valor_Compra, 0) AS Valor_Compra,    
	ISNULL(DEND.Valor_Venta, 0) AS Valor_Venta,     
	ISNULL(DEND.TERC_Codigo_Proveedor, 0) AS TERC_Codigo_Proveedor,     
	ISNULL(PROV.Razon_Social,'') + ' ' + ISNULL(PROV.Nombre,'') + ' ' + ISNULL(PROV.Apellido1,'') + ' ' + ISNULL(PROV.Apellido2,'') As Nombre_Proveedor,     
	ISNULL(DEND.USUA_Codigo_Crea, 0) AS USUA_Codigo_Crea,     
	ISNULL(USUA.Codigo_Usuario,'') As Nombre_Usuario_Crea,    
	ISNULL(DEND.Fecha_Crea,'') As Fecha_Crea,    
	DEND.Anulado,  
	ISNULL(CLPD.Codigo,0) as CLPD_Codigo,  
	ISNULL( CLPD.Valor_Fijo  , 0) as Valor_Fijo,  
	ISNULL( CLPD.Operacion  , 0) as Operacion  
	
	FROM            
	Detalle_Novedades_Despacho as DEND      
	  
	LEFT JOIN Encabezado_Planilla_Despachos AS ENPD      
	ON DEND.EMPR_Codigo = ENPD.EMPR_Codigo      
	AND DEND.ENPD_Numero = ENPD.Numero      

	LEFT JOIN Encabezado_Remesas AS ENRE      
	ON DEND.EMPR_Codigo = ENRE.EMPR_Codigo      
	AND DEND.ENRE_Numero = ENRE.Numero      

	LEFT JOIN Novedades_Despacho AS NDES      
	ON DEND.EMPR_Codigo = NDES.EMPR_Codigo      
	AND DEND.NODE_Codigo = NDES.Codigo      

	LEFT JOIN Novedades_Conceptos AS NOCO     
	ON NDES.EMPR_Codigo = NOCO.EMPR_Codigo      
	AND NDES.Codigo = NOCO.NODE_Codigo 
	
	LEFT JOIN Conceptos_Ventas AS COVE     
	ON NOCO.EMPR_Codigo = COVE.EMPR_Codigo      
	AND NOCO.COVE_Codigo = COVE.Codigo

	LEFT JOIN Conceptos_Liquidacion_Planilla_Despachos AS CLPD     
	ON NOCO.EMPR_Codigo = CLPD.EMPR_Codigo      
	AND NOCO.CLPD_Codigo = CLPD.Codigo  
	   

	LEFT JOIN Terceros AS PROV      
	ON DEND.EMPR_Codigo = PROV.EMPR_Codigo      
	AND DEND.TERC_Codigo_Proveedor = PROV.Codigo      

	LEFT JOIN Usuarios AS USUA      
	ON DEND.EMPR_Codigo = USUA.EMPR_Codigo      
	AND DEND.USUA_Codigo_Crea = USUA.Codigo      
	  
	WHERE            
	DEND.EMPR_Codigo = @par_EMPR_Codigo            
	AND DEND.ENPD_Numero = ISNULL(@par_ENPD_Numero, DEND.ENPD_Numero) 
	AND DEND.ENRE_Numero = ISNULL(@par_ENRE_Numero, DEND.ENRE_Numero)
END      
GO