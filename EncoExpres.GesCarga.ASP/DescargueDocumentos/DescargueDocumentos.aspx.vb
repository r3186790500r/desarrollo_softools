﻿Imports System.Data.SqlClient



Public Class DescargueDocumentos
    Inherits System.Web.UI.Page
    '' Dim objEmpresa As clsEmpresa
    Dim objGeneral As clsGeneral

    Public Sub New()
        ''  Me.objEmpresa = New clsEmpresa(0)

        objGeneral = New clsGeneral
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '' Me.objEmpresa.Codigo = Val(Session("CODIGO_EMPRESA"))
        If Not IsNothing(Me.Request.QueryString("NombDoc")) Then


            If Me.Request.QueryString("NombDoc") = "AutorizacionValorDeclarado" Then

                Call Descargar_Autorizacion_Valor_Declarado()

            End If


            If Me.Request.QueryString("NombDoc") = "Vehiculos" Then

                Call Descargar_Documento_Vehiculo()

            End If

            If Me.Request.QueryString("NombDoc") = "Terceros" Then

                Call Descargar_Documento_Tercero()

            End If

            If Me.Request.QueryString("NombDoc") = "Semirremolques" Then

                Call Descargar_Documento_Semirremolque()

            End If

            If Me.Request.QueryString("NombDoc") = "Estudio_Seguridad" Then

                Call Descargar_Documento_Estudio_Seguridad()

            End If

            If Me.Request.QueryString("NombDoc") = "Orden_Servicio" Then

                Call Descargar_Documento_Orden_Servicio()

            End If

            If Me.Request.QueryString("NombDoc") = "Cumplido" Then
                Call Descargar_Documento_Cumplido_Despacho()
            End If

            If Me.Request.QueryString("NombDoc") = "EquipoMantenimiento" Then

                Call Descargar_Documento_Equipo_Mantenimiento()

            End If

            If Me.Request.QueryString("NombDoc") = "CumplidoRemesa" Then
                Call Descargar_Documento_Cumplido_Remesa()
            End If

            If Me.Request.QueryString("NombDoc") = "Remesa" Then
                Call Descargar_Documento_Remesa()
            End If

        End If
    End Sub

    Public Sub Descargar_Documento_Orden_Servicio()
        If Request.QueryString("CDGD_Codigo") IsNot Nothing Then
            If Request.QueryString("Numero") IsNot Nothing Then
                If Request.QueryString("EMPR_Codigo") IsNot Nothing Then
                    Dim TipoImagen As String = ""
                    Dim CDGDCodigo As Integer = Val(Request.QueryString("CDGD_Codigo"))
                    Dim Numero As Integer = Val(Request.QueryString("Numero"))
                    Dim CodigoEmpresa As Integer = Val(Request.QueryString("EMPR_Codigo"))
                    Dim temp As Integer = Val(Request.QueryString("Temp"))
                    Dim sdrImagenes As SqlDataReader
                    Dim strSQL As String
                    Me.objGeneral.ComandoSQL = New SqlCommand
                    Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
                    Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.strCadenaDeConexionSQLDocumentos)
                    If temp = 0 Then
                        strSQL = "SELECT Archivo,Extension_Documento,TIDO_Codigo,Nombre_Documento" _
                                       & " FROM Gestion_Documental_Documentos WHERE CDGD_Codigo = " & CDGDCodigo & " and EMPR_Codigo = " & CodigoEmpresa & " and Numero = " & Numero
                    Else
                        strSQL = "SELECT Archivo,Extension_Documento,TIDO_Codigo,Nombre_Documento" _
                                       & " FROM T_Gestion_Documental_Documentos WHERE CDGD_Codigo = " & CDGDCodigo & " and EMPR_Codigo = " & CodigoEmpresa & " and Numero = " & Numero
                    End If

                    Me.objGeneral.ComandoSQL.CommandText = strSQL
                    'Me.objGeneral.ComandoSQL.Parameters.Add("@NumeroRemesa", SqlDbType.Int).Value = NumeroRemesa
                    'Me.objGeneral.ComandoSQL.Parameters.Add("@ID", SqlDbType.Int).Value = Val(Request.QueryString("ID"))
                    'Dim RepContrato As New SqlDataAdapter(strSQL, objGeneral.ConexionSQL)

                    If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
                        Me.objGeneral.ComandoSQL.Connection.Open()
                    End If

                    sdrImagenes = Me.objGeneral.ComandoSQL.ExecuteReader

                    If sdrImagenes.Read Then

                        Dim bytes() As Byte = CType(sdrImagenes("Archivo"), Byte())
                        Response.Buffer = True
                        Response.Charset = ""
                        Response.Cache.SetCacheability(HttpCacheability.NoCache)

                        Response.BufferOutput = True
                        Response.ContentType = sdrImagenes("TIDO_Codigo").ToString
                        Response.AddHeader("content-disposition", "attachment;filename=" _
                            & sdrImagenes("Nombre_Documento").ToString & "." & sdrImagenes("Extension_Documento").ToString)
                        Response.BinaryWrite(bytes)
                        Response.Flush()
                        Response.End()

                    End If
                End If
            End If
        End If
    End Sub
    Public Sub Descargar_Documento_Estudio_Seguridad()
        If Request.QueryString("Codigo") IsNot Nothing Then
            If Request.QueryString("EMPR_Codigo") IsNot Nothing Then
                Dim TipoImagen As String = ""
                Dim Codigo As Integer = Val(Request.QueryString("Codigo"))
                Dim CodigoEmpresa As Integer = Val(Request.QueryString("EMPR_Codigo"))
                Dim temp As Integer = Val(Request.QueryString("Temp"))
                Dim Usuariotemp As Integer = Val(Request.QueryString("Usuariotemp"))
                Dim ENES_Numero As Integer = Val(Request.QueryString("ENES_Numero"))
                Dim sdrImagenes As SqlDataReader
                Dim strSQL As String
                Me.objGeneral.ComandoSQL = New SqlCommand
                Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
                Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.strCadenaDeConexionSQLDocumentos)
                If temp = 0 Then
                    strSQL = "SELECT Documento,Extension_Documento,Nombre_Documento" _
                                       & " FROM Estudio_Seguridad_Documentos WHERE EMPR_Codigo  = " & CodigoEmpresa & " AND TDES_Codigo = " & Codigo & " AND ENES_Numero = " & ENES_Numero
                Else
                    strSQL = "SELECT Documento,Extension_Documento,Nombre_Documento" _
                                       & " FROM T_Estudio_Seguridad_Documentos WHERE EMPR_Codigo  = " & CodigoEmpresa & " AND TDES_Codigo = " & Codigo & " AND USUA_Codigo = " & Usuariotemp
                End If

                Me.objGeneral.ComandoSQL.CommandText = strSQL
                'Me.objGeneral.ComandoSQL.Parameters.Add("@NumeroRemesa", SqlDbType.Int).Value = NumeroRemesa
                'Me.objGeneral.ComandoSQL.Parameters.Add("@ID", SqlDbType.Int).Value = Val(Request.QueryString("ID"))
                'Dim RepContrato As New SqlDataAdapter(strSQL, objGeneral.ConexionSQL)

                If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
                    Me.objGeneral.ComandoSQL.Connection.Open()
                End If

                sdrImagenes = Me.objGeneral.ComandoSQL.ExecuteReader

                If sdrImagenes.Read Then

                    Dim bytes() As Byte = CType(sdrImagenes("Documento"), Byte())
                    Response.Buffer = True
                    Response.Charset = ""
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)

                    Response.BufferOutput = True
                    'Response.ContentType = sdrImagenes("TIDD_Codigo").ToString
                    Response.AddHeader("content-disposition", "attachment;filename=" _
                            & sdrImagenes("Nombre_Documento").ToString & "." & sdrImagenes("Extension_Documento").ToString)
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()

                End If
            End If
        End If
    End Sub
    Public Sub Descargar_Documento_Vehiculo()
        If Request.QueryString("Codigo") IsNot Nothing Then
            If Request.QueryString("CDGD_Codigo") IsNot Nothing Then
                Dim TipoImagen As String = ""
                Dim Codigo As Integer = Val(Request.QueryString("Codigo"))
                Dim TipoDocumento As Integer = Val(Request.QueryString("CDGD_Codigo"))
                Dim temp As Integer = Val(Request.QueryString("Temp"))
                Dim sdrImagenes As SqlDataReader
                Dim strSQL As String
                Me.objGeneral.ComandoSQL = New SqlCommand
                Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
                Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.strCadenaDeConexionSQLDocumentos)
                If temp = 0 Then
                    strSQL = "SELECT Archivo,Extension,Tipo,Nombre_Documento" _
                                       & " FROM Vehiculo_Documentos WHERE VEHI_Codigo = " & Codigo & " and CDGD_Codigo = " & TipoDocumento
                Else
                    strSQL = "SELECT Archivo,Extension_Documento as Extension,Tipo,Nombre_Documento" _
                                       & " FROM T_Archivo WHERE USUA_Codigo_Crea = " & Codigo & " and CDGD_Codigo = " & TipoDocumento
                End If


                Me.objGeneral.ComandoSQL.CommandText = strSQL
                'Me.objGeneral.ComandoSQL.Parameters.Add("@NumeroRemesa", SqlDbType.Int).Value = NumeroRemesa
                'Me.objGeneral.ComandoSQL.Parameters.Add("@ID", SqlDbType.Int).Value = Val(Request.QueryString("ID"))
                'Dim RepContrato As New SqlDataAdapter(strSQL, objGeneral.ConexionSQL)

                If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
                    Me.objGeneral.ComandoSQL.Connection.Open()
                End If

                sdrImagenes = Me.objGeneral.ComandoSQL.ExecuteReader

                If sdrImagenes.Read Then

                    Dim bytes() As Byte = CType(sdrImagenes("Archivo"), Byte())
                    Response.Buffer = True
                    Response.Charset = ""
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)


                    Response.BufferOutput = True
                    Response.ContentType = sdrImagenes("Tipo").ToString
                    Response.AddHeader("content-disposition", "attachment;filename=" _
                            & sdrImagenes("Nombre_Documento").ToString & "." & sdrImagenes("Extension").ToString)
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()

                End If
            End If
        End If
    End Sub
    Public Sub Descargar_Documento_Tercero()
        If Request.QueryString("Codigo") IsNot Nothing Then
            If Request.QueryString("CDGD_Codigo") IsNot Nothing Then
                Dim TipoImagen As String = ""
                Dim Codigo As Integer = Val(Request.QueryString("Codigo"))
                Dim TipoDocumento As Integer = Val(Request.QueryString("CDGD_Codigo"))
                Dim temp As Integer = Val(Request.QueryString("Temp"))
                Dim sdrImagenes As SqlDataReader
                Dim strSQL As String
                Me.objGeneral.ComandoSQL = New SqlCommand
                Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
                Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.strCadenaDeConexionSQLDocumentos)
                If temp = 0 Then
                    strSQL = "SELECT Archivo,Extension,Tipo,Nombre_Documento" _
                                       & " FROM Tercero_Documentos WHERE TERC_Codigo = " & Codigo & " and CDGD_Codigo = " & TipoDocumento
                Else
                    strSQL = "SELECT Archivo,Extension_Documento as Extension,Tipo,Nombre_Documento" _
                                       & " FROM T_Archivo WHERE USUA_Codigo_Crea = " & Codigo & " and CDGD_Codigo = " & TipoDocumento
                End If


                Me.objGeneral.ComandoSQL.CommandText = strSQL
                'Me.objGeneral.ComandoSQL.Parameters.Add("@NumeroRemesa", SqlDbType.Int).Value = NumeroRemesa
                'Me.objGeneral.ComandoSQL.Parameters.Add("@ID", SqlDbType.Int).Value = Val(Request.QueryString("ID"))
                'Dim RepContrato As New SqlDataAdapter(strSQL, objGeneral.ConexionSQL)

                If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
                    Me.objGeneral.ComandoSQL.Connection.Open()
                End If

                sdrImagenes = Me.objGeneral.ComandoSQL.ExecuteReader

                If sdrImagenes.Read Then

                    Dim bytes() As Byte = CType(sdrImagenes("Archivo"), Byte())
                    Response.Buffer = True
                    Response.Charset = ""
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)


                    Response.BufferOutput = True
                    Response.ContentType = sdrImagenes("Tipo").ToString
                    Response.AddHeader("content-disposition", "attachment;filename=" _
                            & sdrImagenes("Nombre_Documento").ToString & "." & sdrImagenes("Extension").ToString)
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()

                End If
            End If
        End If
    End Sub
    Public Sub Descargar_Documento_Semirremolque()
        If Request.QueryString("Codigo") IsNot Nothing Then
            If Request.QueryString("CDGD_Codigo") IsNot Nothing Then
                Dim TipoImagen As String = ""
                Dim Codigo As Integer = Val(Request.QueryString("Codigo"))
                Dim TipoDocumento As Integer = Val(Request.QueryString("CDGD_Codigo"))
                Dim temp As Integer = Val(Request.QueryString("Temp"))
                Dim sdrImagenes As SqlDataReader
                Dim strSQL As String
                Me.objGeneral.ComandoSQL = New SqlCommand
                Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
                Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.strCadenaDeConexionSQLDocumentos)
                If temp = 0 Then
                    strSQL = "SELECT Archivo,Extension,Tipo,Nombre_Documento" _
                                       & " FROM Semirremolque_Documentos WHERE SEMI_Codigo = " & Codigo & " and CDGD_Codigo = " & TipoDocumento
                Else
                    strSQL = "SELECT Archivo,Extension_Documento as Extension,Tipo,Nombre_Documento" _
                                       & " FROM T_Archivo WHERE USUA_Codigo_Crea = " & Codigo & " and CDGD_Codigo = " & TipoDocumento
                End If


                Me.objGeneral.ComandoSQL.CommandText = strSQL
                'Me.objGeneral.ComandoSQL.Parameters.Add("@NumeroRemesa", SqlDbType.Int).Value = NumeroRemesa
                'Me.objGeneral.ComandoSQL.Parameters.Add("@ID", SqlDbType.Int).Value = Val(Request.QueryString("ID"))
                'Dim RepContrato As New SqlDataAdapter(strSQL, objGeneral.ConexionSQL)

                If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
                    Me.objGeneral.ComandoSQL.Connection.Open()
                End If

                sdrImagenes = Me.objGeneral.ComandoSQL.ExecuteReader

                If sdrImagenes.Read Then

                    Dim bytes() As Byte = CType(sdrImagenes("Archivo"), Byte())
                    Response.Buffer = True
                    Response.Charset = ""
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)


                    Response.BufferOutput = True
                    Response.ContentType = sdrImagenes("Tipo").ToString
                    Response.AddHeader("content-disposition", "attachment;filename=" _
                            & sdrImagenes("Nombre_Documento").ToString & "." & sdrImagenes("Extension").ToString)
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()

                End If
            End If
        End If
    End Sub

    Public Sub Descargar_Documento_Cumplido_Despacho()

        If Request.QueryString("ID") IsNot Nothing Then
            If Request.QueryString("ENCU_Numero") IsNot Nothing Then
                Dim TipoImagen As String = ""

                Dim Codigo As Integer = Val(Request.QueryString("ID"))
                Dim CodigoUsuario As Integer = Val(Request.QueryString("USUA_Codigo"))
                Dim CodigoEmpresa As Short = Val(Request.QueryString("Empresa"))

                Dim NumeroCumplido As Integer = Val(Request.QueryString("ENCU_Numero"))
                Dim temp As Integer = Val(Request.QueryString("Temp"))
                Dim sdrImagenes As SqlDataReader
                Dim strSQL As String
                Me.objGeneral.ComandoSQL = New SqlCommand
                Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
                Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.strCadenaDeConexionSQLDocumentos)

                If temp = 0 Then
                    strSQL = "SELECT Archivo,ExtensionArchivo,Tipo,NombreArchivo" _
                                       & " FROM Documento_Cumplido_Despachos WHERE EMPR_Codigo = " & CodigoEmpresa & " AND ID = " & Codigo & " and ENCU_Numero = " & NumeroCumplido
                Else
                    strSQL = "SELECT Archivo,ExtensionArchivo,Tipo,NombreArchivo" _
                                       & " FROM T_Documento_Cumplido_Despachos WHERE EMPR_Codigo = " & CodigoEmpresa & " AND ID = " & Codigo & " and ENCU_Numero = " & NumeroCumplido & " and USUA_Codigo = " & CodigoUsuario
                End If


                Me.objGeneral.ComandoSQL.CommandText = strSQL

                If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
                    Me.objGeneral.ComandoSQL.Connection.Open()
                End If

                sdrImagenes = Me.objGeneral.ComandoSQL.ExecuteReader

                If sdrImagenes.Read Then

                    Dim bytes() As Byte = CType(sdrImagenes("Archivo"), Byte())
                    Response.Buffer = True
                    Response.Charset = ""
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)


                    Response.BufferOutput = True
                    Response.ContentType = sdrImagenes("Tipo").ToString
                    Response.AddHeader("content-disposition", "attachment;filename=" _
                            & sdrImagenes("NombreArchivo").ToString & "." & sdrImagenes("ExtensionArchivo").ToString)
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()

                End If
            End If
        End If
    End Sub

    Public Sub Descargar_Documento_Equipo_Mantenimiento()
        If Request.QueryString("Codigo") IsNot Nothing Then
            Dim Referencia As String
            If Request.QueryString("Nombrefile") IsNot Nothing And Request.QueryString("Nombrefile") <> String.Empty Then
                Referencia = Request.QueryString("Nombrefile") & ".pdf"
            Else
                Referencia = "File.pdf"
            End If
            Dim Codigo As Integer = Val(Request.QueryString("Codigo"))
            Dim sdrImagenes As SqlDataReader
            Dim strSQL As String
            Me.objGeneral.ComandoSQL = New SqlCommand
            Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
            Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.strCadenaDeConexionSQLDocumentos)
            strSQL = "SELECT Documento" _
                               & " FROM Equipo_Mantenimiento_Documentos WHERE ID = " & Codigo
            Me.objGeneral.ComandoSQL.CommandText = strSQL

            If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
                Me.objGeneral.ComandoSQL.Connection.Open()
            End If

            sdrImagenes = Me.objGeneral.ComandoSQL.ExecuteReader

            If sdrImagenes.Read Then

                Dim bytes() As Byte = CType(sdrImagenes("Documento"), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)


                Response.BufferOutput = True
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-disposition", "attachment;filename=" _
                 & Referencia)
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

            End If
        End If
    End Sub

    Public Sub Descargar_Documento_Cumplido_Remesa()

        If Request.QueryString("ID") IsNot Nothing Then
            If Request.QueryString("ENRE_Numero") IsNot Nothing Then
                Dim TipoImagen As String = ""

                Dim Codigo As Integer = Val(Request.QueryString("ID"))
                Dim CodigoEmpresa As Short = Val(Request.QueryString("Empresa"))

                Dim NumeroRemesa As Integer = Val(Request.QueryString("ENRE_Numero"))
                Dim temp As Integer = Val(Request.QueryString("Temp"))
                Dim sdrImagenes As SqlDataReader
                Dim strSQL As String
                Me.objGeneral.ComandoSQL = New SqlCommand
                Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
                Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.strCadenaDeConexionSQLDocumentos)

                If temp = 0 Then
                    strSQL = "SELECT Archivo,ExtensionArchivo,Tipo,NombreArchivo" _
                                       & " FROM Documentos_Cumplido_Remesa WHERE EMPR_Codigo = " & CodigoEmpresa & " AND Codigo = " & Codigo & " and ENRE_Numero = " & NumeroRemesa
                Else
                    strSQL = "SELECT Archivo,ExtensionArchivo,Tipo,NombreArchivo" _
                                       & " FROM T_Documentos_Cumplido_Remesa WHERE EMPR_Codigo = " & CodigoEmpresa & " AND Codigo = " & Codigo & " and ENRE_Numero = " & NumeroRemesa
                End If


                Me.objGeneral.ComandoSQL.CommandText = strSQL

                If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
                    Me.objGeneral.ComandoSQL.Connection.Open()
                End If

                sdrImagenes = Me.objGeneral.ComandoSQL.ExecuteReader

                If sdrImagenes.Read Then

                    Dim bytes() As Byte = CType(sdrImagenes("Archivo"), Byte())
                    Response.Buffer = True
                    Response.Charset = ""
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)


                    Response.BufferOutput = True
                    Response.ContentType = sdrImagenes("Tipo").ToString
                    Dim displayAction As String = "attachment"
                    If sdrImagenes("ExtensionArchivo").ToString.Equals("pdf") Then
                        displayAction = "inline"
                    End If
                    Response.AddHeader("content-disposition", displayAction & ";filename=" _
                            & sdrImagenes("NombreArchivo").ToString & "." & sdrImagenes("ExtensionArchivo").ToString)
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Public Sub Descargar_Documento_Remesa()

        If Request.QueryString("ID") IsNot Nothing Then
            Dim TipoImagen As String = ""

            Dim Codigo As Integer = Val(Request.QueryString("ID"))
            Dim CodigoEmpresa As Short = Val(Request.QueryString("Empresa"))
            Dim NumeroRemesa As Integer = Val(Request.QueryString("ENRE_Numero"))
            Dim CodigoTipoDocumento As Short = Val(Request.QueryString("TIDO_Codigo"))
            Dim temp As Integer = Val(Request.QueryString("Temp"))
            Dim sdrImagenes As SqlDataReader
            Dim strSQL As String
            Me.objGeneral.ComandoSQL = New SqlCommand
            Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
            Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.strCadenaDeConexionSQLDocumentos)

            If temp = 0 Then
                strSQL = "SELECT Archivo,ExtensionArchivo,Tipo,NombreArchivo" _
                                   & " FROM Documentos_Remesa WHERE EMPR_Codigo = " & CodigoEmpresa & " AND Codigo = " & Codigo & " and ENRE_Numero = " & NumeroRemesa _
                                   & " AND TIDO_Codigo = " & CodigoTipoDocumento
            Else
                strSQL = "SELECT Archivo,ExtensionArchivo,Tipo,NombreArchivo" _
                                   & " FROM T_Documentos_Remesa WHERE EMPR_Codigo = " & CodigoEmpresa & " AND Codigo = " & Codigo & " AND TIDO_Codigo = " & CodigoTipoDocumento
            End If


            Me.objGeneral.ComandoSQL.CommandText = strSQL

            If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
                Me.objGeneral.ComandoSQL.Connection.Open()
            End If

            sdrImagenes = Me.objGeneral.ComandoSQL.ExecuteReader

            If sdrImagenes.Read Then

                Dim bytes() As Byte = CType(sdrImagenes("Archivo"), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)


                Response.BufferOutput = True
                Response.ContentType = sdrImagenes("Tipo").ToString
                Response.AddHeader("content-disposition", "attachment;filename=" _
                        & sdrImagenes("NombreArchivo").ToString & "." & sdrImagenes("ExtensionArchivo").ToString)
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

            End If
        End If
    End Sub

    Public Sub Descargar_Autorizacion_Valor_Declarado()

        If Request.QueryString("ID") IsNot Nothing Then

            Dim TipoImagen As String = ""

            Dim Codigo As Integer = Val(Request.QueryString("ID"))
            Dim CodigoEmpresa As Short = Val(Request.QueryString("Empresa"))


            Dim sdrImagenes As SqlDataReader
            Dim strSQL As String
            Me.objGeneral.ComandoSQL = New SqlCommand
            Me.objGeneral.ComandoSQL.CommandType = CommandType.Text
            Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.strCadenaDeConexionSQLDocumentos)


            strSQL = "SELECT Archivo,Extension,Tipo,Nombre" _
                                    & " FROM Documentos_Autorizacion_Valor_Declarado WHERE EMPR_Codigo = " & CodigoEmpresa & " AND ENAU_Codigo = " & Codigo



            Me.objGeneral.ComandoSQL.CommandText = strSQL

            If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
                Me.objGeneral.ComandoSQL.Connection.Open()
            End If

            sdrImagenes = Me.objGeneral.ComandoSQL.ExecuteReader

            If sdrImagenes.Read Then

                Dim bytes() As Byte = CType(sdrImagenes("Archivo"), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)


                Response.BufferOutput = True
                Response.ContentType = sdrImagenes("Tipo").ToString
                Response.AddHeader("content-disposition", "attachment;filename=" _
                        & sdrImagenes("Nombre").ToString & "." & sdrImagenes("Extension").ToString)
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

            End If

        End If
    End Sub

End Class