﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref=" DetalleUbicacionAlmacenes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleUbicacionAlmacenes
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleUbicacionAlmacenes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleUbicacionAlmacenes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "EURA_Codigo")
            Almacen = New Almacenes With {.Codigo = Read(lector, "ALMA_Codigo")}
            Fila = Read(lector, "Fila")
            Columna = Read(lector, "Columna")
            Nivel = Read(lector, "Nivel")
            CodigoTipoUbicacion = Read(lector, "CATA_TIUA_Codigo")

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Almacen As Almacenes

        ''' <summary>
        ''' Obtiene o establece el contacto del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Fila As Integer

        ''' <summary>
        ''' Obtiene o establece el codigo alterno
        ''' </summary>
        <JsonProperty>
        Public Property Columna As Integer
        ''' <summary>
        ''' Obtiene o establece el codigo alterno
        ''' </summary>
        <JsonProperty>
        Public Property Nivel As Integer

        ''' <summary>
        ''' Obtiene o establece el codigo alterno
        ''' </summary>
        <JsonProperty>
        Public Property CodigoTipoUbicacion As Integer

        ''' <summary>
        ''' Obtiene o establece el estado del almacen
        ''' </summary>
        <JsonProperty>
        Public Property TipoUbicacion As ValorCatalogos

#Region "Filtros de Búsqueda"

#End Region

    End Class
End Namespace
