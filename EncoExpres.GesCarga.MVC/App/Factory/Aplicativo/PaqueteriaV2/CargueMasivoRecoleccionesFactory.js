﻿EncoExpresApp.factory('CargueMasivoRecoleccionesFactory', ['$http', function ($http) {

    var urlService = GetUrl('CargueMasivoRecolecciones');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.CumplirGuias = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CumplirGuias';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.GenerarPlantilla = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GenerarPlantilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.GuardarEntrega = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GuardarEntrega';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ConsultarIndicadores = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarIndicadores';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.AsociarGuiaPlanilla = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/AsociarGuiaPlanilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.CambiarEstadoRemesas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CambiarEstadoRemesasPaqueteria';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarEstadosRemesa = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarEstadosRemesa';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ObtenerControlEntregas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerControlEntregas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasPendientesControlEntregas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPendientesControlEntregas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasPorPlanillar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPorPlanillar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasRecogerOficina = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasRecogerOficina';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ConsultarRemesasRetornoContado = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasRetornoContado';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ValidarPreimpreso = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ValidarPreimpreso';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasPendientesRecibirOficina = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPendientesRecibirOficina';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ConsultarRemesasPendientesRecibirOficina = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPendientesRecibirOficina';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.RecibirRemesaPaqueteriaOficinaDestino = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/RecibirRemesaPaqueteriaOficinaDestino';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.EntregarOficina = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/EntregarOficina';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasPorLegalizar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPorLegalizar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasPorLegalizarRecaudo = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPorLegalizarRecaudo';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ConsultarGuiasPlanillaEntrega = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarGuiasPlanillaEntrega';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ConsultarGuiasEtiquetasPreimpresas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarGuiasEtiquetasPreimpresas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.InsertarMasivo = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/InsertarMasivo';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ConsultarTercerosIdentificacion = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarTercerosIdentificacion';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ObtenerUltimoConsecutivoTercero = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerUltimoConsecutivoTercero';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    
    factory.InsertarNuevosTerceros = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/InsertarNuevosTerceros';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    
    factory.ObtenerUltimaRecoleccion = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerUltimaRecoleccion';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    return factory;

}]);