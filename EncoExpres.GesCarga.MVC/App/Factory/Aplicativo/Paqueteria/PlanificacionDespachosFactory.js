﻿EncoExpresApp.factory('PlanificacionDespachosFactory', ['$http', function ($http) {

    var urlService = GetUrl('PlanificacionDespachos');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    };

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarDetalleRemesasResumen = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarDetalleRemesasResumen';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    return factory;
}]);