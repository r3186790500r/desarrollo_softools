﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Negocio.Basico.Tesoreria
''' <summary>
''' Controlador <see cref="SucursalesSIESAController"/>
''' </summary>
''' 
<Authorize>
Public Class SucursalesSIESAController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As SucursalesSIESA) As Respuesta(Of IEnumerable(Of SucursalesSIESA))
        Return New LogicaSucursalesSIESA(CapaPersistenciaSucursalesSIESA).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As SucursalesSIESA) As Respuesta(Of SucursalesSIESA)
        Return New LogicaSucursalesSIESA(CapaPersistenciaSucursalesSIESA).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As SucursalesSIESA) As Respuesta(Of Long)
        Return New LogicaSucursalesSIESA(CapaPersistenciaSucursalesSIESA).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As SucursalesSIESA) As Respuesta(Of Boolean)
        Return New LogicaSucursalesSIESA(CapaPersistenciaSucursalesSIESA).Anular(entidad)
    End Function

End Class
