﻿PRINT 'gsp_obtener_ciudades' 
GO
DROP PROCEDURE gsp_obtener_ciudades
GO
CREATE PROCEDURE gsp_obtener_ciudades
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC
)
AS
BEGIN
	SELECT
		1 AS Obtener,
		CIUD.EMPR_Codigo,
		CIUD.Codigo,
		CIUD.Nombre AS Ciudad,
		CIUD.Codigo_Alterno,
		--CIUD.Tarifa_Impuesto1,
		--CIUD.Tarifa_Impuesto2,
		CIUD.Estado,
		CIUD.DEPA_Codigo,
		DEPA.Nombre AS Departamento,
		DEPA.PAIS_Codigo,
		PAIS.Nombre AS Pais,
		CONCAT(CIUD.Nombre,' (',DEPA.Nombre,')') AS CiudadDepartamento

	FROM
		Ciudades CIUD,
		Departamentos DEPA,
		Paises PAIS
	WHERE
		CIUD.EMPR_Codigo = DEPA.EMPR_Codigo
		AND CIUD.EMPR_Codigo = @par_EMPR_Codigo
		AND CIUD.DEPA_Codigo = DEPA.Codigo
		AND DEPA.EMPR_Codigo = PAIS.EMPR_Codigo
		AND DEPA.PAIS_Codigo = PAIS.Codigo
		AND CIUD.Codigo = @par_Codigo 

END
GO