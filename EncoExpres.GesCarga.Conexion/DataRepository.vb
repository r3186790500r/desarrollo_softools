﻿''' <summary>
''' Clase Base <see cref="DataRepository"/>
''' </summary>
''' <typeparam name="T">Entidad</typeparam>
Public MustInherit Class DataRepository(Of T As New)

    ''' <summary>
    ''' Cadena de conexión
    ''' </summary>
    Protected ConnectionString As String
    ''' <summary>
    ''' Cadena de conexión documentos
    ''' </summary>
    Protected ConnectionStringDocumentos As String
End Class
