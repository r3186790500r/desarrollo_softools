﻿PRINT 'gsp_obtener_plan_unico_cuentas'
GO
DROP PROCEDURE gsp_obtener_plan_unico_cuentas
GO
CREATE PROCEDURE  gsp_obtener_plan_unico_cuentas  
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC  
)  
AS  
BEGIN  
  
SELECT   
1 AS Obtener,  
PLUC.EMPR_Codigo,  
PLUC.Codigo,  
PLUC.CATA_CCPU_Codigo,  
PLUC.Codigo_Cuenta,  
PLUC.Codigo_Alterno,  
PLUC.Nombre,  
PLUC.Exige_Centro_Costo,  
PLUC.Exige_Tercero,  
PLUC.Exige_Valor_Base,  
PLUC.Valor_Base,  
PLUC.Exige_Documento_Cruce,  
PLUC.Estado,  
CCPU.Campo1 AS ClasePlanUnicoCuenta, 
CONCAT(PLUC.Codigo_Cuenta,' - ',PLUC.Nombre) AS CodigoNombreCuenta 
FROM   
Plan_Unico_Cuentas PLUC,  
Valor_Catalogos CCPU  
  
WHERE  
  
PLUC.EMPR_Codigo = @par_EMPR_Codigo  
AND PLUC.Codigo = @par_Codigo  
  
AND PLUC.EMPR_Codigo = CCPU.EMPR_Codigo  
AND PLUC.CATA_CCPU_Codigo = CCPU.Codigo  
  
END  
GO