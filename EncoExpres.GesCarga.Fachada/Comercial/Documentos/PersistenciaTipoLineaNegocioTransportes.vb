﻿Imports EncoExpres.GesCarga.IFachada

Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Repositorio.Comercial.Documentos

Namespace Comercial.Documentos
    Public NotInheritable Class PersistenciaTipoLineaNegocioTransportes
        Implements IPersistenciaBase(Of TipoLineaNegocioTransportes)

        Public Function Consultar(filtro As TipoLineaNegocioTransportes) As IEnumerable(Of TipoLineaNegocioTransportes) Implements IPersistenciaBase(Of TipoLineaNegocioTransportes).Consultar
            Return New RepositorioTipoLineaNegocioTransportes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TipoLineaNegocioTransportes) As Long Implements IPersistenciaBase(Of TipoLineaNegocioTransportes).Insertar
            Return New RepositorioTipoLineaNegocioTransportes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TipoLineaNegocioTransportes) As Long Implements IPersistenciaBase(Of TipoLineaNegocioTransportes).Modificar
            Return New RepositorioTipoLineaNegocioTransportes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TipoLineaNegocioTransportes) As TipoLineaNegocioTransportes Implements IPersistenciaBase(Of TipoLineaNegocioTransportes).Obtener
            Return New RepositorioTipoLineaNegocioTransportes().Obtener(filtro)
        End Function

    End Class

End Namespace

