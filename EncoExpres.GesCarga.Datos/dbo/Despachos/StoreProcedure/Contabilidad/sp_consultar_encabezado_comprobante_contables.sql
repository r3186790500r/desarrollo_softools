﻿PRINT 'sp_consultar_encabezado_comprobante_contables'
GO
DROP PROCEDURE sp_consultar_encabezado_comprobante_contables
GO
CREATE PROCEDURE sp_consultar_encabezado_comprobante_contables
(      
	@par_EMPR_Codigo SMALLINT,        
	@par_Numero NUMERIC = NULL,       
	@par_Fecha_Inicial DATETIME = NULL,      
	@par_Fecha_Final DATETIME =  NULL,        
	@par_CATA_DOOR_Codigo NUMERIC = NULL,      
	@par_Numero_Documento NUMERIC = NULL,        
	@par_Estado SMALLINT = NULL,      
	@par_Estado_Interfaz SMALLINT = NULL,        
	@par_NumeroPagina NUMERIC = NULL,       
	@par_RegistrosPagina NUMERIC = NULL       
)      
AS      
BEGIN        
      
	set @par_Fecha_Inicial = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Inicial)                           
	set @par_Fecha_Inicial = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Inicial)),@par_Fecha_Inicial)                                  
	set @par_Fecha_Inicial = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Inicial)),@par_Fecha_Inicial)                                    
	set @par_Fecha_Final = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Final)                                
	set @par_Fecha_Final = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Final)),@par_Fecha_Final)                                  
	set @par_Fecha_Final = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Final)),@par_Fecha_Final)                                  
	set @par_Fecha_Final = DATEADD(MILLISECOND, 998,@par_Fecha_Final)                                  
	set @par_Fecha_Final = DATEADD(SECOND,59,@par_Fecha_Final)                                  
	set @par_Fecha_Final = DATEADD(MINUTE,59,@par_Fecha_Final)                                  
	set @par_Fecha_Final = DATEADD(HOUR,23,@par_Fecha_Final)       
        
	SET NOCOUNT ON;      
	DECLARE      
	@CantidadRegistros INT         
        
	SELECT @CantidadRegistros = (      
		SELECT DISTINCT       
		COUNT(1)       
		FROM     
		Encabezado_Comprobante_Contables as ENCC      
      
		LEFT JOIN Valor_Catalogos AS DOOR      
		ON ENCC.EMPR_Codigo = DOOR.EMPR_Codigo      
		AND ENCC.CATA_DOOR_Codigo = DOOR.Codigo      
    
		LEFT JOIN Valor_Catalogos AS ESIC      
		ON ENCC.EMPR_Codigo = ESIC.EMPR_Codigo      
		AND ENCC.CATA_ESIC_Codigo = ESIC.Codigo       
              
		WHERE     
    
		ENCC.EMPR_Codigo = @par_EMPR_Codigo    
		AND ENCC.Numero = ISNULL(@par_Numero, ENCC.Numero)        
		AND ENCC.Fecha >= ISNULL(@par_Fecha_Inicial, ENCC.Fecha)      
		AND ENCC.Fecha <= ISNULL(@par_Fecha_Final, ENCC.Fecha)        
		AND ENCC.CATA_DOOR_Codigo = ISNULL(@par_CATA_DOOR_Codigo, ENCC.CATA_DOOR_Codigo)      
		AND ENCC.Numero_Documento = ISNULL(@par_Numero_Documento, ENCC.Numero_Documento)      
		AND ENCC.Anulado = ISNULL(@par_Estado, ENCC.Anulado)      
		AND (ENCC.Interfase_Contable = @par_Estado_Interfaz OR @par_Estado_Interfaz IS NULL)
	);      
	WITH Pagina AS      
	(      
		SELECT DISTINCT    
		0 As Obtener      
		,ENCC.EMPR_Codigo      
		,ENCC.Numero      
		,ENCC.Fecha        
		,ENCC.CATA_DOOR_Codigo       
		,DOOR.Campo1 As NombreDocumentoOrigen        
		,ENCC.Año       
		,ENCC.Periodo        
		,ENCC.Numero_Documento       
		,ENCC.Observaciones        
		,ENCC.CATA_ESIC_Codigo       
		,ESIC.Campo2 As NombreEstadoInterfaz        
		,ENCC.Anulado
		,ISNULL(ENCC.Interfase_Contable, 0) Interfase_Contable
		,ISNULL(ENCC.Fecha_Interfase_Contable, '') Fecha_Interfase_Contable
		,ISNULL(ENCC.Intentos_Interfase_Contable, 0) Intentos_Interfase_Contable
		,ISNULL(ENCC.Mensaje_Error, '') Mensaje_Error
		,ROW_NUMBER() OVER(ORDER BY ENCC.Numero ASC) AS RowNumber      
      
		FROM     
		Encabezado_Comprobante_Contables as ENCC      
      
		LEFT JOIN Valor_Catalogos AS DOOR      
		ON ENCC.EMPR_Codigo = DOOR.EMPR_Codigo      
		AND ENCC.CATA_DOOR_Codigo = DOOR.Codigo      
    
		LEFT JOIN Valor_Catalogos AS ESIC      
		ON ENCC.EMPR_Codigo = ESIC.EMPR_Codigo      
		AND ENCC.CATA_ESIC_Codigo = ESIC.Codigo       
              
		WHERE     
    
		ENCC.EMPR_Codigo = @par_EMPR_Codigo    
		AND ENCC.Numero = ISNULL(@par_Numero, ENCC.Numero)        
		AND ENCC.Fecha >= ISNULL(@par_Fecha_Inicial, ENCC.Fecha)      
		AND ENCC.Fecha <= ISNULL(@par_Fecha_Final, ENCC.Fecha)        
		AND ENCC.CATA_DOOR_Codigo = ISNULL(@par_CATA_DOOR_Codigo, ENCC.CATA_DOOR_Codigo)      
		AND ENCC.Numero_Documento = ISNULL(@par_Numero_Documento, ENCC.Numero_Documento)      
		AND ENCC.Anulado = ISNULL(@par_Estado, ENCC.Anulado)      
		AND (ENCC.Interfase_Contable = @par_Estado_Interfaz OR @par_Estado_Interfaz IS NULL)  
	)      
      
	SELECT       
	Obtener      
	,EMPR_Codigo      
	,Numero      
	,Fecha        
	,CATA_DOOR_Codigo      
	,NombreDocumentoOrigen        
	,Año       
	,Periodo        
	,Numero_Documento       
	,Observaciones        
	,CATA_ESIC_Codigo       
	,NombreEstadoInterfaz         
	,Anulado
	,Interfase_Contable
	,Fecha_Interfase_Contable
	,Intentos_Interfase_Contable
	,Mensaje_Error      
	,@CantidadRegistros AS TotalRegistros      
	,@par_NumeroPagina AS PaginaObtener      
	,@par_RegistrosPagina AS RegistrosPagina      
      
	FROM Pagina      
	WHERE RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina  ,@CantidadRegistros)      
	AND RowNumber <= ISNULL(@par_NumeroPagina,1) * ISNULL(@par_RegistrosPagina  ,@CantidadRegistros)    
	ORDER BY Numero ASC      
      
END         
GO