﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento

    Public NotInheritable Class PersistenciaOrdenTrabajoMantenimiento
        Implements IPersistenciaBase(Of OrdenTrabajoMantenimiento)
        Public Function Consultar(filtro As OrdenTrabajoMantenimiento) As IEnumerable(Of OrdenTrabajoMantenimiento) Implements IPersistenciaBase(Of OrdenTrabajoMantenimiento).Consultar
            Return New RepositorioOrdenTrabajoMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As OrdenTrabajoMantenimiento) As Long Implements IPersistenciaBase(Of OrdenTrabajoMantenimiento).Insertar
            Return New RepositorioOrdenTrabajoMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As OrdenTrabajoMantenimiento) As Long Implements IPersistenciaBase(Of OrdenTrabajoMantenimiento).Modificar
            Return New RepositorioOrdenTrabajoMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As OrdenTrabajoMantenimiento) As OrdenTrabajoMantenimiento Implements IPersistenciaBase(Of OrdenTrabajoMantenimiento).Obtener
            Return New RepositorioOrdenTrabajoMantenimiento().Obtener(filtro)
        End Function

        Public Function Anular(entidad As OrdenTrabajoMantenimiento) As Boolean
            Return New RepositorioOrdenTrabajoMantenimiento().Anular(entidad)
        End Function

    End Class
End Namespace
