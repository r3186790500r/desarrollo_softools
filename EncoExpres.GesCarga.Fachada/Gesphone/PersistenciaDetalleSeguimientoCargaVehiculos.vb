﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Repositorio.Gesphone

Namespace Gesphone
    Public NotInheritable Class PersistenciaDetalleSeguimientoCargaVehiculos
        Implements IPersistenciaBase(Of DetalleSeguimientoCargaVehiculos)

        Public Function Consultar(filtro As DetalleSeguimientoCargaVehiculos) As IEnumerable(Of DetalleSeguimientoCargaVehiculos) Implements IPersistenciaBase(Of DetalleSeguimientoCargaVehiculos).Consultar
            Return New RepositorioDetalleSeguimientoCargaVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleSeguimientoCargaVehiculos) As Long Implements IPersistenciaBase(Of DetalleSeguimientoCargaVehiculos).Insertar
            Return New RepositorioDetalleSeguimientoCargaVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleSeguimientoCargaVehiculos) As Long Implements IPersistenciaBase(Of DetalleSeguimientoCargaVehiculos).Modificar
            Return New RepositorioDetalleSeguimientoCargaVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleSeguimientoCargaVehiculos) As DetalleSeguimientoCargaVehiculos Implements IPersistenciaBase(Of DetalleSeguimientoCargaVehiculos).Obtener
            Return New RepositorioDetalleSeguimientoCargaVehiculos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleSeguimientoCargaVehiculos) As Boolean
            Return New RepositorioDetalleSeguimientoCargaVehiculos().Anular(entidad)
        End Function
    End Class

End Namespace

