﻿

GO
PRINT 'Opción Menú Portal-Documentos'
GO
DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 21001		
GO
DELETE Menu_Aplicaciones WHERE Codigo = 21001
GO
INSERT INTO Menu_Aplicaciones( 
EMPR_Codigo,
Codigo,
MOAP_Codigo,
Nombre,
Padre_Menu,
Orden_Menu,
Mostrar_Menu,
Aplica_Habilitar,
Aplica_Consultar,
Aplica_Actualizar,
Aplica_Eliminar_Anular,
Aplica_Imprimir,
Aplica_Contabilidad,
Opcion_Permiso,
Opcion_Listado,
Aplica_Ayuda,
Url_Pagina,
Url_Ayuda) 
VALUES(
1
,21001
,210
,'Documentos'
,210
,10
,1
,1
,1
,1
,1
,1
,1
,0
,0
,0
,'#!'
,'#!'
)
GO
INSERT INTO Permiso_Grupo_Usuarios(
EMPR_Codigo,
MEAP_Codigo,
GRUS_Codigo,
Habilitar,
Consultar,
Actualizar,
Eliminar_Anular,
Imprimir,
Permiso,
Contabilidad
) 
VALUES (
1,
21001,
1,
1,
1,
1,
1,
1,
0,
0
) 



