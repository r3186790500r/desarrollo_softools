﻿for (var j = 0; j < $scope.ListadoTiemposRemesas.length; j++) {
    for (var i = 0; i < $scope.ListadoTiemposRemesas[j].Data.length; i++) {
        var item = $scope.ListadoTiemposRemesas[j].Data[i]
        //Condicionales Basicos de proceso
        if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
            if (item.Fecha >= new Date()) {
                $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor a la fecha actual')
            }
            if (i > 0) {
                if (item.Fecha < $scope.ListadoTiemposRemesas[j].Data[i - 1].Fecha) {
                    $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser menor a la fecha de ' + $scope.ListadoTiemposRemesas[j].Data[i - 1].Nombre)
                }
            }
        }

        //Condicionales Logisticos
        //---------------------------------------------Salida Cargue-----------------------------------------

        if (item.Codigo == 20205 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
            for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                if (item2.Codigo == 20201) { //Llegada cargue
                    if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                        if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor de 3 dias a la fecha de llegada al cargue')
                        }
                    }
                }
                if (item2.Codigo == 20202) { //Ingreso cargue
                    if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                        if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + '  debe ser mayor a 15 minutos de la fecha de ingreso cargue')
                        }
                    }
                }
            }
            if (!(item.Fecha >= new Date())) {
                if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                }
            }
        }

        //---------------------------------------------Fin Cargue-----------------------------------------

        if (item.Codigo == 20204 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
            for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                if (item2.Codigo == 20203) { //Inicio cargue
                    if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                        if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de inicio cargue')
                        }
                    }
                }
            }
            if (!(item.Fecha >= new Date())) {
                if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                    $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                }
            }
        }

        //---------------------------------------------Ingreso Cargue-----------------------------------------

        if (item.Codigo == 20202 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
            if (!(item.Fecha >= new Date())) {
                if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                    $scope.MensajesErrorModalTiempos.push('La fecha de ingreso cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                }
            }
        }

        //---------------------------------------------Llegada Cargue-----------------------------------------

        if (item.Codigo == 20201 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
            if (!(item.Fecha >= new Date())) {
                if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                    $scope.MensajesErrorModalTiempos.push('La fecha de llegada cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                }
            }
        }

        //---------------------------------------------LLegada Descargue-----------------------------------------

        if (item.Codigo == 20206 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
            for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                if (item2.Codigo == 20205) { //Salida cargue
                    if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                        if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 15) && !(item2.Fecha > item.Fecha)) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor a 15 dias de la fecha de salida cargue')
                        } else if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de salida cargue')
                        }
                    }
                }
            }
            if (!(item.Fecha >= new Date())) {
                if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                    $scope.MensajesErrorModalTiempos.push('La fecha de llegada descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                }
            }
        }

        //---------------------------------------------Ingreso Descargue-----------------------------------------

        if (item.Codigo == 20207 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
            if (!(item.Fecha >= new Date())) {
                if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                    $scope.MensajesErrorModalTiempos.push('La fecha de ingreso descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                }
            }
        }

        //---------------------------------------------Salida Descargue-----------------------------------------

        if (item.Codigo == 20210 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
            if (item2.Codigo == 20206) { //Llegada descargue
                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                    if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                        $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor de 3 dias a la fecha de llegada descargue')
                    }
                }
            }
            if (item2.Codigo == 20207) { //Ingreso descargue
                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                    if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                        $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de ingreso descargue')
                    }
                }
            }
            if (!(item.Fecha >= new Date())) {
                if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                    $scope.MensajesErrorModalTiempos.push('La fecha de salida descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                }
            }
        }

        //---------------------------------------------Fin Descargue-----------------------------------------

        if (item.Codigo == 20209 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
            for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                if (item2.Codigo == 20208) { //Inicio descargue
                    if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                        if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de inicio descargue')
                        }
                    }
                }
            }
            if (!(item.Fecha >= new Date())) {
                if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                    $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                }
            }
        }

    }
}