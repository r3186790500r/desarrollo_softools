﻿EncoExpresApp.controller("GestionarPlanMantenimientoEspecialCtrl", ['$scope', '$routeParams', 'blockUI', '$timeout', '$linq', 'UnidadReferenciaMantenimientoFactory', 'SistemasFactory', 'SubSistemasFactory',  'PlanEquipoMantenimientoFactory', 'ReferenciaAlmacenesFactory',
    function ($scope, $routeParams, blockUI, $timeout, $linq, UnidadReferenciaMantenimientoFactory, SistemasFactory, SubSistemasFactory, PlanEquipoMantenimientoFactory, ReferenciaAlmacenesFactory) {
        $scope.ListadoDetallePlanMantenimiento = [];
        $scope.MensajesError = [];
        $scope.MensajesErrorDetalle = [];
        $scope.ListadoEstados = [];
        $scope.ModificarDetalle = 0;
        $scope.Operacion = 'Adicionar';
        $scope.ValidarvalorManoObra = false
        $scope.ValidarValorRepuestos = false
        $scope.ValidarSubtotal = false
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.CodigoAlterno = '';
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PLAN_MANTENIMIENTO);

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ReferenciasTemporales = []

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Mantenimiento' }, { Nombre: 'Documentos' }, { Nombre: 'Planes Mantenimiento' }];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        };
        $scope.ModeloFecha = new Date();
        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Codigo = 0;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Codigo > 0) {
                document.location.href = '#!ConsultarPlanMantenimiento/' + $scope.Modelo.Codigo;
            } else {
                document.location.href = '#!ConsultarPlanMantenimiento';
            }
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar el combo de estados*/
        $scope.ListadoEstados = [           
            { Codigo: 0, Nombre: 'BORRADOR' },
            { Codigo: 1, Nombre: 'DEFINITIVO' }            
        ];

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');
        /*Cargar el combo de Sistema mantenimiento*/
        SistemasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSistemaMantenimiento = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoSistemaMantenimiento.push(item);
                    });
                    //$scope.SistemaMantenimiento = response.data.Datos[0];
                    $scope.ListadoSistemaMantenimiento.Asc = true
                    OrderBy('Nombre', undefined, $scope.ListadoSistemaMantenimiento)
                    $timeout(function () {
                        if ($("[name='tiposistema']")[0].options[0].value == "?") {
                            $("[name='tiposistema']")[0].options[0].remove()
                        }
                    }, 50);

                    $timeout(function () {
                        $("[name='tiposistema']").selectpicker()
                    }, 200);


                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de Referencias*/
        ReferenciaAlmacenesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoReferencias = [];
                    $scope.ListadoReferencias = response.data.Datos;
                    for (var i = 0; i < $scope.ListadoReferencias.length; i++) {
                        $scope.ListadoReferencias[i].NombreCompleto = $scope.ListadoReferencias[i].Referencia.CodigoReferencia + '(' + $scope.ListadoReferencias[i].Referencia.Nombre + ')'

                    }
                    $scope.ListadoReferencias.Asc = true
                    OrderBy('Nombre', undefined, $scope.ListadoReferencias)
                    $timeout(function () {
                        if ($("#CmbReferencia")[0].options[0] != undefined) {
                            if ($("#CmbReferencia")[0].options[0].value == "?") {
                                $("#CmbReferencia")[0].options[0].remove()
                            }
                        }
                    }, 50);

                    $timeout(function () {
                        $("#CmbReferencia").selectpicker()
                    }, 200);
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de Subsistemas Mantenimiento*/
        SubSistemasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSubsistemaMantenimiento = [];
                    $scope.ListadoSubsistemaFiltrado = [];
                    $scope.ListadoSubsistemaMantenimiento = response.data.Datos
                    $scope.ListadoSubsistemaMantenimiento.Asc = true
                    OrderBy('Nombre', undefined, $scope.ListadoSubsistemaMantenimiento)
                  
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo Unidad Referencia Mantenimiento*/
        UnidadReferenciaMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoUnidadReferenciaMantenimiento = [];
                    $scope.ListadoUnidadReferenciaMantenimiento = response.data.Datos
                    $scope.UnidadReferenciaMantenimiento = response.data.Datos[0];
                } else {
                    $scope.ListadoUnidadReferenciaMantenimiento = [];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR PLAN DE MANTENIMIENTO';
            $scope.Deshabilitar = true;
            Obtener();
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando plan de mantenimiento ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando  plan de mantenimiento ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            PlanEquipoMantenimientoFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Nombre = response.data.Datos.Nombre
                        $scope.CodigoAlterno = response.data.Datos.CodigoAlterno
                       // $scope.Descripcion = response.data.Datos.Detalles

                        if (response.data.Datos.Estado !== 2) {
                            try { $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado); } catch (e) { }
                        }
                        else {
                            $scope.ListadoEstados.push({ Nombre: 'Anulado', Codigo: 2 });
                            try { $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado); } catch (e) { }
                        }
                        if (response.data.Datos.Estado == 0) {
                            $scope.Deshabilitar = false;
                            $scope.Bloquear = true;
                        }
                        else if (response.data.Datos.Estado == 1) {
                            $scope.Deshabilitar = true;
                        }

                        $scope.ListadoDetallePlanMantenimiento = response.data.Datos.Detalles



                    }
                    else {
                        ShowError('No se logro consultar el plan de mantenimiento No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarPlanMantenimiento';
                    }
                }, function (response) {
                    ShowError(response.statusText);                    
                    document.location.href = '#!ConsultarPlanMantenimiento';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.InicializarDetalle = function () {
            showModal('modalMantenimiento');
            $scope.ModificarDetalle = 0
            $scope.Cantidad = ''
            $scope.ValorManoObra = ''
            $scope.ValorRepuestos = ''
            $scope.Descripcion = ''
            $scope.FrecuenciaUso = ''
            $timeout(function () {
                $("[name='tiposistema']").prop('disabled', false);
                $timeout(function () {
                    $("[name='tiposubsistema']").prop('disabled', false);
                }, 200);
            }, 100);
        }
        $scope.ConfirmacionGuardarPlan = function () {
            showModal('modalConfirmacionGuardarPlan');
        };
        // Metodo para guardar /modificar
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarPlan');

            $scope.Modelo.Nombre = $scope.Nombre
            $scope.Modelo.Codigo = parseInt($scope.Modelo.Codigo);
            $scope.Modelo.EstadoDocumento = $scope.Estado
            //$scope.Modelo.Descripcion = $scope.Descripcion
            $scope.Modelo.CodigoAlterno = $scope.CodigoAlterno
            $scope.Modelo.Detalles = $scope.ListadoDetallePlanMantenimiento;
            $scope.Modelo.Detalles.forEach(detalle => {
                detalle.Referencias.forEach(referencia => referencia.Cantidad = parseInt(referencia.Cantidad));
                detalle.SistemaMantenimiento = { Codigo: detalle.SistemaMantenimiento.Codigo };
                detalle.SubsistemaMantenimiento = { Codigo: detalle.SubsistemaMantenimiento.Codigo };
                detalle.UnidadReferenciaMantenimiento = { Codigo: detalle.UnidadReferenciaMantenimiento.Codigo };
            }
            );
           
            if (DatosRequeridos()) {
                PlanEquipoMantenimientoFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el plan de mantenimiento ' + $scope.Nombre);
                                    location.href = '#!ConsultarPlanMantenimiento/' + response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó el plan de mantenimiento ' + $scope.Nombre);
                                    location.href = '#!ConsultarPlanMantenimiento/' + $scope.Modelo.Codigo;
                                }
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        // Valida los datos requridos 
        function DatosRequeridos() {
            $scope.MensajesErrorRecorrido = [];
            var continuar = true;
            if ($scope.Nombre == '' || $scope.Nombre == 0 || $scope.Nombre == undefined) {
                $scope.MensajesError.push('Debe ingresar el nombre del plan de mantenimiento');
                continuar = false;
            }
            if ($scope.Estado == '' || $scope.Estado == 0 || $scope.Estado == undefined) {
                $scope.MensajesError.push('Debe ingresar el estado del plan de mantenimiento');
                continuar = false;
            }
            if ($scope.ListadoDetallePlanMantenimiento.length == 0) {
                $scope.MensajesError.push('Debe Ingresar mínimo un detalle al plan de mantenimiento');
                continuar = false;
            }

            if ($scope.CodigoAlterno == undefined || $scope.CodigoAlterno == null || $scope.CodigoAlterno == '') {
                $scope.MensajesError.push('Debe Ingresar un Código Alterno');
                continuar = false;
            }
            return continuar

        }
        /*Asignar Movimiento Al Comprobante*/
        $scope.GuardarDetalleMantenimiento = function () {
            $scope.MensajesErrorDetalleMantenimiento = [];
            var cont = 0
            for (var i = 0; i < $scope.ListadoDetallePlanMantenimiento.length; i++) {
                var item = $scope.ListadoDetallePlanMantenimiento[i]
                if (item.SubsistemaMantenimiento.Codigo == $scope.SubsistemaMantenimiento.Codigo && item.SistemaMantenimiento.Codigo == $scope.SistemaMantenimiento.Codigo) {
                    cont++
                }
            } if (cont > 0) {
                ShowError('Ya hay un registro con el sistema y subsitema seleccionado')
            } else {
                if (DatosRequeridosPlanMantenimiento()) {
                    CargarListadoDetallePlanMantenimiento()
                }
            }
        }
        $scope.AdicionarReferenacia = function () {
            var cont = 0
            for (var i = 0; i < $scope.ReferenciasTemporales.length; i++) {
                var item = $scope.ReferenciasTemporales[i]
                if (item.Codigo == $scope.Referencia.Codigo) {
                    cont++
                }
            }
            if (cont > 0) {
                ShowError('Ya hay un registro con la referencia seleccionada')
            } else {
                if ($scope.Referencia !== undefined && $scope.Referencia !== '') {
                    $scope.ReferenciasTemporales.push({ Codigo: $scope.Referencia.Referencia.Codigo, NombreCompleto: $scope.Referencia.Referencia.Nombre  })
                } else {
                    ShowError('seleccione la referencia')
                }
            }
        }
        function CargarListadoDetallePlanMantenimiento() {
            closeModal('modalMantenimiento');
            $scope.MaskNumero();
            $scope.ListadoDetallePlanMantenimiento.push({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                SistemaMantenimiento: $scope.SistemaMantenimiento,
                SubsistemaMantenimiento: $scope.SubsistemaMantenimiento,
                UnidadReferenciaMantenimiento: $scope.UnidadReferenciaMantenimiento,
                FrecuenciaUso: $scope.FrecuenciaUso,
                Descripcion: $scope.Descripcion,
                Cantidad: $scope.Cantidad,
                ValorManoObra: $scope.ValorManoObra,
                ValorRepuestos: $scope.ValorRepuestos,
                Referencias: [],
            });
        }
        //---------------------------------------------------------------------------------------------------------------//
        $scope.AsignarValorManoObra = function () {
            if ($scope.ValorManoObra > 9999999999) {
                $scope.ValidarvalorManoObra = true
            }
            else {
                $scope.ValidarvalorManoObra = false
            }
        }
        $scope.AsignarValorRepuestos = function () {
            if ($scope.ValorRepuestos > 9999999999) {
                $scope.ValidarValorRepuestos = true
            }
            else {
                $scope.ValidarValorRepuestos = false
            }
        }
        //---------------------------------------------------------------------------------------------------------------------------//

        function DatosRequeridosPlanMantenimiento() {
            $scope.MensajesErrorDetalle = [];
            var continuar = true;

            if ($scope.SistemaMantenimiento == undefined || $scope.SistemaMantenimiento == "" || $scope.SistemaMantenimiento == null) {
                $scope.MensajesErrorDetalle.push('Debe seleccionar un sistema de mantenimiento');
                continuar = false;
            }
            if ($scope.SubsistemaMantenimiento == undefined || $scope.SubsistemaMantenimiento == "" || $scope.SubsistemaMantenimiento == null) {
                $scope.MensajesErrorDetalle.push('Debe seleccionar un subsistema de mantenimiento');
                continuar = false;
            }
            if ($scope.SubsistemaMantenimiento.TipoSubsistema.Codigo != 2) {
                if ($scope.UnidadReferenciaMantenimiento == undefined || $scope.UnidadReferenciaMantenimiento == "" || $scope.UnidadReferenciaMantenimiento == null) {
                    $scope.MensajesErrorDetalle.push('Debe seleccionar una unidad de frecuencia');
                    continuar = false;
                }
                if ($scope.FrecuenciaUso == undefined || $scope.FrecuenciaUso == "" || $scope.FrecuenciaUso == null) {
                    $scope.MensajesErrorDetalle.push('Debe ingresar la frecuencia');
                    continuar = false;
                }
                if ($scope.Cantidad == undefined || $scope.Cantidad == "" || $scope.Cantidad == null) {
                    $scope.MensajesErrorDetalle.push('Debe ingresar la cantidad');
                    continuar = false;
                }
            }
            if ($scope.ValorManoObra == undefined || $scope.ValorManoObra == "" || $scope.ValorManoObra == null) {
                $scope.ValorManoObra = 0;
            }
            if ($scope.ValorRepuestos == undefined || $scope.ValorRepuestos == "" || $scope.ValorRepuestos == null) {
                $scope.ValorRepuestos = 0;
            }
            return continuar;
        }

        /*Funcion que permite modificar el detalle del plan de mantenimiento*/
        $scope.EditarDetallePlan = function (itemDetalle) {
            $("#tiposistema").prop('disabled', false);
            $("#tiposubsistema").prop('disabled', false);
            $scope.tempItem = itemDetalle
            $timeout(function () {
                $scope.ModificarDetalle = 1 // variable que identifica si es para editar o agregar
                $("#tiposistema").selectpicker('destroy')
                $scope.SistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSistemaMantenimiento).First('$.Codigo == ' + itemDetalle.SistemaMantenimiento.Codigo);
                $timeout(function () {
                    if ($("#tiposistema")[0].options[0].value == "?" || $("#tiposubsistema")[0].options[0].value == "") {
                        $("#tiposistema")[0].options[0].remove()
                    }
                }, 50);
                $timeout(function () {
                    $("#tiposistema").selectpicker()
                }, 200);
                $scope.FiltrarSubsistema()
                $timeout(function () {
                    $("#tiposubsistema").selectpicker('destroy')
                    $scope.SubsistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSubsistemaMantenimiento).First('$.Codigo == ' + itemDetalle.SubsistemaMantenimiento.Codigo);;
                    $timeout(function () {
                        if ($("#tiposubsistema")[0].options[0].value == "?" || $("#tiposubsistema")[0].options[0].value == "") {
                            $("#tiposubsistema")[0].options[0].remove()
                        }
                    }, 50);
                    $timeout(function () {
                        $("#tiposubsistema").selectpicker()
                    }, 200);
                    $scope.UnidadReferenciaMantenimiento = $linq.Enumerable().From($scope.ListadoUnidadReferenciaMantenimiento).First('$.Codigo == ' + itemDetalle.UnidadReferenciaMantenimiento.Codigo);
                    $scope.FrecuenciaUso = itemDetalle.FrecuenciaUso;
                    $scope.Descripcion = itemDetalle.Descripcion
                    $scope.Cantidad = itemDetalle.Cantidad
                    $scope.ValorManoObra = itemDetalle.ValorManoObra;
                    $scope.ValorRepuestos = itemDetalle.ValorRepuestos;
                    $scope.AplicarCampos($scope.SubsistemaMantenimiento)
                    showModal('modalMantenimiento');
                    $timeout(function () {
                        $("#tiposistema").prop('disabled', true);
                        $timeout(function () {
                            $("#tiposubsistema").prop('disabled', true);
                        }, 200);
                    }, 100);
                }, 300)

            }, 200);
        }
        $scope.AsociarReferencia = function (itemAsocia) {
            $scope.tempItemAsociacion = itemAsocia

            $scope.Indicador = itemAsocia.$$hashKey

            if (itemAsocia.Referencias !== undefined) {
                $scope.ReferenciasTemporales = itemAsocia.Referencias
            } else {
                $scope.ReferenciasTemporales = []
            }
            $("#tiposistema2").prop('disabled', false);
            $("#tiposubsistema2").prop('disabled', false);
            $timeout(function () {
                $("#tiposistema2").selectpicker('destroy')
                $scope.SistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSistemaMantenimiento).First('$.Codigo == ' + itemAsocia.SistemaMantenimiento.Codigo);
                $timeout(function () {
                    if ($("#tiposistema2")[0].options[0].value == "?" || $("#tiposubsistema2")[0].options[0].value == "") {
                        $("#tiposistema2")[0].options[0].remove()
                    }
                }, 50);
                $timeout(function () {
                    $("#tiposistema2").selectpicker()
                }, 200);
                $scope.FiltrarSubsistema()
                $timeout(function () {
                    $("#tiposubsistema2").selectpicker('destroy')
                    $scope.SubsistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSubsistemaMantenimiento).First('$.Codigo == ' + itemAsocia.SubsistemaMantenimiento.Codigo);;
                    $timeout(function () {
                        if ($("#tiposubsistema2")[0].options[0].value == "?" || $("#tiposubsistema2")[0].options[0].value == "") {
                            $("#tiposubsistema2")[0].options[0].remove()
                        }
                    }, 50);
                    $timeout(function () {
                        $("#tiposubsistema2").selectpicker()
                    }, 200);
                    $scope.Cantidad = itemAsocia.Cantidad
                    $scope.AplicarCampos($scope.SubsistemaMantenimiento)
                    showModal('modalAsociacion');
                    $timeout(function () {
                        $("#tiposistema2").prop('disabled', true);
                        $timeout(function () {
                            $("#tiposubsistema2").prop('disabled', true);
                        }, 200);
                    }, 100);
                }, 300)
            }, 200)

        }
        $scope.GuardarAsosiacionReferanciaMantenimiento = function () {
            closeModal('modalAsociacion');
            $scope.ListadoDetallePlanMantenimiento.forEach(function (item) {
                if ($scope.Indicador == item.$$hashKey) {
                    item.Referencias = $scope.ReferenciasTemporales
                }
            })
            $scope.tempItemAsociacion.Referencias = $scope.ReferenciasTemporales
        }

        $scope.ModificarDetallePlan = function () {
            if (DatosRequeridosPlanMantenimiento()) {
                closeModal('modalMantenimiento');

                $scope.tempItem.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                $scope.tempItem.SistemaMantenimiento = $scope.SistemaMantenimiento;
                $scope.tempItem.SubsistemaMantenimiento = $scope.SubsistemaMantenimiento;
                $scope.tempItem.UnidadReferenciaMantenimiento = $scope.UnidadReferenciaMantenimiento;
                $scope.tempItem.FrecuenciaUso = $scope.FrecuenciaUso;
                $scope.tempItem.Descripcion = $scope.Descripcion;
                $scope.tempItem.Cantidad = $scope.Cantidad;
                $scope.tempItem.ValorManoObra = $scope.ValorManoObra;
                $scope.tempItem.ValorRepuestos = $scope.ValorRepuestos;
            };
        };

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };

        /*Eliminar Detalle Plan mantenimiento*/
        $scope.ConfirmacionEliminarDetalle = function (indice) {
            $scope.MensajeEliminar = { indice };
            showModal('modalEliminarDetalle');
        };

        $scope.EliminarDetalle = function (indice) {
            $scope.ListadoDetallePlanMantenimiento.splice(indice, 1);
            closeModal('modalEliminarDetalle');
        };

        $scope.FiltrarSubsistema = function () {
            $("[name='tiposubsistema']").selectpicker('destroy')
            $scope.ListadoSubsistemaFiltrado = []
            for (var i = 0; i < $scope.ListadoSubsistemaMantenimiento.length; i++) {
                var item = $scope.ListadoSubsistemaMantenimiento[i]
                if (item.SistemaMantenimiento != null && $scope.SistemaMantenimiento != null) {
                    if (item.SistemaMantenimiento.Codigo == $scope.SistemaMantenimiento.Codigo) {
                        $scope.ListadoSubsistemaFiltrado.push(item)
                    }
                }
            }
            $scope.ListadoSubsistemaFiltrado.Asc = true
            OrderBy('Nombre', undefined, $scope.ListadoSubsistemaFiltrado)
            $scope.SubsistemaMantenimiento = $scope.ListadoSubsistemaFiltrado[0]
            $timeout(function () {
                if ($("[name='tiposubsistema']")[0].options[0].value == "?" || $("[name='tiposubsistema']")[0].options[0].value == "") {
                    $("[name='tiposubsistema']")[0].options[0].remove()
                }
            }, 50);
            $timeout(function () {
                $("[name='tiposubsistema']").selectpicker()
                $scope.AplicarCampos($scope.SubsistemaMantenimiento)
            }, 200);
        }

        $scope.AplicarCampos = function (item) {
            if ($scope.SubsistemaMantenimiento != undefined) {
                if ($scope.SubsistemaMantenimiento.TipoSubsistema.Codigo == 2) {
                    $scope.AplicaValores = false
                    $scope.AplicaManoObra = true
                    $scope.Cantidad = 0
                    $scope.FrecuenciaUso = 0
                    $scope.ValorRepuestos = 0
                } else {
                    $scope.AplicaValores = true
                    $scope.AplicaManoObra = false
                    $scope.ValorManoObra = 0
                }
            }
        }

        $scope.MaskMayus = function () {
            try { $scope.Nombre = $scope.Nombre.toUpperCase() } catch (e) { }
        };
        $scope.MaskValores = function () {
            try { $scope.ValorManoObra = MascaraValores($scope.ValorManoObra) } catch (e) { }
            try { $scope.ValorRepuestos = MascaraValores($scope.ValorRepuestos) } catch (e) { }
        };

        $scope.MaskNumero = function () {
            try { $scope.ValorManoObra = MascaraNumero($scope.ValorManoObra) } catch (e) { }
            try { $scope.ValorRepuestos = MascaraNumero($scope.ValorRepuestos) } catch (e) { }
        };
    }]);