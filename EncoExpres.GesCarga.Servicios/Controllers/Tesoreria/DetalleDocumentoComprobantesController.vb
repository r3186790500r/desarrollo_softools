﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Negocio.Tesoreria

''' <summary>
''' Controlador <see cref="DetalleDocumentoComprobantesController"/>
''' </summary>
<Authorize>
Public Class DetalleDocumentoComprobantesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleDocumentoComprobantes) As Respuesta(Of IEnumerable(Of DetalleDocumentoComprobantes))
        Return New LogicaDetalleDocumentoComprobantes(CapaPersistenciaDetalleDocumentoComprobantes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleDocumentoComprobantes) As Respuesta(Of DetalleDocumentoComprobantes)
        Return New LogicaDetalleDocumentoComprobantes(CapaPersistenciaDetalleDocumentoComprobantes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleDocumentoComprobantes) As Respuesta(Of Long)
        Return New LogicaDetalleDocumentoComprobantes(CapaPersistenciaDetalleDocumentoComprobantes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleDocumentoComprobantes) As Respuesta(Of Boolean)
        Return New LogicaDetalleDocumentoComprobantes(CapaPersistenciaDetalleDocumentoComprobantes).Anular(entidad)
    End Function
End Class
