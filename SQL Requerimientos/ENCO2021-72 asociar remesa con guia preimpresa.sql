USE [GESCARGA50_DESA]
GO

/****** Object:  StoredProcedure [dbo].[gsp_consultar_detalle_guias_preimpresas]    Script Date: 11/01/2022 8:18:51 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_modificar_guias_preimpresas_remesas]
(
@par_EMPR_Codigo SMALLINT,
@par_Numero_Preimpreso NUMERIC,
@par_ENRE_Numero NUMERIC
)
AS
BEGIN

UPDATE Detalle_Asignacion_Guias_Preimpresas SET ENRE_Numero = @par_ENRE_Numero
where EMPR_Codigo = @par_EMPR_Codigo
and Numero_Preimpreso = @par_Numero_Preimpreso

select @@ROWCOUNT as Cantidad
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_detalle_guias_preimpresas]  
(                          
 @par_empr_codigo NUMERIC,                       
 @par_oficina_Origen NUMERIC = null,                          
 @par_Oficina_Destino NUMERIC  = null,                   
 @par_Tipo_Precinto NUMERIC  = null,                         
 @par_Numero_Inicial NUMERIC = null  ,                
 @par_Numero_Final NUMERIC = null  ,                
 @par_Estado numeric = null,                
 @par_Estado_Precinto numeric = null,                
 @par_ENPD_Numero NUMERIC = null  ,                
 @par_ENRE_Numero NUMERIC = null  ,                
 @par_ENMC_Numero NUMERIC = null  ,                
 @par_numeropagina numeric = null,                          
 @par_registrospagina numeric = null        ,      
 @par_TERC_Codigo_Responsable numeric(18,0) = null      
)                            
AS                            
BEGIN      
 --estados                
 --0 disponible                
 --1 ocupado                
 --2 anulado             
 DECLARE              
 @cantidadregistros int                            
 select @cantidadregistros = (                            
 select distinct                             
 count(1)                             
                    
 from Detalle_Asignacion_Guias_Preimpresas DAPO                    
                    
 LEFT JOIN Encabezado_Remesas ENRE ON                 
 DAPO.EMPR_Codigo = ENRE.EMPR_Codigo                 
 AND CAST(DAPO.Numero_Preimpreso AS VARCHAR) = ENRE.Numeracion              
                
 LEFT JOIN Encabezado_Planilla_Despachos  ENPD ON                     
 DAPO.EMPR_Codigo = ENPD.EMPR_Codigo                     
 AND ENRE.ENPD_Numero = ENPD.Numero                  
          
 LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON                     
 DAPO.EMPR_Codigo = ENMC.EMPR_Codigo                    
 AND ENPD.ENMC_Numero = ENMC.Numero             
                    
 LEFT JOIN Oficinas OFIC on                     
 DAPO.EMPR_Codigo = OFIC.EMPR_Codigo                    
 AND DAPO.OFIC_Codigo = OFIC.Codigo                    
                    
 LEFT JOIN Oficinas OFOR on                     
 DAPO.EMPR_Codigo = OFOR.EMPR_Codigo                    
 AND DAPO.OFIC_Codigo_Origen = OFOR.Codigo       
      
 LEFT JOIN Terceros TERC ON      
 DAPO.EMPR_Codigo = TERC.EMPR_Codigo AND      
 DAPO.TERC_Codigo_Responsable = TERC.Codigo      
      
 WHERE DAPO.EMPR_Codigo = @par_Empr_Codigo                
 AND DAPO.Numero_Preimpreso >= ISNULL(@par_Numero_Inicial, DAPO.Numero_Preimpreso)              
 AND DAPO.Numero_Preimpreso <= ISNULL(@par_Numero_Final, DAPO.Numero_Preimpreso)                  
 AND DAPO.OFIC_Codigo = ISNULL(@par_Oficina_Destino,DAPO.OFIC_Codigo)                
 AND DAPO.OFIC_Codigo_Origen = ISNULL(@par_oficina_Origen,DAPO.OFIC_Codigo_Origen)               
 AND (ENPD.Numero_Documento = @par_ENPD_Numero or @par_ENPD_Numero is null)               
 AND (ENRE.Numero_Documento = @par_ENRE_Numero or @par_ENRE_Numero is null)               
 AND (ENMC.Numero_Documento = @par_ENMC_Numero or @par_ENMC_Numero is null)                    
 AND (case              
 WHEN DAPO.Anulado = 0 AND  ISNULL(DAPO.TERC_Codigo_Responsable,0) = 0 THEN 3       
 WHEN DAPO.Estado = 1 AND DAPO.Anulado = 0 AND DAPO.ENRE_Numero = 0 THEN 0       
 WHEN DAPO.Estado = 1 AND DAPO.Anulado = 0 AND DAPO.ENRE_Numero > 0  THEN 1                
 WHEN DAPO.Anulado = 1  THEN 2        
 END    
 = @par_Estado_Precinto OR  @par_Estado_Precinto IS NULL         
 AND DAPO.Estado <> 2      
 )    
 AND DAPO.TERC_Codigo_Responsable = ISNULL(@par_TERC_Codigo_Responsable,DAPO.TERC_Codigo_Responsable)      
 --AND (ENPD.TIDO_Codigo = 150 or ENPD.TIDO_Codigo  is null)                         
 );                             
 with pagina as                            
 (  
 select                           
 DAPO.ID IDPrecinto,                    
 DAPO.EMPR_Codigo,                    
 DAPO.Numero_Preimpreso,                    
 ISNULL(ENMC.Numero_Documento ,0) as NumeroMafiesto,                    
 ISNULL(ENPD.Numero_Documento ,0) as NumeroPlanillaDespacho,            
 ISNULL (ENRE.Numero_Documento,0) AS NumeroRemesa,  
  ISNULL (ENRE.Numero,0) AS ENRE_Numero,  
 DAPO.OFIC_Codigo,  
 OFIC.Nombre NombreOficina,                    
 OFOR.Nombre NombreOficinaOrigen,                    
 DAPO.Estado,                    
 DAPO.Anulado ,                
 DAPO.TERC_Codigo_Responsable,      
 LTRIM(RTRIM(CONCAT(TERC.Razon_Social,' ',TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2))) as NombreResponsable,      
 ROW_NUMBER() over(order by DAPO.Numero_Preimpreso) as rownumber                            
 from Detalle_Asignacion_Guias_Preimpresas DAPO                    
       
 LEFT JOIN Encabezado_Remesas ENRE ON                 
 DAPO.EMPR_Codigo = ENRE.EMPR_Codigo                 
 AND CAST(DAPO.Numero_Preimpreso AS VARCHAR) = ENRE.Numeracion                     
                
 LEFT JOIN Encabezado_Planilla_Despachos  ENPD ON                     
 DAPO.EMPR_Codigo = ENPD.EMPR_Codigo                     
 AND ENRE.ENPD_Numero = ENPD.Numero                  
          
 LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON                     
 DAPO.EMPR_Codigo = ENMC.EMPR_Codigo                    
 AND ENPD.ENMC_Numero = ENMC.Numero             
                    
 LEFT JOIN Oficinas OFIC on                     
 DAPO.EMPR_Codigo = OFIC.EMPR_Codigo                    
 AND DAPO.OFIC_Codigo = OFIC.Codigo                    
                    
 LEFT JOIN Oficinas OFOR on                     
 DAPO.EMPR_Codigo = OFOR.EMPR_Codigo                    
 AND DAPO.OFIC_Codigo_Origen = OFOR.Codigo          
      
 LEFT JOIN Terceros TERC ON      
 DAPO.EMPR_Codigo = TERC.EMPR_Codigo AND      
 DAPO.TERC_Codigo_Responsable = TERC.Codigo      
                    
 WHERE DAPO.EMPR_Codigo = @par_Empr_Codigo                    
 AND DAPO.Numero_Preimpreso >= ISNULL(@par_Numero_Inicial, DAPO.Numero_Preimpreso)              
 AND DAPO.Numero_Preimpreso <= ISNULL(@par_Numero_Final, DAPO.Numero_Preimpreso)                 
 AND DAPO.OFIC_Codigo = ISNULL(@par_Oficina_Destino,DAPO.OFIC_Codigo)                
 AND DAPO.OFIC_Codigo_Origen = ISNULL(@par_oficina_Origen,DAPO.OFIC_Codigo_Origen)                
 AND (ENPD.Numero_Documento = @par_ENPD_Numero or @par_ENPD_Numero is null)               
 AND (ENRE.Numero_Documento = @par_ENRE_Numero or @par_ENRE_Numero is null)               
 AND (ENMC.Numero_Documento = @par_ENMC_Numero or @par_ENMC_Numero is null)                 
 AND (case              
 WHEN DAPO.Anulado = 0 AND  ISNULL(DAPO.TERC_Codigo_Responsable,0) = 0 THEN 3       
 WHEN DAPO.Estado = 1 AND DAPO.Anulado = 0 AND DAPO.ENRE_Numero = 0 THEN 0       
 WHEN DAPO.Estado = 1 AND DAPO.Anulado = 0 AND DAPO.ENRE_Numero > 0  THEN 1                    
 WHEN DAPO.Anulado = 1  THEN 2 END                   
 = @par_Estado_Precinto OR  @par_Estado_Precinto IS NULL           
 AND DAPO.Estado <> 2      
 )    
 AND DAPO.TERC_Codigo_Responsable = ISNULL(@par_TERC_Codigo_Responsable,DAPO.TERC_Codigo_Responsable)      
 --AND (ENPD.TIDO_Codigo = 150 or ENPD.TIDO_Codigo  is null)                         
 )                            
 SELECT DISTINCT                            
 0 as obtener,                            
 EMPR_Codigo ,                          
 IDPrecinto,                    
 EMPR_Codigo,                    
 Numero_Preimpreso,  
 TERC_Codigo_Responsable,  
 NombreResponsable,      
 NumeroMafiesto,                    
 NumeroPlanillaDespacho,  
 OFIC_Codigo,         
 NombreOficina,                    
 NombreOficinaOrigen,                    
 NumeroRemesa,    
 ENRE_Numero,
 Estado,                    
              
 Anulado,                          
 @cantidadregistros as TotalRegistros,                            
 @par_numeropagina as PaginaObtener,                            
 @par_registrospagina as RegistrosPagina                            
 FROM                            
 pagina                            
 WHERE                            
 rownumber > (isnull(@par_numeropagina, 1) -1) * isnull(@par_registrospagina, @cantidadregistros)                             and rownumber <= isnull(@par_numeropagina, 1) * isnull(@par_registrospagina, @cantidadregistros)                            
 order by Numero_Preimpreso             
END  
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_obtener_detalle_guia_preimpresa]           
(@par_Empr_Codigo Numeric           
,@par_Numero numeric )          
AS BEGIN          
        
SELECT            
DAPO.ID IDPrecinto,          
DAPO.EMPR_Codigo,          
DAPO.Numero_Preimpreso,               
ISNULL(ENRE.Numero_Documento,0) AS NumeroRemesa,   
OFIC.Nombre NombreOficina,          
OFOR.Nombre NombreOficinaOrigen,          
DAPO.Estado,          
DAPO.Anulado ,      
DAPO.ENRE_Numero    
FROM Detalle_Asignacion_Guias_Preimpresas DAPO          

LEFT JOIN Oficinas OFIC on           
DAPO.EMPR_Codigo = OFIC.EMPR_Codigo          
AND DAPO.OFIC_Codigo = OFIC.Codigo          
          
LEFT JOIN Oficinas OFOR on           
DAPO.EMPR_Codigo = OFOR.EMPR_Codigo          
AND DAPO.OFIC_Codigo_Origen = OFOR.Codigo          
          
 LEFT JOIN Encabezado_Remesas ENRE ON       
DAPO.EMPR_Codigo = ENRE.EMPR_Codigo       
AND CAST(DAPO.Numero_Preimpreso AS VARCHAR) = ENRE.Numeracion              

WHERE DAPO.EMPR_Codigo = @par_Empr_Codigo          
AND DAPO.EAGP_Numero = @par_Numero              
END           
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_obtener_detalle_guia_preimpresa_seleccionados]             
(@par_Empr_Codigo Numeric             
,@par_Numero numeric       
,@par_Precinto_Inicial numeric      
,@par_Precinto_Final numeric      
)            
AS BEGIN            
          
SELECT              
DAPO.ID IDPrecinto,            
DAPO.EMPR_Codigo,            
DAPO.Numero_Preimpreso,          
ISNULL(ENRE.Numero_Documento,0) AS NumeroRemesa, 

DAPO.ENRE_Numero,    

OFIC.Nombre NombreOficina,            
OFOR.Nombre NombreOficinaOrigen,            
DAPO.Estado,            
DAPO.Anulado   
  
FROM Detalle_Asignacion_Guias_Preimpresas DAPO            
            
 
            
LEFT JOIN Oficinas OFIC on             
DAPO.EMPR_Codigo = OFIC.EMPR_Codigo            
AND DAPO.OFIC_Codigo = OFIC.Codigo            
            
LEFT JOIN Oficinas OFOR on             
DAPO.EMPR_Codigo = OFOR.EMPR_Codigo            
AND DAPO.OFIC_Codigo_Origen = OFOR.Codigo            
            
 LEFT JOIN Encabezado_Remesas ENRE ON         
DAPO.EMPR_Codigo = ENRE.EMPR_Codigo         
AND CAST(DAPO.Numero_Preimpreso AS VARCHAR) = ENRE.Numeracion                
        
WHERE DAPO.EMPR_Codigo = @par_Empr_Codigo            
AND DAPO.EAGP_Numero = @par_Numero   
AND DAPO.Numero_Preimpreso >= @par_Precinto_Inicial      
AND DAPO.Numero_Preimpreso <= @par_Precinto_Final   
 
END     
GO


