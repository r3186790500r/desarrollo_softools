﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Facturacion

Namespace Facturacion

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleFacturas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleFacturas
        Inherits RepositorioBase(Of DetalleFacturas)

        Public Overrides Function Consultar(filtro As DetalleFacturas) As IEnumerable(Of DetalleFacturas)
            Dim lista As New List(Of DetalleFacturas)
            Dim item As DetalleFacturas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENFA_Numero", filtro.NumeroEncabezado)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_remesa_facturas]")

                While resultado.Read
                    item = New DetalleFacturas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleFacturas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleFacturas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleFacturas) As DetalleFacturas
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleFacturas) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace