﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Entidades.Basico.Operacion

Namespace ServicioCliente

    ''' <summary>
    ''' Clase <see cref=" DetalleProgramacionOrdenServicios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleProgramacionOrdenServicios
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleProgramacionOrdenServicios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleProgramacionOrdenServicios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo") ''
            Codigo = Read(lector, "ID") ''
            Fecha = Read(lector, "Fecha") ''
            NumeroProgramacion = Read(lector, "EPOS_Numero") ''
            EstadoDetalleProgramacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_EDPO_Codigo")}
            OrdenServicio = New EncabezadoSolicitudOrdenServicios With {
                .Numero = Read(lector, "ESOS_Numero"),''
                .NumeroDocumento = Read(lector, "ESOS_Numero_Documento"),''
                .IDRegistroDetalle = Read(lector, "IDDespacho"),''
                 .NumeroDocumentoOrdenCargue = Read(lector, "ENOC_Numero_Documento"),
                 .NumeroOrdenCargue = Read(lector, "ENOC_Numero"),
                 .NumeroDocumentoRemesa = Read(lector, "ENRE_Numero_Documento"),
                 .NumeroRemesa = Read(lector, "ENRE_Numero"),
                 .NumeroDocumentoPlanilla = Read(lector, "ENPD_Numero_Documento"),
                 .NumeroPlanilla = Read(lector, "ENPD_Numero"),
                 .NumeroDocumentoManifiesto = Read(lector, "ENMC_Numero_Documento"),
                 .NumeroManifiesto = Read(lector, "ENMC_Numero"),
                 .Oficina = New Oficinas With {.Nombre = Read(lector, "Oficina"), .Codigo = Read(lector, "OFIC_Codigo")}
            }
            Cliente = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "Cliente")}
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Ruta")}
            LineaNegocio = New LineaNegocioTransporteCarga With {.Codigo = Read(lector, "LNTC_Codigo"), .Nombre = Read(lector, "Linea_Negocio")}
            TipoLineaNegocio = New TipoLineaNegocioCarga With {.Codigo = Read(lector, "TLNC_Codigo"), .Nombre = Read(lector, "Tipo_Linea_Negocio")}
            TipoTarifa = New TipoTarifaTransportes With {.Nombre = Read(lector, "Tipo_Tarifa"), .Codigo = Read(lector, "TTTC_Codigo"), .TarifaTransporte = New TarifaTransportes With {.Nombre = Read(lector, "Tarifa"), .Codigo = Read(lector, "TATC_Codigo")}}
            CantidadDespacho = Read(lector, "Cantidad_Despacho")
            FechaCargue = Read(lector, "Fecha_Cargue")
            FechaDescargue = Read(lector, "Fecha_Descargue")
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Placa"), .Placa = Read(lector, "Placa")}
            Conductor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Conductor")}
            SitioCargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Cargue"), .Nombre = Read(lector, "Sitio_Cargue")}
            Destinatario = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Destinatario"), .Nombre = Read(lector, "Destinatario")}
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "Producto")}
            FleteCliente = Read(lector, "Valor_Flete_Cliente")
            FleteTransportador = Read(lector, "Valor_Felte_Transportador")
            ValorCargueCliente = Read(lector, "Valor_Cargue_Cliente")
            ValorDescargueCliente = Read(lector, "Valor_Descargue_Cliente")
            ValorCargueTransportador = Read(lector, "Valor_Cargue_Transportador")
            ValorDescargueTransportador = Read(lector, "Valor_Descargue_Transportador")
            TipoDespacho = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TDOS_Codigo"), .Nombre = Read(lector, "Tipo_Despacho")}
            TipoVehiculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIVE_Codigo"), .Nombre = Read(lector, "Tipo_Vehiculo")}
            NumeroContenedor = Read(lector, "Numero_Contenedor")
            TotalRegistros = Read(lector, "TotalRegistros")
            CiudadDescargue = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Descargue"), .Nombre = Read(lector, "CiudadDescargue"), .NombreCompleto = Read(lector, "CiudadDescargue")}
            SitioDescargue = New SitiosTerceroCliente With {.Codigo = Read(lector, "SICD_Codigo_Descargue"), .Nombre = Read(lector, "SitioDescargue"), .SitioCliente = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Descargue"), .Nombre = Read(lector, "SitioDescargue")}}
            Cantidad = Read(lector, "Cantidad")
            Tarifa = New TarifaTransporteCarga With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "Tarifa")}
            PermisoInvias = Read(lector, "Permiso_Invias")
            Observaciones = Read(lector, "Observaciones")
            ValorDeclarado = Read(lector, "Valor_Declarado")
            ValorDeclaradoAutorizado = Read(lector, "Valor_Declarado_Autorizado")
        End Sub
        <JsonProperty>
        Public Property Editado As Short
        <JsonProperty>
        Public Property EditaValoresManejoObservaciones As Short
        <JsonProperty>
        Public Property ValorDeclarado As Double
        <JsonProperty>
        Public Property ValorDeclaradoAutorizado As Short
        <JsonProperty>
        Public Property NumeroProgramacion As Integer
        <JsonProperty>
        Public Property EstadoDetalleProgramacion As ValorCatalogos
        <JsonProperty>
        Public Property TipoVehiculo As ValorCatalogos
        <JsonProperty>
        Public Property TipoDespacho As ValorCatalogos
        <JsonProperty>
        Public Property FechaProgramacion As DateTime
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property FechaCargue As DateTime
        <JsonProperty>
        Public Property FechaDescargue As DateTime
        <JsonProperty>
        Public Property Cliente As Tercero
        <JsonProperty>
        Public Property Destinatario As Tercero
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property Conductor As Tercero
        <JsonProperty>
        Public Property CantidadDespacho As Double
        <JsonProperty>
        Public Property Despachado As Short
        <JsonProperty>
        Public Property OrdenServicio As EncabezadoSolicitudOrdenServicios
        <JsonProperty>
        Public Property FleteCliente As Double
        <JsonProperty>
        Public Property FleteTransportador As Double
        <JsonProperty>
        Public Property ValorCargueCliente As Double
        <JsonProperty>
        Public Property ValorDescargueCliente As Double
        <JsonProperty>
        Public Property ValorCargueTransportador As Double
        <JsonProperty>
        Public Property ValorDescargueTransportador As Double
        <JsonProperty>
        Public Property TipoTarifa As TipoTarifaTransportes
        <JsonProperty>
        Public Property NumeroContenedor As String
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property LineaNegocio As LineaNegocioTransporteCarga
        <JsonProperty>
        Public Property TipoLineaNegocio As TipoLineaNegocioCarga
        <JsonProperty>
        Public Property SitioCargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property Modifica As Short
        <JsonProperty>
        Public Property Tarifa As TarifaTransporteCarga
        <JsonProperty>
        Public Property CiudadDescargue As Ciudades
        <JsonProperty>
        Public Property SitioDescargue As SitiosTerceroCliente
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property PermisoInvias As String
    End Class
End Namespace
