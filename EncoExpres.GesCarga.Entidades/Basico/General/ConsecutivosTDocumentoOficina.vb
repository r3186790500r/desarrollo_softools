﻿Imports Newtonsoft.Json

Namespace Basico.General

    Public NotInheritable Class ConsecutivosTDocumentoOficina
        Inherits BaseBasico

        Sub New()
        End Sub

        Sub New(lector As IDataReader)

            Obtener = Read(lector, "Obtener")

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            TipoDocumento = New TipoDocumentos With {.Codigo = Read(lector, "TIDO_Codigo"), .Nombre = Read(lector, "TIDO_Nombre")}
            Consecutivo = Read(lector, "Consecutivo")
            ConsecutivoFin = Read(lector, "Consecutivo_Hasta")
            AvisoConRestantes = Read(lector, "Numero_Documentos_Faltantes_Aviso")


        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property TipoDocumento As TipoDocumentos
        <JsonProperty>
        Public Property Consecutivo As Double
        <JsonProperty>
        Public Property ConsecutivoFin As Double
        <JsonProperty>
        Public Property AvisoConRestantes As Double
    End Class

End Namespace
