﻿Imports Newtonsoft.Json
Namespace Despachos.Procesos
    <JsonObject>
    Public NotInheritable Class TerceroRNDC
        Sub New()
        End Sub
        <JsonProperty>
        Public Property NUMNITEMPRESATRANSPORTE As String
        <JsonProperty>
        Public Property CODTIPOIDTERCERO As String
        <JsonProperty>
        Public Property DIGITOCHEQUEO As String
        <JsonProperty>
        Public Property NUMIDTERCERO As String
        <JsonProperty>
        Public Property NOMIDTERCERO As String
        <JsonProperty>
        Public Property PRIMERAPELLIDOIDTERCERO As String
        <JsonProperty>
        Public Property SEGUNDOAPELLIDOIDTERCERO As String
        <JsonProperty>
        Public Property CODSEDETERCERO As String
        <JsonProperty>
        Public Property NOMSEDETERCERO As String
        <JsonProperty>
        Public Property NUMTELEFONOCONTACTO As String
        <JsonProperty>
        Public Property NUMCELULARPERSONA As String
        <JsonProperty>
        Public Property NOMENCLATURADIRECCION As String
        <JsonProperty>
        Public Property CODMUNICIPIORNDC As String
        <JsonProperty>
        Public Property CODCATEGORIALICENCIACONDUCCION As String
        <JsonProperty>
        Public Property NUMLICENCIACONDUCCION As String
        <JsonProperty>
        Public Property FECHAVENCIMIENTOLICENCIA As String
        <JsonProperty>
        Public Property Reportar_RNDC As Short
        <JsonProperty>
        Public Property Codigo As Integer
    End Class
End Namespace
