USE USE [ENCOEXPRES]
GO

/****** Object:  StoredProcedure [dbo].[gsp_consultar_remesa_paqueteria]    Script Date: 9/02/2022 1:01:16 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER      PROCEDURE [dbo].[gsp_consultar_remesa_paqueteria]      
(                                                                            
 @par_EMPR_Codigo SMALLINT,                                                                            
 @par_NumeroInicial numeric = NULL,                                                                            
 @par_NumeroFinal numeric = NULL,                                                                            
 @par_FechaInicial Date = NULL,                                                                            
 @par_FechaFinal Date = NULL,            
 @par_Linea_Negocio_Paqueteria NUMERIC = NULL,            
 @par_NumeroPlanillaInicial numeric = NULL,                                                                            
 @par_NumeroPlanillaFinal numeric = NULL,                                                                            
 @par_FechaPlanillaInicial Date = NULL,                                                                            
 @par_FechaPlanillaFinal Date = NULL,                                                                            
 @par_Numero numeric = NULL,                                                                            
 @par_Fecha date = NULL,                                                                            
 @par_Documento_Cliente varchar(30) = NULL,                                                                            
 @par_NombreCliente varchar (150) = NULL,                                                                            
 @par_TERC_Codigo_Cliente numeric = NULL,                                                                            
 @par_PRTR_Codigo numeric = NULL,                                                                            
 @par_VEHI_Codigo numeric= NULL,
 -- FP 2021-12-16 Agrega filtros en master guias
 @par_VEHI_Codigo_Alterno VARCHAR(20) = null,
 @par_TERC_Codigo_Conductor numeric = null,
 @par_ENFA_Numero_Documento numeric = NULL,                                                                            
  -- FP 2021-12-16 Agrega filtros en master guias
 @par_NombreRuta varchar (150) = NULL,                                                                            
 @par_Ciudad_Origen varchar (50) = NULL,                                                                            
 @par_Ciudad_Destino varchar (50) = NULL,                                                                            
 @par_RUTA_Codigo numeric= NULL,                                                                            
 @par_NombreDestinatario varchar (150) = NULL,                                                                            
 @par_NombreRemitente varchar(150) = NULL,                                                                            
 @par_CATA_TERP_Codigo numeric = NULL,                                                                            
 @par_CATA_TTRP_Codigo numeric= NULL,                                                                            
 @par_CATA_TDRP_Codigo numeric= NULL,                                                                            
 @par_CATA_TSRP_Codigo numeric= NULL,                                                                            
 @par_ENPD_Numero numeric = NULL,                                                                            
 @par_Anulado numeric= NULL,                                                                            
 @par_Estado numeric = NULL,                                                                            
 @par_Cumplido smallint = NULL,                                                                            
 @par_Planillado smallint = NULL,                                                                            
 @par_planilla_entrega INT = NULL,                                                                            
 @par_planilla_recoleccion INT = NULL,                                                                            
 @par_TIDO_Codigo INT = NULL,                                                                            
 @par_planilla_despachos NUMERIC = NULL,                  
 @par_Codigo_Ciudad_Planilla_Recolecciones NUMERIC = NULL,                                                                        
 @par_Codigo_Oficina_Planilla_Recolecciones NUMERIC = NULL,                                                        
 @par_NumeroPagina INT = NULL,                                                                
 @par_RegistrosPagina INT = NULL,                                                                
 @par_CATA_ESRP_Codigo INT = NULL,                 
 @par_OFIC_Codigo_Actual_Usuario INT = NULL,                         
 @par_OFIC_Codigo_Actual NUMERIC = NULL,                             
 @par_OFIC_Codigo INT = NULL,                                          
 @par_Reexpedicion INT = NULL,                                          
 @par_CIUD_Codigo_Destino INT = NULL,                                      
 @par_OFIC_Codigo_Origen NUMERIC = NULL,                                    
 /*Modificación AE  9/06/2020*/                                      
 @par_Numeracion varchar(50) = null,                                          
 /*Fin modificación*/                                      
 @par_Forma_Pago INT = null,                            
 @par_USUA_Codigo_Consultar NUMERIC = NULL                            
)                                                                            
AS                                                                          
BEGIN                                                                            
 IF @par_FechaInicial <> NULL BEGIN                                                                            
 SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                                            
 END                                                                            
                                         
 IF @par_FechaFinal <> NULL BEGIN                                                                            
 SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                                         
 END                                                                            
                                                                            
 SET NOCOUNT ON;                                                                                  
 DECLARE @CantidadRegistros INT                                                          
 SELECT                                                                            
 @CantidadRegistros = (SELECT DISTINCT                                                         
 COUNT(1)                                                                            
 FROM Remesas_Paqueteria ENRP                                                                            
                                                                            
 INNER JOIN Encabezado_Remesas AS ENRE                                                                      
 ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                                                            
 AND ENRP.ENRE_Numero = ENRE.Numero                                                                            
                                                                            
 INNER JOIN Oficinas AS OFIC ON                                                              
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                                              
 AND ENRE.OFIC_Codigo = OFIC.Codigo                                                              
                                                            
 LEFT JOIN Terceros AS CLIE                                                     
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                                            
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                            
       
 LEFT JOIN Terceros AS REMI                                                                            
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                          
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                            
                                                                            
 LEFT JOIN Terceros AS DEST                           
 ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                            
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                                          
                                                                        
 LEFT JOIN Producto_Transportados AS PRTR                                                               
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                                                            
 AND ENRE.PRTR_Codigo = PRTR.Codigo                  
                                                                            
 LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                                            
 ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                                  
 AND ENRE.ENPD_Numero = ENPD.Numero                                   
 AND ENPD.TIDO_Codigo IN(130,135)--Planilla Despacho Guias, Planilla Paqueteria                  
 AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                                                   
 AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                     
                   
 LEFT JOIN Vehiculos AS VEHI                  
 ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                  
 AND ENRE.VEHI_Codigo = VEHI.Codigo                  
                                                                     
 LEFT JOIN Ciudades CIOR                                                                   
 ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                     
 AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                            
                                                                            
 LEFT JOIN Ciudades CIDE                                                                            
 ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                                                            
 AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                                      
                                               
 LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                                      
 ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                                      
 AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                             
                                                             
 INNER JOIN Oficinas AS OFAC ON                                                              
 ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                                              
 AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo            
            
 LEFT JOIN Valor_Catalogos AS FOPR ON                                                    
 ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                                    
 AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo            
             
 LEFT JOIN Valor_Catalogos AS LNPA ON                                                    
 ENRP.EMPR_Codigo = LNPA.EMPR_Codigo                                                    
 AND ENRP.CATA_LNPA_Codigo = LNPA.Codigo            
                          
 LEFT JOIN Encabezado_Facturas ENFA ON                          
 ENRE.EMPR_Codigo = ENFA.EMPR_Codigo                        
 AND ENRE.ENFA_Numero = ENFA.Numero 
 
 LEFT JOIN Zona_Ciudades As ZOCI ON                                
 ENRP.EMPR_Codigo = ZOCI.EMPR_Codigo                                
 AND ENRP.ZOCI_Codigo_Entrega = ZOCI.Codigo                       
                      
 WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                       
 AND ENRE.Numero_Documento = ISNULL(@par_Numero,ENRE.Numero_Documento)                      
 AND ENRE.TIDO_Codigo = 110                                      
 AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                                                         
                                        
 AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                                            
 AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)            
 AND (ENRP.CATA_LNPA_Codigo = @par_Linea_Negocio_Paqueteria OR @par_Linea_Negocio_Paqueteria IS NULL)                      
 AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                      
 AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                      
                                                                          
 AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                                            
 AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)                                                                            
                                                                            
 AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                                            
 AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                                                              
 OR (@par_NombreCliente IS NULL))                                                                            
 AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)                                                                            
 AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                                                            
 AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                                            
 --AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                                                            
 --AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                                                            
                                                                            
 AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo) 
   	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                                                           
                                                                            
 AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                                            
 AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                                              
 AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                                                     
 AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)           
 AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado )                                                                            
 AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                                
 AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                                            
 AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                                            
 --AND ((ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero) AND @par_TIDO_Codigo = 130) OR (ENRE.ENPR_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPR_Numero) AND ISNULL(@par_TIDO_Codigo,205) = 205)  )            
 AND ENRE.ENPR_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPR_Numero)                    
 AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                                                        
 AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                     
 AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)                                      
 AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)                                    
                        
 AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                                                          
 AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                                                             
 AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)                                          
 AND (ENRP.CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino OR @par_CIUD_Codigo_Destino IS NULL)                                          
 AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)                                        
 /*Modificación AE  9/06/2020*/                                        
 AND ENRE.Numeracion = ISNULL(@par_Numeracion,ENRE.Numeracion)                      
 AND ENRE.CATA_FOPR_Codigo = ISNULL(@par_Forma_Pago, ENRE.CATA_FOPR_Codigo)                            
 AND (ENRE.USUA_Codigo_Crea = @par_USUA_Codigo_Consultar OR ENRE.USUA_Codigo_Modifica = @par_USUA_Codigo_Consultar OR @par_USUA_Codigo_Consultar IS NULL)                            
 /*Fin modificación*/    
 
  -- FP 2021-12-16 Agrega filtros en master guias
 AND VEHI.Codigo_Alterno = ISNULL(@par_VEHI_Codigo_Alterno, VEHI.Codigo_Alterno)
 AND ((ISNUMERIC(@par_ENFA_Numero_Documento)= 0 and (ENFA.Numero_Documento is null or ENFA.Numero_Documento = ISNULL(@par_ENFA_Numero_Documento,ENFA.Numero_Documento))) or (ISNUMERIC(@par_ENFA_Numero_Documento)> 0 and ENFA.Numero_Documento = @par_ENFA_Numero_Documento))
 AND ENRE.TERC_Codigo_Conductor = ISNULL(@par_TERC_Codigo_Conductor,ENRE.TERC_Codigo_Conductor)
  -- FP 2021-12-16 Agrega filtros en master guias

 );                                                                            
 WITH Pagina                                                                            
 AS                                                                            
 (                                          
 SELECT                                                                            
 ENRE.EMPR_Codigo                                    
 ,ENRE.Numero                                                                            
 ,ENRE.Numero_Documento                                       
 /*Modificación AE  9/06/2020*/                                       
 ,ENRE.Numeracion                                      
 /*Fin modificación*/                                             
 ,ENRE.Fecha            
 ,ENRP.CATA_LNPA_Codigo            
 ,LNPA.Campo1 AS CATA_LNPA_Nombre            
 ,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                                                            
 ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                                                            
 ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                                                        
                               
 ,ENRE.TERC_Codigo_Cliente                                  
 ,ENRE.TERC_Codigo_Remitente                                                    
 ,ENRE.CATA_FOPR_Codigo                                                         
 ,FOPR.Campo1 AS FormaPago                                                                            
 ,ENRE.RUTA_Codigo                                                                            
 ,ENRP.CIUD_Codigo_Origen                                                                            
 ,ENRP.CIUD_Codigo_Destino                                                                            
 ,CIOR.Nombre AS CiudadOrigen               
 ,CIDE.Nombre AS CiudadDestino                                                      
 ,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue                                                      
 ,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue                                                             
 ,ISNULL(ENRP.OFIC_Codigo_Origen, 0) AS OFIC_Codigo_Origen                                                            
 ,ISNULL(OFIC.Nombre, '') AS NombreOficinaOrigen                                                               
 ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                                                               
 ,ENRP.CATA_ESRP_Codigo                                                          
 ,ENRE.Observaciones                                                              
 --,(ENRE.Valor_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS  Valor_Flete_Cliente                                                                       
 --,(ENRE.Total_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS Total_Flete_Cliente                          
 ,ENRE.Valor_Flete_Cliente                        
 ,ENRE.Total_Flete_Cliente                        
 ,ENRE.Valor_Seguro_Cliente                                                                            
 ,ENRE.Cantidad_Cliente                                                                            
 ,ENRE.Peso_Cliente                                   
 ,ENRP.Peso_Volumetrico                                  
 ,ENRP.Peso_A_Cobrar                                                             
 ,ENRE.Estado                                                                            
 ,ENRE.Anulado                                                                            
 ,PRTR.Nombre AS NombreProducto                                                                            
 ,ENRE.Documento_Cliente                                                               
 ,ENRE.OFIC_Codigo                                                              
 ,OFIC.Nombre As NombreOficina                                                                           
 ,'' AS NombreRuta                                                              
 ,ENRE.Valor_Flete_Transportador                                                 
 ,ENRE.Valor_Reexpedicion                                          
 ,ENRP.Reexpedicion 
	/*Modificación FP 2021/12/12*/
	,ISNULL(ENRE.Nombre_Remitente, ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'')+' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'')) AS NombreRemitente  
	,ISNULL(ENRE.Nombre_Destinatario, ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'')+' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'')) AS NombreDestinatario  
	/*Fin Modificación*/ 
	,ISNULL(ENFA.Numero, 0) AS ENFA_Numero                       
 ,ISNULL(ENFA.Numero_Documento, 0) AS ENFA_Numero_Documento                  
 ,ISNULL(VEHI.Placa, '') AS VEHI_Placa                  
 ,ISNULL(VEHI.Codigo_Alterno, '') AS VEHI_CodigoAlterno            
 ,ISNULL(ELRG.Numero_Documento, 0) ELRG_NumeroDocumento      
 ,ISNULL(ENLG.Numero_Documento, 0) AS ELGU_NumeroDocumento         
 ,ISNULL(ZOCI.Nombre, '') As Zona      
 ,ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber
 --FP 2022 02 09 ENCO2021 115
 ,Numero_Etiquetas_Automaticas = (SELECT COUNT(*) FROM Detalle_Unidades_Remesa_Paqueteria DURP WHERE DURP.EMPR_Codigo = @par_EMPR_Codigo AND DURP.ENRE_Numero = ENRE.Numero)
--FP 2022 02 09 ENCO2021 115
 FROM Remesas_Paqueteria AS ENRP                                                                            
                                                                            
 INNER JOIN Encabezado_Remesas AS ENRE                                        
 ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                      
 AND ENRP.ENRE_Numero = ENRE.Numero                               
                                                                        
 INNER JOIN Oficinas AS OFIC ON                                                              
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                                                              
 AND ENRE.OFIC_Codigo = OFIC.Codigo                                                             
                                        
 LEFT JOIN Terceros AS CLIE                                             
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                                  
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                                            
                                                                            
 LEFT JOIN Terceros AS REMI                                                                            
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                                                            
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                                                            
       LEFT JOIN Terceros AS DEST                                                                            
 ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                                                            
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                                                            
                                                                            
 LEFT JOIN Producto_Transportados AS PRTR                   
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                              
 AND ENRE.PRTR_Codigo = PRTR.Codigo                                                                            
                                                                            
 LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                                                            
 ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                                  
 AND ENRE.ENPD_Numero = ENPD.Numero                                   
 AND ENPD.TIDO_Codigo IN(130,135)--Planilla Despacho Guias, Planilla Paqueteria                  
 AND ENPD.Fecha >= ISNULL(@par_FechaPlanillaInicial, ENPD.Fecha)                                                                   
 AND ENPD.Fecha <= ISNULL(@par_FechaPlanillaFinal, ENPD.Fecha)                     
                   
 LEFT JOIN Vehiculos AS VEHI                  
 ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                  
 AND ENRE.VEHI_Codigo = VEHI.Codigo  
                                                                
 LEFT JOIN Ciudades CIOR                                                                            
 ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                                                            
 AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                                                            
                                           
 LEFT JOIN Ciudades CIDE                                                                            
 ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                          
 AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                                                      
                                                         
 LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                                                      
 ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                                             
 AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                                                    
                                                                            
 INNER JOIN Oficinas AS OFAC ON                                                              
 ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                                                              
 AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                                      
                                                          
 LEFT JOIN Valor_Catalogos AS FOPR ON                                                    
 ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                                                    
 AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo            
             
 LEFT JOIN Valor_Catalogos AS LNPA ON                                                    
 ENRP.EMPR_Codigo = LNPA.EMPR_Codigo                                                    
 AND ENRP.CATA_LNPA_Codigo = LNPA.Codigo                      
                            
 LEFT JOIN Encabezado_Facturas ENFA ON             
 ENRE.EMPR_Codigo = ENFA.EMPR_Codigo                          
 AND ENRE.ENFA_Numero = ENFA.Numero 
 
 LEFT JOIN Detalle_Legalizacion_Recaudo_Guias As DLRG ON                                
 ENRE.EMPR_Codigo = DLRG.EMPR_Codigo                                
 AND ENRE.Numero = DLRG.ENRE_Numero                    
     
     
   LEFT JOIN Detalle_Legalizacion_Guias DLGU    
   on DLGU.EMPR_Codigo = ENRE.EMPR_Codigo    
   and DLGU.ENRE_Numero = ENRE.Numero    
    
 LEFT JOIN Encabezado_Legalizacion_Guias ENLG ON                  
 ENLG.EMPR_Codigo = DLGU.EMPR_Codigo                  
 AND ENLG.Numero = DLGU.ELGU_Numero       
                            
 LEFT JOIN Encabezado_Legalizacion_Recaudo_Guias As ELRG ON                                
 DLRG.EMPR_Codigo = ELRG.EMPR_Codigo                                
 AND DLRG.ELRG_Numero = ELRG.Numero      
     
 LEFT JOIN Zona_Ciudades As ZOCI ON                                
 ENRP.EMPR_Codigo = ZOCI.EMPR_Codigo                                
 AND ENRP.ZOCI_Codigo_Entrega = ZOCI.Codigo      
        
 WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo                      
 AND ENRE.Numero_Documento = ISNULL(@par_Numero,ENRE.Numero_Documento)                      
 AND ENRE.TIDO_Codigo = 110                      
 AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                                                  
                                                                      
 AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)                                                                            
 AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)            
 AND (ENRP.CATA_LNPA_Codigo = @par_Linea_Negocio_Paqueteria OR @par_Linea_Negocio_Paqueteria IS NULL)            
 AND (ENPD.Numero_Documento >= @par_NumeroPlanillaInicial OR @par_NumeroPlanillaInicial IS NULL)                      
 AND (ENPD.Numero_Documento <= @par_NumeroPlanillaFinal OR @par_NumeroPlanillaFinal IS NULL)                      
            
 AND ENRE.ENPE_Numero = ISNULL(@par_planilla_entrega, ENRE.ENPE_Numero)                                                                            
 AND ENRE.ENPR_Numero = ISNULL(@par_planilla_recoleccion, ENRE.ENPR_Numero)              
                                             
 AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                                                            
 AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                      
 OR (@par_NombreCliente IS NULL))                                                                            
 AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)          
 AND ENRE.PRTR_Codigo = ISNULL(PRTR_Codigo, @par_PRTR_Codigo)                                                                
 AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, ENRE.VEHI_Codigo)                                                                            
 --AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'                                                                            
 --AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                                                             
                                                                            
 AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo, ENRE.RUTA_Codigo)     
   	/*Modificación FP 2021/12/12*/
  AND ((ISNULL(ENRE.Nombre_Remitente,(CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                                          
  OR (@par_NombreRemitente IS NULL))                                                          
  AND ((ISNULL(ENRE.Nombre_Destinatario,(CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')))) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                                  
  OR (@par_NombreDestinatario IS NULL))
  	/*Fin Modificación*/                                                                           
                                                              
 AND ENRP.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo, ENRP.CATA_TERP_Codigo)                                                                            
 AND ENRP.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo, ENRP.CATA_TTRP_Codigo)                                                                            
 AND ENRP.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo, ENRP.CATA_TDRP_Codigo)                                                                 
 AND ENRP.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo, ENRP.CATA_TSRP_Codigo)                                                                            
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado )  
 AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                                            
 AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                                                            
 AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)                                                     
 --AND ((ENRE.ENPD_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPD_Numero) AND @par_TIDO_Codigo = 130) OR (ENRE.ENPR_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPR_Numero) AND ISNULL(@par_TIDO_Codigo,205) = 205)  )            
 AND ENRE.ENPR_Numero = ISNULL(@par_planilla_despachos, ENRE.ENPR_Numero)                    
 AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                                                
 AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)                                                                          
 AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual, ENRP.OFIC_Codigo_Actual)                                    
 AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo_Origen, ENRE.OFIC_Codigo)                                                                
                               
 AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_Codigo_Ciudad_Planilla_Recolecciones, ENRE.CIUD_Codigo_Remitente)                                                                          
 AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina_Planilla_Recolecciones, ENRE.OFIC_Codigo)                                                                         
 AND ENRP.CATA_ESRP_Codigo = ISNULL(@par_CATA_ESRP_Codigo,ENRP.CATA_ESRP_Codigo)                                          
 AND (ENRP.CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino OR @par_CIUD_Codigo_Destino IS NULL)                                          
 AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)                                          
 /*Modificación AE  9/06/2020*/                                        
 AND ENRE.Numeracion = ISNULL(@par_Numeracion,ENRE.Numeracion)                      
 /*Fin modificación*/                                      
 AND ENRE.CATA_FOPR_Codigo = ISNULL(@par_Forma_Pago, ENRE.CATA_FOPR_Codigo)                            
 AND (ENRE.USUA_Codigo_Crea = @par_USUA_Codigo_Consultar OR ENRE.USUA_Codigo_Modifica = @par_USUA_Codigo_Consultar OR @par_USUA_Codigo_Consultar IS NULL)                            
 
 -- FP 2021-12-16 Agrega filtros en master guias
 AND VEHI.Codigo_Alterno = ISNULL(@par_VEHI_Codigo_Alterno, VEHI.Codigo_Alterno)
AND ((ISNUMERIC(@par_ENFA_Numero_Documento)= 0 and (ENFA.Numero_Documento is null or ENFA.Numero_Documento = ISNULL(@par_ENFA_Numero_Documento,ENFA.Numero_Documento))) or (ISNUMERIC(@par_ENFA_Numero_Documento)> 0 and ENFA.Numero_Documento = @par_ENFA_Numero_Documento))
 AND ENRE.TERC_Codigo_Conductor = ISNULL(@par_TERC_Codigo_Conductor,ENRE.TERC_Codigo_Conductor)
  -- FP 2021-12-16 Agrega filtros en master guias

 )                                                                            
 SELECT DISTINCT                                      
 0 AS Obtener                                                                            
 ,EMPR_Codigo                                                              
 ,Numero                                                                            
 ,Numero_Documento                                      
 /*Modificación AE 9/06/2020*/                                      
 ,Numeracion                                      
 /*Fin Modificación*/                                                                          
 ,Fecha            
 ,CATA_LNPA_Codigo            
 ,CATA_LNPA_Nombre                                                                       
 ,NumeroDocumentoPlanilla                                                                            
 ,VEHI_Codigo                                                                            
 ,NombreCliente                                                                            
 ,TERC_Codigo_Cliente          
 ,NombreRemitente                              
 ,TERC_Codigo_Remitente                                                                   
 ,CATA_FOPR_Codigo                                                         
 ,FormaPago                                                    
 ,RUTA_Codigo                                                                        
 ,CIUD_Codigo_Origen                                                                            
 ,CIUD_Codigo_Destino                                   
 ,CiudadOrigen                                                                            
 ,CiudadDestino                                                  
 ,SICD_Codigo_Descargue                                                      
 ,SitioDescargue                                                           
 ,OFIC_Codigo_Origen                                                              
 ,NombreOficinaOrigen                                                                      
 ,Observaciones                                                                            
 ,Valor_Flete_Cliente                                                                            
 ,Total_Flete_Cliente                                                                            
 ,Valor_Seguro_Cliente                       
 ,Cantidad_Cliente                                                         
 ,Peso_Cliente                          
 ,Peso_Volumetrico                                  
 ,Peso_A_Cobrar                                                                            
 ,Estado                                                        
 ,Anulado                                                
 ,NombreProducto                                                                   
 ,Documento_Cliente                       
 ,OFIC_Codigo                                                              
 ,NombreOficina                                                     
 ,NombreRuta                                                                            
 ,NombreOficinaActual                                                        
 ,CATA_ESRP_Codigo                      
 ,Valor_Flete_Transportador                                                    
 ,NombreRemitente                                                 
 ,Valor_Reexpedicion                                   
 ,Reexpedicion                                              
 , NombreDestinatario                          
 ,ENFA_Numero                          
 ,ENFA_Numero_Documento                  
 ,VEHI_Placa                  
 ,VEHI_CodigoAlterno        
 ,ELRG_NumeroDocumento     
 ,ELGU_NumeroDocumento    
 ,Zona      
 ,@CantidadRegistros AS TotalRegistros                                                                            
 ,@par_NumeroPagina AS PaginaObtener                                                                            
 ,@par_RegistrosPagina AS RegistrosPagina  
 ,Numero_Etiquetas_Automaticas
 FROM Pagina                                                                            
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                                     
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                                            
 ORDER BY Numero ASC                                                                 
END 
GO


