﻿EncoExpresApp.factory('OficinasFactory', ['$http', function ($http) {

    var urlService = GetUrl('Oficinas');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarRegionesPaises = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRegionesPaises';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.ConsultarConsecutivosTDoc = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarConsecutivosPorOficina';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    }

    factory.InsertarConsecutivos = function (entidad) {
        request.methos = 'POST';
        request.url = urlService + '/InsertarConsecutivos';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    }

    return factory;
}]);