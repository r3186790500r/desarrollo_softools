﻿PPRINT 'gps_reporte_detalle_guia_planilla_entrega'
GO
DROP PROCEDURE gps_reporte_detalle_guia_planilla_entrega
GO
CREATE PROCEDURE gps_reporte_detalle_guia_planilla_entrega
(
	@par_EMPR_Codigo Numeric, 
	@par_ENPD_NUMERO numeric
)  
AS 
BEGIN  
	SELECT
	ENRE.Numero,
	ENRE.Numero_Documento AS NumeroRemesa,
	ENRE.Fecha,
	ISNULL(CLIE.Razon_Social,'')+' '+ISNULL(CLIE.Nombre,'') +' '+ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,'') AS NombreCliente,
	CIOR.NOMBRE CiudadOrigen,
	CIDE.NOMBRE CiudadDestino,
	isnull(PRTR.Nombre, '') AS Producto,
	isnull (ENRE.Peso_Cliente, 0) AS Peso,
	isnull(ENRE.Cantidad_Cliente, 0) AS Cantidad,
	ENRE.Observaciones
	
	from  Detalle_Planilla_Despachos DEPD  
  
	LEFT JOIN Encabezado_Remesas ENRE ON   
	DEPD.EMPR_Codigo = ENRE.EMPR_Codigo   
	AND DEPD.ENRE_Numero = ENRE.Numero  

	LEFT JOIN Producto_Transportados PRTR ON   
	PRTR.EMPR_Codigo = ENRE.EMPR_Codigo   
	AND PRTR.Codigo = ENRE.PRTR_Codigo  


	LEFT JOIN CIUDADES CIOR ON
	ENRE.EMPR_CODIGO = CIOR.EMPR_CODIGO
	AND ENRE.CIUD_Codigo_Remitente = CIOR.CODIGO

	LEFT JOIN CIUDADES CIDE ON
	ENRE.EMPR_CODIGO = CIDE.EMPR_CODIGO
	AND ENRE.CIUD_Codigo_Destinatario = CIDE.CODIGO

	LEFT JOIN Terceros CLIE ON
	ENRE.EMPR_CODIGO = CLIE.EMPR_CODIGO
	AND ENRE.TERC_Codigo_Cliente = CLIE.CODIGO
  
	WHERE DEPD.EMPR_Codigo = @par_EMPR_Codigo   
	and DEPD.ENPD_Numero = @par_ENPD_NUMERO   
   
END
GO