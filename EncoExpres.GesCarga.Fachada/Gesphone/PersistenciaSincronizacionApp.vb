﻿Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Repositorio.Gesphone
Imports EncoExpres.GesCarga.Repositorio.Paqueteria

Namespace Gesphone

    ''' <summary>
    ''' 
    ''' </summary>
    Public NotInheritable Class PersistenciaSincronizacionApp
        Implements IPersistenciaSincronizacionApp

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function Consultar(filtro As FiltroSyncApp) As IEnumerable(Of SyncDetalleTarifarioVenta) Implements IPersistenciaSincronizacionApp.Consultar
            Return New RepositorioSincronizacionApp().Consultar(filtro)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="codigoEmpresa"></param>
        ''' <returns></returns>
        Public Function ObtenerEncabezadoTarifario(codigoEmpresa As Integer, codigoTercero As Integer) As SyncEncabezadoTarifarioVenta Implements IPersistenciaSincronizacionApp.ObtenerEncabezadoTarifario
            Return New RepositorioSincronizacionApp().ObtenerEncabezadoTarifario(codigoEmpresa, codigoTercero)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ObtenerEncabezadoPlanilla(filtro As PlanillaGuias) As IEnumerable(Of PlanillaGuias) Implements IPersistenciaSincronizacionApp.ObtenerEncabezadoPlanilla
            Return New RepositorioPlanillaGuias().Consultar(filtro)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ObtenerRemesasPaqueteria(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria) Implements IPersistenciaSincronizacionApp.ObtenerRemesasPaqueteria
            Return New RepositorioRemesaPaqueteria().Consultar(filtro)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="codigoEmpresa"></param>
        ''' <param name="numeroPlanilla"></param>
        ''' <returns></returns>
        Public Function ObtenerDetallePlanilla(codigoEmpresa As Integer, numeroPlanilla As Integer) As IEnumerable(Of SyncDetallePlanilla) Implements IPersistenciaSincronizacionApp.ObtenerDetallePlanilla
            Return New RepositorioSincronizacionApp().ObtenerDetallePlanilla(codigoEmpresa, numeroPlanilla)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ConsultarTerceros(filtro As Terceros) As IEnumerable(Of Terceros) Implements IPersistenciaSincronizacionApp.ConsultarTerceros
            Return New RepositorioSincronizacionApp().ConsultarTerceros(filtro)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ConsultarCiudades(filtro As Ciudades) As IEnumerable(Of Ciudades) Implements IPersistenciaSincronizacionApp.ConsultarCiudades
            Return New RepositorioSincronizacionApp().ConsultarCiudades(filtro)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ConsultarSitiosCargueDescargue(filtro As SitiosCargueDescargue) As IEnumerable(Of SitiosCargueDescargue) Implements IPersistenciaSincronizacionApp.ConsultarSitiosCargueDescargue
            Return New RepositorioSincronizacionApp().ConsultarSitiosCargueDescargue(filtro)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Function ConsultarProductosTransportados(filtro As ProductoTransportados) As IEnumerable(Of ProductoTransportados) Implements IPersistenciaSincronizacionApp.ConsultarProductosTransportados
            Return New Repositorio.Basico.General.RepositorioProductoTransportados().Consultar(filtro)
        End Function

        Public Function ConsultarGestionDocumentosTercero(CodiEmpresa As Short, CodTercero As Long) As IEnumerable(Of GestionDocumentoCliente) Implements IPersistenciaSincronizacionApp.ConsultarGestionDocumentosTercero
            Return New RepositorioSincronizacionApp().ConsultarGestionDocumentosTercero(CodiEmpresa, CodTercero)
        End Function

        Public Function ConsultarDetalleGestionDocumentos(CodiEmpresa As Short, CodRemesa As Integer) As IEnumerable(Of GestionDocumentosRemesa) Implements IPersistenciaSincronizacionApp.ConsultarDetalleGestionDocumentos
            Return New RepositorioRemesaPaqueteria().ConsultarDetalleGestionDocumentos(CodiEmpresa, CodRemesa)
        End Function

        Public Function InsertarDetalleEtiquetasPreimpresas(registro As IEnumerable(Of SyncEtiquetaPreimpresa)) As Boolean Implements IPersistenciaSincronizacionApp.InsertarDetalleEtiquetasPreimpresas
            Return New RepositorioSincronizacionApp().InsertarDetalleEtiquetasPreimpresas(registro)
        End Function

        Public Function InsertarDetalleIntentoEntregaGuia(registro As DetalleIntentoEntregaGuia) As Long Implements IPersistenciaSincronizacionApp.InsertarDetalleIntentoEntregaGuia
            Return New RepositorioSincronizacionApp().InsertarIntentoEntrega(registro)
        End Function

        Public Function ConsultarIntentosEntregaGuia(codigoEmpresa As Short, numeroRemesa As Long) As IEnumerable(Of DetalleIntentoEntregaGuia) Implements IPersistenciaSincronizacionApp.ConsultarIntentosEntregaGuia
            Return New RepositorioSincronizacionApp().ConsultarIntentosEntregaGuia(codigoEmpresa, numeroRemesa)
        End Function

        Public Function ConsultarImagenesRemesaPaqueteria(codigoEmpresa As Short, numeroRemesa As Long) As IEnumerable(Of SyncDetallePlanillaImagenes) Implements IPersistenciaSincronizacionApp.ConsultarImagenesRemesaPaqueteria
            Return New RepositorioSincronizacionApp().ConsultarImagenesRemesaPaqueteria(codigoEmpresa, numeroRemesa)
        End Function


        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="codigoEmpresa"></param>
        ''' <param name="numeroRecoleccion"></param>
        ''' <param name="codigoEstado"></param>
        ''' <returns></returns>
        Public Function ModificarEstadoRecoleccion(codigoEmpresa As Integer, numeroRecoleccion As Integer, codigoEstado As Integer) As Long Implements IPersistenciaSincronizacionApp.ModificarEstadoRecoleccion
            Return New RepositorioSincronizacionApp().ModificarEstadoRecoleccion(codigoEmpresa, numeroRecoleccion, codigoEstado)
        End Function

        Public Function ConsultarFormasPagoTercero(CodiEmpresa As Short, CodTercero As Long) As IEnumerable(Of FormasPago) Implements IPersistenciaSincronizacionApp.ConsultarFormasPagoTercero
            Return New RepositorioSincronizacionApp().ConsultarFormasPagoTercero(CodiEmpresa, CodTercero)
        End Function

        Public Function ConsultarDetalleAxiliares(codigoEmpresa As Integer, numeroPlanilla As Long) As IEnumerable(Of DetalleAuxiliaresPlanillas) Implements IPersistenciaSincronizacionApp.ConsultarDetalleAxiliares
            Return New RepositorioSincronizacionApp().ConsultarDetalleAxiliares(codigoEmpresa, numeroPlanilla)
        End Function

        Public Function ConsultarDetalleAsignacionEtiquetasPreimpresas(codigoEmpresa As Short, codigoOficina As Short, codigoResponsable As Long) As IEnumerable(Of SyncAsignacionEtiquetas) Implements IPersistenciaSincronizacionApp.ConsultarDetalleAsignacionEtiquetasPreimpresas
            Return New RepositorioSincronizacionApp().ConsultarDetalleAsignacionEtiquetas(codigoEmpresa, codigoOficina, codigoResponsable)
        End Function

        Public Function LiberarEtiquetas(entidad As EtiquetaPreimpresa) As Boolean Implements IPersistenciaSincronizacionApp.LiberarEtiquetas
            Return New RepositorioSincronizacionApp().LiberarAsignacionEtiqueta(entidad)
        End Function

        Public Function GuardarEntrega(Entidad As Remesas) As Long Implements IPersistenciaSincronizacionApp.GuardarEntrega
            Return New RepositorioSincronizacionApp().GuardarEntrega(Entidad)
        End Function

    End Class

End Namespace
