﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente
Imports EncoExpres.GesCarga.Negocio.Basico.ServicioCliente

''' <summary>
''' Controlador <see cref="TipoTarifaTransporteCargaController"/>
''' </summary>
<Authorize>
Public Class TipoTarifaTransporteCargaController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TipoTarifaTransporteCarga) As Respuesta(Of IEnumerable(Of TipoTarifaTransporteCarga))
        Return New LogicaTipoTarifaTransporteCarga(CapaPersistenciaTipoTarifaTransporteCarga).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TipoTarifaTransporteCarga) As Respuesta(Of TipoTarifaTransporteCarga)
        Return New LogicaTipoTarifaTransporteCarga(CapaPersistenciaTipoTarifaTransporteCarga).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TipoTarifaTransporteCarga) As Respuesta(Of Long)
        Return New LogicaTipoTarifaTransporteCarga(CapaPersistenciaTipoTarifaTransporteCarga).Guardar(entidad)
    End Function
End Class
