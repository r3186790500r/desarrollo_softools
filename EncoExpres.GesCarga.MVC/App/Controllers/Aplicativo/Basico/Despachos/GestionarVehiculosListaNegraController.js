﻿EncoExpresApp.controller('GestionarVehiculosListaNegraCtrl', ['$scope', '$timeout', '$routeParams', 'blockUI', 'blockUIConfig', '$linq', 'VehiculosFactory', 'VehiculosListaNegraFactory',
    function ($scope, $timeout, $routeParams, blockUI, blockUIConfig, $linq, VehiculosFactory, VehiculosListaNegraFactory) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Vehículos Lista Negra' }, { Nombre: 'Gestionar' }];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_VEHICULOS_LISTA_NEGRA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.NumeroPagina = 1;
        $scope.RegistrosporPagina = 10;

        $scope.MensajesError = [];
        $scope.DeshabilitarActualizar = false;
        
         
        $scope.ListaEstados = [
             
            { Nombre: 'RECHAZADO', Codigo: 1 },
            { Nombre: 'HABILITADO', Codigo: 0 }
        ];

        $scope.ListaVehiculos = [];

        $scope.Modelo = {
            Vehiculo: {
                Placa : '',
                Codigo: '',
                CodigoListaNegra: ''
            },
            Causa: '',
            OrganismoTransito: '',
            Estado: ''

        }
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo == 1');


        if ($routeParams.Codigo > CERO) {
            $scope.Modelo.Vehiculo.Codigo = $routeParams.Codigo;

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Vehiculo.Codigo
            }
            VehiculosFactory.ConsultarListaNegra(filtros)
                .then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo ==' + response.data.Datos[0].EstadoListaNegra);
                            $scope.Modelo.Causa = response.data.Datos[0].Causa;
                            $scope.Modelo.OrganismoTransito = response.data.Datos[0].OrganismoTransito;
                            $scope.Modelo.Vehiculo.Placa = response.data.Datos[0].Placa;
                            $scope.ListaVehiculos = $scope.AutocompleteVehiculos($scope.Modelo.Vehiculo.Placa)
                            if ($scope.ListaVehiculos.length > 0) $scope.Modelo.Vehiculo = $scope.ListaVehiculos[0]
                            $scope.Modelo.Vehiculo.CodigoListaNegra = response.data.Datos[0].CodigoListaNegra;
                        }
                    }
                });

            $scope.DeshabilitarActualizar = true;
            
        }

        //----------Autocomplete
        //----Vehiculo
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Estado: {Codigo: -1},
                        Sync: true
                    });
                    $scope.ListaVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListaVehiculos);
                }
            }
            return $scope.ListaVehiculos;
        };
        //----Vehiculo
        //----------Autocomplete

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarVehiculosListaNegra/' + $scope.Modelo.Vehiculo.Codigo;
        };
        
        $scope.ValidarPlaca = function () {
            if ($scope.Modelo.Vehiculo != null && $scope.Modelo.Vehiculo != '' && $scope.Modelo.Vehiculo != undefined) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Placa: $scope.Modelo.Vehiculo.Placa
                }

                VehiculosFactory.ConsultarListaNegra(filtros)
                    .then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                console.log(response.data.Datos)
                                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo ==' + response.data.Datos[0].EstadoListaNegra);
                                $scope.Modelo.Causa = response.data.Datos[0].Causa;
                                $scope.Modelo.OrganismoTransito = response.data.Datos[0].OrganismoTransito;
                                $scope.Modelo.Vehiculo.CodigoListaNegra = response.data.Datos[0].CodigoListaNegra;

                            }
                        }
                    });
            }
        }


        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                
                var datos = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Modelo.Vehiculo.CodigoListaNegra,
                    //Codigo: $scope.Modelo.Vehiculo.Codigo,
                    Placa: $scope.Modelo.Vehiculo.Placa,
                    Causa: $scope.Modelo.Causa,
                    OrganismoTransito: $scope.Modelo.OrganismoTransito,
                    EstadoListaNegra:  $scope.Modelo.Estado.Codigo ,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }
               
               VehiculosListaNegraFactory.Guardar(datos)
                    .then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > CERO) {
                                if ($scope.Modelo.Vehiculo.CodigoListaNegra == CERO || $scope.Modelo.Vehiculo.CodigoListaNegra == "" || $scope.Modelo.Vehiculo.CodigoListaNegra == undefined) {
                                   
                                    ShowSuccess('Se guardó el vehículo '+datos.Placa+' en la lista negra');
                                    
                                } else {
                                    ShowSuccess('Se modificó el vehículo '+datos.Placa+' en la lista negra');
                                }
                                location.href = '#!ConsultarVehiculosListaNegra/' + response.data.Datos;
                            } else {
                                ShowError('no hay datos de retorno');
                            }
                        } else {
                            ShowError('Error al guardar. Por favor vuelva a intentar');
                        }
                    }, function (response) {

                        ShowError(response.statusText);

                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Vehiculo.Placa == undefined || $scope.Modelo.Vehiculo.Placa == null || $scope.Modelo.Vehiculo.Placa == '') {
                $scope.MensajesError.push('Debe ingresar un vehículo');
                continuar = false;
            }
            if ($scope.Modelo.Causa == undefined || $scope.Modelo.Causa == null || $scope.Modelo.Causa == '') {
                $scope.MensajesError.push('Debe ingresar una causa');
                continuar = false;
            }

            if ($scope.Modelo.OrganismoTransito == undefined || $scope.Modelo.OrganismoTransito == null || $scope.Modelo.OrganismoTransito == '') {
                $scope.MensajesError.push('Debe ingresar un Organismo de Tránsito');
                continuar = false;
            }
            return continuar;
        }

        /*---------------------------------------------------------------------------------------------------Máscaras-----------------------------------------------------------------------------*/
        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }
    }

   
]);