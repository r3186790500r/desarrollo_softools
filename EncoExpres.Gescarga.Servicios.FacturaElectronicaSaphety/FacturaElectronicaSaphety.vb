﻿Imports System.Configuration
Imports System.Diagnostics.Eventing.Reader
Imports System.Net
Imports System.Text
Imports System.Threading
Imports System.Xml
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class FacturaElectronicaSaphety

#Region "Variables"
    ' Variables
    Private strSQL As String
    Private strPlano As String
    Private lonNumeRegi As Long
    Private objGeneral As clsGeneral

    Private ArrEmpresas() As String
    Private ArrServices() As String
    Private ArrUsuarios() As String
    Private ArrClaves() As String
    Private TiempoSuspencionServicio As Long
    Private strRutaArchivoLog As String

    Dim CodEmpr As Integer
    Dim NumFact As Integer
    Dim IdDocu As String

    Private thrProceso As Thread

    'Token
    Public Token As List(Of TokenSaphety)
    Public Usuario As String
    Public Clave As String
    Public OperadorVirtual As String
    Public Ambiente As String

    'Estados
    Public DocumentStatus As String 'Adquiriente
    Public CommunicationStatus As String 'DIAN
    Public MainEmailNotificationStatus As String ' Correo

    ' Hilos Procesos
    Private HilProcesoEstadosDocumento As Thread
#End Region

#Region "Constantes"
    Dim REPLACE_VIRTUAL_OPERATOR As String = "{virtualOperator}"
    REM: ENDPOINTS SAPHETY
    Dim GEN_URL_SAPHETY As String = "https://api-factura-electronica-co-qa.saphety.com/"
    Dim GEN_URL_SAPHETY_PRODUCCION As String = "https://api-factura-electronica-co.saphety.com/"
    Dim API_GET_TOKEN As String = "v2/auth/gettoken"
    Dim API_GET_ESTADOS As String = "v2/" & REPLACE_VIRTUAL_OPERATOR & "/outbounddocuments/"
    REM: ENDPOINTS SOFTTOOLS
    Dim API_SOFTOOLS_TOKEN As String = "SofttoolsToken"
    Dim API_SOFTOOLS_REPORTE_DOCUMENTOS As String = "api/v1/ReporteFacturaElectronica/ReportarDocumentosSaphetyServicioWindows"
#End Region

#Region "Declaracion Enums"
    Public Enum PROVEEDOR_FACTURA_ELECTRONICA
        SAPHETY = 21101
    End Enum
    Public Enum AMBIENTE_FACTURA_ELECTRONICA
        PRUEBAS = 20701
        HABILITACION = 20702
        PRODUCCION = 20703
    End Enum

    Public Enum TIPO_ESTADO_FACTURA_ELECTRONICA
        DIAN = 21401
        ADQUIRIENTE = 21402
        CORREO = 21403
    End Enum
#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            Me.objGeneral = New clsGeneral
            Call Cargar_Datos_App_Config()
            Me.objGeneral.Guardar_Mensaje_Log("inició servicio factura electronica saphety")
            Me.thrProceso = New Thread(AddressOf Iniciar_Proceso)
            Me.thrProceso.Start()
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error OnStart: " & ex.Message)
        End Try

    End Sub

    Protected Overrides Sub OnStop()
        Try
            Me.thrProceso.Abort()
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error OnStop: " & ex.Message)
        End Try
    End Sub

    Public Sub New()
        Try
            InitializeComponent()
            'Me.objGeneral = New clsGeneral
            'Call Cargar_Datos_App_Config()
            'Me.objGeneral.Guardar_Mensaje_Log("Inició servicio Factura Electronica Saphety")
            'Me.thrProceso = New Thread(AddressOf Iniciar_Proceso)
            'Me.thrProceso.Start()
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error swn: " & ex.Message)
        End Try

    End Sub

    Private Sub Iniciar_Proceso()
        Try
            While (True)
                Procesar_Empresas()
                Thread.Sleep(Val(Me.TiempoSuspencionServicio))
            End While
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error funcion Iniciar_Proceso: " & ex.Message)
        End Try
    End Sub

    Private Sub Procesar_Empresas()
        For i = 0 To ArrEmpresas.Length - 1
            Me.objGeneral.Guardar_Mensaje_Log("Inicio Proceso Empresa " & ArrEmpresas(i))
            Dim resultToken As TokenSofttools = GET_TOKEN_SERVICES(ArrEmpresas(i), ArrUsuarios(i), ArrClaves(i), ArrServices(i))
            If Not IsNothing(resultToken) Then
                Dim Entidad = New Dictionary(Of String, Object)
                Entidad.Add("CodigoEmpresa", ArrEmpresas(i))
                Dim urlService As String = ArrServices(i) & API_SOFTOOLS_REPORTE_DOCUMENTOS
                Dim result As ResultadoReporte = RequestPost(urlService, Entidad, resultToken)
                Me.objGeneral.Guardar_Mensaje_Log("Documentos Reportados: Factura: " & result.TotalFacturas.ToString() & " Notas Debito: " & result.TotalNotasDebito.ToString() & " Notas Credito: " & result.TotalNotasDebito.ToString())
                Me.objGeneral.Guardar_Mensaje_Log("Proceso Terminado Empresa " & ArrEmpresas(i))
            Else
                Me.objGeneral.Guardar_Mensaje_Log("No se pudo acceder a la empresa " & ArrEmpresas(i))
            End If
        Next
    End Sub

    Private Function GET_TOKEN_SERVICES(Empresa As String, Usuario As String, Clave As String, UrlService As String) As TokenSofttools
        Dim strAcceso As String = "grant_type=password&username=" & Usuario & "|" & Empresa & "&password=" & Clave
        Dim urlToken As String = UrlService & API_SOFTOOLS_TOKEN
        Dim objResult As TokenSofttools
        Dim webClient As New WebClient()
        Try
            'Headers
            webClient.Headers("content-type") = "application/x-www-form-urlencoded"
            Dim resString As String = webClient.UploadString(urlToken, strAcceso)
            objResult = JsonConvert.DeserializeObject(Of TokenSofttools)(resString)
            webClient.Dispose()
        Catch ex As Exception
            objResult = Nothing
            Me.objGeneral.Guardar_Mensaje_Log("Error funcion GET_TOKEN_SERVICES: " & ex.Message)
        End Try
        Return objResult
    End Function

    Private Function RequestPost(ByVal URL As String, ByVal JSonData As Dictionary(Of String, Object), ByVal Token As TokenSofttools) As ResultadoReporte
        'Se crea el Objeto que realizará la solicitud REST
        Dim webClient As New WebClient()
        Try
            'Headers
            webClient.Headers("content-type") = "application/json"
            webClient.Headers("Accept") = "application / json"
            webClient.Headers("Authorization") = Token.token_type & " " & Token.access_token
            'Se convierte el dictionary que representa los datos en un String
            Dim strJSON As String = JsonConvert.SerializeObject(JSonData, Newtonsoft.Json.Formatting.Indented)
            'El String con el JSON se convierte a un arreglo de Bytes
            Dim reqString() As Byte = Encoding.Default.GetBytes(strJSON)
            'Se realiza la solicitud por el verbo POST enviando el JSON representado en Bytes
            Dim resByte As Byte() = webClient.UploadData(URL, "post", reqString)
            'Se convierte la respuesta que es un arreglo de Bytes a un String
            Dim resString As String = Encoding.Default.GetString(resByte)
            'Se libera el Objeto de la conexión
            webClient.Dispose()
            'Se retorna la respuestaString en formato JObject
            Return JsonConvert.DeserializeObject(Of ResultadoReporte)(resString)
        Catch ex As Exception
            'Se imprime en consola el error generado
            Me.objGeneral.Guardar_Mensaje_Log("Error funcion RequestPost: " & ex.Message)
            Return Nothing
        End Try
        'Se envía una respuesta vacia en caso de no pederse realizar la solicitud
        Return Nothing

    End Function

#Region "Funciones Generales"
    Private Sub Cargar_Datos_App_Config()
        Try
            ' Generales
            Dim Empresas = ConfigurationSettings.AppSettings.Get("Empresa")
            ArrEmpresas = Split(Empresas, ",")
            Dim Services = ConfigurationSettings.AppSettings.Get("Service")
            ArrServices = Split(Services, ",")
            Dim Usuarios = ConfigurationSettings.AppSettings.Get("Usuario")
            ArrUsuarios = Split(Usuarios, ",")
            Dim Claves = ConfigurationSettings.AppSettings.Get("Clave")
            ArrClaves = Split(Claves, ",")
            TiempoSuspencionServicio = ConfigurationSettings.AppSettings.Get("TiempoSuspencionServicio")
            Me.objGeneral.RutaArchivoLog = ConfigurationSettings.AppSettings("RutaArchivoLog").ToString()
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Error Cargar_Datos_App_Config: " & ex.Message)
        End Try
    End Sub
#End Region

#Region "Propiedades"
    Public Property Proceso() As Thread
        Get
            Return thrProceso
        End Get
        Set(ByVal value As Thread)
            thrProceso = value
        End Set
    End Property
#End Region
End Class
