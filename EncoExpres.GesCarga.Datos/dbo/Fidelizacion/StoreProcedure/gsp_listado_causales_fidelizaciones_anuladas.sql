﻿Print 'gsp_listado_causales_fidelizaciones_anuladas'
GO
DROP PROCEDURE gsp_listado_causales_fidelizaciones_anuladas
GO
CREATE PROCEDURE gsp_listado_causales_fidelizaciones_anuladas (

@par_EMPR_Codigo	SMALLINT,
@par_VEHI_Codigo	NUMERIC,
@par_Fecha_Inicial	DATETIME,
@par_Fecha_Final	DATETIME

)
AS
BEGIN
	SET @par_Fecha_Inicial	= CONVERT(DATE , @par_Fecha_Inicial)
	SET @par_Fecha_Final = DATEADD(HOUR , 23 , @par_Fecha_Final)
	SET @par_Fecha_Final = DATEADD(MINUTE , 59 , @par_Fecha_Final)
	SET @par_Fecha_Final = DATEADD(SECOND , 59 , @par_Fecha_Final)

	SELECT DCPV.EMPR_Codigo,
	DCPV.VEHI_Codigo, 
	VEHI.Codigo_Alterno As NumeroVehiculo,
	VEHI.Placa,
	ISNULL(COND.Numero_Identificacion,'') As IdenConductor,
	ISNULL(COND.Razon_Social,'') + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,
	ISNULL(TENE.Numero_Identificacion,'') As IdenTenedor,
	ISNULL(TENE.Razon_Social,'') + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') As NombreTenedor,
	DCPV.CATA_CPPV_Codigo, 
	CFAN.Nombre As CausaAnulacion,
	DCPV.Puntos,
	DCPV.Fecha_Inicio, 
	DCPV.Fecha_Vence, 
	DCPV.Fecha_Crea, 
	DCPV.Fecha_Anula, 
	DCPV.Causa_Anula,
	USAN.Nombre As UsuarioAnula,
	EMPR.Nombre_Razon_Social

	FROM Detalle_Causales_Plan_Puntos_Vehiculos DCPV,
	V_Causal_Fidelizacion_Anuladas CFAN, Vehiculos VEHI, Empresas EMPR,
	Terceros COND, Terceros TENE, Usuarios USAN
	WHERE DCPV.EMPR_Codigo = CFAN.EMPR_Codigo
	AND DCPV.CATA_CPPV_Codigo = CFAN.Codigo
	AND DCPV.EMPR_Codigo = VEHI.EMPR_Codigo
	AND DCPV.VEHI_Codigo = VEHI.Codigo
	AND VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo
	AND VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = TENE.Codigo
	AND DCPV.EMPR_Codigo = USAN.EMPR_Codigo
	AND DCPV.USUA_Codigo_Anula = USAN.Codigo

	AND DCPV.Anulado = 1
	AND DCPV.EMPR_Codigo = EMPR.Codigo

	AND DCPV.EMPR_Codigo = @par_EMPR_Codigo
	AND DCPV.VEHI_Codigo = ISNULL(@par_VEHI_Codigo, DCPV.VEHI_Codigo)
	AND DCPV.Fecha_Anula >= ISNULL(@par_Fecha_Inicial,DCPV.Fecha_Anula)
	AND DCPV.Fecha_Anula <= ISNULL(@par_Fecha_Final,DCPV.Fecha_Anula)

	--AND DCPV.Fecha_Inicio <= ISNULL(@par_Fecha_Inicial,DCPV.Fecha_Inicio)
	--AND DCPV.Fecha_Vence >= ISNULL(@par_Fecha_Inicial,DCPV.Fecha_Vence)
	--AND DCPV.Fecha_Inicio <= ISNULL(@par_Fecha_Final,DCPV.Fecha_Inicio)
	--AND DCPV.Fecha_Vence >= ISNULL(@par_Fecha_Final,DCPV.Fecha_Vence)

END
GO