﻿PRINT 'gsp_insertar_items_secciones_formularios_inspecciones'
GO
DROP PROCEDURE gsp_insertar_items_secciones_formularios_inspecciones
GO
CREATE PROCEDURE gsp_insertar_items_secciones_formularios_inspecciones 
(  
@par_EMPR_Codigo SMALLINT,  
@par_ENFI_Codigo NUMERIC,  
@par_Nombre VARCHAR (50),  
@par_Orden_Formulario SMALLINT,  
@par_SFIN_Codigo NUMERIC,  
@par_CATA_TCFI_Codigo NUMERIC,  
@par_Relevante SMALLINT,  
@par_Tamaño NUMERIC,  
@par_Estado SMALLINT,  
@par_USUA_Codigo_Crea SMALLINT  
)  
AS  
BEGIN  
INSERT INTO Detalle_Items_Formularios_Inspecciones	  
(  
EMPR_Codigo,  
ENFI_Codigo,  
Nombre,  
Orden_Formulario,  
SFIN_Codigo,  
CATA_TCFI_Codigo,  
Relevante,  
Tamaño,  
Estado,  
Fecha_Crea,  
USUA_Codigo_Crea  
)  
VALUES   
(  
@par_EMPR_Codigo,  
@par_ENFI_Codigo,  
@par_Nombre,  
@par_Orden_Formulario,  
@par_SFIN_Codigo,  
@par_CATA_TCFI_Codigo,  
@par_Relevante,  
@par_Tamaño,  
@par_Estado,  
GETDATE(),  
@par_USUA_Codigo_Crea  
)  
  
SELECT @@IDENTITY AS Codigo  
  
END  
GO