﻿PRINT 'gsp_consultar_cuenta_bancaria_oficinas'
GO
DROP PROCEDURE gsp_consultar_cuenta_bancaria_oficinas
GO  
CREATE PROCEDURE gsp_consultar_cuenta_bancaria_oficinas( 
  @par_EMPR_Codigo SMALLINT,    
  @par_OFIC_Codigo SMALLINT    
  )    
AS    
BEGIN      
    SELECT     
 CBOF.EMPR_Codigo,    
 CBOF.CUBA_Codigo,    
 CBOF.OFIC_Codigo,    
 OFIC.Nombre AS NombreOficina,    
 CUBA.Numero_Cuenta AS NumeroCuenta,    
 CUBA.Nombre AS Nombre_Cuenta    
    FROM Cuenta_Bancaria_Oficinas  CBOF,    
 Oficinas OFIC,    
 Cuenta_Bancarias CUBA    
    WHERE CBOF.EMPR_Codigo = @par_EMPR_Codigo     
    AND CBOF.OFIC_Codigo = @par_OFIC_Codigo    
    
 AND CBOF.EMPR_Codigo = OFIC.EMPR_Codigo  
 AND CBOF.OFIC_Codigo = OFIC.Codigo    
    
 AND CBOF.EMPR_Codigo = CUBA.EMPR_Codigo  
 AND CBOF.CUBA_Codigo = CUBA.Codigo    
END   
GO 