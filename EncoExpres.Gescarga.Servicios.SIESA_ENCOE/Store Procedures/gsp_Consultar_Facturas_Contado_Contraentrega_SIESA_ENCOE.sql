CREATE PROCEDURE [dbo].[gsp_Consultar_Facturas_Contado_Contraentrega_SIESA_ENCOE]  
(  
 @par_EMPR_Codigo smallint  
)  
AS  
BEGIN  
 SELECT TOP 10 ENFA.Numero, 
 ENFA.Numero_Documento, 
 ENFA.Fecha, 
 ENFA.TERC_Cliente, 
 ENFA.TERC_Facturar, 
 ENFA.OFIC_Factura, 
 ENFA.Valor_Factura, 
 ENFA.CATA_FPVE_Codigo,  
 TERC.Numero_Identificacion, 
 TERC.Nombre, 
 TERC.Apellido1, 
 TERC.Apellido2, 
 TERC.Representante_Legal  
    
 FROM Encabezado_Facturas AS ENFA
 
 INNER JOIN Terceros  AS TERC
 ON ENFA.EMPR_Codigo = TERC.EMPR_Codigo  
 AND ENFA.TERC_Cliente = TERC.Codigo  
 
 WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo  
 AND ISNULL(ENFA.Interfaz_Contable_Factura, 0) = 0  
 AND ISNULL(ENFA.Intentos_Interfase_Contable, 0) < 3  
 AND ENFA.CATA_FPVE_Codigo IN (4902, 4903) -- Contado, Contraentrega  
 AND ENFA.Estado = 1  
 AND ENFA.Anulado = 0  
 ORDER BY ENFA.Fecha DESC  
END  