﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="ImpuestoTipoDocumentosController"/>
''' </summary>
<Authorize>
Public Class ImpuestoTipoDocumentosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ImpuestoTipoDocumentos) As Respuesta(Of IEnumerable(Of ImpuestoTipoDocumentos))
        Return New LogicaImpuestoTipoDocumentos(CapaPersistenciaImpuestoTipoDocumentos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ImpuestoTipoDocumentos) As Respuesta(Of ImpuestoTipoDocumentos)
        Return New LogicaImpuestoTipoDocumentos(CapaPersistenciaImpuestoTipoDocumentos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ImpuestoTipoDocumentos) As Respuesta(Of Long)
        Return New LogicaImpuestoTipoDocumentos(CapaPersistenciaImpuestoTipoDocumentos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ImpuestoTipoDocumentos) As Respuesta(Of Boolean)
        Return New LogicaImpuestoTipoDocumentos(CapaPersistenciaImpuestoTipoDocumentos).Anular(entidad)
    End Function
End Class
