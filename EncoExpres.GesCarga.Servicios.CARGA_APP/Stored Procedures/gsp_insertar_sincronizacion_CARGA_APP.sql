PRINT 'gsp_insertar_sincronizacion_CARGA_APP'
GO
DROP PROCEDURE gsp_insertar_sincronizacion_CARGA_APP
GO
CREATE PROCEDURE gsp_insertar_sincronizacion_CARGA_APP(
@par_EMPR_Codigo smallint,
@par_CATA_OSCA_Codigo NUMERIC(18,0),
@par_Codigo NUMERIC(18,0),
@par_Mensaje_Sincronizacion Varchar(250) = null,
@par_Sincronizo smallint = null
)
AS
BEGIN
	DECLARE @ExisteRegistro NUMERIC(18,0);
	SET @ExisteRegistro = (SELECT COUNT(1) FROM Registro_Sincronizacion_APP_CARGA WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_OSCA_Codigo = @par_CATA_OSCA_Codigo AND Codigo = @par_Codigo)

	If ISNULL(@ExisteRegistro,0) > 0
	BEGIN
		UPDATE Registro_Sincronizacion_APP_CARGA SET Mensaje_Sincronizacion = @par_Mensaje_Sincronizacion, Sincronizado = @par_Sincronizo, Fecha_Sincronizacion = GETDATE() WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_OSCA_Codigo = @par_CATA_OSCA_Codigo AND Codigo = @par_Codigo
		SELECT ID FROM Registro_Sincronizacion_APP_CARGA WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_OSCA_Codigo = @par_CATA_OSCA_Codigo AND Codigo = @par_Codigo
	END
	ELSE
	BEGIN
		INSERT INTO Registro_Sincronizacion_APP_CARGA(
		EMPR_Codigo
		,CATA_OSCA_Codigo
		,Codigo
		,Fecha_Sincronizacion
		,Mensaje_Sincronizacion
		,Sincronizado
		)
		VALUES(
		@par_EMPR_Codigo,
		@par_CATA_OSCA_Codigo,
		@par_Codigo,
		GETDATE(),
		@par_Mensaje_Sincronizacion,
		@par_Sincronizo
		)

		SELECT @@IDENTITY AS ID
	END
END
GO