﻿
Print 'CREATE PROCEDURE gsp_modificar_encabezado_facturas'
GO
DROP PROCEDURE gsp_modificar_encabezado_facturas
GO

CREATE PROCEDURE gsp_modificar_encabezado_facturas (  
  
 @par_EMPR_Codigo smallint,  
 @par_Numero numeric,  
  
 @par_Fecha      datetime,  
 @par_OFIC_Factura    smallint,  
 @par_TERC_Cliente    numeric,  
 @par_TERC_Facturar    numeric,  
 @par_CATA_FPVE_Codigo   numeric,  
 @par_Dias_Plazo     int = NULL,  
  
 @par_Observaciones    varchar(500) = NULL,  
 @par_Valor_Remesas    money = NULL,  
 @par_Valor_Otros_Conceptos  money = NULL,  
 @par_Valor_Descuentos   money = NULL,  
 @par_Subtotal     money = NULL,  
  
 @par_Valor_Impuestos   money = NULL,  
 @par_Valor_Factura    money = NULL,  
 @par_Valor_TRM     money =  NULL,  
 @par_MONE_Codigo_Alterna  money = NULL,  
 @par_Valor_Moneda_Alterna  money = NULL,  
  
 @par_Valor_Anticipo    money = NULL,  
 @par_Estado      money = NULL  
   
)  
  
AS  
BEGIN  
--Variable Tipo Factura
Declare @par_FactDefinitivo numeric
set @par_FactDefinitivo = 170
-- Limpia el detalle  
UPDATE Encabezado_Remesas SET ENFA_Numero = 0 WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENFA_Numero = @par_Numero  
DELETE Detalle_Remesas_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENFA_Numero = @par_Numero 
DELETE Detalle_Impuestos_Conceptos_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENFA_Numero = @par_Numero 
DELETE Detalle_Conceptos_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENFA_Numero = @par_Numero 
DELETE Detalle_Impuestos_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENFA_Numero = @par_Numero 


--Actuviliza el encabezado  
UPDATE Encabezado_Facturas SET  

Fecha = @par_Fecha,  
OFIC_Factura = @par_OFIC_Factura,  
TERC_Cliente = @par_TERC_Cliente,  
TERC_Facturar = @par_TERC_Facturar,  
CATA_FPVE_Codigo = @par_CATA_FPVE_Codigo,  
Dias_Plazo  = ISNULL(@par_Dias_Plazo,0),  
Observaciones  = ISNULL(@par_Observaciones,''),  
Valor_Remesas = ISNULL(@par_Valor_Remesas,0),  
Valor_Otros_Conceptos = ISNULL(@par_Valor_Otros_Conceptos,0),  
Valor_Descuentos = ISNULL(@par_Valor_Descuentos,0),  
Subtotal = ISNULL(@par_Subtotal,0),  
Valor_Impuestos = ISNULL(@par_Valor_Impuestos,0),  
Valor_Factura = ISNULL(@par_Valor_Factura,0),  
Valor_TRM = ISNULL(@par_Valor_TRM,0),  
MONE_Codigo_Alterna = ISNULL(@par_MONE_Codigo_Alterna,0),  
Valor_Moneda_Alterna = ISNULL(@par_Valor_Moneda_Alterna,0),  
Valor_Anticipo = ISNULL(@par_Valor_Anticipo,0),  
Estado = ISNULL(@par_Estado,0)  

FROM Encabezado_Facturas ENFA  
WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo  
AND ENFA.NUmero = @par_Numero  

--- Consumo de factura definitiva  
IF ISNULL(@par_Estado,0) = 1 BEGIN  
DECLARE @numNumeroFactura NUMERIC = 0  
EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_FactDefinitivo, @par_OFIC_Factura, @numNumeroFactura OUTPUT  

UPDATE Encabezado_Facturas SET Numero_Documento = @numNumeroFactura, TIDO_Codigo =  @par_FactDefinitivo  
WHERE EMPR_Codigo = @par_EMPR_Codigo  
AND Numero = @par_Numero  
END  

--- Retorna la modificación  
SELECT Numero, Numero_Documento FROM Encabezado_Facturas ENFA  
WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo  
AND ENFA.Numero = @par_Numero
--AND ENFA.TIDO_Codigo = @par_FactDefinitivo
  
END  
GO