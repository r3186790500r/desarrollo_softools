﻿Imports Newtonsoft.Json

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref=" DetallePaquteriaVentas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetallePaquteriaVentas
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePaquteriaVentas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePaquteriaVentas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            MinimoKgCobro = Read(lector, "Minimo_Kilo_Cobro")
            MaximoKgCobro = Read(lector, "Maximo_Kilo_Cobro")
            ValorKgAdicional = Read(lector, "Valor_Kilo_Adicional")
            ValorManejo = Read(lector, "Valor_Manejo")
            ValorSeguro = Read(lector, "Valor_Seguro")
            PorcentajeManejo = Read(lector, "Porcentaje_Manejo")
            MinimoValorSeguro = Read(lector, "Minimo_Valor_Seguro")
            MinimoValorUnidad = Read(lector, "Minimo_Valor_Unidad")
            MinimoValorManejo = Read(lector, "Minimo_Valor_Manejo")
            PorcentajeSeguro = Read(lector, "Porcentaje_Seguro")


        End Sub


        <JsonProperty>
        Public Property NumeroTarifario As Integer
        <JsonProperty>
        Public Property MinimoKgCobro As Double
        <JsonProperty>
        Public Property MaximoKgCobro As Double
        <JsonProperty>
        Public Property ValorKgAdicional As Double
        <JsonProperty>
        Public Property ValorManejo As Double
        <JsonProperty>
        Public Property ValorSeguro As Double
        <JsonProperty>
        Public Property PorcentajeManejo As Double
        <JsonProperty>
        Public Property MinimoValorSeguro As Double
        <JsonProperty>
        Public Property MinimoValorUnidad As Double
        <JsonProperty>
        Public Property MinimoValorManejo As Double
        <JsonProperty>
        Public Property PorcentajeSeguro As Double

    End Class
End Namespace
