﻿Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Fachada.ServicioCliente
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace ServicioCliente
    Public NotInheritable Class LogicaEncabezadoProgramacionOrdenServicios
        Inherits LogicaBase(Of EncabezadoProgramacionOrdenServicios)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of EncabezadoProgramacionOrdenServicios)


        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos del detalle.
        ''' </summary>
        ReadOnly _persistenciaDetalle As IPersistenciaBase(Of DetalleProgramacionOrdenServicios)

        Sub New(persistencia As IPersistenciaBase(Of EncabezadoProgramacionOrdenServicios))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As EncabezadoProgramacionOrdenServicios) As Respuesta(Of IEnumerable(Of EncabezadoProgramacionOrdenServicios))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EncabezadoProgramacionOrdenServicios))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EncabezadoProgramacionOrdenServicios))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As EncabezadoProgramacionOrdenServicios) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor <= 0 Then
                Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = False, .MensajeOperacion = Recursos.FalloOperacionGuardarRegistro}
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As EncabezadoProgramacionOrdenServicios) As Respuesta(Of EncabezadoProgramacionOrdenServicios)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EncabezadoProgramacionOrdenServicios)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EncabezadoProgramacionOrdenServicios)(consulta)
        End Function

        Private Function DatosRequeridos(entidad As EncabezadoProgramacionOrdenServicios) As Respuesta(Of EncabezadoProgramacionOrdenServicios)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of EncabezadoProgramacionOrdenServicios) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of EncabezadoProgramacionOrdenServicios) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As EncabezadoProgramacionOrdenServicios) As Respuesta(Of EncabezadoProgramacionOrdenServicios)
            Return New Respuesta(Of EncabezadoProgramacionOrdenServicios)(CType(_persistencia, PersistenciaEncabezadoProgramacionOrdenServicios).Anular(entidad))
        End Function

        Public Function EliminarDetalle(entidad As DetalleProgramacionOrdenServicios) As Respuesta(Of Long)
            Return New Respuesta(Of Long)(CType(_persistencia, PersistenciaEncabezadoProgramacionOrdenServicios).EliminarDetalle(entidad))
        End Function
    End Class

End Namespace