﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioPlanUnicoCuentas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioPlanUnicoCuentas
        Inherits RepositorioBase(Of PlanUnicoCuentas)

        Public Overrides Function Consultar(filtro As PlanUnicoCuentas) As IEnumerable(Of PlanUnicoCuentas)
            Dim lista As New List(Of PlanUnicoCuentas)
            Dim item As PlanUnicoCuentas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.CodigoCuenta) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Cuenta", filtro.CodigoCuenta)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If

                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_plan_unico_cuentas]")

                While resultado.Read
                    item = New PlanUnicoCuentas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As PlanUnicoCuentas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_CATA_CCPU_Codigo", entidad.ClaseCuentaPUC.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Cuenta", entidad.CodigoCuenta)
                conexion.AgregarParametroSQL("@par_Exige_Centro_Costo", entidad.ExigeCentroCosto)
                conexion.AgregarParametroSQL("@par_Exige_Tercero ", entidad.ExigeTercero)
                conexion.AgregarParametroSQL("@par_Exige_Valor_Base", entidad.ExigeValorBase)
                conexion.AgregarParametroSQL("@par_Valor_Base", entidad.ValorBase, SqlDbType.Decimal)
                conexion.AgregarParametroSQL("@par_Exige_Documento_Cruce", entidad.ExigeDocumentoCruce)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_Aplicar_Medio_Pago", entidad.AplicarMedioPago)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_plan_unico_cuentas")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Overrides Function Modificar(entidad As PlanUnicoCuentas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_CATA_CCPU_Codigo", entidad.ClaseCuentaPUC.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Cuenta", entidad.CodigoCuenta)
                conexion.AgregarParametroSQL("@par_Exige_Centro_Costo", entidad.ExigeCentroCosto)
                conexion.AgregarParametroSQL("@par_Exige_Tercero ", entidad.ExigeTercero)
                conexion.AgregarParametroSQL("@par_Exige_Valor_Base", entidad.ExigeValorBase)
                conexion.AgregarParametroSQL("@par_Valor_Base", entidad.ValorBase, SqlDbType.Decimal)
                conexion.AgregarParametroSQL("@par_Exige_Documento_Cruce", entidad.ExigeDocumentoCruce)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_Aplicar_Medio_Pago", entidad.AplicarMedioPago)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_plan_unico_cuentas")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Overrides Function Obtener(filtro As PlanUnicoCuentas) As PlanUnicoCuentas
            Dim item As New PlanUnicoCuentas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("dbo.gsp_obtener_plan_unico_cuentas")

                While resultado.Read
                    item = New PlanUnicoCuentas(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As PlanUnicoCuentas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_anular_plan_unico_cuentas]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace