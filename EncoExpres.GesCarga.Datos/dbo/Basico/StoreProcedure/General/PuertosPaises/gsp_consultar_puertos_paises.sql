﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 04/10/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_consultar_puertos_paises'
GO
DROP PROCEDURE gsp_consultar_puertos_paises
GO
CREATE PROCEDURE gsp_consultar_puertos_paises 
(                
 @par_EMPR_Codigo SMALLINT,                
 @par_Codigo NUMERIC = NULL,                               
 @par_Nombre VARCHAR(50) = NULL,                
 @par_Nombre_Pais VARCHAR(50) = NULL,                
 @par_Estado SMALLINT = NULL,                
 @par_NumeroPagina INT = NULL,                
 @par_RegistrosPagina INT = NULL                
)                
AS                
BEGIN                
 SET NOCOUNT ON;                
  DECLARE                
   @CantidadRegistros INT                
  SELECT @CantidadRegistros = (                
    SELECT DISTINCT                 
     COUNT(1)                 
 FROM            
  Puertos_Paises PUPA       
	    
  LEFT JOIN Paises PAIS    ON     
  PUPA.EMPR_Codigo = PAIS.EMPR_Codigo      
  AND PUPA.PAIS_Codigo = PAIS.Codigo                
    WHERE                
	             
     PUPA.EMPR_Codigo = @par_EMPR_Codigo 
	 AND PUPA.Codigo = ISNULL(@par_Codigo, PUPA.Codigo)                             
     AND ((PUPA.Nombre LIKE '%' + ISNULL(@par_Nombre, PUPA.Nombre) + '%') OR (@par_Nombre IS NULL))  
	 AND ((PAIS.Nombre LIKE '%' + ISNULL(@par_Nombre_Pais, PAIS.Nombre) + '%') OR (@par_Nombre_Pais IS NULL))               
     AND PUPA.Estado = ISNULL(@par_Estado, PUPA.Estado)                   
     );                
                            
    WITH Pagina AS                
    (                
                
    SELECT                
     PUPA.EMPR_Codigo,                
     PUPA.Codigo,                        
     PUPA.Nombre,  
	 PUPA.Contacto,              
     ISNULL(PUPA.PAIS_Codigo, 0) AS PAIS_Codigo, 
	 ISNULL(PAIS.Nombre, '') AS Nombre_Pais,    
	 ISNULL(PUPA.Ciudad, '') AS Nombre_Ciudad,             
     ISNULL(PUPA.Direccion, '') AS Direccion,               
     ISNULL(PUPA.Telefono, '') AS Telefono,     
	 ISNULL(PUPA.Observaciones, '') AS Observaciones,                            
     PUPA.Estado,                          
     ROW_NUMBER() OVER(ORDER BY PUPA.Codigo) AS RowNumber                
   FROM      
           
  Puertos_Paises PUPA       
	    
  LEFT JOIN Paises PAIS    ON     
  PUPA.EMPR_Codigo = PAIS.EMPR_Codigo      
  AND PUPA.PAIS_Codigo = PAIS.Codigo      
                  
    WHERE                
	             
     PUPA.EMPR_Codigo = @par_EMPR_Codigo 
	 AND PUPA.Codigo = ISNULL(@par_Codigo, PUPA.Codigo)                             
     AND ((PUPA.Nombre LIKE '%' + ISNULL(@par_Nombre, PUPA.Nombre) + '%') OR (@par_Nombre IS NULL))  
     AND ((PAIS.Nombre LIKE '%' + ISNULL(@par_Nombre_Pais, PAIS.Nombre) + '%') OR (@par_Nombre_Pais IS NULL))            
     AND PUPA.Estado = ISNULL(@par_Estado, PUPA.Estado)                   
     )
	     
  SELECT DISTINCT                
  0 AS Obtener,                
     EMPR_Codigo,                
     Codigo,                        
     Nombre,
	 Contacto,                
     PAIS_Codigo, 
	 Nombre_Pais,   
	 Nombre_Ciudad,             
     Direccion,                
     Telefono, 
	 Observaciones,                               
     Estado,   
  @CantidadRegistros AS TotalRegistros,                
  @par_NumeroPagina AS PaginaObtener,                
  @par_RegistrosPagina AS RegistrosPagina                
  FROM                
Pagina                
  WHERE                
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                
  ORDER BY Nombre                
END  
GO