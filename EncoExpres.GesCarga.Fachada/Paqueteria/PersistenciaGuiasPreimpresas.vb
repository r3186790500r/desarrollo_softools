﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaGuiasPreimpresas
        Implements IPersistenciaBase(Of GuiaPreimpresa)

        Public Function Consultar(filtro As GuiaPreimpresa) As IEnumerable(Of GuiaPreimpresa) Implements IPersistenciaBase(Of GuiaPreimpresa).Consultar
            Return New RepositorioGuiasPreimpresas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As GuiaPreimpresa) As Long Implements IPersistenciaBase(Of GuiaPreimpresa).Insertar
            Return New RepositorioGuiasPreimpresas().Insertar(entidad)
        End Function

        Public Function InsertarPrecintos(entidad As GuiaPreimpresa) As GuiaPreimpresa
            Return New RepositorioGuiasPreimpresas().InsertarPrecintos(entidad)
        End Function

        Public Function Modificar(entidad As GuiaPreimpresa) As Long Implements IPersistenciaBase(Of GuiaPreimpresa).Modificar
            Return New RepositorioGuiasPreimpresas().Modificar(entidad)
        End Function

        Public Function ModificarPrecintos(entidad As GuiaPreimpresa) As GuiaPreimpresa
            Return New RepositorioGuiasPreimpresas().ModificarPrecintos(entidad)
        End Function

        Public Function Obtener(filtro As GuiaPreimpresa) As GuiaPreimpresa Implements IPersistenciaBase(Of GuiaPreimpresa).Obtener
            Return New RepositorioGuiasPreimpresas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As GuiaPreimpresa) As Boolean
            Return New RepositorioGuiasPreimpresas().Anular(entidad)
        End Function
        Public Function LiberarPrecinto(entidad As GuiaPreimpresa) As Boolean
            Return New RepositorioGuiasPreimpresas().LiberarPrecinto(entidad)
        End Function

        Public Function ConsultarUnicoPrecinto(filtro As DetalleGuiaPreimpresa) As IEnumerable(Of DetalleGuiaPreimpresa)
            Return New RepositorioGuiasPreimpresas().ConsultarDetallePrecinto(filtro)
        End Function
        Public Function ValidarPrecintoPlanillaPaqueteria(entidad As GuiaPreimpresa) As GuiaPreimpresa
            Return New RepositorioGuiasPreimpresas().ValidarPrecintoPlanillaPaqueteria(entidad)
        End Function

        Public Function ActualizarResponsable(entidad As GuiaPreimpresa) As Long
            Return New RepositorioGuiasPreimpresas().ActualizarTercero(entidad)
        End Function
    End Class

End Namespace

