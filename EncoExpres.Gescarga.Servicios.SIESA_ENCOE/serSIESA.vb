﻿Option Explicit On

Imports System.Configuration
Imports System.Diagnostics.Eventing.Reader
Imports System.Net
Imports System.Threading
Imports System.Xml
Imports EncoExpres.Gescarga.Servicios.SIESA_ENCOE.co.encoexpres.gti

Public Class serSIESA

#Region "Variables Generales"

    ' Variables
    Private strSQL As String
    Private strPlano As String
    Private lonNumeRegi As Long
    Private objGeneral As clsGeneral

    Private lonNumeComp As Long = 0
    Private lonNumeCompAnte As Long = 0
    Private intCodigoEmpresa As Integer
    Private lonIntervaloMilisegundosEjecucionProceso As Long
    Private strRutaArchivoLog As String
    Private strRutaArchivoXML As String

    ' Hilos Procesos
    Private HilProcesoComprobantesEgreso As Thread
    Private HilProcesoFacturaContadoContraEntrega As Thread
    Private HilProcesoCumplidoContraEntrega As Thread
    Private HilProcesoLegalizacionRecaudo As Thread
    Private HilProcesoLegalizacionGastosConductor As Thread
    Private HilProcesoLiquidacionPlanillaDespachos As Thread
    Private HilProcesoOrdenVentaServicio As Thread

    ' GenericTransfer
    Dim objGenericTransfer As New wsGenerarPlano()


#End Region

#Region "Constantes GESTRANS"
    ' GESTRANS
    Const CODIGO_USUARIO_SYSTEM As Integer = 0 ' Usuario Sistema

    Const LINEA_NEGOCIO_PAQUETERIA As Integer = 3

    ' TIPOS DOCUMENTOS GESTRANS
    Const TIDO_COMPROBANTE_EGRESO As Integer = 30
    Const TIDO_LIQUIDACION_PLANILLA_MASIVO As Integer = 160
    Const TIDO_LIQUIDACION_PLANILLA_PAQUETERIA As Integer = 165

    ' CONCEPTOS CONTABLES
    Const CONCEPTO_CONTABLE_ANTICIPO As Integer = 13

    ' FORMAS PAGO
    Const FORMA_PAGO_NOAPLICA As Long = 4900

    ' FORMAS PAGO FACTURAS
    Const FORMA_PAGO_FACTURA_CONTADO As Long = 4902
    Const FORMA_PAGO_FACTURA_CONTRAENTREGA As Long = 4903

    ' FORMA PAGO LEGALIZACION RECAUDO
    Const FORMA_PAGO_LEGALIZACION_RECAUDO_EFECTIVO As Long = 5101
    Const FORMA_PAGO_LEGALIZACION_RECAUDO_TRANSFERENCIA As Long = 5102
    Const FORMA_PAGO_LEGALIZACION_RECAUDO_CHEQUE As Long = 5103

    ' CONCEPTOS LEGALIZACION GASTOS SALDO A FAVOR EMPRESA Y SALDO A FAVOR CONDUCTOR
    Const CODIGO_CONCEPTO_LEGALIZACION_GASTOS_SALDO_FAVOR_EMPRESA As Long = 10001
    Const CODIGO_CONCEPTO_LEGALIZACION_GASTOS_SALDO_FAVOR_CONDUCTOR As Long = 10002

    ' CONCEPTOS LIQUIDACION PLANILLA DESPACHOS
    Const CONCEPTO_ANTICIPO_LIQUIDACION_PLANILLA_DESPACHOS As Integer = 1
    Const CONCEPTO_REANTICIPO_LIQUIDACION_PLANILLA_DESPACHOS As Integer = 2
    Const CONCEPTO_FONDO_RESPONSABILIDAD_LIQUIDACION_PLANILLA_DESPACHOS As Integer = 7
    Const CONCEPTO_MAYOR_VALOR_FLETE_LIQUIDACION_PLANILLA_DESPACHOS As Integer = 10
    Const CONCEPTO_MENOR_VALOR_FLETE_LIQUIDACION_PLANILLA_DESPACHOS As Integer = 11

    ' PERFILES TERCEROS
    Const PETE_CLIENTE As Long = 1401
    Const PETE_CONDUCTOR As Long = 1403
    Const PETE_DESTINATARIO As Long = 1404
    Const PETE_EMPLEADO As Long = 1405
    Const PETE_REMITENTE As Long = 1410

    ' CATALOGO TIPO CONECTORES SIESA
    Const CATA_TICS_COMPROBANTES_EGRESO As Long = 23809
    Const CATA_TICS_FACTURAS_CONTADO_CONTRAENTREGA As Long = 23806
    Const CATA_TICS_LEGALIZACION_GASTOS_CONDUCTOR As Long = 23810
    Const CATA_TICS_CUMPLIDO_CONTRAENTREGA As Long = 23808

    ' CATALOGO NATURALEZA CUENTA CONTABLE
    Const CATA_NACC_DEBITO As Long = 2901
    Const CATA_NACC_CREDITO As Long = 2902

#End Region

#Region "Variables-Constantes SIESA"

    ' SIESA TABLAS
    Const TIPO_NATURALEZA_NATURAL_SIESA As Integer = 1
    Const TIPO_NATURALEZA_JURIDICA_SIESA As Integer = 2

    Const TIPO_IDENTIFICACION_CEDULA_SIESA As String = "C"
    Const TIPO_IDENTIFICACION_EXTRANJERIA_SIESA As String = "E"
    Const TIPO_IDENTIFICACION_NIT_SIESA As String = "N"

    Dim TIPO_DOCUMENTO_SIESA As String
    Dim UNIDAD_NEGOCIO_SIESA As String
    Dim FLUJO_EFECTIVO_SIESA As String
    Dim PREFIJO_CRUCE_SIESA As String
    Dim CUENTA_AUXILIAR_SIESA As String
    Dim CODIGO_CAJA_SIESA As String
    Dim DOCUMENTO_CRUCE_SIESA As String
    Dim SUCURSAL_TERCERO_SIESA As String

    ' Anticipos 
    Const CENTRO_OPERACION_SIESA As String = "FZ2"
    Const TIPO_DOCUMENTO_EGRESO_SIESA As String = "ANT"
    Const PREFIJO_CRUCE_ANTICIPO_SIESA As String = "PPL"
    Const SUCURSAL_SIESA As String = "ANT"

    ' Facturas Contado y Contraentrega
    Const TIPO_DOCUMENTO_FACTURA_SIESA As String = "FCO"
    Dim ID_SUCURSAL_CLIENTE_FACTURA_SIESA As String
    Dim TIPO_CLIENTE_FACTURA_SIESA As String = ""
    Dim CONDICION_PAGO_FACTURA_SIESA As String = ""
    Dim TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA As String = ""
    Dim CENTRO_OPERACION_FACTURA_SIESA As String = ""
    Dim CENTRO_COSTO_FACTURA_SIESA As String = ""
    Dim ID_CAJA As String = ""
    Dim UNIDAD_NEGOCIO_FACTURA_SIESA As String = ""
    Dim TIPO_SERVICIO_FACTURA_SIESA As String = ""
    Const MEDIO_PAGO_EFECTIVO As String = "EFE"
    ' Motivos Servicio Factura
    Const ID_MOTIVO_FLETE_SIESA As String = "01"
    Const ID_MOTIVO_MANEJO_SIESA As String = "02"
    Const ID_MOTIVO_REEXPEDICION_SIESA As String = "03"
    Const ID_MOTIVO_CARGE_ACARREO_LOCAL_SIESA As String = "04"
    Const ID_MOTIVO_DESCARGUE_ACARREO_DESTINO_SIESA As String = "05"

    ' Legalización Recaudo Contado - Traslado Cajas Efectivo
    Const TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA As String = "TDD"

    ' Legalización Gastos Conductor
    Const TIPO_DOCUMENTO_LEGALIZA_GASTOS_CONDUCTOR_SIESA As String = "LGV"
    Dim SUCURSAL_CONCEPTO_GASTO_SIESA As String = ""
    Dim CENTRO_COSTO_CONCEPTO_GASTO_SIESA As String = ""
    Dim DOCUMENTO_CRUCE_CONCEPTO_GASTO_SIESA As String = ""
    Dim SUCURSAL_GASTO_CONDUCTOR_SIESA As String
    Dim DOCUMENTO_CRUCE_GASTO_CONDUCTOR_SIESA As String

    ' Liquidación Planilla Despachos
    Const TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA As String = "FL" ' Se concadena 1,2,3 de acuerdo al número de tenedores del vehículo

#End Region

#Region "Funciones Base Servicio"

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            Me.HilProcesoComprobantesEgreso = New Thread(AddressOf Proceso_Comprobantes_Egreso)
            Me.HilProcesoComprobantesEgreso.Start()

            Me.HilProcesoFacturaContadoContraEntrega = New Thread(AddressOf Proceso_Facturas_Contado_Contraentrega)
            Me.HilProcesoFacturaContadoContraEntrega.Start()

            Me.HilProcesoCumplidoContraEntrega = New Thread(AddressOf Proceso_Cumplido_ContraEntrega)
            Me.HilProcesoCumplidoContraEntrega.Start()

            'Me.HilProcesoLegalizacionRecaudo = New Thread(AddressOf Proceso_Legalizacion_Recaudo)
            'Me.HilProcesoLegalizacionRecaudo.Start()

            'Me.HilProcesoLegalizacionGastosConductor = New Thread(AddressOf Proceso_Legalizacion_Gastos_Conductor)
            'Me.HilProcesoLegalizacionGastosConductor.Start()

            'Me.HilProcesoLiquidacionPlanillaDespachos = New Thread(AddressOf Proceso_Liquidacion_Despachos)
            'Me.HilProcesoLiquidacionPlanillaDespachos.Start()


        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error OnStart: " & ex.Message)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        Try
            Me.HilProcesoComprobantesEgreso.Abort()

            Me.HilProcesoFacturaContadoContraEntrega.Abort()

            Me.HilProcesoLegalizacionGastosConductor.Abort()

            'Me.HilProcesoCumplidoContraEntrega.Abort()

            'Me.HilProcesoLegalizacionRecaudo.Abort()

            'Me.HilProcesoLiquidacionPlanillaDespachos.Abort()

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error OnStop: " & ex.Message)
        End Try
    End Sub

    Public Sub New()
        Try
            InitializeComponent()

            Me.objGeneral = New clsGeneral
            Call Cargar_Datos_App_Config()

            Me.objGeneral.Guardar_Mensaje_Log("Inició servicio")

            'Proceso_Facturas_Contado_Contraentrega()
            'Proceso_Legalizacion_Gastos_Conductor()
            'Proceso_Comprobantes_Egreso()
            'Proceso_Legalizacion_Gastos_Conductor()

            ' COMENTARIAR CUANDO SE GENERE EL INSTALADOR DEL SERVICIO, SOLO PARA PRUEBAS

            'Me.HilProcesoComprobantesEgreso = New Thread(AddressOf Proceso_Comprobantes_Egreso)
            'Me.HilProcesoComprobantesEgreso.Start()

            'Me.HilProcesoFacturaContadoContraEntrega = New Thread(AddressOf Proceso_Facturas_Contado_Contraentrega)
            'Me.HilProcesoFacturaContadoContraEntrega.Start()

            'Me.HilProcesoLegalizacionGastosConductor = New Thread(AddressOf Proceso_Legalizacion_Gastos_Conductor)
            'Me.HilProcesoLegalizacionGastosConductor.Start()

            Me.HilProcesoCumplidoContraEntrega = New Thread(AddressOf Proceso_Cumplido_ContraEntrega)
            Me.HilProcesoCumplidoContraEntrega.Start()

            'Me.HilProcesoLegalizacionRecaudo = New Thread(AddressOf Proceso_Legalizacion_Recaudo)
            'Me.HilProcesoLegalizacionRecaudo.Start()

            'Me.HilProcesoLiquidacionPlanillaDespachos = New Thread(AddressOf Proceso_Liquidacion_Planilla_Despachos)
            'Me.HilProcesoLiquidacionPlanillaDespachos.Start()

            'Me.HilProcesoOrdenVentaServicio = New Thread(AddressOf Proceso_Orden_Venta_Servicio)
            'Me.HilProcesoOrdenVentaServicio.Start()

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error New: " & ex.Message)
        End Try

    End Sub

#End Region

#Region "Proceso Comprobantes Egreso"

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 06-MAY-2021
    ' Observaciones Creación: Se consulta el Encabezado y Detalle de Comprobantes Contables, el cual debe traer 2 registros por comprobante
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Proceso_Comprobantes_Egreso()
        Try
            Dim dtsCompCont As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Comprobantes_Egreso")
                strSQL = "gsp_Obtenter_Comprobantes_Egresos_SIESA " & intCodigoEmpresa.ToString
                dtsCompCont = objGeneral.Retorna_Dataset(strSQL)

                If dtsCompCont.Tables.Count > 0 Then

                    If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Comprobantes_Egreso(dtsCompCont)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Comprobantes_Egreso: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 06-MAY-2021
    ' Observaciones Creación: Se construye XML con base en especificación entregada por GenericTransfer 19-ABR-2021
    ' Modifica: Jesus Cuadros
    ' Fecha Modificación: 02-MAR-2022
    ' Observaciones Modificación: Se realizan ajustes para parametrizar valores que se encontraban fijos

    Private Sub Crear_XML_Comprobantes_Egreso(ByVal dtsCompCont As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = ""
            Dim strErrorDetalleSIESA As String, lonNumeCompEgre As Long, strIdenTercCompEgre As String, strFechCompEgre As String
            Dim strNotas As String, dblValor As Double, strCodiAlteOfic As String, lonCodiCompEgre As Long

            Dim xmlDocumento As XmlDocument
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeComp = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")
                lonNumeCompEgre = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                lonCodiCompEgre = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Documento").ToString()
                strIdenTercCompEgre = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString()
                strFechCompEgre = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                strCodiAlteOfic = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Alterno_Oficina").ToString() ' El Código Alterno de la oficina corresponde al Centro Operación SIESA
                strNotas = "COMPROBANTE EGRESO ANTICIPO No. " & lonNumeCompEgre.ToString()
                If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Debito").ToString() > 0 Then
                    dblValor = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Debito").ToString()
                Else
                    dblValor = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Credito").ToString()
                End If

                If lonNumeComp <> lonNumeCompAnte Then

                    Call Consultar_Catalogo_Tipo_Conector_SIESA(CATA_TICS_COMPROBANTES_EGRESO)

                    xmlDocumento = New XmlDocument
                    xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                    xmlDocumento.AppendChild(xmlElementoRaiz)

                    ' DOCUMENTO CONTABLE
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Documentocontable")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic) ' El Código Alterno de la oficina corresponde al Centro Operación SIESA
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeCompEgre)
                    ' F350_FECHA (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechCompEgre)
                    ' F350_ID_TERCERO (Alfanumerico(15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenTercCompEgre)
                    ' F350_NOTAS (Alfanumerico(255))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strNotas)

                    ' CAJA (Comprobantes Egreso Efectivo)
                    Call Consultar_Detalle_Concepto_Contable(CONCEPTO_CONTABLE_ANTICIPO, CATA_NACC_CREDITO)
                    Call Consultar_Caja_Comprobante_Egreso(lonCodiCompEgre)

                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Caja")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeCompEgre)
                    ' F351_ID_AUXILIAR (Alfanumerico(20))
                    strValorCampo = CUENTA_AUXILIAR_SIESA
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                    ' F351_ID_CO_MOV (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiAlteOfic)

                    ' F351_ID_UN (Alfanumerico(20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                    ' F351_ID_CCOSTO (Alfanumerico(15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                    ' F351_ID_FE (Alfanumerico(10))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", FLUJO_EFECTIVO_SIESA)
                    ' F351_VALOR_DB (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", 0)
                    ' F351_VALOR_CR (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValor)

                    ' F351_NOTAS (Alfanumerico(255))
                    strValorCampo = "CAJA ANTICIPO No. Egreso: " & lonNumeCompEgre.ToString()
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                    ' F358_ID_CAJA (Alfanumerico(3))
                    strValorCampo = CODIGO_CAJA_SIESA
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CAJA", strValorCampo)
                    ' F358_NOTAS (Alfanumerico(255))
                    strValorCampo = "CAJA ANTICIPO No. Egreso: " & lonNumeCompEgre.ToString()
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strValorCampo)

                Else
                    ' MOVIMIENTO CXP
                    Call Consultar_Detalle_Concepto_Contable(CONCEPTO_CONTABLE_ANTICIPO, CATA_NACC_DEBITO)

                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_EGRESO_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeCompEgre)
                    ' F351_ID_AUXILIAR (Alfanumerico(20))
                    strValorCampo = CUENTA_AUXILIAR_SIESA
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                    ' F351_ID_TERCERO (Alfanumerico (15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTercCompEgre)
                    ' F351_ID_CO_MOV (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiAlteOfic)
                    ' F351_ID_UN (Alfanumerico(20) 05=PASAJEROS)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                    ' F351_VALOR_DB (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValor)
                    ' F351_VALOR_CR (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", 0)
                    ' F351_NOTAS (Alfanumerico(255))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", "")
                    ' F353_ID_SUCURSAL (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_SIESA)
                    ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", PREFIJO_CRUCE_SIESA)
                    ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", 0)
                    ' F353_NRO_CUOTA_CRUCE (Entero(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", 1)
                    ' F353_ID_FE (Alfanumerico(10))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", FLUJO_EFECTIVO_SIESA)
                    ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechCompEgre)
                    ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechCompEgre)
                    ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechCompEgre)
                    ' F354_NOTAS (Alfanumerico(255))
                    strValorCampo = "CxP ANTICIPO No. Egreso: " & lonNumeCompEgre.ToString()
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                    strXML = xmlDocumento.InnerXml
                    Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                    ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                    Me.objGenericTransfer = New wsGenerarPlano()
                    strPlanoXML = objGenericTransfer.ImportarDatosXML(105599, "ANTICIPOS", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                    If strPlanoXML = "Importacion exitosa" Then

                        ' Actualizar el Comprobante Contable con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                        strSQL = "UPDATE Encabezado_Comprobante_Contables SET Interfase_Contable = 1, Fecha_Interfase_Contable = getDate()"
                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND Numero = " & lonNumeComp

                        ' Ejecutar UPDATE
                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                    Else
                        strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                        ' Actualizar el Comprobante Contable con Interfase_Contable = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                        strSQL = "UPDATE Encabezado_Comprobante_Contables SET Interfase_Contable = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND Numero = " & lonNumeComp

                        ' Ejecutar UPDATE
                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                    End If

                End If

                lonNumeCompAnte = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")

            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Comprobantes_Egreso: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 19-FEB-2022
    ' Observaciones Creación: Consulta la información definida para el código del catálogo del conector
    ' Modifico: Jesus Cuadros
    ' Fecha Modificacion: 05-MAR-2022
    ' Observaciones Modificación: Para el conector legalización gastos se consulta campo3 donde se almacena la unidad de negocio

    Private Sub Consultar_Catalogo_Tipo_Conector_SIESA(lonCodiValoCata As Long)
        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT * FROM Valor_Catalogos WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString()
            strSQL += " AND Codigo = " & lonCodiValoCata.ToString()
            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    TIPO_DOCUMENTO_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Campo2").ToString()
                    If lonCodiValoCata = CATA_TICS_LEGALIZACION_GASTOS_CONDUCTOR Then
                        UNIDAD_NEGOCIO_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Campo3").ToString()
                    End If
                Else
                    TIPO_DOCUMENTO_SIESA = ""
                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró el Tipo Documento con código " & lonCodiValoCata.ToString)
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Catalogo_Tipo_Conector_SIESA: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 19-FEB-2022
    ' Observaciones Creación: Consulta el detalle del código del concepto enviado con base en su naturaleza (Débito/Crédito)

    Private Sub Consultar_Detalle_Concepto_Contable(lonCodiConcCont As Long, lonNatuCuenCont As Long)
        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT DECC.Codigo_Anexo AS Unidad_Negocio, DECC.Campo_Auxiliar AS Flujo_Efectivo, DECC.Prefijo, PLUC.Codigo_Cuenta AS Codigo_Cuenta_PUC"
            strSQL += " FROM Detalle_Concepto_Contables AS DECC"
            strSQL += " INNER JOIN Plan_Unico_Cuentas AS PLUC "
            strSQL += " ON DECC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DECC.PLUC_Codigo = PLUC.Codigo"
            strSQL += " WHERE DECC.EMPR_Codigo = " & intCodigoEmpresa.ToString()
            strSQL += " AND DECC.ECCO_Codigo = " & lonCodiConcCont.ToString()
            strSQL += " AND DECC.CATA_NACC_Codigo = " & lonNatuCuenCont.ToString()

            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    UNIDAD_NEGOCIO_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Unidad_Negocio").ToString()
                    FLUJO_EFECTIVO_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Flujo_Efectivo").ToString()
                    PREFIJO_CRUCE_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Prefijo").ToString()
                    CUENTA_AUXILIAR_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Cuenta_PUC").ToString()
                Else
                    UNIDAD_NEGOCIO_SIESA = ""
                    FLUJO_EFECTIVO_SIESA = ""
                    PREFIJO_CRUCE_SIESA = ""
                    CUENTA_AUXILIAR_SIESA = ""
                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró el detalle del código del concepto " & lonCodiConcCont.ToString)
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Detalle_Concepto_Contable: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 02-MAR-2022
    Private Sub Consultar_Caja_Comprobante_Egreso(ByVal lonCodiCompEgre As Long)
        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT ENDC.Codigo, ENDC.Numero, ISNULL(CAJA.Codigo_Alterno,0) As Codigo_Alterno_Oficina"
            strSQL += " FROM Encabezado_Documento_Comprobantes As ENDC"
            strSQL += " LEFT JOIN Cajas AS CAJA"
            strSQL += " ON ENDC.EMPR_Codigo = CAJA.EMPR_Codigo"
            strSQL += " AND ENDC.CAJA_Codigo = CAJA.Codigo"
            strSQL += " WHERE ENDC.EMPR_codigo = " & intCodigoEmpresa.ToString()
            strSQL += " AND ENDC.Codigo = " & lonCodiCompEgre.ToString()

            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    CODIGO_CAJA_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Alterno_Oficina").ToString()
                Else
                    CODIGO_CAJA_SIESA = 0
                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró la Caja del comprobante egreso con código " & lonCodiCompEgre.ToString)
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Caja_Comprobante_Egreso: " & ex.Message)
        End Try
    End Sub

#End Region

#Region "Proceso Facturas Contado Contraentrega"

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 24-MAY-2021
    ' Observaciones Creación: Se consulta la Factura Contado
    ' Modifica: 
    ' Fecha Modificación: 18-DIC-2021
    ' Observaciones Modificación: Se crea SP gsp_Consultar_Facturas_Contado_Contraentrega_SIESA_ENCOE

    Public Sub Proceso_Facturas_Contado_Contraentrega()
        Try
            Dim dtsCompCont As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Facturas_Contado_Contraentrega")
                strSQL = "gsp_Consultar_Facturas_Contado_Contraentrega_SIESA_ENCOE " & intCodigoEmpresa.ToString
                dtsCompCont = objGeneral.Retorna_Dataset(strSQL)

                If dtsCompCont.Tables.Count > 0 Then

                    If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Facturas_Contado_Contraentrega(dtsCompCont)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Facturas_Contado_Contraentrega: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 24-MAY-2021
    ' Observaciones Creación: Se construye XML con base en especificación entregada por GenericTransfer 19-ABR-2021
    ' Modifica: 
    ' Fecha Modificación: 18-DIC-2021
    ' Observaciones Modificación: Se parametrizan campos fijos y se generan movimientos de venta de servicios por cada concepto de la Guia

    Private Sub Crear_XML_Facturas_Contado_Contraentrega(ByVal dtsCompCont As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = "", strErrorDetalleSIESA As String

            Dim lonNumeFact As Long, lonNumeDocuFact As Long, strFechFact As String, strIdenTercFact As String, strNotas As String
            Dim lonFormPago As Long, dblValoFact As Double, lonCodiOficFact As Long, lonUsaCrea As Long
            Dim lonNumeReme As Long, dblCantReme As Double, dblPesoReme As Double, dblValoFlet As Double, dblValoMane As Double
            Dim dblValoReex As Double, dblValoCarg As Double, dblValoDesc As Double, dtsGuia As DataSet, strAforador As Long, lonOficina As Long
            Dim blnExisteCaja As Boolean = False

            Dim xmlDocumento As XmlDocument
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement
            Dim PETE_Sucursal_SIESA As Long

            For intCon = 0 To dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeFact = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocuFact = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                strIdenTercFact = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Identificacion").ToString()
                strFechFact = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                strNotas = "Factura No. " & lonNumeDocuFact.ToString()
                dblValoFact = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Factura").ToString()
                lonFormPago = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CATA_FPVE_Codigo").ToString()
                lonCodiOficFact = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("OFIC_Factura").ToString()
                ID_CAJA = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Caja_Factura").ToString()

                Consultar_CodigoAlterno(ID_CAJA)

                If lonFormPago = FORMA_PAGO_FACTURA_CONTADO Then
                    PETE_Sucursal_SIESA = PETE_REMITENTE

                    If IsNothing(ID_CAJA) Or ID_CAJA = "" Then
                        blnExisteCaja = False
                    Else
                        blnExisteCaja = True
                    End If

                ElseIf lonFormPago = FORMA_PAGO_FACTURA_CONTRAENTREGA Then
                    PETE_Sucursal_SIESA = PETE_DESTINATARIO
                    If IsNothing(ID_CAJA) Or ID_CAJA = "" Then
                        blnExisteCaja = False
                        ID_CAJA = ""
                    End If
                End If


                If blnExisteCaja Then
                    Call Consultar_Sucursales_SIESA_Perfiles_Terceros(PETE_Sucursal_SIESA, CATA_TICS_FACTURAS_CONTADO_CONTRAENTREGA, lonFormPago)

                    Call Consultar_Parametros_Forma_Pago_Factura(lonFormPago)

                    Call Consultar_Codigo_Alterno_Oficina_Factura(lonCodiOficFact)

                    ' MOVIMIENTO VENTA SERVICIOS
                    ' Se debe crear un MovtoVentasServicios por cada concepto de la Guia (ACARREOS LOCAL, ACARREO DESTINO, FLETE, MANEJO, REEXPEDICION)
                    strSQL = "gsp_Consultar_Detalle_Facturas_SIESA_ENCOE " & intCodigoEmpresa.ToString() & ", " & lonNumeFact.ToString()
                    dtsGuia = objGeneral.Retorna_Dataset(strSQL)
                    If dtsGuia.Tables.Count > 0 Then
                        If dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                            lonNumeReme = dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Numero_Documento").ToString
                            dblCantReme = dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Cantidad_Cliente")
                            dblPesoReme = dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Peso_Cliente")
                            dblValoFlet = dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Valor_Flete")
                            dblValoMane = dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Valor_Seguro")
                            dblValoReex = dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Valor_Reexpedicion")
                            dblValoCarg = dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Valor_Cargue")
                            dblValoDesc = dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Valor_Descargue")
                            strAforador = IIf(dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Aforador") > 0, dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Aforador"), dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Conductor"))
                            lonUsaCrea = dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("USUA_Codigo_Crea")
                            lonOficina = dtsGuia.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("OFIC_Codigo")

                            'Call Consultar_Caja_Aforador(strAforador, lonOficina, lonUsaCrea)

                            xmlDocumento = New XmlDocument
                            xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                            xmlDocumento.AppendChild(xmlElementoRaiz)

                            ' DOCUMENTO VENTA SERVICIOS
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("DoctoVentasServicios")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F311_ID_UN_CRUCE (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_UN_CRUCE", "")
                            ' F311_ID_CCOSTO_CRUCE (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_CCOSTO_CRUCE", "")
                            ' F311_ID_CAJA (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_CAJA", ID_CAJA) 'ID_CAJA)
                            ' F311_REFERENCIA (Alfanumerico(10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_REFERENCIA", lonNumeDocuFact)
                            ' F311_ID_COND_PAGO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_COND_PAGO", CONDICION_PAGO_FACTURA_SIESA) ' 05D=Contraentrega, 00D=Contado
                            ' F311_ID_TERCERO_VENDEDOR (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_TERCERO_VENDEDOR", "800209179") ' PENDIENTE: Ejecutar Consulta Clientes SIESA y tomar Vendedor dependiendo si es Contado y Contraentrega
                            ' F311_ID_TIPO_CLI (Alfanumerico(4))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_TIPO_CLI", TIPO_CLIENTE_FACTURA_SIESA) ' 0002=Contraentrea, 0001=Contado
                            ' F311_ID_SUCURSAL_CLI (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_SUCURSAL_CLI", ID_SUCURSAL_CLIENTE_FACTURA_SIESA)
                            ' F350_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strNotas)
                            ' F350_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenTercFact)
                            ' F350_FECHA (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechFact)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuFact)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA) 'FCE=Contra Entrega, FCO=Contado
                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA) ' Codigo_Alterno de la Agencia que genero la numeración factura B04=Contra Entrega, B25=Contado

                            ' Valor Flete
                            If dblValoFlet > 0 Then
                                ' Se envia la Línea negocio y Motivo para consultar Centro Costo, Unidad Negocio, Tipo Servicio
                                Call Consultar_Parametros_SIESA_Factura_Linea_Negocio(LINEA_NEGOCIO_PAQUETERIA, ID_MOTIVO_FLETE_SIESA)

                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentasServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' F320_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_DETALLE", strNotas)
                                ' F320_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_NOTAS", strNotas)
                                ' F320_VLR_DSCTO_1 (Decimal(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_DSCTO_1", "0")
                                ' F320_VLR_BRUTO (Decimal(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_BRUTO", dblValoFlet)
                                ' F320_CANTIDAD (Decimal(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_CANTIDAD", "1")
                                ' F320_ID_SUCURSAL_CLIENTE (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SUCURSAL_CLIENTE", ID_SUCURSAL_CLIENTE_FACTURA_SIESA) ' Constante
                                ' F320_ID_TERCERO_MOVTO (Alfanumerico(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_TERCERO_MOVTO", strIdenTercFact)
                                ' F320_ID_CCOSTO_MOVTO (Alfanumerico(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CCOSTO_MOVTO", CENTRO_COSTO_FACTURA_SIESA)
                                ' F320_ID_UN_MOVTO (Alfanumerico(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_UN_MOVTO", UNIDAD_NEGOCIO_FACTURA_SIESA)
                                ' F320_ID_CO_MOVTO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CO_MOVTO", CENTRO_OPERACION_FACTURA_SIESA)
                                ' F320_ID_MOTIVO (Alfanumerico(2)
                                ' FLETE = 01, MANEJO = 02, REEXPEDICION = 03, CARGUE-ACARREO LOCAL = 04, DESCARGUE-ACARREO DESTINO = 05
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_MOTIVO", ID_MOTIVO_FLETE_SIESA)
                                ' F320_ID_SERVICIO (Alfanumerico(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SERVICIO", TIPO_SERVICIO_FACTURA_SIESA)
                                ' F320_ROWID (Entero(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ROWID", "1") ' Consecutivo por envío intCon
                                ' F350_CONSEC_DOCTO (Entero(8))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuFact)
                                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA) 'FCE=Contra Entrega, FCO=Contado
                                ' F350_ID_CO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA) 'B04=Contra Entrega, B25=Contado
                            End If

                            ' Valor Manejo
                            If dblValoMane > 0 Then
                                ' Se envia la Línea negocio y Motivo para consultar Centro Costo, Unidad Negocio, Tipo Servicio
                                Call Consultar_Parametros_SIESA_Factura_Linea_Negocio(LINEA_NEGOCIO_PAQUETERIA, ID_MOTIVO_MANEJO_SIESA)

                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentasServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' F320_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_DETALLE", strNotas)
                                ' F320_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_NOTAS", strNotas)
                                ' F320_VLR_DSCTO_1 (Decimal(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_DSCTO_1", "0")
                                ' F320_VLR_BRUTO (Decimal(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_BRUTO", dblValoMane)
                                ' F320_CANTIDAD (Decimal(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_CANTIDAD", "1")
                                ' F320_ID_SUCURSAL_CLIENTE (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SUCURSAL_CLIENTE", ID_SUCURSAL_CLIENTE_FACTURA_SIESA) ' Constante
                                ' F320_ID_TERCERO_MOVTO (Alfanumerico(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_TERCERO_MOVTO", strIdenTercFact)
                                ' F320_ID_CCOSTO_MOVTO (Alfanumerico(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CCOSTO_MOVTO", CENTRO_COSTO_FACTURA_SIESA)
                                ' F320_ID_UN_MOVTO (Alfanumerico(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_UN_MOVTO", UNIDAD_NEGOCIO_FACTURA_SIESA)
                                ' F320_ID_CO_MOVTO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CO_MOVTO", CENTRO_OPERACION_FACTURA_SIESA)
                                ' F320_ID_MOTIVO (Alfanumerico(2)
                                ' FLETE = 01, MANEJO = 02, REEXPEDICION = 03, CARGUE-ACARREO LOCAL = 04, DESCARGUE-ACARREO DESTINO = 05
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_MOTIVO", ID_MOTIVO_MANEJO_SIESA)
                                ' F320_ID_SERVICIO (Alfanumerico(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SERVICIO", TIPO_SERVICIO_FACTURA_SIESA)
                                ' F320_ROWID (Entero(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ROWID", "1") ' Consecutivo por envío intCon
                                ' F350_CONSEC_DOCTO (Entero(8))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuFact)
                                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA) 'FCE=Contra Entrega, FCO=Contado
                                ' F350_ID_CO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA) 'B04=Contra Entrega, B25=Contado
                            End If

                            ' Valor Reexpedicion
                            If dblValoReex > 0 Then
                                ' Se envia la Línea negocio y Motivo para consultar Centro Costo, Unidad Negocio, Tipo Servicio
                                Call Consultar_Parametros_SIESA_Factura_Linea_Negocio(LINEA_NEGOCIO_PAQUETERIA, ID_MOTIVO_REEXPEDICION_SIESA)

                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentasServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' F320_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_DETALLE", strNotas)
                                ' F320_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_NOTAS", strNotas)
                                ' F320_VLR_DSCTO_1 (Decimal(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_DSCTO_1", "0")
                                ' F320_VLR_BRUTO (Decimal(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_BRUTO", dblValoReex)
                                ' F320_CANTIDAD (Decimal(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_CANTIDAD", "1")
                                ' F320_ID_SUCURSAL_CLIENTE (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SUCURSAL_CLIENTE", ID_SUCURSAL_CLIENTE_FACTURA_SIESA) ' Constante
                                ' F320_ID_TERCERO_MOVTO (Alfanumerico(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_TERCERO_MOVTO", strIdenTercFact)
                                ' F320_ID_CCOSTO_MOVTO (Alfanumerico(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CCOSTO_MOVTO", CENTRO_COSTO_FACTURA_SIESA)
                                ' F320_ID_UN_MOVTO (Alfanumerico(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_UN_MOVTO", UNIDAD_NEGOCIO_FACTURA_SIESA)
                                ' F320_ID_CO_MOVTO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CO_MOVTO", CENTRO_OPERACION_FACTURA_SIESA)
                                ' F320_ID_MOTIVO (Alfanumerico(2)
                                ' FLETE = 01, MANEJO = 02, REEXPEDICION = 03, CARGUE-ACARREO LOCAL = 04, DESCARGUE-ACARREO DESTINO = 05
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_MOTIVO", ID_MOTIVO_REEXPEDICION_SIESA)
                                ' F320_ID_SERVICIO (Alfanumerico(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SERVICIO", TIPO_SERVICIO_FACTURA_SIESA)
                                ' F320_ROWID (Entero(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ROWID", "1") ' Consecutivo por envío intCon
                                ' F350_CONSEC_DOCTO (Entero(8))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuFact)
                                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA) 'FCE=Contra Entrega, FCO=Contado
                                ' F350_ID_CO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA) 'B04=Contra Entrega, B25=Contado
                            End If

                            ' Valor Cargue
                            If dblValoCarg > 0 Then
                                ' Se envia la Línea negocio y Motivo para consultar Centro Costo, Unidad Negocio, Tipo Servicio
                                Call Consultar_Parametros_SIESA_Factura_Linea_Negocio(LINEA_NEGOCIO_PAQUETERIA, ID_MOTIVO_CARGE_ACARREO_LOCAL_SIESA)

                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentasServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' F320_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_DETALLE", strNotas)
                                ' F320_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_NOTAS", strNotas)
                                ' F320_VLR_DSCTO_1 (Decimal(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_DSCTO_1", "0")
                                ' F320_VLR_BRUTO (Decimal(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_BRUTO", dblValoCarg)
                                ' F320_CANTIDAD (Decimal(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_CANTIDAD", "1")
                                ' F320_ID_SUCURSAL_CLIENTE (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SUCURSAL_CLIENTE", ID_SUCURSAL_CLIENTE_FACTURA_SIESA) ' Constante
                                ' F320_ID_TERCERO_MOVTO (Alfanumerico(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_TERCERO_MOVTO", strIdenTercFact)
                                ' F320_ID_CCOSTO_MOVTO (Alfanumerico(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CCOSTO_MOVTO", CENTRO_COSTO_FACTURA_SIESA)
                                ' F320_ID_UN_MOVTO (Alfanumerico(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_UN_MOVTO", UNIDAD_NEGOCIO_FACTURA_SIESA)
                                ' F320_ID_CO_MOVTO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CO_MOVTO", CENTRO_OPERACION_FACTURA_SIESA)
                                ' F320_ID_MOTIVO (Alfanumerico(2)
                                ' FLETE = 01, MANEJO = 02, REEXPEDICION = 03, CARGUE-ACARREO LOCAL = 04, DESCARGUE-ACARREO DESTINO = 05
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_MOTIVO", ID_MOTIVO_CARGE_ACARREO_LOCAL_SIESA)
                                ' F320_ID_SERVICIO (Alfanumerico(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SERVICIO", TIPO_SERVICIO_FACTURA_SIESA)
                                ' F320_ROWID (Entero(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ROWID", "1") ' Consecutivo por envío intCon
                                ' F350_CONSEC_DOCTO (Entero(8))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuFact)
                                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA) 'FCE=Contra Entrega, FCO=Contado
                                ' F350_ID_CO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA) 'B04=Contra Entrega, B25=Contado
                            End If

                            ' Valor Descargue
                            If dblValoDesc > 0 Then
                                ' Se envia la Línea negocio y Motivo para consultar Centro Costo, Unidad Negocio, Tipo Servicio
                                Call Consultar_Parametros_SIESA_Factura_Linea_Negocio(LINEA_NEGOCIO_PAQUETERIA, ID_MOTIVO_DESCARGUE_ACARREO_DESTINO_SIESA)

                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentasServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' F320_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_DETALLE", strNotas)
                                ' F320_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_NOTAS", strNotas)
                                ' F320_VLR_DSCTO_1 (Decimal(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_DSCTO_1", "0")
                                ' F320_VLR_BRUTO (Decimal(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_BRUTO", dblValoDesc)
                                ' F320_CANTIDAD (Decimal(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_CANTIDAD", "1")
                                ' F320_ID_SUCURSAL_CLIENTE (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SUCURSAL_CLIENTE", ID_SUCURSAL_CLIENTE_FACTURA_SIESA) ' Constante
                                ' F320_ID_TERCERO_MOVTO (Alfanumerico(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_TERCERO_MOVTO", strIdenTercFact)
                                ' F320_ID_CCOSTO_MOVTO (Alfanumerico(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CCOSTO_MOVTO", CENTRO_COSTO_FACTURA_SIESA)
                                ' F320_ID_UN_MOVTO (Alfanumerico(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_UN_MOVTO", UNIDAD_NEGOCIO_FACTURA_SIESA)
                                ' F320_ID_CO_MOVTO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CO_MOVTO", CENTRO_OPERACION_FACTURA_SIESA)
                                ' F320_ID_MOTIVO (Alfanumerico(2)
                                ' FLETE = 01, MANEJO = 02, REEXPEDICION = 03, CARGUE-ACARREO LOCAL = 04, DESCARGUE-ACARREO DESTINO = 05
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_MOTIVO", ID_MOTIVO_DESCARGUE_ACARREO_DESTINO_SIESA)
                                ' F320_ID_SERVICIO (Alfanumerico(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SERVICIO", TIPO_SERVICIO_FACTURA_SIESA)
                                ' F320_ROWID (Entero(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ROWID", "1") ' Consecutivo por envío intCon
                                ' F350_CONSEC_DOCTO (Entero(8))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuFact)
                                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA) 'FCE=Contra Entrega, FCO=Contado
                                ' F350_ID_CO (Alfanumerico(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA) 'B04=Contra Entrega, B25=Contado
                            End If

                        End If
                    End If

                    If lonFormPago = FORMA_PAGO_FACTURA_CONTADO Then
                        ' CAJA  (CONTADO) 
                        xmlElementoRoot = xmlDocumento.DocumentElement
                        xmlElemento = xmlDocumento.CreateElement("Caja")
                        xmlElementoRoot.AppendChild(xmlElemento)
                        xmlElementoRoot = xmlDocumento.DocumentElement

                        ' F350_ID_CO (Alfanumerico (3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA)
                        ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA)
                        ' F350_CONSEC_DOCTO (Entero(8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuFact)
                        ' F358_ID_MEDIOS_PAGO (Alfanumerico(3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_MEDIOS_PAGO", MEDIO_PAGO_EFECTIVO) ' Constante
                        ' F_VLR_MEDIO_PAGO (Decimal(20))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F_VLR_MEDIO_PAGO", dblValoFact)
                        ' F_VLR_MEDIO_PAGO_LOCAL (Decimal(20))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F_VLR_MEDIO_PAGO_LOCAL", dblValoFact)
                        ' F358_ID_BANCO (Alfanumerico(10))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_BANCO", "")
                        ' F358_NRO_CHEQUE (Entero(8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NRO_CHEQUE", "0")
                        ' F358_NRO_CUENTA (Alfanumerico(25))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NRO_CUENTA", "")
                        ' F358_COD_SEGURIDAD (Alfanumerico(3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_COD_SEGURIDAD", "")
                        ' F358_NRO_AUTORIZACION (Alfanumerico(10))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NRO_AUTORIZACION", "")
                        ' F358_FECHA_VCTO (Alfanumerico(8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_FECHA_VCTO", "")
                        ' F358_REFERENCIA_OTROS (Alfanumerico(30))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_REFERENCIA_OTROS", "")
                        ' F358_FECHA_CONSIGNACION (Alfanumerico(8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_FECHA_CONSIGNACION", "")
                        ' F358_NOTAS (Alfanumerico(255))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strNotas)
                        ' F358_ID_CCOSTO (Alfanumerico(15))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CCOSTO", "")
                    End If

                    If lonFormPago = FORMA_PAGO_FACTURA_CONTRAENTREGA Then

                        ' CUOTAS CxC (CONTRA ENTREGA)
                        xmlElementoRoot = xmlDocumento.DocumentElement
                        xmlElemento = xmlDocumento.CreateElement("CuotasCxC")
                        xmlElementoRoot.AppendChild(xmlElemento)
                        xmlElementoRoot = xmlDocumento.DocumentElement

                        ' F353_FECHA_DSCTO_PP (Alfanumerico (8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechFact)
                        ' F353_FECHA_VCTO (Alfanumerico(8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechFact)
                        ' F350_CONSEC_DOCTO (Entero(8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuFact)
                        ' F350_ID_TIPO_DOCTO (Alfanumerico (3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA) 'FCE=Contra Entrega, FCO=Contado
                        ' F350_ID_CO (Alfanumerico(3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA) 'B04=Contra Entrega, B25=Contado

                    End If

                    strXML = xmlDocumento.InnerXml
                    Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                    ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                    Me.objGenericTransfer = New wsGenerarPlano()
                    Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Facturas_Contado_Contraentrega: XML: " & strXML)
                    strPlanoXML = objGenericTransfer.ImportarDatosXML(105794, "FACTURAS_CONTADO", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)
                    Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Facturas_Contado_Contraentrega: respuesta servicio " & strPlanoXML)
                    If strPlanoXML = "Importacion exitosa" Then

                        ' Actualizar en la Factura los campos Interfaz_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                        strSQL = "UPDATE Encabezado_Facturas SET Mensaje_Error = '', Interfaz_Contable_Factura = 1, Fecha_Interfase_Contable = getDate()"
                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND Numero = " & lonNumeFact
                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                    Else
                        strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)
                        Dim estado As Integer
                        If strErrorDetalleSIESA.ToLower().Contains("Documento venta servicios: El documento no fue procesado porque ya fue actualizado por otra importación".ToLower()) Then
                            estado = 1
                        Else
                            estado = 0
                        End If
                        ' Actualizar en la Factura los campos Interfaz_Contable_Factura = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                        strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = " & estado & ", Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND Numero = " & lonNumeFact
                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                    End If

                Else
                    strErrorDetalleSIESA = "El aforador no tiene una caja asociada"
                    ' Actualizar en la Factura los campos Interfaz_Contable_Factura = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                    strSQL = "UPDATE Encabezado_Facturas SET Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumeFact
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                End If





            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Facturas_Contado_Contraentrega: " & ex.Message)
        End Try
    End Sub

    Private Sub Consultar_CodigoAlterno(ByVal lonCodigoCaja As String)
        Try
            Dim dtsConsulta As DataSet

            strSQL = "select Codigo_Alterno from cajas where Codigo = " & lonCodigoCaja
            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    ID_CAJA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Alterno").ToString()
                Else
                    ID_CAJA = Nothing
                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró la CAJA con el codigo: " & lonCodigoCaja.ToString())
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_CodigoAlterno: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 18-DIC-2021
    ' Observaciones Creación: Función que consulta los parámetros SIESA usados en la integración Facturas Contado y Contraentrega

    ' Cambio #01: Jesus Cuadros
    ' Fecha Cambio #01: 18-ABR-2022
    ' Observaciones Cambio #01: Se adiciono la variable genérica SUCURSAL_TERCERO_SIESA

    Private Sub Consultar_Sucursales_SIESA_Perfiles_Terceros(ByVal lonPerfTerc As Long, ByVal lonTipConSer As Long, ByVal lonFormPagoVent As Long)
        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT * FROM Sucursales_SIESA_Perfiles_Terceros WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString()
            strSQL += " AND CATA_PETE_Codigo = " & lonPerfTerc.ToString()
            strSQL += " AND CATA_TICS_Codigo = " & lonTipConSer.ToString()
            strSQL += " AND CATA_FPVE_Codigo = " & lonFormPagoVent.ToString()
            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    ID_SUCURSAL_CLIENTE_FACTURA_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_SIESA").ToString()
                    SUCURSAL_TERCERO_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_SIESA").ToString()
                Else
                    ID_SUCURSAL_CLIENTE_FACTURA_SIESA = ""
                    SUCURSAL_TERCERO_SIESA = ""

                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró la sucursal SIESA para el perfil tercero " & lonPerfTerc.ToString())

                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Sucursales_SIESA_Perfiles_Terceros: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 18-DIC-2021
    ' Observaciones Creación: Función que consulta los parámetros SIESA usados en la integración Facturas Contado y Contraentrega
    ' Modifica: Jesus Cuadros
    ' Fecha Modificación: 19-FEB-2022
    ' Observaciones Modificación: Se adiciona el parámetro Motivo
    Private Sub Consultar_Parametros_SIESA_Factura_Linea_Negocio(ByVal intLineNego As Integer, ByVal intMotivo As Integer)
        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT * FROM Parametros_Factura_SIESA_Linea_Negocio WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString()
            strSQL += " AND LNTC_Codigo = " & intLineNego.ToString()
            strSQL += " AND ID_Motivo = " & intMotivo.ToString()

            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    CENTRO_COSTO_FACTURA_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Centro_Costo").ToString()
                    UNIDAD_NEGOCIO_FACTURA_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Unidad_Negocio").ToString()
                    TIPO_SERVICIO_FACTURA_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Tipo_Servicio").ToString()
                Else
                    CENTRO_COSTO_FACTURA_SIESA = ""
                    UNIDAD_NEGOCIO_FACTURA_SIESA = ""
                    TIPO_SERVICIO_FACTURA_SIESA = ""
                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró los parámetros SIESA de la factura para la linea de negocio " & intLineNego.ToString())
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Parametros_SIESA_Factura_Linea_Negocio: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 18-DIC-2021
    ' Observaciones Creación: Función que consulta el Código Alterno de la Oficina Enviada (Ej: Oficina Factura)

    Private Sub Consultar_Codigo_Alterno_Oficina_Factura(ByVal lonCodiOfic As Long)
        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT * FROM Oficinas WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString()
            strSQL += " AND Codigo = " & lonCodiOfic.ToString()
            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    CENTRO_OPERACION_FACTURA_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Alterno").ToString() ' Parametrizar en Forma pago Catalogo = 49 Campo2
                Else
                    CENTRO_OPERACION_FACTURA_SIESA = ""
                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró la oficina con código  " & lonCodiOfic.ToString)
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Parametros_Forma_Pago_Factura: " & ex.Message)
        End Try
    End Sub

    ' Autor: Rommel Rodriguez
    ' Fecha Creación: 30-DIC-2021
    ' Observaciones Creación: Función que consulta la caja del aforador
    ' Modifico: Jesus Cuadros
    ' Fecha Modificación: 19-FEB-2022
    ' Observaciones Modificación: Se adiciono el campo empresa y un espacio en la sentencia SQL porque generaba error

    Private Sub Consultar_Caja_Aforador(ByVal Aforador As Long, ByVal Oficina As Long, ByVal Usuario As Long)
        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT Codigo_Alterno FROM Cajas WHERE Codigo = (SELECT CAJA_Codigo FROM Usuario_Oficinas WHERE EMPR_Codigo = " & intCodigoEmpresa & " AND USUA_Codigo = " & Usuario & " AND OFIC_Codigo =" & Oficina & ") "
            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    ID_CAJA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Alterno").ToString()
                Else
                    ID_CAJA = ""

                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró la caja con los criterios Aforador: " & Aforador & " Oficina: " & Oficina)
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Caja_Aforador: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 18-DIC-2021
    ' Observaciones Creación: Rutina que consulta parámetros SIESA definidos en el catálog de Forma de Pago Venta

    Private Sub Consultar_Parametros_Forma_Pago_Factura(ByVal lonFormPago As Long)
        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT * FROM Valor_Catalogos WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString()
            strSQL += " AND Codigo = " & lonFormPago.ToString()
            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    TIPO_CLIENTE_FACTURA_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Campo2").ToString() ' ct 0001 - ctx 0002 
                    CONDICION_PAGO_FACTURA_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Campo3").ToString() ' CT 00D - CXT 05D 
                    TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Campo4").ToString() ' CT Y CXT FCO 
                Else
                    TIPO_CLIENTE_FACTURA_SIESA = ""
                    CONDICION_PAGO_FACTURA_SIESA = ""
                    TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA = ""
                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró en el catálogo los parámetros de la forma de pago " & lonFormPago.ToString())
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Parametros_Forma_Pago_Factura: " & ex.Message)
        End Try

    End Sub

#End Region

#Region "Proceso Cumplido Contraentrega"

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 10-JUN-2021
    ' Observaciones Creación: Se consulta los Cumplidos ContraEntrega

    Private Sub Proceso_Cumplido_ContraEntrega()
        Try
            Dim dtsCompCont As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Cumplido_ContraEntrega")
                strSQL = "gsp_Consultar_Cumplidos_Contraentrega_SIESA_ENCOE " & intCodigoEmpresa.ToString
                dtsCompCont = objGeneral.Retorna_Dataset(strSQL)

                If dtsCompCont.Tables.Count > 0 Then

                    If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Cumplido_ContraEntrega(dtsCompCont)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Cumplido_ContraEntrega: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 10-JUN-2021
    ' Observaciones Creación: Se construye XML con base en especificación entregada por GenericTransfer 08-JUN-2021

    ' Cambio #1: Jesús Cuadros
    ' Fecha Cambio #1: 15-MAR-2022
    ' Observaciones Cambio #1: Se parametrizan valores fijos para Centro Operación, Tipo Documento

    ' Cambio #2: Jesús Cuadros
    ' Fecha Cambio #2: 31-MAR-2022
    ' Observaciones Cambio #2:  Se ajusta para consultar código alterno de oficina realiza venta, se parametriza unidad negocio y documento cruce
    '                           Se implementa llamado a la función Consultar_Sucursales_SIESA_Perfiles_Terceros para consultar la sucursal de acuerdo al perfil tercero entrega

    Private Sub Crear_XML_Cumplido_ContraEntrega(ByVal dtsCompCont As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = "", strErrorDetalleSIESA As String

            Dim lonNumero As Long, lonNumeGuia As Long, strFechGuia As String, lonNumePlan As Long, strIdenTercDest As String, strCodiAlteOfic As String
            Dim strNotas As String, dblValoGuia As Double, datFechVenc As Date, strFechVenc As String, strIdenTercEntr As String, strFechEntr As String
            Dim lonPerfTercEntr As Long ' Perfil Tercero Entrega

            Dim xmlDocumento As XmlDocument
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumero = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeGuia = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Guia").ToString()
                strCodiAlteOfic = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Alterno_Oficina").ToString() ' El Código Alterno de la oficina factura venta corresponde al Centro Operación SIESA
                strFechGuia = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Guia").ToString())
                strFechEntr = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Entrega").ToString())
                datFechVenc = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Guia").ToString()
                strFechVenc = datFechVenc.AddDays(3).ToString ' Sumar 3 días
                strFechVenc = Retorna_Fecha_Formato_SIESA(strFechVenc)
                strIdenTercDest = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Destinatario").ToString()
                If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Empleado_Entrega").ToString() <> "0" Then ' PENDIENTE: GUARDAR TERCERO ENTREGA Y No. PLANILLA ENTREGA EN CUMPLIDO
                    strIdenTercEntr = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Empleado_Entrega").ToString()
                    lonPerfTercEntr = PETE_EMPLEADO
                Else
                    strIdenTercEntr = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Conductor_Entrega").ToString()
                    lonPerfTercEntr = PETE_CONDUCTOR
                End If
                lonNumePlan = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Ultima_Planilla").ToString()
                dblValoGuia = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Total_Guia").ToString()
                strNotas = "Guia No. " & lonNumeGuia.ToString() & " Planilla No. " & lonNumePlan

                Call Consultar_Catalogo_Tipo_Conector_SIESA(CATA_TICS_CUMPLIDO_CONTRAENTREGA)

                xmlDocumento = New XmlDocument
                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' DOCUMENTO CONTABLE
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("DocumentoContable")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic)
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_SIESA)
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeGuia)
                ' F350_FECHA (Alfanumerico(8)) 
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechEntr) ' Fecha Entrega Mercancia
                ' F350_ID_TERCERO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenTercDest)
                ' F350_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strNotas)

                ' MOVIMIENTO CxC 
                Call Consultar_Detalle_Parametrizacion_Cumplido_Contraentrega(CATA_NACC_CREDITO)

                Call Consultar_Sucursales_SIESA_Perfiles_Terceros(PETE_CLIENTE, CATA_TICS_CUMPLIDO_CONTRAENTREGA, FORMA_PAGO_FACTURA_CONTRAENTREGA)

                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("MovimientoCxC")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic)
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_SIESA)
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeGuia)
                ' F351_ID_AUXILIAR (Alfanumerico(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", CUENTA_AUXILIAR_SIESA)
                ' F351_ID_TERCERO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTercDest)
                ' F351_ID_CO_MOV (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiAlteOfic)
                ' F351_ID_UN (Alfanumerico(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                ' F351_VALOR_DB (Decimal(21))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                ' F351_VALOR_CR (Decimal(21))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoGuia)
                ' F351_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                ' F353_ID_SUCURSAL (Alfanumerico(3)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", ID_SUCURSAL_CLIENTE_FACTURA_SIESA)
                ' F353_ID_TIPO_DOCTO_CRUCE (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_TIPO_DOCTO_CRUCE", DOCUMENTO_CRUCE_SIESA)
                ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumeGuia)
                ' F353_NRO_CUOTA_CRUCE (Entero(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "0")
                ' F353_FECHA_VCTO (Alfanumerico(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechVenc) '
                ' F353_FECHA_DSCTO_PP (Alfanumerico(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechVenc)
                ' F354_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strNotas)

                ' MOVIMIENTO CxC 
                Call Consultar_Detalle_Parametrizacion_Cumplido_Contraentrega(CATA_NACC_DEBITO)

                Call Consultar_Sucursales_SIESA_Perfiles_Terceros(lonPerfTercEntr, CATA_TICS_CUMPLIDO_CONTRAENTREGA, FORMA_PAGO_FACTURA_CONTRAENTREGA)

                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("MovimientoCxC")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic)
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_SIESA)
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeGuia)
                ' F351_ID_AUXILIAR (Alfanumerico(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", CUENTA_AUXILIAR_SIESA)
                ' F351_ID_TERCERO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTercEntr)
                ' F351_ID_CO_MOV (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiAlteOfic)
                ' F351_ID_UN (Alfanumerico(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                ' F351_VALOR_DB (Decimal(21))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoGuia)
                ' F351_VALOR_CR (Decimal(21))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                ' F351_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                ' F353_ID_SUCURSAL (Alfanumerico(3)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", ID_SUCURSAL_CLIENTE_FACTURA_SIESA)
                ' F353_ID_TIPO_DOCTO_CRUCE (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_TIPO_DOCTO_CRUCE", DOCUMENTO_CRUCE_SIESA)
                ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumeGuia)
                ' F353_NRO_CUOTA_CRUCE (Entero(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "0")
                ' F353_FECHA_VCTO (Alfanumerico(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechVenc) '
                ' F353_FECHA_DSCTO_PP (Alfanumerico(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechVenc)
                ' F354_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strNotas)

                strXML = xmlDocumento.InnerXml
                Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                Me.objGenericTransfer = New wsGenerarPlano()
                strPlanoXML = objGenericTransfer.ImportarDatosXML(105794, "FACTURAS_CONTADO", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                If strPlanoXML = "Importacion exitosa" Then

                    ' Actualizar en Cumplido_Remesas los campos Interfaz_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                    strSQL = "UPDATE Cumplido_Remesas SET Mensaje_Error = '', Interfaz_Contable_Factura = 1, Fecha_Interfase_Contable = getDate()"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumero

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                Else
                    strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                    ' Actualizar en Cumplido_Remesas los campos Interfaz_Contable_Factura = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                    strSQL = "UPDATE Cumplido_Remesas SET Interfaz_Contable_Factura = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumero

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                End If

            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Cumplido_ContraEntrega: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Modificación: 15-MAR-2022
    ' Observaciones: Se consulta la Parametrización de la Cuenta PUC parametrizada para el Cumplido Contra Entrega
    Private Sub Consultar_Detalle_Parametrizacion_Cumplido_Contraentrega(ByVal lonNatuCuenCont As Long)

        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT DEPC.PLUC_Codigo, PLUC.Codigo_Cuenta, DEPC.Codigo_Anexo, DEPC.Prefijo"
            strSQL += " FROM Encabezado_Parametrizacion_Contables AS ENPC"
            strSQL += " INNER JOIN Detalle_Parametrizacion_Contables AS DEPC"
            strSQL += " ON ENPC.EMPR_Codigo = DEPC.EMPR_Codigo"
            strSQL += " AND ENPC.Codigo = DEPC.ENPC_Codigo"
            strSQL += " INNER JOIN Plan_Unico_Cuentas AS PLUC"
            strSQL += " ON DEPC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DEPC.PLUC_Codigo = PLUC.Codigo"
            strSQL += " WHERE ENPC.EMPR_codigo = " & intCodigoEmpresa.ToString()
            strSQL += " AND ENPC.Codigo_Alterno = 10000" ' Se asigno este código temporalmente para identificar el movimiento contable de Cumplido Contraentrega
            strSQL += " AND DEPC.CATA_NACC_Codigo = " & lonNatuCuenCont.ToString() ' Nataraleza 

            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    CUENTA_AUXILIAR_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Cuenta").ToString()
                    UNIDAD_NEGOCIO_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Anexo").ToString()
                    DOCUMENTO_CRUCE_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Prefijo").ToString()
                Else
                    CUENTA_AUXILIAR_SIESA = ""
                    UNIDAD_NEGOCIO_SIESA = ""
                    DOCUMENTO_CRUCE_SIESA = ""
                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró la cuenta en el movimiento contable de Cumplido Contraentrega")
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Detalle_Parametrizacion_Cumplido_Contraentrega: " & ex.Message)
        End Try

    End Sub
#End Region

#Region "Proceso Legalizacion Recaudo"

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 22-JUN-2021
    ' Observaciones Creación: Se consulta la Legalizacion Recaudo

    ' Cambio #1: Jesus Cuadros
    ' Fecha Cambio #1: 01-JUL-2021
    ' Observaciones Cambio #1: Se modifica para enviar la Legalización Recaudo con sus detalles Efectivo y Bancos

    Private Sub Proceso_Legalizacion_Recaudo()
        Try
            Dim dtsConsulta As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Legalizacion_Recaudo")
                strSQL = "gsp_Consultar_Encabezado_Legalizacion_Recaudo_SIESA_ENCOE " & intCodigoEmpresa.ToString & ", 26" ' PRUEBA No. 1000020
                dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

                If dtsConsulta.Tables.Count > 0 Then

                    If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Legalización Recaudo a Procesar: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Proceso_Legalizacion_Recaudo(dtsConsulta)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Legalizacion_Recaudo: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 22-JUN-2021
    ' Observaciones: 

    ' Cambio #1: Jesus Cuadros
    ' Fecha Cambio #1: 09-JUL-2021
    ' Observaciones Cambio #1: Se estructura el envío de toda la Legalización Recaudo

    Private Sub Crear_XML_Proceso_Legalizacion_Recaudo(ByVal dtsEncabezado As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = ""
            Dim strErrorDetalleSIESA As String, strNotas As String, dblValor As Double
            Dim lonNumLegRec As Long, lonNumDocLegRec As Long, lonNumeReme As Long, lonNumeDocuReme As Long, strFechPago As String, strIdenCond As String, strIdenAfor As String
            Dim strCodiOficLega As String, strCodiOficLegaSIESA As String, lonFormPagoReca As Long, lonFormPagoVent As Long, lonNumLegRecAnt As Long = 0, lonNumLegRecSig As Long = 0
            Dim dtsDetalle As DataSet, strFechReme As String, strFechLega As String, strDocuPago As String, strTipoDocuPago As String

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumLegRec = dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Legalizacion_Recaudo")
                lonNumDocLegRec = dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento_Legalizacion_Recaudo").ToString()
                strFechLega = Retorna_Fecha_Formato_SIESA(dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Legalizacion_Recaudo").ToString())
                strCodiOficLega = dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Oficina_Legaliza").ToString()
                strCodiOficLegaSIESA = dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_SIESA_Oficina_Legaliza").ToString()
                strNotas = "LEGALIZACION RECAUDO No." & lonNumDocLegRec.ToString()

                xmlDocumento = New XmlDocument
                'xmlDeclaracion = xmlDocumento.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                'xmlDocumento.AppendChild(xmlDeclaracion)
                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' DOCUMENTO CONTABLE
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("Documentocontable")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina del Tesorero que legaliza el recaudo
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec) ' Documento Legalización Recaudo
                ' F350_FECHA (Alfanumerico(YYYYMMDD))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechLega) ' Fecha Legalización Recaudo
                ' F350_ID_TERCERO (Alfanumerico(15))
                strIdenCond = "79540540"
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenCond) ' AJUSTAR: Guardar en Legalización Recaudo Guía Responsable Oficina (Agencista) o el Aforador
                ' F350_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strNotas)

                strSQL = "gsp_Consultar_Detalle_Legalizacion_Recaudo_SIESA_ENCOE " & intCodigoEmpresa.ToString() & "," & lonNumLegRec.ToString()
                dtsDetalle = objGeneral.Retorna_Dataset(strSQL)

                ' Se recorre el detalle con las Guías de la Legalización Recaudo
                For intCon1 = 0 To dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                    lonNumeReme = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Numero_Remesa")
                    lonNumeDocuReme = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Numero_Documento_Remesa").ToString()
                    strFechPago = Retorna_Fecha_Formato_SIESA(dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Fecha_Pago").ToString())
                    strFechReme = Retorna_Fecha_Formato_SIESA(dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Fecha_Remesa").ToString())
                    strIdenCond = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Identificacion_Conductor").ToString()
                    strIdenAfor = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Identificacion_Aforador").ToString()
                    lonFormPagoVent = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Forma_Pago_Venta") ' 4902-Contado, 4903-Contra Entrega
                    lonFormPagoReca = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Forma_Pago_Recaudo") ' 5101-Efectivo, 5102-Transferencia, 5103-Cheque
                    dblValor = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Valor")

                    ' LEGALIZACION RECAUDO EFECTIVO
                    If lonFormPagoReca = FORMA_PAGO_LEGALIZACION_RECAUDO_EFECTIVO Then

                        If lonFormPagoVent = FORMA_PAGO_FACTURA_CONTADO Then

                            strNotas = "LEGALIZACION RECAUDO EFECTIVO CONTADO, No. GUIA: " & lonNumeDocuReme.ToString

                            ' CAJA ENTREGA RECAUDO
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Caja")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Facturación Venta Guía
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "11050501" ' PARAMETRIZAR 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' Oficina Facturación Venta Guía
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_ID_FE (Alfanumerico(10))
                            strValorCampo = "1132" ' PARAMETRIZAR Flujo de Efectivo
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", strValorCampo)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValor)
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F358_ID_CAJA (Alfanumerico(3))
                            strValorCampo = "R04" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CAJA", strValorCampo) ' AJUSTAR: Guardar en la Guia la Caja del responsable Venta
                            ' F358_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strNotas)

                            ' CAJA RECIBE RECAUDO 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Caja")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Tesorero Legaliza Recaudo
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "11050501" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' Oficina Tesorero Legaliza Recaudo
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_ID_FE (Alfanumerico(10))
                            strValorCampo = "1132" ' PARAMETRIZAR Flujo de Efectivo 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", strValorCampo)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValor)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F358_ID_CAJA (Alfanumerico(3))
                            strValorCampo = "RO4" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CAJA", strValorCampo) ' AJUSTAR: Guardar en la Legalización Recaudo Guias la Caja del Tesorero
                            ' F358_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strNotas)

                        End If

                        If lonFormPagoVent = FORMA_PAGO_FACTURA_CONTRAENTREGA Then

                            strNotas = "LEGALIZACION RECAUDO EFECTIVO CONTRAENTREGA, No. GUIA: " & lonNumeDocuReme.ToString

                            ' MOVIMIENTO CxC 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxC")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Facturación Venta Guía
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "13050520" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenAfor) ' PENDIENTE Responsable de realizar la Entrega (Aforador o Agencista)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' Oficina Facturación Venta Guía
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_VALOR_DB (Decimal(21))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValor)
                            ' F351_VALOR_CR (Decimal(21))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "")
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F353_ID_SUCURSAL (Alfanumerico(3)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "001") ' PENDIENTE 
                            ' F353_ID_TIPO_DOCTO_CRUCE (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_TIPO_DOCTO_CRUCE", "LCE") ' PARAMETRIZAR
                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocLegRec)
                            ' F353_NRO_CUOTA_CRUCE (Entero(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "0")
                            ' F353_FECHA_VCTO (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechReme)
                            ' F353_FECHA_DSCTO_PP (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechReme)
                            ' F354_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strNotas)

                            ' CAJA RECIBE RECAUDO 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Caja")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Tesorero Legaliza Recaudo
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "11050501" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' Oficina Tesorero Legaliza Recaudo
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_ID_FE (Alfanumerico(10))
                            strValorCampo = "1103" ' PARAMETRIZAR Flujo de Efectivo 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", strValorCampo)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValor)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F358_ID_CAJA (Alfanumerico(3))
                            strValorCampo = "RO4" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CAJA", strValorCampo) ' PENDIENTE Caja del Tesorero, AJUSTAR: Guardar en la Legalización Recaudo Guias la Caja del Tesorero 
                            ' F358_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strNotas)
                        End If

                    End If

                    ' LEGALIZACION RECAUDO BANCOS 
                    If lonFormPagoReca = FORMA_PAGO_LEGALIZACION_RECAUDO_TRANSFERENCIA Or lonFormPagoReca = FORMA_PAGO_LEGALIZACION_RECAUDO_CHEQUE Then

                        ' AJUSTE: Revision 14-JUL-2021
                        ' Generar un registro de  MovimientoContable Agrupado por Documento Pago independiente para Contado y Contraentrega y no por cada Guía

                        If lonFormPagoVent = FORMA_PAGO_FACTURA_CONTADO Then

                            strNotas = "LEGALIZACION RECAUDO BANCOS CONTADO, No. GUIA: " & lonNumeDocuReme.ToString

                            ' CAJA RECIBE RECAUDO 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Caja")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' PENDIENTE Oficina Factura Venta
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "11050501" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' PENDIENTE Oficina Factura Venta
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_ID_FE (Alfanumerico(10))
                            strValorCampo = "4202" ' PARAMETRIZAR Flujo de Efectivo 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", strValorCampo)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValor)
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F358_ID_CAJA (Alfanumerico(3))
                            strValorCampo = "AN2" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CAJA", strValorCampo) ' PENDIENTE Caja Realizo Venta
                            ' F358_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strNotas)

                        End If

                        If lonFormPagoVent = FORMA_PAGO_FACTURA_CONTRAENTREGA Then

                            strNotas = "LEGALIZACION RECAUDO BANCOS CONTRAENTREGA, No. GUIA: " & lonNumeDocuReme.ToString

                            ' MOVIMIENTO CxC 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxC")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Recibe Recaudo
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "13050520" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenAfor) ' PENDIENTE Aforador o Entrega Oficina
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' PENDIENTE Oficina Factura Contra Entrega
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_VALOR_DB (Decimal(21))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal(21))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValor)
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F353_ID_SUCURSAL (Alfanumerico(3)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "001") ' PENDIENTE Sucursal del Conductor
                            ' F353_ID_TIPO_DOCTO_CRUCE (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_TIPO_DOCTO_CRUCE", "LCE") ' PARAMETRIZAR CONSTANTE
                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocLegRec)
                            ' F353_NRO_CUOTA_CRUCE (Entero(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "0")
                            ' F353_FECHA_VCTO (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechReme)
                            ' F353_FECHA_DSCTO_PP (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechReme)
                            ' F354_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strNotas)

                        End If

                    End If

                Next

                ' Para la contrapardida en LEGALIZACION RECAUDO BANCOS se genera un registro de MovimientoContable Agrupado por:
                ' Forma Pago Recaudo (Transferencia, Cheque) y Documento Pago  
                strSQL = "SELECT CATA_FPDC_Codigo AS Forma_Pago_Recaudo, Documento_Pago, SUM(Valor) AS Valor FROM Detalle_Legalizacion_Recaudo_Guias"
                strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString()
                strSQL += " And ELRG_Numero = " & lonNumLegRec.ToString()
                strSQL += " AND CATA_FPDC_Codigo IN (5102, 5103)" 'TRANSFERENCIA, CHEQUE
                strSQL += " GROUP BY CATA_FPDC_Codigo, Documento_Pago"

                dtsDetalle = objGeneral.Retorna_Dataset(strSQL)
                If dtsDetalle.Tables.Count > 0 Then
                    If dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                        For intCon1 = 0 To dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            'lonFormPagoVent = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Forma_Pago_Venta") ' 4902-Contado, 4903-Contra Entrega
                            lonFormPagoReca = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Forma_Pago_Recaudo") ' 5102-Transferencia, 5103-Cheque
                            dblValor = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Valor")
                            strDocuPago = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Documento_Pago")
                            If lonFormPagoReca = FORMA_PAGO_LEGALIZACION_RECAUDO_TRANSFERENCIA Then
                                strTipoDocuPago = "CG"
                            ElseIf lonFormPagoReca = FORMA_PAGO_LEGALIZACION_RECAUDO_CHEQUE Then
                                strTipoDocuPago = "CH"
                            End If

                            ' MOVIMIENTO CONTABLE  

                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F351_NOTAS (Alfanumerico(255))
                            strNotas = "LEGALIZACION RECAUDO BANCOS SUMATORIA FORMA PAGO RECAUDO"
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F351_NRO_DOCTO_BANCO (Entero(8))a
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NRO_DOCTO_BANCO", strDocuPago) ' No Transferencia o No. Cheque se ingresa en la Legalización
                            ' F351_DOCTO_BANCO (Alfanumerico(2))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_DOCTO_BANCO", strTipoDocuPago)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValor)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                            ' F351_ID_FE (Alfanuerico(10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", "4203") ' 4203-Transferencia 4102-Consignación Cheque
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' Oficina Tesorero Recibe Recaudo
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenAfor) ' PENDIENTE: Tercero que entrega el dinero, se envio por correo pregunta?
                            ' F351_ID_AUXILIAR (Entero (10))
                            strValorCampo = "11100503"
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo) ' PENDIENTE: Solicitar la Cuenta Bancaria en el detalle de la Legalización Guías?
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            'F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Tesorero Recibe Recaudo

                        Next

                    End If
                End If

                strXML = xmlDocumento.InnerXml
                Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                Me.objGenericTransfer = New wsGenerarPlano()
                strPlanoXML = objGenericTransfer.ImportarDatosXML(105885, "LEGALIZACION RECAUDO", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                If strPlanoXML = "Importacion exitosa" Then

                    ' Actualizar el Detalle_Legalizacion_Recaudo_Guias con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                    strSQL = "UPDATE Encabezado_Legalizacion_Recaudo_Guias SET Interfaz_Contable = 1, Fecha_Interfase_Contable = getDate()"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumLegRec

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                Else
                    strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                    ' Actualizar el Detalle_Legalizacion_Recaudo_Guias con Interfaz_Contable = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                    strSQL = "UPDATE Encabezado_Legalizacion_Recaudo_Guias SET Interfaz_Contable = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumLegRec

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                End If

                lonNumLegRecAnt = lonNumLegRec

            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Proceso_Legalizacion_Recaudo: " & ex.Message)
        End Try

    End Sub

#End Region

#Region "Proceso Legalizacion Gastos Conductor"

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 01-JUL-2021
    ' Observaciones Creación: Se consulta la Legalizacion Gastos Conductor

    Private Sub Proceso_Legalizacion_Gastos_Conductor()
        Try
            Dim dtsConsulta As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Legalizacion_Gastos_Conductor")
                strSQL = "gsp_Consultar_Encabezado_Legalizacion_Gastos_SIESA_ENCOE " & intCodigoEmpresa.ToString & ", 269" ' TEMPORAL PRUEBAS No. Legalizacion 1000009 (Masivo) y 209-1000028(Paqueteria)
                dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

                If dtsConsulta.Tables.Count > 0 Then

                    If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Legalización Gastos Conductor a Procesar: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Proceso_Legalizacion_Gastos_Conductor(dtsConsulta)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Legalizacion_Gastos_Conductor: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 21-JUL-2021
    ' Observaciones Creación: Se crea XML de la Legalizacion Gastos Conductor

    ' Cambio #01: Jesus Cuadros
    ' Fecha Cambio 01: 02-MAR-2022
    ' Observaciones Cambio #01: Se realizaron ajustes para parametrizar valores que se encontraban fijos

    ' Cambio #02: Jesus Cuadros
    ' Fecha Cambio #02: 25-MAR-2022
    ' Observaciones Cambio #02: Se ajusto el manejo de la CxP = Total Descuentos y se implemento el manejo de Saldo a Favor Empresa y Saldo Favor Conductor

    Private Sub Crear_XML_Proceso_Legalizacion_Gastos_Conductor(ByVal dtsEncaLegaGast As DataSet)
        Try
            Dim strFechLega As String, lonCodiCond As Long, lonNumPlaDes As Long, strPlacVehi As String, strIdenCond As String
            Dim dblValoGast As Double, dblSaldFavoEmpr As Double, dblSaldFavoCond As Double
            Dim strIdenProv As String, lonCodConGas As Long, strNombConc As String, dblValoConcDebi As Double, dblValoConcCred As Double
            Dim intCon As Integer, intCon2 As Integer, strValorCampo As String, lonNumeDocuLega As Long, lonNumeLega As Long, strCtaPucCon As String
            Dim strXML As String, strPlanoXML As String, strErrorDetalleSIESA As String, strCodiAlteOfic As String

            Dim xmlDocumento As XmlDocument
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeLega = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocuLega = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                strFechLega = Retorna_Fecha_Formato_SIESA(dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                lonCodiCond = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("TERC_Codigo_Conductor").ToString()
                strIdenCond = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Conductor").ToString()
                strCodiAlteOfic = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Alterno_Oficina").ToString() ' El código alterno de la oficina corresponde al centro de operacion SIESA
                dblSaldFavoEmpr = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Saldo_Favor_Empresa")
                dblSaldFavoCond = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Saldo_Favor_Conductor")

                Call Consultar_Catalogo_Tipo_Conector_SIESA(CATA_TICS_LEGALIZACION_GASTOS_CONDUCTOR)

                xmlDocumento = New XmlDocument
                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' DOCUMENTO CONTABLE
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("DocumentoContable")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                'F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic) ' El código alterno de la oficina corresponde al centro de operacion SIESA
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_SIESA)
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuLega.ToString) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria
                ' F350_FECHA (Alfanumerico(YYYYMMDD))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechLega)
                ' F350_ID_TERCERO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenCond)
                ' F350_NOTAS (Alfanumerico(2000))
                strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strValorCampo)

                ' Consultar Detalle Legalizacion Gastos
                Dim dtsDetaLegaGast As DataSet

                strSQL = "gsp_Consultar_Detalle_Legalizacion_Gastos_SIESA_ENCOE " & intCodigoEmpresa.ToString() & ", " & lonNumeLega.ToString()
                dtsDetaLegaGast = objGeneral.Retorna_Dataset(strSQL)

                If dtsDetaLegaGast.Tables.Count > 0 Then
                    If dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        ' Recorrer los Conceptos de Gasto de la Planilla Legalización
                        For intCon2 = 0 To dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            strIdenProv = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Identificacion_Proveedor").ToString()
                            lonCodConGas = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Codigo_Concepto")
                            strNombConc = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Nombre_Concepto").ToString()
                            dblValoConcDebi = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Debito")
                            dblValoConcCred = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Credito")
                            lonNumPlaDes = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("ENPD_Numero_Documento")
                            strPlacVehi = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Placa").ToString()
                            strCtaPucCon = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Codigo_Cuenta").ToString()

                            Call Consultar_Detalle_Parametrizacion_Concepto_Legalizacion_Gastos_Conductor(lonCodConGas)

                            ' MOVIMIENTO CONTABLE (Conceptos de Gasto) 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_GASTOS_CONDUCTOR_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuLega) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCtaPucCon)
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenProv)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiAlteOfic)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", CENTRO_COSTO_CONCEPTO_GASTO_SIESA)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoConcDebi)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoConcCred)
                            ' F351_BASE_GRAVABLE (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", "0")
                            ' F351_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No. PLANILLA:" & lonNumPlaDes & ", PLACA:" & strPlacVehi & ", CONCEPTO GASTO:" & strNombConc
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)

                        Next

                        If dblSaldFavoEmpr Then

                            Call Consultar_Detalle_Parametrizacion_Concepto_Legalizacion_Gastos_Conductor(CODIGO_CONCEPTO_LEGALIZACION_GASTOS_SALDO_FAVOR_EMPRESA)

                            ' MOVIMIENTO CONTABLE (Conceptos de Gasto) 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_GASTOS_CONDUCTOR_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuLega)
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", CUENTA_AUXILIAR_SIESA)
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenProv) ' PREGUNTA: La identificación debería ser la de ENCOE?
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiAlteOfic)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", CENTRO_COSTO_CONCEPTO_GASTO_SIESA)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblSaldFavoEmpr)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", 0)
                            ' F351_BASE_GRAVABLE (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", "0")
                            ' F351_NOTAS (Alfanumerico(255))
                            strNombConc = "SALDO A FAVOR EMPRESA"
                            strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No. PLANILLA:" & lonNumPlaDes & ", CONCEPTO GASTO:" & strNombConc
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                        End If

                        If dblSaldFavoCond Then

                            Call Consultar_Detalle_Parametrizacion_Concepto_Legalizacion_Gastos_Conductor(CODIGO_CONCEPTO_LEGALIZACION_GASTOS_SALDO_FAVOR_CONDUCTOR)

                            ' MOVIMIENTO CONTABLE (Conceptos de Gasto) 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_GASTOS_CONDUCTOR_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuLega)
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", CUENTA_AUXILIAR_SIESA)
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond) ' PREGUNTA: La identificación debería ser la del conductor?
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiAlteOfic)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", CENTRO_COSTO_CONCEPTO_GASTO_SIESA)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", 0)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblSaldFavoCond)
                            ' F351_BASE_GRAVABLE (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", "0")
                            ' F351_NOTAS (Alfanumerico(255))
                            strNombConc = "SALDO A FAVOR CONDUCTOR"
                            strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No. PLANILLA:" & lonNumPlaDes & ", CONCEPTO GASTO:" & strNombConc
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                        End If

                        ' JCB:30-MAR-2022: Se comentarea la sección relacionada con Total Gastos ya se se toma es Valor Anticipos
                        If dblValoGast > 0 Then

                            'Call Consultar_Detalle_Parametrizacion_CxP_Legalizacion_Gastos_Conductor()

                            '' MOVIMIENTO CxP
                            'xmlElementoRoot = xmlDocumento.DocumentElement
                            'xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                            'xmlElementoRoot.AppendChild(xmlElemento)
                            'xmlElementoRoot = xmlDocumento.DocumentElement

                            ''F350_ID_CO (Alfanumerico(3))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic)
                            '' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_GASTOS_CONDUCTOR_SIESA)
                            '' F350_CONSEC_DOCTO (Entero(8))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuLega.ToString)
                            '' F351_ID_AUXILIAR (Entero (10))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", CUENTA_AUXILIAR_SIESA)
                            '' F350_ID_TERCERO (Alfanumerico(15))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond)
                            '' F351_ID_CO_MOV (Alfanumerico(3))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiAlteOfic)
                            '' F351_ID_UN (Alfanumerico(20))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", strCodiAlteOfic)
                            '' F351_ID_CCOSTO (Alfanumerico(15)) 
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            '' F351_VALOR_DB (Decimal)
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            '' F351_VALOR_CR (Decimal)
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoGast.ToString)
                            '' F351_NOTAS (Alfanumerico(255))
                            'strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No.PLANILLA:" & lonNumPlaDes.ToString
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                            '' F353_ID_SUCURSAL (Alfanumerico(3))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_GASTO_CONDUCTOR_SIESA)
                            '' F353_PREFIJO_CRUCE (Alfanumerico(20))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", DOCUMENTO_CRUCE_GASTO_CONDUCTOR_SIESA)
                            '' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumPlaDes.ToString) ' No Planilla Despacho
                            '' F353_NRO_CUOTA_CRUCE (Entero(3))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "1")
                            '' F353_ID_FE (Alfanumerico(10))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "")
                            '' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLega)
                            '' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLega)
                            '' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLega)
                            '' F354_NOTAS (Alfanumerico(255))
                            'strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No.PLANILLA:" & lonNumPlaDes.ToString
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                        End If

                        ' JCB:25-MAR-2022: Se comentarea la Consulta Egresos Anticipos y se Asigna a la sección MovimientoCxP el Valor Gastos de la Legalización
                        ' JCB:30-MAR-2021: Se descomentarea la sección
#Region "                        Consultar Egresos Anticipo Planilla"
                        ' Se consultan los Comprobante Egreso Anticipo relacionado con la Legalizacion Gastos y el Conductor para crear una CxP por cada una
                        Dim dtsCompEgreAnti As DataSet, lonNumeCompEgreAnti As Long, dblValoCompEgreAnti As Double, strFechCompEgre As String

                        strSQL = "SELECT ENDC.Numero, ENDC.Fecha, ENDC.Valor_Pago_Recaudo_Total, TERC.Numero_Identificacion"
                        strSQL += " FROM Encabezado_Documento_Comprobantes ENDC, Terceros As TERC"
                        strSQL += " WHERE ENDC.EMPR_Codigo = TERC.EMPR_Codigo"
                        strSQL += " AND ENDC.TERC_Codigo_Titular = TERC.Codigo"
                        strSQL += " AND ENDC.EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND ENDC.TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO
                        strSQL += " AND ENDC.ELGC_Numero = " & lonNumeLega.ToString() ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                        strSQL += " AND ENDC.TERC_Codigo_Titular = " & lonCodiCond.ToString
                        strSQL += " AND ENDC.Estado = 1"
                        strSQL += " AND ENDC.Anulado = 0"

                        dtsCompEgreAnti = objGeneral.Retorna_Dataset(strSQL)

                        If dtsCompEgreAnti.Tables.Count > 0 Then
                            If dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                Call Consultar_Detalle_Parametrizacion_CxP_Legalizacion_Gastos_Conductor()

                                Call Consultar_Sucursales_SIESA_Perfiles_Terceros(PETE_CONDUCTOR, CATA_TICS_LEGALIZACION_GASTOS_CONDUCTOR, FORMA_PAGO_NOAPLICA)

                                For intCont3 = 0 To dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                                    lonNumeCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero").ToString()
                                    dblValoCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Valor_Pago_Recaudo_Total").ToString()
                                    strIdenCond = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero_Identificacion").ToString()
                                    strFechCompEgre = Retorna_Fecha_Formato_SIESA(dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Fecha").ToString())

                                    ' MOVIMIENTO CxP
                                    xmlElementoRoot = xmlDocumento.DocumentElement
                                    xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                    xmlElementoRoot.AppendChild(xmlElemento)
                                    xmlElementoRoot = xmlDocumento.DocumentElement

                                    'F350_ID_CO (Alfanumerico(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiAlteOfic)
                                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_GASTOS_CONDUCTOR_SIESA)
                                    ' F350_CONSEC_DOCTO (Entero(8))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuLega.ToString)
                                    ' F351_ID_AUXILIAR (Entero (10))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", CUENTA_AUXILIAR_SIESA)
                                    ' F350_ID_TERCERO (Alfanumerico(15))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond)
                                    ' F351_ID_CO_MOV (Alfanumerico(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiAlteOfic)
                                    ' F351_ID_UN (Alfanumerico(20))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", strCodiAlteOfic)
                                    ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                    ' F351_VALOR_DB (Decimal)
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                                    ' F351_VALOR_CR (Decimal)
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoCompEgreAnti.ToString)
                                    ' F351_NOTAS (Alfanumerico(255))
                                    strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No. EGRESO:" & lonNumeCompEgreAnti.ToString & ", No.PLANILLA:" & lonNumPlaDes.ToString
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                                    ' F353_ID_SUCURSAL (Alfanumerico(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_TERCERO_SIESA)
                                    ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", DOCUMENTO_CRUCE_GASTO_CONDUCTOR_SIESA)
                                    ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumPlaDes.ToString) ' No Planilla Despacho
                                    ' F353_NRO_CUOTA_CRUCE (Entero(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "1")
                                    ' F353_ID_FE (Alfanumerico(10))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "")
                                    ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLega)
                                    ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLega)
                                    ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLega)
                                    ' F354_NOTAS (Alfanumerico(255))
                                    strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No. EGRESO:" & lonNumeCompEgreAnti.ToString & ", No.PLANILLA:" & lonNumPlaDes.ToString
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                                Next

                            End If
                        End If
#End Region

                        strXML = xmlDocumento.InnerXml
                        Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                        ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                        Me.objGenericTransfer = New wsGenerarPlano()
                        strPlanoXML = objGenericTransfer.ImportarDatosXML(105686, "LEGALIZACION GASTOS CONDUCTOR", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                        If strPlanoXML = "Importacion exitosa" Then

                            ' Actualizar Encabezado_Legalizacion_Recaudo_Guias con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                            strSQL = "UPDATE Encabezado_Legalizacion_Gastos_Conductor SET Interfaz_Contable_Legalizacion = 1, Fecha_Interfase_Contable = getDate()"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeLega

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        Else
                            strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                            ' Actualizar Encabezado_Legalizacion_Recaudo_Guias con Interfaz_Contable = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                            strSQL = "UPDATE Encabezado_Legalizacion_Gastos_Conductor SET Interfaz_Contable_Legalizacion = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeLega

                            ' Ejecutar UPDATE
                            'lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        End If

                    End If

                End If



            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Crear_XML_Proceso_Legalizacion_Gastos_Conductor: " & ex.Message)

        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Modificación: 11-MAR-2022
    ' Observaciones: Se consulta la CxP parametrizada en Parametrizacion Contable Legalizacion Gastos, campos Cuenta PUC, Sucursal y Documento Cruce

    ' Cambio #01: Jesus Cuadros
    ' Fecha Cambio #01: 01-ABR-2022
    ' Observaciones Cambio #01: Se adiciono el WHERE el Codigo del registro en Detalle_Parametrizacion_Contable relacionado con la CxP 

    Private Sub Consultar_Detalle_Parametrizacion_CxP_Legalizacion_Gastos_Conductor()

        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT DEPC.PLUC_Codigo, PLUC.Codigo_Cuenta, DEPC.Codigo_Anexo, DEPC.Prefijo"
            strSQL += " FROM Encabezado_Parametrizacion_Contables AS ENPC"
            strSQL += " INNER JOIN Detalle_Parametrizacion_Contables AS DEPC"
            strSQL += " ON ENPC.EMPR_Codigo = DEPC.EMPR_Codigo"
            strSQL += " AND ENPC.Codigo = DEPC.ENPC_Codigo"
            strSQL += " INNER JOIN Plan_Unico_Cuentas AS PLUC"
            strSQL += " ON DEPC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DEPC.PLUC_Codigo = PLUC.Codigo"
            strSQL += " WHERE ENPC.EMPR_codigo = " & intCodigoEmpresa.ToString()
            strSQL += " AND ENPC.CATA_TIDG_Codigo = 3607" ' Tipo Documento Genera Movimiento Contable Legalización Gastos Conductor
            strSQL += " AND DEPC.Codigo = 516" ' Codigo del registro en Detalle_Parametrizacion_Contable relacionado con la CxP 
            'strSQL += " AND DEPC.CATA_NACC_Codigo = 2902" ' Nataraleza Crédito - CxP Contrapartida Legalización Gastos 

            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    CUENTA_AUXILIAR_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Cuenta").ToString()
                    SUCURSAL_GASTO_CONDUCTOR_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Anexo").ToString()
                    DOCUMENTO_CRUCE_GASTO_CONDUCTOR_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Prefijo").ToString()
                Else
                    CUENTA_AUXILIAR_SIESA = ""
                    SUCURSAL_GASTO_CONDUCTOR_SIESA = ""
                    DOCUMENTO_CRUCE_GASTO_CONDUCTOR_SIESA = ""
                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró en el movimiento contable de Gastos Conductor la cuenta PUC de la CxP")
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Detalle_Parametrizacion_CxP_Legalizacion_Gastos_Conductor: " & ex.Message)
        End Try

    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 11-MAR-2022
    ' Observaciones Creación: Se consulta el Detalle Parametrizacion Contable con el código del concepto de gasto enviado

    Private Sub Consultar_Detalle_Parametrizacion_Concepto_Legalizacion_Gastos_Conductor(ByVal lonCodConGas As Long)
        Try
            Dim dtsConsulta As DataSet

            strSQL = "SELECT DEPC.PLUC_Codigo, PLUC.Codigo_Cuenta, DEPC.Sufijo_Codigo_Anexo, DEPC.Codigo_Anexo, DEPC.Prefijo"
            strSQL += " FROM Encabezado_Parametrizacion_Contables AS ENPC"
            strSQL += " INNER JOIN Detalle_Parametrizacion_Contables AS DEPC"
            strSQL += " ON ENPC.EMPR_Codigo = DEPC.EMPR_Codigo"
            strSQL += " AND ENPC.Codigo = DEPC.ENPC_Codigo"
            strSQL += " INNER JOIN Plan_Unico_Cuentas AS PLUC"
            strSQL += " ON DEPC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DEPC.PLUC_Codigo = PLUC.Codigo"
            strSQL += " WHERE ENPC.EMPR_codigo = " & intCodigoEmpresa.ToString()
            strSQL += " AND ENPC.CATA_TIDG_Codigo = 3607" ' Tipo Documento Genera Movimiento Contable = Legalización Gastos Conductor
            strSQL += " AND DEPC.CATA_VTPC_Codigo = " & lonCodConGas.ToString() ' Código Concepto Legalización Gastos

            dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsulta.Tables.Count > 0 Then
                If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    CENTRO_COSTO_CONCEPTO_GASTO_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Sufijo_Codigo_Anexo").ToString()
                    CUENTA_AUXILIAR_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Cuenta").ToString()
                    'SUCURSAL_CONCEPTO_GASTO_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Anexo").ToString()
                    'DOCUMENTO_CRUCE_CONCEPTO_GASTO_SIESA = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Prefijo").ToString()
                Else
                    CENTRO_COSTO_CONCEPTO_GASTO_SIESA = ""
                    CUENTA_AUXILIAR_SIESA = ""
                    'SUCURSAL_CONCEPTO_GASTO_SIESA = ""
                    'DOCUMENTO_CRUCE_CONCEPTO_GASTO_SIESA = ""
                    Me.objGeneral.Guardar_Mensaje_Log("No se encontró el detalle del movimiento contable Legalización Gastos para el código Concepto " & lonCodConGas.ToString())
                End If
            End If

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Detalle_Parametrizacion_Concepto_Legalizacion_Gastos_Conductor: " & ex.Message)
        End Try

    End Sub

#End Region

#Region "Proceso Liquidacion Planilla Despachos"

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 27-AGO-2021
    ' Observaciones Creación: Se consulta las Liquidaciones Despacho
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Proceso_Liquidacion_Planilla_Despachos()
        Try
            Dim dtsConsulta As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Liquidacion_Despachos")
                strSQL = "gsp_Consultar_Encabezado_Liquidacion_Planilla_Despachos_SIESA_ENCOE " & intCodigoEmpresa.ToString & ", 40255" ' TEMPORAL PRUEBAS No. Liquidación Paqueteria 1000020
                dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

                If dtsConsulta.Tables.Count > 0 Then

                    If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Liquidación Despachos a Procesar: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Proceso_Liquidacion_Planilla_Despachos(dtsConsulta)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Liquidacion_Despachos: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 27-AGO-2021
    ' Observaciones Creación: Se crea XML de la Liquidación Planilla Despachos

    ' Modifica: Jesús Cuadros
    ' Fecha Modificación: 13-SEP-2021
    ' Observaciones Modificación: Nuevo XML

    ' Modifica: Jesús Cuadros
    ' Fecha Modificación: 19-OCT-2021
    ' Observaciones Modificación: Nuevo XML

    Private Sub Crear_XML_Proceso_Liquidacion_Planilla_Despachos(ByVal dtsEncaLiquPlan As DataSet)
        Try
            Dim intTipoLiquDesp As Integer, lonNumeLiqu As Long, lonNumeDocuLiqu As Long, strFechLiqu As String, lonNumePlanDesp As Long, lonNumDocPlaDes As Long
            Dim strPlacVehi As String, strCodiVehi As String, lonCodiCond As Long, strIdenCond As String, strIdenTene As String
            Dim dblValorPagar As Double, dblValFleTra As Double, dblValoImpuLiqu As Double
            Dim intCodiConc As String, strNombConc As String, dblValoConcDebi As Double, dblValoConcCred As Double, strCodCtaPuc As String, lonCodiDocuComp As Long
            Dim strXML As String, strPlanoXML As String, strErrorDetalleSIESA As String
            Dim intCon As Integer, intCon1 As Integer, strValorCampo As String, intFL As Integer
            Dim dtsConsulta As DataSet, dblPorcTeneVehi As Double, dblValRetFue As Double, dblValRetIca As Double, dblValConLiq As Double

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                intTipoLiquDesp = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("TIDO_Codigo").ToString() ' M
                lonNumeLiqu = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocuLiqu = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                lonNumePlanDesp = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Planilla").ToString()
                lonNumDocPlaDes = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento_Planilla").ToString()
                'lonNumDocPlaDes = lonNumDocPlaDes + 2 ' ' TEMPORAL PRUEBA
                strFechLiqu = Retorna_Fecha_Formato_SIESA(dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                lonCodiCond = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Conductor").ToString()
                strIdenCond = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Conductor").ToString()
                strIdenTene = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tenedor")
                dblValorPagar = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Pagar")
                dblValFleTra = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Flete_Transportador")
                dblValoImpuLiqu = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Impuestos")
                strPlacVehi = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Placa_Vehiculo")
                strCodiVehi = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Vehiculo")
                dblValRetFue = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Retefuente")
                dblValRetIca = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Reteica")
                dblValConLiq = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Conceptos_Liquidacion")

                xmlDocumento = New XmlDocument

                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' CONSULTAR LOS TENEDORES DEL VEHICULO PARA CREAR UN COMPROBANTE POR CADA TENEDOR CON BASE EN SU % PROPIEDAD
                strSQL = "SELECT VEPR.TERC_Codigo_Tenedor, VEPR.Porcentaje_Propiedad, TERC.Numero_Identificacion AS Identificacion_Tenedor"
                strSQL += " FROM Vehiculo_Propietarios AS VEPR, Terceros AS TERC"
                strSQL += " WHERE VEPR.EMPR_Codigo = TERC.EMPR_Codigo"
                strSQL += " AND VEPR.TERC_Codigo_Tenedor = TERC.Codigo"
                strSQL += " AND VEPR.EMPR_codigo = " & intCodigoEmpresa.ToString()
                strSQL += " AND VEPR.VEHI_Codigo = " & strCodiVehi

                dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

                If dtsConsulta.Tables.Count > 0 Then
                    If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        For intCon1 = 0 To dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            ' DOCUMENTO CONTABLE
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("DocumentoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            strIdenTene = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Identificacion_Tenedor")
                            dblPorcTeneVehi = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Porcentaje_Propiedad")
                            intFL = intCon1 + 1

                            'F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA) ' PENDIENTE: Oficina Liquida
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria
                            ' F350_FECHA (Alfanumerico(YYYYMMDD))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechLiqu)
                            ' F350_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenTene)
                            ' F350_NOTAS (Alfanumerico(2000))
                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString() & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString()
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strValorCampo)


                            ' MOVIMIENTO CONTABLE - FLETE TRANSPORTADOR
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "75050501") ' FIJO PARAMETRIZAR
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "300301") ' FIJO PARAMETRIZAR
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValFleTra * dblPorcTeneVehi / 100)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                            ' F351_BASE_GRAVABLE (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", "0")
                            ' F351_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString()
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)


                            ' MOVIMIENTO CONTABLE - RETENCION ICA
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "236801032") ' FIJO PARAMETRIZAR
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "") ' FIJO PARAMETRIZAR
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValRetIca * dblPorcTeneVehi / 100)
                            ' F351_BASE_GRAVABLE (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", dblValFleTra) ' PREGUNTAR: Los otros conceptos que afectan base?
                            ' F351_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString()
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)


                            ' MOVIMIENTO CONTABLE - RETENCION FUENTE
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "23652501") ' FIJO PARAMETRIZAR
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "") ' FIJO PARAMETRIZAR
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValRetFue * dblPorcTeneVehi / 100)
                            ' F351_BASE_GRAVABLE (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", dblValFleTra) ' PREGUNTAR: Los otros conceptos?
                            ' F351_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString()
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)


                            ' MOVIMIENTO CxP - VALOR A PAGAR
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            'F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "23354501") ' FIJO PARAMETRIZAR
                            ' F350_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal)

                            dblValorPagar = dblValFleTra - dblValRetFue - dblValRetIca + 500000 ' PENDIENTE Ajustar este cálculo Mayor Valor Flete

                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValorPagar * dblPorcTeneVehi / 100)
                            ' F351_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.LIQUIDACION:" & lonNumeDocuLiqu.ToString & ", No.PLANILLA:" & lonNumDocPlaDes.ToString
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                            ' F353_ID_SUCURSAL (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "178") ' PENDIENTE PARAMETRIZAR SUCURSAL SIESA EN VEHICULOS 
                            ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocPlaDes.ToString) ' No Planilla Despacho
                            ' F353_ID_FE (Alfanumerico(10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "1214") ' FIJO PARAMETRIZAR
                            ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLiqu)
                            ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLiqu)
                            ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLiqu)
                            ' F354_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)


                            ' CONSULTAR LOS CONCEPTOS DE LA LIQUIDACION
                            Dim dtsDetaLiquPlan As DataSet

                            strSQL = "gsp_Consultar_Detalle_Liquidacion_Planilla_Despachos_SIESA_ENCOE " & intCodigoEmpresa.ToString() & ", " & lonNumeLiqu.ToString()
                            dtsDetaLiquPlan = objGeneral.Retorna_Dataset(strSQL)

                            If dtsDetaLiquPlan.Tables.Count > 0 Then
                                If dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                    For intCon2 = 0 To dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                                        intCodiConc = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Codigo_Concepto").ToString()
                                        strNombConc = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Nombre_Concepto").ToString()
                                        dblValoConcDebi = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Debito").ToString()
                                        dblValoConcCred = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Credito").ToString()
                                        strCodCtaPuc = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Codigo_Cuenta").ToString()
                                        lonCodiDocuComp = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Codigo_Documento_Comprobante").ToString() ' Comprobante Egreso Anticipo

                                        If intCodiConc = CONCEPTO_FONDO_RESPONSABILIDAD_LIQUIDACION_PLANILLA_DESPACHOS Then

                                            ' Causación Fondo Responsabilidad
                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strCodCtaPuc = "28101504" ' PENDIENTE Parametrizar Concepto
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCodCtaPuc)
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0") ' PREGUNTA: Conceptos que suman?
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoConcDebi * dblPorcTeneVehi / 100)
                                            ' F351_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                                            ' F353_ID_SUCURSAL (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "PRV") ' PREGUNTA: SUCURSAL SIESA QUE CORRESPONDE AL VEHICULO DE LA PLANILLA?
                                            ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intCon1.ToString())
                                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocPlaDes.ToString) ' No Planilla Despacho
                                            ' F353_ID_FE (Alfanumerico(10))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "1214") ' FIJO PARAMETRIZAR
                                            ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLiqu)
                                            ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLiqu)
                                            ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLiqu)
                                            ' F354_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)


                                            ' Abono Fondo Responsabilidad
                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strCodCtaPuc = "23354501" ' PENDIENTE Parametrizar Concepto
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCodCtaPuc)
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoConcDebi * dblPorcTeneVehi / 100)
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                                            ' F351_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                                            ' F353_ID_SUCURSAL (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "178") ' PENDIENTE PARAMETRIZAR SUCURSAL VEHICULO
                                            ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "FDR")
                                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocPlaDes.ToString) ' No Planilla Despacho
                                            ' F353_ID_FE (Alfanumerico(10))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "1214") ' FIJO PARAMETRIZAR
                                            ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLiqu)
                                            ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLiqu)
                                            ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLiqu)
                                            ' F354_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)


                                        ElseIf intCodiConc = CONCEPTO_MAYOR_VALOR_FLETE_LIQUIDACION_PLANILLA_DESPACHOS Then

                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strCodCtaPuc = "75050501" ' PENDIENTE Parametrizar en Concepto
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCodCtaPuc)
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "300301") ' FIJO PARAMETRIZAR
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoConcCred * dblPorcTeneVehi / 100)
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", 0)
                                            ' F351_BASE_GRAVABLE (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", 0)
                                            ' F351_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)


                                        ElseIf intCodiConc = CONCEPTO_MENOR_VALOR_FLETE_LIQUIDACION_PLANILLA_DESPACHOS Then

                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strCodCtaPuc = "75050501" ' PENDIENTE Parametrizar en Concepto
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCodCtaPuc)
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "300301") ' FIJO PARAMETRIZAR
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", 0)
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoConcDebi * dblPorcTeneVehi / 100)
                                            ' F351_BASE_GRAVABLE (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", 0)
                                            ' F351_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)

                                        End If

                                    Next

                                End If
                            End If

                            ' CONSULTAR LOS COMOPROBANTES EGRESO DE ANTICIPO RELACIONADOS A LA PLANILLA LIQUIDADA
                            Dim dtsCompEgreAnti As DataSet, lonNumeCompEgreAnti As Long, dblValoCompEgreAnti As Double, strFechCompEgre As String

                            strSQL = "SELECT ENDC.Numero, ENDC.Fecha, ENDC.Valor_Pago_Recaudo_Total, TERC.Numero_Identificacion"
                            strSQL += " FROM Encabezado_Documento_Comprobantes ENDC, Terceros As TERC"
                            strSQL += " WHERE ENDC.EMPR_Codigo = TERC.EMPR_Codigo"
                            strSQL += " AND ENDC.TERC_Codigo_Titular = TERC.Codigo"
                            strSQL += " AND ENDC.EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND ENDC.TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO
                            ' FALTA Guardar en los Comprobantes de Egreso el ELPD_Numero como se tiene para ELGC_Numero (Encabezado Legalizacion Gastos Conductor)
                            If intTipoLiquDesp = TIDO_LIQUIDACION_PLANILLA_PAQUETERIA Then
                                strSQL += " AND ENDC.CATA_DOOR_Codigo = 2613" '2613=Anticipo Planilla Paqueteria 
                            Else
                                strSQL += " AND ENDC.CATA_DOOR_Codigo IN (2604, 2605)"  '2604=Anticipos Planilla Masivo 2605=SobreAnticipo 
                            End If
                            strSQL += " AND ENDC.Documento_Origen = " & lonNumDocPlaDes.ToString() ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                            strSQL += " AND ENDC.TERC_Codigo_Titular = " & lonCodiCond.ToString
                            'strSQL += " AND ENDC.Estado = 1"
                            'strSQL += " AND ENDC.Anulado = 0"

                            dtsCompEgreAnti = objGeneral.Retorna_Dataset(strSQL)

                            If dtsCompEgreAnti.Tables.Count > 0 Then
                                If dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                    For intCont3 = 0 To dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                                        lonNumeCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero").ToString()
                                        dblValoCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Valor_Pago_Recaudo_Total").ToString()
                                        strIdenCond = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero_Identificacion").ToString()
                                        strFechCompEgre = Retorna_Fecha_Formato_SIESA(dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Fecha").ToString())

                                        ' MOVIMIENTO CxP ANTICIPO CONDUCTOR
                                        xmlElementoRoot = xmlDocumento.DocumentElement
                                        xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                        xmlElementoRoot.AppendChild(xmlElemento)
                                        xmlElementoRoot = xmlDocumento.DocumentElement

                                        'F350_ID_CO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                        ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                        ' F350_CONSEC_DOCTO (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                        ' F351_ID_AUXILIAR (Entero (10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "13301502") ' FIJO PARAMETRIZAR
                                        ' F350_ID_TERCERO (Alfanumerico(15))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond)
                                        ' F351_ID_CO_MOV (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                        ' F351_ID_UN (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                        ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                        ' F351_VALOR_DB (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                                        ' F351_VALOR_CR (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoCompEgreAnti * dblPorcTeneVehi / 100)
                                        ' F353_ID_SUCURSAL (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "ANT") ' PREGUNTA: SUCURSAL SIESA CONDUCTOR? RPTA: EL CONDUCTOR TIENE UNA SUCURSAL ASOCIADA
                                        ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "ANT")
                                        ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocPlaDes.ToString) ' No Planilla Despacho
                                        ' F353_ID_FE (Alfanumerico(10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "")
                                        ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLiqu)
                                        ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLiqu)
                                        ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLiqu)
                                        ' F354_NOTAS (Alfanumerico(255))
                                        strValorCampo = "No. EGRESO:" & lonNumeCompEgreAnti.ToString & ", No.PLANILLA:" & lonNumDocPlaDes.ToString
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                                        ' MOVIMIENTO CxP ANTICIPO PROPIETARIO
                                        xmlElementoRoot = xmlDocumento.DocumentElement
                                        xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                        xmlElementoRoot.AppendChild(xmlElemento)
                                        xmlElementoRoot = xmlDocumento.DocumentElement

                                        'F350_ID_CO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                        ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                        ' F350_CONSEC_DOCTO (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                        ' F351_ID_AUXILIAR (Entero (10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "23354501") ' FIJO PARAMETRIZAR
                                        ' F350_ID_TERCERO (Alfanumerico(15))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                                        ' F351_ID_CO_MOV (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                        ' F351_ID_UN (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                        ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                        ' F351_VALOR_DB (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoCompEgreAnti * dblPorcTeneVehi / 100)
                                        ' F351_VALOR_CR (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                                        ' F353_ID_SUCURSAL (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "178") ' PENDIENTE PARAMETRIZAR SUCURSAL VEHICULO
                                        ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "ANT")
                                        ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocPlaDes.ToString) ' No Planilla Despacho
                                        ' F353_ID_FE (Alfanumerico(10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "")
                                        ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLiqu)
                                        ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLiqu)
                                        ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLiqu)
                                        ' F354_NOTAS (Alfanumerico(255))
                                        strValorCampo = "No. EGRESO:" & lonNumeCompEgreAnti.ToString & ", No.PLANILLA:" & lonNumDocPlaDes.ToString
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)
                                    Next

                                End If
                            End If

                        Next

                        strXML = xmlDocumento.InnerXml
                        Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                        ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                        Me.objGenericTransfer = New wsGenerarPlano()
                        strPlanoXML = objGenericTransfer.ImportarDatosXML(111646, "Causacion pago de flete", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                        If strPlanoXML = "Importacion exitosa" Then

                            ' Actualizar Encabezado_Liquidacion_Planilla_Despachos con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                            strSQL = "UPDATE Encabezado_Liquidacion_Planilla_Despachos SET Interfaz_Contable = 1, Fecha_Interfase_Contable = getDate()"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeLiqu

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        Else
                            strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                            ' Actualizar Encabezado_Liquidacion_Planilla_Despachos con Interfaz_Contable = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                            strSQL = "UPDATE Encabezado_Liquidacion_Planilla_Despachos SET Interfaz_Contable = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeLiqu

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                End If
            End If
            End If

            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Crear_XML_Proceso_Legalizacion_Gastos_Conductor: " & ex.Message)

        End Try
    End Sub

#End Region

#Region "Proceso Orden Venta Servicio"

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 20-OCT-2021
    ' Observaciones Creación: Se crea XML de la Orden Venta Servicio

    Private Sub Proceso_Orden_Venta_Servicio()
        Try
            Dim dtsConsulta As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Orden_Venta_Servicio")
                strSQL = "gsp_Consultar_Encabezado_Facturas_SIESA_ENCOE " & intCodigoEmpresa.ToString & ", 131430" ' Factura 2000577
                dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

                If dtsConsulta.Tables.Count > 0 Then

                    If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Factura a Procesar: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Proceso_Orden_Venta_Servicio(dtsConsulta)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Orden_Venta_Servicio: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 20-OCT-2021
    ' Observaciones Creación: Se crea XML de la Orden Venta Servicio

    Private Sub Crear_XML_Proceso_Orden_Venta_Servicio(ByVal dtsEncFact As DataSet)
        Try
            Dim lonNumeFact As Long, lonNumeDocuFact As Long, strFechFact As String, lonCodiClie As Long, intCon3 As Integer
            Dim strIdenClie As String, strIdenFact As String, strIdenComeClie As String, strObservaciones As String
            Dim strXML As String, strPlanoXML As String, strErrorDetalleSIESA As String, dtsDetaFact As DataSet
            Dim dblCantReme As Double, dblPesoReme As Double, strOrigen As String, strDestino As String, lonNumeReme As Long
            Dim dblValoFlet As Double, dblValoMane As Double, dblValoReex As Double, dblValoCarg As Double, dblValoDesc As Double

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeFact = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocuFact = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                strFechFact = Retorna_Fecha_Formato_SIESA(dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                lonCodiClie = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Cliente").ToString()
                strIdenClie = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Cliente").ToString()
                strIdenFact = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Facturar")
                strIdenComeClie = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Comercial")
                strObservaciones = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones")

                xmlDocumento = New XmlDocument
                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' CONSULTAR DETALLE FACTURA
                strSQL = "gsp_Consultar_Detalle_Facturas_SIESA_ENCOE " & intCodigoEmpresa.ToString() & ", " & lonNumeFact.ToString()
                dtsDetaFact = objGeneral.Retorna_Dataset(strSQL)

                If dtsDetaFact.Tables.Count > 0 Then
                    If dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                        For intCon2 = 0 To dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            lonNumeReme = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Numero_Documento").ToString
                            lonNumeReme = lonNumeReme + 10 ' TEMPORAL PRUEBA
                            strOrigen = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Ciudad_Origen").ToString
                            strDestino = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Ciudad_Destino").ToString
                            dblCantReme = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Cantidad_Cliente")
                            dblPesoReme = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Peso_Cliente")
                            dblValoFlet = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Flete")
                            dblValoMane = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Seguro")
                            dblValoReex = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Reexpedicion")
                            dblValoCarg = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Cargue")
                            dblValoDesc = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Descargue")
                            strObservaciones = "Guía:" & lonNumeReme.ToString() & ", Origen:" & strOrigen & ", Destino:" & strDestino

                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("OrdenVentaServicios")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            'F310_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_NOTAS", strObservaciones)
                            ' F310_NUMERO_ORDEN_COMPRA (Alfanumerico(12))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_NUMERO_ORDEN_COMPRA", lonNumeReme.ToString())
                            ' F310_ID_TIPO_CLI (Alfanumerico(4))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_CLI", "0003") ' FIJO CONSTANTE
                            ' F310_ID_COND_PAGO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_COND_PAGO", "30D") ' FIJO CONSTANTE
                            ' F310_REFERENCIA (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_REFERENCIA", lonNumeDocuFact.ToString())
                            ' F310_ID_TERCERO_VENDEDOR (Alfanumerico(15))
                            strIdenComeClie = "800209179" ' TEMPORAL PRUEBA TERCERO ENCOE
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TERCERO_VENDEDOR", strIdenComeClie)
                            ' F310_ID_SUCURSAL_CLI (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_SUCURSAL_CLI", "CRD") ' FIJO CONSTANTE
                            ' F310_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TERCERO", strIdenClie)
                            ' F310_FECHA (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_FECHA", strFechFact)
                            ' F310_CONSEC_DOCTO (Entero)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                            ' F310_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV") ' FIJO CONSTANTE
                            ' F310_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)

                            If dblValoFlet > 0 Then
                                ' Sección VALOR FLETE
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentaServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' f318_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_DETALLE", strObservaciones)
                                ' f318_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_NOTAS", strObservaciones)
                                ' f318_VLR_BRUTO (DECIMAL(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_VLR_BRUTO", dblValoFlet)
                                ' f318_CANTIDAD (DECIMAL(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_CANTIDAD", dblCantReme)
                                ' F318_ID_SUCURSAL_CLIENTE (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F318_ID_SUCURSAL_CLIENTE", "CRD") ' FIJO CONSTANTE
                                ' f318_ID_TERCERO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_TERCERO_MOVTO", strIdenClie)
                                ' f318_ID_CCOSTO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CCOSTO_MOVTO", "210101") ' FIJO CONSTANTE
                                ' f318_ID_UN_MOVTO (ALFANUMERICO (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_UN_MOVTO", "200") ' FIJO CONSTANTE
                                ' f318_ID_CO_MOVTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CO_MOVTO", CENTRO_OPERACION_SIESA)
                                ' f318_ID_MOTIVO (ALFANUMERICO(2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_MOTIVO", "01") ' FIJO CONSTANTE
                                ' PENDIENTE PARAMETRIZAR ID_MOTIVO POR TIPO SERVICIO Y GENERAR SECCION APARTE CON CADA VALOR
                                ' FLETE = 01
                                ' MANEJO = 02
                                ' REEXPEDICION = 03
                                ' CARGUE-ACARREO LOCAL = 04
                                ' DESCARGUE-ACARREO DESTINO = 05
                                ' f318_ID_SERVICIO (ALFANUMERICO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_SERVICIO", "vs450501") ' FIJO CONSTANTE 
                                ' f318_ROWID (ENTERO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ROWID", "1") ' FIJO CONSTANTE
                                ' F310_CONSEC_DOCTO (Entero)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                                ' F310_ID_TIPO_DOCTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV")
                                ' F310_ID_CO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)
                            End If

                            If dblValoMane > 0 Then
                                ' Sección VALOR MANJEO
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentaServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' f318_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_DETALLE", strObservaciones)
                                ' f318_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_NOTAS", strObservaciones)
                                ' f318_VLR_BRUTO (DECIMAL(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_VLR_BRUTO", dblValoMane)
                                ' f318_CANTIDAD (DECIMAL(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_CANTIDAD", dblCantReme)
                                ' F318_ID_SUCURSAL_CLIENTE (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F318_ID_SUCURSAL_CLIENTE", "CRD") ' FIJO CONSTANTE
                                ' f318_ID_TERCERO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_TERCERO_MOVTO", strIdenClie)
                                ' f318_ID_CCOSTO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CCOSTO_MOVTO", "210101") ' FIJO CONSTANTE
                                ' f318_ID_UN_MOVTO (ALFANUMERICO (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_UN_MOVTO", "200") ' FIJO CONSTANTE
                                ' f318_ID_CO_MOVTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CO_MOVTO", CENTRO_OPERACION_SIESA)
                                ' f318_ID_MOTIVO (ALFANUMERICO(2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_MOTIVO", "02") ' FIJO CONSTANTE MANEJO
                                ' f318_ID_SERVICIO (ALFANUMERICO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_SERVICIO", "vs450501") ' FIJO CONSTANTE 
                                ' f318_ROWID (ENTERO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ROWID", "2") ' PENDIENTE Variable Fila Unica y Consecutiva
                                ' F310_CONSEC_DOCTO (Entero)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                                ' F310_ID_TIPO_DOCTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV")
                                ' F310_ID_CO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)
                            End If

                            If dblValoReex > 0 Then
                                ' Sección VALOR REEXPEDICION
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentaServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                strObservaciones = "Guía:" & lonNumeReme.ToString() & ", Origen:" & strOrigen & ", Destino:" & strDestino
                                ' f318_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_DETALLE", strObservaciones)
                                ' f318_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_NOTAS", strObservaciones)
                                ' f318_VLR_BRUTO (DECIMAL(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_VLR_BRUTO", dblValoReex)
                                ' f318_CANTIDAD (DECIMAL(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_CANTIDAD", dblCantReme)
                                ' F318_ID_SUCURSAL_CLIENTE (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F318_ID_SUCURSAL_CLIENTE", "CRD") ' FIJO CONSTANTE
                                ' f318_ID_TERCERO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_TERCERO_MOVTO", strIdenClie)
                                ' f318_ID_CCOSTO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CCOSTO_MOVTO", "210101") ' FIJO CONSTANTE
                                ' f318_ID_UN_MOVTO (ALFANUMERICO (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_UN_MOVTO", "200") ' FIJO CONSTANTE
                                ' f318_ID_CO_MOVTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CO_MOVTO", CENTRO_OPERACION_SIESA)
                                ' f318_ID_MOTIVO (ALFANUMERICO(2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_MOTIVO", "03") ' FIJO CONSTANTE REEXPEDICION
                                ' f318_ID_SERVICIO (ALFANUMERICO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_SERVICIO", "vs450501") ' FIJO CONSTANTE 
                                ' f318_ROWID (ENTERO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ROWID", "3") ' PENDIENTE Variable Fila Unica y Consecutiva
                                ' F310_CONSEC_DOCTO (Entero)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                                ' F310_ID_TIPO_DOCTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV")
                                ' F310_ID_CO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)
                            End If

                            If dblValoCarg > 0 Then
                                ' Sección VALOR ACARREO LOCAL
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentaServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                strObservaciones = "Guía:" & lonNumeReme.ToString() & ", Origen:" & strOrigen & ", Destino:" & strDestino
                                ' f318_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_DETALLE", strObservaciones)
                                ' f318_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_NOTAS", strObservaciones)
                                ' f318_VLR_BRUTO (DECIMAL(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_VLR_BRUTO", dblValoCarg)
                                ' f318_CANTIDAD (DECIMAL(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_CANTIDAD", dblCantReme)
                                ' F318_ID_SUCURSAL_CLIENTE (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F318_ID_SUCURSAL_CLIENTE", "CRD") ' FIJO CONSTANTE
                                ' f318_ID_TERCERO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_TERCERO_MOVTO", strIdenClie)
                                ' f318_ID_CCOSTO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CCOSTO_MOVTO", "210101") ' FIJO CONSTANTE
                                ' f318_ID_UN_MOVTO (ALFANUMERICO (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_UN_MOVTO", "200") ' FIJO CONSTANTE
                                ' f318_ID_CO_MOVTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CO_MOVTO", CENTRO_OPERACION_SIESA)
                                ' f318_ID_MOTIVO (ALFANUMERICO(2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_MOTIVO", "04") ' FIJO CONSTANTE CARGUE
                                ' f318_ID_SERVICIO (ALFANUMERICO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_SERVICIO", "vs450501") ' FIJO CONSTANTE 
                                ' f318_ROWID (ENTERO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ROWID", "4") ' PENDIENTE Variable Fila Unica y Consecutiva
                                ' F310_CONSEC_DOCTO (Entero)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                                ' F310_ID_TIPO_DOCTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV")
                                ' F310_ID_CO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)
                            End If

                            If dblValoDesc > 0 Then
                                ' Sección VALOR ACARREO DESTINO
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentaServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                strObservaciones = "Guía:" & lonNumeReme.ToString() & ", Origen:" & strOrigen & ", Destino:" & strDestino
                                ' f318_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_DETALLE", strObservaciones)
                                ' f318_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_NOTAS", strObservaciones)
                                ' f318_VLR_BRUTO (DECIMAL(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_VLR_BRUTO", dblValoCarg)
                                ' f318_CANTIDAD (DECIMAL(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_CANTIDAD", dblCantReme)
                                ' F318_ID_SUCURSAL_CLIENTE (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F318_ID_SUCURSAL_CLIENTE", "CRD") ' FIJO CONSTANTE
                                ' f318_ID_TERCERO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_TERCERO_MOVTO", strIdenClie)
                                ' f318_ID_CCOSTO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CCOSTO_MOVTO", "210101") ' FIJO CONSTANTE
                                ' f318_ID_UN_MOVTO (ALFANUMERICO (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_UN_MOVTO", "200") ' FIJO CONSTANTE
                                ' f318_ID_CO_MOVTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CO_MOVTO", CENTRO_OPERACION_SIESA)
                                ' f318_ID_MOTIVO (ALFANUMERICO(2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_MOTIVO", "05") ' FIJO CONSTANTE DESCARGUE
                                ' f318_ID_SERVICIO (ALFANUMERICO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_SERVICIO", "vs450501") ' FIJO CONSTANTE 
                                ' f318_ROWID (ENTERO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ROWID", "5") ' PENDIENTE Variable Fila Unica y Consecutiva
                                ' F310_CONSEC_DOCTO (Entero)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                                ' F310_ID_TIPO_DOCTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV")
                                ' F310_ID_CO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)
                            End If

                        Next
                    End If
                End If

                strXML = xmlDocumento.InnerXml
                Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                Me.objGenericTransfer = New wsGenerarPlano()
                strPlanoXML = objGenericTransfer.ImportarDatosXML(105833, "Orden de Venta de Servicio", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                If strPlanoXML = "Importacion exitosa" Then

                    ' Actualizar Encabezado_Facturas con Interfase_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                    strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = 1, Fecha_Interfase_Contable = getDate()"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumeFact

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                Else
                    strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)
                    Dim estado As Integer
                    If strErrorDetalleSIESA.ToLower().Contains("Documento venta servicios: El documento no fue procesado porque ya fue actualizado por otra importación".ToLower()) Then
                        estado = 1
                    Else
                        estado = 0
                    End If
                    ' Actualizar Encabezado_Facturas con Interfaz_Contable_Factura = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                    strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = " & estado & ", Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumeFact

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                End If
            Next


        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Crear_XML_Proceso_Orden_Venta_Servicio: " & ex.Message)
        End Try

    End Sub

#End Region

#Region "Funciones Generales"

    Public Sub AdicionarNodoXml(ByRef xmlDocumento As XmlDocument, ByRef xmlElementoRoot As XmlElement, strElemento As String, strElementoNodo As String)
        Try
            xmlElementoRoot.LastChild.AppendChild(xmlDocumento.CreateElement(strElemento))
            xmlElementoRoot.LastChild.LastChild.AppendChild(xmlDocumento.CreateTextNode(strElementoNodo))

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Cargar_Datos_App_Config()
        Try
            ' Generales
            intCodigoEmpresa = ConfigurationSettings.AppSettings.Get("Empresa")
            lonIntervaloMilisegundosEjecucionProceso = ConfigurationSettings.AppSettings.Get("IntervaloMilisegundosEjecucionProceso")
            strRutaArchivoLog = ConfigurationSettings.AppSettings("RutaArchivoLog").ToString()
            strRutaArchivoXML = ConfigurationSettings.AppSettings("RutaArchivoXML").ToString()

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Error Cargar_Datos_App_Config: " & ex.Message)
        End Try
    End Sub

    'Private Function Retorna_Valor_Tabla_SIESA(strTabla As String, strValoGest As String) As String
    '    Try
    '        Dim strRetorna As String = ""

    '        If strTabla = "TINA" Then

    '            Select Case strValoGest
    '                Case serSIESA.TIPO_NATURALEZA_NATURAL_GESTRANS : strRetorna = serSIESA.TIPO_NATURALEZA_NATURAL_SIESA.ToString
    '                Case serSIESA.TIPO_NATURALEZA_JURIDICA_GESTRANS : strRetorna = serSIESA.TIPO_NATURALEZA_JURIDICA_SIESA.ToString
    '            End Select

    '        ElseIf strTabla = "TIID" Then

    '            Select Case strValoGest
    '                Case serSIESA.TIPO_IDENTIFICACION_CEDULA_GESTRANS : strRetorna = serSIESA.TIPO_IDENTIFICACION_CEDULA_SIESA.ToString
    '                Case serSIESA.TIPO_IDENTIFICACION_EXTRANJERIA_GESTRANS : strRetorna = serSIESA.TIPO_IDENTIFICACION_EXTRANJERIA_SIESA.ToString
    '                Case serSIESA.TIPO_IDENTIFICACION_NIT_GESTRANS : strRetorna = serSIESA.TIPO_IDENTIFICACION_NIT_SIESA.ToString
    '            End Select
    '        End If

    '        Return strRetorna
    '    Catch ex As Exception


    '    End Try

    'End Function

    Private Function Retorna_Fecha_Formato_SIESA(ByVal datFecha As Date) As String
        Dim strFecha As String = ""

        Try
            strFecha = datFecha.Year.ToString + String.Format("{0:MM}", datFecha) + String.Format("{0:dd}", datFecha)
        Catch ex As Exception

        End Try
        Return strFecha

    End Function

    Private Function Retorna_Error_Detalle_SIESA(strMensajeSIESA As String) As String

        ' Ejemplo Mensaje Retorna SIESA
        'strMensajeSIESA = "Error al importar el plano<NewDataSet>"
        'strMensajeSIESA += "<Table>"
        'strMensajeSIESA += "<f_nro_linea>2</f_nro_linea>"
        'strMensajeSIESA += "<f_tipo_reg>0350</f_tipo_reg>"
        'strMensajeSIESA += "<f_subtipo_reg>00</f_subtipo_reg>"
        'strMensajeSIESA += "<f_version>02</f_version>"
        'strMensajeSIESA += "<f_nivel>00</f_nivel>"
        'strMensajeSIESA += "<f_valor>1-201-A1-01000278</f_valor>"
        'strMensajeSIESA += "<f_detalle>La fecha del documento debe estar abierta para el modulo Contabilidad. Documento: 201-A1-1000278.</f_detalle>"
        'strMensajeSIESA += "</Table>"
        'strMensajeSIESA += "</NewDataSet>"
        'strMensajeSIESA += "Se genero el plano correctamente"

        Dim intPosiInicDeta As Integer, intPosiFinaDeta As Integer, strDetalle As String = ""

        Try

            intPosiInicDeta = InStr(strMensajeSIESA, "<f_detalle>")
            intPosiFinaDeta = InStr(strMensajeSIESA, "</f_detalle>")

            If intPosiInicDeta > 0 Then
                strDetalle = strMensajeSIESA.Substring(intPosiInicDeta + 10, intPosiFinaDeta - intPosiInicDeta - 12)
            Else
                strDetalle = Mid$(Trim$(strMensajeSIESA), 1, 250)
            End If

        Catch ex As Exception

        End Try
        Retorna_Error_Detalle_SIESA = strDetalle

    End Function

#End Region

End Class
