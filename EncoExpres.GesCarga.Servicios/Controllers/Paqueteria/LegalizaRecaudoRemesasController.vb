﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Negocio.Paqueteria

<Authorize>
Public Class LegalizarRecaudoRemesasController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As LegalizarRecaudoRemesas) As Respuesta(Of IEnumerable(Of LegalizarRecaudoRemesas))
        Return New LogicaLegalizarRecaudoRemesas(CapaPersistenciaLegalizarRecaudoRemesas).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As LegalizarRecaudoRemesas) As Respuesta(Of LegalizarRecaudoRemesas)
        Return New LogicaLegalizarRecaudoRemesas(CapaPersistenciaLegalizarRecaudoRemesas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As LegalizarRecaudoRemesas) As Respuesta(Of Long)
        Return New LogicaLegalizarRecaudoRemesas(CapaPersistenciaLegalizarRecaudoRemesas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("EliminarGuia")>
    Public Function EliminarGuia(ByVal filtro As DetalleLegalizarRecaudoRemesas) As Respuesta(Of Boolean)
        Return New LogicaLegalizarRecaudoRemesas(CapaPersistenciaLegalizarRecaudoRemesas).EliminarGuia(filtro)
    End Function
End Class
