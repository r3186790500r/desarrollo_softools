﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Utilitarios

Namespace Utilitarios

    ''' <summary>
    ''' Clase <see cref="RepositorioListadosAuditoriaDocumentos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioListadosAuditoriaDocumentos
        Inherits RepositorioBase(Of ListadosAuditoriaDocumentos)


        Public Overrides Function Consultar(filtro As ListadosAuditoriaDocumentos) As IEnumerable(Of ListadosAuditoriaDocumentos)
            Dim lista As New List(Of ListadosAuditoriaDocumentos)
            Dim item As ListadosAuditoriaDocumentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.TipoConsulta > -1 Then
                    conexion.AgregarParametroSQL("@par_Tipo_Consulta", filtro.TipoConsulta)
                End If

                If filtro.NumeroInicio > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero_Inicio", filtro.NumeroInicio)
                End If
                If filtro.NumeroFin > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero_Fin", filtro.NumeroFin)
                End If

                If filtro.FechaInicio > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicio, SqlDbType.DateTime)
                End If
                If filtro.FechaFin > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFin, SqlDbType.DateTime)
                End If

                If Not IsNothing(filtro.NombreUsuario) Then
                    If Not IsNothing(filtro.NombreUsuario) And filtro.NombreUsuario <> "" Then
                        conexion.AgregarParametroSQL("@par_Usuario", filtro.NombreUsuario)
                    End If
                End If

                If filtro.Estado > -1 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If filtro.Anulado > -1 Then
                    conexion.AgregarParametroSQL("@par_Anulado", filtro.Anulado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_auditoria_documentos]")

                While resultado.Read
                    item = New ListadosAuditoriaDocumentos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ListadosAuditoriaDocumentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ListadosAuditoriaDocumentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ListadosAuditoriaDocumentos) As ListadosAuditoriaDocumentos
            Throw New NotImplementedException()
        End Function

    End Class

End Namespace
