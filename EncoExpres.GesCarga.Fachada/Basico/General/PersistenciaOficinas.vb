﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaOficinas
        Implements IPersistenciaBase(Of Oficinas)

        Public Function Consultar(filtro As Oficinas) As IEnumerable(Of Oficinas) Implements IPersistenciaBase(Of Oficinas).Consultar
            Return New RepositorioOficinas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Oficinas) As Long Implements IPersistenciaBase(Of Oficinas).Insertar
            Return New RepositorioOficinas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Oficinas) As Long Implements IPersistenciaBase(Of Oficinas).Modificar
            Return New RepositorioOficinas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Oficinas) As Oficinas Implements IPersistenciaBase(Of Oficinas).Obtener
            Return New RepositorioOficinas().Obtener(filtro)
        End Function

        Public Function Anular(entidad As Oficinas) As Boolean
            Return New RepositorioOficinas().Anular(entidad)
        End Function

        Public Function ConsultarPorUsuario(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of Oficinas)
            Return New RepositorioOficinas().ConsultarPorUsuario(codigoEmpresa, codigoUsuario)
        End Function

        Public Function ConsultarRegionesPaises(filtro As RegionesPaises) As IEnumerable(Of RegionesPaises)
            Return New RepositorioOficinas().ConsultarRegionesPaises(filtro)
        End Function

        Public Function InsertarConsecutivos(entidad As Oficinas) As Boolean
            Return New RepositorioOficinas().InsertarConsecutivos(entidad)
        End Function

    End Class

End Namespace

