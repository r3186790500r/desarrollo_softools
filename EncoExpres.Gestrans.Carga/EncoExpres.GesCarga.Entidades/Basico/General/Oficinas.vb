﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="Oficinas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Oficinas
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Oficinas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Oficinas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            Obtener = Read(lector, "Obtener")

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            Direccion = Read(lector, "Direccion")
            Email = Read(lector, "Email")
            Telefono = Read(lector, "Telefono")
            ResolucionFacturacion = Read(lector, "Resolucion_Facturacion")
            Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo"), .Nombre = Read(lector, "Ciudad")}
            TipoOficina = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIOF_Codigo")}
            Region = New RegionesPaises With {.Codigo = Read(lector, "REPA_Codigo")}
            Estado = Read(lector, "Estado")
            CodigoTipoOficina = Read(lector, "CATA_TIOF_Codigo")
            CodigoZona = Read(lector, "Codigo_Zona")
            NombreZona = Read(lector, "Nombre_Zona")
            AplicaEnturnamiento = Read(lector, "Aplica_Enturnamiento")
            Tercero = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Comision"), .Nombre = Read(lector, "TerceroComision")}
            Porcentaje = Read(lector, "Porcentaje_Comision")
            OficinaPrincipal = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Principal"), .Nombre = Read(lector, "NombreOficinaPrincipal")}
            TerceroAgencista = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Agencista"), .Nombre = Read(lector, "TerceroAgencista")}
            PorcentajeAgencista = Read(lector, "Porcentaje_Comision_Agencista")
            PorcentajeSeguroPaqueteria = Read(lector, "Porcentaje_Seguro_Paqueteria")
            PrefijoFactura = Read(lector, "Prefijo_Factura")
            ClaveTecnicaFactura = Read(lector, "Clave_Tecnica_Factura")
            ClaveExternaFactura = Read(lector, "Clave_Externa_Factura")
            OficinaFactura = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Factura"), .Nombre = Read(lector, "OFIC_Nombre_Factura")}
            If Obtener = 0 Then 'Consultar
                TotalRegistros = Read(lector, "TotalRegistros")
                ResolucionFacturacion = Read(lector, "Resolucion_Facturacion")
            End If
            CuentaPUCICA = Read(lector, "Cuenta_PUC_ICA")
        End Sub

        <JsonProperty>
        Public Property CuentaPUCICA As Long

        <JsonProperty>
        Public Property PorcentajeAgencista As Double

        <JsonProperty>
        Public Property TerceroAgencista As Terceros

        <JsonProperty>
        Public Property OficinaPrincipal As Oficinas

        <JsonProperty>
        Public Property Porcentaje As Double

        <JsonProperty>
        Public Property Tercero As Terceros

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Ciudad As Ciudades

        <JsonProperty>
        Public Property Direccion As String

        <JsonProperty>
        Public Property Telefono As String

        <JsonProperty>
        Public Property Email As String

        <JsonProperty>
        Public Property TipoOficina As ValorCatalogos

        <JsonProperty>
        Public Property Region As RegionesPaises

        <JsonProperty>
        Public Property Principal As Short

        <JsonProperty>
        Public Property ResolucionFacturacion As String

        <JsonProperty>
        Public Property CodigoTipoOficina As Integer

        <JsonProperty>
        Public Property CodigoCiudad As Integer

        <JsonProperty>
        Public Property CodigoZona As Integer

        <JsonProperty>
        Public Property NombreZona As String

        <JsonProperty>
        Public Property AplicaEnturnamiento As Integer

        <JsonProperty>
        Public Property Consecutivos As IEnumerable(Of ConsecutivosTDocumentoOficina)

        <JsonProperty>
        Public Property ConsultarConsecutivosTDOficina As Boolean
        <JsonProperty>
        Public Property OficinaFactura As Oficinas


#Region "Filtros de Búsqueda"

        <JsonProperty>
        Public Property CodigoInicial As Integer

        <JsonProperty>
        Public Property CodigoFinal As Integer

        <JsonProperty>
        Public Property NombreCiudad As String

        <JsonProperty>
        Public Property AplicaConsultaMaster As Integer

        <JsonProperty>
        Public Property Usuario As Usuarios
        <JsonProperty>
        Public Property PorcentajeSeguroPaqueteria As Double

#End Region

#Region "Factura Electronica"
        <JsonProperty>
        Public Property PrefijoFactura As String
        <JsonProperty>
        Public Property ClaveTecnicaFactura As String
        <JsonProperty>
        Public Property ClaveExternaFactura As String
#End Region

    End Class
End Namespace

