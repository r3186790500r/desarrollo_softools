﻿PRINT 'gsp_modificar_detalle_liquidacion_planilla_despachos'
GO

DROP PROCEDURE gsp_modificar_detalle_liquidacion_planilla_despachos
GO

CREATE PROCEDURE gsp_modificar_detalle_liquidacion_planilla_despachos
(
	@par_EMPR_Codigo SMALLINT,
	@par_ELPD_Numero NUMERIC,
	@par_CLPD_Codigo NUMERIC,
	@par_Observaciones VARCHAR(500),
	@par_Valor MONEY
)
AS
BEGIN
	UPDATE
		Detalle_Liquidacion_Planilla_Despachos
	SET
		Observaciones = @par_Observaciones,
		Valor = @par_Valor
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND ELPD_Numero = @par_ELPD_Numero
		AND CLPD_Codigo = @par_CLPD_Codigo


	SELECT
		ID
	FROM
		Detalle_Liquidacion_Planilla_Despachos
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND ELPD_Numero = @par_ELPD_Numero
		AND CLPD_Codigo = @par_CLPD_Codigo
END
GO