import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
// Components
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
// Basico
import { comCiudades } from './Aplicativo/Basico/General/comCiudades';
import { comGestionarCiudades } from './Aplicativo/Basico/General/comGestionarCiudades';
import { comDepartamentos } from './Aplicativo/Basico/General/comDepartamentos';

// Ruteo
import { comMapa } from './Aplicativo/Ruteo/comMapa';

// Services
// Basico
import { serCiudades } from './services/serCiudades';
import { serDepartamentos } from './services/serDepartamentos';
import { serPaises } from './services/serPaises';
import { GoogleMapsModule } from '@angular/google-maps';
 

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    comCiudades, comGestionarCiudades, comDepartamentos, comMapa
  ],
  imports: [
    GoogleMapsModule,
    MatAutocompleteModule,
    MatInputModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'ciudades', component: comCiudades },
      { path: 'gesCiudad', component: comGestionarCiudades },
      { path: 'departamentos', component: comDepartamentos },
      { path: 'mapa', component:comMapa}
    ])
  ],
  providers: [serCiudades, serDepartamentos, serPaises],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
