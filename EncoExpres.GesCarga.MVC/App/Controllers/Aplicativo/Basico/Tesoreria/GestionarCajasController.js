﻿EncoExpresApp.controller("GestionarCajasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', '$http', 'CajasFactory', 'OficinasFactory', 'PlanUnicoCuentasFactory', 'ServicioSIESAFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, $http, CajasFactory, OficinasFactory, PlanUnicoCuentasFactory, ServicioSIESAFactory) {

    $scope.Titulo = 'GESTIONAR CAJAS';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Cajas' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CAJAS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.ListadoOficinas = [];
    $scope.ListadoCuentasPUC = [];
    $scope.CuentaPUCValida = true
    $scope.ValidarValorBase = false
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: '',
        Nombre: '',
        Oficina: '',
        Estado: 0
    }

    $scope.AsignarCuentaPUC = function (CuentaPUC) {
        if (CuentaPUC != undefined || CuentaPUC != null) {
            if (angular.isObject(CuentaPUC)) {
                $scope.LongitudCuentaPUC = CuentaPUC.length;
                $scope.CuentaPUCValida = true;
            }
            else if (angular.isString(CuentaPUC)) {
                $scope.LongitudCuentaPUC = CuentaPUC.length;
                $scope.CuentaPUCValida = false;
            }
        }
    };

    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVA" },
        { Codigo: 1, Nombre: "ACTIVA" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    //----------------------------Informacion Requerida para funcionar---------------------------------//
    //----------AutoComplete Oficinas
    $scope.AutocompleteOficinas = function (value) {
        $scope.ListadoOficinas = [];
        $scope.ListadoOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
        return $scope.ListadoOficinas;
    }


    /*Cargar el combo de cuentas PUC*/
    PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCuentasPUC = response.data.Datos

                    if ($scope.CodigoCuentaPUC !== undefined && $scope.CodigoCuentaPUC !== '' && $scope.CodigoCuentaPUC !== null) {
                        $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                    }
                }
                else {
                    $scope.ListadoCuentasPUC = [];
                }
            }
        }, function (response) {
        });
    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando caja código No. ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando caja código No.' + $scope.Modelo.Codigo);
        }, 200);

        var filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        CajasFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.Modelo.Codigo = response.data.Datos.Codigo
                    $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno
                    $scope.Modelo.Nombre = response.data.Datos.Nombre
                    $scope.Modelo.CuentaPUC = response.data.Datos.CuentaPUC
                    $scope.Modelo.Oficina = response.data.Datos.Oficina
                    $scope.Modelo.Estado = response.data.Datos.Estado
                    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                }
                else {
                    ShowError('No se logro consultar la caja código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarCajas';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarCajas';
            });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------Funciones CAJAS SIESA -------------------------------------------------------------------*/

    $scope.CajasSIESA = function (listado) {
        if (!($scope.Modelo.Oficina == undefined || $scope.Modelo.Oficina == '' || $scope.Modelo.Oficina == null || $scope.Modelo.Oficina.Codigo == 0)) {
            $scope.Modelo.Oficina = OficinasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.Oficina.Codigo, Sync: true }).Datos;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CodigoCaja: $scope.Modelo.CodigoAlterno !== '' ? $scope.Modelo.CodigoAlterno : '',
                CentroOperacion: $scope.Modelo.Oficina.CodigoAlterno,
            }
            $scope.ListadoCajas = []
            ServicioSIESAFactory.ConsultarCAJAS(filtros).then(function (response) {
                $scope.ListadoCajas = response.data
                if ($scope.Modelo.CodigoAlterno !== '') {
                    if ($scope.ListadoCajas.length > 0) {
                        $scope.Modelo.Nombre = $scope.ListadoCajas[0].Descripcion
                        $scope.Modelo.CodigoAlterno = $scope.ListadoCajas[0].CodigoCaja
                    } else {
                        $scope.Modelo.CodigoAlterno = ''
                        ShowError('No se encontró una caja que coincida con el código alterno y oficina indicados');
                    }
                } else if (listado) {
                    showModal('modalListadoCajas');
                }

            }, function (response) {
                ShowError(response.statusText);
            });
        } else {
            ShowError('Debe seleccionar una oficina');
        }
    };

    $scope.SeleccionarCaja = function (item) {
        closeModal('modalListadoCajas');
        $scope.Modelo.Nombre = item.Descripcion
        $scope.Modelo.CodigoAlterno = item.CodigoCaja
    };

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {
            if (!ValidarCaja()) {
                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

                CajasFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó la caja "' + $scope.Modelo.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se modificó  la caja "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = '#!ConsultarCajas/' + $scope.Modelo.Codigo;
                            }
                        }
                        $scope.MaskValores();
                    }, function (response) {
                        if (response.statusText.includes('IX_Caja_Oficina')) {
                            ShowError('No se pudo guardar, el código alterno ingresado ya está siendo utilizado para la oficina indicada')
                        } else {
                            ShowError(response.statusText);
                        }
                    });
            } else {
                ShowError('La caja seleccionada ya se encuentra asignada a la oficina: ' + $scope.Modelo.Oficina.Nombre);
            }
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
            $scope.MensajesError.push('Debe ingresar el nombre de la caja');
            continuar = false;
        }
        if ($scope.Modelo.Oficina == undefined || $scope.Modelo.Oficina == '' || $scope.Modelo.Oficina == null || $scope.Modelo.Oficina.Codigo == 0) {
            $scope.MensajesError.push('Debe seleccionar una oficina');
            continuar = false;
        }

        if ($scope.Modelo.CuentaPUC == undefined || $scope.Modelo.CuentaPUC == '' || $scope.Modelo.CuentaPUC == null || $scope.Modelo.CuentaPUC.Codigo == 0 || $scope.CuentaPUCValida == false) {
            $scope.MensajesError.push('Debe seleccionar una cuenta PUC');
            continuar = false;
        }
        if ($scope.Modelo.CodigoAlterno == undefined || $scope.Modelo.CodigoAlterno == '') {
            $scope.MensajesError.push('Debe vincular la caja SIESA correspondiente');
            continuar = false;
        }
        return continuar;
    }

    //--Validar existencia de Caja para Oficina
    function ValidarCaja() {

        var filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            CodigoAlterno: $scope.Modelo.CodigoAlterno,
            Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
            Estado: ESTADO_ACTIVO,
            Sync: true
        }
        var resVal = CajasFactory.Consultar(filtros)
        if (resVal.ProcesoExitoso) {
            if (resVal.ProcesoExitoso === true) {
                if (resVal.Datos.length > 0) {
                    for (i = 0; i < resVal.Datos.length; i++) {
                        if (resVal.Datos[i].Codigo != $scope.Modelo.Codigo) {
                            return true
                        }
                    }
                    return false
                }
                else {
                    return false
                }
            }
        } else {
            ShowError(resVal.statusText);
        }
        return false
    }
    //--Validar existencia de Caja para Oficina

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarCajas/' + $scope.Modelo.Codigo;
    };


    $scope.MaskMayus = function () {
        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskValores = function () {
        try { $scope.Modelo.SobregiroAutorizado = MascaraValores($scope.Modelo.SobregiroAutorizado) } catch (e) { }
        try { $scope.Modelo.SaldoActual = MascaraValores($scope.Modelo.SaldoActual) } catch (e) { }
    };
}]);