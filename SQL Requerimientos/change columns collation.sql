DECLARE @collate nvarchar(100);
DECLARE @table nvarchar(255);
DECLARE @column_name nvarchar(255);
DECLARE @column_id int;
DECLARE @data_type nvarchar(255);
DECLARE @max_length int;
DECLARE @row_id int;
DECLARE @sql nvarchar(max);
DECLARE @sql_column nvarchar(max);

SET @collate = 'SQL_Latin1_General_CP1_CI_AS';

DECLARE local_table_cursor CURSOR FOR

SELECT [name]
FROM sysobjects
WHERE OBJECTPROPERTY(id, N'IsUserTable') = 1

OPEN local_table_cursor
FETCH NEXT FROM local_table_cursor
INTO @table

WHILE @@FETCH_STATUS = 0
BEGIN

    DECLARE local_change_cursor CURSOR FOR

    SELECT ROW_NUMBER() OVER (ORDER BY c.column_id) AS row_id
        , c.name column_name
        , t.Name data_type
        , c.max_length
        , c.column_id
    FROM sys.columns c
    JOIN sys.types t ON c.system_type_id = t.system_type_id
    LEFT OUTER JOIN sys.index_columns ic ON ic.object_id = c.object_id AND ic.column_id = c.column_id
    LEFT OUTER JOIN sys.indexes i ON ic.object_id = i.object_id AND ic.index_id = i.index_id
    WHERE c.object_id = OBJECT_ID(@table)
    ORDER BY c.column_id

    OPEN local_change_cursor
    FETCH NEXT FROM local_change_cursor
    INTO @row_id, @column_name, @data_type, @max_length, @column_id

    WHILE @@FETCH_STATUS = 0
    BEGIN

        IF (@max_length = -1) OR (@max_length > 4000) SET @max_length = 4000;

        IF (@data_type LIKE '%char%')
        BEGIN TRY
            SET @sql = 'ALTER TABLE ' + @table + ' ALTER COLUMN ' + @column_name + ' ' + @data_type + '(' + CAST(@max_length AS nvarchar(100)) + ') COLLATE ' + @collate
            --PRINT @sql
            EXEC sp_executesql @sql
        END TRY
        BEGIN CATCH
          PRINT 'ERROR: Some index or constraint rely on the column' + @column_name + '. No conversion possible.'
          PRINT @sql
        END CATCH

        FETCH NEXT FROM local_change_cursor
        INTO @row_id, @column_name, @data_type, @max_length, @column_id

    END

    CLOSE local_change_cursor
    DEALLOCATE local_change_cursor

    FETCH NEXT FROM local_table_cursor
    INTO @table

END

CLOSE local_table_cursor
DEALLOCATE local_table_cursor

GO


USE [ENCOEXPRES]
GO
SET ANSI_PADDING ON
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Marca_Vehiculos_Nombre] ON [dbo].[Marca_Vehiculos]
GO

ALTER TABLE dbo.Marca_Vehiculos 
ALTER COLUMN Nombre VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Marca_Vehiculos_Nombre] ON [dbo].[Marca_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Marca_Vehiculos_Codigo_Alterno] ON [dbo].[Marca_Vehiculos]
GO

ALTER TABLE dbo.Marca_Vehiculos 
ALTER COLUMN Codigo_Alterno VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Marca_Vehiculos_Codigo_Alterno] ON [dbo].[Marca_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Color_Vehiculos_Nombre] ON [dbo].[Color_Vehiculos]
GO

ALTER TABLE dbo.Color_Vehiculos 
ALTER COLUMN Nombre VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Color_Vehiculos_Nombre] ON [dbo].[Color_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Color_Vehiculos_Codigo_Alterno] ON [dbo].[Color_Vehiculos]
GO

ALTER TABLE dbo.Color_Vehiculos 
ALTER COLUMN Codigo_Alterno VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Color_Vehiculos_Codigo_Alterno] ON [dbo].[Color_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Conceptos_Ventas_Nombre] ON [dbo].[Conceptos_Ventas]
GO

ALTER TABLE dbo.Conceptos_Ventas 
ALTER COLUMN Nombre VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Conceptos_Ventas_Nombre] ON [dbo].[Conceptos_Ventas]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Conceptos_Legalizacion_Gastos_Conductor_Nombre] ON [dbo].[Conceptos_Legalizacion_Gastos_Conductor]
GO

ALTER TABLE dbo.Conceptos_Legalizacion_Gastos_Conductor 
ALTER COLUMN Nombre VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Conceptos_Legalizacion_Gastos_Conductor_Nombre] ON [dbo].[Conceptos_Legalizacion_Gastos_Conductor]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Novedades_Despacho_Nombre] ON [dbo].[Novedades_Despacho]
GO

ALTER TABLE dbo.Novedades_Despacho 
ALTER COLUMN Nombre VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Novedades_Despacho_Nombre] ON [dbo].[Novedades_Despacho]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Encabezado_Impuestos_Codigo_Alterno] ON [dbo].[Encabezado_Impuestos]
GO

ALTER TABLE dbo.Encabezado_Impuestos 
ALTER COLUMN Codigo_Alterno VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Encabezado_Impuestos_Codigo_Alterno] ON [dbo].[Encabezado_Impuestos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Unidad_Empaque_Producto_Transportados_Nombre_Corto] ON [dbo].[Unidad_Empaque_Producto_Transportados]
GO

ALTER TABLE dbo.Unidad_Empaque_Producto_Transportados 
ALTER COLUMN Nombre_Corto VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Unidad_Empaque_Producto_Transportados_Nombre_Corto] ON [dbo].[Unidad_Empaque_Producto_Transportados]
(
	[EMPR_Codigo] ASC,
	[Nombre_Corto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Unidad_Empaque_Producto_Transportados_Nombre_Corto] ON [dbo].[Unidad_Empaque_Producto_Transportados]
GO

ALTER TABLE dbo.Unidad_Empaque_Producto_Transportados 
ALTER COLUMN Nombre_Corto VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Unidad_Empaque_Producto_Transportados_Nombre_Corto] ON [dbo].[Unidad_Empaque_Producto_Transportados]
(
	[EMPR_Codigo] ASC,
	[Nombre_Corto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Conceptos_Liquidacion_Planilla_Despachos_Nombre] ON [dbo].[Conceptos_Liquidacion_Planilla_Despachos]
GO

ALTER TABLE dbo.Conceptos_Liquidacion_Planilla_Despachos 
ALTER COLUMN Nombre VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Conceptos_Liquidacion_Planilla_Despachos_Nombre] ON [dbo].[Conceptos_Liquidacion_Planilla_Despachos]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Departamentos_Codigo_DANE] ON [dbo].[Departamentos]
GO

ALTER TABLE dbo.Departamentos 
ALTER COLUMN Codigo_DANE VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Departamentos_Codigo_DANE] ON [dbo].[Departamentos]
(
	[EMPR_Codigo] ASC,
	[Codigo_DANE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Encabezado_Parametrizacion_Contables_Nombre] ON [dbo].[Encabezado_Parametrizacion_Contables]
GO

ALTER TABLE dbo.Encabezado_Parametrizacion_Contables 
ALTER COLUMN Nombre VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Encabezado_Parametrizacion_Contables_Nombre] ON [dbo].[Encabezado_Parametrizacion_Contables]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Encabezado_Parametrizacion_Contables_Codigo_Alterno] ON [dbo].[Encabezado_Parametrizacion_Contables]
GO

ALTER TABLE dbo.Encabezado_Parametrizacion_Contables 
ALTER COLUMN Codigo_Alterno VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Encabezado_Parametrizacion_Contables_Codigo_Alterno] ON [dbo].[Encabezado_Parametrizacion_Contables]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Encabezado_Concepto_Contables_Nombre] ON [dbo].[Encabezado_Concepto_Contables]
GO

ALTER TABLE dbo.Encabezado_Concepto_Contables 
ALTER COLUMN Nombre VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Encabezado_Concepto_Contables_Nombre] ON [dbo].[Encabezado_Concepto_Contables]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Encabezado_Concepto_Contables_Codigo_Alterno] ON [dbo].[Encabezado_Concepto_Contables]
GO

ALTER TABLE dbo.Encabezado_Concepto_Contables 
ALTER COLUMN Codigo_Alterno VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Encabezado_Concepto_Contables_Codigo_Alterno] ON [dbo].[Encabezado_Concepto_Contables]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Producto_Transportados_Nombre] ON [dbo].[Producto_Transportados]
GO

ALTER TABLE dbo.Producto_Transportados 
ALTER COLUMN Nombre VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Producto_Transportados_Nombre] ON [dbo].[Producto_Transportados]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Caja_Oficina] ON [dbo].[Cajas]
GO

ALTER TABLE dbo.Cajas 
ALTER COLUMN Codigo_Alterno VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Caja_Oficina] ON [dbo].[Cajas]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC,
	[OFIC_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Rutas_Codigo_Alterno] ON [dbo].[Rutas]
GO

ALTER TABLE dbo.Rutas 
ALTER COLUMN Codigo_Alterno VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Rutas_Codigo_Alterno] ON [dbo].[Rutas]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Marca_Semirremolques_Nombre] ON [dbo].[Marca_Semirremolques]
GO

ALTER TABLE dbo.Marca_Semirremolques 
ALTER COLUMN Nombre VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Marca_Semirremolques_Nombre] ON [dbo].[Marca_Semirremolques]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Marca_Semirremolques_Codigo_Alterno] ON [dbo].[Marca_Semirremolques]
GO

ALTER TABLE dbo.Marca_Semirremolques 
ALTER COLUMN Codigo_Alterno VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Marca_Semirremolques_Codigo_Alterno] ON [dbo].[Marca_Semirremolques]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Linea_Vehiculos_Nombre] ON [dbo].[Linea_Vehiculos]
GO

ALTER TABLE dbo.Linea_Vehiculos 
ALTER COLUMN Nombre VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Linea_Vehiculos_Nombre] ON [dbo].[Linea_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[MAVE_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Linea_Vehiculos_Codigo_Alterno] ON [dbo].[Linea_Vehiculos]
GO

ALTER TABLE dbo.Linea_Vehiculos 
ALTER COLUMN Codigo_Alterno VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Linea_Vehiculos_Codigo_Alterno] ON [dbo].[Linea_Vehiculos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC,
	[MAVE_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Bancos] ON [dbo].[Bancos]
GO
DROP INDEX [IX_Bancos_Codigo_Cuenta] ON [dbo].[Bancos]
GO

ALTER TABLE dbo.Bancos 
ALTER COLUMN Codigo_Alterno VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Bancos] ON [dbo].[Bancos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Bancos_Codigo_Cuenta] ON [dbo].[Bancos]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Encabezado_Remesas] ON [dbo].[Encabezado_Remesas]
GO

ALTER TABLE dbo.Encabezado_Remesas 
ALTER COLUMN Numeracion varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE NONCLUSTERED INDEX [IX_Encabezado_Remesas] ON [dbo].[Encabezado_Remesas]
(
	[EMPR_Codigo] ASC,
	[Numeracion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Terceros] ON [dbo].[Terceros]
GO

ALTER TABLE dbo.Terceros 
ALTER COLUMN Numero_Identificacion varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Terceros] ON [dbo].[Terceros]
(
	[EMPR_Codigo] ASC,
	[Numero_Identificacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Plan_Unico_Cuentas] ON [dbo].[Plan_Unico_Cuentas]
GO
DROP INDEX [IX_Plan_Unico_Cuentas_Codigo_Cuenta] ON [dbo].[Plan_Unico_Cuentas]
GO

ALTER TABLE dbo.Plan_Unico_Cuentas 
ALTER COLUMN Codigo_Cuenta varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Plan_Unico_Cuentas] ON [dbo].[Plan_Unico_Cuentas]
(
	[EMPR_Codigo] ASC,
	[Codigo_Cuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Plan_Unico_Cuentas_Codigo_Cuenta] ON [dbo].[Plan_Unico_Cuentas]
(
	[EMPR_Codigo] ASC,
	[Codigo_Cuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Seguimientos_Empresas_GPS] ON [dbo].[Seguimientos_Empresas_GPS]
GO

ALTER TABLE dbo.Seguimientos_Empresas_GPS 
ALTER COLUMN VEHI_Placa varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE NONCLUSTERED INDEX [IX_Seguimientos_Empresas_GPS] ON [dbo].[Seguimientos_Empresas_GPS]
(
	[EMPR_Codigo] ASC,
	[Fecha_Reporte] ASC,
	[VEHI_Placa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Catalogos_1] ON [dbo].[Catalogos]
GO

ALTER TABLE dbo.Catalogos 
ALTER COLUMN Nombre_Corto char(4) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Catalogos_1] ON [dbo].[Catalogos]
(
	[EMPR_Codigo] ASC,
	[Nombre_Corto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[Detalle_Etiquetas_Remesas_Paqueteria] DROP CONSTRAINT [PK_Detalle_Etiquetas_Remesas_Paqueteria] WITH ( ONLINE = OFF )
GO

ALTER TABLE dbo.Detalle_Etiquetas_Remesas_Paqueteria 
ALTER COLUMN Numero_Etiqueta varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS not null
GO

ALTER TABLE [dbo].[Detalle_Etiquetas_Remesas_Paqueteria] ADD  CONSTRAINT [PK_Detalle_Etiquetas_Remesas_Paqueteria] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[ENRE_Numero] ASC,
	[Numero_Etiqueta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Cuenta_Bancarias] ON [dbo].[Cuenta_Bancarias]
GO
DROP INDEX [IX_Cuenta_Bancarias_Numero_Cuenta] ON [dbo].[Cuenta_Bancarias]
GO

ALTER TABLE dbo.Cuenta_Bancarias 
ALTER COLUMN Numero_Cuenta varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE NONCLUSTERED INDEX [IX_Cuenta_Bancarias] ON [dbo].[Cuenta_Bancarias]
(
	[EMPR_Codigo] ASC,
	[BANC_Codigo] ASC,
	[Numero_Cuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Cuenta_Bancarias_Numero_Cuenta] ON [dbo].[Cuenta_Bancarias]
(
	[EMPR_Codigo] ASC,
	[Numero_Cuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Cuenta_Bancarias_Nombre] ON [dbo].[Cuenta_Bancarias]
GO

ALTER TABLE dbo.Cuenta_Bancarias 
ALTER COLUMN Nombre varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Cuenta_Bancarias_Nombre] ON [dbo].[Cuenta_Bancarias]
(
	[EMPR_Codigo] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[Codigos_Respuesta_Proceso_Transferencia_Bancolombia] DROP CONSTRAINT [PK_Codigos_Respuesta_Proceso_Transferencia_Bancolombia] WITH ( ONLINE = OFF )
GO

ALTER TABLE dbo.Codigos_Respuesta_Proceso_Transferencia_Bancolombia 
ALTER COLUMN Codigo varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS not null
GO

ALTER TABLE [dbo].[Codigos_Respuesta_Proceso_Transferencia_Bancolombia] ADD  CONSTRAINT [PK_Codigos_Respuesta_Proceso_Transferencia_Bancolombia] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[Codigo_DANE_Ciudades] DROP CONSTRAINT [PK_Codigo_DANE_Ciudades] WITH ( ONLINE = OFF )
GO

ALTER TABLE dbo.Codigo_DANE_Ciudades 
ALTER COLUMN Codigo varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS not null
GO

ALTER TABLE [dbo].[Codigo_DANE_Ciudades] ADD  CONSTRAINT [PK_Codigo_DANE_Ciudades] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Ciudades] ON [dbo].[Ciudades]
GO
DROP INDEX [IX_Ciudad_Codigo_DANE] ON [dbo].[Ciudades]
GO
ALTER TABLE [dbo].[Ciudades] DROP CONSTRAINT [FK_Ciudades_Codigo_DANE_Ciudades]
GO

ALTER TABLE dbo.Ciudades 
ALTER COLUMN Codigo_Alterno varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE NONCLUSTERED INDEX [IX_Ciudades] ON [dbo].[Ciudades]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Ciudad_Codigo_DANE] ON [dbo].[Ciudades]
(
	[EMPR_Codigo] ASC,
	[Codigo_Alterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Ciudades]  WITH CHECK ADD  CONSTRAINT [FK_Ciudades_Codigo_DANE_Ciudades] FOREIGN KEY([EMPR_Codigo], [Codigo_Alterno])
REFERENCES [dbo].[Codigo_DANE_Ciudades] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Ciudades] CHECK CONSTRAINT [FK_Ciudades_Codigo_DANE_Ciudades]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [IX_Usuarios] ON [dbo].[Usuarios]
GO

ALTER TABLE dbo.Usuarios 
ALTER COLUMN Codigo_Usuario varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Usuarios] ON [dbo].[Usuarios]
(
	[EMPR_Codigo] ASC,
	[Codigo_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO

-------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[Equivalencia_Cuentas_Contables_Codigos_Contables_Oficinas] DROP CONSTRAINT [PK_Equivalencia_Cuentas_Contables_Codigos_Contables_Oficinas] WITH ( ONLINE = OFF )
GO

ALTER TABLE dbo.Equivalencia_Cuentas_Contables_Codigos_Contables_Oficinas 
ALTER COLUMN OFIC_Codigo_Alterno varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS not null
GO

ALTER TABLE [dbo].[Equivalencia_Cuentas_Contables_Codigos_Contables_Oficinas] ADD  CONSTRAINT [PK_Equivalencia_Cuentas_Contables_Codigos_Contables_Oficinas] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[OFIC_Codigo_Alterno] ASC,
	[PLUC_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[Equivalencia_Centro_Costos_Oficinas] DROP CONSTRAINT [PK_Equivalencia_Centro_Costos_Oficinas] WITH ( ONLINE = OFF )
GO

ALTER TABLE dbo.Equivalencia_Centro_Costos_Oficinas 
ALTER COLUMN OFIC_Codigo_Alterno varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS not null
GO

ALTER TABLE [dbo].[Equivalencia_Centro_Costos_Oficinas] ADD  CONSTRAINT [PK_Equivalencia_Centro_Costos_Oficinas] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[OFIC_Codigo_Alterno] ASC,
	[PLUC_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------
DROP INDEX [idx_Sucursal] ON [dbo].[Vehiculos]
GO

ALTER TABLE dbo.Vehiculos 
ALTER COLUMN Sucursal varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [idx_Sucursal] ON [dbo].[Vehiculos]
(
	[Sucursal] ASC
)
WHERE ([Sucursal] IS NOT NULL AND [Estado]=(1))
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO