﻿Imports Newtonsoft.Json

Namespace Basico.General
    Public Class NovedadTercero
        Inherits Base

        Sub New()
        End Sub

        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Fecha = Read(lector, "Fecha_Crea")
            Responsable = New Terceros With {.NombreCompleto = Read(lector, "Responsable"), .Codigo = Read(lector, "TERC_Codigo")}
            Estado = Read(lector, "Estado")
            Novedad = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NOTE_Codigo"), .Nombre = Read(lector, "Novedad")}
            JustificacionNovedad = Read(lector, "Observaciones")


        End Sub

        <JsonProperty>
        Public Property Fecha As DateTime

        <JsonProperty>
        Public Property Responsable As Terceros
        <JsonProperty>
        Public Property Estado As Short
        <JsonProperty>
        Public Property Novedad As ValorCatalogos
        <JsonProperty>
        Public Property JustificacionNovedad As String

    End Class



End Namespace
