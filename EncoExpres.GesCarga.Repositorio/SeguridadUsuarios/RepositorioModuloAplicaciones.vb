﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="RepositorioModuloAplicaciones"/>
    ''' </summary> 
    Public NotInheritable Class RepositorioModuloAplicaciones
        Inherits RepositorioBase(Of ModuloAplicaciones)

        Public Overrides Function Consultar(filtro As ModuloAplicaciones) As IEnumerable(Of ModuloAplicaciones)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Insertar(entidad As ModuloAplicaciones) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ModuloAplicaciones) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ModuloAplicaciones) As ModuloAplicaciones
            Throw New NotImplementedException()
        End Function
        Public Function ConsultarModuloPorUsuario(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of ModuloAplicaciones)
            Dim lista As New List(Of ModuloAplicaciones)
            Dim item As ModuloAplicaciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_modulos]")

                While resultado.Read
                    item = New ModuloAplicaciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function


    End Class
End Namespace