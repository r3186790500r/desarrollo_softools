﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="Cajas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Cajas
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="Cajas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Cajas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .Nombre = Read(lector, "CuentaPUC"), .CodigoCuenta = Read(lector, "Codigo_Cuenta_PUC")}
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "Oficina")}
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            Else

            End If
        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property Oficina As Oficinas

    End Class

End Namespace
