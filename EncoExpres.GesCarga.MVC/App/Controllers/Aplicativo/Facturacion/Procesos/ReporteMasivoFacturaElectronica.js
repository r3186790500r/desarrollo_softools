﻿EncoExpresApp.controller("ReporteMasivoFacturaElectronicaCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'TercerosFactory', 'ValorCatalogosFactory', 'OficinasFactory',
    'ReporteFacturaElectronicaFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, TercerosFactory, ValorCatalogosFactory, OficinasFactory, ReporteFacturaElectronicaFactory) {

        $scope.MapaSitio = [{ Nombre: 'Facturacion' }, { Nombre: 'Procesos' }, { Nombre: 'Reporte Masivo Factura Electrónica' }];
        $scope.Titulo = 'REPORTE MASIVO FACTURA ELECTRONÍCA';
        //-----------------------------------------------------------------DECLARACION VARIABLES---------------------------------------------------------------------------------//
        $scope.Buscando = false;
        $scope.Deshabilitar = false;
        $scope.MensajesError = [];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListaFacturas = [];
        $scope.ListaNotaCredito = [];
        $scope.ListaNotaDebito = [];
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.paginaActual = 1;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ===' + OPCION_MENU_REPORTE_MASIVO_FACTURA_ELECTRONICA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.REPORTAR_FACTURA_ELECTRONICA = {
            FACTURA: CODIGO_TIPO_DOCUMENTO_FACTURAS,
            NOTA_CREDITO: CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO,
            NOTA_DEBITO: CODIGO_TIPO_DOCUMENTO_NOTA_DEBITO
        };
        var OPCION_REPORTE_FACTURA_ELECTRONICA = 0;

        //-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------//
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            PantallaBloqueo(Find, 'Espere por favor...');
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                PantallaBloqueo(Find, 'Espere por favor...');
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                PantallaBloqueo(Find, 'Espere por favor...');
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            PantallaBloqueo(Find, 'Espere por favor...');
        };
        //--Pantalla Bloqueo
        function PantallaBloqueo(fun, mensaje) {
            if (mensaje == undefined) {
                mensaje = "Espere por favor...";
            }
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start(mensaje);
            $timeout(function () { blockUI.message(mensaje); fun(); }, 100);
        }
        //--Pantalla Bloqueo
        //-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------//
        //---------------------------------------AutoCompletes y Listados----------------------------------------------//
        //---- Clientes
        $scope.ListadoCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) === 0 || value.length === 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //--Cargar el combo de forma pago
        $scope.ListadoFormaPago = [];
        $scope.ListadoFormaPago = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS },
            Sync: true
        }).Datos;
        $scope.ListadoFormaPago.splice($scope.ListadoFormaPago.findIndex(item => item.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA), 1);
        $scope.ListadoFormaPago.push({ Codigo: -1, Nombre: '(TODAS)' });
        $scope.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo === -1');
        //--Cargar el combo de Oficinas
        $scope.ListadoOficinas = [];
        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            $scope.ListadoOficinas = OficinasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: ESTADO_DEFINITIVO,
                Sync: true
            }).Datos;
            $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 });
            $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
        }
        else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 });
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item);
                });
                $scope.Oficina = $scope.ListadoOficinas[0];
            } else {
                ShowError('El usuario no tiene oficinas asociadas');
            }
        }
        //---------------------------------------AutoCompletes y Listados----------------------------------------------//
        //---------------------------------------Taps----------------------------------------------//
        $scope.MostrarTab = function (IdTab, OpcionDocumento) {
            if (OPCION_REPORTE_FACTURA_ELECTRONICA != OpcionDocumento) {
                LimpirInformacion();
                ocultarTabs();
                $("#" + IdTab).show();
                OPCION_REPORTE_FACTURA_ELECTRONICA = OpcionDocumento;
            }
        };

        function ocultarTabs() {
            $("#TapFacturas").hide();
            $("#TapNotaCredito").hide();
            $("#TapNotaDebito").hide();
        }

        function LimpirInformacion() {
            $scope.ListaFacturas = [];
            $scope.ListaNotaCredito = [];
            $scope.ListaNotaDebito = [];
            $scope.FechaInicial = '';
            $scope.FechaFinal = '';
            $scope.Cliente = '';
            $scope.SeleccionFactura = false;
            $scope.SeleccionNotaCredito = false;
            $scope.SeleccionFactura = false;
            $scope.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo === -1');
            $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
        }
        $scope.MostrarTab('TapFacturas', $scope.REPORTAR_FACTURA_ELECTRONICA.FACTURA);// inicial Facturas
        //---------------------------------------Taps----------------------------------------------//
        //---------------------------------------Busquedas----------------------------------------------//
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                    PantallaBloqueo(Find, 'Espere por favor...');
                }
            }
        };

        function Find() {
            $scope.ListaFacturas = [];
            $scope.ListaNotaCredito = [];
            $scope.ListaNotaDebito = [];
            $scope.Buscando = true;
            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: OPCION_REPORTE_FACTURA_ELECTRONICA,
                FechaInicial: $scope.FechaInicial,
                FechaFinal: $scope.FechaFinal,
                Cliente: { Codigo: $scope.Cliente.Codigo },
                Oficina: { Codigo: $scope.Oficina.Codigo },
                FormaPago: { Codigo: $scope.FormaPago.Codigo },
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                Sync: true
            };
            var ResponseConsulta = ReporteFacturaElectronicaFactory.Consultar(Filtro);
            if (ResponseConsulta.ProcesoExitoso === true) {
                var i = 0;
                if (ResponseConsulta.Datos.length > 0) {
                    switch (OPCION_REPORTE_FACTURA_ELECTRONICA) {
                        case $scope.REPORTAR_FACTURA_ELECTRONICA.FACTURA:
                            for (i = 0; i < ResponseConsulta.Datos.length; i++) {
                                $scope.ListaFacturas.push(ResponseConsulta.Datos[i].Factura);
                            }
                            IniciarPaginacion($scope.ListaFacturas, ResponseConsulta.Datos[0].TotalRegistros);
                            break;
                        case $scope.REPORTAR_FACTURA_ELECTRONICA.NOTA_CREDITO:
                            for (i = 0; i < ResponseConsulta.Datos.length; i++) {
                                $scope.ListaNotaCredito.push(ResponseConsulta.Datos[i].NotaFactura);
                            }
                            IniciarPaginacion($scope.ListaNotaCredito, ResponseConsulta.Datos[0].TotalRegistros);
                            break;
                        case $scope.REPORTAR_FACTURA_ELECTRONICA.NOTA_DEBITO:
                            for (i = 0; i < ResponseConsulta.Datos.length; i++) {
                                $scope.ListaNotaDebito.push(ResponseConsulta.Datos[i].NotaFactura);
                            }
                            IniciarPaginacion($scope.ListaNotaDebito, ResponseConsulta.Datos[0].TotalRegistros);
                            break;
                    }
                }
                else {
                    $scope.totalRegistros = 0;
                    $scope.totalPaginas = 0;
                    $scope.paginaActual = 1;
                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                    $scope.Buscando = false;
                }
            }
            else {
                ShowError(ResponseConsulta.statusText);
            }
            blockUI.stop();
        }

        function IniciarPaginacion(Lista, TotalRegistros) {
            $scope.totalRegistros = TotalRegistros;
            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
            $scope.Buscando = false;
            $scope.ResultadoSinRegistros = '';
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicial === null || $scope.FechaInicial === undefined || $scope.FechaInicial === '')
                && ($scope.FechaFinal === null || $scope.FechaFinal === undefined || $scope.FechaFinal === '')) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;
            }
            else if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)
                || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')) {
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                    && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')) {
                    if ($scope.FechaFinal < $scope.FechaInicial) {
                        $scope.MensajesError.push('La fecha final debe ser mayor a la fecha inicial');
                        continuar = false;
                    } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                        $scope.FechaFinal = $scope.FechaInicial;
                    }
                    else {
                        $scope.FechaInicial = $scope.FechaFinal;
                    }
                }
            }
            return continuar;
        }
        //---------------------------------------Busquedas----------------------------------------------//
        //---------------------------------------Funciones----------------------------------------------//
        $scope.SlcTodo = function (seleccion) {
            switch (OPCION_REPORTE_FACTURA_ELECTRONICA) {
                case $scope.REPORTAR_FACTURA_ELECTRONICA.FACTURA:
                    $scope.ListaFacturas.forEach(function (item) {
                        item.Seleccion = seleccion;
                    });
                    break;
                case $scope.REPORTAR_FACTURA_ELECTRONICA.NOTA_CREDITO:
                    $scope.ListaNotaCredito.forEach(function (item) {
                        item.Seleccion = seleccion;
                    });
                    break;
                case $scope.REPORTAR_FACTURA_ELECTRONICA.NOTA_DEBITO:
                    $scope.ListaNotaDebito.forEach(function (item) {
                        item.Seleccion = seleccion;
                    });
                    break;
            }
        };
        //---------------------------------------Funciones----------------------------------------------//
        //--------------------------------------Reporte Masivo------------------------------------------//
        $scope.ReportarDocumentos = function () {
            var count = 0;
            var listaDocumentos = [];
            switch (OPCION_REPORTE_FACTURA_ELECTRONICA) {
                case $scope.REPORTAR_FACTURA_ELECTRONICA.FACTURA:
                    for (var i = 0; i < $scope.ListaFacturas.length; i++) {
                        if ($scope.ListaFacturas[i].Seleccion == true) {
                            listaDocumentos.push($scope.ListaFacturas[i].Numero);
                            count++;
                        }
                    }
                    break;
                case $scope.REPORTAR_FACTURA_ELECTRONICA.NOTA_CREDITO:
                    
                    for (var i = 0; i < $scope.ListaNotaCredito.length; i++) {
                        if ($scope.ListaNotaCredito[i].Seleccion == true) {
                            listaDocumentos.push($scope.ListaNotaCredito[i].Numero);
                            count++;
                        }
                    }
                    break;
               // NotasElectrónicas:
                case $scope.REPORTAR_FACTURA_ELECTRONICA.NOTA_DEBITO:                    

                    for (var i = 0; i < $scope.ListaNotaDebito.length; i++) {
                        if ($scope.ListaNotaDebito[i].Seleccion == true) {
                            listaDocumentos.push($scope.ListaNotaDebito[i].Numero);
                            count++;
                        }
                    }
                    break;
            }

            if (count > 0) {
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Reportando Documentos...");
                $timeout(function () {
                    blockUI.message('Reportando Documentos...');
                    ProcesoReporte(listaDocumentos);
                }, 100);
                //Bloqueo Pantalla                
            }
            else {
                ShowError("Debe seleccionar al menos un documento");
            }
        };
        function ProcesoReporte(listaDocumentos){
            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: OPCION_REPORTE_FACTURA_ELECTRONICA,
                ListaDocumentos: listaDocumentos
                //Sync: true
            };
            //var ResponseProceso = ReporteFacturaElectronicaFactory.ReportarDocumentosElectronicos(Filtro);
            //if (ResponseProceso.ProcesoExitoso === true) {
            //    ShowInfo("Se reportaron " + ResponseProceso.Datos.Numero + " de " + listaDocumentos.length + " documentos");
            //}
            //else {
            //    ShowError(ResponseProceso.statusText);
            //}
            //blockUI.stop();
            //$scope.ListaFacturas = [];
            //$scope.ListaNotaCredito = [];
            //$scope.ListaNotaDebito = [];
            //$scope.SeleccionFactura = false;
            //$scope.SeleccionNotaCredito = false;
            //$scope.SeleccionFactura = false;
            //$scope.Buscar();

            ReporteFacturaElectronicaFactory.ReportarDocumentosElectronicos(Filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowInfo("Se reportaron " + response.data.Datos.Numero + " de " + listaDocumentos.length + " documentos");
                    }
                    else {
                        ShowError(response.statusText);
                    }
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    $scope.ListaFacturas = [];
                    $scope.ListaNotaCredito = [];
                    $scope.ListaNotaDebito = [];
                    $scope.SeleccionFactura = false;
                    $scope.SeleccionNotaCredito = false;
                    $scope.SeleccionFactura = false;
                    $scope.Buscar();
                }, function (response) {
                        ShowError(response.statusText);
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        $scope.ListaFacturas = [];
                        $scope.ListaNotaCredito = [];
                        $scope.ListaNotaDebito = [];
                        $scope.SeleccionFactura = false;
                        $scope.SeleccionNotaCredito = false;
                        $scope.SeleccionFactura = false;
                        $scope.Buscar();
                });

        }
        //--------------------------------------Reporte Masivo------------------------------------------//
    }]);
