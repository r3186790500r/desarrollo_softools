﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Negocio.Gesphone

''' <summary>
''' Controlador <see cref="DetalleCheckListVehiculosController"/>
''' </summary>
<Authorize>
Public Class DetalleCheckListVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleCheckListVehiculos) As Respuesta(Of IEnumerable(Of DetalleCheckListVehiculos))
        Return New LogicaDetalleCheckListVehiculos(CapaPersistenciaDetalleCheckListVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleCheckListVehiculos) As Respuesta(Of DetalleCheckListVehiculos)
        Return New LogicaDetalleCheckListVehiculos(CapaPersistenciaDetalleCheckListVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleCheckListVehiculos) As Respuesta(Of Long)
        Return New LogicaDetalleCheckListVehiculos(CapaPersistenciaDetalleCheckListVehiculos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleCheckListVehiculos) As Respuesta(Of Boolean)
        Return New LogicaDetalleCheckListVehiculos(CapaPersistenciaDetalleCheckListVehiculos).Anular(entidad)
    End Function
End Class
