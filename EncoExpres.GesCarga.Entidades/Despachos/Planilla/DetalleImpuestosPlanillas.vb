﻿Imports Newtonsoft.Json

Namespace Despachos.Planilla
    <JsonObject>
    Public NotInheritable Class DetalleImpuestosPlanillas
        Inherits BaseDetalleDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleImpuestosPlanillas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            NumeroEncabezado = Read(lector, "ENPD_Numero")
            Codigo = Read(lector, "ENIM_Codigo")
            CodigoImpuesto = Read(lector, "ENIM_Codigo")
            Nombre = Read(lector, "Nombre")
            ValorTarifa = Read(lector, "Valor_Tarifa")
            ValorBase = Read(lector, "Valor_Base")
            ValorImpuesto = Read(lector, "Valor_Impuesto")

        End Sub

        <JsonProperty>
        Public Property CodigoImpuesto As Integer
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Codigo As Integer
        <JsonProperty>
        Public Property ValorTarifa As Double
        <JsonProperty>
        Public Property ValorBase As Double
        <JsonProperty>
        Public Property ValorImpuesto As Double


    End Class
End Namespace
