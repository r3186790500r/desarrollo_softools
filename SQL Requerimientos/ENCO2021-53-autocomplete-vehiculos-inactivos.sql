USE [ENCOEXPRES]
GO

/****** Object:  StoredProcedure [dbo].[gsp_consultar_vehiculos_Autocomplete]    Script Date: 22/12/2021 4:37:15 p.�m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER   PROCEDURE [dbo].[gsp_consultar_vehiculos_Autocomplete]                  
(                                  
@par_EMPR_Codigo SMALLINT,                                  
@par_Filtro VARCHAR(100) = NULL,                                                    
@par_Codigo NUMERIC = null       ,                        
@par_Conductor NUMERIC = null,
@par_Estado SMALLINT = null
)                                  
AS                                  
BEGIN --FP 2021-12-22 Refactoriazaci�n de autocomplete y agrega parametro estado
SELECT TOP(20)                                 
 COND.EMPR_Codigo,                                  
 VEHI.Placa,                     
 VEHI.Codigo,                                  
 VEHI.Codigo_Alterno,
 VEHI.Modelo,
 VEHI.Numero_Motor,
 VEHI.TERC_Codigo_Propietario,                                   
 ISNULL(COND.Razon_Social,'') + ' ' + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,           
 COND.Numero_Identificacion As IdentificacionConductor,                                  
 VEHI.TERC_Codigo_Conductor,                                  
 ISNULL(TENE.Razon_Social,'') + ' ' + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') As NombreTenedor,                                  
 TENE.Numero_Identificacion As IdentificacionTenedor,                                  
 VEHI.TERC_Codigo_Tenedor,                                  
 VEHI.Estado,                                  
 VEHI.Capacidad_Kilos AS Capacidad,                                  
 CASE WHEN VEHI.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END AS EstadoVehiculo,                                  
 VEHI.CATA_TIVE_Codigo,                                  
 TIVE.Campo1 as TipoVehiculo,                      
 ISNULL(TIVE.Campo3,0) as AplicaSemirremolque,                                
 SEMI.Placa as PlacaSemirremolque,                                  
 SEMI.Codigo as CodigoSemirremolque,     
 VEHI.SEMI_Codigo,                         
 VEHI.Justificacion_Bloqueo,        
 VEHI.CATA_TIDV_Codigo,      
 (SELECT SUM(Total_Puntos) FROM Plan_Puntos_Vehiculos where EMPR_Codigo = VEHI.EMPR_Codigo and VEHI_Codigo = VEHI.Codigo) as Puntos,                
 ROW_NUMBER() OVER(ORDER BY VEHI.Codigo) AS RowNumber                                  
FROM                                  
 Vehiculos AS VEHI                                 
 LEFT JOIN Semirremolques SEMI ON                                
 VEHI.EMPR_Codigo = SEMI.EMPR_Codigo                                  
 AND VEHI.SEMI_Codigo = SEMI.Codigo         
         
LEFT JOIN Terceros COND ON            
COND.EMPR_Codigo = VEHI.EMPR_Codigo                                
AND COND.Codigo = VEHI.TERC_Codigo_Conductor        
                             
LEFT JOIN Terceros TENE ON        
TENE.EMPR_Codigo = VEHI.EMPR_Codigo                                
AND TENE.Codigo = VEHI.TERC_Codigo_Tenedor        
                                
LEFT JOIN Valor_Catalogos TIVE ON        
VEHI.EMPR_Codigo = TIVE.EMPR_Codigo                                
AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo        
                                
WHERE 

VEHI.Codigo != 0                                  
 AND VEHI.Codigo = ISNULL(@par_Codigo,VEHI.Codigo)                            
 AND VEHI.EMPR_Codigo = @par_EMPR_Codigo                                  
 AND ((VEHI.Placa LIKE '%' + @par_Filtro + '%') OR  (VEHI.Codigo_Alterno LIKE '%' + @par_Filtro + '%') OR (@par_Filtro IS NULL))                                  
 AND VEHI.TERC_Codigo_Conductor = ISNULL(@par_Conductor,VEHI.TERC_Codigo_Conductor) 
 AND (
 --FP 2021-12-22 refactorizacion, conserva logica anterior (caso 1 y 4) y agrega nueva (caso 2 y 3)
 --si hay param codigo (caso 1) o param estado -1 (caso 2) busca cualquier vehiculo
 --si no, filtra por param estado (caso 3)
 --si no, filtra por estado y conductor activo (caso 4)
 (@par_Codigo > 0 OR @par_Estado = -1)
 OR 
 (VEHI.Estado = @par_Estado AND @par_Estado IS NOT NULL)
 OR
 (VEHI.Estado = 1 AND COND.Estado = 1)
 )

END   
GO