﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Negocio.Gesphone

''' <summary>
''' Controlador <see cref="DetalleCheckListCargueDescarguesController"/>
''' </summary>
<Authorize>
Public Class DetalleCheckListCargueDescarguesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleCheckListCargueDescargues) As Respuesta(Of IEnumerable(Of DetalleCheckListCargueDescargues))
        Return New LogicaDetalleCheckListCargueDescargues(CapaPersistenciaDetalleCheckListCargueDescargues).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleCheckListCargueDescargues) As Respuesta(Of DetalleCheckListCargueDescargues)
        Return New LogicaDetalleCheckListCargueDescargues(CapaPersistenciaDetalleCheckListCargueDescargues).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleCheckListCargueDescargues) As Respuesta(Of Long)
        Return New LogicaDetalleCheckListCargueDescargues(CapaPersistenciaDetalleCheckListCargueDescargues).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleCheckListCargueDescargues) As Respuesta(Of Boolean)
        Return New LogicaDetalleCheckListCargueDescargues(CapaPersistenciaDetalleCheckListCargueDescargues).Anular(entidad)
    End Function
End Class
