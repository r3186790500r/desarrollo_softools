﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General


Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioOficinas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioOficinas
        Inherits RepositorioBase(Of Oficinas)

        Public Overrides Function Consultar(filtro As Oficinas) As IEnumerable(Of Oficinas)
            Dim lista As New List(Of Oficinas)
            Dim item As Oficinas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If

                If Not IsNothing(filtro.Nombre) Then
                    If Len(filtro.Nombre) > 0 Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If

                If Not IsNothing(filtro.NombreCiudad) Then
                    conexion.AgregarParametroSQL("@par_Ciudad", filtro.NombreCiudad)
                End If

                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If Not IsNothing(filtro.Region) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Region", filtro.Region.Codigo)
                End If

                If Not IsNothing(filtro.Usuario) Then
                    If filtro.Usuario.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario", filtro.Usuario.Codigo)
                    End If
                End If

                If filtro.CodigoCiudad > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo_Ciudad", filtro.CodigoCiudad)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If




                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_oficinas]")

                While resultado.Read
                    item = New Oficinas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Oficinas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo", entidad.Ciudad.Codigo)
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Email", entidad.Email)
                conexion.AgregarParametroSQL("@par_Resolucion_Facturacion", entidad.ResolucionFacturacion)
                conexion.AgregarParametroSQL("@par_CATA_TIOF_Codigo", entidad.TipoOficina.Codigo)
                If Not IsNothing(entidad.Region) Then
                    conexion.AgregarParametroSQL("@par_REPA_Codigo", entidad.Region.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_Aplica_Enturnamiento", entidad.AplicaEnturnamiento)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_ZOCI_Codigo", entidad.CodigoZona)

                If Not IsNothing(entidad.Porcentaje) Then
                    conexion.AgregarParametroSQL("@par_Porcentaje_Comision", entidad.Porcentaje, SqlDbType.Decimal)
                End If
                If Not IsNothing(entidad.Tercero) Then
                    conexion.AgregarParametroSQL("@par_Tercero_Comision", entidad.Tercero.Codigo)
                End If
                If Not IsNothing(entidad.TerceroAgencista) Then
                    conexion.AgregarParametroSQL("@par_Tercero_Agencista_Comision", entidad.TerceroAgencista.Codigo)
                End If
                If Not IsNothing(entidad.OficinaPrincipal) Then
                    conexion.AgregarParametroSQL("@par_Oficina_Principal", entidad.OficinaPrincipal.Codigo)
                End If
                If Not IsNothing(entidad.PorcentajeAgencista) Then
                    conexion.AgregarParametroSQL("@par_Porcentaje_Agencista_Comision", entidad.PorcentajeAgencista, SqlDbType.Decimal)
                End If

                If Not IsNothing(entidad.PorcentajeSeguroPaqueteria) Then
                    conexion.AgregarParametroSQL("@par_Porcentaje_Seguro_Paq", entidad.PorcentajeSeguroPaqueteria, SqlDbType.Decimal)
                End If

                conexion.AgregarParametroSQL("@par_Prefijo_Factura", entidad.PrefijoFactura)
                conexion.AgregarParametroSQL("@par_Clave_Tecnica_Factura", entidad.ClaveTecnicaFactura)
                conexion.AgregarParametroSQL("@par_Clave_Externa_Factura", entidad.ClaveExternaFactura)

                If Not IsNothing(entidad.OficinaFactura) Then
                    If entidad.OficinaFactura.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Factura", entidad.OficinaFactura.Codigo)
                    End If
                End If

                If Not IsNothing(entidad.CuentaPUCICA) Then
                    If entidad.CuentaPUCICA > 0 Then
                        conexion.AgregarParametroSQL("@par_Cuenta_PUC_ICA", entidad.CuentaPUCICA)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_oficinas")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As Oficinas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo", entidad.Ciudad.Codigo)
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Email", entidad.Email)
                conexion.AgregarParametroSQL("@par_CATA_TIOF_Codigo", entidad.TipoOficina.Codigo)
                conexion.AgregarParametroSQL("@par_REPA_Codigo", entidad.Region.Codigo)
                conexion.AgregarParametroSQL("@par_Resolucion_Facturacion", entidad.ResolucionFacturacion)
                conexion.AgregarParametroSQL("@par_Aplica_Enturnamiento", entidad.AplicaEnturnamiento)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Modifica", entidad.UsuarioModifica.Codigo)
                conexion.AgregarParametroSQL("@par_ZOCI_Codigo", entidad.CodigoZona)
                If Not IsNothing(entidad.Porcentaje) Then


                    conexion.AgregarParametroSQL("@par_Porcentaje_Comision", entidad.Porcentaje, SqlDbType.Decimal)
                End If
                If Not IsNothing(entidad.Tercero) Then
                    conexion.AgregarParametroSQL("@par_Tercero_Comision", entidad.Tercero.Codigo)
                End If
                If Not IsNothing(entidad.TerceroAgencista) Then
                    conexion.AgregarParametroSQL("@par_Tercero_Agencista_Comision", entidad.TerceroAgencista.Codigo)
                End If
                If Not IsNothing(entidad.OficinaPrincipal) Then
                    conexion.AgregarParametroSQL("@par_Oficina_Principal", entidad.OficinaPrincipal.Codigo)
                End If
                If Not IsNothing(entidad.PorcentajeAgencista) Then
                    conexion.AgregarParametroSQL("@par_Porcentaje_Agencista_Comision", entidad.PorcentajeAgencista, SqlDbType.Decimal)
                End If
                If Not IsNothing(entidad.PorcentajeSeguroPaqueteria) Then
                    conexion.AgregarParametroSQL("@par_Porcentaje_Seguro_Paq", entidad.PorcentajeSeguroPaqueteria, SqlDbType.Decimal)
                End If

                conexion.AgregarParametroSQL("@par_Prefijo_Factura", entidad.PrefijoFactura)
                conexion.AgregarParametroSQL("@par_Clave_Tecnica_Factura", entidad.ClaveTecnicaFactura)
                conexion.AgregarParametroSQL("@par_Clave_Externa_Factura", entidad.ClaveExternaFactura)

                If Not IsNothing(entidad.OficinaFactura) Then
                    If entidad.OficinaFactura.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Factura", entidad.OficinaFactura.Codigo)
                    End If
                End If

                If Not IsNothing(entidad.CuentaPUCICA) Then
                    If entidad.CuentaPUCICA > 0 Then
                        conexion.AgregarParametroSQL("@par_Cuenta_PUC_ICA", entidad.CuentaPUCICA)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_oficinas")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As Oficinas) As Oficinas
            Dim item As New Oficinas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_oficinas]")

                While resultado.Read
                    item = New Oficinas(resultado)
                End While

                resultado.Close()

                If filtro.ConsultarConsecutivosTDOficina Then
                    Dim ListaConsecutivos As New List(Of ConsecutivosTDocumentoOficina)
                    Dim ObjConsecutivo As ConsecutivosTDocumentoOficina
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_TDocumentos_Oficina")
                    While resultado.Read
                        ObjConsecutivo = New ConsecutivosTDocumentoOficina(resultado)
                        ListaConsecutivos.Add(ObjConsecutivo)
                    End While
                    item.Consecutivos = ListaConsecutivos
                End If

            End Using

            Return item
        End Function

        Public Function Anular(entidad As Oficinas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_oficinas]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function ConsultarPorUsuario(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of Oficinas)
            Dim lista As New List(Of Oficinas)
            Dim item As Oficinas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_oficinas_usuario]")

                While resultado.Read
                    item = New Oficinas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarRegionesPaises(filtro As RegionesPaises) As IEnumerable(Of RegionesPaises)
            Dim lista As New List(Of RegionesPaises)
            Dim item As RegionesPaises

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_regiones_paises_oficinas]")

                While resultado.Read
                    item = New RegionesPaises(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function InsertarConsecutivos(entidad As Oficinas) As Boolean
            Dim inserto As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_consecutivos_oficina]")
                resultado.Close()

                For Each detalle In entidad.Consecutivos
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", detalle.TipoDocumento.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Consecutivo", detalle.Consecutivo)
                    conexion.AgregarParametroSQL("@par_Consecutivo_Hasta", detalle.ConsecutivoFin)
                    conexion.AgregarParametroSQL("@par_Numero_Documentos_Faltantes_Aviso", detalle.AvisoConRestantes)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_consecutivos_oficina]")
                    While resultado.Read
                        inserto = IIf((resultado.Item("OFIC_Codigo")) > 0, True, False)
                    End While
                    resultado.Close()
                    If Not inserto Then
                        Exit For
                    End If
                Next

            End Using

            Return inserto
        End Function

    End Class

End Namespace