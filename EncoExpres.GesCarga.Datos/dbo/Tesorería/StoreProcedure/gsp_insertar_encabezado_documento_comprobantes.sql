﻿PRINT 'gsp_insertar_encabezado_documento_comprobantes'
GO
DROP PROCEDURE gsp_insertar_encabezado_documento_comprobantes
GO
CREATE PROCEDURE gsp_insertar_encabezado_documento_comprobantes
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo_Alterno VARCHAR (20),
@par_TIDO_Codigo NUMERIC,
@par_Fecha DATETIME,
@par_TERC_Codigo_Titular NUMERIC,
@par_TERC_Codigo_Beneficiario NUMERIC,
@par_Observaciones VARCHAR (1000) = NULL,
@par_CATA_DOOR_Codigo NUMERIC,
@par_CATA_FPDC_Codigo NUMERIC,
@par_CATA_DEIN_Codigo NUMERIC,
@par_Documento_Origen NUMERIC = NULL,
@par_CUBA_Codigo SMALLINT = NULL,
@par_CAJA_Codigo SMALLINT = NULL,
@par_Numero_Pago_Recaudo NUMERIC = NULL,
@par_Fecha_Pago_Recaudo DATETIME,
@par_Valor_Pago_Recaudo_Transferencia MONEY,
@par_Valor_Pago_Recaudo_Efectivo MONEY,
@par_Valor_Pago_Recaudo_Cheque MONEY,
@par_Valor_Pago_Recaudo_Total MONEY,
@par_Numeracion VARCHAR (20) = NULL,
@par_OFIC_Codigo_Creacion SMALLINT,
@par_OFIC_Codigo_Destino SMALLINT,
@par_Genero_Consignacion SMALLINT,
@par_Valor_Alterno MONEY,
@par_VEHI_Codigo NUMERIC = NULL,
@par_Estado SMALLINT,
@par_USUA_Codigo_Crea SMALLINT,
@par_ECCO_Codigo NUMERIC = NULL
)
AS
BEGIN
DECLARE  @intNumero NUMERIC

EXEC gsp_generar_consecutivo  @par_EMPR_Codigo, @par_TIDO_Codigo, 0, @intNumero OUTPUT  

INSERT INTO Encabezado_Documento_Comprobantes (
	EMPR_Codigo,
	Numero,
	Codigo_Alterno,
	TIDO_Codigo,
	Fecha,
	TERC_Codigo_Titular,
	TERC_Codigo_Beneficiario,
	Observaciones,
	CATA_DOOR_Codigo,
	CATA_FPDC_Codigo,
	CATA_DEIN_Codigo,
	Documento_Origen,
	CUBA_Codigo,
	CAJA_Codigo,
	Numero_Pago_Recaudo,
	Fecha_Pago_Recaudo,
	Valor_Pago_Recaudo_Transferencia,
	Valor_Pago_Recaudo_Efectivo,
	Valor_Pago_Recaudo_Cheque,
	Valor_Pago_Recaudo_Total,
	Numeracion,
	OFIC_Codigo_Creacion,
	OFIC_Codigo_Destino,
	Genero_Consignacion,
	Valor_Alterno,
	Anulado,
	VEHI_Codigo,
	Estado,
	USUA_Codigo_Crea,
	Fecha_Crea,
	ECCO_Codigo
	)
VALUES (
	@par_EMPR_Codigo,
	@intNumero,
	@par_Codigo_Alterno,
	@par_TIDO_Codigo,
	@par_Fecha,
	@par_TERC_Codigo_Titular,
	@par_TERC_Codigo_Beneficiario,
	ISNULL(@par_Observaciones,''),
	@par_CATA_DOOR_Codigo,
	@par_CATA_FPDC_Codigo,
	@par_CATA_DEIN_Codigo,
	ISNULL(@par_Documento_Origen,0),
	ISNULL(@par_CUBA_Codigo,0),
	ISNULL(@par_CAJA_Codigo,0),
	ISNULL(@par_Numero_Pago_Recaudo,0),
	@par_Fecha_Pago_Recaudo,
	@par_Valor_Pago_Recaudo_Transferencia,
	@par_Valor_Pago_Recaudo_Efectivo,
	@par_Valor_Pago_Recaudo_Cheque,
	@par_Valor_Pago_Recaudo_Total,
	ISNULL(@par_Numeracion,''),
	@par_OFIC_Codigo_Creacion,
	@par_OFIC_Codigo_Destino,
	@par_Genero_Consignacion,
	@par_Valor_Alterno,
	0,
	ISNULL(@par_VEHI_Codigo,0),
	@par_Estado,
	@par_USUA_Codigo_Crea,
	GETDATE(),
	ISNULL(@par_ECCO_Codigo,0)
	)

SELECT @@IDENTITY AS Codigo, @intNumero AS Numero

END
GO