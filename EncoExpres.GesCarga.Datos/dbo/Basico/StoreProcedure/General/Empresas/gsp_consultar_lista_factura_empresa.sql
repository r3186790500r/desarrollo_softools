﻿
Print 'CREATE PROCEDURE gsp_consultar_lista_factura_empresa'
GO
DROP PROCEDURE gsp_consultar_lista_factura_empresa
GO

  
CREATE PROCEDURE gsp_consultar_lista_factura_empresa       
(@par_EMPR_Codigo SMALLINT)          
          
AS          
BEGIN          
SELECT            
EMPR_Codigo AS Codigo_Empresa          
,ISNULL(Proveedor, '') AS Proveedor           
,ISNULL(Usuario, '') AS Usuario          
,ISNULL(Clave, '') AS Clave          
,ISNULL(Operador_Virtual, '') AS Operador_Virtual          
,ISNULL(Prefijo_Factura, '') AS Prefijo_Factura          
,ISNULL(Clave_Tecnica_Factura, '') AS Clave_Tecnica_Factura  
,ISNULL(Clave_Externa_Factura, '') AS Clave_Externa_Factura          
,ISNULL(Prefijo_Nota_Credito, '') AS Prefijo_Nota_Credito  
,ISNULL(Clave_Externa_Nota_Credito, '') AS Clave_Externa_Nota_Credito          
          
FROM Empresa_Factura_Electronica       
          
WHERE           
EMPR_Codigo = @par_EMPR_Codigo             
          
END 
GO
