﻿'Imports System.Data
'Imports System.Data.SqlClient

'Namespace Entidades.ReporteMinisterio
'    Public Class Banco

'#Region "Declaracion Variables"

'        Dim strSQL As String
'        Dim objGeneral As General
'        Public strError As String
'        Public bolModificar As Boolean

'        Private objEmpresa As Empresas
'        Private intCodigo As Integer
'        Private strCodigoAlterno As String
'        Private strNombre As String
'        Private objPlanUnicoCuenta As PlanUnicoCuenta
'        Private objUsuario As Usuario

'#End Region

'#Region "Constructores"

'        Sub New()
'            Me.objEmpresa = New Empresas(0)
'            Me.objPlanUnicoCuenta = New PlanUnicoCuenta(Me.objEmpresa)
'            Me.objGeneral = New General()
'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.strCodigoAlterno = ""
'            Me.strNombre = ""
'        End Sub

'        Sub New(ByRef Empresa As Empresas)
'            Me.objEmpresa = Empresa
'            Me.objPlanUnicoCuenta = New PlanUnicoCuenta(Me.objEmpresa)
'            Me.objGeneral = New General()
'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.strCodigoAlterno = ""
'            Me.strNombre = ""
'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property Codigo() As Integer
'            Get
'                Return Me.intCodigo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCodigo = value
'            End Set
'        End Property

'        Public Property CodigoAlterno() As String
'            Get
'                Return Me.strCodigoAlterno
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoAlterno = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        Public Property PlanUnicoCuenta() As PlanUnicoCuenta
'            Get
'                Return Me.objPlanUnicoCuenta
'            End Get
'            Set(ByVal value As PlanUnicoCuenta)
'                Me.objPlanUnicoCuenta = value
'            End Set
'        End Property

'        Public Property Usuario() As Usuario
'            Get
'                Return Me.objUsuario
'            End Get
'            Set(ByVal value As Usuario)
'                Me.objUsuario = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean

'            Try

'                Consultar = True
'                Dim sdrBanco As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Codigo, Codigo_Alterno, Nombre FROM Bancos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrBanco = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrBanco.Read() Then
'                    Me.strNombre = sdrBanco("Nombre").ToString()
'                    Me.strCodigoAlterno = sdrBanco("Codigo_Alterno").ToString()
'                    Me.intCodigo = Val(sdrBanco("Codigo").ToString())
'                    Consultar = True
'                Else
'                    Me.intCodigo = 0
'                    Consultar = False
'                End If

'                sdrBanco.Close()
'                sdrBanco.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Consultar = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'            Return Consultar
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean

'            Try

'                Consultar = True

'                Me.objEmpresa = Empresa
'                Me.intCodigo = Codigo

'                Dim sdrBanco As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Codigo, Codigo_Alterno, Nombre FROM Bancos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrBanco = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrBanco.Read() Then
'                    Me.strNombre = sdrBanco("Nombre").ToString()
'                    Me.strCodigoAlterno = sdrBanco("Codigo_Alterno").ToString()
'                    Me.intCodigo = sdrBanco("Codigo").ToString()
'                    Consultar = True
'                Else
'                    Me.intCodigo = 0
'                    Consultar = False
'                End If

'                sdrBanco.Close()
'                sdrBanco.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean

'            If Datos_Requeridos(Mensaje) Then
'                If Not bolModificar Then
'                    Guardar = Insertar_SQL()
'                Else
'                    Guardar = Modificar_SQL()
'                End If
'            End If
'            Mensaje = Me.strError
'            Return Guardar
'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Eliminar = Borrar_SQL()
'            Mensaje = Me.strError
'        End Function

'#End Region

'#Region "Funciones Privadas"

'        Private Function Insertar_SQL() As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_bancos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo
'                ComandoSQL.Parameters.Add("@par_Codigo_Alterno", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Codigo_Alterno").Value = Me.strCodigoAlterno
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 40) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_PLUC_Codigo", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_PLUC_Codigo").Value = Me.objPlanUnicoCuenta.Codigo
'                ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Crea").Value = Me.objUsuario.Codigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Me.intCodigo = ComandoSQL.Parameters("@par_Codigo").Value
'                    Insertar_SQL = True
'                Else
'                    Insertar_SQL = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Insertar_SQL = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Private Function Modificar_SQL() As Boolean

'            Try
'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_bancos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo
'                ComandoSQL.Parameters.Add("@par_Codigo_Alterno", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Codigo_Alterno").Value = Me.strCodigoAlterno
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 40) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_PLUC_Codigo", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_PLUC_Codigo").Value = Me.objPlanUnicoCuenta.Codigo
'                ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Modifica").Value = Me.objUsuario.Codigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Modificar_SQL = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Borrar_SQL() As Boolean

'            Try
'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_bancos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Borrar_SQL = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean

'            Try

'                Dim intCont As Integer
'                Datos_Requeridos = True

'                If Not IsNumeric(Me.intCodigo) And Me.intCodigo <> 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El Código debe ser numérico."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.strNombre = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Digite el nombre del banco."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.strNombre <> "" And Len(Me.strNombre) < 4 Then
'                    intCont += 1
'                    strError += intCont & ". El nombre del Banco no puede tener menos de cuatro (4) carácteres"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If


'            Catch ex As Exception
'                Datos_Requeridos = False
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'            End Try

'        End Function

'#End Region
'    End Class
'End Namespace

