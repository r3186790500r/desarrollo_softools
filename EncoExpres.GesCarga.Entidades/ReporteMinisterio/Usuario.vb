﻿
'Imports System.Data
'Imports System.Data.SqlClient


'Namespace Entidades.ReporteMinisterio


'    Public Class Usuario



'#Region "Declaracion Variables"

'        Dim objGeneral As New General
'        Dim strSQL As String

'        Public strError As String

'        Public bolModificar As Boolean
'        Public isCambioContrasena As Boolean
'        Private objEmpresa As Empresas
'        Private strCodigo As String
'        Private strNombre As String

'        Private strDescripcion As String
'        Private strClave As String
'        Private strConfirmacionClave As String
'        Private strClaveActual As String
'        Private strConfirmacionClaveActual As String

'        Private intDiasCambioClave As Integer
'        Private intCuentaActiva As Integer
'        Private intCambiarClave As Integer
'        Private dteFechaUltimoCambioClave As Date
'        Private objFuncionario As Tercero

'        Private strGrupo As String
'        Private objOficina As Oficina
'        Private intLogin As Integer
'        Private dteFechaUltimoLogin As Date

'        Private objValidacion As Validacion

'        Private strPrefijoContableIngreso As String
'        Private strPrefijoContableEgreso As String

'        Private objCliente As Tercero
'        Private intExterno As Integer
'        Private strMensaje As String
'        Private intAdministradorUsuarios As Byte
'        Private intTipoAplicacion As Integer
'        Private intManager As Byte

'#End Region

'#Region "Declaracion Constantes"

'        Public Const CUENTA_ACTIVA As Integer = 1
'        Public Const LOGEADO As Integer = 1
'        Public Const MINIMO_CARACTERES As Integer = 8
'        Public Const MAXIMO_CARACTERES As Integer = 10
'        Public Const NOMBRE_CLASE As String = "Usuario"

'#End Region

'#Region "Constantes Validaciones"

'        Const VALIDA_USUARIOS_CONCURRENTES_NIVEL_GENERAL As String = "UsuarioVali1"

'#End Region

'#Region "Constructor"

'        Sub New()

'            Me.objEmpresa = New Empresas(0)
'            Me.strCodigo = ""
'            Me.strNombre = ""
'            Me.strDescripcion = ""
'            Me.strClave = ""

'            Me.strConfirmacionClave = ""
'            Me.strClaveActual = ""
'            Me.strConfirmacionClaveActual = ""
'            Me.intDiasCambioClave = 0
'            Me.intCuentaActiva = 0

'            Me.intCambiarClave = 0
'            Me.objOficina = New Oficina(Me.objEmpresa)
'            Me.intLogin = 0
'            Me.objValidacion = New Validacion(Me.objEmpresa)

'            Me.intExterno = 0
'            Me.strMensaje = String.Empty
'            Me.strPrefijoContableIngreso = ""
'            Me.strPrefijoContableEgreso = ""
'            Me.intAdministradorUsuarios = 0
'            Me.intTipoAplicacion = 0
'            Me.intManager = 0

'        End Sub

'        Sub New(ByRef Empresa As Empresas)

'            Me.objEmpresa = Empresa
'            Me.strCodigo = ""
'            Me.strNombre = ""
'            Me.strDescripcion = ""
'            Me.strClave = ""

'            Me.strConfirmacionClave = ""
'            Me.strClaveActual = ""
'            Me.strConfirmacionClaveActual = ""
'            Me.intDiasCambioClave = 0
'            Me.intCuentaActiva = 0

'            Me.intCambiarClave = 0
'            Me.objOficina = New Oficina(Me.objEmpresa)
'            Me.intLogin = 0
'            Me.objValidacion = New Validacion(Me.objEmpresa)
'            Me.intExterno = 0
'            Me.strMensaje = String.Empty

'            Me.strPrefijoContableIngreso = ""
'            Me.strPrefijoContableEgreso = ""
'            Me.intAdministradorUsuarios = 0
'            Me.intTipoAplicacion = 0
'            Me.intManager = 0

'        End Sub

'#End Region

'#Region "Atributos"

'        Public Property PrefijoContableEgreso() As String
'            Get
'                Return Me.strPrefijoContableEgreso
'            End Get
'            Set(ByVal value As String)
'                Me.strPrefijoContableEgreso = value
'            End Set
'        End Property

'        Public Property PrefijoContableIngreso() As String
'            Get
'                Return Me.strPrefijoContableIngreso
'            End Get
'            Set(ByVal value As String)
'                Me.strPrefijoContableIngreso = value
'            End Set
'        End Property

'        Public Property CambiarClave() As Integer
'            Get
'                Return Me.intCambiarClave
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCambiarClave = value
'            End Set
'        End Property

'        Public Property CuentaActiva() As Integer
'            Get
'                Return Me.intCuentaActiva
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then
'                    value = value * -1
'                End If
'                Me.intCuentaActiva = value
'            End Set
'        End Property

'        Public Property DiasCambioClave() As Integer
'            Get
'                Return Me.intDiasCambioClave
'            End Get
'            Set(ByVal value As Integer)
'                Me.intDiasCambioClave = value
'            End Set
'        End Property

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property Codigo() As String
'            Get
'                Return Me.strCodigo
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigo = value
'            End Set
'        End Property

'        Public Property ConfirmacionClave() As String
'            Get
'                Return Me.strConfirmacionClave
'            End Get
'            Set(ByVal value As String)
'                Me.strConfirmacionClave = value
'            End Set
'        End Property

'        Public Property ConfirmacionClaveActual() As String
'            Get
'                Return Me.strConfirmacionClaveActual
'            End Get
'            Set(ByVal value As String)
'                Me.strConfirmacionClaveActual = value
'            End Set
'        End Property

'        Public Property ClaveActual() As String
'            Get
'                Return Me.strClaveActual
'            End Get
'            Set(ByVal value As String)
'                Me.strClaveActual = value
'            End Set
'        End Property

'        Public Property Clave() As String
'            Get
'                Return Me.strClave
'            End Get
'            Set(ByVal value As String)
'                Me.strClave = value
'            End Set
'        End Property

'        Public Property Descripcion() As String
'            Get
'                Return Me.strDescripcion
'            End Get
'            Set(ByVal value As String)
'                Me.strDescripcion = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        Public Property FechaUltimoCambioClave() As Date
'            Get
'                Return Me.dteFechaUltimoCambioClave
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaUltimoCambioClave = value
'            End Set
'        End Property

'        Public Property Funcionario() As Tercero
'            Get
'                If IsNothing(Me.objFuncionario) Then
'                    Me.objFuncionario = New Tercero(Me.objEmpresa)
'                End If
'                Return Me.objFuncionario
'            End Get
'            Set(ByVal value As Tercero)
'                If IsNothing(Me.objFuncionario) Then
'                    Me.objFuncionario = New Tercero(Me.objEmpresa)
'                End If
'                Me.objFuncionario = value
'            End Set
'        End Property

'        Public Property Grupo() As String
'            Get
'                Return Me.strGrupo
'            End Get
'            Set(ByVal value As String)
'                Me.strGrupo = value
'            End Set
'        End Property

'        Public Property Oficina() As Oficina
'            Get
'                Return Me.objOficina
'            End Get
'            Set(ByVal value As Oficina)
'                Me.objOficina = value
'            End Set
'        End Property

'        Public Property Login() As Integer
'            Get
'                Return Me.intLogin
'            End Get
'            Set(ByVal value As Integer)
'                Me.intLogin = value
'            End Set
'        End Property

'        Public ReadOnly Property FechaUltimoLogin() As Date
'            Get
'                Return Me.dteFechaUltimoLogin
'            End Get
'        End Property

'        Public Property Cliente() As Tercero
'            Get
'                If IsNothing(Me.objCliente) Then
'                    Me.objCliente = New Tercero(Me.objEmpresa)
'                End If
'                Return Me.objCliente
'            End Get
'            Set(ByVal value As Tercero)
'                If IsNothing(Me.objCliente) Then
'                    Me.objCliente = New Tercero(Me.objEmpresa)
'                End If
'                Me.objCliente = value
'            End Set
'        End Property

'        Public Property Externo() As Integer
'            Get
'                Return Me.intExterno
'            End Get
'            Set(ByVal value As Integer)
'                Me.intExterno = value
'            End Set
'        End Property

'        Public Property Mensaje() As String
'            Get
'                Return Me.strMensaje
'            End Get
'            Set(ByVal value As String)
'                Me.strMensaje = value
'            End Set
'        End Property

'        Public Property AdministradorUsuarios As Byte
'            Get
'                Return Me.intAdministradorUsuarios
'            End Get
'            Set(value As Byte)
'                Me.intAdministradorUsuarios = value
'            End Set
'        End Property

'        Public Property TipoAplicacion() As Integer
'            Get
'                Return Me.intTipoAplicacion
'            End Get
'            Set(ByVal value As Integer)
'                Me.intTipoAplicacion = value
'            End Set
'        End Property

'        Public Property Manager() As Byte
'            Get
'                Return Me.intManager
'            End Get
'            Set(ByVal value As Byte)
'                Me.intManager = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Retorna_Cantidad_Usuarios(ByRef Empresa As Empresas, ByVal Codigo As String) As Integer

'            Try

'                Dim sdrUsuario As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                Me.objEmpresa = Empresa
'                Me.strCodigo = Codigo

'                strSQL = "SELECT COUNT(EMPR_Codigo) as Cantidad FROM Grupo_Usuarios"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND USUA_Codigo = '" & Me.strCodigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrUsuario = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrUsuario.Read() Then

'                    Retorna_Cantidad_Usuarios = Val(sdrUsuario("Cantidad").ToString)
'                Else
'                    Retorna_Cantidad_Usuarios = General.CERO
'                End If

'                sdrUsuario.Close()
'                sdrUsuario.Dispose()

'            Catch ex As Exception
'                Retorna_Cantidad_Usuarios = General.CERO
'                Me.strCodigo = ""

'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Retorna_Grupo_Usuario(ByRef strGrupos As String) As Boolean

'            Try

'                Dim strSQL As String
'                Dim intCont As Integer

'                Retorna_Grupo_Usuario = True
'                strGrupos = ""
'                strSQL = "SELECT EMPR_Codigo, GRUP_Codigo FROM Grupo_Usuarios"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND USUA_Codigo = '" & Me.strCodigo & "'"

'                Dim dtsGrupoUsuarios As DataSet = objGeneral.Retorna_Dataset(strSQL)
'                intCont = dtsGrupoUsuarios.Tables(Lista.PRIMERA_TABLA).Rows.Count

'                If dtsGrupoUsuarios.Tables(Lista.PRIMERA_TABLA).Rows.Count > General.CERO Then

'                    Dim row As DataRow

'                    For Each row In dtsGrupoUsuarios.Tables(Lista.PRIMERA_TABLA).Rows
'                        strGrupos += "'"
'                        strGrupos += Trim(row("GRUP_Codigo"))
'                        strGrupos += "', "
'                        If Trim(row("GRUP_Codigo")) = General.GRUPO_SUPER_ADMINISTRADOR Then
'                            strGrupos = "'" & General.GRUPO_ADMINISTRADOR & "',"
'                            strGrupos += "'" & General.GRUPO_SUPER_ADMINISTRADOR & "'"
'                            Exit Try
'                        End If
'                    Next
'                End If
'                strGrupos = Mid(strGrupos, General.UNO, strGrupos.Length - General.DOS)

'                dtsGrupoUsuarios.Dispose()

'            Catch ex As Exception
'                Retorna_Grupo_Usuario = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            End Try

'        End Function

'        Public Function Pertenece_Grupo(ByVal strGrupo As String) As Boolean

'            Try
'                Dim strSQL As String
'                Dim intCont As Integer
'                Pertenece_Grupo = False

'                strSQL = "SELECT EMPR_Codigo, GRUP_Codigo FROM Grupo_Usuarios"
'                strSQL += " WHERE EMPR_Codigo=" & Me.objEmpresa.Codigo
'                strSQL += " AND GRUP_Codigo='" & strGrupo & "'"
'                strSQL += " AND USUA_Codigo='" & Me.strCodigo & "'"

'                Dim ResultSet As DataSet = objGeneral.Retorna_Dataset(strSQL)
'                intCont = ResultSet.Tables(Lista.PRIMERA_TABLA).Rows.Count

'                If ResultSet.Tables(Lista.PRIMERA_TABLA).Rows.Count > General.CERO Then
'                    Pertenece_Grupo = True
'                End If

'            Catch ex As Exception
'                Pertenece_Grupo = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            End Try

'        End Function

'        Public Sub Retorna_Permiso_Grupo_Usuario(ByVal lonCodigoOpcion As String, ByRef bolConsultar As Boolean, ByRef bolActualizar As Boolean,
'        ByRef bolEliminar As Boolean, ByRef bolImprimir As Boolean, Optional ByRef bolAyuda As Boolean = False, Optional ByRef strUrlAyuda As String = "", Optional ByRef bolAvisoActualizar As Boolean = False,
'        Optional ByRef bolAvisoEliminar As Boolean = False, Optional ByRef bolAuditar As Boolean = False)

'            Try

'                Dim strSQL As String, strOpcion As String
'                Dim intContConsultar = 0, intContActualizar = 0, intContEliminar = 0, intContImprimir = 0, intContAyuda = 0, intContAvisoActualizar = 0, intContAvisoEliminar = 0

'                strSQL = "SELECT PEGR.Consultar AS Consultar, PEGR.Actualizar AS Actualizar, PEGR.Eliminar_Anular As Eliminar," _
'                & " PEGR.Imprimir, MEAP.Aplica_Ayuda AS Ayuda, MEAP.Codigo, MEAP.Nombre, MEAP.URL_Ayuda," _
'                & " MEAP.Aplica_Envio_Correo_Actualizar as Aplica_Envio_Correo_Actualizar, MEAP.Aplica_Envio_Correo_Eliminar as Aplica_Envio_Correo_Eliminar, PEGR.Auditar as Puede_Auditar" _
'                & " FROM Menu_Aplicaciones AS MEAP, Permiso_Grupos AS PEGR" _
'                & " WHERE MEAP.EMPR_Codigo = PEGR.EMPR_Codigo" _
'                & " AND MEAP.MOAP_Codigo = PEGR.MOAP_Codigo" _
'                & " AND MEAP.Codigo = PEGR.MEAP_Codigo" _
'                & " AND MEAP.Codigo = " & lonCodigoOpcion & "" _
'                & " AND PEGR.Habilitar = 1"
'                '& " AND MEAP.EMPR_Codigo = " & Session("CODIGO_EMPRESA") _
'                '& " AND PEGR.GRUP_Codigo IN (" & Session("GRUPO_USUARIO") & ")" _

'                Dim ResultSet As DataSet = objGeneral.Retorna_Dataset(strSQL)

'                If ResultSet.Tables(Lista.PRIMERA_TABLA).Rows.Count > 0 Then
'                    Dim row As DataRow = ResultSet.Tables(Lista.PRIMERA_TABLA).Rows(0)
'                    For Each row In ResultSet.Tables(Lista.PRIMERA_TABLA).Rows
'                        If row("Consultar") = 1 Then
'                            bolConsultar = row("Consultar")
'                        End If
'                        If row("Actualizar") = 1 Then
'                            bolActualizar = row("Actualizar")
'                        End If
'                        If row("Eliminar") = 1 Then
'                            bolEliminar = row("Eliminar")
'                        End If
'                        If row("Imprimir") = 1 Then
'                            bolImprimir = row("Imprimir")
'                        End If
'                        If row("Ayuda") = 1 Then
'                            bolAyuda = row("Ayuda")
'                        End If
'                        If row("Aplica_Envio_Correo_Actualizar") = 1 Then
'                            bolAvisoActualizar = row("Aplica_Envio_Correo_Actualizar")
'                        End If
'                        If row("Aplica_Envio_Correo_Eliminar") = 1 Then
'                            bolAvisoEliminar = row("Aplica_Envio_Correo_Eliminar")
'                        End If
'                        If row("Puede_Auditar") = 1 Then
'                            bolAuditar = row("Puede_Auditar")
'                        End If
'                        strUrlAyuda = row("URL_Ayuda")
'                    Next
'                    If ResultSet.Tables(Lista.PRIMERA_TABLA).Rows.Count > 0 Then
'                        If objGeneral.intLogMenuUsuarios Then
'                            strOpcion = row("Codigo").ToString & "-" & row("Nombre").ToString
'                        End If
'                    End If
'                Else
'                    bolConsultar = False
'                    bolActualizar = False
'                    bolEliminar = False
'                    bolImprimir = False
'                    bolAyuda = False
'                    bolAvisoActualizar = False
'                    bolAvisoEliminar = False
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            End Try

'        End Sub

'        Public Function Opcion_Habilitada_Grupo_Usuario(ByVal strOpcion As String) As Boolean

'            Try
'                Dim strSQL As String
'                strSQL = "SELECT PEGR.Consultar AS Consultar, PEGR.Actualizar AS Actualizar, PEGR.Eliminar_Anular As Eliminar" _
'                & " FROM Menu_Aplicaciones AS MEAP, Permiso_Grupos AS PEGR" _
'                & " WHERE MEAP.EMPR_Codigo = PEGR.EMPR_Codigo" _
'                & " AND MEAP.MOAP_Codigo = PEGR.MOAP_Codigo" _
'                & " AND MEAP.Codigo = PEGR.MEAP_Codigo" _
'                & " AND MEAP.Nombre = '" & strOpcion & "'"
'                '& " AND MEAP.EMPR_Codigo = " & Session("CODIGO_EMPRESA") _
'                '& " AND PEGR.GRUP_Codigo = '" & Session("GRUPO_USUARIO") & "'" _
'                '& " AND PEGR.Habilitar = " & clsPermisoGrupo.HABILITADO

'                Dim ResultSet As DataSet = objGeneral.Retorna_Dataset(strSQL)

'                If ResultSet.Tables(Lista.PRIMERA_TABLA).Rows.Count > General.CERO Then
'                    Opcion_Habilitada_Grupo_Usuario = True
'                Else
'                    Opcion_Habilitada_Grupo_Usuario = False
'                End If
'            Catch ex As Exception
'                Opcion_Habilitada_Grupo_Usuario = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Consultar(Optional ByVal CargaCompleta As Boolean = True) As Boolean

'            Try
'                Dim sdrUsuario As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo,Codigo,Nombre,Descripcion,Habilitado,"
'                strSQL += " Login, Fecha_Ultimo_Login, OFIC_Codigo, Dias_Cambio_Clave, Fecha_Ultimo_Cambio_Clave,"
'                strSQL += " Fecha_Crea,USUA_Crea,Fecha_Modifica,USUA_Modifica,FUNC_Codigo,"
'                strSQL += " CABU_Codigo,Externo,CLIE_Codigo,MENSAJE,"
'                strSQL += " Clave, Prefijo_Contable_Ingreso, Prefijo_Contable_Egreso, Administrador_Usuarios, APLI_Codigo, Manager"
'                strSQL += " FROM Usuarios"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = '" & Me.strCodigo & "'"


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Call Instanciar_Objetos_Alternos()

'                Me.objGeneral.ConexionSQL.Open()
'                sdrUsuario = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrUsuario.Read() Then

'                    Me.strNombre = sdrUsuario("Nombre").ToString()
'                    Me.strDescripcion = sdrUsuario("Descripcion").ToString()
'                    Me.strClave = Me.objGeneral.Desencriptar_Clave_Usuario(Trim(sdrUsuario("Clave").ToString()))
'                    Me.strClaveActual = Me.objGeneral.Desencriptar_Clave_Usuario(Trim(sdrUsuario("Clave").ToString()))
'                    Me.intCuentaActiva = Val(sdrUsuario("Habilitado").ToString())

'                    Me.intLogin = Val(sdrUsuario("Login").ToString())
'                    Me.intDiasCambioClave = Val(sdrUsuario("Dias_Cambio_Clave").ToString())
'                    Date.TryParse(sdrUsuario("Fecha_Ultimo_Cambio_Clave").ToString(), Me.dteFechaUltimoCambioClave)

'                    Date.TryParse(sdrUsuario("Fecha_Ultimo_Login").ToString(), Me.dteFechaUltimoLogin)
'                    Me.intExterno = Val(sdrUsuario("Externo").ToString())
'                    Me.strMensaje = sdrUsuario("Mensaje").ToString()
'                    Me.strPrefijoContableIngreso = sdrUsuario("Prefijo_Contable_Ingreso").ToString()
'                    Me.strPrefijoContableEgreso = sdrUsuario("Prefijo_Contable_Egreso").ToString()

'                    Me.intAdministradorUsuarios = Val(sdrUsuario("Administrador_Usuarios").ToString)
'                    Me.intTipoAplicacion = Val(sdrUsuario("APLI_Codigo").ToString)
'                    Me.intManager = Val(sdrUsuario("Manager").ToString)

'                    If CargaCompleta Then

'                        Me.objFuncionario.Existe_Registro(Me.objEmpresa, Val(sdrUsuario("FUNC_Codigo")))
'                        Me.objOficina.Existe_Registro(Me.objEmpresa, sdrUsuario("OFIC_Codigo").ToString)

'                        If Me.intExterno > Lista.SELECCIONO_NO Then
'                            Cliente.Existe_Registro(Me.objEmpresa, Val(sdrUsuario("CLIE_Codigo")))
'                        End If

'                    Else

'                        Me.objFuncionario.Codigo = Val(sdrUsuario("FUNC_Codigo").ToString())
'                        Me.objCliente.Codigo = Val(sdrUsuario("CLIE_Codigo").ToString())
'                        Me.objOficina.Codigo = Val(sdrUsuario("OFIC_Codigo").ToString())

'                    End If
'                    Consultar = True
'                Else
'                    Me.strCodigo = ""
'                    Consultar = False
'                End If

'                sdrUsuario.Close()
'                sdrUsuario.Dispose()

'            Catch ex As Exception
'                Consultar = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As String) As Boolean

'            Try
'                Dim sdrUsuario As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                Me.objEmpresa = Empresa
'                Me.strCodigo = Codigo

'                'strSQL = "SELECT EMPR_Codigo, Nombre, OFIC_Codigo, rtrim(CONVERT(char,Clave)) AS Clave, Login, Habilitado FROM Usuarios"
'                strSQL = "SELECT EMPR_Codigo, Nombre, OFIC_Codigo, Clave, Login, Habilitado, Externo, CLIE_Codigo FROM Usuarios"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = '" & Me.strCodigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrUsuario = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrUsuario.Read() Then

'                    Me.strNombre = sdrUsuario("Nombre").ToString()
'                    Me.objOficina.Codigo = sdrUsuario("OFIC_Codigo").ToString
'                    Me.strClave = Me.objGeneral.Desencriptar_Clave_Usuario(Trim(sdrUsuario("Clave").ToString()))
'                    Me.Login = Val(sdrUsuario("Login").ToString)
'                    Me.CuentaActiva = Val(sdrUsuario("Habilitado").ToString)
'                    Me.Externo = Val(sdrUsuario("Externo").ToString)
'                    Me.Cliente.Codigo = Val(sdrUsuario("CLIE_Codigo").ToString)

'                    Existe_Registro = True
'                Else
'                    Me.strCodigo = ""
'                    Existe_Registro = False
'                End If

'                sdrUsuario.Close()
'                sdrUsuario.Dispose()

'            Catch ex As Exception
'                Existe_Registro = False
'                Me.strCodigo = ""

'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As String, Optional ByVal CargaCompleta As Boolean = True) As Boolean

'            Try

'                Dim sdrUsuario As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                Me.objEmpresa = Empresa

'                Me.strCodigo = Codigo

'                strSQL = "SELECT EMPR_Codigo,Codigo,Nombre,Descripcion,Habilitado,"
'                strSQL += " Login,Fecha_Ultimo_Login,OFIC_Codigo,Dias_Cambio_Clave,Fecha_Ultimo_Cambio_Clave,"
'                strSQL += " Fecha_Crea,USUA_Crea,Fecha_Modifica,USUA_Modifica,FUNC_Codigo,"
'                strSQL += " CABU_Codigo, Externo, CLIE_Codigo, Mensaje, Clave,  Prefijo_Contable_Ingreso, Prefijo_Contable_Egreso, Administrador_Usuarios, APLI_Codigo"
'                strSQL += " FROM Usuarios"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = '" & Me.strCodigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Call Instanciar_Objetos_Alternos()

'                Me.objGeneral.ConexionSQL.Open()
'                sdrUsuario = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrUsuario.Read() Then

'                    Me.strNombre = sdrUsuario("Nombre").ToString()
'                    Me.strDescripcion = sdrUsuario("Descripcion").ToString()
'                    Me.strClave = Me.objGeneral.Desencriptar_Clave_Usuario(Trim(sdrUsuario("Clave").ToString()))
'                    Me.strClaveActual = Me.objGeneral.Desencriptar_Clave_Usuario(Trim(sdrUsuario("Clave").ToString()))
'                    Me.intCuentaActiva = Val(sdrUsuario("Habilitado").ToString())

'                    Date.TryParse(sdrUsuario("Fecha_Ultimo_Cambio_Clave").ToString(), Me.dteFechaUltimoCambioClave)
'                    Me.intLogin = Val(sdrUsuario("Login").ToString())
'                    Me.intDiasCambioClave = Val(sdrUsuario("Dias_Cambio_Clave").ToString())

'                    Date.TryParse(sdrUsuario("Fecha_Ultimo_Login").ToString(), Me.dteFechaUltimoLogin)
'                    Me.intExterno = Val(sdrUsuario("Externo").ToString())
'                    Me.strMensaje = sdrUsuario("Mensaje").ToString()
'                    Me.strPrefijoContableIngreso = sdrUsuario("Prefijo_Contable_Ingreso").ToString()
'                    Me.strPrefijoContableEgreso = sdrUsuario("Prefijo_Contable_Egreso").ToString()
'                    Me.intAdministradorUsuarios = Val(sdrUsuario("Administrador_Usuarios").ToString)
'                    Me.intTipoAplicacion = Val(sdrUsuario("APLI_Codigo").ToString)

'                    If CargaCompleta Then

'                        Me.objFuncionario.Existe_Registro(Me.objEmpresa, Val(sdrUsuario("FUNC_Codigo")))
'                        Me.objOficina.Existe_Registro(Me.objEmpresa, Val(sdrUsuario("OFIC_Codigo").ToString))

'                        If Me.intExterno > Lista.SELECCIONO_NO Then
'                            Cliente.Existe_Registro(Me.objEmpresa, Val(sdrUsuario("CLIE_Codigo")))
'                        End If

'                    Else
'                        Me.objFuncionario.Codigo = Val(sdrUsuario("FUNC_Codigo").ToString())
'                        Me.objCliente.Codigo = Val(sdrUsuario("CLIE_Codigo").ToString())
'                        Me.objOficina.Codigo = Val(sdrUsuario("OFIC_Codigo").ToString())
'                    End If

'                    Consultar = True
'                Else
'                    Me.strCodigo = ""
'                    Consultar = False
'                End If

'                sdrUsuario.Close()
'                sdrUsuario.Dispose()

'            Catch ex As Exception
'                Consultar = False
'                Me.strCodigo = ""

'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Guardar(ByRef Mensaje As String) As Boolean

'            Try

'                Call Instanciar_Objetos_Alternos()

'                If Datos_Requeridos(Mensaje) Then
'                    If Not bolModificar Then
'                        Guardar = Insertar_SQL()
'                    Else
'                        Guardar = Modificar_SQL()
'                    End If
'                Else
'                    Guardar = False
'                End If
'                Mensaje = Me.strError

'            Catch ex As Exception
'                Guardar = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            End Try

'        End Function

'        Public Function Guardar_Grupo(ByRef Mensaje As String) As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_grupo_usuarios", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_GRUP_Codigo", SqlDbType.Char, 15) : ComandoSQL.Parameters("@par_GRUP_Codigo").Value = Me.strGrupo
'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.strCodigo
'                'ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Crea").Value = Session("USUARIO_ACTIVO")

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Guardar_Grupo = True
'                Else
'                    Guardar_Grupo = False
'                End If

'            Catch ex As Exception
'                Guardar_Grupo = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Mensaje = strError
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Guardar_Login(ByRef Mensaje As String) As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_usuario_login", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_Codigo").Value = Me.strCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Guardar_Login = True
'                Else
'                    Guardar_Login = False
'                End If

'            Catch ex As Exception
'                Guardar_Login = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Mensaje = strError
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Logout(ByRef Mensaje As String) As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_logout_usuario", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_Codigo").Value = Me.strCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Logout = True
'                Else
'                    Logout = False
'                End If

'            Catch ex As Exception
'                Logout = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Mensaje = strError
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Eliminar(ByRef Mensaje As String) As Boolean
'            Eliminar = Borrar_SQL()
'            Mensaje = Me.strError
'        End Function

'        Public Function Obtener_Numero_Usuarios_Conectados() As Integer

'            Try

'                Dim ComandoSQL As SqlCommand
'                Dim intNumeroUsuariosConectados As Integer

'                strSQL = "SELECT COUNT(*) FROM Usuarios"

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_USUARIOS_CONCURRENTES_NIVEL_GENERAL) Then
'                    strSQL += " WHERE Login = " & LOGEADO
'                Else
'                    strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                    strSQL += " AND Login = " & LOGEADO
'                End If

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                intNumeroUsuariosConectados = CInt(ComandoSQL.ExecuteScalar())
'                Return intNumeroUsuariosConectados

'            Catch ex As Exception
'                Obtener_Numero_Usuarios_Conectados = General.CERO
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try

'        End Function

'        Public Function Es_Usuario_Manager() As Boolean
'            Try
'                Dim ComandoSQL As SqlCommand
'                Dim intCant As Integer

'                Es_Usuario_Manager = False

'                strSQL = "SELECT COUNT(*)"
'                strSQL += " FROM Usuarios"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = '" & Me.Codigo & "'"
'                strSQL += " AND Manager = " & General.UNO

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
'                Me.objGeneral.ConexionSQL.Open()

'                intCant = CInt(ComandoSQL.ExecuteScalar())
'                If intCant = General.UNO Then
'                    Es_Usuario_Manager = True
'                End If

'            Catch ex As Exception
'                Es_Usuario_Manager = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Public Function Existe_Usuario_Manager() As Boolean
'            Try
'                Dim ComandoSQL As SqlCommand
'                Dim intCant As Integer

'                Existe_Usuario_Manager = False

'                strSQL = "SELECT COUNT(*)"
'                strSQL += " FROM Usuarios"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Manager = " & General.UNO

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
'                Me.objGeneral.ConexionSQL.Open()

'                intCant = CInt(ComandoSQL.ExecuteScalar())
'                If intCant = General.UNO Then
'                    Existe_Usuario_Manager = True
'                End If

'            Catch ex As Exception
'                Existe_Usuario_Manager = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        ''' <summary>
'        ''' Permite validar un usuario que se ha logueado a través de un web service 
'        ''' </summary>
'        ''' <param name="CodigoUsuario">usuario ingresado</param>
'        ''' <param name="Clave">clave del usuario</param>
'        ''' <param name="MensajeError">mensaje que se indica si el usuario es valido o no</param>
'        ''' <returns></returns>
'        ''' <remarks></remarks>

'        Public Function Validar_Usuario_Web_Services(ByVal CodigoUsuario As String, ByVal Clave As String, ByRef MensajeError As String) As Boolean

'            Try

'                Dim intDiaSemana As Integer = 0

'                intDiaSemana = Date.Now.DayOfWeek

'                If intDiaSemana = 0 Then
'                    intDiaSemana = 7
'                End If

'                'Dim objUsuarioHorario As New UsuarioHorario(Me.objEmpresa)

'                Dim strError As String = ""

'                If Not CodigoUsuario <> "" Then
'                    strError = "Debe ingresar el usuario"
'                    Return False
'                End If
'                If Not Clave <> "" Then
'                    strError = "Debe ingresar la contraseña"
'                    Return False
'                End If

'                If Existe_Registro(Me.objEmpresa, CodigoUsuario) Then

'                    If Trim(Me.Clave) = Me.objGeneral.Desencriptar_Clave_Usuario((Clave)) Then

'                        If Me.CuentaActiva = Usuario.CUENTA_ACTIVA Then
'                            'If Not Me.Login = Usuario.LOGEADO Then
'                            '    objUsuarioHorario.Usuarios.Codigo = strCodigo

'                            '    If objUsuarioHorario.Consultar(Me.objEmpresa, Me.strCodigo, intDiaSemana) Then

'                            '        If objUsuarioHorario.CataHoraInicio.Retorna_Codigo <= Date.Now.TimeOfDay.Hours And objUsuarioHorario.CataHoraFin.Retorna_Codigo > Date.Now.Hour Then

'                            '            If (Me.FechaUltimoCambioClave.AddDays(Me.DiasCambioClave) < Date.Today) Then
'                            '                strError = "La contraseña del usuario esta vencida"
'                            '            End If
'                            '            CodigoUsuario = UCase(CodigoUsuario)
'                            '            Me.Codigo = CodigoUsuario
'                            '            Retorna_Grupo_Usuario("")
'                            '            Return True

'                            '        Else
'                            '            MensajeError = "Este usuario no tiene permisos para ingresar este día y en este horario"
'                            '            Return False
'                            '        End If
'                            '    Else
'                            '        MensajeError = "Este usuario no tiene permisos para ingresar este día de la semana"
'                            '        Return False
'                            '    End If
'                            'Else
'                            '    MensajeError = "El usuario ya se encuentra logueado"
'                            '    Return False
'                            'End If
'                        Else
'                            MensajeError = "Esta cuenta se encuentra inactiva"
'                            Return False
'                        End If
'                    Else
'                        MensajeError = "Contraseña incorrecta"
'                        Return False
'                    End If
'                Else
'                    MensajeError = "Este usuario no existe en la base de datos"
'                    Return False
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Return False
'            Finally

'            End Try

'        End Function

'        ''' <summary>
'        ''' Permite cambiar la contraseña de un usuario a través de un web service 
'        ''' </summary>
'        ''' <param name="CodigoUsuario"></param>
'        ''' <param name="Clave"></param>
'        ''' <param name="ClaveNueva"></param>
'        ''' <param name="MensajeError"></param>
'        ''' <returns></returns>
'        ''' <remarks></remarks>

'        Public Function Actualizar_Clave_Usuario_Web_Services(ByVal CodigoUsuario As String, ByVal Clave As String, ByVal ClaveNueva As String, ByRef MensajeError As String) As Boolean

'            Try

'                Dim intDiaSemana As Integer = 0
'                intDiaSemana = Date.Now.DayOfWeek

'                If intDiaSemana = 0 Then
'                    intDiaSemana = 7
'                End If

'                'Dim objUsuarioHorario As New UsuarioHorario(Me.objEmpresa)
'                Dim strError As String = ""

'                If Not CodigoUsuario <> "" Then
'                    MensajeError = "Debe ingresar el usuario"
'                    Return False
'                End If
'                If Not Clave <> "" Then
'                    MensajeError = "Debe ingresar la contraseña"
'                    Return False
'                End If

'                If Existe_Registro(Me.objEmpresa, CodigoUsuario) Then


'                    If Trim(Me.Clave) = Clave Then
'                        If Validar_Clave(ClaveNueva) Then

'                            Me.strSQL = "UPDATE Usuarios SET Clave = '" & Me.objGeneral.Encriptar_Clave_Usuario(ClaveNueva) & "'," & vbNewLine
'                            Me.strSQL += " Fecha_Ultimo_Cambio_Clave = GETDATE()"
'                            Me.strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                            Me.strSQL += " AND Codigo = '" & CodigoUsuario & "'"

'                            If Me.objGeneral.Ejecutar_SQL(Me.strSQL, strError) Then
'                                Actualizar_Clave_Usuario_Web_Services = True
'                            Else
'                                Actualizar_Clave_Usuario_Web_Services = False
'                                MensajeError = "Sucedió un error al actualizar el usuario Error: " & strError
'                            End If
'                        Else
'                            Actualizar_Clave_Usuario_Web_Services = False
'                            MensajeError = "Debe ingresar una contraseña nueva fuerte." & " <Br />" & "La longitud debe ser mínimo 8 caracteres y máximo 10. Debe contener al menos un caracter númérico, un caracter alfabético y un caracter especial."
'                        End If

'                    Else
'                        Actualizar_Clave_Usuario_Web_Services = False
'                        MensajeError = "Contraseña incorrecta"
'                        Return False
'                    End If
'                Else
'                    Actualizar_Clave_Usuario_Web_Services = False
'                    MensajeError = "Este usuario no existe en la base de datos"
'                    Return False
'                End If

'            Catch ex As Exception
'                Actualizar_Clave_Usuario_Web_Services = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Return False
'            Finally
'            End Try

'        End Function

'#End Region

'#Region "Funciones Privadas"

'        Private Function Insertar_SQL() As Boolean

'            Try
'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_usuarios", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_Codigo").Value = Me.strCodigo
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_Descripcion", SqlDbType.VarChar, 40) : ComandoSQL.Parameters("@par_Descripcion").Value = Me.strDescripcion
'                ComandoSQL.Parameters.Add("@par_Clave", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_Clave").Value = Me.objGeneral.Encriptar_Clave_Usuario(Trim(Me.strClave))

'                ComandoSQL.Parameters.Add("@par_Habilitado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Habilitado").Value = Me.intCuentaActiva
'                ComandoSQL.Parameters.Add("@par_Login", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Login").Value = Me.intLogin
'                ComandoSQL.Parameters.Add("@par_OFIC_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_OFIC_Codigo").Value = Me.objOficina.Codigo
'                ComandoSQL.Parameters.Add("@par_FUNC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_FUNC_Codigo").Value = Me.objFuncionario.Codigo
'                ComandoSQL.Parameters.Add("@par_Dias_Cambio_Clave", SqlDbType.Int) : ComandoSQL.Parameters("@par_Dias_Cambio_Clave").Value = Me.intDiasCambioClave

'                ComandoSQL.Parameters.Add("@par_Fecha_Ultimo_Cambio_Clave", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Fecha_Ultimo_Cambio_Clave").Value = Me.FechaUltimoCambioClave
'                'ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Crea").Value = Session("USUARIO_ACTIVO")
'                ComandoSQL.Parameters.Add("@par_Externo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Externo").Value = Me.intExterno
'                ComandoSQL.Parameters.Add("@par_CLIE_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_CLIE_Codigo").Value = Me.objCliente.Codigo
'                ComandoSQL.Parameters.Add("@par_Mensaje", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Mensaje").Value = Me.strMensaje

'                ComandoSQL.Parameters.Add("@par_Prefijo_Contable_Ingreso", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Prefijo_Contable_Ingreso").Value = Me.strPrefijoContableIngreso
'                ComandoSQL.Parameters.Add("@par_Prefijo_Contable_Egreso", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Prefijo_Contable_Egreso").Value = Me.strPrefijoContableEgreso
'                ComandoSQL.Parameters.Add("@par_Administrador_Usuarios", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Administrador_Usuarios").Value = Me.intAdministradorUsuarios
'                ComandoSQL.Parameters.Add("@par_Tipo_Aplicacion_Usuario", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Tipo_Aplicacion_Usuario").Value = Me.intTipoAplicacion
'                ComandoSQL.Parameters.Add("@par_Manager", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Manager").Value = Me.intManager

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                Else
'                    Insertar_SQL = False
'                End If

'            Catch ex As Exception
'                Insertar_SQL = False
'                Try
'                    Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                    Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                    Try
'                        Me.objGeneral.ExcepcionSQL = ex
'                        Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                    Catch Exc As Exception
'                        Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                    End Try
'                Catch Exc As Exception
'                    Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Modificar_SQL() As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_usuarios", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_Codigo").Value = Me.strCodigo
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_Descripcion", SqlDbType.VarChar, 40) : ComandoSQL.Parameters("@par_Descripcion").Value = Me.strDescripcion
'                ComandoSQL.Parameters.Add("@par_Clave", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Clave").Value = Me.objGeneral.Encriptar_Clave_Usuario(Trim(Me.strClave))

'                ComandoSQL.Parameters.Add("@par_Habilitado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Habilitado").Value = Me.intCuentaActiva
'                ComandoSQL.Parameters.Add("@par_Login", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Login").Value = Me.intLogin
'                ComandoSQL.Parameters.Add("@par_OFIC_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_OFIC_Codigo").Value = Me.objOficina.Codigo
'                ComandoSQL.Parameters.Add("@par_FUNC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_FUNC_Codigo").Value = Me.objFuncionario.Codigo
'                ComandoSQL.Parameters.Add("@par_Dias_Cambio_Clave", SqlDbType.Int) : ComandoSQL.Parameters("@par_Dias_Cambio_Clave").Value = Me.intDiasCambioClave

'                ComandoSQL.Parameters.Add("@par_Fecha_Ultimo_Cambio_Clave", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Fecha_Ultimo_Cambio_Clave").Value = Me.FechaUltimoCambioClave
'                'ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Modifica").Value = Session("USUARIO_ACTIVO")
'                ComandoSQL.Parameters.Add("@par_Externo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Externo").Value = Me.intExterno
'                ComandoSQL.Parameters.Add("@par_CLIE_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_CLIE_Codigo").Value = Me.objCliente.Codigo
'                ComandoSQL.Parameters.Add("@par_Mensaje", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Mensaje").Value = Me.strMensaje

'                ComandoSQL.Parameters.Add("@par_Prefijo_Contable_Ingreso", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Prefijo_Contable_Ingreso").Value = Me.strPrefijoContableIngreso
'                ComandoSQL.Parameters.Add("@par_Prefijo_Contable_Egreso", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Prefijo_Contable_Egreso").Value = Me.strPrefijoContableEgreso
'                ComandoSQL.Parameters.Add("@par_Administrador_Usuarios", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Administrador_Usuarios").Value = Me.intAdministradorUsuarios
'                ComandoSQL.Parameters.Add("@par_Tipo_Aplicacion_Usuario", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Tipo_Aplicacion_Usuario").Value = Me.intTipoAplicacion
'                ComandoSQL.Parameters.Add("@par_Manager", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Manager").Value = Me.intManager

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'            Catch ex As Exception
'                Modificar_SQL = False

'                Try
'                    Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                    Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                    Try
'                        Me.objGeneral.ExcepcionSQL = ex
'                        Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                    Catch Exc As Exception
'                        Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                    End Try
'                Catch Exc As Exception
'                    Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Borrar_SQL() As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_usuarios", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_Codigo").Value = Me.strCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If

'            Catch ex As Exception

'                Borrar_SQL = False

'                Try
'                    Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                    Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                    Try
'                        Me.objGeneral.ExcepcionSQL = ex
'                        Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                    Catch Exc As Exception
'                        Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                    End Try
'                Catch Exc As Exception
'                    Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean

'            Try

'                Call Instanciar_Objetos_Alternos()

'                If Not Me.isCambioContrasena Then
'                    Datos_Requeridos = Datos_Requeridos_Usuario(Mensaje)
'                Else
'                    Datos_Requeridos = Datos_Requeridos_Cambiar_Contrasena(Mensaje)
'                End If

'            Catch ex As Exception
'                Datos_Requeridos = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            End Try

'        End Function

'        Private Function Datos_Requeridos_Cambiar_Contrasena(ByRef Mensaje As String) As Boolean

'            Try

'                Me.strError = ""
'                Dim intCont As Short
'                intCont = 0
'                Datos_Requeridos_Cambiar_Contrasena = True

'                If Len(Me.strConfirmacionClaveActual) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar la contraseña actual del usuario."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Cambiar_Contrasena = False
'                Else
'                    If Not Trim(Me.strClaveActual) = Me.strConfirmacionClaveActual Then
'                        intCont += 1
'                        Me.strError += intCont & ". La contraseña actual no corresponde al usuario."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Cambiar_Contrasena = False
'                    End If
'                End If

'                If Me.strClaveActual = Me.strClave Then
'                    intCont += 1
'                    Me.strError += intCont & ". La contraseña nueva debe ser diferente de la actual."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Cambiar_Contrasena = False
'                End If

'                If Len(Me.strClave) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar la contraseña nueva del usuario."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Cambiar_Contrasena = False
'                Else
'                    If Not Validar_Clave(Me.strClave) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar una contraseña nueva fuerte." & " <Br />" & "La longitud debe ser mínimo 8 caracteres y máximo 10. Debe contener al menos un caracter númérico, un caracter alfabético y un caracter especial."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Cambiar_Contrasena = False
'                    End If
'                End If

'                If Len(Me.strConfirmacionClave) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar la confirmación de la contraseña del usuario."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Cambiar_Contrasena = False
'                End If

'                If Me.strClave <> Me.strConfirmacionClave Then
'                    intCont += 1
'                    Me.strError += intCont & ". Las contraseñas nueva y confirmación no coinciden."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Cambiar_Contrasena = False
'                End If

'            Catch ex As Exception
'                Datos_Requeridos_Cambiar_Contrasena = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            End Try

'        End Function

'        Private Function Datos_Requeridos_Usuario(ByRef Mensaje As String) As Boolean

'            Try

'                Me.strError = ""
'                Dim intCont As Short
'                intCont = 0
'                Datos_Requeridos_Usuario = True

'                If Len(Me.strCodigo) = 0 Then
'                    intCont += 1
'                    Me.strError = intCont & ". Debe digitar el código del usuario."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Usuario = False
'                Else
'                    If Len(Me.strCodigo) < General.CARACTERES_BUSQUEDA_NOMBRE Then
'                        intCont += 1
'                        Me.strError += intCont & ". El código de usuario debe tener al menos cinco (5) caracteres."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Usuario = False
'                    End If

'                End If

'                If Len(Me.strNombre) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar el nombre del usuario."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Usuario = False
'                Else
'                    If Len(Me.strNombre) < General.CARACTERES_BUSQUEDA_NOMBRE Then
'                        intCont += 1
'                        Me.strError += intCont & ". El nombre del usuario debe tener al menos cinco (5) caracteres."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Usuario = False
'                    End If
'                End If

'                If Len(Me.strClave) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar la contraseña de usuario."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Usuario = False
'                Else
'                    If Not Validar_Clave(Me.strClave) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar una contraseña fuerte." & " <Br />" & "La longitud debe ser mínimo 8 caracteres y máximo 10. Debe contener al menos un caracter númérico, un caracter alfabético y un caracter especial."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Usuario = False
'                    End If
'                End If

'                If Len(Me.strConfirmacionClave) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar la confirmación de la contraseña del usuario."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Usuario = False
'                End If

'                If Me.strClave <> Me.strConfirmacionClave Then
'                    intCont += 1
'                    Me.strError += intCont & ". Las contraseña y la confirmación no coinciden."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Usuario = False
'                End If

'                If Me.intDiasCambioClave <= 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el número de días para el próximo cambio de clave."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Usuario = False
'                End If

'                If Me.objOficina.Codigo = General.CERO Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe seleccionar la oficina."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Usuario = False
'                End If

'                If Me.intExterno <> 0 Then
'                    If Me.objCliente.Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la identificación del cliente y su nombre."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Usuario = False
'                    End If
'                Else
'                    Me.objCliente.Codigo = Tercero.CODIGO_EMPRESA_PRINCIPAL
'                End If

'                If Me.intCambiarClave <> 0 Then
'                    Me.dteFechaUltimoCambioClave = General.FECHA_NULA
'                Else
'                    Me.dteFechaUltimoCambioClave = Date.Today
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Datos_Requeridos_Usuario = False
'            End Try

'        End Function

'        Private Sub Instanciar_Objetos_Alternos()

'            Try

'                If IsNothing(Me.objFuncionario) Then
'                    Me.objFuncionario = New Tercero(Me.objEmpresa)
'                End If
'                If IsNothing(Me.objCliente) Then
'                    Me.objCliente = New Tercero(Me.objEmpresa)
'                End If

'            Catch ex As Exception
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            End Try

'        End Sub

'        Private Function Validar_Clave(ByVal strClave As String) As Boolean

'            Try

'                Dim intCon As Integer
'                Dim bolNumero As Boolean
'                Dim bolCaraEspe As Boolean
'                Dim bolLetra As Boolean
'                Dim chrCaracter As Char

'                bolNumero = False
'                bolCaraEspe = False
'                bolLetra = False

'                'Validar longitud de 8
'                If Len(Trim$(strClave)) < MINIMO_CARACTERES Then
'                    Validar_Clave = False
'                    Exit Function
'                End If

'                'Validar longitud máxima de 10
'                If Len(Trim$(strClave)) > MAXIMO_CARACTERES Then
'                    Validar_Clave = False
'                    Exit Function
'                End If
'                Validar_Clave = True
'                'Validar que tenga al menos un número
'                For intCon = 1 To Len(strClave)

'                    Char.TryParse(Mid$(strClave, intCon, 1), chrCaracter)

'                    If Char.IsNumber(chrCaracter) Then
'                        bolNumero = True
'                    End If

'                    If Char.IsSymbol(chrCaracter) Or Char.IsPunctuation(chrCaracter) Then
'                        bolCaraEspe = True
'                    End If

'                    If Char.IsLetter(chrCaracter) Then
'                        bolLetra = True
'                    End If

'                    If bolNumero And bolCaraEspe And bolLetra Then
'                        Validar_Clave = True
'                        Exit For
'                    Else
'                        Validar_Clave = False
'                    End If

'                Next

'            Catch ex As Exception
'                Validar_Clave = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            End Try

'        End Function

'#End Region
'    End Class
'End Namespace




