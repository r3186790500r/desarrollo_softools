﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Negocio.Facturacion

<Authorize>
Public Class NotasFacturasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As NotasFacturas) As Respuesta(Of IEnumerable(Of NotasFacturas))
        Return New LogicaNotasFacturas(CapaPersistenciaNotasFacturas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As NotasFacturas) As Respuesta(Of NotasFacturas)
        Return New LogicaNotasFacturas(CapaPersistenciaNotasFacturas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As NotasFacturas) As Respuesta(Of Long)
        Return New LogicaNotasFacturas(CapaPersistenciaNotasFacturas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As NotasFacturas) As Respuesta(Of Boolean)
        Return New LogicaNotasFacturas(CapaPersistenciaNotasFacturas).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerDatosNotaElectronicaSaphety")>
    Public Function ObtenerDatosFacturaElectronicaSaphety(ByVal filtro As NotasFacturas) As Respuesta(Of NotasFacturas)
        Return New LogicaNotasFacturas(CapaPersistenciaNotasFacturas).ObtenerDatosNotaElectronicaSaphety(filtro)
    End Function

    <HttpPost>
    <ActionName("GuardarNotaElectronica")>
    Public Function GuardarNotaElectronica(ByVal entidad As NotasFacturas) As Respuesta(Of Long)
        Return New LogicaNotasFacturas(CapaPersistenciaNotasFacturas).GuardarNotaElectronica(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerDocumentoReporteSaphety")>
    Public Function ObtenerDocumentoReporteSaphety(ByVal filtro As NotasFacturas) As Respuesta(Of NotasFacturas)
        Return New LogicaNotasFacturas(CapaPersistenciaNotasFacturas).ObtenerDocumentoReporteSaphety(filtro)
    End Function

End Class
