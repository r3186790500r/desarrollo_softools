﻿EncoExpresApp.factory('FotosVehiculosFactory', ['$http', function ($http) {

    var urlService = GetUrl('FotosVehiculos');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    };

    factory.InsertarTemporal = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/InsertarTemporal';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.EliminarFoto = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/EliminarFoto';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.LimpiarTemporalUsuario = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/LimpiarTemporalUsuario';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    return factory;
}]);