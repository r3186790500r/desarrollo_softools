﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Despachos.Planilla
    <JsonObject>
    Public Class DetalleAuxiliaresPlanillaDespachos
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleAuxiliaresPlanillaDespachos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroPlanilla = Read(lector, "ENPD_Numero")
            Tercero = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Funcionario"), .Nombre = Read(lector, "Nombre_Funcionario")}
            Horas = Read(lector, "Numero_Horas_Trabajadas")
            Valor = Read(lector, "Valor")
            Observaciones = Read(lector, "Observaciones")


        End Sub


        <JsonProperty>
        Public Property Tercero As Terceros

        <JsonProperty>
        Public Property NumeroPlanilla As Integer

        <JsonProperty>
        Public Property Horas As Integer

        <JsonProperty>
        Public Property Valor As Double


    End Class
End Namespace
