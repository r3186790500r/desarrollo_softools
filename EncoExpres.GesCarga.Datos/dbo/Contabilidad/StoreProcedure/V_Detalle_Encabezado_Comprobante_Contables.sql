﻿CREATE VIEW V_Detalle_Encabezado_Comprobante_Contables  
AS  
SELECT   
       EMPR.Nombre_Razon_Social,  
       DECC.EMPR_Codigo,  
       ENCC.TIDO_Codigo,--TIDO_Comprobante_Contable (45)  
       ENCC.TIDO_Documento,--TIDO_Comprobante del documento  
       ENCC.Numero,  
       ENCC.Numero_Documento,  
       ENCC.Fecha_Documento,  
       ENCC.TERC_Documento,  
       DECC.ID,  
  
       DECC.TERC_Codigo,  
       DECC.Valor_Debito,  
       DECC.Valor_Credito,  
       DECC.Valor_Base,  
       DECC.Observaciones,  
  
       DECC.Centro_Costo,   
       DECC.Documento_Cruce,  
       DECC.Prefijo,  
       DECC.Codigo_Anexo,  
  
       DECC.Sufijo_Codigo_Anexo,  
      ENCC.Fecha,  
  
       DECC.Centro_Costo AS CECO_Documento, -- CENTRO COSTO  
  
       ENCC.OFIC_Codigo AS OFIC_Documento,  
       ENCC.Año,  
       ENCC.Periodo,  
       VEHI.Placa AS VEHI_Placa ,--ENPD.VEHI_Codigo,     
       ENCC.Observaciones as ObservacionesEncabezado,    
  
       ENCC.Valor_Comprobante,     
       ENDC.TIDO_Codigo AS TIDO_Documento_Cruce,  
       DECC.Documento_Cruce AS Numero_Documento_Cruce,  
       ENCC.Anulado,  
       ENDC.Estado,   
  
       ENDC.Fecha_Crea,     
       USUACR.Nombre AS USUA_Crea, 
	   TEEX.Numero_Identificacion AS USUA_Identificacion_Crea,
       ENDC.Fecha_Modifica,     
       USUAMO.Nombre AS USUA_Modifica,     
       USUANU.Nombre AS USUA_Anula,     
    
       ENDC.Fecha_Anula,     
       ENDC.Causa_Anulacion AS Causa_Anula,    
       ENCC.OFIC_Codigo,    
       ENCC.Interfase_Contable AS Interfaz_contable,  
       PLUC.Codigo AS PLUC_Codigo,  
       PLUC.Nombre as NombreCuenta   
  
FROM  Detalle_Comprobante_Contables DECC  
  
       INNER JOIN Encabezado_Comprobante_Contables ENCC ON  
       DECC.EMPR_Codigo = ENCC.EMPR_Codigo  
       AND DECC.ENCC_Numero = ENCC.Numero  
  
       INNER JOIN Empresas EMPR ON  
       DECC.EMPR_Codigo = EMPR.Codigo  
  
       --  
       INNER JOIN Detalle_Documento_Cuenta_Comprobantes DDCC on  
       ENCC.EMPR_Codigo = DDCC.EMPR_Codigo   
       AND ENCC.Codigo_Documento =  DDCC.EDCO_Codigo  
  
       INNER JOIN Encabezado_Documento_Comprobantes ENDC on  
       DDCC.EMPR_Codigo = ENDC.EMPR_Codigo  
       AND DDCC.EDCO_Codigo = ENDC.Codigo  
         
       --- IMPORTATE  
       LEFT JOIN Encabezado_Planilla_Despachos ENPD ON  
       ENCC.CATA_DOOR_Codigo IN (2604,2605)  
       AND ENCC.EMPR_Codigo = ENPD.EMPR_Codigo  
       AND ENCC.Codigo_Documento = ENPD.Numero  
  
       LEFT JOIN Vehiculos VEHI ON  
       ENPD.EMPR_Codigo = VEHI.EMPR_Codigo  
          AND ENPD.VEHI_Codigo = VEHI.Codigo  
  
       INNER JOIN Usuarios USUACR ON   
       ENDC.EMPR_Codigo = USUACR.EMPR_Codigo  
       AND ENDC.USUA_Codigo_Crea = USUACR.Codigo  

	   /*Modificación AE 18/06/2020*/
	   LEFT JOIN Terceros TEEX ON
	   ENDC.EMPR_Codigo = TEEX.EMPR_Codigo AND
	   ENDC.USUA_Codigo_Crea = TEEX.Codigo
	   /*Fin Modificación*/
  
    -- LEFT JOIN en caso que no tenga un usuario que modifica  
       LEFT JOIN Usuarios USUAMO ON   
       ENDC.EMPR_Codigo = USUAMO.EMPR_Codigo  
       AND ENDC.USUA_Codigo_Modifica = USUAMO.Codigo  
       -- LEFT JOIN en caso que no tenga un usuario que anula  
       LEFT JOIN Usuarios USUANU ON   
       ENDC.EMPR_Codigo = USUANU.EMPR_Codigo  
       AND ENDC.USUA_Codigo_Anula = USUANU.Codigo  
  
       INNER JOIN Plan_Unico_Cuentas PLUC ON   
       DECC.EMPR_Codigo = PLUC.EMPR_Codigo  
       AND DECC.PLUC_Codigo = PLUC.Codigo  

GO