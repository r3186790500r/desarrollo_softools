﻿EncoExpresApp.controller("GestionarTarifarioVentasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TarifarioVentasFactory', 'LineaNegocioTransportesFactory', 'TarifaTransportesFactory', 'TipoLineaNegocioTransportesFactory', 'TipoTarifaTransportesFactory', 'RutasFactory', 'CiudadesFactory', 'ValorCatalogosFactory', 'ProductoTransportadosFactory', 'blockUIConfig',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TarifarioVentasFactory, LineaNegocioTransportesFactory, TarifaTransportesFactory, TipoLineaNegocioTransportesFactory, TipoTarifaTransportesFactory, RutasFactory, CiudadesFactory, ValorCatalogosFactory, ProductoTransportadosFactory, blockUIConfig) {
        console.clear()
        blockUI.stop()
        $scope.Titulo = 'GESTIONAR TARIFARIO VENTAS';
        $scope.MapaSitio = [{ Nombre: 'Comercial' }, { Nombre: 'Documentos' }, { Nombre: 'Tarifario Ventas' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_TARIFARIO_VENTAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
         

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.NumeroTarifario = 0;
        $scope.HabilitarBotonesFiltros = true;
        var Nombre = '';
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }
        $scope.Modal = {}
        $scope.ListaCiudadOrigen = [];
        $scope.ListaCiudadDestino = [];
        $scope.ListadoTarifarioVentas = [];
        $scope.ListaFormaPagoVentas = [];
        $scope.HabilitarFormaPago = false
        $scope.habilitarTarifarioBase = false
        $scope.HabilitarLineaNegocio = true
        $scope.HabilitarTipoLineaNegocio = true
        $scope.Buscando = true;
        $scope.ResultadoSinRegistros = '';
        $scope.TarifasEliminar = [];
        $scope.CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA = CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA;
        $scope.CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO = CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO;
        $scope.Modelo.Paqueteria = {};
        $scope.LineaNegocio = '';
        $scope.TipoLineaNegocio = '';
        $scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral = [];
        //$scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral.push({ Nombre: '(TODOS)', Codigo: -1 });
        //$scope.TipoLineaNegocio = $linq.Enumerable().From($scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral).First('$.Codigo== -1');
        $scope.TipoLineaNegocio = $scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral[0];

        $scope.ListadoReexpedicion = [
            { Codigo: 0, Nombre: 'NO' },
            {Codigo: 1, Nombre:'SI'}
        ];
        /*Cargar el combo modo transporte*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaFormaPagoVentas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaFormaPagoVentas = response.data.Datos;
                        $scope.Modal.FormaPagoVentas = $scope.ListaFormaPagoVentas[0]
                    }
                    else {
                        $scope.ListaFormaPagoVentas = []
                    }
                }
            }, function (response) {
            });


        TarifarioVentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTarifarioVentas = [];
                        $scope.ListadoTarifarioVentas = response.data.Datos

                        $scope.ListadoTarifarioVentas.forEach(function (item) {
                            if (item.TarifarioBase == 1) {
                                $scope.habilitarTarifarioBase = true
                            }
                        })
                    }
                }
            })

        $scope.CargarDatosFunciones = function () {
            $scope.ListadoEstados = [
                { Codigo: 0, Nombre: "INACTIVA" },
                { Codigo: 1, Nombre: "ACTIVA" },
            ]
            try {
                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado.Codigo);
                for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                    var tarifa = $scope.Modelo.Tarifas[i]
                    for (var j = 0; j < $scope.ListadoEstados.length; j++) {
                        var estado = $scope.ListadoEstados[j]
                        try {
                            if (tarifa.Estado.Codigo == estado.Codigo) {
                                tarifa.Estado = estado
                            }
                        } catch (e) {

                        }

                    }
                }
            } catch (e) {
                $scope.Modelo.Estado = $scope.ListadoEstados[1]
            }

            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
            /*Cargar el combo de LineaNegocioTransportes*/
            LineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoLineaNegocioTransportes = [];
                        if (response.data.Datos.length > 0) {
                           // $scope.ListadoLineaNegocioTransportes.push({ Nombre: '(TODOS)', Codigo :-1 });
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListadoLineaNegocioTransportes.push(response.data.Datos[i]);
                            }
                            $scope.Modal.LineaNegocioTransportes = $scope.ListadoLineaNegocioTransportes[0];
                            $scope.LineaNegocio = $scope.ListadoLineaNegocioTransportes[0];
                            $scope.CargarTipoLineaFiltrosGeneral($scope.LineaNegocio.Codigo)
                            //$scope.Modal.LineaNegocioTransportes = $linq.Enumerable().From($scope.ListadoLineaNegocioTransportes).First('$.Codigo== -1');
                            //$scope.LineaNegocio = $linq.Enumerable().From($scope.ListadoLineaNegocioTransportes).First('$.Codigo== -1');
                            if ($scope.Modelo.Tarifas != null) {
                                for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                    var tarifa = $scope.Modelo.Tarifas[i]
                                    for (var j = 0; j < $scope.ListadoLineaNegocioTransportes.length; j++) {
                                        var linea = $scope.ListadoLineaNegocioTransportes[j]
                                        try {
                                            if (tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == linea.Codigo) {
                                                tarifa.LineaNegocioTransportes = linea
                                            }
                                        } catch (e) {

                                        }

                                    }
                                    tarifa.Mostrar = true;
                                }
                            }
                        
                            try { $scope.ListadoTarifas = AgruparListados($scope.Modelo.Tarifas, 'LineaNegocioTransportes', 'TipoLineaNegocioTransportes'); } catch (e) { }
                        }
                        else {
                            $scope.ListadoLineaNegocioTransportes = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TarifaTransportes*/
            TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTarifaTransportes = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTarifaTransportes = response.data.Datos;
                            //$scope.Modal.TarifaTransportes = $scope.ListadoTarifaTransportes[0]
                            if ($scope.Modelo.Tarifas != null) {
                                for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                    var tarifa = $scope.Modelo.Tarifas[i]
                                    for (var j = 0; j < $scope.ListadoTarifaTransportes.length; j++) {
                                        var lstarifa = $scope.ListadoTarifaTransportes[j]
                                        try {
                                            if (tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == lstarifa.Codigo) {
                                                tarifa.TarifaTransportes = lstarifa
                                            }
                                        } catch (e) {

                                        }

                                    }
                                }
                            }
                            try { $scope.ListadoTarifas = AgruparListados($scope.Modelo.Tarifas, 'LineaNegocioTransportes', 'TipoLineaNegocioTransportes'); } catch (e) { }
                        }
                        else {
                            $scope.ListadoTarifaTransportes = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TipoLineaNegocioTransportes*/
            var ResponseTipoLinea = TipoLineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true });
                
            if (ResponseTipoLinea.ProcesoExitoso === true) {
                        $scope.ListadoTipoLineaNegocioTransportes = [];
                if (ResponseTipoLinea.Datos.length > 0) {

                    $scope.ListadoTipoLineaNegocioTransportes = ResponseTipoLinea.Datos;
                    $scope.ListadoTipoLineaNegocioTransportes.push({ Nombre: '(TODOS)', Codigo: -1 });
                    //$scope.TipoLineaNegocio = $linq.Enumerable().From($scope.ListadoTipoLineaNegocioTransportes).First('$.Codigo== -1');

                    if ($scope.Modelo.Tarifas != null) {
                        for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                            var tarifa = $scope.Modelo.Tarifas[i]
                            for (var j = 0; j < $scope.ListadoTipoLineaNegocioTransportes.length; j++) {
                                var Linea = $scope.ListadoTipoLineaNegocioTransportes[j]
                                try {
                                    if (tarifa.TipoLineaNegocioTransportes.Codigo == Linea.Codigo) {
                                        tarifa.TipoLineaNegocioTransportes = Linea
                                    }
                                } catch (e) {

                                }

                            }
                        }
                    }
                    try { $scope.ListadoTarifas = AgruparListados($scope.Modelo.Tarifas, 'LineaNegocioTransportes', 'TipoLineaNegocioTransportes'); } catch (e) { }
                }
                else {
                    $scope.ListadoTipoLineaNegocioTransportes = []
                }
            }
               
            /*Cargar el combo de TipoTarifaTransportes*/
            TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTarifaTransportes = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoTarifaTransportes = response.data.Datos;
                            if ($scope.Modelo.Tarifas != null) {
                                for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                    var tarifa = $scope.Modelo.Tarifas[i]
                                    for (var j = 0; j < $scope.ListadoTipoTarifaTransportes.length; j++) {
                                        var Linea = $scope.ListadoTipoTarifaTransportes[j]
                                        try {
                                            if (tarifa.TipoTarifaTransportes.Codigo == Linea.Codigo && tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == Linea.TarifaTransporte.Codigo && !(tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 301 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 300 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 302 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 6 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 5)) {
                                                tarifa.TipoTarifaTransportes = Linea
                                            }
                                        } catch (e) {

                                        }

                                    }
                                }
                            }
                            try { $scope.ListadoTarifas = AgruparListados($scope.Modelo.Tarifas, 'LineaNegocioTransportes', 'TipoLineaNegocioTransportes'); } catch (e) { }
                        }
                        else {
                            $scope.ListadoTipoTarifaTransportes = []
                        }
                    }
                }, function (response) {
                });
            RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoRutas = [];
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListadoRutas.push(response.data.Datos[i]);
                            }
                            $scope.Modal.Rutas = $scope.ListadoRutas[0]
                            if ($scope.Modelo.Tarifas != null) {
                                for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                    var tarifa = $scope.Modelo.Tarifas[i]
                                    for (var j = 0; j < $scope.ListadoRutas.length; j++) {
                                        var ruta = $scope.ListadoRutas[j]
                                        try {
                                            if (tarifa.Ruta.Codigo == ruta.Codigo) {
                                                tarifa.Ruta = ruta
                                            }
                                        } catch (e) {

                                        }

                                    }
                                }
                            }
                            try { $scope.ListadoTarifas = AgruparListados($scope.Modelo.Tarifas, 'LineaNegocioTransportes', 'TipoLineaNegocioTransportes'); } catch (e) { }
                            $scope.FiltrarRutasFiltrosGeneral()
                        }
                        else {
                            $scope.ListadoRutas = []
                        }
                    }
                }, function (response) {
                });
            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, AplicaTarifario: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoProductosTransportados = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoProductosTransportados = response.data.Datos;
                            for (var j = 0; j < $scope.ListadoProductosTransportados.length; j++) {
                                $scope.ListadoProductosTransportados[j] = { Codigo: $scope.ListadoProductosTransportados[j].Codigo, Nombre: $scope.ListadoProductosTransportados[j].Nombre }
                            }
                            if ($scope.Modelo.Tarifas != null) {
                                for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                    var tarifa = $scope.Modelo.Tarifas[i]
                                    for (var j = 0; j < $scope.ListadoProductosTransportados.length; j++) {
                                        var Linea = JSON.parse(JSON.stringify($scope.ListadoProductosTransportados[j]))
                                        try {
                                            if (tarifa.TipoTarifaTransportes.Codigo == Linea.Codigo && (tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 301 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 300 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 302 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 6 || tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 5)) {
                                                Linea.TarifaTransporte = tarifa.TipoTarifaTransportes.TarifaTransporte
                                                tarifa.TipoTarifaTransportes = Linea
                                            }
                                        } catch (e) {

                                        }

                                    }
                                }
                            }
                            try { $scope.ListadoTarifas = AgruparListados($scope.Modelo.Tarifas, 'LineaNegocioTransportes', 'TipoLineaNegocioTransportes'); } catch (e) { }
                        }
                        else {
                            $scope.ListadoProductosTransportados = []
                        }
                    }
                }, function (response) {
                });

        }
        $scope.ListadoCiudades = [];
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }
        $scope.CargarTipoLinea = function (Codigo) {

            $scope.ListadoTipoLineaNegocioTransportesFiltrado = []
            $scope.ListadoTipoTarifaTransportesFiltrado = []
            $scope.ListadoTarifaTransportesFiltrado = []
            $scope.ListadoTipoLineaNegocioTransportesFiltrado.push({ Nombre: '(NO APLICA)' });
            if (Codigo == 1/*Masivo*/) {
                $scope.Modal.FormaPagoVentas = $linq.Enumerable().From($scope.ListaFormaPagoVentas).First('$.Codigo==' + CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO)
            } else if (Codigo == 3/*Paquetería*/) {
                if ($scope.Sesion.UsuarioAutenticado.ProcesoOcultarFormaPagoTarifarios) {
                    $scope.Modal.FormaPagoVentas = $linq.Enumerable().From($scope.ListaFormaPagoVentas).First('$.Codigo==' + CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO)
                } else {
                    $scope.Modal.FormaPagoVentas = $linq.Enumerable().From($scope.ListaFormaPagoVentas).First('$.Codigo==' + CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA)
                }
            }
            for (var i = 0; i < $scope.ListadoTipoLineaNegocioTransportes.length; i++) {
                var item = $scope.ListadoTipoLineaNegocioTransportes[i];
                if (item.LineaNegocioTransporte != null) {
                    if (item.LineaNegocioTransporte.Codigo == Codigo) {
                        $scope.ListadoTipoLineaNegocioTransportesFiltrado.push(item)
                    }
                }
            }
            $scope.Modal.TipoLineaNegocioTransportes = $scope.ListadoTipoLineaNegocioTransportesFiltrado[0]
            $scope.HabilitarLineaNegocio = false
            $scope.HabilitarTipoLineaNegocio = true
            $scope.HabilitarTipoTarifa = true


            if (Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA || Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO) {
                if (Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA) {
                    $scope.HabilitarRuta = false
                } else {
                    $scope.HabilitarRuta = true
                    $scope.Modal.Reexpedicion = false;
                }
                if (!$scope.Sesion.UsuarioAutenticado.ProcesoOcultarFormaPagoTarifarios) {
                    $scope.HabilitarFormaPago = true
                }

            }
            else {
                $scope.HabilitarRuta = true
                $scope.HabilitarFormaPago = false
                $scope.Modal.Reexpedicion = false;
            }

        }
        $scope.CargarTipoLineaFiltrosGeneral = function (Codigo) {
            if ($scope.LineaNegocio != undefined && $scope.LineaNegocio != null && $scope.LineaNegocio != '') {
                $scope.HabilitarBotonesFiltros = false;
            }

            $scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral = []
            $scope.ListadoTipoTarifaTransportesFiltrado = []
            $scope.ListadoTarifaTransportesFiltrado = []
            //$scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral.push({ Nombre: '(TODOS)', Codigo: -1 });
            for (var i = 0; i < $scope.ListadoTipoLineaNegocioTransportes.length; i++) {
                var item = $scope.ListadoTipoLineaNegocioTransportes[i]
                if (item.LineaNegocioTransporte != undefined){
                    if (item.LineaNegocioTransporte.Codigo == Codigo) {
                        $scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral.push(item)
                    }
                }
            }
            //$scope.TipoLineaNegocio = $linq.Enumerable().From($scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral).First('$.Codigo==-1');
            $scope.TipoLineaNegocio = $scope.ListadoTipoLineaNegocioTransportesFiltrosGeneral[0];
            $scope.HabilitarLineaNegocioFiltrosGeneral = false
            //$scope.HabilitarTipoLineaNegocio = true
            //$scope.HabilitarTipoTarifa = true


            if (Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA) {
                $scope.HabilitarRutaFiltrosGeneral = false
                $scope.HabilitarFormaPagoFiltrosGeneral = true
            }
            else {
                if (Codigo != -1) {
                    $scope.HabilitarRutaFiltrosGeneral = true
                } else {
                    $scope.HabilitarRutaFiltrosGeneral = false
                }
                $scope.HabilitarFormaPagoFiltrosGeneral = false
            }

        }
        $scope.CargarTarifa = function (Codigo) {
            $scope.ListadoTarifaTransportesFiltrado = []
            $scope.ListadoTarifaTransportesFiltrado.push({ Nombre: '(NO APLICA)' });
            for (var i = 0; i < $scope.ListadoTarifaTransportes.length; i++) {
                var item = $scope.ListadoTarifaTransportes[i]
                if (item.TipoLineaNegocioTransporte.Codigo == Codigo) {
                    $scope.ListadoTarifaTransportesFiltrado.push(item)
                }
            }
            $scope.Modal.TarifaTransportes = $scope.ListadoTarifaTransportesFiltrado[0]
            $scope.HabilitarLineaNegocio = false
            $scope.HabilitarTipoLineaNegocio = false
            $scope.HabilitarTipoTarifa = true
        }
        $scope.CargarTarifaFiltrosGeneral = function (Codigo) {
            $scope.ListadoTarifaTransportesFiltrado = []
            $scope.ListadoTarifaTransportesFiltrado.push({ Nombre: '(NO APLICA)' });
            for (var i = 0; i < $scope.ListadoTarifaTransportes.length; i++) {
                var item = $scope.ListadoTarifaTransportes[i]
                if (item.TipoLineaNegocioTransporte.Codigo == Codigo) {
                    $scope.ListadoTarifaTransportesFiltrado.push(item)
                }
            }
            $scope.Modal.TarifaTransportes = $scope.ListadoTarifaTransportesFiltrado[0]
            $scope.HabilitarLineaNegocioFiltrosGeneral = false
            //$scope.HabilitarTipoLineaNegocioFiltrosGeneral = false
            //$scope.HabilitarTipoTarifa = true
        }
        $scope.CargarTipoTarifa = function (Codigo) {
            $scope.ListadoTipoTarifaTransportesFiltrado = []
            if (Codigo == 300 || Codigo == 6 || Codigo == 302 || Codigo == 5) { //BARRIL GALON TONELADA PRODUCTO
                $scope.ListadoTipoTarifaTransportesFiltrado = $scope.ListadoProductosTransportados
            }
            else {
                for (var i = 0; i < $scope.ListadoTipoTarifaTransportes.length; i++) {
                    var item = $scope.ListadoTipoTarifaTransportes[i]
                    if (item.TarifaTransporte.Codigo == Codigo) {
                        $scope.ListadoTipoTarifaTransportesFiltrado.push(item)
                    }
                } if ($scope.ListadoTipoTarifaTransportesFiltrado.length == 0) {
                    $scope.ListadoTipoTarifaTransportesFiltrado.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                }
            }
            $scope.Modal.TipoTarifaTransportes = $scope.ListadoTipoTarifaTransportesFiltrado[0]
            $scope.HabilitarTipoTarifa = false
        }
        $scope.CargarTipoTarifaFiltrosGeneral = function (Codigo) {
            $scope.ListadoTipoTarifaTransportesFiltrado = []
            if (Codigo == 300 || Codigo == 6 || Codigo == 302 || Codigo == 5) { //BARRIL GALON TONELADA PRODUCTO
                $scope.ListadoTipoTarifaTransportesFiltrado = $scope.ListadoProductosTransportados
            }
            else {
                for (var i = 0; i < $scope.ListadoTipoTarifaTransportes.length; i++) {
                    var item = $scope.ListadoTipoTarifaTransportes[i]
                    if (item.TarifaTransporte.Codigo == Codigo) {
                        $scope.ListadoTipoTarifaTransportesFiltrado.push(item)
                    }
                } if ($scope.ListadoTipoTarifaTransportesFiltrado.length == 0) {
                    $scope.ListadoTipoTarifaTransportesFiltrado.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                }
            }
            $scope.Modal.TipoTarifaTransportes = $scope.ListadoTipoTarifaTransportesFiltrado[0]
            //$scope.HabilitarTipoTarifa = false
        }

        $scope.ciudadesOrigen = []

        $scope.AsignarCiudadesOrigen = function () {
            if ($scope.CiudadOrigen != "" & (!($scope.ciudadesOrigen.includes($scope.CiudadOrigen))) & $scope.ciudadesOrigen.length<5) {
                $scope.ciudadesOrigen.push($scope.CiudadOrigen)
            }
            $scope.CiudadOrigen = ''

        };

        $scope.ciudadesDestino = []

        $scope.AsignarCiudadesDestino = function () {
            if ($scope.CiudadDestino != "" & (!($scope.ciudadesDestino.includes($scope.CiudadDestino))) & $scope.ciudadesDestino.length<5) {
                $scope.ciudadesDestino.push($scope.CiudadDestino)
            }
            $scope.CiudadDestino = ''

        };

        $scope.EliminarCiudadOrigen = function (indexCiudad, item) {
            $scope.ciudadesOrigen.splice(indexCiudad, 1)
        }

        $scope.EliminarCiudadDestino = function (indexCiudad, item) {
            $scope.ciudadesDestino.splice(indexCiudad, 1)
        }

        $scope.FiltrarRutas = function () {
            $scope.ListadoRutasFiltrado = []
            var cteTipoRuta
            if ($scope.Modal.TipoLineaNegocioTransportes.Nombre.toUpperCase() == 'Urbana'.toUpperCase() || $scope.Modal.TipoLineaNegocioTransportes.Nombre.toUpperCase() == 'Urbano'.toUpperCase()) {
                cteTipoRuta = 4402
            } else if ($scope.Modal.TipoLineaNegocioTransportes.Nombre.toUpperCase() != '(NO APLICA)'.toUpperCase()) {
                cteTipoRuta = 4401
            }
            for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                var ruta = $scope.ListadoRutas[i]
                if (ruta.TipoRuta.Codigo == cteTipoRuta) {
                    $scope.ListadoRutasFiltrado.push(ruta)
                }
            }
            if ($scope.Modal.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL) {
                $scope.HabilitarCiudadOrigenDestino = true
                $scope.HabilitarCiudadUrbana = false
            }
            else if ($scope.Modal.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA) {
                $scope.HabilitarCiudadOrigenDestino = false
                $scope.HabilitarCiudadUrbana = true
            }
            else {
                $scope.HabilitarCiudadOrigenDestino = false
                $scope.HabilitarCiudadUrbana = false
            }
        }

        $scope.FiltrarRutasFiltrosGeneral = function () {
            $scope.ListadoRutasFiltrosGeneral = []
            var cteTipoRuta
            if ($scope.TipoLineaNegocio.Nombre.toUpperCase() == 'Urbana'.toUpperCase() || $scope.TipoLineaNegocio.Nombre.toUpperCase() == 'Urbano'.toUpperCase()) {
                cteTipoRuta = 4402
            } else if ($scope.TipoLineaNegocio.Nombre.toUpperCase() != '(NO APLICA)'.toUpperCase()) {
                cteTipoRuta = 4401
            }
            for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                var ruta = $scope.ListadoRutas[i]
                if (ruta.TipoRuta.Codigo == cteTipoRuta) {
                    $scope.ListadoRutasFiltrosGeneral.push(ruta)
                }
            }
            if ($scope.TipoLineaNegocio.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL) {
                //$scope.HabilitarCiudadOrigenDestino = true
                //$scope.HabilitarCiudadUrbana = false
            }
            else if ($scope.TipoLineaNegocio.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA) {
                //$scope.HabilitarCiudadOrigenDestino = false
                //$scope.HabilitarCiudadUrbana = true
            }
            else {
                //$scope.HabilitarCiudadOrigenDestino = false
                //$scope.HabilitarCiudadUrbana = false
            }
        }


        $scope.LimpiarFiltrosGeneral = function () {
            $scope.LineaNegocio = '';
            $scope.TipoLineaNegocio = '';
            $scope.CiudadOrigen = '';
            $scope.CiudadDestino = '';
            $scope.Tarifa = '';
            $scope.TipoTarifa = '';
            $scope.FormaPago = '';
            $scope.FiltroRuta = '';
            $scope.HabilitarBotonesFiltros = true;
            $scope.FiltrarTarifarioPaqueteria();

        }


      


        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando tarifario ventas código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando tarifario ventas Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            TarifarioVentasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo = response.data.Datos
                        Nombre = $scope.Modelo.Nombre;
                        $scope.NumeroTarifario = response.data.Datos.Codigo;
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.FechaInicio = new Date(response.data.Datos.FechaInicio)
                        $scope.Modelo.FechaFin = new Date(response.data.Datos.FechaFin)
                        $scope.DeshabilitarCampos = true
                        if ($scope.Modelo.Tarifas != null) {
                            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                                var tarifa = $scope.Modelo.Tarifas[i]
                                if (new Date(tarifa.FechaInicioVigencia) < MIN_DATE) {
                                    tarifa.FechaInicioVigencia = ''
                                }
                                if (new Date(tarifa.FechaFinVigencia) < MIN_DATE) {
                                    tarifa.FechaFinVigencia = ''
                                }

                                tarifa.ValorFlete = $scope.MaskValoresGrid(tarifa.ValorFlete)
                                tarifa.ValorEscolta = $scope.MaskValoresGrid(tarifa.ValorEscolta)
                                tarifa.ValorCargue = $scope.MaskValoresGrid(tarifa.ValorCargue)
                                tarifa.ValorDescargue = $scope.MaskValoresGrid(tarifa.ValorDescargue)
                                tarifa.ValorFleteTransportador = $scope.MaskValoresGrid(tarifa.ValorFleteTransportador)
                            }
                        }
                        if ($scope.Modelo.TarifarioBase == 1) {
                            $scope.habilitarTarifarioBase = false
                            $scope.Modelo.TarifarioBase = true
                        }
                        $scope.CargarDatosFunciones()
                        if ($scope.aplicabase) {
                            $scope.Modelo.Codigo = 0
                            $scope.DeshabilitarCampos = false
                            $scope.Modelo.Nombre = ''
                            $scope.Modelo.Estado = $scope.ListadoEstados[1]
                            $scope.Modelo.TarifarioBase = false
                            ShowSuccess('El tarifario se copio correctamente')
                        }
                        if ($scope.Modelo.Paqueteria !== undefined && $scope.Modelo.Paqueteria !== '' && $scope.Modelo.Paqueteria !== null) {
                            $scope.Modelo.Paqueteria.MinimoKgCobro = $scope.MaskMonedaGrid($scope.Modelo.Paqueteria.MinimoKgCobro)
                            $scope.Modelo.Paqueteria.MaximoKgCobro = $scope.MaskMonedaGrid($scope.Modelo.Paqueteria.MaximoKgCobro)
                            $scope.Modelo.Paqueteria.ValorKgAdicional = $scope.MaskValoresGrid($scope.Modelo.Paqueteria.ValorKgAdicional)
                            $scope.Modelo.Paqueteria.ValorManejo = $scope.MaskValoresGrid($scope.Modelo.Paqueteria.ValorManejo)
                            $scope.Modelo.Paqueteria.PorcentajeManejo = $scope.MaskMonedaGrid($scope.Modelo.Paqueteria.PorcentajeManejo)
                            $scope.Modelo.Paqueteria.MinimoValorSeguro = $scope.MaskValoresGrid($scope.Modelo.Paqueteria.MinimoValorSeguro)
                            $scope.Modelo.Paqueteria.MinimoValorUnidad = $scope.MaskValoresGrid($scope.Modelo.Paqueteria.MinimoValorUnidad)
                            $scope.Modelo.Paqueteria.MinimoValorManejo = $scope.MaskValoresGrid($scope.Modelo.Paqueteria.MinimoValorManejo)
                            $scope.Modelo.Paqueteria.PorcentajeSeguro = $scope.MaskMonedaGrid($scope.Modelo.Paqueteria.PorcentajeSeguro)
                            $scope.Modelo.Paqueteria.ValorSeguro = $scope.MaskValoresGrid($scope.Modelo.Paqueteria.ValorSeguro)
                        } else {
                            $scope.Modelo.Paqueteria = {};
                        }
                    }
                    else {
                        ShowError('No se logro consultar el tarifario ventas código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarTarifarioVentas';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarTarifarioVentas';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                if ($scope.Modelo.TarifarioBase) {
                    $scope.Modelo.TarifarioBase = 1
                } else {
                    $scope.Modelo.TarifarioBase = 0
                }
                if ($scope.Modelo.Paqueteria !== undefined && $scope.Modelo.Paqueteria !== '' && $scope.Modelo.Paqueteria !== null) {
                    var paqueteria = JSON.parse(JSON.stringify($scope.Modelo.Paqueteria))
                    $scope.Modelo.Paqueteria = {
                        MinimoKgCobro: paqueteria.MinimoKgCobro,
                        MaximoKgCobro: paqueteria.MaximoKgCobro,
                        ValorKgAdicional: paqueteria.ValorKgAdicional,
                        ValorManejo: paqueteria.ValorManejo,
                        PorcentajeManejo: paqueteria.PorcentajeManejo,
                        MinimoValorSeguro: paqueteria.MinimoValorSeguro,
                        MinimoValorUnidad: paqueteria.MinimoValorUnidad,
                        MinimoValorManejo: paqueteria.MinimoValorManejo,
                        PorcentajeSeguro: paqueteria.PorcentajeSeguro,
                        ValorSeguro: paqueteria.ValorSeguro
                    }
                }
                var ObjTarifario = angular.copy($scope.Modelo)

                if ($scope.aplicabase) {
                    ObjTarifario.Tarifas.forEach(function (item) {
                        item.Codigo = 0
                    })
                } else {
                    ObjTarifario.Tarifas = []
                    if ($scope.Modelo.Tarifas != null) {
                        $scope.Modelo.Tarifas.forEach(function (item) {
                            if (item.Modifico) {
                                
                                ObjTarifario.Tarifas.push(item)
                            }
                        })
                    }
                }

                if ($scope.TarifasEliminar.length > 0) {
                    ObjTarifario.TarifasEliminar = $scope.TarifasEliminar;
                }


                //Estado
                TarifarioVentasFactory.Guardar(ObjTarifario).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el tarifario ventas "' + $scope.Modelo.Nombre + '"');
                                    location.href = '#!ConsultarTarifarioVentas/' + response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó el tarifario ventas "' + $scope.Modelo.Nombre + '"');
                                    location.href = '#!ConsultarTarifarioVentas/' + $scope.Modelo.Codigo;
                                }
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var modelo = $scope.Modelo

            if (ValidarCampo(modelo.Nombre) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar el nombre ');
                continuar = false;
            }
            if (ValidarFecha(modelo.FechaInicio) == FECHA_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar la fecha de inicio ');
                continuar = false;
            }
            if (ValidarFecha(modelo.FechaFin) == FECHA_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar la fecha fin ');
                continuar = false;
            } else if (ValidarFecha(modelo.FechaFin, false, true) == FECHA_ES_MENOR) {
                $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha actual');
                continuar = false;
            }
            if (ValidarFecha(modelo.FechaInicio) == FECHA_VALIDA && ValidarFecha(modelo.FechaFin, false, true) == FECHA_VALIDA) {
                if (modelo.FechaInicio > modelo.FechaFin) {
                    $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha de inicio');
                    continuar = false;
                }
            }
            //if ($scope.Modelo.Tarifas.length == 0) {
            //    $scope.MensajesError.push('Debe ingresar al menos una tarifa');
            //    continuar = false;
            //}
            return continuar;
        }
        $scope.NuevaTarifa = function (paqueria) {
            $scope.MensajesErrorModal = [];
            $scope.ListadoLineaNegocioTransportesFiltrado = []
            if (paqueria) {
                for (var i = 0; i < $scope.ListadoLineaNegocioTransportes.length; i++) {
                    if ($scope.ListadoLineaNegocioTransportes[i].Codigo == 3) {
                        $scope.ListadoLineaNegocioTransportesFiltrado.push($scope.ListadoLineaNegocioTransportes[i])
                    }
                }
                $scope.Modal.LineaNegocioTransportes = $linq.Enumerable().From($scope.ListadoLineaNegocioTransportesFiltrado).First('$.Codigo == 3')
                $scope.AplicaPaqueteria = true
                $('#Secciontarifa').hide()
                $('#Seccionpaqueteria').show()

            } else {
                //for (var i = 0; i < $scope.ListadoLineaNegocioTransportes.length; i++) {
                //    if ($scope.ListadoLineaNegocioTransportes[i].Codigo !== 3) {
                //        $scope.ListadoLineaNegocioTransportesFiltrado.push($scope.ListadoLineaNegocioTransportes[i])
                //    }
                //}
                $scope.ListadoLineaNegocioTransportesFiltrado = $scope.ListadoLineaNegocioTransportes
                $scope.Modal.LineaNegocioTransportes = $scope.ListadoLineaNegocioTransportesFiltrado[0]
                $scope.AplicaPaqueteria = false
                $('#Secciontarifa').show()
                $('#Seccionpaqueteria').hide()
            }
            $scope.HabilitarLineaNegocio = true
            $scope.HabilitarTipoTarifa = true
            $scope.Modal.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
            $scope.Modal.FormaPagoVentas = $scope.ListaFormaPagoVentas[0]
            $scope.Modal.Ruta = ''
            $scope.Modal.ValorFlete = ''
            $scope.Modal.ValorEscolta = ''
            $scope.Modal.ValorCargue = ''
            $scope.Modal.ValorDescargue = ''
            $scope.Modal.ValorFleteTransportador = ''
            $scope.Modal.ValorKgAdicional = ''
            $scope.Modal.ValorAdicional1 = ''
            $scope.Modal.ValorAdicional2 = ''
            $scope.Modal.MinimoKgCobro = ''
            $scope.Modal.MaximoKgCobro = ''
            $scope.Modal.ValorKgAdicional = ''
            $scope.Modal.ValorManejo = ''
            $scope.Modal.PorcentajeManejo = ''
            $scope.Modal.MinimoValorSeguro = ''
            $scope.Modal.MinimoValorUnidad = ''
            $scope.Modal.CiudadOrigen = ''
            $scope.Modal.CiudadDestino = ''
            $scope.Modal.Ciudad = ''
            $scope.Modal.TiempoEntrega = ''
            $scope.Modal.FechaFinVigencia = $scope.Modelo.FechaFin
            $scope.Modal.FechaInicioVigencia = $scope.Modelo.FechaInicio
            $scope.Modal.PorcentajeSeguro = $scope.Modelo.PorcentajeSeguro;
            $scope.CargarTipoLinea($scope.Modal.LineaNegocioTransportes.Codigo)
            showModal('ModalNuevaTarifa')
        }


        function DatosRequeridosNuevaTarifa() {
            $scope.MensajesErrorModal = [];
            window.scrollTo(top, top);
            var modelo = $scope.Modal
            var continuar = true;
            if (ValidarCampo(modelo.LineaNegocioTransportes, undefined, true) > 0) {
                $scope.MensajesErrorModal.push('Debe ingresar la linea de transporte');
                continuar = false;
            }

            if ($scope.Modal.LineaNegocioTransportes.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA && $scope.Modal.FormaPagoVentas.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NO_APLICA) {
                $scope.MensajesErrorModal.push('Debe ingresar la forma de pago');
                continuar = false;
            }

            if (ValidarCampo(modelo.TipoLineaNegocioTransportes, undefined, true) > 0) {
                $scope.MensajesErrorModal.push('Debe ingresar el tipo de linea de transporte');
                continuar = false;
            }
            if (ValidarCampo(modelo.TarifaTransportes, undefined, true) > 0) {
                $scope.MensajesErrorModal.push('Debe ingresar la tarifa de transporte');
                continuar = false;
            }
            if (ValidarCampo(modelo.TipoTarifaTransportes, undefined, true) > 0 && modelo.TarifaTransportes.Codigo !== 5 && modelo.TarifaTransportes.Codigo !== 5 && modelo.TarifaTransportes.Codigo !== 301 && modelo.TarifaTransportes.Codigo !== 303) {
                $scope.MensajesErrorModal.push('Debe ingresar el tipo de tarifa de transporte');
                continuar = false;
            }

            if ($scope.Modal.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL) {
                if (ValidarCampo(modelo.CiudadOrigen, undefined, true) == 1) {
                    $scope.MensajesErrorModal.push('Debe ingresar la ciudad origen');
                    continuar = false;
                }
                if (ValidarCampo(modelo.CiudadDestino, undefined, true) == 1) {
                    $scope.MensajesErrorModal.push('Debe ingresar la ciudad destino');
                    continuar = false;
                }
                if ($scope.Modal.Reexpedicion == true) {
                    if ($scope.Modal.PorcentajeAfiliado == 0 || $scope.Modal.PorcentajeAfiliado == '' || $scope.Modal.PorcentajeAfiliado == undefined || isNaN($scope.Modal.PorcentajeAfiliado)) {
                        $scope.MensajesErrorModal.push('Debe ingresar un porcentaje reexpedición');
                        continuar = false;
                    }
                }
            }
            else if ($scope.Modal.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA) {
                if (ValidarCampo(modelo.Ciudad, undefined, true) == 1) {
                    $scope.MensajesErrorModal.push('Debe ingresar la ciudad');
                    continuar = false;
                }
            }
            else {
                if (ValidarCampo(modelo.Ruta, undefined, true) == 1) {
                    $scope.MensajesErrorModal.push('Debe ingresar la ruta');
                    continuar = false;
                }
            }
            if (ValidarCampo(modelo.ValorFlete) == 1) {
                $scope.MensajesErrorModal.push('Debe ingresar el flete');
                continuar = false;
            }
            if (ValidarFecha(modelo.FechaFin, false, true) == FECHA_ES_MENOR) {
                $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha actual');
                continuar = false;
            }
            if (ValidarFecha(modelo.FechaInicio) == FECHA_VALIDA && ValidarFecha(modelo.FechaFin, false, true) == FECHA_VALIDA) {
                if (modelo.FechaInicio > modelo.FechaFin) {
                    $scope.MensajesError.push('La fecha fin debe ser mayor a la fecha de inicio');
                    continuar = false;
                }
            }

            if (ValidarCampo(modelo.TiempoEntrega, undefined, true) < 0) {
                $scope.MensajesErrorModal.push('Debe ingresar el tiempo de entrega estimado');
                continuar = false;
            }

            return continuar
        }
        function DatosRequeridosNuevaTarifaPaqueteria() {
            $scope.MensajesErrorModal = [];
            window.scrollTo(top, top);
            var modelo = $scope.Modal
            var continuar = true;

            if (ValidarCampo(modelo.MinimoKgCobro) == 1) {
                modelo.MinimoKgCobro = 0
            }
            if (ValidarCampo(modelo.MaximoKgCobro) == 1) {
                modelo.MaximoKgCobro = 0
            }
            if (ValidarCampo(modelo.ValorKgAdicional) == 1) {
                modelo.ValorKgAdicional = 0
            }
            if (ValidarCampo(modelo.ValorManejo) == 1) {
                modelo.ValorManejo = 0
            }
            if (ValidarCampo(modelo.PorcentajeManejo) == 1) {
                modelo.PorcentajeManejo = 0
            }

            if (ValidarCampo(modelo.MinimoValorManejo) == 1) {
                modelo.MinimoValorManejo = 0
            }
            if (ValidarCampo(modelo.ValorSeguro) == 1) {
                modelo.ValorSeguro = 0
            }
            if (ValidarCampo(modelo.PorcentajeSeguro) == 1) {
                modelo.PorcentajeSeguro = 0
            }
            if (ValidarCampo(modelo.MinimoValorSeguro) == 1) {
                modelo.MinimoValorSeguro = 0
            }
            if (ValidarCampo(modelo.MinimoValorUnidad) == 1) {
                modelo.MinimoValorUnidad = 0
            }

            if (ValidarCampo(modelo.TiempoEntrega, undefined, true) > 0) {
                $scope.MensajesErrorModal.push('Debe ingresar el tiempo de entrega estimado');
                continuar = false;
            }

            return continuar
        }
        $scope.ValidarCampos = function () {
            $scope.MensajesErrorPaqueteria = [];
            var continuar = true;

            if (continuar) {
                $scope.Modelo.Paqueteria.editable = false
            } else {
                var cadenaerror = ''
                for (var i = 0; i < $scope.MensajesErrorPaqueteria.length; i++) {
                    cadenaerror += $scope.MensajesErrorPaqueteria[i] + '\n'
                }
                ShowError('No se pude editar la tarifa\n' + cadenaerror)
            }
        }
        $scope.RestaurarCampos = function () {
            $scope.Modelo.Paqueteria = JSON.parse(JSON.stringify($scope.tempitempaqueteria))

        }
        $scope.GuardarCampos = function () {
            $scope.tempitempaqueteria = JSON.parse(JSON.stringify($scope.Modelo.Paqueteria))
            $timeout(function () {
                $scope.RestaurarCampos()
            }, 200);

        }
        $scope.Modelo.Tarifas = []
        $scope.ListadoTarifas = []
        $scope.AgregarTarifa = function () {
            if ($scope.AplicaPaqueteria) {
                if (DatosRequeridosNuevaTarifaPaqueteria()) {
                    $scope.Modelo.Paqueteria = JSON.parse(JSON.stringify($scope.Modal))
                    $('#BtnPaquetria').hide()
                    closeModal('ModalNuevaTarifa')
                }
            } else {

                if (DatosRequeridosNuevaTarifa()) {
                    var cont = 0
                    if ($scope.Modelo.Tarifas != null) {
                        for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                            var item = $scope.Modelo.Tarifas[i]

                            if (item.LineaNegocioTransportes.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA) {
                                if ($scope.Modal.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL) {
                                    if (
                                        item.LineaNegocioTransportes.Codigo == $scope.Modal.LineaNegocioTransportes.Codigo
                                        && item.TipoLineaNegocioTransportes.Codigo == $scope.Modal.TipoLineaNegocioTransportes.Codigo
                                        && item.TarifaTransportes.Codigo == $scope.Modal.TarifaTransportes.Codigo
                                        && item.TipoTarifaTransportes.Codigo == $scope.Modal.TipoTarifaTransportes.Codigo
                                        && item.CiudadOrigen.Codigo == $scope.Modal.CiudadOrigen.Codigo
                                        && item.CiudadDestino.Codigo == $scope.Modal.CiudadDestino.Codigo
                                        && item.FormaPagoVentas.Codigo == $scope.Modal.FormaPagoVentas.Codigo
                                    ) {
                                        cont++
                                    }
                                }
                                if ($scope.Modal.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA) {
                                    if (
                                        item.LineaNegocioTransportes.Codigo == $scope.Modal.LineaNegocioTransportes.Codigo
                                        && item.TipoLineaNegocioTransportes.Codigo == $scope.Modal.TipoLineaNegocioTransportes.Codigo
                                        && item.TarifaTransportes.Codigo == $scope.Modal.TarifaTransportes.Codigo
                                        && item.TipoTarifaTransportes.Codigo == $scope.Modal.TipoTarifaTransportes.Codigo
                                        && item.CiudadOrigen.Codigo == $scope.Modal.Ciudad.Codigo
                                        && item.FormaPagoVentas.Codigo == $scope.Modal.FormaPagoVentas.Codigo
                                    ) {
                                        cont++
                                    }
                                    $scope.Modal.CiudadOrigen = JSON.parse(JSON.stringify($scope.Modal.Ciudad))
                                }

                            } else {
                                if (
                                    item.LineaNegocioTransportes.Codigo == $scope.Modal.LineaNegocioTransportes.Codigo
                                    && item.TipoLineaNegocioTransportes.Codigo == $scope.Modal.TipoLineaNegocioTransportes.Codigo
                                    && item.TarifaTransportes.Codigo == $scope.Modal.TarifaTransportes.Codigo
                                    && item.TipoTarifaTransportes.Codigo == $scope.Modal.TipoTarifaTransportes.Codigo
                                    && item.Ruta.Codigo == $scope.Modal.Ruta.Codigo
                                ) {
                                    cont++
                                }
                            }

                            item.Mostrar = true;
                            $scope.Modal.Mostrar = true;
                        }
                    } else {
                        $scope.Modelo.Tarifas = [];
                    }
                    if (cont > 0) {
                        ShowError('Ya hay una tarifa ingresada con las opciones seleccionadas')
                    } else {
                        $scope.Modal.Modifico = true
                        $scope.Modal.TipoTarifaTransportes.TarifaTransporte = { Codigo: $scope.Modal.TarifaTransportes.Codigo }
                        $scope.Modal.Reexpedicion = $scope.Modal.Reexpedicion != undefined ? $scope.Modal.Reexpedicion == true ? 1 : 0 : 0
                       
                        $scope.Modelo.Tarifas.push(JSON.parse(JSON.stringify($scope.Modal)))
                        
                        if ($scope.ListadoTarifas.length == 0) {
                            $scope.ListadoTarifas = $scope.Modelo.Tarifas
                            $scope.ListadoTarifas.forEach(item => {
                                
                                item.Mostrar = true;
                                item.ValorReexpedicion = item.Reexpedicion == 1 ? true : false 
                                    
                            });
                        } else {
                            $scope.ListadoTarifas.forEach(item => {
                                 item.ValorReexpedicion = item.Reexpedicion == 1 ? true : false

                            });
                            
                            $scope.ListadoTarifas = $scope.Modelo.Tarifas
                            
                        }
                        $scope.Modal.Reexpedicion = false;
                        $scope.Modal.PorcentajeAfiliado = '';
                        $scope.Modal.TipoLineaNegocioTransportes = '';
                        $scope.Modal.TarifaTransportes = '';
                        $scope.Modal.TipoTarifaTransportes = '';

                        closeModal('ModalNuevaTarifa');
                    }
                }
            }
        }
        $scope.ListTarifas = function (item) {
            if (item.lstTipoLineaNegocioTransportes == true) {
                item.lstTipoLineaNegocioTransportes = false
            } else {
                item.lstTipoLineaNegocioTransportes = true
            }
        }
        $scope.ListTipoTarifas = function (item) {
            if (item.lstData == true) {
                item.lstData = false
            } else {
                item.lstData = true
            }
        }
        $scope.EliminarTarifa = function (item, index, lista) {
            showModal('modalConfirmacionEliminar')
            $scope.tempItemElimina = item
            $scope.tempItemindex = index
            $scope.lista = lista
        }
        $scope.EliminarTarifaPaqueteria = function () {
            showModal('modalConfirmacionEliminarPaqueteria')
        }
        $scope.ListPaqueteria = function (item) {
            if (item.lstData == true) {
                item.lstData = false
            } else {
                item.lstData = true
            }
        }
        $scope.ConfirmarEliminarTarifa = function () {
            closeModal('modalConfirmacionEliminar')

            if ($scope.tempItemElimina.Codigo == 0 || $scope.tempItemElimina.Codigo == undefined) {
                for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                    var item = $scope.Modelo.Tarifas[i]

                    if (item.LineaNegocioTransportes.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA) {
                        if ($scope.tempItemElimina.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL) {
                            if (
                                item.LineaNegocioTransportes.Codigo == $scope.tempItemElimina.LineaNegocioTransportes.Codigo
                                && item.TipoLineaNegocioTransportes.Codigo == $scope.tempItemElimina.TipoLineaNegocioTransportes.Codigo
                                && item.TarifaTransportes.Codigo == $scope.tempItemElimina.TarifaTransportes.Codigo
                                && item.TipoTarifaTransportes.Codigo == $scope.tempItemElimina.TipoTarifaTransportes.Codigo
                                && item.CiudadOrigen.Codigo == $scope.tempItemElimina.CiudadOrigen.Codigo
                                && item.CiudadDestino.Codigo == $scope.tempItemElimina.CiudadDestino.Codigo
                            ) {
                                $scope.Modelo.Tarifas.splice(i, 1)
                                $scope.lista.splice($scope.tempItemindex, 1)
                                break;
                            }
                        }
                        if ($scope.tempItemElimina.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA) {
                            if (
                                item.LineaNegocioTransportes.Codigo == $scope.tempItemElimina.LineaNegocioTransportes.Codigo
                                && item.TipoLineaNegocioTransportes.Codigo == $scope.tempItemElimina.TipoLineaNegocioTransportes.Codigo
                                && item.TarifaTransportes.Codigo == $scope.tempItemElimina.TarifaTransportes.Codigo
                                && item.TipoTarifaTransportes.Codigo == $scope.tempItemElimina.TipoTarifaTransportes.Codigo
                                && item.CiudadOrigen.Codigo == $scope.tempItemElimina.CiudadOrigen.Codigo
                            ) {
                                $scope.Modelo.Tarifas.splice(i, 1)
                                $scope.lista.splice($scope.tempItemindex, 1)
                                break;
                            }
                        }

                    } else {
                        if (
                            item.LineaNegocioTransportes.Codigo == $scope.tempItemElimina.LineaNegocioTransportes.Codigo
                            && item.TipoLineaNegocioTransportes.Codigo == $scope.tempItemElimina.TipoLineaNegocioTransportes.Codigo
                            && item.TarifaTransportes.Codigo == $scope.tempItemElimina.TarifaTransportes.Codigo
                            && item.TipoTarifaTransportes.Codigo == $scope.tempItemElimina.TipoTarifaTransportes.Codigo
                            && item.Ruta.Codigo == $scope.tempItemElimina.Ruta.Codigo
                        ) {
                            $scope.Modelo.Tarifas.splice(i, 1)
                            $scope.lista.splice($scope.tempItemindex, 1)
                            break;
                        }
                    }
                }
            } else {
                if ($scope.tempItemElimina.Reexpedicion != undefined) {
                    $scope.tempItemElimina.Reexpedicion = $scope.tempItemElimina.Reexpedicion.Codigo
                }
                $scope.TarifasEliminar.push($scope.tempItemElimina);
                $scope.lista.splice($scope.tempItemindex, 1);
            }
        }
        $scope.ConfirmarEliminarTarifaPaqueteria = function () {
            closeModal('modalConfirmacionEliminarPaqueteria')
            $scope.Modelo.Paqueteria = undefined
            $('#BtnPaquetria').show()
        }
        $scope.AsignarValor = function (item2) {
            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                var item = $scope.Modelo.Tarifas[i]
                if (item.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL) {
                    if (
                        item.LineaNegocioTransportes.Codigo == item2.LineaNegocioTransportes.Codigo
                        && item.TipoLineaNegocioTransportes.Codigo == item2.TipoLineaNegocioTransportes.Codigo
                        && item.TarifaTransportes.Codigo == item2.TarifaTransportes.Codigo
                        && item.TipoTarifaTransportes.Codigo == item2.TipoTarifaTransportes.Codigo
                        && item.CiudadOrigen.Codigo == item2.CiudadOrigen.Codigo
                        && item.CiudadDestino.Codigo == item2.CiudadDestino.Codigo
                        && item.FormaPagoVentas.Codigo == item2.FormaPagoVentas.Codigo
                    ) {
                        $scope.Modelo.Tarifas[i].ValorFlete = item2.ValorFlete
                        $scope.Modelo.Tarifas[i].ValorEscolta = item2.ValorEscolta
                        $scope.Modelo.Tarifas[i].ValorCargue = item2.ValorCargue
                        $scope.Modelo.Tarifas[i].ValorDescargue = item2.ValorDescargue
                        $scope.Modelo.Tarifas[i].ValorFleteTransportador = item2.ValorFleteTransportador
                        $scope.Modelo.Tarifas[i].ValorKgAdicional = item2.ValorKgAdicional
                        $scope.Modelo.Tarifas[i].ValorAdicional1 = item2.ValorAdicional1
                        $scope.Modelo.Tarifas[i].ValorAdicional2 = item2.ValorAdicional2
                        $scope.Modelo.Tarifas[i].FechaInicioVigencia = item2.FechaInicioVigencia
                        $scope.Modelo.Tarifas[i].TiempoEntrega = item2.TiempoEntrega
                        $scope.Modelo.Tarifas[i].FechaFinVigencia = item2.FechaFinVigencia
                        $scope.Modelo.Tarifas[i].Estado = item2.Estado
                        $scope.Modelo.Tarifas[i].PorcentajeAfiliado = item2.PorcentajeAfiliado
                        $scope.Modelo.Tarifas[i].Reexpedicion = item2.ValorReexpedicion == true ? 1 : 0
                       
                        //item2.Reexpedicion = $linq.Enumerable().From($scope.ListadoReexpedicion).First('$.Codigo==' + item2.temItem.Reexpedicion.Codigo)
                        $scope.Modelo.Tarifas[i].PorcentajeSeguro = item2.PorcentajeSeguro
                        $scope.Modelo.Tarifas[i].Modifico = true
                        //$scope.Modelo.Tarifas[i].FormaPagoVentas = item2.FormaPagoVentas
                    }
                }
                else if (item.TipoLineaNegocioTransportes.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA) {
                    if (
                        item.LineaNegocioTransportes.Codigo == item2.LineaNegocioTransportes.Codigo
                        && item.TipoLineaNegocioTransportes.Codigo == item2.TipoLineaNegocioTransportes.Codigo
                        && item.TarifaTransportes.Codigo == item2.TarifaTransportes.Codigo
                        && item.TipoTarifaTransportes.Codigo == item2.TipoTarifaTransportes.Codigo
                        && item.CiudadOrigen.Codigo == item2.CiudadOrigen.Codigo
                        && item.FormaPagoVentas.Codigo == item2.FormaPagoVentas.Codigo
                    ) {
                        $scope.Modelo.Tarifas[i].ValorFlete = item2.ValorFlete
                        $scope.Modelo.Tarifas[i].ValorEscolta = item2.ValorEscolta
                        $scope.Modelo.Tarifas[i].ValorCargue = item2.ValorCargue
                        $scope.Modelo.Tarifas[i].ValorDescargue = item2.ValorDescargue
                        $scope.Modelo.Tarifas[i].ValorFleteTransportador = item2.ValorFleteTransportador
                        $scope.Modelo.Tarifas[i].ValorKgAdicional = item2.ValorKgAdicional
                        $scope.Modelo.Tarifas[i].ValorAdicional1 = item2.ValorAdicional1
                        $scope.Modelo.Tarifas[i].ValorAdicional2 = item2.ValorAdicional2
                        $scope.Modelo.Tarifas[i].FechaInicioVigencia = item2.FechaInicioVigencia
                        $scope.Modelo.Tarifas[i].TiempoEntrega = item2.TiempoEntrega
                        $scope.Modelo.Tarifas[i].FechaFinVigencia = item2.FechaFinVigencia
                        $scope.Modelo.Tarifas[i].Estado = item2.Estado
                        $scope.Modelo.Tarifas[i].FormaPagoVentas = item2.FormaPagoVentas
                        $scope.Modelo.Tarifas[i].PorcentajeAfiliado = item2.PorcentajeAfiliado
                        $scope.Modelo.Tarifas[i].PorcentajeSeguro = item2.PorcentajeSeguro
                        $scope.Modelo.Tarifas[i].Modifico = true
                        $scope.Modelo.Tarifas[i].Reexpedicion = item2.ValorReexpedicion == true ? 1 : 0
                    }
                }
                else {
                    if (
                        item.LineaNegocioTransportes.Codigo == item2.LineaNegocioTransportes.Codigo
                        && item.TipoLineaNegocioTransportes.Codigo == item2.TipoLineaNegocioTransportes.Codigo
                        && item.TarifaTransportes.Codigo == item2.TarifaTransportes.Codigo
                        && item.TipoTarifaTransportes.Codigo == item2.TipoTarifaTransportes.Codigo
                        && item.Ruta.Codigo == item2.Ruta.Codigo
                    ) {
                        $scope.Modelo.Tarifas[i].ValorFlete = item2.ValorFlete
                        $scope.Modelo.Tarifas[i].ValorEscolta = item2.ValorEscolta
                        $scope.Modelo.Tarifas[i].ValorCargue = item2.ValorCargue
                        $scope.Modelo.Tarifas[i].ValorDescargue = item2.ValorDescargue
                        $scope.Modelo.Tarifas[i].ValorFleteTransportador = item2.ValorFleteTransportador
                        $scope.Modelo.Tarifas[i].ValorKgAdicional = item2.ValorKgAdicional
                        $scope.Modelo.Tarifas[i].ValorAdicional1 = item2.ValorAdicional1
                        $scope.Modelo.Tarifas[i].ValorAdicional2 = item2.ValorAdicional2
                        $scope.Modelo.Tarifas[i].FechaInicioVigencia = item2.FechaInicioVigencia
                        $scope.Modelo.Tarifas[i].TiempoEntrega = item2.TiempoEntrega
                        $scope.Modelo.Tarifas[i].FechaFinVigencia = item2.FechaFinVigencia
                        $scope.Modelo.Tarifas[i].Estado = item2.Estado
                        $scope.Modelo.Tarifas[i].FormaPagoVentas = item2.FormaPagoVentas
                        $scope.Modelo.Tarifas[i].PorcentajeAfiliado = item2.PorcentajeAfiliado
                        $scope.Modelo.Tarifas[i].PorcentajeSeguro = item2.PorcentajeSeguro
                        $scope.Modelo.Tarifas[i].Modifico = true
                    }
                }

            }
        }
        $scope.ReplicarVigencias = function () {
            for (var i = 0; i < $scope.Modelo.Tarifas.length; i++) {
                $scope.Modelo.Tarifas[i].FechaInicioVigencia = $scope.Modelo.FechaInicio;
                $scope.Modelo.Tarifas[i].FechaFinVigencia = $scope.Modelo.FechaFin;
                $scope.Modelo.Tarifas[i].Modifico = true;
            }

            $scope.ListadoTarifas = AgruparListados($scope.Modelo.Tarifas, 'LineaNegocioTransportes', 'TipoLineaNegocioTransportes');
            closeModal('ModalReplicarVIgencia')

        }
        $scope.MostrarReplicar = function () {
            if ($scope.ListadoTarifas.length > 0) {
                showModal('ModalReplicarVIgencia')
            }
        }
        $scope.ValidarFlete = function (item) {
            if (item.ValorFlete == '' || item.ValorFlete == 0 || item.ValorFlete == undefined) {
                return item.temValorFlete
            } else {
                return item.ValorFlete
            }
        }

        $scope.VerificarExistencia = function () {
            if (Nombre != $scope.Modelo.Nombre) {
                blockUI.start('Cargando tarifario ventas código ' + $scope.Modelo.Codigo);

                $timeout(function () {
                    blockUI.message('Cargando tarifario ventas Código ' + $scope.Modelo.Codigo);
                }, 200);

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Nombre: $scope.Modelo.Nombre,
                };

                blockUI.delay = 1000;
                TarifarioVentasFactory.Obtener(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                ShowError('Ya hay un tarifario guardado con el nombre ingresado')
                                $scope.Modelo.Nombre = ''
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });

                blockUI.stop();
            }
        };

        $scope.VerificarValorCliente = function (valorA, valorB, option, item) {
            if (valorA !== '' && valorA !== 0 && valorA !== undefined && valorA !== null && valorB !== '' && valorB !== 0 && valorB !== undefined && valorB !== null) {
                valor1 = MascaraNumero(valorA);
                valor2 = MascaraNumero(valorB);
                if (valor1 < valor2) {
                    if (option === 1) {
                        $scope.Modal.ValorFleteTransportador = '';
                        ShowWarning('', 'El valor flete transportador no puede ser mayor al valor flete cliente');
                    }
                    if (option === 2) {
                        item.ValorFleteTransportador = '';
                        ShowWarning('', 'El valor flete transportador no puede ser mayor al valor flete cliente');
                    }
                }
            }
        };

        $scope.VerificarValorTransportador = function (transportador, valorCargue, valorDescargue, option, item) {
            if (transportador !== '' && transportador !== 0 && transportador !== undefined && transportador !== null && (valorCargue !== '' && valorCargue !== 0 && valorCargue !== undefined && valorCargue !== null || valorDescargue !== '' && valorDescargue !== 0 && valorDescargue !== undefined && valorDescargue !== null)) {
                if (valorCargue === '') {
                    valorCargue = 0;
                }
                if (valorDescargue === '') {
                    valorDescargue = 0;
                }
                valorT = MascaraNumero(transportador);
                valorC = MascaraNumero(valorCargue);
                valorD = MascaraNumero(valorDescargue);
                valor2 = valorC + valorD
                if (valorT < valor2) {
                    if (option === 1) {
                        $scope.Modal.ValorCargue = '';
                        ShowWarning('', 'El valor cargue y descargue transportador no puede ser mayor al valor flete transportador');
                    }
                    if (option === 2) {
                        $scope.Modal.ValorDescargue = '';
                        ShowWarning('', 'El valor cargue y descargue transportador no puede ser mayor al valor flete transportador');
                    }
                    if (option === 3) {
                        item.ValorCargue = '';
                        ShowWarning('', 'El valor cargue y descargue transportador no puede ser mayor al valor flete transportador');
                    }
                    if (option === 4) {
                        item.ValorDescargue = '';
                        ShowWarning('', 'El valor cargue y descargue transportador no puede ser mayor al valor flete transportador');
                    }
                }
            }
        };

       
        $scope.CiudadOrigen = '';
        $scope.CiudadDestino = '';
        $scope.Tarifa = '';
        $scope.TipoTarifa = '';
        $scope.FormaPago = '';
        $scope.FiltroRuta = '';
        $scope.ListadoTarifas.forEach(lineaNegocio => {
            lineaNegocio.forEach(tipolineaNegocio => {
                tipolineaNegocio.forEach(item => {
                    item.Mostrar = true;
                });
            });            
        });
        $scope.FiltrarTarifarioPaqueteria = function () {           

            

            $scope.ListadoTarifas.forEach(lineaNegocio => {
                if ($scope.LineaNegocio != undefined && $scope.LineaNegocio != null && $scope.LineaNegocio != '') {
                    if ($scope.LineaNegocio.Codigo == CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA) {
                        if (lineaNegocio.LineaNegocioTransportes.Codigo == CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA) {
                            lineaNegocio.Data.forEach(tipolineaNegocio => {
                                tipolineaNegocio.Data.forEach(item => {
                                    item.Mostrar = true;
                                    if ($scope.CiudadOrigen != undefined && $scope.CiudadOrigen != null && $scope.CiudadOrigen != '') {
                                        if (item.CiudadOrigen.Codigo == $scope.CiudadOrigen.Codigo) {
                                            //item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }
                                    if ($scope.CiudadDestino != undefined && $scope.CiudadDestino != null && $scope.CiudadDestino != '') {
                                        if (item.CiudadDestino.Codigo == $scope.CiudadDestino.Codigo) {
                                           // item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }
                                    if ($scope.Tarifa != undefined && $scope.Tarifa != null && $scope.Tarifa != '') {
                                        if (item.TarifaTransportes.Codigo == $scope.Tarifa.Codigo) {
                                            //item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }
                                    if ($scope.TipoTarifa != undefined && $scope.TipoTarifa != null && $scope.TipoTarifa != '') {
                                        if (item.TipoTarifaTransportes.Codigo == $scope.TipoTarifa.Codigo) {
                                            //item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }
                                    if ($scope.FormaPago != undefined && $scope.FormaPago != null && $scope.FormaPago != '') {
                                        if (item.FormaPagoVentas.Codigo == $scope.FormaPago.Codigo) {
                                           // item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }
                                });
                            });
                        } else {
                            lineaNegocio.Data.forEach(tipolineanegocio => {
                                tipolineanegocio.Data.forEach(item => {
                                    item.Mostrar = false;
                                });
                            });
                        }
                    } else {
                        //lineaNegocio.Data.forEach(tipolineanegocio => {
                        //    tipolineanegocio.Data.forEach(item => {
                        //        item.Mostrar = true;
                        //    });
                        //});
                    }

                    if ($scope.LineaNegocio.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO) {
                        if (lineaNegocio.LineaNegocioTransportes.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO) {
                            lineaNegocio.Data.forEach(tipolineaNegocio => {
                                tipolineaNegocio.Data.forEach(item => {
                                    item.Mostrar = true;
                                    if ($scope.FiltroRuta != undefined && $scope.FiltroRuta != null && $scope.FiltroRuta != '') {
                                        if (item.Ruta.Codigo == $scope.FiltroRuta.Codigo) {
                                            //item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }
                                   
                                    if ($scope.Tarifa != undefined && $scope.Tarifa != null && $scope.Tarifa != '') {
                                        if (item.TarifaTransportes.Codigo == $scope.Tarifa.Codigo) {
                                            //item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }
                                    if ($scope.TipoTarifa != undefined && $scope.TipoTarifa != null && $scope.TipoTarifa != '') {
                                        if (item.TipoTarifaTransportes.Codigo == $scope.TipoTarifa.Codigo) {
                                            //item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }
                                    
                                });
                            });
                        } else {
                            lineaNegocio.Data.forEach(tipolineanegocio => {
                                tipolineanegocio.Data.forEach(item => {
                                    item.Mostrar = false;
                                });
                            });
                        }
                    } else {
                        //lineaNegocio.Data.forEach(tipolineanegocio => {
                        //    tipolineanegocio.Data.forEach(item => {
                        //        item.Mostrar = true;
                        //    });
                        //});
                    }


                    if ($scope.LineaNegocio.Codigo ==  CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                        if (lineaNegocio.LineaNegocioTransportes.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                            lineaNegocio.Data.forEach(tipolineaNegocio => {
                                tipolineaNegocio.Data.forEach(item => {
                                    item.Mostrar = true;
                                    if ($scope.FiltroRuta != undefined && $scope.FiltroRuta != null && $scope.FiltroRuta != '') {
                                        if (item.Ruta.Codigo == $scope.FiltroRuta.Codigo) {
                                            //item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }

                                    if ($scope.Tarifa != undefined && $scope.Tarifa != null && $scope.Tarifa != '') {
                                        if (item.TarifaTransportes.Codigo == $scope.Tarifa.Codigo) {
                                            //item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }
                                    if ($scope.TipoTarifa != undefined && $scope.TipoTarifa != null && $scope.TipoTarifa != '') {
                                        if (item.TipoTarifaTransportes.Codigo == $scope.TipoTarifa.Codigo) {
                                           // item.Mostrar = true;
                                        } else {
                                            item.Mostrar = false;
                                        }
                                    }

                                });
                            });
                        } else {
                            lineaNegocio.Data.forEach(tipolineanegocio => {
                                tipolineanegocio.Data.forEach(item => {
                                    item.Mostrar = false;
                                });
                            });
                        }
                    } else {
                        //lineaNegocio.Data.forEach(tipolineanegocio => {
                        //    tipolineanegocio.Data.forEach(item => {
                        //        item.Mostrar = true;
                        //    });
                        //});
                    }
                } else {
                    lineaNegocio.Data.forEach(tipolineaNegocio => {
                        tipolineaNegocio.Data.forEach(item => {
                            item.Mostrar = true;
                        });
                    });
                }
            });
        }

        

        $scope.VolverMaster = function () {
            var Modificaciones = false;
            $scope.Modelo.Tarifas.forEach(item => {
                if (item.Modifico == true) {
                    Modificaciones = true
                }
            });
            if (Modificaciones) {
                ShowConfirm('Adicionó o modificó una tarifa, desea guardar el tarifario?', $scope.Guardar, function () { document.location.href = '#!ConsultarTarifarioVentas/' })
            } else {
                document.location.href = '#!ConsultarTarifarioVentas/'// + $scope.Modelo.Codigo;
            }
            
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskDecimales = function (item) {
            return MascaraDecimales(item);
        }

        $scope.MaskPorcentaje = function (value, item) {
            var numeroarray = [];
            for (var i = 0; i < value.length; i++) {
                patron = /[0123456789.]/
                if (i == 0 && value[i] == '-') {
                    numeroarray.push(value[i])
                }
                else if (patron.test(value[i])) {
                    if (patron.test(value[i]) == ' ') {
                        option[i] = '';
                    } else {
                        numeroarray.push(value[i])
                    }
                }
            }

            var val = numeroarray.join('');
            item = val;
            var nval = [];
            if (val[1] != '.') {
                if (val.length == 3) {
                    if (val[0] != 1 && val[0] != "1" && val[2] != ".") {
                        var MaxLength = 3;
                        nval.push(val[0]);
                        nval.push(val[1]);
                        item = nval.join('');
                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        }
                    } else if (val[2] == ".") {
                        MaxLength = 5;
                    }
                    else if ((val[0] == 1 || val[0] == "1") && val[2] != ".") {
                        MaxLength = 3;

                        if (val[1] != 0 || val[1] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        } else if (val[2] != 0 || val[2] != "0") {
                            nval = [];
                            nval.push(val[0]);
                            nval.push(val[1]);
                            item = nval.join('');
                        }
                    }

                } else if (val.length > 3) {
                    if (val[2] == ".") {
                        if (val.length == 4) {
                            nval.push(val[0]);
                            nval.push(val[1]);
                            nval.push(val[2]);
                            if (val[3] != ".") {
                                nval.push(val[3]);
                            }
                            item = nval.join('');
                        } else if (val.length >= 5) {
                            nval.push(val[0]);
                            nval.push(val[1]);
                            nval.push(val[2]);
                            if (val[3] != ".") {
                                nval.push(val[3]);
                                if (val[4] != ".") {
                                    nval.push(val[4]);
                                }
                            }

                            item = nval.join('');
                        }
                    } else if (val[3] == ".") {
                        nval = [];
                        nval.push(val[0]);                        
                        nval.push(val[1]);
                        nval.push(val[3]);
                        nval.push(val[2]);
                        if (val[4] != undefined) {
                            nval.push(val[4]);
                        }
                        item = nval.join('');
                    } else {
                        nval.push(val[0]);
                        nval.push(val[1]);
                        nval.push(val[2]);
                        item = nval.join('');
                    }

                }
            } else {
                nval.push(val[0]);
                nval.push(val[1]);
                if (val[2] != ".") {
                    nval.push(val[2]);
                    if (val[3] != ".") {
                        nval.push(val[3]);
                    }
                }


                item = nval.join('');
                MaxLength = 4;
            }
            return item;
        }
        /* Obtener parametros*/
        try {
            var Parametros = $routeParams.Codigo.split(',')
            if (Parametros.length > 1) {
                $scope.Modelo.Codigo = Parametros[0];
                $scope.aplicabase = true
                Obtener();
            } else {
                if ($routeParams.Codigo > 0) {
                    $scope.Modelo.Codigo = $routeParams.Codigo;
                    Obtener();
                }
                else {
                    $scope.Modelo.Codigo = 0;
                    $scope.CargarDatosFunciones()
                }

            }
        } catch (e) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.Codigo = $routeParams.Codigo;
                Obtener();
            }
            else {
                $scope.Modelo.Codigo = 0;
                $scope.CargarDatosFunciones()
            }

        }

        $scope.BuscarDetalle = function () {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Validando información del detalle del tarifario...");
            blockUI.message("Validando información del detalle del tarifario...");

            if (DatosRequeridosDetalle()) {
                
                $scope.CodigosCiudadesOrigen = ''
                $scope.CodigosCiudadesDestino = ''

                $scope.ciudadesOrigen.forEach(item => {
                    $scope.CodigosCiudadesOrigen += item.Codigo + ','
                });
                $scope.CodigosCiudadesOrigen = $scope.CodigosCiudadesOrigen.substring(0, $scope.CodigosCiudadesOrigen.length - 1);

                $scope.ciudadesDestino.forEach(item => {
                    $scope.CodigosCiudadesDestino += item.Codigo + ','
                });
                $scope.CodigosCiudadesDestino = $scope.CodigosCiudadesDestino.substring(0, $scope.CodigosCiudadesDestino.length - 1);

                filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroTarifario: $scope.Modelo.Codigo,
                    LineaNegocioTransportes: { Codigo: $scope.LineaNegocio.Codigo },
                    TipoLineaNegocioTransportes: { Codigo: $scope.TipoLineaNegocio.Codigo },
                    Ruta: { Codigo: $scope.LineaNegocio.Codigo != 3 ? $scope.FiltroRuta.Codigo : 0 },
                    CiudadOrigen: { Codigo: $scope.LineaNegocio.Codigo == 3 ? $scope.CiudadOrigen.Codigo : 0 },
                    CiudadDestino: { Codigo: $scope.LineaNegocio.Codigo == 3 ? $scope.CiudadDestino.Codigo : 0 },
                    TarifaTransporteCarga: { Codigo: $scope.Tarifa != null ? $scope.Tarifa.Codigo : 0 },
                    TipoTarifaTransporteCarga: { Codigo: $scope.TipoTarifa != null ? $scope.TipoTarifa.Codigo : 0 },
                    FormaPagoVentas: { Codigo: $scope.FormaPago.Codigo },
                    CiudadesOrigen: $scope.CodigosCiudadesOrigen,
                    CiuadesDestino: $scope.CodigosCiudadesDestino
                }
                TarifarioVentasFactory.ConsultarDetalle(filtro).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos.length > 0) {
                                if (response.data.Datos.length > 50) {
                                    (response.data.Datos.splice(50))
                                    $scope.ExportarTarifarioExcel($scope.Modelo.Codigo, $scope.CodigosCiudadesOrigen, $scope.CodigosCiudadesDestino)
                                }
                                try {
                                    $scope.ListadoTarifas = response.data.Datos
                                    
                                    $scope.ListadoTarifas.forEach(item => {
                                        var patronnumerico = /(\d+)/g;
                                        var valornumerico = isNaN(parseInt(item.TipoTarifaTransportes.Nombre.match(patronnumerico))) ? "" : parseInt(item.TipoTarifaTransportes.Nombre.match(patronnumerico).reduce((a, b) => parseInt(a) + parseInt(b), 0));
                                        var patronstring = /[a-z]+/gi;
                                        var valors = item.TipoTarifaTransportes.Nombre.match(patronstring).toString();
                                        var valorstring = valors.replace(/,/gi, valors);
                                        var primerCaracter = item.TipoTarifaTransportes.Nombre.split('')[0];
                                        var patron = /[0123456789]/;
                                        if (patron.test(primerCaracter)) {
                                            item.s1 = valornumerico;
                                            item.s2 = valors;
                                        } else {
                                            item.s1 = valors;
                                            item.s2 = valornumerico;
                                        }

                                        item.Mostrar = true;
                                        item.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + item.Estado.Codigo);
                                        item.ValorFlete = $scope.MaskValoresGrid(item.ValorFlete);
                                        item.ValorEscolta = $scope.MaskValoresGrid(item.ValorEscolta);
                                        item.ValorFleteTransportador = $scope.MaskValoresGrid(item.ValorFleteTransportador);
                                        item.ValorReexpedicion = item.Reexpedicion == 1 ? true : false;
                                        item.ValorCargue = $scope.MaskValoresGrid(item.ValorCargue);
                                        item.ValorDescargue = $scope.MaskValoresGrid(item.ValorDescargue)
                                    });
                                } catch (e) { console.log(e); }
                                    
                                try { $scope.Modelo.Tarifas = response.data.Datos } catch (e) { console.log(e); }
                                   

                                //console.log("Resultado Tarifas: ", $scope.ListadoTarifas);
                                $scope.Buscando = true;
                            } else {
                                $scope.ListadoTarifas = [];
                                $scope.Modelo.Tarifas = [];
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    });

                
            }
            blockUI.stop();
        }

        $scope.ExportarTarifarioExcel = function (codigo,CiudadesOrigen,CiudadesDestino) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.codigo = codigo;
            $scope.NombreReporte = NOMBRE_LISTADO_EXPORTAR_TARIFARIO_VENTA;

            $scope.FiltroArmado = '&Numero=' + $scope.codigo + '&CiudadesOrigen=' + CiudadesOrigen  + '&CiudadesDestino=' + CiudadesDestino;

            window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + 1 + '&Prefijo=' + $scope.pref);
        }

        function DatosRequeridosDetalle() {
            $scope.MensajesError = [];
            var buscar = true;
            if ($scope.LineaNegocio != undefined || $scope.LineaNegocio != null || $scope.LineaNegocio != '') {
                if ($scope.TipoLineaNegocio == undefined || $scope.TipoLineaNegocio == null || $scope.TipoLineaNegocio == '') {
                    $scope.MensajesError.push('Debe seleccionar un tipo de línea negocio');
                    buscar = false;
                }
                if ($scope.LineaNegocio.Codigo == 3) {
                    if ($scope.TipoLineaNegocio == undefined || $scope.TipoLineaNegocio == null || $scope.TipoLineaNegocio == '') {
                        $scope.MensajesError.push('Debe seleccionar un tipo de línea negocio');
                        buscar = false;
                    } else if ($scope.TipoLineaNegocio.Codigo == undefined) {
                        $scope.MensajesError.push('Debe seleccionar un tipo de línea negocio');
                        buscar = false;
                    }
                
                    if (($scope.ciudadesOrigen == undefined || $scope.ciudadesOrigen == null || $scope.ciudadesOrigen == '') && ($scope.ciudadesDestino == undefined || $scope.ciudadesDestino == null || $scope.ciudadesDestino == '')) {
                        $scope.MensajesError.push('Debe seleccionar una Ciudad Origen o Ciudad Destino');
                        buscar = false;
                    }
                    
                }
            } 

            return buscar;
        }

        $scope.ValidarPorcentajeReexpedicion = function (item) {
            if (item.ValorReexpedicion == true) {
                if (parseFloat(item.PorcentajeAfiliado) <= 0 || item.PorcentajeAfiliado == undefined) {
                    ShowError('Debe ingresar un porcentaje reexpedicion');
                }
            }
        }

        $scope.Date = function (d) {
            return new Date(d)
        }
        $scope.CopyItem = function (d) {
            var Objet = JSON.parse(JSON.stringify(new Object(d)))
            return Objet
        }

    }]);    