﻿Print 'Func_Formatear_Ceros_Numeracion'
GO
DROP FUNCTION Func_Formatear_Ceros_Numeracion
GO
CREATE FUNCTION Func_Formatear_Ceros_Numeracion (

	@Cadena	VARCHAR(10),
	@cantCaract INT,
	@ColocarDelante SMALLINT
)
RETURNS VARCHAR(10)
AS
BEGIN

----***********-----
--Autor: Johan Ospina
--Fecha: 04-SEPT-2019 V1
---************-------

		DECLARE @Cont INT = 0
		DECLARE @ResCadena VARCHAR(10) = ''
		SET @ResCadena = CONVERT(VARCHAR,@Cadena)

	IF (LEN(@Cadena) < @cantCaract) BEGIN
		
		
		IF @ColocarDelante = 1 BEGIN

			WHILE @Cont < (@cantCaract - LEN(@Cadena)) BEGIN

					SET @ResCadena += '0'
					SET @Cont += 1
			END
		END
		ELSE BEGIN
			WHILE @Cont < (@cantCaract - LEN(@Cadena)) BEGIN

					SET @ResCadena = '0' + @ResCadena
					SET @Cont += 1
			END
		END
	END

	RETURN @ResCadena
END
GO