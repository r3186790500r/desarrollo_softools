﻿PRINT 'gsp_eliminar_tercero_cliente_cupo_oficina'
GO
DROP PROCEDURE gsp_eliminar_tercero_cliente_cupo_oficina
GO
create procedure gsp_eliminar_tercero_cliente_cupo_oficina  
(@par_EMPR_Codigo SMALLINT,  
@par_TERC_Codigo numeric)  
as  
begin
delete Tercero_Clientes_Cupo_Oficinas WHERE EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo = @par_TERC_Codigo
end
GO