﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	PRINT 'gsp_Insertar_TRM'
GO
DROP PROCEDURE gsp_Insertar_TRM
GO
CREATE PROCEDURE gsp_Insertar_TRM(  
@par_EMPR_Codigo NUMERIC,  
@par_MONE_Codigo VARCHAR(20),   
@par_FECHA DATE,   
@par_Valor_Moneda_local MONEY,   
@par_Usua_Crea NUMERIC)  
  
AS BEGIN 

DECLARE @Exite_Registro NUMERIC

SET @Exite_Registro = (SELECT COUNT (*) FROM Tasa_Cambio_Monedas WHERE EMPR_Codigo= @par_EMPR_Codigo 
AND MONE_Codigo = @par_MONE_Codigo AND Fecha = @par_FECHA )

	IF @Exite_Registro = 0 BEGIN 
		INSERT INTO Tasa_Cambio_Monedas (EMPR_Codigo,MONE_Codigo,Fecha,Valor_Moneda_Local,USUA_Codigo_Crea,Fecha_Crea)  
		VALUES(@par_EMPR_Codigo,@par_MONE_Codigo,@par_FECHA,@par_Valor_Moneda_local,@par_Usua_Crea,GETDATE())  
		SELECT MONE_Codigo FROM Tasa_Cambio_Monedas WHERE EMPR_Codigo= @par_EMPR_Codigo 
		AND MONE_Codigo = @par_MONE_Codigo AND Fecha = @par_FECHA
	END
		
END  

GO