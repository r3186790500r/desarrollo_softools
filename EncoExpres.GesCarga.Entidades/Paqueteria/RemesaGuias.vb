﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa

Namespace Paqueteria
    <JsonObject>
    Public NotInheritable Class RemesaGuias
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Remesa = New Remesas With {.Numero = Read(lector, "Codigo")}
            Remesa.NumeroDocumento = Read(lector, "Numero_Documento")
            Remesa.Fecha = Read(lector, "Fecha")
            Remesa.Observaciones = Read(lector, "Observaciones")
            Remesa.NumeroDocumentoCliente = Read(lector, "Documento_Cliente")
            ' EstadoRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESRP_Codigo")}
            Remesa.FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FOPR_Codigo")}
            If Read(lector, "Obtener") = 0 Then
                Remesa.TotalFleteCliente = Read(lector, "Total_Flete_Cliente")
                Remesa.ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
                Remesa.ValorSeguroCliente = Read(lector, "Valor_Seguro_Cliente")
                Remesa.CantidadCliente = Read(lector, "Cantidad_Cliente")
                Remesa.PesoCliente = Read(lector, "Peso_Cliente")
                NumeroPlanilla = Read(lector, "ENPD_Numero")
                Remesa.ProductoTransportado = New ProductoTransportados With {.Nombre = Read(lector, "NombreProducto")}
                Remesa.Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .NombreCompleto = Read(lector, "NombreCliente")}
                Remesa.Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")}
                'Remesa.Detalle = New DetalleRemesas With {.Destinatario = New Terceros With {.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino")}}}
                Remesa.Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}
                'Remesa.Detalle = New DetalleRemesas With {.Cumplido = Read(lector, "Cumplido")}
                TotalRegistros = Read(lector, "TotalRegistros")
            Else
                Remesa.FechaDocumentoCliente = Read(lector, "Fecha_Documento_Cliente")
                Remesa.FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FPRP_Codigo")}
                Remesa.Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente")}
                DetalleTarifario = New DetalleTarifarioVentas With {.Codigo = Read(lector, "DTCV_Codigo")}
                Remesa.ProductoTransportado = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo")}
                Remesa.PesoCliente = Read(lector, "Peso_Cliente")
                Remesa.CantidadCliente = Read(lector, "Cantidad_Cliente")
                Remesa.ValorComercialCliente = Read(lector, "Valor_Comercial_Cliente")
                Remesa.PesoVolumetricoCliente = Read(lector, "Peso_Volumetrico_Cliente")
                TipoEntregaRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TERP_Codigo")}
                TipoTransporteRemsaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TTRP_Codigo")}
                TipoDespachoRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TDRP_Codigo")}
                TemperaturaProductoRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TPRP_Codigo")}
                TipoServicioRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TSRP_Codigo")}
                TipoInterfazRemesaPaqueteria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIRP_Codigo")}
                FechaInterfaz = Read(lector, "Fecha_Interfaz")
                Remesa.Remitente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Remitente")}
                Remesa.Remitente.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Remitente")}
                Remesa.Remitente.Direccion = Read(lector, "Direccion_Remitente")
                Remesa.Remitente.Telefonos = Read(lector, "Telefonos_Remitente")
                'Remesa.Detalle = New DetalleRemesas
                'Remesa.Detalle.Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario")}
                'Remesa.Detalle.Destinatario.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destinatario")}
                'Remesa.Detalle.Destinatario.Direccion = Read(lector, "Direccion_Destinatario")
                'Remesa.Detalle.Destinatario.Telefonos = Read(lector, "Telefonos_Destinatario")
                Remesa.ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
                Remesa.ValorManejoCliente = Read(lector, "Valor_Manejo_Cliente")
                Remesa.ValorSeguroCliente = Read(lector, "Valor_Seguro_Cliente")
                Remesa.ValorDescuentoCliente = Read(lector, "Valor_Descuento_Cliente")
                Remesa.TotalFleteCliente = Read(lector, "Total_Flete_Cliente")
                Remesa.TotalFleteTransportador = Read(lector, "Total_Flete_Transportador")
                Remesa.Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}
                DetalleTarifario.NumeroTarifario = Read(lector, "ETCV_Numero")
                DetalleTarifario.TipoLineaNegocioTransportes = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo"), .LineaNegocioTransporte = New LineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo")}}
                DetalleTarifario.Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo")}
                DetalleTarifario.TipoTarifaTransportes = New TipoTarifaTransportes With {.Codigo = Read(lector, "TTTC_Codigo"), .TarifaTransporte = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo")}}
            End If
        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Remesa As Remesas
        <JsonProperty>
        Public Property DetalleTarifario As DetalleTarifarioVentas
        <JsonProperty>
        Public Property TransportadorExterno As Terceros
        <JsonProperty>
        Public Property NumeroGuiaExterna As String
        <JsonProperty>
        Public Property TipoTransporteRemsaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property TipoDespachoRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property TemperaturaProductoRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property TipoServicioRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property TipoEntregaRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property EstadoRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property TipoInterfazRemesaPaqueteria As ValorCatalogos
        <JsonProperty>
        Public Property FechaInterfaz As Date
        <JsonProperty>
        Public Property CodigoOficina As Integer
        <JsonProperty>
        Public Property NumeroPlanilla As Integer
        <JsonProperty>
        Public Property NumeroPlanillaInicial As Integer
        <JsonProperty>
        Public Property NumeroPlanillaFinal As Integer
        <JsonProperty>
        Public Property FechaPlanillaInicial As Date
        <JsonProperty>
        Public Property FechaPlanillaFinal As Date

    End Class
End Namespace
