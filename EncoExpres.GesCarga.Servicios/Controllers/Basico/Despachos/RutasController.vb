﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="RutasController"/>
''' </summary>
<Authorize>
Public Class RutasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Rutas) As Respuesta(Of IEnumerable(Of Object))
        Return SrLstObject(Of Object)(New LogicaRutas(CapaPersistenciaRutas).Consultar(filtro))
    End Function

    <HttpPost>
    <ActionName("ConsultarTrayectos")>
    Public Function ConsultarTrayectos(ByVal filtro As Rutas) As Respuesta(Of IEnumerable(Of TrayectoRuta))
        Return New LogicaRutas(CapaPersistenciaRutas).ConsultarTrayectos(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Rutas) As Respuesta(Of Rutas)
        Return New LogicaRutas(CapaPersistenciaRutas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Rutas) As Respuesta(Of Long)
        Return New LogicaRutas(CapaPersistenciaRutas).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Rutas) As Respuesta(Of Boolean)
        Return New LogicaRutas(CapaPersistenciaRutas).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("InsertarPuestosControl")>
    Public Function InsertarPuestosControl(ByVal entidad As Rutas) As Respuesta(Of Boolean)
        Return New LogicaRutas(CapaPersistenciaRutas).InsertarPuestosControl(entidad)
    End Function
    <HttpPost>
    <ActionName("InsertarLegalizacionGastosRuta")>
    Public Function InsertarLegalizacionGastosRuta(ByVal entidad As Rutas) As Respuesta(Of Boolean)
        Return New LogicaRutas(CapaPersistenciaRutas).InsertarLegalizacionGastosRuta(entidad)
    End Function
    <HttpPost>
    <ActionName("InsertarPeaje")>
    Public Function InsertarPeaje(ByVal entidad As Rutas) As Respuesta(Of Boolean)
        Return New LogicaRutas(CapaPersistenciaRutas).InsertarPeaje(entidad)
    End Function
    <ActionName("GenerarPlanitilla")>
    Public Function GenerarPlanitilla(ByVal Filtro As Rutas) As Respuesta(Of Rutas)
        Return New LogicaRutas(CapaPersistenciaRutas).GenerarPlanitilla(Filtro)
    End Function

    <HttpPost>
    <ActionName("InsertarTrayectos")>
    Public Function InsertarTrayectos(ByVal entidad As Rutas) As Respuesta(Of Boolean)
        Return New LogicaRutas(CapaPersistenciaRutas).InsertarTrayectos(entidad)
    End Function
End Class
