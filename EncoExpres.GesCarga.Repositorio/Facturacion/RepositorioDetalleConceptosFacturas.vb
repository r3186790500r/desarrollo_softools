﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Facturacion

Namespace Facturacion
    ''' <summary>
    ''' Clase <see cref="RepositorioFacturas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleConceptosFacturas
        Inherits RepositorioBase(Of DetalleConceptosFacturas)
        Public Overrides Function Consultar(filtro As DetalleConceptosFacturas) As IEnumerable(Of DetalleConceptosFacturas)
            'Throw New NotImplementedException()
            Dim lista As New List(Of DetalleConceptosFacturas)
            Dim item As DetalleConceptosFacturas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENFA_Numero", filtro.NumeroEncabezado)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_conceptos_facturas]")

                While resultado.Read
                    item = New DetalleConceptosFacturas(resultado)
                    Dim DetalleImpuestosConceptosFacturas As New DetalleImpuestosConceptosFacturas With {
                        .CodigoEmpresa = filtro.CodigoEmpresa,
                        .NumeroEncabezado = filtro.NumeroEncabezado,
                        .DECF_Codigo = item.DECF_Codigo
                    }
                    Dim RepoDetalleImpuestosConceptoFacturaIntermuni As New RepositorioDetalleImpuestosConceptosFacturas()
                    item.DetalleImpuestoConceptosFacturas = RepoDetalleImpuestosConceptoFacturaIntermuni.Consultar(DetalleImpuestosConceptosFacturas)
                    lista.Add(item)
                End While

            End Using

            Return lista

        End Function
        Public Overrides Function Insertar(entidad As DetalleConceptosFacturas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleConceptosFacturas) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As DetalleConceptosFacturas) As DetalleConceptosFacturas
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleConceptosFacturas) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
