﻿PRINT 'gsp_insertar_semirremolques'
go
DROP PROCEDURE gsp_insertar_semirremolques
GO
CREATE PROCEDURE gsp_insertar_semirremolques
(
@par_EMPR_Codigo SMALLINT
,@Codigo NUMERIC = 0
,@par_Placa	varchar(20)
,@par_Codigo_AlternO varchar(20) = null
,@par_TERC_Codigo_Propietario NUMERIC = NULL
,@par_Equipo_Propio SMALLINT = NULL
,@par_MASE_Codigo NUMERIC = NULL
,@par_CATA_TISE_Codigo NUMERIC = NULL
,@par_CATA_TICA_Codigo NUMERIC = NULL
,@par_CATA_TIEQ_Codigo NUMERIC = NULL
,@par_Modelo NUMERIC = NULL
,@par_Numero_Ejes SMALLINT = NULL
,@par_Levanta_Ejes SMALLINT = NULL
,@par_Ancho NUMERIC(18,2) = NULL
,@par_Alto NUMERIC(18,2)  = NULL
,@par_Largo NUMERIC(18,2)  = NULL
,@par_Peso_Vacio NUMERIC(18,2)  = NULL
,@par_Capacidad_Galones NUMERIC(18,2)  = NULL
,@par_Capacidad_Kilos NUMERIC(18,2)  = NULL

,@par_Justificacion_Bloqueo varchar(150) = NULL
,@par_Estado SMALLINT = NULL
,@par_USUA_Codigo_Crea SMALLINT = NULL

)
AS
BEGIN

IF( @Codigo = 0)
BEGIN
	EXEC gsp_generar_consecutivo  @par_EMPR_Codigo, 50, 0, @Codigo OUTPUT  
END
INSERT INTO Semirremolques
           (EMPR_Codigo  
           ,Codigo
           ,Placa
		   ,Codigo_Alterno
           ,TERC_Codigo_Propietario
           ,Equipo_Propio
           ,MASE_Codigo
           ,CATA_TISE_Codigo
           ,CATA_TICA_Codigo
           ,CATA_TIEQ_Codigo
           ,Modelo
           ,Numero_Ejes
           ,Levanta_Ejes
           ,Ancho
           ,Alto
           ,Largo
           ,Peso_Vacio
           ,Capacidad_Galones
           ,Capacidad_Kilos
        
           ,Justificacion_Bloqueo
           ,Estado
           ,USUA_Codigo_Crea
           ,Fecha_Crea
           )
     VALUES
           (@par_EMPR_Codigo
           ,@Codigo
           ,@par_Placa
		   ,@par_Codigo_Alterno
           ,ISNULL(@par_TERC_Codigo_Propietario,0)
           ,ISNULL(@par_Equipo_Propio,0)
           ,ISNULL(@par_MASE_Codigo,0)
           ,ISNULL(@par_CATA_TISE_Codigo,3400)
           ,ISNULL(@par_CATA_TICA_Codigo,2300)
           ,ISNULL(@par_CATA_TIEQ_Codigo,3500)
           ,@par_Modelo
           ,@par_Numero_Ejes
           ,@par_Levanta_Ejes
           ,@par_Ancho
           ,@par_Alto
           ,@par_Largo
           ,@par_Peso_Vacio
           ,@par_Capacidad_Galones
           ,@par_Capacidad_Kilos
          
           ,@par_Justificacion_Bloqueo
           ,ISNULL(@par_Estado,0)
           ,@par_USUA_Codigo_Crea
           ,GETDATE()
           )
		   
select @Codigo as Codigo
END
GO