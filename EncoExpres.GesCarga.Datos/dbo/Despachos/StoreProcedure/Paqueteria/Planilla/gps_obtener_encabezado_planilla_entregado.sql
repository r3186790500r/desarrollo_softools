﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gps_obtener_encabezado_planilla_entregado' 

DROP PROCEDURE gps_obtener_encabezado_planilla_entregado
GO
CREATE PROCEDURE gps_obtener_encabezado_planilla_entregado
(@par_EMPR_Codigo NUMERIC
,@par_Numero NUMERIC
)
AS BEGIN
SELECT 
ENPE.Numero
,ENPE.Numero_Documento
,ENPE.Fecha
,ENPE.VEHI_Codigo 
,ENPE.TERC_Codigo_Conductor
,ENPE.Cantidad_Total
,ENPE.Peso_Total
,ENPE.OFIC_Codigo CodigoOficina
,ISNULL(ENPE.Numeracion,0) AS Numeracion
,ENPE.Estado
,ENPE.Anulado
,VEHI.Placa
,ISNULL(COND.Razon_Social,'')+' '+ISNULL(COND.Nombre,'')  
	  +' '+ISNULL(COND.Apellido1,'')+' '+ISNULL(COND.Apellido2,'') AS NombreCondutor
,ENPE.Observaciones
,1 AS Obtener
FROM Encabezado_Planilla_Entregas ENPE
,Terceros COND 
,Vehiculos VEHI
WHERE ENPE.EMPR_Codigo = COND.EMPR_Codigo 
AND ENPE.TERC_Codigo_Conductor = COND.Codigo

AND ENPE.EMPR_Codigo = VEHI.EMPR_Codigo 
AND ENPE.VEHI_Codigo = VEHI.Codigo

AND ENPE.EMPR_Codigo = @par_EMPR_Codigo
AND ENPE.Numero = @par_Numero
END
GO