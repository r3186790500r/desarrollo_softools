﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Proveedores

    <JsonObject>
    Public Class PlanillasDespachosProveedores
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Planilla")
            Fecha = Read(lector, "Fecha_Crea")
            NumeroDocumentoTransporte = Read(lector, "Numero_Documento_Transporte")
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .NombreCompleto = Read(lector, "PerfilCliente")}
            DocumentoCliente = Read(lector, "Documento_Cliente")
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Ruta")}
            Vehiculo = Read(lector, "PlacaVehiculo")
            Semirremolque = Read(lector, "Semirremolque")
            IdentificacionConductor = Read(lector, "TERC_Codigo_Conductor")
            NombreConductor = Read(lector, "Conductor")
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "Producto")}
            Cantidad = Read(lector, "Cantidad")
            Peso = Read(lector, "Peso")
            TipoTarifa = New TipoTarifaTransportes With {.Codigo = Read(lector, "TTTC_Codigo"), .Nombre = Read(lector, "TipoTarifa")}
            Tarifa = New ValorCatalogos With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "TipoTarifa")}
            FleteTransportador = Read(lector, "Valor_Flete_Transportador")
            TotalFlete = Read(lector, "Valor_Flete_Cliente")
            RetencionFuente = Read(lector, "Valor_Retencion_Fuente")
            RetencionICA = Read(lector, "Valor_Retencion_ICA")
            AnticipoCliente = Read(lector, "Valor_Anticipo_Cliente")
            ValorPagar = Read(lector, "Valor_Total_Flete_Cliente")
            AnticipoPropio = Read(lector, "Valor_Anticipo_Propio")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            FechaCrea = Read(lector, "Fecha_Crea")
            CodigoUsuarioCrea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea")}
            FechaModifica = Read(lector, "Fecha_Modifica")
            UsuarioModifica = Read(lector, "USUA_Codigo_Modifica")
            FechaAnula = Read(lector, "Fecha_Anula")
            UsuarioAnula = Read(lector, "USUA_Codigo_Anula")
            CausaAnulacion = Read(lector, "Causa_Anula")
            TotalRegistros = Read(lector, "TotalRegistros")
            Observaciones = Read(lector, "Observaciones")
            Consecutivo = Read(lector, "Numero_Documento")
            EncabezadoFactura = Read(lector, "EncabezadoFactura")
            Facturado = Read(lector, "Facturado")
            Manifiesto = New Documentos With {.NumeroDocumento = Read(lector, "NumeroManifiesto"), .Fecha = Read(lector, "FechaManifiesto")}
            Remesa = New Documentos With {.NumeroDocumento = Read(lector, "NumeroRemesa"), .Fecha = Read(lector, "FechaRemesa")}
            OrdenServicio = Read(lector, "OrdenServicio")
            Transportador = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Transportador"), .NombreCompleto = Read(lector, "Transportador")}
            AnticipoTransportador = Read(lector, "Valor_Anticipo_Transportador")
            TotalTransportador = Read(lector, "Valor_Total_Flete_Transportador")

        End Sub
#Region "Propiedades"

        <JsonProperty>
        Public Property Liquidacion As Short
        <JsonProperty>
        Public Property Consecutivo As Double
        <JsonProperty>
        Public Property NumeroDocumentoTransporte As Integer
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property DocumentoCliente As Integer
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property Vehiculo As String
        <JsonProperty>
        Public Property Semirremolque As String
        <JsonProperty>
        Public Property IdentificacionConductor As String
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property TipoTarifa As TipoTarifaTransportes
        <JsonProperty>
        Public Property FleteTransportador As Double
        <JsonProperty>
        Public Property TotalFlete As Double
        <JsonProperty>
        Public Property RetencionFuente As Double
        <JsonProperty>
        Public Property RetencionICA As Double
        <JsonProperty>
        Public Property AnticipoCliente As Double
        <JsonProperty>
        Public Property ValorPagar As Double
        <JsonProperty>
        Public Property AnticipoPropio As Double
        <JsonProperty>
        Public Property CodigoUsuarioCrea As Usuarios
        <JsonProperty>
        Public Property NombreConductor As String
        <JsonProperty>
        Public Property PlacaVehiculo As String
        <JsonProperty>
        Public Property EncabezadoFactura As Long
        <JsonProperty>
        Public Property Facturado As String

        <JsonProperty>
        Public Property Manifiesto As Documentos

        <JsonProperty>
        Public Property Remesa As Documentos
        <JsonProperty>
        Public Property OrdenServicio As Long

        <JsonProperty>
        Public Property Transportador As Terceros
        <JsonProperty>
        Public Property Tarifa As ValorCatalogos

        <JsonProperty>
        Public Property AnticipoTransportador As Double

        <JsonProperty>
        Public Property TotalTransportador As Double
#End Region
    End Class

End Namespace

