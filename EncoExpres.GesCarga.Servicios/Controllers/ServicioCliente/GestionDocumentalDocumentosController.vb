﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Negocio.ServicioCliente

''' <summary>
''' Controlador <see cref="GestionDocumentalDocumentosController"/>
''' </summary>
<Authorize>
Public Class GestionDocumentalDocumentosController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As GestionDocumentalDocumentos) As Respuesta(Of IEnumerable(Of GestionDocumentalDocumentos))
        Return New LogicaGestionDocumentalDocumentos(CapaPersistenciaGestionDocumentalDocumentos).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("InsertarTemporal")>
    Public Function Guardar(ByVal entidad As GestionDocumentalDocumentos) As Respuesta(Of Long)
        Return New LogicaGestionDocumentalDocumentos(CapaPersistenciaGestionDocumentalDocumentos).InsertarTemporal(entidad)
    End Function
    <HttpPost>
    <ActionName("EliminarDocumento")>
    Public Function EliminarDocumento(ByVal entidad As GestionDocumentalDocumentos) As Respuesta(Of Boolean)
        Return New LogicaGestionDocumentalDocumentos(CapaPersistenciaGestionDocumentalDocumentos).EliminarDocumento(entidad)
    End Function
    <HttpPost>
    <ActionName("EliminarDocumentoDefinitivo")>
    Public Function EliminarDocumentoDefinitivo(ByVal entidad As GestionDocumentalDocumentos) As Respuesta(Of Boolean)
        Return New LogicaGestionDocumentalDocumentos(CapaPersistenciaGestionDocumentalDocumentos).EliminarDocumentoDefinitivo(entidad)
    End Function
    <HttpPost>
    <ActionName("LimpiarDocumentoTemporalUsuario")>
    Public Function LimpiarDocumentoTemporalUsuario(ByVal entidad As GestionDocumentalDocumentos) As Respuesta(Of Boolean)
        Return New LogicaGestionDocumentalDocumentos(CapaPersistenciaGestionDocumentalDocumentos).LimpiarDocumentoTemporalUsuario(entidad)
    End Function

End Class
