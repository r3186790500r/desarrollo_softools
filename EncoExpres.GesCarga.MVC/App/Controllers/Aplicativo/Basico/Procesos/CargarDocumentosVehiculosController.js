﻿EncoExpresApp.controller("CargarDocumentosVehiculosCtrl", ['$scope', '$timeout', '$linq', 'TercerosFactory', 'blockUI', 'VehiculosFactory', 'blockUIConfig', 'GestionDocumentosFactory', 'DocumentosFactory',
    function ($scope, $timeout, $linq, TercerosFactory, blockUI, VehiculosFactory, blockUIConfig, GestionDocumentosFactory, DocumentosFactory) {
        console.clear()



        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.continuar = false
        $scope.VerErrores = false;
        $scope.DeshabilitarCliente = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.ListadoRutasPuntosGestion = []
        $scope.Modelo = {
            Fecha: new Date(),
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,

        }
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Procesos' }, { Nombre: 'Cargue Documentos Vehículos' }];
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/


        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ];
        GestionDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, EntidadDocumento: { Codigo: 1 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDocumentos = []
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            var item = response.data.Datos[i]
                            if (item.Habilitado == 1) {
                                if (item.Documento.Codigo == 1) {
                                }
                                else {
                                    $scope.ListadoDocumentos.push(item)
                                }
                            }

                        }
                    }
                    else {
                        $scope.Documentos = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                }
            }
            return $scope.ListadoVehiculos
        }
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.DataActual = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.DataActual = []
                for (var i = a - 10; i < $scope.DataArchivo.length; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }

        //Paginacion MOdal de errores

        $scope.PrimerPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10) {
                $scope.DataErrorActual = []
                $scope.paginaActualError = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.AnteriorModalError = function () {
            if ($scope.paginaActualError > 1) {
                $scope.paginaActualError -= 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteModalError = function () {
            if ($scope.paginaActualError < $scope.totalPaginasErrores) {
                $scope.paginaActualError += 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    $scope.UltimaPaginaModalError()

                }
            } else if ($scope.paginaActualError == $scope.totalPaginasErrores) {
                $scope.UltimaPaginaModalError()
            }
        }
        $scope.UltimaPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10 && $scope.totalPaginasErrores > 1) {
                $scope.paginaActualError = $scope.totalPaginasErrores
                var a = $scope.paginaActualError * 10
                $scope.DataErrorActual = []
                for (var i = a - 10; i < $scope.ListaErrores.length; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10) {
                $scope.ProgramacionesNoGuardadasActual = []
                $scope.paginaActualNoGuardadas = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }
        $scope.AnteriorNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas -= 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas < $scope.totalPaginasNoGuardadas) {
                $scope.paginaActualNoGuardadas += 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardados()

                }
            } else if ($scope.paginaActualNoGuardadas == $scope.totalPaginasNoGuardadas) {
                $scope.UltimaPaginaNoGuardados()
            }
        }
        $scope.UltimaPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10 && $scope.totalPaginasNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas = $scope.totalPaginasNoGuardadas
                var a = $scope.paginaActualNoGuardadas * 10
                $scope.ProgramacionesNoGuardadasActual = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10) {
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                $scope.paginaActualNoGuardadasRemplazo = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }
        $scope.AnteriorNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo -= 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo < $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.paginaActualNoGuardadasRemplazo += 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardadosRemplazo()

                }
            } else if ($scope.paginaActualNoGuardadasRemplazo == $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.UltimaPaginaNoGuardadosRemplazo()
            }
        }
        $scope.UltimaPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10 && $scope.totalPaginasNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo = $scope.totalPaginasNoGuardadasRemplazo
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }


        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardaras = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardadasRemplazo = function (item) {
            if ($scope.Option.name == 'Omitido') {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Omitido'
                }
            } else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Remplazado'
                }
            }

        }
        $scope.LimpiarData = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            $scope.DataVerificada = []
            $scope.DataActual = []
            $scope.DataErrorActual = []
            $scope.DataArchivo = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            $scope.VerErrores = false
        }

        /*-----------------------------------------------------------------------funciones de lectura y apertura de archivos----------------------------------------------------------------------------*/

        $scope.ListaProgramaciones = []
        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined || item == '') {
                return true
            } else {
                return false
            }
        }
        function AsignarValoresTabla() {
            $scope.NumeroCargue = 0

            if ($scope.DataArchivo.Valores.length > 0) {
                var DataTemporal = []
                var DataError = []
                $scope.error = 0
                for (var i = 0; i < $scope.DataArchivo.Valores.length; i++) {
                    var item = $scope.DataArchivo.Valores[i]
                    if (!Eval(item.Placa)) {
                        try {
                            item.Vehiculo = $scope.CargarVehiculosPlaca(item.Placa)
                            if (item.Vehiculo.Codigo) {
                                DataTemporal.push(item)
                            } else {
                                $scope.error++
                                DataError.push(item)
                            }
                        } catch (e) {
                            $scope.error++
                            DataError.push(item)
                        }
                    }
                }

                $scope.DataArchivo = DataTemporal
                $scope.DataError = DataError
                $scope.totalRegistros = ($scope.DataArchivo.length)
                $scope.paginaActual = 1
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                blockUI.start();
                blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');
                $timeout(function () {
                    try {
                        var data = []
                        for (var i = 0; i < $scope.DataArchivo.length; i++) {
                            var DataItem = $scope.DataArchivo[i]

                            if (!Eval(DataItem.CDGD_Codigo)) { try { DataItem.Configuracion = $linq.Enumerable().From($scope.ListadoDocumentos).First('$.Codigo == ' + DataItem.CDGD_Codigo); } catch (e) { } }
                            if (!Eval(DataItem.Referencia)) { try { DataItem.Referencia = DataItem.Referencia; } catch (e) { } }
                            if (!Eval(DataItem.Emisor)) { try { DataItem.Emisor = DataItem.Emisor; } catch (e) { } }
                            if (!Eval(DataItem.Fecha_Emision)) {
                                try {
                                    if (new Date(DataItem.Fecha_Emision) > MIN_DATE) {
                                        DataItem.FechaEmision = new Date(DataItem.Fecha_Emision);

                                    }
                                } catch (e) { }
                            }
                            if (!Eval(DataItem.Fecha_Vencimiento)) {
                                try {
                                    if (new Date(DataItem.Fecha_Vencimiento) > MIN_DATE) {
                                        DataItem.FechaVence = new Date(DataItem.Fecha_Vencimiento);

                                    }
                                } catch (e) { }
                            }
                        }
                        if ($scope.totalRegistros > 10) {
                            for (var i = 0; i < 10; i++) {
                                $scope.DataActual.push($scope.DataArchivo[i])
                            }
                        } else {
                            $scope.DataActual = $scope.DataArchivo
                        }
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    } catch (e) {
                        ShowError('Error en la lectura del archivo por favor intente nuevamente')
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    }
                    if ($scope.error > 0) {
                        try {
                            var data = []
                            for (var i = 0; i < $scope.DataError.length; i++) {
                                var DataItem = $scope.DataError[i]
                                if (!Eval(DataItem.Numero_Identificacion)) { try { DataItem.Numero_Identificacion = DataItem.Numero_Identificacion } catch (e) { } }
                                if (!Eval(DataItem.CDGD_Codigo)) { try { DataItem.Configuracion = $linq.Enumerable().From($scope.ListadoDocumentos).First('$.Codigo == ' + DataItem.CDGD_Codigo); } catch (e) { } }
                                if (!Eval(DataItem.Referencia)) { try { DataItem.Referencia = DataItem.Referencia; } catch (e) { } }
                                if (!Eval(DataItem.Emisor)) { try { DataItem.Emisor = DataItem.Emisor; } catch (e) { } }
                                if (!Eval(DataItem.Fecha_Emision)) {
                                    try {
                                        if (new Date(DataItem.Fecha_Emision) > MIN_DATE) {
                                            DataItem.FechaEmision = new Date(DataItem.Fecha_Emision);

                                        }
                                    } catch (e) { }
                                }
                                if (!Eval(DataItem.Fecha_Vencimiento)) {
                                    try {
                                        if (new Date(DataItem.Fecha_Vencimiento) > MIN_DATE) {
                                            DataItem.FechaVence = new Date(DataItem.Fecha_Vencimiento);

                                        }
                                    } catch (e) { }
                                }
                            }
                            blockUI.stop()
                            $timeout(blockUI.stop(), 1000)
                        } catch (e) {
                        }
                        showModal('ModalErrorCargue')
                        ShowError('No se cargarón (' + $scope.error.toString() + ') Restristos dado que la placa diligenciada no corresponde a ningun vehículo registrado en el sistema')
                    }
                }, 1000)
            }

        }
        var conterroresprevalidar = 0

        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            if ($scope.DataArchivo.Valores != undefined) {
                $scope.DataArchivo = { Valores: $scope.DataArchivo.Valores }
                AsignarValoresTabla()
            }
            else {
                ShowError('El arhivo cargado no corresponde al formato de carge masivo del tarifario')
            }
            blockUI.stop()

        }

        $scope.handleFile = function (e) {
            var files = e.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = e.target.result;
                $scope.arr = fixdata(data);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(btoa($scope.arr), { type: 'base64' });
                        process_wb($scope.wb);
                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);
            });
        }

        function restrow(item) {
            item.st = {}
            item.ms = {}
        }

        $scope.ValidarDocumento = function () {
            $scope.ListaErrores = []
            $scope.MensajesError = []
            /*
                        blockUI.start();
                        $timeout(function() {
                            blockUI.message('Validando Archivo');
                        }, 1000);*/
            var contadorgeneral = 0
            var contadorRegistros = 0
            $scope.DataVerificada = []
            $scope.DataVerificadatmp = []
            for (var i = 0; i < $scope.DataArchivo.length; i++) {
                var item = $scope.DataArchivo[i]
                item.ID = i
                //resetea todos los estilos de la fila que se va a evaluar
                restrow(item);
                var errores = 0
                //Condicion para saber si aplican las validaciones (Si el registro no tiene una ruta seleccionada no validara el resto de campos)
                if (item.Omitido !== true) {
                    contadorRegistros++
                    if (item.Configuracion == undefined || item.Configuracion == '') {
                        item.st.Configuracion = stError
                        item.ms.Configuracion = 'Seleccione el documento'
                        errores++
                    }
                    if (item.Referencia == undefined || item.Referencia == '') {
                        item.st.Referencia = stError
                        item.ms.Referencia = 'Ingrese la referencia'
                        errores++
                    }
                    if (ValidarCampo(item.Emisor) == 1) {
                        if (item.Configuracion.AplicaEmisor == CAMPO_DOCUMENTO_OBLIGATORIO) {
                            item.st.Emisor = stError
                            item.ms.Emisor = 'Ingrese el emisor'
                            errores++
                        }
                    }
                    if (ValidarFecha(item.FechaEmision) == 1) {
                        if (item.Configuracion.AplicaFechaEmision == CAMPO_DOCUMENTO_OBLIGATORIO) {
                            item.st.FechaEmision = stError
                            item.ms.FechaEmision = 'Ingrese la fecha de emisión'
                            errores++
                        } else {
                            item.FechaEmision = ''
                        }
                    } else {
                        var f = new Date();
                        if (item.FechaEmision > f) {
                            item.st.FechaEmision = stError
                            item.ms.FechaEmision = 'La fecha de emisión debe ser menor a la fecha actual'
                            errores++
                        }
                    }
                    if (ValidarFecha(item.FechaVence) == 1) {
                        if (item.Configuracion.AplicaFechaVencimiento == CAMPO_DOCUMENTO_OBLIGATORIO) {
                            item.st.FechaVence = stError
                            item.ms.FechaVence = 'Ingrese la fecha de vencimientos'
                            errores++
                        } else {
                            item.FechaVence = ''
                        }
                    } else {
                        if (item.Configuracion.AplicaFechaVencimiento != CAMPO_DOCUMENTO_NO_APLICA) {
                            var f = new Date();
                            if (item.FechaVence < f) {
                                item.st.FechaVence = stError
                                item.ms.FechaVence = 'La fecha de vencimiento debe ser mayor a la fecha actual'
                                errores++
                            }
                        } else {
                            item.FechaVence = ''
                        }
                    }

                    if (errores > 0) {
                        item.st.Row = stwarning
                        $scope.ListaErrores.push(item)
                        contadorgeneral++
                    }
                    else {
                        item.stRow = ''
                        $scope.DataVerificada.push(item)
                    }
                }
                else {
                    restrow(item);
                }
            }
            //blockUI.stop();

            if (contadorgeneral > 0) {
                ShowError('Se encontraron inconsistencias en los datos ingresados, por favor corrija los datos marcados o seleccione omitirlos.');
                $scope.paginaActualError = 1
                $scope.DataErrorActual = []
                $scope.totalRegistrosErrores = $scope.ListaErrores.length
                $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 10);
                if ($scope.totalRegistrosErrores > 10) {
                    for (var i = 0; i < 10; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    for (var i = 0; i < $scope.ListaErrores.length; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                if (contadorRegistros == 0) {
                    ShowError('No se encontraron registros para validar, por favor revise que la casilla de omitir no se encuentre marcada en al menos un registro')
                } else {
                    ShowWarningConfirm('Los datos se verificaron correctamente.¿Desea continuar con el cargue de la información?', $scope.Guardar)
                }
            }


        }


        $scope.Guardar = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        $scope.GuardarErrados = function () {
            //closeModal('ModalErrores')
            data = []
            for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                if (!$scope.ProgramacionesNoGuardadas[i].Omitido && $scope.ProgramacionesNoGuardadas[i].Option != 'Omitido') {
                    if ($scope.ProgramacionesNoGuardadas[i].Option == 'Remplazado') {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 1
                    } else {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 0
                    }
                    data.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
            $scope.DataVerificada = data
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        var contGuard = 0
        var contGuardGeneral = 0
        var contGeneral = 0
        var Guardaros = 0
        function GuardarCompleto(inicio, opt) {
            contGuard++
            if (contGuard > $scope.DataVerificada.length) {

                ShowSuccess('Se guardaron ' + contGuardGeneral + ' de ' + contGeneral + ' Documentos')
                closeModal('modalConfirmacionRemplazarProgramacion');
                closeModal('ModalErrores');
                $scope.LimpiarData()

            } else {

                blockUI.start();
                $timeout(function () {
                    blockUI.message('Guardando Documentos');
                }, 1000);

                //conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                //conexion.AgregarParametroSQL("@par_Emisor", entidad.Emisor)
                //conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
                //If entidad.FechaEmision > Date.MinValue Then
                //conexion.AgregarParametroSQL("@par_Fecha_Emision", entidad.FechaEmision, SqlDbType.DateTime)
                //End If
                //If entidad.FechaVence > Date.MinValue Then
                //conexion.AgregarParametroSQL("@par_Fecha_Vence", entidad.FechaVence, SqlDbType.DateTime)
                //End If
                //conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                //conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.Configuracion.Codigo)
                //conexion.AgregarParametroSQL("@par_Elimina_Archivo", entidad.EliminaDocumento)

                //conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.CodigoTerceros)
                var Documento = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Emisor: $scope.DataVerificada[inicio].Emisor,
                    Referencia: $scope.DataVerificada[inicio].Referencia,
                    FechaEmision: $scope.DataVerificada[inicio].FechaEmision,
                    FechaVence: $scope.DataVerificada[inicio].FechaVence,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Configuracion: { Codigo: $scope.DataVerificada[inicio].Configuracion.Codigo },
                    CodigoVehiculo: $scope.DataVerificada[inicio].Vehiculo.Codigo
                }
                DocumentosFactory.GuardarDocumentoTercero(Documento).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        Guardaros++
                        contGuardGeneral++
                        GuardarCompleto(contGuard)
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $scope.ProgramacionesNoGuardadas.push($scope.DataVerificada[inicio])
                    GuardarCompleto(contGuard)
                    blockUI.stop();
                })
            }
        }


        $scope.ModalConfirmacionNuevoProceso = function () {
            showModal('modalConfirmacionCancelarProceso');
        }

        $scope.CerrarModalNuevoProceso = function (e) {
            var fileVal = document.getElementById("fileModal");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
            closeModal('modalConfirmacionCancelarProceso');


        }

        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (o) {
            return MascaraNumero(o)
        }
        $scope.MaskValoresGrid = function (o) {
            return MascaraValores(o)
        }
        $scope.MaskMayusGrid = function (o) {
            return MascaraMayus(o)
        }
        $scope.MaskplacaGrid = function (o) {
            return MascaraPlaca(o)
        }
        $scope.MaskplacaSemiremolqueGrid = function (o) {
            return MascaraPlacaSemirremolque(o)
        }

        $scope.GenerarPlanilla = function () {
            var entidad = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, PlantillaDocumento: 1 }
            blockUI.start();
            $timeout(function () {
                blockUI.message('Generando Plantilla..');
            }, 1000);
            VehiculosFactory.GenerarPlanitilla(entidad).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var pdfAsDataUri = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data.Datos.Planitlla;
                    var link = document.createElement('a');
                    link.href = pdfAsDataUri;
                    link.download = "Plantilla_Cargue_Masivo_Documentos_Vehiculos.xlsx";
                    link.click();
                    //window.open(pdfAsDataUri);
                    blockUI.stop();
                    ShowSuccess('La plantilla se generó correctamente')
                }
            }, function (response) {
                blockUI.stop();
                ShowError(response.statusText)
            })

        }

        $scope.DataActual = []

        function ValidarCampo(objeto, minlength, Esobjeto) {
            var resultado = 0
            if (objeto == undefined || objeto == '' || objeto == null || objeto == 0 || objeto == '0') {
                resultado = 1
            } else {
                if (resultado == 0) {
                    if (minlength !== undefined) {
                        if (objeto.length < minlength) {
                            resultado = 3
                        }
                        else {
                            resultado = 0
                        }
                    }
                } if (resultado == 0) {
                    if (Esobjeto) {
                        if (objeto.Codigo == undefined || objeto.Codigo == '' || objeto.Codigo == null || objeto.Codigo == 0) {
                            resultado = 2
                        }
                    }
                }
            }
            return resultado
        }
        //$scope.DataActual.push({})
    }]);

