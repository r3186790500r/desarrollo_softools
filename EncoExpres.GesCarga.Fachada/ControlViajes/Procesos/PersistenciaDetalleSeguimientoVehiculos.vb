﻿Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Repositorio.ControlViajes

Namespace ControlViajes
    Public NotInheritable Class PersistenciaDetalleSeguimientoVehiculos
        Implements IPersistenciaBase(Of DetalleSeguimientoVehiculos)

        Public Function Consultar(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos) Implements IPersistenciaBase(Of DetalleSeguimientoVehiculos).Consultar
            Return New RepositorioDetalleSeguimientoVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleSeguimientoVehiculos) As Long Implements IPersistenciaBase(Of DetalleSeguimientoVehiculos).Insertar
            Return New RepositorioDetalleSeguimientoVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleSeguimientoVehiculos) As Long Implements IPersistenciaBase(Of DetalleSeguimientoVehiculos).Modificar
            Return New RepositorioDetalleSeguimientoVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleSeguimientoVehiculos) As DetalleSeguimientoVehiculos Implements IPersistenciaBase(Of DetalleSeguimientoVehiculos).Obtener
            Return New RepositorioDetalleSeguimientoVehiculos().Obtener(filtro)
        End Function

        Public Function CambiarRutaSeguimiento(entidad As DetalleSeguimientoVehiculos) As Boolean
            Return New RepositorioDetalleSeguimientoVehiculos().CambiarRutaSeguimiento(entidad)
        End Function
        Public Function Anular(entidad As DetalleSeguimientoVehiculos) As Boolean
            Return New RepositorioDetalleSeguimientoVehiculos().Anular(entidad)
        End Function

        Public Function ConsultarRemesas(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Return New RepositorioDetalleSeguimientoVehiculos().ConsultarRemesas(filtro)
        End Function

        Public Function ConsultarNovedadesSitiosReporte(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Return New RepositorioDetalleSeguimientoVehiculos().ConsultarNovedadesSitiosReporte(filtro)
        End Function

        Public Function ConsultarMasterRemesa(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Return New RepositorioDetalleSeguimientoVehiculos().ConsultarMasterRemesa(filtro)
        End Function
        Public Function ConsultarPuntosControl(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Return New RepositorioDetalleSeguimientoVehiculos().ConsultarPuntosControl(filtro)
        End Function

        Public Function ConsultarPuestoControlRutas(filtro As DetalleSeguimientoVehiculos) As IEnumerable(Of DetalleSeguimientoVehiculos)
            Return New RepositorioDetalleSeguimientoVehiculos().ConsultarPuestoControlRutas(filtro)
        End Function
        Public Function EnviarCorreoCliente(entidad As DetalleSeguimientoVehiculos) As Boolean
            Return New RepositorioDetalleSeguimientoVehiculos().EnviarCorreoCliente(entidad)
        End Function

        Public Function EnviarCorreoSeguimiento(entidad As DetalleSeguimientoVehiculos) As Boolean
            Return New RepositorioDetalleSeguimientoVehiculos().EnviarEmailSeguimientoVehicular(entidad)
        End Function

        Public Function EnviarCorreoAvanceVehiculosCliente(entidad As DetalleSeguimientoVehiculos) As Boolean
            Return New RepositorioDetalleSeguimientoVehiculos().EnviarCorreoAvanceVehiculosCliente(entidad)
        End Function
        Public Function EnviarDocumentosConductor(entidad As DetalleSeguimientoVehiculos) As Boolean
            Return New RepositorioDetalleSeguimientoVehiculos().EnviarDocumentosConductor(entidad)
        End Function

    End Class

End Namespace

