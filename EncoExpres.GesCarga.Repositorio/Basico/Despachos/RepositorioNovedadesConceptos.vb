﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports System.Transactions

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioNovedadesConceptos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioNovedadesConceptos
        Inherits RepositorioBase(Of NovedadesConceptos)

        Public Overrides Function Consultar(filtro As NovedadesConceptos) As IEnumerable(Of NovedadesConceptos)
            Dim lista As New List(Of NovedadesConceptos)
            Dim item As NovedadesConceptos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.NODECodigo) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Novedad", filtro.NODECodigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_paises]")

                While resultado.Read
                    item = New NovedadesConceptos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As NovedadesConceptos) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_NODE_Codigo", entidad.NODECodigo)
                    conexion.AgregarParametroSQL("@par_COVE_Codigo", entidad.COVECodigo)
                    conexion.AgregarParametroSQL("@par_CLPD_Codigo", entidad.CLPDCodigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_paises")
                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While


                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As NovedadesConceptos) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_NODE_Codigo", entidad.NODECodigo)
                conexion.AgregarParametroSQL("@par_COVE_Codigo", entidad.COVECodigo)
                conexion.AgregarParametroSQL("@par_CLPD_Codigo", entidad.CLPDCodigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_paises")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As NovedadesConceptos) As NovedadesConceptos
            Dim item As New NovedadesConceptos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.NODECodigo > Cero Then
                    conexion.AgregarParametroSQL("@par_NODE_Codigo", filtro.NODECodigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_paises]")

                While resultado.Read
                    item = New NovedadesConceptos(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As NovedadesConceptos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_NODE_Codigo", entidad.NODECodigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_paises]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace