﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Namespace Fidelizacion.Documentos
    <JsonObject>
    Public NotInheritable Class PuntosVehiculos
        Inherits BaseBasico

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "VEHI_Placa")}
            Tenedor = New Tercero With {.Codigo = Read(lector, "TENE_Codigo"), .NombreCompleto = Read(lector, "TENE_Nombre")}
            Conductor = New Tercero With {.Codigo = Read(lector, "COND_Codigo"), .NombreCompleto = Read(lector, "COND_Nombre")}
            CATA_CFTR = New ValorCatalogos With {.Codigo = Read(lector, "CFTR_Codigo"), .Nombre = Read(lector, "CFTR_Campo1")}

            Fecha = Read(lector, "Fecha")
            TotalPuntos = Read(lector, "Total_Puntos")
            FechaActulizacion = Read(lector, "PPVE_FechaAct")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub

        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Tenedor As Tercero
        <JsonProperty>
        Public Property Conductor As Tercero
        <JsonProperty>
        Public Property CATA_CFTR As ValorCatalogos ' Categoria Fidelizacion Transportadores
        <JsonProperty>
        Public Property TotalPuntos As Long
        <JsonProperty>
        Public Property FechaActulizacion As String
        <JsonProperty>
        Public Property Fecha As Date
    End Class
End Namespace

