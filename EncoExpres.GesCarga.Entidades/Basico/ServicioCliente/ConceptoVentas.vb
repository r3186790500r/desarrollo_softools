﻿Imports Newtonsoft.Json

Namespace Basico.ServicioCliente

    ''' <summary>
    ''' Clase <see cref="ConceptoVentas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ConceptoVentas
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="ConceptoVentas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConceptoVentas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            ConceptoSistema = Read(lector, "Concepto_Sistema")
            Estado = Read(lector, "Estado")

        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property ConceptoSistema As String

    End Class

End Namespace
