﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 27/03/2021 
-- Módulo: Gesphone

PRINT 'gsp_consultar_encabezado_tarifario_base_ventas_sincronizacion'
GO
DROP PROCEDURE gsp_consultar_encabezado_tarifario_base_ventas_sincronizacion
GO
CREATE PROCEDURE dbo.gsp_consultar_encabezado_tarifario_base_ventas_sincronizacion      
(                      
  @par_EMPR_Codigo SMALLINT,
  @par_TERC_Codigo INTEGER                     
)                      
AS                       
BEGIN                      
 
  IF ISNULL(@par_TERC_Codigo, 0) > 0               
 BEGIN  

  SELECT TOP 1 @par_EMPR_Codigo AS CodigoEmpresa, convert(INTEGER,ETCV.Numero) AS Numero, ETCV.Nombre, ETCV.Estado, ISNULL(TERC.Gestion_Documentos, 0) AS GestionDocumentos 
    FROM Encabezado_Tarifario_Carga_Ventas ETCV 
    INNER JOIN Tercero_Clientes TERC ON ETCV.Numero = TERC.ETCV_Numero
    AND TERC.TERC_Codigo = @par_TERC_Codigo
    AND Estado = 1 
     ORDER BY ETCV.Numero DESC 
 
    END                        
 ELSE              
 BEGIN
  
   SELECT TOP 1 @par_EMPR_Codigo AS CodigoEmpresa, convert(INTEGER,Numero) AS Numero, Nombre, Estado, 0 AS GestionDocumentos
  FROM Encabezado_Tarifario_Carga_Ventas WITH(NOLOCK) WHERE 
    EMPR_Codigo = @par_EMPR_Codigo 
    AND Tarifario_Base = 1 
    AND Estado = 1 
    AND Fecha_Vigencia_Inicio <= GETDATE() AND Fecha_Vigencia_Fin >= GETDATE() 
  ORDER BY Numero DESC 
 
  END          
                  
END    
GO