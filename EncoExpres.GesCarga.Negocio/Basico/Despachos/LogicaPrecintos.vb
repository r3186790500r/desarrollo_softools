﻿Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Fachada.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Basico.Despachos
    Public NotInheritable Class LogicaPrecintos
        Inherits LogicaBase(Of Precintos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Precintos)
        ReadOnly _persistencia2 As IPersistenciaBase(Of DetallePrecintos)
        Sub New(persistencia As IPersistenciaBase(Of Precintos))
            _persistencia = persistencia

        End Sub

        Public Overrides Function Consultar(filtro As Precintos) As Respuesta(Of IEnumerable(Of Precintos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Precintos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Precintos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Precintos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function GuardarPrecintos(entidad As Precintos) As Respuesta(Of Precintos)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Precintos

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Precintos) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = CType(_persistencia, PersistenciaPrecintos).InsertarPrecintos(entidad)
            Else
                valor = CType(_persistencia, PersistenciaPrecintos).ModificarPrecintos(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Precintos)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Precintos)(valor)
        End Function

        Public Overrides Function Obtener(filtro As Precintos) As Respuesta(Of Precintos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Precintos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Precintos)(consulta)
        End Function
        Public Function ConsultarUnicoPrecinto(filtro As DetallePrecintos) As Respuesta(Of IEnumerable(Of DetallePrecintos))

            Dim consulta = CType(_persistencia, PersistenciaPrecintos).ConsultarUnicoPrecinto(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetallePrecintos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetallePrecintos))(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As Precintos) As Respuesta(Of Precintos)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Precintos) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Precintos) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As Precintos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaPrecintos).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function LiberarPrecinto(entidad As Precintos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaPrecintos).LiberarPrecinto(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, "liberó"), .Datos = anulo}
        End Function

        Public Function ValidarPrecintoPlanillaPaqueteria(entidad As Precintos) As Respuesta(Of Precintos)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valPrecinto As Precintos

            valPrecinto = CType(_persistencia, PersistenciaPrecintos).ValidarPrecintoPlanillaPaqueteria(entidad)

            If IsNothing(valPrecinto) Then
                Return New Respuesta(Of Precintos)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of Precintos)(valPrecinto) With {.ProcesoExitoso = True}
        End Function

    End Class

End Namespace