﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico
Imports EncoExpres.GesCarga.Entidades.Basico.General
Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref=" DetalleTareaMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleTareaMantenimiento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleTareaMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleTareaMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "ID")
            Numero = Read(lector, "ETEM_Numero")
            SistemaMantenimiento = New Sistemas With {.Codigo = Read(lector, "SIMA_Codigo"), .Nombre = Read(lector, "SistemaMantenimiento")}
            SubsistemaMantenimiento = New SubSistemas With {.Codigo = Read(lector, "SUMA_Codigo"), .Nombre = Read(lector, "SubsistemaMantenimiento")}
            TipoMantenimiento = New ValorCatalogos With {.Codigo = Read(lector, "TIME_Codigo"), .Nombre = Read(lector, "TipoMantenimiento")}
            UnidadReferenciaMantenimiento = New UnidadReferenciaMantenimiento With {.Codigo = Read(lector, "UNFM_Codigo"), .NombreCorto = Read(lector, "UnidadReferencia")}
            If Not IsDBNull(Read(lector, "Descripcion")) Then
                Descripcion = Read(lector, "Descripcion")
            End If
            Proveedor = New Tercero With {.Codigo = Read(lector, "TERC_Proveedor"), .Nombre = Read(lector, "NombreProveedor")}
            NumeroFactura = Read(lector, "Numero_Factura")
            FechaFactura = Read(lector, "Fecha_Factura")
            FechaVenceGarantia = Read(lector, "Fecha_Vence_Garantia")
            Valor = Read(lector, "Valor")
            ValorIva = Read(lector, "Valor_IVA")
            ValorTotal = Read(lector, "Valor_Total")
            Cantidad = Read(lector, "Cantidad")
            If Not IsDBNull(Read(lector, "Km_Uso")) Then
                KmUso = Read(lector, "Km_Uso")
            Else
                KmUso = 0
            End If

        End Sub

        ''' <summary>
        ''' Obtiene o establece el sistema mantenimineto del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property SistemaMantenimiento As Sistemas


        ''' <summary>
        ''' Obtiene o establece subsistema mantenimiento del detalle
        ''' </summary>
        <JsonProperty>
        Public Property SubsistemaMantenimiento As SubSistemas


        ''' <summary>
        ''' Obtiene o establece el tipo mantenimineto del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property TipoMantenimiento As ValorCatalogos

        ''' <summary>
        ''' Obtiene o establece el tipo mantenimineto del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property UnidadReferenciaMantenimiento As UnidadReferenciaMantenimiento


        ''' <summary>
        ''' Obtiene o establece la descripcion del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property Descripcion As String


        ''' <summary>
        ''' Obtiene o establece el proveedor del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property Proveedor As Tercero

        ''' <summary>
        ''' Obtiene o estableceel numero de la factura del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property NumeroFactura As String

        ''' <summary>
        ''' Obtiene o establece la fecha de la factura del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property FechaFactura As DateTime


        ''' <summary>
        ''' Obtiene o establece la fecha vence garantia del detalle
        ''' </summary>
        <JsonProperty>
        Public Property FechaVenceGarantia As DateTime


        ''' <summary>
        ''' Obtiene o estableceel el valor del detalle
        ''' </summary>
        <JsonProperty>
        Public Property Valor As Double

        ''' <summary>
        ''' Obtiene o establece el valor iva del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property ValorIva As Double

        ''' <summary>
        ''' Obtiene o establece el valor total del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property ValorTotal As Double

        ''' <summary>
        ''' Obtiene o establece  ENDA Tido codgigo del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property ENDATiDOCodigo As Integer

        ''' <summary>
        ''' Obtiene o establece el ENDA Numero del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property ENDANumero As Integer


        ''' <summary>
        ''' Obtiene o establece el cantidad del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property Cantidad As Integer
        <JsonProperty>
        Public Property KmUso As Integer
    End Class
End Namespace
