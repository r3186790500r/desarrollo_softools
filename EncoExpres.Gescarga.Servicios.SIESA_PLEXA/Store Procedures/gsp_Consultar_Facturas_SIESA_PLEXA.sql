CREATE PROCEDURE [dbo].[gsp_Consultar_Facturas_SIESA_PLEXA]    
(    
 @par_EMPR_Codigo smallint, 
 @par_Numero numeric = NULL
)    
AS    
BEGIN    
 SELECT DISTINCT TOP 10    
 ENFA.EMPR_Codigo,    
 ENFA.Numero,    
 ENFA.Numero_Documento,    
 ENFA.Fecha,    
 ENFA.TERC_Cliente,    
 CLIE.Numero_Identificacion Identificacion_Tercero,    
 ENFA.OFIC_Factura,    
 ENFA.Observaciones,    
 EMPR.Numero_Identificacion Identificacion_Empresa,    
 ISNULL(ENFA.Interfaz_Contable_Factura, 0 ) Interfaz_Contable_Factura,    
 ISNULL(ENFA.Intentos_Interfase_Contable, 0 ) Intentos_Interfase_Contable,  
 DPFS.Bodega,  
 DPFS.Unidad_Negocio,  
 DPFS.Item,  
 DPFS.Motivo  
  
 FROM Encabezado_Facturas ENFA    
    
 INNER JOIN Terceros CLIE ON    
 ENFA.EMPR_Codigo = CLIE.EMPR_Codigo    
 AND ENFA.TERC_Cliente = CLIE.Codigo    
    
 INNER JOIN Empresas EMPR ON    
 ENFA.EMPR_Codigo = EMPR.Codigo    
  
 INNER JOIN Detalle_Remesas_Facturas DERF ON        
 ENFA.EMPR_Codigo = DERF.EMPR_Codigo        
 AND ENFA.Numero = DERF.ENFA_Numero        
        
 LEFT JOIN Encabezado_Remesas ENRE ON        
 DERF.EMPR_Codigo = ENRE.EMPR_Codigo        
 AND DERF.ENRE_Numero = ENRE.Numero        
      
 LEFT JOIN Producto_Transportados PRTR ON      
 ENRE.EMPR_Codigo = PRTR.EMPR_Codigo      
 AND ENRE.PRTR_Codigo = PRTR.Codigo    
   
 LEFT JOIN Detalle_Producto_Factura_Siesa DPFS ON  
 DPFS.EMPR_Codigo = PRTR.EMPR_Codigo  
 AND DPFS.PRTR_Codigo = PRTR.Codigo  
    
 WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo   
 AND (ENFA.Numero = @par_Numero OR @par_Numero IS NULL)
 AND ENFA.CATA_TIFA_Codigo = 5201--Factura Ventas    
 AND ISNULL(ENFA.Interfaz_Contable_Factura, 0 ) = 0    
 AND ISNULL(ENFA.Intentos_Interfase_Contable, 0 ) < 5    
 AND ENFA.Estado = 1 
 AND ENFA.Anulado = 0
    
 ORDER BY ENFA.Numero, ENFA.Fecha    
    
END    
GO


