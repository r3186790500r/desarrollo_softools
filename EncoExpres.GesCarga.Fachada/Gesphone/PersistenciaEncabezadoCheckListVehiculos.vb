﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Repositorio.Gesphone

Namespace Gesphone
    Public NotInheritable Class PersistenciaEncabezadoCheckListVehiculos
        Implements IPersistenciaBase(Of EncabezadoCheckListVehiculos)

        Public Function Consultar(filtro As EncabezadoCheckListVehiculos) As IEnumerable(Of EncabezadoCheckListVehiculos) Implements IPersistenciaBase(Of EncabezadoCheckListVehiculos).Consultar
            Return New RepositorioEncabezadoCheckListVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoCheckListVehiculos) As Long Implements IPersistenciaBase(Of EncabezadoCheckListVehiculos).Insertar
            Return New RepositorioEncabezadoCheckListVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoCheckListVehiculos) As Long Implements IPersistenciaBase(Of EncabezadoCheckListVehiculos).Modificar
            Return New RepositorioEncabezadoCheckListVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoCheckListVehiculos) As EncabezadoCheckListVehiculos Implements IPersistenciaBase(Of EncabezadoCheckListVehiculos).Obtener
            Return New RepositorioEncabezadoCheckListVehiculos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EncabezadoCheckListVehiculos) As Boolean
            Return New RepositorioEncabezadoCheckListVehiculos().Anular(entidad)
        End Function
    End Class

End Namespace

