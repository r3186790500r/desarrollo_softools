﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 13/08/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_insertar_paises'
GO
DROP PROCEDURE gsp_insertar_paises
GO
CREATE PROCEDURE gsp_insertar_paises 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo_Alterno VARCHAR(20),  
@par_Nombre VARCHAR(50),  
@par_MONE_Codigo NUMERIC,  
@par_Estado SMALLINT,  
@par_Nombre_Corto CHAR(5) = NULL,  
@par_USUA_Codigo_Crea SMALLINT  
)  
AS  
BEGIN  
INSERT INTO Paises  
(  
EMPR_Codigo,  
Codigo_Alterno,  
Nombre,  
MONE_Codigo,  
Estado,  
Nombre_Corto,  
USUA_Codigo_Crea,  
Fecha_Crea  
)  
VALUES   
(  
@par_EMPR_Codigo,  
@par_Codigo_Alterno,  
@par_Nombre,  
@par_MONE_Codigo,  
@par_Estado,  
ISNULL(@par_Nombre_Corto, ''),  
@par_USUA_Codigo_Crea,  
GETDATE()  
)  
  
SELECT @@IDENTITY AS Codigo  
END  
GO