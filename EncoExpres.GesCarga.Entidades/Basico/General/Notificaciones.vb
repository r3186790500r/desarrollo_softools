﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Basico.General
    <JsonObject>
    Public NotInheritable Class Notificaciones
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Notificaciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = lector.Item("EMPR_Codigo")
            UsuarioNotificacion = New Usuarios With {.Codigo = lector.Item("USUA_Codigo_Notificacion")}
            ID = lector.Item("ID")
            ProcesoNotificacion = New ValorCatalogos With {.Codigo = lector.Item("CATA_PRNO_Codigo")}
            Mensaje = lector.Item("Mensaje")
            NumeroOrigen = lector.Item("Numero_Origen")
            FechaCreacion = lector.Item("Fecha_Crea_Notificacion")
            FechaUltimaNotificacion = lector.Item("Fecha_Ultima_Notificacion")
            NumeroNotificaciones = lector.Item("Numero_Notificaciones")


        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property UsuarioNotificacion As Usuarios
        <JsonProperty>
        Public Property ProcesoNotificacion As ValorCatalogos
        <JsonProperty>
        Public Property Mensaje As String
        <JsonProperty>
        Public Property NumeroOrigen As Integer
        <JsonProperty>
        Public Property ID As Integer
        <JsonProperty>
        Public Property FechaCreacion As DateTime
        <JsonProperty>
        Public Property FechaUltimaNotificacion As DateTime
        <JsonProperty>
        Public Property NumeroNotificaciones As Integer
    End Class
End Namespace
