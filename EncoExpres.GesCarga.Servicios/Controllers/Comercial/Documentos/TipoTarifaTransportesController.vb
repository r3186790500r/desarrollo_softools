﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Negocio.Comercial.Documentos

''' <summary>
''' Controlador <see cref="TipoTarifaTransportesController"/>
''' </summary>
<Authorize>
Public Class TipoTarifaTransportesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TipoTarifaTransportes) As Respuesta(Of IEnumerable(Of TipoTarifaTransportes))
        Return New LogicaTipoTarifaTransportes(CapaPersistenciaTipoTarifaTransportes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TipoTarifaTransportes) As Respuesta(Of TipoTarifaTransportes)
        Return New LogicaTipoTarifaTransportes(CapaPersistenciaTipoTarifaTransportes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TipoTarifaTransportes) As Respuesta(Of Long)
        Return New LogicaTipoTarifaTransportes(CapaPersistenciaTipoTarifaTransportes).Guardar(entidad)
    End Function

End Class
