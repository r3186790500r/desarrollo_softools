﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaLineaVehiculos
        Implements IPersistenciaBase(Of LineaVehiculos)

        Public Function Consultar(filtro As LineaVehiculos) As IEnumerable(Of LineaVehiculos) Implements IPersistenciaBase(Of LineaVehiculos).Consultar
            Return New RepositorioLineaVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As LineaVehiculos) As Long Implements IPersistenciaBase(Of LineaVehiculos).Insertar
            Return New RepositorioLineaVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As LineaVehiculos) As Long Implements IPersistenciaBase(Of LineaVehiculos).Modificar
            Return New RepositorioLineaVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As LineaVehiculos) As LineaVehiculos Implements IPersistenciaBase(Of LineaVehiculos).Obtener
            Return New RepositorioLineaVehiculos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As LineaVehiculos) As Boolean
            Return New RepositorioLineaVehiculos().Anular(entidad)
        End Function
    End Class

End Namespace

