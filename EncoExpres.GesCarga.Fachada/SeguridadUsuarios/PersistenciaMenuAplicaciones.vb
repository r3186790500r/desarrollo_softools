﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Repositorio.SeguridadUsuarios

Namespace SeguridadUsuarios
    Public NotInheritable Class PersistenciaMenuAplicaciones
        Implements IPersistenciaBase(Of MenuAplicaciones)

        Public Function Consultar(filtro As MenuAplicaciones) As IEnumerable(Of MenuAplicaciones) Implements IPersistenciaBase(Of MenuAplicaciones).Consultar
            Return New RepositorioMenuAplicaciones().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As MenuAplicaciones) As Long Implements IPersistenciaBase(Of MenuAplicaciones).Insertar
            Return New RepositorioMenuAplicaciones().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As MenuAplicaciones) As Long Implements IPersistenciaBase(Of MenuAplicaciones).Modificar
            Return New RepositorioMenuAplicaciones().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As MenuAplicaciones) As MenuAplicaciones Implements IPersistenciaBase(Of MenuAplicaciones).Obtener
            Return New RepositorioMenuAplicaciones().Obtener(filtro)
        End Function

        Public Function ConsultarMenuPorUsuario(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of MenuAplicaciones)
            Return New RepositorioMenuAplicaciones().ConsultarMenuPorUsuario(codigoEmpresa, codigoUsuario)
        End Function
        Public Function ConsultarModuloPorUsuario(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of MenuAplicaciones)
            Return New RepositorioMenuAplicaciones().ConsultarModuloPorUsuario(codigoEmpresa, codigoUsuario)
        End Function

    End Class
End Namespace
