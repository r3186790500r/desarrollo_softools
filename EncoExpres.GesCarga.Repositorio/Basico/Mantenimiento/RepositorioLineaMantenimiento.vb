﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Mantenimiento


Namespace Mantenimiento
    Public NotInheritable Class RepositorioLineaMantenimiento
        Inherits RepositorioBase(Of LineaMantenimiento)
        Public Overrides Function Consultar(filtro As LineaMantenimiento) As IEnumerable(Of LineaMantenimiento)
            Dim lista As New List(Of LineaMantenimiento)
            Dim item As LineaMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Nombre) And filtro.Nombre <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre.ToString)
                End If
                If Not IsNothing(filtro.Estado) Then
                    If Not IsNothing(filtro.Estado.Codigo) Or filtro.Estado.Codigo = vbNull Then
                        If filtro.Estado.Codigo >= 0 Then 'NO APLICA
                            conexion.AgregarParametroSQL("@par_estado", filtro.Estado.Codigo)
                        End If
                    End If

                End If
                If Not IsNothing(filtro.Codigo) Then
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.CodigoAlterno) Then
                    If filtro.CodigoAlterno <> "" Then
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_consultar_linea_mantenimientos")

                While resultado.Read
                    item = New LineaMantenimiento(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As LineaMantenimiento) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_insertar_linea_mantenimientos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As LineaMantenimiento) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Modifica ", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_modificar_linea_mantenimientos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As LineaMantenimiento) As LineaMantenimiento
            Dim item As New LineaMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_obtener_linea_mantenimientos")

                While resultado.Read
                    item = New LineaMantenimiento(resultado)
                End While

            End Using

            Return item
        End Function
    End Class
End Namespace
