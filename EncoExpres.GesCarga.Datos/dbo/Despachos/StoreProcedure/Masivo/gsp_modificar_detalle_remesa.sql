﻿PRINT 'gsp_modificar_detalle_remesa'
go
DROP PROCEDURE gsp_modificar_detalle_remesa
GO
create procedure gsp_modificar_detalle_remesa
(
@par_EMPR_Codigo  smallint
,@par_ENRE_Numero  numeric(18,0) 
,@par_PRTR_Codigo  numeric(18,0)
,@par_Descripcion  varchar(200) = null
,@par_Serie  varchar(50)= null
,@par_Modelo  varchar(50)= null
,@par_Cantidad  numeric(18,2)= null
,@par_Peso  numeric(18,2)= null
,@par_Remision  varchar(50)= null
,@par_Valor_Mercancia  money= null
,@par_TERC_Codigo_Destinatario  numeric(18,0)= null
,@par_CIUD_Codigo_Destinatario  numeric(18,0)= null
,@par_Direccion_Destinatario  varchar(150)= null
,@par_Telefonos_Destinatario  varchar(100)= null
)
AS BEGIN
update Detalle_Remesas
           
           set PRTR_Codigo= @par_PRTR_Codigo
           ,Descripcion = ISNULL(@par_Descripcion,'')
           ,Serie = ISNULL(@par_Serie ,'')
           ,Modelo = ISNULL(@par_Modelo ,'')
           ,Cantidad = ISNULL(@par_Cantidad ,0 )
           ,Peso = ISNULL(@par_Peso  ,0)
           ,Remision = ISNULL(@par_Remision  ,'')
           ,Valor_Mercancia = ISNULL(@par_Valor_Mercancia ,0)
           ,TERC_Codigo_Destinatario = @par_TERC_Codigo_Destinatario
           ,CIUD_Codigo_Destinatario = @par_CIUD_Codigo_Destinatario
           ,Direccion_Destinatario = ISNULL(@par_Direccion_Destinatario ,'')
           ,Telefonos_Destinatario = ISNULL(@par_Telefonos_Destinatario ,'')

		   WHERE 
		   EMPR_Codigo = @par_EMPR_Codigo
           AND ENRE_Numero = @par_ENRE_Numero

		  select ID as Codigo from Detalle_Remesas where EMPR_Codigo = @par_EMPR_Codigo
           AND ENRE_Numero = @par_ENRE_Numero
END
GO
