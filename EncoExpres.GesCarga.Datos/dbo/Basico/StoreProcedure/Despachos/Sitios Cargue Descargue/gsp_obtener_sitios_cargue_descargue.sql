﻿
PRINT 'gsp_obtener_sitios_cargue_descargue'
GO
DROP PROCEDURE gsp_obtener_sitios_cargue_descargue
GO
CREATE PROCEDURE gsp_obtener_sitios_cargue_descargue  
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_Codigo NUMERIC  
)  
AS   
BEGIN  
  SELECT  
     1 AS Obtener,  
     SICD.EMPR_Codigo,  
     SICD.Codigo,  
     SICD.Nombre,  
  	 SICD.CATA_TSCD_Codigo,  
	 SICD.PAIS_Codigo,
	 PAIS.Nombre AS Nombre_Pais, 
	 SICD.CIUD_Codigo,
	 CIUD.Nombre AS Nombre_Ciudad, 
	 SICD.Direccion,
	 SICD.Telefono,
	 ISNULL(SICD.Codigo_Postal, '') AS Codigo_Postal,
	 ISNULL(SICD.Contacto, '') AS Contacto,
     SICD.Estado,  
   ROW_NUMBER() OVER(ORDER BY SICD.Nombre) AS RowNumber  
    FROM  
     Sitios_Cargue_Descargue AS SICD	  
     LEFT JOIN Valor_Catalogos AS VACA                   
     ON SICD.Codigo = VACA.EMPR_Codigo                    
     AND SICD.CATA_TSCD_Codigo = VACA.Codigo  
	 LEFT JOIN Paises AS PAIS                   
     ON SICD.Codigo = PAIS.EMPR_Codigo                    
     AND SICD.PAIS_Codigo = PAIS.Codigo  
	 LEFT JOIN Ciudades AS CIUD                   
     ON SICD.Codigo = CIUD.EMPR_Codigo                    
     AND SICD.CIUD_Codigo = CIUD.Codigo  
  WHERE  
   SICD.EMPR_Codigo = @par_EMPR_Codigo  
   AND SICD.Codigo = @par_Codigo    
END  
GO