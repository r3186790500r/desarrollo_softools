﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Entidades.Tesoreria

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="ConceptoLiquidacionPlanillaDespacho"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ConceptoLiquidacionPlanillaDespacho
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConceptoLiquidacionPlanillaDespacho"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConceptoLiquidacionPlanillaDespacho"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            FechaCrea = Read(lector, "Fecha_Crea")
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            ConceptoSistema = Read(lector, "Concepto_Sistema")
            AplicaImpuesto = Read(lector, "Impuesto")
            Operacion = Read(lector, "Operacion")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            ValorMinimo = Read(lector, "Valor_Minimo")
            ValorMaximo = Read(lector, "Valor_Maximo")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")

            Else
                ValorFijo = Read(lector, "Valor_Fijo")
                ValorPorcentaje = Read(lector, "Valor_Porcentaje")
                PUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo")}
            End If


        End Sub

        <JsonProperty>
        Public Property DocumentoCuenta As EncabezadoDocumentoCuentas

        <JsonProperty>
        Public Property Placa As String

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property ConceptoSistema As Integer

        <JsonProperty>
        Public Property AplicaImpuesto As Integer

        <JsonProperty>
        Public Property ValorFijo As Double

        <JsonProperty>
        Public Property ValorPorcentaje As Double

        <JsonProperty>
        Public Property PUC As PlanUnicoCuentas


        <JsonProperty>
        Public Property Operacion As Integer

        <JsonProperty>
        Public Property Anular As Integer

        <JsonProperty>
        Public Property CodigoImpuesto As Integer



        <JsonProperty>
        Public Property ListadoImpuestos As IEnumerable(Of ImpuestoConceptoLiquidacionPlanillaDespacho)

        <JsonProperty>
        Public Property ValorMinimo As Double
        <JsonProperty>
        Public Property ValorMaximo As Double

        <JsonProperty>
        Public Property Codigo As Integer



    End Class
End Namespace
