﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Negocio.Despachos.Paqueteria
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria

''' <summary>
''' Controlador <see cref="DetallePlanillaDespachosController"/>
''' </summary>
<Authorize>
Public Class DetallePlanillaDespachosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetallePlanillaDespachos) As Respuesta(Of IEnumerable(Of DetallePlanillaDespachos))
        Return New LogicaDetallePlanillaDespachos(CapaPersistenciaDetallePlanillaDespachos).Consultar(filtro)
    End Function

End Class
