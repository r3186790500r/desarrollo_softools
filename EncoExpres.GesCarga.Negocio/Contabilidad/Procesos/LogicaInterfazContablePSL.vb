﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Contabilidad.Procesos
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Contabilidad
    Public NotInheritable Class LogicaInterfazContablePSL
        Inherits LogicaBase(Of InterfazContablePSL)

        ReadOnly _persistencia As IPersistenciaBase(Of InterfazContablePSL)

        Sub New(persistencia As IPersistenciaBase(Of InterfazContablePSL))
            _persistencia = persistencia
        End Sub


        Public Overrides Function Consultar(filtro As InterfazContablePSL) As Respuesta(Of IEnumerable(Of InterfazContablePSL))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of InterfazContablePSL))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of InterfazContablePSL))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As InterfazContablePSL) As Respuesta(Of InterfazContablePSL)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Guardar(entidad As InterfazContablePSL) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace

