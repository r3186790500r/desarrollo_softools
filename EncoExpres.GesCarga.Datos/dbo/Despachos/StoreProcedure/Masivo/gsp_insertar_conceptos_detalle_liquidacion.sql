﻿PRINT 'gsp_insertar_conceptos_detalle_liquidacion'
GO

DROP PROCEDURE gsp_insertar_conceptos_detalle_liquidacion
GO

CREATE PROCEDURE gsp_insertar_conceptos_detalle_liquidacion
(
	@par_EMPR_Codigo NUMERIC,
	@par_ELPD_Numero NUMERIC
)
AS
BEGIN
	DECLARE @ValorAnticipo MONEY
	DECLARE @ValorReanticipo MONEY

	CREATE TABLE #Conceptos_Liquidacion_Planilla_Despachos
	(
		EMPR_Codigo SMALLINT,
		ELPD_Numero NUMERIC,
		CLPD_Codigo NUMERIC,
		Observaciones VARCHAR(500),
		Valor MONEY	
	)

	SELECT
		@ValorAnticipo = ISNULL(SUM(Valor_Pago_Recaudo_Total),0)
	FROM
		Encabezado_Documento_Comprobantes
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND TIDO_Codigo = 30 --Comprobante Egreso
		AND CATA_DOOR_Codigo = 2604 --Anticipo
		AND Documento_Origen = @par_ELPD_Numero

	--Se buscan los comprobantes de egreso de reanticipo realizados a la planilla
	SELECT
		@ValorReanticipo = ISNULL(SUM(Valor_Pago_Recaudo_Total),0)
	FROM
		Encabezado_Documento_Comprobantes
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND TIDO_Codigo = 30 --Comprobante Egreso
		AND CATA_DOOR_Codigo = 2605 --Reanticipo
		AND Documento_Origen = @par_ELPD_Numero

	--Se insertar todos los conceptos de liquidación a la tabla temportal
	INSERT INTO
		#Conceptos_Liquidacion_Planilla_Despachos
	SELECT
		@par_EMPR_Codigo,
		0,
		Codigo,
		'',
		0
	FROM
		Conceptos_Liquidacion_Planilla_Despachos
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Estado = 1

	--Se actualizan los valores de la tabla temporal de acuerdo a los comprobantes de egreso
	UPDATE
		#Conceptos_Liquidacion_Planilla_Despachos
	SET
		Valor = @ValorAnticipo
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND (ELPD_Numero = 0 OR ELPD_Numero = @par_ELPD_Numero)
		AND CLPD_Codigo = 1 --Anticipo

	UPDATE
		#Conceptos_Liquidacion_Planilla_Despachos
	SET
		Valor = @ValorReanticipo
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND (ELPD_Numero = 0 OR ELPD_Numero = @par_ELPD_Numero)
		AND CLPD_Codigo = 2 --Reanticipo

	--Se retorna la lista
	SELECT
		TCLPD.EMPR_Codigo,
		TCLPD.ELPD_Numero,
		ISNULL(ELPD.Numero_Documento,0) AS Numero_Documento,
		TCLPD.CLPD_Codigo,
		CLPD.Nombre,
		TCLPD.Observaciones,
		TCLPD.Valor,
		CLPD.Operacion,
		CLPD.Concepto_Sistema,
		CLPD.Aplica_Valor_Base_Impuestos
	FROM
		#Conceptos_Liquidacion_Planilla_Despachos TCLPD

		INNER JOIN Conceptos_Liquidacion_Planilla_Despachos CLPD
		ON TCLPD.EMPR_Codigo = CLPD.EMPR_Codigo AND TCLPD.CLPD_Codigo = CLPD.Codigo
		AND TCLPD.EMPR_Codigo = @par_EMPR_Codigo
		AND TCLPD.ELPD_Numero = 0 OR TCLPD.ELPD_Numero = @par_ELPD_Numero

		LEFT JOIN Encabezado_Liquidacion_Planilla_Despachos ELPD
		ON TCLPD.EMPR_Codigo = ELPD.EMPR_Codigo	AND TCLPD.ELPD_Numero = ELPD.Numero
END
GO