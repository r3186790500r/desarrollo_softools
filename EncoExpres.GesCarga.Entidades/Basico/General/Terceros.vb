﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.Operacion
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Basico.General
    <JsonObject>
    Public Class Cliente
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Cliente"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Cliente"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        ''' <param name="autocomplete">Indica si la consulta es para cargar un autocomplete</param>
        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            RepresentanteComercial = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Comercial")}
            FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FPCL_Codigo")}
            DiasPlazo = Read(lector, "Dias_Plazo_Pago")
            Tarifario = New TarifarioVentas With {.Codigo = Read(lector, "ETCV_Numero")}
            MargenUtilidad = Read(lector, "Margen_Utilidad")
            DiasCierreFacturacion = Read(lector, "Dia_Cierre_Facturacion")
            ManejaCondicionesPesoCumplido = Read(lector, "Maneja_Condiciones_Peso_Cumplido")
            CondicionesComercialesTarifas = Read(lector, "Maneja_Condiciones_Comerciales_Tarifas")
            ModalidadFacturacionPeso = New ValorCatalogos With {.Codigo = Read(lector, "CATA_MFPC_Codigo"), .Nombre = Read(lector, "ModalidadFacturacion")}
            CorreoFacturaElectronica = Read(lector, "Correo_Facturacion")
            GestionDocumentos = Read(lector, "Gestion_Documentos")
            BloquearDespachos = Read(lector, "Bloquear_Despachos")

            AnalistaCartera = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Analista_Cartera")}


        End Sub


        <JsonProperty>
        Public Property Codigo As Integer
        <JsonProperty>
        Public Property RepresentanteComercial As Tercero
        <JsonProperty>
        Public Property FormaPago As ValorCatalogos
        <JsonProperty>
        Public Property DiasPlazo As Integer
        <JsonProperty>
        Public Property Tarifario As TarifarioVentas
        <JsonProperty>
        Public Property TarifariosPaq As IEnumerable(Of TarifarioVentas)
        <JsonProperty>
        Public Property Impuestos As IEnumerable(Of ImpuestosTerceros)
        <JsonProperty>
        Public Property MargenUtilidad As Decimal
        <JsonProperty>
        Public Property DiasCierreFacturacion As Integer
        <JsonProperty>
        Public Property ManejaCondicionesPesoCumplido As Short
        <JsonProperty>
        Public Property CondicionesComercialesTarifas As Short
        <JsonProperty>
        Public Property ListaGestionDocumentos As IEnumerable(Of GestionDocumentoCliente)

        <JsonProperty>
        Public Property GestionDocumentos As Short
        Public Property BloquearDespachos As Object
        <JsonProperty>
        Public Property AnalistaCartera As Terceros
        <JsonProperty>
        Public Property CondicionesComerciales As IEnumerable(Of CondicionComercialTarifaTercero)
        <JsonProperty>
        Public Property ListadoCondicionesComerciales As IEnumerable(Of CondicionComercialTarifaTercero)
        <JsonProperty>
        Public Property ModalidadFacturacionPeso As ValorCatalogos
        <JsonProperty>
        Public Property CorreoFacturaElectronica As String

    End Class
    <JsonObject>
    Public Class Conductor
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Conductor"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Conductor"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        ''' <param name="autocomplete">Indica si la consulta es para cargar un autocomplete</param>
        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Propio = Read(lector, "Conductor_Propio")
            Afiliado = Read(lector, "Conductor_Afiliado")
            BloqueadoAltoRiesgo = Read(lector, "Bloqueo_Alto_Riesgo")
            TipoContrato = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TCCO_Codigo")}
            CategoriaLicencia = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CALC_Codigo")}
            TipoSangre = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TISA_Codigo")}
            ViajesConductor = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CCCV_Codigo")}
            NumeroLicencia = Read(lector, "Numero_Licencia")
            FechaVencimiento = Read(lector, "Fecha_Vencimiento_Licencia")
            FechaUltimoViaje = Read(lector, "Fecha_Ultimo_Viaje")
            ReferenciasPersonales = Read(lector, "Referencias_Personales")
        End Sub
        <JsonProperty>
        Public Property Codigo As Integer
        <JsonProperty>
        Public Property Propio As Long
        <JsonProperty>
        Public Property BloqueadoAltoRiesgo As Long
        <JsonProperty>
        Public Property Afiliado As Long
        <JsonProperty>
        Public Property TipoContrato As ValorCatalogos
        <JsonProperty>
        Public Property NumeroLicencia As String
        <JsonProperty>
        Public Property CategoriaLicencia As ValorCatalogos
        <JsonProperty>
        Public Property ViajesConductor As ValorCatalogos
        <JsonProperty>
        Public Property TipoSangre As ValorCatalogos
        <JsonProperty>
        Public Property FechaVencimiento As Date
        <JsonProperty>
        Public Property FechaUltimoViaje As Date
        <JsonProperty>
        Public Property ReferenciasPersonales As String
    End Class
    <JsonObject>
    Public Class Empleado
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Empleado"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Empleado"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        ''' <param name="autocomplete">Indica si la consulta es para cargar un autocomplete</param>
        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            FechaVinculacion = Read(lector, "Fecha_Vinculacion")
            FechaFinalizacion = Read(lector, "Fecha_Finalizacion")
            TipoContrato = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICE_Codigo")}
            Cargo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CARG_Codigo")}
            Departamento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DEEM_Codigo")}
            PorcentajeComision = Read(lector, "Porcentaje_Comision")
            Salario = Read(lector, "Salario")
            ValorAuxilioTransporte = Read(lector, "Valor_Auxilio_Transporte")
            ValorSeguridadSocial = Read(lector, "Valor_Seguridad_Social")
            ValorAporteParafiscales = Read(lector, "Valor_Aporte_Parafiscales")
            ValorSeguroVida = Read(lector, "Valor_Seguro_Vida")
            ValorProvisionPrestacionesSociales = Read(lector, "Valor_Provision_Prestaciones_Sociales")
        End Sub
        <JsonProperty>
        Public Property Codigo As Integer
        <JsonProperty>
        Public Property FechaVinculacion As Date
        <JsonProperty>
        Public Property FechaFinalizacion As Date
        <JsonProperty>
        Public Property TipoContrato As ValorCatalogos
        <JsonProperty>
        Public Property Departamento As ValorCatalogos
        <JsonProperty>
        Public Property Cargo As ValorCatalogos
        <JsonProperty>
        Public Property PorcentajeComision As Double
        <JsonProperty>
        Public Property Salario As Double
        <JsonProperty>
        Public Property ValorAuxilioTransporte As Double
        <JsonProperty>
        Public Property ValorSeguridadSocial As Double
        <JsonProperty>
        Public Property ValorAporteParafiscales As Double
        <JsonProperty>
        Public Property ValorSeguroVida As Double
        <JsonProperty>
        Public Property ValorProvisionPrestacionesSociales As Double
        <JsonProperty>
        Public Property Externo As Short
    End Class
    <JsonObject>
    Public Class Proveedor
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Proveedor"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Proveedor"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        ''' <param name="autocomplete">Indica si la consulta es para cargar un autocomplete</param>
        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Tarifario = New TarifarioCompras With {.Codigo = Read(lector, "ETCC_Numero")}
            FormaCobro = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FOCO_Codigo")}
            DiasPlazo = Read(lector, "Dias_Plazo_Cobro")
        End Sub
        <JsonProperty>
        Public Property Tarifario As TarifarioCompras
        <JsonProperty>
        Public Property FormaCobro As ValorCatalogos
        <JsonProperty>
        Public Property DiasPlazo As Integer
        <JsonProperty>
        Public Property Impuestos As IEnumerable(Of ImpuestosTerceros)
    End Class



    <JsonObject>
    Public Class FormasPago
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Perfil"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Perfil"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader con resultado de consulta</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")

        End Sub

        ''' <summary>
        ''' Obtiene o establece el codigo del tercero asociado al perfil
        ''' </summary>
        Public Property CodigoTercero As Long

        ''' <summary>
        ''' Obtiene o establece el tipo perfil
        ''' </summary>
        ''' <returns></returns>
        Public Property Codigo As Integer

    End Class

    <JsonObject>
    Public Class Perfil
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Perfil"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Perfil"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader con resultado de consulta</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
        End Sub

        ''' <summary>
        ''' Obtiene o establece el codigo del tercero asociado al perfil
        ''' </summary>
        Public Property CodigoTercero As Long

        ''' <summary>
        ''' Obtiene o establece el tipo perfil
        ''' </summary>
        ''' <returns></returns>
        Public Property Codigo As Integer

    End Class

    <JsonObject>
    Public Class Tercero
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Tercero"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Tercero"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        ''' <param name="autocomplete">Indica si la consulta es para cargar un autocomplete</param>
        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            NombreCompleto = Read(lector, "NombreTercero")
            NumeroIdentificacion = Read(lector, "Numero_Identificacion")
            FechaModifica = Read(lector, "Fecha_Modifica")
            UsuarioModifica = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Modifica"), .Nombre = Read(lector, "UsuarioModifica")}
            JustificacionBloqueo = Read(lector, "Justificacion_Bloqueo")
            EventoAuditoria = Read(lector, "Evento_Auditoria")
            Estado = Read(lector, "Estado")
        End Sub

        <JsonProperty>
        Public Property JustificacionBloqueo As String
        <JsonProperty>
        Public Property EventoAuditoria As String
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Identificacion As String
        <JsonProperty>
        Public Property NombreCompleto As String
        <JsonProperty>
        Public Property NumeroIdentificacion As String
        <JsonProperty>
        Public Property Celulares As String
    End Class
    <JsonObject>
    Public Class TerceroDirecciones
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TerceroDirecciones"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TerceroDirecciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        ''' <param name="autocomplete">Indica si la consulta es para cargar un autocomplete</param>
        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Tercero = New Tercero With {.Codigo = Read(lector, "TERC_Codigo")}
            Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Direccion")}
            Direccion = Read(lector, "Direccion")
            Telefonos = Read(lector, "Telefonos")
            Barrio = Read(lector, "Barrio")
            CodigoPostal = Read(lector, "Codigo_Postal")
            Estado = Read(lector, "Estado")

        End Sub
        ''' <summary>
        ''' Obtiene o establece el origen de la consulta
        ''' </summary>
        <JsonProperty>
        Public Property Codigo As Long
        <JsonProperty>
        Public Property Ciudad As Ciudades
        <JsonProperty>
        Public Property Tercero As Tercero
        <JsonProperty>
        Public Property Direccion As String
        <JsonProperty>
        Public Property Telefonos As String
        <JsonProperty>
        Public Property Barrio As String
        <JsonProperty>
        Public Property CodigoPostal As String
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property CodigoAlterno As String
        <JsonProperty>
        Public Property Estado As Byte

    End Class

    <JsonObject>
    Public Class TerceroCondicionesPesoCumplido
        Inherits Base
        Sub New()
        End Sub
        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TerceroCondicionesPesoCumplido"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        ''' <param name="autocomplete">Indica si la consulta es para cargar un autocomplete</param>
        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)

            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "PRTR_Nombre")}
            UnidadEmpaque = New UnidadEmpaque With {.Codigo = Read(lector, "UMPT_Codigo"), .Nombre = Read(lector, "UMPT_Nombre")}
            BaseCalculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_BCPC_Codigo"), .Nombre = Read(lector, "CATA_BCPC_Nombre")}
            ValorUnidad = Read(lector, "Valor_Unidad")
            TipoCalculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TCPC_Codigo"), .Nombre = Read(lector, "CATA_TCPC_Nombre")}
            Tolerancia = Read(lector, "Tolerancia")
            CondicionCobro = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CCPC_Codigo"), .Nombre = Read(lector, "CATA_CCPC_Nombre")}

        End Sub
        ''' <summary>
        ''' Obtiene o establece el origen de la consulta
        ''' </summary>
        <JsonProperty>
        Public Property Tercero As Tercero
        <JsonProperty>
        Public Property Codigo As Long
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property UnidadEmpaque As UnidadEmpaque
        <JsonProperty>
        Public Property BaseCalculo As ValorCatalogos
        <JsonProperty>
        Public Property ValorUnidad As Double
        <JsonProperty>
        Public Property TipoCalculo As ValorCatalogos
        <JsonProperty>
        Public Property Tolerancia As Double
        <JsonProperty>
        Public Property CondicionCobro As ValorCatalogos
        <JsonProperty>
        Public Property UsuarioCodigoCrea As Integer


    End Class

    <JsonObject>
    Public Class Terceros
        Inherits Base

        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Terceros"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        ''' <param name="autocomplete">Indica si la consulta es para cargar un autocomplete</param>
        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False, Optional TarifarioProveedoresConsulta As Boolean = False, Optional TarifarioClienteConsulta As Boolean = False)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Pais = New Paises With {.Codigo = Read(lector, "PAIS_Codigo")}
            If TarifarioProveedoresConsulta Then
                NumeroIdentificacion = Read(lector, "Numero_Identificacion")
                CodigoAlterno = Read(lector, "Codigo_Alterno")
                Celular = Read(lector, "Celulares")
                Codigo = Read(lector, "TERC_Codigo")
                NombreCompleto = Read(lector, "NombreCompleto")
                Telefonos = Read(lector, "Telefonos")
                NumeroTarifarioCompra = Read(lector, "ETCC_Numero")
                DiasPlazoCobro = Read(lector, "Dias_Plazo_Cobro")
                CodigoFormaPago = Read(lector, "CATA_FOCO_Codigo")
                NombreTarifario = Read(lector, "NombreTarifario")
                EstadoTarifario = Read(lector, "EstadoTarifario")
                ValorFlete = Read(lector, "Valor_Flete")
                TarifaCarga = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "NombreTarifaCarga")}
                TipoTarifaCarga = New TipoTarifaTransportes With {.Codigo = Read(lector, "TTTC_Codigo"), .Nombre = Read(lector, "NombreTipoTarifaCarga")}
                CodigoFormaPagoTarifa = Read(lector, "CodigoFormaPagoTarifa")
            ElseIf TarifarioClienteConsulta Then
                NumeroTarifarioVenta = Read(lector, "ETCV_Numero")
                CodigoDetalleTarifa = Read(lector, "DTCV_Codigo")
                CodigoLineaNegocioTransportes = Read(lector, "LNTC_Codigo")
                CodigoTipoLineaNegocioTransportes = Read(lector, "TLNC_Codigo")
                TarifaCarga = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "NombreTarifaCarga")}
                TipoTarifaCarga = New TipoTarifaTransportes With {.Codigo = Read(lector, "TTTC_Codigo"), .Nombre = Read(lector, "NombreTipoTarifaCarga")}
                ValorCatalogoTipoTarifa = New ValorCatalogos With {
                    .Codigo = Read(lector, "VACA_Codigo"),
                    .CampoAuxiliar2 = Read(lector, "VACA_Campo2"),
                    .CampoAuxiliar3 = Read(lector, "VACA_Campo3"),
                    .Catalogo = New Catalogos With {.Codigo = Read(lector, "CATA_Codigo")}
                }
                CodigoFormaPago = Read(lector, "CATA_FPCL_Codigo")
                CodigoFormaPagoTarifa = Read(lector, "CodigoFormaPagoTarifa")
                ValorFlete = Read(lector, "Valor_Flete")
                ValorEscolta = Read(lector, "Valor_Escolta")
                ValorOtros1 = Read(lector, "Valor_Otros1")
                ValorOtros2 = Read(lector, "Valor_Otros2")
                ValorOtros3 = Read(lector, "Valor_Otros3")
                ValorOtros4 = Read(lector, "Valor_Otros4")
                ValorManejo = Read(lector, "Valor_Manejo")
                ValorSeguro = Read(lector, "Valor_Seguro")
                PorcentajeSeguro = Read(lector, "Porcentaje_Seguro")
                Reexpedicion = Read(lector, "Reexpedicion")
                PorcentajeReexpedicion = Read(lector, "Porcentaje_Reexpedicion")
                ValorCargue = Read(lector, "Valor_Cargue")
                ValorDescargue = Read(lector, "Valor_Descargue")
                TiempoEntrega = Read(lector, "Tiempo_Entrega")
            Else
                NumeroIdentificacion = Read(lector, "Numero_Identificacion")
                CodigoAlterno = Read(lector, "Codigo_Alterno")
                Celular = Read(lector, "Celulares")
                Codigo = Read(lector, "Codigo")
                NombreCompleto = ""
                If Len(Trim(Read(lector, "Razon_Social"))) > 0 Then
                    NombreCompleto = Read(lector, "Razon_Social")
                Else
                    If Len(Trim(Read(lector, "Nombre"))) > 0 Then
                        NombreCompleto += Read(lector, "Nombre")
                    End If
                    If Len(Trim(Read(lector, "Apellido1"))) > 0 Then
                        NombreCompleto += " " + Read(lector, "Apellido1")
                    End If
                    If Len(Trim(Read(lector, "Apellido2"))) > 0 Then
                        NombreCompleto += " " + Read(lector, "Apellido2")
                    End If
                End If
                CodigoComercial = Read(lector, "TERC_Codigo_Beneficiario")
                If Read(lector, "Obtener") = 1 Then
                    CodigoContable = Read(lector, "Codigo_Contable")
                    TipoNaturaleza = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TINT_Codigo")}
                    TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIID_Codigo")}
                    DigitoChequeo = Read(lector, "Digito_Chequeo")
                    RazonSocial = Read(lector, "Razon_Social")
                    RepresentanteLegal = Read(lector, "Representante_Legal")
                    Nombre = Read(lector, "Nombre")
                    PrimeroApellido = Read(lector, "Apellido1")
                    SegundoApellido = Read(lector, "Apellido2")
                    Sexo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_SETE_Codigo")}
                    CiudadExpedicionIdentificacion = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Identificacion")}
                    CiudadNacimiento = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Nacimiento")}
                    Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Direccion")}
                    Direccion = Read(lector, "Direccion")
                    Barrio = Read(lector, "Barrio")
                    CodigoPostal = Read(lector, "Codigo_Postal")
                    Telefonos = Read(lector, "Telefonos")
                    Correo = Read(lector, "Emails")
                    Banco = New Bancos With {.Codigo = Read(lector, "BANC_Codigo")}
                    TipoBanco = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICB_Codigo")}
                    CuentaBancaria = Read(lector, "Numero_Cuenta_Bancaria")
                    Beneficiario = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Beneficiario")}
                    TitularCuentaBancaria = Read(lector, "Titular_Cuenta_Bancaria")
                    Observaciones = Read(lector, "Observaciones")
                    If Not IsDBNull(Read(lector, "Foto")) Then
                        Foto = Read(lector, "Foto")
                    End If
                    'EstadoTerceros = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESTE_Codigo")}
                    JustificacionBloqueo = Read(lector, "Justificacion_Bloqueo")
                Else
                    TipoNaturaleza = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TINT_Codigo")}
                    TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIID_Codigo")}
                    Ciudad = New Ciudades With {.Nombre = Read(lector, "NombreCuidadDireccion"), .Codigo = Read(lector, "CIUD_Codigo_Direccion"), .NombreCompleto = Read(lector, "CiudadDepartamento")}
                    CiudadExpedicionIdent = New Ciudades With {.Nombre = Read(lector, "Ciudad_Expedicion"), .Codigo = Read(lector, "CIUD_Codigo_Expedicion")}
                    Telefonos = Read(lector, "Telefonos")
                    DigitoChequeo = Read(lector, "Digito_Chequeo")
                    Barrio = Read(lector, "Barrio")
                    Direccion = Read(lector, "Direccion")
                    CodigoPostal = Read(lector, "Codigo_Postal")
                    'Estado = New Estado With {.Codigo = Read(lector, "Estado")}
                    RazonSocial = Read(lector, "Razon_Social")
                    Nombre = Read(lector, "Nombre")
                    PrimeroApellido = Read(lector, "Apellido1")
                    SegundoApellido = Read(lector, "Apellido2")
                    'EstadoTerceros = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESTE_Codigo")}
                    TotalRegistros = Read(lector, "TotalRegistros")
                End If
            End If
            TipoAnticipo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIAN_Codigo")}
            TipoValidacionCupo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIVC_Codigo")}
            Saldo = Read(lector, "Saldo")
            Cupo = Read(lector, "Cupo")
            JustificacionBloqueo = Read(lector, "Justificacion_Bloqueo")
            Estado = New Estado With {.Codigo = Read(lector, "Estado")}
            PorcentajeAfiliado = Read(lector, "Porcentaje_Afiliado")
            CorreoFacturacion = Read(lector, "Correo_Facturacion")
            JustificacionNovedad = Read(lector, "Justificacion_Novedad")
            Novedad = New ValorCatalogos With {.Nombre = Read(lector, "Novedad"), .Codigo = Read(lector, "CATA_NOTE_Codigo")}
            AnalistaCartera = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Analista_Cartera")}
            CodigoCiudadOrigen = Read(lector, "CIUD_Codigo_Origen")
            CodigoCiudadDestino = Read(lector, "CIUD_Codigo_Destino")
        End Sub


        <JsonProperty>
        Public Property ListaNuevosTerceros As IEnumerable(Of Terceros)
        <JsonProperty>
        Public Property strIdentificaciones As String


        <JsonProperty>
        Public Property AnalistaCartera As Terceros

        <JsonProperty>
        Public Property ListaNovedades As IEnumerable(Of NovedadTercero)
        <JsonProperty>
        Public Property Novedad As ValorCatalogos
        <JsonProperty>
        Public Property JustificacionNovedad As String

        <JsonProperty>
        Public Property CorreoFacturacion As String

        <JsonProperty>
        Public Property SoloCondicionesPesoCumplido As Short

        <JsonProperty>
        Public Property AutoComplete As Short
        <JsonProperty>
        Public Property PlantillaDocumento As Short
        <JsonProperty>
        Public Property Codigo As Double
        <JsonProperty>
        Public Property NumeroEstudioSeguridad As Double
        <JsonProperty>
        Public Property CodigoPerfil As Double

        <JsonProperty>
        Public Property CodigoAlterno As String
        <JsonProperty>
        Public Property CodigoContable As Integer
        <JsonProperty>
        Public Property TipoNaturaleza As ValorCatalogos
        <JsonProperty>
        Public Property TipoIdentificacion As ValorCatalogos
        <JsonProperty>
        Public Property Pais As Paises
        <JsonProperty>
        Public Property NumeroIdentificacion As String
        <JsonProperty>
        Public Property DigitoChequeo As Short
        <JsonProperty>
        Public Property RazonSocial As String
        <JsonProperty>
        Public Property RepresentanteLegal As String
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property PrimeroApellido As String
        <JsonProperty>
        Public Property SegundoApellido As String
        <JsonProperty>
        Public Property Sexo As ValorCatalogos
        <JsonProperty>
        Public Property CiudadExpedicionIdentificacion As Ciudades
        <JsonProperty>
        Public Property CiudadExpedicionIdent As Ciudades
        <JsonProperty>
        Public Property CiudadNacimiento As Ciudades
        <JsonProperty>
        Public Property Ciudad As Ciudades
        <JsonProperty>
        Public Property Direccion As String
        <JsonProperty>
        Public Property CodigoPostal As Integer
        <JsonProperty>
        Public Property Telefonos As String
        <JsonProperty>
        Public Property Celular As String
        <JsonProperty>
        Public Property Correo As String
        <JsonProperty>
        Public Property Banco As Bancos
        <JsonProperty>
        Public Property TipoBanco As ValorCatalogos
        <JsonProperty>
        Public Property TipoAnticipo As ValorCatalogos
        <JsonProperty>
        Public Property CuentaBancaria As Long
        <JsonProperty>
        Public Property TitularCuentaBancaria As String
        <JsonProperty>
        Public Property Foto As Byte()
        <JsonProperty>
        Public Property Cliente As Cliente
        <JsonProperty>
        Public Property Conductor As Conductor
        <JsonProperty>
        Public Property Empleado As Empleado
        <JsonProperty>
        Public Property Perfiles As IEnumerable(Of Perfil)

        <JsonProperty>
        Public Property FormasPago As IEnumerable(Of FormasPago)


        Public Property Beneficiario As Tercero
        <JsonProperty>
        Public Property CadenaFormasPago As String
        <JsonProperty>
        Public Property CadenaPerfiles As String
        <JsonProperty>
        Public Property Observaciones As String
        '<JsonProperty>
        'Public Property EstadoTerceros As ValorCatalogos
        <JsonProperty>
        Public Property Estado As Estado
        <JsonProperty>
        Public Property ListadoCorreos As IEnumerable(Of ListasCorreos)
        <JsonProperty>
        Public Property CondicionesPeso As IEnumerable(Of TerceroCondicionesPesoCumplido)
        <JsonProperty>
        Public Property JustificacionBloqueo As String
        <JsonProperty>
        Public Property ReportoContabilidad As Short
        <JsonProperty>
        Public Property CodigoRetornoContabilidad As String
        <JsonProperty>
        Public Property MensajeRetornoContabilidad As String
        <JsonProperty>
        Public Property UsuaCodigoCrea As Integer
        <JsonProperty>
        Public Property Documentos As IEnumerable(Of Documentos)
        <JsonProperty>
        Public Property ListadoFOTOS As IEnumerable(Of Documentos)
        <JsonProperty>
        Public Property Direcciones As IEnumerable(Of TerceroDirecciones)
        <JsonProperty>
        Public Property Proveedor As Proveedor
        <JsonProperty>
        Public Property TarifarioProveedores As Boolean
        <JsonProperty>
        Public Property TarifarioClientes As Boolean
        <JsonProperty>
        Public Property TarifarioPaqueteria As String
        <JsonProperty>
        Public Property NumeroTarifarioCompra As Long
        <JsonProperty>
        Public Property DiasPlazoCobro As Integer
        <JsonProperty>
        Public Property CodigoFormaPago As Integer
        <JsonProperty>
        Public Property CodigoFormaPagoTarifa As Integer
        <JsonProperty>
        Public Property NombreTarifario As String
        <JsonProperty>
        Public Property EstadoTarifario As Integer
        <JsonProperty>
        Public Property Barrio As String
        <JsonProperty>
        Public Property CodigoComercial As Integer
        <JsonProperty>
        Public Property Departamentos As Departamentos
        <JsonProperty>
        Public Property SitiosTerceroCliente As IEnumerable(Of SitiosTerceroCliente)
        <JsonProperty>
        Public Property SitiosTerceroClienteModifica As IEnumerable(Of SitiosTerceroCliente)
        <JsonProperty>
        Public Property TipoValidacionCupo As ValorCatalogos
        <JsonProperty>
        Public Property Saldo As Double
        <JsonProperty>
        Public Property Cupo As Double
        <JsonProperty>
        Public Property LineaServicio As IEnumerable(Of ValorCatalogos)
        <JsonProperty>
        Public Property LineaSerivicioCliente As ValorCatalogos
        <JsonProperty>
        Public Property CentroCosto As String
        <JsonProperty>
        Public Property SitioCargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property SitioDescargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property TerceroClienteCupoSedes As IEnumerable(Of TerceroClienteCupoSedes)
        <JsonProperty>
        Public Property Planitlla As Byte()
        <JsonProperty>
        Public Property PorcentajeAfiliado As Double
        <JsonProperty>
        Public Property ConsultaConductoreslegalizacion As Double
#Region "Tarifa Terceros"

        <JsonProperty>
        Public Property CodigoLineaNegocio As Integer
        <JsonProperty>
        Public Property CodigoTipoLineaNegocio As Integer
        <JsonProperty>
        Public Property TarifaCarga As TarifaTransportes
        <JsonProperty>
        Public Property TipoTarifaCarga As TipoTarifaTransportes
        <JsonProperty>
        Public Property ValorCatalogoTipoTarifa As ValorCatalogos
#End Region
#Region "Tarifa Proveedor"
        <JsonProperty>
        Public Property CodigoRuta As Integer
#End Region
#Region "Tarifa Cliente"
        <JsonProperty>
        Public Property ValorFlete As Double
        <JsonProperty>
        Public Property NumeroTarifarioVenta As Long
        <JsonProperty>
        Public Property CodigoDetalleTarifa As Long
        <JsonProperty>
        Public Property CodigoLineaNegocioTransportes As Integer
        <JsonProperty>
        Public Property CodigoTipoLineaNegocioTransportes As Integer
        <JsonProperty>
        Public Property ValorEscolta As Double
        <JsonProperty>
        Public Property ValorManejo As Double
        <JsonProperty>
        Public Property ValorSeguro As Double
        <JsonProperty>
        Public Property PorcentajeSeguro As Double
        <JsonProperty>
        Public Property ValorOtros1 As Double
        <JsonProperty>
        Public Property ValorOtros2 As Double
        <JsonProperty>
        Public Property ValorOtros3 As Double
        <JsonProperty>
        Public Property ValorOtros4 As Double
        <JsonProperty>
        Public Property CodigoCiudadOrigen As Integer
        <JsonProperty>
        Public Property CodigoCiudadDestino As Integer
        <JsonProperty>
        Public Property Reexpedicion As Integer
        <JsonProperty>
        Public Property PorcentajeReexpedicion As Double
        <JsonProperty>
        Public Property ValorCargue As Double
        <JsonProperty>
        Public Property ValorDescargue As Double
        <JsonProperty>
        Public Property TiempoEntrega As Integer

#End Region

#Region "Filtros de Búsqueda"

        <JsonProperty>
        Public Property CodigoTercero As Long

        <JsonProperty>
        Public Property NombrePasajero As String

        <JsonProperty>
        Public Property ClientesGrupoempresarial As Long

        <JsonProperty>
        Public Property NombreTercero As String

        <JsonProperty>
        Public Property CodigoPerfilTercero As Long

        <JsonProperty>
        Public Property CodigoInicial As Integer

        <JsonProperty>
        Public Property CodigoFinal As Integer

        <JsonProperty>
        Public Property NombreCiudad As String

        <JsonProperty>
        Public Property AplicaConsultaMaster As Integer

        <JsonProperty>
        Public Property NombreCompleto As String


#End Region

        <JsonProperty>
        Public Property PrivadoSitema As Boolean
    End Class

#Region "Terceros Documentos Vencer"
    <JsonObject>
    Public Class TercerosDocumentos
        Inherits Base

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Tercero = New Terceros With {
                .Codigo = Read(lector, "TERC_COdigo"),
                .NombreCompleto = Read(lector, "TERC_Nombre"),
                .NumeroIdentificacion = Read(lector, "TERC_Identificacion"),
                .Celular = Read(lector, "TERC_Celular"),
                .Correo = Read(lector, "TERC_Emails")
            }
            Documento = Read(lector, "Documento")
            FechaVenceDocumento = Read(lector, "Fecha_Vence")
            CantidadDiasFaltantes = Read(lector, "Dias_por_Vencer")
            FechaEmision = Read(lector, "Fecha_Emision")
        End Sub

        <JsonProperty>
        Public Property FechaEmision As DateTime

        <JsonProperty>
        Public Property Codigo As Long
        <JsonProperty>
        Public Property Tercero As Terceros
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property NumeroIdentificacion As String
        <JsonProperty>
        Public Property Documento As String
        <JsonProperty>
        Public Property FechaVenceDocumento As DateTime
        <JsonProperty>
        Public Property CantidadDiasFaltantes As String
        <JsonProperty>
        Public Property TipoAplicativo As Integer
    End Class
#End Region

#Region "Terceros Cliente Cupo Sedes"
    <JsonObject>
    Public Class TerceroClienteCupoSedes
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TerceroClienteCupoSedes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TerceroClienteCupoSedes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        ''' <param name="autocomplete">Indica si la consulta es para cargar un autocomplete</param>
        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Nombre = Read(lector, "Nombre")
            Codigo = Read(lector, "Codigo")
            Cupo = Read(lector, "Cupo")
            Saldo = Read(lector, "Saldo")
        End Sub
        ''' <summary>
        ''' Obtiene o establece el origen de la consulta
        ''' </summary>
        <JsonProperty>
        Public Property Tercero As Tercero
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Codigo As Long
        <JsonProperty>
        Public Property Cupo As Integer
        <JsonProperty>
        Public Property Saldo As Integer
    End Class
#End Region
End Namespace

