﻿Imports EncoExpres.GesCarga.IFachada

Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Repositorio.Comercial.Documentos

Namespace Comercial.Documentos
    Public NotInheritable Class PersistenciaTarifaTransportes
        Implements IPersistenciaBase(Of TarifaTransportes)

        Public Function Consultar(filtro As TarifaTransportes) As IEnumerable(Of TarifaTransportes) Implements IPersistenciaBase(Of TarifaTransportes).Consultar
            Return New RepositorioTarifaTransportes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TarifaTransportes) As Long Implements IPersistenciaBase(Of TarifaTransportes).Insertar
            Return New RepositorioTarifaTransportes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TarifaTransportes) As Long Implements IPersistenciaBase(Of TarifaTransportes).Modificar
            Return New RepositorioTarifaTransportes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TarifaTransportes) As TarifaTransportes Implements IPersistenciaBase(Of TarifaTransportes).Obtener
            Return New RepositorioTarifaTransportes().Obtener(filtro)
        End Function

    End Class

End Namespace

