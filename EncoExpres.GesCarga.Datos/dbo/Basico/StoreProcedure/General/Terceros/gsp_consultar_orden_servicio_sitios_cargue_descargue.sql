﻿PRINT 'gsp_consultar_orden_servicio_sitios_cargue_descargue'
GO
DROP PROCEDURE gsp_consultar_orden_servicio_sitios_cargue_descargue
GO
CREATE PROCEDURE gsp_consultar_orden_servicio_sitios_cargue_descargue 
(@par_EMPR_Codigo SMALLINT,    
@par_TERC_Codigo NUMERIC = NULL,  
@par_CIUD_Codigo NUMERIC = NULL  
)      
AS   
BEGIN   
SELECT     
TSCD.EMPR_Codigo    
,TSCD.TERC_Codigo    
,CONCAT(ISNULL(TERC.Nombre, ''),' ',ISNULL(TERC.Apellido1, ''),' ',ISNULL(TERC.Apellido2, ''),' ',ISNULL(TERC.Razon_Social, '')) AS Nombre_Cliente   
,TSCD.SICD_Codigo  
,ISNULL(SICD.Nombre, '') AS Nombre_Sitio  
,ISNULL(SICD.Direccion, '') AS Direccion_Sitio  
,ISNULL(SICD.Telefono, '') AS Telefono  
,ISNULL(SICD.Contacto, '') AS Contacto  
    
FROM Terceros_Sitos_Cargue_Descargue TSCD     
    
LEFT JOIN Terceros TERC ON    
TSCD.EMPR_Codigo = TERC.EMPR_Codigo  
AND TSCD.TERC_Codigo =  TERC.Codigo  
  
LEFT JOIN Sitios_Cargue_Descargue SICD ON    
TSCD.EMPR_Codigo = SICD.EMPR_Codigo  
AND TSCD.SICD_Codigo = SICD.Codigo  
  
LEFT JOIN Ciudades CIUD ON    
SICD.EMPR_Codigo = CIUD.EMPR_Codigo  
AND SICD.CIUD_Codigo = CIUD.Codigo  
    
WHERE  
TSCD.EMPR_Codigo = SICD.EMPR_Codigo     
AND SICD.EMPR_Codigo = @par_EMPR_Codigo    
AND TERC.Codigo = @par_TERC_Codigo    
AND CIUD.Codigo = @par_CIUD_Codigo      
  
END     
GO