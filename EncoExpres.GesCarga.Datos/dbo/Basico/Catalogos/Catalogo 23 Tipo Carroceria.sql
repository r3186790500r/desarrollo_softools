﻿Print 'Catalogo 23 Tipo Carroceria'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 23
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 23
GO
DELETE Catalogos WHERE Codigo = 23
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,23,'TICA','Tipo Carroceria',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,23,2300,UPPER('(No Aplica)'),1,GETDATE()),
(1,23,2301,UPPER('CAMA BAJA'),1,GETDATE()),
(1,23,2302,UPPER('ESTACAS'),1,GETDATE()),
(1,23,2303,UPPER('FURGÓN'),1,GETDATE()),
(1,23,2304,UPPER('GRUA'),1,GETDATE()),
(1,23,2305,UPPER('NIÑERA'),1,GETDATE()),
(1,23,2306,UPPER('PLANCHON'),1,GETDATE()),
(1,23,2307,UPPER('PORTA CONTENEDOR'),1,GETDATE()),
(1,23,2308,UPPER('REMOLQUE'),1,GETDATE()),
(1,23,2309,UPPER('REPARTO'),1,GETDATE()),
(1,23,2310,UPPER('S.R.S'),1,GETDATE()),
(1,23,2311,UPPER('TOLVA'),1,GETDATE()),
(1,23,2312,UPPER('VOLCO'),1,GETDATE())

GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,23,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,23,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 