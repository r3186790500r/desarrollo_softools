﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="EmpresasController"/>
''' </summary>
Public Class EmpresasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Empresas) As Respuesta(Of IEnumerable(Of Empresas))
        'Dim cache As ObjectCache = MemoryCache.[Default]
        'Dim nombreRecursoEnCache = String.Format(String.Format("ListadoEmpresas"))

        'Dim datosEnMemoria = New List(Of Empresas)
        'datosEnMemoria = cache.Get(nombreRecursoEnCache)

        'If Not IsNothing(datosEnMemoria) Then
        '    If datosEnMemoria.Count > 0 Then
        '        Return New Respuesta(Of IEnumerable(Of Empresas))(datosEnMemoria)
        '    End If
        'End If

        Dim consulta = New LogicaEmpresas(CapaPersistenciaEmpresas).Consultar(filtro)

        'If Not IsNothing(consulta) And Not IsNothing(consulta.Datos) Then
        '    If consulta.Datos.Count > 0 Then
        '        cache.Add(nombreRecursoEnCache, consulta.Datos, PoliticaDeCache)
        '    End If
        'End If

        Return consulta
    End Function

    <HttpPost>
    <ActionName("ConsultarMaster")>
    Public Function ConsultarMaster(ByVal filtro As Empresas) As Respuesta(Of IEnumerable(Of Empresas))
        Return New LogicaEmpresas(CapaPersistenciaEmpresas).ConsultarMaster(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Empresas) As Respuesta(Of Empresas)
        Return New LogicaEmpresas(CapaPersistenciaEmpresas).Obtener(filtro)
    End Function
    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Empresas) As Respuesta(Of Long)
        Return New LogicaEmpresas(CapaPersistenciaEmpresas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("CambioClave")>
    Public Function CambioClave(ByVal filtro As Empresas) As Respuesta(Of IEnumerable(Of Empresas))
        Dim consulta = New LogicaEmpresas(CapaPersistenciaEmpresas).CambioClave(filtro)
        Return consulta
    End Function

End Class
