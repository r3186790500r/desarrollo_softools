﻿Imports Newtonsoft.Json


Namespace ControlTrafico

    ''' <summary>
    ''' Clase <see cref="SeguimientoTransportadoraGPS"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class SeguimientoTransportadoraGPS
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="SeguimientoTransportadoraGPS"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SeguimientoTransportadoraGPS"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = lector.Item("EMPR_Codigo")
            Codigo = lector.Item("Codigo")

            NITTransportadora = lector.Item("Identificacion_Transportadora")
            NombreTransportadora = lector.Item("Nombre_Transportadora")
            VEHIPlaca = lector.Item("VEHI_Placa")
            CedulaConductor = lector.Item("Cedula_Conductor")
            FechaReporte = lector.Item("Fecha_Reporte")
            NombreEvento = lector.Item("Nombre_Evento")
            NumeroSecuencia = lector.Item("Numero_Secuencia")
            Latitud = lector.Item("Latitud")
            Longitud = lector.Item("Longitud")
            Altitud = lector.Item("Altitud")
            VelocidadInstantanea = lector.Item("Velocidad")
            Heading = lector.Item("Heading")
            SatelitesUsados = lector.Item("Satelites_Usados")
            HDOP = lector.Item("HDOP")
            TipoPosicion = lector.Item("Tipo_Posicion")
            Inputs = lector.Item("Inputs")
            CodigoEvento = lector.Item("Codigo_Evento")
            MensajeEvento = lector.Item("Mensaje_Evento")
            DistanciaRecorrida = lector.Item("Distancia_Recorrida")
            TiempoTrabajo = lector.Item("Tiempo_Trabajo")
            TiempoFalta = lector.Item("Tiempo_Falta")
            MagnitudFalta = lector.Item("Magnitud_Falta")



        End Sub

        <JsonProperty>
        Public Property NITTransportadora As String
        <JsonProperty>
        Public Property NombreTransportadora As String
        <JsonProperty>
        Public Property VEHIPlaca As String
        <JsonProperty>
        Public Property CedulaConductor As String
        <JsonProperty>
        Public Property FechaReporte As Date
        <JsonProperty>
        Public Property NombreEvento As String
        <JsonProperty>
        Public Property NumeroSecuencia As Integer
        <JsonProperty>
        Public Property Latitud As Double
        <JsonProperty>
        Public Property Longitud As Double
        <JsonProperty>
        Public Property Altitud As Integer
        <JsonProperty>
        Public Property VelocidadInstantanea As Integer
        <JsonProperty>
        Public Property Heading As Integer
        <JsonProperty>
        Public Property SatelitesUsados As Integer
        <JsonProperty>
        Public Property HDOP As Integer
        <JsonProperty>
        Public Property TipoPosicion As Integer
        <JsonProperty>
        Public Property Inputs As Integer
        <JsonProperty>
        Public Property CodigoEvento As Integer
        <JsonProperty>
        Public Property MensajeEvento As String
        <JsonProperty>
        Public Property DistanciaRecorrida As Int64
        <JsonProperty>
        Public Property TiempoTrabajo As Integer
        <JsonProperty>
        Public Property TiempoFalta As Integer
        <JsonProperty>
        Public Property MagnitudFalta As Integer
        <JsonProperty>
        Public Property CodigoUsuarioCrea As Integer

    End Class

End Namespace
