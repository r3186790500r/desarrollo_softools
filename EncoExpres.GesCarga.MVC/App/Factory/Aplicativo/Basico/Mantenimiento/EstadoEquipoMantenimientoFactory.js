﻿EncoExpresApp.factory('EstadoEquipoMantenimientoFactory', ['$http', function ($http) {

    var urlService = GetUrl('EstadoEquipoMantenimiento');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro;
        return $http(request);
    };

    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro;
        return $http(request);
    };

    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad;
        return $http(request);
    };

    return factory;
}]);