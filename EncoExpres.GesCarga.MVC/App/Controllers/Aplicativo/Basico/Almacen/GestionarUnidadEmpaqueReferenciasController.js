﻿EncoExpresApp.controller("GestionarUnidadEmpaqueReferenciasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'UnidadEmpaqueReferenciasFactory', 'MarcaVehiculosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, UnidadEmpaqueReferenciasFactory, MarcaVehiculosFactory) {

        $scope.Titulo = 'GESTIONAR UNIDAD EMPAQUE';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Almacén' }, { Nombre: 'Unidad Empaque' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_UNIDAD_EMPAQUE);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVA" },
            { Codigo: 1, Nombre: "ACTIVA" },
        ]
       
        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;      
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando unidad empaque código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando unidad empaqueCódigo ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            UnidadEmpaqueReferenciasFactory.Obtener(filtros).
               then(function (response) {
                   if (response.data.ProcesoExitoso === true) {
                       $scope.Modelo = response.data.Datos;
                       $scope.Modelo.Codigo = response.data.Datos.Codigo;
                       $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                   }
                   else {
                       ShowError('No se logro consultar la unidad empaque código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                       document.location.href = '#!ConsultarUnidadEmpaqueReferencias';
                   }
               }, function (response) {
                   ShowError(response.statusText);
                   document.location.href = '#!ConsultarUnidadEmpaqueReferencias';
               });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
                $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                UnidadEmpaqueReferenciasFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó la unidad empaque "' + $scope.Modelo.Nombre + '"');
                                    $scope.Modelo.Codigo = response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó la unidad empaque "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = '#!ConsultarUnidadEmpaqueReferencias/' + $scope.Modelo.Codigo;
                            }                          
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        
        
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
                $scope.MensajesError.push('Debe ingresar el nombre de la unidad empaque');
                continuar = false;
            }
            if ($scope.Modelo.NombreCorto == undefined || $scope.Modelo.NombreCorto == '') {
                $scope.MensajesError.push('Debe ingresar el nombre corto de la unidad empaque');
                continuar = false;
            }
            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarUnidadEmpaqueReferencias/' + $scope.Modelo.Codigo;
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
            try { $scope.Modelo.NombreCorto = $scope.Modelo.NombreCorto.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
        };
}]);