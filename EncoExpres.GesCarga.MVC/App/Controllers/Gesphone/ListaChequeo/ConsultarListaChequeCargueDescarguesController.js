﻿EncoExpresApp.controller("ConsultarListaChequeoCargueDescarguesCtrl", ['$scope', '$timeout', 'EncabezadoCheckListCargueDescarguesFactory', 'TipoDocumentosFactory', '$linq', 'blockUI', '$routeParams', function ($scope, $timeout, EncabezadoCheckListCargueDescarguesFactory, TipoDocumentosFactory, $linq, blockUI, $routeParams) {

    $scope.MapaSitio = [{ Nombre: 'Gesphone' }, { Nombre: 'Lista de Chequeo' }, { Nombre: 'Cargue y Descargue' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

    var filtros = {};
    $scope.cantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.ModalErrorCompleto = ''
    $scope.ModalError = ''
    $scope.MostrarMensajeError = false
    $scope.ListadoEstados = [
        { Nombre: '(TODOS)', Codigo: -1 },
        { Nombre: 'DEFINITIVO', Codigo: 1 },
        { Nombre: 'BORRADOR', Codigo: 0 },
        { Nombre: 'ANULADO', Codigo: 2 }
    ];
    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CHECK_CARGUE_DESCARGUES); } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };


    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarListaChequeoCargueDescargue';
        }
    };

    TipoDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoDocumentos = [];
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo == CODIGO_TIPO_DOCUMENTO_CARGUE || item.Codigo == CODIGO_TIPO_DOCUMENTO_DESCARGUE) {
                            $scope.ListadoTipoDocumentos.push(item)
                        }
                    });
                    $scope.ListadoTipoDocumentos.push({ Nombre: '(Todos)', Codigo: 0 })

                    $scope.Documento = $scope.ListadoTipoDocumentos[$scope.ListadoTipoDocumentos.length - 1];
                }
                else {
                    $scope.ListadoTipoDocumentos = [];
                }
            }
        }, function (response) {
        });

    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = 0
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoCargueDescargue = [];
        if ($scope.Buscando) {
            if ($scope.MensajesError.length == 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: $scope.Documento.Codigo,
                    Placa: $scope.Placa,
                    NumeroDocumento: $scope.NumeroDocumento,
                    Numero: $scope.Numero,
                    FechaInicial: $scope.FechaInicial,
                    FechaFinal: $scope.FechaFinal,
                    Estado: $scope.ModalEstado.Codigo,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                }
                blockUI.delay = 1000;
                EncabezadoCheckListCargueDescarguesFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                response.data.Datos.forEach(function (item) {
                                    var registro = item;
                                    $scope.ListadoCargueDescargue.push(registro);
                                });

                                $scope.ListadoCargueDescargue.forEach(function (item) {
                                    $scope.codigo = item.Codigo;
                                });
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                            var listado = [];
                            response.data.Datos.forEach(function (item) {
                                listado.push(item);
                            });
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        blockUI.stop();
    }

    /* Obtener parametros*/
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }

    $scope.MaskNumero = function () {
        $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
    };

}]);