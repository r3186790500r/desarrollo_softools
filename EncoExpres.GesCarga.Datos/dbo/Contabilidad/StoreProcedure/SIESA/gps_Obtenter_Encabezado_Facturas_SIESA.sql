﻿PRINT 'gps_Obtenter_Encabezado_Facturas_SIESA'
GO
DROP PROCEDURE gps_Obtenter_Encabezado_Facturas_SIESA
GO
CREATE PROCEDURE gps_Obtenter_Encabezado_Facturas_SIESA
(
	@par_EMPR_Codigo smallint
)
AS
BEGIN
	SELECT TOP 5
	ENFA.EMPR_Codigo,
	ENFA.Numero,
	ENFA.Numero_Documento,
	ENFA.Fecha,
	ENFA.TERC_Cliente,
	CLIE.Numero_Identificacion Identificacion_Tercero,
	ENFA.OFIC_Factura,
	ENFA.Observaciones,
	EMPR.Numero_Identificacion Identificacion_Empresa,
	ISNULL(ENFA.Interfaz_Contable_Factura, 0 ) Interfaz_Contable_Factura,
	ISNULL(ENFA.Intentos_Interfase_Contable, 0 ) Intentos_Interfase_Contable

	FROM Encabezado_Facturas ENFA

	INNER JOIN Terceros CLIE ON
	ENFA.EMPR_Codigo = CLIE.EMPR_Codigo
	AND ENFA.TERC_Cliente = CLIE.Codigo

	INNER JOIN Empresas EMPR ON
	ENFA.EMPR_Codigo = EMPR.Codigo

	WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo 
	AND ENFA.CATA_TIFA_Codigo = 5201--Factura Ventas
	AND ISNULL(ENFA.Interfaz_Contable_Factura, 0 ) = 0
	AND ISNULL(ENFA.Intentos_Interfase_Contable, 0 ) < 3

	ORDER BY ENFA.Numero, ENFA.Fecha

END
GO