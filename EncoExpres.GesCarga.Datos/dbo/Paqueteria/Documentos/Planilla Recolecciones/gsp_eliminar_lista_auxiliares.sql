﻿PRINT 'gsp_eliminar_lista_auxiliares'
GO
DROP PROCEDURE gsp_eliminar_lista_auxiliares
GO
CREATE PROCEDURE gsp_eliminar_lista_auxiliares 
(@par_EMPR_Codigo SMALLINT,  
@par_ENPR_Codigo NUMERIC)  
AS
BEGIN  
DELETE Detalle_Auxiliares_Planilla_Recolecciones  
WHERE   
EMPR_Codigo = @par_EMPR_Codigo  
AND ENPR_Numero = @par_ENPR_Codigo  
END  
GO