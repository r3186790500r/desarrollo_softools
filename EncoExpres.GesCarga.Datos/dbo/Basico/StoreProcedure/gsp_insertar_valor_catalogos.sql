﻿PRINT 'gsp_insertar_valor_catalogos'
GO
DROP PROCEDURE gsp_insertar_valor_catalogos
GO 
CREATE PROCEDURE  gsp_insertar_valor_catalogos
(
@par_EMPR_Codigo SMALLINT,
@par_CATA_Codigo SMALLINT,
@par_Codigo NUMERIC,
@par_Campo1 VARCHAR(50),
@par_Campo2 VARCHAR(50) = NULL,
@par_Campo3 VARCHAR(50) = NULL,
@par_Campo4 VARCHAR(50) = NULL,
@par_Campo5 VARCHAR(50) = NULL,
@par_USUA_Codigo_Crea SMALLINT

)
AS 
BEGIN 
	INSERT INTO Valor_Catalogos
	(
	EMPR_Codigo,
	CATA_Codigo,
	Codigo,
	Campo1,
	Campo2,
	Campo3,
	Campo4,
	Campo5,
	USUA_Codigo_Crea,
	Fecha_Crea
	)
	VALUES
	(
	@par_EMPR_Codigo,
	@par_CATA_Codigo,
	@par_Codigo,
	@par_Campo1,
	ISNULL(@par_Campo2,''),
	ISNULL(@par_Campo3,''),
	ISNULL(@par_Campo4,''),
	ISNULL(@par_Campo5,''),
	@par_USUA_Codigo_Crea,
	GETDATE()
	)

SELECT Codigo 
FROM Valor_Catalogos
WHERE 
EMPR_Codigo = @par_EMPR_Codigo
AND Codigo = @par_Codigo

END
GO