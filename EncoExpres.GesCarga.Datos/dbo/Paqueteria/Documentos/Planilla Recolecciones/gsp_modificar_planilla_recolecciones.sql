﻿PRINT 'gsp_modificar_planilla_recolecciones'
GO
DROP PROCEDURE gsp_modificar_planilla_recolecciones
GO
CREATE PROCEDURE gsp_modificar_planilla_recolecciones 
(    
  @par_EMPR_Codigo SMALLINT, 
  @par_Numero NUMERIC,
  @par_TIDO_Codigo NUMERIC = NULL,       
  @par_Numero_Documento NUMERIC = NULL, 
  @par_Fecha DATE = NULL,  
  @par_VEHI_Codigo NUMERIC = NULL, 
  @par_TERC_Codigo_Conductor NUMERIC = NULL, 
  @par_Cantidad_Total NUMERIC = NULL,     
  @par_Peso_Total NUMERIC = NULL,           
  @par_OFIC_Codigo NUMERIC = NULL,    
  @par_Estado SMALLINT, 
  @par_Observaciones VARCHAR(500) = NULL,  
  @par_Numero_Recolecciones VARCHAR(1000) = NULL, --> Funciona como vector para guardar la lista de recolecciones
  @par_Numero_Guias VARCHAR(1000) = NULL, --> Funciona como vector para guardar la lista de guias 
  @par_USUA_Codigo_Modifica SMALLINT    
)    
AS    
BEGIN  

--------------------- RUTINA PARA CONCURRENCIA DE RECOLECCIONES Y GUIAS--------------------------    
    
-- Guardar las recolecciones traidas en una variable tabla    
DECLARE @tblRecolecciones table (Numero NUMERIC)    
INSERT INTO @tblRecolecciones  
SELECT * FROM dbo.Func_Dividir_String (@par_Numero_Recolecciones, ',')    
    
-- De las recolecciones traidas sacar los posibles planillados    
DECLARE @tblRecoleccionesPlanilladas table (Numero NUMERIC, Numero_Documento NUMERIC)    
INSERT INTO @tblRecoleccionesPlanilladas     
SELECT Numero, Numero_Documento FROM Encabezado_Recolecciones WHERE EMPR_Codigo = @par_EMPR_Codigo     
AND Numero IN (SELECT Numero FROM @tblRecolecciones)    
AND ISNULL(ENPR_Numero, 0) > 0
AND ENPR_Numero <>  @par_Numero   
    
-- De las recolecciones traidas sacar las posibles anuladas    
DECLARE @tblRecoleccionesAnuladas table (Numero NUMERIC, Numero_Documento NUMERIC)    
INSERT INTO @tblRecoleccionesAnuladas     
SELECT Numero, Numero_Documento FROM Encabezado_Recolecciones WHERE EMPR_Codigo = @par_EMPR_Codigo     
AND Numero in (SELECT Numero FROM @tblRecolecciones)    
AND Anulado = 1        
    
-- Guardar el listado de recolecciones planilladas en un varchar para armar mensaje    
DECLARE @varRecoleccionesPlanilladas VARCHAR(1000) = ''    
SELECT DISTINCT @varRecoleccionesPlanilladas = ISNULL(CAST((SELECT DISTINCT CONVERT(VARCHAR,Numero_Documento) + ', '     
FROM @tblRecoleccionesPlanilladas FOR XML PATH(''))AS VARCHAR(max)), '')    
FROM @tblRecoleccionesPlanilladas     
    
-- Guardar el listado de recolecciones anuladas en un varchar para armar mensaje    
DECLARE @varRecoleccionesAnuladas VARCHAR(1000) = ''    
SELECT DISTINCT @varRecoleccionesAnuladas = ISNULL(CAST((SELECT DISTINCT CONVERT(VARCHAR,Numero_Documento) + ', '     
FROM @tblRecoleccionesAnuladas FOR XML PATH(''))AS VARCHAR(max)), '')    
FROM @tblRecoleccionesAnuladas     
 
--Guardar las guias traidas en una variable tabla    
DECLARE @tblGuias table (Numero NUMERIC)    
INSERT INTO @tblGuias
SELECT * FROM dbo.Func_Dividir_String (@par_Numero_Guias, ',')    
    
-- De las guias traidas sacar los posibles planillados    
DECLARE @tblGuiasPlanilladas table (Numero NUMERIC, Numero_Documento NUMERIC)    
INSERT INTO @tblGuiasPlanilladas     
SELECT Numero, Numero_Documento FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo     
AND Numero IN (SELECT Numero FROM @tblGuias)    
AND ISNULL(ENPR_Numero, 0) > 0   
AND ENPR_Numero <>  @par_Numero   
    
-- De las guias traidas sacar las posibles anuladas    
DECLARE @tblGuiasAnuladas table (Numero NUMERIC, Numero_Documento NUMERIC)    
INSERT INTO @tblGuiasAnuladas     
SELECT Numero, Numero_Documento FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo     
AND Numero in (SELECT Numero FROM @tblGuias)    
AND Anulado = 1        
    
-- Guardar el listado de guias planilladas en un varchar para armar mensaje    
DECLARE @varGuiasPlanilladas VARCHAR(1000) = ''    
SELECT DISTINCT @varGuiasPlanilladas = ISNULL(CAST((SELECT DISTINCT CONVERT(VARCHAR,Numero_Documento) + ', '     
FROM @tblGuiasPlanilladas FOR XML PATH(''))AS VARCHAR(max)), '')    
FROM @tblGuiasPlanilladas     
    
-- Guardar el listado de guias anuladas en un varchar para armar mensaje    
DECLARE @varGuiasAnuladas VARCHAR(1000) = ''    
SELECT DISTINCT @varGuiasAnuladas = ISNULL(CAST((SELECT DISTINCT CONVERT(VARCHAR,Numero_Documento) + ', '     
FROM @tblGuiasAnuladas FOR XML PATH(''))AS VARCHAR(max)), '')    
FROM @tblGuiasAnuladas  

DECLARE @intContinuar SMALLINT = 1 

DECLARE @varMensajeProceso VARCHAR(MAX) = ''    

--Mensaje para recolecciones  
IF ISNULL(@varRecoleccionesPlanilladas,'') <> '' OR ISNULL(@varRecoleccionesAnuladas,'') <> '' BEGIN    
	 

	 SET @varMensajeProceso += 'La planilla no se puede generar debido a que presenta las siguientes inconsistencias: ' + char(10) + char(10)    
     
	 IF ISNULL(@varRecoleccionesPlanilladas,'') <> '' BEGIN    
	  SET @varMensajeProceso += '-> Recoleccion(s) No. ' + @varRecoleccionesPlanilladas + ' planillada(s).' + char(10)    
	 END    
	 IF ISNULL(@varRecoleccionesAnuladas,'') <> '' BEGIN    
	  SET @varMensajeProceso += '-> Recoleccion(s) No. ' + @varRecoleccionesAnuladas + ' anulada(s).'   
	 END  
	 SET @intContinuar = 0  
END   
 
--Mensaje para Remesa Paqueterìas
    
IF ISNULL(@varGuiasPlanilladas,'') <> '' OR ISNULL(@varGuiasAnuladas,'') <> '' BEGIN    
 
 SET @varMensajeProceso += 'La planilla no se puede generar debido a que las guias presentan las siguientes inconsistencias: ' + char(10) + char(10)    
     
 IF ISNULL(@varGuiasPlanilladas,'') <> '' BEGIN    
  SET @varMensajeProceso += '-> Guia(s) No. ' + @varGuiasPlanilladas + ' planillada(s).' + char(10)    
 END    
 IF ISNULL(@varGuiasAnuladas,'') <> '' BEGIN    
  SET @varMensajeProceso += '-> Guia(s) No. ' + @varGuiasAnuladas + ' anulada(s).'
 END    

	SET @intContinuar = 0
END
    
IF ISNULL(@intContinuar,0) = 0 BEGIN
 
 SELECT 0 As Numero, 0 As Numero_Documento, @varMensajeProceso As Mensaje    
  ------------------------------------- FIN RUTINA CONCURRENCIA -------------------------------------   
 
END    
ELSE    
BEGIN     
   
 UPDATE    
  Encabezado_Planilla_Recolecciones    
 SET    
  EMPR_Codigo = @par_EMPR_Codigo,  
  TIDO_Codigo = @par_TIDO_Codigo,       
  Numero_Documento = @par_Numero_Documento, 
  Fecha = @par_Fecha, 
  VEHI_Codigo = @par_VEHI_Codigo,  
  TERC_Codigo_Conductor = @par_TERC_Codigo_Conductor,    
  Cantidad_Total = @par_Cantidad_Total,      
  Peso_Total = @par_Peso_Total,           
  OFIC_Codigo = @par_OFIC_Codigo, 
  Estado = @par_Estado,  
  Observaciones = @par_Observaciones,
  Fecha_Modifica = GETDATE(),  
  USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica  

 WHERE      
  EMPR_Codigo = @par_EMPR_codigo     
  AND Numero = @par_Numero 

  SELECT Numero, Numero_Documento, '' As Mensaje FROM Encabezado_Planilla_Recolecciones WHERE EMPR_Codigo = @par_EMPR_codigo AND Numero = @par_Numero     

  END
  END     
GO