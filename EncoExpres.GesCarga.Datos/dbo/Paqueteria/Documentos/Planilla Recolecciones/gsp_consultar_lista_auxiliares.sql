﻿PRINT 'gsp_consultar_lista_auxiliares'
GO
DROP PROCEDURE gsp_consultar_lista_auxiliares
GO
CREATE PROCEDURE gsp_consultar_lista_auxiliares  
(@par_EMPR_Codigo SMALLINT,  
@par_ENPR_Codigo NUMERIC)  
  
AS  
BEGIN  
SELECT    
DAPR.EMPR_Codigo  
,DAPR.ENPR_Numero 
,ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1, '')+ ' ' + ISNULL(TERC.Apellido2,'')+ ' ' + ISNULL(TERC.Razon_Social,'') AS Nombre_Funcionario   
,DAPR.TERC_Codigo_Funcionario  
,DAPR.Numero_Horas_Trabajadas  
,DAPR.Valor  
,DAPR.Observaciones  
  
FROM Detalle_Auxiliares_Planilla_Recolecciones DAPR  

LEFT JOIN Terceros TERC ON
DAPR.EMPR_Codigo = TERC.EMPR_Codigo
AND DAPR.TERC_Codigo_Funcionario = TERC.Codigo 
    
WHERE   
DAPR.EMPR_Codigo = @par_EMPR_Codigo  
AND DAPR.ENPR_Numero = @par_ENPR_Codigo  
  
END  
GO