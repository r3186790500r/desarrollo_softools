﻿EncoExpresApp.controller("GestionarCausalesFidelizacionCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUIConfig', 'blockUI', 'TercerosFactory', 'VehiculosFactory', 'ValorCatalogosFactory',
    'DetalleCausalesPuntosVehiculosFactory', 'PuntosVehiculosFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUIConfig, blockUI, TercerosFactory, VehiculosFactory, ValorCatalogosFactory,
        DetalleCausalesPuntosVehiculosFactory, PuntosVehiculosFactory) {

        $scope.Titulo = "NUEVA CAUSALES FIDELIZACIÓN";
        $scope.MapaSitio = [{ Nombre: 'Fidelización' }, { Nombre: 'Documentos' }, { Nombre: 'Causales Fidelización' }, { Nombre: 'Gestionar' }];
        $scope.Master = "#!ConsultarCausalesFidelizacion";
        $scope.DiasVigencia = $scope.Sesion.Empresa.DiasVigenciaPlanPuntos;
        //------------------------------------------------------------DECLARACION VARIABLES------------------------------------------------------------//
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.Deshabilitar = false;
        $scope.DeshabilitarEstado = false;
        $scope.DeshabilitarCategoria = false;
        $scope.MensajesError = [];
        var filtros = {};
        $scope.pref = '';
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CAUSALES_FIDELIZACION); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Vehiculo: "",
            FechaInicioVigencia : new Date(),
            Puntos: "",
        }
        $scope.Codigo = CERO;
        $scope.ListadoCausales = [];
        $scope.ListadoEstados = [
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'VENCIDO', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        //------------------------------------------------------------OBTENER INFORMACION------------------------------------------------------------//
        //-- catalogo Categoria fidelizacion
        var CATA_CPPV_Cargado = false;
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CAUSAL_PLAN_PUNTOS_VEHICULOS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCausales.push({ Nombre: 'SELECCIONE', Codigo: -1 });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoCausales.push({ Codigo: response.data.Datos[i].Codigo, Nombre: response.data.Datos[i].Nombre,  CampoAuxiliar2: response.data.Datos[i].CampoAuxiliar2})
                        }
                        $scope.Modelo.Causal = $linq.Enumerable().From($scope.ListadoCausales).First('$.Codigo == -1');
                        CATA_CPPV_Cargado = true;
                    }
                    else {
                        $scope.ListadoCausales = [];
                        CATA_CPPV_Cargado = true;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
                CATA_CPPV_Cargado = true;
            });

        //--Informacion Tenedero
        $scope.ObtenerInformacionTenedor = function () {
            if ($scope.Modelo.Vehiculo.Tenedor !== undefined) {
                $scope.Modelo.Tenedor = $scope.Modelo.Vehiculo.Tenedor;
            }
            if ($scope.Modelo.Vehiculo.Conductor !== undefined) {
                $scope.Modelo.Conductor = $scope.Modelo.Vehiculo.Conductor;
            }
        }
        //--Asignar Punctos Causal
        $scope.AsignarValorCausal = function () {
            $scope.Modelo.Puntos = $scope.Modelo.Causal.CampoAuxiliar2;
        };
        //--Valida Existencia Fidelizacion Vehiculo
        $scope.ValidarFidelizacionVehiculo = function () {
            if ($scope.Modelo.Vehiculo.Codigo != undefined) {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo },
                    Estado: -1
                };
                PuntosVehiculosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length <= 0) {
                                ShowError("El Vehículo aún no tiene una Fidelización");
                                $scope.Modelo.Vehiculo = "";
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        //--Asignar Fecha Fin Vigencia
        $scope.AsignarFechaFinVigencia = function (FechaIniVigencia) {
            if (FechaIniVigencia != '' && FechaIniVigencia != null && FechaIniVigencia != undefined){
                var DateIniVigencia = new Date(FechaIniVigencia.valueOf());
                var DateFinVigencia = DateIniVigencia.setDate(DateIniVigencia.getDate() + $scope.DiasVigencia);
                var FinalDate = new Date(DateFinVigencia);
                $scope.Modelo.FechaFinVigencia = FinalDate;
            }
        };
        $scope.AsignarFechaFinVigencia($scope.Modelo.FechaInicioVigencia);
        //------------------------------------------------------------FUNCIONES AUTOCOMPLETE------------------------------------------------------------//
        //--Vehiculos
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                }
            }
            return $scope.ListadoVehiculos
        }
        //------------------------------------------------------------MASCARAS------------------------------------------------------------//
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        //------------------------------------------------------------FUNCIONES GENERALES------------------------------------------------------------
        $scope.VolverMaster = function () {
            document.location.href = $scope.Master;
        };

        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };
        //--Funcion Guardar Datos
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            var PlanDetalleCausalPuntosVehiculos = {
                CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                Codigo: $scope.Codigo,
                UsuarioCrea: { Codigo: $scope.Modelo.UsuarioCrea.Codigo },
                UsuarioModifica: { Codigo: $scope.Modelo.UsuarioModifica.Codigo },
                Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo },
                Puntos: $scope.Modelo.Puntos,
                CATA_CPPV: { Codigo: $scope.Modelo.Causal.Codigo },
                FechaInicioVigencia: $scope.Modelo.FechaInicioVigencia,
                FechaFinVigencia: $scope.Modelo.FechaFinVigencia,
                Estado: $scope.Modelo.Estado.Codigo,
                Anulado: CERO
            }
            DetalleCausalesPuntosVehiculosFactory.Guardar(PlanDetalleCausalPuntosVehiculos).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > CERO) {
                            if ($scope.Codigo == CERO) {
                                ShowSuccess('Se guardó las causales correctamente');
                            }
                            else {
                                ShowSuccess('Se modificó las causales correctamente');
                            }
                            location.href = $scope.Master + '/' + response.data.Datos;
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        //--Funcion Guardar Datos
        //--Funcion Obtener Datos
        function Obtener() {
            blockUI.start('Cargando Causales');

            $timeout(function () {
                blockUI.message('Cargando Causales');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };
            blockUI.delay = 1000;

            DetalleCausalesPuntosVehiculosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        $scope.Modelo.Vehiculo = response.data.Datos.Vehiculo;
                        $scope.Modelo.Tenedor = response.data.Datos.Tenedor;
                        $scope.Modelo.Conductor = response.data.Datos.Conductor;
                        $scope.Modelo.Causal = $linq.Enumerable().From($scope.ListadoCausales).First('$.Codigo == ' + response.data.Datos.CATA_CPPV.Codigo);
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        $scope.Modelo.FechaInicioVigencia = new Date(response.data.Datos.FechaInicioVigencia);
                        $scope.Modelo.FechaFinVigencia = new Date(response.data.Datos.FechaFinVigencia);
                        $scope.Modelo.Puntos = response.data.Datos.Puntos;
                        $scope.Deshabilitar = true;
                        $scope.Titulo = "CAUSALES FIDELIZACIÓN";
                    }
                    else {
                        ShowError('No se logro consultar las causales ' + response.data.MensajeOperacion);
                        document.location.href = $scope.Master;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = $scope.Master;
                });
            blockUI.stop();
        }
        //--Funcion Obtener Datos
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo;

            if (Modelo.Vehiculo == undefined || Modelo.Vehiculo == '' || Modelo.Vehiculo == null) {
                $scope.MensajesError.push('Debe ingresar un vehiculo');
                continuar = false;
            }
            if (Modelo.Causal == undefined || Modelo.Causal == '' || Modelo.Causal == null) {
                $scope.MensajesError.push('Debe ingresar causal');
                continuar = false;
            }
            else {
                if (Modelo.Causal.Codigo == -1) {
                    $scope.MensajesError.push('Debe seleccionar una causal');
                    continuar = false;
                }
            }
            if (Modelo.FechaInicioVigencia == undefined || Modelo.FechaInicioVigencia == '' || Modelo.FechaInicioVigencia == null) {
                $scope.MensajesError.push('Debe ingresar fecha inicio vigencia');
                continuar = false;
            }
            else {
                var FechaVigencia = new Date(Modelo.FechaInicioVigencia);
                FechaVigencia.setHours(0, 0, 0, 0);
                var FechaHoy = new Date(Date.now());
                FechaHoy.setHours(0, 0, 0, 0);
                if (FechaVigencia < FechaHoy) {
                    $scope.MensajesError.push('La fecha inicio vigencia debe ser mayor o igual a hoy');
                    continuar = false;
                }
            }

            if (Modelo.FechaFinVigencia == undefined || Modelo.FechaFinVigencia == '' || Modelo.FechaFinVigencia == null) {
                $scope.MensajesError.push('Debe ingresar fecha fin vigencia');
                continuar = false;
            }
            else {
                if (Modelo.FechaInicioVigencia != undefined && Modelo.FechaInicioVigencia != '' && Modelo.FechaInicioVigencia != null) {
                    var FechaIniVigencia = new Date(Modelo.FechaInicioVigencia);
                    FechaIniVigencia.setHours(0, 0, 0, 0);
                    var FechaFinVigencia = new Date(Modelo.FechaFinVigencia);
                    FechaFinVigencia.setHours(0, 0, 0, 0);

                    if (FechaFinVigencia <= FechaIniVigencia) {
                        $scope.MensajesError.push('Fecha fin vigencia debe ser posterior a fecha inicio vigencia');
                        continuar = false;
                    }
                }
            }

            /*if (Modelo.Estado == undefined || Modelo.Estado == '' || Modelo.Estado == null) {
                $scope.MensajesError.push('Debe ingresar estado');
                continuar = false;
            }
            else {
                if (Modelo.Estado.Codigo == -1) {
                    $scope.MensajesError.push('Debe Seleccionar un estado');
                }
            }*/
            return continuar;
        }
        //------------------------------------------------------------PARAMETROS------------------------------------------------------------//
        try {
            if ($routeParams !== undefined && $routeParams.Codigo !== null) {
                if (parseInt($routeParams.Codigo) > CERO) {
                    $scope.Codigo = parseInt($routeParams.Codigo);
                    ValEnviaObtener();
                }
            }
        }
        catch (e) {
            if ($routeParams.Codigo > CERO) {
                $scope.Codigo = $routeParams.Codigo;
                ValEnviaObtener();
            }
        }
        var TimeOutCarga;
        function ValEnviaObtener() {
            if (CATA_CPPV_Cargado == true) {
                clearTimeout(TimeOutCarga);
                Obtener();
            }
            else {
                clearTimeout(TimeOutCarga);
                TimeOutCarga = setTimeout(ValEnviaObtener, 1000);
            }
        }
    }]);