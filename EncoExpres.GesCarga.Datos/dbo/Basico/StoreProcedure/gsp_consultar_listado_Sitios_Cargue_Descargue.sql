﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 02/07/2019
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	
PRINT 'gsp_consultar_listado_Sitios_Cargue_Descargue'
GO
DROP PROCEDURE gsp_consultar_listado_Sitios_Cargue_Descargue
GO     
CREATE PROCEDURE gsp_consultar_listado_Sitios_Cargue_Descargue 
(
@par_Empr_Codigo NUMERIC,      
@par_pais VARCHAR(20) = NULL,      
@par_Ciudad VARCHAR(20) = NULL,  
@par_Estado SMALLINT = NULL,                  
@Par_Tipo_Sitio NUMERIC = NULL       
)      
AS BEGIN       
SELECT       
EMPR.Nombre_Razon_Social      
,SICD.Nombre       
,SICD.Contacto      
,CIUD.Nombre Ciudad      
,SICD.Direccion       
,SICD.Telefono       
,PAIS.Nombre Pais      
,TICD.Campo1 TipoSitio      
FROM Sitios_Cargue_Descargue SICD       
LEFT JOIN Empresas EMPR ON       
EMPR.Codigo = SICD.EMPR_Codigo       
      
LEFT JOIN Paises PAIS ON       
SICD.EMPR_Codigo = PAIS.EMPR_Codigo       
AND SICD.PAIS_Codigo = PAIS.Codigo       
      
LEFT JOIN Ciudades CIUD ON       
SICD.EMPR_Codigo = CIUD.EMPR_Codigo       
AND SICD.CIUD_Codigo = CIUD.Codigo        
      
LEFT JOIN Valor_Catalogos TICD ON       
SICD.EMPR_Codigo = TICD.EMPR_Codigo      
AND SICD.CATA_TSCD_Codigo = TICD.Codigo       
      
WHERE SICD.EMPR_Codigo = @par_Empr_Codigo      
AND (PAIS.Nombre LIKE '%'+@par_pais+'%' or @par_pais is null)    
AND (CIUD.Nombre LIKE '%'+@par_Ciudad+'%' or @par_Ciudad is null)     
AND SICD.CATA_TSCD_Codigo = ISNULL (@Par_Tipo_Sitio,SICD.CATA_TSCD_Codigo) 
AND SICD.Estado = ISNULL(@par_Estado,SICD.Estado)                  
END       
GO