﻿EncoExpresApp.controller("GestionarOrdenCargueCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'OrdenCargueFactory',
    'SemirremolquesFactory', 'CiudadesFactory', 'TercerosFactory', 'ProductoTransportadosFactory', 'ManifiestoFactory', 'RutasFactory', 'OficinasFactory',
    'VehiculosFactory', 'EncabezadoSolicitudOrdenServiciosFactory', 'blockUIConfig', 'SitiosTerceroClienteFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ValorCatalogosFactory, OrdenCargueFactory,
        SemirremolquesFactory, CiudadesFactory, TercerosFactory, ProductoTransportadosFactory, ManifiestoFactory, RutasFactory, OficinasFactory,
        VehiculosFactory, EncabezadoSolicitudOrdenServiciosFactory, blockUIConfig, SitiosTerceroClienteFactory) {
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Documentos' }, { Nombre: 'Orden Cargue' }, { Nombre: 'Gestionar' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ORDEN_CARGUE);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Numero = 0;
        $scope.ModeloFecha = new Date();
        $scope.TEMPORALOCULTARanticipo = false

        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 }
        ];

        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');

        $scope.ListaOrdenServicio = [];
        $scope.ListadoCiudades = [];
        $scope.ListaRemolque = [];
        $scope.ListaCliente = [];
        $scope.ListaRemite = [];
        $scope.ListaConductor = [];
        $scope.ListaCiudadCargue = [];
        $scope.ListaCiudadDescargue = [];
        $scope.ListaProductoTransportado = [];
        $scope.ListaRuta = [];
        $scope.ListaPlaca = [];
        $scope.ListadoOficina = [];

        $scope.ListaCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCliente)
                }
            }
            return $scope.ListaCliente
        }
        $scope.ListaRemite = [];
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_REMITENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaRemite = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRemite)
                }
            }
            return $scope.ListaRemite
        }
        $scope.ListaConductor = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListaConductor = ValidarListadoAutocomplete(Response.Datos, $scope.ListaConductor)
                }
            }
            return $scope.ListaConductor
        }

        /*Cargar el combo de ciudades*/

        $scope.ListaCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListaCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCiudades)
                }
            }
            return $scope.ListaCiudades
        }

        // Productos Transportados
        ProductoTransportadosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: ESTADO_ACTIVO

        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListaProductoTransportado = response.data.Datos;


                        ////if ($scope.CodigoProducto != undefined && $scope.CodigoProducto != '' && $scope.CodigoProducto != 0 && $scope.CodigoProducto != null) {
                        //if ($scope.CodigoProducto > 0) {
                        //    $scope.ModeloProductoTransportado = $linq.Enumerable().From($scope.ListaProductoTransportado).First('$.Codigo ==' + $scope.CodigoProducto);
                        //} else {
                        //    $scope.ModeloProductoTransportado = $linq.Enumerable().From($scope.ListaProductoTransportado).First('$.Codigo ==0');
                        //}
                        ////} else {
                        ////    $scope.ModeloProductoTransportado = $linq.Enumerable().From($scope.ListaProductoTransportado).First('$.Codigo ==0');
                        ////}

                    }
                    else {
                        $scope.ListaProductoTransportado = [];
                        $scope.ModeloProductoTransportado = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        // Ruta
        RutasFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: -1
        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListaRuta = response.data.Datos;


                        if ($scope.CodigoRuta != undefined && $scope.CodigoRuta != '' && $scope.CodigoRuta != 0 && $scope.CodigoRuta != null) {
                            if ($scope.CodigoRuta > 0) {
                                $scope.ModeloRuta = $linq.Enumerable().From($scope.ListaRuta).First('$.Codigo ==' + $scope.CodigoRuta);

                            } else {
                                $scope.ModeloRuta = $linq.Enumerable().From($scope.ListaRuta).First('$.Codigo ==0');
                            }
                        } else {
                            $scope.ModeloRuta = $linq.Enumerable().From($scope.ListaRuta).First('$.Codigo ==0');
                        }

                    }
                    else {
                        $scope.ListaRuta = [];
                        $scope.ModeloRuta = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        // Vehiculo
        $scope.ListaPlaca = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 2) == 0 || value.length == 1) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaPlaca = ValidarListadoAutocomplete(Response.Datos, $scope.ListaPlaca)
                }
            }
            return $scope.ListaPlaca
        }
        // Semirremolques
        SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Placa: '', Codigo: 0 })
                        $scope.ListaRemolque = response.data.Datos;
                        if ($scope.CodigoRemolque != undefined && $scope.CodigoRemolque != '' && $scope.CodigoRemolque != 0 && $scope.CodigoRemolque != null) {
                            if ($scope.CodigoRemolque > 0) {
                                $scope.ModeloRemolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo ==' + $scope.CodigoRemolque);

                            } else {
                                $scope.ModeloRemolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo == 0');
                            }
                        } else {
                            $scope.ModeloRemolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListaRemolque = [];
                        $scope.ModeloRemolque = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //Ordenes de servicio 
        EncabezadoSolicitudOrdenServiciosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Numero: '', NumeroDocumento: '' })
                        $scope.ListaOrdenServicio = response.data.Datos;
                        if ($scope.CodigoOrdenServicio != undefined && $scope.CodigoOrdenServicio != '' && $scope.CodigoOrdenServicio != 0 && $scope.CodigoOrdenServicio != null) {
                            $scope.ModeloOrdenServicio = $linq.Enumerable().From($scope.ListaOrdenServicio).First('$.Numero ==' + $scope.CodigoOrdenServicio);
                        } else {
                            $scope.ModeloOrdenServicio = '';
                        }
                    }
                    else {
                        $scope.ListaOrdenServicio = [];
                        $scope.ModeloOrdenServicio = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar oficina de la ciudad*/
        OficinasFactory.Consultar({ CodigoCiudad: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficina = response.data.Datos;
                    $scope.ModeloOficina = $scope.ListadoOficina[$scope.ListadoOficina.length - 1];
                    if ($scope.CodigoOficina > 0) {
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficina).First('$.Codigo == ' + $scope.CodigoOficina);
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        if ($routeParams.Numero !== undefined) {
            $scope.Numero = $routeParams.Numero;
            if ($scope.Numero > 0) {
                Obtener()
            }
        }
        else {
            $scope.Codigo = 0;
        }

        //Cargar semirremolque de la placa vehiculo
        $scope.CargarSemirremolque = function (modeloplaca) {
            $scope.CodigoVehiculo = modeloplaca.Codigo;
            $scope.ModeloCodigoVehiculo = modeloplaca
            $scope.ModeloPlaca = modeloplaca
            // Vehiculo
            VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.CodigoVehiculo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (!(response.data.Datos.Semirremolque == undefined || response.data.Datos.Semirremolque == '' || response.data.Datos.Semirremolque == null)) {
                            $scope.CodigoRemolqueVehiculo = response.data.Datos.Semirremolque.Codigo;
                            if ($scope.CodigoRemolqueVehiculo > 0) {
                                $scope.ModeloRemolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo == ' + $scope.CodigoRemolqueVehiculo);
                            }                            
                        } else {
                            $scope.ModeloRemolque = null;
                        }
                        
                    } else {
                        $scope.ModeloRemolque = null;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.ListadoSitiosCargueAlterno2 = [];
        $scope.AutocompleteSitiosClienteCargue = function (value, cliente, ciudad) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    $scope.ListadoSitiosCargueAlterno2 = [];
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo;
                    }
                    $scope.ListadoSitiosCargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosCargueAlterno2);
                }
            }
            return $scope.ListadoSitiosCargueAlterno2;
        };

        //-----Asignaciones ---------------------

        $scope.AsignarCodigoAlterno = function (CodigoAlterno) {
            $scope.ModeloCodigoAlterno = CodigoAlterno;
        }
        $('#DocumentoOrdenCargue').show(); $('#Cargue').hide(); $('#Descargue').hide(); $('#Detalle').hide();


        $scope.MostrarOrdenCargue = function () {
            $('#DocumentoOrdenCargue').show()
            $('#Cargue').hide()
            $('#Descargue').hide()
            $('#Detalle').hide()
        }
        $scope.MostrarCargue = function () {
            $('#DocumentoOrdenCargue').hide()
            $('#Cargue').show()
            $('#Descargue').hide()
            $('#Detalle').hide()
        }
        $scope.MostrarDescargue = function () {
            $('#DocumentoOrdenCargue').hide()
            $('#Cargue').hide()
            $('#Descargue').show()
            $('#Detalle').hide()
        }
        $scope.MostrarDetalle = function () {
            $('#DocumentoOrdenCargue').hide()
            $('#Cargue').hide()
            $('#Descargue').hide()
            $('#Detalle').show()

        }
        //----------------------------------------
        if ($scope.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR OFICINA';
            $scope.Deshabilitar = true;
            Obtener();
        }



        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando Orden Cargue...');

            $timeout(function () {
                blockUI.message('Cargando Orden Cargue...');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
            };

            blockUI.delay = 1000;
            OrdenCargueFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ModeloCodigoAlterno = response.data.Datos.CodigoAlterno;
                        $scope.ModeloNumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.ModeloNumeracion = response.data.Datos.Numeracion;
                        $scope.ModeloDireccionCargue = response.data.Datos.DireccionCargue;
                        $scope.ModeloTelefonoCargue = response.data.Datos.TelefonosCargue;
                        $scope.ModeloContactoCargue = response.data.Datos.ContactoCargue;
                        $scope.ModeloDireccionDescargue = response.data.Datos.DireccionDescargue;
                        $scope.ModeloTelefonoDescargue = response.data.Datos.TelefonoDescargue;
                        $scope.ModeloContactoDescargue = response.data.Datos.ContactoDescargue;
                        $scope.ModeloCantidad = $scope.MaskMoneda(response.data.Datos.CantidadCliente);
                        $scope.ModeloPeso = $scope.MaskMoneda(response.data.Datos.PesoCliente);
                        $scope.ModeloValorAnticipo = response.data.Datos.ValorAnticipo;
                        $scope.ModeloObservaciones = response.data.Datos.Observaciones;
                        $scope.ModeloFecha = new Date(response.data.Datos.Fecha);
                        $scope.CodigoOrdenServicio = response.data.Datos.NumeroSolicitudServicio;
                        if ($scope.ListaOrdenServicio.length > 0 && $scope.CodigoOrdenServicio > 0) {
                            $scope.ModeloOrdenServicio = $linq.Enumerable().From($scope.ListaOrdenServicio).First('$.Numero == ' + response.data.Datos.NumeroSolicitudServicio);
                        }
                        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        if (response.data.Datos.Estado == 1) {
                            $scope.DeshabilitarActualizar = true
                        }
                        $scope.ModeloCliente = $scope.CargarTercero(response.data.Datos.TerceroCliente.Codigo)
                        $scope.ModeloRemite = $scope.CargarTercero(response.data.Datos.TerceroRemitente.Codigo)
                        $scope.ModeloConductor = $scope.CargarTercero(response.data.Datos.TerceroConductor.Codigo)
                        $scope.ModeloConductor = $scope.CargarTercero(response.data.Datos.TerceroConductor.Codigo)
                        $scope.ModeloPlaca = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo)
                        $scope.CodigoRemolque = response.data.Datos.Semirremolque.Codigo;
                        if ($scope.ListaRemolque !== null && $scope.ListaRemolque !== undefined) {
                            if ($scope.ListaRemolque.length > 0) {
                                $scope.ModeloRemolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo == ' + response.data.Datos.Semirremolque.Codigo);
                            }
                        };

                        $scope.CodigoOficina = response.data.Datos.Oficina.Codigo
                        if ($scope.ListadoOficina.length > 0 && $scope.CodigoOficina > 0) {
                            try {
                                $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficina).First('$.Codigo == ' + $scope.CodigoOficina);
                            } catch (e) {

                            }
                        }

                        $scope.ModeloManifiestoCarga = response.data.Datos.EncabezadoManifiestoCarga.Numero;

                        $scope.ModeloPlanillasDespachos = response.data.Datos.EncabezadoPlanillaDespacho;
                        $scope.ModeloCiudadCargue = $scope.CargarCiudad(response.data.Datos.CiudadCargue.Codigo);
                        $scope.ModeloFechaCargue = new Date(response.data.Datos.FechaCargue);
                        $scope.ModeloSitioCargue = { SitioCliente: response.data.Datos.SitioCargue };
                        $scope.ModeloCiudadDescargue = $scope.CargarCiudad(response.data.Datos.CiudadDescargue.Codigo);
                        $scope.ModeloFechaDescargue = new Date(response.data.Datos.FechaDescargue);
                        $scope.ModeloSitioDescargue = { SitioCliente: response.data.Datos.SitioDescargue };
                        $scope.CodigoRuta = response.data.Datos.Ruta.Codigo;

                        if ($scope.ListaRuta !== null && $scope.ListaRuta !== undefined) {
                            if ($scope.ListaRuta.length > 0) {
                                $scope.ModeloRuta = $linq.Enumerable().From($scope.ListaRuta).First('$.Codigo == ' + response.data.Datos.Ruta.Codigo);
                            }
                        };

                        $scope.ModeloProductoTransportado = $scope.CargarProducto(response.data.Datos.Producto.Codigo)


                    }
                    else {
                        ShowError('No se logro consultar la Orden de Cargue ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarOrdenCargue';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarOrdenCargue';
                });
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardarOrdenCargue = function () {
            showModal('modalConfirmacionGuardarOrdenCargue');

        };
        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarOrdenCargue');
            if (DatosRequeridos()) {
                $scope.objEnviar = {
                    Numero: $scope.Numero,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numeracion: $scope.ModeloNumeracion,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_ORDEN_CARGUE },
                    NumeroSolicitudServicio: $scope.ModeloOrdenServicio.Numero,
                    Fecha: $scope.ModeloFecha,
                    Estado: $scope.ModeloEstado.Codigo,
                    TerceroCliente: { Codigo: $scope.ModeloCliente.Codigo },
                    TerceroRemitente: { Codigo: $scope.ModeloRemite.Codigo },
                    TerceroConductor: { Codigo: $scope.ModeloConductor.Codigo },

                    Vehiculo: { Codigo: $scope.ModeloPlaca.Codigo },
                    Oficina: { Codigo: $scope.ModeloOficina.Codigo },
                    EncabezadoManifiestoCarga: $scope.ModeloManifiestoCarga,
                    EncabezadoPlanillaDespacho: $scope.ModeloPlanillasDespachos,

                    CiudadCargue: { Codigo: $scope.ModeloCiudadCargue.Codigo },
                    FechaCargue: $scope.ModeloFechaCargue,
                    DireccionCargue: $scope.ModeloDireccionCargue,
                    TelefonosCargue: $scope.ModeloTelefonoCargue,
                    ContactoCargue: $scope.ModeloContactoCargue,

                    CiudadDescargue: { Codigo: $scope.ModeloCiudadDescargue.Codigo },
                    FechaDescargue: $scope.ModeloFechaDescargue,
                    DireccionDescargue: $scope.ModeloDireccionDescargue,
                    TelefonoDescargue: $scope.ModeloTelefonoDescargue,
                    ContactoDescargue: $scope.ModeloContactoDescargue,
                    Semirremolque: { Codigo: $scope.ModeloRemolque.Codigo },

                    Producto: { Codigo: $scope.ModeloProductoTransportado.Codigo },
                    CantidadCliente: $scope.ModeloCantidad,
                    PesoCliente: $scope.ModeloPeso,
                    ValorAnticipo: $scope.ModeloValorAnticipo,
                    Observaciones: $scope.ModeloObservaciones,

                    Ruta: { Codigo: $scope.ModeloRuta.Codigo },

                    CodigoUsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CodigoUsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                };

                if ($scope.ModeloSitioCargue != undefined && $scope.ModeloSitioCargue != null && $scope.ModeloSitioCargue != "") {
                    $scope.objEnviar.SitioCargue = $scope.ModeloSitioCargue.SitioCliente;
                }

                if ($scope.ModeloSitioDescargue != undefined && $scope.ModeloSitioDescargue != null && $scope.ModeloSitioDescargue != "") {
                    $scope.objEnviar.SitioDescargue = $scope.ModeloSitioDescargue.SitioCliente;
                }

                OrdenCargueFactory.Guardar($scope.objEnviar).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Numero == 0) {
                                    ShowSuccess('Se guardó la Orden de Cargue: ' + response.data.Datos);
                                }
                                else {
                                    ShowSuccess('Se modificó la Orden de Cargue: ' + response.data.Datos);
                                }
                                location.href = '#!ConsultarOrdenCargue/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridos() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.ModeloNumeracion == -1 || $scope.ModeloNumeracion == undefined || $scope.ModeloNumeracion == null || $scope.ModeloNumeraciono == "") {  //NO APLICA
                $scope.ModeloNumeracion = 0;
            }
            if ($scope.ModeloFecha == undefined || $scope.ModeloFecha == null || $scope.ModeloFecha == "") {  //NO APLICA
                $scope.MensajesError.push('Debe ingresar una fecha');
                continuar = false;
            }
            if ($scope.ModeloCantidad == -1 || $scope.ModeloCantidad == undefined || $scope.ModeloCantidad == null || $scope.ModeloCantidad == "") {  //NO APLICA
                $scope.MensajesError.push('Debe ingresar un cantidad');
                continuar = false;
            }
            if ($scope.ModeloPeso == -1 || $scope.ModeloPeso == undefined || $scope.ModeloPeso == null || $scope.ModeloPeso == "") {  //NO APLICA
                $scope.MensajesError.push('Debe ingresar un peso');
                continuar = false;
            }
            if ($scope.ModeloValorAnticipo == -1 || $scope.ModeloValorAnticipo == undefined || $scope.ModeloValorAnticipo == null || $scope.ModeloValorAnticipo == "") {  //NO APLICA
                $scope.ModeloValorAnticipo = 0
            }
            if ($scope.ModeloObservaciones == -1 || $scope.ModeloObservaciones == undefined || $scope.ModeloObservaciones == null || $scope.ModeloObservaciones == "") {  //NO APLICA
                $scope.ModeloObservaciones = ' '
            }

            if ($scope.ModeloEstado.Codigo == -1) {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar un Estado');
                continuar = false;
            }


            if ($scope.ModeloOrdenServicio.Numero == -1 || $scope.ModeloOrdenServicio.Numero == undefined || $scope.ModeloOrdenServicio.Numero == null || $scope.ModeloOrdenServicio.Numero == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar una Orden de Servicio');
                continuar = false;
            }
            if ($scope.ModeloCliente.Codigo == -1 || $scope.ModeloCliente.Codigo == undefined || $scope.ModeloCliente.Codigo == null || $scope.ModeloCliente.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar un Cliente');
                continuar = false;
            }
            if ($scope.ModeloRemite.Codigo == -1 || $scope.ModeloRemite.Codigo == undefined || $scope.ModeloRemite.Codigo == null || $scope.ModeloRemite.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar un Remitente');
                continuar = false;
            }
            if ($scope.ModeloConductor.Codigo == -1 || $scope.ModeloConductor.Codigo == undefined || $scope.ModeloConductor.Codigo == null || $scope.ModeloConductor.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar un Coductor');
                continuar = false;
            }
            if ($scope.ModeloPlaca.Codigo == -1 || $scope.ModeloPlaca.Codigo == undefined || $scope.ModeloPlaca.Codigo == null || $scope.ModeloPlaca.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar un Vehiculo');
                continuar = false;
            }
            if ($scope.ModeloOficina.Codigo == -1 || $scope.ModeloOficina.Codigo == undefined || $scope.ModeloOficina.Codigo == null || $scope.ModeloOficina.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar una oficina');
                continuar = false;
            }
            if ($scope.ModeloCiudadCargue.Codigo == -1 || $scope.ModeloCiudadCargue.Codigo == undefined || $scope.ModeloCiudadCargue.Codigo == null || $scope.ModeloCiudadCargue.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar la ciudad de cargue');
                continuar = false;
            }
            if ($scope.ModeloCiudadDescargue.Codigo == -1 || $scope.ModeloCiudadDescargue.Codigo == undefined || $scope.ModeloCiudadDescargue.Codigo == null || $scope.ModeloCiudadDescargue.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar la ciudad de descargue');
                continuar = false;
            }
            if ($scope.ModeloRuta.Codigo == -1 || $scope.ModeloRuta.Codigo == undefined || $scope.ModeloRuta.Codigo == null || $scope.ModeloRuta.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar la ruta');
                continuar = false;
            }
            if ($scope.ModeloProductoTransportado.Codigo == -1 || $scope.ModeloProductoTransportado.Codigo == undefined || $scope.ModeloProductoTransportado.Codigo == null || $scope.ModeloProductoTransportado.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar el producto a transportar');
                continuar = false;
            }

            return continuar;
        }
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);

        };



        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarOrdenCargue/' + $scope.ModeloNumeroDocumento;
            } else {
                document.location.href = '#!ConsultarOrdenCargue';
            }
        };


        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);

        };
        $scope.MaskMoneda = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope);
        }

        $scope.MaskTelefono = function () {
            return MascaraTelefono($scope);
        }
        $scope.MaskDireccion = function () {
            return MascaraDireccion($scope)
        }
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };

        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };

    }]);