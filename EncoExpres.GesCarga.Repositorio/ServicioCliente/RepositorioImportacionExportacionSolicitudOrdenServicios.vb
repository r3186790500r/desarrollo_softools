﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class RepositorioImportacionExportacionSolicitudOrdenServicios
        Inherits RepositorioBase(Of ImportacionExportacionSolicitudOrdenServicios)
        Public Overrides Function Consultar(filtro As ImportacionExportacionSolicitudOrdenServicios) As IEnumerable(Of ImportacionExportacionSolicitudOrdenServicios)
            Dim lista As New List(Of ImportacionExportacionSolicitudOrdenServicios)
            Dim item As ImportacionExportacionSolicitudOrdenServicios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If


                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_grupo_usuarios")
                While resultado.Read
                    item = New ImportacionExportacionSolicitudOrdenServicios(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function



        Public Overrides Function Insertar(entidad As ImportacionExportacionSolicitudOrdenServicios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ImportacionExportacionSolicitudOrdenServicios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ImportacionExportacionSolicitudOrdenServicios) As ImportacionExportacionSolicitudOrdenServicios
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As ImportacionExportacionSolicitudOrdenServicios) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
