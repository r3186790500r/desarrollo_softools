﻿Namespace Facturacion.FacturaElectronicaSaphety
    Public Class TaxSubTotal
        Public Property TaxCategory As String
        Public Property TaxPercentage As String
        Public Property TaxableAmount As String
        Public Property TaxAmount As String


    End Class

    Public Class TaxTotal
        Public Property TaxCategory As String
        Public Property TaxAmount As String
    End Class

    Public Class WithholdingTaxSubTotal
        Public Property WithholdingTaxCategory As String
        Public Property TaxPercentage As String
        Public Property TaxableAmount As String
        Public Property TaxAmount As String
    End Class

    Public Class WithholdingTaxTotal
        Public Property WithholdingTaxCategory As String
        Public Property TaxAmount As String
    End Class

    Public Class PaymentMean
        Public Property Code As String
        Public Property Mean As String
        Public Property DueDate As String
    End Class

    Public Class BillingPeriod
        Public Property From As String

    End Class

    Public Class DocumentContact
        'Public Property Code As String
        Public Property Name As String '
        Public Property Telephone As String '
        'Public Property Telefax As String
        Public Property Email As String '
        'Public Property Note As String
        Public Property Type As String '
    End Class

    Public Class Identification
        Public Property DocumentNumber As String
        Public Property DocumentType As String
        Public Property CountryCode As String
        Public Property CheckDigit As String
    End Class

    Public Class Address

        Public Property DepartmentCode As String
        Public Property CityCode As String

        Public Property AddressLine As String
        Public Property PostalCode As String
        Public Property Country As String
    End Class

    Public Class Person
        Public Property FirstName As String
        Public Property MiddleName As String
        Public Property FamilyName As String
    End Class

    Public Class Item
        Public Property Description As String
    End Class

    Public Class IssuerParty
        Public Property DocumentContacts As List(Of DocumentContact) = New List(Of DocumentContact)
        Public Property Identification As Identification
    End Class

    Public Class CustomerParty

        Public Property DocumentContacts As List(Of DocumentContact) = New List(Of DocumentContact)

        Public Property LegalType As String
        Public Property Identification As Identification
        Public Property Name As String

        Public Property Email As String
        Public Property Address As Address
        Public Property TaxScheme As String
        Public Property Person As Person
        Public Property ResponsabilityTypes As List(Of String)

    End Class

    Public Class Line
        Public Property Number As String
        Public Property Quantity As String
        Public Property QuantityUnitOfMeasure As String
        Public Property TaxSubTotals As List(Of TaxSubTotal) = New List(Of TaxSubTotal)
        Public Property TaxTotals As List(Of TaxTotal) = New List(Of TaxTotal)
        Public Property WithholdingTaxSubTotals As List(Of WithholdingTaxSubTotal) = New List(Of WithholdingTaxSubTotal)
        Public Property WithholdingTaxTotals As List(Of WithholdingTaxTotal) = New List(Of WithholdingTaxTotal)
        Public Property UnitPrice As String
        Public Property GrossAmount As String
        Public Property NetAmount As String
        Public Property Item As Item
    End Class

    Public Class Total
        Public Property GrossAmount As String
        Public Property TotalBillableAmount As String
        Public Property PayableAmount As String
        Public Property TaxableAmount As String
        Public Property AllowancesTotalAmount As String
    End Class

    Public Class AllowanceCharge
        Public Property ChargeIndicator As String
        Public Property BaseAmount As String
        Public Property ReasonCode As String
        Public Property Reason As String
        Public Property Amount As String
        Public Property Percentage As String
        Public Property SequenceIndicator As String
    End Class

    Public Class DocumentReference 'Notas
        Public Property DocumentReferred As String
        Public Property IssueDate As String
        Public Property Type As String
        Public Property OtherReferenceTypeId As String
        Public Property OtherReferenceTypeDescription As String
        Public Property DocumentReferredCUFE As String
    End Class

    Public Class DocumentLineReference 'Notas
        Public Property DocumentReferred As String
        Public Property IssueDate As String
        Public Property Type As String
        Public Property DocumentReferredLineNumber As String
    End Class

    Public Class GasAndOilProperty
        Public Property PropertyName As String
        Public Property PropertyValue As String
    End Class

    Public Class PdfData
        Public Property Pdf As String
    End Class



    Public Class DocumentoFacturaJson
        Public Property TaxSubTotals As List(Of TaxSubTotal) = New List(Of TaxSubTotal)
        Public Property TaxTotals As List(Of TaxTotal) = New List(Of TaxTotal)
        Public Property WithholdingTaxSubTotals As List(Of WithholdingTaxSubTotal) = New List(Of WithholdingTaxSubTotal)
        Public Property WithholdingTaxTotals As List(Of WithholdingTaxTotal) = New List(Of WithholdingTaxTotal)
        Public Property DeliveryDate As String
        Public Property PaymentMeans As List(Of PaymentMean) = New List(Of PaymentMean)
        Public Property IssuerParty As IssuerParty
        Public Property CustomerParty As CustomerParty
        Public Property Currency As String
        Public Property Lines As List(Of Line) = New List(Of Line)
        Public Property Total As Total
        Public Property IssueDate As String
        Public Property DueDate As String
        Public Property SeriePrefix As String
        Public Property SerieNumber As String
        Public Property OperationType As String
        Public Property IssueMode As String
        Public Property CorrelationDocumentId As String
        Public Property SerieExternalKey As String
        Public Property AllowanceCharges As List(Of AllowanceCharge) = New List(Of AllowanceCharge)
        Public Property PdfData As PdfData
    End Class

    Public Class DocumentoNotaJson
        Public Property SerieNumber As String
        Public Property SeriePrefix As String
        Public Property SerieExternalKey As String
        Public Property ReasonDebit As String
        Public Property ReasonCredit As String
        Public Property DeliveryDate As String
        Public Property IssueDate As String
        Public Property DueDate As String
        Public Property Currency As String
        Public Property CorrelationDocumentId As String
        Public Property PaymentMeans As List(Of PaymentMean) = New List(Of PaymentMean)
        Public Property IssuerParty As IssuerParty
        Public Property CustomerParty As CustomerParty
        Public Property Lines As List(Of Line) = New List(Of Line)
        Public Property TaxSubTotals As List(Of TaxSubTotal) = New List(Of TaxSubTotal)
        Public Property TaxTotals As List(Of TaxTotal) = New List(Of TaxTotal)
        Public Property Total As Total
        Public Property DocumentReferences As List(Of DocumentReference) = New List(Of DocumentReference)
        Public Property PdfData As PdfData

    End Class



End Namespace