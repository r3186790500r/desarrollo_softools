﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Masivo
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Contabilidad
Imports System.Transactions

Namespace Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioLiquidaciones"/>
    ''' </summary>
    Public Class RepositorioLiquidaciones
        Inherits RepositorioBase(Of Liquidacion)

        Public Overrides Function Consultar(filtro As Liquidacion) As IEnumerable(Of Liquidacion)
            Dim lista As New List(Of Liquidacion)
            Dim item As Liquidacion

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If filtro.NumeroPlanilla > Cero Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                End If

                If filtro.NumeroManifiesto > Cero Then
                    conexion.AgregarParametroSQL("@par_ENMC_Numero", filtro.NumeroManifiesto)
                End If

                If filtro.PlacaVehiculo <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_VEHI_Placa", filtro.PlacaVehiculo)
                End If
                If Not IsNothing(filtro.TipoDocumento) Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo_Planilla", filtro.TipoDocumento.Codigo)
                End If
                If Not IsNothing(filtro.Tenedor) Then
                    If filtro.Tenedor.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TENE_Codigo", filtro.Tenedor.Codigo)
                    End If
                    If Not IsNothing(filtro.Tenedor.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Tenedor", filtro.Tenedor.Nombre)
                    End If
                End If
                If Not IsNothing(filtro.Conductor) Then
                    If filtro.Conductor.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_COND_Codigo", filtro.Conductor.Codigo)
                    End If
                    If Not IsNothing(filtro.Conductor.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.Nombre)
                    End If
                End If
                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo <> -1 Then 'TODAS
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If
                'If filtro.Estado > -1 Then
                '    If filtro.Estado = 2 Then 'ANULADO
                '        conexion.AgregarParametroSQL("@par_Anulado", 1)
                '    Else
                '        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                '        conexion.AgregarParametroSQL("@par_Anulado", 0)
                '    End If

                'End If
                If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If

                If filtro.Aprobado <> -1 Then 'TODOS
                    conexion.AgregarParametroSQL("@par_Aprobado", filtro.Aprobado)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_liquidacion_planilla_despachos")

                While resultado.Read
                    item = New Liquidacion(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Liquidacion) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha_Entrega", entidad.FechaEntrega, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.NumeroPlanilla)
                    conexion.AgregarParametroSQL("@par_ENMC_Numero", entidad.NumeroManifiesto)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador", entidad.ValorFleteTranportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Conceptos_Liquidacion", entidad.ValorConceptosLiquidacion, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Cvt", entidad.ValorCvt, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Base_Impuestos", entidad.ValorBaseImpuestos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.ValorImpuestos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Pagar", entidad.ValorPagar, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_Peso_Cumplido", entidad.PesoCumplido, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso_Cargue", entidad.PesoCargue, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso_Faltante", entidad.PesoFaltante, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_liquidacion_planilla_despachos")
                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString
                        entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                    End While
                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    If entidad.Numero > Cero Then
                        If Not IsNothing(entidad.ListaDetalleLiquidacion) Then
                            inserto = InsertarDetalle(entidad, conexion)
                        End If
                        If Not IsNothing(entidad.ListaImpuestosConceptosLiquidacion) Then
                            inserto = InsertarDetalleImpuestos(entidad, conexion)
                        End If
                    End If

                    If entidad.Estado = SI_APLICA Then
                        If Not IsNothing(entidad.CuentaPorPagar) Then
                            SaldarDocumentosCuentasLiquidacion(entidad, conexion)

                            entidad.CuentaPorPagar.Numero = entidad.NumeroDocumento
                            inserto = Generar_CxP(entidad.CuentaPorPagar, conexion)
                        End If
                        Dim rep As New RepositorioEncabezadoComprobantesContables
                        Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Numero, 160, conexion, resultado)
                    End If
                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                    entidad.NumeroDocumento = Cero
                End If
            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As Liquidacion) As Long
            Dim modifico As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador", entidad.ValorFleteTranportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Conceptos_Liquidacion", entidad.ValorConceptosLiquidacion, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Cvt", entidad.ValorCvt, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Base_Impuestos", entidad.ValorBaseImpuestos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.ValorImpuestos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Pagar", entidad.ValorPagar, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                    conexion.AgregarParametroSQL("@par_Peso_Cargue", entidad.PesoCargue, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso_Cumplido", entidad.PesoCumplido, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso_Faltante", entidad.PesoFaltante, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_liquidacion_planilla_despachos")
                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString
                    End While
                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    If entidad.Numero > Cero Then
                        If Not IsNothing(entidad.ListaDetalleLiquidacion) Then
                            modifico = ModificarDetalle(entidad, conexion)
                        End If
                    End If

                    If entidad.Estado = 1 Then


                        If Not IsNothing(entidad.CuentaPorPagar) Then
                            SaldarDocumentosCuentasLiquidacion(entidad, conexion)
                            entidad.CuentaPorPagar.Numero = entidad.NumeroDocumento
                            modifico = Generar_CxP(entidad.CuentaPorPagar, conexion)
                        End If
                        Dim rep As New RepositorioEncabezadoComprobantesContables
                        Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Numero, 160, conexion, resultado)
                    End If

                End Using

                If modifico Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As Liquidacion) As Liquidacion
            Dim item As New Liquidacion

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_liquidacion_planilla_despacho")

                While resultado.Read
                    item = New Liquidacion(resultado)
                End While

                If item.Numero > 0 Then
                    item.ListaDetalleLiquidacion = ObtenerDetalle(item)
                    item.ListaImpuestosConceptosLiquidacion = RetornarImpuestoConceptosLiquidacion(item)
                End If

            End Using
            Return item
        End Function

        Public Function ObtenerDetalle(filtro As Liquidacion) As IEnumerable(Of DetalleLiquidacion)
            Dim lista As New List(Of DetalleLiquidacion)
            Dim item As DetalleLiquidacion

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ELPD_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_liquidacion_planilla_despacho")

                While resultado.Read
                    item = New DetalleLiquidacion(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function

        Public Function Anular(entidad As Liquidacion) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnula)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_encabezado_liquidacion_despacho_planilla")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While
                resultado.Close()
                'llamado de sp gsp_generar_movimiento_contable_anulacion 
                'Dim rep As New RepositorioEncabezadoComprobantesContables()
                'rep.GenerarMovimientoContableAnulacion(entidad.CodigoEmpresa, entidad.Numero, entidad.TipoDocumento.Codigo, conexion, resultado)

            End Using

            Return anulo
        End Function

        Public Function ObtenerPlanillaDespacho(filtro As Liquidacion) As PlanillaDespachos
            Dim item As New PlanillaDespachos
            Dim Conceptos As New Liquidacion

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento.Codigo)
                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If
                If Not IsNothing(filtro.NumeroManifiesto) Then
                    conexion.AgregarParametroSQL("@par_Numero_Manifiesto", filtro.NumeroManifiesto)

                End If
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_planilla_despachos")

                While resultado.Read
                    filtro.Numero = resultado.Item("Numero")
                    item = New PlanillaDespachos(resultado)
                End While

                'JV: Se cierra el reader actual para utlizar un nuevo reader dentro de la misma conexión para insertar el detalle
                resultado.Close()
                Dim DetalleRemesa As Remesas
                Dim Detalle As DetallePlanillas
                Dim ListaDetalle As New List(Of DetallePlanillas)
                conexion.CleanParameters()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_remesas_planilla]")

                While resultado.Read
                    DetalleRemesa = New Remesas(resultado)
                    Detalle = New DetallePlanillas With {.Remesa = DetalleRemesa}
                    ListaDetalle.Add(Detalle)
                End While
                item.Detalles = ListaDetalle
                'conexion.CloseConnection()
                resultado.Close()


                If item.Numero > 0 Then
                    filtro.NumeroDocumento = item.NumeroDocumento
                    filtro.Tenedor = New Entidades.Basico.General.Terceros With {.Codigo = item.Tenedor.Codigo}
                    item.ListaConceptosLiquidacion = RetornarConceptos(filtro, conexion)
                    filtro.CodigoRuta = item.Ruta.Codigo
                    item.ListaImpuestosConceptosLiquidacion = RetornarImpuestoConceptos(filtro)
                End If
                conexion.CloseConnection()
                resultado.Close()
            End Using

            Return item
        End Function

        Public Function InsertarDetalle(filtro As Liquidacion, ByVal Conexion As Conexion.DataBaseFactory) As Boolean
            Dim lista As New List(Of DetalleLiquidacion)
            Dim item As DetalleLiquidacion
            Dim resultado As IDataReader

            If Not IsNothing(filtro.ListaDetalleLiquidacion) Then
                For Each item In filtro.ListaDetalleLiquidacion
                    Conexion.CleanParameters()
                    Conexion.AgregarParametroSQL("@par_EMPR_Codigo", item.CodigoEmpresa)
                    Conexion.AgregarParametroSQL("@par_ELPD_Numero", filtro.Numero)
                    Conexion.AgregarParametroSQL("@par_CLPD_Codigo", item.ConceptoLiquidacion.Codigo)
                    Conexion.AgregarParametroSQL("@par_ENDC_Codigo", item.DocumentoCuentaCodigo)
                    Conexion.AgregarParametroSQL("@par_Observaciones", item.Observaciones)
                    Conexion.AgregarParametroSQL("@par_Valor", item.Valor)

                    resultado = Conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_liquidacion_planilla_despachos")

                    While resultado.Read
                        InsertarDetalle = IIf(resultado.Item("ID") > 0, True, False)
                    End While
                    resultado.Close()
                Next
            End If

            Return InsertarDetalle
        End Function
        Public Function InsertarDetalleImpuestos(filtro As Liquidacion, ByVal Conexion As Conexion.DataBaseFactory) As Boolean
            Dim resultado As IDataReader
            Dim InsertarDetalle As Boolean = False
            If Not IsNothing(filtro.ListaImpuestosConceptosLiquidacion) Then
                For Each item In filtro.ListaImpuestosConceptosLiquidacion
                    Conexion.CleanParameters()
                    Conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    Conexion.AgregarParametroSQL("@par_ELPD_Numero", filtro.Numero)
                    Conexion.AgregarParametroSQL("@par_ENIM_Codigo", item.Codigo)
                    Conexion.AgregarParametroSQL("@par_Valor_Base", item.Valor_base, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Valor_Tarifa", item.Valor_tarifa, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Valor_Impuesto", item.ValorImpuesto)


                    resultado = Conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_impuesto_liquidacion_planilla_despachos")

                    While resultado.Read
                        InsertarDetalle = IIf(resultado.Item("ID") > 0, True, False)
                    End While
                    resultado.Close()
                Next
            End If

            Return InsertarDetalle
        End Function
        Public Function ModificarDetalle(filtro As Liquidacion, ByVal Conexion As Conexion.DataBaseFactory) As Boolean
            ModificarDetalle = True
            Dim lista As New List(Of DetalleLiquidacion)
            Dim item As DetalleLiquidacion
            Dim resultado As IDataReader

            If Not IsNothing(filtro.ListaDetalleLiquidacion) Then
                For Each item In filtro.ListaDetalleLiquidacion
                    Conexion.CleanParameters()
                    Conexion.AgregarParametroSQL("@par_EMPR_Codigo", item.CodigoEmpresa)
                    Conexion.AgregarParametroSQL("@par_ELPD_Numero", filtro.Numero)
                    Conexion.AgregarParametroSQL("@par_CLPD_Codigo", item.ConceptoLiquidacion.Codigo)
                    Conexion.AgregarParametroSQL("@par_Observaciones", item.Observaciones)
                    Conexion.AgregarParametroSQL("@par_Valor", item.Valor)

                    resultado = Conexion.ExecuteReaderStoreProcedure("gsp_modificar_detalle_liquidacion_planilla_despachos")

                    While resultado.Read
                        ModificarDetalle = IIf(resultado.Item("ID") > 0, True, False)
                    End While
                    resultado.Close()
                Next
            End If

            Return ModificarDetalle
        End Function

        Public Function RetornarConceptos(filtro As Liquidacion, ByVal Conexion As Conexion.DataBaseFactory) As IEnumerable(Of DetalleLiquidacion)
            Dim lista As New List(Of DetalleLiquidacion)
            Dim item As DetalleLiquidacion
            Dim repImp = New RepositorioConceptoLiquidacion()
            Conexion.CleanParameters()
            Conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
            If filtro.Numero > 0 Then
                Conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)
            End If
            Conexion.AgregarParametroSQL("@par_ELPD_Numero", filtro.NumeroDocumento)
            Conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento.Codigo)
            If Not IsNothing(filtro.Tenedor) Then
                If filtro.Tenedor.Codigo > 0 Then
                    Conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Tenedor.Codigo)
                End If
            End If

            Dim resultado As IDataReader = Conexion.ExecuteReaderStoreProcedure("gsp_insertar_conceptos_detalle_liquidacion")

            While resultado.Read
                item = New DetalleLiquidacion(resultado)
                item.ConceptoLiquidacion.Pagina = 1
                item.ConceptoLiquidacion.RegistrosPagina = 1000
                item.ConceptoLiquidacion.CodigoEmpresa = filtro.CodigoEmpresa
                item.ConceptoLiquidacion.ListadoImpuestos = repImp.Obtener_Detalle(item.ConceptoLiquidacion)
                lista.Add(item)
            End While

            Return lista
        End Function

        Public Function RetornarImpuestoConceptos(filtro As Liquidacion) As IEnumerable(Of ImpuestoConceptoLiquidacionPlanillaDespacho)
            Dim lista As New List(Of ImpuestoConceptoLiquidacionPlanillaDespacho)
            Dim item As ImpuestoConceptoLiquidacionPlanillaDespacho

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)
                End If
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", filtro.CodigoRuta)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_impuestos_conceptos_liquidacion_planilla_Despachos")

                While resultado.Read
                    item = New ImpuestoConceptoLiquidacionPlanillaDespacho(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function
        Public Function RetornarImpuestoConceptosLiquidacion(filtro As Liquidacion) As IEnumerable(Of ImpuestoConceptoLiquidacionPlanillaDespacho)
            Dim lista As New List(Of ImpuestoConceptoLiquidacionPlanillaDespacho)
            Dim item As ImpuestoConceptoLiquidacionPlanillaDespacho

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ELPD_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_impuestos_conceptos_liquidacion_planilla_Despachos")

                While resultado.Read
                    item = New ImpuestoConceptoLiquidacionPlanillaDespacho(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function

        Public Function SaldarDocumentosCuentasLiquidacion(entidad As Liquidacion, conexion As DataBaseFactory) As Boolean
            Dim CodigoDocumentoOrigenLiquidacion = 2602
            If entidad.TipoDocumento.Codigo = 165 Then 'LiquidacionPaqueteria
                CodigoDocumentoOrigenLiquidacion = 2614
            End If
            Dim modifico As Boolean
            conexion.CleanParameters()




            If Not IsNothing(entidad.ListaDetalleLiquidacion) Then
                For Each item In entidad.ListaDetalleLiquidacion
                    conexion.CleanParameters()
                    If item.ConceptoLiquidacion.Codigo = 10000 Or item.ConceptoLiquidacion.Codigo = 10009 Then 'Cuenta x Cobrar Saldo Liquidación/CxC PlanillaAnulada

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", item.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", IIf(item.ConceptoLiquidacion.Codigo = 10009, 2604, CodigoDocumentoOrigenLiquidacion))
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.CuentaPorPagar.Tercero.Codigo)
                        conexion.AgregarParametroSQL("@par_Numero_Documento_Origen", item.ConceptoLiquidacion.DocumentoCuenta.NumeroDocumento)
                        conexion.AgregarParametroSQL("@par_Valor", item.Valor, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                        conexion.AgregarParametroSQL("@par_ENDC_Codigo", item.DocumentoCuentaCodigo)
                        conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo_Liquidacion", CodigoDocumentoOrigenLiquidacion)
                        conexion.AgregarParametroSQL("@par_Numero_Documento_Liquidacion", entidad.NumeroDocumento)
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_documentos_cuentas_liquidacion")

                        While resultado.Read
                            modifico = IIf(resultado.Item("Codigo") > 0, True, False)
                        End While
                        resultado.Close()
                    End If


                Next
            End If

            Return modifico
        End Function

        Public Function Generar_CxP(entidad As EncabezadoDocumentoCuentas, conexion As DataBaseFactory) As Boolean
            Generar_CxP = False
            Const intTipoDocumentoCuentaporPagar As Integer = 85
            Const intTipoDocumentoCuentaporCobrar As Integer = 80
            'Se quema las observaciones, ya que cuando el documento se guarda de una vez en definitivo,
            'en el controller.js no se tiene aún el número documento generado de la liquidación, es decir, en el proceso de insertar
            If entidad.TipoDocumento = intTipoDocumentoCuentaporPagar Then
                entidad.Observaciones = "CUENTA POR PAGAR LIQUIDACIÓN PLANILLA No. " & entidad.Numero & ", VALOR $ " & entidad.ValorTotal
            ElseIf entidad.TipoDocumento = intTipoDocumentoCuentaporCobrar Then
                If entidad.ValorTotal < 0 Then
                    entidad.ValorTotal = entidad.ValorTotal * -1
                    entidad.Saldo = entidad.Saldo * -1
                End If
                entidad.Observaciones = "CUENTA POR COBRAR LIQUIDACIÓN PLANILLA No. " & entidad.Numero & ", VALOR $ " & entidad.ValorTotal

            End If

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", 160)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
            conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
            conexion.AgregarParametroSQL("@par_Codigo_Documento_Origen", entidad.CodigoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.Numero)
            conexion.AgregarParametroSQL("@par_Fecha_Documento_Origen", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Fecha_Vence_Documento_Origen", DateAdd(DateInterval.Day, 30, entidad.Fecha), SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPuc.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            conexion.AgregarParametroSQL("@par_Abono", entidad.Abono)
            conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)
            conexion.AgregarParametroSQL("@par_Fecha_Cancelacion_Pago", entidad.FechaCancelacionPago, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Aprobado", entidad.Aprobado)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Aprobo", entidad.UsuarioAprobo.Codigo)
            conexion.AgregarParametroSQL("@par_Fecha_Aprobo", entidad.FechaAprobo, SqlDbType.DateTime)
            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_cuentas")

            While resultado.Read
                Generar_CxP = IIf(resultado.Item("Codigo") > 0, True, False)
            End While
            resultado.Close()

            Return Generar_CxP
        End Function

        Public Function Rechazar(entidad As Liquidacion) As Boolean
            Dim rechazo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_Causa_Rechazo", entidad.CausaRechazo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_rechazar_planilla_liquidacion_despacho")

                While resultado.Read
                    rechazo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return rechazo
        End Function

        Public Function Aprobar(entidad As Liquidacion) As Boolean
            Dim apruebo As Boolean = False

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Aprueba", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_aprobar_planilla_liquidacion_despacho")

                    While resultado.Read
                        apruebo = IIf((resultado.Item("Numero")) > 0, True, False)
                    End While
                    resultado.Close()

                    If Generar_CxP(entidad.CuentaPorPagar, conexion) Then
                        transaccion.Complete()
                    End If
                End Using
            End Using

            Return apruebo
        End Function

    End Class

End Namespace