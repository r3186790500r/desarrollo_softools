﻿Imports Newtonsoft.Json
Namespace Facturacion
    <JsonObject>
    Public NotInheritable Class DetalleImpuestosConceptosFacturas
        Inherits BaseDetalleDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroEncabezado = Read(lector, "ENFA_Numero")
            DECF_Codigo = Read(lector, "DECF_Codigo")
            ENIM_Codigo = Read(lector, "ENIM_Codigo")
            Valor_tarifa = Read(lector, "Valor_Tarifa")
            Valor_base = Read(lector, "Valor_Base")
            Valor_impuesto = Read(lector, "Valor_Impuesto")
        End Sub
        <JsonProperty>
        Public Property DECF_Codigo As Integer
        <JsonProperty>
        Public Property ENIM_Codigo As Integer
        <JsonProperty>
        Public Property Valor_tarifa As Double
        <JsonProperty>
        Public Property Valor_base As Long
        <JsonProperty>
        Public Property Valor_impuesto As Long
    End Class
End Namespace
