﻿EncoExpresApp.controller("GestionarPlanillaDespachoCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TarifarioComprasFactory', 'TarifaTransportesFactory',
    'TipoTarifaTransportesFactory', 'RutasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'TipoLineaNegocioTransportesFactory', 'TarifaTransportesFactory',
    'ProductoTransportadosFactory', 'CiudadesFactory', 'RemesasFactory', 'VehiculosFactory', 'PlanillaDespachosFactory', 'ImpuestosTipoDocumentosFactory', 'ImpuestosFactory',
    'OficinasFactory', 'SemirremolquesFactory', 'ManifiestoFactory', 'blockUIConfig',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TarifarioComprasFactory, TarifaTransportesFactory, TipoTarifaTransportesFactory, RutasFactory, ValorCatalogosFactory,
        TercerosFactory, TipoLineaNegocioTransportesFactory, TarifaTransportesFactory, ProductoTransportadosFactory, CiudadesFactory, RemesasFactory, VehiculosFactory, PlanillaDespachosFactory,
        ImpuestosTipoDocumentosFactory, ImpuestosFactory, OficinasFactory, SemirremolquesFactory, ManifiestoFactory, blockUIConfig) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'GESTIONAR PLANILLA DESPACHOS';
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Documentos' }, { Nombre: 'Planilla Despacho' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PLANILLA_DESPACHOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.AplicaImpuestos = false
        $scope.ListaFormaPago = [
            { Nombre: 'Otros', Codigo: 1 },
            { Nombre: 'Transferecia', Codigo: 2 }
        ]
        $scope.FormaPagoRecaudo = $scope.ListaFormaPago[0]
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Planilla: {
                Numero: 0,
                Fecha: new Date(),
                FechaSalida: new Date(),
                DetalleTarifarioCompra: {},
                AnticipoPagadoA: 'Conductor'
            },
            TipoDocumento: 150,
            Remesa: { Cliente: {}, Ruta: {} },
        }
        $scope.Planilla = {}

        $scope.ListaAuxiliares = [];
        $scope.ListadoImpuestosFiltrado = [];
        $scope.Filtro = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoOficina: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: CERO,
            Remesa: {
                DetalleTarifarioVenta: {},
                Remitente: { Direccion: '' },
                Detalle: { Destinatario: { Direccion: '' }, }
            },
            EstadoRemesaPaqueteria: { Codigo: 6001 }

        }

        $scope.Filtro2 = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoOficina: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: CERO,
            Remesa: {
                DetalleTarifarioVenta: {},
                Remitente: { Direccion: '' },
                Detalle: { Destinatario: { Direccion: '' }, }
            },
            EstadoRemesaPaqueteria: { Codigo: 6001 }

        }
        $scope.DetallesAuxiliares = []
        $scope.ListadoEstados = [
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR },
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO }
        ];
        $scope.Modelo.Planilla.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_BORRADOR);

        $scope.ListadoTarifas = [];
        $scope.ListadoVehiculos = [];
        $scope.ListadoCliente = [];
        $scope.ListadoConductores = [];
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        $scope.AplicaVariosConductores = false;
        if ($scope.Sesion.UsuarioAutenticado.ManejoHabilitarVariosConductoresPlanillaDespachos) {
            $scope.AplicaVariosConductores = true;
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete
        //--Vehiculos
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                }
            }
            return $scope.ListadoVehiculos

        };
        //--Clientes
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //--Conductores
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }
        //----------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //--Rutas
            $scope.ListadoRutas = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos
            //--Oficina
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoOficina = response.data.Datos;
                        $scope.ModeloOficina = $scope.ListadoOficina[$scope.ListadoOficina.length - 1];
                        try {
                            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficina).First('$.Codigo == ' + $scope.Modelo.Planilla.Oficina.Codigo);
                        } catch (e) {
                            try {
                                $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficina).First('$.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                            } catch (e) {
                                $scope.ModeloOficina = $scope.ListadoOficina[$scope.ListadoOficina.length - 1];
                            }
                        }

                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            //--Semirremolques
            SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaRemolque = response.data.Datos;
                            try {
                                if ($scope.Modelo.Planilla.Semirremolque.Codigo > CERO) {
                                    $scope.Modelo.Planilla.Semirremolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo ==' + $scope.Modelo.Planilla.Semirremolque.Codigo);
                                }
                            } catch (e) {
                                $scope.Modelo.Planilla.Semirremolque = ''
                            }
                        }
                        else {
                            $scope.ListaRemolque = [];
                            $scope.ModeloRemolque = null;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            /*Cargar el combo de funcionarios*/
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, CadenaPerfiles: PERFIL_EMPLEADO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                            $scope.ListaFuncionario = response.data.Datos;
                            if ($scope.CodigoFuncionario !== undefined && $scope.CodigoFuncionario !== '' && $scope.CodigoFuncionario !== 0 && $scope.CodigoFuncionario !== null) {
                                if ($scope.CodigoFuncionario > 0) {
                                    $scope.ModeloFuncionario = $linq.Enumerable().From($scope.ListaFuncionario).First('$.Codigo ==' + $scope.CodigoFuncionario);
                                } else {
                                    $scope.ModeloFuncionario = $linq.Enumerable().From($scope.ListaFuncionario).First('$.Codigo ==0');
                                }
                            } else {
                                $scope.ModeloFuncionario = $linq.Enumerable().From($scope.ListaFuncionario).First('$.Codigo ==0');
                            }
                        }
                        else {
                            $scope.ListaFuncionario = [];
                            $scope.ModeloFuncionario = null;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });



            /*Cargar el combo de tipo entrega*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo transporte*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_TRANSPORTE_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTransporteRemsaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoTransporteRemsaPaqueteria = $scope.ListadoTipoTransporteRemsaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo despacho*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DESPACHO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoDespachoRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoDespachoRemesaPaqueteria = $scope.ListadoTipoDespachoRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de tipo servicio*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SERVICIO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoServicioRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoServicioRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoServicioRemesaPaqueteria = $scope.ListadoTipoServicioRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoServicioRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });

            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoProductoTransportados = []
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoProductoTransportados = response.data.Datos;
                        }
                        else {
                            $scope.ListadoProductoTransportados = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/



            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCliente = []
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoCliente = response.data.Datos;
                        }
                        else {
                            $scope.ListadoCliente = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_EMPLEADO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoEmpleados = []
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoEmpleados = response.data.Datos;
                            $scope.DetallesAuxiliares = []
                            if ($scope.Modelo.Planilla.DetallesAuxiliares !== undefined && $scope.Modelo.Planilla.DetallesAuxiliares !== null) {
                                $scope.DetallesAuxiliares = $scope.Modelo.Planilla.DetallesAuxiliares
                                for (var i = 0; i < $scope.DetallesAuxiliares.length; i++) {
                                    $scope.DetallesAuxiliares[i].Funcionario = $linq.Enumerable().From($scope.ListadoEmpleados).First('$.Codigo == ' + $scope.DetallesAuxiliares[i].Funcionario.Codigo);
                                }
                            }
                        }
                        else {
                            $scope.ListadoEmpleados = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCiudades = response.data.Datos;
                        }
                        else {
                            $scope.ListadoCiudades = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            /*Cargar el combo de TipoLineaNegocioTransportes*/
            TipoLineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoLineaNegocioTransportes = [];
                        $scope.ListadoTipoLineaNegocioTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoLineaNegocioTransportesInicial = response.data.Datos;
                            for (var i = 0; i < $scope.ListadoTipoLineaNegocioTransportesInicial.length; i++) {
                                if ($scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte !== undefined && $scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte !== null) {
                                    if ($scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA) {
                                        $scope.ListadoTipoLineaNegocioTransportes.push($scope.ListadoTipoLineaNegocioTransportesInicial[i])
                                    }
                                }
                            }
                            $scope.ListadoTipoLineaNegocioTransportesInicial = angular.copy($scope.ListadoTipoLineaNegocioTransportes)
                            $scope.ListadoTipoLineaNegocioTransportes = []
                        }
                        else {
                            $scope.ListadoTipoLineaNegocioTransportes = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TarifaTransportes*/
            TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTarifaTransportes = [];
                        $scope.ListadoTarifaTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTarifaTransportesInicial = response.data.Datos;
                        }
                        else {
                            $scope.ListadoTarifaTransportesInicial = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TipoTarifaTransportes*/
            TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTarifaTransportes = [];
                        $scope.ListadoTipoTarifaTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoTarifaTransportesInicial = response.data.Datos;
                        }
                        else {
                            $scope.ListadoTipoTarifaTransportesInicial = []
                        }
                    }
                }, function (response) {
                });
            //--Obtener
            try {
                var Parametros = $routeParams.Codigo.split(',')
                if (Parametros.length > 1) {
                    $scope.Modelo.Numero = Parametros[CERO];
                    Obtener();
                } else {
                    if ($routeParams.Codigo > CERO) {
                        $scope.Modelo.Numero = $routeParams.Codigo;
                        Obtener();
                    }
                    else {
                        $scope.Modelo.Numero = 0;
                    }

                }
            } catch (e) {
                if ($routeParams.Codigo > CERO) {
                    $scope.Modelo.Numero = $routeParams.Codigo;
                    Obtener();
                }
                else {
                    $scope.Modelo.Numero = 0;
                }
            }

        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //ComboOficinaDespacha
        $scope.CargarOficinaDespacha = function () {
            $scope.ListaOficinasDespacha = []
            var Response = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true });
            $scope.ListaOficinasDespacha = ValidarListadoAutocomplete(Response.Datos, $scope.ListaOficinasDespacha)
            try {
                $scope.Filtro.Oficina = $linq.Enumerable().From($scope.ListaOficinasDespacha).First('$.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);

            } catch (e) {
                $scope.Filtro.Oficina = $scope.ListaOficinasDespacha[0]

            }
        }
        $scope.CargarOficinaDespacha();
        $scope.CargarImpuestos = function (Ruta) {
            if (Ruta.Codigo > 0 && Ruta !== undefined && Ruta !== null && Ruta !== "") {
                if (Ruta.Codigo > 0) {
                    /*Cargar los impuestos*/
                    ImpuestosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, CodigoTipoDocumento: 150, CodigoCiudad: Ruta.CiudadOrigen.Codigo, AplicaTipoDocumento: 1 }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.ListadoImpuestos = []
                                $scope.ListadoImpuestosFiltrado = []
                                if (response.data.Datos.length > CERO) {
                                    $scope.ListadoImpuestos = response.data.Datos;
                                    try {
                                        if ($scope.Modelo.Planilla.DetalleImpuesto !== undefined && $scope.Modelo.Planilla.DetalleImpuesto !== null && $scope.Modelo.Planilla.DetalleImpuesto !== []) {
                                            $scope.ListadoImpuestosFiltrado = $scope.Modelo.Planilla.DetalleImpuesto
                                        }
                                        else {
                                            $scope.ListadoImpuestosFiltrado = []
                                            if ($scope.ListadoImpuestos.length > 0) {
                                                for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                                                    var impuesto = {
                                                        Nombre: $scope.ListadoImpuestos[i].Nombre,
                                                        CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                                                        ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                                                        ValorBase: $scope.ListadoImpuestos[i].valor_base,
                                                        ValorImpuesto: 0
                                                    }
                                                    $scope.ListadoImpuestosFiltrado.push(impuesto)
                                                }
                                            }
                                        }
                                    } catch (e) {
                                        $scope.ListadoImpuestosFiltrado = []
                                        if ($scope.ListadoImpuestos.length > 0) {
                                            for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                                                var impuesto = {
                                                    Nombre: $scope.ListadoImpuestos[i].Nombre,
                                                    CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                                                    ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                                                    ValorBase: $scope.ListadoImpuestos[i].valor_base,
                                                    ValorImpuesto: 0
                                                }
                                                $scope.ListadoImpuestosFiltrado.push(impuesto)
                                                //$scope.Calcular()
                                            }
                                        }
                                    }


                                }
                                else {
                                    $scope.ListadoImpuestos = [];
                                    $scope.ListadoImpuestosFiltrado = [];
                                }
                            }
                            try {
                                $scope.Calcular(0, true)

                            } catch (e) {

                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        }
        $scope.FiltrarTarifas = function (Ruta) {
            if (Ruta !== undefined && Ruta !== null && Ruta !== "" && $scope.Sesion.UsuarioAutenticado.ManejoTarifasProgramacion == false) {
                if (Ruta.Codigo > 0) {
                    $scope.ListaTarifas = [];
                    $scope.ListadoTipoTarifas = [];
                    for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                        var item = $scope.ListadoTarifas[i]
                        if (item.Estado.Codigo == 1 && (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO || item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO)) {
                            if (Ruta.Codigo == item.Ruta.Codigo) {
                                if ($scope.ListaTarifas.length == 0) {
                                    $scope.ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte)
                                }
                                else {
                                    var cont = 0
                                    for (var j = 0; j < $scope.ListaTarifas.length; j++) {
                                        if ($scope.ListaTarifas[j].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                            cont++
                                        }
                                    }
                                    if (cont == 0) {
                                        $scope.ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte)
                                    }
                                }
                                item.Codigo = item.TipoTarifaTransportes.Codigo
                                $scope.ListadoTipoTarifas.push(item)
                            }
                        }
                    }
                    if ($scope.ListaTarifas.length == 0 && $scope.ListadoTipoTarifas == 0) {
                        ShowError('La ruta ingresada no se encuentra asociada al tarifario de compras del tenedor ' + $scope.Modelo.Planilla.Vehiculo.Tenedor.NombreCompleto);
                    } else {
                        try {
                            $scope.Modelo.Planilla.TarifaTransportes = $linq.Enumerable().From($scope.ListaTarifas).First('$.Codigo ==' + $scope.Modelo.Planilla.TarifaTransportes.Codigo);
                            $scope.FiltrarTipoTarifa()
                        } catch (e) {
                            try {
                                $scope.Modelo.Planilla.TarifaTransportes = $scope.ListaTarifas[0]
                                $scope.FiltrarTipoTarifa()
                            } catch (e) {

                            }

                        }
                    }
                }
            }
        }
        $scope.FiltrarTipoTarifa = function () {
            $scope.ListadoTipoTarifa = []
            for (var i = 0; i < $scope.ListadoTipoTarifas.length; i++) {
                var item = $scope.ListadoTipoTarifas[i]
                if ($scope.Modelo.Planilla.TarifaTransportes.Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                    if ($scope.Modelo.Planilla.TarifaTransportes.Codigo == 301) {
                        item.NombreTipoTarifa = $scope.Modelo.Planilla.TarifaTransportes.Nombre + ' (' + item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Nombre + ', ' + item.TipoLineaNegocioTransportes.Nombre + ')'
                    }
                    $scope.ListadoTipoTarifa.push(item)
                }
            }
            try {
                $scope.TipoTarifaTransportes = $linq.Enumerable().From($scope.ListadoTipoTarifa).First('$.Codigo ==' + $scope.Modelo.Planilla.TipoTarifaTransportes.Codigo);
                //$scope.Modelo.Planilla.TipoTarifaTransportes = undefined
            } catch (e) {
                $scope.TipoTarifaTransportes = $scope.ListadoTipoTarifa[0]
            }
            try {
                var continuar = true
                try {
                    if ($scope.Modelo.Planilla.NumeroDocumento > 0) {
                        continuar = false
                    } else {
                        continuar = true
                    }
                } catch (e) {
                    continuar = true
                }
                if (continuar) {
                    $scope.Calcular(1)
                } else {
                    $scope.Calcular(0, true)
                }
            } catch (e) {
                $scope.Calcular(1)
            }

        }
        /*Consultar Tarifario Tenedor*/
        $scope.ObtenerInformacionTenedor = function (Vehi) {
            try {
                $scope.Modelo.Planilla.Semirremolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Placa =="' + Vehi.Semirremolque.Placa + '"');

            } catch (e) {

            }

            var response = TercerosFactory.Obtener({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Planilla.Vehiculo.Tenedor.Codigo,
                Sync: true
            })

                    if (response.ProcesoExitoso === true) {
                        $scope.Modelo.Planilla.Vehiculo.Tenedor.NombreCompleto = response.Datos.NombreCompleto
                        $scope.Modelo.Planilla.Vehiculo.Tenedor.NumeroIdentificacion = response.Datos.NumeroIdentificacion

                    }

                    if ($scope.Sesion.UsuarioAutenticado.ManejoTarifasProgramacion) {
                        $scope.LustadoRutasFiltrado = []
                        for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                            $scope.LustadoRutasFiltrado.push($scope.ListadoRutas[i])
                        }
                        try {
                            $scope.Modelo.Planilla.Peso = $scope.pesotemp
                            $scope.Modelo.Planilla.Ruta = $linq.Enumerable().From($scope.LustadoRutasFiltrado).First('$.Codigo ==' + $scope.Modelo.Planilla.Ruta.Codigo);
                            $scope.CargarImpuestos($scope.Modelo.Planilla.Ruta)
                        } catch (e) {

                        }
                    } else {
                        if ($scope.Modelo.Planilla.Vehiculo.Tenedor !== undefined) {
                            if ($scope.Modelo.Planilla.Vehiculo.Tenedor.Codigo > 0) {
                                $scope.Modelo.Planilla.Vehiculo.Tenedor.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa

                                if (response.ProcesoExitoso === true) {
                                    if (response.Datos.Codigo > 0) {
                                        if (response.Datos.Proveedor != undefined) {
                                            if (response.Datos.Proveedor.Tarifario !== undefined) {
                                                if (response.Datos.Proveedor.Tarifario.Codigo > 0) {
                                                    $scope.Planilla.TarifarioCompra = response.Datos.Proveedor.Tarifario
                                                    $scope.ObtenerInformacionTarifario()
                                                    if (!$scope.esObtener) {
                                                        if (response.Datos.TipoAnticipo.Codigo == 18702) {
                                                            $scope.Modelo.Planilla.AnticipoPagadoA = 'Tenedor'
                                                            $scope.AplicAnticipoTenedor = true
                                                        } else {
                                                            $scope.Modelo.Planilla.AnticipoPagadoA = 'Conductor'
                                                            $scope.AplicAnticipoTenedor = false
                                                        }
                                                    }
                                                }
                                                else {
                                                    ShowError('El tenedor del vehículo seleccionado no posee un tarifario de compra asignado');
                                                    $scope.Planilla.TarifarioCompra = {}
                                                }
                                            }
                                            else {
                                                ShowError('El tenedor del vehículo seleccionado no posee un tarifario de compra asignado');
                                                $scope.Planilla.TarifarioCompra = {}
                                            }
                                        }
                                        else {
                                            ShowError('El tenedor del vehículo seleccionado no posee un tarifario de compra asignado');
                                            $scope.Planilla.TarifarioCompra = {}
                                        }
                                    } else {
                                        ShowError('Error al consultar la información del tarifario');
                                        $scope.Planilla.TarifarioCompra = {}
                                    }
                            
                                }
                            }
                        }
                    }
            if (Vehi !== '' && Vehi !== undefined && Vehi !== null) {

                if (Vehi.Conductor !== '' && Vehi.Conductor !== undefined && Vehi.Conductor !== null && Vehi.Conductor.Codigo !== 0 && Vehi.Conductor.Codigo !== undefined) {
                    if ($scope.ListaConductores.length < 4) {
                        var concidencias = 0
                        if ($scope.ListaConductores.length > 0) {
                            for (var i = 0; i < $scope.ListaConductores.length; i++) {
                                if (Vehi.Conductor.Codigo === $scope.ListaConductores[i].Codigo) {
                                    concidencias++
                                    break;
                                }
                            }
                            if (concidencias == 0) {
                                $scope.ListaConductores.push($scope.CargarTercero(Vehi.Conductor.Codigo));
                            }
                        } else {
                            $scope.ListaConductores.push($scope.CargarTercero(Vehi.Conductor.Codigo));
                        }
                    }
                }
            }
        }
        $scope.ObtenerInformacionTarifario = function () {
            $scope.TarifarioValido = false
            $scope.ListadoTarifaTransportes = []
            $scope.ListadoTipoTarifaTransportes = []
            $scope.Planilla.DetalleTarifarioCompra = { TipoLineaNegocioTransportes: '' }
            if ($scope.Planilla.TarifarioCompra !== undefined) {
                if ($scope.Planilla.TarifarioCompra.Codigo > 0) {
                    $scope.Planilla.TarifarioCompra.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                    TarifarioComprasFactory.Obtener($scope.Planilla.TarifarioCompra).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Planilla.TarifarioCompra = response.data.Datos
                                $scope.ListadoTarifas = [];
                                $scope.LustadoRutasFiltrado = []
                                for (var i = 0; i < $scope.Planilla.TarifarioCompra.Tarifas.length; i++) {
                                    if ($scope.Planilla.TarifarioCompra.Tarifas[i].Estado.Codigo == 1 && $scope.Planilla.TarifarioCompra.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == 1 || $scope.Planilla.TarifarioCompra.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == 2) {
                                        $scope.ListadoTarifas.push($scope.Planilla.TarifarioCompra.Tarifas[i])
                                    }
                                }
                                for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                                    var aplica = 0
                                    for (var j = 0; j < $scope.ListadoTarifas.length; j++) {
                                        if ($scope.ListadoRutas[i].Codigo == $scope.ListadoTarifas[j].Ruta.Codigo) {
                                            aplica++
                                        }
                                    }
                                    if (aplica > 0) {
                                        $scope.LustadoRutasFiltrado.push($scope.ListadoRutas[i])
                                    }
                                }
                                try {
                                    if ($scope.Modelo.Planilla.Ruta.Codigo > CERO) {
                                        $scope.Modelo.Planilla.Ruta = $linq.Enumerable().From($scope.LustadoRutasFiltrado).First('$.Codigo ==' + $scope.Modelo.Planilla.Ruta.Codigo);
                                        if ($scope.Modelo.Planilla.Estado.Codigo == 0 && $scope.Modelo.Planilla.NumeroDocumento > 0) {
                                            $scope.ValidarAnticipoPlanilla($scope.Modelo.Planilla.ValorAnticipo)
                                        }
                                        $scope.CargarImpuestos($scope.Modelo.Planilla.Ruta)
                                        $scope.FiltrarTarifas($scope.Modelo.Planilla.Ruta)
                                        $scope.FiltrarTarifas($scope.Modelo.Planilla.Ruta)
                                    }
                                } catch (e) {
                                    $scope.Modelo.Planilla.Ruta = ''
                                }
                            }
                            else {
                                ShowError('No se logro consultar el tarifario compras');
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });

                    blockUI.stop();
                }
            }
        }
        //Calcular valores
        $scope.Calcular = function (RecalculaAnticipo, CambioFlete) {
            var continuar = true
            try {
                if ($scope.Modelo.Planilla.NumeroDocumento > 0 && $scope.Modelo.Planilla.Estado.Codigo == 1) {
                    continuar = false
                } else {
                    continuar = true

                }
            } catch (e) {
                continuar = true
            }
            $scope.Modelo.Planilla.Cantidad = 0
            $scope.Modelo.Planilla.Peso = 0
            $scope.Modelo.Planilla.ValorRetencionFuente = 0
            $scope.Modelo.Planilla.ValorRetencionICA = 0
            $scope.Modelo.Planilla.ValorFleteCliente = 0
            $scope.Modelo.Planilla.ValorSeguroMercancia = 0
            $scope.Modelo.Planilla.ValorOtrosCobros = 0
            $scope.Modelo.Planilla.ValorTotalCredito = 0
            $scope.Modelo.Planilla.ValorTotalContado = 0
            $scope.Modelo.Planilla.ValorTotalAlcobro = 0
            try {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    var item = $scope.ListadoRemesasFiltradas[i]
                    if (item.Seleccionado) {
                        $scope.Modelo.Planilla.Cantidad += item.CantidadCliente
                        $scope.Modelo.Planilla.Peso += item.PesoCliente

                    }
                }
            } catch (e) {

            }

            if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== []) {
                for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                    var item = $scope.ListadoGuiaGuardadas[i]
                    $scope.Modelo.Planilla.Cantidad += item.Remesa.CantidadCliente
                    $scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente
                }
            }
            if (continuar && !CambioFlete) {
                $scope.Modelo.Planilla.ValorFleteTransportador = 0
                if ($scope.Sesion.UsuarioAutenticado.ManejoTarifasProgramacion) {
                    try {
                        for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                            var item = $scope.ListadoRemesasFiltradas[i]
                            if (item.Seleccionado) {
                                $scope.Modelo.Planilla.ValorFleteTransportador += item.TotalFleteTransportador

                            }
                        }
                    } catch (e) {

                    }

                    if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== []) {
                        for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                            var item = $scope.ListadoGuiaGuardadas[i]
                            $scope.Modelo.Planilla.ValorFleteTransportador += item.TotalFleteTransportador
                        }
                    }

                }
                else {
                    try {
                        if ($scope.Modelo.Planilla.TarifaTransportes.Codigo == 6 || $scope.Modelo.Planilla.TarifaTransportes.Codigo == 300 || $scope.Modelo.Planilla.TarifaTransportes.Codigo == 302 || $scope.Modelo.Planilla.TarifaTransportes.Codigo == 5) {
                            $scope.Modelo.Planilla.ValorFleteTransportador = Math.round($scope.TipoTarifaTransportes.ValorFlete * $scope.Modelo.Planilla.Cantidad)
                        }
                        if ($scope.Modelo.Planilla.TarifaTransportes.Codigo == 301) {
                            $scope.Modelo.Planilla.ValorFleteTransportador = Math.round($scope.TipoTarifaTransportes.ValorFlete * ($scope.Modelo.Planilla.Peso / 1000))
                        }
                        else {
                            $scope.Modelo.Planilla.ValorFleteTransportador = $scope.TipoTarifaTransportes.ValorFlete
                        }

                    } catch (e) {

                    }
                    try {
                        if ($scope.Modelo.Planilla.TarifaTransportes.Codigo == 5) {
                            $scope.Modelo.Planilla.ValorFleteTransportador = Math.round(MascaraNumero($scope.TipoTarifaTransportes.ValorFlete) * (MascaraNumero($scope.Modelo.Planilla.Peso) / 1000))
                        }
                    } catch (e) {

                    }
                }
                $scope.ModalTotalFleteTransportadorTEMP = $scope.Modelo.Planilla.ValorFleteTransportador
            }
            if (RecalculaAnticipo > 0) {
                if ($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo != undefined) {
                    $scope.Modelo.Planilla.ValorAnticipo = MascaraValores(Math.round((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))));
                }
                $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorFleteTransportador) - $scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorAnticipo));
            } else {
                if ($scope.Modelo.Planilla.ValorAnticipo !== undefined && $scope.Modelo.Planilla.ValorAnticipo !== '') {
                    if ($scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorAnticipo) > $scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorFleteTransportador)) {
                        ShowError('El valor del anticipo no puede ser mayor al valor del flete del transportador')
                        $scope.Modelo.Planilla.ValorPagarTransportador = 0
                        $scope.AutorizacionAnticipo = 0
                    } else {
                        $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorFleteTransportador) - $scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorAnticipo))
                    }
                } else {
                    if ($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo != undefined) {
                        $scope.Modelo.Planilla.ValorAnticipo = MascaraValores(Math.round((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))));
                    }
                    $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorFleteTransportador) - $scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorAnticipo));
                    //$scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorFleteTransportador)
                }
            }
            try {
                $scope.ListadoImpuestosFiltrado = []
                if ($scope.ListadoImpuestos.length > 0) {
                    for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                        var impuesto = {
                            Nombre: $scope.ListadoImpuestos[i].Nombre,
                            CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                            ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                            ValorBase: $scope.ListadoImpuestos[i].valor_base,
                        }
                        if ($scope.ListadoImpuestos[i].valor_base < parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))) {
                            impuesto.ValorBase = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))
                        }

                        impuesto.ValorImpuesto = Math.round(impuesto.ValorTarifa * impuesto.ValorBase)
                        $scope.ListadoImpuestosFiltrado.push(impuesto)
                    }
                }
            } catch (e) {

            }
            try {
                $scope.Modelo.Planilla.ValorImpuestos = 0;
                if ($scope.ListadoImpuestosFiltrado.length > 0) {
                    for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                        $scope.Modelo.Planilla.ValorImpuestos += parseFloat($scope.ListadoImpuestosFiltrado[i].ValorImpuesto);
                    }
                }
                $scope.Modelo.Planilla.ValorPagarTransportador = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorPagarTransportador)) - $scope.Modelo.Planilla.ValorImpuestos;
            } catch (e) {

            }

            $scope.Modelo.Planilla.ValorRetencionFuente = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorRetencionFuente)
            $scope.Modelo.Planilla.ValorRetencionICA = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorRetencionICA)
            $scope.Modelo.Planilla.ValorFleteCliente = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorFleteCliente)
            $scope.Modelo.Planilla.ValorSeguroMercancia = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorSeguroMercancia)
            $scope.Modelo.Planilla.ValorOtrosCobros = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorOtrosCobros)
            $scope.Modelo.Planilla.ValorTotalCredito = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalCredito)
            $scope.Modelo.Planilla.ValorTotalContado = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalContado)
            $scope.Modelo.Planilla.ValorTotalAlcobro = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalAlcobro)
            $scope.Modelo.Planilla.ValorFleteTransportador = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorFleteTransportador)
            $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorPagarTransportador)
            $scope.Modelo.ValorImpuestos = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorImpuestos)
            //CalcularImpuesto();
            $scope.MaskValores();

        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        $scope.ModeloEstado = { Codigo: 1, Nombre: 'DEFINITIVO' };
        $scope.Modelo.FechaFinal = new Date();
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesErrorConslta = [];
            $scope.ListadoGuias = [];
            if (DatosRequeridosConsultaRemesas()) {
                if ($scope.MensajesErrorConslta.length == 0) {

                    blockUI.delay = 1000;
                    $scope.Filtro.Pagina = 0
                    $scope.Filtro.RegistrosPagina = 0
                    //$scope.Modelo.RegistrosPagina = 10
                    $scope.Filtro.NumeroPlanilla = -1
                    $scope.Filtro.TipoDocumento = CODIGO_TIPO_DOCUMENTO_REMESAS;
                    if ($scope.ModeloEstado !== undefined && $scope.ModeloEstado !== null) {
                        $scope.Filtro.Estado = $scope.ModeloEstado.Codigo;
                    } else {
                        $scope.Filtro.Estado = -1;
                    }
                    $scope.Filtro.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                    RemesasFactory.ConsultarRemesasPlanillaDespachos($scope.Filtro).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Modelo.Codigo = 0
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoRemesas = response.data.Datos
                                    $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                    $scope.PrimerPagina();
                                    $('#btncriterios').click()
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                $scope.ListadoOriginalRemesas = $scope.ListadoRemesas;
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.ListadoGuias = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                }
            } else {
                $scope.ListadoGuias = $scope.ListadoRemesasFiltradas
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.ListadoGuias = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.ListadoGuias = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.ListadoGuias = []
                for (var i = a - 10; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                }
            }
        }

        function DatosRequeridosConsultaRemesas() {
            $scope.MensajesErrorConslta = [];
            var modelo = $scope.Modelo
            var continuar = true;
            if ((modelo.FechaInicial == null || modelo.FechaInicial == undefined || modelo.FechaInicial == '')
                && (modelo.FechaFinal == null || modelo.FechaFinal == undefined || modelo.FechaFinal == '')
                && (modelo.NumeroInicial == null || modelo.NumeroInicial == undefined || modelo.NumeroInicial == '' || modelo.NumeroInicial == 0)
                && (modelo.NumeroFinal == null || modelo.NumeroFinal == undefined || modelo.NumeroFinal == '' || modelo.NumeroFinal == 0)
            ) {
                $scope.MensajesErrorConslta.push('Debe ingresar los filtros de números o fechas');
                continuar = false

            } else if ((modelo.NumeroInicial !== null && modelo.NumeroInicial !== undefined && modelo.NumeroInicial !== '' && modelo.NumeroInicial !== 0)
                || (modelo.NumeroFinal !== null && modelo.NumeroFinal !== undefined && modelo.NumeroFinal !== '' && modelo.NumeroFinal !== 0)
                || (modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')
                || (modelo.FechaFinal !== null && modelo.FechaFinal !== undefined && modelo.FechaFinal !== '')

            ) {
                if ((modelo.NumeroInicial !== null && modelo.NumeroInicial !== undefined && modelo.NumeroInicial !== '' && modelo.NumeroInicial !== 0)
                    && (modelo.NumeroFinal !== null && modelo.NumeroFinal !== undefined && modelo.NumeroFinal !== '' && modelo.NumeroFinal !== 0)) {
                    if (modelo.NumeroFinal < modelo.NumeroInicial) {
                        $scope.MensajesErrorConslta.push('El número final debe ser mayor al número final');
                        continuar = false
                    }
                } else {
                    if ((modelo.NumeroInicial !== null && modelo.NumeroInicial !== undefined && modelo.NumeroInicial !== '' && modelo.NumeroInicial !== 0)) {
                        $scope.Modelo.NumeroFinal = modelo.NumeroInicial
                    } else {
                        $scope.Modelo.NumeroInicial = modelo.NumeroFinal
                    }
                }
                if ((modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')
                    && (modelo.FechaFinal !== null && modelo.FechaFinal !== undefined && modelo.FechaFinal !== '')) {
                    if (modelo.FechaFinal < modelo.FechaInicial) {
                        $scope.MensajesErrorConslta.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if (((modelo.FechaFinal - modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorConslta.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if ((modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = modelo.FechaInicial
                    } else {
                        $scope.Modelo.FechaInicial = modelo.FechaFinal
                    }
                }
            }


            return continuar
        }
        $scope.MarcarRemesas = function (chk) {
            if (chk) {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoRemesasFiltradas[i].Seleccionado = true
                }
            } else {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoRemesasFiltradas[i].Seleccionado = false
                }
            }
        }
        $scope.Filtrar = function () {
            $scope.totalRegistros = 0;
            var Filtro = $scope.Filtro2

            $scope.ListadoRemesas = [];
            $scope.ListadoRemesasFiltradas = [];
            $scope.ListadoGuias = [];
            for (var i = 0; i < $scope.ListadoOriginalRemesas.length; i++) {
                var item = $scope.ListadoOriginalRemesas[i];
                var agregar = true;
                if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {

                    if (item.NumeroDocumento < Filtro.NumeroInicial) {
                        agregar = false;
                    }
                }
                if (Filtro.NumeroFinal !== undefined && Filtro.NumeroFinal > 0) {

                    if (item.NumeroDocumento > Filtro.NumeroFinal) {
                        agregar = false;
                    }
                }
                if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {

                    if (new Date(item.Fecha) < Filtro.FechaInicial) {
                        agregar = false;
                    }
                }
                if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {

                    if (new Date(item.Fecha) > Filtro.FechaFinal) {
                        agregar = false;
                    }
                }
                if (agregar) {
                    $scope.ListadoRemesas.push(item);
                    $scope.ListadoRemesasFiltradas.push(item);
                    $scope.totalRegistros = $scope.totalRegistros + 1;
                }

            }

            $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
            $scope.PrimerPagina();
        }
        $scope.AgregarAuxiliar = function () {
            if ($scope.ModeloFuncionario !== '' && $scope.ModeloFuncionario !== undefined && $scope.ModeloFuncionario !== null && $scope.ModeloFuncionario.Codigo !== 0 && $scope.ModeloFuncionario.Codigo !== undefined) {
                var concidencias = 0
                if ($scope.ListaAuxiliares.length > 0) {
                    for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                        if ($scope.ModeloFuncionario.Codigo === $scope.ListaAuxiliares[i].Tercero.Codigo) {
                            concidencias++
                            break;
                        }
                    }
                    if (concidencias > 0) {
                        ShowError('El funcionario ya fue ingresado')
                        $scope.ModeloFuncionario = '';
                    } else {
                        $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                        $scope.ModeloFuncionario = '';
                    }
                } else {
                    $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                    $scope.ModeloFuncionario = '';
                }
            } else {
                ShowError('Debe ingresar un funcionario valido');
            }
            $scope.ValidarDatosFuncionario();
        }
        $scope.ValidarDatosFuncionario = function () {
            for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                $scope.ListaAuxiliares[i].Modificarfuncionario = false;
            }
        }
        $scope.EliminarFuncionario = function (codigo) {
            if ($scope.ListaAuxiliares.length > 0) {
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (codigo === $scope.ListaAuxiliares[i].Codigo) {
                        $scope.ListaAuxiliares.splice(i, 1);
                    }
                }
            }
        };
        $scope.ListaConductores = []
        $scope.AgregarConductores = function () {
            if ($scope.Conductores !== '' && $scope.Conductores !== undefined && $scope.Conductores !== null && $scope.Conductores.Codigo !== 0 && $scope.Conductores.Codigo !== undefined) {
                if ($scope.ListaConductores.length < 4) {

                    var concidencias = 0
                    if ($scope.ListaConductores.length > 0) {
                        for (var i = 0; i < $scope.ListaConductores.length; i++) {
                            if ($scope.Conductores.Codigo === $scope.ListaConductores[i].Codigo) {
                                concidencias++
                                break;
                            }
                        }
                        if (concidencias > 0) {
                            ShowError('El conductor ya fue ingresado')
                            $scope.Conductores = '';
                        } else {
                            $scope.ListaConductores.push($scope.Conductores);
                            $scope.Conductores = '';
                        }
                    } else {
                        $scope.ListaConductores.push($scope.Conductores);
                        $scope.Conductores = '';
                    }
                } else {
                    ShowError('No se pueden adicionar mas de 4 conductores')
                }
            } else {
                ShowError('Debe ingresar un conductor valido');
            }
        }
        $scope.EliminarConductor = function (codigo) {
            if ($scope.ListaConductores.length > 0) {
                for (var i = 0; i < $scope.ListaConductores.length; i++) {
                    if (codigo === $scope.ListaConductores[i].Codigo) {
                        $scope.ListaConductores.splice(i, 1);
                    }
                }
            }
        };
        $scope.ListaImpuestos = []
        $scope.AgregarImpuestos = function () {
            if ($scope.Impuestos == '' || $scope.Impuestos == null || $scope.Impuestos == undefined) {
                ShowError('De ingresar el impuesto')
            }
            else {
                if ($scope.ListaImpuestos.length == 0) {
                    $scope.ListaImpuestos.push($scope.Impuestos)
                    $scope.Impuestos = ''
                } else {
                    var cont = 0
                    for (var i = 0; i < $scope.ListaImpuestos.length; i++) {
                        if ($scope.Impuestos.Codigo == $scope.ListaImpuestos[i].Codigo) {
                            cont++
                            break;
                        }
                    }
                    if (cont > 0) {
                        ShowError('El impuesto que intenta agregar ya se encuentra adicionado')
                    } else {
                        $scope.ListaImpuestos.push($scope.Impuestos)
                    }
                    $scope.Impuestos = ''
                }

            }
        }
        $scope.EliminarImpuesto = function (index) {
            $scope.ListaImpuestos.splice(index, 1);
        };

        $scope.VolverMaster = function () {
            if ($scope.Modelo.Planilla.Numero == undefined) {
                document.location.href = '#!ConsultarPlanillasDespachos';
            }
            else {
                document.location.href = '#!ConsultarPlanillasDespachos/' + $scope.Modelo.Planilla.Numero;
            }
        };
        //----Guardar
        $scope.ValidarGuardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Generando Planilla...");
                $timeout(function () { blockUI.message("Generando Planilla..."); Guardar(); }, 100);
                //Bloqueo Pantalla
            }
        };
        function Guardar() {
            $scope.Modelo.Planilla.Detalles = []
            if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== '') {
                if ($scope.ListadoGuiaGuardadas.length > 0) {
                    $scope.Modelo.Planilla.Detalles = JSON.parse(JSON.stringify($scope.ListadoGuiaGuardadas))
                }
            }
            if ($scope.ListadoRemesasFiltradas !== undefined && $scope.ListadoRemesasFiltradas !== null && $scope.ListadoRemesasFiltradas !== '') {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    var guia = $scope.ListadoRemesasFiltradas[i]
                    if (guia.Seleccionado) {
                        $scope.Modelo.Planilla.Detalles.push({ Remesa: guia })
                    }
                }
            }
            if ($scope.DetallesAuxiliares.length > 0) {
                $scope.Modelo.Planilla.DetallesAuxiliares = $scope.DetallesAuxiliares
            }
            $scope.Modelo.Planilla.FechaHoraSalida = new Date($scope.Modelo.Planilla.FechaSalida)
            var Horasminutos = []
            Horasminutos = $scope.Modelo.Planilla.HoraSalida.split(':')
            if (Horasminutos.length > 0) {
                $scope.Modelo.Planilla.FechaHoraSalida.setHours(Horasminutos[0])
                $scope.Modelo.Planilla.FechaHoraSalida.setMinutes(Horasminutos[1])
            }
            $scope.Modelo.Planilla.TarifaTransportes = { Codigo: $scope.Sesion.UsuarioAutenticado.ManejoTarifasProgramacion ? 301 : $scope.Modelo.Planilla.TarifaTransportes.Codigo }
            $scope.Modelo.Planilla.TipoTarifaTransportes = { Codigo: $scope.Sesion.UsuarioAutenticado.ManejoTarifasProgramacion ? 0 : $scope.TipoTarifaTransportes.Codigo }

            if ($scope.ListadoImpuestosFiltrado.length > 0) {
                $scope.Modelo.Planilla.DetalleImpuesto = $scope.ListadoImpuestosFiltrado;
            }
            if ($scope.ListaAuxiliares.length > 0) {
                $scope.Modelo.Planilla.DetallesAuxiliares = []
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (!$scope.ListaAuxiliares[i].Modificarfuncionario) {
                        $scope.Modelo.Planilla.DetallesAuxiliares.push({
                            Funcionario: $scope.ListaAuxiliares[i].Tercero
                            , NumeroHorasTrabajadas: $scope.ListaAuxiliares[i].Horas
                            , Valor: $scope.ListaAuxiliares[i].Valor
                            , Observaciones: $scope.ListaAuxiliares[i].Observaciones
                        })
                    }
                }
            }
            if ($scope.ListaConductores.length > 0) {
                $scope.Modelo.Conductores = $scope.ListaConductores
            }
            try {
                if (parseInt($scope.Modelo.Planilla.ValorAnticipo) > CERO) {
                    $scope.Modelo.CuentaPorPagar = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                        CodigoAlterno: '',
                        Fecha: $scope.Modelo.Planilla.Fecha,
                        Tercero: {
                            Codigo: ($scope.Modelo.Planilla.AnticipoPagadoA == 'Tenedor' ? $scope.Modelo.Planilla.Vehiculo.Tenedor.Codigo : $scope.Modelo.Planilla.Vehiculo.Conductor.Codigo)
                        },
                        AplicaPSL: ($scope.Sesion.UsuarioAutenticado.ManejoAnticiposPSL && $scope.FormaPagoRecaudo.Codigo == 2) ? 1 : 0,
                        DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO },
                        CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO,
                        Numeracion: '',
                        CuentaPuc: { Codigo: 0 },
                        ValorTotal: $scope.Modelo.Planilla.ValorAnticipo,
                        Abono: 0,
                        Saldo: $scope.Modelo.Planilla.ValorAnticipo,
                        FechaCancelacionPago: $scope.Modelo.Planilla.Fecha,
                        Aprobado: 1,
                        UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        FechaAprobo: $scope.Modelo.Planilla.Fecha,
                        Oficina: { Codigo: $scope.ModeloOficina.Codigo },
                        Vehiculo: { Codigo: $scope.Modelo.Planilla.Vehiculo.Codigo },
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    }
                }
            } catch (e) {

            }
            $scope.Modelo.Planilla.AutorizacionAnticipo = $scope.AutorizacionAnticipo;
            $scope.Modelo.Planilla.AutorizacionFlete = $scope.AutorizacionFlete;
            if ($scope.AutorizacionFlete > 0) {
                $scope.Modelo.Planilla.ValorFleteAutorizacion = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))
                $scope.Modelo.Planilla.ValorFleteTransportador = parseInt(MascaraNumero($scope.ModalTotalFleteTransportadorTEMP))
                $scope.Modelo.Planilla.strValorFleteAutorizacion = MascaraValores(parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador)))
                $scope.Modelo.Planilla.strValorFleteTransportador = MascaraValores(parseInt(MascaraNumero($scope.ModalTotalFleteTransportadorTEMP)))
            }
            //--Detalle Remesas
            var ListadoRemesaDetalle = [];
            for (var i = 0; i < $scope.Modelo.Planilla.Detalles.length; i++) {
                var RemObj = $scope.Modelo.Planilla.Detalles[i].Remesa;
                var remesa = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Remesa: {
                        Numero: RemObj.Numero,
                        NumeroDocumento: RemObj.NumeroDocumento
                    },
                };
                ListadoRemesaDetalle.push(remesa);
            }
            //--Detalle Remesas
            //--Auxiliares
            var DetallesAuxiliares = [];
            if ($scope.ListaAuxiliares.length > 0) {
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (!$scope.ListaAuxiliares[i].Modificarfuncionario) {
                        DetallesAuxiliares.push({
                            Funcionario: $scope.ListaAuxiliares[i].Tercero
                            , NumeroHorasTrabajadas: $scope.ListaAuxiliares[i].Horas
                            , Valor: $scope.ListaAuxiliares[i].Valor
                            , Observaciones: $scope.ListaAuxiliares[i].Observaciones
                        });
                    }
                }
            }
            //--Auxiliares
            //--Conductores
            var DetalleConductores = [];
            if ($scope.ListaConductores.length > 0) {
                for (var i = 0; i < $scope.ListaConductores.length; i++) {
                    DetalleConductores.push({ Codigo: $scope.ListaConductores[i].Codigo });
                }
            }
            //--Conductores
            //--Detalle Impuestos
            var ListadoDetalleImpuestos = [];
            if ($scope.ListadoImpuestosFiltrado.length > 0) {
                for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                    var ImpArr = $scope.ListadoImpuestosFiltrado[i];
                    var impuesto = {
                        CodigoImpuesto: ImpArr.CodigoImpuesto,
                        ValorTarifa: ImpArr.ValorTarifa,
                        ValorBase: ImpArr.ValorBase,
                        ValorImpuesto: ImpArr.ValorImpuesto
                    };
                    ListadoDetalleImpuestos.push(impuesto);
                }
            }
            //--Detalle Impuestos
            //--Genera Obj Planilla
            var PlanillaDespachos = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                Numero: $scope.Modelo.Planilla.Numero,
                Fecha: $scope.Modelo.Planilla.Fecha,
                FechaHoraSalida: $scope.Modelo.Planilla.FechaHoraSalida,
                Ruta: { Codigo: $scope.Modelo.Planilla.Ruta.Codigo },
                Vehiculo: { Codigo: $scope.Modelo.Planilla.Vehiculo.Codigo },
                Tenedor: { Codigo: $scope.Modelo.Planilla.Vehiculo.Tenedor.Codigo },
                Conductor: { Codigo: $scope.Modelo.Planilla.Vehiculo.Conductor.Codigo },
                Semirremolque: { Codigo: $scope.Modelo.Planilla.Semirremolque != '' && $scope.Modelo.Planilla.Semirremolque != null && $scope.Modelo.Planilla.Semirremolque != undefined ? $scope.Modelo.Planilla.Semirremolque.Codigo : 0 },
                Cantidad: $scope.Modelo.Planilla.Cantidad,
                Peso: $scope.Modelo.Planilla.Peso,
                ValorFleteAutorizacion: $scope.Modelo.Planilla.ValorFleteAutorizacion,
                ValorFleteTransportador: $scope.Modelo.Planilla.ValorFleteTransportador,
                strValorFleteAutorizacion: $scope.Modelo.Planilla.strValorFleteAutorizacion,
                strValorFleteTransportador: $scope.Modelo.Planilla.strValorFleteTransportador,
                AutorizacionFlete: $scope.Modelo.Planilla.AutorizacionFlete,
                ValorAnticipo: $scope.Modelo.Planilla.ValorAnticipo,
                ValorImpuestos: $scope.Modelo.Planilla.ValorImpuestos,
                ValorPagarTransportador: $scope.Modelo.Planilla.ValorPagarTransportador,
                ValorFleteCliente: $scope.Modelo.Planilla.ValorFleteCliente,
                ValorSeguroMercancia: $scope.Modelo.Planilla.ValorSeguroMercancia,
                ValorOtrosCobros: $scope.Modelo.Planilla.ValorOtrosCobros,
                ValorTotalCredito: $scope.Modelo.Planilla.ValorTotalCredito,
                ValorTotalContado: $scope.Modelo.Planilla.ValorTotalContado,
                ValorTotalAlcobro: $scope.Modelo.Planilla.ValorTotalAlcobro,
                TarifaTransportes: { Codigo: $scope.Modelo.Planilla.TarifaTransportes.Codigo },
                TipoTarifaTransportes: { Codigo: $scope.Modelo.Planilla.TipoTarifaTransportes.Codigo },
                LineaNegocioTransportes: { Codigo: CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO },
                //TipoLineaNegocioTransportes:{},
                //NumeroTarifarioCompra: 0,
                //Numeracion:0,
                Observaciones: $scope.Modelo.Planilla.Observaciones,
                Estado: { Codigo: $scope.Modelo.Planilla.Estado.Codigo },
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                ValorSeguroPoliza: $scope.Modelo.Planilla.ValorSeguroPoliza,
                AnticipoPagadoA: $scope.Modelo.Planilla.AnticipoPagadoA,
                Detalles: ListadoRemesaDetalle,
                DetallesAuxiliares: DetallesAuxiliares,
                Conductores: DetalleConductores,
                DetalleImpuesto: ListadoDetalleImpuestos,
                CuentaPorPagar: $scope.Modelo.CuentaPorPagar
            };

            //--Valida Manifiesto Para Generacion
            if ($scope.Sesion.Empresa.GeneraManifiesto === 1 && $scope.Modelo.Planilla.Estado.Codigo == 1 && $scope.Modelo.Planilla.Ruta.TipoRuta.Codigo == 4401) {
                PlanillaDespachos.Manifiesto = {
                    Numero: 0,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroPlanilla: 0,
                    Tipo_Documento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO },
                    Usuario_Crea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                    Tipo_Manifiesto: { Codigo: CODIGO_CATALOGO_TIPO_MANIFIESTO_MASIVO },
                    Valor_Anticipo: $scope.Modelo.Planilla.ValorAnticipo,
                    CuentaPorPagar: {},
                };
            }
            //--Valida Manifiesto Para Generacion

            //($scope.Modelo.Planilla.ValorFleteTransportador)) > parseInt(MascaraNumero($scope.ModalTotalFleteTransportadorTEMP
            PlanillaDespachosFactory.Guardar(PlanillaDespachos).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > CERO) {
                            if ($scope.Modelo.Planilla.Numero == CERO) {
                                ShowSuccess('Se guardó la planilla de despachos con el número ' + response.data.Numero);
                            }
                            else {
                                ShowSuccess('Se modificó la planilla de despachos con el número ' + response.data.Numero);
                            }
                            $timeout(function () {
                                blockUI.stop();
                                blockUIConfig.autoBlock = false;
                                if (blockUI.state().blocking == true) { blockUI.reset(); }
                                location.href = '#!ConsultarPlanillasDespachos/' + response.data.Datos;
                            }, 500);
                        }
                        else if (response.data.Datos == -2) {
                            ShowError('Algunas de las remesas que intenta guardar ya se enceuntran asociadas a otra planilla de despacho');
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        }
                        else if (response.data.Datos == -3) {
                            ShowWarning('', 'El vehículo ingresado tiene planillas pendientes por cumplir');
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        }
                        else {
                            ShowError(response.statusText);
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                });
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo
            var fecha1 = new Date(Modelo.Planilla.Fecha)
            var fecha2 = new Date(Modelo.Planilla.FechaSalida)
            var fecha3 = new Date()
            fecha1.setHours('00')
            fecha1.setMinutes('00')
            fecha1.setSeconds('00')
            fecha1.setMilliseconds('00')
            fecha2.setHours('00')
            fecha2.setMinutes('00')
            fecha2.setSeconds('00')
            fecha2.setMilliseconds('00')
            fecha3.setHours('00')
            fecha3.setMinutes('00')
            fecha3.setSeconds('00')
            fecha3.setMilliseconds('00')
            if (Modelo.Planilla.Fecha == undefined || Modelo.Planilla.Fecha == '' || Modelo.Planilla.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha ');
                continuar = false;
            }
            else if (fecha1 < fecha3) {
                $scope.MensajesError.push('La fecha ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Modelo.Planilla.Vehiculo == undefined || Modelo.Planilla.Vehiculo == '') {
                $scope.MensajesError.push('Debe ingresar el vehículo');
                continuar = false;
            }
            if (Modelo.Planilla.FechaSalida == undefined || Modelo.Planilla.FechaSalida == '' || Modelo.Planilla.FechaSalida == null) {
                $scope.MensajesError.push('Debe ingresar la fecha de salida');
                continuar = false;
            }
            else if (fecha2 < fecha3) {
                $scope.MensajesError.push('La fecha de salida ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Modelo.Planilla.HoraSalida == undefined || Modelo.Planilla.HoraSalida == '' || Modelo.Planilla.HoraSalida == null) {
                $scope.MensajesError.push('Debe ingresar la hora de salida');
                continuar = false;
            }
            else {
                var horas = Modelo.Planilla.HoraSalida.split(':')
                if (horas.length < 2) {
                    $scope.MensajesError.push('Debe ingresar una hora valida');
                    continuar = false;
                } else if (fecha2 == fecha3) {
                    if (parseInt(horas[0]) < fecha3.getHours()) {
                        $scope.MensajesError.push('La hora ingresada debe ser mayor a la actual');
                        continuar = false;
                    } else if (parseInt(horas[0]) == fecha3.getHours()) {
                        if (parseInt(horas[1]) < fecha3.getMinutes()) {
                            $scope.MensajesError.push('La hora ingresada debe ser mayor a la actual');
                            continuar = false;
                        } else {
                            if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                                $scope.MensajesError.push('Debe ingresar una hora valida');
                                continuar = false;
                            }
                        }

                    }

                } else {
                    if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                        $scope.MensajesError.push('Debe ingresar una hora valida');
                        continuar = false;
                    }
                }

            }
            if (Modelo.Planilla.Ruta == undefined || Modelo.Planilla.Ruta == null || Modelo.Planilla.Ruta == '') {
                $scope.MensajesError.push('Debe ingresar una ruta');
                continuar = false;
            }
            var masivo = 0;
            var semimasivo = 0;
            var lsttem = [];
            var CountReme = 0;
            if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== '') {
                if ($scope.ListadoGuiaGuardadas.length > 0) {
                    lsttem = JSON.parse(JSON.stringify($scope.ListadoGuiaGuardadas))
                }
                CountReme = $scope.ListadoGuiaGuardadas.length;
            }
            if ($scope.ListadoRemesasFiltradas !== undefined && $scope.ListadoRemesasFiltradas !== null && $scope.ListadoRemesasFiltradas !== '') {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    var guia = $scope.ListadoRemesasFiltradas[i]
                    if (guia.Seleccionado) {
                        lsttem.push({ Remesa: guia })
                        CountReme += 1;
                    }
                }
                for (var i = 0; i < lsttem.length; i++) {
                    var remesa = lsttem[i].Remesa
                    if (remesa.LineaNegocio.Codigo == 1) {
                        masivo++
                    }
                    if (remesa.LineaNegocio.Codigo == 2) {
                        semimasivo++
                    }
                }
            }

            if (CountReme == 0) {
                $scope.MensajesError.push('Debe seleccionar al menos una remesa');
                continuar = false;
            }

            if ((masivo > 0 && semimasivo > 0) && !$scope.Sesion.UsuarioAutenticado.ManejoDistintasLineasPlanillaDespachos) {
                $scope.MensajesError.push('No puede ingresar remesas de distinta linea de negocio');
                continuar = false;
            }
            if (parseInt(MascaraNumero($scope.Modelo.Planilla.Peso)) > 0 && parseInt(MascaraNumero($scope.Modelo.Planilla.Peso)) > $scope.Modelo.Planilla.Vehiculo.Capacidad) {
                $scope.MensajesError.push('La sumatoria del peso de las remesas excede la capacidad de vehículo');
                continuar = false;
            }

            return continuar;
        }
        $scope.ConfirmacionGuardar = function () {
            if ($scope.DeshabilitarActualizar) {
                location.href = '#!ConsultarPlanillasDespachos/' + $scope.Modelo.Planilla.Numero;
            } else {
                showModal('modalConfirmacionGuardar');
            }
        };
        //----Guardar
        function Obtener() {
            blockUI.start('Cargando Planilla Despachos...');

            $timeout(function () {
                blockUI.message('Cargando Planilla Despachos...');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO
            };

            blockUI.delay = 1000;
            PlanillaDespachosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.esObtener = true
                        $scope.ListadoGuiaGuardadas = []
                        $scope.Modelo.Planilla = response.data.Datos;
                        try {


                            $scope.pesotemp = $scope.Modelo.Planilla.Peso
                            $scope.Modelo.Planilla.FechaSalida = new Date($scope.Modelo.Planilla.FechaHoraSalida)

                            $scope.Modelo.Planilla.HoraSalida = $scope.Modelo.Planilla.FechaSalida.getHours().toString()

                            if ($scope.Modelo.Planilla.FechaSalida.getMinutes().toString().length == 1) {
                                $scope.Modelo.Planilla.HoraSalida += ':0' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString()
                            } else {
                                $scope.Modelo.Planilla.HoraSalida += ':' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString()
                            }
                            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                            $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                            $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                            if ($scope.Modelo.Planilla.Estado.Codigo == 1) {
                                $scope.DeshabilitarActualizar = true;
                            }
                            $scope.Modelo.Planilla.Fecha = new Date($scope.Modelo.Planilla.Fecha)
                            ////Obtiene el detalle de impuestos aplicados
                            //if ($scope.Modelo.Planilla.DetalleImpuesto !== undefined && $scope.Modelo.Planilla.DetalleImpuesto !== null && $scope.Modelo.Planilla.DetalleImpuesto !== []) {
                            //    $scope.ListaImpuestos = $scope.Modelo.Planilla.DetalleImpuesto;
                            //    //formatea el valor de cada impuesto
                            //    for (var i = 0; i < $scope.ListaImpuestos.length; i++) {
                            //        $scope.ListaImpuestos[i].ValorImpuesto = $scope.MaskValoresGrid($scope.ListaImpuestos[i].ValorImpuesto);
                            //    }
                            //}
                            //CalcularImpuesto();
                            $scope.ListadoGuiaGuardadas = []
                            if ($scope.Modelo.Planilla.ListadoRemesas !== undefined && $scope.Modelo.Planilla.ListadoRemesas !== null && $scope.Modelo.Planilla.ListadoRemesas !== []) {
                                for (var i = 0; i < $scope.Modelo.Planilla.ListadoRemesas.length; i++) {
                                    var itmRemesa = $scope.Modelo.Planilla.ListadoRemesas[i];
                                    $scope.ListadoGuiaGuardadas.push({
                                        Remesa: {
                                            Numero: itmRemesa.NumeroInternoRemesa,
                                            NumeroDocumento: itmRemesa.NumeroRemesa,
                                            Fecha: itmRemesa.Fecha,
                                            Ruta: { Nombre: itmRemesa.NombreRuta, Codigo: '' },
                                            Cliente: $scope.CargarTercero(itmRemesa.CodigoCliente),
                                            LineaNegocio: { Nombre: '' },
                                            ProductoTransportado: { Codigo: itmRemesa.CodigoProducto, Nombre: itmRemesa.NombreProducto },
                                            CantidadCliente: itmRemesa.CantidadRemesa,
                                            PesoCliente: itmRemesa.PesoRemesa,
                                            Observaciones: itmRemesa.Observaciones
                                        },
                                        TotalFleteTransportador: itmRemesa.TotalFleteTransportador,
                                    });
                                }
                                $('#btncriterios').click();
                                $('#btnFiltros').click();
                            }
                            $scope.ListaAuxiliares = []
                            if ($scope.Modelo.Planilla.ListadoAuxiliar !== undefined && $scope.Modelo.Planilla.ListadoAuxiliar !== null && $scope.Modelo.Planilla.ListadoAuxiliar !== []) {
                                for (var i = 0; i < $scope.Modelo.Planilla.ListadoAuxiliar.length; i++) {
                                    $scope.ListaAuxiliares.push({
                                        Tercero: $scope.Modelo.Planilla.ListadoAuxiliar[i].Tercero,
                                        Horas: $scope.Modelo.Planilla.ListadoAuxiliar[i].Horas,
                                        Valor: $scope.Modelo.Planilla.ListadoAuxiliar[i].Valor,
                                        Observaciones: $scope.Modelo.Planilla.ListadoAuxiliar[i].Observaciones,
                                    })
                                }
                            }
                        } catch (e) {

                        }
                        if ($scope.Modelo.Planilla.Conductores != undefined && $scope.Modelo.Planilla.Conductores != null) {
                            $scope.ListaConductores = $scope.Modelo.Planilla.Conductores;
                        }

                        $scope.Modelo.Planilla.Vehiculo = $scope.CargarVehiculos($scope.Modelo.Planilla.Vehiculo.Codigo);
                        if ($scope.Modelo.Planilla.Estado.Codigo == 1) {
                            $scope.LustadoRutasFiltrado = []
                            $scope.LustadoRutasFiltrado.push($scope.Modelo.Planilla.Ruta);
                            $scope.Modelo.Planilla.Ruta = $scope.LustadoRutasFiltrado[0];
                            $scope.ListaTarifas = [];
                            $scope.ListaTarifas.push($scope.Modelo.Planilla.TarifaTransportes);
                            $scope.Modelo.Planilla.TarifaTransportes = $scope.ListaTarifas[0];
                            $scope.ListadoTipoTarifa = [];
                            $scope.ListadoTipoTarifa.push({ Codigo: $scope.Modelo.Planilla.TipoTarifaTransportes.Codigo, NombreTipoTarifa: $scope.Modelo.Planilla.TipoTarifaTransportes.Nombre });
                            $scope.TipoTarifaTransportes = $scope.ListadoTipoTarifa[0];
                            for (var i = 0; i < $scope.Modelo.Planilla.ListadoImpuestos.length; i++) {
                                $scope.ListadoImpuestosFiltrado.push({
                                    Nombre: $scope.Modelo.Planilla.ListadoImpuestos[i].Label,
                                    ValorBase: $scope.Modelo.Planilla.ListadoImpuestos[i].valor_base,
                                    ValorTarifa: $scope.Modelo.Planilla.ListadoImpuestos[i].Valor_tarifa,
                                    ValorImpuesto: $scope.Modelo.Planilla.ListadoImpuestos[i].ValorImpuesto
                                });
                            }
                        } else {
                            $scope.ObtenerInformacionTenedor();
                        }
                        $scope.MaskValores();
                        if (response.data.Datos.PagoAnticipo > 0) {
                            $scope.FormaPagoRecaudo = $scope.ListaFormaPago[1]
                        } else {
                            $scope.FormaPagoRecaudo = $scope.ListaFormaPago[0]
                        }
                        $scope.Modelo.Planilla.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + $scope.Modelo.Planilla.Estado.Codigo);

                    }
                    else {
                        ShowError('No se logro consultar el tarifario Compras código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarPlanillasDespachos';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarPlanillasDespachos';
                });

            blockUI.stop();
        };
        //----Guardar
        $scope.EliminarGuiaGuardada = function (index) {
            $scope.ListadoGuiaGuardadas.splice(index, 1)
        }
        $scope.ValidarFleteTransportador = function () {
            $scope.AutorizacionFlete = 0
            $scope.AutorizacionAnticipo = 0
            if ($scope.Sesion.UsuarioAutenticado.ManejoAutorizacionFlete) {
                if (parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador)) > parseInt(MascaraNumero($scope.ModalTotalFleteTransportadorTEMP))) {
                    ShowConfirm('El valor del flete ingresado es superior al estipulado en el tarifario. ¿Desea solicitar autorización para el cambio de flete?', $scope.CambiarFlete, $scope.RechazoCambioFlete)
                } else {
                    $scope.Calcular(1, true)
                }
            } else {
                $scope.Calcular(1, true)
            }
        }
        $scope.CambiarFlete = function () {
            $scope.AutorizacionFlete = 1
            $scope.AutorizacionAnticipo = 0
            $scope.Calcular(1, true)
        }
        $scope.RechazoCambioFlete = function () {
            $scope.AutorizacionFlete = 0
            $scope.Modelo.Planilla.ValorFleteTransportador = MascaraValores($scope.ModalTotalFleteTransportadorTEMP)
            $scope.Calcular(1, true)
        }
        $scope.ValidarAnticipoPlanilla = function (anticipo) {
            $scope.AutorizacionAnticipo = 0
            anticipo = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorAnticipo))
            if (parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) > 0) {
                if (anticipo > parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))) {
                    ShowError(' El valor del anticipo no puede ser mayor al valor del flete del transportador')
                    anticipo = 0
                    $scope.AutorizacionAnticipo = 0
                } else if (anticipo > ((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador)))) {
                    $scope.AnticipoAutorizado = MascaraValores((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador)))
                    showModal('modalConfirmacionAnticipo')
                } else {
                    $scope.AutorizacionAnticipo = 0
                }
                $scope.Modelo.Planilla.ValorAnticipo = MascaraValores(anticipo)
            }
        }
        //--Mascaras
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };
        //--Mascaras
    }]);