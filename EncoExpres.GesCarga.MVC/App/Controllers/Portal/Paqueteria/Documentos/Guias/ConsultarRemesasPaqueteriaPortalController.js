﻿EncoExpresApp.controller("ConsultarRemesasPaqueteriaPortalCtrl", ['$scope', '$timeout', 'TarifarioVentasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'RemesaGuiasFactory', 'EmpresasFactory',
    'OficinasFactory', 'RemesasFactory', 'PlanillaGuiasFactory', 'SemirremolquesFactory', 'RutasFactory',
    function ($scope, $timeout, TarifarioVentasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, RemesaGuiasFactory, EmpresasFactory,
        OficinasFactory, RemesasFactory, PlanillaGuiasFactory, SemirremolquesFactory, RutasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Despachos' }, { Nombre: 'Remesas' }];
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} },
            Numeracion: ''
        }
        $scope.Modelo.Remesa.Cliente = $scope.CargarTercero($scope.Sesion.UsuarioAutenticado.Cliente.Codigo);
        try {
            try {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_REMESAS_PAQUETERIA_PORTAL);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

            } catch (e) {

                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GUIAS_PAQUETERIA);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
            }
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        $scope.ListadoEstadoGuia = [
            { Codigo: -1, Nombre: '(TODOS)' }
        ]
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 60 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != 6000) {
                                $scope.ListadoEstadoGuia.push(response.data.Datos[i])
                            }
                        }
                    }
                    $scope.Modelo.EstadoRemesaPaqueteria = $scope.ListadoEstadoGuia[0]
                }
            }, function (response) {
            });

        $scope.ListadoOficinasActual = [
            { Codigo: 0, Nombre: '(TODAS)' }
        ];
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasActual.push(item);
                    })
                    $scope.Modelo.OficinaActual = $scope.ListadoOficinasActual[0];
                    $scope.Modelo.Oficina = $scope.ListadoOficinasActual[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRemesasPaqueteriaPortal';
            }
        };
        $scope.CopiarDocumento = function (Codigo) {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRemesasPaqueteriaPortal/' + Codigo.toString() + ',1';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.NumeroInicial = $routeParams.Codigo;
                $scope.Modelo.NumeroFinal = $routeParams.Codigo;
                Find();
                $scope.Modelo.NumeroInicial = ''
                $scope.Modelo.NumeroFinal = ''
            }
        }
        /*Cargar el combo de ESTADO DE REMESAS PAQUETERIA*/

        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }, { Codigo: 2, Nombre: 'ANULADO' }];
        $scope.ModeloEstado = $scope.ListadoEstadoRemesaPaqueteria[0];
        $scope.Modelo.FechaInicial = new Date();
        $scope.Modelo.FechaFinal = new Date();
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroInicial === null || $scope.Modelo.NumeroInicial === undefined || $scope.Modelo.NumeroInicial === '' || $scope.Modelo.NumeroInicial === 0 || isNaN($scope.Modelo.NumeroInicial) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.Modelo.NumeroInicial !== null && $scope.Modelo.NumeroInicial !== undefined && $scope.Modelo.NumeroInicial !== '' && $scope.Modelo.NumeroInicial !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial
                    } else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal
                    }
                }
            }
            return continuar
        }
        function Find() {

            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            if (DatosRequeridos()) {

                blockUI.delay = 1000;
                $scope.Modelo.Pagina = $scope.paginaActual
                $scope.Modelo.RegistrosPagina = 10
                $scope.Modelo.NumeroPlanilla = -1


                $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;

                if ($scope.ModeloEstado !== undefined && $scope.ModeloEstado !== null) {
                    $scope.Modelo.Estado = $scope.ModeloEstado.Codigo;
                } else {
                    $scope.Modelo.Estado = -1;
                }

                var filtro = angular.copy($scope.Modelo);
                filtro.Anulado = -1;


                RemesaGuiasFactory.Consultar(filtro).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            console.log("responseConsulta: ", response.data.Datos);
                            $scope.Modelo.Codigo = 0
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoGuias = response.data.Datos
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                            var listado = [];
                            response.data.Datos.forEach(function (item) {
                                listado.push(item);
                            });
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            blockUI.stop();
        }
        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/
        $scope.EliminarTarifarioVentas = function (codigo, Nombre) {
            $scope.Codigo = codigo
            $scope.Nombre = Nombre
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarTarifarioVentas');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            TarifarioVentasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se elimino la linea vehículos ' + $scope.Nombre);
                        closeModal('modalEliminarTarifarioVentas');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarTarifarioVentas');
                    $scope.ModalError = 'No se puede eliminar la linea vehículos ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)

        };

        //Función que ejecuta el boton de vista preliminar
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroFactura = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_GUIA;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';




        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroFactura != undefined && $scope.NumeroFactura != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroFactura;
            }

        }
        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }

        $scope.AnularDocumento = function (Numero, NumeroDocumento) {
            $scope.Numero = Numero

            $scope.ModalErrorCompleto = ''
            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };

        $scope.Anular = function () {
            var aplicaplanilla = false
            var aplicaFactura = false
            var aplicaManifiesto = false

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnula: $scope.ModeloCausaAnula,
                Numero: $scope.Numero,

            };
            if (DatosRequeridosAnular()) {
                RemesasFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló la remesa ' + $scope.NumeroDocumento);
                                closeModal('modalAnular', 1);
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular la remesa ya que se encuentra relacionado con los siguientes documentos: '

                                var Facturas = '\nFacturas: '
                                var Planillas = '\nPlanillas: '
                                var Manifiestos = '\nManifiestos: '
                                for (var i = 0; i < response.data.Datos.DocumentosRelacionados.length; i++) {
                                    if (response.data.Datos.DocumentosRelacionados[i].FacturaVentas.Numero > 0) {
                                        Facturas += response.data.Datos.DocumentosRelacionados[i].FacturaVentas.Numero + ','
                                        aplicaFactura = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Planilla.Numero > 0) {
                                        Planillas += response.data.Datos.DocumentosRelacionados[i].Planilla.Numero + ','
                                        aplicaplanilla = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero > 0) {
                                        Manifiestos += response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero + ','
                                        aplicaManifiesto = true
                                    }
                                }

                                if (aplicaplanilla) {
                                    mensaje += Planillas
                                }
                                if (aplicaFactura) {
                                    mensaje += Facturas
                                }
                                if (aplicaManifiesto) {
                                    mensaje += Manifiestos
                                }
                                ShowError(mensaje)
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            }
        };
        $scope.ConsultarEstadosRemesas = function (item) {
            $scope.ListadoEstadoGuias = []
            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            }
            RemesaGuiasFactory.ConsultarEstadosRemesa(Filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoEstadoGuias = response.data.Datos
                            showModal('ModalEstadosRemesas')
                        } else {
                            ShowError('No se encontro registro de estados de la remesa seleccionada')
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        $scope.ConsultarControlEntregas = function (item) {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            };

            RemesaGuiasFactory.ObtenerControlEntregas(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Remesa.NombreRecibe != null && response.data.Datos.Remesa.NumeroIdentificacionRecibe != null && response.data.Datos.Remesa.TelefonoRecibe != null) {
                            $scope.FotoControlEntrega = response.data.Datos.Remesa.Fotografia;
                            $scope.FotoControlEntrega.FotoCargada = 'data:' + response.data.Datos.Remesa.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Remesa.Fotografia.FotoBits;
                            $scope.ControlEntregaRemesa = response.data.Datos.Remesa;
                            $scope.ControlEntregaRemesa.FirmaImg = "data:image/png;base64," + $scope.ControlEntregaRemesa.Firma;
                            showModal('modalControlEntregas');
                        }
                        else {
                            ShowError('El documento no cuenta con un control de entregas');
                        }
                    }
                    else {
                        ShowError('El documento no cuenta con un control de entregas');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
    }]);