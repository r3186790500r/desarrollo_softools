﻿Print 'gsp_obtener_encabezado_solicitud_orden_servicios'
GO
DROP PROCEDURE [dbo].[gsp_obtener_encabezado_solicitud_orden_servicios]
GO

CREATE PROCEDURE [dbo].[gsp_obtener_encabezado_solicitud_orden_servicios]
(
@par_EMPR_Codigo SMALLINT,
@par_Numero NUMERIC,
@par_TIDO_Codigo NUMERIC 

)
AS
BEGIN

SELECT
1 AS Obtener,
ESOS.EMPR_Codigo,
ESOS.Numero,
ESOS.TIDO_Codigo,
ESOS.Numero_Documento,
ESOS.Fecha,
ESOS.ETCV_Numero,
ESOS.LNTC_Codigo,
ESOS.TLNC_Codigo,
ESOS.TERC_Codigo_Cliente,
ESOS.TERC_Codigo_Facturar,
ESOS.Documento_Cliente,
ESOS.TERC_Codigo_Comercial,
ESOS.Facturar_Despachar,
ESOS.Facturar_Cantidad_Peso_Cumplido,
ESOS.TERC_Codigo_Destinatario,
ESOS.Fecha_Cargue,
ESOS.CIUD_Codigo_Cargue,
ESOS.Direccion_Cargue,
ESOS.Telefono_Cargue,
ESOS.Contacto_Cargue,
ESOS.OFIC_Codigo_Despacha,
ESOS.Observaciones,
ESOS.Aplica_Poliza,
ESOS.Estado,
ESOS.Anulado,
OFDE.Nombre As OficinaDespacha,
LNTC.Nombre As LineaNegocioTransporteCarga,
TLNC.Nombre As TipoLineaNegocioCarga,
ISNULL(CLIE.Razon_Social,'') +  ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') As NombreCliente,
CLIE.Telefonos As TelefonosCliente,
CLIE.Celulares As CelularesCliente,
CLIE.Direccion As DireccionCliente,
CIUD.Codigo As CIUD_Codigo_Cliente,
CIUD.Nombre + ' (' + DEPA.Nombre + ')' As NombreCiudadCliente,

ETCV.Nombre As NombreTarifario

FROM
Encabezado_Solicitud_Orden_Servicios ESOS, Oficinas OFDE, Tipo_Linea_Negocio_Carga TLNC,
Linea_Negocio_Transporte_Carga LNTC, Terceros CLIE,
Encabezado_Tarifario_Carga_Ventas ETCV, Ciudades CIUD, Departamentos DEPA
WHERE ESOS.EMPR_Codigo = OFDE.EMPR_Codigo 
AND ESOS.OFIC_Codigo_Despacha = OFDE.Codigo
AND ESOS.EMPR_Codigo = TLNC.EMPR_Codigo
AND ESOS.TLNC_Codigo = TLNC.Codigo
AND ESOS.EMPR_Codigo = LNTC.EMPR_Codigo
AND ESOS.LNTC_Codigo = LNTC.Codigo
AND ESOS.EMPR_Codigo = CLIE.EMPR_Codigo
AND ESOS.TERC_Codigo_Cliente = CLIE.Codigo
AND ESOS.EMPR_Codigo = ETCV.EMPR_Codigo
AND ESOS.ETCV_Numero = ETCV.Numero
AND CLIE.EMPR_Codigo = CIUD.EMPR_Codigo
AND CLIE.CIUD_Codigo = CIUD.Codigo
AND CIUD.EMPR_Codigo = DEPA.EMPR_Codigo
AND CIUD.DEPA_Codigo = DEPA.Codigo

AND ESOS.EMPR_Codigo = @par_EMPR_Codigo
AND ESOS.Numero = @par_Numero
AND ESOS.TIDO_Codigo = @par_TIDO_Codigo
END
GO