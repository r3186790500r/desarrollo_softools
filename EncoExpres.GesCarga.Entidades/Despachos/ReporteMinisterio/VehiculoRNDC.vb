﻿Imports Newtonsoft.Json
Namespace Despachos.Procesos
    <JsonObject>
    Public NotInheritable Class VehiculoRNDC
        Sub New()
        End Sub
        <JsonProperty>
        Public Property NUMNITEMPRESATRANSPORTE As String
        <JsonProperty>
        Public Property NUMPLACA As String
        <JsonProperty>
        Public Property CODCONFIGURACIONUNIDADCARGA As String
        <JsonProperty>
        Public Property CODMARCAVEHICULOCARGA As String
        <JsonProperty>
        Public Property CODLINEAVEHICULOCARGA As String
        <JsonProperty>
        Public Property NUMEJES As String
        <JsonProperty>
        Public Property ANOFABRICACIONVEHICULOCARGA As String
        <JsonProperty>
        Public Property ANOREPOTENCIACION As String
        <JsonProperty>
        Public Property CODCOLORVEHICULOCARGA As String
        <JsonProperty>
        Public Property PESOVEHICULOVACIO As String
        <JsonProperty>
        Public Property CAPACIDADUNIDADCARGA As String
        <JsonProperty>
        Public Property UNIDADMEDIDACAPACIDAD As String
        <JsonProperty>
        Public Property CODTIPOCARROCERIA As String
        <JsonProperty>
        Public Property NUMCHASIS As String
        <JsonProperty>
        Public Property CODTIPOCOMBUSTIBLE As String
        <JsonProperty>
        Public Property NUMSEGUROSOAT As String
        <JsonProperty>
        Public Property FECHAVENCIMIENTOSOAT As String
        <JsonProperty>
        Public Property NUMNITASEGURADORASOAT As String
        <JsonProperty>
        Public Property CODTIPOIDPROPIETARIO As String
        <JsonProperty>
        Public Property NUMIDPROPIETARIO As String
        <JsonProperty>
        Public Property CODTIPOIDTENEDOR As String
        <JsonProperty>
        Public Property NUMIDTENEDOR As String
        <JsonProperty>
        Public Property Reportar_RNDC As Short
        <JsonProperty>
        Public Property Codigo As Integer
        Public Const CABEZOTE_DOS_EJES As Integer = 53
        Public Const CABEZOTE_TRES_EJES As Integer = 54
        Public Const PESO_MAXIMO_VEHICULO_SIN_REMOLQUE As Integer = 37
    End Class
End Namespace
