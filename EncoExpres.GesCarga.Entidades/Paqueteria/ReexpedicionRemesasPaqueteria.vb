﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Paqueteria
    <JsonObject>
    Public Class ReexpedicionRemesasPaqueteria
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ReexpedicionRemesasPaqueteria"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            CodigoRemesaPaqueteria = Read(lector, "ENRE_Numero")
            TipoReexpedicion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TRRP_Codigo"), .Nombre = Read(lector, "TipoReexpedicion")}
            ProveedorExterno = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Proveedor_Externo"), .NombreCompleto = Read(lector, "Proveedor")}
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "Nombre")}
            Valor = Read(lector, "Valor_Flete")
            CodigoCiudad = Read(lector, "CIUD_Codigo")
            Observaciones = Read(lector, "Observaciones")
            Estado = Read(lector, "Estado")

        End Sub

        <JsonProperty>
        Public Property TipoReexpedicion As ValorCatalogos
        <JsonProperty>
        Public Property CodigoRemesaPaqueteria As Integer
        <JsonProperty>
        Public Property CodigoCiudad As Integer
        <JsonProperty>
        Public Property ProveedorExterno As Terceros
        <JsonProperty>
        Public Property Valor As Double
        <JsonProperty>
        Public Property Modificar As Integer

    End Class

End Namespace
