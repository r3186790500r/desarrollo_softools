ALTER PROCEDURE [dbo].[gsp_Consultar_Detalle_Liquidacion_Planilla_Despachos_SIESA_ENCOE]            
(            
@par_EMPR_Codigo SMALLINT,              
@par_ELPD_Numero NUMERIC  
)            
AS             
BEGIN            
	SELECT PLUC.Codigo_Cuenta,            
	CLPD.Codigo AS Codigo_Concepto,           
	CLPD.Nombre AS Nombre_Concepto,        
	CASE WHEN CLPD.Operacion = 2 THEN DLPD.Valor            
	ELSE 0 END AS Valor_Debito,            
	CASE WHEN CLPD.Operacion = 1 THEN DLPD.Valor            
	ELSE 0 END AS Valor_Credito,            
	ISNULL(DLPD.Observaciones,'') Observaciones,
	DLPD.ENDC_Codigo AS Codigo_Documento_Comprobante
	           
	FROM Detalle_Liquidacion_Planilla_Despachos AS DLPD           
          
	INNER JOIN Conceptos_Liquidacion_Planilla_Despachos AS CLPD         
	ON CLPD.EMPR_Codigo = DLPD.EMPR_Codigo            
	AND CLPD.Codigo = DLPD.CLPD_Codigo            
            
	LEFT JOIN Plan_Unico_Cuentas AS PLUC            
	ON PLUC.EMPR_Codigo = CLPD.EMPR_Codigo            
	AND PLUC.Codigo = CLPD.PLUC_Codigo            
            
	WHERE DLPD.EMPR_Codigo = @par_EMPR_Codigo           
	AND DLPD.ELPD_Numero = @par_ELPD_Numero          
             
END 