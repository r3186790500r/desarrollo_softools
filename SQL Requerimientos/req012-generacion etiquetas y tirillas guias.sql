USE [GESCARGA50_DESA]
GO

/****** Object:  StoredProcedure [dbo].[gsp_detalle_guia_paqueteria]    Script Date: 17/01/2022 1:43:12 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_detalle_guia_paqueteria]
(
@par_EMPR_Codigo smallint,
@par_ENRE_Numero numeric
)
AS
BEGIN
DECLARE @QRNumeroDocumento Varbinary = NULL
DECLARE @total_unidades int = 0
SELECT @total_unidades = COUNT(*) FROM Detalle_Unidades_Remesa_Paqueteria WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero
SELECT @total_unidades = @total_unidades + COUNT(*) FROM Detalle_Etiquetas_Remesas_Paqueteria WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero
SELECT 
EMPR.Nombre_Razon_Social as NombreRazonSocial,
ENRE.Numero_Documento as NumeroDocumento,
ENRE.Fecha,
OFOR.Nombre as OficinaOrigen,
@total_unidades-(ROW_NUMBER() OVER(ORDER BY DURP.ID)) + 1 As NumeroUnidad,
@total_unidades As TotalUnidades,
CONCAT(CIDE.Nombre, ', ', DEDE.Nombre) As CiudadDestinatario,
@QRNumeroDocumento AS QRNumeroDocumento

FROM Detalle_Unidades_Remesa_Paqueteria DURP 
LEFT JOIN Empresas EMPR ON
DURP.EMPR_Codigo = EMPR.Codigo

LEFT JOIN Ciudades CIUD ON
DURP.EMPR_Codigo = CIUD.EMPR_Codigo AND
EMPR.CIUD_Codigo = CIUD.Codigo

LEFT JOIN Encabezado_Remesas ENRE ON
DURP.EMPR_Codigo = ENRE.EMPR_Codigo AND
DURP.ENRE_Numero = ENRE.Numero

LEFT JOIN Ciudades CIDE ON 
ENRE.EMPR_Codigo = CIDE.EMPR_Codigo AND
ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo

LEFT JOIN Departamentos DEDE ON 
ENRE.EMPR_Codigo = DEDE.EMPR_Codigo AND
CIDE.DEPA_Codigo = DEDE.Codigo

LEFT JOIN Remesas_Paqueteria REPA ON 
ENRE.EMPR_Codigo = REPA.EMPR_Codigo AND
ENRE.Numero = REPA.ENRE_Numero

LEFT JOIN Oficinas OFOR ON 
ENRE.EMPR_Codigo = OFOR.EMPR_Codigo AND
REPA.OFIC_Codigo_Registro_Manual = OFOR.Codigo

WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo
AND ENRE.Numero = @par_ENRE_Numero
END
GO



CREATE OR ALTER PROCEDURE [dbo].[gsp_encabezado_guia_paqueteria]
(
@par_EMPR_Codigo smallint,
@par_ENRE_Numero numeric
)
AS
BEGIN
DECLARE @QRNumeroDocumento Varbinary = NULL
DECLARE @total_unidades int = 0
SELECT @total_unidades = COUNT(*) FROM Detalle_Unidades_Remesa_Paqueteria WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero
SELECT @total_unidades = @total_unidades + COUNT(*) FROM Detalle_Etiquetas_Remesas_Paqueteria WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero
SELECT 
EMPR.Nombre_Razon_Social AS EmpresaNombre,
LTRIM(RTRIM(CONCAT(EMPR.Numero_Identificacion,'-',EMPR.Digito_Chequeo))) AS EmpresaNit,
'+57(1) 378-8060' AS EmpresaTelefono,
'd.domicilios@encoexpres.com.co' AS EmpresaCorreo,
EMPR.Pagina_Web AS EmpresaWeb,

OFOR.Nombre as OficinaOrigen,

ENRE.Numero_Documento AS NumeroDocumento,
ENRE.Fecha,
REPA.Peso_A_Cobrar AS PesoCobrar,
@total_unidades AS TotalUnidades,
ENRE.Total_Flete_Cliente AS ValorTotal,
FOPR.Campo1 AS FormaPago,
LTRIM(RTRIM(CONCAT(AFOR.Nombre, ' ', AFOR.Apellido1,' ' , AFOR.Apellido2,' - ',AFOR.Numero_Identificacion))) AS Aforador,

LTRIM(RTRIM(CONCAT(ENRE.Nombre_Destinatario,' - ',DEST.Numero_Identificacion))) AS Destinatario,
CONCAT(CIDE.Nombre, ', ', DEDE.Nombre) As DestinatarioCiudad,
ENRE.Direccion_Destinatario AS DestinatarioDireccion,
ENRE.Telefonos_Destinatario AS DestinatarioTelefono,
ISNULL(ENRE.Email_Destinatario,'') AS DestinatarioCorreo,

LTRIM(RTRIM(CONCAT(ENRE.Nombre_Remitente,' - ',REMI.Numero_Identificacion))) AS Remitente,
CONCAT(CIRE.Nombre, ', ', DERE.Nombre) As RemitenteCiudad,
ENRE.Direccion_Remitente AS RemitenteDireccion,
ENRE.Telefonos_Remitente AS RemitenteTelefono,
ISNULL(ENRE.Email_Remitente,'') AS RemitenteCorreo,

@QRNumeroDocumento AS QRNumeroDocumento

FROM Encabezado_Remesas ENRE

LEFT JOIN Empresas EMPR ON 
ENRE.EMPR_Codigo = EMPR.Codigo

LEFT JOIN Terceros DEST ON
ENRE.EMPR_Codigo = DEST.EMPR_Codigo AND
ENRE.TERC_Codigo_Destinatario = DEST.Codigo

LEFT JOIN Ciudades CIDE ON 
ENRE.EMPR_Codigo = CIDE.EMPR_Codigo AND
ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo

LEFT JOIN Departamentos DEDE ON 
ENRE.EMPR_Codigo = DEDE.EMPR_Codigo AND
CIDE.DEPA_Codigo = DEDE.Codigo

LEFT JOIN Terceros REMI ON
ENRE.EMPR_Codigo = REMI.EMPR_Codigo AND
ENRE.TERC_Codigo_Remitente = REMI.Codigo

LEFT JOIN Ciudades CIRE ON 
ENRE.EMPR_Codigo = CIRE.EMPR_Codigo AND
ENRE.CIUD_Codigo_Remitente = CIRE.Codigo

LEFT JOIN Departamentos DERE ON 
ENRE.EMPR_Codigo = DERE.EMPR_Codigo AND
CIRE.DEPA_Codigo = DERE.Codigo

LEFT JOIN Remesas_Paqueteria REPA ON 
ENRE.EMPR_Codigo = REPA.EMPR_Codigo AND
ENRE.Numero = REPA.ENRE_Numero

LEFT JOIN Oficinas OFOR ON 
ENRE.EMPR_Codigo = OFOR.EMPR_Codigo AND
REPA.OFIC_Codigo_Registro_Manual = OFOR.Codigo

LEFT JOIN Valor_Catalogos FOPR ON 
ENRE.EMPR_Codigo = FOPR.EMPR_Codigo AND
ENRE.CATA_FOPR_Codigo = FOPR.Codigo

LEFT JOIN Terceros AFOR ON
ENRE.EMPR_Codigo = AFOR.EMPR_Codigo AND
REPA.TERC_Codigo_Aforador = AFOR.Codigo

WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo
AND ENRE.Numero = @par_ENRE_Numero

END
GO

