﻿Print 'V_Categoria_Licencia_Conduccion'
GO
DROP VIEW V_Categoria_Licencia_Conduccion
GO
CREATE VIEW V_Categoria_Licencia_Conduccion 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 18
GO