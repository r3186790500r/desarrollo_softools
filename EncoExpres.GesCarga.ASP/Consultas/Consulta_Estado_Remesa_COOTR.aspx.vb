﻿Imports System.Data.SqlClient
Imports System.Globalization

Public Class Consulta_Estado_Remesa_COOTR1
    Inherits System.Web.UI.Page

#Region "Variables"
    Private strSQL As String
    Private strSQLFoto As String
    Private strSQLEstado As String
    Public strUser As String = ""
    Public strPass As String = ""
    Public strDocuRemesa As String = ""
    Public strNumeroPreimpreso As String = ""

    Dim Numero As Object = Nothing
    Dim CATA_ESRP_Codigo As Object
    Dim objGeneral As clsGeneral
    Dim objDocument As clsGeneral

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            strUser = Me.Request.QueryString("Usuario")
            strPass = Me.Request.QueryString("Clave")
            strDocuRemesa = Me.Request.QueryString("Documento_Remesa")
            strNumeroPreimpreso = Me.Request.QueryString("Numero_Preimpreso")
            Dim MyCultureInfo = New CultureInfo("en-US") ' es-ES
            lblMensajeError.Visible = True
            infoSeguimiento.Style.Add("display", "none")
            infoEntrega.Style.Add("display", "none")
            objGeneral = New clsGeneral
            Anio.Text = Date.Now.Year.ToString

            If strUser = String.Empty Or
               strPass = String.Empty Or
               strDocuRemesa = String.Empty Then
                lblMensajeError.Text = "Acceso no autorizado"
            Else
                GenerarConsultaDistribucion()
            End If

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = ex.Message.ToString
        End Try

    End Sub

    Private Sub GenerarConsultaDistribucion()
        Dim ComandoSQL As SqlCommand
        Dim habilitado As Integer = 0
        Dim ExisteRegistro As Boolean = False
        'Consulta generada para crear el archivo plano de Terceros
        strSQL = "SELECT Habilitado FROM Usuarios"
        strSQL += " WHERE EMPR_Codigo = 1"
        strSQL += " AND Codigo_Usuario ='" & strUser & "'"
        'strSQL += " AND CATA_APLI_Codigo = 202"
        strSQL += " AND Habilitado = 1 "
        strSQL += " AND Clave = '" & strPass & "'"

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrUser As SqlDataReader = ComandoSQL.ExecuteReader

        While sdrUser.Read
            habilitado = CInt(sdrUser("Habilitado"))
        End While

        sdrUser.Close()
        Me.objGeneral.ConexionSQL.Close()


        If habilitado <> 1 Then
            lblMensajeError.Text = "El Usuario no tiene permisos"
        Else
            strSQL = "EXEC gsp_obtener_remesa_paqueteria_control_entregas_PAGINA_ASP " & "1, " & strDocuRemesa & "," & strNumeroPreimpreso
            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrInfo As SqlDataReader = ComandoSQL.ExecuteReader

            While sdrInfo.Read
                ExisteRegistro = True
                Numero_Documento.Text = sdrInfo("Numero_Documento")
                NombreCliente.Text = sdrInfo("NombreCliente")
                Numero = sdrInfo("Numero")
                Dim Anulado As Object
                Anulado = sdrInfo("Anulado")
                CATA_ESRP_Codigo = sdrInfo("CATA_ESRP_Codigo")

                If CATA_ESRP_Codigo = 6030 Then
                    infoEntrega.Style.Add("display", "block")
                    Nombre_Recibe.Text = sdrInfo("Nombre_Recibe")
                    Telefonos_Recibe.Text = sdrInfo("Telefonos_Recibe")
                    Usuario_Entrega.Text = sdrInfo("Usuario_Entrega")
                    Numero_Identificacion_Recibe.Text = sdrInfo("Numero_Identificacion_Recibe")
                    Cantidad_Recibe.Text = String.Format(CultureInfo.InvariantCulture, "{0:#,#}", sdrInfo("Cantidad_Recibe"))
                    Peso.Text = String.Format(CultureInfo.InvariantCulture, "{0:#,#}", sdrInfo("Peso_Recibe"))
                    NovedadRecibe.Text = sdrInfo("NovedadRecibe")
                    Observaciones_Recibe.Text = sdrInfo("Observaciones_Recibe")
                    Dim imagen As Byte() = sdrInfo("Firma_Recibe")
                    imagenFirma.ImageUrl = "data:image;base64," + Convert.ToBase64String(imagen)

                End If
            End While

            Me.objGeneral.ConexionSQL.Close()
            sdrInfo.Close()
            GenerarFoto(Numero)
            GenerarEstados(Numero)

            If ExisteRegistro Then
                infoSeguimiento.Style.Add("display", "block")
            Else
                lblMensajeError.Text = "No se encontro la información solicitada"
            End If

        End If
    End Sub


    Private Sub GenerarFoto(Numero)

        objDocument = New clsGeneral
        Dim ComandoSQL As SqlCommand
        If CATA_ESRP_Codigo = 6030 Then
            strSQLFoto = "EXEC gsp_consultar_foto_detalle_distribucion_remesas " & "1, " & Numero
            ComandoSQL = New SqlCommand(strSQLFoto, Me.objDocument.ConexionSQLDocumental)
            Me.objDocument.ConexionSQLDocumental.Open()
            Dim sdrFoto As SqlDataReader = ComandoSQL.ExecuteReader

            While sdrFoto.Read
                Dim imagenF As Byte() = sdrFoto("Foto")
                imagenFoto.ImageUrl = "data:image;base64," + Convert.ToBase64String(imagenF)
            End While
            sdrFoto.Close()
            Me.objDocument.ConexionSQLDocumental.Close()
        End If
    End Sub
    Private Sub GenerarEstados(Numero)
        objDocument = New clsGeneral
        Dim ComandoSQL As SqlCommand

        strSQLEstado = "EXEC gsp_consultar_detalle_estados_remesas_paqueteria " & "1, " & Numero
        ComandoSQL = New SqlCommand(strSQLEstado, Me.objDocument.ConexionSQL)
        Me.objDocument.ConexionSQL.Open()
        Dim sdrEstados As SqlDataReader = ComandoSQL.ExecuteReader
        Dim arryEstado As List(Of EstadosGuias) = New List(Of EstadosGuias)

        While sdrEstados.Read

            arryEstado.Add(New EstadosGuias With {.ESTADOGUIA = sdrEstados("Estado"),
                           .Fecha = sdrEstados("Fecha_Crea"),
                           .Oficina = sdrEstados("Oficina")})


        End While

        Dim html As String = ""
        For indice As Integer = 0 To arryEstado.Count - 1
            html += "<tr>"
            html += "<td>" & arryEstado(indice).ESTADOGUIA & "</td>"
            html += "<td>" & arryEstado(indice).Fecha & "</td>"
            html += "<td>" & arryEstado(indice).Oficina & "</td>"
            html += "</tr>"
        Next
        divID.InnerHtml = html
        sdrEstados.Close()
        Me.objDocument.ConexionSQL.Close()

    End Sub



End Class

Public Class EstadosGuias
    Dim strESTADOGUIA As String
    Dim strFecha As Date
    Dim strOficina As String

    Public Property ESTADOGUIA() As String
        Get
            Return Me.strESTADOGUIA
        End Get
        Set(ByVal value As String)
            Me.strESTADOGUIA = value
        End Set
    End Property
    Public Property Fecha() As Date
        Get
            Return Me.strFecha
        End Get
        Set(ByVal value As Date)
            Me.strFecha = value
        End Set
    End Property
    Public Property Oficina() As String
        Get
            Return Me.strOficina
        End Get
        Set(ByVal value As String)
            Me.strOficina = value
        End Set
    End Property


End Class
