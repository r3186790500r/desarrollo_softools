﻿PRINT 'gsp_obtener_encabezado_formularios_inspecciones'
GO
DROP PROCEDURE gsp_obtener_encabezado_formularios_inspecciones
GO
CREATE PROCEDURE gsp_obtener_encabezado_formularios_inspecciones 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC  
)  
AS  
BEGIN  
SELECT   
 1 AS Obtener,  
 EMPR_Codigo,  
 Codigo,  
 Codigo_Alterno,  
 Nombre,  
 Estado,  
 TIDO_Codigo_Origen,  
 Version 
FROM  
 Encabezado_Formularios_Inspecciones  
WHERE   
 EMPR_Codigo = @par_EMPR_Codigo   
 AND Codigo = @par_Codigo   
END 
GO 