﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaMarcaSemirremolques
        Implements IPersistenciaBase(Of MarcaSemirremolques)

        Public Function Consultar(filtro As MarcaSemirremolques) As IEnumerable(Of MarcaSemirremolques) Implements IPersistenciaBase(Of MarcaSemirremolques).Consultar
            Return New RepositorioMarcaSemirremolques().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As MarcaSemirremolques) As Long Implements IPersistenciaBase(Of MarcaSemirremolques).Insertar
            Return New RepositorioMarcaSemirremolques().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As MarcaSemirremolques) As Long Implements IPersistenciaBase(Of MarcaSemirremolques).Modificar
            Return New RepositorioMarcaSemirremolques().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As MarcaSemirremolques) As MarcaSemirremolques Implements IPersistenciaBase(Of MarcaSemirremolques).Obtener
            Return New RepositorioMarcaSemirremolques().Obtener(filtro)
        End Function
        Public Function Anular(entidad As MarcaSemirremolques) As Boolean
            Return New RepositorioMarcaSemirremolques().Anular(entidad)
        End Function
    End Class

End Namespace

