﻿Print 'V_Causa_Inactividad_Vehiculo'
GO
DROP VIEW V_Causa_Inactividad_Vehiculo
GO
CREATE VIEW V_Causa_Inactividad_Vehiculo 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 24
GO
