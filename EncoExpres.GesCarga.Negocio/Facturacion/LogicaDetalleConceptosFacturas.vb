﻿Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Namespace Facturacion
    Public Class LogicaDetalleConceptosFacturas
        Inherits LogicaBase(Of DetalleConceptosFacturas)
        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of DetalleConceptosFacturas)

        Sub New(persistencia As IPersistenciaBase(Of Facturas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As DetalleConceptosFacturas) As Respuesta(Of IEnumerable(Of DetalleConceptosFacturas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleConceptosFacturas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleConceptosFacturas))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As DetalleConceptosFacturas) As Respuesta(Of DetalleConceptosFacturas)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Guardar(entidad As DetalleConceptosFacturas) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace

