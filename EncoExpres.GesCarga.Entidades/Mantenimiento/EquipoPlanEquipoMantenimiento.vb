﻿Imports Newtonsoft.Json

Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref="EquipoPlanEquipoMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EquipoPlanEquipoMantenimiento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EquipoPlanEquipoMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EquipoPlanEquipoMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Equipo = New EquipoMantenimiento With {.Codigo = Read(lector, "EQMA_Codigo"), .Nombre = Read(lector, "Nombre_Equipo")}

        End Sub
        <JsonProperty>
        Public Property Equipo As EquipoMantenimiento
        <JsonProperty>
        Public Property PlanMantenimiento As PlanEquipoMantenimiento

    End Class
End Namespace
