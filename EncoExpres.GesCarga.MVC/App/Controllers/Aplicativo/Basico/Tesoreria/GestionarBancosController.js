﻿EncoExpresApp.controller("GestionarBancosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'BancosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, BancosFactory) {

    $scope.Titulo = 'GESTIONAR BANCO';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesorería' }, { Nombre: 'Bancos' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_BANCOS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0
    }

    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVO" },
        { Codigo: 1, Nombre: "ACTIVO" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        ObtenerBanco();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function ObtenerBanco() {
        blockUI.start('Cargando banco código ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando banco Código ' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        BancosFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.Modelo.Codigo = response.data.Datos.Codigo;
                    $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                    $scope.Modelo.Nombre = response.data.Datos.Nombre;
                    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                }
                else {
                    ShowError('No se logro consultar el banco código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarBancos';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarBancos';
            });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    $scope.ConfirmacionGuardarBanco = function () {
        showModal('modalConfirmacionGuardarBanco');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardarBanco');
        if (DatosRequeridos()) {

            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

            BancosFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó el banco "' + $scope.Modelo.Nombre + '"');
                                location.href = '#!ConsultarBancos/' + response.data.Datos;
                            }
                            else {
                                ShowSuccess('Se modificó el banco "' + $scope.Modelo.Nombre + '"');
                                location.href = '#!ConsultarBancos/' + $scope.Modelo.Codigo;
                            }
                        }
                        else {
                            ShowError('No se pudo guardar el banco, por favor verifique que el código que esta intentando ingresar no corresponda a una banco ya creado');
                        }
                    }
                    else {
                        ShowError('No se pudo guardar el banco, por favor verifique que el código que esta intentando ingresar no corresponda a una banco ya creado');
                    }
                }, function (response) {
                    if (response.statusText.includes('IX_Bancos_Codigo_Cuenta')) {
                        ShowError('No se pudo guardar, el código de cuenta ingresado ya está siendo utilizado')
                    } else {
                        ShowError(response.statusText);
                    }
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
            $scope.MensajesError.push('Debe ingresar el nombre del banco');
            continuar = false;
        }
        if ($scope.Modelo.CodigoAlterno == undefined || $scope.Modelo.CodigoAlterno == '' || $scope.Modelo.CodigoAlterno == null || $scope.Modelo.CodigoAlterno == 0) {
            $scope.MensajesError.push('Debe ingresar el codigo alterno');
            continuar = false;
        }
        return continuar;
    }

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarBancos/' + $scope.Modelo.Codigo;
    };
    $scope.MaskMayus = function () {
        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Modelo.Codigo = MascaraNumero($scope.Modelo.Codigo) } catch (e) { }
    };
}]);