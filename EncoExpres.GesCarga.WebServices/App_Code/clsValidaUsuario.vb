﻿Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Fachada.SeguridadUsuarios
Imports EncoExpres.GesCarga.Negocio.Aplicativo.SeguridadUsuarios

Public Class clsValidaUsuario

    Public Function Validar_Usuario(ByVal CodigoEmpresa As Integer, ByVal IdentificadorUsuario As String, ByVal Clave As String, ByVal CodigoAplicacion As Integer, ByRef MensajeProceso As String, ByRef CodigoUsuario As Integer) As Boolean


        Dim bolContinuar As Boolean
        CodigoUsuario = 0
        Try
            Dim datosAutenticacion As New Usuarios
            Dim PersistenciaUsuario = New PersistenciaUsuarios

            datosAutenticacion.CodigoEmpresa = CodigoEmpresa
            datosAutenticacion.CodigoUsuario = IdentificadorUsuario
            datosAutenticacion.Clave = Clave
            datosAutenticacion.Token = 0

            Dim result = New LogicaUsuarios(PersistenciaUsuario).Validar(datosAutenticacion.CodigoEmpresa, datosAutenticacion.CodigoUsuario, datosAutenticacion.Clave, datosAutenticacion.Token)

            bolContinuar = False
            If result.ProcesoExitoso Then
                If result.Datos.Codigo > 0 Then

                    If result.Datos.AplicacionUsuario = CodigoAplicacion Then
                        bolContinuar = True
                        CodigoUsuario = result.Datos.Codigo
                    Else
                        CodigoUsuario = 0
                        result.MensajeOperacion = "El usuario '" & IdentificadorUsuario & "' no es de tipo Aplicación WebService correcto."
                    End If

                End If
            End If

            MensajeProceso = result.MensajeOperacion
            Validar_Usuario = bolContinuar

        Catch ex As Exception
            bolContinuar = False
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(0).GetMethod().Name
            MensajeProceso = "Error en " & Me.ToString & " en la función " & NombreFuncion & ": " & ex.Message.ToString
        End Try
        Return bolContinuar
    End Function
    Public Function Validar_Usuario_Cliente(ByVal CodigoEmpresa As Integer, ByVal IdentificadorUsuario As String, ByVal Clave As String, ByVal CodigoAplicacion As Integer, ByRef MensajeProceso As String, ByRef CodigoUsuario As Integer) As Integer


        Dim bolContinuar As Boolean
        Dim CodigoCliente As Integer
        CodigoUsuario = 0
        Try
            Dim datosAutenticacion As New Usuarios
            Dim PersistenciaUsuario = New PersistenciaUsuarios

            datosAutenticacion.CodigoEmpresa = CodigoEmpresa
            datosAutenticacion.CodigoUsuario = IdentificadorUsuario
            datosAutenticacion.Clave = Clave
            datosAutenticacion.Token = 0

            Dim result = New LogicaUsuarios(PersistenciaUsuario).Validar(datosAutenticacion.CodigoEmpresa, datosAutenticacion.CodigoUsuario, datosAutenticacion.Clave, datosAutenticacion.Token)

            bolContinuar = False
            If result.ProcesoExitoso Then
                If result.Datos.Codigo > 0 Then

                    If result.Datos.AplicacionUsuario = CodigoAplicacion Then
                        bolContinuar = True
                        CodigoUsuario = result.Datos.Codigo
                    Else
                        CodigoUsuario = 0
                        result.MensajeOperacion = "El usuario '" & IdentificadorUsuario & "' no es de tipo Aplicación WebService correcto."
                    End If

                End If
            End If

            MensajeProceso = result.MensajeOperacion
            If bolContinuar Then
                Try
                    CodigoCliente = result.Datos.Cliente.Codigo
                Catch ex As Exception
                    CodigoCliente = 0
                End Try
            End If
        Catch ex As Exception
            bolContinuar = False
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(0).GetMethod().Name
            MensajeProceso = "Error en " & Me.ToString & " en la función " & NombreFuncion & ": " & ex.Message.ToString
        End Try
        Return CodigoCliente
    End Function
End Class
