﻿Imports System.Net.Http
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Routing
Imports Microsoft.Owin.Security.OAuth

''' <summary>
''' Modilo <see cref="WebApiConfig"/>
''' </summary>
Public Module WebApiConfig
    Public Sub Register(config As HttpConfiguration)
        ' Web API configuration and services
        ' Configure Web API to use only bearer token authentication.
        config.SuppressDefaultHostAuthentication()
        config.Filters.Add(New HostAuthenticationFilter(OAuthDefaults.AuthenticationType))
        config.Filters.Add(New ExceptionFilter())
        ' Web API routes
        config.MapHttpAttributeRoutes()

        Dim cors As New EnableCorsAttribute("*", "*", "*")
        config.EnableCors(cors)

        config.Routes.MapHttpRoute("DefaultApi", "api/v1/{controller}")
        config.Routes.MapHttpRoute("DefaultApiWithId", "api/v1/{controller}/{id}", New With {.id = RouteParameter.Optional}, New With {.id = "\\id"})
        config.Routes.MapHttpRoute("DefaultApiWithAction", "api/v1/{controller}/{action}")
        config.Routes.MapHttpRoute("DefaultApiGet", "api/v1/{controller}", New With {.action = "Get"}, New With {.httpMethod = New HttpMethodConstraint(HttpMethod.Get)})
        config.Routes.MapHttpRoute("DefaultApiPost", "api/v1/{controller}", New With {.action = "Post"}, New With {.httpMethod = New HttpMethodConstraint(HttpMethod.Post)})
        config.Routes.MapHttpRoute("DefaultApiPut", "api/v1/{controller}", New With {.action = "Put"}, New With {.httpMethod = New HttpMethodConstraint(HttpMethod.Put)})
        config.Routes.MapHttpRoute("DefaultApiDelete", "api/v1/{controller}", New With {.action = "Delete"}, New With {.httpMethod = New HttpMethodConstraint(HttpMethod.Delete)})

        'Configuración para manejar las fechas en el formato local del servidor.
        config.Formatters.JsonFormatter.SerializerSettings = New Newtonsoft.Json.JsonSerializerSettings With {
            .DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local
        }

    End Sub
End Module
