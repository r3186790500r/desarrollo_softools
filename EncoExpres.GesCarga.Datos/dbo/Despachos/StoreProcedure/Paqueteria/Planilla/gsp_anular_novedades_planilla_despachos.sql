﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 30/09/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_anular_novedades_planilla_despachos'
GO
DROP PROCEDURE gsp_anular_novedades_planilla_despachos
GO 
CREATE PROCEDURE gsp_anular_novedades_planilla_despachos 
(        
@par_EMPR_Codigo SMALLINT,        
@par_Codigo NUMERIC,        
@par_Causa_Anulacion VARCHAR(150),        
@par_USUA_Codigo_Anula SMALLINT        
)        
AS        
BEGIN       
     
 UPDATE Detalle_Novedades_Despacho      
 SET         
 Causa_Anula = @par_Causa_Anulacion,        
 USUA_Codigo_Anula = @par_USUA_Codigo_Anula,        
 Fecha_Anula = GETDATE(),        
 Anulado = 1        
 WHERE        
 EMPR_Codigo = @par_EMPR_Codigo        
 AND ID = @par_Codigo        
    
SELECT @@ROWCOUNT  AS Codigo    
     
END        
GO