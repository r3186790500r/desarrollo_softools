﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class RepositorioDetalleDespachoSolicitudOrdenServicios
        Inherits RepositorioBase(Of DetalleDespachoSolicitudOrdenServicios)

        Public Overrides Function Consultar(filtro As DetalleDespachoSolicitudOrdenServicios) As IEnumerable(Of DetalleDespachoSolicitudOrdenServicios)
            Dim lista As New List(Of DetalleDespachoSolicitudOrdenServicios)
            Dim item As DetalleDespachoSolicitudOrdenServicios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ESOS_Numero", filtro.Numero)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_ID_Codigo", filtro.Codigo)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_despacho_orden_servicios")

                While resultado.Read
                    item = New DetalleDespachoSolicitudOrdenServicios(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleDespachoSolicitudOrdenServicios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleDespachoSolicitudOrdenServicios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleDespachoSolicitudOrdenServicios) As DetalleDespachoSolicitudOrdenServicios
            Throw New NotImplementedException()
        End Function

        Public Function Anular(entidad As DetalleDespachoSolicitudOrdenServicios) As Boolean
            Throw New NotImplementedException()
        End Function

    End Class
End Namespace
