﻿EncoExpresApp.controller("ConsultarControlEntregasCtrl", ['$scope', '$timeout', 'DetalleDistribucionRemesasFactory', '$linq', 'blockUI', 'RemesasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'RemesaGuiasFactory',
    function ($scope, $timeout, DetalleDistribucionRemesasFactory, $linq, blockUI, RemesasFactory, ValorCatalogosFactory, TercerosFactory, RemesaGuiasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Entregas' }, { Nombre: 'Control Entregas' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.DeshabilitarDatosDestinatario = true;
        $scope.DeshabilitarDatosRecibe = false;
        $scope.Destinatario = {};
        $scope.Foto = [];
        $scope.FotoAgregada = 0;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONTROL_ENTREGAS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        document.getElementById("ProductoPaqueteria").click();
        document.getElementById("DestinatarioPaqueteria").click();
        console.clear();
        //----------------------------------------------------------------------------------------------------------------FUNCIONES-----------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------Funciones validar identificacion recibe-------------------------------------------------------
        $scope.VerificarRecibe = function (identificacion) {
            $scope.ValidarIdentificacion = '';
            $scope.ValidarIdentificacion = identificacion;
            if ($scope.ValidarIdentificacion !== '') {
                if ($scope.ValidarIdentificacion === undefined || $scope.ValidarIdentificacion === null || isNaN($scope.ValidarIdentificacion) === true) {
                    ShowInfo('No hay registro de la identificación ingresada , por favor diligenciar todos los campos.');
                    $scope.Modal.NumeroIdentificacion = '';
                    $scope.Modal.NombreCompleto = '';
                    $scope.Modal.Telefonos = '';
                } else {
                    TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroIdentificacion: $scope.ValidarIdentificacion }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    if (response.data.Datos[0].Estado.Codigo === 1) {
                                        item = response.data.Datos[0];
                                        $scope.Modal.NumeroIdentificacion = item.NumeroIdentificacion;
                                        $scope.Modal.NombreCompleto = item.NombreCompleto;
                                        $scope.Modal.Telefonos = item.Telefonos;
                                    } else {
                                        $scope.Modal.NumeroIdentificacion = '';
                                        $scope.Modal.NombreCompleto = '';
                                        $scope.Modal.Telefonos = '';
                                        ShowInfo('El destinatario ingresado se encuentra inactivo en el sistema.');
                                    }
                                } else {
                                    ShowInfo('No hay registro de la identificación ingresada, por favor diligenciar todos los campos.');
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        };

        /*-----------------------------------------------------------------FUNCION BUSCAR REMESAS POR PLACA CONDUCTOR--------------------------------------------------------------------------------- */

        FindRemesas();
        function FindRemesas() {
            $scope.ListaDistribucion = []
            $scope.ListadoNumerosRemesas = [];
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Conductor: { Codigo: $scope.Sesion.UsuarioAutenticado.Conductor.Codigo }
            };

            RemesasFactory.ConsultarNumerosRemesas(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNumerosRemesas.push({ Nombre: '(Seleccione remesa)', Numero: 0 });
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListadoNumerosRemesas.push(response.data.Datos[i]);
                            }
                            for (var i = 0; i < $scope.ListadoNumerosRemesas.length; i++) {
                                if ($scope.ListadoNumerosRemesas[i].Numero > 0) {
                                    $scope.ListadoNumerosRemesas[i].Nombre = $scope.ListadoNumerosRemesas[i].NumeroDocumento.toString();
                                    //Remesa masivo
                                    if ($scope.ListadoNumerosRemesas[i].TipoDocumento.Codigo === 100) {
                                        $scope.ListadoNumerosRemesas[i].Nombre += ' (Masivo)';
                                    }
                                    //Remesa Paqueteria
                                    else {
                                        $scope.ListadoNumerosRemesas[i].Nombre += ' (Paqueteria)';
                                    }
                                }
                            }
                            $scope.Remesa = $scope.ListadoNumerosRemesas[0];
                            $scope.ResultadoSinRegistros = '';
                        } else {
                            $scope.ListadoNumerosRemesas.push({ Nombre: '(No tiene remesas)', Numero: 0 });
                            $scope.Remesa = $scope.ListadoNumerosRemesas[0];
                            $scope.ResultadoSinRegistros = 'No hay remesas para el conductor';
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 203 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoNovedades = response.data.Datos;
                    }
                    else {
                        $scope.ListadoNovedades = []
                    }
                }
            }, function (response) {
            });
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.VerificarNumeroRemesa = function () {

            $timeout(function () {
                blockUI.message('Espere por favor ...');
                //consultar detalles de la remesa
                $scope.ListaDistribucion = [];
                var remesafiltros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Remesa.Numero,
                    Obtener: 1
                };
                RemesasFactory.Obtener(remesafiltros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.NumeroDocumentoRemesa = $scope.Remesa.NumeroDocumento;
                            $scope.NombreCliente = response.data.Datos.Cliente.NombreCompleto;
                            $scope.NombreProductoTransportado = response.data.Datos.ProductoTransportado.Nombre;
                            $scope.NumeroDocumentoCliente = response.data.Datos.NumeroDocumentoCliente;
                            $scope.NumeroCantidad = response.data.Datos.CantidadCliente;
                            $scope.NumeroPeso = response.data.Datos.PesoCliente;
                            //Remesa Paqueteria
                            if (response.data.Datos.TipoDocumento.Codigo == 110) {
                                $scope.RemesaPaqueteria = response.data.Datos;
                                var destinatario = $scope.RemesaPaqueteria.Destinatario;
                                destinatario.Direccion = $scope.RemesaPaqueteria.DireccionDestinatario;
                                $scope.ListaDistribucion.push({
                                    TipoDocumento: $scope.RemesaPaqueteria.TipoDocumento,
                                    Destinatario: destinatario,
                                    Producto: $scope.RemesaPaqueteria.ProductoTransportado,
                                    Cantidad: $scope.RemesaPaqueteria.CantidadCliente
                                });
                            }
                            //Remesa Masivo / Semimasivo
                            else {
                                $scope.NumeroDocumentoRemesa = $scope.Remesa.NumeroDocumento;
                                $scope.NombreCliente = response.data.Datos.Cliente.NombreCompleto;
                                Find();
                            }
                        } else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }, 100);

        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListaDistribucion = [];
            if ($scope.Buscando) {
                filtroDistribucion = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroRemesa: $scope.Remesa.Numero,
                    Estado: 1,
                    Pagina: 1,
                    RegistrosPagina: 1000,
                }
                blockUI.delay = 1000;
                DetalleDistribucionRemesasFactory.Consultar(filtroDistribucion).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.Buscando = true;
                                $scope.ListaDistribucion = response.data.Datos;
                                var recibidos = 0;
                                for (var i = 0; i < $scope.ListaDistribucion.length; i++) {
                                    item = $scope.ListaDistribucion[i];
                                    if (item.UsuarioModifica.Codigo > 0) {
                                        recibidos += 1;
                                    }
                                }
                                if (recibidos === $scope.ListaDistribucion.length) {

                                    var entidad = {
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        Numero: $scope.Remesa.Numero
                                    };
                                    RemesasFactory.ActualizarDistribuido(entidad).
                                        then(function (response) {
                                            if (response.data.ProcesoExitoso === true) {
                                                if (response.data.Datos.Anulado > 0) {
                                                    ShowSuccess('Se guardaron todas las distribuciones para la remesa ' + $scope.Remesa.NumeroDocumento);
                                                    $scope.NombreCliente = '';
                                                    $scope.NombreProductoTransportado = '';
                                                    $scope.NumeroCantidad = '';
                                                    $scope.NumeroPeso = '';
                                                    FindRemesas();
                                                }
                                            }
                                            else {
                                                ShowError(response.data.MensajeOperacion);
                                            }
                                        }, function (response) {
                                        });
                                }

                            } else {
                                $scope.Buscando = false;
                                $scope.ListaDistribucion = [];
                                $scope.ResultadoSinRegistros = 'No hay distribuciones para la remesa';
                            }
                        }
                    }, function (response) {
                    });
            }
            blockUI.stop();
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.RecibirDistribucion = function (item, DevuelveRemesa) {

            $scope.Foto = [];
            $scope.FotoAgregada = 0;
            $scope.Modal = {
                FechaRecibe: new Date()
            };
            $scope.NumeroDocumentoCliente = item.DocumentoCliente;
            $scope.Codigo = item.Codigo
            $scope.MensajesErrorRecibir = [];
            $scope.MostrarModalDetalle = true;
            $scope.ListadoTipoIdentificacion = []
            $scope.itemRemesa = item
            if (DevuelveRemesa) {
                $scope.DevuelveRemesa = true
            }
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {

                            $scope.ListadoTipoIdentificacion = response.data.Datos;

                            if ($scope.CodigoTipoIdentificacionDestinatario !== undefined && $scope.CodigoTipoIdentificacionDestinatario !== '' && $scope.CodigoTipoIdentificacionDestinatario !== null && $scope.CodigoTipoIdentificacionDestinatario !== 0) {
                                $scope.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionDestinatario);
                            } else {
                                $scope.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + CODIGO_NO_APLICA_TIPO_IDENTIFICACION);
                            }

                            if ($scope.CodigoTipoIdentificacionRecibe !== undefined && $scope.CodigoTipoIdentificacionRecibe !== '' && $scope.CodigoTipoIdentificacionRecibe !== null && $scope.CodigoTipoIdentificacionRecibe !== 0) {
                                $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRecibe);
                            } else {
                                $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + CODIGO_NO_APLICA_TIPO_IDENTIFICACION);
                            }

                        }
                        else {
                            $scope.ListadoTipoIdentificacion = []
                        }
                        try {
                            if ($scope.itemRemesa.TipoDocumento.Codigo == 110) {
                                if ($scope.DevuelveRemesa) {
                                    showModal('modalConfirmacionDevolucionRemesa');
                                } else {
                                    AsignarRemesaPaqueteria();
                                    $scope.Modal.NovedadEntrega = $scope.ListadoNovedades[0]
                                    showModal('ModalRecibirDistribucionPaqueteria');
                                }

                            } else {
                                Obtener();
                                showModal('ModalRecibirDistribucion');
                            }
                        } catch (e) {
                            Obtener();
                            showModal('ModalRecibirDistribucion');
                        }
                    }
                }, function (response) {
                });
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };
        $scope.ConfirmacionGuardarPaqueteria = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardarPaqueteria');
            }
        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar', 1);

            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            $scope.Firma = image.src.replace(/data:image\/png;base64,/g, '');

            $scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);

            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                NumeroRemesa: $scope.Remesa.Numero,
                FechaRecibe: $scope.Modal.FechaRecibe,
                CantidadRecibe: $scope.Modal.Cantidad,
                PesoRecibe: $scope.Modal.Peso,
                CodigoTipoIdentificacionRecibe: $scope.Modal.TipoIdentificacionRecibe.Codigo,
                NumeroIdentificacionRecibe: $scope.Modal.NumeroIdentificacion,
                NombreRecibe: $scope.Modal.NombreCompleto,
                TelefonoRecibe: $scope.Modal.Telefonos,
                ObservacionesRecibe: $scope.Modal.Observaciones,
                Fotografia: $scope.Foto[0],
                Firma: $scope.Firma,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            };

            DetalleDistribucionRemesasFactory.Guardar($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('La remesa fue recibida por ' + $scope.Modal.NombreCompleto);
                            closeModal('ModalRecibirDistribucion', 1);
                            Find();
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };
        $scope.DevolverRemesa = function () {
            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Remesa.Numero,
                DevolucionRemesa: 1
            };
            RemesaGuiasFactory.GuardarEntrega($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('Se realizó la devolución de la remesa satisfactoriamente');
                            closeModal('modalConfirmacionDevolucionRemesa', 1);
                            FindRemesas();
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        $scope.GuardarPaqueteria = function () {
            closeModal('modalConfirmacionGuardarPaqueteria', 1);
            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            $scope.Firma = image.src.replace(/data:image\/png;base64,/g, '');

            $scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);

            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Numero: $scope.Remesa.Numero,
                FechaRecibe: $scope.Modal.FechaRecibe,
                CantidadRecibe: $scope.Modal.Cantidad,
                PesoRecibe: $scope.Modal.Peso,
                CodigoTipoIdentificacionRecibe: $scope.Modal.TipoIdentificacionRecibe.Codigo,
                NumeroIdentificacionRecibe: $scope.Modal.NumeroIdentificacion,
                NombreRecibe: $scope.Modal.NombreCompleto,
                TelefonoRecibe: $scope.Modal.Telefonos,
                NovedadEntrega: $scope.Modal.NovedadEntrega,
                ObservacionesRecibe: $scope.Modal.Observaciones,
                Fotografia: $scope.Foto[0],
                Firma: $scope.Firma,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            };

            RemesaGuiasFactory.GuardarEntrega($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('La remesa fue recibida por ' + $scope.Modal.NombreCompleto);
                            closeModal('ModalRecibirDistribucionPaqueteria', 1);
                            FindRemesas();
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        function DatosRequeridos() {
            $scope.MensajesErrorRecibir = [];
            var continuar = true;

            if ($scope.Sesion.UsuarioAutenticado.Oficinas === undefined) {
                $scope.MensajesErrorRecibir.push('El usuario no cuenta con una oficina asignada');
                continuar = false;
            }
            if ($scope.Modal.Cantidad === undefined || $scope.Modal.Cantidad === '' || $scope.Modal.Cantidad === null || $scope.Modal.Cantidad === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if ($scope.Modal.Peso === undefined || $scope.Modal.Peso === '' || $scope.Modal.Peso === null || $scope.Modal.Peso === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el peso');
                continuar = false;
            }
            if ($scope.Modal.TipoIdentificacionRecibe === undefined || $scope.Modal.TipoIdentificacionRecibe === '' || $scope.Modal.TipoIdentificacionRecibe === null || $scope.Modal.TipoIdentificacionRecibe.Codigo === CODIGO_NO_APLICA_TIPO_IDENTIFICACION) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el tipo de identificación');
                continuar = false;
            }
            if ($scope.Modal.NumeroIdentificacion === undefined || $scope.Modal.NumeroIdentificacion === '' || $scope.Modal.NumeroIdentificacion === null || $scope.Modal.NumeroIdentificacion === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el número de identificación');
                continuar = false;
            }
            if ($scope.Modal.NombreCompleto === undefined || $scope.Modal.NombreCompleto === '' || $scope.Modal.NombreCompleto === null || $scope.Modal.NombreCompleto === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el nombre de la persona que recibe');
                continuar = false;
            }
            if ($scope.Modal.Telefonos === undefined || $scope.Modal.Telefonos === '' || $scope.Modal.Telefonos === null || $scope.Modal.Telefonos === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el teléfono');
                continuar = false;
            }
            return continuar;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        function Obtener() {

            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: $scope.Remesa.Numero,
                Codigo: $scope.Codigo
            };

            DetalleDistribucionRemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.DeshabilitarDatosRecibe = false;
                        $scope.Destinatario.Producto = response.data.Datos.Producto.Nombre;
                        $scope.DestinatarioCantidad = MascaraValores(response.data.Datos.Cantidad);
                        $scope.DestinatarioPeso = MascaraValores(response.data.Datos.Peso);
                        $scope.Destinatario.Cantidad = response.data.Datos.Cantidad;
                        $scope.Destinatario.Peso = response.data.Datos.Peso;
                        $scope.CodigoTipoIdentificacionDestinatario = response.data.Datos.Destinatario.TipoIdentificacion.Codigo
                        if ($scope.ListadoTipoIdentificacion.length > 0 && $scope.CodigoTipoIdentificacionDestinatario > 0) {
                            $scope.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.Destinatario.TipoIdentificacion.Codigo);
                        }
                        $scope.Destinatario.NumeroIdentificacion = response.data.Datos.Destinatario.NumeroIdentificacion;
                        $scope.Destinatario.NombreCompleto = response.data.Datos.Destinatario.NombreCompleto;
                        $scope.Destinatario.Ciudad = response.data.Datos.Destinatario.Ciudad.Nombre;
                        if (response.data.Datos.ZonaDestinatario === undefined) {
                            $scope.Destinatario.ZonaDestinatario = '(NO APLICA)';
                        } else {
                            $scope.Destinatario.ZonaDestinatario = response.data.Datos.ZonaDestinatario.Nombre;
                        }
                        $scope.Destinatario.SitioEntrega = response.data.Datos.SitioEntrega;
                        $scope.Destinatario.Barrio = response.data.Datos.Destinatario.Barrio;
                        $scope.Destinatario.Direccion = response.data.Datos.Destinatario.Direccion;
                        $scope.Destinatario.CodigoPostal = response.data.Datos.Destinatario.CodigoPostal;
                        $scope.Destinatario.Telefonos = response.data.Datos.Destinatario.Telefonos;
                        $scope.CodigoUsuarioModifica = response.data.Datos.UsuarioModifica.Codigo;
                        if ($scope.CodigoUsuarioModifica > 0) {
                            $scope.CodigoTipoIdentificacionRecibe = response.data.Datos.CodigoTipoIdentificacionRecibe
                            if ($scope.ListadoTipoIdentificacion.length > 0 && $scope.CodigoTipoIdentificacionRecibe > 0) {
                                $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.CodigoTipoIdentificacionRecibe);
                            }
                            $scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);
                            $scope.Modal.NumeroIdentificacion = response.data.Datos.NumeroIdentificacionRecibe;
                            $scope.Modal.Cantidad = response.data.Datos.CantidadRecibe;
                            $scope.Modal.Peso = response.data.Datos.PesoRecibe;
                            $scope.Modal.NombreCompleto = response.data.Datos.NombreRecibe;
                            $scope.Modal.Telefonos = response.data.Datos.TelefonoRecibe;
                            $scope.Modal.Observaciones = response.data.Datos.ObservacionesRecibe;
                            $scope.DeshabilitarDatosRecibe = true;
                        } else {
                            $scope.CodigoTipoIdentificacionRecibe = response.data.Datos.CodigoTipoIdentificacionRecibe
                            if ($scope.ListadoTipoIdentificacion.length > 0 && $scope.CodigoTipoIdentificacionRecibe == 0) {
                                $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.Destinatario.TipoIdentificacion.Codigo);
                            }
                            $scope.Modal.NumeroIdentificacion = response.data.Datos.Destinatario.NumeroIdentificacion;
                            $scope.Modal.Cantidad = response.data.Datos.Cantidad;
                            $scope.Modal.Peso = response.data.Datos.Peso;
                            $scope.Modal.NombreCompleto = response.data.Datos.Destinatario.NombreCompleto;
                            $scope.Modal.Telefonos = response.data.Datos.Destinatario.Telefonos;
                            $scope.Modal.Observaciones = '';
                            $scope.DeshabilitarDatosRecibe = false;
                            $scope.VerificarRecibe($scope.Modal.NumeroIdentificacion);
                        }
                        if (response.data.Datos.Fotografia.FotoBits !== undefined && response.data.Datos.Fotografia.FotoBits !== null && response.data.Datos.Fotografia.FotoBits !== '') {
                            $scope.Foto = [];
                            $scope.Foto.push({
                                NombreFoto: response.data.Datos.Fotografia.NombreFoto,
                                TipoFoto: response.data.Datos.Fotografia.TipoFoto,
                                ExtensionFoto: response.data.Datos.Fotografia.ExtensionFoto,
                                FotoBits: response.data.Datos.Fotografia.FotoBits,
                                ValorDocumento: 1,
                                FotoCargada: 'data:' + response.data.Datos.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Fotografia.FotoBits
                            });
                        } else {
                            $scope.Foto = [];
                        }
                        if ($scope.Foto.length > 0) {
                            $scope.FotoAgregada = 1
                        } else {
                            $scope.FotoAgregada = 0
                        }

                        $scope.LimpiarFirma();
                        $timeout(function () {
                            blockUI.message('Espere por favor ...');
                            var image = new Image();
                            image.src = "data:image/png;base64," + response.data.Datos.Firma
                            var img = cargaContextoCanvas('Firma')
                            img.drawImage(image, 0, 0)
                        }, 150);

                    }
                    else {
                        ShowError('No se logró consultar la distribución ' + response.data.MensajeOperacion);
                        closeModal('ModalRecibirDistribucion', 1);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    closeModal('ModalRecibirDistribucion', 1);
                });
            blockUI.stop();
        };
        function AsignarRemesaPaqueteria(item) {

            $scope.DeshabilitarDatosRecibe = false;
            $scope.Destinatario.Producto = $scope.RemesaPaqueteria.ProductoTransportado.Nombre;
            $scope.Destinatario.Cantidad = $scope.RemesaPaqueteria.CantidadCliente;
            $scope.Destinatario.Peso = $scope.RemesaPaqueteria.PesoCliente;
            $scope.CodigoTipoIdentificacionDestinatario = $scope.RemesaPaqueteria.Destinatario.TipoIdentificacion.Codigo
            if ($scope.ListadoTipoIdentificacion.length > 0 && $scope.CodigoTipoIdentificacionDestinatario > 0) {
                $scope.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + $scope.RemesaPaqueteria.Destinatario.TipoIdentificacion.Codigo);
            }
            $scope.Destinatario.NumeroIdentificacion = $scope.RemesaPaqueteria.Destinatario.NumeroIdentificacion;
            $scope.Destinatario.NombreCompleto = $scope.RemesaPaqueteria.Destinatario.NombreCompleto;
            $scope.Destinatario.Ciudad = $scope.RemesaPaqueteria.Destinatario.Ciudad.Nombre;
            if ($scope.RemesaPaqueteria.Destinatario.ZonaDestinatario === undefined) {
                $scope.Destinatario.ZonaDestinatario = '(NO APLICA)';
            } else {
                $scope.Destinatario.ZonaDestinatario = $scope.RemesaPaqueteria.Destinatario.ZonaDestinatario.Nombre;
            }
            //$scope.Destinatario.Barrio = $scope.RemesaPaqueteria.Destinatario.Barrio;
            $scope.Destinatario.Direccion = $scope.RemesaPaqueteria.Destinatario.Direccion;
            $scope.Destinatario.CodigoPostal = $scope.RemesaPaqueteria.Destinatario.CodigoPostal;
            $scope.Destinatario.Telefonos = $scope.RemesaPaqueteria.Destinatario.Telefonos;
            if ($scope.RemesaPaqueteria.Entregado > 0) {
                $scope.CodigoTipoIdentificacionRecibe = $scope.RemesaPaqueteria.CodigoTipoIdentificacionRecibe
                if ($scope.ListadoTipoIdentificacion.length > 0 && $scope.CodigoTipoIdentificacionRecibe > 0) {
                    $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + $scope.RemesaPaqueteria.CodigoTipoIdentificacionRecibe);
                }
                $scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);
                $scope.Modal.NumeroIdentificacion = $scope.RemesaPaqueteria.NumeroIdentificacionRecibe;
                $scope.Modal.Cantidad = $scope.RemesaPaqueteria.CantidadRecibe;
                $scope.Modal.Peso = $scope.RemesaPaqueteria.PesoRecibe;
                $scope.Modal.NombreCompleto = $scope.RemesaPaqueteria.NombreRecibe;
                $scope.Modal.Telefonos = $scope.RemesaPaqueteria.TelefonoRecibe;
                $scope.Modal.Observaciones = $scope.RemesaPaqueteria.ObservacionesRecibe;
                $scope.DeshabilitarDatosRecibe = true;
            } else {
                $scope.CodigoTipoIdentificacionRecibe = $scope.RemesaPaqueteria.CodigoTipoIdentificacionRecibe
                if ($scope.ListadoTipoIdentificacion.length > 0 && $scope.CodigoTipoIdentificacionRecibe == 0) {
                    $scope.Modal.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + $scope.RemesaPaqueteria.Destinatario.TipoIdentificacion.Codigo);
                }
                $scope.Modal.NumeroIdentificacion = $scope.RemesaPaqueteria.Destinatario.NumeroIdentificacion;
                $scope.Modal.Cantidad = $scope.RemesaPaqueteria.CantidadCliente;
                $scope.Modal.Peso = $scope.RemesaPaqueteria.PesoCliente;
                $scope.Modal.NombreCompleto = $scope.RemesaPaqueteria.Destinatario.NombreCompleto;
                $scope.Modal.Telefonos = $scope.RemesaPaqueteria.Destinatario.Telefonos;
                $scope.Modal.Observaciones = '';
                $scope.DeshabilitarDatosRecibe = false;
            }
            try {
                if ($scope.RemesaPaqueteria.Fotografia.FotoBits !== undefined && $scope.RemesaPaqueteria.Fotografia.FotoBits !== null && $scope.RemesaPaqueteria.Fotografia.FotoBits !== '') {
                    $scope.Foto = [];
                    $scope.Foto.push({
                        NombreFoto: $scope.RemesaPaqueteria.Fotografia.NombreFoto,
                        TipoFoto: $scope.RemesaPaqueteria.Fotografia.TipoFoto,
                        ExtensionFoto: $scope.RemesaPaqueteria.Fotografia.ExtensionFoto,
                        FotoBits: $scope.RemesaPaqueteria.Fotografia.FotoBits,
                        ValorDocumento: 1,
                        FotoCargada: 'data:' + $scope.RemesaPaqueteria.Fotografia.TipoFoto + ';base64,' + $scope.RemesaPaqueteria.Fotografia.FotoBits
                    });
                } else {
                    $scope.Foto = [];
                }
            } catch (e) {
                $scope.Foto = [];
            }
            $scope.LimpiarFirma();
            $timeout(function () {
                blockUI.message('Espere por favor ...');
                var image = new Image();
                image.src = "data:image/png;base64," + $scope.RemesaPaqueteria.Firma
                var img = cargaContextoCanvas('Firma')
                img.drawImage(image, 0, 0)
            }, 150);
        }
        $scope.VerificarCantidad = function () {
            if (MascaraDecimales($scope.Modal.Cantidad) > MascaraDecimales($scope.Destinatario.Cantidad)) {
                ShowError('La cantidad recibida no puede ser mayor a la cantidad designada en el producto')
                $scope.Modal.Cantidad = $scope.Destinatario.Cantidad
            }
        }
        $scope.VerificarPeso = function () {
            if (MascaraDecimales($scope.Modal.Peso) > MascaraDecimales($scope.Destinatario.Cantidad)) {
                ShowError('El peso recibido no puede ser mayor al peso designado en el producto')
                $scope.Modal.Cantidad = $scope.Destinatario.Peso
            }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConsultarFoto = function () {
            showModal('ModalFoto')
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.TomarFoto = function () {
            showModal('ModalTomarFoto')

            // References to all the element we will need.
            $scope.video = document.querySelector('#camera-stream');
            var image = document.querySelector('#snap'),
                start_camera = document.querySelector('#start-camera'),
                controls = document.querySelector('.controls'),
                take_photo_btn = document.querySelector('#take-photo'),
                delete_photo_btn = document.querySelector('#delete-photo'),
                download_photo_btn = document.querySelector('#save-photo'),
                error_message = document.querySelector('#error-message');

            // The getUserMedia interface is used for handling camera input.
            // Some browsers need a prefix so here we're covering all the options
            navigator.getMedia = (navigator.getUserMedia ||
                navigator.webkitGetUserMedia ||
                navigator.mozGetUserMedia ||
                navigator.msGetUserMedia);

            if (!navigator.getMedia) {
                displayErrorMessage("Your browser doesn't have support for the navigator.getUserMedia interface.");
            }
            else {

                // Request the camera.
                navigator.getMedia(
                    {
                        video: true
                    },
                    // Success Callback
                    function (Stream) {

                        // Create an object URL for the video stream and
                        // set it as src of our HTLM video element.
                        $scope.video.srcObject = Stream;

                        // Play the video element to start the stream.
                        $scope.video.play();
                        $scope.video.onplay = function () {
                            showVideo();
                        };

                    },
                    // Error Callback
                    function (err) {
                        displayErrorMessage("There was an error with accessing the camera stream: " + err.name, err);
                    }
                );
            }

            // Mobile browsers cannot play video without user input,
            // so here we're using a button to start it manually.
            start_camera.addEventListener("click", function (e) {

                e.preventDefault();

                // Start video playback manually.
                $scope.video.play();
                showVideo();

            });

            take_photo_btn.addEventListener("click", function (e) {

                e.preventDefault();

                var snap = takeSnapshot();

                // Show image. 
                image.setAttribute('src', snap);
                image.classList.add("visible");

                // Enable delete and save buttons
                delete_photo_btn.classList.remove("disabled");
                download_photo_btn.classList.remove("disabled");
                take_photo_btn.classList.add("disabled")

                // Pause video playback of stream.
                $scope.video.pause();

            });

            delete_photo_btn.addEventListener("click", function (e) {

                e.preventDefault();

                // Hide image.
                image.setAttribute('src', "");
                image.classList.remove("visible");

                // Disable delete and save buttons
                delete_photo_btn.classList.add("disabled");
                download_photo_btn.classList.add("disabled");
                take_photo_btn.classList.remove("disabled")

                // Resume playback of stream.
                $scope.video.play();

            });

            function showVideo() {
                // Display the video stream and the controls.

                hideUI();
                $scope.video.classList.add("visible");
                controls.classList.add("visible");
                $timeout(function () {
                    try {
                        document.getElementById('delete-photo').click();
                    } catch (e) {

                    }

                }, 1000)
            }

            function takeSnapshot() {
                // Here we're using a trick that involves a hidden canvas element.  

                var hidden_canvas = document.querySelector('canvas.FotoRecibe'),
                    context = hidden_canvas.getContext('2d');

                var width = $scope.video.videoWidth,
                    height = $scope.video.videoHeight;

                if (width && height) {

                    // Setup a canvas with the same dimensions as the video.
                    hidden_canvas.width = width;
                    hidden_canvas.height = height;

                    // Make a copy of the current frame in the video on the canvas.
                    context.drawImage($scope.video, 0, 0, width, height);

                    // Turn the canvas image into a dataURL that can be used as a src for our photo.
                    $scope.FotoTomada = hidden_canvas.toDataURL('image/png');
                    return $scope.FotoTomada;
                }
            }

            function displayErrorMessage(error_msg, error) {
                error = error || "";
                if (error) {
                    console.log(error);
                }

                error_message.innerText = error_msg;

                hideUI();
                error_message.classList.add("visible");
            }

            function hideUI() {
                // Helper function for clearing the app UI.

                controls.classList.remove("visible");
                start_camera.classList.remove("visible");
                $scope.video.classList.remove("visible");
                snap.classList.remove("visible");
                error_message.classList.remove("visible");
            }


        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.CargarImagen = function (element) {
            var continuar = true;
            $scope.TipoImagen = element
            $scope.NombreDocumento = element.files[0].name
            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato JPEG, PNG", false);
                continuar = false;
            }

            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png') {
                    ShowError("Solo se pueden cargar archivos en formato JPEG, PNG ", false);
                    continuar = false;
                }
            }

            if (continuar == true) {
                var reader = new FileReader();
                reader.onload = $scope.AsignarImagenListado;
                reader.readAsDataURL(element.files[0]);
            }

        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.AsignarImagenListado = function (e) {
            try {
                RedimHorizontal('')
                RedimVertical('')
            } catch (e) {
            }
            $scope.$apply(function () {
                $scope.Convertirfoto = null
                var TipoImagen = ''

                var name = $scope.TipoImagen.files[0].name.split('.')

                $scope.NombreFoto = name[0]

                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/jpeg'
                        AsignarFotoListado()

                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/png'
                        AsignarFotoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.Convertirfoto = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoFoto = 'application/pdf'
                    $scope.TipoImagen = TipoImagen
                    AsignarFotoListado()

                }
                var input = document.getElementById('inputfile')
                input.value = ''
            });
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.AsignarFoto = function () {
            closeModal('ModalTomarFoto', 1)
            $scope.video.srcObject = undefined
            $scope.Foto = [];
            $scope.FotoAux = $scope.FotoTomada.toString();
            $scope.FotoBits = $scope.FotoAux.replace(/data:image\/png;base64,/g, '')
            $scope.Foto.push({
                NombreFoto: 'Fotografía',
                TipoFoto: 'image/png',
                ExtensionFoto: 'PNG',
                FotoBits: $scope.FotoBits,
                ValorDocumento: 1,
                FotoCargada: $scope.FotoAux
            });
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        function AsignarFotoListado() {
            $scope.Foto = [];
            $scope.Foto.push({
                NombreFoto: $scope.NombreFoto,
                TipoFoto: $scope.TipoFoto,
                ExtensionFoto: $scope.TipoImagen,
                FotoBits: $scope.Convertirfoto,
                ValorDocumento: 1,
                FotoCargada: 'data:' + $scope.TipoFoto + ';base64,' + $scope.Convertirfoto
            });
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.GuardarFoto = function () {
            if ($scope.ModificarFoto == true && $scope.Foto.length == CERO) {
                closeModal('ModalFoto', 1);
            }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.LimpiarFirma = function () {
            var canvas = document.getElementsByClassName('Firma');
            var context = canvas[0].getContext('2d')
            context.clearRect(0, 0, canvas[0].width, canvas[0].height);
            var context = canvas[1].getContext('2d')
            context.clearRect(0, 0, canvas[1].width, canvas[1].height);
            //var canvas = document.getElementById('Firma');
            //var context = canvas.getContext("2d");
            //context.clearRect(0, 0, canvas.width, canvas.height);
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }

        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.CerrarModalFoto = function () {
            closeModal('ModalFoto', 1);
            if ($scope.Foto.length > 0) {
                $scope.FotoAgregada = 1
            } else {
                $scope.FotoAgregada = 0
            }
        };

        $scope.CerrarModalTomarFoto = function () {
            closeModal('ModalTomarFoto', 1);
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.EliminarFoto = function (item) {
            $scope.Foto = [];
            $scope.ModificarFoto = true;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MaskTelefono = function (option) {
            try { $scope.Modal.Telefonos = MascaraTelefono($scope.Modal.Telefonos) } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };

    }]);