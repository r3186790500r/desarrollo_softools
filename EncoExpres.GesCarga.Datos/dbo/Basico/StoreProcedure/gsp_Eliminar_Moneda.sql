﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_Eliminar_Moneda'
GO
DROP PROCEDURE gsp_Eliminar_Moneda
GO
CREATE PROCEDURE gsp_Eliminar_Moneda(  
@par_EMPR_Codigo NUMERIC,  
@par_Codigo NUMERIC
) 
AS BEGIN  
DECLARE @Codigo smallint = 0 		
DELETE Tasa_Cambio_Monedas FROM Tasa_Cambio_Monedas TACM, Monedas MONE WHERE
TACM.EMPR_Codigo=  MONE.EMPR_Codigo AND  
TACM.MONE_Codigo = MONE.Codigo AND 
MONE.Local = 0 AND
TACM.EMPR_Codigo = @par_EMPR_Codigo AND  
MONE_Codigo = @par_Codigo

DELETE Monedas 
WHERE 
Local=0 
AND EMPR_Codigo = @par_EMPR_Codigo 
AND  Codigo = @par_Codigo

SELECT @@ROWCOUNT AS Codigo

END  
GO