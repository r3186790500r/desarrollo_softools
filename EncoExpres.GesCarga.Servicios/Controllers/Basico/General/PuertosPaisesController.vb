﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="PuertosPaisesController"/>
''' </summary>
<Authorize>
Public Class PuertosPaisesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PuertosPaises) As Respuesta(Of IEnumerable(Of PuertosPaises))
        Return New LogicaPuertosPaises(CapaPersistenciaPuertosPaises).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PuertosPaises) As Respuesta(Of PuertosPaises)
        Return New LogicaPuertosPaises(CapaPersistenciaPuertosPaises).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PuertosPaises) As Respuesta(Of Long)
        Return New LogicaPuertosPaises(CapaPersistenciaPuertosPaises).Guardar(entidad)
    End Function

    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PuertosPaises) As Respuesta(Of Boolean)
        Return New LogicaPuertosPaises(CapaPersistenciaPuertosPaises).Anular(entidad)
    End Function

End Class
