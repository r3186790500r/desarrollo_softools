﻿PRINT 'gps_Obtenter_Encabezado_Liquidacion_Despachos_SIESA'
GO
DROP PROCEDURE gps_Obtenter_Encabezado_Liquidacion_Despachos_SIESA
GO
CREATE PROCEDURE gps_Obtenter_Encabezado_Liquidacion_Despachos_SIESA
(
	@par_EMPR_Codigo smallint
)
AS
BEGIN
	SELECT TOP 5 
	ENCC.EMPR_Codigo,
	ENCC.Numero,
	ENCC.Fecha,
	ENCC.Numero_Documento,
	ENCC.Fecha_Documento,
	ENCC.TERC_Documento,
	ENCC.OFIC_Documento,
	ENCC.CATA_DOOR_Codigo,
	ENCC.Observaciones,
	ENCC.Valor_Comprobante,
	ENCC.OFIC_Codigo,
	ISNULL(ENCC.Interfase_Contable, 0) AS Interfase_Contable, 
	ENCC.Anulado,
	TERC.Numero_Identificacion AS Identificacion_Tercero,
	VEHI.Placa

	FROM Encabezado_Comprobante_Contables AS ENCC

	LEFT JOIN Terceros AS TERC ON
	ENCC.EMPR_Codigo = TERC.EMPR_Codigo
	AND ENCC.TERC_Documento = TERC.Codigo

	LEFT JOIN Encabezado_Liquidacion_Planilla_Despachos AS ENLD ON
	ENCC.EMPR_Codigo = ENLD.EMPR_Codigo
	AND ENCC.Codigo_Documento = ENLD.Numero

	LEFT JOIN Encabezado_Planilla_Despachos AS ENPD ON
	ENLD.EMPR_Codigo = ENPD.EMPR_Codigo
	AND ENLD.ENPD_Numero = ENPD.Numero

	LEFT JOIN Vehiculos AS VEHI ON
	ENPD.EMPR_Codigo = VEHI.EMPR_Codigo
	AND ENPD.VEHI_Codigo = VEHI.Codigo

	WHERE ENCC.EMPR_Codigo = @par_EMPR_Codigo
	And ENCC.CATA_DOOR_Codigo = 2602--Liquidacion
	AND ISNULL(ENCC.Interfase_Contable, 0) = 0
	AND ISNULL(ENCC.Intentos_Interfase_Contable, 0) < 3
	AND ENCC.Anulado = 0
	ORDER BY ENCC.Numero
END
GO