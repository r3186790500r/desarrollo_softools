﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class PersistenciaPlanillaGuias
        Implements IPersistenciaBase(Of PlanillaGuias)

        Public Function Consultar(filtro As PlanillaGuias) As IEnumerable(Of PlanillaGuias) Implements IPersistenciaBase(Of PlanillaGuias).Consultar
            Return New RepositorioPlanillaGuias().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PlanillaGuias) As Long Implements IPersistenciaBase(Of PlanillaGuias).Insertar
            Return New RepositorioPlanillaGuias().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PlanillaGuias) As Long Implements IPersistenciaBase(Of PlanillaGuias).Modificar
            Return New RepositorioPlanillaGuias().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PlanillaGuias) As PlanillaGuias Implements IPersistenciaBase(Of PlanillaGuias).Obtener
            Return New RepositorioPlanillaGuias().Obtener(filtro)
        End Function
        Public Function Anular(entidad As PlanillaGuias) As PlanillaGuias
            Return New RepositorioPlanillaGuias().Anular(entidad)
        End Function
        Public Function EliminarGuia(entidad As PlanillaGuias) As Boolean
            Return New RepositorioPlanillaGuias().EliminarGuia(entidad)
        End Function
        Public Function EliminarRecoleccion(entidad As PlanillaGuias) As Boolean
            Return New RepositorioPlanillaGuias().EliminarRecoleccion(entidad)
        End Function
    End Class

End Namespace

