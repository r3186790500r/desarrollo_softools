﻿EncoExpresApp.controller("GestionarRendimientoGalonCombustibleCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'RendimientoGalonCombustibleFactory', 'ValorCatalogosFactory', 'RutasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, RendimientoGalonCombustibleFactory, ValorCatalogosFactory, RutasFactory) {

        $scope.Titulo = 'GESTIONAR RENDIMIENTO GALÓN';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Rendimiento Galón' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + 100214);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVO" },
            { Codigo: 1, Nombre: "ACTIVO" },
        ]


        $scope.CargarDatosFunciones = function () {
            /*Cargar el combo de tipo vehiculo*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoVehiculo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoVehiculo = response.data.Datos;
                            try {
                                $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo ==' + $scope.Modelo.TipoVehiculo.Codigo);
                                
                            } catch (e) {
                                $scope.Modelo.TipoVehiculo = $scope.ListadoTipoVehiculo[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoVehiculo = []
                        }
                    }
                }, function (response) {
                });
            RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoRutas = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoRutas = response.data.Datos
                            try {
                                $scope.Modelo.Ruta = $linq.Enumerable().From($scope.ListadoRutas).First('$.Codigo ==' + $scope.Modelo.Ruta.Codigo);
                            } catch (e) {
                            }
                        }
                        else {
                            $scope.ListadoRutas = []
                        }
                    }
                }, function (response) {
                });
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando rendimiento código No. ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando rendimiento Código No.' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            RendimientoGalonCombustibleFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)
                        $scope.CargarDatosFunciones()
                        $scope.MaskValores()
                    }
                    else {
                        ShowError('No se logro consultar el rendimiento galón. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarRendimientoGalonCombustible';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarRendimientoGalonCombustible';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () { 
            showModal('modalConfirmacionGuardar')
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar')
            if (DatosRequeridos()) {
                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo; 
                RendimientoGalonCombustibleFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo === 0) {
                                    ShowSuccess('Se guardó el rendimiento para la ruta "' + $scope.Modelo.Ruta.Nombre + '"');
                                    location.href = '#!ConsultarRendimientoGalonCombustible/' + response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó el rendimiento para la ruta "' + $scope.Modelo.Ruta.Nombre + '"');
                                    location.href = '#!ConsultarRendimientoGalonCombustible/' + $scope.Modelo.Codigo;
                                }
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError('No se pudo guardar el rendimiento galón. ' + response.statusText);
                    });
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Ruta === undefined || $scope.Modelo.Ruta === '') {
                $scope.MensajesError.push('Debe ingresar la ruta');
                continuar = false;
            }

            if ($scope.Modelo.TipoVehiculo === undefined || $scope.Modelo.TipoVehiculo === '') {
                $scope.MensajesError.push('Debe ingresar el tipo vehículo');
                continuar = false;
            } else {
                if ($scope.Modelo.TipoVehiculo.Codigo === 2200) {
                    $scope.MensajesError.push('Debe ingresar el tipo vehículo');
                    continuar = false;
                }
            } 
            if (!($scope.Modelo.PesoDesde === undefined || $scope.Modelo.PesoDesde === '') && !($scope.Modelo.PesoHasta === undefined || $scope.Modelo.PesoHasta === '' || $scope.Modelo.PesoHasta === 0)) {
                if (parseInt(MascaraNumero($scope.Modelo.PesoDesde)) > parseInt(MascaraNumero($scope.Modelo.PesoHasta))) {
                    $scope.MensajesError.push('El peso hasta debe ser mayor al peso desde');
                    continuar = false;
                }
            }
            if ($scope.Modelo.RendimientoGalon === undefined || $scope.Modelo.RendimientoGalon === '' || $scope.Modelo.RendimientoGalon === 0) {
                $scope.MensajesError.push('Debe ingresar el rendimiento galón');
                continuar = false;
            }
         
            return continuar;
        }

        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
            $scope.CargarDatosFunciones()
        }
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope);
        };
        $scope.MaskValores = function () {
            return MascaraValoresGeneral($scope);
        };
        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarRendimientoGalonCombustible/' + $scope.Modelo.Codigo;
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        };
        $scope.MaskMayus = function (option) {
            MascaraMayusGeneral($scope)
        };
    }]);