﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gps_reporte_detalle_auxiliar_planilla_entregada' 
DROP PROCEDURE gps_reporte_detalle_auxiliar_planilla_entregada
GO

CREATE PROCEDURE gps_reporte_detalle_auxiliar_planilla_entregada
(@par_EMPR_Codigo NUMERIC,
@par_numero_planilla NUMERIC 
) AS 
BEGIN 
SELECT 
ISNULL(FUNC.Razon_Social,'')+' '+ISNULL(FUNC.Nombre,'')  
+' '+ISNULL(FUNC.Apellido1,'')+' '+ISNULL(FUNC.Apellido2,'') AS Nombre_Funcionario
,DAPE.Numero_Horas_Trabajadas
,DAPE.Valor 
,DAPE.Observaciones
 FROM Detalle_Auxiliares_Planilla_Entregas DAPE
INNER JOIN Terceros FUNC
ON  DAPE.EMPR_Codigo = FUNC.EMPR_Codigo 
AND DAPE.TERC_Codigo_Funcionario = FUNC.Codigo 

WHERE DAPE.EMPR_Codigo = @par_EMPR_Codigo
AND DAPE.ENPE_Numero = @par_numero_planilla
END 
GO