﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	Print 'gsp_insertar_detalle_auxiliar_planilla_entregada'
GO

DROP PROCEDURE gsp_insertar_detalle_auxiliar_planilla_entregada
GO

CREATE PROCEDURE gsp_insertar_detalle_auxiliar_planilla_entregada (      
@par_EMPR_Codigo NUMERIC,
@par_Numero_Planilla NUMERIC,
@par_Terc_Funcionario NUMERIC,
@par_Horas_trabajadas NUMERIC, 
@par_Valor MONEY,
@par_Observaciones VARCHAR(200) = NULL 
)     
AS BEGIN   
 INSERT INTO Detalle_Auxiliares_Planilla_Entregas(  
         EMPR_Codigo,
		 ENPE_Numero,
		 TERC_Codigo_Funcionario,
		 Numero_Horas_Trabajadas,
		 Valor,
		 Observaciones
		 )      
         VALUES(    
        @par_EMPR_Codigo,
		@par_Numero_Planilla,
		@par_Terc_Funcionario,
		@par_Horas_trabajadas, 
	    @par_Valor, 
		ISNULL(@par_Observaciones,'')
		)  
  
  SELECT @@ROWCOUNT As CantidadRegistros
     
END  
GO