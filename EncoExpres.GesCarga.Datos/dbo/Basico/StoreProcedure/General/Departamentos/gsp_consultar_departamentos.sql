﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 13/08/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_consultar_departamentos'
GO
DROP PROCEDURE gsp_consultar_departamentos
GO
CREATE PROCEDURE  gsp_consultar_departamentos 
(  
  @par_EMPR_Codigo SMALLINT,  
  @par_Codigo NUMERIC = NULL,  
  @par_Nombre VARCHAR(50) = NULL,  
  @par_Codigo_Alterno VARCHAR(20) = NULL,  
  @par_Nombre_Pais VARCHAR(50) = NULL, 
  @par_Estado SMALLINT = NULL,  
  @par_NumeroPagina INT = NULL,  
  @par_RegistrosPagina INT = NULL  
 )  
 AS  
 BEGIN  
  SET NOCOUNT ON;  
   DECLARE  
    @CantidadRegistros INT  
   SELECT @CantidadRegistros = (  
     SELECT DISTINCT   
      COUNT(1)   
      FROM  
      Departamentos DEPA,  
      Paises  PAIS  
      WHERE  
      DEPA.EMPR_Codigo = @par_EMPR_Codigo  
      AND DEPA.EMPR_Codigo = PAIS.EMPR_Codigo   
      AND DEPA.PAIS_Codigo = PAIS.Codigo  
      AND DEPA.Codigo > 0  
      AND DEPA.Codigo = ISNULL(@par_Codigo, DEPA.Codigo)  
      AND DEPA.Estado = ISNULL(@par_Estado, DEPA.Estado)  
	  AND ((PAIS.Nombre LIKE '%' + @par_Nombre_Pais + '%') OR (@par_Nombre_Pais IS NULL))  
      AND ((DEPA.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
      AND ((DEPA.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))  
      );  
              
     WITH Pagina AS  
     (  
  
     SELECT  
      DEPA.EMPR_Codigo,  
      DEPA.Codigo,  
      DEPA.PAIS_Codigo,  
      DEPA.Codigo_Alterno,  
      DEPA.Nombre,  
      DEPA.Estado,  
      PAIS.Nombre AS Pais,  
      ROW_NUMBER() OVER(ORDER BY DEPA.Nombre) AS RowNumber  
      FROM  
      Departamentos DEPA,  
      Paises  PAIS  
      WHERE  
      DEPA.EMPR_Codigo = @par_EMPR_Codigo
	  AND DEPA.EMPR_Codigo = PAIS.EMPR_Codigo   
      AND DEPA.PAIS_Codigo = PAIS.Codigo  
      AND DEPA.Codigo > 0  
      AND DEPA.Codigo = ISNULL(@par_Codigo, DEPA.Codigo)  
      AND DEPA.Estado = ISNULL(@par_Estado, DEPA.Estado)  
	  AND ((PAIS.Nombre LIKE '%' + @par_Nombre_Pais + '%') OR (@par_Nombre_Pais IS NULL))
	  AND ((DEPA.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
      AND ((DEPA.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))  
   )  
   SELECT DISTINCT  
   0 AS Obtener,  
   EMPR_Codigo,  
   Codigo,  
   PAIS_Codigo,  
   Codigo_Alterno,  
   Nombre,  
   Estado,  
   Pais,  
   @CantidadRegistros AS TotalRegistros,  
   @par_NumeroPagina AS PaginaObtener,  
   @par_RegistrosPagina AS RegistrosPagina  
   FROM  
    Pagina  
   WHERE  
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
   order by Nombre  
   END  
GO