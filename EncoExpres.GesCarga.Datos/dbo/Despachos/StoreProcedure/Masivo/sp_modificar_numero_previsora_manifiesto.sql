﻿Print 'sp_modificar_numero_previsora_manifiesto'
GO
DROP PROCEDURE sp_modificar_numero_previsora_manifiesto
GO
CREATE PROCEDURE sp_modificar_numero_previsora_manifiesto      
(      
 @par_EMPR_Codigo INT,      
 @par_Numero INT,      
 @par_NumeroConfirmacionEmpresa VARCHAR(20),      
 @par_MensajeEmpresa VARCHAR(200)      
)      
AS      
BEGIN      
 UPDATE      
  Encabezado_Manifiesto_Carga      
 SET      
  Numero_Confirmacion_Empresa_Aseguradora = @par_NumeroConfirmacionEmpresa,      
  Mensaje_Empresa_Aseguradora = @par_MensajeEmpresa    ,  
  Fecha_Reporte_Empresa_Aseguradora = GETDATE() ,
  Reporto_Empresa_Aseguradora = CASE WHEN CONVERT(NUMERIC,@par_NumeroConfirmacionEmpresa) > 0 THEN 1  ELSE 0 END
 WHERE      
  EMPR_Codigo = @par_EMPR_Codigo      
  AND Numero = @par_Numero      
END      
GO