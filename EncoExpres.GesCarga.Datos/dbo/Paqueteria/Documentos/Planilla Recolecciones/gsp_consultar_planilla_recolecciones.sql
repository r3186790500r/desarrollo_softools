﻿PRINT 'gsp_consultar_planilla_recolecciones'
GO
DROP PROCEDURE gsp_consultar_planilla_recolecciones
GO 
CREATE PROCEDURE gsp_consultar_planilla_recolecciones( 
  @par_EMPR_Codigo SMALLINT,    
  @par_Numero NUMERIC = NULL,              
  @par_Numero_Documento NUMERIC = NULL,      
  @par_Fecha_Inicial DATETIME =NULL,                          
  @par_Fecha_Final DATETIME =NULL,                  
  @par_OFIC_Codigo NUMERIC = NULL,          
  @par_Placa VARCHAR(20) = NULL,      
  @par_Nombre_Conductor VARCHAR(100) = NULL,      
  @par_Estado SMALLINT = NULL,     
  @par_Anulado SMALLINT = NULL,         
  @par_NumeroPagina INT = NULL,           
  @par_RegistrosPagina INT = NULL           
  )          
AS          
BEGIN        
    
 set @par_Fecha_Inicial = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Inicial)                     
 set @par_Fecha_Inicial = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Inicial)),@par_Fecha_Inicial)                            
 set @par_Fecha_Inicial = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Inicial)),@par_Fecha_Inicial)                              
 set @par_Fecha_Final = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Final)                          
 set @par_Fecha_Final = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Final)),@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Final)),@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(MILLISECOND, 998,@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(SECOND,59,@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(MINUTE,59,@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(HOUR,23,@par_Fecha_Final)       
      
  SET NOCOUNT ON;          
  DECLARE @CantidadRegistros INT          
               
  BEGIN          
   SELECT @CantidadRegistros = (          
    SELECT DISTINCT           
    COUNT(1)           
    FROM           
     Encabezado_Planilla_Recolecciones ENPR              
        
  LEFT JOIN Oficinas AS OFIC                       
     ON ENPR.EMPR_Codigo = OFIC.EMPR_Codigo                        
     AND ENPR.OFIC_Codigo = OFIC.Codigo        
    
  LEFT JOIN Terceros AS TERC                       
     ON ENPR.EMPR_Codigo = TERC.EMPR_Codigo                        
     AND ENPR.TERC_Codigo_Conductor = TERC.Codigo      
            
  LEFT JOIN Vehiculos AS VEHI                       
     ON ENPR.EMPR_Codigo = VEHI.EMPR_Codigo                        
     AND ENPR.VEHI_Codigo = VEHI.Codigo      
       
   WHERE                
     ENPR.EMPR_Codigo = @par_EMPR_Codigo         
  AND ENPR.Numero = ISNULL(@par_Numero, ENPR.Numero)         
    AND ENPR.Numero_Documento = ISNULL(@par_Numero_Documento, ENPR.Numero_Documento)        
  AND ENPR.Fecha >= (ISNULL(@par_Fecha_Inicial, ENPR.Fecha))                            
     AND ENPR.Fecha <= (ISNULL(@par_Fecha_Final, ENPR.Fecha))          
     AND OFIC.Codigo = ISNULL(@par_OFIC_Codigo, OFIC.Codigo)    
  AND ((VEHI.Placa LIKE '%' + RTRIM(LTRIM(@par_Placa)) + '%' OR (@par_Placa IS NULL)))              
     AND ENPR.Estado = ISNULL(@par_Estado, ENPR.Estado)          
  AND ENPR.Anulado = ISNULL(@par_Anulado, ENPR.Anulado)        
   );                 
  WITH Pagina AS          
  (          
  SELECT DISTINCT     
     1 AS Obtener     
     ,ENPR.Numero    
  ,ENPR.EMPR_Codigo    
     ,ENPR.Numero_Documento          
     ,ENPR.Fecha AS Fecha_Planilla        
  ,ISNULL(ENPR.VEHI_Codigo, '') AS Codigo_Vehiculo     
  ,ISNULL(VEHI.Placa, '') AS Placa_Vehiculo     
  ,ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1, '')+ ' ' + ISNULL(TERC.Apellido2,'')+ ' ' + ISNULL(TERC.Razon_Social,'') AS Nombre_Conductor      
  ,ISNULL(ENPR.TERC_Codigo_Conductor, 0) AS TERC_Codigo_Conductor        
  ,ISNULL(ENPR.OFIC_Codigo, 0) AS Codigo_Oficina        
  ,ISNULL(OFIC.Nombre,'') AS Nombre_Oficina            
     ,ISNULL(ENPR.Cantidad_Total, 0) AS Cantidad_Total      
  ,ISNULL(ENPR.Peso_Total, '') AS Peso_Total          
  ,ISNULL(ENPR.Observaciones, '') AS Observaciones        
  ,CASE WHEN ENPR.Estado = 1 AND ENPR.Anulado = 0 OR ENPR.Estado = 1 THEN 1     
  WHEN ENPR.Estado = 0 AND ENPR.Anulado = 0 OR ENPR.Estado = 0 THEN 2     
  WHEN ENPR.Anulado = 1 THEN 3 END AS Estado               
     ,ROW_NUMBER() OVER(ORDER BY ENPR.Numero) AS RowNumber          
                 
  FROM           
     Encabezado_Planilla_Recolecciones ENPR              
        
  LEFT JOIN Oficinas AS OFIC                       
     ON ENPR.EMPR_Codigo = OFIC.EMPR_Codigo                        
     AND ENPR.OFIC_Codigo = OFIC.Codigo        
    
  LEFT JOIN Terceros AS TERC                       
     ON ENPR.EMPR_Codigo = TERC.EMPR_Codigo                        
     AND ENPR.TERC_Codigo_Conductor = TERC.Codigo      
            
  LEFT JOIN Vehiculos AS VEHI                       
     ON ENPR.EMPR_Codigo = VEHI.EMPR_Codigo                        
     AND ENPR.VEHI_Codigo = VEHI.Codigo      
       
   WHERE                
     ENPR.EMPR_Codigo = @par_EMPR_Codigo    
  AND ENPR.Numero = ISNULL(@par_Numero, ENPR.Numero)           
  AND ENPR.Numero_Documento = ISNULL(@par_Numero_Documento, ENPR.Numero_Documento)      
  AND ENPR.Fecha >= (ISNULL(@par_Fecha_Inicial, ENPR.Fecha))                            
     AND ENPR.Fecha <= (ISNULL(@par_Fecha_Final, ENPR.Fecha))          
     AND OFIC.Codigo = ISNULL(@par_OFIC_Codigo, OFIC.Codigo)     
  AND ((VEHI.Placa LIKE '%' + RTRIM(LTRIM(@par_Placa)) + '%' OR (@par_Placa IS NULL)))             
     AND ENPR.Estado = ISNULL(@par_Estado, ENPR.Estado)          
  AND ENPR.Anulado = ISNULL(@par_Anulado, ENPR.Anulado)       
    )          
          
   SELECT      
      Obtener     
     ,Numero      
  ,EMPR_Codigo      
     ,Numero_Documento          
     ,Fecha_Planilla      
  ,Codigo_Vehiculo      
  ,Placa_Vehiculo     
  ,Nombre_Conductor     
  ,TERC_Codigo_Conductor       
  ,Codigo_Oficina     
  ,Nombre_Oficina            
     ,Cantidad_Total      
  ,Peso_Total       
  ,Observaciones    
  ,Estado           
     ,@CantidadRegistros AS TotalRegistros          
     ,@par_NumeroPagina AS PaginaObtener          
     ,@par_RegistrosPagina AS RegistrosPagina          
   FROM          
    Pagina          
   WHERE          
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)          
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)          
   ORDER BY Numero ASC          
  END       
END
GO   