﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaPuertosPaises
        Implements IPersistenciaBase(Of PuertosPaises)

        Public Function Consultar(filtro As PuertosPaises) As IEnumerable(Of PuertosPaises) Implements IPersistenciaBase(Of PuertosPaises).Consultar
            Return New RepositorioPuertosPaises().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PuertosPaises) As Long Implements IPersistenciaBase(Of PuertosPaises).Insertar
            Return New RepositorioPuertosPaises().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PuertosPaises) As Long Implements IPersistenciaBase(Of PuertosPaises).Modificar
            Return New RepositorioPuertosPaises().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PuertosPaises) As PuertosPaises Implements IPersistenciaBase(Of PuertosPaises).Obtener
            Return New RepositorioPuertosPaises().Obtener(filtro)
        End Function

        Public Function Anular(entidad As PuertosPaises) As Boolean
            Return New RepositorioPuertosPaises().Anular(entidad)
        End Function

    End Class

End Namespace

