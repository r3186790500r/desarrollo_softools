﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports System.Transactions

Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="RepositorioEncabezadoCheckListVehiculos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEncabezadoCheckListVehiculos
        Inherits RepositorioBase(Of EncabezadoCheckListVehiculos)

        Public Overrides Function Consultar(filtro As EncabezadoCheckListVehiculos) As IEnumerable(Of EncabezadoCheckListVehiculos)
            Dim lista As New List(Of EncabezadoCheckListVehiculos)
            Dim item As EncabezadoCheckListVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.Placa) Then
                    conexion.AgregarParametroSQL("@par_Placa", filtro.Placa)
                End If
                If (filtro.FechaInicial > Date.MinValue) And (filtro.FechaFinal >= filtro.FechaInicial) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroDocumento", filtro.NumeroDocumento)
                End If

                If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_check_list_vehiculos]")

                While resultado.Read
                    item = New EncabezadoCheckListVehiculos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As EncabezadoCheckListVehiculos) As Long
            Dim inserto As Boolean = True
            Dim insertoDetalle As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    If (entidad.FechaVigencia > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia", entidad.FechaVigencia, SqlDbType.DateTime)
                    End If
                    conexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_Kilometros_Vehiculo", entidad.KilometrosVehiculo)
                    conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", entidad.Responsable.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_check_list_vehiculos")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While

                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.NumeroDocumento = entidad.Numero

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoDetalle = False
                            inserto = False
                            Exit For
                        End If
                    Next


                End Using
                If entidad.Numero > Cero Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using
            Return entidad.Numero
        End Function
        Public Function InsertarDetalle(entidad As DetalleCheckListVehiculos, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ECLV_Numero", entidad.NumeroDocumento)
            contextoConexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
            contextoConexion.AgregarParametroSQL("@par_Orden_Formulario", entidad.OrdenFormulario)
            contextoConexion.AgregarParametroSQL("@par_CATA_TPCL_Codigo", entidad.TipoPregunta.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_TCON_Codigo", entidad.TipoControl.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Valor", entidad.Valor)
            contextoConexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_check_list_vehiculos]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As EncabezadoCheckListVehiculos) As Long
            Dim inserto As Boolean = True
            Dim insertoDetalle As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Vigencia", entidad.FechaVigencia, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_Kilometros_Vehiculo", entidad.KilometrosVehiculo)
                    conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", entidad.Responsable.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_check_list_vehiculos")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.NumeroDocumento = entidad.Numero

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoDetalle = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using

            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As EncabezadoCheckListVehiculos) As EncabezadoCheckListVehiculos
            Dim item As New EncabezadoCheckListVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_check_list_vehiculos]")

                While resultado.Read
                    item = New EncabezadoCheckListVehiculos(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As EncabezadoCheckListVehiculos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_anular_encabezado_check_list_vehiculos]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace