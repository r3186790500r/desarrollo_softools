﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Fidelizacion.Documentos
Imports EncoExpres.GesCarga.Fachada.Fidelizacion.Documentos
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Fidelizacion.Documentos
    Public NotInheritable Class LogicaDetalleDespachosPuntosVehiculos
        Inherits LogicaBase(Of DetalleDespachosPuntosVehiculos)

        ReadOnly _persistencia As IPersistenciaBase(Of DetalleDespachosPuntosVehiculos)

        Public Sub New(capaPersistenciaDetalleDespachoPuntosVehiculos As IPersistenciaBase(Of DetalleDespachosPuntosVehiculos))
            _persistencia = capaPersistenciaDetalleDespachoPuntosVehiculos
        End Sub

        Public Overrides Function Consultar(filtro As DetalleDespachosPuntosVehiculos) As Respuesta(Of IEnumerable(Of DetalleDespachosPuntosVehiculos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleDespachosPuntosVehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleDespachosPuntosVehiculos))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As DetalleDespachosPuntosVehiculos) As Respuesta(Of DetalleDespachosPuntosVehiculos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of DetalleDespachosPuntosVehiculos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of DetalleDespachosPuntosVehiculos)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As DetalleDespachosPuntosVehiculos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function Anular(entidad As DetalleDespachosPuntosVehiculos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaDetalleDespachosPuntosVehiculos).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
    End Class
End Namespace

