USE [ENCOEXPRES]
GO

delete from [dbo].[Menu_Aplicaciones] where codigo in (47010301,40030401);
go 

delete from [dbo].[Permiso_Grupo_Usuarios] where MEAP_Codigo in (47010301,40030401);
go

declare @par_empr_codigo smallint = 1

--planilla paq
insert into [dbo].[Menu_Aplicaciones] (
EMPR_Codigo,Codigo,MOAP_Codigo,Nombre,Padre_Menu,Orden_Menu,Mostrar_Menu,Aplica_Habilitar,Aplica_Consultar,Aplica_Actualizar,Aplica_Eliminar_Anular,Aplica_Imprimir,Aplica_Contabilidad,Opcion_Permiso,Opcion_Listado,Aplica_Ayuda,Url_Pagina,Url_Ayuda,Imagen,Aplica_Crear
) values (
@par_empr_codigo,47010301,47,'Consultar Novedades',470103,10,0,1,1,1,1,1,0,1,0,0,NULL,NULL,NULL,1
);

--planilla desp
insert into [dbo].[Menu_Aplicaciones] (
EMPR_Codigo,Codigo,MOAP_Codigo,Nombre,Padre_Menu,Orden_Menu,Mostrar_Menu,Aplica_Habilitar,Aplica_Consultar,Aplica_Actualizar,Aplica_Eliminar_Anular,Aplica_Imprimir,Aplica_Contabilidad,Opcion_Permiso,Opcion_Listado,Aplica_Ayuda,Url_Pagina,Url_Ayuda,Imagen,Aplica_Crear
) values (
@par_empr_codigo,40030401,47,'Consultar Novedades',400304,10,0,1,1,1,1,1,0,1,0,0,NULL,NULL,NULL,1
);

INSERT INTO 
 Permiso_Grupo_Usuarios  
 SELECT EMPR_Codigo  
 ,47010301  
 ,Codigo  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0
 ,0
 FROM Grupo_Usuarios   
 WHERE EMPR_Codigo = @par_empr_codigo

INSERT INTO 
 Permiso_Grupo_Usuarios  
 SELECT EMPR_Codigo  
 ,40030401  
 ,Codigo  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0  
 ,0
 ,0
 FROM Grupo_Usuarios   
 WHERE EMPR_Codigo = @par_empr_codigo  
 go