﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Public Class TerceroSIESA

    <JsonProperty>
    Public Property Estado As String
    <JsonProperty>
    Public Property TipoTercero As String
    <JsonProperty>
    Public Property DescripcionSucursal As String
    <JsonProperty>
    Public Property Sucursal As String
    <JsonProperty>
    Public Property Nit As String
    <JsonProperty>
    Public Property Nombres As String
    <JsonProperty>
    Public Property Apellido1 As String
    <JsonProperty>
    Public Property Apellido2 As String
    <JsonProperty>
    Public Property RazonSocial As String
    <JsonProperty>
    Public Property NombreEstablecimiento As String
    <JsonProperty>
    Public Property IdVendedor As String
    <JsonProperty>
    Public Property CedulaVendedor As String
    <JsonProperty>
    Public Property NombreVendedor As String
    <JsonProperty>
    Public Property IdCobrador As String
    <JsonProperty>
    Public Property CedulaCobrador As String
    <JsonProperty>
    Public Property NombreCobrador As String
    <JsonProperty>
    Public Property Contacto As String
    <JsonProperty>
    Public Property Ciudad As String
    <JsonProperty>
    Public Property CodigoCiudad As String
    <JsonProperty>
    Public Property Direccion As String
    <JsonProperty>
    Public Property Telefono As String
    <JsonProperty>
    Public Property Celular As String
    <JsonProperty>
    Public Property Email As String
    <JsonProperty>
    Public Property TipoCliente As String
    <JsonProperty>
    Public Property Calificacion As String
    <JsonProperty>
    Public Property CondicionPago As String
    <JsonProperty>
    Public Property Cupo As String
    <JsonProperty>
    Public Property IdCia As String

End Class
