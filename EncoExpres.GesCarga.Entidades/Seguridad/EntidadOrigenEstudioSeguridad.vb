﻿Imports Newtonsoft.Json

Namespace Seguridad

    ''' <summary>
    ''' Clase <see cref="EntidadOrigenEstudioSeguridad"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EntidadOrigenEstudioSeguridad
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EntidadOrigenEstudioSeguridad"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EntidadOrigenEstudioSeguridad"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")

        End Sub

        <JsonProperty>
        Public Property Nombre As String

    End Class
End Namespace
