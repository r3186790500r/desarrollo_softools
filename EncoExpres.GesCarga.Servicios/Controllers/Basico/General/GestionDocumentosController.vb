﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="GestionDocumentosController"/>
''' </summary>
<Authorize>
Public Class GestionDocumentosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ConfiguracionGestionDocumentos) As Respuesta(Of IEnumerable(Of ConfiguracionGestionDocumentos))
        Return New LogicaGestionDocumentos(CapaPersistenciaConfiguracionGestionDocumentos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ConfiguracionGestionDocumentos) As Respuesta(Of ConfiguracionGestionDocumentos)
        Return New LogicaGestionDocumentos(CapaPersistenciaConfiguracionGestionDocumentos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ConfiguracionGestionDocumentos) As Respuesta(Of Long)
        Return New LogicaGestionDocumentos(CapaPersistenciaConfiguracionGestionDocumentos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ConfiguracionGestionDocumentos) As Respuesta(Of Boolean)
        Return New LogicaGestionDocumentos(CapaPersistenciaConfiguracionGestionDocumentos).Anular(entidad)
    End Function
End Class
