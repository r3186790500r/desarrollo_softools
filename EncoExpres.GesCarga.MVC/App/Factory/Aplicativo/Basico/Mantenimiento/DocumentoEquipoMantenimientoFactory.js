﻿EncoExpresApp.factory('DocumentoEquipoMantenimientoFactory', ['$http', function ($http) {

    var urlService = GetUrl('DocumentoEquipoMantenimiento');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.InsertarTemporal = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/InsertarTemporal';
        request.data = filtro;
        return $http(request);
    };
    factory.EliminarDocumento = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/EliminarDocumento';
        request.data = filtro;
        return $http(request);
    };
    return factory;
}]);