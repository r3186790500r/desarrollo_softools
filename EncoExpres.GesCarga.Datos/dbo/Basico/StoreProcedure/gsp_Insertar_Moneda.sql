﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_Insertar_Moneda'
GO
DROP PROCEDURE gsp_Insertar_Moneda
GO
CREATE PROCEDURE gsp_Insertar_Moneda(  
@par_EMPR_Codigo NUMERIC,  
@par_Nombre VARCHAR(20),   
@par_Nombre_Corto VARCHAR(10),   
@par_Simbolo VARCHAR(5),   
@par_Local SMALLINT = null,   
@par_Estado SMALLINT)  
  
AS BEGIN   
INSERT INTO Monedas (EMPR_Codigo,Nombre,Nombre_Corto,Simbolo,Local,Estado)  
VALUES(@par_EMPR_Codigo,@par_Nombre,@par_Nombre_Corto,@par_Simbolo,@par_Local,@par_Estado)  
select @@IDENTITY as Codigo
END  
GO 