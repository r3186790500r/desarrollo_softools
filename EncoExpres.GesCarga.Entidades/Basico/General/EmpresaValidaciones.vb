﻿Imports Newtonsoft.Json
Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="EmpresaValidaciones"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EmpresaValidaciones
        Inherits BaseDocumento
        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="EmpresaValidaciones"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EmpresaValidaciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "Codigo_Empresa")
            Nombre = Read(lector, "Nombre")
            Codigo = Read(lector, "Codigo")

        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Codigo As Integer


    End Class

End Namespace
