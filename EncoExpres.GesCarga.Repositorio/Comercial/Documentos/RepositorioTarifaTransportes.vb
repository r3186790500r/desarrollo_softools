﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref="RepositorioTarifaTransportes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTarifaTransportes
        Inherits RepositorioBase(Of TarifaTransportes)

        Public Overrides Function Consultar(filtro As TarifaTransportes) As IEnumerable(Of TarifaTransportes)
            Dim lista As New List(Of TarifaTransportes)
            Dim item As TarifaTransportes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                If filtro.OpcionPlantilla > 0 Then
                    conexion.AgregarParametroSQL("@par_LNTC_Codigo", filtro.TipoLineaNegocioTransporte.LineaNegocioTransporte.Codigo)
                    conexion.AgregarParametroSQL("@par_TLNC_Codigo", filtro.TipoLineaNegocioTransporte.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tarifa_transportes_plantilla]")
                ElseIf filtro.OPOtrasEmpresas > 0 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tipo_tarifa]")
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tarifa_transportes]")
                End If


                While resultado.Read
                    item = New TarifaTransportes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As TarifaTransportes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As TarifaTransportes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As TarifaTransportes) As TarifaTransportes
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As TarifaTransportes) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace