﻿EncoExpresApp.factory('GestionDocumentalDocumentosFactory', ['$http', function ($http) {

    var urlService = GetUrl('GestionDocumentalDocumentos');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }
    factory.InsertarTemporal = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/InsertarTemporal';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.EliminarDocumento = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/EliminarDocumento';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.EliminarDocumentoDefinitivo = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/EliminarDocumentoDefinitivo';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.LimpiarDocumentoTemporalUsuario = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/LimpiarDocumentoTemporalUsuario';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    return factory; 
}]);