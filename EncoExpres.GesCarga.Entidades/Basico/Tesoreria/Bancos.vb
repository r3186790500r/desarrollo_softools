﻿Imports Newtonsoft.Json

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="Bancos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Bancos
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="Bancos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Bancos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            'EG: Parte de la entidad usada para devolver listas pequeñas
            Obtener = Read(lector, "Obtener")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")

            If Obtener = 0 Then 'EG: Consulta
                TotalRegistros = Read(lector, "TotalRegistros")
                CodigoAlterno = Read(lector, "Codigo_Alterno")
                Estado = Read(lector, "Estado")

            Else 'EG: Obtener

                CodigoAlterno = Read(lector, "Codigo_Alterno")
                Estado = Read(lector, "Estado")

            End If
        End Sub

        <JsonProperty>
        Public Property Nombre As String

    End Class

End Namespace
