﻿PRINT 'gsp_consultar_encabezado_facturas'
GO
DROP PROCEDURE gsp_consultar_encabezado_facturas
GO
CREATE PROCEDURE gsp_consultar_encabezado_facturas(
	@par_EMPR_Codigo  smallint,                      
	@par_Numero    numeric = NULL,                      
	@par_Numero_Documento numeric = NULL,              
	@par_CATA_TIFA_Codigo     numeric = NULL,              
	@par_Fecha_Inicial  date = NULL,                      
	@par_Fecha_Final  date = NULL,                      
	@par_TERC_Cliente  numeric = NULL,                      
	@par_TERC_Facturar  numeric = NULL,                      
	@par_Estado    smallint = NULL,                      
	@par_Anulado   smallint = NULL,                      
	@par_OFIC_Facturar  smallint = NULL,    
	@par_CATA_FPVE_Codigo numeric = NULL,    
	@par_Usuario_Consulta INT  = NULL,        
	@par_NumeroPagina INT = NULL,                      
	@par_RegistrosPagina INT = NULL                 
)                      
AS                      
BEGIN    
	DECLARE @CantidadRegistros NUMERIC                
               
	SELECT @CantidadRegistros = (    
		SELECT DISTINCT COUNT(1)    
		FROM Encabezado_Facturas ENFA                   
                  
		LEFT JOIN                  
		Factura_Electronica FAEL ON                  
		ENFA.EMPR_Codigo = FAEL.EMPR_Codigo                  
		AND ENFA.Numero = FAEL.ENFA_Numero                  
                   
		LEFT JOIN                  
		Empresa_Factura_Electronica EMFE ON                  
		ENFA.EMPR_Codigo = EMFE.EMPR_Codigo                  
                       
		INNER JOIN                      
		Oficinas OFIC ON                      
		ENFA.EMPR_Codigo = OFIC.EMPR_Codigo                      
		AND ENFA.OFIC_Factura = OFIC.Codigo                      
                      
		INNER JOIN                      
		Terceros CLIE ON                      
		ENFA.EMPR_Codigo = CLIE.EMPR_Codigo                      
		AND ENFA.TERC_Cliente = CLIE.Codigo                      
                      
		INNER JOIN                      
		Terceros TEFA ON                      
		ENFA.EMPR_Codigo = TEFA.EMPR_Codigo                      
		AND ENFA.TERC_Facturar = TEFA.Codigo                      
                      
		INNER JOIN                      
		V_Forma_Pago_Ventas FPVE ON                      
		ENFA.EMPR_Codigo = FPVE.EMPR_Codigo                      
		AND ENFA.CATA_FPVE_Codigo = FPVE.Codigo                      
                      
		WHERE                      
		ENFA.EMPR_Codigo = @par_EMPR_Codigo               
		AND (ENFA.CATA_TIFA_Codigo = @par_CATA_TIFA_Codigo OR @par_CATA_TIFA_Codigo IS NULL)              
		AND (ENFA.Numero = @par_Numero OR @par_Numero IS NULL)                      
		AND (ENFA.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)              
		AND (ENFA.Fecha >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)                      
		AND (ENFA.Fecha <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)                      
		AND (ENFA.TERC_Cliente = @par_TERC_Cliente OR @par_TERC_Cliente IS NULL)                      
		AND (ENFA.TERC_Facturar = @par_TERC_Facturar OR @par_TERC_Facturar IS NULL)                      
		AND (ENFA.OFIC_Factura = @par_OFIC_Facturar OR @par_OFIC_Facturar IS NULL)    
		AND (ENFA.CATA_FPVE_Codigo = @par_CATA_FPVE_Codigo OR @par_CATA_FPVE_Codigo IS NULL)    
		AND (ENFA.Estado = @par_Estado OR @par_Estado IS NULL)                
		AND (ENFA.Anulado = @par_Anulado OR @par_Anulado IS NULL)              
		AND (OFIC.Codigo IN (                SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE EMPR_Codigo = @par_EMPR_Codigo AND USOF.USUA_Codigo = @par_Usuario_Consulta               ) OR @par_Usuario_Consulta IS NULL)                   
	);                      
	WITH Pagina    
	AS                      
	(                      
		SELECT                       
                      
		0 As Obtener,                      
		ENFA.EMPR_Codigo,                      
		ENFA.Numero,                      
		ENFA.TIDO_Codigo,                      
		ENFA.Numero_Documento,                      
		ENFA.Numero_Preimpreso,               
		ENFA.CATA_TIFA_Codigo,               
		ENFA.Fecha,                      
		ENFA.OFIC_Factura,                      
		OFIC.Nombre As OficinaFactura,                      
                   
		ENFA.TERC_Cliente,                      
		ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'') As NombreCliente,                      
		ENFA.TERC_Facturar,                      
		ISNULL(TEFA.Razon_Social,'') + ISNULL(TEFA.Nombre,'') + ' ' + ISNULL(TEFA.Apellido1,'') + ' ' + ISNULL(TEFA.Apellido2,'') As NombreFacturarA,                      
		ENFA.CATA_FPVE_Codigo,                      
		FPVE.Nombre As CATA_FPVE_Nombre,                      
                      
		ENFA.Valor_Factura,                      
		ENFA.Valor_Remesas,                   
		ISNULL(ENFA.Valor_Otros_Conceptos,0) As Valor_Otros_Conceptos,                    
		ISNULL(ENFA.Valor_Impuestos,0) As Valor_Impuestos,                  
		ISNULL(ENFA.Factura_Electronica,0) As Estado_FAEL,                       
		ENFA.Estado,                      
		ENFA.Anulado,                      
		ENFA.Fecha_Crea,                      
		ENFA.USUA_Codigo_Crea,                      
		ENFA.Fecha_Modifica,                      
		ENFA.USUA_Codigo_Modifica,     
		ENFA.Causa_Anula,  
		ISNULL(FAEL.ID_Pruebas, '') AS ID_Pruebas,      
		ISNULL(FAEL.ID_Habilitacion, '') AS ID_Habilitacion,      
		ISNULL(FAEL.ID_Emision, '') AS ID_Emision,      
		(SELECT COUNT(*) FROM Encabezado_Remesas WHERE EMPR_Codigo = ENFA.EMPR_Codigo AND ENFA_Numero = ENFA.Numero and TIDO_Codigo = 110) AS RemesasPaqueteria,
		ISNULL(ENFA.Interfaz_Contable_Factura, 0) Interfaz_Contable_Factura,
		ISNULL(ENFA.Fecha_Interfase_Contable, '') Fecha_Interfase_Contable,
		ISNULL(ENFA.Intentos_Interfase_Contable, 0) Intentos_Interfase_Contable,
		ISNULL(ENFA.Mensaje_Error, '') Mensaje_Error,
		ROW_NUMBER() OVER (ORDER BY ENFA.Numero) AS RowNumber                      
                      
		FROM Encabezado_Facturas ENFA                      
                   
		LEFT JOIN                  
		Factura_Electronica FAEL ON                  
		ENFA.EMPR_Codigo = FAEL.EMPR_Codigo                  
		AND ENFA.Numero = FAEL.ENFA_Numero                  
                   
		INNER JOIN                      
		Oficinas OFIC ON                      
		ENFA.EMPR_Codigo = OFIC.EMPR_Codigo                      
		AND ENFA.OFIC_Factura = OFIC.Codigo                      
                      
		INNER JOIN                      
		Terceros CLIE ON                      
		ENFA.EMPR_Codigo = CLIE.EMPR_Codigo                      
		AND ENFA.TERC_Cliente = CLIE.Codigo                      
                      
		INNER JOIN                      
		Terceros TEFA ON                      
		ENFA.EMPR_Codigo = TEFA.EMPR_Codigo                      
		AND ENFA.TERC_Facturar = TEFA.Codigo                      
                      
		INNER JOIN                      
		V_Forma_Pago_Ventas FPVE ON                      
		ENFA.EMPR_Codigo = FPVE.EMPR_Codigo                      
		AND ENFA.CATA_FPVE_Codigo = FPVE.Codigo                      
                      
		WHERE                      
		ENFA.EMPR_Codigo = @par_EMPR_Codigo                      
		AND (ENFA.CATA_TIFA_Codigo = @par_CATA_TIFA_Codigo OR @par_CATA_TIFA_Codigo IS NULL)              
		AND (ENFA.Numero = @par_Numero OR @par_Numero IS NULL)                      
		AND (ENFA.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)              
		AND (ENFA.Fecha >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)                      
		AND (ENFA.Fecha <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)                      
		AND (ENFA.TERC_Cliente = @par_TERC_Cliente OR @par_TERC_Cliente IS NULL)                      
		AND (ENFA.TERC_Facturar = @par_TERC_Facturar OR @par_TERC_Facturar IS NULL)                      
		AND (ENFA.OFIC_Factura = @par_OFIC_Facturar OR @par_OFIC_Facturar IS NULL)    
		AND (ENFA.CATA_FPVE_Codigo = @par_CATA_FPVE_Codigo OR @par_CATA_FPVE_Codigo IS NULL)                      
		AND (ENFA.Estado = @par_Estado OR @par_Estado IS NULL)                      
		AND (ENFA.Anulado = @par_Anulado OR @par_Anulado IS NULL)        
		AND (OFIC.Codigo IN (                SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE EMPR_Codigo = @par_EMPR_Codigo AND USOF.USUA_Codigo = @par_Usuario_Consulta               ) OR @par_Usuario_Consulta IS NULL)             
	)    
	SELECT                       
	Obtener,                      
	EMPR_Codigo,                      
	Numero,                      
	TIDO_Codigo,                      
	Numero_Documento,                      
	Numero_Preimpreso,              
	CATA_TIFA_Codigo,               
	Fecha,                      
	OFIC_Factura,                      
	OficinaFactura,                      
                      
	TERC_Cliente,                      
	NombreCliente,                      
	TERC_Facturar,                      
	NombreFacturarA,                      
	CATA_FPVE_Codigo,                      
	CATA_FPVE_Nombre,                      
                      
	Valor_Factura,                      
	Valor_Remesas,                  
	Valor_Otros_Conceptos,                  
	Valor_Impuestos ,                  
	Estado_FAEL,                      
	Estado,                      
	Anulado,                      
	Fecha_Crea,                      
	USUA_Codigo_Crea,                      
	Fecha_Modifica,                      
	USUA_Codigo_Modifica,  
	Causa_Anula,    
	ID_Pruebas,      
	ID_Habilitacion,                   
	ID_Emision,      
	RemesasPaqueteria,
	Interfaz_Contable_Factura,
	Fecha_Interfase_Contable,
	Intentos_Interfase_Contable,
	Mensaje_Error,
	              
	@CantidadRegistros AS TotalRegistros,                      
	@par_NumeroPagina AS PaginaObtener,                      
	@par_RegistrosPagina AS RegistrosPagina                      
                      
	FROM Pagina                      
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                      
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                      
	ORDER BY Numero    
END
GO