﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports System.Transactions
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.MantenimientosPendientes
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen

Namespace Mantenimiento
    Public NotInheritable Class RepositorioPlanEquipoMantenimiento
        Inherits RepositorioBase(Of PlanEquipoMantenimiento)

        Public Overrides Function Consultar(filtro As PlanEquipoMantenimiento) As IEnumerable(Of PlanEquipoMantenimiento)
            Dim lista As New List(Of PlanEquipoMantenimiento)
            Dim item As PlanEquipoMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Pagina) Then
                    If (filtro.Pagina) Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If (filtro.RegistrosPagina) Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.Codigo) Then
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Nombre) And filtro.Nombre <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If
                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado <> -1 Then 'NO APLICA
                        conexion.AgregarParametroSQL("@par_Estado_Codigo", filtro.Estado)
                    End If
                End If
                If Not IsNothing(filtro.CodigoAlterno) Then
                    If filtro.CodigoAlterno <> "" Then
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_encabezado_plan_equipo_mantenimientos]")

                While resultado.Read
                    item = New PlanEquipoMantenimiento(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As PlanEquipoMantenimiento) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.EstadoDocumento.Codigo)
                    conexion.AgregarParametroSQL("@par_USUA_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_encabezado_plan_equipo_mantenimientos]")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While

                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalles
                        detalle.Codigo = entidad.Codigo
                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto And insertoRecorrido Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using
            Return entidad.Codigo
        End Function

        Public Function InsertarDetalle(entidad As DetallePlanEquipoMantenimiento, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_EPEM_Codigo", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_SUMA_SIMA_Codigo", entidad.SistemaMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_SUMA_Codigo", entidad.SubsistemaMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_UNFM_Codigo", entidad.UnidadReferenciaMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Frecuencia_Uso", entidad.FrecuenciaUso)
            contextoConexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
            contextoConexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
            contextoConexion.AgregarParametroSQL("@par_Valor_Mano_Obra", entidad.ValorManoObra)
            contextoConexion.AgregarParametroSQL("@par_Valor_Repuestos", entidad.ValorRepuestos)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_detalle_plan_equipo_mantenimientos]")

            While resultado.Read
                inserto = IIf(resultado.Item("Codigo") > Cero, True, False)
            End While

            resultado.Close()

            For Each detalle In entidad.Referencias
                detalle.Numero = entidad.Codigo
                detalle.CodigoEmpresa = entidad.CodigoEmpresa
                If Not InsertarReferencias(detalle, entidad.SistemaMantenimiento.Codigo, entidad.SubsistemaMantenimiento.Codigo, contextoConexion) Then
                    inserto = False
                    Exit For
                End If
            Next

            Return inserto
        End Function

        Public Function InsertarReferencias(entidad As ReferenciaAlmacenes, Sistema As Integer, Subsistema As Integer, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_EPEM_Codigo", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_SUMA_SIMA_Codigo", Sistema)
            contextoConexion.AgregarParametroSQL("@par_SUMA_Codigo", Subsistema)
            contextoConexion.AgregarParametroSQL("@par_REAL_Codigo", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_referencia_plan_equipo_mantenimientos]")

            While resultado.Read
                inserto = IIf(resultado.Item("Codigo") > Cero, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function


        Public Overrides Function Modificar(entidad As PlanEquipoMantenimiento) As Long

            Dim Modificodetalle As Boolean = True
            Dim ModificoRecargo As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.EstadoDocumento.Codigo)
                    conexion.AgregarParametroSQL("@par_USUA_Modifica", entidad.UsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_modificar_encabezado_plan_equipo_mantenimientos]")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While

                    resultado.Close()

                    If resultado.RecordsAffected > 0 Then
                        Modificar = resultado.RecordsAffected
                        resultado.Close()
                    End If

                    For Each detalle In entidad.Detalles
                        detalle.Codigo = entidad.Codigo
                        If Not InsertarDetalle(detalle, conexion) Then
                            Modificodetalle = False

                            Exit For
                        End If
                    Next

                    If Modificodetalle Then
                        transaccion.Complete()
                    End If

                End Using
            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As PlanEquipoMantenimiento) As PlanEquipoMantenimiento
            Dim item As New PlanEquipoMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_encabezado_Plan_equipo_mantenimientos]")

                While resultado.Read
                    item = New PlanEquipoMantenimiento(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As PlanEquipoMantenimiento) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_anular_encabezado_plan_equipo_mantenimiento]")

                While resultado.Read
                    anulo = IIf(Convert.ToInt16(resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
        Public Function ConsultarEquipos(filtro As PlanEquipoMantenimiento) As IEnumerable(Of EquipoPlanEquipoMantenimiento)
            Dim lista As New List(Of EquipoPlanEquipoMantenimiento)
            Dim item As EquipoPlanEquipoMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_equipo_plan_equipo_mantenimientos]")

                While resultado.Read
                    item = New EquipoPlanEquipoMantenimiento(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Function EliminarEquipos(filtro As PlanEquipoMantenimiento, ByRef conexion As Conexion.DataBaseFactory) As Long
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_eliminar_equipo_plan_equipo_mantenimientos]")
            resultado.Close()
            Return 1
        End Function
        Public Function InsertarEquipos(entidad As IEnumerable(Of EquipoPlanEquipoMantenimiento)) As Long
            Try
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()

                    Dim filtro As PlanEquipoMantenimiento
                    filtro = New PlanEquipoMantenimiento With {.Codigo = entidad(0).PlanMantenimiento.Codigo, .CodigoEmpresa = entidad(0).PlanMantenimiento.CodigoEmpresa}
                    EliminarEquipos(filtro, conexion)
                    For Each Item In entidad
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Item.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EQMA_Codigo", Item.Equipo.Codigo)
                        conexion.AgregarParametroSQL("@par_EPEM_Codigo", Item.PlanMantenimiento.Codigo)
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_equipo_plan_equipo_mantenimientos]")
                        resultado.Close()
                    Next

                End Using
                Return 1
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Function ConsultarMantenimientosPendientes(filtro As MantenimientosPendientes) As IEnumerable(Of MantenimientosPendientes)
            Dim lista As New List(Of MantenimientosPendientes)
            Dim item As MantenimientosPendientes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_EQMA_Codigo", filtro.Codigo)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If (filtro.Pagina) Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If (filtro.RegistrosPagina) Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_mantenimientos_pendientes]")

                While resultado.Read
                    item = New MantenimientosPendientes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function CambiarEstado(filtro As PlanEquipoMantenimiento) As Boolean
            Dim Modifico As Boolean

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_cambiar_estado_plan_mantenimientos]")

                While resultado.Read
                    Modifico = IIf(Convert.ToInt16(resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return Modifico
        End Function
    End Class
End Namespace
