﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General


Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="DetalleCheckListVehiculos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleCheckListVehiculos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleCheckListVehiculos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleCheckListVehiculos"/>
        ''' </summary>
        ''' <param name="lector">   Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "ID")
            NumeroDocumento = Read(lector, "ECLV_Numero")
            Nombre = Read(lector, "Nombre")
            OrdenFormulario = Read(lector, "Orden_Formulario")
            TipoPregunta = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TPCL_Codigo")}
            TipoControl = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TCON_Codigo")}
            Valor = Read(lector, "Valor")
            Observaciones = Read(lector, "Observaciones")
        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property OrdenFormulario As Integer
        <JsonProperty>
        Public Property Valor As String
        <JsonProperty>
        Public Property TipoPregunta As ValorCatalogos
        <JsonProperty>
        Public Property TipoControl As ValorCatalogos
    End Class
End Namespace
