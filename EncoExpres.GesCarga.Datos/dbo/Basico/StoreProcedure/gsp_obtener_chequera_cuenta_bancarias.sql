﻿PRINT 'gsp_obtener_chequera_cuenta_bancarias'
GO 
DROP PROCEDURE gsp_obtener_chequera_cuenta_bancarias
GO
CREATE PROCEDURE gsp_obtener_chequera_cuenta_bancarias
(
 @par_EMPR_Codigo SMALLINT,
 @par_Codigo NUMERIC
)
AS
BEGIN
	SELECT 
	1 AS Obtener,
	CCBA.EMPR_Codigo,
	CCBA.Codigo,
	CCBA.CUBA_Codigo,
	CCBA.Cheque_Inicial,
	CCBA.Cheque_Final,
	CCBA.Cheque_Actual,
	CCBA.Estado,
	CUBA.Nombre AS CuentaBancaria
	FROM
	Chequera_Cuenta_Bancarias CCBA,
	Cuenta_Bancarias CUBA

	WHERE

	CCBA.EMPR_Codigo = @par_EMPR_Codigo 
	AND CCBA.Codigo = @par_Codigo

	AND CCBA.EMPR_Codigo = CUBA.EMPR_Codigo
	AND CCBA.CUBA_Codigo = CUBA.Codigo

END 
GO