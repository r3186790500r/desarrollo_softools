﻿PRINT 'gsp_consultar_items_secciones_formularios_inspecciones'
GO
DROP PROCEDURE gsp_consultar_items_secciones_formularios_inspecciones
GO
CREATE PROCEDURE gsp_consultar_items_secciones_formularios_inspecciones 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC  
)  
AS  
BEGIN  
  
SELECT   
IFIN.EMPR_Codigo,  
IFIN.ENFI_Codigo,  
IFIN.Codigo,  
IFIN.Nombre,  
IFIN.Orden_Formulario,  
IFIN.SFIN_Codigo,  
IFIN.CATA_TCFI_Codigo,  
IFIN.Estado,  
IFIN.Tamaño,  
IFIN.Relevante,  
TCFI.Campo1 AS TipoControl  
FROM  
Detalle_Items_Formularios_Inspecciones IFIN,  
Valor_Catalogos TCFI   
WHERE  
IFIN.EMPR_Codigo =  TCFI.EMPR_Codigo  
AND IFIN.CATA_TCFI_Codigo = TCFI.Codigo  
  
AND IFIN.EMPR_Codigo = @par_EMPR_Codigo  
AND IFIN.ENFI_Codigo = @par_Codigo  
END  
GO