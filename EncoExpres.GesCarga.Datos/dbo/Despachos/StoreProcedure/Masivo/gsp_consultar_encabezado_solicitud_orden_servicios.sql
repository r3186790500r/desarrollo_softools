﻿
PRINT 'gsp_consultar_encabezado_solicitud_orden_servicios'
GO
DROP PROCEDURE gsp_consultar_encabezado_solicitud_orden_servicios 
GO
CREATE PROCEDURE gsp_consultar_encabezado_solicitud_orden_servicios       
(                    
@par_EMPR_Codigo SMALLINT,                             
@par_Numero NUMERIC = NULL,        
@par_Numero_Documento NUMERIC = NULL,                  
@par_Fecha_Inicial DATETIME = NULL,                    
@par_Fecha_Final DATETIME = NULL,                
@par_TERC_Codigo_Cliente NUMERIC = NULL,                    
@par_TIDO_Codigo NUMERIC = NULL,              
@par_Estado NUMERIC = NULL,              
@par_Anulado NUMERIC = NULL,                    
@par_OFIC_Despacha INT = NULL,                    
@par_NumeroPagina INT = NULL,                      
@par_RegistrosPagina INT = NULL,    
@par_ESOS_Numero numeric = NULL                    
)                    
AS                    
BEGIN                    
               
SET @par_Fecha_Inicial = CONVERT(DATE,@par_Fecha_Inicial,101)                    
SET @par_Fecha_Final = DATEADD(HOUR , 23 , @par_Fecha_Final)                    
SET @par_Fecha_Final = DATEADD(MINUTE , 59 , @par_Fecha_Final)                    
SET @par_Fecha_Final = DATEADD(SECOND , 59 , @par_Fecha_Final)                    
                    
 SET NOCOUNT ON;                        
  DECLARE                        
   @CantidadRegistros INT                        
  SELECT @CantidadRegistros = (                        
    SELECT DISTINCT                         
     COUNT(1)                         
    FROM                        
     Encabezado_Solicitud_Orden_Servicios AS ESOS                     
                      
  INNER JOIN Encabezado_Tarifario_Carga_Ventas AS ETCV ON                    
  ESOS.EMPR_Codigo = ETCV.EMPR_Codigo                    
  AND ESOS.ETCV_Numero = ETCV.Numero                    
                    
  INNER JOIN Linea_Negocio_Transporte_Carga AS LNTC ON                     
  ESOS.EMPR_Codigo =  LNTC.EMPR_Codigo                    
  AND ESOS.LNTC_Codigo = LNTC.Codigo                    
                    
  INNER JOIN                     
  Tipo_Linea_Negocio_Carga TLNC ON                    
  ESOS.EMPR_Codigo = TLNC.EMPR_Codigo                     
  AND  ESOS.TLNC_Codigo = TLNC.Codigo                    
                    
  INNER JOIN                     
  Terceros CLIE ON                     
  ESOS.EMPR_Codigo = CLIE.EMPR_Codigo                     
  AND  ESOS.TERC_Codigo_Cliente = CLIE.Codigo                    
                    
  INNER JOIN                     
  Terceros FACT ON                     
  ESOS.EMPR_Codigo = FACT.EMPR_Codigo                     
  AND  ESOS.TERC_Codigo_Facturar = FACT.Codigo                    
                    
  INNER JOIN                     
  Terceros COME ON                     
  ESOS.EMPR_Codigo = COME.EMPR_Codigo                     
  AND  ESOS.TERC_Codigo_Comercial = COME.Codigo                    
                    
  INNER JOIN                     
  Terceros DEST ON                     
  ESOS.EMPR_Codigo = DEST.EMPR_Codigo                     
  AND  ESOS.TERC_Codigo_Destinatario = DEST.Codigo                    
                    
  INNER JOIN                     
  Ciudades CIUD ON                     
  ESOS.EMPR_Codigo = CIUD.EMPR_Codigo                     
  AND  ESOS.CIUD_Codigo_Cargue = CIUD.Codigo                    
                    
  INNER JOIN                     
  Oficinas OFIC ON                     
  ESOS.EMPR_Codigo = OFIC.EMPR_Codigo                     
  AND  ESOS.OFIC_Codigo_Despacha = OFIC.Codigo                    
                    
  INNER JOIN                    
  Usuarios USUA ON                    
  ESOS.EMPR_Codigo = USUA.EMPR_Codigo                    
  AND ESOS.USUA_Codigo_Crea = USUA.Codigo                    
                    
   WHERE                        
   ESOS.EMPR_Codigo = @par_EMPR_Codigo               
   AND ESOS.Numero = ISNULL(@par_Numero, ESOS.Numero)   
   AND ESOS.Numero_Documento = ISNULL(@par_Numero_Documento, ESOS.Numero_Documento)                     
   AND ESOS.Fecha >= ISNULL(@par_Fecha_Inicial, ESOS.Fecha)                    
   AND ESOS.Fecha <= ISNULL(@par_Fecha_Final, ESOS.Fecha)                    
   AND ESOS.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ESOS.TERC_Codigo_Cliente)                    
   AND ESOS.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ESOS.TIDO_Codigo)    
   AND (ISNULL(ESOS.ESOS_Numero,0) = ISNULL(@par_ESOS_Numero,0) OR @par_ESOS_Numero IS NULL)
   AND ESOS.Estado = ISNULL(@par_Estado, ESOS.Estado )                
   AND ESOS.Anulado = ISNULL(@par_Anulado,ESOS.Anulado)                    
     );                               
    WITH Pagina AS                        
(                        
    SELECT                        
  ESOS.EMPR_Codigo,                    
  ESOS.Numero,                    
  ESOS.TIDO_Codigo,                    
  ESOS.Numero_Documento,                    
  ESOS.Fecha,                 
  ESOS.ETCV_Numero,                    
  ESOS.LNTC_Codigo,                    
  ESOS.TLNC_Codigo,                    
  ESOS.TERC_Codigo_Cliente,                    
  ESOS.TERC_Codigo_Facturar,                    
  ESOS.Documento_Cliente,                    
  ESOS.TERC_Codigo_Comercial,                    
  ESOS.Facturar_Despachar,                    
  ESOS.Facturar_Cantidad_Peso_Cumplido,                    
  ESOS.TERC_Codigo_Destinatario,                    
  ESOS.Fecha_Cargue,                    
  ESOS.CIUD_Codigo_Cargue,                    
  ESOS.Direccion_Cargue,                    
  ESOS.Telefono_Cargue,                    
  ESOS.Contacto_Cargue,                    
  ESOS.OFIC_Codigo_Despacha,                    
  ESOS.Observaciones,                    
  ESOS.Aplica_Poliza,             
  ESOS.OFIC_Codigo,               
  ESOS.Estado,                    
  ESOS.Anulado,                    
  LNTC.Nombre AS LineaNegocioTransporteCarga,                    
  TLNC.Nombre AS TipoLineaNegocioCarga,                    
  ESOS.USUA_Codigo_Crea,                    
  ISNULL(ESOS.CATA_ESSO_Codigo,0) As CATA_ESSO_Codigo,                   
  USUA.Nombre As USUA_Crea_Nombre,                    
  CONVERT(VARCHAR,ETCV.Numero) + ' - ' + ETCV.Nombre As NombreTarifario,                    
  ISNULL(CLIE.Razon_Social ,'') + ISNULL(CLIE.Nombre ,'') + ' ' + ISNULL(CLIE.Apellido1 ,'')+ ' ' + ISNULL(CLIE.Apellido2 ,'') AS NombreCliente,                    
  ISNULL(FACT.Razon_Social ,'') + ISNULL(FACT.Nombre ,'') + ' ' + ISNULL(FACT.Apellido1 ,'')+ ' ' + ISNULL(FACT.Apellido2 ,'') AS NombreFacturar,                    
  ISNULL(COME.Razon_Social ,'') + ISNULL(COME.Nombre ,'') + ' ' + ISNULL(COME.Apellido1 ,'')+ ' ' + ISNULL(COME.Apellido2 ,'') AS NombreComercial,                    
  ISNULL(DEST.Razon_Social ,'') + ISNULL(DEST.Nombre ,'') + ' ' + ISNULL(DEST.Apellido1 ,'')+ ' ' + ISNULL(DEST.Apellido2 ,'') AS NombreDestinatario,                    
  CIUD.Nombre AS Ciudad,                    
  OFIC.Nombre AS Oficina,                    
     ROW_NUMBER() OVER(ORDER BY ESOS.Numero desc) AS RowNumber                     
    FROM                        
     Encabezado_Solicitud_Orden_Servicios AS ESOS                     
                      
  LEFT JOIN Encabezado_Tarifario_Carga_Ventas AS ETCV ON                    
  ESOS.EMPR_Codigo = ETCV.EMPR_Codigo                    
  AND ESOS.ETCV_Numero = ETCV.Numero                    
                    
  LEFT JOIN Linea_Negocio_Transporte_Carga AS LNTC ON                     
  ESOS.EMPR_Codigo =  LNTC.EMPR_Codigo                    
  AND ESOS.LNTC_Codigo = LNTC.Codigo                    
                    
  LEFT JOIN                     
  Tipo_Linea_Negocio_Carga TLNC ON                    
  ESOS.EMPR_Codigo = TLNC.EMPR_Codigo                     
  AND  ESOS.TLNC_Codigo = TLNC.Codigo                    
                    
  LEFT JOIN                     
  Terceros CLIE ON                     
  ESOS.EMPR_Codigo = CLIE.EMPR_Codigo                     
  AND  ESOS.TERC_Codigo_Cliente = CLIE.Codigo                    
                    
  LEFT JOIN                     
  Terceros FACT ON                     
  ESOS.EMPR_Codigo = FACT.EMPR_Codigo                     
  AND  ESOS.TERC_Codigo_Facturar = FACT.Codigo                    
                    
  LEFT JOIN                     
  Terceros COME ON                     
  ESOS.EMPR_Codigo = COME.EMPR_Codigo                     
  AND  ESOS.TERC_Codigo_Comercial = COME.Codigo                    
                    
  LEFT JOIN                     
  Terceros DEST ON                     
  ESOS.EMPR_Codigo = DEST.EMPR_Codigo                     
  AND  ESOS.TERC_Codigo_Destinatario = DEST.Codigo                    
                    
  LEFT JOIN                     
  Ciudades CIUD ON                     
  ESOS.EMPR_Codigo = CIUD.EMPR_Codigo                     
  AND  ESOS.CIUD_Codigo_Cargue = CIUD.Codigo                    
                 
  LEFT JOIN                     
  Oficinas OFIC ON                     
  ESOS.EMPR_Codigo = OFIC.EMPR_Codigo                     
  AND  ESOS.OFIC_Codigo= OFIC.Codigo                      
                    
  LEFT JOIN                    
  Usuarios USUA ON                    
  ESOS.EMPR_Codigo = USUA.EMPR_Codigo                    
  AND ESOS.USUA_Codigo_Crea = USUA.Codigo                    
                    
    WHERE                        
   ESOS.EMPR_Codigo = @par_EMPR_Codigo                                  
   AND ESOS.Numero = ISNULL(@par_Numero, ESOS.Numero)                          
   AND ESOS.Numero_Documento = ISNULL(@par_Numero_Documento, ESOS.Numero_Documento)           
   AND ESOS.Fecha >= ISNULL(@par_Fecha_Inicial, ESOS.Fecha)                    
   AND ESOS.Fecha <= ISNULL(@par_Fecha_Final, ESOS.Fecha)                    
   AND ESOS.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ESOS.TERC_Codigo_Cliente)                    
   AND ESOS.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ESOS.TIDO_Codigo)       
   AND (ISNULL(ESOS.ESOS_Numero,0) = ISNULL(@par_ESOS_Numero,0) OR @par_ESOS_Numero IS NULL)
   AND ESOS.Estado = ISNULL(@par_Estado, ESOS.Estado )                
   AND ESOS.Anulado = ISNULL(@par_Anulado,ESOS.Anulado)                    
  )                         
  SELECT DISTINCT                        
        0 As Obtener,                    
  EMPR_Codigo,                    
  Numero,                    
  TIDO_Codigo,                    
  Numero_Documento,                    
  Fecha,                    
  ETCV_Numero,                    
  LNTC_Codigo,                    
  TLNC_Codigo,                    
  TERC_Codigo_Cliente,                    
  TERC_Codigo_Facturar,                    
  Documento_Cliente,                    
  TERC_Codigo_Comercial,                    
  Facturar_Despachar,                    
  Facturar_Cantidad_Peso_Cumplido,                    
  TERC_Codigo_Destinatario,                    
  Fecha_Cargue,                    
  CIUD_Codigo_Cargue,                    
  Direccion_Cargue,                    
  Telefono_Cargue,                    
  Contacto_Cargue,                    
  OFIC_Codigo_Despacha,           
  OFIC_Codigo,                 
  Observaciones,                    
  Aplica_Poliza,                    
  Estado,                    
  Anulado,                    
  LineaNegocioTransporteCarga,                    
  TipoLineaNegocioCarga,                    
  NombreTarifario,                    
  NombreCliente,                    
  NombreFacturar,                    
  NombreComercial,                    
  NombreDestinatario,                    
  Ciudad,                    
  Oficina,                    
  USUA_Codigo_Crea,                    
  CATA_ESSO_Codigo,                  
  USUA_Crea_Nombre,                    
    @CantidadRegistros AS TotalRegistros,                        
    @par_RegistrosPagina AS RegistrosPagina                        
  FROM                        
   Pagina                        
  WHERE      
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                      
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                      
  ORDER BY Numero desc                        
                    
END 