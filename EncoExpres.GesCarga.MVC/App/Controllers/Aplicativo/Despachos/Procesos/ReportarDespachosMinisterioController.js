﻿EncoExpresApp.controller("ReportarDespachosMinisterioCtrl", ['$scope', '$routeParams', '$timeout', 'ReporteMinisterioFactory', 'RemesasFactory', 'ManifiestoFactory', '$linq', 'blockUI', 'OficinasFactory',
    function ($scope, $routeParams, $timeout, ReporteMinisterioFactory, RemesasFactory, ManifiestoFactory, $linq, blockUI, OficinasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Procesos' }, { Nombre: 'Reportar Despachos Ministerio Transporte' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        //var ReportarRemesas = false;
        var ReportarManifiestos = false;
        var ReportarCumplidos = false;
        var ReportarAnulacionRemesas = false;
        var ReportarAnulacionManifiestos = false;
        var ReportarAnulacionCumplidos = false;
        $scope.REPORTAR_RNDC = {
            REMESA: 1,
            REMESA_CUMPLIDA: 2,
            REMESA_ANULADA: 3,
            REMESA_CUMLIDA_ANULADA: 4,
            MANIFIESTO: 5,
            MANIFIESTO_CUMPLIDO: 6,
            MANIFIESTO_ANULADO: 7,
            MANIFIESTO_CUMPLIDO_ANULADO: 8,
            REMESA_CUMPLIDO_INICIAL: 9
        }
        var OPCION_REPORTE_RNDC = 0;
        $scope.ListadoRemesas = [];
        $scope.ListadoManifiestos = [];
        $scope.ListadoRemesasCumplidas = [];
        $scope.ListadoManifiestosCumplidos = [];
        $scope.ListaResultadoFiltroConsulta = [];
        $scope.ListadoRecorridosTotal = [];
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.MostrarMensajeError = false

        $scope.Modelo = {
            NumeroInicial: '',
            NumeroFinal: '',
            FechaInicial: '',
            FechaFinal: '',
            NumeroManifiesto: ''
        }

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + 400115);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        $scope.Imprimir = function () {
            Print();
        };

        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            $scope.ListadoOficinas = [];
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });

                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==-1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas = $scope.Sesion.UsuarioAutenticado.ListadoOficinas
                $scope.ModeloOficina = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }


        $scope.MostrarTab = function (IdTab, OpcionRNDC) {
            if (OPCION_REPORTE_RNDC != OpcionRNDC) {
                LimpirInformacion();
                ocultarTabs();
                $("#" + IdTab).show();
                OPCION_REPORTE_RNDC = OpcionRNDC;
            }
        }

        function ocultarTabs() {
            $("#TapRemesas").hide();
            $("#TapCumplidoRemesa").hide();
            $("#TapRemesasAnuladas").hide();
            $("#TapCumplidosRemesasAnulados").hide();
            $("#TapManifiestos").hide();
            $("#TapCumplidoManifiesto").hide();
            $("#TapManifiestosAnulados").hide();
            $("#TapCumplidosManifiestosAnulados").hide();
        }

        function LimpirInformacion() {
            $scope.FiltroFechaInicial = '';
            $scope.FiltroFechaFinal = '';
            $scope.FiltroNumeroRemesa = '';
            $scope.FiltroNumeroManifiesto = '';
            $scope.totalRegistros = 0
            $scope.totalPaginas = 0
            $scope.ListadoRemesas = [];
            $scope.ListadoManifiestos = [];
            $scope.ListadoRemesasCumplidas = [];
            $scope.ListadoManifiestosCumplidos = [];
        }
        $scope.MostrarTab('TapRemesas', $scope.REPORTAR_RNDC.REMESA);// inicial remesas
        //-------------------------------------------------FUNCIONES--------------------------------------------------------//
        //-- Selecciona o Des selecciona todos los items de la lista correspondiente
        $scope.SlcTodo = function (seleccion) {
            $scope.ListadoRemesas.forEach(function (item) {
                item.Seleccionado = seleccion;
            });
        }
        //-- Selecciona o Des selecciona todos los items de la lista correspondiente
        //-- Gestiona la Carga de las listas correspondientes
        $scope.Buscar = function () {
            switch (OPCION_REPORTE_RNDC) {
                case $scope.REPORTAR_RNDC.REMESA:
                case $scope.REPORTAR_RNDC.REMESA_CUMPLIDA:
                case $scope.REPORTAR_RNDC.REMESA_CUMPLIDO_INICIAL:
                case $scope.REPORTAR_RNDC.REMESA_ANULADA:
                case $scope.REPORTAR_RNDC.REMESA_CUMLIDA_ANULADA:
                    //Pendiente para validacion de datos filtro Remesas
                    break;
                case $scope.REPORTAR_RNDC.MANIFIESTO:
                case $scope.REPORTAR_RNDC.MANIFIESTO_CUMPLIDO:
                case $scope.REPORTAR_RNDC.MANIFIESTO_ANULADO:
                case $scope.REPORTAR_RNDC.MANIFIESTO_CUMPLIDO_ANULADO:
                    //Pendiente para validacion de datos filtro Manifiestos
                    break;
            }
            Find();
        }
        //-- Gestiona la Carga de las listas correspondientes
        /*
        $scope.BuscarManifiesto = function (FechaInicia, FechaFinal, NumeroManifiesto) {
            $scope.ModeloFechaInicial = FechaInicia;
            $scope.ModeloFechaFinal = FechaFinal;
            $scope.ModeloManifiesto = NumeroManifiesto;
            Find();
        }

        $scope.BuscarRemesa = function (FechaInicia, FechaFinal, NumeroRemesa, NumeroManifiesto) {
            $scope.ModeloFechaInicial = FechaInicia;
            $scope.ModeloFechaFinal = FechaFinal;
            $scope.ModeloNumero = NumeroRemesa;
            $scope.ModeloManifiesto = NumeroManifiesto;
            Find();
        }
        */

        function Find() {
            $scope.totalRegistros = 0
            $scope.totalPaginas = 0
            $scope.ListadoRemesas = [];
            blockUI.start('Buscando registros ...');
            //-- Crea Filtro de Envio para cada caso de reporte
            switch (OPCION_REPORTE_RNDC) {
                case $scope.REPORTAR_RNDC.REMESA:
                case $scope.REPORTAR_RNDC.REMESA_CUMPLIDA:
                case $scope.REPORTAR_RNDC.REMESA_CUMPLIDO_INICIAL:
                case $scope.REPORTAR_RNDC.REMESA_ANULADA:
                case $scope.REPORTAR_RNDC.REMESA_CUMLIDA_ANULADA:
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        FechaInicial: $scope.FiltroFechaInicial,
                        FechaFinal: $scope.FiltroFechaFinal,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        OpcionReporte: OPCION_REPORTE_RNDC,
                        Oficina: $scope.ModeloOficina,
                        Remesa: {
                            NumeroDocumento: $scope.FiltroNumeroRemesa,
                            Manifiesto: { NumeroDocumento: $scope.FiltroNumeroManifiesto }
                        }
                    }
                    break;
                case $scope.REPORTAR_RNDC.MANIFIESTO:
                case $scope.REPORTAR_RNDC.MANIFIESTO_CUMPLIDO:
                case $scope.REPORTAR_RNDC.MANIFIESTO_ANULADO:
                case $scope.REPORTAR_RNDC.MANIFIESTO_CUMPLIDO_ANULADO:
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        FechaInicial: $scope.FiltroFechaInicial,
                        FechaFinal: $scope.FiltroFechaFinal,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        OpcionReporte: OPCION_REPORTE_RNDC,
                        Oficina: $scope.ModeloOficina,
                        Manifiesto: {
                            NumeroDocumento: $scope.FiltroNumeroRemesa,
                        }
                    }
                    break;
            }
            //-- Crea Filtro de Envio para cada caso de reporte

            ReporteMinisterioFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            //switch (OPCION_REPORTE_RNDC) {
                            //    case $scope.REPORTAR_RNDC.REMESA:
                            $scope.ListadoRemesas = [];
                            //console.log("Resultado: ", response.data.Datos);
                            response.data.Datos.forEach(function (item) {
                                item.Remesa.Mensaje = item.Mensaje;
                                $scope.ListadoRemesas.push(item.Remesa);
                            });
                            //console.log("Registros Remesa sin Reportar: ", $scope.ListadoRemesas);
                            //break;
                            //}
                            /*----------------------------*/
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }

                        /*
                        if ($scope.opcion == 0) {
                            $scope.ListadoRemesas = [];
                            response.data.Datos.forEach(function (item) {
                                $scope.ListadoRemesas.push(item);
                            });
                        }
                        if ($scope.opcion == 1) {
                            $scope.ListadoManifiestos = [];
                            response.data.Datos.forEach(function (item) {
                                $scope.ListadoManifiestos.push(item);
                            });
                        }
                        if ($scope.opcion == 2) {
                            $scope.ListadoRemesasCumplidas = [];
                            response.data.Datos.forEach(function (item) {
                                $scope.ListadoRemesasCumplidas.push(item);
                            });
                        }
                        if ($scope.opcion == 3) {
                            $scope.ListadoManifiestosCumplidos = [];
                            response.data.Datos.forEach(function (item) {
                                $scope.ListadoManifiestosCumplidos.push(item);
                            });
                        }
                        */
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();

            /*if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Usuario: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        OpcionReporte: OPCION_REPORTE_RNDC,
                        NumeroDocumento: $scope.ModeloNumero,
                        NumeroDocumentoManifiesto: $scope.ModeloManifiesto,
                        FechaInicia: $scope.ModeloFechaInicial,
                        FechaFinal: $scope.ModeloFechaFinal

                    }
                    blockUI.delay = 1000;

                    ReporteMinisterioFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {

                                if ($scope.opcion == 0) {
                                    $scope.ListadoRemesas = [];
                                    response.data.Datos.forEach(function (item) {
                                        $scope.ListadoRemesas.push(item);
                                    });
                                }
                                if ($scope.opcion == 1) {
                                    $scope.ListadoManifiestos = [];
                                    response.data.Datos.forEach(function (item) {
                                        $scope.ListadoManifiestos.push(item);
                                    });
                                }
                                if ($scope.opcion == 2) {
                                    $scope.ListadoRemesasCumplidas = [];
                                    response.data.Datos.forEach(function (item) {
                                        $scope.ListadoRemesasCumplidas.push(item);
                                    });
                                }
                                if ($scope.opcion == 3) {
                                    $scope.ListadoManifiestosCumplidos = [];
                                    response.data.Datos.forEach(function (item) {
                                        $scope.ListadoManifiestosCumplidos.push(item);
                                    });
                                }


                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                    
                }
            }*/
        }

        $scope.ReportarRemesas = function () {
            var ListadoReporteRemesas = [];//listado de las remesas a reportar
            $scope.MensajesError = [];
            for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                if ($scope.ListadoRemesas[i].Seleccionado == true) {
                    console.log("Remesa: ", $scope.ListadoRemesas[i]);
                    ListadoReporteRemesas.push($scope.ListadoRemesas[i]);
                }
            }

            if (ListadoReporteRemesas.length <= 0) {
                $scope.MensajesError.push("Debe asignar al menos una remesa");
            }
            else {
                console.log("remesas a reportar: ", ListadoReporteRemesas);
                EnviarReporteRemesa(ListadoReporteRemesas);
            }
        }

        function EnviarReporteRemesa(ListadoReporteRemesas) {
            BloqueoPantalla.start('Reportando documentos');

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                OpcionReporte: OPCION_REPORTE_RNDC,
                DetalleReporteManifiesto: ListadoReporteRemesas,
                DetalleReporteRemesas: ListadoReporteRemesas
            }

            ReporteMinisterioFactory.ReportarRNDC(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        Find();
                        BloqueoPantalla.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.MarcarRecorridos = function (RecorridosSeleccionados, opcion) {
            /*
            $scope.opcion = opcion;

            $scope.RecorridosSeleccionados = RecorridosSeleccionados;
            if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                if ($scope.RecorridosSeleccionados == true) {
                    $scope.ListaResultadoFiltroConsulta.forEach(function (itemTotal) {
                        itemTotal.Seleccionado = true;
                    });
                    $scope.ListadoRemesas.forEach(function (item) {
                        item.Seleccionado = true;
                    });
                } else {
                    $scope.ListaResultadoFiltroConsulta.forEach(function (itemTotal) {
                        itemTotal.Seleccionado = false;
                    });
                    $scope.ListadoRemesas.forEach(function (item) {
                        item.Seleccionado = false;
                    });
                }

            }
            else {
                if ($scope.opcion == 0) {
                    if ($scope.RecorridosSeleccionados == true) {
                        $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                            itemTotal.Seleccionado = true;
                        });
                        $scope.ListadoRemesas.forEach(function (item) {
                            item.Seleccionado = true;
                        });
                    } else {
                        $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                            itemTotal.Seleccionado = false;
                        });
                        $scope.ListadoRemesas.forEach(function (item) {
                            item.Seleccionado = false;
                        });
                    }
                }
                if ($scope.opcion == 1) {
                    if ($scope.RecorridosSeleccionados == true) {
                        $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                            itemTotal.Seleccionado = true;
                        });
                        $scope.ListadoManifiestos.forEach(function (item) {
                            item.Seleccionado = true;
                        });
                    } else {
                        $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                            itemTotal.Seleccionado = false;
                        });
                        $scope.ListadoManifiestos.forEach(function (item) {
                            item.Seleccionado = false;
                        });
                    }
                }
                if ($scope.opcion == 2) {
                    if ($scope.RecorridosSeleccionados == true) {
                        $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                            itemTotal.Seleccionado = true;
                        });
                        $scope.ListadoRemesasCumplidas.forEach(function (item) {
                            item.Seleccionado = true;
                        });
                    } else {
                        $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                            itemTotal.Seleccionado = false;
                        });
                        $scope.ListadoRemesasCumplidas.forEach(function (item) {
                            item.Seleccionado = false;
                        });
                    }
                }
                if ($scope.opcion == 3) {
                    if ($scope.RecorridosSeleccionados == true) {
                        $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                            itemTotal.Seleccionado = true;
                        });
                        $scope.ListadoManifiestosCumplidos.forEach(function (item) {
                            item.Seleccionado = true;
                        });
                    } else {
                        $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                            itemTotal.Seleccionado = false;
                        });
                        $scope.ListadoManifiestosCumplidos.forEach(function (item) {
                            item.Seleccionado = false;
                        });
                    }
                }

            }

            $scope.Calcular();
            */
        }


        $scope.Guardar = function (Opcion, listado) {
            /*$scope.opcion = 0;
            $scope.opcion = Opcion;
            $scope.listadoAuxiliar = listado
            $scope.listadoArchivosSeleccionados = [];

            $scope.listadoAuxiliar.forEach(function (item) {
                if (item.Seleccionado) {
                    var Documento = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Numero: item.NumeroDocumento
                    }
                    $scope.listadoAuxiliar.push(Documento)
                }
            })
            */


        }


    }]);