﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Negocio.Gesphone

''' <summary>
''' Controlador <see cref="ItemCheckListVehiculosController"/>
''' </summary>
<Authorize>
Public Class ItemCheckListVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ItemCheckListVehiculos) As Respuesta(Of IEnumerable(Of ItemCheckListVehiculos))
        Return New LogicaItemCheckListVehiculos(CapaPersistenciaItemCheckListVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ItemCheckListVehiculos) As Respuesta(Of ItemCheckListVehiculos)
        Return New LogicaItemCheckListVehiculos(CapaPersistenciaItemCheckListVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ItemCheckListVehiculos) As Respuesta(Of Long)
        Return New LogicaItemCheckListVehiculos(CapaPersistenciaItemCheckListVehiculos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ItemCheckListVehiculos) As Respuesta(Of Boolean)
        Return New LogicaItemCheckListVehiculos(CapaPersistenciaItemCheckListVehiculos).Anular(entidad)
    End Function
End Class
