﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Repositorio.Gesphone

Namespace Gesphone
    Public NotInheritable Class PersistenciaItemCheckListVehiculos
        Implements IPersistenciaBase(Of ItemCheckListVehiculos)

        Public Function Consultar(filtro As ItemCheckListVehiculos) As IEnumerable(Of ItemCheckListVehiculos) Implements IPersistenciaBase(Of ItemCheckListVehiculos).Consultar
            Return New RepositorioItemCheckListVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ItemCheckListVehiculos) As Long Implements IPersistenciaBase(Of ItemCheckListVehiculos).Insertar
            Return New RepositorioItemCheckListVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ItemCheckListVehiculos) As Long Implements IPersistenciaBase(Of ItemCheckListVehiculos).Modificar
            Return New RepositorioItemCheckListVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ItemCheckListVehiculos) As ItemCheckListVehiculos Implements IPersistenciaBase(Of ItemCheckListVehiculos).Obtener
            Return New RepositorioItemCheckListVehiculos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ItemCheckListVehiculos) As Boolean
            Return New RepositorioItemCheckListVehiculos().Anular(entidad)
        End Function
    End Class

End Namespace

