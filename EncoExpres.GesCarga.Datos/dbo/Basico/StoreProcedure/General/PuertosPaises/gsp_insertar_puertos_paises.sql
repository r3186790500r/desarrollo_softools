﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 04/10/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_insertar_puertos_paises'
GO
DROP PROCEDURE gsp_insertar_puertos_paises
GO
CREATE PROCEDURE gsp_insertar_puertos_paises 
(        
@par_EMPR_Codigo SMALLINT,     
@par_PAIS_Codigo NUMERIC,          
@par_Nombre VARCHAR(50),
@par_Contacto VARCHAR(50),     
@par_Nombre_Ciudad VARCHAR(50),       
@par_Direccion VARCHAR(150),        
@par_Telefono VARCHAR(20),   
@par_Observaciones VARCHAR(500),         
@par_Estado SMALLINT,        
@par_USUA_Codigo_Crea SMALLINT 
)        
AS         
BEGIN                 
 INSERT INTO Puertos_Paises        
 (        
  EMPR_Codigo,  
  PAIS_Codigo,               
  Nombre,  
  Contacto,  
  Ciudad,      
  Direccion,        
  Telefono,
  Observaciones,              
  Estado,        
  USUA_Codigo_Crea,        
  Fecha_Crea      
 )        
 VALUES        
 (        
  @par_EMPR_Codigo, 
  @par_PAIS_Codigo,                
  @par_Nombre,  
  @par_Contacto,       
  @par_Nombre_Ciudad,        
  @par_Direccion,        
  @par_Telefono,  
  @par_Observaciones,               
  @par_Estado,        
  @par_USUA_Codigo_Crea,        
  GETDATE()    
 )        
 SELECT Codigo = @@IDENTITY        
END        
GO