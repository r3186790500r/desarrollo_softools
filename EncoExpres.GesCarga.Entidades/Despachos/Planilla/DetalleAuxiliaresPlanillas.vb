﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Despachos.Planilla
    <JsonObject>
    Public NotInheritable Class DetalleAuxiliaresPlanillas
        Inherits Base

        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleAuxiliaresPlanillas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            Funcionario = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Funcionario"), .Nombre = Read(lector, "Funcionario")}
            NumeroHorasTrabajadas = Read(lector, "Numero_Horas_Trabajadas")
            Valor = Read(lector, "Valor")
            Observaciones = Read(lector, "Observaciones")
            Aforador = Read(lector, "Aforador")
        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Funcionario As Tercero
        <JsonProperty>
        Public Property NumeroHorasTrabajadas As Integer
        <JsonProperty>
        Public Property Valor As Double
        <JsonProperty>
        Public Property Observaciones As String
        <JsonProperty>
        Public Property Aforador As Integer
    End Class
End Namespace
